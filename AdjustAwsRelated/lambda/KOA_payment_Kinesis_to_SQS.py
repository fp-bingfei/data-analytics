from __future__ import print_function

import json
import boto3
import base64
import traceback
import logging
import datetime
import time

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

#(app_id, os, app_token, event_token
#app_info = [('koa.global.prod', 'iPhone OS', 'fpkxwvp0afwg', 'bt76qn'),
#            ('koa.global.prod', 'android', 'mf4dlnm4a8lc', 'ic7u71')]
#events = ['payment']
s3 = boto3.client('s3',
                  aws_access_key_id = 'AKIAJRKDNC52OAINDWKA',
                  aws_secret_access_key = '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')
response = s3.get_object(Bucket = 'com.funplus.public',
                         Key = 'dev/koa/test_users_000')
test_users = response['Body'].read().split()


def lambda_handler(event, context):

    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName='SQS_KOA_payment_adjust')

    for record in event['Records']:
        try:
            payload = base64.b64decode(record['kinesis']['data'])
            #logger.debug('payload: {}'.format(payload))
            data_dict = json.loads(payload)

            if not (data_dict['msg']['event'] == 'payment' and
                            data_dict['msg']['app_id'] == 'koa.global.prod' and
                            data_dict['msg']['user_id'] not in test_users):
                #logger.debug('Message {} is discarded'.format(payload))
                continue

            properties = data_dict['msg']['properties']

            result = {"s2s": 1}
            result['ip_address'] = data_dict['ip']

            if properties['os'] == 'iPhone OS':
                result['idfa'] = properties['idfa']
                result['idfv'] = properties['idfv']
                result['app_token'] = 'fpkxwvp0afwg'
                result['event_token'] = 'wc17lj'
            elif properties['os'] == 'android':
                result['gps_adid'] = properties['gaid']
                result['android_id'] = properties['android_id']
                result['app_token'] = 'mf4dlnm4a8lc'
                result['event_token'] = 'r03j54'
            else:
                logger.error("Message {} doesn't has correct os info".format(payload))
                continue

            result['revenue'] = properties['amount']
            result['currency'] = properties['currency']
            result['device_name'] = properties['device']
            ts = min(long(data_dict['msg']['ts']), long(time.time() * 1000)) / 1000
            result['created_at'] = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%dT%H:%M:%SZ+0000') #2006-01-02T15:04:05Z-0700

            response = queue.send_message(MessageBody = json.dumps(result))

            #logger.debug(response.get('MessageId'))
        except Exception as e:
            logger.error('Record: {} \n Error happened for {}'.format(record, traceback.format_exc()))
            continue



from __future__ import print_function

import json
import boto3
import time
import base64
import traceback
import logging
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

redshift_table_columns = ['adid', 'app_id', 'app_version', 'device_name', 'device_type', 'os_name',
                          'os_version', 'idfa', 'idfv', 'gps_adid', 'android_id', 'sdk_version', 'ip_address',
                          'country', 'city', 'language', 'isp', 'tracker', 'tracker_name',
                          'last_tracker_name', 'rejection_reason', 'click_id', 'search_term',
                          'referrer', 'click_time', 'engagement_time', 'installed_at', 'created_at',
                          'received_at', 'timezone', 'conversion_duration', 'time_spent', 'last_time_spent',
                          'lifetime_session_count', 'event', 'fp_app_id', 'user_id']


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]


def lambda_handler(event, context):
    clusters = {
        'diandian': {
            'delivery_stream': 'bi-adjust-callback-diandian-redshift-delivery-stream',
            'fp_app_ids': {
                "crazyplanets.global.prod",
                "ffs.amazon.prod",
                "ffs.flexion.prod",
                "ffs.global.prod",
                "ffs.th.prod",
                "lc.global.prod",
                "lc_patch.global.prod",
                "loe.global.prod",
                "poker.ar.prod",
                "sandigma.global.prod",
                "undergroundguardian.global.prod",
            },
            'json_lst': []
        },
        'funplus': {
            'delivery_stream': 'bi-adjust-callback-funplus-redshift-delivery-stream',
            'fp_app_ids': {
                "koa.global.prod",
                "loe.global.prod",
                "wartide.global.prod",
            },
            'json_lst': []
        }
    }

    firehose_client = boto3.client('firehose')

    for record in event['Records']:
        try:
            payload = base64.b64decode(record['kinesis']['data'])
            logger.info('payload: {}'.format(payload))
            data_dict = json.loads(payload.replace("'", '"'))

            fp_app_id = data_dict.get('fp_app_id', '')
            tracker = data_dict.get('tracker', '')
            tracker_name = data_dict.get('tracker_name', '')
            adid = data_dict.get('adid', '')
            app_id = data_dict.get('app_id', '')
            ip_address = data_dict.get('ip_address', '')
            country = data_dict.get('country', '')

            if not (fp_app_id and tracker and tracker_name and adid and app_id and ip_address and country):
                logger.warning("Missing required arguments: fp_app_id=%s, tracker=%s, tracker_name=%s, adid=%s, app_id=%s, ip_address=%s, country=%s",
                               fp_app_id, tracker, tracker_name, adid, app_id, ip_address, country)
                continue

            data_dict['received_at'] = str(int(time.time()))

            # pick first one that exist from the candidates of userid, snsid, fpid, user_id (in order)
            if data_dict.get('event') == 'open':

                user_id = next((item for item in [data_dict.get('userid', None),
                                                  data_dict.get('snsid', None),
                                                  data_dict.get('fpid', None),
                                                  data_dict.get('user_id', None)] if item), None)
                if not user_id:
                    logger.warning("Missing required arguments for first open: userid=%s, snsid=%s, fpid=%s, user_id=%s,",
                                   data_dict.get('userid', None), data_dict.get('snsid', None), data_dict.get('fpid', None), data_dict.get('user_id', None))
                    continue

                data_dict['user_id'] = user_id

            for k in redshift_table_columns:
                # make sure contains all the keys needed,
                # didn't use defaultdict for speeding up
                if k not in data_dict:
                    data_dict[k] = ''

            json_str_data = json.dumps(data_dict)
            logger.info('json_str: {}'.format(json_str_data))

            # put the json_str_data into the cluster's json_lst it belongs
            for cluster_key, v in clusters.items():
                if fp_app_id in v['fp_app_ids']:
                    clusters[cluster_key]['json_lst'].append(json_str_data)
                    break

        except Exception as e:
            logger.error('Record: {} \n Error happened for {}'.format(record, traceback.format_exc()))
            continue

    for v in clusters.values():
        json_list = v['json_lst']
        delivery_stream_name = v['delivery_stream']
        for json_sub_list in chunks(json_list, 500):
            try:
                records = []
                for json_str_data in json_sub_list:
                    d = json.loads(json_str_data)
                    data_str_lst = []
                    for k in redshift_table_columns:
                        column_value = str(d[k]).replace('|', '').replace('\n', '').replace('\r', '').replace('\t', ' ').strip()
                        if k == 'device_name':
                            column_value = '{}'.format(column_value[:64])
                        data_str_lst.append(column_value)
                    data = '|'.join(data_str_lst)
                    data += '\n'
                    records.append({'Data': data})
                response = firehose_client.put_record_batch(
                    DeliveryStreamName=delivery_stream_name,
                    Records=records
                )
            except Exception as e:
                logger.error('Request to Firehose {}, (with {} Records)  Error happened for {}'.format(delivery_stream_name, len(json_sub_list), traceback.format_exc()))
                continue
    return 'OK'
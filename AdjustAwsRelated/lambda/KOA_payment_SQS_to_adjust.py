import boto3
import urllib
import urllib2
import json
import logging
import traceback

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}
url = 'https://s2s.adjust.com/event'

def lambda_handler(event, context):
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName='SQS_KOA_payment_adjust')
    dead_queue = sqs.get_queue_by_name(QueueName='SQS_KOA_payment_adjust_failed')
    i = 0

    while i < 15:

        messages = queue.receive_messages(MaxNumberOfMessages = 10, WaitTimeSeconds = 10)
        if len(messages) == 0:
            break

        for message in messages:
            logger.debug('message: {}'.format(message))
            try:
                data = str(message.body)
                payload = urllib.urlencode(json.loads(data))
                req = urllib2.Request(url, payload, headers = hdr)
                response = urllib2.urlopen(req)
                result = response.getcode()
                if result == 200:
                    message.delete()
                    logger.debug('Message being send to adjust correctly')
                else:
                    logger.error('Error when posting to adjust server: {}'.format(response.read()))
            except urllib2.HTTPError as e:
                dead_queue.send_message(MessageBody = message.body,
                                        MessageAttributes = {"HTTPError": {
                                            "StringValue": str(e.code),
                                            "DataType": "String"
                                        }
                                        })
                message.delete()
                logger.error('HTTP ERROR: {} \n Error happened for {}'.format(message.body, traceback.format_exc()))
            except Exception as e:
                dead_queue.send_message(MessageBody = message.body)
                message.delete()
                logger.error('ERROR: {} \n Error happened for {}'.format(message.body, traceback.format_exc()))

        i += 1


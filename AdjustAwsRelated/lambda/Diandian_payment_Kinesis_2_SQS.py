from __future__ import print_function

import json
import boto3
import base64
import traceback
import logging
import datetime
import time

logger = logging.getLogger()
logger.setLevel(logging.WARNING)

# (app_id, os, app_token, event_token
# app_info = [('koa.global.prod', 'iPhone OS', 'fpkxwvp0afwg', 'bt76qn'),
#            ('koa.global.prod', 'android', 'mf4dlnm4a8lc', 'ic7u71')]
# events = ['payment']
# s3 = boto3.client('s3',
#                   aws_access_key_id='AKIAJRKDNC52OAINDWKA',
#                   aws_secret_access_key='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')
# response = s3.get_object(Bucket='com.funplus.public',
#                          Key='dev/koa/test_users_000')
# test_users = response['Body'].read().split()

config = {
    'ffs.global.prod': {
        'os': {
            'ios': {
                'app_token': 'eqgwx9hq8uvx',
                'event_token': 'jndn0j',
            },
            'android': {
                'app_token': 'x4e5kgunchqq',
                'event_token': 'yacadr',
            }
        },
        'amount_multi_factor': 0.01,
    },
    'valiantforce.global.prod': {
        'os': {
            'ios': {
                'app_token': 'oo8d15snslq8',
                'event_token': 'gugad5',
            },
            'android': {
                'app_token': 'bclzojrv1clc',
                'event_token': 'c5ujyt',
            }
        },
        'amount_multi_factor': 1,
    }
}


def get_os_standard_name(os_string):
    os_string = os_string.lower()
    android_tags = ['android']
    ios_tags = ['ios', 'iphone']
    for os_name, os_tags in (('android', android_tags), ('ios', ios_tags)):
        if any(tag in os_string for tag in os_tags):
            return os_name
    return 'unknown'


def lambda_handler(event, context):
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName='SQS_diandian_payment_adjust')

    for record in event['Records']:
        try:
            payload = base64.b64decode(record['kinesis']['data'])
            # logger.debug('payload: {}'.format(payload))
            data_dict = json.loads(payload)
            if 'msg' in data_dict:
                data_dict['msg']['properties']['ip'] = data_dict['ip']
                data_dict = data_dict['msg']

            if not (data_dict['event'] == 'payment' and
                            data_dict['app_id'] in config.keys()):
                # logger.debug('Message {} is discarded'.format(payload))
                continue

            app_id = data_dict['app_id']
            properties = data_dict['properties']

            result = dict(s2s=1)
            result['ip_address'] = properties['ip']
            os_name = get_os_standard_name(properties['os'])
            result['app_token'] = config[app_id]['os'][os_name]['app_token']
            result['event_token'] = config[app_id]['os'][os_name]['event_token']

            if os_name == 'ios':
                result['idfa'] = properties['idfa']
                result['idfv'] = properties['idfv']
            elif os_name == 'android':
                result['gps_adid'] = properties['gaid']
                result['android_id'] = properties['android_id']
            else:
                logger.error("Message {} doesn't has correct os info".format(payload))
                continue

            result['revenue'] = format(float(properties['amount']) * float(config[app_id]['amount_multi_factor']), '.2f')
            result['currency'] = properties['currency']
            result['device_name'] = properties['device']
            ts = min(long(data_dict['ts']), long(time.time() * 1000)) / 1000
            result['created_at'] = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%dT%H:%M:%SZ+0000')  # 2006-01-02T15:04:05Z-0700

            response = queue.send_message(MessageBody=json.dumps(result))

            # logger.debug(response.get('MessageId'))
        except Exception as e:
            logger.error('Record: {} \n Error happened for {}'.format(record, traceback.format_exc()))
            continue

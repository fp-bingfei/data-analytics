{
    "StreamName": "adjust_kinesis_stream",

    #set($params = $input.params().get("querystring"))
    #set($params['event'] = 'open')
    #set($qu = '"')
    #set($result = "{#foreach($paramName in $params.keySet()) '$paramName': '$util.escapeJavaScript($params.get($paramName))' #if($foreach.hasNext),#end #end}")
    "Data": $qu$util.base64Encode("$result")$qu,

    "PartitionKey": "adjust"
}
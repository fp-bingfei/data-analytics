CREATE SCHEMA IF NOT EXISTS raw_data;

CREATE TABLE IF NOT EXISTS raw_data.cost (
  date      DATE NOT NULL ENCODE DELTA,
  app_id               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  country VARCHAR(64) ENCODE BYTEDICT DEFAULT '',
  install_source    VARCHAR(1024) ENCODE BYTEDICT DEFAULT '',
  os                VARCHAR(32) ENCODE BYTEDICT DEFAULT '',
  amount        decimal(14,4) DEFAULT 0 
) SORTKEY(date, app_id) ;


COMMIT;

TRUNCATE TABLE raw_data.cost;

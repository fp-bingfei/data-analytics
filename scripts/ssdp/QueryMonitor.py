import MySQLdb as mdb
import yaml
import pprint
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import Encoders
import os
import commands
import HTML
import datetime
import time
import httplib
import urllib

class QueryMonitor:

    def __init__(self):
        self.results = {}
        self.yamls = ['farm_1_2.yaml']

    def run(self):
        for filename in self.yamls:
            self.setDBParams(filename)
            conn = self.getConnection()

            # check running query
            result = self.checkRunningQuery(conn)

            # execute submitted query on idle
            if (result >= 1):
                self.execSubmittedQuery(conn)

            # clean timeout query
            self.cleanTimeoutQuery(conn)

    def checkRunningQuery(self, conn):
        cursor = conn.cursor()
        result = 0

        command = "select * from ssdp.query_plan where query_status=1 order by submit_ts limit 1;"
        try:
            cursor.execute(command)
            record = cursor.fetchone()

            # check query status
            if record:
                # check process
                result,output = commands.getstatusoutput("ps -ef | grep %s | grep -v grep" % (record[5]))
                if (result >= 1):
                    # update query status and execution duration
                    status,output = commands.getstatusoutput("grep -i 'exception' /mnt/ssdp/monitor/running/%s.log" % (record[5]))
                    if (status >= 1):
                        query_status = 2
                    else:
                        query_status = 3
                    exec_start = int(os.path.getmtime("/mnt/ssdp/monitor/running/%s" % (record[5])))
                    exec_end = int(os.path.getmtime("/mnt/ssdp/monitor/running/%s.log" % (record[5])))
                    exec_duration = exec_end - exec_start
                    command = "update ssdp.query_plan set query_status=%d, exec_duration=%d where id=%d" % (query_status, exec_duration, record[0])
                    cursor.execute(command)
                    conn.commit()
                    # move query file and log file to history
                    os.rename("/mnt/ssdp/monitor/running/%s" % (record[5]), "/mnt/ssdp/monitor/history/%s" % (record[5]))
                    os.rename("/mnt/ssdp/monitor/running/%s.log" % (record[5]), "/mnt/ssdp/monitor/history/%s.log" % (record[5]))
                    # send email
                    # submit_ts, query, temp_table, exec_duration, query_status, email, logfile
                    self.sendMail(record[2], record[4], record[5], exec_duration, query_status, record[7], "/mnt/ssdp/monitor/history/%s.log" % (record[5]))
            else:
                # set default
                result = 1
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def execSubmittedQuery(self, conn):
        # check current hour
        hour = datetime.datetime.now().hour
        if (hour >= 1 and hour <= 12):
            return

        cursor = conn.cursor()

        command = "select * from ssdp.query_plan where query_status=0 order by submit_ts limit 1;"
        try:
            cursor.execute(command)
            record = cursor.fetchone()

            # Generate query file
            if (record and record[4]):
                query = """
USE %s;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;
SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;
SET hive.stats.autogather=false;

DROP TABLE IF EXISTS %s;

CREATE TABLE %s
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t'
    STORED AS PARQUET
    TBLPROPERTIES ('serialization.null.format'='')
    AS
%s
"""
                # generate query file
                f = open('/mnt/ssdp/monitor/running/' + record[5], 'w')
                print >> f, query % (record[3], record[5], record[5], record[4])
                f.close()
                # update query status
                command = "update ssdp.query_plan set query_status=1 where id=%d" % (record[0])
                cursor.execute(command)
                conn.commit()
                # execute query
                os.system("nohup hive -f /mnt/ssdp/monitor/running/%s > /mnt/ssdp/monitor/running/%s.log 2>&1 &" % (record[5], record[5]))
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

    def cleanTimeoutQuery(self, conn):
        # check current hour
        hour = datetime.datetime.now().hour
        if (hour >= 1 and hour <= 12):
            return

        cursor = conn.cursor()

        command = "select * from ssdp.query_plan where (query_status=2 or query_status=3) and save_days>0 order by submit_ts;"
        try:
            cursor.execute(command)
            records = cursor.fetchall()

            # clean temp table 
            for record in records:
                # check time diff
                timeDiff = (datetime.datetime.now() - record[2]).days
                if (timeDiff > record[9]):
                    if (record[6] == 2):
                        # drop temp table
                        os.system("nohup hive -e 'drop table %s' >> /mnt/ssdp/monitor/history/%s.log 2>&1 &" % (record[5], record[5]))
                    # update save days
                    command = "update ssdp.query_plan set save_days=0 where id=%d" % (record[0])
                    cursor.execute(command)
                    conn.commit()
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

    def getConnection(self):
        conn = mdb.connect(self.db_host, self.db_username, self.db_password, self.db_name)
        return conn

    def setDBParams(self, filename):
        #filename = '/mnt/funplus/data-analytics/python/conf/' + filename
        filename = '/mnt/ssdp/monitor/scripts/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['db-props']['db_host']
        self.db_port = confMap['tasks']['db-props']['db_port']
        self.db_name = confMap['tasks']['db-props']['db_name']
        self.db_username = confMap['tasks']['db-props']['db_username']
        self.db_password = confMap['tasks']['db-props']['db_password']

    def sendMail(self, submit_ts, query, temp_table, exec_duration, query_status, email, logfile):
        # check email
        if not email:
            return

        footer = "<p>*Please use the temp table to make query.</p>"

        table_header = ['Submit Time', 'Query', 'Temp Table', 'Duration','Query Status']

        t = HTML.Table(header_row = table_header)
        color = 'AliceBlue'
        tmp_row = []
        # Submit Time
        value = submit_ts
        tmp_row.append(HTML.TableCell(value, bgcolor = color, width = '80px', align = 'center'))
        # Query
        value = query
        tmp_row.append(HTML.TableCell(value, bgcolor = color))
        # Temp Table
        value = temp_table
        tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
        # Duration
        value = '%ds' % exec_duration
        tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
        # Query Status
        if (query_status == 2):
            value = "Completed"
        else:
            value = "Failed"
        tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))

        # Append the row
        t.rows.append(tmp_row)

        # Generate HTML
        htmlcode = str(t) + footer
        #print htmlcode

        # Send Email
        msg = MIMEMultipart()
        msg['Subject'] = 'Query Monitor ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "SSDP<tableau_admin@funplus.com>"
        msg['To'] = email

        # attach html code
        html = MIMEText(htmlcode, 'html')
        msg.attach(html)

        # attach log file
        part = MIMEBase('application', 'octet-stream')
        part.set_payload( open(logfile, 'rb').read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(logfile)))
        msg.attach(part)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], email.split(','), msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue

def main():
    qm = QueryMonitor()
    qm.run()

if __name__ == '__main__':
    main()
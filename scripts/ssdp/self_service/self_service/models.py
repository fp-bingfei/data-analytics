__author__ = 'Jia Ting'

 # from __future__ import unicode_literals

from django.db import models


class QueryPlan(models.Model):
    user_name = models.CharField(max_length=255, blank=True, null=True)
    submit_ts = models.DateTimeField()
    db_name = models.CharField(max_length=255, blank=True, null=True)
    query = models.TextField(blank=True, null=True)
    temp_table = models.CharField(max_length=255, blank=True, null=True)
    query_status = models.IntegerField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    exec_duration = models.IntegerField(blank=True, null=True)
    save_days = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'query_plan'

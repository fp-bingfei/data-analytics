__author__ = 'Jia Ting'
# -*- coding: utf-8 -*-
import time
from django.http import HttpResponse
from django.shortcuts import render
from model_app.models import QueryPlan
from django.core.context_processors import csrf


def index(request):
    context = {}
    result_list = QueryPlan.objects.all().order_by('-submit_ts')
    context['queries'] = result_list
    context['message'] = ""
    return render(request, 'index.html', context)


def query_list(request):
    context = {}
    result_list = QueryPlan.objects.all().order_by('-submit_ts')
    context['queries'] = result_list
    return render(request, 'list.html', context)


def testdb(request):
    response = ""
    list = QueryPlan.objects.all()
    for var in list:
        response += var.user_name + " / "
    return HttpResponse("<p>" + response + "</p>")


def submit_query(request):
    request.encoding = 'utf-8'
    message = ""
    hql = ""
    save_days = 1
    database = ""
    user_email = ""

    if 'input-sql' in request.POST:
        hql = request.POST['input-sql']
    else:
        message = "Please input Hive Sql!"

    if 'input-save-days' in request.POST:
        save_days = request.POST['input-save-days']
    else:
        message = "Please select save days!"

    if 'input-database' in request.POST:
        database = request.POST['input-database']
    else:
        message = "Please select database!"

    if 'input-email' in request.POST:
        user_email = request.POST['input-email']
    else:
        message = "Please input your email!"

    if hql == "" or user_email == "":
        message = "Hive SQL or Notify Email should not be empty, please check it!"

    # message is "" means all parameters are okay
    if message == "":
        try:
            current_ts = time.strftime('%Y-%m-%d %X', time.localtime())
            temp_table_name = 'farm_pm.gen_temp_' + str(int(time.time()))
            query_plan = QueryPlan(user_name='farm_pm', submit_ts=current_ts, db_name=database, query=hql,
                                   save_days=int(save_days), query_status=0, email=user_email, exec_duration=0,
                                   temp_table=temp_table_name)
            query_plan.save()
        except Exception as e:
            print e
            print('Cat not save query_plan record to DB: ' + hql + '; ' + save_days + '; ' + user_email)

    context = {}
    context.update(csrf(request))
    result_list = QueryPlan.objects.all().order_by('-submit_ts')
    context['queries'] = result_list
    context['message'] = message
    return render(request, 'index.html', context)

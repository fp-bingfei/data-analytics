from __future__ import unicode_literals

from django.apps import AppConfig


class ModelAppConfig(AppConfig):
    name = 'model_app'

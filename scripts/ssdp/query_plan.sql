CREATE SCHEMA IF NOT EXISTS ssdp;

DROP TABLE IF EXISTS ssdp.query_plan;
CREATE TABLE ssdp.query_plan (
    id INT NOT NULL AUTO_INCREMENT,                     -- primary key
    user_name VARCHAR(255),                             -- user name
    submit_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,      -- submit timestamp
    db_name VARCHAR(255),                               -- hive db
    query TEXT,                                         -- query string
    temp_table VARCHAR(255),                            -- the temp table name, will be cleaned based on save days
    query_status TINYINT,                               -- query status, such as 0 submitted, 1 running, 2 completed, 3 failed
    email VARCHAR(255),                                 -- email list seperated by comma
    exec_duration INT,                                  -- execution duration
    save_days INT,                                      -- save days for generated temp table, -1 means forever, 0 means cleaned
    PRIMARY KEY (id)
);

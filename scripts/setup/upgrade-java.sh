sudo mkdir -p /usr/java
cd /usr/java
sudo wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u31-b13/jdk-8u31-linux-x64.tar.gz"
sudo tar xzf jdk-8u31-linux-x64.tar.gz
sudo alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_31/bin/java 2
option=$(echo 1 | sudo alternatives --config java | grep /usr/java/jdk1.8.0_31/bin/java| awk '{print $1}')
echo $option | sudo alternatives --config java
sudo alternatives --install /usr/bin/jar jar /usr/java/jdk1.8.0_31/bin/jar 2
sudo alternatives --install /usr/bin/javac javac /usr/java/jdk1.8.0_31/bin/javac 2
sudo alternatives --set jar /usr/java/jdk1.8.0_31/bin/jar
sudo alternatives --set javac /usr/java/jdk1.8.0_31/bin/javac 
[ -L /usr/java/latest ] && sudo rm /usr/java/latest
sudo ln -s /usr/java/jdk1.8.0_31/ /usr/java/latest
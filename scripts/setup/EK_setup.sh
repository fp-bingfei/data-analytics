#/bin/bash

##########################################################
### MAIN
##########################################################
main() {
  elasticsearch
  kibana3
  kibana4
  nginx
  start_and_chkconfig
}


##########################################################
### ELASTICSEARCH
##########################################################

elasticsearch() {
	echo ""
	echo "Elasticsearch"

	sudo rpm --import http://packages.elasticsearch.org/GPG-KEY-elasticsearch 
	
cat <<EOF >> /etc/yum.repos.d/elasticsearch.repo
[elasticsearch-1.4]
name=Elasticsearch repository for 1.4.x packages
baseurl=http://packages.elasticsearch.org/elasticsearch/1.4/centos
gpgcheck=1
gpgkey=http://packages.elasticsearch.org/GPG-KEY-elasticsearch
enabled=1
EOF

	#sudo cp elasticsearch.repo /etc/yum.repos.d/

	sudo yum -y install elasticsearch

	## Configure ElasticSearch

	# restrict outside access to your Elasticsearch instance, so outsiders can't read your data or shutdown your Elasticseach cluster through the HTTP API
	# sudo sed -i '/network.host/c\network.host: localhost' /etc/elasticsearch/elasticsearch.yml

	# disable multicast by finding the discovery.zen.ping.multicast.enabled item and uncommenting
	sudo sed -i '/discovery.zen.ping.multicast.enabled/c\discovery.zen.ping.multicast.enabled: false' /etc/elasticsearch/elasticsearch.yml

	# Set Cluster Name
	sudo sed -i '/cluster.name/c\cluster.name: elasticsearch' /etc/elasticsearch/elasticsearch.yml

	# ES Permissions
	sudo chown -R elasticsearch:elasticsearch /var/lib/elasticsearch/ /var/log/elasticsearch/
  
	# Increase the openfile limits to elasticsearch by:
	sudo echo 'elasticsearch soft nofile 32000' >> /etc/security/limits.conf
	sudo echo 'elasticsearch hard nofile 32000' >> /etc/security/limits.conf

	# Required for ES 1.4.x
	echo "http.cors.enabled: true" >> /etc/elasticsearch/elasticsearch.yml
	echo "http.cors.allow-origin: \"http://localhost\" " >> /etc/elasticsearch/elasticsearch.yml

	# Configure elasticsearch data storage path
	echo 'path.data: /data/es/logs' >> /etc/elasticsearch/elasticsearch.yml
	mkdir -p /data/es/logs
	chown -R elasticsearch:elasticsearch /data/es/logs
}

##########################################################
### KIBANA
##########################################################
kibana3() {
	echo ""
	echo "Kibana"
	## Install Kibana

	cd /opt
	wget https://download.elasticsearch.org/kibana/kibana/kibana-3.1.2.tar.gz
	tar xzf kibana-3.1.2.tar.gz
	ln -s kibana-3.1.2 kibana
}

kibana4() {
	## Install Kibana 4

	cd /opt
	sudo wget https://download.elasticsearch.org/kibana/kibana/kibana-4.0.1-linux-x64.tar.gz
	tar xzf kibana-4.0.1-linux-x64.tar.gz
	ln -s kibana-4.0.1-linux-x64 kibana4
}

##########################################################
### NGINX
##########################################################

nginx() {
	## Install Nginx
	echo ""
	echo "Install and configure NGINX to server Kibana3"
	rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
	yum -y install nginx

	## Configure Nginx to server kibana3
	mkdir -p /usr/share/nginx/kibana3
	cp -R /opt/kibana/* /usr/share/nginx/kibana3/


	# Download sample nginx config
	cd ~; curl -OL https://raw.githubusercontent.com/elasticsearch/kibana/kibana3/sample/nginx.conf
	sed -i "s|kibana.myhost.org|localhost|" nginx.conf
	sed -i "s|root.*/usr/share/kibana3;|root /usr/share/nginx/kibana3;|" nginx.conf
	cp ~/nginx.conf /etc/nginx/conf.d/default.conf
}

apache2-utils(){
	## Install apache2-utils to generate username and password pair
	yum -y install httpd-tools-2.2.15
	htpasswd -c /etc/nginx/conf.d/localhost.htpasswd admin
}

start_and_chkconfig(){
	echo ""
	echo "Starting services + chkconfig"
	# Start elasticsearch and to make sure that es is available after reboot's
	chkconfig elasticsearch on
	service elasticsearch start
	
	#	Start Kibana 4
	sudo /opt/kibana4/bin/kibana &
	
	# Start nginx for serving kibana and to make sure that kibana is available after reboot's
	chkconfig nginx on
	service nginx start
}

main
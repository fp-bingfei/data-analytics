#!/bin/bash

echo "hive/spark setup started"

mkdir ~/customsetup
cd ~/customsetup

### remove existing files ###
rm hive-site.xml
rm json-serde-1.3-hive-jar-with-dependencies.jar
rm hive_udfs.jar

### download from s3 ###
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/conf/hive-site.xml .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/json-serde-1.3-hive-jar-with-dependencies.jar .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/hive_udfs.jar .

### copy files to right location ###
cp hive-site.xml ~/hive/conf/
cp hive-site.xml ~/spark/conf/
cp hive_udfs.jar ~/hive/auxlib/
cp json-serde-1.3-hive-jar-with-dependencies.jar ~/spark/classpath/emr/

echo "hive/spark setup completed"

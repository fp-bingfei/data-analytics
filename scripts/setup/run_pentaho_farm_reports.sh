#!/bin/bash

source /home/ec2-user/.bashrc
#export PENTAHO_DI_JAVA_OPTIONS="-Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=ec2-54-149-42-69.us-west-2.compute.amazonaws.com -Dcom.sun.management.jmxremote.port=9010 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Xmx4096m -XX:MaxPermSize=2048m"
#export PENTAHO_DI_JAVA_OPTIONS="-Xmx2048m -XX:MaxPermSize=512m -XX:+UseConcMarkSweepGC"
export PENTAHO_DI_JAVA_OPTIONS=""

DT=$1
if [ ! "$DT" ]; then
	DT=`date -d "1 day ago" +"%Y-%m-%d"`;
fi
mkdir -p /var/log/pentaho/farm_reports
echo "farm $DT"
log_file=/var/log/pentaho/farm_reports/$DT-farm.log
/mnt/funplus/softwares/pdi/data-integration/kitchen.sh -rep=prod -job=generic/reports/AllReports -param:RPT_DATE=$DT -param:RPT_GAME_ID=farm -param:RPT_VERSION=1.1 -param:HIVE_DB_SCHEMA=farm -param:REDSHIFT_GENERIC_DB_HOST=bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com -param:REDSHIFT_GENERIC_DB_NAME=farm -param:REDSHIFT_GENERIC_DB_USERNAME=biadmin -param:YAML_CONF=/mnt/funplus/analytics/python/conf/farm.yaml -param:MAIL_FROM_NAME=ReportJob >> $log_file 2>&1 & 

#/mnt/funplus/softwares/data-integration/kitchen.sh -rep=prod -job=v1_1/Reports -param:HIVE_DB_SCHEMA=farm_1_1 -param:HIVE_DB_HOST=ec2-52-27-154-130.us-west-2.compute.amazonaws.com -param:RPT_APP=$app -param:RPT_DATE=$DT -param:RPT_GEO=$geo >> $log_file 2>&1 & 

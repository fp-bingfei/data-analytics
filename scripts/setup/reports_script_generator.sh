#!/bin/bash

usage()
{
cat << EOF
usage: $0 options

This script executes the sql file in spark-sql client.

OPTIONS:
   -o    s3 script output file path
   -f    s3 script input file path
   -d    report date time
   -b    days back
   -A    all app (farm.all.prod)
   -a    app   

EOF
}

fail()
{
echo "ERROR: sql script generation process failed"
}

rpt_datetime=
rpt_days_back=
s3_input_file=
s3_output_file=
rpt_app=
rpt_all_app=

echo "INFO: sql script generation process started"

while getopts ":f:a:A:o:d:b:h" opt; do
        case $opt in
                h)
                        usage
                        exit 1
                ;;
 		a)
                        echo "INFO: -a (app) was triggered, Parameter: $OPTARG" >&2
                        rpt_app=$OPTARG
                ;;
 		A)
                        echo "INFO: -A (all app) was triggered, Parameter: $OPTARG" >&2
                        rpt_all_app=$OPTARG
                ;;
                f)
                        echo "INFO: -f (s3 script input file path) was triggered, Parameter: $OPTARG" >&2
                        s3_input_file=$OPTARG
                ;;
                b)
                        echo "INFO: -b (days back) was triggered, Parameter: $OPTARG" >&2
                	rpt_days_back=$OPTARG
		;;
                o)
                        echo "INFO: -o (s3 script ouptut file path) was triggered, Parameter: $OPTARG" >&2
                        s3_output_file=$OPTARG
                ;;
		d)
                        echo "INFO: -d (report date time) was triggered, Parameter: $OPTARG" >&2
                        rpt_datetime=$OPTARG
                ;;
                \?)
                        echo "ERROR: Invalid option: -$OPTARG" >&2
			fail
                        exit 1
                ;;
                :)
                        echo "ERROR: Option -$OPTARG requires an argument" >&2
			fail
                        exit 1
                ;;
        esac
done

#check if any argument is empty
if [ ! "$s3_input_file" ] || [ ! "$rpt_app" ] || [ ! "$rpt_days_back" ] || [ ! "$rpt_days_back" ] || [ ! "$s3_output_file" ]; then
        echo "ERROR: one of the argument value is missing. See -h for help."  >&2
	fail
        exit 1
fi

if ! hadoop fs -test -f $s3_input_file;
then
	echo "ERROR: $s3_input_file does not exist"  >&2
	fail
	exit 1
fi

if hadoop fs -test -f $s3_output_file;
then
	hadoop fs -rm $s3_output_file
fi

temp_file=`echo $s3_output_file | awk -F'/' '{print $NF}'`;
s3_output_dir=`echo $s3_output_file | sed "s/$temp_file//"`

### date init ###
rpt_date=`date -d "$rpt_datetime" +"%Y-%m-%d"`;
rpt_date_nohyphen=`date -d "$rpt_datetime" +"%Y%m%d"`;  
rpt_date_plus1=`date -d "$rpt_datetime +1 day" +"%Y-%m-%d"`;
rpt_date_plus1_nohyphen=`date -d "$rpt_datetime +1 day" +"%Y%m%d"`;  
rpt_year=`date -d "$rpt_datetime" +"%Y"`; 
rpt_year_plus1=`date -d "$rpt_datetime +1 day" +"%Y"`;
rpt_month=`date -d "$rpt_datetime" +"%m"`;
rpt_month_plus1=`date -d "$rpt_datetime +1 day" +"%m"`;
rpt_day=`date -d "$rpt_datetime" +"%d"`;
rpt_day_plus1=`date -d "$rpt_datetime +1 day" +"%d"`;
rpt_hour=`date -d "$rpt_datetime" +"%H"`;
rpt_date_days_back=`date -d "$rpt_datetime $rpt_days_back days ago" +"%Y-%m-%d"`;
rpt_date_start=`date -d "$rpt_datetime $rpt_days_back days ago" +"%Y-%m-%d"`;
rpt_year_days_back=`date -d "$rpt_datetime $rpt_days_back days ago" +"%Y"`;
rpt_month_days_back=`date -d "$rpt_datetime $rpt_days_back days ago" +"%m"`;
rpt_day_days_back=`date -d "$rpt_datetime $rpt_days_back days ago" +"%d"`;
rpt_date_yesterday=`date -d "$rpt_datetime 1 day ago" +"%Y-%m-%d"`;
rpt_year_yesterday=`date -d "$rpt_datetime 1 day ago" +"%Y"`;
rpt_month_yesterday=`date -d "$rpt_datetime 1 day ago" +"%m"`;
rpt_day_yesterday=`date -d "$rpt_datetime 1 day ago" +"%d"`;

rpt_date_d3=`date -d "$rpt_datetime 3 days ago" +"%Y-%m-%d"`;
rpt_date_d7=`date -d "$rpt_datetime 7 days ago" +"%Y-%m-%d"`;
rpt_date_d15=`date -d "$rpt_datetime 15 days ago" +"%Y-%m-%d"`;
rpt_date_d30=`date -d "$rpt_datetime 30 days ago" +"%Y-%m-%d"`;
rpt_date_d45=`date -d "$rpt_datetime 45 days ago" +"%Y-%m-%d"`;
rpt_date_d60=`date -d "$rpt_datetime 60 days ago" +"%Y-%m-%d"`;

echo "INFO: rpt_datetime => $rpt_datetime";
echo "INFO: rpt_date_days_back => $rpt_date_days_back";
echo "INFO: rpt_date_yesterday => $rpt_date_yesterday;";

if [ $rpt_month = $rpt_month_days_back ];
then
        rpt_days_back_date_predicate="((year >= $rpt_year_days_back AND month >= $rpt_month_days_back and day > $rpt_day_days_back) AND (year <= $rpt_year AND month <= $rpt_month AND day <= $rpt_day))";
else
        rpt_days_back_date_predicate="(((year >= $rpt_year_days_back AND month >= $rpt_month_days_back) AND (year <= $rpt_year AND month < $rpt_month) AND day > $rpt_day_days_back ) OR ((year <= $rpt_year AND month <= $rpt_month) AND (year >= $rpt_year_days_back AND month > $rpt_month_days_back) AND day <= $rpt_day))";
fi

### hive configuration ###

echo "---------------------------------------------------------------------" > $temp_file;
echo "-- hive configuration --" >> $temp_file;
echo "---------------------------------------------------------------------" >> $temp_file;

#echo "SET hive.hadoop.supports.splittable.combineinputformat=true;" >> $temp_file;
#echo "SET mapred.reduce.tasks=50;" >> $temp_file;
#echo "SET hive.optimize.s3.query=true;" >> $temp_file;
echo "SET hive.variable.substitute.depth=100;" >> $temp_file;


### add date variables to output ###

echo "---------------------------------------------------------------------" >> $temp_file;
echo "-- script variables --" >> $temp_file;
echo "---------------------------------------------------------------------" >> $temp_file;
echo "set rpt_date=$rpt_date;" >> $temp_file;
echo "set rpt_date_days_back=$rpt_date_days_back;" >> $temp_file;
echo "set rpt_date_start=$rpt_date_start;" >> $temp_file;
echo "set rpt_date_nohyphen=$rpt_date_nohyphen;" >> $temp_file;
echo "set rpt_date_plus1=$rpt_date_plus1;" >> $temp_file;
echo "set rpt_date_plus1_nohyphen=$rpt_date_plus1_nohyphen;" >> $temp_file;
echo "set rpt_app=$rpt_app;" >> $temp_file;
echo "set rpt_all_app=$rpt_all_app;" >> $temp_file;
echo "set rpt_year=$rpt_year;" >> $temp_file;
echo "set rpt_year_plus1=$rpt_year_plus1;" >> $temp_file;
echo "set rpt_month=$rpt_month;" >> $temp_file;
echo "set rpt_month_plus1=$rpt_month_plus1;" >> $temp_file;
echo "set rpt_day=$rpt_day;" >> $temp_file;
echo "set rpt_day_plus1=$rpt_day_plus1;" >> $temp_file;
echo "set rpt_hour=$rpt_hour;" >> $temp_file;
echo "set rpt_year_days_back=$rpt_year_days_back;" >> $temp_file;
echo "set rpt_month_days_back=$rpt_month_days_back;" >> $temp_file;
echo "set rpt_day_days_back=$rpt_day_days_back;" >> $temp_file;
echo "set rpt_date_yesterday=$rpt_date_yesterday;" >> $temp_file;
echo "set rpt_year_yesterday=$rpt_year_yesterday;" >> $temp_file;
echo "set rpt_month_yesterday=$rpt_month_yesterday;" >> $temp_file;
echo "set rpt_day_yesterday=$rpt_day_yesterday;" >> $temp_file;
echo "set rpt_date_d3=$rpt_date_d3;" >> $temp_file;
echo "set rpt_date_d7=$rpt_date_d7;" >> $temp_file;
echo "set rpt_date_d15=$rpt_date_d15;" >> $temp_file;
echo "set rpt_date_d30=$rpt_date_d30;" >> $temp_file;
echo "set rpt_date_d45=$rpt_date_d45;" >> $temp_file;
echo "set rpt_date_d60=$rpt_date_d60;" >> $temp_file;

### add script file from s3 to output ###

echo "---------------------------------------------------------------------" >> $temp_file;
echo "-- script queries --" >> $temp_file;
echo "---------------------------------------------------------------------" >> $temp_file;
hadoop fs -cat $s3_input_file >> $temp_file;

hadoop fs -mkdir -p $s3_output_dir
hadoop fs -copyFromLocal $temp_file $s3_output_dir


echo "INFO: sql script generation process completed"

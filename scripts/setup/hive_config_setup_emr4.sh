#!/bin/bash

echo "hive setup started"

mkdir -p ~/customsetup
cd ~/customsetup

### remove existing files ###
#rm -f hive-site_emr4.xml
rm -f hive-site.xml
rm -f json-serde-1.3-hive-jar-with-dependencies.jar
rm -f hive_udfs.jar
rm -f brickhouse-0.7.1-SNAPSHOT.jar
rm -f hive-env.sh
rm -f mysql-connector-java-bin.jar
rm -f install-tez.rb

### download from s3 ###
#hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/conf/hive-site_emr4.xml .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/conf/hive-site.xml .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/json-serde-1.3-hive-jar-with-dependencies.jar .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/hive_udfs.jar .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/brickhouse-0.7.1-SNAPSHOT.jar .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/conf/hive-env.sh .

### copy mysql jar to right location ###
wget http://s3.amazonaws.com/datapipeline-prod-us-east-1/software/latest/TaskRunner/mysql-connector-java-bin.jar
sudo cp mysql-connector-java-bin.jar /usr/lib/hive/lib/

### install tez ###
#hadoop fs -copyToLocal s3://support.elasticmapreduce/tez/bigtop/install-tez.rb .
#ruby install-tez.rb
#sudo mv /usr/lib/tez/lib/slf4j*.jar .

### copy files to right location ###
#sudo cp -f hive-site_emr4.xml /usr/lib/hive/conf/hive-site.xml
sudo cp -f hive-site.xml /usr/lib/hive/conf/hive-site.xml
sudo cp json-serde-1.3-hive-jar-with-dependencies.jar /usr/lib/hive/auxlib/
sudo cp hive_udfs.jar /usr/lib/hive/auxlib/
sudo cp brickhouse-0.7.1-SNAPSHOT.jar /usr/lib/hive/auxlib/

HADOOP_HEAPSIZE_VALUE="unspecified"

while getopts "h:" opt; do
  case $opt in
    h)
      HADOOP_HEAPSIZE_VALUE=$OPTARG
      ;;
  esac
done

#echo $HADOOP_HEAPSIZE_VALUE

if [ $HADOOP_HEAPSIZE_VALUE != "unspecified" ]; 
then 
	#hdfs dfs -get hive-env.sh
	#sed -e s/"export HADOOP_HEAPSIZE=12288"/"export HADOOP_HEAPSIZE=$HADOOP_HEAPSIZE_VALUE"/g hive-env.sh > test.sh
	sed -i s/"export HADOOP_HEAPSIZE=12288"/"export HADOOP_HEAPSIZE=$HADOOP_HEAPSIZE_VALUE"/g hive-env.sh
	sudo cp hive-env.sh /usr/lib/hive/conf/
fi

ps=`sudo netstat -anlp | grep 10000  | grep 0.0.0.0:* | awk -v OFS='\t' '{print $7}' | sed 's/\/java//'`
echo "killing hive process -> $ps" 
sudo kill -9 $ps
sleep 30s

echo "hive setup completed"

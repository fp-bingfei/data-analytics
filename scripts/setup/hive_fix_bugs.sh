#!/bin/bash

echo "hive fix started"

HOME=/home/hadoop
mkdir -p ${HOME}/fixes
cd ${HOME}/fixes

### copy files to right location ###
if [ -f "${HOME}/hive/lib/hive-exec-0.13.1-amzn-1.jar" ]; then
    rm -f hive-exec-0.13.1-amzn-1.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-0.13.1-amzn-1.jar .
    cp -f ${HOME}/hive/lib/hive-exec-0.13.1-amzn-1.jar hive-exec-0.13.1-amzn-1.jar.bak
    cp -f hive-exec-0.13.1-amzn-1.jar ${HOME}/hive/lib/
elif [ -f "${HOME}/hive/lib/hive-exec-0.13.1-amzn-2.jar" ]; then
    rm -f hive-exec-0.13.1-amzn-2.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-0.13.1-amzn-2.jar .
    cp -f ${HOME}/hive/lib/hive-exec-0.13.1-amzn-2.jar hive-exec-0.13.1-amzn-2.jar.bak
    cp -f hive-exec-0.13.1-amzn-2.jar ${HOME}/hive/lib/
elif [ -f "${HOME}/hive/lib/hive-exec-1.0.0-amzn-0-SNAPSHOT.jar" ]; then
    # Fix the issue of nested map value.
    rm -f hive-exec-1.0.0-amzn-0-SNAPSHOT.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-1.0.0-amzn-0-SNAPSHOT.jar .
    cp -f ${HOME}/hive/lib/hive-exec-1.0.0-amzn-0-SNAPSHOT.jar hive-exec-1.0.0-amzn-0-SNAPSHOT.jar.bak
    cp -f hive-exec-1.0.0-amzn-0-SNAPSHOT.jar ${HOME}/hive/lib/
elif [ -f "/usr/lib/hive/lib/hive-exec-1.0.0-amzn-1.jar" ]; then
    # EMR 4.1.0, fix the issue of big endian.
    # The issue of nested map value was fixed in this build.
    rm -f hive-exec-1.0.0-amzn-1_BigEndian.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-1.0.0-amzn-1_BigEndian.jar .
    cp -f /usr/lib/hive/lib/hive-exec-1.0.0-amzn-1.jar hive-exec-1.0.0-amzn-1.jar.bak
    sudo cp -f hive-exec-1.0.0-amzn-1_BigEndian.jar /usr/lib/hive/lib/hive-exec-1.0.0-amzn-1.jar
    # Restart Hive
    ps=`sudo netstat -anlp | grep 10000  | grep 0.0.0.0:* | awk -v OFS='\t' '{print $7}' | sed 's/\/java//'`
    echo "killing hive process -> $ps" 
    sudo kill -9 $ps
    sleep 30s
elif [ -f "/usr/lib/hive/lib/hive-exec-1.0.0-amzn-4.jar" ]; then
    # EMR 4.6.0, fix the issue of big endian.
    # The issue of nested map value was fixed in this build.
    rm -f hive-exec-1.0.0-amzn-4_BigEndian.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-1.0.0-amzn-4_BigEndian.jar .
    cp -f /usr/lib/hive/lib/hive-exec-1.0.0-amzn-4.jar hive-exec-1.0.0-amzn-4.jar.bak
    sudo cp -f hive-exec-1.0.0-amzn-4_BigEndian.jar /usr/lib/hive/lib/hive-exec-1.0.0-amzn-4.jar
    # Restart Hive
    ps=`sudo netstat -anlp | grep 10000  | grep 0.0.0.0:* | awk -v OFS='\t' '{print $7}' | sed 's/\/java//'`
    echo "killing hive process -> $ps" 
    sudo kill -9 $ps
    sleep 30s
elif [ -f "/usr/lib/hive/lib/hive-exec-1.0.0-amzn-5.jar" ]; then
    # EMR 4.7.0, fix the issue of big endian.
    # The issue of nested map value was fixed in this build.
    rm -f hive-exec-1.0.0-amzn-5_BigEndian.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-1.0.0-amzn-5_BigEndian.jar .
    cp -f /usr/lib/hive/lib/hive-exec-1.0.0-amzn-5.jar hive-exec-1.0.0-amzn-5.jar.bak
    sudo cp -f hive-exec-1.0.0-amzn-5_BigEndian.jar /usr/lib/hive/lib/hive-exec-1.0.0-amzn-5.jar
    # Restart Hive
    ps=`sudo netstat -anlp | grep 10000  | grep 0.0.0.0:* | awk -v OFS='\t' '{print $7}' | sed 's/\/java//'`
    echo "killing hive process -> $ps" 
    sudo kill -9 $ps
    sleep 30s
elif [ -f "/usr/lib/hive/lib/hive-exec-1.0.0-amzn-6.jar" ]; then
    # EMR 4.7.2, fix the issue of big endian.
    # The issue of nested map value was fixed in this build.
    rm -f hive-exec-1.0.0-amzn-6_BigEndian.jar
    hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/hive/jars/hive-exec-1.0.0-amzn-6_BigEndian.jar .
    cp -f /usr/lib/hive/lib/hive-exec-1.0.0-amzn-6.jar hive-exec-1.0.0-amzn-6.jar.bak
    sudo cp -f hive-exec-1.0.0-amzn-6_BigEndian.jar /usr/lib/hive/lib/hive-exec-1.0.0-amzn-6.jar
    # Restart Hive
    ps=`sudo netstat -anlp | grep 10000  | grep 0.0.0.0:* | awk -v OFS='\t' '{print $7}' | sed 's/\/java//'`
    echo "killing hive process -> $ps" 
    sudo kill -9 $ps
    sleep 30s
fi

if [ -f "/etc/init.d/hive-serverd" ]; then
    sudo /etc/init.d/hive-serverd stop
    sleep 30s
    sudo /etc/init.d/hive-serverd start
fi

echo "hive fix completed"
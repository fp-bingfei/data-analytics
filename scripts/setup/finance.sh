#!/bin/bash

set -e

usage()
{
cat << EOF
usage: $0 options

This script executes the sql file in spark-sql client.

OPTIONS:
   -t    job type [currency|consumableitem]
   -i    s3 script input path
   -o    s3 script output path
EOF
}


job_type=
input_path=
output_path=

while getopts ":t:i:o:h" opt; do
        case $opt in
                h)
                        usage
                        exit 1
                ;;
		t)
                        echo "INFO: -t (job type) was triggered, Parameter: $OPTARG" >&2
                        job_type=$OPTARG
                ;;                
		i)
                        echo "INFO: -i (input path) was triggered, Parameter: $OPTARG" >&2
                        input_path=$OPTARG
                ;;
                o)
                        echo "INFO: -o (output path) was triggered, Parameter: $OPTARG" >&2
                        output_path=$OPTARG
                ;;
                \?)
                        echo "ERROR: Invalid option: -$OPTARG" >&2
                        fail
                        exit 1
                ;;
                :)
                        echo "ERROR: Option -$OPTARG requires an argument" >&2
                        fail
                        exit 1
                ;;
        esac
done



#check if any argument is empty
if [ ! "$input_path" ] || [ ! "$output_path" ] ; then
        echo "ERROR: one of the argument value is missing. See -h for help."  >&2
        fail
        exit 1
fi


if ! hadoop fs -test -f $input_path;
then
        echo "ERROR: $input_path does not exist"  >&2
        fail
        exit 1
fi

if hdfs dfs -test -f $output_path;
then
        hdfs dfs -rm -f $output_path
fi

echo "INFO: $job_type started"


###########################################################
# 3 executors in each node with 5 vcores 14G memory
# driver with 1 vcore 1G memory
###########################################################
export SPARK_YARN_MODE=true

/usr/lib/spark/bin/spark-submit --master yarn-client --driver-memory 4G --conf spark.yarn.am.memory=4G --num-executors 6 --executor-cores 5 --executor-memory 10G --jars /usr/lib/spark/auxlib/json-schema-validator-2.2.6.jar,/usr/lib/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/usr/lib/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/usr/lib/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/usr/lib/spark/extras/lib/spark-streaming-kinesis-asl_2.10-1.5.0.jar,/usr/lib/spark/auxlib/amazon-kinesis-client-1.2.1.jar,/usr/lib/spark/auxlib/maxmind-db-1.0.0.jar,/usr/lib/spark/auxlib/geoip2-2.1.0.jar --class com.funplus.finance.$job_type /usr/lib/spark/auxlib/kinesis-app-1.0.jar $job_type $input_path $output_path

echo "INFO: $job_type completed"

#!/bin/bash

usage()
{
cat << EOF
usage: $0 options

This script setup taskrunner in the cluster.
OPTIONS:
   -h      show this message
   -w      WORKER GROUP
   -t	   TOTAL TASKS
   -l	   LOG URI
EOF
}

wg=
tasks=
loguri=

while getopts ":w:l:t:h" opt; do
        case $opt in
                h)
                        usage
                        exit 1
                ;;
                w)
                        echo "INFO: -w was triggered, Parameter: $OPTARG." >&2
                        wg=$OPTARG
                ;;
                l)
                        echo "INFO: -l was triggered, Parameter: $OPTARG." >&2
                        loguri=$OPTARG
                ;;
		t)
                        echo "INFO: -t was triggered, Parameter: $OPTARG." >&2
                        tasks=$OPTARG
                ;;
                \?)
                        echo "ERROR: Invalid option: -$OPTARG." >&2
                        exit 1
                ;;
                :)
                        echo "ERROR: Option -$OPTARG requires an argument." >&2
                        exit 1
                ;;
        esac
done

#check s3File is not empty
if [ ! "$wg" ] || [ ! "$tasks" ] || [ ! "$loguri" ]; then
        echo "ERROR: required args missing. See -h for help."  >&2
        exit 1
fi

mkdir -p /mnt/taskrunner
cd /mnt/taskrunner/

if [ -f TaskRunner-1.0.jar ]
then
	rm TaskRunner-1.0.jar
fi
wget https://s3.amazonaws.com/datapipeline-us-east-1/us-east-1/software/latest/TaskRunner/TaskRunner-1.0.jar

if [ -f mysql-connector-java-bin.jar ]
then
	rm mysql-connector-java-bin.jar
fi
wget http://s3.amazonaws.com/datapipeline-prod-us-east-1/software/latest/TaskRunner/mysql-connector-java-bin.jar

if [ -f credentials.json ];
then
	rm credentials.json; 
fi
cat >> /mnt/taskrunner/credentials.json << EOF
{
"access-id": "AKIAJRKDNC52OAINDWKA",
"private-key": "FTTVxv9SngsUztHSEuXIdoBODYFRDnHic309+AmI",
"endpoint": "https://datapipeline.us-west-2.amazonaws.com",
"region": "us-west-2",
"log-uri": "s3://com.funplusgame.bidata/datapipeline/logs/"

}
EOF
nohup java -jar TaskRunner-1.0.jar --config credentials.json --workerGroup=$wg --tasks=$tasks --logUri=$loguri > taskrunner.log &

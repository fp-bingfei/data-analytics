#/bin/bash

worker=$1
if [ ! "$worker" ]; then
        worker="wg-ec2-bv"
fi

##########################################################
### MAIN
##########################################################
main() {
  install_dependencies
  setup_password-less_git
  setup_git_repository
  setup_password-less_redshift_connection
  install_taskrunner
}

##########################################################
### INSTALL ALL THE DEPENDECIES REQUIRED FOR THE SCRIPT
##########################################################

install_dependencies(){
# Install dependencies for the copy_to_redshift.py file
sudo yum -y install PyYAML
sudo yum -y install python-psycopg2
sudo yum -y install python-argparse

# Install psql client to connect to redshift
sudo yum -y install postgresql

# Install git
sudo yum -y install git
}

# Create Private Key file for Password Less Git Access

##########################################################
### SETUP PASSWORD LESS CONNECTION TO GIT - USEFUL FOR GIT PUSH AND PULL
##########################################################

setup_password-less_git(){
cat > ~/.ssh/analytics_deployment_id_rsa << EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAvmHj2TrRl7feAQ9kcmB9GITgqmy0HjvdVV/RnyTEMJFBV809
/+ovHHEaZoi+A9xE8nI+XmbLwZwUoKXrR48z1QU/flznx2KyhmPxEeaCeJQm3Gcd
tlUcZOJK/Tbb2uliL6eauhEpbCnrNxNr1jRnXh6HNTjvvhybV/q6BUeiyBhSO5hz
FomJZuv87oxN3caNOzHE4qAqzE1gSAG/xNcyyu0sqs/0amXobatzOhJoavEIA+70
AWLiogHgm8dHCPg/ia9W6Q7aIjlzD2yySqYjIbsvaN2U8/HUHMGm2IObdhbsgJJI
nYa8xe0iRiWWZ90ePMS7TjHXmRKzNJzTPDESOwIDAQABAoIBAQCUExUhlzL/ex3r
2yn4H/4MPQV06FlrjopG5eDnk7QYRlH5VYQccOu1hm7PQxnvbLofjCXgTLm724bb
sFD/9myNCajpwp7voF9UUZVlo9Dkse5LzNP6RdZwzOUgbzOu34jjocuhXsnF/BCb
ZLqh/15KgRoOljs5tYxJcJSQmLgOQiqQnANWTAQN5qCtVOCXF4LZ396UZE089Aep
2aW3p5lnbh/TxDgoOtmjM8tr+hH5kNvCdwkgJeixrW6QtIKoL/eDdRIuN+TxVDqH
oSjoHR8VGrQGo/qNKRzMC6ZWJ25vSREc9dq6jACkhOwQ2l0oRiAYcYZulMoqMPqa
Ld8XUB+BAoGBAPitY98mDbMMFtcVNmPOPMWSDwjlQwTOsZA17QvYCrr6pprzhnYq
0gUUDA5A/45I062tdVDQzFCGHWEGOj3xFhuUr+kPJ6piDHdnQ2RzS8bJbYoWEEtS
kU1MJRhEwIjx7t6hB34dtDRwd7MnlUn7teQGJQS7dtfR3q2eeibHYoL7AoGBAMP9
Dc+wD7TGv2YGEO8Ra8mINMHIueBZwshP0/zyfguHQRf4xGS3yR1fQBvdbbmn9Xq+
ruVIKuOcxLDuqmdnJtTdPpna1jsE///SMHFq5umzVT0igfzhIFow9iRoT33wXRt/
nKv99oa0OJYbvnfaEbM6IFWEb5x1OBDXL72APInBAoGAQnDhLC/YnSvvtkuemo/x
wL/DcJcAjvpCiss9qFpyk36AoQnLtSq/rkeq6OwI25UtqUuc+piu3hzXIKMvUHln
dLsHPPelm2pS9lwKr332kwgiUjCO7fEDjWl4x8Emx/AL6EOR4upAyXn7IUzEIF42
PNQ/c8I6gHM8UZrz/ARtl50CgYEAnj+UyE67z9busn1RbQefBcIR/yqJU2qGyWET
pVlThxF18NowEbcID8xaImW/dDn3D14qL/3+i3yIoOiE9SsoApaGNoqpkkwuPKzF
6r5LpwXjXJKhbMOGcOy5wpnrHaBYPZzjr0hHp2qbijDqsTGGOzZC52ohCK7pT0fB
R3zk5MECgYB6xueXL3nRMe9E8sil7JcOShRaGiE3eU38xbdlQVHdrHcwEUAD6m4D
ueEC1klDhJKOdz6EZIiBTr7SX1xXYj8/0Sx81xErW66Ns5jsWzK4zsmw9UMPvm23
oQ+jwv5LyAGC5YWEQ/h70n0F6Ys7glYB8hcbobEfOLDqM8cfeAk1xw==
-----END RSA PRIVATE KEY-----
EOF

# It is required that your private key files are NOT accessible by others.
chmod 600 /home/ec2-user/.ssh/analytics_deployment_id_rsa

# Add Private Key for password less git access from this machine
# cat  >> ~/.bash_profile << EOF
# ssh-agent /bin/bash
# ssh-add ~/.ssh/analytics_deployment_id_rsa
# EOF
# source ~/.bash_profile

#echo -e "eval $(ssh-agent)\nssh-add ~/.ssh/analytics_deployment_id_rsa" >> ~/.bash_profile

eval $(ssh-agent)
ssh-add ~/.ssh/analytics_deployment_id_rsa
}

##########################################################
### DOWNLOAD PROJECTS IN GIT
##########################################################
setup_git_repository(){
# Clone the analytics git repository
sudo mkdir -p /mnt/funplus
sudo chown ec2-user:ec2-user /mnt/funplus/
cd /mnt/funplus

ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts # *Required - To prevent prompt on git clone for authenticity of hot 'bitbucket.org'
git clone git@bitbucket.org:yitao/analytics.git
}


##########################################################
### SETUP PASSWORD LESS CONNECTION TO REDSHIFT
##########################################################
setup_password-less_redshift_connection(){
# Password less connection to redshift
echo "*:*:*:*:Halfquest_2014" > ~/.pgpass
}

##########################################################
### INSTALL TASK RUNNER
##########################################################
install_taskrunner(){
# Install TaskRunner
mkdir -p /mnt/funplus/taskrunner
cd /mnt/funplus/taskrunner
wget https://s3.amazonaws.com/datapipeline-us-east-1/us-east-1/software/latest/TaskRunner/TaskRunner-1.0.jar
wget http://s3.amazonaws.com/datapipeline-prod-us-east-1/software/latest/TaskRunner/mysql-connector-java-bin.jar
cat >> /mnt/funplus/taskrunner/credentials.json << EOF
{
"access-id": "AKIAJRKDNC52OAINDWKA",
"private-key": "FTTVxv9SngsUztHSEuXIdoBODYFRDnHic309+AmI",
"endpoint": "https://datapipeline.us-west-2.amazonaws.com",
"region": "us-west-2",
"log-uri": "s3://com.funplusgame.bidata/datapipeline/logs/"

}
EOF

nohup java -jar TaskRunner-1.0.jar --config /mnt/funplus/taskrunner/credentials.json --workerGroup=$worker --region=us-west-2 --tasks 5 &
}

main
#!/bin/bash

echo "hive setup started"

mkdir ~/customsetup
cd ~/customsetup

### remove existing files ###
rm hive-site.xml
rm json-serde-1.3-hive-jar-with-dependencies.jar
rm hive_udfs.jar

### download from s3 ###
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/conf/hive-site.xml .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/json-serde-1.3-hive-jar-with-dependencies.jar .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/serde/hive_udfs.jar .
hdfs dfs -copyToLocal s3://com.funplusgame.bidata/dev/serde/brickhouse-0.7.1-SNAPSHOT.jar .
hadoop fs -copyToLocal s3://com.funplusgame.bidata/dev/conf/hive-env.sh .

### copy files to right location ###
cp hive-site.xml ~/hive/conf/
cp json-serde-1.3-hive-jar-with-dependencies.jar ~/hive/auxlib/
cp hive_udfs.jar ~/hive/auxlib/
cp brickhouse-0.7.1-SNAPSHOT.jar ~/hive/auxlib/

HADOOP_HEAPSIZE_VALUE="unspecified"

while getopts "h:" opt; do
  case $opt in
    h)
      HADOOP_HEAPSIZE_VALUE=$OPTARG
      ;;
  esac
done

#echo $HADOOP_HEAPSIZE_VALUE

if [ $HADOOP_HEAPSIZE_VALUE != "unspecified" ]; 
then 
	#hdfs dfs -get hive-env.sh
	#sed -e s/"export HADOOP_HEAPSIZE=12288"/"export HADOOP_HEAPSIZE=$HADOOP_HEAPSIZE_VALUE"/g hive-env.sh > test.sh
	sed -i s/"export HADOOP_HEAPSIZE=12288"/"export HADOOP_HEAPSIZE=$HADOOP_HEAPSIZE_VALUE"/g hive-env.sh
	cp hive-env.sh ~/hive/conf/
fi

cat >> ~/hive/conf/hive-env.sh << EOF
if [ "\$SERVICE" = "hiveserver2" ]; then
                export HADOOP_HEAPSIZE=12288
        else
                export HADOOP_HEAPSIZE=2048
fi
EOF

sudo /etc/init.d/hive-serverd stop
sleep 30s
sudo /etc/init.d/hive-serverd start

echo "hive setup completed"

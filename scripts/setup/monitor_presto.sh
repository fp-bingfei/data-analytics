#!/bin/bash

####################################
# Monitor Presto JDOException
# Solution: restart Hive processes
####################################

oldCount=0
if [ -e count.txt ]; then
    oldCount=`cat count.txt`
fi

newCount=`grep JDOException /var/log/presto/server.log | wc -l`

if [ $newCount -gt $oldCount ]; then
    # Restart Hive processes
    echo "Restart Hive processes at $(date)" >> restart.log
    ps=`ps -ef | grep HiveMetaStore | grep -v grep | awk '{print $2}'`
    sudo kill -9 $ps
    ps=`ps -ef | grep HiveServer2 | grep -v grep | awk '{print $2}'`
    sudo kill -9 $ps
    sleep 10s
fi

# Save the latest count
grep JDOException /var/log/presto/server.log | wc -l > count.txt

#!/bin/bash


usage()
{
cat << EOF
usage: $0 options

This script executes the sql file in spark-sql client.

OPTIONS:
   -h      show this message
   -f      s3 location of the sql script
   [-e]    num-executors for spark-sql process
   [-m]	   spark.executor.memory

EOF
}

s3File=
exec=2
mem=512m

while getopts ":f:eh" opt; do
        case $opt in
                h)
                        usage
                        exit 1
                ;;
                f)
                        echo "INFO: -f was triggered, Parameter: $OPTARG." >&2
                        s3File=$OPTARG
                ;;
                e)
                        echo "INFO: -e was triggered, Parameter: $OPTARG." >&2
                        if [ "$OPTARG" ]; then
                                exec=$OPTARG
                        fi
                ;;
		m)
                        echo "INFO: -m was triggered, Parameter: $OPTARG." >&2
                        if [ "$OPTARG" ]; then
                                mem=$OPTARG
                        fi
                ;;
                \?)
                        echo "ERROR: Invalid option: -$OPTARG." >&2
                        exit 1
                ;;
                :)
                        echo "ERROR: Option -$OPTARG requires an argument." >&2
                        exit 1
                ;;
        esac
done

#check s3File is not empty
if [ ! "$s3File" ]; then
        echo "ERROR: option '-f FILE' not given. See -h for help."  >&2
        exit 1
fi
echo "INFO: option '-e num-executors' set to $exec." >&2
echo "INFO: option '-m spark.executor.memory' set to $mem." >&2


echo "INFO: script execution started"
mkdir -p ~/scripts
cd ~/scripts
file=`echo $s3File | awk -F'/' '{print $(NF)}'`
rm $file
hadoop fs -copyToLocal $s3File
~/spark/bin/spark-sql --master yarn-client --jars s3://com.funplusgame.bidata/staging/serde/json-serde-1.3-spark-jar-with-dependencies.jar --num-executors  $exec  --hiveconf spark.executor.memory $mem -i $file
echo "INFO: script execution completed"

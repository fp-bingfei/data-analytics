delete from processed.fact_dau_snapshot where date = '${RPT_DATE}';
delete from processed.dim_user;

copy processed.fact_dau_snapshot from 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_dau_snapshot/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}' DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.dim_user from 's3://com.funplus.datawarehouse/farm_1_1/copy/dim_user/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;

commit;

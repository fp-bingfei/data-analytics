-- Delete Queries
delete from processed.fact_tradeorders where date = '${RPT_DATE}';

-- Copy Scripts

copy processed.fact_tradeorders from 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_tradeorders/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}' DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;


-- Delete Queries in redshift
delete from processed.agg_tradeorders_action where date = '${RPT_DATE}';
delete from processed.agg_tradeorders_item where date = '${RPT_DATE}';

-- Insert Queries in redshift

--insert data to agg_tradeorders_action
insert into processed.agg_tradeorders_action
select date
      ,app_id
      ,level
      ,action
      ,sum(times)
from  processed.fact_tradeorders
where date = '${RPT_DATE}'
group by 1,2,3,4;

--insert data to agg_tradeorders_item
insert into processed.agg_tradeorders_item
select date
      ,app_id
      ,level
      ,action
      ,exchange_item_1 as item
      ,sum(exchange_num_1) as item_num
      ,count(exchange_item_1) as item_times
from  processed.fact_tradeorders
where date = '${RPT_DATE}'     
group by 1,2,3,4,5
union all
select date
      ,app_id
      ,level
      ,action
      ,exchange_item_2 as item
      ,sum(exchange_num_2) as item_num
      ,count(exchange_item_2) as item_times
from  processed.fact_tradeorders 
where exchange_item_2 is not null 
and date = '${RPT_DATE}'   
group by 1,2,3,4,5
union all
select date
      ,app_id
      ,level
      ,action
      ,exchange_item_3 as item
      ,sum(exchange_num_3) as item_num
      ,count(exchange_item_3) as item_times
from  processed.fact_tradeorders 
where exchange_item_3 is not null  
and date = '${RPT_DATE}'  
group by 1,2,3,4,5;


-------------------------------
-- ff_level_progression.sql
-------------------------------
truncate processed.tab_level_dropoff;
insert into processed.tab_level_dropoff
with this_date as (select max(install_date) as date from processed.dim_user)
select
  u.install_date,
  l.level,
  u.level as current_level,
  u.app_id,
  u.install_os,
  u.install_country,
  u.install_source,
  u.is_payer,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days,
  count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join this_date t on 1=1
where install_date<t.date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;


commit;

--Create temp table to get latest additional email address

delete from processed.personal_info where date = '${RPT_DATE}' and app_id='${RPT_APP}';
copy processed.personal_info from 's3://com.funplus.datawarehouse/farm_1_1/copy/personal_info_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;


--Create temp table to get latest additional email address

create temp table personal_email_latest
as
select distinct   app_id 
          ,last_value(browser   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser
          ,last_value(browser_version   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser_version
          ,last_value(country_code   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as country_code
          ,event 
          ,last_value(install_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_source
          ,last_value(install_date   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_date
          ,last_value(ip   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as ip
          ,last_value(lang   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as lang
          ,last_value(level   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as level
          ,last_value(os   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os
          ,last_value(os_version   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os_version
          ,uid
          ,snsid
          ,last_value(fb_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as fb_source
          ,last_value(additional_email  ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following)  as additional_email
from processed.personal_info
;

-- TODO: For EAS report
create temp table payment_info as
select app, uid, snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from
(select p.app_id as app, u.app_user_id as uid, p.uid as snsid, paid_time as ts
from tbl_payment p
    join processed.dim_user u on p.app_id = u.app_id and p.uid = u.facebook_id
union
select r.app_id as app, r.app_user_id as uid, u.facebook_id as snsid, ts
from processed.fact_revenue r
    join processed.dim_user u on r.app_id = u.app_id and r.app_user_id = u.app_user_id
) t
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    t.app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,coalesce(pe.additional_email,'') as additional_email
    ,t.track_ref as install_source
    ,t.addtime as install_ts
    ,u.language as language
    ,u.gender
    ,cast(t.level as int)
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,CAST(t.reward_points AS BIGINT) as rc
    ,CAST(t.coins AS BIGINT) as coins
    ,dateadd(second, CAST(t.logintime AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
from tbl_user t
    left join payment_info p on t.app = p.app and t.uid = p.uid and t.snsid = p.snsid
    left join processed.dim_user u on t.app = u.app_id and t.uid = u.app_user_id and t.snsid = u.facebook_id
    left join personal_email_latest pe on t.app=pe.app_id and t.uid=pe.uid and t.snsid=pe.snsid;

-------update null value in language column
update processed.eas_user_info 
set language = 'am'
where language is null and app = 'farm.us.prod';

update processed.eas_user_info 
set language = 'th'
where language is null and app = 'farm.th.prod';

update processed.eas_user_info 
set language = 'zh_TW'
where language is null and app = 'farm.tw.prod';

update processed.eas_user_info 
set language = 'de-DE'
where language is null and app = 'farm.de.prod';

update processed.eas_user_info 
set language = 'fr'
where language is null and app = 'farm.fr.prod';

update processed.eas_user_info 
set language = 'nl'
where language is null and app = 'farm.nl.prod';

update processed.eas_user_info 
set language = 'it'
where language is null and app = 'farm.it.prod';

update processed.eas_user_info 
set language = 'pl'
where language is null and app = 'farm.pl.prod';

update processed.eas_user_info 
set language = 'pt'
where language is null and app = 'farm.br.prod';

	------------------------Cheating list--------------------------------------------------------------
	delete from processed.cheating_list                                                      
	where date >= '${RPT_DATE_D30}';  
              
	insert into processed.cheating_list
	select t1.app_id
	       ,t1.date
	       ,coalesce(t2.facebook_id,'Unkown') as snsid
	       ,t1.rc_in
	from processed.fact_dau_snapshot t1
	left join processed.dim_user t2
	on t1.user_key = t2.user_key
	where t1.rc_in >1000
	and t1.date >= '${RPT_DATE_D30}' 
	;
	
	commit;
	
	--------------------------
	--- Market Order Events --
	--------------------------
	
	-- Delete --
	
--	delete from raw_data.raw_marketcratefinish where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
--	delete from raw_data.raw_marketcrateneedhelp where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketexchange where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
--	delete from raw_data.raw_marketorderfinish where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketorderbegin where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
--	delete from raw_data.raw_marketordernew where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	
--	delete from raw_data.raw_marketorderfinish_temp where key='${RPT_APP}';
	delete from raw_data.raw_marketorderbegin_temp where key='${RPT_APP}';
--	delete from raw_data.raw_marketordernew_temp where key='${RPT_APP}';
		
	-- Load --
	
--	copy raw_data.raw_marketcratefinish from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketcratefinish_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
--	copy raw_data.raw_marketcrateneedhelp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketcrateneedhelp_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketexchange from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketexchange_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
--	copy raw_data.raw_marketorderfinish_temp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketorderfinish_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketorderbegin_temp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketorderbegin_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
--	copy raw_data.raw_marketordernew_temp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketordernew_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	
	
	-- Denormalize Crates --
	
--	INSERT INTO raw_data.raw_marketorderfinish
--	SELECT uid, key, ts_pretty, country_code, snsid, level, order_points_get, item_quantity, time_left,
--	 new_cash1_get, new_cash2_get, new_cash3_get, crates,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',1) crate_1_item_id, 
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',2) crate_1_item_amount,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',3) crate_1_number,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',1) crate_2_item_id, 
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',2) crate_2_item_amount,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',3) crate_2_number,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',1) crate_3_item_id, 
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',2) crate_3_item_amount,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',3) crate_3_number
--	FROM raw_data.raw_marketorderfinish_temp;
	
	INSERT INTO raw_data.raw_marketorderbegin
	SELECT uid, key, ts_pretty, country_code, snsid, level, order_points, time_left,
	 new_cash1, new_cash2, new_cash3, crates, rc_cost, rc_bal,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',1) crate_1_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',2) crate_1_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',3) crate_1_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',1) crate_2_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',2) crate_2_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',3) crate_2_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',1) crate_3_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',2) crate_3_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',3) crate_3_number
	FROM raw_data.raw_marketorderbegin_temp;
	
	
--	INSERT INTO raw_data.raw_marketordernew
--	SELECT uid, key, ts_pretty, country_code, snsid, level, order_points, time_left,
--	 new_cash1, new_cash2, new_cash3, crates, rc_cost, rc_bal,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',1) crate_1_item_id, 
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',2) crate_1_item_amount,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',3) crate_1_number,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',1) crate_2_item_id, 
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',2) crate_2_item_amount,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',3) crate_2_number,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',1) crate_3_item_id, 
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',2) crate_3_item_amount,
--	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',3) crate_3_number
--	FROM raw_data.raw_marketordernew_temp;
	
	
	commit;
	

--------------------------
--- AGG Handprint  --
--------------------------
--Delete--
delete from processed.agg_handprint where date = '${RPT_DATE}';
	
--Load--
copy processed.agg_handprint from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_handprint/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	
commit;


--------------------------
--- 4th scene  --
--------------------------
--Delete--
delete from processed.agg_4th_scene_login where date = '${RPT_DATE}';
delete from processed.agg_4th_scene_currency_transaction where date = '${RPT_DATE}';
delete from processed.raw_4th_scene_level_up_daily where trunc(ts_pretty) = '${RPT_DATE}';
delete from processed.raw_4th_scene_login_daily where date = '${RPT_DATE}';	
--Load--
	
copy processed.agg_4th_scene_login from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_4th_scene_login/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;

copy processed.agg_4th_scene_currency_transaction from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_4th_scene_currency_transaction/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;	

copy processed.raw_4th_scene_login_daily from 's3://com.funplus.datawarehouse/farm_1_1/copy/4th_scene_login_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;		

copy processed.raw_4th_scene_level_up_daily from 's3://com.funplus.datawarehouse/farm_1_1/copy/4th_scene_level_up_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;		

----------------------------
-- 4th scene levelup report
----------------------------
create temp table scene_level_daily as
select app_id           
      ,date             
      ,browser          
      ,browser_version  
      ,country_code     
      ,install_source       
      ,ip               
      ,language         
      ,max(scene_level) as scene_level            
      ,os               
      ,os_version       
      ,snsid            
      ,uid              
      ,fb_source        
      ,first_login
from
(
  select t1.app_id           
        ,t1.date             
        ,t1.browser          
        ,t1.browser_version  
        ,t1.country_code     
        ,t1.install_source       
        ,t1.ip               
        ,t1.language         
        ,coalesce(t2.scene_level,1) as scene_level            
        ,t1.os               
        ,t1.os_version       
        ,t1.snsid            
        ,t1.uid              
        ,t1.fb_source        
        ,t1.first_login
  from  processed.raw_4th_scene_login_daily t1
  left join 
        ( select  app_id 
                 ,date(ts_pretty) as date
                 ,snsid 
                 ,uid  
                 ,max(current_level) as scene_level 
          from  processed.raw_4th_scene_level_up_daily
          group by 1,2,3,4
        )t2
  on t1.app_id =t2.app_id
  and t1.uid = t2.uid
  and t1.snsid=t2.snsid
  and t1.date>=t2.date
)
group by 1,2,3,4,5,6,7,8,10,11,12,13,14,15
;

truncate table processed.dim_4th_scene_level;
insert into processed.dim_4th_scene_level
select t1.app_id
      ,md5(t1.app_id||t1.uid) as user_key
      ,t1.browser
      ,t1.browser_version 
      ,coalesce(c.country,'unknown') as country
      ,t1.install_source
      ,t1.scene_install_date
      ,t1.ip
      ,t1.language
      ,t2.scene_level
      ,t1.os
      ,t1.os_version
      ,t1.snsid
      ,t1.uid
      ,t1.fb_source
      ,t2.date as latest_login_date
      ,d.is_payer
from
     (
       select app_id
             ,browser
             ,browser_version 
             ,country_code
             ,install_source
             ,date as scene_install_date
             ,ip
             ,language
             ,os
             ,os_version
             ,snsid
             ,uid
             ,fb_source
        from processed.raw_4th_scene_login_daily 
        where first_login = 1
     )t1
left join
     (
       select *
              ,row_number() over(partition by app_id,uid order by date desc) as rnum
       from scene_level_daily
     )t2
on t1.app_id = t2.app_id
and t1.uid = t2.uid
and t1.snsid=t2.snsid
and t2.rnum =1
left join processed.dim_user d
on md5(t1.app_id||t1.uid) = d.user_key
left join processed.dim_country c
on t1.country_code=c.country_code
;


drop table if exists processed.agg_lapsed_scene_level;
create table processed.agg_lapsed_scene_level
as 
select d.app_id
      ,d.browser
      ,d.browser_version 
      ,d.country
      ,d.install_source
      ,d.install_date
      ,d.language
      ,l.scene_level as level
      ,d.scene_level as current_level
      ,d.os
      ,d.os_version
      ,d.fb_source
      ,d.is_payer
      ,case when l.scene_level=d.scene_level and datediff(day,d.latest_login_date,current_date)>=3 then 1 else 0 end as is_churned_3days
      ,case when l.scene_level=d.scene_level and datediff(day,d.latest_login_date,current_date)>=7 then 1 else 0 end as is_churned_7days
      ,case when l.scene_level=d.scene_level and datediff(day,d.latest_login_date,current_date)>=14 then 1 else 0 end as is_churned_14days
      ,case when l.scene_level=d.scene_level and datediff(day,d.latest_login_date,current_date)>=21 then 1 else 0 end as is_churned_21days
      ,case when l.scene_level=d.scene_level and datediff(day,d.latest_login_date,current_date)>=30 then 1 else 0 end as is_churned_30days
      ,count(user_key) as user_cnt
from processed.dim_4th_scene_level d
join (select distinct scene_level from processed.dim_4th_scene_level) l
on d.scene_level>= l.scene_level
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18;
	
commit;
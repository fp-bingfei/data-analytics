delete from processed.agg_daily_ledger_test where date = '${RPT_DATE}';
delete from processed.agg_daily_ledger_test where date < current_date - 14;
delete from processed.fact_session where date < current_date -30;

copy processed.agg_daily_ledger_test from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_daily_ledger_test/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}' DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;


commit;

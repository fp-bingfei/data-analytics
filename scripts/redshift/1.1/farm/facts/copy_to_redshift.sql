delete from processed.fact_session where date_start = '${RPT_DATE}';
delete from processed.fact_levelup where levelup_date = '${RPT_DATE}';
delete from processed.fact_revenue where date = '${RPT_DATE}';

copy processed.fact_session from 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_session/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.fact_levelup from 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_levelup/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.fact_revenue from 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_revenue/app=all/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=${AWS_ACCESS_ID};aws_secret_access_key=${AWS_SECRET_KEY}'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;

commit;

delete from kpi_processed.fact_notification
where ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
       --and app_id like '_game_%'
       ;
INSERT INTO kpi_processed.fact_notification
SELECT
MD5(app_id||event||user_id||session_id||ts_pretty) as id,
app_id as app_id,
json_extract_path_text(properties,'app_version') as app_version,
MD5(app_id||user_id) as user_key,
trunc(ts_pretty) as date,
ts_pretty,
(TIMESTAMP 'epoch' + (json_extract_path_text(properties,'click_ts')::int) * INTERVAL '1 Second ') as click_ts,
json_extract_path_text(properties,'notif_id') as notification_id,
json_extract_path_text(properties,'notif_name') as notification_name,
(TIMESTAMP 'epoch' + (json_extract_path_text(properties,'install_ts')::int) * INTERVAL '1 Second ') as install_ts,
session_id,
json_extract_path_text(properties,'facebook_id') as facebook_id,
json_extract_path_text(properties,'install_source') as install_source,
json_extract_path_text(properties,'os') as os,
json_extract_path_text(properties,'os_version') as os_version,
json_extract_path_text(properties,'browser') as browser,
json_extract_path_text(properties,'browser_version') as browser_version,
json_extract_path_text(properties,'country_code') as country_code,
json_extract_path_text(properties,'level')::int as level,
json_extract_path_text(properties,'gender') as gender,
json_extract_path_text(properties,'email') as email,
json_extract_path_text(properties,'lang') as language
from raw_events.events 
where app_id like 'poker%' 
and event = 'notif_click'
and json_extract_path_text(properties,'click_ts') <> '' ;
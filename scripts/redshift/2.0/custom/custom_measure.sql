delete from custom.agg_measure_by_date where date >= (select start_date from custom.init_start_date);
insert into custom.agg_measure_by_date 
select 
    a.event_name       
   ,a.app_id           
   ,a.date::date
   ,a.d_browser        
   ,a.d_browser_version
   ,coalesce(c.country,'Unknown') as country  
   ,a.d_install_source 
   ,a.d_install_date::date
   ,a.d_lang           
   ,a.d_level          
   ,a.d_os             
   ,a.d_os_version     
   ,a.d_fb_source      
   ,json_extract_path_text(replace(a.d_c1,'\\',' '),'value') as d_c1
   ,json_extract_path_text(a.d_c2,'value') as d_c2
   ,json_extract_path_text(a.d_c3,'value') as d_c3
   ,json_extract_path_text(a.d_c4,'value') as d_c4
   ,json_extract_path_text(a.d_c5,'value') as d_c5
   ,sum(nvl(nullif(json_extract_path_text(a.m1,'value'), '')::numeric, 0)) as m1               
   ,sum(nvl(nullif(json_extract_path_text(a.m2,'value'), '')::numeric, 0)) as m2              
   ,sum(nvl(nullif(json_extract_path_text(a.m3,'value'), '')::numeric, 0)) as m3              
   ,sum(nvl(nullif(json_extract_path_text(a.m4,'value'), '')::numeric, 0)) as m4              
   ,sum(nvl(nullif(json_extract_path_text(a.m5,'value'), '')::numeric, 0)) as m5              
   ,sum(a.event_cnt) as event_cnt
   ,count(distinct a.user_key) as user_cnt
from custom.agg_measure a
left join custom.dim_country c   
on a.d_country_code = c.country_code
where date >= (select start_date from custom.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
;


drop table if exists custom.custom_mapping;
create table custom.custom_mapping as
select 
    event_name                 
   ,max(json_extract_path_text(replace(d_c1,'\\',' '),'key')) as d_c1
   ,max(json_extract_path_text(d_c2,'key')) as d_c2
   ,max(json_extract_path_text(d_c3,'key')) as d_c3
   ,max(json_extract_path_text(d_c4,'key')) as d_c4
   ,max(json_extract_path_text(d_c5,'key')) as d_c5
   ,max(json_extract_path_text(m1,'key')) as m1               
   ,max(json_extract_path_text(m2,'key')) as m2              
   ,max(json_extract_path_text(m3,'key')) as m3              
   ,max(json_extract_path_text(m4,'key')) as m4              
   ,max(json_extract_path_text(m5,'key')) as m5              
from custom.agg_measure where date=(CURRENT_DATE-3)
group by 1
;

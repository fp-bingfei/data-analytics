--
----- rc and coins transaction
delete from legendsawake.currency_transaction where date>=current_date-3;

insert into legendsawake.currency_transaction
select
'rc' as currency_type,
c.app_id,
trunc(ts_pretty) as date,
case when c.os like '%iphone%' then 'iphone os' 
when c.os like '%mac%' then 'mac os' else c.os end,
c.level::int,
c.app_version,
c.lang,
u.install_date,
u.country,
c.server,
json_extract_path_text(d_c1,'value') as action,
sum(json_extract_path_text(m1,'value')::bigint) as currency_received,
sum(json_extract_path_text(m2,'value')::bigint) as currency_spent from legendsawake.catchall c
left join legendsawake.dim_user u on c.app_id=u.app_id and 
c.user_id=u.user_id where event='rc_transaction' and ts_pretty>=current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11;

insert into legendsawake.currency_transaction
select
'coins' as currency_type,
c.app_id,
trunc(ts_pretty) as date,
case when c.os like '%iphone%' then 'iphone os' 
when c.os like '%mac%' then 'mac os' else c.os end,
c.level::int,
c.app_version,
c.lang,
u.install_date,
u.country,
c.server,
json_extract_path_text(d_c1,'value') as action,
sum(json_extract_path_text(m1,'value')::bigint) as currency_received,
sum(json_extract_path_text(m2,'value')::bigint) as currency_spent from legendsawake.catchall c
left join legendsawake.dim_user u on c.app_id=u.app_id and 
c.user_id=u.user_id where event='coins_transaction' and ts_pretty>=current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11; 

---- item transaction
delete from legendsawake.item_transaction where date>=current_date-3;
insert into legendsawake.item_transaction
select
c.app_id,
trunc(ts_pretty) as date,
case when c.os like '%iphone%' then 'iphone os' 
when c.os like '%mac%' then 'mac os' else c.os end,
c.level::int,
c.app_version,
c.lang,
u.install_date,
u.country,
c.server,
json_extract_path_text(d_c1,'value') as item_id,
m.item_name ,
json_extract_path_text(d_c2,'value') as item_type,
json_extract_path_text(d_c3,'value') as item_stage,
json_extract_path_text(d_c4,'value') as action,
sum(json_extract_path_text(m2,'value')::bigint) as item_received,
sum(json_extract_path_text(m1,'value')::bigint) as item_spent from legendsawake.catchall c
left join legendsawake.dim_user u on c.app_id=u.app_id and 
c.user_id=u.user_id 
left join legendsawake.item_mapping m on json_extract_path_text(d_c1,'value')=m.item_id 
where event='item_transaction' and ts_pretty>=current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14; 

------- round flow

DELETE FROM legendsawake.round_flow where ts_pretty>=current_date-3 ;

insert into legendsawake.round_flow 
select ts_pretty as ts,
c.app_id,
c.user_id,
c.lang,
json_extract_path_text(d_c1,'value') as battle_type,
json_extract_path_text(d_c2,'value') as pve_id,
json_extract_path_text(d_c3,'value') as pve_difficulty,
nvl(nullif(json_extract_path_text(m2,'value'), '')::numeric(14,4), 0) as round_time,
u.install_date,
d.country,
case when c.os like '%iphone%' then 'iphone os' 
when c.os like '%mac%' then 'mac os' else c.os end,
c.level,
c.server,
json_extract_path_text(m3,'value') as round_result,
json_extract_path_text(d_c4,'value') as action 
from legendsawake.catchall c
left join legendsawake.dim_user u on c.user_id=u.user_id and c.app_id=u.app_id
left join dim_country d on c.country_code=d.country_code
where event='round_flow' and ts_pretty>=current_date-3;

---- pet flow

delete from legendsawake.pet_flow where date>=current_date-3;

insert into legendsawake.pet_flow

select date(ts_pretty) as date,
c.app_id,
json_extract_path_text(d_c1,'value') as pet_id,
json_extract_path_text(d_c2,'value') as pet_identify,
c.lang,
json_extract_path_text(m2,'value') as pet_level,
null as pet_start_level,--json_extract_path_text(m3,'value') as pet_star_level,
json_extract_path_text(m1,'value') as pet_stage,
u.install_date,
d.country,
case when c.os like '%iphone%' then 'iphone os' 
when c.os like '%mac%' then 'mac os' else c.os end,
c.level,
c.server,
json_extract_path_text(d_c3,'value') as action,
count(1),
sum(nvl(nullif(json_extract_path_text(m3,'value'), '')::int, 0)) as pet_in,
sum(nvl(nullif(json_extract_path_text(m4,'value'), '')::int, 0)) as pet_out,
p.pet_name
from legendsawake.catchall c
left join legendsawake.dim_user u on c.user_id=u.user_id and c.app_id=u.app_id
left join legendsawake.pet_mapping p on json_extract_path_text(d_c1,'value')=p.pet_id 
left join dim_country d on c.country_code=d.country_code
where event='pet_flow' and ts_pretty>=current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,18;

----- mission flow
delete from legendsawake.mission_flow where ts_pretty>=current_date-3;

insert into legendsawake.mission_flow 
select ts_pretty,
c.app_id,
c.user_id,
json_extract_path_text(d_c3,'value') as mission_id,
json_extract_path_text(d_c1,'value') as mission_maintype,
json_extract_path_text(d_c2,'value') as mission_subtype,
json_extract_path_text(m2,'value') as mission_progress,
c.lang,
u.install_date,
d.country,
case when c.os like '%iphone%' then 'iphone os' 
when c.os like '%mac%' then 'mac os' else c.os end,
c.level,
c.server,
json_extract_path_text(m1,'value') as action
from legendsawake.catchall c
left join legendsawake.dim_user u on c.user_id=u.user_id and c.app_id=u.app_id
left join dim_country d on c.country_code=d.country_code
where event='mission_flow' and ts_pretty>=current_date-3 ;
---group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

---- pve_progress

drop table if exists last_play;
create  temp table last_play as
select app_id,user_id, last_ts from (select app_id,user_id,ts_pretty as last_ts,
 row_number() over (partition BY app_id,user_id
                                       ORDER BY ts_pretty DESC) AS rank from legendsawake.catchall)t where t.rank=1
    ;                         
    

delete from legendsawake.pve_progress;

insert into legendsawake.pve_progress
select  t.ts
       ,t.app_id
       ,t.user_id
       ,coalesce(u.install_date,t.install_date) as install_date
       ,coalesce(u.country,'Unknown') as country
       ,t.level
       ,case when lower(t.os) = 'ios' then 'iOS' when lower(t.os) = 'android' then 'Android' else 'Unknown' end as os
       ,t.battle_type
       ,t.lang
       ,t.pveid
       ,t.pvelocation
       ,coalesce(last_date,u.last_login_date) as latest_login_date
from 
       (select ts_pretty as ts
              ,c.app_id
              ,c.user_id
              ,cast(install_ts as date) as install_date
              ,country
              ,level
              ,os
              ,json_extract_path_text(d_c1,'value') as battle_type
              ,lang
              ,json_extract_path_text(d_c2,'value') as pveid
              ,json_extract_path_text(m3,'value') as pvelocation -- result,
              ,date(p.last_ts) as last_date
              ,row_number() over(partition by c.app_id,c.user_id order by ts_pretty desc) as rank
       from   legendsawake.catchall c
       left join last_play p on c.app_id=p.app_id and c.user_id=p.user_id
       left join dim_country t on c.country_code=t.country_code where event='round_flow'
       )t                               
left join legendsawake.dim_user u
on u.user_id = t.user_id and u.app_id=t.app_id     
where t.rank =1;   

--by superadmin
CREATE USER la_pm with password 'pq8sSvK4Vne2w438';
--by biadmin
CREATE SCHEMA legendsawake;
GRANT ALL ON SCHEMA legendsawake to la_pm;
GRANT ALL ON ALL TABLES IN SCHEMA legendsawake to la_pm;

CREATE TABLE legendsawake.agg_kpi
(
   date                  date             NOT NULL  encode delta,
   app_id                varchar(64)      NOT NULL encode LZO,
   app_version           varchar(32) encode LZO,
   install_source_group  varchar(1024) encode LZO,
   install_source        varchar(1024) encode LZO,
   level_end             integer,
   browser               varchar(64) encode LZO,
   country               varchar(64) encode LZO,
   os                    varchar(64) encode LZO,
   language              varchar(16) encode LZO,
   is_new_user           integer,
   is_payer              integer,
   new_user_cnt          integer,
   dau_cnt               integer,
   newpayer_cnt          integer,
   payer_today_cnt       integer,
   payment_cnt           integer,
   revenue_usd           numeric(14,4),
   session_cnt           integer,
   playtime_sec          bigint,
   scene                 varchar(32)      DEFAULT 'Main'::character varying encode LZO,
   revenue_iap           numeric(14,4),
   revenue_ads           numeric(14,4)
)
DISTSTYLE EVEN
SORTKEY
(
   date
);
CREATE TABLE legendsawake.fact_dau_snapshot
(
   id                  varchar(128)     NOT NULL,
   user_key            varchar(128)     NOT NULL,
   date                date             NOT NULL,
   app_id              varchar(64)      NOT NULL,
   app_version         varchar(32),
   level_start         integer,
   level_end           integer,
   os                  varchar(64),
   os_version          varchar(64),
   device              varchar(128),
   browser             varchar(64),
   browser_version     varchar(128),
   country             varchar(64),
   language            varchar(16),
   is_new_user         integer,
   is_payer            integer,
   is_converted_today  integer,
   revenue_usd         numeric(15,4),
   payment_cnt         integer,
   session_cnt         integer,
   playtime_sec        integer,
   scene               varchar(32)      DEFAULT 'Main'::character varying,
   revenue_iap         numeric(14,4),
   revenue_ads         numeric(14,4)
)
DISTSTYLE EVEN
SORTKEY
(
   user_key,
   date
);


CREATE TABLE legendsawake.dim_user
(
   id                    varchar(128)     NOT NULL encode LZO,
   user_key              varchar(128)     NOT NULL encode LZO,
   app_id                varchar(64)      NOT NULL encode LZO,
   app_version           varchar(32) encode LZO,
   user_id               varchar(128)     NOT NULL encode LZO,
   facebook_id           varchar(128) encode LZO,
   install_ts            timestamp        NOT NULL encode delta,
   install_date          date             NOT NULL encode delta,
   install_source        varchar(1024) encode LZO,
   install_subpublisher  varchar(1024) encode LZO,
   install_campaign      varchar(512) encode LZO,
   install_language      varchar(16) encode LZO,
   install_country       varchar(64) encode LZO,
   install_os            varchar(64) encode LZO,
   install_device        varchar(128) encode LZO,
   install_browser       varchar(64) encode LZO,
   install_gender        varchar(20) encode LZO,
   language              varchar(16) encode LZO,
   birthday              date encode delta,
   first_name            varchar(64) encode LZO,
   last_name             varchar(64) encode LZO,
   gender                varchar(20) encode LZO,
   country               varchar(64) encode LZO,
   email                 varchar(256) encode LZO,
   os                    varchar(64) encode LZO,
   os_version            varchar(64) encode LZO,
   device                varchar(128) encode LZO,
   browser               varchar(64) encode LZO,
   browser_version       varchar(128) encode LZO,
   last_ip               varchar(32) encode LZO,
   level                 integer,
   is_payer              integer,
   conversion_ts         timestamp encode delta,
   revenue_usd           numeric(14,4),
   payment_cnt           integer,
   last_login_date       date encode delta,
   install_source_group  varchar(1024) encode LZO,
   install_creative_id   varchar(500) encode LZO,
   last_ref              varchar(1024) encode LZO
)
DISTSTYLE EVEN
SORTKEY
(
   user_key,
   user_id,
   install_date
);

CREATE TABLE legendsawake.catchall
(
	data_version VARCHAR(4) ENCODE lzo,
	app_id VARCHAR(32) ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP,
	event VARCHAR(32) ENCODE lzo,
	user_id VARCHAR(64) ENCODE lzo,
	session_id VARCHAR(100) ENCODE lzo,
	os VARCHAR(16) ENCODE lzo,
	install_ts TIMESTAMP,
	lang VARCHAR(16) ENCODE lzo,
	level INTEGER,
	browser VARCHAR(32) ENCODE lzo,
	app_version VARCHAR(16) ENCODE lzo,
	ip VARCHAR(64) ENCODE lzo,
	country_code VARCHAR(8) ENCODE lzo,
	d_c1 VARCHAR(100) ENCODE lzo,
	d_c2 VARCHAR(100) ENCODE lzo,
	d_c3 VARCHAR(100) ENCODE lzo,
	d_c4 VARCHAR(100) ENCODE lzo,
	d_c5 VARCHAR(100) ENCODE lzo,
	m1 VARCHAR(100) ENCODE lzo,
	m2 VARCHAR(100) ENCODE lzo,
	m3 VARCHAR(100) ENCODE lzo,
	m4 VARCHAR(100) ENCODE lzo,
	m5 VARCHAR(100) ENCODE lzo,
	c1 VARCHAR(1000) ENCODE lzo,
	c2 VARCHAR(1000) ENCODE lzo,
	idfa VARCHAR(100) ENCODE lzo,
	gaid VARCHAR(100) ENCODE lzo,
   server VARCHAR(20) ENCODE lzo,
	load_hour TIMESTAMP
)
DISTSTYLE EVEN
SORTKEY
(
	ts_pretty,
	event
);


CREATE TABLE legendsawake.fact_revenue
(
   id VARCHAR(128) NOT NULL ENCODE lzo,
   app_id VARCHAR(64) NOT NULL ENCODE lzo,
   app_version VARCHAR(32) ENCODE lzo,
   user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
   user_id VARCHAR(128) NOT NULL ENCODE lzo,
   date DATE NOT NULL ENCODE delta,
   ts TIMESTAMP NOT NULL ENCODE delta,
   install_ts TIMESTAMP ENCODE delta,
   install_date DATE ENCODE delta,
   session_id VARCHAR(128) ENCODE lzo,
   level INTEGER ENCODE lzo,
   vip_level INTEGER ENCODE lzo,
   os VARCHAR(64) ENCODE lzo,
   os_version VARCHAR(64) ENCODE lzo,
   device VARCHAR(128) ENCODE lzo,
   browser VARCHAR(64) ENCODE lzo,
   browser_version VARCHAR(128) ENCODE lzo,
   country VARCHAR(64) ENCODE lzo,
   ip VARCHAR(32) ENCODE lzo,
   install_source VARCHAR(1024) ENCODE lzo,
   language VARCHAR(16) ENCODE lzo,
   payment_processor VARCHAR(128) ENCODE lzo,
   iap_product_id VARCHAR(128) ENCODE lzo,
   iap_product_name VARCHAR(128) ENCODE lzo,
   iap_product_type VARCHAR(128) ENCODE lzo,
   currency VARCHAR(8) NOT NULL ENCODE lzo,
   revenue_amount NUMERIC(14, 4),
   revenue_usd NUMERIC(14, 4),
   transaction_id VARCHAR(128) ENCODE lzo,
   scene VARCHAR(32),
   fb_source VARCHAR(1024),
   charged_amount numeric(14,4),
   buyer_forex_rate numeric(10,8),
   buyer_currency character varying(8)
))
SORTKEY
(
   date,
   user_key,
   app_id,
   user_id
);

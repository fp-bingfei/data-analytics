drop table if exists maitai_install_app_version;
create temp table maitai_install_app_version as
select distinct d.user_key,d.app_version as install_app_version from kpi_processed.fact_dau_snapshot d
join kpi_processed.dim_user u on d.user_key=u.user_key and d.date=u.install_date
where d.app_id like 'maitai%';


drop table if exists maitai_player_day_cube_ab;
--retention and ltv
CREATE TEMP TABLE maitai_player_day_cube_ab AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    v.install_app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    c.ab_experiment1,
    c.ab_variable1,
    c.ab_experiment2,
    c.ab_variable2,
    c.ab_experiment3,
    c.ab_variable3,
    c.ab_experiment4,
    c.ab_variable4,
    d.is_payer,
    count(distinct d.user_key) new_user_cnt
FROM kpi_processed.dim_user d
JOIN kpi_processed.fact_dau_snapshot u ON d.user_key=u.user_key
join maitai_install_app_version v on d.user_key=v.user_key
 JOIN player_day p ON DATEDIFF('day', d.install_date, u.date)<=p.day
 join last_date l on 1=1
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM maitaimadness.ab_test)
            WHERE rank=1)
     ) c
      on d.user_key = c.user_key
where
    d.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    u.user_key= d.user_key and u.date=d.install_date and u.scene='Main' and d.app_id like '%maitai%prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;


drop table if exists maitai_baseline_ab;
CREATE TEMP TABLE maitai_baseline_ab AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    v.install_app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    c.ab_experiment1,
    c.ab_variable1,
    c.ab_experiment2,
    c.ab_variable2,
    c.ab_experiment3,
    c.ab_variable3,
    c.ab_experiment4,
    c.ab_variable4,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM kpi_processed.fact_dau_snapshot d
 JOIN kpi_processed.dim_user u ON d.user_key=u.user_key
 join maitai_install_app_version v on d.user_key=v.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM maitaimadness.ab_test)
            WHERE rank=1)
     ) c
      on d.user_key = c.user_key
WHERE
    u.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day and d.app_id like '%maitai%prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22;

-- TODO: insert instead of create table

delete from maitaimadness.agg_retention_ltv_ab
where app_id like '%maitai%prod'
;

insert into maitaimadness.agg_retention_ltv_ab
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.ab_experiment1,
    pc.ab_variable1,
    pc.ab_experiment2,
    pc.ab_variable2,
    pc.ab_experiment3,
    pc.ab_variable3,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt,
    pc.ab_experiment4,
    pc.ab_variable4
FROM maitai_player_day_cube_ab pc
LEFT JOIN maitai_baseline_ab b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
   COALESCE(pc.install_app_version,'') =COALESCE(b.install_app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.ab_experiment1,'') = COALESCE(b.ab_experiment1,'') AND
    COALESCE(pc.ab_variable1,'') = COALESCE(b.ab_variable1,'') AND
    COALESCE(pc.ab_experiment2,'') = COALESCE(b.ab_experiment2,'') AND
    COALESCE(pc.ab_variable2,'') = COALESCE(b.ab_variable2,'') AND
    COALESCE(pc.ab_experiment3,'') = COALESCE(b.ab_experiment3,'') AND
    COALESCE(pc.ab_variable3,'') = COALESCE(b.ab_variable3,'') AND
    COALESCE(pc.ab_experiment4,'') = COALESCE(b.ab_experiment4,'') AND
    COALESCE(pc.ab_variable4,'') = COALESCE(b.ab_variable4,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0)
    ;
delete from custom.fact_quests where date >= dateadd(day,-1,current_date);

insert into custom.fact_quests
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,level
    ,quest_id
    ,action
    ,task_id
    ,rc_out
    ,coins_in
)
select
    trunc(ts_pretty) as date
    ,ts_pretty
    ,MD5(app_id||user_id) as user_key
    ,app_id as app
    ,user_id as uid
    ,cast(json_extract_path_text(properties,'level') as smallint) as level
    ,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value') as quest_id
    ,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key') as action
    ,json_extract_path_text(json_extract_path_text(properties,'c1'),'value') as task_id
    ,case when json_extract_path_text(properties,'rc_out') = '' then 0 else cast(json_extract_path_text(properties,'rc_out') as INTEGER) end as rc_out
    ,case when json_extract_path_text(properties,'coins_in') = '' then 0 else cast(json_extract_path_text(properties,'coins_in') as INTEGER) end as coins_in
from custom.events
where event = 'quest' and
    trunc(ts_pretty) >= dateadd(day,-1,current_date)
;

--/* 2.0 dim_user does NOT have the column 'is_tester' */
-- delete
-- from processed.fact_quests
-- using processed.dim_user u
-- where processed.fact_quests.user_key = u.user_key and u.is_tester != 0;

-- /* the duplication is caused by different ts give the same user_key, should not just delete the user from the table.
-- remove duplicated records for quest
-- create temp table fact_quests_duplicated_records as
--select user_key, quest_id, action, task_id, count(1) as count
--from processed.fact_quests
--group by 1,2,3,4
-- having count(1) > 1;
--delete
--from processed.fact_quests
--using fact_quests_duplicated_records d
--where processed.fact_quests.user_key = d.user_key;


-- create agg_quests
create temp table quests_start_finish as
select
    cast(s.date as VARCHAR) as date
    ,s.date as date_start
    ,f.date as date_finish
    ,s.user_key
    ,s.app
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,u.install_date
    ,u.last_login_date
    ,s.level
    ,s.quest_id
    ,s.ts as ts_start
    ,f.ts as ts_finish
    ,f.coins_in
from
	(select *
	from custom.fact_quests
	where action = 'start_quest') s
left join
     custom.dim_user u on s.user_key = u.user_key
left join
	(select *
	from custom.fact_quests
	where action = 'finish_quest') f
       on s.quest_id = f.quest_id and s.user_key = f.user_key;

delete from quests_start_finish where date_start < '2014-08-01';
delete from quests_start_finish where date_start = CURRENT_DATE;
delete from quests_start_finish where ts_start > ts_finish;
--delete from quests_start_finish where country is null;
delete from quests_start_finish where level > 200;

drop table if exists custom.agg_quests;
create table custom.agg_quests as
select
    date
    ,date_start
    ,date_finish
    ,app
    ,country
    ,install_source
    ,install_source_group
    ,install_date
    ,last_login_date
    ,level
    ,quest_id
    --,count(1) as start_count
    ,count(distinct user_key) as start_count
    --,sum(case when ts_finish is null then 0 else 1 end) as finish_count
    ,count(distinct case when ts_finish is not null then user_key else null end) as finish_count
    ,sum(coalesce(datediff(minutes, ts_start, ts_finish), 0)) as duration_minutes
from quests_start_finish
group by 1,2,3,4,5,6,7,8,9,10,11;

-- create skip_quests
drop table if exists custom.agg_skip_quests;
create table custom.agg_skip_quests as
select
    q.date
    ,q.app
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,u.install_date
    ,u.last_login_date
    ,q.level
    ,q.quest_id
    ,q.task_id
    ,count(distinct q.user_key) as skip_task_count
    ,sum(q.rc_out) as rc_out
from custom.fact_quests q
    join custom.dim_user u on q.user_key = u.user_key
where action = 'skip_task'
group by 1,2,3,4,5,6,7,8,9,10;
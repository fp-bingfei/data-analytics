insert into custom.item_transaction
(
	date
	, type
	, item_id
	, item_name
	, item_type
	, item_class
	, app_id
	, scene
	, install_source
	, action
	, action_detail
	, level
	, transaction_type
	, item_count
)
select 
	date_trunc('day', ts_pretty) as date
    ,'items_used' as type
	, json_extract_path_text(items_used, 'd_item_id') as item_id
	, json_extract_path_text(items_used, 'd_item_name') as item_name
	, json_extract_path_text(items_used, 'd_item_type') as item_type
	, json_extract_path_text(items_used, 'd_item_class') as item_class
	, app_id
	, json_extract_path_text(properties, 'scene') as scene
	, json_extract_path_text(properties, 'install_source') as install_source
	, json_extract_path_text(properties, 'action') as action
	, nullif(json_extract_path_text(properties, 'action_detail')::varchar, '')::VARCHAR(255) as action_detail
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, json_extract_path_text(properties, 'd_transaction_type') as transaction_type
	, sum(nullif(json_extract_path_text(items_used, 'm_item_amount')::varchar, '')::int) as item_count 
from
    (select 
    	*
    	, json_extract_array_element_text(g.exploded_items, seq.i) as items_used 
    from
        (select 
        	*
        	, replace(json_extract_path_text(properties, 'c_items_used'),'\\','') as exploded_items
----json_extract_path_text(properties, 'c_items_used') as exploded_items 
        from finance.raw_transaction 
        where date(ts_pretty)='${SCHEDULE_DATE}'
			and json_extract_path_text(properties, 'scene') = '2'
        ) as g, seq_0_to_100 as seq
    where seq.i<json_array_length(g.exploded_items)
    )

group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13;
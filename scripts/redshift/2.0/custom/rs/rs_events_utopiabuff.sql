delete from custom_reports.utopiabuff where date >= '_YESTERDAY_';

INSERT INTO custom_reports.utopiabuff 
select 
date
,app_id
,cast(level as integer) as level
,type
,count(distinct user_id) as distinct_user_count
from
	(
	select 
		date(ts_pretty) as date
		,app_id
		,user_id
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key') as type
	from custom.events
	where 
		event = 'utopiaBuff'
		and ts_pretty>='_YESTERDAY_'
	)
group by 1,2,3,4
;
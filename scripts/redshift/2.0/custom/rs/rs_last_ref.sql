delete from royal_story.first_login_source where date >='_YESTERDAY_';

INSERT INTO royal_story.first_login_source
with session as (
select 
	date
	,app_id
	,case when last_ref = 'bookmarks' then 'bookmarks'
	      when last_ref = 'canvas_bookmark' then 'canvas_bookmark'
	      when last_ref = 'sidebar_bookmark' then 'sidebar_bookmark'
	      when last_ref ='' then 'organic'
	      when last_ref = 'notif' then 'notif'
	      when last_ref like 'feed%' then 'feed'
	      when last_ref = 'desktop_app' then 'desktop_app'
	      when last_ref = 'redeem' then 'redeem'
	      when last_ref = 'rightcolumn' then 'right_column'
	      when last_ref = 'appcenter' then 'app_center'
	      when last_ref = 'request' then 'request'
	      when last_ref = 'reminders' then 'reminders'
	      when last_ref ='fbpage' then 'fbpage'
	      when last_ref = 'sidebar_recommmended' then 'sidebar_recommnended'
	      when last_ref = 'search' then 'search'
	      when last_ref = 'timeline' then 'timeline'
	      when last_ref like 'RS%' then 'facebook_new_installs'
	      else 'others' end as session_source
	,last_ref
	,user_key
	,level_start
	,country
from
(
select 
    *
	,rank() over(partition by date,app_id,user_key order by ts_start) as rk
from
	(
	select 
		trunc(ts_start) as date
		,*
	from kpi_processed.fact_session
	where 
		ts_start >='_YESTERDAY_'
		and app_id like 'royal%'
	)
)
where rk=1
)

,users as (
select 
date
,app_id
,is_new_user
,user_key
,os
,case when login_days_7>=6 then 'highly_active'
      when login_days_7>0 and login_days_7<6 then 'active'
      else 'inactive_7' end as activity_7
,case when login_days_30>0 then 'active_30'
      else 'inactive_30' end as activity_30
,case when login_days_1>0 then 'y' else 'n' end as d1_return
from
	(
	select 
		a.date
		,a.app_id
		,a.is_new_user
		,a.user_key
		,a.os
		,count(distinct case when datediff(day,a.date, b.date)>=-7 and b.date is not null then b.date else null end) as login_days_7
		,count(distinct case when datediff(day,a.date, b.date)>=-30 and b.date is not null then b.date else null end) as login_days_30
		,count(distinct case when datediff(day,a.date, b.date)=-1 and b.date is not null then b.date else null end) as login_days_1

	from 
	(select * from kpi_processed.fact_dau_snapshot where date >='_YESTERDAY_' and app_id like 'royal%') a 
	left join kpi_processed.fact_dau_snapshot b
		on a.app_id = b.app_id
		and a.user_key = b.user_key
		and datediff(day,a.date, b.date)<0
		and datediff(day,a.date, b.date)>=-30
	group by 1,2,3,4,5
	)
)

select 
a.date
,a.app_id
,a.is_new_user
,a.activity_7
,a.activity_30
,a.d1_return
,a.os
,b.session_source
,b.last_ref
,case when b.level_start <=10 then '<=10'
      when b.level_start >10 and b.level_start <=20 then '(10, 20]'
      when b.level_start >20 and b.level_start <=30 then '(20, 30]'
      when b.level_start >30 and b.level_start <=40 then '(30, 40]'
      when b.level_start >40 and b.level_start <=50 then '(40, 50]'
      when b.level_start >50 and b.level_start <=60 then '(50, 60]'
      when b.level_start >60 and b.level_start <=70 then '(60, 70]'
      when b.level_start >70 and b.level_start <=80 then '(70, 80]'
      when b.level_start >80 and b.level_start <=90 then '(80, 90]'
      when b.level_start >90 and b.level_start <=100 then '(90, 100]'
      else '>100' end as level_start
,b.country
,count(distinct a.user_key) as users 
from users a 
left join session b
	on a.date = b.date
	and a.app_id = b.app_id
	and a.user_key = b.user_key
group by 1,2,3,4,5,6,7,8,9,10,11
;

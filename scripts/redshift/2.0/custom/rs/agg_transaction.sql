delete from custom.currency_transaction where date='${SCHEDULE_DATE}';


--delete from custom.currency_transaction where date>=(select min(date_trunc('day', ts_pretty)) from --finance.raw_transaction );

-- currency received transaction
insert into custom.currency_transaction
(
    date
    , type
    , currency_type
    , app_id
    , scene
    , install_source
    , action
    , level
    , transaction_type
    , currency_count
)
select 
    date_trunc('day', ts_pretty) as date
    , 'currency_received' as type
    , json_extract_path_text(currency_received, 'd_currency_type')::VARCHAR(64) as currency_type
    , app_id
    , json_extract_path_text(properties, 'scene')::VARCHAR(64) as scene
    , json_extract_path_text(properties, 'install_source')::VARCHAR(128) as install_source
    , json_extract_path_text(properties, 'action')::VARCHAR(128) as action
    , nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
    , json_extract_path_text(properties, 'd_transaction_type')::VARCHAR(64) as transaction_type
    , sum(nullif(json_extract_path_text(currency_received, 'm_currency_amount')::varchar, '')::int) as currency_count 
from 
    (select 
        *
        , json_extract_array_element_text(g.exploded_items, seq.i) as currency_received 
    from
        (select 
            *
            , replace(json_extract_path_text(properties, 'c_currency_received'),'\\','') as exploded_items 
        from finance.raw_transaction 
        where date(ts_pretty)='${SCHEDULE_DATE}'
        ) as g, seq_0_to_100 as seq 
    where seq.i<json_array_length(g.exploded_items)
    )   
    
group by 1, 2, 3, 4, 5, 6, 7, 8, 9;


-- currency spent transaction
insert into custom.currency_transaction
(   
    date
    , type
    , currency_type
    , app_id
    , scene
    , install_source
    , action
    , level
    , transaction_type
    , currency_count
)
select 
    date_trunc('day', ts_pretty) as date
    , 'currency_spent' as type
    , json_extract_path_text(currency_spent, 'd_currency_type')::VARCHAR(64) as currency_type
    , app_id
    , json_extract_path_text(properties, 'scene')::VARCHAR(64) as scene
    , json_extract_path_text(properties, 'install_source')::VARCHAR(128) as install_source
    , json_extract_path_text(properties, 'action')::VARCHAR(128) as action
    , nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
    , json_extract_path_text(properties, 'd_transaction_type')::VARCHAR(64) as transaction_type
    , sum(nullif(json_extract_path_text(currency_spent, 'm_currency_amount')::varchar, '')::int) as currency_count 
from 
    (select 
        *
        , json_extract_array_element_text(g.exploded_items, seq.i) as currency_spent 
    from
        (select 
            *
            , replace(json_extract_path_text(properties, 'c_currency_spent'),'\\','') as exploded_items 
        from finance.raw_transaction
        where date(ts_pretty)='${SCHEDULE_DATE}'
        ) as g, seq_0_to_100 as seq 
        where seq.i<json_array_length(g.exploded_items)
        )   
group by 1, 2, 3, 4, 5, 6, 7, 8, 9;


-- item received transaction

delete from custom_processed.agg_botanic_shed where date>=dateadd(day,-1,current_date) and date<current_date;

insert into custom_processed.agg_botanic_shed

with events as (
select * from
	(
	select 
		app_id
		,date
		,user_id
		,properties
		,row_number() over(partition by app_id,date,user_id order by ts_pretty desc) as rk
	from
		(
		select 
			app_id
			,date(ts_pretty) as date
			,user_id
			,properties
			,ts_pretty
		from custom.events
		where 
		event = 'botanic_shed'
		and date(ts_pretty)>=dateadd(day,-1,current_date)
		and date(ts_pretty)<current_date
		)
	)
where rk=1
)
,temp as (
select 
app_id
,date
,user_id
,properties
,install_date
,case when days2lastlogin <=7 then '7'
      when days2lastlogin >7 and days2lastlogin <=30 then '30'
      when days2lastlogin >30 and days2lastlogin <=60 then '60'
      when days2lastlogin >60 and days2lastlogin <=90 then '90'
      when days2lastlogin >90 and days2lastlogin <=180 then '180'
      else '180+' end as last_login_days
from
	(
	select 
		e.*
		,d.install_date
		,datediff('day',d.last_login_date,date) as days2lastlogin
	from events e 
	left join processed.dim_user d 
	on e.app_id = d.app_id
	and e.user_id = d.user_id
	)

)
select * from
	(
	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),0),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),0),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),1),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),1),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),2),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),2),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),3),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),3),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),4),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),4),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),5),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),5),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),6),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),6),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),7),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),7),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),8),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),8),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),9),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),9),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),10),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),10),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11

	union all 

	select 
		app_id
		,date
		,install_date
		,last_login_days
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'key')as theme
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')as theme_value
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'key')as status
		,json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value') as status_value
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),11),'key') as slot
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),11),'value') as slot_level
		,count(distinct user_id) as user_count
	from temp
	group by 1,2,3,4,5,6,7,8,9,10,11
	)
where slot <> ''
;

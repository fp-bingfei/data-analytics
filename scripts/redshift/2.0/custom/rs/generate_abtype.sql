delete from royal_story.player_abtype 
	where date>=(select start_date 
				from kpi_processed.init_start_date);

insert into royal_story.player_abtype
select
	date(ts_pretty) as date,
	md5(app_id||user_id) as user_key,
	json_extract_path_text(properties,'abtype') 
from raw_events.events 
where 
	event='session_start' 
	and app_id like 'royal%'
	and date(ts_pretty)>=(select start_date 
		from kpi_processed.init_start_date)
	and properties like '%abtype%' 
group by 1,2,3;


delete from custom.agg_order_item where date='${SCHEDULE_DATE}';
	
-- finishElfOrder reward items aggregation 
insert into custom.agg_order_item
(
	date
	, event
	, key_name
	, item_name
	, app_id
	, scene
	, install_source
	, country_code
	, level
	, coins_cnt
	, item_cnt
	, user_cnt
)
select 
	date_trunc('day', ts_pretty) as date 
    , event
	, json_extract_path_text(json_extract_path_text(properties, 'c1'), 'key') as key_name
	, json_extract_path_text(idents, 'ident') as item_name
	, app_id
	, json_extract_path_text(properties, 'scene') as scene
	, json_extract_path_text(properties, 'install_source')::varchar(128) as install_source
	, json_extract_path_text(properties, 'country_code') as country_code
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, 0 as coins_cnt
	, sum(nullif(json_extract_path_text(idents, 'count')::varchar, '')::int) as item_cnt
	, count(user_id) as user_cnt 
from 
    (select 
    	*
    	, json_extract_array_element_text(g.exploded_items, seq.i) as idents 
    from
        (select 
        	*
        	, json_extract_path_text(json_extract_path_text(properties, 'c1'), 'value') as exploded_items 
        from custom.events where date(ts_pretty)='${SCHEDULE_DATE}' and event in ('finishElfOrder') ) as g
        , seq_0_to_100 as seq 
    where seq.i<json_array_length(g.exploded_items)
    )   
    
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10; 



-- finishOrder coins aggregation 

insert into custom.agg_order_item
(
	date
	, event
	, key_name
	, item_name
	, app_id
	, scene
	, install_source
	, country_code
	, level
	, coins_cnt
	, item_cnt
	, user_cnt
)
select 
	date_trunc('day', ts_pretty) as date
    , event
	, json_extract_path_text(json_extract_path_text(properties, 'm1'), 'key') as key_name
	, '' as item_name
	, app_id
	, json_extract_path_text(properties, 'scene') as scene
	, json_extract_path_text(properties, 'install_source')::varchar(128) as install_source
	, json_extract_path_text(properties, 'country_code') as country_code
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, nullif(json_extract_path_text(json_extract_path_text(properties, 'm1'), 'value')::varchar, '')::int as coins_cnt
	, 0 as item_cnt
	, count(user_id) as user_cnt 
from custom.events 
where date(ts_pretty)='${SCHEDULE_DATE}' and event in ('finishOrder')
      
group by 1, 2, 3, 4, 5, 6, 7, 8, 9,10; 


-- refresh order aggregation 
insert into custom.agg_order_item
(
	date
	, event
	, key_name
	, item_name
	, app_id
	, scene
	, install_source
	, country_code
	, level
	, coins_cnt
	, item_cnt
	, user_cnt
)
select 
	date_trunc('day', ts_pretty) as date
    , event
	, 'refreshOrder' as key_name
	, '' as item_name
	, app_id
	, json_extract_path_text(properties, 'scene') as scene
	, json_extract_path_text(properties, 'install_source')::varchar(128) as install_source
	, json_extract_path_text(properties, 'country_code') as country_code
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, 0 as coins_cnt
	, 0 as item_cnt
	, count(user_id) as user_cnt 
from custom.events 
where date(ts_pretty)='${SCHEDULE_DATE}' and event in ('refreshOrder')
      
group by 1, 2, 3, 4, 5, 6, 7, 8, 9,10; 

-- correct scene from main to 2, because sometimes when turn to main scene this field is send a wrong value
update custom.agg_order_item set scene='2' where scene='1';

-------------------------------------------------------------------------
-- complete discover events 
delete from custom.complete_discover_events  where date='${SCHEDULE_DATE}';
insert into custom.complete_discover_events
(	date
	, app_id
	, user_id
	, session_id
	, event
	, level
	, install_source
	, scene
	, lang
	, dis_map_num
	, dis_start_time
	, dis_end_time
	, dis_map_xp
	, dis_map_fog
)
select 
	date_trunc('day', ts_pretty) as date
	, app_id
	, user_id
	, session_id
  , event
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, json_extract_path_text(properties, 'install_source')::varchar(128) as install_source
	, json_extract_path_text(properties, 'scene')::varchar(32) as scene
	, json_extract_path_text(properties, 'lang')::varchar(32) as lang	
	, nullif(json_extract_path_text(properties, 'dis_map_num')::varchar, '')::int as dis_map_num
	, (TIMESTAMP 'epoch' + (json_extract_path_text(properties, 'dis_start_time')::int) * INTERVAL '1 Second ')  as dis_start_time
	, (TIMESTAMP 'epoch' + (json_extract_path_text(properties, 'dis_end_time')::int) * INTERVAL '1 Second ') as dis_end_time
	, sum(nullif(json_extract_path_text(properties, 'dis_map_xp')::varchar, '')::int) as dis_map_xp
	, sum(nullif(json_extract_path_text(properties, 'dis_map_fog')::varchar, '')::int) as dis_map_fog	
	
from custom.events 
where date(ts_pretty)='${SCHEDULE_DATE}' and event='completeDiscover'
      
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12; 

-- date(dis_start_time)>'2015-01-01'  because when dis_map_num = 0 dis_start_time is 1970-01-01, will get wrong and big hours.
drop table if exists tmp_complete_discover_0;
create temp table tmp_complete_discover_0 as 
	select * from  custom.complete_discover_events c
	where c.dis_map_num = 0;


update custom.complete_discover_events
set dis_start_time = t.dis_end_time
from tmp_complete_discover_0 t
where  custom.complete_discover_events.user_id = t.user_id 
			and custom.complete_discover_events.app_id = t.app_id 
			and custom.complete_discover_events.session_id = t.session_id
			and custom.complete_discover_events.scene = t.scene
			and custom.complete_discover_events.lang = t.lang
			and custom.complete_discover_events.install_source = t.install_source
			and custom.complete_discover_events.dis_map_num = 1
			and custom.complete_discover_events.date='${SCHEDULE_DATE}';

-- update time duration for every layer
update custom.complete_discover_events 
set dis_map_duration = datediff(hour, dis_start_time, dis_end_time)
where date='${SCHEDULE_DATE}' and date(dis_start_time)>'2015-01-01';



-- daily mission
delete from custom.agg_daily_mission_item where date = '${SCHEDULE_DATE}';
insert into custom.agg_daily_mission_item
(
	date
	, ts_pretty
	, event
	, daily_mission_type
	, item_id
	, app_id
	, scene
	, install_source
	, country_code
	, level
	, item_cnt
	, user_cnt
)
select 
	date_trunc('day', ts_pretty) as date
	, ts_pretty
  , event
	, json_extract_path_text(json_extract_path_text(properties, 'd_c1'), 'value') as daily_mission_type
	, json_extract_path_text(idents, 'item_id') as item_id
	, app_id
	, json_extract_path_text(properties, 'scene') as scene
	, json_extract_path_text(properties, 'install_source')::varchar(128) as install_source
	, json_extract_path_text(properties, 'country_code') as country_code
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, sum(nullif(json_extract_path_text(idents, 'item_amount')::varchar, '')::int) as item_cnt
	, count(distinct user_id) as user_cnt 
from 
    (select 
    	*
    	, json_extract_array_element_text(g.exploded_items, seq.i) as idents 
    from
        (select 
        	*
        	, json_extract_path_text(properties, 'c1') as exploded_items 
        from custom.events where date(ts_pretty)='${SCHEDULE_DATE}' and event='daily_mission' ) as g
        , seq_0_to_100 as seq 
    where seq.i<json_array_length(g.exploded_items)
    )     
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;




-- daily activity
delete from custom.agg_activity where date='${SCHEDULE_DATE}';
insert into custom.agg_activity
(
	date
	, app_id
    , event
    , user_id
    , scene
    , install_source
    , country_code
	, level
	, activity_id
	, action_id
	, process_ident
	, measure_key
	, measure_value
	, activity_cnt
)
select 
	date_trunc('day', ts_pretty) as date
	, app_id
	, event
	, user_id
	, json_extract_path_text(properties, 'scene') as scene
	, json_extract_path_text(properties, 'install_source')::varchar(128) as install_source
	, json_extract_path_text(properties, 'country_code')::varchar(128) as country_code
	, nullif(json_extract_path_text(properties, 'level')::varchar, '')::int as level
	, nullif(json_extract_path_text(json_extract_path_text(properties, 'd_c1'), 'value')::varchar(128), '') as activity_id
	, nullif(json_extract_path_text(json_extract_path_text(properties, 'd_c2'), 'value')::varchar(128), '') as action_id
	, nullif(json_extract_path_text(json_extract_path_text(properties, 'd_c3'), 'value')::varchar(128), '') as process_ident
	, json_extract_path_text(json_extract_path_text(properties, 'm1'), 'key')::varchar(128) as measure_key
	, nullif(json_extract_path_text(json_extract_path_text(properties, 'm1'), 'value')::varchar, '')::int as measure_value
	, count(*) as activity_cnt
from custom.events
where event = 'activity'
	and date(ts_pretty) = '${SCHEDULE_DATE}'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13
	;

-- calendar
delete from custom.calendar_users where signin_date>='${SCHEDULE_DATE}';
insert into custom.calendar_users
select 
	md5(app_id||user_id) as user_key,
	to_date(json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value'), 'YYYY-MM-DD') as signin_date
from custom.events
where event='calendar' and date(ts_pretty)>='${SCHEDULE_DATE}';


delete from custom.lapsed_users_last_action 
	where date(update_ts)='${SCHEDULE_DATE}';

-- 7 days
drop table if exists lapsed_7_days;
create temp table lapsed_7_days
as 
select app, uid 
from processed.dim_user 
where '${SCHEDULE_DATE}' - trunc(last_login_ts) = 7;

insert into custom.lapsed_users_last_action
(
	update_ts
	,ts
	, action
	, user_key
	, app
	, uid
	, lapsed_days
	, action_detail
	, level
	, location
	, country_code
	, install_source
	, rc_in
	, rc_out
)
select 
	update_ts
	,ts
	, action
	, user_key
	, app
	, uid
	, lapsed_days
	, action_detail
	, level
	, location
	, country_code
	, install_source
	, rc_in
	, rc_out
from (
	select 
		cast('${SCHEDULE_DATE} 00:00:00' as timestamp) as update_ts
		, e.ts as ts
		, json_extract_path_text(e.properties,'action') as action
		, MD5(e.app||e.uid) as user_key
		, e.app as app
		, e.uid as uid
		, 7 as lapsed_days
		, json_extract_path_text(e.properties,'action_detail') as action_detail
		, cast(json_extract_path_text(e.properties,'level') as smallint) as level
		, json_extract_path_text(e.properties,'location') as location
		, e.country_code as country_code
		, e.install_source as install_source
		, cast(case 
			when json_extract_path_text(e.properties,'rc_in')='' 
			then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT) as rc_in
		, cast(case 
			when json_extract_path_text(e.properties,'rc_out')='' 
			then '0' else json_extract_path_text(e.properties,'rc_out') end as BIGINT) as rc_out
		, row_number() over(partition by e.app, e.uid order by e.ts desc) as row_num
 
	from rc_transaction_events e, lapsed_7_days l
	where e.event='rc_transaction'
		and e.app = l.app 
		and e.uid =l.uid
		and '${SCHEDULE_DATE}' - date(e.ts) > 5 
		and date(e.ts) < '2015-06-01'
) a where a.row_num=1;


-- 30 days

drop table if exists lapsed_30_days;
create temp table lapsed_30_days
as 
select app, uid 
from processed.dim_user 
where '${SCHEDULE_DATE}' - trunc(last_login_ts) = 30;

insert into custom.lapsed_users_last_action
(
	update_ts
	,ts
	, action
	, user_key
	, app
	, uid
	, lapsed_days
	, action_detail
	, level
	, location
	, country_code
	, install_source
	, rc_in
	, rc_out
)
select 
	update_ts
	,ts
	, action
	, user_key
	, app
	, uid
	, lapsed_days
	, action_detail
	, level
	, location
	, country_code
	, install_source
	, rc_in
	, rc_out
from (
	select 
		cast('${SCHEDULE_DATE} 00:00:00' as timestamp) as update_ts
		, e.ts as ts
		, json_extract_path_text(e.properties,'action') as action
		, MD5(e.app||e.uid) as user_key
		, e.app as app
		, e.uid as uid
		, 30 as lapsed_days
		, json_extract_path_text(e.properties,'action_detail') as action_detail
		, cast(json_extract_path_text(e.properties,'level') as smallint) as level
		, json_extract_path_text(e.properties,'location') as location
		, e.country_code as country_code
		, e.install_source as install_source
		, cast(case 
			when json_extract_path_text(e.properties,'rc_in')='' 
			then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT) as rc_in
		, cast(case 
			when json_extract_path_text(e.properties,'rc_out')='' 
			then '0' else json_extract_path_text(e.properties,'rc_out') end as BIGINT) as rc_out
		, row_number() over(partition by e.app, e.uid order by e.ts desc) as row_num
 
	from rc_transaction_events e, lapsed_30_days l
	where e.event='rc_transaction'
		and e.app = l.app 
		and e.uid =l.uid
		and '${SCHEDULE_DATE}' - date(e.ts) > 28 
		and date(e.ts) < '2015-06-01'
) a where a.row_num=1;


-- 60 days 

drop table if exists lapsed_60_days;
create temp table lapsed_60_days
as 
select app, uid 
from processed.dim_user 
where '${SCHEDULE_DATE}' - trunc(last_login_ts) = 60;

insert into custom.lapsed_users_last_action
(
	update_ts
	,ts
	, action
	, user_key
	, app
	, uid
	, lapsed_days
	, action_detail
	, level
	, location
	, country_code
	, install_source
	, rc_in
	, rc_out
)
select 
	update_ts
	,ts
	, action
	, user_key
	, app
	, uid
	, lapsed_days
	, action_detail
	, level
	, location
	, country_code
	, install_source
	, rc_in
	, rc_out
from (
	select 
		cast('${SCHEDULE_DATE} 00:00:00' as timestamp) as update_ts
		, e.ts as ts
		, json_extract_path_text(e.properties,'action') as action
		, MD5(e.app||e.uid) as user_key
		, e.app as app
		, e.uid as uid
		, 60 as lapsed_days
		, json_extract_path_text(e.properties,'action_detail') as action_detail
		, cast(json_extract_path_text(e.properties,'level') as smallint) as level
		, json_extract_path_text(e.properties,'location') as location
		, e.country_code as country_code
		, e.install_source as install_source
		, cast(case 
			when json_extract_path_text(e.properties,'rc_in')='' 
			then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT) as rc_in
		, cast(case 
			when json_extract_path_text(e.properties,'rc_out')='' 
			then '0' else json_extract_path_text(e.properties,'rc_out') end as BIGINT) as rc_out
		, row_number() over(partition by e.app, e.uid order by e.ts desc) as row_num
 
	from rc_transaction_events e, lapsed_60_days l
	where e.event='rc_transaction'
		and e.app = l.app 
		and e.uid =l.uid
		and '${SCHEDULE_DATE}' - date(e.ts) > 58 
		and date(e.ts) < '2015-06-01'
) a where a.row_num=1;





------------ agg_last_ref

create temp table tmp_dim_user as
select
    u.app_id
    ,u.user_key
    ,u.os
    ,u.country
    ,u.language
    ,u.is_payer
    ,u.last_ref
    ,coalesce(g.install_source_group, 'others') as install_source_group
from  processed.dim_user u 
    left join processed.install_source_group g on u.last_ref = g.install_source
;




drop table if exists custom.agg_last_ref;
create table custom.agg_last_ref as
select
    s.date as date
    ,u.app_id
    ,u.os
    ,u.country
    ,u.language
    ,u.is_payer
    ,u.last_ref
    ,u.install_source_group
    ,count(distinct s.user_key) as dau
from processed.fact_dau_snapshot s
    join tmp_dim_user u on s.app_id = u.app_id and s.user_key = u.user_key
group by 1,2,3,4,5,6,7,8;
delete from custom_reports.whackamole where date>='_YESTERDAY_';

INSERT INTO custom_reports.whackamole
select 
	date
	,app_id
	,cast(level as integer) as level
	,action_type
	,cast(active as integer) as active
	,count(distinct user_id) as distinct_user_cnt
	,count(user_id) as user_cnt
	,sum(cast(token as numeric)) as token_sum
	,sum(cast(combo as numeric)) as combo_sum
	,sum(cast(getscore as numeric)) as getscore_sum
	,sum(cast(getcoins as numeric)) as getcoins_sum
from
(
select
	date
	,app_id
	,user_id
	,level
	,action_type
	,active
	,case when token = '' then '0' else token end as token
	,case when combo = '' then '0' else combo end as combo
	,case when getscore = '' then '0' else getscore end as getscore
	,case when getcoins = '' then '0' else getcoins end as getcoins
from
	(
	select 
		date(ts_pretty) as date
		,app_id
		,user_id
		,json_extract_path_text(properties,'level') as level
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),0),'value')  as active
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),1),'value')  as token
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),2),'value')  as combo
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),3),'value')  as getscore
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(properties,'c1'),4),'value')  as getcoins
		,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value') as action_type
	from custom.events
	where 
		event = 'whackamole'
		and ts_pretty>='_YESTERDAY_'
	)
)
group by 1,2,3,4,5
;

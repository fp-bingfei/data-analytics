-----------------------------
-- Dump tbl_user to Redshift
-----------------------------

DROP TABLE if exists raw_data.rs_tbl_user;
CREATE TABLE raw_data.rs_tbl_user
(
   app             varchar(50) ENCODE lzo,
   uid             integer,
   snsid           varchar(100) NOT NULL ENCODE lzo,
   install_source  varchar(100) ENCODE lzo,
   install_ts      timestamp,
   os              varchar(10) ENCODE lzo,
   os_version      varchar(100) ENCODE lzo,
   country_code    varchar(10) ENCODE lzo,
   level           integer,
   device          varchar(100) ENCODE lzo,
   language        varchar(10) ENCODE lzo,
   rc              varchar(25) ENCODE lzo,
   coins           varchar(25) ENCODE lzo,
   email           varchar(100) ENCODE lzo,
   gender          varchar(20) ENCODE lzo,
   name            varchar(5000) ENCODE lzo,
   age_num         varchar(10) ENCODE lzo,
   birthday        varchar(30) ENCODE lzo,
   friends_total_count varchar(10) ENCODE lzo
);

DROP TABLE if exists raw_data.rs_tbl_user_staging;
CREATE TABLE raw_data.rs_tbl_user_staging
(
  app varchar(50) ENCODE lzo,
  uid varchar(32) encode lzo,
  snsid varchar(100) ENCODE lzo,
  install_source varchar(100) ENCODE lzo,
  install_ts varchar(100) ENCODE LZO,
  lang varchar(10) ENCODE lzo,
  rc varchar(30) ENCODE lzo,
  coins varchar(30) ENCODE lzo,
  email varchar(100) ENCODE lzo,
  gender varchar(20) ENCODE lzo,
  name varchar(100) ENCODE lzo,
  birthday varchar(30) ENCODE lzo,
  friends_total_count varchar(10) ENCODE lzo
);


copy raw_data.rs_tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/results/rs/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO raw_data.rs_tbl_user
select 
  app,
  cast (case when uid = '' then '0' else uid end as integer) as uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null,
  birthday,
  friends_total_count
  from raw_data.rs_tbl_user_staging  
   where snsid is not NULL
;


commit;

drop table dragonwar.notif_send;
create table dragonwar.notif_send as
select
    distinct app_id,
    ts,
    TIMESTAMP 'epoch' + CAST(ts AS BIGINT)/1000 * INTERVAL '1 Second ' as ts_pretty,
    user_id,
    md5(app_id||user_id) as user_key,
    event,
    json_extract_path_text(properties, 'source') as push_source,
    json_extract_path_text(properties, 'os') as os,
    json_extract_path_text(properties, 'gameserver_id') as gameserver_id,
    json_extract_path_text(properties, 'push_status') as push_status,
    json_extract_path_text(properties, 'fail_reason') as fail_reason,
    json_extract_path_text(properties, 'notif_id') as notif_id,
    json_extract_path_text(properties, 'notif_name') as notif_name
from dragonwar.raw_notif
where event='push-server-request';

delete from dragonwar.notif_click where trunc(TIMESTAMP 'epoch' + CAST(ts AS BIGINT)/1000 * INTERVAL '1 Second ') = current_date-1;
insert into dragonwar.notif_click
select
    distinct app_id,
    ts,
    TIMESTAMP 'epoch' + CAST(ts AS BIGINT)/1000 * INTERVAL '1 Second ' as ts_pretty,
    user_id,
    md5(app_id||user_id) as user_key,
    event,
    json_extract_path_text(properties, 'source') as push_source,
    json_extract_path_text(properties, 'os') as os,
    json_extract_path_text(properties, 'gameserver_id') as gameserver_id,
    json_extract_path_text(properties, 'push_status') as push_status,
    json_extract_path_text(properties, 'fail_reason') as fail_reason,
    json_extract_path_text(properties, 'notif_id') as notif_id,
    json_extract_path_text(properties, 'notif_name') as notif_name,
    json_extract_path_text(properties, 'client_version') as client_version
from dragonwar.raw_notif
where event in ('delivered','open')
and trunc(TIMESTAMP 'epoch' + CAST(ts AS BIGINT)/1000 * INTERVAL '1 Second ') = current_date-1;
--------------------------
--kpi_by_device for KOA
------------------------------------------------
--Data 2.0 fact_session.sql
------------------------------------------------
--fact_session

delete from kpi_by_device.fact_session
where date_start >= 
                (
                   select start_date
                   from   kpi_processed.init_start_date
                )
                and app_id in ('koa.global.prod', 'vegas.global.prod')
                ;

CREATE TEMP TABLE tmp_fact_session AS
select   md5(s.app_id||s.user_id||s.session_id) as id
        ,s.app_id 
        ,s.app_version::varchar(32)
        ,md5(s.app_id||s.android_id) as user_key
        ,s.user_id
        ,trunc(s.ts_pretty) as date_start
        ,trunc(e.ts_pretty) as date_end
        ,s.ts_pretty as ts_start
        ,e.ts_pretty as ts_end
        ,s.install_ts     
        ,trunc(s.install_ts) as install_date     
        ,s.session_id     
        ,s.facebook_id    
        ,s.install_source 
        ,case when s.os is null then 'Unknown'
      when s.os='null' then 'Unknown'
      when s.os='' then 'Unknown'
      when s.os='ios' then 'iOS'
      when s.os='android' then 'Android'
      when s.os='windows' then 'Windows'
    else s.os end  as os     
        ,s.os_version     
        ,case when s.browser is null then 'Unknown'
      when s.browser='' then 'Unknown'
    else s.browser end as browser        
        ,s.browser_version
        ,s.device         
        ,coalesce(c.country,'Unknown')  as country      
        ,s.email          
        ,s.first_name     
        ,s.last_name      
        ,s.level as level_start
        ,e.level as level_end
        ,s.vip_level
        ,s.gender         
        ,s.birthday       
        ,s.ip             
        ,s.lang as language
        ,coalesce(e.session_length,0) as playtime_sec
        ,s.scene
        ,s.fb_source
        ,s.last_ref
        --,s.gameserver_id
        ,s.android_id as gaid
        ,s.idfa
from    kpi_processed.session_start s
left join kpi_processed.session_end e
on      s.session_id = e.session_id
and     s.app_id = e.app_id
and     s.user_id = e.user_id 
and     s.scene=e.scene  
and     e.ts_pretty >=
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
left join kpi_processed.dim_country c
on      s.country_code = c.country_code
where   s.ts_pretty >=
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
        and s.app_id in ('koa.global.prod', 'vegas.global.prod')
        and s.os = 'Android'
UNION
select   md5(s.app_id||s.user_id||s.session_id) as id
        ,s.app_id 
        ,s.app_version::varchar(32)
        ,md5(s.app_id||s.idfv) as user_key
        ,s.user_id
        ,trunc(s.ts_pretty) as date_start
        ,trunc(e.ts_pretty) as date_end
        ,s.ts_pretty as ts_start
        ,e.ts_pretty as ts_end
        ,s.install_ts     
        ,trunc(s.install_ts) as install_date     
        ,s.session_id     
        ,s.facebook_id    
        ,s.install_source 
        ,s.os     
        ,s.os_version     
        ,case when s.browser is null then 'Unknown'
      when s.browser='' then 'Unknown'
    else s.browser end as browser        
        ,s.browser_version
        ,s.device         
        ,coalesce(c.country,'Unknown')  as country      
        ,s.email          
        ,s.first_name     
        ,s.last_name      
        ,s.level as level_start
        ,e.level as level_end
        ,s.vip_level
        ,s.gender         
        ,s.birthday       
        ,s.ip             
        ,s.lang as language
        ,coalesce(e.session_length,0) as playtime_sec
        ,s.scene
        ,s.fb_source
        ,s.last_ref
        --,s.gameserver_id
        ,s.idfv as gaid
        ,s.idfa
from    kpi_processed.session_start s
left join kpi_processed.session_end e
on      s.session_id = e.session_id
and     s.app_id = e.app_id
and     s.user_id = e.user_id 
and     s.scene=e.scene  
and     e.ts_pretty >=
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
left join kpi_processed.dim_country c
on      s.country_code = c.country_code
where   s.ts_pretty >=
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
        and s.app_id in ('koa.global.prod', 'vegas.global.prod')
        and s.os in ('iPhone OS', 'iOS')
        ;

insert into kpi_by_device.fact_session
select
  id
  ,app_id
  ,app_version
  ,user_key
  ,user_id
  ,date_start
  ,date_end
  ,ts_start
  ,ts_end
  ,install_ts
  ,install_date
  ,session_id
  ,facebook_id
  ,install_source
  ,os
  ,os_version
  ,browser
  ,browser_version
  ,device
  ,country
  ,email
  ,first_name
  ,last_name
  ,level_start
  ,level_end
  ,vip_level
  ,gender
  ,birthday
  ,ip
  ,language
  ,playtime_sec
  ,scene
  ,fb_source
  ,last_ref
  --,gameserver_id
  ,gaid
  ,idfa
from tmp_fact_session;
                 
--fact_revenue
delete from kpi_by_device.fact_revenue
where date >= 
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
                and app_id in ('koa.global.prod', 'vegas.global.prod')
                ;

insert into kpi_by_device.fact_revenue
select      md5(app_id||md5(p.app_id||p.user_id)||transaction_id) as id               
           ,p.app_id           
           ,p.app_version      
           ,md5(p.app_id||p.android_id) as user_key
           ,p.user_id
           ,trunc(p.ts_pretty) as date
           ,p.ts_pretty               
           ,p.install_ts               
           ,date(p.install_ts)               
           ,p.session_id       
           ,p.level            
           ,p.vip_level        
           ,p.os         
           ,p.os_version       
           ,p.device           
           ,case when p.browser is null then 'Unknown'
when p.browser='' then 'Unknown'
else p.browser end as browser           
           ,p.browser_version  
           ,coalesce(cy.country,'Unknown') as country          
           ,p.ip               
           ,p.install_source
           ,p.lang as language         
           ,p.payment_processor
           ,p.iap_product_id   
           ,p.iap_product_name 
           ,p.iap_product_type 
           ,p.currency         
           ,p.amount*1.0000/100 as revenue_amount   
           ,nvl(p.amount*1.0000*c.factor/100, 0) as revenue_usd      
           ,p.transaction_id  
           ,p.scene  
           ,p.fb_source
           ,0
           ,0
           ,0
           ,p.android_id as gaid,
           p.idfa
from       kpi_processed.payment p
left join  kpi_processed.currency c
on         p.currency = c.currency
and        trunc(p.ts_pretty) = c.dt
left join kpi_processed.dim_country cy
on        p.country_code = cy.country_code
where     p.ts_pretty >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
          and p.app_id in ('koa.global.prod', 'vegas.global.prod')
          and p.os = 'Android'
          and p.user_id not in (select distinct user_id from kpi_processed.payment_test_users where app_id in ('koa.global.prod', 'vegas.global.prod'))
UNION
select      md5(app_id||md5(p.app_id||p.user_id)||transaction_id) as id               
           ,p.app_id           
           ,p.app_version      
           ,md5(p.app_id||p.idfv) as user_key
           ,p.user_id
           ,trunc(p.ts_pretty) as date
           ,p.ts_pretty               
           ,p.install_ts               
           ,date(p.install_ts)               
           ,p.session_id       
           ,p.level            
           ,p.vip_level        
           ,p.os            
           ,p.os_version       
           ,p.device           
           ,case when p.browser is null then 'Unknown'
when p.browser='' then 'Unknown'
else p.browser end as browser           
           ,p.browser_version  
           ,coalesce(cy.country,'Unknown') as country          
           ,p.ip               
           ,p.install_source
           ,p.lang as language         
           ,p.payment_processor
           ,p.iap_product_id   
           ,p.iap_product_name 
           ,p.iap_product_type 
           ,p.currency         
           ,p.amount*1.0000/100 as revenue_amount   
           ,nvl(p.amount*1.0000*c.factor/100, 0) as revenue_usd      
           ,p.transaction_id  
           ,p.scene  
           ,p.fb_source
           ,0
           ,0
           ,0
           ,p.idfv as gaid,
           p.idfa
from       kpi_processed.payment p
left join  kpi_processed.currency c
on         p.currency = c.currency
and        trunc(p.ts_pretty) = c.dt
left join kpi_processed.dim_country cy
on        p.country_code = cy.country_code
where     p.ts_pretty >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
          and p.app_id in ('koa.global.prod', 'vegas.global.prod')
          and p.os in ('iPhone OS', 'iOS')
          and p.user_id not in (select distinct user_id from kpi_processed.payment_test_users where app_id in ('koa.global.prod', 'vegas.global.prod'))
          ;


--fact_new_user
delete from kpi_by_device.fact_new_user
where date_start >= 
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
        and app_id in ('koa.global.prod', 'vegas.global.prod')
        ;


insert into kpi_by_device.fact_new_user
select     md5(u.app_id||u.user_id||u.session_id) as id
          ,u.app_id 
          ,u.app_version::varchar(32)
          ,md5(u.app_id||u.android_id) as user_key
          ,u.user_id
          ,trunc(u.ts_pretty) as date_start
          ,'2016-01-01'::DATE as date_end
          ,u.ts_pretty as ts_start
          ,'2016-01-01 00:00:00'::TIMESTAMP  as ts_end
          ,u.install_ts     
          ,trunc(u.install_ts) as install_date     
          ,u.session_id     
          ,u.facebook_id    
          ,u.install_source 
          ,u.os           
          ,u.os_version     
          ,case when u.browser is null then 'Unknown'
when u.browser='' then 'Unknown'
else u.browser end as browser         
          ,u.browser_version
          ,u.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,u.email          
          ,u.first_name     
          ,u.last_name      
          ,u.level as level_start
          ,u.level as level_end
          ,u.vip_level
          ,u.gender         
          ,u.birthday       
          ,u.ip             
          ,u.lang as language
          ,u.scene
          ,u.fb_source
          ,u.android_id as gaid
          ,u.idfa
from      kpi_processed.new_user u
left join kpi_processed.dim_country c
on        u.country_code = c.country_code
where     u.ts_pretty >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
                 and u.app_id in ('koa.global.prod', 'vegas.global.prod')
                 and u.os = 'Android'
UNION
select     md5(u.app_id||u.user_id||u.session_id) as id
          ,u.app_id 
          ,u.app_version::varchar(32)
          ,md5(u.app_id||u.idfv) as user_key
          ,u.user_id
          ,trunc(u.ts_pretty) as date_start
          ,'2016-01-01'::DATE as date_end
          ,u.ts_pretty as ts_start
          ,'2016-01-01 00:00:00'::TIMESTAMP as ts_end
          ,u.install_ts     
          ,trunc(u.install_ts) as install_date     
          ,u.session_id     
          ,u.facebook_id    
          ,u.install_source 
          ,u.os           
          ,u.os_version     
          ,case when u.browser is null then 'Unknown'
when u.browser='' then 'Unknown'
else u.browser end as browser         
          ,u.browser_version
          ,u.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,u.email          
          ,u.first_name     
          ,u.last_name      
          ,u.level as level_start
          ,u.level as level_end
          ,u.vip_level
          ,u.gender         
          ,u.birthday       
          ,u.ip             
          ,u.lang as language
          ,u.scene
          ,u.fb_source
          ,u.idfv as gaid
          ,u.idfa
from      kpi_processed.new_user u
left join kpi_processed.dim_country c
on        u.country_code = c.country_code
where     u.ts_pretty >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
                 and u.app_id in ('koa.global.prod', 'vegas.global.prod')
                 and u.os in ('iPhone OS', 'iOS')
                 ;

------------------------------------------------
--Data 2.0 tmp_user_daily_login.sql
------------------------------------------------

delete from  kpi_by_device.tmp_user_daily_login
where  date  >=  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
and app_id in ('koa.global.prod', 'vegas.global.prod')
;

insert into kpi_by_device.tmp_user_daily_login
SELECT DISTINCT
            s.date_start AS date
           ,s.app_id
           ,s.user_key
           ,s.gaid
           ,last_value(s.ts_start ignore nulls)
               OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_login_ts
           ,last_value(s.facebook_id ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
           ,last_value(s.birthday ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
            ,last_value(s.first_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
            ,last_value(s.last_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
           ,substring(last_value(s.email ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_ts ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following)  
               WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts
                     then u.install_ts  
               ELSE
                   min(s.install_ts ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_ts
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_date ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
               min(s.install_date ignore nulls)
                       OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                       ROWS BETWEEN unbounded preceding AND unbounded following) 
                WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date
                     then u.install_date    
               ELSE
                   min(s.install_date ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_date
           ,last_value(s.app_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS app_version
           ,first_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS level_start
           ,nvl(last_value(s.level_end ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following),
                last_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following))
           AS level_end
           ,last_value(s.os ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os
           ,last_value(s.os_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os_version
           ,last_value(s.country ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS country
           ,last_value(s.ip ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ip
           ,last_value(s.install_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS install_source
           ,last_value(s.language ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS language
           ,last_value(s.gender ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
           ,last_value(s.device ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)::varchar(64)
           AS device
           ,last_value(s.browser ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser
           ,last_value(s.browser_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser_version
           ,sum(1)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene)
            AS session_cnt
           ,sum(playtime_sec)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene )
            AS playtime_sec
            ,s.scene
            ,last_value(s.fb_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS fb_source
            ,null as install_source_group
            ,last_value(s.last_ref ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ref
           -- ,last_value(s.gameserver_id ignore nulls)
           --     OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
           --      ROWS BETWEEN unbounded preceding AND unbounded following)
           -- AS gameserver_id
FROM      kpi_by_device.fact_session s
LEFT JOIN kpi_by_device.dim_user u
ON        u.user_key=s.user_key
WHERE     s.date_start >= 
                  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
and s.app_id in ('koa.global.prod', 'vegas.global.prod')       
        ;

-- Attempt to catch missing users from fact_new_user

INSERT INTO kpi_by_device.tmp_user_daily_login
SELECT DISTINCT
     n.date_start
    ,n.app_id
    ,n.user_key
    ,n.gaid
    ,last_value(n.ts_start ignore nulls)
        OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,last_value(n.facebook_id ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
    ,last_value(n.birthday ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
     ,last_value(n.first_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
     ,last_value(n.last_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
    ,substring(last_value(n.email ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_ts ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    n.scene='Main' and min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(n.install_ts ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_date ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN   n.scene='Main' and  min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(n.install_date ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(n.app_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(n.level_start ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_start
    ,last_value(n.level_end ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(n.os ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(n.os_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(n.country ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(n.ip ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(n.install_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(n.language ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(n.gender ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
    ,last_value(n.device ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(n.browser ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(n.browser_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,n.scene
    ,last_value(n.fb_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM kpi_by_device.fact_new_user n
LEFT JOIN kpi_by_device.tmp_user_daily_login s
    ON  s.user_key=n.user_key
    AND s.date=n.date_start
LEFT JOIN kpi_by_device.dim_user u
    ON  u.user_key=s.user_key
WHERE n.date_start >= 
           (
             select start_date 
             from   kpi_processed.init_start_date
            )
           and n.app_id in ('koa.global.prod', 'vegas.global.prod')
AND s.user_key is null
;


                  
-- Attempt to catch missing users from fact_revenue

INSERT INTO kpi_by_device.tmp_user_daily_login
WITH revenue_session_cnt AS 
(
    SELECT  user_key
           ,date
           ,scene
           ,count(distinct session_id) as session_cnt
    FROM   kpi_by_device.fact_revenue
    WHERE  session_id is not null
    and app_id in ('koa.global.prod', 'vegas.global.prod')
    GROUP  BY 1,2,3
)
SELECT DISTINCT
     p.date
    ,p.app_id
    ,p.user_key
    ,p.gaid
    ,last_value(p.ts ignore nulls)
        OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,cast(null as varchar) AS facebook_id
    ,cast(null as date) AS birthday
    ,null AS  first_name
    ,null AS last_name
    ,cast(null as varchar) AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_ts ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_ts ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(p.install_ts ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_date ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_date ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(p.install_date ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(p.app_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level
    ,last_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(p.os ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(p.os_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(p.country ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(p.ip ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(p.install_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(p.language ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,null AS gender
    ,last_value(p.device ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(p.browser ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(p.browser_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,COALESCE(c.session_cnt,1) AS session_cnt
    ,0 AS playtime_sec
    ,p.scene
    ,last_value(p.fb_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM kpi_by_device.fact_revenue p
LEFT JOIN revenue_session_cnt c
    ON  p.user_key=c.user_key
    AND p.date=c.date 
    and p.scene=c.scene
LEFT JOIN kpi_by_device.tmp_user_daily_login s
    ON  s.user_key=p.user_key
    AND s.date=p.date
LEFT JOIN kpi_by_device.dim_user u
    ON  u.user_key=s.user_key
WHERE p.date >= 
           (
             select start_date 
             from   kpi_processed.init_start_date
            )
    and p.app_id in ('koa.global.prod', 'vegas.global.prod')
AND s.user_key is null
;



--update the user info where level_end < level_start
update kpi_by_device.tmp_user_daily_login
set level_start = level_end
where level_end < level_start;


--update the install_ts info
create temp table tmp_user_last_day as
select t.*
from
    (
      select *
             ,row_number() OVER (partition by user_key order by date_start desc) as row
      from kpi_by_device.fact_session ds
      where date_start>=current_date-60
          and ds.install_date is not null 
      and ds.app_id in ('koa.global.prod', 'vegas.global.prod')
     ) t
where row = 1;

update kpi_by_device.tmp_user_daily_login
set    install_date = t.install_date,
       install_ts = t.install_ts,
       device = t.device,
       country = t.country,
       language = t.language,
       browser = t.browser,
       browser_version = t.browser_version,
       os = t.os,
       os_version = t.os_version
from   tmp_user_last_day t
where  kpi_by_device.tmp_user_daily_login.install_date is null 
and    kpi_by_device.tmp_user_daily_login.user_key = t.user_key
and kpi_by_device.tmp_user_daily_login.app_id in ('koa.global.prod', 'vegas.global.prod')
;

--if we still have missing install_ts, remove
delete from kpi_by_device.tmp_user_daily_login where install_ts is NULL
and app_id in ('koa.global.prod', 'vegas.global.prod')
  ;
drop table if exists kpi_by_device.koa_install_ts_mapping;
create table kpi_by_device.koa_install_ts_mapping as
select gaid, min(ts_pretty) as install_ts
from kpi_processed.session_start
where app_id = 'koa.global.prod'
group by 1
;

update kpi_by_device.tmp_user_daily_login
set install_ts = v.install_ts
,install_date = date(v.install_ts)
from kpi_by_device.koa_install_ts_mapping v
where kpi_by_device.tmp_user_daily_login.user_id = v.gaid;

------------------------------------------------
--Data 2.0 tmp_user_payment.sql
------------------------------------------------

delete from kpi_by_device.tmp_user_payment
where date >=
          (
                    select start_date
                    from   kpi_processed.init_start_date
          )
          and app_id in ('koa.global.prod', 'vegas.global.prod')
         ;

insert into kpi_by_device.tmp_user_payment
select  DISTINCT
        app_id
        ,user_key
        ,date
        ,min(r.ts) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)                      AS conversion_ts
        ,sum(revenue_usd) over (partition by r.user_key, date,scene ORDER BY ts ASC rows between unbounded preceding and unbounded following)         AS revenue_usd
        ,sum(revenue_usd) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)               AS total_revenue_usd
        ,count(transaction_id) over (partition by r.user_key, date,scene ORDER BY ts ASC rows between unbounded preceding and unbounded following )   AS purchase_cnt
        ,count(transaction_id) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)          AS total_purchase_cnt
        ,scene
FROM kpi_by_device.fact_revenue r
where date >=
          (
                    select start_date
                    from   kpi_processed.init_start_date
          )
          and app_id in ('koa.global.prod', 'vegas.global.prod')
;

------------------------------------------------
--Data 2.0 fact_dau_snapshot.sql
------------------------------------------------
----- insert data to fact_dau_snapshot
DELETE FROM kpi_by_device.fact_dau_snapshot
WHERE date >=
           (
             select start_date
             from kpi_processed.init_start_date
           )
           and app_id in ('koa.global.prod', 'vegas.global.prod')
           ;

insert into kpi_by_device.fact_dau_snapshot
(
           id
           ,user_key
           ,date
           ,app_id
           ,app_version
           ,level_start
           ,level_end
           ,os
           ,os_version
           ,device
           ,browser
           ,browser_version
           ,country
           ,language
           ,is_new_user
           ,is_payer
           ,is_converted_today
           ,revenue_usd
           ,payment_cnt
           ,session_cnt
           ,playtime_sec
           ,scene
           --,gameserver_id
)
select     md5(s.app_id||s.date||s.user_id) as id
          ,s.user_key
          ,s.date
          ,s.app_id
          ,s.app_version
          ,s.level_start
          ,s.level_end
          ,s.os
          ,s.os_version
          ,s.device
          ,s.browser
          ,s.browser_version
          ,s.country
          ,s.language
          ,CASE
                  WHEN s.date=s.install_date THEN
                      1
                  ELSE
                      0
              END AS is_new_user
              ,CASE
                  WHEN s.date >= trunc(p.conversion_ts) THEN
                      1
                  ELSE
                      0
              END AS is_payer
              ,CASE
                  WHEN s.date = trunc(p.conversion_ts) THEN
                      1
                  ELSE
                      0
              END AS is_converted_today
          ,COALESCE(pd.revenue_usd,0) AS revenue_usd
          ,COALESCE(pd.purchase_cnt,0) AS payment_cnt
          ,s.session_cnt
          ,case
              when s.playtime_sec>1000000 then 1000000
              when s.playtime_sec<0 then 0
              else s.playtime_sec
           end as playtime_sec
           ,s.scene
           --,s.gameserver_id
FROM      kpi_by_device.tmp_user_daily_login s
LEFT JOIN kpi_by_device.tmp_user_payment pd
ON        s.user_key=pd.user_key
AND       s.date=pd.date and s.scene=pd.scene
LEFT JOIN
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
        FROM kpi_by_device.tmp_user_payment
        GROUP BY 1
    ) AS p
ON        p.user_key=s.user_key
WHERE     s.date >=
                 (
                   select start_date
                   from kpi_processed.init_start_date
                 )
                 and s.app_id in ('koa.global.prod', 'vegas.global.prod')
;

-- Attempt to backfill missing row on install date

insert into kpi_by_device.fact_dau_snapshot
(
    id
    ,user_key
    ,date
    ,app_id
    ,app_version
    ,level_start
    ,level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,is_new_user
    ,is_payer
    ,is_converted_today
    ,revenue_usd
    ,payment_cnt
    ,session_cnt
    ,playtime_sec
    ,scene
)
WITH missing_install_date AS
(
    SELECT    DISTINCT
              l.user_key
             ,l.install_date
    FROM      kpi_by_device.tmp_user_daily_login l
    LEFT JOIN kpi_by_device.fact_dau_snapshot i
    ON        l.user_key = i.user_key
    AND       l.install_date = i.date
    WHERE     l.install_date < (select start_date from kpi_processed.init_start_date)
    and     l.app_id in ('koa.global.prod', 'vegas.global.prod')
    AND       i.user_key is null
    UNION
    SELECT     DISTINCT
               l.user_key
              ,l.install_date
    FROM      kpi_by_device.tmp_user_daily_login l
    LEFT JOIN kpi_by_device.tmp_user_daily_login i
        ON    l.user_key = i.user_key
        AND   l.install_date = i.date
    WHERE     l.install_date >= (select start_date from kpi_processed.init_start_date)
    and     l.app_id in ('koa.global.prod', 'vegas.global.prod')
      AND     i.user_key is null
)
SELECT
     MD5(user_key || install_date) AS id
    ,user_key
    ,install_date as date
    ,app_id
    ,app_version
    ,1 AS level_start
    ,level_start as level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,1 AS is_new_user
    ,0 AS is_payer
    ,0 AS is_converted_today
    ,0 AS revenue_usd
    ,0 AS purchase_cnt
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,scene
FROM
    (
    SELECT
        d.*
        ,m.install_date
        ,row_number() over (partition by d.user_key order by d.date asc) as row
    FROM kpi_by_device.fact_dau_snapshot d
    JOIN missing_install_date m
        ON  d.user_key = m.user_key
    WHERE d.date > m.install_date
    and d.app_id in ('koa.global.prod', 'vegas.global.prod')
    )  t
WHERE t.row=1
;

delete from kpi_by_device.fact_dau_snapshot where date >= current_date;
-------------------------------------------------
--Data 2.0 dim_user.sql
-------------------------------------------------

DROP TABLE IF EXISTS tmp_dim_user;
CREATE TEMP TABLE tmp_dim_user AS
WITH
    last_info AS
    (
        SELECT
            *
            ,row_number() over (partition by user_key order by date desc) as row
        FROM kpi_by_device.tmp_user_daily_login
        where date >=
                 (
                   select start_date
                   from kpi_processed.init_start_date
                 )
        and app_id in ('koa.global.prod', 'vegas.global.prod')
    )
    ,install_info AS
    (
        SELECT
             distinct user_key
            ,last_value(nullif(language,'') ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
            ,last_value(install_source ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
            ,last_value(install_source_group ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source_group
            ,last_value(country ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
            ,last_value(os ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
            ,last_value(device ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
            ,last_value(browser ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
            ,last_value(gender ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS gender
        FROM kpi_by_device.tmp_user_daily_login
        where install_date <= date
        and install_date > date - 15
        and app_id in ('koa.global.prod', 'vegas.global.prod')
    )
    ,payment_info AS
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
            ,max(total_revenue_usd) as total_revenue_usd
            ,max(total_purchase_cnt) as total_purchase_cnt
        FROM kpi_by_device.tmp_user_payment
        where app_id in ('koa.global.prod', 'vegas.global.prod')
        GROUP BY 1
    )

SELECT DISTINCT
  tu.user_key AS id
  ,tu.user_key
  ,tu.app_id
  ,tu.app_version
  ,tu.user_id
  ,COALESCE(du.facebook_id, tu.facebook_id) AS facebook_id
  ,case when tu.app_id like 'royal%' and tu.scene='2' then nvl(du.install_ts, tu.install_ts) else tu.install_ts end as install_ts
  ,case when tu.app_id like 'royal%' and tu.scene='2' then nvl(du.install_date, tu.install_date) else tu.install_date end as install_date
  ,ti.install_source_group
  ,coalesce(nullif(split_part(ti.install_source, '::', 1),''), 'Organic') AS install_source
  ,nullif(split_part(ti.install_source, '::', 3),'') AS install_subpublisher
  ,nullif(split_part(ti.install_source, '::', 2),'') AS install_campaign
  ,coalesce(ti.language,du.install_language) AS install_language
  ,coalesce(ti.country, du.install_country) AS install_country
  ,coalesce(ti.os, du.install_os) AS install_os
  ,coalesce(ti.device, du.install_device) AS install_device
  ,coalesce(ti.browser, du.install_browser) AS install_browser
  ,coalesce(ti.gender, du.install_gender) AS install_gender
  ,tu.language
  ,tu.birthday
  ,tu.first_name
  ,tu.last_name
  ,tu.gender
  ,tu.country
  ,COALESCE (tu.email, du.email) AS email
  ,tu.os
  ,tu.os_version
  ,tu.device
  ,tu.browser
  ,tu.browser_version
  ,COALESCE (tu.last_ip, du.last_ip) AS last_ip
  ,COALESCE(tu.level_end, tu.level_start,du.level) AS level
  ,CASE WHEN COALESCE(tp.total_revenue_usd, 0) > 0 THEN 1 ELSE 0 END AS is_payer
  ,tp.conversion_ts
  ,COALESCE(tp.total_revenue_usd,0) AS revenue_usd
  ,COALESCE(tp.total_purchase_cnt,0) AS payment_cnt
  ,date(tu.last_login_ts) as last_login_date
  ,nullif(split_part(ti.install_source, '::', 4),'') AS install_creative_id
  ,tu.last_ref
FROM last_info tu
LEFT JOIN install_info ti
    ON ti.user_key=tu.user_key
LEFT JOIN kpi_by_device.dim_user du
    ON tu.user_key = du.user_key
LEFT JOIN payment_info tp
    ON tp.user_key=tu.user_key
WHERE tu.row=1
;

DELETE FROM kpi_by_device.dim_user
where user_key in (
  select user_key from kpi_by_device.tmp_user_daily_login
  where date >=
                 (
                   select start_date
                   from kpi_processed.init_start_date
                 )
    )
and app_id in ('koa.global.prod', 'vegas.global.prod');

INSERT INTO kpi_by_device.dim_user
(
   id
  ,user_key
  ,app_id
  ,app_version
  ,user_id
  ,facebook_id
  ,install_ts
  ,install_date
  ,install_source
  ,install_source_group
  ,install_subpublisher
  ,install_campaign
  ,install_language
  ,install_country
  ,install_os
  ,install_device
  ,install_browser
  ,install_gender
  ,language
  ,birthday
  ,first_name
  ,last_name
  ,gender
  ,country
  ,email
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,last_ip
  ,level
  ,is_payer
  ,conversion_ts
  ,revenue_usd
  ,payment_cnt
  ,last_login_date
  ,install_creative_id
  ,last_ref
)
SELECT
   id
  ,user_key
  ,app_id
  ,app_version
  ,user_id
  ,facebook_id
  ,install_ts
  ,install_date
  ,case when install_source='appia' then 'Appia'
when install_source like 'app_page%' then 'app_page'
when install_source like 'book%ks' then 'bookmark'
when install_source like 'canvas_bookma%' then 'canvas_bookmark'
when install_source like 'easonboard2%' then 'easonboard2'
when install_source like 'easwelcome%' then 'easwelcome'
when install_source like 'search%' then 'search'
when install_source like 'shortcut%' then 'shortcut'
when install_source like 'sidebar_bo%' then 'sidebar_bookmark'
when install_source like 'timeline/' then 'timeline'
when install_source like 'ticker/' then 'ticker'
else install_source
end as install_source
  ,install_source_group
  ,install_subpublisher
  ,install_campaign
  ,install_language
  ,install_country
  ,install_os
  ,install_device
  ,install_browser
  ,install_gender
  ,language
  ,birthday
  ,first_name
  ,last_name
  ,gender
  ,country
  ,email
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,last_ip
  ,level
  ,is_payer
  ,conversion_ts
  ,revenue_usd
  ,payment_cnt
  ,last_login_date
  ,install_creative_id
  ,last_ref
FROM tmp_dim_user
;

--update fact_install_source
create temp table g as
select fp_app_id, idfv as user_id, md5(fp_app_id||idfv) as user_key, installed_at, tracker_name,
    split_part(tracker_name, '::', 1) as install_source,
    nullif(split_part(tracker_name, '::', 2),'') as install_campaign,
    nullif(split_part(tracker_name, '::', 3),'') as install_subpublisher,
    nullif(split_part(tracker_name, '::', 4),'') as install_creative_id
from
  (SELECT *, row_number() over (partition by fp_app_id, idfv order by created_at) as rnum
  FROM adjust.raw_events where lower(tracker_name)!='organic' and idfv!=''
  and md5(fp_app_id || idfv) not in (select md5(fp_app_id || user_id) from kpi_by_device.fact_install_source)
  and tracker_name!=''
  and fp_app_id in ('koa.global.prod', 'vegas.global.prod')
  )
where rnum=1;
--add gaid part when SDK fix the problem

insert into kpi_by_device.fact_install_source
select * from g;
--update dim_user table
update kpi_by_device.dim_user
set install_source = f.install_source,
    install_campaign = f.install_campaign,
    install_subpublisher =  f.install_subpublisher,
    install_creative_id = f.install_creative_id
from
  (select * from kpi_by_device.fact_install_source
    where user_key in (select user_key from kpi_by_device.tmp_user_daily_login)
  ) f
where app_id in ('koa.global.prod', 'vegas.global.prod') and
  kpi_by_device.dim_user.user_key = f.user_key
  and lower(kpi_by_device.dim_user.install_source) in ('organic','','mobile');

-- Separate Adjust Organic from Organic for KOA
update kpi_by_device.dim_user
set install_source = 'Adjust Organic'
from adjust.raw_events_unique r
where
r.event = 'install'
and r.fp_app_id in ('koa.global.prod', 'vegas.global.prod')
and lower(r.tracker_name) = 'organic'
and r.os_name = 'ios'
and kpi_by_device.dim_user.install_source = 'Organic'
and kpi_by_device.dim_user.install_os = 'iPhone OS'
and r.idfv = kpi_by_device.dim_user.user_id ;
----------------------------------------------
--Data 2.0 agg_kpi.sql
----------------------------------------------

delete from kpi_by_device.agg_kpi
where date >=(select start_date from kpi_processed.init_start_date)
and app_id in ('koa.global.prod', 'vegas.global.prod')
;

insert into kpi_by_device.agg_kpi
(
   date                       
  ,app_id                     
  ,app_version                             
  ,install_source_group       
  ,install_source             
  ,level_end                  
  ,browser                                             
  ,country                    
  ,os                                          
  ,language                   
  ,is_new_user                
  ,is_payer
--    ,gameserver_id 
  ,new_user_cnt               
  ,dau_cnt                    
  ,newpayer_cnt               
  ,payer_today_cnt            
  ,payment_cnt                
  ,revenue_usd
  ,session_cnt
          ,playtime_sec
          ,scene
          ,revenue_iap
          ,revenue_ads
)
select d.date
      ,d.app_id
      ,d.app_version
      ,case when u.app_id like 'farm%' then u.install_source_group
       else u.install_source end as install_source_group
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,d.is_new_user
      ,d.is_payer
   --   ,d.gameserver_id
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
      ,d.scene
      ,sum(coalesce(d.revenue_iap,0)) as revenue_iap
      ,sum(coalesce(d.revenue_ads,0)) as revenue_ads
from kpi_by_device.fact_dau_snapshot d
join kpi_by_device.dim_user u on d.user_key=u.user_key
where date >=(select start_date from kpi_processed.init_start_date)
and d.app_id in ('koa.global.prod', 'vegas.global.prod')
group by 1,2,3,4,5,6,7,8,9,10,11,12,21;

----------------------------------------------
--Data 2.0 agg_retention_ltv.sql
----------------------------------------------

DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (
    SELECT 1 AS day UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 32 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);

drop table if exists player_day_cube;
CREATE TEMP TABLE player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_by_device.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.is_payer,
    count(distinct d.user_key) new_user_cnt
FROM kpi_by_device.dim_user d, player_day p, last_date l, kpi_by_device.fact_dau_snapshot dau
where
    d.install_date> DATEADD(day, -363, current_date) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
    and dau.scene='Main'
and d.app_id in ('koa.global.prod', 'vegas.global.prod')
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

CREATE TEMP TABLE baseline AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_by_device.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM kpi_by_device.fact_dau_snapshot d
 JOIN kpi_by_device.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 WHERE
    u.install_date> DATEADD(day, -363, current_date) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
    and d.scene='Main'
    and d.app_id in ('koa.global.prod', 'vegas.global.prod')
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-- TODO: insert instead of create table

delete from kpi_by_device.agg_retention_ltv
where app_id in ('koa.global.prod', 'vegas.global.prod')
;

insert into kpi_by_device.agg_retention_ltv
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube pc
LEFT JOIN baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0);
-- agg_loading by device
--delete from dragonwar.agg_loading where date >= dateadd('day',-1,current_date);
--insert into dragonwar.agg_loading
--select
--c.app_id,
--trunc(c.ts_pretty) as date,
--s.os,
--s.os_version,
--c.level,
--s.device,
--s.is_payer,
--s.is_new_user,
--s.language,
--json_extract_path_text(d_c1,'value') as loading_step,
--count(distinct c.user_id) as user_count,
--count(1) as counts
--from dragonwar.catchall c
--join dragonwar.fact_dau_snapshot s
--on md5(c.app_id||c.user_id) = s.user_key
--and trunc(c.ts_pretty) = s.date
--where event='loading' and s.date >= dateadd('day',-1,current_date)
--group by 1,2,3,4,5,6,7,8,9,10;

-- agg_loading by user_id
delete from dragonwar.agg_loading_user where date >= dateadd('day',-1,current_date);
insert into dragonwar.agg_loading_user
select
c.app_id,
trunc(c.ts_pretty) as date,
s.is_payer,
s.is_new_user,
json_extract_path_text(d_c1,'value') as loading_step,
count(distinct c.user_id) as user_count,
count(1) as counts
from dragonwar.catchall c
join dragonwar.fact_dau_snapshot s
on md5(c.app_id||c.user_id) = s.user_key
and trunc(c.ts_pretty) = s.date
where event='loading' and s.date >= dateadd('day',-1,current_date)
group by 1,2,3,4,5;
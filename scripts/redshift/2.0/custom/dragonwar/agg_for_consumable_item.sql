delete from dragonwar.agg_consumable_items where date >= dateadd('day',-1,current_date);
insert into dragonwar.agg_consumable_items
select
a.app_id, 
a.date,
a.action,
a.item_name,
a.item_id,
sum(a.quantity) as quantity,
count(distinct user_id) as user_count
from
(SELECT 
app_id,
date(ts_pretty) as date, 
user_id,
json_extract_path_text(d_c1, 'value') as action, 
JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(d_c2, 'value'), seq.i) AS item_name,
JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(d_c3, 'value'), seq.i) AS item_id,
JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(m1, 'value'), seq.i) AS quantity
FROM dragonwar.catchall, seq_0_to_100 AS seq
WHERE  event='consumable_item' 
and app_id like 'koa%' 
and seq.i < JSON_ARRAY_LENGTH(json_extract_path_text(d_c3, 'value')) 
and right(d_c2,1) = '}'
and date(ts_pretty) >= dateadd('day',-1,current_date)) a
group by 1,2,3,4,5;


DELETE FROM dragonwar.tbl_user WHERE date >= CURRENT_DATE - 1;
INSERT INTO dragonwar.tbl_user
(
	date,
	uid,
	fpid,
	ctime,
	world_id,
	vip_points,
	vip_job_id,
	power,
	lord_power,
	legend_power,
	dragon_power,
	building_power,
	troops_power,
	research_power,
	trap_power,
	steel,
	honor,
	last_login,
	gold,
	normal_gold,
	hidden_gold,
	hold_gold,
	card_last_award_time,
	card_expiration_date,
	city_level,
	city_resource,
	dragon_level,
	hero_level
)
SELECT
    CURRENT_DATE - 1 as date,
	i.uid,
	i.fpid,
	i.ctime,
	i.world_id,
	i.vip_points,
	i.vip_job_id,
	i.power,
	i.lord_power,
	i.legend_power,
	i.dragon_power,
	i.building_power,
	i.troops_power,
	i.research_power,
	i.trap_power,
	i.steel,
	i.honor,
	i.last_login,
	i.gold,
	i.normal_gold,
	i.hidden_gold,
	i.hold_gold,
	card.last_award_time,
	card.expiration_date,
	city.level,
	city.resource,
	dragon.level,
	hero.level
FROM 
(SELECT * FROM dragonwar.tbl_user_info WHERE date(last_login) >= CURRENT_DATE - 1) i
LEFT JOIN dragonwar.tbl_user_card card on i.uid = card.uid
LEFT JOIN dragonwar.tbl_user_city city on i.uid = city.uid
LEFT JOIN dragonwar.tbl_user_dragon dragon on i.uid = dragon.uid
LEFT JOIN dragonwar.tbl_user_hero hero on i.uid = hero.uid;

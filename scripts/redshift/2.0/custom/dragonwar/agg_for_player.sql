delete from dragonwar.agg_player_attr where date='${SCHEDULE_DATE}';
insert into dragonwar.agg_player_attr
(
	date,
	app_id,
	user_id,
	user_key,
	install_date,
	install_days,
	playtime_sec,
	revenue_usd
)
select 
	'${SCHEDULE_DATE}'::date as date,
	u.app_id as app_id,
	u.user_id as user_id,
	u.user_key as user_key,
	u.install_date as install_date,
	datediff('day', u.install_date, '${SCHEDULE_DATE}') as install_days,
	sum(d.playtime_sec) as playtime_sec,
	sum(d.revenue_usd) as revenue_usd
from dragonwar.dim_user u, dragonwar.fact_dau_snapshot d
where d.date <= '${SCHEDULE_DATE}'
	and d.user_key = u.user_key
group by 1,2,3,4,5;


-- daily_dim_user
delete from dragonwar.daily_dim_user where date='${SCHEDULE_DATE}';
insert into dragonwar.daily_dim_user
(
	date,
	user_key,
	app_id,
	user_id,
	install_date,
	install_language,
	language,
	level,
	is_payer
)
select
	'${SCHEDULE_DATE}'::date as date,
	user_key,
	app_id,
	user_id,
	install_date,
	install_language,
	language,
	level,
	is_payer
from dragonwar.dim_user
where app_id like 'koa%';

update dragonwar.daily_dim_user
set level = d.level_end
from dragonwar.fact_dau_snapshot d
where dragonwar.daily_dim_user.date = '${SCHEDULE_DATE}'
	and dragonwar.daily_dim_user.user_key = d.user_key
	and dragonwar.daily_dim_user.level < d.level_end
	;

-- level up average time
delete from dragonwar.agg_levelup where date='${SCHEDULE_DATE}';
insert into dragonwar.agg_levelup
(
	date,
	app_id,
	user_id,
	action,
	type,
	from_level,
	to_level,
	to_ts_pretty
)
select 
	date(ts_pretty),
	app_id,
	user_id,
	json_extract_path_text(d_c1,'value') as action,
	json_extract_path_text(d_c2,'value') as type,
	json_extract_path_text(d_c3,'value')::int as from_level,
	json_extract_path_text(d_c4,'value')::int as to_level,
	ts_pretty as to_ts_pretty

from dragonwar.catchall 
where app_id like 'koa%' 
	and event='levelup'
	and date(ts_pretty)='${SCHEDULE_DATE}';

drop table if exists temp_levelup;
create temp table temp_levelup as
select 
	date,
	app_id,
	user_id,
	action,
	type,
	from_level,
	to_level,
	to_ts_pretty
from dragonwar.agg_levelup;

update dragonwar.agg_levelup
set from_ts_pretty = t.to_ts_pretty
from temp_levelup t
where dragonwar.agg_levelup.date = '${SCHEDULE_DATE}'
	and dragonwar.agg_levelup.app_id = t.app_id
	and dragonwar.agg_levelup.user_id = t.user_id
	and dragonwar.agg_levelup.from_level = t.to_level;

update dragonwar.agg_levelup
set from_ts_pretty = d.install_ts
from dragonwar.dim_user d
where dragonwar.agg_levelup.date = '${SCHEDULE_DATE}'
	and dragonwar.agg_levelup.app_id = d.app_id
	and dragonwar.agg_levelup.user_id = d.user_id
	and dragonwar.agg_levelup.from_level = 1;

update dragonwar.agg_levelup
set levelup_time_sec = datediff(second, from_ts_pretty, to_ts_pretty)
where date='${SCHEDULE_DATE}' and from_ts_pretty is not null;


-- drop table if exists player_week_detail;
-- CREATE TEMP TABLE player_week_detail AS
-- WITH last_date AS (SELECT max(date) AS date FROM dragonwar.fact_dau_snapshot)
-- SELECT
-- 	d.user_key as user_key,
--     p.week AS player_week,
--     d.app_id,
--     d.app_version,
--     date(date_trunc('week', date(d.conversion_ts))) as install_week_date,
--     d.install_source,
--     d.install_subpublisher,
--     d.install_campaign,
--     d.install_device as device_alias,
--     d.install_os as os,
--     d.install_browser as browser,
--     d.install_country as country,
--     d.install_language as language,
--     count(r.id) as pay_c,
--     sum(r.revenue_usd) as revenue_usd_s

-- FROM dragonwar.dim_user d, player_week p, last_date l, dragonwar.fact_revenue r
-- where
--     date(d.conversion_ts)> DATEADD(day,-360,current_date) and
--     DATEDIFF('week', date(d.conversion_ts), l.date) = p.week and
--     r.user_key= d.user_key and r.date=date(d.conversion_ts)
-- --and d.app_id like '_game_%'
-- group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-- drop table if exists player_week_cube;
-- CREATE TEMP TABLE player_week_cube AS
-- WITH last_date AS (SELECT max(date) AS date FROM dragonwar.fact_dau_snapshot)
-- SELECT
--     player_week,
--     app_id,
--     app_version,
--     install_week_date,
--     install_source,
--     install_subpublisher,
--     install_campaign,
--     device_alias,
--     os,
--     browser,
--     country,
--     language,
--     0 as pay_cnt_p50,
--     0.00 as revenue_usd_p50,
--     count(distinct user_key) as new_payer_cnt,
--     sum(pay_c) as pay_cnt,
--     sum(revenue_usd_s) as revenue_usd_sum

-- FROM player_week_detail
-- group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

-- drop table if exists temp_p50;
-- CREATE TEMP TABLE temp_p50 AS
-- select 	player_week, 
-- 	   	install_week_date, 
-- 		percentile_disc(0.5) 
-- 		within group (order by pay_c) 
-- 		over(partition by player_week, install_week_date) as pay_cnt_p50,
-- 		percentile_disc(0.5) 
-- 		within group (order by revenue_usd_s) 
-- 		over(partition by player_week, install_week_date) as revenue_usd_p50
-- from player_week_detail;

-- update player_week_cube
-- set pay_cnt_p50 = t.pay_cnt_p50,
-- 	revenue_usd_p50 = t.revenue_usd_p50
-- from temp_p50 t
-- where player_week_cube.player_week = t.player_week
-- 	and player_week_cube.install_week_date = t.install_week_date;

-- DROP TABLE IF EXISTS temp_baseline;
-- CREATE TEMP TABLE temp_baseline AS
-- WITH last_date AS (SELECT max(date) AS date FROM dragonwar.fact_dau_snapshot)
-- SELECT
-- 	u.user_key,
--     p.week as player_week,
--     u.app_id,
--     u.app_version,
--     date(date_trunc('week', date(u.conversion_ts))) as install_week_date,
--     u.install_date,
--     d.date,
--     u.install_source,
--     u.install_subpublisher,
--     u.install_campaign,
--     u.install_device as device_alias,
--     u.install_os AS os,
--     u.install_browser AS browser,
--     u.install_country AS country,
--     u.install_language AS language,
--     d.is_converted_today as is_converted_today,
--     SUM(d.revenue_usd) AS sum_revenue_usd
--  FROM dragonwar.fact_dau_snapshot d
--  JOIN dragonwar.dim_user u ON d.user_key=u.user_key
--  -- JOIN player_week p ON DATEDIFF('week', date(u.conversion_ts), d.date)<=p.week
--  JOIN player_week p ON DATEDIFF('week', date(u.conversion_ts), d.date)=p.week
--  join last_date l on 1=1
--  WHERE
--     u.install_date> DATEADD(day,-360,current_date) AND
--     u.is_payer = 1 AND
--     -- DATEDIFF('week', date(u.conversion_ts), l.date) >= p.week
--     DATEDIFF('week', date(u.conversion_ts), l.date) = p.week
--     --and d.app_id like '_game_%'
-- group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;


-- DROP TABLE IF EXISTS baseline;
-- CREATE TEMP TABLE baseline AS
-- SELECT
--     player_week,
--     app_id,
--     app_version,
--     install_week_date,
--     install_source,
--     install_subpublisher,
--     install_campaign,
--     device_alias,
--     os,
--     browser,
--     country,
--     language,
--     case when player_week = install_week_date then count(distinct user_key) else 0 end AS retained_user_cnt,
--     case when player_week = install_week_date and sum(sum_revenue_usd) > 0 then count(distinct user_key) else 0 end AS retained_payer_cnt,
--     SUM(sum_revenue_usd) AS cumulative_revenue_usd,
--     sum(is_converted_today) as new_payer_cnt
--  FROM temp_baseline
-- group by 1,2,3,4,5,6,7,8,9,10,11,12;

-- -- TODO: insert instead of create table

-- delete from dragonwar.agg_payer_retention_weekly
-- --where app_id like '_game_%'
-- ;

-- insert into dragonwar.agg_payer_retention_weekly
-- SELECT
--     pc.player_week,
--     pc.app_id,
--     pc.app_version,
--     pc.install_week_date,
--     pc.install_source,
--     pc.install_subpublisher,
--     pc.install_campaign,
--     pc.device_alias,
--     pc.os,
--     pc.browser,
--     pc.country,
--     pc.language,
--     COALESCE(b.retained_user_cnt,0) retained_user_cnt,
--     COALESCE(b.retained_payer_cnt,0) retained_payer_cnt,
--     COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
--     COALESCE(b.new_payer_cnt,0) as new_payer_cnt,
--     pc.pay_cnt,
--     pc.pay_cnt_p50,
--     pc.revenue_usd_p50
-- FROM player_week_cube pc
-- LEFT JOIN baseline b ON
--     pc.player_week = b.player_week AND
--     COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
--     COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
--     pc.install_week_date = b.install_week_date AND
--     COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
--     COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
--     COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
--     COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
--     COALESCE(pc.os,'') = COALESCE(b.os,'') AND
--     COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
--     COALESCE(pc.country,'') = COALESCE(b.country,'') AND
--     COALESCE(pc.language,'') = COALESCE(b.language,'')
-- ;

----------------------------------------------
--Data 2.0 retention for payers weekly
----------------------------------------------

DROP VIEW IF EXISTS player_week;
CREATE VIEW  player_week AS (
    SELECT 1 AS week UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 8  
);

DROP VIEW IF EXISTS player_big_week;
CREATE VIEW  player_big_week AS (
    SELECT 12 AS week UNION ALL
    SELECT 16 UNION ALL
    SELECT 20 UNION ALL
    SELECT 24   
);

DROP TABLE IF EXISTS temp_baseline;
CREATE TEMP TABLE temp_baseline AS
SELECT 
    DATE(date_trunc('week', d.date)) AS pay_week_date,
    d.user_key,
    d.app_id,
    d.os,
    d.language,
    u.install_source,
    DATEDIFF('hour', date(u.install_ts), date(u.conversion_ts)) as pay_delay_hour,
    SUM(d.payment_cnt) AS payment_cnt,
    SUM(d.revenue_usd) AS revenue_usd
FROM dragonwar.fact_dau_snapshot d, dragonwar.dim_user u
WHERE d.app_id LIKE 'koa%'
    AND d.user_key = u.user_key
    AND DATE(date_trunc('week', d.date)) = DATE(date_trunc('week', DATE(u.conversion_ts)))
GROUP BY 1,2,3,4,5,6,7;

DROP TABLE IF EXISTS temp_week_retained;
CREATE TEMP TABLE temp_week_retained AS
SELECT
    t.pay_week_date,
    p.week AS player_week,
    t.user_key,
    t.app_id,
    t.os,
    t.language,
    t.install_source,
    t.pay_delay_hour,
    SUM(d.payment_cnt) AS payment_cnt,
    SUM(d.revenue_usd) AS revenue_usd
FROM player_week p, temp_baseline t, dragonwar.fact_dau_snapshot d
WHERE DATEDIFF('week', t.pay_week_date, d.date)=p.week
    AND t.user_key = d.user_key
GROUP BY 1,2,3,4,5,6,7,8
;

INSERT INTO temp_week_retained
SELECT
    pay_week_date,
    0 AS player_week,
    user_key,
    app_id,
    os,
    language,
    install_source,
    pay_delay_hour,
    payment_cnt,
    revenue_usd
FROM temp_baseline
;

INSERT INTO temp_week_retained
SELECT
    t.pay_week_date,
    p.week AS player_week,
    t.user_key,
    t.app_id,
    t.os,
    t.language,
    t.install_source,
    t.pay_delay_hour,
    SUM(d.payment_cnt) AS payment_cnt,
    SUM(d.revenue_usd) AS revenue_usd
FROM player_big_week p, temp_baseline t, dragonwar.fact_dau_snapshot d
WHERE DATEDIFF('week', t.pay_week_date, d.date)<=p.week
    AND DATEDIFF('week', t.pay_week_date, d.date)>(p.week-4)
    AND t.user_key = d.user_key
GROUP BY 1,2,3,4,5,6,7,8
;

DROP TABLE IF EXISTS temp_p50;
CREATE TEMP TABLE temp_p50 AS
SELECT 
    player_week, 
    pay_week_date, 
    percentile_disc(0.5) 
    within group (ORDER BY payment_cnt) 
    over(partition by player_week, pay_week_date) AS pay_cnt_p50,
    percentile_disc(0.5) 
    within group (ORDER BY revenue_usd) 
    over(partition by player_week, pay_week_date) AS revenue_usd_p50
FROM temp_week_retained
WHERE revenue_usd > 0;

DROP TABLE IF EXISTS temp_payer_cube;
CREATE TEMP TABLE temp_payer_cube AS
SELECT
    pay_week_date,
    player_week,
    user_key,
    app_id,
    os,
    language,
    install_source,
    CASE WHEN pay_delay_hour<0 THEN 0 ELSE pay_delay_hour END AS pay_delay_hour,
    payment_cnt,
    revenue_usd,
    CASE WHEN player_week=0 THEN 1 ELSE 0 END AS new_payer_cnt,
    CASE WHEN revenue_usd>0 THEN 1 ELSE 0 END AS retained_payer_cnt
FROM temp_week_retained
;

DELETE FROM dragonwar.agg_payer_retention_weekly;
INSERT INTO dragonwar.agg_payer_retention_weekly
SELECT
    pay_week_date,
    player_week,
    DATE(dateadd(week,player_week,pay_week_date)) as retained_week_date,
    app_id,
    os,
    language,
    install_source,
    0 as pay_cnt_p50,
    0 as revenue_usd_p50,
    SUM(new_payer_cnt) AS new_payer_cnt,
    SUM(retained_payer_cnt) AS retained_payer_cnt,
    COUNT(distinct user_key) as retained_user_cnt,
    SUM(pay_delay_hour) AS pay_delay_hours,
    SUM(payment_cnt) AS payment_cnt,
    SUM(revenue_usd) AS revenue_usd
FROM temp_payer_cube
GROUP BY 1,2,3,4,5,6,7,8,9
;

UPDATE dragonwar.agg_payer_retention_weekly
SET pay_cnt_p50 = p.pay_cnt_p50, revenue_usd_p50 = p.revenue_usd_p50
FROM temp_p50 p
WHERE dragonwar.agg_payer_retention_weekly.player_week = p.player_week
    and dragonwar.agg_payer_retention_weekly.pay_week_date = p.pay_week_date
;


--  Monthly
DROP VIEW IF EXISTS player_month;
CREATE VIEW  player_month AS (
    SELECT 1 AS month UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 
);

DROP TABLE IF EXISTS temp_baseline;
CREATE TEMP TABLE temp_baseline AS
SELECT 
    DATE(date_trunc('month', d.date)) AS pay_month_date,
    d.user_key,
    d.app_id,
    d.os,
    d.language,
    u.install_source,
    DATEDIFF('hour', date(u.install_ts), date(u.conversion_ts)) as pay_delay_hour,
    SUM(d.payment_cnt) AS payment_cnt,
    SUM(d.revenue_usd) AS revenue_usd
FROM dragonwar.fact_dau_snapshot d, dragonwar.dim_user u
WHERE d.app_id LIKE 'koa%'
    AND d.user_key = u.user_key
    AND DATE(date_trunc('month', d.date)) = DATE(date_trunc('month', DATE(u.conversion_ts)))
GROUP BY 1,2,3,4,5,6,7;

DROP TABLE IF EXISTS temp_month_retained;
CREATE TEMP TABLE temp_month_retained AS
SELECT
    t.pay_month_date,
    p.month AS player_month,
    t.user_key,
    t.app_id,
    t.os,
    t.language,
    t.install_source,
    t.pay_delay_hour,
    SUM(d.payment_cnt) AS payment_cnt,
    SUM(d.revenue_usd) AS revenue_usd
FROM player_month p, temp_baseline t, dragonwar.fact_dau_snapshot d
WHERE DATEDIFF('month', t.pay_month_date, d.date)=p.month
    AND t.user_key = d.user_key
GROUP BY 1,2,3,4,5,6,7,8
;

INSERT INTO temp_month_retained
SELECT
    pay_month_date,
    0 AS player_month,
    user_key,
    app_id,
    os,
    language,
    install_source,
    pay_delay_hour,
    payment_cnt,
    revenue_usd
FROM temp_baseline
;

DROP TABLE IF EXISTS temp_p50;
CREATE TEMP TABLE temp_p50 AS
SELECT 
    player_month, 
    pay_month_date, 
    percentile_disc(0.5) 
    within group (ORDER BY payment_cnt) 
    over(partition by player_month, pay_month_date) AS pay_cnt_p50,
    percentile_disc(0.5) 
    within group (ORDER BY revenue_usd) 
    over(partition by player_month, pay_month_date) AS revenue_usd_p50
FROM temp_month_retained
WHERE revenue_usd > 0;

DROP TABLE IF EXISTS temp_payer_cube;
CREATE TEMP TABLE temp_payer_cube AS
SELECT
    pay_month_date,
    player_month,
    user_key,
    app_id,
    os,
    language,
    install_source,
    CASE WHEN pay_delay_hour<0 THEN 0 ELSE pay_delay_hour END AS pay_delay_hour,
    payment_cnt,
    revenue_usd,
    CASE WHEN player_month=0 THEN 1 ELSE 0 END AS new_payer_cnt,
    CASE WHEN revenue_usd>0 THEN 1 ELSE 0 END AS retained_payer_cnt
FROM temp_month_retained
;

DELETE FROM dragonwar.agg_payer_retention_monthly;
INSERT INTO dragonwar.agg_payer_retention_monthly
SELECT
    pay_month_date,
    player_month,
    DATE(dateadd(month,player_month,pay_month_date)) as retained_month_date,
    app_id,
    os,
    language,
    install_source,
    0 as pay_cnt_p50,
    0 as revenue_usd_p50,
    SUM(new_payer_cnt) AS new_payer_cnt,
    SUM(retained_payer_cnt) AS retained_payer_cnt,
    COUNT(distinct user_key) as retained_user_cnt,
    SUM(pay_delay_hour) AS pay_delay_hours,
    SUM(payment_cnt) AS payment_cnt,
    SUM(revenue_usd) AS revenue_usd
FROM temp_payer_cube
GROUP BY 1,2,3,4,5,6,7,8,9
;

UPDATE dragonwar.agg_payer_retention_monthly
SET pay_cnt_p50 = p.pay_cnt_p50, revenue_usd_p50 = p.revenue_usd_p50
FROM temp_p50 p
WHERE dragonwar.agg_payer_retention_monthly.player_month = p.player_month
    and dragonwar.agg_payer_retention_monthly.pay_month_date = p.pay_month_date
;




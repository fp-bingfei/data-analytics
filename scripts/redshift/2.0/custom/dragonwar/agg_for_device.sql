-- koa dau and new user by device
drop table if exists tmp_user_device_info;
create temp table tmp_user_device_info as
select md5(app_id||user_id) as user_key, os, idfa, android_id, count(*) as cnt
from kpi_processed.session_start 
where app_id like 'koa.global%'
  and date(ts_pretty) >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
group by 1,2,3,4;

delete from kpi_processed.fact_dau_snapshot_by_device
where date>=
       (
         select start_date
         from   kpi_processed.init_start_date
        );

insert into kpi_processed.fact_dau_snapshot_by_device
select 
   d.id,
   d.user_key,
   d.date,
   d.app_id,
   d.app_version,
   d.level_start,
   d.level_end,
   d.os,
   d.os_version,
   d.device,
   d.browser,
   d.browser_version,
   d.country,
   d.language,
   d.is_new_user,
   s.idfa,
   d.is_converted_today,
   d.revenue_usd
from kpi_processed.fact_dau_snapshot d, tmp_user_device_info s
where d.user_key = s.user_key 
  and d.os like '%OS%'
  and d.date>=
       (
         select start_date
         from   kpi_processed.init_start_date
        );
  
insert into kpi_processed.fact_dau_snapshot_by_device
select 
   d.id,
   d.user_key,
   d.date,
   d.app_id,
   d.app_version,
   d.level_start,
   d.level_end,
   d.os,
   d.os_version,
   d.device,
   d.browser,
   d.browser_version,
   d.country,
   d.language,
   d.is_new_user,
   s.android_id,
   d.is_converted_today,
   d.revenue_usd
from kpi_processed.fact_dau_snapshot d, tmp_user_device_info s
where d.user_key = s.user_key 
  and d.os like '%Android%'
  and d.date>=
       (
         select start_date
         from   kpi_processed.init_start_date
        );

-- update to new device
drop table if exists tmp_dragonwar_new_device;
create temp table tmp_dragonwar_new_device as
select
    id,
    date,
    device_id,
    app_id,
    row_number() over(partition by app_id, device_id order by date asc) as row_num
  from kpi_processed.fact_dau_snapshot_by_device
  where is_new_user = 1;

update kpi_processed.fact_dau_snapshot_by_device
set is_new_user = 0
from tmp_dragonwar_new_device t
where t.row_num > 1
  and t.id = kpi_processed.fact_dau_snapshot_by_device.id
  and t.app_id = kpi_processed.fact_dau_snapshot_by_device.app_id
  and t.device_id = kpi_processed.fact_dau_snapshot_by_device.device_id
  ;

-- update to dim_user_by_device
delete from kpi_processed.dim_user_by_device where app_id like 'koa%';
insert into kpi_processed.dim_user_by_device
select
distinct d.*,
f.device_id
from kpi_processed.dim_user d
join dragonwar.fact_dau_snapshot_by_device f
on f.user_key = d.user_key
where d.app_id like 'koa%';

--update  kpi_processed.dim_user_by_device
--  set device_id = d.device_id
--from kpi_processed.fact_dau_snapshot_by_device d
--where kpi_processed.dim_user_by_device.user_key = d.user_key;

-- There are duplicate device id in dim_user_by_device and fact_dau_snapshot_by_device
--delete from kpi_processed.dim_user_by_device
--where   device_id in (
--  select device_id from kpi_processed.dim_user_by_device
--  group by device_id having count(device_id) > 1)
--and   user_key not in (
--  select min(user_key) from kpi_processed.dim_user_by_device
--  group by device_id having count(device_id)>1);
--
--delete from kpi_processed.fact_dau_snapshot_by_device
--where   device_id in (
--  select device_id from kpi_processed.fact_dau_snapshot_by_device
--  group by device_id having count(device_id) > 1)
--and   user_key not in (
--  select min(user_key) from kpi_processed.fact_dau_snapshot_by_device
--  group by device_id having count(device_id)>1);


----------------------------------------------
-- agg_retention_ltv_by_device
----------------------------------------------

DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (
    SELECT 1 AS day UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);

drop table if exists player_day_cube;
CREATE TEMP TABLE player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot_by_device)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.is_payer,
    count(distinct d.device_id) new_user_cnt
FROM kpi_processed.dim_user_by_device d, player_day p, last_date l, kpi_processed.fact_dau_snapshot_by_device dau
where
    d.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
    and (d.app_id != 'loe.global.prod' or d.level > 0)
--Remove level = 0 data from Citadel Realms, which are fake users
--and d.app_id like '_game_%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

drop table if exists baseline;
CREATE TEMP TABLE baseline AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot_by_device)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM kpi_processed.fact_dau_snapshot_by_device d
 JOIN kpi_processed.dim_user_by_device u ON d.device_id=u.device_id
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 WHERE
    u.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
    and (u.app_id != 'loe.global.prod' or u.level > 0)
    --Remove level = 0 data from Citadel Realms, which are fake users
    --and d.app_id like '_game_%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-- TODO: insert instead of create table

delete from kpi_processed.agg_retention_ltv_by_device
--where app_id like '_game_%'
;

insert into kpi_processed.agg_retention_ltv_by_device
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube pc
LEFT JOIN baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0);
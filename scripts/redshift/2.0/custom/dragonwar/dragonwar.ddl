
CREATE TABLE dragonwar.fact_dau_snapshot
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	date DATE NOT NULL ENCODE delta,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_new_user INTEGER,
	is_payer INTEGER,
	is_converted_today INTEGER,
	revenue_usd NUMERIC(15, 4),
	payment_cnt INTEGER,
	session_cnt INTEGER,
	playtime_sec INTEGER,
	scene VARCHAR(32) DEFAULT 'Main'::character varying,
	revenue_iap NUMERIC(14, 4),
	revenue_ads NUMERIC(14, 4)
)
SORTKEY
(
	date,
	user_key,
	app_id,
	country
);

------- dim_user

CREATE TABLE dragonwar.dim_user
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_language VARCHAR(16) ENCODE lzo,
	install_country VARCHAR(64) ENCODE lzo,
	install_os VARCHAR(64) ENCODE lzo,
	install_device VARCHAR(128) ENCODE lzo,
	install_browser VARCHAR(64) ENCODE lzo,
	install_gender VARCHAR(20) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	birthday DATE ENCODE delta,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	last_ip VARCHAR(32) ENCODE lzo,
	level INTEGER,
	is_payer INTEGER,
	conversion_ts TIMESTAMP ENCODE delta,
	revenue_usd NUMERIC(14, 4),
	payment_cnt INTEGER,
	last_login_date DATE,
	install_creative_id VARCHAR(500) ENCODE lzo,
	install_source_group VARCHAR(1024) ENCODE lzo,
	last_ref VARCHAR(1024) ENCODE lzo,
	gaid VARCHAR(64) ENCODE lzo,
	idfa VARCHAR(64) ENCODE lzo
)
SORTKEY
(
	user_key,
	app_id,
	user_id
);

CREATE TABLE dragonwar.agg_retention_ltv
(
	player_day INTEGER,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	install_date DATE DISTKEY,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_creative_id VARCHAR(1),
	device_alias VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_payer INTEGER,
	new_user_cnt BIGINT,
	retained_user_cnt BIGINT,
	cumulative_revenue_usd NUMERIC(38, 4),
	new_payer_cnt BIGINT
);

CREATE TABLE dragonwar.user_device
(
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	gaid VARCHAR(64) ENCODE lzo,
	idfa VARCHAR(64) ENCODE lzo
);

CREATE TABLE dragonwar.agg_consumable_items
(
	app_id VARCHAR(100) ENCODE lzo,
	date DATE ENCODE delta,
	action VARCHAR(100) ENCODE lzo,
	item_name VARCHAR(100),
	item_id VARCHAR(100) ENCODE lzo,
	quantity INTEGER ENCODE runlength,
	user_count INTEGER ENCODE runlength
)
SORTKEY
(
	app_id,
	action,
	item_name,
	date
);
CREATE TABLE dragonwar.agg_loading
(
	app_id VARCHAR(100) ENCODE lzo,
	date DATE ENCODE delta,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	level INTEGER,
	device VARCHAR(128) ENCODE lzo,
	is_payer INTEGER,
	is_new_user INTEGER,
	language VARCHAR(16) ENCODE lzo,
	loading_step VARCHAR(100) ENCODE lzo,
	step_name VARCHAR(100) ENCODE lzo,
	user_count INTEGER ENCODE runlength,
	counts INTEGER ENCODE runlength
)
SORTKEY
(
	date,
	loading_step
);



Create table dragonwar.tbl_user_info
(
	uid BIGINT distkey,
	fpid BIGINT,
	ctime TIMESTAMP,
	world_id BIGINT,
	vip_points INT,
	vip_job_id BIGINT,
	power BIGINT,
	lord_power BIGINT,
	legend_power BIGINT,
	dragon_power BIGINT,
	building_power BIGINT,
	troops_power BIGINT,
	research_power BIGINT,
	trap_power BIGINT,
	steel BIGINT,
	honor BIGINT,
	last_login TIMESTAMP,
	gold BIGINT,
	normal_gold BIGINT,
	hidden_gold BIGINT,
	hold_gold BIGINT
)
diststyle key
sortkey 
(
	uid
);

CREATE TABLE dragonwar.tbl_user_hero
(
	uid BIGINT distkey,
	level INT
)
diststyle key
sortkey 
(
	uid
);

CREATE TABLE dragonwar.tbl_user_dragon
(
	uid BIGINT distkey,
	level INT
)
diststyle key
sortkey 
(
	uid
);

CREATE TABLE dragonwar.tbl_user_card
(
	uid BIGINT distkey,
	last_award_time TIMESTAMP,
	expiration_date TIMESTAMP
)
diststyle key
sortkey 
(
	uid
);

CREATE TABLE dragonwar.tbl_user_city
(
	uid BIGINT distkey,
	level INT,
	resource VARCHAR(1000)
)
diststyle key
sortkey 
(
	uid
);

CREATE TABLE dragonwar.tbl_user
(
	date DATE ENCODE runlength,
	uid BIGINT ENCODE delta,
	fpid BIGINT ENCODE mostly32,
	ctime TIMESTAMP,
	world_id BIGINT ENCODE delta,
	vip_points INT ENCODE bytedict,
	vip_job_id BIGINT ENCODE runlength,
	power BIGINT ENCODE mostly32,
	lord_power BIGINT ENCODE bytedict,
	legend_power BIGINT ENCODE runlength,
	dragon_power BIGINT ENCODE bytedict,
	building_power BIGINT ENCODE mostly16,
	troops_power BIGINT ENCODE bytedict,
	research_power BIGINT ENCODE bytedict,
	trap_power BIGINT ENCODE bytedict,
	steel BIGINT ENCODE bytedict,
	honor BIGINT ENCODE bytedict,
	last_login TIMESTAMP,
	gold BIGINT ENCODE bytedict,
	normal_gold BIGINT ENCODE runlength,
	hidden_gold BIGINT ENCODE runlength,
	hold_gold VARCHAR(1000) ENCODE lzo,
	card_last_award_time TIMESTAMP ENCODE runlength,
	card_expiration_date TIMESTAMP ENCODE runlength,
	city_level INT ENCODE delta,
	city_resource VARCHAR(1000) ENCODE lzo,
	dragon_level INT ENCODE delta,
	hero_level INT ENCODE delta
)
interleaved sortkey 
(
	date,
	uid
);
DELETE FROM dragonwar.user_device
--WHERE date > ??
;

INSERT INTO dragonwar.user_device
SELECT
md5(app_id||user_id) as user_key
,user_id
,gaid
,idfa
FROM kpi_processed.session_start
WHERE app_id like 'dragonwar%'
--AND date > ??
;

DELETE FROM dragonwar.dim_user
;
INSERT INTO dragonwar.dim_user
SELECT distinct d.*
,ud.gaid
,ud.idfa
FROM kpi_processed.dim_user d
JOIN dragonwar.user_device ud
ON d.user_key = ud.user_key
AND d.
WHERE app_id like 'dragonwar%'
;


DELETE FROM dragonwar.fact_dau_snapshot
;
INSERT INTO dragonwar.fact_dau_snapshot
SELECT distinct f.*
,ud.gaid
,ud.idfa
FROM kpi_processed.fact_dau_snapshot f
JOIN dragonwar.user_device ud
ON ud.user_key = f.user_key
WHERE app_id like 'dragonwar%'
;


----------------------------------------------
--Data 2.0 agg_retention_ltv.sql
----------------------------------------------

DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (
    SELECT 1 AS day UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);

drop table if exists player_day_cube;
CREATE TEMP TABLE player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM dragonwar.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.is_payer,
    count(distinct case when os = 'Android' then d.gaid else d.idfa end) new_user_cnt
FROM dragonwar.dim_user d, player_day p, last_date l, dragonwar.fact_dau_snapshot dau
where
    d.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
    and dau.scene='Main'
--and d.app_id like '_game_%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

CREATE TEMP TABLE baseline AS
WITH last_date AS (SELECT max(date) AS date FROM dragonwar.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM dragonwar.fact_dau_snapshot d
 JOIN dragonwar.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 WHERE
    u.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
    and d.scene='Main'
    --and d.app_id like '_game_%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-- TODO: insert instead of create table

delete from kpi_processed.agg_retention_ltv
--where app_id like '_game_%'
;

insert into kpi_processed.agg_retention_ltv
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube pc
LEFT JOIN baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0);
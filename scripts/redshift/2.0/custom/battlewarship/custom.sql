DELETE FROM battlewarship.march
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO battlewarship.march
(
	app_id
	,user_id
	,date
	,install_date
	,country
	,lang
	,os
	,level
	,app_version
	,target_type
	,target_level
	,action
)
SELECT
	app_id
	,user_id
	,coalesce(trunc(ts_pretty), '2016-01-01') as date
	,coalesce(trunc(install_ts), '2016-01-01') as install_date
	,country_code as country
	,coalesce(lang, 'EN') as lang
	,coalesce(os, 'Android') as os
	,level
	,coalesce(app_version, '0.0.0') as app_version
	,json_extract_path_text(d_c1, 'value') as target_type
	,json_extract_path_text(d_c2, 'value') as target_level
	,json_extract_path_text(d_c3, 'value') as action
FROM battlewarship.catchall
WHERE event = 'March' and user_id is not null and d_c1 != 'US'
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
DELETE FROM battlewarship.goldcoin
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
INSERT INTO battlewarship.goldcoin
(
	app_id
	,user_id
	,date
	,install_date
	,country
	,lang
	,os
	,level
	,app_version
	,target_type
	,target_level
	,action
)
SELECT
	app_id
	,user_id
	,coalesce(trunc(ts_pretty), '2016-01-01') as date
	,coalesce(trunc(install_ts), '2016-01-01') as install_date
	,country_code as country
	,coalesce(lang, 'EN') as lang
	,coalesce(os, 'Android') as os
	,level
	,coalesce(app_version, '0.0.0') as app_version
	,json_extract_path_text(d_c1, 'value') as target_type
	,json_extract_path_text(d_c2, 'value') as target_level
	,json_extract_path_text(d_c3, 'value') as action
FROM battlewarship.catchall
WHERE event = 'Goldcoin' and user_id is not null and d_c1 != 'US'
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM battlewarship.user_behavior
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
INSERT INTO battlewarship.user_behavior
(
	date
	,install_date
--  ,country
--	,lang
--	,os
	,level
--	,app_version
	,dau_cnt
	,new_user_cnt
	,target_type
	,target_level
	,user_cnt
	,cnt
)
SELECT 
	f.date
	,m.install_date
--  ,f.country
--	,f.lang
--	,f.os
	,m.level
--	,f.app_version
	,f.dau_cnt
	,f.new_user_cnt
	,m.target_type
	,m.target_level
	,m.user_cnt
	,m.cnt
FROM 
	(SELECT
		date
--		,install_date
--		,country
--		,lang
--		,os
--		,level
--		,app_version
		,sum(dau_cnt) as dau_cnt
		,sum(new_user_cnt) as new_user_cnt
	FROM battlewarship.agg_kpi
	-- WHERE date >=
	--     (
	--         SELECT start_date
	--         FROM   kpi_processed.init_start_date
	--     )
	GROUP BY 1--,2,3,4,5,6,7
	)f
JOIN 
	(SELECT
		date
		,install_date
--		,country
--		,lang
--		,os
		,level
--		,app_version
		,target_type
		,target_level
		,count(distinct user_id) as user_cnt
		,count(1) as cnt
	FROM battlewarship.march
		-- WHERE date >=
	--     (
	--         SELECT start_date
	--         FROM   kpi_processed.init_start_date
	--     )
	GROUP BY 1,2,3,4,5--,6,7,8,9
	)m
ON f.date = m.date
--AND f.install_date = m.install_date
--AND f.country = m.country
--AND f.lang = m.lang
--AND f.os = m.os
--AND f.level = m.level
--AND f.app_version = m.app_version
DELETE FROM valiantforce.tutorial
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO valiantforce.tutorial
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,action
    ,step_name
    ,tutorial_step
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-08-01') as date
    ,coalesce(trunc(install_ts), '2016-08-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,coalesce(level, 0) as level
    ,coalesce(app_version, 'CB') as app_version
    ,json_extract_path_text(d_c1, 'action') as action
    ,json_extract_path_text(d_c2, 'step_name') as step_name
    ,json_extract_path_text(d_c3, 'tutorial_step') as tutorial_step
FROM valiantforce.catchall
WHERE app_id = 'valiantforce.global.prod' and event = 'tutorial' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;


DELETE FROM valiantforce.tutorial_funnel
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO valiantforce.tutorial_funnel
(
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,action
    ,step_name
    ,tutorial_step
    ,cnt
)
SELECT
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,action
    ,step_name
    ,tutorial_step
    ,count(distinct user_id) as cnt
FROM valiantforce.tutorial
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
GROUP BY 1,2,3,4,5,6,7,8,9,10,11
;
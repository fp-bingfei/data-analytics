--/* agg yearly kpi */
delete from kpi_by_device.agg_kpi_yearly
where datediff(year, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_by_device.agg_kpi_yearly
select 
    date,
    app_id,
    os,
    country, 
    sum(is_new_user) as new_installs, 
    count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
    sum(is_new_payer) as new_payers,
    count(1) as yau,
    sum(revenue_usd) as revenue,
    install_source_group,
    install_source
from
    (select 
        a.*, 
        u.app_id, 
        u.os, 
        u.country,
        case when u.app_id like 'farm%' then u.install_source_group
             else u.install_source end as install_source_group,
        u.install_source
    from
        (select 
            date_trunc('year', date) as date, 
            user_key,
            max(is_new_user) as is_new_user,
            max(is_converted_today) as is_new_payer,
            sum(revenue_usd) as revenue_usd
        from kpi_by_device.fact_dau_snapshot 
        where datediff(year, date, sysdate)<=2
        --and app_id like '_game_%'
        group by 1,2) a
    join kpi_by_device.dim_user u
    on a.user_key = u.user_key
    )
group by date,app_id,os,country,install_source_group,install_source;


---/* agg monthly kpi */
delete from kpi_by_device.agg_kpi_monthly
where datediff(month, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_by_device.agg_kpi_monthly
select 
    date,
    app_id,
    os,
    country, 
    sum(is_new_user) as new_installs, 
    count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
    sum(is_new_payer) as new_payers,
    count(1) as mau,
    sum(revenue_usd) as revenue,
    install_source_group,
    install_source
from
    (select 
        a.*, 
        u.app_id, 
        u.os, 
        u.country,
        case when u.app_id like 'farm%' then u.install_source_group
             else u.install_source end as install_source_group,
        u.install_source
    from
        (select 
            date_trunc('month', date) as date, 
            user_key,
            max(is_new_user) as is_new_user,
            max(is_converted_today) as is_new_payer,
            sum(revenue_usd) as revenue_usd
        from kpi_by_device.fact_dau_snapshot 
        where datediff(month, date, sysdate)<=2
        --and app_id like '_game_%'
        group by 1,2) a
    join kpi_by_device.dim_user u
    on a.user_key = u.user_key
    )
group by date,app_id,os,country,install_source_group,install_source;

------ agg weekly kpi -------------
delete from kpi_by_device.agg_kpi_weekly
where date>=(select max(date) from kpi_by_device.agg_kpi_weekly)
and date<=(select dateadd('day',7,max(date) ) from kpi_by_device.agg_kpi_weekly)
--and app_id like '_game_%'
;

insert into kpi_by_device.agg_kpi_weekly
select date, app_id, os, country, 
sum(is_new_user) as new_installs, 
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as wau,
sum(revenue_usd) as revenue,
install_source_group,
install_source
from
    (select a.*, u.app_id, u.os, u.country,
        case when u.app_id like 'farm%' then u.install_source_group
           else u.install_source end as install_source_group,
        u.install_source
    from
    (select 
        date_trunc('week', date) as date,
        user_key,
        max(is_new_user) as is_new_user,
        max(is_converted_today) as is_new_payer,
        sum(revenue_usd) as revenue_usd
    from kpi_by_device.fact_dau_snapshot 
    where 
      date>=(select dateadd('day',7,max(date) ) from kpi_by_device.agg_kpi_weekly)
     and date<=(select dateadd('day',14,max(date) ) from kpi_by_device.agg_kpi_weekly)
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_by_device.dim_user u
    on a.user_key = u.user_key)
group by date,app_id,os,country,install_source_group,install_source;
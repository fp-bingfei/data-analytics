create schema custom;
create table custom.agg_retention_ltv_custom (
player_day             integer,
app_id                 varchar(64),
app_version            varchar(32),
install_date           date,
install_source         varchar(1024),
install_subpublisher   varchar(1024),
install_campaign       varchar(512),
install_creative_id    varchar(1),
device_alias           varchar(128),
os                     varchar(64),
browser                varchar(64),
country                varchar(64),
language               varchar(16),
is_payer               integer,
new_user_cnt           bigint,
retained_user_cnt      bigint,
cumulative_revenue_usd numeric(38,4),
new_payer_cnt          bigint
)
;
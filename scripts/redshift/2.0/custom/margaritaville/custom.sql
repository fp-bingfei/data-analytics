--------- fact_mission
delete from mv.fact_mission where date>=current_date-3;
insert into mv.fact_mission
( id,
	date,
    ts_pretty,
    user_id,
    session_id,
    app_id,
    ip,
    os,
    os_version,
    gaid,
    mission_start_ts,
    mission_id,
    mission_status,
    mission_type,
    mission_name,
    app_version,
    language,
    android_id,
    install_source,
    facebook_id,
    mission_island,
    mission_stars,
    user_cnt,
    average_possible_moves,
    total_plays,
    level_streak,
    win_ratio
)
select   MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')||json_extract_path_text(properties, 'mission_start_ts')) as id,
	date(ts_pretty) as date,
	ts_pretty,
	user_id,
	session_id,
	app_id,
	json_extract_path_text(properties, 'ip')::VARCHAR(64) as ip,
	json_extract_path_text(properties, 'os')::VARCHAR(32) as os,
	json_extract_path_text(properties, 'os_version')::VARCHAR(32) as os_version,
	json_extract_path_text(properties, 'gaid')::VARCHAR(100) as gaid,
	(TIMESTAMP 'epoch' + json_extract_path_text(properties, 'mission_start_ts')::int * INTERVAL '1 Second ') as mission_start_ts,
	json_extract_path_text(properties, 'mission_id')::VARCHAR(32) as mission_id,
	json_extract_path_text(properties, 'mission_status')::int as mission_status,
	json_extract_path_text(properties, 'mission_type')::VARCHAR(32) as mission_type,
	json_extract_path_text(properties, 'mission_name')::VARCHAR(64) as mission_name,
	json_extract_path_text(properties, 'app_version')::VARCHAR(32) as app_version,
	json_extract_path_text(properties, 'lang')::VARCHAR(32) as language,
	json_extract_path_text(properties, 'android_id')::VARCHAR(100) as android_id,
	json_extract_path_text(properties, 'install_source')::VARCHAR(200) as install_source,
	json_extract_path_text(properties, 'facebook_id')::VARCHAR(100) as facebook_id,
	json_extract_path_text(properties, 'mission_island') as mission_island,
  json_extract_path_text(properties, 'mission_stars') as mission_stars,
  count(user_id) as user_cnt,
  nvl(nullif(json_extract_path_text(properties,'average_possible_moves'), '')::numeric(14,4), 0) as average_possible_moves,
   nvl(nullif(json_extract_path_text(properties,'total_plays'), '')::numeric(14,4), 0) as total_plays,
      nvl(nullif(json_extract_path_text(properties,'level_streak'), '')::numeric(14,4), 0) as level_streak,
        json_extract_path_text(properties,'win_ratio') as win_ratio
    
    
from mv.events
where event = 'mission'
  and app_id like 'mv%prod' 
 and ts_pretty >= current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,24,25,26,27;
delete from mv.fact_mission where date>=current_date;

------ mission objectives

delete from mv.fact_mission_objectives where ts_pretty>=current_date-3;
INSERT INTO mv.fact_mission_objectives 
SELECT 
app_id,
user_id,
ts_pretty,
session_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_id') as objective_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_name') as objective_name,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_type') as objective_type,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_amount') as objective_amount,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_amount_remaining') as objective_amount_remaining,
MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')||json_extract_path_text(properties, 'mission_start_ts')) as id
from mv.events, 
seq_0_to_10 AS seq
WHERE event='mission' and app_id like 'mv%' and ts_pretty>=current_date-3 
and json_extract_path_text(properties,'c2') like '%mission_objectives%' and 
seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'))
;



-------------- parameters
delete from mv.fact_mission_parameters where ts_pretty>=current_date-3;
INSERT INTO mv.fact_mission_parameters
SELECT 
app_id,
user_id,
ts_pretty,
session_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c3'),'mission_parameters'),seq.i),'name') as parameter_name,
cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c3'),'mission_parameters'),seq.i),'value') as int) as parameter_value,
MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')||json_extract_path_text(properties, 'mission_start_ts')) as id
from mv.events, 
seq_0_to_10 AS seq
WHERE event='mission' and app_id like 'mv%' and ts_pretty>=current_date-3
and json_extract_path_text(properties,'c3') like '%mission_parameter%' and 
seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (json_extract_path_text(properties,'c3'),'mission_parameters'));


---------statistics
delete from mv.fact_mission_statistics where ts_pretty>=current_date-3;
INSERT INTO mv.fact_mission_statistics
SELECT 
app_id,
user_id,
ts_pretty,
session_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c4'),'mission_statistics'),seq.i),'name') as statistic_name,
cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c4'),'mission_statistics'),seq.i),'value') as int) as statistic_value,
MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')||json_extract_path_text(properties, 'mission_start_ts')) as id
from mv.events, 
seq_0_to_10 AS seq
WHERE event='mission' and app_id like 'mv%' and ts_pretty>=current_date-3 
and json_extract_path_text(properties,'c4') like '%mission_statistic%' and 
seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (json_extract_path_text(properties,'c4'),'mission_statistics'));


----objectives split

delete from mv.mission_objectives_split where date>=current_date-3
;
insert into mv.mission_objectives_split
select 
 MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')||json_extract_path_text(properties, 'mission_start_ts')) as id,
  data_version,
  app_id,
  trunc(ts_pretty) as date,
  user_id,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),0),'objective_name') as objective_name1,
   json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),1),'objective_name') as objective_name2,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),2),'objective_name') as objective_name3,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),3),'objective_name') as objective_name4,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),4),'objective_name') as objective_name5,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),5),'objective_name') as objective_name6,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),6),'objective_name') as objective_name7,
  nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),0),'objective_amount_remaining'),'')::int,0) as objective_remain1,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),1),'objective_amount_remaining'),'')::int,0) as objective_remain2,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),2),'objective_amount_remaining'),'')::int,0) as objective_remain3,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),3),'objective_amount_remaining'),'')::int,0) as objective_remain4,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),4),'objective_amount_remaining'),'')::int,0) as objective_remain5,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),5),'objective_amount_remaining'),'')::int,0) as objective_remain6,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),6),'objective_amount_remaining'),'')::int,0) as objective_remain7,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),0),'objective_amount'),'')::int,0) as objective_amount1,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),1),'objective_amount'),'')::int,0) as objective_amount2,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),2),'objective_amount'),'')::int,0) as objective_amount3,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),3),'objective_amount'),'')::int,0) as objective_amount4,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),4),'objective_amount'),'')::int,0) as objective_amount5,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),5),'objective_amount'),'')::int,0) as objective_amount6,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),6),'objective_amount'),'')::int,0) as objective_amount7
 from mv.events where app_id like '%mv%' and event='mission' and ts_pretty>=current_date-3;




----------fact_level_up


delete from mv.fact_level_up 
where ts_pretty >= current_date-3;
insert into mv.fact_level_up
(
    session_id,
    app_id,
    data_version,
    user_id,
    gaid,
    from_level,
    level,
    app_version,
    os,
    os_version,
    lang,
    android_id,
    facebook_id,
    ts_pretty
)
select 
    session_id,
    app_id,
    data_version,
    user_id,
    json_extract_path_text(properties, 'mission_status')::VARCHAR(128) as gaid,
    json_extract_path_text(properties, 'from_level')::smallint as from_level,
    json_extract_path_text(properties, 'level')::smallint as level,
    json_extract_path_text(properties, 'app_version')::VARCHAR(32) as app_version,
    json_extract_path_text(properties, 'os')::VARCHAR(32) as os,
    json_extract_path_text(properties, 'os_version')::VARCHAR(32) as os_version,
    json_extract_path_text(properties, 'lang')::VARCHAR(16) as lang,
    json_extract_path_text(properties, 'android_id')::VARCHAR(64) as android_id,
    json_extract_path_text(properties, 'facebook_id')::VARCHAR(64) as facebook_id,
    ts_pretty
from mv.events where event='level_up' and   ts_pretty>=current_date-3;



-- -- update dim_user and fact_dau with level

create temp table level_update as

select app_id,
user_id,
ts_pretty,
mission_id::int as level,
'mission' as event from mv.fact_mission
;

drop table if exists dim_update;
 create temp table dim_update as 
 select * from
  	(select 
  		user_id, 
  		level, 
  		app_id, 
 		row_number() over(partition by user_id, app_id order by ts_pretty desc) as row_num  
  	from level_update
  	) t 
 where t.row_num = 1 ; 

update mv.dim_user 
 set level=t.level
 from dim_update t
 where mv.dim_user.user_id = t.user_id
  	and mv.dim_user.app_id = t.app_id and mv.dim_user.app_id like '%mv%'
 	and mv.dim_user.level<>t.level;

drop table if exists dau_update;
 create temp table dau_update as 
 select * from
  	(select 
  		user_id, 
  		trunc(ts_pretty) as date,
  		level, 
  		app_id, 
 		row_number() over(partition by user_id, app_id,trunc(ts_pretty) order by ts_pretty desc) as row_num  
  	from level_update
  	) t 
 where t.row_num = 1 ; 

update mv.fact_dau_snapshot 
 set level_end=t.level
 from dau_update t
 where mv.fact_dau_snapshot.user_key = md5(t.app_id||t.user_id) and mv.fact_dau_snapshot.date = t.date
  	and mv.fact_dau_snapshot.app_id = t.app_id and mv.fact_dau_snapshot.app_id like '%mv%'
 	and mv.fact_dau_snapshot.level_end<>t.level;
 	

drop table if exists mv.install_app_version;
create table mv.install_app_version as
select distinct d.user_key,d.app_version as install_app_version from mv.fact_dau_snapshot d
join mv.dim_user u on d.user_key=u.user_key and d.date=u.install_date
where d.app_id like 'mv%';

-----------level churn
drop table if exists attempts;
create temp table attempts as
select m.mission_status,m.mission_id::int as level,m.app_id,m.user_id,count(1) as attempts from mv.fact_mission m
join
(select * from (select app_id,user_id,mission_status,mission_id::int as mission_id,
row_number() over (partition by app_id,user_id order by ts_pretty desc) as rank from mv.fact_mission)m where rank=1)t
on m.app_id=t.app_id and m.user_id=t.user_id and m.mission_id::int=t.mission_id::int and t.mission_status=m.mission_status
group by 1,2,3,4;




update attempts set attempts=1 where mission_status<>-10;







drop table if exists cum_user_temp;
create table cum_user_temp as
with this_date as (select max(date) as date from mv.fact_dau_snapshot)
select install_app_version,
u.app_version,
u.install_date,
u.install_source,
u.country,
u.is_payer,
u.browser,
u.language,
m.app_id,
u.install_os as os,
m.level,
ab_experiment1,
ab_experiment2,
ab_experiment3,
ab_experiment4,
ab_variable1,
ab_variable2,
ab_variable3,
ab_variable4,
mission_status,
attempts,
case when  datediff(day,u.last_login_date,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when datediff(day,u.last_login_date,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when  datediff(day,u.last_login_date,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when datediff(day,u.last_login_date,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when  datediff(day,u.last_login_date,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when  datediff(day,u.last_login_date,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,case when  datediff(day,u.last_login_date,t.date)>=60 then 1 else 0 end as is_churned_60days
,count(distinct m.user_id) as users from level_update m
left join mv.install_app_version a on md5(m.app_id||m.user_id)=a.user_key
join mv.dim_user u on md5(m.app_id||m.user_id)=u.user_key
left join attempts s on m.app_id=s.app_id and m.user_id=s.user_id and m.level=s.level 
join this_date t on 1=1
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on u.user_key = c.user_key
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28
;


drop table if exists level_churn_temp;
create table  level_churn_temp as
with this_date as (select max(date) as date from mv.fact_dau_snapshot)
select
  u.app_id
  ,u.app_version
  ,a.install_app_version
  ,u.level 
  ,u.install_date
  ,u.install_source
  ,c.ab_experiment1
  ,c.ab_variable1
  ,c.ab_experiment2
  ,c.ab_variable2
  ,c.ab_experiment3
  ,c.ab_variable3
  ,c.ab_experiment4
  ,c.ab_variable4
  ,u.country
  ,u.install_os as os
  ,u.browser
  ,u.language
  ,u.is_payer
  ,mission_status
  ,attempts
  ,case when  datediff(day,u.last_login_date,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when datediff(day,u.last_login_date,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when  datediff(day,u.last_login_date,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when datediff(day,u.last_login_date,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when  datediff(day,u.last_login_date,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when  datediff(day,u.last_login_date,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,case when  datediff(day,u.last_login_date,t.date)>=60 then 1 else 0 end as is_churned_60days
  ,count(distinct u.user_key) as user_cnt
from mv.dim_user u
left join mv.install_app_version a on a.user_key=u.user_key
left join attempts s on u.app_id=s.app_id and u.user_id=s.user_id and u.level=s.level 
join this_date t on 1=1
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on u.user_key = c.user_key
where u.app_id like 'mv%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28;


drop table if exists mv.level_churn;
create table mv.level_churn as
select t.app_id
  ,t.app_version
  ,t.install_app_version
  ,t.level as current_level
  ,t.install_date
  ,t.install_source
  ,t.ab_experiment1
  ,t.ab_variable1
  ,t.ab_experiment2
  ,t.ab_variable2
  ,t.ab_experiment3
  ,t.ab_variable3
  ,t.ab_experiment4
  ,t.ab_variable4
  ,t.country
  ,t.os as os
  ,t.browser
  ,t.language
  ,t.is_payer
  ,t.mission_status
  ,t.attempts
  ,t.is_churned_1days
  ,t.is_churned_3days
  ,t.is_churned_7days
  ,t.is_churned_14days
  , t.is_churned_21days
  , t.is_churned_30days
  , t.is_churned_60days
  ,t.users as cum_users
  ,m.user_cnt as churned_cnt
  
from cum_user_temp t 
left join level_churn_temp m
on coalesce(t.app_id,'Unknown')=coalesce(m.app_id,'Unknown')
 and coalesce(t.app_version,'Unknown')=coalesce(m.app_version,'Unknown')
  and coalesce(t.install_app_version,'Unknown')=coalesce(m.install_app_version,'Unknown')
  and coalesce(t.level,0) =coalesce(m.level,0)
  and coalesce(t.install_date,null)=coalesce(m.install_date,null)
  and coalesce(t.install_source,'Unknown')=coalesce(m.install_source,'Unknown')
  and coalesce(t.ab_experiment1,'Unknown')=coalesce(m.ab_experiment1,'Unknown')
  and coalesce(t.ab_variable1,'Unknown')=coalesce(m.ab_variable1,'Unknown')
  and coalesce(t.ab_experiment2,'Unknown')=coalesce(m.ab_experiment2,'Unknown')
 and coalesce(t.ab_variable2,'Unknown')=coalesce(m.ab_variable2,'Unknown')
  and coalesce(t.ab_experiment3,'Unknown')=coalesce(m.ab_experiment3,'Unknown')
  and coalesce(t.ab_variable3,'Unknown')=coalesce(m.ab_variable3,'Unknown')
  and coalesce(t.ab_experiment4,'Unknown')=coalesce(m.ab_experiment4,'Unknown')
  and coalesce(t.ab_variable4,'Unknown')=coalesce(m.ab_variable4,'Unknown')
  and coalesce(t.country,'Unknown')=coalesce(m.country,'Unknown')
  and coalesce(t.os,'Unknown')=coalesce(m.os,'Unknown')
  and coalesce(t.browser,'Unknown')=coalesce(m.browser,'Unknown')
  and coalesce(t.language,'Unknown')=coalesce(m.language,'Unknown')
  and coalesce(t.mission_status,0)=coalesce(m.mission_status,0)
  and coalesce(t.is_payer,0)=coalesce(m.is_payer,0)
  and coalesce(t.attempts,0)=coalesce(m.attempts,0)
  and coalesce(t.is_churned_1days,0)=coalesce(m.is_churned_1days,0)
  and coalesce(t.is_churned_3days,0)=coalesce(m.is_churned_3days,0)
  and coalesce(t.is_churned_7days,0)=coalesce(m.is_churned_7days,0)
  and coalesce(t.is_churned_14days,0)=coalesce(m.is_churned_14days,0)
  and  coalesce(t.is_churned_21days,0)=coalesce(m.is_churned_21days,0)
  and coalesce(t.is_churned_30days,0)=coalesce(m.is_churned_30days,0)
  and  coalesce(t.is_churned_60days,0)=coalesce(m.is_churned_60days,0);



drop table if exists mv_mission_star;
create temp table mv_mission_star as
select a.*,coalesce(s.statistic_name,'Unknown') as statistic_name,cast(s.statistic_value as int) as statistic_value,cast(t.statistic_value as int) as no_possible_moves from mv.fact_mission a
     left join (select distinct id,app_id,user_id,statistic_name,statistic_value from mv.fact_mission_statistics where statistic_name='star') s on a.id=s.id 
          left join (select distinct id,app_id,user_id,statistic_name,statistic_value from mv.fact_mission_statistics where statistic_name='no_possible_moves') t on a.id=t.id 
     where a.app_id like '%mv%';
 

drop table if exists mv.fact_mission_completion;
create table mv.fact_mission_completion as
SELECT
	s.mission_id
	,s.mission_island
	,s.date
	,s.app_id
	,d.app_version
	,a.install_app_version
	,u.os
	,c.ab_experiment1
    ,c.ab_variable1
    ,c.ab_experiment2
    ,c.ab_variable2
    ,c.ab_experiment3
    ,c.ab_variable3
    ,c.ab_experiment4
    ,c.ab_variable4
    , case s.mission_status when 0 then 'started' end asmv_mission_start_status
	,case e.mission_status
	    when 0      then 'started'
	    when 10     then 'completed'
	    when -10    then 'failed'
	    when -20    then 'abandoned'
	    when -40    then 'suspended_abandoned'
             when -50    then 'Game Failed as no more moves'
	    	    when -30    then 'app_crashed'
	    else 'missing end status'
	 end as mission_status_end
	,s.statistic_name as start_name
    ,e.statistic_name as end_name
    ,coalesce(cast(s.mission_stars as varchar(10)),'N/A') as start_stars
    ,coalesce(cast(e.statistic_value as varchar(10)),'Unknown') end_stars
	,count(distinct s.id) as mission_cnt
	,count(distinct md5(s.app_id||s.user_id)) as user_cnt
	,sum(e.no_possible_moves) as no_possible_moves
	,sum(e.average_possible_moves) as average_possible_moves
FROM mv_mission_star s
left join mv.install_app_version a on a.user_key=md5(s.app_id||s.user_id)
left join mv.fact_dau_snapshot d
   on d.user_key=md5(s.app_id||s.user_id) and s.date=d.date
join mv.dim_user u
    ON u.user_key=md5(s.app_id||s.user_id)
join mv_mission_star e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_island=e.mission_island and 
        s.mission_id=e.mission_id and
        md5(e.app_id||e.user_id)=md5(s.app_id||s.user_id) and
        e.mission_status!=s.mission_status
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on md5(s.app_id||s.user_id) = c.user_key
where s.app_id like '%mv%' and 
    s.mission_status=0
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21
;

update mv.fact_mission_completion set start_stars='N/A' where ( start_stars='' or start_stars='');




drop table if exists mv_mission_attempt;
create temp table mv_mission_attempt as
SELECT
    md5(s.app_id||s.user_id) as user_key
    ,s.mission_id
    ,s.mission_island
    ,s.app_version
    ,s.date
    ,count(distinct s.mission_start_ts) as attempt_cnt
FROM mv.fact_mission s
left join mv.fact_mission e
    on  s.mission_start_ts=e.mission_start_ts and
         s.mission_island=e.mission_island and
        s.mission_id=e.mission_id and
        e.user_id=s.user_id and e.app_id=s.app_id and
        e.mission_status!=s.mission_status
where s.app_id like '%mv%' and
    s.mission_status=0 
group by 1,2,3,4,5
;

drop table if exists mv_attempt_to_pass;
create temp table mv_attempt_to_pass as
SELECT
    m.user_key
	,m.mission_id
	,m.mission_island
	,m.date
	,u.app_id
	,m.app_version
	,u.os
	,MEDIAN(attempt_cnt) over (partition by m.date,m.mission_id,m.mission_island,u.app_id,m.app_version) as attempt_median
	,PERCENTILE_CONT(0.25) within group (order by attempt_cnt) over (partition by 
m.date,m.mission_id,m.mission_island,u.app_id,m.app_version) as attempt_q1
	,PERCENTILE_CONT(0.75) within group (order by attempt_cnt) over (partition by 
m.date,m.mission_id,m.mission_island,u.app_id,m.app_version) as attempt_q3
FROM mv_mission_attempt m
join mv.dim_user u ON u.user_key=m.user_key
where u.app_id like '%mv%'

;



drop table if exists mv.mission_attempt;
create  table mv.mission_attempt as
SELECT
	p.mission_id
	,p.mission_island
	,p.date
	,p.app_id
	,p.app_version
	,a.install_app_version
	,p.os
	,c.ab_experiment1
    ,c.ab_variable1
    ,c.ab_experiment2
    ,c.ab_variable2
    ,c.ab_experiment3
    ,c.ab_variable3
    ,c.ab_experiment4
    ,c.ab_variable4
    ,max(attempt_median) as attempt_median
    ,max(attempt_q1) as attempt_q1
    ,max(attempt_q3) as attempt_q3
    ,sum(attempt_cnt) as attempt_cnt
	,count(distinct p.user_key) as user_cnt
FROM mv_attempt_to_pass p
join mv.install_app_version a on a.user_key=p.user_key
join mv_mission_attempt m on m.user_key=p.user_key and m.mission_id=p.mission_id and m.mission_island=p.mission_island
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on p.user_key = c.user_key
      where p.app_id like '%mv%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
order by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

------------order_table
DROP TABLE IF EXISTS ORDER_STATUS;
CREATE TEMP TABLE ORDER_STATUS (id VARCHAR(50), order_status VARCHAR(1000));
INSERT INTO ORDER_STATUS
SELECT id,
       CASE
           WHEN order_status_10 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9|| ', ' ||order_status_10
           WHEN order_status_9 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9
           WHEN order_status_8 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8
           WHEN order_status_7 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7
           WHEN order_status_6 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6
           WHEN order_status_5 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5
           WHEN order_status_4 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4
           WHEN order_status_3 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3
           WHEN order_status_2 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2
           WHEN order_status_1 IS NOT NULL THEN order_status_1
       END AS order_status
FROM
  (SELECT DISTINCT *
   FROM
     (SELECT id,
             nth_value(order_status,1)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_1,
             nth_value(order_status,2)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_2,
             nth_value(order_status,3)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_3,
             nth_value(order_status,4)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_4,
             nth_value(order_status,5)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_5,
             nth_value(order_status,6)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_6,
             nth_value(order_status,7)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_7,
             nth_value(order_status,8)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_8,
             nth_value(order_status,9)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_9,
             nth_value(order_status,10)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_10
      FROM
        (SELECT id,
                objective_name,
                objective_name|| ':' ||objective_amount::INTEGER- objective_amount_remaining::INTEGER|| '/' ||objective_amount AS order_status
         FROM
           (SELECT DISTINCT *
            FROM mv.fact_mission_objectives
            where ts_pretty >=current_date-3 and 
            app_id like 'mv%'))
      WHERE order_status != ''));



DELETE FROM mv.order_table WHERE date >=current_date-3
;



INSERT INTO mv.order_table
SELECT DISTINCT *
FROM
  (SELECT m.id,
          m.mission_id,
          m.app_id,
          m.app_version,
          install_app_version,
          md5(m.app_id||m.user_id) as user_key,
          m.user_id,
          m.date,
          m.ts_pretty,
          m.session_id,
          m.os,
          m.os_version,
          m.ip,
          m.language,
          m.mission_start_ts,
          m.mission_status,
          initial_moves AS initial_moves,
          points_2_stars AS points_2_stars,
          points_3_stars AS points_3_stars,
          frozen_fruits_rate AS frozen_fruits_rate,
          butterfly_number AS butterfly_number,
          moves_remaining AS moves_remaining,
          moves_used AS moves_used,
          moves_buy AS moves_buy,
          points_final AS points_final,
          points_initial AS points_initial,
          no_possible_moves AS no_possible_moves,
          o.order_status,
          stars,
          m.mission_island,
          score_final
   FROM mv.fact_mission m
   join mv.install_app_version p on md5(m.app_id||m.user_id)=p.user_key
   

   LEFT JOIN
     (SELECT id,
             SUM(CASE parameter_name WHEN 'initial_moves' THEN parameter_value::INTEGER ELSE NULL END) AS initial_moves,
             SUM(CASE parameter_name WHEN 'points_2_stars' THEN parameter_value::INTEGER ELSE NULL END) AS points_2_stars,
             SUM(CASE parameter_name WHEN 'points_3_stars' THEN parameter_value::INTEGER ELSE NULL END) AS points_3_stars,
             SUM(CASE parameter_name WHEN 'frozen_fruits_rate' THEN parameter_value::INTEGER ELSE NULL END) AS frozen_fruits_rate,
             SUM(CASE parameter_name WHEN 'butterfly_number' THEN parameter_value::INTEGER ELSE NULL END) AS butterfly_number
      FROM (select distinct * from mv.fact_mission_parameters
      WHERE date(ts_pretty) >=current_date-3 and 
      app_id like 'mv%')m
      GROUP BY id) mp
   ON mp.id=m.id
   LEFT JOIN
     (SELECT id,
             SUM(CASE statistic_name WHEN 'moves_remaining' THEN statistic_value::INTEGER ELSE NULL END) AS moves_remaining,
             SUM(CASE statistic_name WHEN 'star' THEN statistic_value::INTEGER ELSE NULL END) AS stars,
             SUM(CASE statistic_name WHEN 'moves_used' THEN statistic_value::INTEGER ELSE NULL END) AS moves_used,
             SUM(CASE statistic_name WHEN 'moves_buy' THEN statistic_value::INTEGER ELSE NULL END) AS moves_buy,
             SUM(CASE statistic_name WHEN 'points_final' THEN statistic_value::INTEGER ELSE NULL END) AS points_final,
             SUM( CASE statistic_name WHEN 'points_initial' THEN statistic_value::INTEGER ELSE NULL END) AS points_initial,
             SUM(CASE statistic_name WHEN 'no_possible_moves' THEN statistic_value::INTEGER ELSE NULL END) AS no_possible_moves,
             SUM(CASE statistic_name WHEN 'score_final' THEN statistic_value::INTEGER ELSE NULL END) AS score_final
      FROM 
      (select distinct * from mv.fact_mission_statistics
      where ts_pretty >=current_date-3 and 
      app_id like 'mv%') f
      GROUP BY id
      ) ms
   ON ms.id=m.id
   LEFT JOIN ORDER_STATUS o ON o.id=m.id
     WHERE m.date >=current_date-3
     and 
     m.app_id like 'mv%'
  );
   


-----lapsed users by lapsed days

drop table if exists mv.lapsed_users_us;
create table mv.lapsed_users_us as
SELECT COUNT(DISTINCT u.user_key) AS user_cnt,
       u.app_id,
       u.app_version,
       install_app_version,
       u.install_date,
       u.install_source,
       u.install_language,
       u.install_country,
       u.install_os,
       u.level AS level,
       u.is_payer,
       ab_experiment1,
       ab_variable1,
       ab_experiment2,
       ab_variable2,
       ab_experiment3,
       ab_variable3,
       ab_experiment4,
       ab_variable4,
       CAST(datediff (day,u.install_date,last_login_date +1) AS varchar(10)) AS lapsed_days
FROM mv.dim_user u
left join mv.install_app_version a on u.user_key=a.user_key
  LEFT JOIN ( (SELECT DISTINCT user_key,
                      CASE
                        WHEN (ab_experiment1 = '' OR ab_experiment1 IS NULL) THEN 'None'
                        ELSE ab_experiment1
                      END,
                      CASE
                        WHEN (ab_variable1 = '' OR ab_variable1 IS NULL) THEN 'None'
                        ELSE ab_variable1
                      END,
                      CASE
                        WHEN (ab_experiment2 = '' OR ab_experiment2 IS NULL) THEN 'None'
                        ELSE ab_experiment2
                      END,
                      CASE
                        WHEN (ab_variable2 = '' OR ab_variable2 IS NULL) THEN 'None'
                        ELSE ab_variable2
                      END,
                      CASE
                        WHEN (ab_experiment3 = '' OR ab_experiment3 IS NULL) THEN 'None'
                        ELSE ab_experiment3
                      END,
                      CASE
                        WHEN (ab_variable3 = '' OR ab_variable3 IS NULL) THEN 'None'
                        ELSE ab_variable3
                      END,
                      CASE
                        WHEN (ab_experiment4 = '' OR ab_experiment4 IS NULL) THEN 'None'
                        ELSE ab_experiment4
                      END,
                      CASE
                        WHEN (ab_variable4 = '' OR ab_variable4 IS NULL) THEN 'None'
                        ELSE ab_variable4
                      END
               FROM (SELECT *,
                            ROW_NUMBER() OVER (PARTITION BY user_key ORDER BY DATE ASC) AS RANK
                     FROM mv.ab_test)
               WHERE RANK = 1)) c ON u.user_key = c.user_key
WHERE u.app_id LIKE 'mv%'
AND   install_date <= CURRENT_DATE -1
GROUP BY 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
UNION ALL
SELECT COUNT(DISTINCT u.user_key) AS user_cnt,
       u.app_id,
       u.app_version,
       install_app_version,
       u.install_date,
       u.install_source,
       u.install_language,
       u.install_country,
       u.install_os,
       0 AS level,
       u.is_payer,
       ab_experiment1,
       ab_variable1,
       ab_experiment2,
       ab_variable2,
       ab_experiment3,
       ab_variable3,
       ab_experiment4,
       ab_variable4,
       '_0' AS lapsed_days
FROM mv.dim_user u
left join mv.install_app_version a on u.user_key=a.user_key
  LEFT JOIN ( (SELECT DISTINCT user_key,
                      CASE
                        WHEN (ab_experiment1 = '' OR ab_experiment1 IS NULL) THEN 'None'
                        ELSE ab_experiment1
                      END,
                      CASE
                        WHEN (ab_variable1 = '' OR ab_variable1 IS NULL) THEN 'None'
                        ELSE ab_variable1
                      END,
                      CASE
                        WHEN (ab_experiment2 = '' OR ab_experiment2 IS NULL) THEN 'None'
                        ELSE ab_experiment2
                      END,
                      CASE
                        WHEN (ab_variable2 = '' OR ab_variable2 IS NULL) THEN 'None'
                        ELSE ab_variable2
                      END,
                      CASE
                        WHEN (ab_experiment3 = '' OR ab_experiment3 IS NULL) THEN 'None'
                        ELSE ab_experiment3
                      END,
                      CASE
                        WHEN (ab_variable3 = '' OR ab_variable3 IS NULL) THEN 'None'
                        ELSE ab_variable3
                      END,
                      CASE
                        WHEN (ab_experiment4 = '' OR ab_experiment4 IS NULL) THEN 'None'
                        ELSE ab_experiment4
                      END,
                      CASE
                        WHEN (ab_variable4 = '' OR ab_variable4 IS NULL) THEN 'None'
                        ELSE ab_variable4
                      END
               FROM (SELECT *,
                            ROW_NUMBER() OVER (PARTITION BY user_key ORDER BY DATE ASC) AS RANK
                     FROM mv.ab_test)
               WHERE RANK = 1)) c ON u.user_key = c.user_key
WHERE u.app_id LIKE 'mv%'
AND   install_date <= CURRENT_DATE -1
GROUP BY 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20;

------------island_hop
delete from mv.island_hop where ts_pretty>=current_date-3;
insert into mv.island_hop
select e.app_id,
       e.ts_pretty,
       e.user_id,
       d.app_version,
       install_app_version,
       d.os,
       d.os_version,
       nvl(nullif(json_extract_path_text(properties,'last_level'), '')::int, 0) as last_level,
       nvl(nullif(json_extract_path_text(properties,'to_level'), '')::int, 0) as to_level,
       json_extract_path_text(properties,'last_status') as last_status,
       json_extract_path_text(properties,'last_island') as last_island,
       json_extract_path_text(properties,'to_island') as to_island,
       json_extract_path_text(properties,'last_stars') as last_stars,
       u.is_payer,
       u.install_date
       from mv.events e
       left join mv.install_app_version v on md5(e.app_id||e.user_id)=v.user_key
       left join mv.fact_dau_snapshot d on md5(e.app_id||e.user_id)=d.user_key and trunc(e.ts_pretty)=d.date
       join mv.dim_user u on md5(e.app_id||e.user_id)=u.user_key where event='island_hop' and 
       ts_pretty>=current_date-3 and 
       ts_pretty<=current_date;

delete from  mv.agg_island_hop where date>=current_date-3;


insert into mv.agg_island_hop
SELECT app_id,
       TRUNC(ts_pretty) as date,
       last_level,
       to_level,
       CASE
         WHEN last_status = -10 THEN 'failed'
         WHEN last_status = -20 THEN 'abandoned'
         WHEN last_status = 10 THEN 'completed'
         WHEN last_status = 0 THEN 'started'
         WHEN last_status = -40 THEN 'suspended_abandoned'
         WHEN last_status = -30 THEN 'app_crashed'
         ELSE 'Not Known'
       END as last_status,
       last_island,
       to_island,
       last_stars,
       is_payer,
       os,
       os_version,
       install_app_version,
       app_version,
       ab_experiment1,
       ab_experiment2,
       ab_experiment3,
       ab_experiment4,
       ab_variable1,
       ab_variable2,
       ab_variable3,
       ab_variable4,
       COUNT(1) AS island_hop,
       install_date
FROM mv.island_hop h
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on md5(h.app_id||h.user_id) = c.user_key where ts_pretty>=current_date-3 and 
      last_island<>to_island
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23; 


--- island unlock order distribution
delete from mv.agg_island_unlocked where date>=current_date-3;
insert into mv.agg_island_unlocked
select e.app_id,
       count(distinct e.user_id) as users_unlocked,
       trunc(ts_pretty) as date,
       json_extract_path_text(properties,'number') as unlock_number,
       json_extract_path_text(properties,'island') as island,
       install_app_version,
       d.app_version,
       d.os,
       u.is_payer,
       u.install_source,
       ab_experiment1,
       ab_experiment2 ,
       ab_experiment3 ,
       ab_experiment4 ,
       ab_variable1 ,
       ab_variable2 ,
       ab_variable3 ,
       ab_variable4 
       from mv.events e
       left join mv.fact_dau_snapshot d on md5(e.app_id||e.user_id)=d.user_key and trunc(e.ts_pretty)=d.date
       left join mv.dim_user u on md5(e.app_id||e.user_id)=u.user_key
       left join mv.install_app_version v on md5(e.app_id||e.user_id)=v.user_key 
       left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on md5(e.app_id||e.user_id) = c.user_key
       where event='island_unlocked' and ts_pretty>=current_date-3 and ts_pretty<=current_date
       group by 1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18;

-------------island churn

drop table if exists last_island;
 create temp table last_island as 
 select * from
  	(select 
  		user_id, 
  		mission_id::int as level, 
  		mission_island,
  		app_id, 
  		ts_pretty,
 		row_number() over(partition by user_id, app_id order by ts_pretty desc) as row_num  
  	from mv.fact_mission
  	) t 
 where t.row_num = 1 ; 


drop table if exists temp_lastlevel_island;
create temp table temp_lastlevel_island as 
with max_level as (select max(mission_id::int) as max_level, mission_island,10 as mission_status from mv.fact_mission group by 2)
select * from
  	(select 
  		user_id, 
  		mission_id::int as level, 
  		mission_island,
  		mission_status,
  		app_version,
  		os,
  		app_id, 
  		ts_pretty,
 		row_number() over(partition by user_id, app_id,mission_island order by ts_pretty desc) as row_num  
  	from mv.fact_mission
  	) t 
 where t.row_num = 1 and 
 (level,mission_island,mission_status) not in (select max_level,mission_island,mission_status from max_level); 
 
drop table if exists mv.island_churn;
create table  mv.island_churn as
with this_date as (select max(date) as date from mv.fact_dau_snapshot)
select
  u.app_id
  ,u.app_version
  ,a.install_app_version
  ,u.level as current_level
  ,d.install_date
  ,d.install_source
  ,c.ab_experiment1
  ,c.ab_variable1
  ,c.ab_experiment2
  ,c.ab_variable2
  ,c.ab_experiment3
  ,c.ab_variable3
  ,c.ab_experiment4
  ,c.ab_variable4
  ,d.country
  ,u.os
  ,d.language
  ,d.is_payer
  ,u.mission_island
  ,u.mission_status
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=30 then 1 else 0 end as is_churned_30days
  ,case when datediff(day,trunc(u.ts_pretty),t.date)>=60 then 1 else 0 end as is_churned_60days
  ,count(distinct md5(u.app_id||u.user_id)) as user_cnt
from temp_lastlevel_island u
left join mv.install_app_version a on a.user_key=md5(u.app_id||u.user_id)
join mv.dim_user d on u.user_id=d.user_id and u.app_id=d.app_id
join this_date t on 1=1
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on md5(u.app_id||u.user_id) = c.user_key
where u.app_id like 'mv%' and (u.user_id,trunc(ts_pretty), u.mission_island) not in (select user_id,trunc(ts_pretty),mission_island from last_island)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27;




---- funnel
delete from mv.funnel where date>=current_date-3;
insert into mv.funnel
select count(1) as  user_cnt,
trunc(ts_pretty) as date,
e.app_id,
json_extract_path_text(properties,'app_version') as app_version,
install_app_version,
u.language,
u.is_payer,
u.country,
u.install_source,
ab_experiment1,
ab_experiment2,
ab_experiment3,
ab_experiment4,
ab_variable1,
ab_variable2,
ab_variable3,
ab_variable4,
json_extract_path_text(properties,'funnel_event') as funnel_event,
nvl(nullif(json_extract_path_text(properties,'level'), '')::int, 0) as level
from mv.events e
join mv.dim_user u on md5(e.app_id||e.user_id)=u.user_key
join mv.install_app_version v on md5(e.app_id||e.user_id)=v.user_key
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on md5(e.app_id||e.user_id) = c.user_key
      where event='funnel' and ts_pretty>=current_date-3
group by 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19;


drop table if exists total_levels;
create temp table total_levels as
select coalesce(count(distinct mission_id),0) as total_levels,m.app_id,m.user_id,u.install_date from mv.fact_mission m
join mv.dim_user u on m.app_id=u.app_id and m.user_id=u.user_id
where mission_status=10 group by 2,3,4;





DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (

    SELECT 1 as day UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 32 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);


drop table if exists mv_player_day_cube_ab;
--retention and ltv
CREATE TEMP TABLE mv_player_day_cube_ab AS
WITH last_date AS (SELECT max(date) AS date FROM mv.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    v.install_app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    c.ab_experiment1,
    c.ab_variable1,
    c.ab_experiment2,
    c.ab_variable2,
    c.ab_experiment3,
    c.ab_variable3,
    c.ab_experiment4,
    c.ab_variable4,
    d.is_payer,
    s.total_levels,
    count(distinct d.user_key) new_user_cnt
FROM mv.dim_user d
 left join total_levels s on d.user_key=md5(s.app_id||s.user_id)
JOIN mv.fact_dau_snapshot u ON d.user_key=u.user_key
join mv.install_app_version v on d.user_key=v.user_key
 JOIN player_day p ON DATEDIFF('day', d.install_date, u.date)<=p.day
 join last_date l on 1=1
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on d.user_key = c.user_key
where
    d.install_date> DATEADD(day,-360,current_date) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    u.user_key= d.user_key and u.date=d.install_date and u.scene='Main' and d.app_id like '%mv%prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24;


drop table if exists mv_baseline_ab;
CREATE TEMP TABLE mv_baseline_ab AS
WITH last_date AS (SELECT max(date) AS date FROM mv.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    v.install_app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    c.ab_experiment1,
    c.ab_variable1,
    c.ab_experiment2,
    c.ab_variable2,
    c.ab_experiment3,
    c.ab_variable3,
    c.ab_experiment4,
    c.ab_variable4,
    u.is_payer,
    s.total_levels,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM mv.fact_dau_snapshot d
 left join total_levels s on d.user_key=md5(s.app_id||s.user_id)
 JOIN mv.dim_user u ON d.user_key=u.user_key
 join mv.install_app_version v on d.user_key=v.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on d.user_key = c.user_key
WHERE
    u.install_date> DATEADD(day,-360,current_date) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day and d.app_id like '%mv%prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;

-- TODO: insert instead of create table

delete from mv.agg_retention_ltv_ab
where app_id like '%mv%prod'
;

insert into mv.agg_retention_ltv_ab
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.ab_experiment1,
    pc.ab_variable1,
    pc.ab_experiment2,
    pc.ab_variable2,
    pc.ab_experiment3,
    pc.ab_variable3,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt,
    pc.ab_experiment4,
    pc.ab_variable4,
    pc.total_levels
FROM mv_player_day_cube_ab pc
LEFT JOIN mv_baseline_ab b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
   COALESCE(pc.install_app_version,'') =COALESCE(b.install_app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.ab_experiment1,'') = COALESCE(b.ab_experiment1,'') AND
    COALESCE(pc.ab_variable1,'') = COALESCE(b.ab_variable1,'') AND
    COALESCE(pc.ab_experiment2,'') = COALESCE(b.ab_experiment2,'') AND
    COALESCE(pc.ab_variable2,'') = COALESCE(b.ab_variable2,'') AND
    COALESCE(pc.ab_experiment3,'') = COALESCE(b.ab_experiment3,'') AND
    COALESCE(pc.ab_variable3,'') = COALESCE(b.ab_variable3,'') AND
    COALESCE(pc.ab_experiment4,'') = COALESCE(b.ab_experiment4,'') AND
    COALESCE(pc.ab_variable4,'') = COALESCE(b.ab_variable4,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0) and
    COALESCE(pc.total_levels,0) = COALESCE(b.total_levels,0)

    ;
------ levels completed by player days

drop table if exists levels_by_day;
create temp table levels_by_day as
select coalesce(count(distinct mission_id),0) as levels_completed,m.date,m.app_id,m.user_id,u.user_key from mv.fact_mission m
join mv.dim_user u on m.app_id=u.app_id and m.user_id=u.user_id
where mission_status=10 group by 2,3,4,5;


delete from mv.levels_player_day;

insert into mv.levels_player_day

select u.install_date,
d.date,
d.app_id,
datediff('day',u.install_date,d.date) as player_day,
install_app_version,
d.app_version,
d.os,
u.install_source,
coalesce(l.levels_completed,0) as levels_completed,
u.is_payer,
ab_experiment1,
ab_experiment2,
ab_experiment3,
ab_experiment4,
ab_variable1,
ab_variable2,
ab_variable3,
ab_variable4,
count(distinct d.user_key) as users
from mv.fact_dau_snapshot d
join mv.dim_user u on d.user_key=u.user_key
left join mv.install_app_version v on d.user_key=v.user_key
left join levels_by_day l on l.user_key=d.user_key and d.date=l.date
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on d.user_key = c.user_key
where DATEDIFF('day', u.install_date, d.date)>=0
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18;

------ score_final

drop table if exists mv.score_final;
create table mv.score_final as 

select  count(distinct o.id) as attempts ,o.date,e.score_final,o.mission_id,o.install_app_version,o.app_id,e.mission_status,
o.mission_island,ab_experiment1,ab_experiment2,ab_experiment3,ab_experiment4,ab_variable1,ab_variable2,ab_variable3,ab_variable4 from (select distinct * from mv.order_table) o 

join (select distinct * from mv.order_table) e on o.mission_start_ts=e.mission_start_ts and
        o.mission_island=e.mission_island and 
        o.mission_id=e.mission_id and
        md5(e.app_id||e.user_id)=md5(o.app_id||o.user_id) and
        e.mission_status!=o.mission_status
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM mv.ab_test)
            WHERE rank=1)
     ) c
      on o.user_key = c.user_key where o.mission_status=0        
group by 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

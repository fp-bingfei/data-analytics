-------------------------------
-- custom 2.0
-- for royal story measure
-- by ting.jia@funplus.com
-- date 2015-08-28
-------------------------------

delete from custom.agg_measure where date='${SCHEDULE_DATE}' and app_id like 'royal%';
insert into custom.agg_measure
select 
   event      
   ,app  
   ,MD5(app||uid) as user_key
   ,uid::varchar(100)
   ,NULL as facebook_id      
   ,ts::date as date 
   ,browser        
   ,browser_version
   ,country_code  
   ,install_source::varchar(100)
   ,install_ts::date
   ,ip
   ,json_extract_path_text(properties,'lang')::varchar(100) as d_lang           
   ,json_extract_path_text(properties,'level')::varchar(100) as d_level          
   ,os            
   ,os_version
   ,NULL as d_fb_source --d_df_source   
   ,('{\"key\":\"action\",\"value\":\"' || json_extract_path_text(properties,'action') || '\"}')::varchar(2048) as d_c1
   ,('{\"key\":\"action_detail\",\"value\":\"' || json_extract_path_text(properties,'action_detail') || '\"}')::varchar(2048) as d_c1
   ,('{\"key\":\"location\",\"value\":\"' || json_extract_path_text(properties,'location') || '\"}')::varchar(2048) as d_c1
   ,NULL as d_c4
   ,NULL as d_c5
   ,('{\"key\":\"rc_bal\",\"value\":\"' || json_extract_path_text(properties,'rc_bal') || '\"}')::varchar(2048) as m1   
   ,('{\"key\":\"rc_in\",\"value\":\"' || json_extract_path_text(properties,'rc_in') || '\"}')::varchar(2048) as m2            
   ,NULL as m3              
   ,NULL as m4              
   ,NULL as m5              
   ,count(1) as event_cnt
from public.events
where date(ts)='${SCHEDULE_DATE}' and app like 'royal%' and event in('rc_transaction','coins_transaction','item_transaction')  
group by 1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,18,19,20,23,24;
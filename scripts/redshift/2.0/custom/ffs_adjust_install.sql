select
andr_in.date
,andr_in.install_source
,andr_all.cnt as android_all
,andr_in.cnt as android_in
,100.0*andr_in.cnt/andr_all.cnt as android_pct
,ios_all.cnt as ios_all
,ios_in.cnt as ios_in
,100.0*ios_in/ios_all as ios_pct
from 
	(select
		trunc(a.ts) as date
		,case
			when tracker_name = 'Organic' then tracker_name
			else split_part(tracker_name, '::', 1)
		end as install_source
		,count(distinct c.gaid) as cnt
	from ffs.adjust a
	left join ffs.catchall c
	on a.gaid = c.gaid
		and trunc(a.ts) = trunc(c.ts_pretty)
	where a.userid is null and a.gaid is not null and c.gaid is not null group by 1,2) andr_in
join 
	(select
		trunc(a.ts) as date
		,case
			when tracker_name = 'Organic' then tracker_name
			else split_part(tracker_name, '::', 1)
		end as install_source
		,count(distinct c.idfa) as cnt
	from ffs.adjust a
	left join ffs.catchall c
	on a.idfa = c.idfa
	and trunc(a.ts) = trunc(c.ts_pretty)
	where a.userid is null and a.idfa is not null and c.idfa is not null  group by 1,2) ios_in
on andr_in.date = ios_in.date and andr_in.install_source = ios_in.install_source
join
	(select
		trunc(ts) as date
		,case
			when tracker_name = 'Organic' then tracker_name
			else split_part(tracker_name, '::', 1)
		end as install_source
		,count(distinct gaid) as cnt
	from ffs.adjust
	where userid is null and gaid is not null group by 1,2) andr_all
on andr_in.date = andr_all.date and andr_in.install_source = andr_all.install_source
join
	(select
		trunc(ts) as date
		,case
			when tracker_name = 'Organic' then tracker_name
			else split_part(tracker_name, '::', 1)
		end as install_source
		,count(distinct idfa) as cnt
	from ffs.adjust
	where userid is null and idfa is not null group by 1,2) ios_all
on andr_in.date = ios_all.date and andr_in.install_source = ios_all.install_source;
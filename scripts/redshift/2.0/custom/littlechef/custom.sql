delete from littlechef.catchall where event='transaction';
delete from littlechef.catchall where app_id like 'lc.global%' and date(ts_pretty)>=current_date-3;
delete from littlechef.raw_transaction where app_id like 'lc.global%' and date(ts_pretty)>=current_date-3;


delete from littlechef.raw_session_length where ts_pretty>=current_date-1;

insert into littlechef.raw_session_length
select * from littlechef.raw_transaction where ts_pretty>=current_date-1 and event='lc_session_length';


delete from littlechef.energy_level where app_id like 'lc%prod' and date(ts_pretty)>=current_date-1;
insert into littlechef.energy_level
select t.app_id,t.user_id,ts_pretty,
json_extract_path_text(json_extract_path_text(properties,'d_c4'),'value') as mission_id,
nvl(nullif(json_extract_path_text(properties,'energy'), '')::int, 0) as energy,
json_extract_path_text(properties,'app_version') as app_version,
json_extract_path_text(properties,'os') as os,
json_extract_path_text(properties,'os_version') as os_version,
install_source,
country,
language,
json_extract_path_text(json_extract_path_text(properties,'c1'),'value') as restaurant_id from littlechef.raw_transaction t 
left join littlechef.dim_user u on t.app_id=u.app_id and t.user_id=u.user_id where   t.app_id like 'lc%prod' and event='mission' and date(ts_pretty)>=current_date-1 and
json_extract_path_text(json_extract_path_text(properties,'d_c2'),'value')='0';

delete from littlechef.facebook_connected where date>=current_date-1;
insert into littlechef.facebook_connected
select app_id,user_id,facebook_connected,date from (select app_id,user_id,date(ts_pretty) as date, json_extract_path_text(properties,'facebook_connected') as facebook_connected,
ROW_NUMBER() OVER (PARTITION BY app_id,user_id,date(ts_pretty) ORDER BY ts_pretty desc) AS RANK from 
littlechef.raw_transaction where app_id like 'lc_patch%' and event='mission' and date(ts_pretty)>=current_date-1) t where rank=1 ;


delete from littlechef.raw_transaction where event<>'transaction';


drop table if exists catchall_temp;
create temp table catchall_temp as
select c.*,u.facebook_id from littlechef.catchall c
left join littlechef.dim_user_temp u on c.app_id=u.app_id and c.user_id=u.user_id where c.app_id like 'lc_patch%' and date(ts_pretty)>=current_date-5;

create temp table fb_id as
select count(1),facebook_id from littlechef.dim_user where facebook_id is not null and facebook_id<>'' group  by 2 having count(1)>1
;

update catchall_temp set user_id=u.user_id from littlechef.dim_user u where catchall_temp.app_id=u.app_id and catchall_temp.facebook_id=u.facebook_id
and  catchall_temp.user_id<>u.user_id and catchall_temp.app_id like 'lc_patch%' and catchall_temp.facebook_id is not null and catchall_temp.facebook_id<>'' and
catchall_temp.facebook_id not in (select facebook_id from fb_id);


delete from littlechef.catchall where app_id like 'lc_patch%prod' and date(ts_pretty)>=current_date-5;

insert into littlechef.catchall 
select data_version,
app_id,
ts,
ts_pretty,
event,
user_id,
session_id,
os,
install_ts,
lang,
level,
browser,
app_version,
ip,
country_code,
d_c1,
d_c2,
d_c3,
d_c4,
d_c5,
m1,
m2,
m3,
m4,
m5,
c1,
c2,
idfa,
gaid,
load_hour from catchall_temp where app_id like 'lc_patch%prod' and date(ts_pretty)>=current_date-5;


----orders_missed 

DELETE FROM littlechef.orders_missed WHERE DATE>= CURRENT_DATE -3;
INSERT INTO littlechef.orders_missed
SELECT c.app_id,
       c.os,
       c.lang,
       c.app_version,
       TRUNC(ts_pretty) AS DATE,
       u.country,
       u.is_payer,
       json_extract_path_text(d_c2,'value')::int AS mission_id,
       SUM(json_extract_path_text (m1,'value')::int) AS orders_missed,
       COUNT(DISTINCT c.app_id||c.user_id) AS user_cnt,
       json_extract_path_text(c1,'value') AS restaurant_id
FROM littlechef.catchall c
  JOIN littlechef.dim_user u
    ON c.app_id = u.app_id
   AND c.user_id = u.user_id
WHERE event = 'order_missed'
AND   ts_pretty >= CURRENT_DATE -3
GROUP BY 1,2,3,4,5,6,7,8,11;

-----ui_tapped
delete from littlechef.ui_tapped where date>=current_date-3;

insert into littlechef.ui_tapped
SELECT c.app_id,
       c.os,
       c.lang as language,
       c.app_version,
       TRUNC(ts_pretty) AS DATE,
       coalesce(u.install_date, trunc(c.install_ts)) as install_date,
case when json_extract_path_text(d_c2,'value')='music' and json_extract_path_text(d_c3,'value')='True' then 'Music_toggle_on'
when json_extract_path_text(d_c2,'value')='music' and json_extract_path_text(d_c3,'value')='False' then 'Music_toggle_off'
when json_extract_path_text(d_c2,'value')='sfx' and json_extract_path_text(d_c3,'value')='True' then 'sfx_toggle_on'
when json_extract_path_text(d_c2,'value')='sfx' and json_extract_path_text(d_c3,'value')='False' then 'sfx_toggle_off'
when json_extract_path_text(d_c2,'value')='notifications' and json_extract_path_text(d_c3,'value')='True' then 'push_notifications_toggle_on'
when json_extract_path_text(d_c2,'value')='notifications' and json_extract_path_text(d_c3,'value')='False' then 'push_notifications_toggle_off'
when json_extract_path_text(d_c2,'value')='facebook' and json_extract_path_text(d_c3,'value')='True' then 'fb_connect_toggle_on'
when json_extract_path_text(d_c2,'value')='facebook' and json_extract_path_text(d_c3,'value')='False' then 'fb_connect_toggle_off'
else json_extract_path_text(d_c2,'value') end as ui,
       u.country,
       u.is_payer,
      count(1) as times_tapped,
      d.os_version,
     d.device,
     c.level::int
FROM littlechef.catchall c
  join littlechef.fact_dau_snapshot d
  on md5(c.app_id||c.user_id)=d.user_key and trunc(c.ts_pretty)=d.date
  JOIN littlechef.dim_user u
    ON c.app_id = u.app_id
   AND c.user_id = u.user_id
WHERE event = 'button' and json_extract_path_text(d_c1,'value')='ui'
and coalesce(u.install_date, date(c.install_ts))<=current_date and ts_pretty>=current_date-3 and  
ts_pretty<=current_date
group by 1,2,3,4,5,6,7,8,9,11,12,13;


--- mission_completion
delete from littlechef.mission_completion where date>=current_date-3;

insert into littlechef.mission_completion
SELECT c.app_id,
       c.os,
       c.lang,
       c.app_version,
       TRUNC(ts_pretty) AS DATE,
       coalesce(u.install_date,trunc(c.install_ts)) as install_date,
       u.country,
       u.is_payer,
       json_extract_path_text(d_c4,'value')::int AS mission_id,
       json_extract_path_text(d_c3,'value') AS mission_type,
       case when json_extract_path_text(d_c2,'value')=0 then 'started' 
       when json_extract_path_text(d_c2,'value')=1 then 'completed' 
       when json_extract_path_text(d_c2,'value')<0 then 'failed' else 'Status Not Known' end as mission_status,
       count(1) as attempt_cnt,
       sum(case when json_extract_path_text(d_c2,'value')<0 then -(json_extract_path_text(d_c2,'value'))::int else 0 end) 
as orders_missed,
       COUNT(DISTINCT c.app_id||c.user_id) AS user_cnt,
       d.os_version,
       d.device,
       json_extract_path_text(c2,'value') AS restaurant_id
FROM littlechef.catchall c
 join littlechef.fact_dau_snapshot d
  on md5(c.app_id||c.user_id)=d.user_key and date(c.ts_pretty)=d.date
  JOIN littlechef.dim_user u
    ON c.app_id = u.app_id
   AND c.user_id = u.user_id
WHERE event = 'mission'
AND   ts_pretty>=current_date-3 and ts_pretty <= CURRENT_DATE  and coalesce(u.install_date,date
(c.install_ts))<=current_date
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,15,16,17;

---- level_up
delete from littlechef.fact_level_up where ts >= current_date-3;
insert into littlechef.fact_level_up
(
	ts,
    session_id,
    app_id,
    user_id,
    from_level,
    level,
    app_version,
    os,
    lang,
    install_date,
    restaurant_id
)
select 
	ts_pretty,
    session_id,
    c.app_id,
    c.user_id,
    json_extract_path_text(d_c2, 'value')::int as from_level,
    json_extract_path_text(d_c3, 'value')::int as level,
    c.app_version,
    c.os,
    c.lang,
    coalesce(u.install_date,trunc(c.install_ts)) as install_date,
    json_extract_path_text(c1, 'value') as restaurant_id
from littlechef.catchall c
join littlechef.dim_user u on c.app_id=u.app_id and
c.user_id=u.user_id where event='level_up' and  json_extract_path_text(d_c2, 'from_level') is not null and json_extract_path_text(d_c3, 'to_level') is not null
and ts_pretty>=current_date-3 and ts_pretty<=current_date and coalesce(u.install_date,date(c.install_ts))<=current_date ;


drop table if exists level_update;
create temp table level_update as

select app_id,
user_id,
ts_pretty,
json_extract_path_text(d_c4,'value')::int as level,
json_extract_path_text(c2,'value') as restaurant_id,
'mission' as event from littlechef.catchall where event='mission'
union all
select app_id,
user_id,
ts as ts_pretty,
level,
restaurant_id,
'level_up' as event from littlechef.fact_level_up
;


drop table if exists dim_update;
 create temp table dim_update as 
 select * from
  	(select 
  		user_id, 
  		level, 
  		app_id, 
 		row_number() over(partition by user_id, app_id order by ts_pretty desc) as row_num  
  	from level_update
  	) t 
 where t.row_num = 1 ; 

update littlechef.dim_user 
 set level=t.level
 from dim_update t
 where littlechef.dim_user.user_id = t.user_id
  	and littlechef.dim_user.app_id = t.app_id and littlechef.dim_user.app_id like '%lc%'
 	and littlechef.dim_user.level<>t.level;

drop table if exists dau_update;
 create temp table dau_update as 
 select * from
  	(select 
  		user_id, 
  		date(ts_pretty) as date,
  		level, 
  		app_id, 
 		row_number() over(partition by user_id, app_id,trunc(ts_pretty) order by ts_pretty desc) as row_num  
  	from level_update
  	) t 
 where t.row_num = 1 ; 

update littlechef.fact_dau_snapshot 
 set level_end=t.level
 from dau_update t
 where littlechef.fact_dau_snapshot.user_key = md5(t.app_id||t.user_id) and littlechef.fact_dau_snapshot.date = t.date
  	and littlechef.fact_dau_snapshot.app_id = t.app_id and littlechef.fact_dau_snapshot.app_id like '%lc%'
 	and littlechef.fact_dau_snapshot.level_end<>t.level;



---- level_churn
create temp table attempts as
select * from (select app_id,user_id,ts_pretty,json_extract_path_text(d_c2,'value') as mission_status,       
json_extract_path_text(d_c4,'value')::int as mission_id,
json_extract_path_text(c2,'value') as restaurant_id,
row_number() over (partition by app_id,user_id order by ts_pretty desc) as rank from littlechef.catchall where event='mission')m where rank=1;


create temp table last_restaurant as
select * from (select app_id,user_id,ts_pretty, restaurant_id,level,
row_number() over (partition by app_id,user_id order by ts_pretty desc) as rank from level_update)l where rank=1;




drop table if exists littlechef.level_churn;
create table  littlechef.level_churn as
with this_date as (select max(date) as date from littlechef.fact_dau_snapshot)
select
  u.app_id
  ,u.app_version
  ,l.level
  ,u.level as current_level
  ,u.install_date
  ,u.install_source
  ,u.country
  ,u.os
  ,u.os_version
  ,u.device
  ,u.language
  ,u.is_payer
  ,n.install_app_version
   ,case when a.mission_status=0 then 'started' 
       when a.mission_status=1 then 'completed' 
       when a.mission_status<0 then 'failed' else 'Status Not Known' end as mission_status
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,case  when l.level=u.level and datediff(day,u.last_login_date,t.date)>=60 then 1 else 0 end as is_churned_60days
  ,r.restaurant_id
  ,count(distinct u.user_key) as user_cnt
from littlechef.dim_user u
left join last_restaurant r
on u.app_id=r.app_id and u.user_id=r.user_id and u.level=r.level and last_login_date=date(r.ts_pretty)
left join attempts a
on u.app_id=a.app_id and u.user_id=a.user_id and u.level=a.mission_id::int and last_login_date=date(a.ts_pretty)
join (select distinct level from littlechef.dim_user) l on u.level>=l.level
join this_date t on 1=1
left join (select app_version as install_app_version, app_id,user_key from littlechef.fact_dau_snapshot where is_new_user=1) n
on u.user_key=n.user_key
where u.app_id like 'lc%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22;



----------- churn_attempts
delete from littlechef.churn_attempts --where trunc(ts) >= current_date-3
;
insert into littlechef.churn_attempts
SELECT c.app_id,
       c.os,
       c.lang,
       c.app_version,
       d.os_version,
       d.device,
       TRUNC(c.ts_pretty) AS DATE,
       coalesce(u.install_date,trunc(c.install_ts)) as install_date,
       u.country,
       u.is_payer,
       u.install_source,
       json_extract_path_text(d_c4,'value')::int AS mission_id,
       count(case when json_extract_path_text(d_c2,'value')=0 then 1 else 0 end) as attempt_cnt,
       COUNT(DISTINCT c.app_id||c.user_id) AS user_cnt,
       json_extract_path_text(c2,'value') AS restaurant_id
FROM littlechef.catchall c
  join littlechef.fact_dau_snapshot d
  on md5(c.app_id||c.user_id)=d.user_key and date(c.ts_pretty)=d.date
  JOIN littlechef.dim_user u
    ON c.app_id = u.app_id
   AND c.user_id = u.user_id
  join last_restaurant r on c.app_id=r.app_id and c.user_id=r.user_id and json_extract_path_text(d_c4,'value')::int=r.level and 
  coalesce(json_extract_path_text(c2,'value'),'Unknown')=coalesce(r.restaurant_id,'Unknown')
WHERE event = 'mission'
AND   ---trunc(ts_pretty)>=current_date-3 and 
c.ts_pretty <= CURRENT_DATE  and coalesce(u.install_date,date(c.install_ts))<=current_date
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,15;



delete from littlechef.transaction where ts>=current_date-3;
insert into littlechef.transaction
select t.app_id,
       t.user_id,
       session_id,
       ts_pretty as ts,
       event,
       json_extract_path_text(properties,'app_version') as app_version,
       json_extract_path_text(properties,'idfv') as idfv,
       json_extract_path_text(properties,'idfa') as idfa,
       json_extract_path_text(properties,'os') as os,
       json_extract_path_text(properties,'os_version') as os_version,
       json_extract_path_text(properties,'lang') as lang,
       json_extract_path_text(properties,'device') as device,
              json_extract_path_text(properties,'level') as level,
        json_extract_path_text(properties,'c_currency_received') as c_currency_received,
        json_extract_path_text(properties,'c_currency_spent') as c_currency_spent,
        json_extract_path_text(properties,'c_items_received') as c_items_received,
        json_extract_path_text(properties,'c_items_spent') as c_items_spent,
        json_extract_path_text(properties,'d_c1') as d_c1,
        json_extract_path_text(properties,'d_c2') as d_c2,
        json_extract_path_text(properties,'d_c3') as d_c3,
        json_extract_path_text(properties,'d_c4') as d_c4,
        u.country,
        u.is_payer,
        json_extract_path_text(properties,'m1') as m1,
        json_extract_path_text(properties,'m2') as m2,
        json_extract_path_text(properties,'c1') as c1 from littlechef.raw_transaction t
        join littlechef.dim_user u on t.user_id=u.user_id and t.app_id=u.app_id where ts_pretty>=current_date-3;

-------------currency_transaction
delete from littlechef.currency_transaction where date>=current_date-3;

insert into littlechef.currency_transaction
(
    date
    , type
    , currency_type
    , app_id
    , os
    , os_version
    , device
    , language
    , level_id
    , action
    , level
    ,is_payer
    , country
    , app_version
    , currency_amount
    , restaurant_id
)
select 
    date(ts) as date
    , 'currency_received' as type
    , json_extract_path_text(currency_received, 'd_currency_type')::VARCHAR(64) as currency_type
    , app_id
    , os
    , os_version
    , device
    , language
    , json_extract_path_text(d_c2, 'value')::varchar(10) as level_id
    , json_extract_path_text(d_c1, 'value') as action
    , level
    ,is_payer
    , country
    , app_version
    , sum(nullif(json_extract_path_text(currency_received, 'm_currency_amount')::varchar, '')::bigint) as currency_amount
    , json_extract_path_text(c1, 'value') as restaurant_id
from 
    (select 
        *
        , json_extract_array_element_text(c_currency_received, seq.i) as currency_received 
    
        from  littlechef.transaction 
         g, seq_0_to_10 as seq 
    where ts>=current_date-3 and  seq.i<json_array_length(c_currency_received)
    )
group by 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,16;


-- currency spent transaction
insert into littlechef.currency_transaction
(   
    date
    , type
    , currency_type
    , app_id
    , os
    , os_version
    , device
    , language
    , level_id
    , action
    , level
    , is_payer
    , country
    , app_version
    , currency_amount
    , restaurant_id
)
select 
    date(ts) as date
    , 'currency_spent' as type
    , json_extract_path_text(currency_spent, 'd_currency_type')::VARCHAR(64) as currency_type
    , app_id
    , os
    , os_version
    , device
    , language
    , json_extract_path_text(d_c2, 'value') as level_id
    , json_extract_path_text(d_c1, 'value') as action
    , level
    , is_payer
    , country
    , app_version
    , sum(nullif(json_extract_path_text(currency_spent, 'm_currency_amount')::varchar, '')::bigint) as currency_amount
    , json_extract_path_text(c1, 'value') as restaurant_id
from 
    (select 
        *
        , json_extract_array_element_text(c_currency_spent, seq.i) as currency_spent 
    from
         littlechef.transaction
         g, seq_0_to_10 as seq 
        where ts>=current_date-3 and seq.i<json_array_length(c_currency_spent)
        )   
group by 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,16;

-----  item and currency

delete from littlechef.currency_spent_by_item where date>=current_date-3;
insert into littlechef.currency_spent_by_item
select date(ts) as date,
'items_received' as type,
json_extract_path_text(currency_spent,'d_currency_type') as current_type,
a.app_id,
a.os,
a.os_version,
a.device,
a.language,
json_extract_path_text(d_c1,'value') as action,
a.level,
u.is_payer,
u.country,
a.app_version,
json_extract_path_text(json_extract_array_element_text(c_items_received, 0),'d_item_id') as item_id,
json_extract_path_text(json_extract_array_element_text(c_items_received, 0),'d_item_type') as item_type,
json_extract_path_text(json_extract_array_element_text(c_items_received, 0),'d_item_name') as item_name,
json_extract_path_text(json_extract_array_element_text(c_items_received, 0),'d_item_class') as item_class,
count(1) as purchase_cnt,
sum(nullif(json_extract_path_text(currency_spent, 'm_currency_amount')::varchar, '')::bigint) as currency_spent_amount,
sum(nullif(json_extract_path_text(json_extract_array_element_text(c_items_received, 0),'m_item_amount')::varchar, '')::bigint) as item_received_amount,
json_extract_path_text(c1,'value') as restaurant_id
 from 
(select 
        *
        , json_extract_array_element_text(c_currency_spent, seq.i) as currency_spent 
    
        from  littlechef.transaction 
         g, seq_0_to_10 as seq 
    where  app_id like 'lc%prod%' and seq.i<json_array_length(c_currency_spent)
    ) a
    join littlechef.dim_user u on a.user_id=u.user_id and a.app_id=u.app_id
    where ts>=current_date-3 and --json_extract_path_text(currency_received,'d_currency_type')='hard' and 
    nullif(json_extract_path_text(currency_spent, 'm_currency_amount')::varchar, '')::bigint>0
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,21;
    

--- session_length

delete from littlechef.session_length where date>=current_date-3;

insert into littlechef.session_length
select 
 data_version,
t.app_id,
date(ts_pretty) as date,
 event,
 t.user_id,
json_extract_path_text(properties,'os') as os,
json_extract_path_text(properties,'os_version') as os_version,
json_extract_path_text(properties,'device') as device,
json_extract_path_text(properties,'lang') as lang,
nvl(nullif(json_extract_path_text(properties,'level'), '')::int, 0) as level,
json_extract_path_text(properties,'browser') as browser,
json_extract_path_text(properties,'app_version') as app_version,
json_extract_path_text(properties,'idfa') as idfa,
json_extract_path_text(properties,'gaid') as gaid,
u.is_payer,
u.install_source,
u.country,
sum(nullif(json_extract_path_text(properties,'session_length_seconds')::varchar, '')::numeric(20,4)) as session_length
from littlechef.raw_session_length t
 join littlechef.dim_user u on t.app_id=u.app_id 
 and t.user_id=u.user_id
where event='lc_session_length' and ts_pretty>=current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;

---- wallet report

--drop table if exists max_level;
--create temp table max_level as 
--select distinct app_id, user_id, level, restaurant_id from (select app_id,
  --     user_id,
    --   level,
      -- json_extract_path_text(c1,'value') as restaurant_id,
      -- row_number() over (partition by app_id,user_id order by ts desc) as rank
      -- from littlechef.transaction )t where rank=1;

delete from littlechef.wallet;

insert into littlechef.wallet 
select distinct app_id,
       user_id,
       ts,
       level,
       app_version,
       os,
       os_version,
       language,
       device,
       install_source,
       country,
       is_payer,
       soft_currency_type,
       soft_wallet_amount,
       hard_currency_type,
       hard_wallet_amount,
       restaurant_id from (select t.app_id,
       t.user_id,
       ts,
       nvl(nullif(t.level,'')::int, 0) as level,
       t.app_version,
       t.os,
       t.os_version,
       t.language,
       t.device,
       install_source,
       u.country,
       u.is_payer,
       json_extract_path_text(m1,'d_currency_type') as soft_currency_type,
       nvl(nullif(json_extract_path_text(m1,'m_currency_amount'), '')::bigint, 0) as soft_wallet_amount,
       json_extract_path_text(m2,'d_currency_type') as hard_currency_type,
       nvl(nullif(json_extract_path_text(m2,'m_currency_amount'), '')::bigint, 0) as hard_wallet_amount,
       json_extract_path_text(c1,'value') as restaurant_id,
       row_number() over (partition by t.app_id,t.user_id order by ts desc) as rank
       from littlechef.transaction t
       --join max_level l on t.app_id=l.app_id and t.user_id=l.user_id and nvl(nullif(t.level,'')::int, 0)=l.level
       --and coalesce(l.restaurant_id, 'Unknown')=coalesce(json_extract_path_text(c1,'value'),'Unknown')
       join littlechef.dim_user u on t.app_id=u.app_id and t.user_id=u.user_id
       )t where rank=1;

----------iap trigger access points
delete from littlechef.iap_access_points where date>=current_date-3;
insert into littlechef.iap_access_points 
select count(1) as iap_tapped, trunc(ts_pretty) as date,json_extract_path_text(d_c1,'value') as access_point_category,
f.app_version,c.app_id,u.install_source,u.is_payer,f.device,u.language,c.os,f.os_version,c.level,
json_extract_path_text(d_c3,'value') as is_first_purchase from littlechef.catchall c
join littlechef.dim_user u on c.app_id=u.app_id and c.user_id=u.user_id
left join littlechef.fact_dau_snapshot f on md5(c.app_id||c.user_id)=f.user_key and date(c.ts_pretty)=f.date where event='iap_funnel' and
 ts_pretty>=current_date-3
group by 2,3,4,5,6,7,8,9,10,11,12,13;


------ camera funnel
drop table if exists temp1;
create temp table temp1 as
select app_id,trunc(ts_pretty) as date,os,lang,level,os_version,is_payer,app_version,device,count(1) as camera_roll_tap from ( 
select c.app_id,c.user_id,c.ts_pretty,c.os,c.lang,t.os_version,t.is_payer,c.level,c.app_version,t.device,json_extract_path_text(d_c2,'value') as funnel,
row_number() over (partition by c.app_id,c.user_id order by ts_pretty asc) as rank from littlechef.catchall c 
left join littlechef.fact_dau_snapshot t on md5(c.app_id||c.user_id)=t.user_key and t.date=date(c.ts_pretty) where event='button' and
json_extract_path_text(d_c2,'value')='camera_roll')m where rank=1
group by 1,2,3,4,5,6,7,8,9;

drop table if exists temp2;
create temp table temp2 as
select app_id,trunc(ts_pretty) as date,os,lang,level,os_version,is_payer,app_version,device,count(1) as camera_tap from ( 
select c.app_id,c.user_id,c.ts_pretty,c.os,c.lang,c.level,t.os_version,t.is_payer,c.app_version,t.device,json_extract_path_text(d_c2,'value') as funnel,
row_number() over (partition by c.app_id,c.user_id order by ts_pretty asc) as rank from littlechef.catchall c 
left join littlechef.fact_dau_snapshot t on md5(c.app_id||c.user_id)=t.user_key and t.date=date(c.ts_pretty) where event='button' and
json_extract_path_text(d_c2,'value')='camera')m where rank=1
group by 1,2,3,4,5,6,7,8,9;



drop table if exists temp3;
create temp table temp3 as
select app_id,trunc(ts_pretty) as date,os,lang,level,os_version,is_payer,app_version,device,count(1) as camera_roll_permission_true from ( 
select c.app_id,c.user_id,c.ts_pretty,c.os,c.lang,c.level,t.os_version,t.is_payer,c.app_version,t.device,json_extract_path_text(d_c2,'value') as funnel,
row_number() over (partition by c.app_id,c.user_id order by ts_pretty asc) as rank from littlechef.catchall c 
left join littlechef.fact_dau_snapshot t on md5(c.app_id||c.user_id)=t.user_key and t.date=date(c.ts_pretty) where event='button' and
json_extract_path_text(d_c2,'value')='camera_roll_permission' and json_extract_path_text(d_c3,'value')='True')m where rank=1
group by 1,2,3,4,5,6,7,8,9;

drop table if exists temp4;
create temp table temp4 as
select app_id,trunc(ts_pretty) as date,os,os_version,is_payer,lang,level,app_version,device,count(1) as custom_guests_accepted from ( 
select c.app_id,c.user_id,c.ts_pretty,c.os,c.lang,c.level,t.os_version,t.is_payer,c.app_version,t.device,json_extract_path_text(d_c2,'value') as funnel from littlechef.catchall c 
left join littlechef.fact_dau_snapshot t on md5(c.app_id||c.user_id)=t.user_key and t.date=date(c.ts_pretty) where event='button' and
json_extract_path_text(d_c1,'value')='custom_guests' and json_extract_path_text(d_c2,'key')='accepted' and json_extract_path_text(d_c2,'value')='True'
 and json_extract_path_text(d_c3,'key')='first' and json_extract_path_text(d_c3,'value')='True')m 
group by 1,2,3,4,5,6,7,8,9;

drop table if exists temp5;
create temp table temp5 as
select app_id,trunc(ts_pretty) as date,os,lang,level,os_version,is_payer,app_version,device,count(1) as camera_permission_true from ( 
select c.app_id,c.user_id,c.ts_pretty,c.os,c.lang,c.level,t.os_version,t.is_payer,c.app_version,t.device,json_extract_path_text(d_c2,'value') as funnel,
row_number() over (partition by c.app_id,c.user_id order by ts_pretty asc) as rank from littlechef.catchall c 
left join littlechef.fact_dau_snapshot t on md5(c.app_id||c.user_id)=t.user_key and t.date=date(c.ts_pretty) where event='button' and
json_extract_path_text(d_c2,'value')='camera_permission' and json_extract_path_text(d_c3,'value')='True')m where rank=1
group by 1,2,3,4,5,6,7,8,9;

drop table if exists littlechef.camera_funnel;
create table littlechef.camera_funnel as 
select t1.app_id,t1.date,t1.os,t1.lang,t1.level,t1.app_version,t1.device,t1.os_version,t1.is_payer,sum(t1.camera_roll_tap) as camera_roll_tap,sum(t2.camera_tap) as camera_tap,sum(t3.camera_roll_permission_true) as camera_roll_permission_true,
sum(t4.custom_guests_accepted) as custom_guests_accepted,sum(t5.camera_permission_true) as camera_permission_true
from temp1 t1
left join temp2 t2 on t1.app_id=t2.app_id and t1.date=t2.date and t1.os=t2.os and t1.lang=t2.lang and t1.level=t2.level and t1.app_version=t2.app_version and t1.device=t2.device and t1.os_version=t2.os_version and t1.is_payer=t2.is_payer
left join temp3 t3 on t1.app_id=t3.app_id and t1.date=t3.date and t1.os=t3.os and t1.lang=t3.lang and t1.level=t3.level and t1.app_version=t3.app_version and t1.device=t3.device and t1.os_version=t3.os_version and t1.is_payer=t3.is_payer
left join temp4 t4 on t1.app_id=t4.app_id and t1.date=t4.date and t1.os=t4.os and t1.lang=t4.lang and t1.level=t4.level and t1.app_version=t4.app_version and t1.device=t4.device and t1.os_version=t4.os_version and t1.is_payer=t4.is_payer
left join temp5 t5 on t1.app_id=t5.app_id and t1.date=t5.date and t1.os=t5.os and t1.lang=t5.lang and t1.level=t5.level and t1.app_version=t5.app_version and t1.device=t5.device and t1.os_version=t5.os_version and t1.is_payer=t5.is_payer
group by 1,2,3,4,5,6,7,8,9;


---- payers report
drop table if exists fb_connected;
create temp table fb_connected as
select * from (select app_id,
       user_id,
       facebook_connected,
       row_number() over (partition by app_id,user_id order by date desc) as rank
       from littlechef.facebook_connected) t where rank=1;

drop table if exists littlechef.payers;
create table littlechef.payers as
select p.app_id,
       p.user_id,
       u.level,
       u.app_version,
       u.os,
       u.os_version,
       u.language,
       u.device,
       u.install_source,
       u.country,
       install_date,
       facebook_connected,
       p.revenue_usd,
       p.total_transactions,
       soft_wallet_amount,
       hard_wallet_amount from  littlechef.dim_user u 
       join (select app_id,user_id,sum(revenue_usd) as revenue_usd,count(1) as total_transactions from littlechef.fact_revenue group by 1,2) p 
       on p.app_id=u.app_id and p.user_id=u.user_id
       left join fb_connected b on u.app_id=b.app_id and u.user_id=b.user_id
       left join (select app_id,
       user_id,

       nvl(nullif(json_extract_path_text(m1,'m_currency_amount'), '')::bigint, 0) as soft_wallet_amount,

       nvl(nullif(json_extract_path_text(m2,'m_currency_amount'), '')::bigint, 0) as hard_wallet_amount,
       row_number() over (partition by app_id,user_id order by ts desc) as rank
       from littlechef.transaction
       )d on u.app_id=d.app_id and u.user_id=d.user_id and d.rank=1;



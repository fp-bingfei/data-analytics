drop table if exists fruitscoot.fact_mission; 
CREATE TABLE fruitscoot.fact_mission
(
  date  date ENCODE delta,
   ts_pretty                  timestamp  ENCODE delta, 
   user_id varchar(100) ENCODE  LZO,
session_id varchar(100) ENCODE  LZO,
app_id varchar(100) ENCODE  LZO,
ip varchar(32) ENCODE  LZO,
os varchar(32) ENCODE  LZO,
os_version varchar(32) ENCODE  LZO,
gaid varchar(100) ENCODE  LZO,
level integer,
mission_start_ts timestamp  ENCODE  delta,
mission_id varchar(32) ENCODE  LZO,
mission_status  integer,
mission_type varchar(32) ENCODE  LZO,
mission_name  varchar(64) ENCODE  LZO,
app_version  varchar(32) ENCODE  LZO,
language varchar(32) ENCODE  LZO,
android_id  varchar(100) ENCODE  LZO,
install_ts_pretty timestamp  ENCODE delta, 
install_source  varchar(200) ENCODE  LZO,
facebook_id  varchar(100) ENCODE  LZO,
user_cnt integer 
)
 
SORTKEY
(
	app_id,
	ts,
	level
);


--CREATE TABLE fruitscoot.fact_item_transaction
--(
--   id                  varchar(128) encode LZO,
--   ts_pretty           timestamp encode delta,
--   ts                  varchar(32) encode LZO,
--   app_id              varchar(64) encode LZO,
--   user_id             varchar(64) encode LZO,
--   session_id          varchar(128) encode LZO,
--   event               varchar(64) encode LZO,
--   level               integer,
--   item_id             varchar(64) encode LZO,
--   item_type           varchar(64) encode LZO,
--   item_name           varchar(64) encode LZO,
--   item_class          varchar(32) encode LZO,
--   item_purchased_qty  integer default 0,
--   item_used_qty       integer default 0,
--   transaction_type    varchar(64) encode LZO,
--   usd_paid            numeric(14,4),
--   cost_per_unit       numeric(14,4),
--   data_version        varchar(16) encode LZO,
--   app_version         varchar(64) encode LZO,
--   action              varchar(64) encode LZO,
--   currency_type       varchar(64) encode LZO
--);

CREATE TABLE finance.fact_item_transaction
(
   id                  varchar(128) encode LZO,
   ts_pretty           timestamp encode delta,
   ts                  bigint,
   app_id              varchar(64) encode LZO,
   user_id             varchar(64) encode LZO,
   session_id          varchar(128) encode LZO,
   event               varchar(64) encode LZO,
   item_id             varchar(64) encode LZO,
   item_type           varchar(64) encode LZO,
   item_name           varchar(64) encode LZO,
   item_class          varchar(32) encode LZO,
   item_purchased_qty  integer,
   item_used_qty       integer,
   transaction_type    varchar(64) encode LZO,
   usd_paid            numeric(14,4),
   cost_per_unit       numeric(14,4),
   data_version        varchar(16) encode LZO,
   app_version         varchar(64) encode LZO,
   action              varchar(64) encode LZO,
   currency_type       varchar(64) encode LZO
);


DROP TABLE IF EXISTS fruitscoot.dim_user CASCADE;

CREATE TABLE fruitscoot.dim_user
(
   id                    varchar(128)     NOT NULL encode LZO,
   user_key              varchar(128)     NOT NULL encode LZO,
   app_id                varchar(64)      NOT NULL encode LZO,
   app_version           varchar(32) encode LZO,
   user_id               varchar(128)     NOT NULL encode LZO,
   facebook_id           varchar(128) encode LZO,
   install_ts            timestamp        NOT NULL encode delta,
   install_date          date             NOT NULL encode delta,
   install_source        varchar(1024) encode LZO,
   install_subpublisher  varchar(1024) encode LZO,
   install_campaign      varchar(512) encode LZO,
   install_language      varchar(16) encode LZO,
   install_country       varchar(64) encode LZO,
   install_os            varchar(64) encode LZO,
   install_device        varchar(128) encode LZO,
   install_browser       varchar(64) encode LZO,
   install_gender        varchar(20) encode LZO,
   language              varchar(16) encode LZO,
   birthday              date encode delta,
   first_name            varchar(64) encode LZO,
   last_name             varchar(64) encode LZO,
   gender                varchar(20) encode LZO,
   country               varchar(64) encode LZO,
   email                 varchar(256) encode LZO,
   os                    varchar(64) encode LZO,
   os_version            varchar(64) encode LZO,
   device                varchar(128) encode LZO,
   browser               varchar(64) encode LZO,
   browser_version       varchar(128) encode LZO,
   last_ip               varchar(32) encode LZO,
   level                 integer,
   is_payer              integer,
   conversion_ts         timestamp encode delta,
   revenue_usd           numeric(14,4),
   payment_cnt           integer,
   last_login_date       date encode delta,
   install_source_group  varchar(1024) encode LZO,
   install_creative_id   varchar(500) encode LZO
);

COMMIT;


CREATE TABLE fruitscoot.fact_lapsed_users
(
   app_id           varchar(64) encode LZO,
   facebook_id      varchar(128) encode LZO,
   user_id varchar(128) encode LZO,
   install_date     date encode delta,
   install_source   varchar(1024) encode LZO,
   browser          varchar(64) encode LZO,
   language         varchar(16) encode LZO,
   country          varchar(64) encode LZO,
   os               varchar(30) encode LZO,
   level            smallint,
   is_payer         smallint,
   last_login_date  date encode delta,
   lapsed_days      smallint
);



CREATE TABLE fruitscoot.fact_level_up
(
ts  varchar(32) encode LZO,
session_id      varchar(128) encode LZO,
app_id           varchar(64) encode LZO,
data_version      varchar(16) encode LZO,
user_id       varchar(64) encode LZO,
gaid       varchar(128) encode LZO,
from_level smallint,
level smallint,
app_version  varchar(32) encode LZO,
os  varchar(32) encode LZO,
os_version varchar(32) encode LZO, 
lang varchar(16) encode LZO, 
android_id varchar(64) encode LZO, 
install_ts bigint,
facebook_id varchar (64) encode LZO
);
 

create table fruitscoot.agg_level_norm
(
  date date encode delta,
  level integer,
  d3_lapsed_cnt integer default 0,
  d3_first_play_cnt integer default 0,
  d7_lapsed_cnt integer default 0,
  d7_first_play_cnt integer default 0,
  d7_failed_cnt integer default 0,
  sucess_cnt integer default 0,
  first_play_cnt integer default 0,
  total_play_cnt integer default 0, 
  booster_cnt integer default 0,
  booster_user_cnt integer default 0
)


---


with g as
(select a.*, datediff('day', u.last_login_date, '2015-12-17') as diff
from
(SELECT date, md5(app_id||user_id) as user_key, max(level) as level 
FROM custom.fruitscoot.fact_mission 
where first_play=1
and datediff('day', date, '2015-12-17')>=3
group by 1,2) a
join
fruitscoot.dim_user u
on a.user_key = u.user_key)

select a.*, b.u3, c.u7
from
(select date, level, count(distinct user_key) as u
from g
group by 1,2) a
left join
(select date, level, count(distinct user_key) as u3
from g
where diff>=3
group by 1,2) b
on a.date = b.date
and a.level = b.level
left join
(select date, level, count(distinct user_key) as u7
from g
where diff>=7
group by 1,2) c
on a.date = c.date
and a.level = c.level
order by level, date






delete from fruitscoot.fact_whale_users where date='${SCHEDULE_DATE}';

drop table if exists tmp_top_payers;
create temp table tmp_top_payers as
select 
	'${SCHEDULE_DATE}' as date,
	app_id,
	user_id,
	sum(revenue_usd) as total_revenue
from fruitscoot.fact_revenue
where app_id like 'fruitscoot.mobile%'
group by 1,2,3;

insert into fruitscoot.fact_whale_users
(
	date,
	app_id,
	user_id,
	rank_num,
	total_revenue
)
select * 
from
(
	select 
		'${SCHEDULE_DATE}'::date as date,
		app_id,
		user_id,
		row_number() over(partition by app_id order by total_revenue desc) as rank_num,
		total_revenue
	from tmp_top_payers
	where app_id like 'fruitscoot.mobile%'
) t
where t.rank_num <= 100;

drop table if exists tmp_revenue_90_days;
create temp table tmp_revenue_90_days as
select '${SCHEDULE_DATE}' as date, app_id, user_id, sum(revenue_usd) as revenue_90_days
from fruitscoot.fact_revenue
where datediff('day', date, '${SCHEDULE_DATE}') < 90
group by 1,2,3;

update fruitscoot.fact_whale_users 
set revenue_90_days = t.revenue_90_days
from tmp_revenue_90_days t
where fruitscoot.fact_whale_users.app_id = t.app_id
	and fruitscoot.fact_whale_users.user_id = t.user_id
	and fruitscoot.fact_whale_users.date = t.date;

update fruitscoot.fact_whale_users 
set last_login_date = u.last_login_date, last_level = u.level
from fruitscoot.dim_user u
where fruitscoot.fact_whale_users.app_id = u.app_id
	and fruitscoot.fact_whale_users.user_id = u.user_id;

update fruitscoot.fact_whale_users
set last_pay_date = l.last_pay_date
from 
(
	select * from
	(
		select 
			'${SCHEDULE_DATE}' as date,
			date as last_pay_date,
			app_id,
			user_id,
			row_number() over(partition by app_id, user_id order by date desc) as row_num
		from fruitscoot.fact_revenue
		where app_id like 'fruitscoot.mobile%'
	) t
	where t.row_num = 1	
) l
where fruitscoot.fact_whale_users.date = l.date
	and fruitscoot.fact_whale_users.app_id = l.app_id
	and fruitscoot.fact_whale_users.user_id = l.user_id;



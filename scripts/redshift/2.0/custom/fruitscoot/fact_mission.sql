---------------------------------------
-- create by jiating, 2015-12-15
---------------------------------------

-- fact mission , for mission events
delete from fruitscoot.fact_mission where date='${SCHEDULE_DATE}';
insert into fruitscoot.fact_mission
(
	date,
    ts_pretty,
    user_id,
    session_id,
    app_id,
    ip,
    os,
    os_version,
    gaid,
    level,
    mission_start_ts,
    mission_id,
    mission_status,
    mission_type,
    mission_name,
    app_version,
    language,
    android_id,
    install_ts_pretty,
    install_source,
    facebook_id,
    mission_unique_key,
    user_cnt,
    id,
    mission_stars
)
select 
	date(ts_pretty) as date,
	ts_pretty,
	user_id,
	session_id,
	app_id,
	json_extract_path_text(properties, 'ip')::VARCHAR(64) as ip,
	json_extract_path_text(properties, 'os')::VARCHAR(32) as os,
	json_extract_path_text(properties, 'os_version')::VARCHAR(32) as os_version,
	json_extract_path_text(properties, 'gaid')::VARCHAR(100) as gaid,
	json_extract_path_text(properties, 'level')::int as level,
	(TIMESTAMP 'epoch' + json_extract_path_text(properties, 'mission_start_ts')::int * INTERVAL '1 Second ') as mission_start_ts,
	json_extract_path_text(properties, 'mission_id')::VARCHAR(32) as mission_id,
	json_extract_path_text(properties, 'mission_status')::int as mission_status,
	json_extract_path_text(properties, 'mission_type')::VARCHAR(32) as mission_type,
	json_extract_path_text(properties, 'mission_name')::VARCHAR(64) as mission_name,
	json_extract_path_text(properties, 'app_version')::VARCHAR(32) as app_version,
	json_extract_path_text(properties, 'lang')::VARCHAR(32) as language,
	json_extract_path_text(properties, 'android_id')::VARCHAR(100) as android_id,
	cast(json_extract_path_text(properties, 'install_ts_pretty') as TIMESTAMP) as install_ts_pretty,
	json_extract_path_text(properties, 'install_source')::VARCHAR(200) as install_source,
	json_extract_path_text(properties, 'facebook_id')::VARCHAR(100) as facebook_id,
    json_extract_path_text(properties, 'mission_unique_key')::VARCHAR(64) as mission_unique_key,
	count(user_id) as user_cnt,
        MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')) as id,
    json_extract_path_text(properties, 'mission_stars') as mission_stars
from fruitscoot.events
where event = 'mission'
  and app_id like 'fruitscoot%' 
  and date(ts_pretty) = '${SCHEDULE_DATE}'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,24,25;

delete from fruitscoot.fact_mission where date>=current_date;

-- item used table for fruitscoot.mobile.prod
delete from fruitscoot.fact_item_transaction_mobile where date(ts_pretty)='${SCHEDULE_DATE}';
insert into fruitscoot.fact_item_transaction_mobile
(
    id,
    ts_pretty,
    ts,
    app_id,
    user_id,
    session_id,
    event,
    mission_unique_key,
    level,
    item_id,
    item_name,
    item_type,
    item_class,
    transaction_type,
    data_version,
    app_version,
    action,
    currency_type,
    item_used_qty
)
select 
    md5(app_id||user_id||session_id||ts) as id,
    ts_pretty,
    ts,
    app_id,
    user_id,
    session_id,
    event,
    json_extract_path_text(properties, 'mission_unique_key')::varchar(64) as mission_unique_key,
    json_extract_path_text(properties, 'level')::int as level,
    json_extract_path_text(items_used, 'd_item_id') as item_id,
    json_extract_path_text(items_used, 'd_item_name') as item_name,
    json_extract_path_text(items_used, 'd_item_type') as item_type,
    json_extract_path_text(items_used, 'd_item_class') as item_class,
    json_extract_path_text(properties, 'd_transaction_type') as transaction_type,
    data_version,
    json_extract_path_text(properties, 'app_version') as app_version,
    json_extract_path_text(properties, 'd_action') as action,
    json_extract_path_text(properties, 'currency_type') as currency_type,
    sum(nullif(json_extract_path_text(items_used, 'm_item_amount')::varchar, '')::int) as item_used_qty
from
    (select 
        *
        , json_extract_array_element_text(g.exploded_items, seq.i) as items_used 
    from
        (select 
            *
            , json_extract_path_text(properties, 'c_items_used') as exploded_items 
        from fruitscoot.events 
        where event='transaction' 
            and app_id like 'fruitscoot.mobile.prod' 
            and date(ts_pretty)='${SCHEDULE_DATE}'
        ) as g, seq_0_to_100 as seq
    where seq.i<json_array_length(g.exploded_items)
    )
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18;

update fruitscoot.fact_item_transaction_mobile set level=cast(f.mission_id as integer)
from fruitscoot.fact_mission f
where fruitscoot.fact_item_transaction_mobile.mission_unique_key = f.mission_unique_key
        and f.mission_unique_key <> ''
        and f.mission_status in (10, -10, -20);


-- update first play
drop table if exists tmp_fact_mission_first_play;
create temp table tmp_fact_mission_first_play as 
select * from
 	(select 
 		ts_pretty, 
 		user_id, 
 		mission_id, 
 		app_id, 
        app_version,
 		mission_status, 
 		row_number() over(partition by user_id, mission_id, app_id, app_version order by ts_pretty asc) as row_num  
 	from fruitscoot.fact_mission 
 	where mission_status=0 
 	) t 
where t.row_num = 1 ; 

update fruitscoot.fact_mission 
set first_play=1 
from tmp_fact_mission_first_play t
where fruitscoot.fact_mission.user_id = t.user_id
 	and fruitscoot.fact_mission.mission_id = t.mission_id
 	and fruitscoot.fact_mission.app_id = t.app_id
    and fruitscoot.fact_mission.app_version = t.app_version
	and fruitscoot.fact_mission.mission_status = t.mission_status
	and fruitscoot.fact_mission.ts_pretty = t.ts_pretty
 	and fruitscoot.fact_mission.date='${SCHEDULE_DATE}';

-- actually this is for first success of a level
drop table if exists tmp_fact_mission_first_play;
create temp table tmp_fact_mission_first_play as 
select * from
  (select 
    ts_pretty, 
    user_id, 
    mission_id, 
    app_id,
    app_version, 
    mission_status, 
    row_number() over(partition by user_id, mission_id, app_id, app_version order by ts_pretty asc) as row_num  
  from fruitscoot.fact_mission 
  where mission_status=10 
  ) t 
where t.row_num = 1 ; 

update fruitscoot.fact_mission 
set first_play=1 
from tmp_fact_mission_first_play t
where fruitscoot.fact_mission.user_id = t.user_id
  and fruitscoot.fact_mission.mission_id = t.mission_id
  and fruitscoot.fact_mission.app_id = t.app_id
  and fruitscoot.fact_mission.app_version = t.app_version
  and fruitscoot.fact_mission.mission_status = t.mission_status
  and fruitscoot.fact_mission.ts_pretty = t.ts_pretty
  and fruitscoot.fact_mission.date='${SCHEDULE_DATE}';




delete from fruitscoot.fact_dau_snapshot where date>current_date;

delete from fruitscoot.dim_user where install_date>current_date;

-- fact level up
delete from fruitscoot.fact_level_up 
where trunc(TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') = '${SCHEDULE_DATE}';
insert into fruitscoot.fact_level_up
(
	ts,
    session_id,
    app_id,
    data_version,
    user_id,
    gaid,
    from_level,
    level,
    app_version,
    os,
    os_version,
    lang,
    android_id,
    install_ts,
    facebook_id
)
select 
	ts,
    session_id,
    app_id,
    data_version,
    user_id,
    json_extract_path_text(properties, 'mission_status')::VARCHAR(128) as gaid,
    json_extract_path_text(properties, 'from_level')::smallint as from_level,
    json_extract_path_text(properties, 'level')::smallint as level,
    json_extract_path_text(properties, 'app_version')::VARCHAR(32) as app_version,
    json_extract_path_text(properties, 'os')::VARCHAR(32) as os,
    json_extract_path_text(properties, 'os_version')::VARCHAR(32) as os_version,
    json_extract_path_text(properties, 'lang')::VARCHAR(16) as lang,
    json_extract_path_text(properties, 'android_id')::VARCHAR(64) as android_id,
    json_extract_path_text(properties, 'install_ts')::bigint as install_ts,
    json_extract_path_text(properties, 'facebook_id')::VARCHAR(64) as facebook_id
from fruitscoot.events where event='level_up' and  date(ts_pretty)='${SCHEDULE_DATE}';

-- -- update dim_user level to highest level
 drop table if exists tmp_highest_level_for_user;
 create temp table tmp_highest_level_for_user as 
 select * from
  	(select 
  		user_id, 
  		level, 
  		app_id, 
 		row_number() over(partition by user_id, app_id order by level desc) as row_num  
  	from fruitscoot.fact_level_up 
  	) t 
 where t.row_num = 1 ; 

update fruitscoot.dim_user 
 set level=t.level
 from tmp_highest_level_for_user t
 where fruitscoot.dim_user.user_id = t.user_id
  	and fruitscoot.dim_user.app_id = t.app_id and fruitscoot.dim_user.app_id like '%fruitscoot2%'
 	and fruitscoot.dim_user.level<t.level;

----update fact_dau_snapshot
drop table if exists update_level_for_user_daily;
create temp table update_level_for_user_daily as 
select * from
 	(select 
 		md5(app_id||user_id) as user_key, 
 		level, 
 		app_id, 
 		trunc(TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') as date,
 		row_number() over(partition by user_id, app_id,trunc(TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') order by ts desc) as row_num  
 	from fruitscoot.fact_level_up 
 	) t 
where t.row_num = 1 ; 


update fruitscoot.fact_dau_snapshot set level_end=t.level from update_level_for_user_daily t
where fruitscoot.fact_dau_snapshot.user_key=t.user_key and t.date=fruitscoot.fact_dau_snapshot.date;


-- For lapsed user
truncate fruitscoot.fact_lapsed_users;
insert into fruitscoot.fact_lapsed_users
(
    app_id
    ,user_id
    ,facebook_id
    ,install_date
    ,install_source
    ,browser
    ,language
    ,country
    ,os
    ,level
    ,is_payer
    ,last_login_date
    ,lapsed_days
)
select
    app_id
    ,user_id
    ,facebook_id
    ,install_date
    ,install_source
    ,browser
    ,language
    ,country
    ,os
    ,level
    ,is_payer
    ,last_login_date
    ,trunc(CONVERT_TIMEZONE('UTC', CURRENT_DATE)) - trunc(last_login_date) as lapsed_days
from fruitscoot.dim_user
where level >= 1 ;


-- agg_mission
drop table if exists tmp_d3_first_play;
create temp table tmp_d3_first_play as
select '${SCHEDULE_DATE}' - 3 as date, mission_id, app_id, app_version, count(user_id) as d3_first_play_cnt 
from fruitscoot.fact_mission 
where datediff('day', date, '${SCHEDULE_DATE}') <= 3
        and date<= '${SCHEDULE_DATE}' 
        and first_play=1
        and mission_status = 0
group by mission_id, app_id, app_version;

drop table if exists tmp_d3_lapsed;
create temp table tmp_d3_lapsed as
select '${SCHEDULE_DATE}' - 3 as date, level, app_id, app_version, count(user_id) as d3_lapsed_cnt 
from fruitscoot.dim_user as d
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) = 3
        and d.user_id in(
                select f.user_id 
                from fruitscoot.fact_mission as f
                where datediff('day', f.date, '${SCHEDULE_DATE}') <= 3
                        and f.date<= '${SCHEDULE_DATE}' 
                        and d.level = f.mission_id
                        and d.app_id = f.app_id
                        and d.app_version = f.app_version
                        and f.mission_status = 0
                        and f.first_play = 1
            )
group by level, app_id, app_version;

drop table if exists tmp_d7_first_play;
create temp table tmp_d7_first_play as
select '${SCHEDULE_DATE}' - 7 as date, mission_id, app_id, app_version, count(user_id) as d7_first_play_cnt 
from fruitscoot.fact_mission 
where  datediff('day', date, '${SCHEDULE_DATE}') <= 7
        and date<= '${SCHEDULE_DATE}'
        and first_play=1
        and mission_status = 0
group by mission_id, app_id, app_version;

drop table if exists tmp_d7_lapsed;
create temp table tmp_d7_lapsed as
select '${SCHEDULE_DATE}' - 7 as date, level, app_id, app_version, count(user_id) as d7_lapsed_cnt 
from fruitscoot.dim_user as d
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) = 7 
        and d.user_id in(
                select f.user_id 
                from fruitscoot.fact_mission  as f
                where datediff('day', f.date, '${SCHEDULE_DATE}') <= 7
                    and f.date<= '${SCHEDULE_DATE}'
                    and d.level = f.mission_id
                    and d.app_id = f.app_id
                    and d.app_version = f.app_version
                    and f.mission_status = 0
                    and f.first_play=1
            )
group by level, app_id, app_version;

drop table if exists tmp_failed_today;
create temp table tmp_failed_today as
select date, ts_pretty, mission_id, mission_status, app_id, app_version, user_id,first_play
from fruitscoot.fact_mission
where mission_status = 10
    and first_play = 1
    and date = '${SCHEDULE_DATE}';
    
drop table if exists tmp_d7_failed; 
create temp table tmp_d7_failed as
select '${SCHEDULE_DATE}' as date, f.mission_id, f.app_id, f.app_version, count(f.user_id) as d7_failed_cnt 
from fruitscoot.fact_mission f, tmp_failed_today f1
where datediff('day', f.date, '${SCHEDULE_DATE}') <= 7
            and f.ts_pretty < f1.ts_pretty
            and f.mission_id = f1.mission_id
            and f.user_id = f1.user_id
      and f.app_id = f1.app_id 
      and f.app_version = f1.app_version 
      and f.mission_status=-10
group by 1,2,3,4;

-- success cnt 
drop table if exists tmp_total_success;
create temp table tmp_total_success as
select '${SCHEDULE_DATE}' as date, mission_id, app_id, app_version, count(user_id) as success_cnt
from fruitscoot.fact_mission
where date='${SCHEDULE_DATE}' 
        and mission_status = 10 
        and first_play=1
group by mission_id, app_id, app_version;


drop table if exists tmp_first_play;
create temp table tmp_first_play as
select '${SCHEDULE_DATE}' as date, mission_id, app_id, app_version, count(user_id) as first_play_cnt
from fruitscoot.fact_mission
where date='${SCHEDULE_DATE}' 
        and first_play=1
        and mission_status = 0
group by mission_id, app_id, app_version;

drop table if exists tmp_total_play;
create temp table tmp_total_play as
select '${SCHEDULE_DATE}' as date, mission_id, app_id, app_version, count(distinct user_id) as total_play_cnt
from fruitscoot.fact_mission
where date='${SCHEDULE_DATE}' 
        and mission_status=0 
group by mission_id, app_id, app_version;

drop table if exists tmp_d7_play;
create temp table tmp_d7_play as
select '${SCHEDULE_DATE}' as date, f.mission_id, f.app_id, f.app_version, f.user_id, f.mission_status, 0 as c
from custom.fruitscoot.fact_mission f
where 
datediff('day', date, '${SCHEDULE_DATE}') <= 7
and date<= '${SCHEDULE_DATE}'
and mission_status<>20 
and user_id in (
        select distinct f1.user_id 
        from custom.fruitscoot.fact_mission f1
        where f1.date = '${SCHEDULE_DATE}'
            and f1.mission_id = f.mission_id
            and f1.app_id = f.app_id 
            and f1.app_version = f.app_version 
            and f1.mission_status = 10 
            and f1.first_play=1

);
update tmp_d7_play set c=1 where mission_status=-10;

drop table if exists tmp_d7_failed_cnt_total;
create temp table tmp_d7_failed_cnt_total as
select date, mission_id, app_id, app_version, user_id, sum(c) as c
from tmp_d7_play 
group by 1,2,3,4,5;

drop table if exists tmp_item_used_qty;
create temp table tmp_item_used_qty as 
select date(ts_pretty) as date, level, app_id, app_version, sum(item_used_qty) as item_used_qty
from fruitscoot.fact_item_transaction_mobile 
where date(ts_pretty)='${SCHEDULE_DATE}'
group by 1,2,3,4;

drop table if exists tmp_item_used_user_count;
create temp table tmp_item_used_user_count as 
select date(ts_pretty) as date, level, app_id, app_version, count(distinct user_id) as item_used_user_count
from fruitscoot.fact_item_transaction_mobile 
where date(ts_pretty)='${SCHEDULE_DATE}'
group by 1,2,3,4;

drop table if exists tmp_failed_count_p;
create temp table tmp_failed_count_p as 
select  date, mission_id, app_id, app_version, p10, p50, p90, count(1) as c
from
(select date, mission_id, app_id, app_version, user_id, 
percentile_disc(0.1) 
within group (order by c) 
over(partition by date, mission_id, app_id, app_version) as p10,
percentile_disc(0.5) 
within group (order by c) 
over(partition by date, mission_id, app_id, app_version ) as p50,
percentile_disc(0.9) 
within group (order by c) 
over(partition by date,mission_id, app_id, app_version) as p90
from tmp_d7_failed_cnt_total)
group by 1,2,3,4,5,6,7;

drop table if exists tmp_level;
create temp table tmp_level as 
select level, app_id, app_version, '${SCHEDULE_DATE}' as date
from fruitscoot.fact_mission 
where date = '${SCHEDULE_DATE}' 
group by level, app_id, app_version;


delete from fruitscoot.agg_level_norm  where date='${SCHEDULE_DATE}';
insert into fruitscoot.agg_level_norm 
(
    date,
    level,
    app_id,
    app_version, 
    d3_lapsed_cnt,
    d3_first_play_cnt,
    d7_lapsed_cnt,
    d7_first_play_cnt,
    d7_failed_cnt,
    success_cnt,
    first_play_cnt,
    total_play_cnt,
    d7_failed_p10,
    d7_failed_p50,
    d7_failed_p90,
    item_used_qty,
    item_used_user_count
) 
select 
    '${SCHEDULE_DATE}' as date,
    tl.level as level,
    tl.app_id as app_id,
    tl.app_version as app_version,
    0 as d3_lapsed_cnt,
    1 as d3_first_play_cnt,
    0 as d7_lapsed_cnt,
    1 as d7_first_play_cnt,
    d7_fail.d7_failed_cnt as d7_failed_cnt,
    s.success_cnt as success_cnt,
    f.first_play_cnt as first_play_cnt,
    t.total_play_cnt as total_play_cnt,
    p.p10 as d7_failed_p10,
    p.p50 as d7_failed_p50,
    p.p90 as d7_failed_p90,
    iq.item_used_qty as item_used_qty,
    iu.item_used_user_count as item_used_user_count
from 
    tmp_level tl
    left join tmp_d3_first_play d3_f on tl.date = d3_f.date and tl.level = d3_f.mission_id and tl.app_id = d3_f.app_id and tl.app_version = d3_f.app_version
    left join tmp_d3_lapsed d3_l on tl.date = d3_l.date and tl.level = d3_l.level and tl.app_id = d3_l.app_id and tl.app_version = d3_l.app_version
    left join tmp_d7_first_play d7_f on tl.date = d7_f.date and tl.level = d7_f.mission_id and tl.app_id = d7_f.app_id and tl.app_version = d7_f.app_version
    left join tmp_d7_lapsed d7_l on tl.date = d7_l.date and tl.level = d7_l.level and tl.app_id = d7_l.app_id and tl.app_version = d7_l.app_version
    left join tmp_d7_failed d7_fail on tl.date = d7_fail.date and tl.level = d7_fail.mission_id and tl.app_id = d7_fail.app_id and tl.app_version = d7_fail.app_version
    left join tmp_total_success s on tl.date = s.date and tl.level = s.mission_id and tl.app_id = s.app_id and tl.app_id = s.app_id and tl.app_version = s.app_version
    left join tmp_first_play f on tl.date = f.date and tl.level = f.mission_id and tl.app_id = f.app_id and tl.app_version = f.app_version
    left join tmp_total_play t on tl.date = t.date and tl.level = t.mission_id and tl.app_id = t.app_id and tl.app_version = t.app_version
    left join tmp_failed_count_p p on tl.date = p.date and tl.level = p.mission_id and tl.app_id = p.app_id and tl.app_version = p.app_version
    left join tmp_item_used_qty iq on tl.date = iq.date and tl.level = iq.level and tl.app_id = iq.app_id and tl.app_version = iq.app_version
    left join tmp_item_used_user_count iu on tl.date = iu.date and tl.level = iu.level and tl.app_id = iu.app_id and tl.app_version = iu.app_version
    ;

-- update d3 & d7 lapsed data
update fruitscoot.agg_level_norm 
  set d3_lapsed_cnt = t.d3_lapsed_cnt
  from tmp_d3_lapsed t
  where fruitscoot.agg_level_norm.date = t.date
    and fruitscoot.agg_level_norm.level = t.level
    and fruitscoot.agg_level_norm.app_id = t.app_id
    and fruitscoot.agg_level_norm.app_version = t.app_version;

update fruitscoot.agg_level_norm 
  set d3_first_play_cnt = t.d3_first_play_cnt
  from tmp_d3_first_play t
  where fruitscoot.agg_level_norm.date = t.date
    and fruitscoot.agg_level_norm.level = t.mission_id
    and fruitscoot.agg_level_norm.app_id = t.app_id
    and fruitscoot.agg_level_norm.app_version = t.app_version;

update fruitscoot.agg_level_norm 
  set d7_lapsed_cnt = t.d7_lapsed_cnt
  from tmp_d7_lapsed t
  where fruitscoot.agg_level_norm.date = t.date
    and fruitscoot.agg_level_norm.level = t.level
    and fruitscoot.agg_level_norm.app_id = t.app_id
    and fruitscoot.agg_level_norm.app_version = t.app_version;

update fruitscoot.agg_level_norm 
  set d7_first_play_cnt = t.d7_first_play_cnt
  from tmp_d7_first_play t
  where fruitscoot.agg_level_norm.date = t.date
    and fruitscoot.agg_level_norm.level = t.mission_id
    and fruitscoot.agg_level_norm.app_id = t.app_id
    and fruitscoot.agg_level_norm.app_version = t.app_version;

update fruitscoot.agg_level_norm set item_used_user_count=0 where item_used_user_count is null ;
update fruitscoot.agg_level_norm set item_used_qty=0 where item_used_qty is null;

/*delete from fruitscoot.level_churn;

insert into fruitscoot.level_churn
with max_date as (select max(install_date) as date from fruitscoot.dim_user where install_date<=current_date)
select
  u.app_id
  ,u.app_version
  ,l.level
  ,u.level as current_level
  ,u.install_date
  ,u.install_source
  ,u.country
  ,u.os
  ,u.browser
  ,u.language
  ,u.is_payer
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=60 then 1 else 0 end as is_churned_60days
  ,count(distinct u.user_key) as user_cnt
from fruitscoot.dim_user u
join (select distinct level from fruitscoot.dim_user) l on u.level>=l.level
join max_date t on 1=1
where install_date<=current_date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18; 
*/




drop table if exists install_app_version;
create table install_app_version as
select distinct d.user_key,d.app_version as install_app_version from fruitscoot.fact_dau_snapshot d
join fruitscoot.dim_user u on d.user_key=u.user_key and d.date=u.install_date
where d.app_id like 'fruitscoot%';


drop table if exists fruitscoot.level_churn_ab;
create table  fruitscoot.level_churn_ab as
with this_date as (select max(date) as date from fruitscoot.fact_dau_snapshot)
select
  u.app_id
  ,u.app_version
  ,a.install_app_version
  ,l.level
  ,u.level as current_level
  ,u.install_date
  ,u.install_source
  ,c.ab_experiment1
  ,c.ab_variable1
  ,c.ab_experiment2
  ,c.ab_variable2
  ,c.ab_experiment3
  ,c.ab_variable3
  ,c.ab_experiment4
  ,c.ab_variable4
  ,u.country
  ,u.os
  ,u.browser
  ,u.language
  ,u.is_payer
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,case when l.level=u.level and datediff(day,u.last_login_date,t.date)>=60 then 1 else 0 end as is_churned_60days
  ,count(distinct u.user_key) as user_cnt
from fruitscoot.dim_user u
left join install_app_version a on a.user_key=u.user_key
join (select distinct level from fruitscoot.dim_user) l on u.level>=l.level
join this_date t on 1=1
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date DESC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) c
      on u.user_key = c.user_key
where app_id like 'fruitscoot2%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27;



-- step lapsed table
delete from fruitscoot.fact_mission_step where date='${SCHEDULE_DATE}';
insert into fruitscoot.fact_mission_step
(
  date,
  ts_pretty,
  user_id,
  session_id,
  app_id,
  ip,
  os,
  os_version,
  gaid,
  level,
  mission_start_ts,
  mission_id,
  mission_status,
  mission_type,
  mission_name,
  app_version,
  language,
  android_id,
  install_ts_pretty,
  install_source,
  facebook_id,
  user_cnt,
  id,
  step
)
select 
  date(ts_pretty) as date,
  ts_pretty,
  user_id,
  session_id,
  app_id,
  json_extract_path_text(properties, 'ip')::VARCHAR(64) as ip,
  json_extract_path_text(properties, 'os')::VARCHAR(32) as os,
  json_extract_path_text(properties, 'os_version')::VARCHAR(32) as os_version,
  json_extract_path_text(properties, 'gaid')::VARCHAR(100) as gaid,
  json_extract_path_text(properties, 'level')::int as level,
  (TIMESTAMP 'epoch' + json_extract_path_text(properties, 'mission_start_ts')::int * INTERVAL '1 Second ') as mission_start_ts,
  json_extract_path_text(properties, 'mission_id')::VARCHAR(32) as mission_id,
  json_extract_path_text(properties, 'mission_status')::int as mission_status,
  json_extract_path_text(properties, 'mission_type')::VARCHAR(32) as mission_type,
  json_extract_path_text(properties, 'mission_name')::VARCHAR(64) as mission_name,
  json_extract_path_text(properties, 'app_version')::VARCHAR(32) as app_version,
  json_extract_path_text(properties, 'lang')::VARCHAR(32) as language,
  json_extract_path_text(properties, 'android_id')::VARCHAR(100) as android_id,
  cast(json_extract_path_text(properties, 'install_ts_pretty') as TIMESTAMP) as install_ts_pretty,
  json_extract_path_text(properties, 'install_source')::VARCHAR(200) as install_source,
  json_extract_path_text(properties, 'facebook_id')::VARCHAR(100) as facebook_id,
  count(distinct user_id) as user_cnt,
  MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')) as id,
  json_extract_path_text(properties, 'step')::SMALLINT as step
from fruitscoot.events
where event = 'mission'
  and app_id like 'fruitscoot%' 
  and date(ts_pretty) = '${SCHEDULE_DATE}'
  and json_extract_path_text(properties,'mission_status')=20
  and json_extract_path_text(properties,'level')=json_extract_path_text(properties,'mission_id')
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23,24;

delete from fruitscoot.fact_mission_step where date > '${SCHEDULE_DATE}';

-- update new user
update fruitscoot.fact_mission_step set is_new_user = 1 
from fruitscoot.dim_user d
where fruitscoot.fact_mission_step.date ='${SCHEDULE_DATE}'
  and fruitscoot.fact_mission_step.date = d.install_date
  and fruitscoot.fact_mission_step.user_id = d.user_id
  and fruitscoot.fact_mission_step.app_id = d.app_id;

-- special handing for new user step lapsed report
delete from fruitscoot.fact_mission_step where level = 2 and step > 2;
delete from fruitscoot.fact_mission_step where level = 3 and step > 2;
delete from fruitscoot.fact_mission_step where level = 4 and step > 4;



----------- dau_dou_retained

drop table if exists fruitscoot.dau_dou_retained;
create table  fruitscoot.dau_dou_retained as
select d.app_id,
    d.app_version,
    d.user_key,
    u.install_date,
    u.install_source,
    u.install_os as os,
    u.install_browser as browser,
    u.install_country as country,
    u.install_language as language,
    u.is_payer,
    d.date,
    f.date as retained_date,
    d.is_new_user,
    d.level_end
     from fruitscoot.fact_dau_snapshot d
    left join fruitscoot.dim_user u on d.user_key=u.user_key
    left join (select * from fruitscoot.fact_dau_snapshot where app_id like '%fruitscoot.m%') f
    on d.user_key=f.user_key and f.date>=d.date where d.app_id like 'fruitscoot.m%';


-- aggregation for level churn of 7 days ago and 8-14 days week
drop table if exists tmp_level;
create temp table tmp_level as 
select '${SCHEDULE_DATE}' as date, level, app_id
from fruitscoot.fact_mission
where app_id='fruitscoot.mobile.prod' 
group by 1, 2, 3;

-- highest level of user
drop table if exists tmp_highest_level_for_user;
create temp table tmp_highest_level_for_user as 
select * from
    (select 
        user_id, 
        level, 
        app_id, 
        row_number() over(partition by user_id, app_id order by level desc) as row_num  
    from fruitscoot.fact_level_up 
    ) t 
where t.row_num = 1 ; 

drop table if exists tmp_d7_lapsed;
create temp table tmp_d7_lapsed as
select '${SCHEDULE_DATE}' as date, level, app_id, count(distinct user_id) as d7_lapsed_cnt 
from fruitscoot.dim_user
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) = 7 
group by 1, 2, 3;

drop table if exists tmp_d7_highest_level;
create temp table tmp_d7_highest_level as
select '${SCHEDULE_DATE}' as date, u.level, u.app_id, count(distinct u.user_id) as d7_highest_level
from fruitscoot.dim_user u, tmp_highest_level_for_user h
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(u.last_login_date) = 7
    and u.user_id = h.user_id
    and u.app_id = h.app_id
    and u.level < h.level
group by 1, 2, 3;

drop table if exists tmp_d7_dau;
create temp table tmp_d7_dau as
select '${SCHEDULE_DATE}' as date, app_id, count(distinct user_key) as d7_dau
from fruitscoot.fact_dau_snapshot
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(date) = 7
group by 1, 2;

drop table if exists tmp_d8_14_lapsed;
create temp table tmp_d8_14_lapsed as
select '${SCHEDULE_DATE}' as date, level, app_id, count(distinct user_id) as d8_14_lapsed_cnt 
from fruitscoot.dim_user
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) >= 8
    and trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) <= 14
group by 1, 2, 3;

drop table if exists tmp_d8_14_highest_level;
create temp table tmp_d8_14_highest_level as
select '${SCHEDULE_DATE}' as date, u.level, u.app_id, count(distinct u.user_id) as d8_14_highest_level
from fruitscoot.dim_user u, tmp_highest_level_for_user h
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) >= 8
    and trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(last_login_date) <= 14
    and u.user_id = h.user_id
    and u.app_id = h.app_id
    and u.level < h.level
group by 1, 2, 3;

drop table if exists tmp_d8_14_dau;
create temp table tmp_d8_14_dau as
select '${SCHEDULE_DATE}' as date, app_id, count(distinct user_key) as d8_14_dau
from fruitscoot.fact_dau_snapshot
where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(date) >= 8
    and trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(date) <= 14
group by 1, 2;


delete from fruitscoot.agg_level_churn_ch where date='${SCHEDULE_DATE}';
insert into fruitscoot.agg_level_churn_ch
(
    date,
    app_id,
    level,
    user_count_7,
    dau_7,
    highest_level_count_7,
    user_count_8_14,
    dau_8_14,
    highest_level_count_8_14
) 
select 
    tl.date::date as date,
    tl.app_id as app_id,
    tl.level as level,
    d7_l.d7_lapsed_cnt as user_count_7,
    d7_d.d7_dau as dau_7,
    d7_h.d7_highest_level as highest_level_count_7,
    d8_14_l.d8_14_lapsed_cnt as user_count_8_14,
    d8_14_d.d8_14_dau as dau_8_14,
    d8_14_h.d8_14_highest_level as highest_level_count_8_14
from
    tmp_level tl
    left join tmp_d7_lapsed d7_l on tl.date = d7_l.date and tl.app_id = d7_l.app_id and tl.level = d7_l.level
    left join tmp_d7_dau d7_d on tl.date = d7_d.date and tl.app_id = d7_d.app_id
    left join tmp_d7_highest_level d7_h on tl.date = d7_h.date and tl.app_id = d7_h.app_id and tl.level = d7_h.level
    left join tmp_d8_14_lapsed d8_14_l on tl.date = d8_14_l.date and tl.app_id = d8_14_l.app_id and tl.level = d8_14_l.level
    left join tmp_d8_14_dau d8_14_d on tl.date = d8_14_d.date and tl.app_id = d8_14_d.app_id
    left join tmp_d8_14_highest_level d8_14_h on tl.date = d8_14_h.date and tl.app_id = d8_14_h.app_id and tl.level = d8_14_h.level
    ;
-- agg_level_churn_ch end ---

-- agg_level_kpi
drop table if exists tmp_30day_mau;
create temp table tmp_30day_mau as 
select '${SCHEDULE_DATE}' as date, t.app_id, t.level_end, count(distinct t.user_key) as mau_cnt from
    (select 
        user_key, 
        app_id,
        level_end, 
        row_number() over(partition by user_key, app_id order by level_end desc) as row_num  
    from fruitscoot.fact_dau_snapshot
    where trunc(CONVERT_TIMEZONE('UTC', '${SCHEDULE_DATE}')) - trunc(date) <= 30 
    ) t 
where t.row_num = 1 
group by 1, 2, 3; 

delete from fruitscoot.agg_level_kpi where date = '${SCHEDULE_DATE}';
insert into fruitscoot.agg_level_kpi
(
    date,
    app_id,
    level,
    mau_cnt,
    new_user_cnt,
    dau_cnt
) 
select 
    k.date as date,
    k.app_id as app_id,
    k.level_end as level,
    t.mau_cnt as mau_cnt,
    sum(k.new_user_cnt) as new_user_cnt,
    sum(k.dau_cnt) as dau_cnt
from fruitscoot.agg_kpi k, tmp_30day_mau t
where 
    k.app_id like 'fruitscoot.mobile%'
    and k.date = '${SCHEDULE_DATE}'
    and k.date = t.date
    and k.app_id = t.app_id
    and k.level_end = t.level_end
group by 1,2,3,4
;
-- agg_level_kpi end ---
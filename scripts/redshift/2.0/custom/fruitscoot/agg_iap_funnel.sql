delete from fruitscoot.iap_funnel where trunc(ts_pretty)>=current_date-3;
insert into fruitscoot.iap_funnel
select md5(app_id||user_id) as user_key,app_id,ts_pretty,user_id,session_id,
json_extract_path_text(properties,'app_version') as app_version,
json_extract_path_text(properties,'event_type') as event_type,
json_extract_path_text(properties,'iap_product_id') as iap_product_id,
json_extract_path_text(properties,'iap_product_name') as iap_product_name,
cast(json_extract_path_text(properties,'level') as int) as level
 from fruitscoot.events where event='iap_funnel' and app_id like '%fruitscoot.m%' and 
trunc(ts_pretty)>=current_date-3;

drop table if exists session_revenue;
create table session_revenue as
select s.app_id,s.user_key,s.date_start,s.level_start,p.revenue_usd,t.total_orders from (select distinct app_id, user_key,date_start,level_start from fruitscoot.fact_session where app_id like 'fruitscoot.m%'
and date_start>=current_date-3) s
left join (select level,date,user_key,sum(revenue_usd) as revenue_usd from fruitscoot.fact_revenue where app_id like 'fruitscoot.m%' and date>=current_date-3 group by 1,2,3) p
on s.user_key=p.user_key and s.date_start=p.date and s.level_start=p.level
left join (select distinct user_key, level,trunc(ts_pretty) as date,count(1) as Total_Orders from fruitscoot.iap_funnel where event_type='buy' and trunc(ts_pretty)>=current_date-3 group by 1,2,3)t
on s.user_key=t.user_key and s.date_start=t.date and s.level_start=t.level;


create temp table revenue_missing as
select p.app_id,p.user_key,p.date,p.level,revenue_usd,total_orders from 
(select app_id,level,date,user_key,sum(revenue_usd) as revenue_usd from fruitscoot.fact_revenue where app_id like 'fruitscoot.m%' and date>=current_date-3 group by 1,2,3,4) p
left join (select distinct user_key, level,trunc(ts_pretty) as date,count(1) as Total_Orders from fruitscoot.iap_funnel where event_type='buy' and trunc(ts_pretty)>=current_date-3 group by 1,2,3)t
on p.user_key=t.user_key and p.date=t.date and p.level=t.level where
(p.user_key,p.date,p.level) not in (select user_key,date_start,level_start from session_revenue where date_start >=current_date-3);


insert into session_revenue
select app_id,user_key,date,level,revenue_usd,total_orders from revenue_missing;

create temp table buy_missing as
select app_id,user_key,date,level,0 as revenue_usd,total_orders from  (select distinct app_id,user_key, level,trunc(ts_pretty) as date,count(1) as Total_Orders from fruitscoot.iap_funnel where 
trunc(ts_pretty)>=current_date-3 and event_type='buy' group by 1,2,3,4)t
 where
(user_key,date,level) not in (select user_key,date_start,level_start from session_revenue where date_start>=current_date-3 );

insert into session_revenue
select app_id,user_key,date,level,revenue_usd,total_orders from buy_missing;


---drop table if exists fruitscoot.agg_iap_funnel;

delete from fruitscoot.agg_iap_funnel where date>=current_date-3;
insert into fruitscoot.agg_iap_funnel 
select d.app_id,d.date,d.app_version,u.country,u.language,d.os,install_source,
d.user_key,coalesce(p.level_start,d.level_start) as level,p.revenue_usd,0 as is_converted_today,p.total_orders
from (select app_id,user_key,date,app_version,level_start,os,(case when revenue_usd>0 then 1 else 0 end) as is_payer_today,is_converted_today,
is_new_user from  fruitscoot.fact_dau_snapshot where date>=current_date-3) d
left join fruitscoot.dim_user u on d.user_key=u.user_key
left join (select level_start,date_start,user_key,sum(revenue_usd) as revenue_usd,sum(total_orders) as total_orders from session_revenue where app_id like 'fruitscoot.m%' group by 1,2,3) p
on d.user_key=p.user_key and d.date=p.date_start;

CREATE TEMP TABLE conversion as
select user_key,min(level) as level,min(ts) as conversion_ts from fruitscoot.fact_revenue group by 1;

update fruitscoot.agg_iap_funnel set is_converted_today=1 from conversion c where 
fruitscoot.agg_iap_funnel.user_key=c.user_key and fruitscoot.agg_iap_funnel.level=c.level and fruitscoot.agg_iap_funnel.date=trunc(c.conversion_ts);

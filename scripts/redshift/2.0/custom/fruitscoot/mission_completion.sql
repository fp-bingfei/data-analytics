
---   objectives

delete from fruitscoot.fact_mission_objectives where trunc(ts_pretty)>=current_date-5;
INSERT INTO fruitscoot.fact_mission_objectives 
SELECT 
app_id,
user_id,
ts_pretty,
session_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_id') as objective_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_name') as objective_name,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_type') as objective_type,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_amount') as objective_amount,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),seq.i),'objective_amount_remaining') as objective_amount_remaining,
MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')) as id
from fruitscoot.events, 
seq_0_to_10 AS seq
WHERE event='mission' and app_id like 'fruitscoot2%' and trunc(ts_pretty)>=current_date-5 and json_extract_path_text(properties,'c2') like '%mission_objectives%' and 
seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'))
;



-------------- parameters
delete from fruitscoot.fact_mission_parameters where trunc(ts_pretty)>=current_date-5;
INSERT INTO fruitscoot.fact_mission_parameters
SELECT 
app_id,
user_id,
ts_pretty,
session_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c3'),'mission_parameters'),seq.i),'name') as parameter_name,
cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c3'),'mission_parameters'),seq.i),'value') as int) as parameter_value,
MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')) as id
from fruitscoot.events, 
seq_0_to_10 AS seq
WHERE event='mission' and app_id like 'fruitscoot2%' and trunc(ts_pretty)>=current_date-5 and json_extract_path_text(properties,'c3') like '%mission_parameter%' and 
seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (json_extract_path_text(properties,'c3'),'mission_parameters'));


---------statistics
delete from fruitscoot.fact_mission_statistics where trunc(ts_pretty)>=current_date-5;
INSERT INTO fruitscoot.fact_mission_statistics
SELECT 
app_id,
user_id,
ts_pretty,
session_id,
json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c4'),'mission_statistics'),seq.i),'name') as statistic_name,
cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c4'),'mission_statistics'),seq.i),'value') as int) as statistic_value,
MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')) as id
from fruitscoot.events, 
seq_0_to_10 AS seq
WHERE event='mission' and app_id like 'fruitscoot2%' and trunc(ts_pretty)>=current_date-5 and json_extract_path_text(properties,'c4') like '%mission_statistic%' and 
seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (json_extract_path_text(properties,'c4'),'mission_statistics'));


----objectives split

delete from fruitscoot.mission_objectives_split where date>=current_date-5
;
insert into fruitscoot.mission_objectives_split
select 
 MD5(app_id||event||user_id||session_id||ts_pretty||coalesce(json_extract_path_text(properties, 'mission_id'),'Unknown')||coalesce(json_extract_path_text(properties, 'mission_status'),'Unknown')) as id,
  data_version,
  app_id,
  trunc(ts_pretty) as date,
  user_id,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),0),'objective_name') as objective_name1,
   json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),1),'objective_name') as objective_name2,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),2),'objective_name') as objective_name3,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),3),'objective_name') as objective_name4,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),4),'objective_name') as objective_name5,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),5),'objective_name') as objective_name6,
  json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (json_extract_path_text(properties,'c2'),'mission_objectives'),6),'objective_name') as objective_name7,
  nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),0),'objective_amount_remaining'),'')::int,0) as objective_remain1,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),1),'objective_amount_remaining'),'')::int,0) as objective_remain2,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),2),'objective_amount_remaining'),'')::int,0) as objective_remain3,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),3),'objective_amount_remaining'),'')::int,0) as objective_remain4,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),4),'objective_amount_remaining'),'')::int,0) as objective_remain5,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),5),'objective_amount_remaining'),'')::int,0) as objective_remain6,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),6),'objective_amount_remaining'),'')::int,0) as objective_remain7,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),0),'objective_amount'),'')::int,0) as objective_amount1,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),1),'objective_amount'),'')::int,0) as objective_amount2,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),2),'objective_amount'),'')::int,0) as objective_amount3,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),3),'objective_amount'),'')::int,0) as objective_amount4,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),4),'objective_amount'),'')::int,0) as objective_amount5,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),5),'objective_amount'),'')::int,0) as objective_amount6,
nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(json_extract_path_text(properties,'c2'),'mission_objectives'),6),'objective_amount'),'')::int,0) as objective_amount7
 from fruitscoot.events where app_id like '%2%' and event='mission' and trunc(ts_pretty)>=current_date-5;






----order completion

drop table if exists mission_attempt;
create temp table mission_attempt as
SELECT
    md5(s.app_id||s.user_id) as user_key
    ,s.mission_id
    ,s.app_version
    ,max(s.date) as date
    ,count(distinct s.mission_start_ts) as attempt_cnt
FROM fruitscoot.fact_mission s
join fruitscoot.fact_level_up l
    on l.user_id=s.user_id and l.app_id=s.app_id and l.level=cast(s.mission_id as integer)+1
left join fruitscoot.fact_mission e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_id=e.mission_id and
        e.user_id=s.user_id and e.app_id=s.app_id and
        e.mission_status!=s.mission_status
where s.app_id like '%2%' and
    s.mission_status=0 
and s.ts_pretty<(TIMESTAMP 'epoch' + l.ts::BIGINT * INTERVAL '0.001 Second ')
group by 1,2,3
;

drop table if exists attempt_to_pass;
create temp table attempt_to_pass as
SELECT
    m.user_key
	,m.mission_id
	,m.date
	,u.app_id
	,m.app_version
	,u.os
	,MEDIAN(attempt_cnt) over (partition by m.date,m.mission_id,u.app_id,m.app_version) as attempt_median
	,PERCENTILE_CONT(0.25) within group (order by attempt_cnt) over (partition by 
m.date,m.mission_id,u.app_id,m.app_version) as attempt_q1
	,PERCENTILE_CONT(0.75) within group (order by attempt_cnt) over (partition by 
m.date,m.mission_id,u.app_id,m.app_version) as attempt_q3
FROM mission_attempt m
--join fruitscoot.fact_dau_snapshot d on d.user_key=m.user_key and m.date=d.date
join fruitscoot.dim_user u ON u.user_key=m.user_key
where u.app_id like '%2%'

;



drop table if exists fruitscoot.tab_mission_attempt;
create  table fruitscoot.tab_mission_attempt as
SELECT
	p.mission_id
	,p.date
	,p.app_id
	,p.app_version
	,a.install_app_version
	,p.os
	,c.ab_experiment1
    ,c.ab_variable1
    ,c.ab_experiment2
    ,c.ab_variable2
    ,c.ab_experiment3
    ,c.ab_variable3
    ,c.ab_experiment4
    ,c.ab_variable4
    ,max(attempt_median) as attempt_median
    ,max(attempt_q1) as attempt_q1
    ,max(attempt_q3) as attempt_q3
    ,sum(attempt_cnt) as attempt_cnt
	,count(distinct p.user_key) as user_cnt
FROM attempt_to_pass p
join install_app_version a on a.user_key=p.user_key
join mission_attempt m on m.user_key=p.user_key and m.mission_id=p.mission_id
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) c
      on p.user_key = c.user_key
      where p.app_id like '%2%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14
order by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

drop table if exists boosters_moves;
create temp table boosters_moves as
select a.*,(case when s.statistic_name='ingame_boosts' and cast(s.statistic_value as int)>0 then 1 else 0 end) as ingame_boosts,
(case when d.statistic_name='pregame_boosts' and cast(d.statistic_value as int)>0 then 1 else 0 end) as pregame_boosts,
(case when t.statistic_name='moves_buy' and cast(t.statistic_value as int)>0 then 1 else 0 end) as buy_moves from fruitscoot.fact_mission a
     left join (select distinct id,user_id,app_id,statistic_name,statistic_value from fruitscoot.fact_mission_statistics where statistic_name like '%ingame%') s on a.id=s.id
     left join (select distinct id,user_id,app_id,statistic_name,statistic_value from fruitscoot.fact_mission_statistics where statistic_name like '%pregame%') d on a.id=d.id
     left join (select distinct id,user_id,app_id,statistic_name,statistic_value from fruitscoot.fact_mission_statistics where statistic_name like '%moves_buy%') t on a.id=t.id where a.app_id like '%2%';



drop table if exists moves_used;
create temp table moves_used as
SELECT
    m.id
	,m.mission_id
	,MEDIAN(statistic_value) over (partition by m.date,m.mission_id,m.app_id,m.app_version,m.mission_status) as 
moves_used_median
	,PERCENTILE_CONT(0.25) within group (order by statistic_value) over (partition by 
m.date,m.mission_id,m.app_id,m.app_version,m.mission_status) as moves_used_q1
	,PERCENTILE_CONT(0.75) within group (order by statistic_value) over (partition by 
m.date,m.mission_id,m.app_id,m.app_version,m.mission_status) as moves_used_q3
FROM fruitscoot.fact_mission  m
join fruitscoot.fact_mission_statistics st
    ON st.id=m.id and statistic_name='moves_used'
--join fruitscoot.fact_dau_snapshot d
  --  on d.user_key=md5(m.app_id||m.user_id) and m.date=d.date
join fruitscoot.dim_user u
    ON u.user_key=md5(m.app_id||m.user_id)
where m.app_id like '%2%' and
    m.mission_status!=0
;

drop table if exists fruitscoot.fact_mission_completion;
create table fruitscoot.fact_mission_completion as
SELECT
	s.mission_id
	,s.date
	,s.app_id
	,s.app_version
	,a.install_app_version
	,u.os
	,c.ab_experiment1
    ,c.ab_variable1
    ,c.ab_experiment2
    ,c.ab_variable2
    ,c.ab_experiment3
    ,c.ab_variable3
    ,c.ab_experiment4
    ,c.ab_variable4
	,case e.mission_status
	    when 0      then 'started'
	    when 10     then 'completed'
	    when -10    then 'failed'
	    when -20    then 'abandoned'
	    when -40    then 'suspended_abandoned'
	    	    when -30    then 'app_crashed'
	    else 'missing end status'
	 end as mission_status_end
	 ,e.ingame_boosts
	 ,e.pregame_boosts
	 ,e.buy_moves
	,max(moves_used_median) as moves_used_median
	,max(moves_used_q1) as moves_used_q1
	,max(moves_used_q3) as moves_used_q3
	,count(distinct s.id) as mission_cnt
	,count(distinct md5(s.app_id||s.user_id)) as user_cnt
FROM boosters_moves s
join install_app_version a on a.user_key=md5(s.app_id||s.user_id)
--join fruitscoot.fact_dau_snapshot d
   -- on d.user_key=md5(s.app_id||s.user_id) and s.date=d.date
join fruitscoot.dim_user u
    ON u.user_key=md5(s.app_id||s.user_id)
join boosters_moves e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_id=e.mission_id and
        md5(e.app_id||e.user_id)=md5(s.app_id||s.user_id) and
        e.mission_status!=s.mission_status
left join moves_used m
    ON m.id=e.id
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) c
      on md5(s.app_id||s.user_id) = c.user_key
where s.app_id like '%2%' and 
    s.mission_status=0
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
;


---------star completion

drop table if exists mission_star;
create temp table mission_star as
select a.*,coalesce(s.statistic_name,'Unknown') as statistic_name,cast(s.statistic_value as int) as statistic_value from fruitscoot.fact_mission a
     left join (select distinct id,app_id,user_id,statistic_name,statistic_value from fruitscoot.fact_mission_statistics where statistic_name='star') s on a.id=s.id and a.app_id like '%2%';




drop table if exists fruitscoot.star_completion;
create table fruitscoot.star_completion as
SELECT
	s.mission_id
	,s.date
	,s.app_id
	,s.app_version
	,a.install_app_version
	,u.os
	,c.ab_experiment1
    ,c.ab_variable1
    ,c.ab_experiment2
    ,c.ab_variable2
    ,c.ab_experiment3
    ,c.ab_variable3
    ,c.ab_experiment4
    ,c.ab_variable4
    , case s.mission_status when 0 then 'started' end as mission_start_status
	,case e.mission_status
	    when 0      then 'started'
	    when 10     then 'completed'
	    when -10    then 'failed'
	    when -20    then 'abandoned'
	    when -40    then 'suspended_abandoned'
	    	    when -30    then 'app_crashed'
	    else 'missing end status'
	 end as mission_status_end
	,s.statistic_name as start_name
    ,e.statistic_name as end_name
    ,coalesce(cast(s.mission_stars as varchar(10)),'N/A') as start_stars
    ,coalesce(cast(e.statistic_value as varchar(10)),'Unknown') end_stars
	,count(distinct s.id) as mission_cnt
	,count(distinct md5(s.app_id||s.user_id)) as user_cnt
FROM mission_star s
join install_app_version a on a.user_key=md5(s.app_id||s.user_id)
--join fruitscoot.fact_dau_snapshot d
  --  on d.user_key=md5(s.app_id||s.user_id) and s.date=d.date
join fruitscoot.dim_user u
    ON u.user_key=md5(s.app_id||s.user_id)
join mission_star e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_id=e.mission_id and
        md5(e.app_id||e.user_id)=md5(s.app_id||s.user_id) and
        e.mission_status!=s.mission_status
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) c
      on md5(s.app_id||s.user_id) = c.user_key
where s.app_id like '%2%' and 
    s.mission_status=0
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
;

update fruitscoot.star_completion set start_stars='N/A' where ( start_stars='' or start_stars='');

-------------- objectives remain data

drop table if exists objectives_temp;
create temp table objectives_temp as
select s.app_id,s.user_id,s.id,s.ts_pretty,mission_status,mission_id,mission_start_ts
,objective_name1
	,objective_name2
	,objective_name3
	,objective_name4
	,objective_name5
	,objective_name6
	,coalesce(objective_remain1,0) as objective_remain1
	,coalesce(objective_remain2,0) as objective_remain2
	,coalesce(objective_remain3,0) as objective_remain3
	,coalesce(objective_remain4,0) as objective_remain4
        ,coalesce(objective_remain5,0) as objective_remain5
	,coalesce(objective_remain6,0) as objective_remain6
	,coalesce(objective_amount1,0) as objective_amount1
	,coalesce(objective_amount2,0) as objective_amount2
	,coalesce(objective_amount3,0) as objective_amount3
	,coalesce(objective_amount4,0) as objective_amount4
        ,coalesce(objective_amount5,0) as objective_amount5
	,coalesce(objective_amount6,0) as objective_amount6, total_remain,total_objectives from fruitscoot.fact_mission s
left join 
(select distinct * , (coalesce(objective_amount1,0)+coalesce(objective_amount2,0)+coalesce(objective_amount3,0)+coalesce(objective_amount4,0)+coalesce(objective_amount5,0)+coalesce(objective_amount6,0)) as total_objectives,
(coalesce(objective_remain1,0)+coalesce(objective_remain2,0)+coalesce(objective_remain3,0)+coalesce(objective_remain4,0)+coalesce(objective_remain5,0)+coalesce(objective_remain6,0)) as total_remain from 
 fruitscoot.mission_objectives_split) m on s.id=m.id where s.app_id like '%2%';



drop table if exists fruitscoot.objectives;
create table fruitscoot.objectives as
SELECT
	s.mission_id
	,s.ts_pretty
	,s.app_id
	,s.app_version
	,a.install_app_version
	,u.os
	,c.ab_experiment1
    ,c.ab_variable1
    ,c.ab_experiment2
    ,c.ab_variable2
    ,c.ab_experiment3
    ,c.ab_variable3
    ,c.ab_experiment4
    ,c.ab_variable4
	,e.objective_name1
	,e.objective_name2
	,e.objective_name3
	,e.objective_name4
	,e.objective_name5
	,e.objective_name6
	,e.objective_remain1
	,e.objective_remain2
	,e.objective_remain3
	,e.objective_remain4
        ,e.objective_remain5
	,e.objective_remain6
	,e.objective_amount1
	,e.objective_amount2
	,e.objective_amount3
	,e.objective_amount4
        ,e.objective_amount5
	,e.objective_amount6
	,case e.mission_status
	    when 0      then 'started'
	    when 10     then 'completed'
	    when -10    then 'failed'
	    when -20    then 'abandoned'
	    when -40    then 'suspended_abandoned'
	    	    when -30    then 'app_crashed'
	    else 'missing end status'
	 end as mission_status_end
	,total_remain
	,total_objectives
	,count(distinct s.id) as mission_cnt
	,count(distinct md5(s.app_id||s.user_id)) as user_cnt
FROM fruitscoot.fact_mission s
join install_app_version a on a.user_key=md5(s.app_id||s.user_id)
--join fruitscoot.fact_dau_snapshot d
  --  on d.user_key=md5(s.app_id||s.user_id) and s.date=d.date
join fruitscoot.dim_user u
    ON u.user_key=md5(s.app_id||s.user_id)
join objectives_temp e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_id=e.mission_id and
        md5(e.app_id||e.user_id)=md5(s.app_id||s.user_id) and
        e.mission_status!=s.mission_status
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) c
      on md5(s.app_id||s.user_id) = c.user_key
where s.app_id like '%2%' and 
    s.mission_status=0 and (e.mission_status=-10 or e.mission_status=-20)
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35;



--------lapsed users fruitscoot us
drop table if exists fruitscoot.lapsed_users_us;
create table fruitscoot.lapsed_users_us as
SELECT COUNT(DISTINCT u.user_key) AS user_cnt,
       u.app_id,
       u.app_version,
       install_app_version,
       u.install_date,
       u.install_source,
       u.install_language,
       u.install_country,
       u.install_os,
       u.level AS level,
       u.is_payer,
       ab_experiment1,
       ab_variable1,
       ab_experiment2,
       ab_variable2,
       ab_experiment3,
       ab_variable3,
       ab_experiment4,
       ab_variable4,
       CAST(datediff (day,u.install_date,last_login_date +1) AS varchar(10)) AS lapsed_days
FROM fruitscoot.dim_user u
left join install_app_version a on u.user_key=a.user_key
  LEFT JOIN ( (SELECT DISTINCT user_key,
                      CASE
                        WHEN (ab_experiment1 = '' OR ab_experiment1 IS NULL) THEN 'None'
                        ELSE ab_experiment1
                      END,
                      CASE
                        WHEN (ab_variable1 = '' OR ab_variable1 IS NULL) THEN 'None'
                        ELSE ab_variable1
                      END,
                      CASE
                        WHEN (ab_experiment2 = '' OR ab_experiment2 IS NULL) THEN 'None'
                        ELSE ab_experiment2
                      END,
                      CASE
                        WHEN (ab_variable2 = '' OR ab_variable2 IS NULL) THEN 'None'
                        ELSE ab_variable2
                      END,
                      CASE
                        WHEN (ab_experiment3 = '' OR ab_experiment3 IS NULL) THEN 'None'
                        ELSE ab_experiment3
                      END,
                      CASE
                        WHEN (ab_variable3 = '' OR ab_variable3 IS NULL) THEN 'None'
                        ELSE ab_variable3
                      END,
                      CASE
                        WHEN (ab_experiment4 = '' OR ab_experiment4 IS NULL) THEN 'None'
                        ELSE ab_experiment4
                      END,
                      CASE
                        WHEN (ab_variable4 = '' OR ab_variable4 IS NULL) THEN 'None'
                        ELSE ab_variable4
                      END
               FROM (SELECT *,
                            ROW_NUMBER() OVER (PARTITION BY user_key ORDER BY DATE ASC) AS RANK
                     FROM fruitscoot.ab_test)
               WHERE RANK = 1)) c ON u.user_key = c.user_key
WHERE u.app_id LIKE 'fruitscoot2%'
AND   install_date <= CURRENT_DATE -1
GROUP BY 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
UNION ALL
SELECT COUNT(DISTINCT u.user_key) AS user_cnt,
       u.app_id,
       u.app_version,
       install_app_version,
       u.install_date,
       u.install_source,
       u.install_language,
       u.install_country,
       u.install_os,
       0 AS level,
       u.is_payer,
       ab_experiment1,
       ab_variable1,
       ab_experiment2,
       ab_variable2,
       ab_experiment3,
       ab_variable3,
       ab_experiment4,
       ab_variable4,
       '_0' AS lapsed_days
FROM fruitscoot.dim_user u
left join install_app_version a on u.user_key=a.user_key
  LEFT JOIN ( (SELECT DISTINCT user_key,
                      CASE
                        WHEN (ab_experiment1 = '' OR ab_experiment1 IS NULL) THEN 'None'
                        ELSE ab_experiment1
                      END,
                      CASE
                        WHEN (ab_variable1 = '' OR ab_variable1 IS NULL) THEN 'None'
                        ELSE ab_variable1
                      END,
                      CASE
                        WHEN (ab_experiment2 = '' OR ab_experiment2 IS NULL) THEN 'None'
                        ELSE ab_experiment2
                      END,
                      CASE
                        WHEN (ab_variable2 = '' OR ab_variable2 IS NULL) THEN 'None'
                        ELSE ab_variable2
                      END,
                      CASE
                        WHEN (ab_experiment3 = '' OR ab_experiment3 IS NULL) THEN 'None'
                        ELSE ab_experiment3
                      END,
                      CASE
                        WHEN (ab_variable3 = '' OR ab_variable3 IS NULL) THEN 'None'
                        ELSE ab_variable3
                      END,
                      CASE
                        WHEN (ab_experiment4 = '' OR ab_experiment4 IS NULL) THEN 'None'
                        ELSE ab_experiment4
                      END,
                      CASE
                        WHEN (ab_variable4 = '' OR ab_variable4 IS NULL) THEN 'None'
                        ELSE ab_variable4
                      END
               FROM (SELECT *,
                            ROW_NUMBER() OVER (PARTITION BY user_key ORDER BY DATE ASC) AS RANK
                     FROM fruitscoot.ab_test)
               WHERE RANK = 1)) c ON u.user_key = c.user_key
WHERE u.app_id LIKE 'fruitscoot2%'
AND   install_date <= CURRENT_DATE -1
GROUP BY 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20;

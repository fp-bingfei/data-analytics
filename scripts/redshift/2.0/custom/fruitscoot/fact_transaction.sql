-- fact_item_received table 
delete from fruitscoot.fact_item_received where date(ts_pretty)='${SCHEDULE_DATE}';
insert into fruitscoot.fact_item_received
(
    id,
    ts_pretty,
    ts,
    app_id,
    user_id,
    session_id,
    event,
    mission_unique_key,
    level,
    item_id,
    item_name,
    item_type,
    item_class,
    transaction_type,
    data_version,
    app_version,
    action,
    currency_type,
    currency_spent,
    item_received_qty
)
select 
    md5(app_id||user_id||session_id||ts) as id,
    ts_pretty,
    ts,
    app_id,
    user_id,
    session_id,
    event,
    json_extract_path_text(items_received, 'mission_unique_key') as mission_unique_key,
    json_extract_path_text(properties, 'level')::int as level,
    json_extract_path_text(items_received, 'd_item_id') as item_id,
    json_extract_path_text(items_received, 'd_item_name') as item_name,
    json_extract_path_text(items_received, 'd_item_type') as item_type,
    json_extract_path_text(items_received, 'd_item_class') as item_class,
    json_extract_path_text(properties, 'd_transaction_type') as transaction_type,
    data_version,
    json_extract_path_text(properties, 'app_version') as app_version,
    json_extract_path_text(properties, 'd_action') as action,
    json_extract_path_text(properties, 'd_currency_spent_type') as currency_type,
    nullif(json_extract_path_text(properties, 'm_currency_spent')::varchar, '')::int as currency_spent,
    sum(nullif(json_extract_path_text(items_received, 'm_item_amount')::varchar, '')::int) as item_received_qty
from
    (select 
        *
        , json_extract_array_element_text(g.exploded_items, seq.i) as items_received 
    from
        (select 
            *
            , json_extract_path_text(properties, 'c_items_received') as exploded_items 
        from fruitscoot.events 
        where event='transaction' 
            and app_id like 'fruitscoot%' 
            and date(ts_pretty)='${SCHEDULE_DATE}'
        ) as g, seq_0_to_100 as seq
    where seq.i<json_array_length(g.exploded_items)
    )
group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19;

delete from fruitscoot.fact_item_received where date(ts_pretty)>'${SCHEDULE_DATE}';

update fruitscoot.fact_item_received set level=cast(f.mission_id as integer)
from fruitscoot.fact_mission f
where fruitscoot.fact_item_received.mission_unique_key = f.mission_unique_key
        and f.mission_unique_key <> ''
        and f.mission_status in (0, 10, -10, -20);

-- Mark which item fruitscoot.mobile.prod not concern
update fruitscoot.fact_item_received set is_delete = 1
where item_id not in (
    select item_id 
    from fruitscoot.item_price
    );

-- Mark which item in package booster_moves_1/booster_moves_2/booster_moves_3
update fruitscoot.fact_item_received set is_delete = 1
from fruitscoot.item_price p
where fruitscoot.fact_item_received.item_id = p.item_id
      and fruitscoot.fact_item_received.is_delete = 0
      and fruitscoot.fact_item_received.currency_spent > p.acorns
      and p.item_id in ('booster_moves', 'booster_slingshot');


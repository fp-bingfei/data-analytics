---------------
-- Order table
---------------

CREATE TABLE IF NOT EXISTS fruitscoot.order_table
(
	id VARCHAR(100) encode lzo,
	mission_id VARCHAR(100) encode lzo,
	app_id VARCHAR(100) encode lzo,
	app_version VARCHAR(32) encode lzo,
	install_app_version VARCHAR(32) encode lzo,
	user_key VARCHAR(100) encode lzo,
	user_id VARCHAR(100) encode lzo,
	date DATE,
	ts TIMESTAMP,
	session_id VARCHAR(256) encode lzo,
	os VARCHAR(100) encode lzo,
	os_version VARCHAR(100) encode lzo,
        ip VARCHAR(100) encode lzo,
	language VARCHAR(32) encode lzo,
	mission_start_ts TIMESTAMP,
	mission_status INTEGER,
	level INTEGER,
	initial_moves BIGINT,
	points_2_stars BIGINT,
	points_3_stars BIGINT,
	frozen_fruits_rate BIGINT,
	butterfly_number BIGINT,
	moves_remaining BIGINT,
	moves_used BIGINT,
	moves_buy BIGINT,
	points_final BIGINT,
	points_initial BIGINT,
	no_possible_moves BIGINT,
	order_status VARCHAR(1000) encode lzo
);



DROP TABLE IF EXISTS TEMP_MISSION_ORDER_STATUS;
CREATE TEMPORARY TABLE TEMP_MISSION_ORDER_STATUS (id VARCHAR(50), order_status VARCHAR(1000));
INSERT INTO TEMP_MISSION_ORDER_STATUS
SELECT id,
       CASE
           WHEN order_status_10 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9|| ', ' ||order_status_10
           WHEN order_status_9 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9
           WHEN order_status_8 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8
           WHEN order_status_7 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7
           WHEN order_status_6 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6
           WHEN order_status_5 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5
           WHEN order_status_4 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4
           WHEN order_status_3 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3
           WHEN order_status_2 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2
           WHEN order_status_1 IS NOT NULL THEN order_status_1
       END AS order_status
FROM
  (SELECT DISTINCT *
   FROM
     (SELECT id,
             nth_value(order_status,1)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_1,
             nth_value(order_status,2)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_2,
             nth_value(order_status,3)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_3,
             nth_value(order_status,4)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_4,
             nth_value(order_status,5)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_5,
             nth_value(order_status,6)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_6,
             nth_value(order_status,7)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_7,
             nth_value(order_status,8)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_8,
             nth_value(order_status,9)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_9,
             nth_value(order_status,10)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_10
      FROM
        (SELECT id,
                objective_name,
                objective_name|| ':' ||objective_amount::INTEGER- objective_amount_remaining::INTEGER|| '/' ||objective_amount AS order_status
         FROM
           (SELECT DISTINCT *
            FROM fruitscoot.fact_mission_objectives
            where date(ts_pretty) >=current_date-3 and 
            app_id like 'fruitscoot2%'))
      WHERE order_status != ''));

 ---------------------------
-------- Fact_mission -----
---------------------------

DELETE FROM fruitscoot.order_table WHERE date >=current_date-3
;

INSERT INTO fruitscoot.order_table
SELECT DISTINCT *
FROM
  (SELECT m.id,
          m.mission_id,
          m.app_id,
          m.app_version,
          install_app_version,
          md5(m.app_id||m.user_id) as user_key,
          m.user_id,
          m.date,
          m.ts_pretty,
          m.session_id,
          m.os,
          m.os_version,
          m.ip,
          m.language,
          m.mission_start_ts,
          m.mission_status,
          m.level,
          initial_moves AS initial_moves,
          points_2_stars AS points_2_stars,
          points_3_stars AS points_3_stars,
          frozen_fruits_rate AS frozen_fruits_rate,
          butterfly_number AS butterfly_number,
          moves_remaining AS moves_remaining,
          moves_used AS moves_used,
          moves_buy AS moves_buy,
          points_final AS points_final,
          points_initial AS points_initial,
          no_possible_moves AS no_possible_moves,
          o.order_status
   FROM fruitscoot.fact_mission m
   join install_app_version p on md5(m.app_id||m.user_id)=p.user_key

   LEFT JOIN
     (SELECT id,
             SUM(CASE parameter_name WHEN 'initial_moves' THEN parameter_value::INTEGER ELSE NULL END) AS initial_moves,
             SUM(CASE parameter_name WHEN 'points_2_stars' THEN parameter_value::INTEGER ELSE NULL END) AS points_2_stars,
             SUM(CASE parameter_name WHEN 'points_3_stars' THEN parameter_value::INTEGER ELSE NULL END) AS points_3_stars,
             SUM(CASE parameter_name WHEN 'frozen_fruits_rate' THEN parameter_value::INTEGER ELSE NULL END) AS frozen_fruits_rate,
             SUM(CASE parameter_name WHEN 'butterfly_number' THEN parameter_value::INTEGER ELSE NULL END) AS butterfly_number
      FROM (select distinct * from fruitscoot.fact_mission_parameters
      WHERE date(ts_pretty) >=current_date-3 and 
      app_id like 'fruitscoot2%')m
      GROUP BY id) mp
   ON mp.id=m.id
   LEFT JOIN
     (SELECT id,
             SUM(CASE statistic_name WHEN 'moves_remaining' THEN statistic_value::INTEGER ELSE NULL END) AS moves_remaining,
             SUM(CASE statistic_name WHEN 'moves_used' THEN statistic_value::INTEGER ELSE NULL END) AS moves_used,
             SUM(CASE statistic_name WHEN 'moves_buy' THEN statistic_value::INTEGER ELSE NULL END) AS moves_buy,
             SUM(CASE statistic_name WHEN 'points_final' THEN statistic_value::INTEGER ELSE NULL END) AS points_final,
             SUM( CASE statistic_name WHEN 'points_initial' THEN statistic_value::INTEGER ELSE NULL END) AS points_initial,
             SUM(CASE statistic_name WHEN 'no_possible_moves' THEN statistic_value::INTEGER ELSE NULL END) AS no_possible_moves
      FROM 
      (select distinct * from fruitscoot.fact_mission_statistics
      where date(ts_pretty) >=current_date-3 and 
      app_id like 'fruitscoot2%') f
      GROUP BY id
      ) ms
   ON ms.id=m.id
   LEFT JOIN TEMP_MISSION_ORDER_STATUS o ON o.id=m.id
     WHERE m.date >=current_date-3
     and 
     m.app_id like 'fruitscoot2%'
  );

---------------
-- Order table: only keep data before first completion
---------------


CREATE TABLE IF NOT EXISTS fruitscoot.order_table_attempt
(
	id VARCHAR(100) encode lzo,
	mission_id VARCHAR(100) encode lzo,
	app_id VARCHAR(100) encode lzo,
	app_version VARCHAR(32) encode lzo,
	install_app_version varchar(32) encode lzo,
	user_key VARCHAR(100) encode lzo,
	user_id VARCHAR(100) encode lzo,
	date DATE,
	ts TIMESTAMP,
	session_id VARCHAR(256) encode lzo,
	os VARCHAR(100) encode lzo,
	os_version VARCHAR(100) encode lzo,
	ip VARCHAR(100) encode lzo,
	language VARCHAR(32) encode lzo,
	mission_start_ts TIMESTAMP,
	mission_status INTEGER,
	level INTEGER,
	initial_moves BIGINT,
	points_2_stars BIGINT,
	points_3_stars BIGINT,
	frozen_fruits_rate BIGINT,
	butterfly_number BIGINT,
	moves_remaining BIGINT,
	moves_used BIGINT,
	moves_buy BIGINT,
	points_final BIGINT,
	points_initial BIGINT,
	no_possible_moves BIGINT,
	order_status VARCHAR(1000) encode lzo,
	LevelsGroup varchar(10)  encode lzo,
	TutorialEnabled varchar(10)  encode lzo,
	EnabledGoldenFruit varchar(10)  encode lzo,
	ExtraMoves varchar(10)  encode lzo
);

DELETE FROM fruitscoot.order_table_attempt WHERE date >=current_date-3
;



INSERT INTO fruitscoot.order_table_attempt
select
    o.*,c.ab_variable1,c.ab_variable2,c.ab_variable3,c.ab_variable4
from fruitscoot.order_table o
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) c
      on o.user_key = c.user_key
--join fruitscoot.fact_level_up l
   -- on  md5(l.app_id||l.user_id)=o.user_key
   -- and l.level=cast(o.mission_id as integer)+1
where o.date >=current_date-3
  --AND 
  --o.ts<=(TIMESTAMP 'epoch' + l.ts::BIGINT * INTERVAL '0.001 Second ')
;


--------------- boosters data

delete from  fruitscoot.booster_transaction where date>=current_date-3 ;


drop table if exists booster_consumed;
create temp table booster_consumed as
select a.app_id,a.user_id, 'booster_consume' as action,trunc(ts_pretty) as date,
json_extract_path_text(properties,'app_version') as app_version,
json_extract_path_text(properties,'booster') as booster,
json_extract_path_text(properties,'level')::int as level,count(1) as booster_consumed from fruitscoot.events a
where trunc(ts_pretty)>=current_date-3 and a.event='booster_consumed' 
group by 1,2,3,4,5,6,7;
 
drop table if exists booster_received;
create temp table booster_received as 
SELECT app_id,user_id,app_version,action,trunc(ts_pretty) as date, level,item_name as booster,sum(acorns_spent) as acorns_spent,sum(item_purchased_qty) as boosters_purchased
from (select 
       app_id,
       user_id,
       ts_pretty,
       json_extract_path_text(properties,'level')::int as level,
      json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
            json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class,
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') as int) AS item_purchased_qty,
       cast(json_extract_path_text(properties,'m_currency_spent') as int)/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) as acorns_spent,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
         FROM fruitscoot.events,
     seq_0_to_10 AS seq
WHERE event='transaction' and seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and 
json_extract_path_text (properties,'d_transaction_type')='currency_spent')t
where trunc(ts_pretty)>=current_date-3 and booster not like '%move%' and item_class like 'consumable%'
group by 1,2,3,4,5,6,7;





---------- insert boosters consumed data

insert into fruitscoot.booster_transaction
select app_id,md5(app_id||user_id) as user_key,app_version,install_app_version,date,level,booster,d.ab_experiment1
      ,d.ab_variable1, d.ab_experiment2, d.ab_variable2, d.ab_experiment3, d.ab_variable3, d.ab_experiment4, d.ab_variable4, sum(booster_consumed) as booster_consumed,0 as booster_received,0 as acorns_spent
from booster_consumed b
left join install_app_version a on md5(b.app_id||b.user_id)=a.user_key
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) d
      on d.user_key = md5(b.app_id||b.user_id) where b.date>=current_date-3
      group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;
      
---------- insert boosters purchased data

insert into fruitscoot.booster_transaction
select app_id,md5(app_id||user_id) as user_key,app_version,install_app_version,date,level,booster,d.ab_experiment1
      ,d.ab_variable1, d.ab_experiment2, d.ab_variable2, d.ab_experiment3, d.ab_variable3, d.ab_experiment4, d.ab_variable4, 0 as booster_consumed,sum(boosters_purchased) as booster_received,sum(acorns_spent) as acorns_spent
from booster_received b
left join install_app_version a on md5(b.app_id||b.user_id)=a.user_key
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) d
      on d.user_key = md5(b.app_id||b.user_id) where b.date>=current_date-3
      group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;



------------- monetization_proxy data

delete from fruitscoot.iap_proxy where date>=current_date-3;

insert into fruitscoot.iap_proxy
select b.app_id,b.user_id, 'iap_proxy' as action,trunc(ts_pretty) as date, install_app_version, ab_experiment1,ab_variable1,
 ab_experiment2,ab_variable2,  ab_experiment3,ab_variable3,  ab_experiment4,ab_variable4,
json_extract_path_text(properties,'app_version') as app_version,
json_extract_path_text(properties,'proxy') as proxy,
json_extract_path_text(properties,'last_played_mission')::int as level,count(1) as proxy_count from fruitscoot.events b
left join install_app_version a on md5(b.app_id||b.user_id)=a.user_key
left join 
      (
         (SELECT distinct user_key,
                 case when (ab_experiment1='' or ab_experiment1 is null) then 'None' else ab_experiment1 end,
                 case when (ab_variable1='' or ab_variable1 is null) then 'None' else ab_variable1 end,
                 case when (ab_experiment2='' or ab_experiment2 is null) then 'None' else ab_experiment2 end,
                 case when (ab_variable2='' or ab_variable2 is null) then 'None' else ab_variable2 end,
                 case when (ab_experiment3='' or ab_experiment3 is null) then 'None' else ab_experiment3 end,
                 case when (ab_variable3='' or ab_variable3 is null) then 'None' else ab_variable3 end,
                 case when (ab_experiment4='' or ab_experiment4 is null) then 'None' else ab_experiment4 end,
                 case when (ab_variable4='' or ab_variable4 is null) then 'None' else ab_variable4 end
          
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key
                                       ORDER BY date ASC) AS rank
            FROM fruitscoot.ab_test)
            WHERE rank=1)
     ) d on d.user_key = md5(b.app_id||b.user_id)
where  b.event='iap_proxy' and trunc(b.ts_pretty)>=current_date-3
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;


grant usage on schema fruitscoot to fsus_pm;
grant select on all tables in  schema fruitscoot to fsus_pm;

-----------------------------------
--cusotm 2.0
-----------------------------------

delete from custom.agg_funnel_by_date where d_cohort_date >= (select start_date from custom.init_start_date);

insert into custom.agg_funnel_by_date
select a.d_cohort_date      
      ,a.d_app_id           
      ,a.d_browser          
      ,a.d_browser_version  
      ,coalesce(c.country,'Unknown') as country
      ,a.d_install_source   
      ,a.d_install_date     
      ,a.d_lang             
      ,a.d_os               
      ,a.d_os_version       
      ,json_extract_path_text(a.d_c1,'value') as d_c1
      ,json_extract_path_text(a.d_c2,'value') as d_c2
      ,coalesce(json_extract_path_text(a.d_c3,'value'),'not complete') as d_c3
      ,json_extract_path_text(a.d_c4,'value') as d_c4
      ,json_extract_path_text(a.d_c5,'value') as d_c5
      ,sum(nvl(nullif(json_extract_path_text(a.m1,'value'), '')::numeric, 0)) as m1               
      ,sum(nvl(nullif(json_extract_path_text(a.m2,'value'), '')::numeric, 0)) as m2              
      ,sum(nvl(nullif(json_extract_path_text(a.m3,'value'), '')::numeric, 0)) as m3              
      ,sum(nvl(nullif(json_extract_path_text(a.m4,'value'), '')::numeric, 0)) as m4              
      ,sum(nvl(nullif(json_extract_path_text(a.m5,'value'), '')::numeric, 0)) as m5              
      ,sum(coalesce(time_spent_to_done,'0')::bigint) as time_spent_to_done
      ,sum(user_cnt) as user_cnt
from  custom.agg_funnel a
left join custom.dim_country c
on a.d_country_code = c.country_code
where a.d_cohort_date >= (select start_date from custom.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;



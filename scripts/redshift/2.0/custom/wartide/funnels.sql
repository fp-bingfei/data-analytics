DELETE FROM wartide.player_level
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.player_level
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,level
    ,coalesce(app_version, 'CB') as app_version
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event = 'player_level' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;


DELETE FROM wartide.player_level_funnel
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO wartide.player_level_funnel
(
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,cnt
)
SELECT 
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,count(distinct user_id) as cnt
FROM wartide.player_level
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
GROUP BY 1,2,3,4,5,6,7,8
;

DELETE FROM wartide.tutorial
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.tutorial
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,ts
    ,level
    ,app_version
    ,tutorial_type
    ,tutorial_step
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,ts
    ,level
    ,coalesce(app_version, 'CB') as app_version
    ,event as tutorial_type
    ,json_extract_path_text(d_c1, 'value') as tutorial_step
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event in ('tutorial_action_completed','tutorial_step_completed') and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;


DELETE FROM wartide.tutorial_funnel
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO wartide.tutorial_funnel
(
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,tutorial_type
    ,tutorial_step
    ,min_ts
    ,cnt
)
SELECT
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,tutorial_type
    ,tutorial_step
    ,min(ts) as min_ts
    ,count(1) as cnt
FROM wartide.tutorial
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
GROUP BY 1,2,3,4,5,6,7,8,9,10
;


DELETE FROM wartide.battle_initiated
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.battle_initiated
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,encounter_name
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,level
    ,coalesce(app_version, 'CB') as app_version
    ,json_extract_path_text(d_c1, 'value') as encounter_name
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event = 'battle_initiated' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM wartide.battle_result
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.battle_result
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,encounter_name
    ,encounter_result
    ,time_length
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,level
    ,coalesce(app_version, 'CB') as app_version
    ,json_extract_path_text(d_c1, 'value') as encounter_name
    ,json_extract_path_text(d_c2, 'value') as encounter_result
    ,json_extract_path_text(m1, 'value') as time_length
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event = 'battle_result' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM wartide.battle_funnel
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.battle_funnel
(
    app_id
--    ,date
    ,install_date
--    ,country
    ,lang
    ,os
--    ,level
    ,app_version
    ,encounter_name
    ,cnt
    ,victory_cnt
    ,defeat_cnt
    ,avg_time
)
SELECT
    t1.app_id
--    ,t1.date
    ,t1.install_date
--    ,t1.country
    ,t1.lang
    ,t1.os
--    ,t1.level
    ,t1.app_version
    ,t1.encounter_name
    ,t1.cnt
    ,t2.victory_cnt
    ,t2.defeat_cnt
    ,t2.avg_time
FROM 
    (SELECT 
        app_id
--        ,date
        ,install_date
--        ,country
        ,lang
        ,os
--        ,level
        ,app_version
        ,encounter_name
        ,count(1) as cnt
    FROM wartide.battle_initiated
    -- WHERE date >=
    --     (
    --         SELECT start_date
    --         FROM   kpi_processed.init_start_date
    --     )
    group by 1,2,3,4,5,6) t1 --,7,8,9) t1
LEFT JOIN 
    (SELECT 
        app_id
        ,install_date
--        ,country
        ,lang
        ,os
--        ,level
        ,app_version
        ,encounter_name
        ,sum(case when encounter_result = 'combatresult_victory' then 1 else 0 end) as victory_cnt
        ,sum(case when encounter_result = 'combatresult_defeat' then 1 else 0 end) as defeat_cnt
        ,sum(time_length)/count(user_id) as avg_time
    FROM wartide.battle_result
    -- WHERE date >=
    --     (
    --         SELECT start_date
    --         FROM   kpi_processed.init_start_date
    --     )
    group by 1,2,3,4,5,6) t2 --,7,8,9) t2
ON t1.app_id = t2.app_id
AND t1.install_date = t2.install_date
--AND t1.country = t2.country
AND t1.lang = t2.lang
AND t1.os = t2.os
AND t1.app_version = t2.app_version
AND t1.encounter_name = t2.encounter_name
;


DELETE FROM wartide.quest_presented
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.quest_presented
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,quest_id
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,level
    ,coalesce(app_version, 'CB') as app_version
    ,json_extract_path_text(d_c1, 'value') as quest_id
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event = 'quest_presented' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM wartide.quest_completed
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.quest_completed
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,quest_id
    ,time_length
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,level
    ,coalesce(app_version, 'CB') as app_version
    ,json_extract_path_text(d_c1, 'value') as encounter_name
    ,json_extract_path_text(m1, 'value') as time_length
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event = 'quest_completed' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM wartide.quest_funnel
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.quest_funnel
(
    app_id
--    ,date
    ,install_date
--    ,country
    ,lang
    ,os
--    ,level
    ,app_version
    ,quest_id
    ,start_cnt
    ,finish_cnt
    ,avg_time
)
SELECT
    t1.app_id
--    ,t1.date
    ,t1.install_date
--    ,t1.country
    ,t1.lang
    ,t1.os
--    ,t1.level
    ,t1.app_version
    ,t1.quest_id
    ,t1.start_cnt
    ,t2.finish_cnt
    ,t2.avg_time
FROM 
    (SELECT 
        app_id
--        ,date
        ,install_date
--        ,country
        ,lang
        ,os
--        ,level
        ,app_version
        ,quest_id
        ,count(1) as start_cnt
    FROM wartide.quest_presented
    -- WHERE date >=
    --     (
    --         SELECT start_date
    --         FROM   kpi_processed.init_start_date
    --     )
    group by 1,2,3,4,5,6) t1 --,7,8,9) t1
LEFT JOIN 
    (SELECT 
        app_id
        ,install_date
--        ,country
        ,lang
        ,os
--        ,level
        ,app_version
        ,quest_id
        ,count(1) as finish_cnt
        ,sum(time_length)/count(1) as avg_time
    FROM wartide.quest_completed
    -- WHERE date >=
    --     (
    --         SELECT start_date
    --         FROM   kpi_processed.init_start_date
    --     )
    group by 1,2,3,4,5,6) t2 --,7,8,9) t2
ON t1.app_id = t2.app_id
AND t1.install_date = t2.install_date
--AND t1.country = t2.country
AND t1.lang = t2.lang
AND t1.os = t2.os
AND t1.app_version = t2.app_version
AND t1.quest_id = t2.quest_id
;


DELETE FROM wartide.compaign_complete
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;
--Questions: country? language?
INSERT INTO wartide.compaign_complete
(
    app_id
    ,user_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,campaign_id
    ,score
    ,star
)
SELECT DISTINCT
    app_id
    ,user_id
    ,coalesce(trunc(ts_pretty), '2016-01-01') as date
    ,coalesce(trunc(install_ts), '2016-01-01') as install_date
    ,country_code as country
    ,coalesce(lang, 'EN') as lang
    ,coalesce(os, 'Android') as os
    ,level
    ,coalesce(app_version, 'CB') as app_version
    ,json_extract_path_text(d_c1, 'value') as campaign_id
    ,json_extract_path_text(m1, 'value')::INTEGER as score
    ,json_extract_path_text(m2, 'value')::INTEGER as star
FROM wartide.catchall
WHERE app_id = 'wartide.global.prod' and event = 'campaign_complete' and user_id is not null
-- and trunc(ts_pretty) >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;


DELETE FROM wartide.compaign_funnel
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO wartide.compaign_funnel
(
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,campaign_id
    ,score
    ,star
    ,cnt
)
SELECT
    app_id
    ,date
    ,install_date
    ,country
    ,lang
    ,os
    ,level
    ,app_version
    ,campaign_id
    ,score
    ,star
    ,count(1) as cnt
FROM wartide.compaign_complete
-- WHERE date >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
GROUP BY 1,2,3,4,5,6,7,8,9,10,11
;
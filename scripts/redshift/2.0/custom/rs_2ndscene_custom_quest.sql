delete from custom_processed.mid_funnel  where date(start_date) >= (select start_date from custom_processed.init_start_date);


insert into custom_processed.mid_funnel
SELECT
    l.user_id::integer as user_id
    ,l.quest_id AS funnel_id
    ,r.task_id AS step
    ,r.action AS action
    ,l.ts AS start_date
    ,CASE WHEN (r.action = 'finish_quest' OR r.action = 'finish_task') THEN r.ts ELSE NULL END AS end_date
    ,CASE WHEN r.action = 'skip_task' THEN r.ts ELSE NULL END AS skip_date
    ,coalesce(datediff(s,l.ts,r.ts),0) AS length_sec
    ,l.browser AS browser
    ,l.browser_version AS browser_version
    ,l.country_code AS country_code
    ,l.install_source AS install_source
    ,date(l.install_ts) AS install_date
    ,l.lang AS lang
    ,l.os AS os
    ,l.os_version AS os_version
    ,l.scene
    ,CAST(coalesce(r.rc_in,'0') AS numeric) AS rc_in
    ,CAST(coalesce(r.rc_out,'0') AS numeric) AS rc_out
    ,CAST(coalesce(r.rc_bal,'0') AS numeric) AS rc_bal
    ,CAST(coalesce(r.coins_in,'0') AS numeric) AS coins_in
    ,CAST(coalesce(r.coins_out,'0') AS numeric) AS coins_out
    ,CAST(coalesce(r.coins_bal,'0') AS numeric) AS coins_bal
    ,'quest' AS event
    ,l.app_id
FROM
    (
        SELECT user_id, quest_id, task_id, action, ts, browser, browser_version,
               country_code,scene, install_source, install_ts, lang, os, os_version,
               rc_in, rc_out, rc_bal, coins_in, coins_out, coins_bal, app_id
        FROM
            (
                 SELECT user_id
                      ,ts_pretty as ts
                      ,json_extract_path_text(properties,'os') as os
                      ,json_extract_path_text(properties,'os_version') as os_version
                      ,json_extract_path_text (properties,'browser') as browser
                      ,json_extract_path_text (properties,'browser_version') as browser_version
                      ,json_extract_path_text (properties,'country_code') as country_code
                      ,json_extract_path_text (properties,'scene') as scene                     
                      ,json_extract_path_text (properties,'install_source') as install_source
                      ,json_extract_path_text (properties,'install_ts_pretty') as install_ts
                      ,json_extract_path_text(properties,'quest_id')  as quest_id
                      ,json_extract_path_text(properties,'task_id')  as task_id
                      ,json_extract_path_text(properties,'action')  as action
                      ,json_extract_path_text(properties,'lang')  as lang
                      ,nvl(nullif(json_extract_path_text(properties,'rc_in'),''),'0') as rc_in
                      ,nvl(nullif(json_extract_path_text(properties,'rc_out'),''),'0')  as rc_out
                      ,nvl(nullif(json_extract_path_text(properties,'rc_bal'),''),'0')  as rc_bal
                      ,nvl(nullif(json_extract_path_text(properties,'coins_in'),''),'0')  as coins_in
                      ,nvl(nullif(json_extract_path_text(properties,'coins_out'),''),'0')  as coins_out
                      ,nvl(nullif(json_extract_path_text(properties,'coins_bal'),''),'0')  as coins_bal
                      ,app_id
                      ,row_number() over(partition by app_id, user_id, quest_id, task_id, action, scene order by ts_pretty DESC) rnum
                FROM custom.events 
                WHERE date(ts_pretty) >= (select start_date from custom_processed.init_start_date)
                and event = 'quest'
                AND json_extract_path_text(properties,'action') = 'start_quest'
            ) t2
        WHERE rnum = 1
    ) l
    LEFT OUTER JOIN
    (
        SELECT user_id, quest_id, task_id, action, ts, browser, browser_version,
               country_code,scene, install_source, install_ts, lang, os, os_version,
               rc_in, rc_out, rc_bal, coins_in, coins_out, coins_bal, app_id
        FROM
            (
                SELECT user_id
                      ,ts_pretty as ts
                      ,json_extract_path_text(properties,'os') as os
                      ,json_extract_path_text(properties,'os_version') as os_version
                      ,json_extract_path_text (properties,'browser') as browser
                      ,json_extract_path_text (properties,'browser_version') as browser_version
                      ,json_extract_path_text (properties,'country_code') as country_code
                      ,json_extract_path_text (properties,'scene') as scene
                      ,json_extract_path_text (properties,'install_source') as install_source
                      ,json_extract_path_text (properties,'install_ts_pretty') as install_ts
                      ,json_extract_path_text(properties,'quest_id')  as quest_id
                      ,json_extract_path_text(properties,'task_id')  as task_id
                      ,json_extract_path_text(properties,'action')  as action
                      ,json_extract_path_text(properties,'lang')  as lang
                      ,nvl(nullif(json_extract_path_text(properties,'rc_in'),''),'0') as rc_in
                      ,nvl(nullif(json_extract_path_text(properties,'rc_out'),''),'0')  as rc_out
                      ,nvl(nullif(json_extract_path_text(properties,'rc_bal'),''),'0')  as rc_bal
                      ,nvl(nullif(json_extract_path_text(properties,'coins_in'),''),'0')  as coins_in
                      ,nvl(nullif(json_extract_path_text(properties,'coins_out'),''),'0')  as coins_out
                      ,nvl(nullif(json_extract_path_text(properties,'coins_bal'),''),'0')  as coins_bal
                      ,app_id
                      ,row_number() over(partition by app_id, user_id, quest_id, task_id, action,scene order by ts_pretty DESC) rnum
                FROM custom.events 
                WHERE date(ts_pretty) >= (select start_date from custom_processed.init_start_date)
                 AND (json_extract_path_text(properties,'action') = 'finish_quest' OR json_extract_path_text(properties,'action') = 'finish_task' OR json_extract_path_text(properties,'action') = 'skip_task')
                 and event = 'quest'
            ) t3
        WHERE rnum = 1
    ) r
    ON (l.app_id = r.app_id AND l.quest_id = r.quest_id AND l.user_id = r.user_id and l.scene=r.scene)
;




create temp table tmp_mid_funnel_update
as
SELECT
     m.user_id AS user_id
    ,m.funnel_id AS funnel_id
    ,q.task_id AS step
    ,q.action AS action
    ,m.start_date AS start_date
    ,CASE WHEN (q.action = 'finish_quest' OR q.action = 'finish_task') THEN q.ts ELSE NULL END AS end_date
    ,CASE WHEN q.action = 'skip_task' THEN q.ts ELSE NULL END AS skip_date
    ,coalesce(datediff(s,m.start_date,q.ts),0) AS length_sec
    ,m.browser AS browser
    ,m.browser_version AS browser_version
    ,m.country_code AS country_code
    ,m.install_source AS install_source
    ,m.install_date AS install_date
    ,m.lang AS lang
    ,m.os AS os
    ,m.os_version AS os_version
    ,m.scene
    ,CAST(coalesce(q.rc_in,'0') AS numeric) AS rc_in
    ,CAST(coalesce(q.rc_out,'0') AS numeric) AS rc_out
    ,CAST(coalesce(q.rc_bal,'0') AS numeric) AS rc_bal
    ,CAST(coalesce(q.coins_in,'0') AS numeric) AS coins_in
    ,CAST(coalesce(q.coins_out,'0') AS numeric) AS coins_out
    ,CAST(coalesce(q.coins_bal,'0') AS numeric) AS coins_bal
    ,m.event AS event
    ,m.app_id
FROM
    (
        SELECT * 
        FROM custom_processed.mid_funnel 
        WHERE start_date IS NOT NULL AND end_date IS NULL AND skip_date IS NULL
            AND date(start_date) > (select start_date -20 from custom_processed.init_start_date) AND date(start_date) <= (select start_date from custom_processed.init_start_date)
    ) m
    LEFT OUTER JOIN
    (
        SELECT user_id::int as user_id, quest_id, task_id, action, ts, browser, browser_version,
               country_code,scene, install_source, install_ts, lang, os, os_version,
               rc_in, rc_out, rc_bal, coins_in, coins_out, coins_bal, app_id
        FROM
            (
                SELECT user_id
                      ,ts_pretty as ts
                      ,json_extract_path_text(properties,'os') as os
                      ,json_extract_path_text(properties,'os_version') as os_version
                      ,json_extract_path_text (properties,'browser') as browser
                      ,json_extract_path_text (properties,'browser_version') as browser_version
                      ,json_extract_path_text (properties,'country_code') as country_code
                      ,json_extract_path_text (properties,'scene') as scene
                      ,json_extract_path_text (properties,'install_source') as install_source
                      ,json_extract_path_text (properties,'install_ts_pretty') as install_ts
                      ,json_extract_path_text(properties,'quest_id')  as quest_id
                      ,json_extract_path_text(properties,'task_id')  as task_id
                      ,json_extract_path_text(properties,'action')  as action
                      ,json_extract_path_text(properties,'lang')  as lang
                      ,nvl(nullif(json_extract_path_text(properties,'rc_in'),''),'0') as rc_in
                      ,nvl(nullif(json_extract_path_text(properties,'rc_out'),''),'0')  as rc_out
                      ,nvl(nullif(json_extract_path_text(properties,'rc_bal'),''),'0')  as rc_bal
                      ,nvl(nullif(json_extract_path_text(properties,'coins_in'),''),'0')  as coins_in
                      ,nvl(nullif(json_extract_path_text(properties,'coins_out'),''),'0')  as coins_out
                      ,nvl(nullif(json_extract_path_text(properties,'coins_bal'),''),'0')  as coins_bal
                      ,app_id
                      ,row_number() over(partition by app_id, user_id, quest_id,task_id, action,scene order by ts_pretty DESC) rnum
                FROM custom.events 
                WHERE date(ts_pretty) >= (select start_date from custom_processed.init_start_date) 
                AND (json_extract_path_text(properties,'action') = 'finish_quest' OR json_extract_path_text(properties,'action') = 'finish_task' OR json_extract_path_text(properties,'action') = 'skip_task')
                and event = 'quest'
            ) t1
        WHERE rnum = 1
    ) q
    ON (m.app_id = q.app_id AND m.funnel_id = q.quest_id AND m.user_id = q.user_id and m.scene=q.scene)
;

update custom_processed.mid_funnel
set end_date = t.end_date
    ,skip_date = t.skip_date
    ,length_sec = t.length_sec
    ,rc_in = t.rc_in
    ,rc_out = t.rc_out
    ,rc_bal = t.rc_bal
    ,coins_in = t.coins_in
    ,coins_out = t.coins_out
    ,coins_bal = t.coins_bal
from tmp_mid_funnel_update t
where custom_processed.mid_funnel.app_id=t.app_id
and custom_processed.mid_funnel.funnel_id = t.funnel_id
and custom_processed.mid_funnel.user_id = t.user_id
and custom_processed.mid_funnel.scene = t.scene; 

delete from custom_processed.agg_funnel where d_cohort_date >= (select start_date from custom_processed.init_start_date); 
insert into custom_processed.agg_funnel
select date(start_date) as d_cohort_date
      ,app_id as d_app_id
      ,browser            as d_browser 
      ,browser_version    as d_browser_version 
      ,country_code       as d_country_code 
      ,install_source     as d_install_source 
      ,install_date       as d_install_date 
      ,lang               as d_lang 
      ,os                 as d_os
      ,os_version         as d_os_version
      ,scene              as d_scene 
      ,'{\"key\":\"funnel_id\",\"value\":\"'||funnel_id||'\"}' as d_c1 
      ,'{\"key\":\"step\",\"value\":\"'||step||'\"}' as d_c2 
      ,'{\"key\":\"action\",\"value\":\"'||action||'\"}' as d_c3 
      ,null as d_c4 
      ,null as d_c5 
      ,'{\"key\":\"rc_in\",\"value\":\"'||sum(rc_in)||'\"}' as m1 
      ,'{\"key\":\"rc_out\",\"value\":\"'||sum(rc_out)||'\"}' as m2 
      ,'{\"key\":\"coins_in\",\"value\":\"'||sum(coins_in)||'\"}' as m3 
      ,'{\"key\":\"coins_out\",\"value\":\"'||sum(coins_out)||'\"}' as m4 
      ,null as m5
      ,SUM(length_sec) AS time_spent_to_done
      ,count(1) AS user_cnt
from custom_processed.mid_funnel
where date(start_date) >=  (select start_date from custom_processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14; 

-------- update agg_funnel

update custom_processed.agg_funnel set d_scene='Main' where d_scene='1';


-----------------------------------
--cusotm 2.0
-----------------------------------

delete from custom_processed.agg_funnel_by_date where d_cohort_date >= (select start_date from custom_processed.init_start_date);

insert into custom_processed.agg_funnel_by_date
select a.d_cohort_date      
      ,a.d_app_id           
      ,a.d_browser          
      ,a.d_browser_version  
      ,coalesce(c.country,'Unknown') as country
      ,a.d_install_source   
      ,a.d_install_date     
      ,a.d_lang             
      ,a.d_os               
      ,a.d_os_version   
      ,a.d_scene    
      ,json_extract_path_text(a.d_c1,'value') as d_c1
      ,json_extract_path_text(a.d_c2,'value') as d_c2
      ,coalesce(json_extract_path_text(a.d_c3,'value'),'not complete') as d_c3
      ,json_extract_path_text(a.d_c4,'value') as d_c4
      ,json_extract_path_text(a.d_c5,'value') as d_c5
      ,sum(nvl(nullif(json_extract_path_text(a.m1,'value'), '')::numeric, 0)) as m1               
      ,sum(nvl(nullif(json_extract_path_text(a.m2,'value'), '')::numeric, 0)) as m2              
      ,sum(nvl(nullif(json_extract_path_text(a.m3,'value'), '')::numeric, 0)) as m3              
      ,sum(nvl(nullif(json_extract_path_text(a.m4,'value'), '')::numeric, 0)) as m4              
      ,sum(nvl(nullif(json_extract_path_text(a.m5,'value'), '')::numeric, 0)) as m5              
      ,sum(coalesce(time_spent_to_done,'0')::bigint) as time_spent_to_done
      ,sum(user_cnt) as user_cnt
from  custom_processed.agg_funnel a
left join custom.dim_country c
on a.d_country_code = c.country_code
where a.d_cohort_date >= (select start_date from custom_processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;



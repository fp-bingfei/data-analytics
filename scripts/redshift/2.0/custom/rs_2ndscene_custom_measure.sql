-------------------------------
-- custom 2.0
-- for royal story 2nd scene measure

-------------------------------

delete from custom_processed.agg_measure where date>= (
                    select start_date 
                    from   custom_processed.init_start_date
                  ) and app_id like 'royal%';

--- insert rc_transaction data

insert into custom_processed.agg_measure
select 
   event      
   ,app_id 
   ,MD5(app_id||user_id) as user_key
   ,user_id::varchar(100)
   ,json_extract_path_text(properties,'facebook_id')::varchar(100) as facebook_id      
   ,ts_pretty::date as date 
   ,json_extract_path_text(properties,'browser')::varchar(100) as browser        
   ,json_extract_path_text(properties,'browser_version')::varchar(100) as browser_version
   ,json_extract_path_text(properties,'country_code')::varchar(100) country_code  
   ,json_extract_path_text(properties,'install_source')::varchar(100) as install_source
   ,json_extract_path_text(properties,'install_ts_pretty')::date as install_date
   ,json_extract_path_text(properties,'ip') as ip
   ,json_extract_path_text(properties,'lang')::varchar(100) as d_lang           
   ,json_extract_path_text(properties,'level')::varchar(100) as d_level          
   ,json_extract_path_text(properties,'os') as os            
   ,json_extract_path_text(properties,'os_version') as os_version
   ,json_extract_path_text(properties,'scene')::varchar(32) as d_scene
   ,NULL as d_fb_source --d_df_source   
   ,('{\"key\":\"action\",\"value\":\"' || json_extract_path_text(properties,'action') || '\"}')::varchar(2048) as d_c1
   ,('{\"key\":\"action_detail\",\"value\":\"' || json_extract_path_text(properties,'action_detail') || '\"}')::varchar(2048) as d_c2
   ,('{\"key\":\"location\",\"value\":\"' || json_extract_path_text(properties,'location') || '\"}')::varchar(2048) as d_c3
   ,NULL as d_c4
   ,NULL as d_c5
   ,('{\"key\":\"rc_bal\",\"value\":\"' || json_extract_path_text(properties,'rc_bal') || '\"}')::varchar(2048) as m1   
   ,('{\"key\":\"rc_in\",\"value\":\"' || json_extract_path_text(properties,'rc_in') || '\"}')::varchar(2048) as m2            
   ,('{\"key\":\"rc_out\",\"value\":\"' || json_extract_path_text(properties,'rc_out') || '\"}')::varchar(2048) as m3              
   ,NULL as m4              
   ,NULL as m5              
   ,count(1) as event_cnt
from custom.events
where date(ts_pretty)>= (
                    select start_date 
                    from   custom_processed.init_start_date
                  ) 
and app_id like 'royal%' and event='rc_transaction'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28;


---- insert coins_transaction data

insert into custom_processed.agg_measure
select 
   event      
   ,app_id 
   ,MD5(app_id||user_id) as user_key
   ,user_id::varchar(100)
   ,json_extract_path_text(properties,'facebook_id')::varchar(100) as facebook_id      
   ,ts_pretty::date as date 
   ,json_extract_path_text(properties,'browser')::varchar(100) as browser        
   ,json_extract_path_text(properties,'browser_version')::varchar(100) as browser_version
   ,json_extract_path_text(properties,'country_code')::varchar(100) country_code  
   ,json_extract_path_text(properties,'install_source')::varchar(100) as install_source
   ,json_extract_path_text(properties,'install_ts_pretty')::date as install_date
   ,json_extract_path_text(properties,'ip') as ip
   ,json_extract_path_text(properties,'lang')::varchar(100) as d_lang           
   ,json_extract_path_text(properties,'level')::varchar(100) as d_level          
   ,json_extract_path_text(properties,'os') as os            
   ,json_extract_path_text(properties,'os_version') as os_version
   ,json_extract_path_text(properties,'scene')::varchar(32) as d_scene
   ,NULL as d_fb_source --d_df_source   
   ,('{\"key\":\"action\",\"value\":\"' || json_extract_path_text(properties,'action') || '\"}')::varchar(2048) as d_c1
   ,('{\"key\":\"action_detail\",\"value\":\"' || json_extract_path_text(properties,'action_detail') || '\"}')::varchar(2048) as d_c2
   ,('{\"key\":\"location\",\"value\":\"' || json_extract_path_text(properties,'location') || '\"}')::varchar(2048) as d_c3
   ,NULL as d_c4
   ,NULL as d_c5
   ,('{\"key\":\"coins_bal\",\"value\":\"' || json_extract_path_text(properties,'coins_bal') || '\"}')::varchar(2048) as m1   
   ,('{\"key\":\"coins_in\",\"value\":\"' || json_extract_path_text(properties,'coins_in') || '\"}')::varchar(2048) as m2            
   ,('{\"key\":\"coins_out\",\"value\":\"' || json_extract_path_text(properties,'coins_out') || '\"}')::varchar(2048) as m3               
   ,NULL as m4              
   ,NULL as m5              
   ,count(1) as event_cnt
from custom.events
where date(ts_pretty)>= (
                    select start_date 
                    from   custom_processed.init_start_date
                  ) 
and app_id like 'royal%' and event='coins_transaction'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28;


--- insert item_transacion data

insert into custom_processed.agg_measure
select 
   event      
   ,app_id 
   ,MD5(app_id||user_id) as user_key
   ,user_id::varchar(100)
   ,json_extract_path_text(properties,'facebook_id')::varchar(100) as facebook_id      
   ,ts_pretty::date as date 
   ,json_extract_path_text(properties,'browser')::varchar(100) as browser        
   ,json_extract_path_text(properties,'browser_version')::varchar(100) as browser_version
   ,json_extract_path_text(properties,'country_code')::varchar(100) country_code  
   ,json_extract_path_text(properties,'install_source')::varchar(100) as install_source
   ,json_extract_path_text(properties,'install_ts_pretty')::date as install_date
   ,json_extract_path_text(properties,'ip') as ip
   ,json_extract_path_text(properties,'lang')::varchar(100) as d_lang           
   ,json_extract_path_text(properties,'level')::varchar(100) as d_level          
   ,json_extract_path_text(properties,'os') as os            
   ,json_extract_path_text(properties,'os_version') as os_version
   ,json_extract_path_text(properties,'scene')::varchar(32) as d_scene
   ,NULL as d_fb_source --d_df_source   
   ,('{\"key\":\"action\",\"value\":\"' || json_extract_path_text(properties,'action') || '\"}')::varchar(2048) as d_c1
   ,('{\"key\":\"item_type\",\"value\":\"' || json_extract_path_text(properties,'item_type') || '\"}')::varchar(2048) as d_c2
   ,('{\"key\":\"item_name\",\"value\":\"' || json_extract_path_text(properties,'item_name') || '\"}')::varchar(2048) as d_c3
   ,('{\"key\":\"item_id\",\"value\":\"' || json_extract_path_text(properties,'item_id') || '\"}')::varchar(2048) as d_c4
   ,NULL as d_c5
   ,NULL as m1   
   ,('{\"key\":\"item_in\",\"value\":\"' || json_extract_path_text(properties,'item_in') || '\"}')::varchar(2048) as m2            
   ,('{\"key\":\"item_out\",\"value\":\"' || json_extract_path_text(properties,'item_out') || '\"}')::varchar(2048) as m3               
   ,NULL as m4              
   ,NULL as m5                
   ,count(1) as event_cnt
from custom.events
where date(ts_pretty)>= (
                    select start_date 
                    from   custom_processed.init_start_date
                  ) 
                and app_id like 'royal%' and event='item_transaction'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28;

--updating agg_measure

update custom_processed.agg_measure set d_scene='Main' where d_scene='1';


delete from custom_processed.agg_measure_by_date  where date>= (select start_date from custom_processed.init_start_date) and app_id like 'royal%';


insert into custom_processed.agg_measure_by_date 
select 
    a.event_name       
   ,a.app_id           
   ,a.date::date
   ,a.d_browser        
   ,a.d_browser_version
   ,coalesce(c.country,'Unknown') as country  
   ,a.d_install_source 
   ,a.d_install_date::date
   ,a.d_lang           
   ,a.d_level          
   ,a.d_os             
   ,a.d_os_version 
   ,a.d_scene
   ,a.d_fb_source      
   ,json_extract_path_text(a.d_c1,'value') as d_c1
   ,json_extract_path_text(a.d_c2,'value') as d_c2
   ,json_extract_path_text(a.d_c3,'value') as d_c3
   ,json_extract_path_text(a.d_c4,'value') as d_c4
   ,json_extract_path_text(a.d_c5,'value') as d_c5
   ,sum(nvl(nullif(json_extract_path_text(a.m1,'value'), '')::numeric, 0)) as m1               
   ,sum(nvl(nullif(json_extract_path_text(a.m2,'value'), '')::numeric, 0)) as m2              
   ,sum(nvl(nullif(json_extract_path_text(a.m3,'value'), '')::numeric, 0)) as m3              
   ,sum(nvl(nullif(json_extract_path_text(a.m4,'value'), '')::numeric, 0)) as m4              
   ,sum(nvl(nullif(json_extract_path_text(a.m5,'value'), '')::numeric, 0)) as m5              
   ,sum(a.event_cnt) as event_cnt
   ,count(distinct a.user_key) as user_cnt
from custom_processed.agg_measure a
left join custom.dim_country c   
on a.d_country_code = c.country_code
where app_id like 'royal%' and date >= (select start_date from custom_processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
;


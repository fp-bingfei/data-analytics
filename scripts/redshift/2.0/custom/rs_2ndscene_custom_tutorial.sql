-- tutorial report

delete from custom_processed.agg_tutorial
where install_date >current_date-30 ;

--- temp table
create temp table tmp_tutorial_step as
select MD5(e.app_id||e.user_id) as user_key
        ,e.app_id
        ,e.user_id
        ,json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value') as step
        ,CAST(CASE WHEN json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value') = '' THEN '0' ELSE json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value') END AS integer) as step_number
        ,json_extract_path_text(properties,'abid') as tutorial_type
        ,json_extract_path_text(properties,'scene') as scene
        ,json_extract_path_text(properties,'level')::int as level
        ,u.install_ts
        ,min(ts_pretty) as min_ts
from custom.events e
join custom_processed.fact_new_user u on e.app_id=u.app_id and e.user_id = u.user_id
where event = 'tutorial' and e.app_id like 'royal%'
      and      u.install_date > current_date-30
      and json_extract_path_text(properties,'scene')='2'
group by 1,2,3,4,5,6,7,8,9;

--select current_date-30

---- temp table to remove duplicates if any
create temp table tmp_tutorial_step_first_time as
select distinct * from (
select *, first_value(min_ts) over (partition by user_key order by step_number rows between unbounded preceding and unbounded following) as pre_min_ts
from tmp_tutorial_step);


----- INSERT INTO agg_tutorial

insert into custom_processed.agg_tutorial
select
     u.install_date
    ,u.app_id
    ,u.app_version
    ,u.os
    ,u.os_version
    ,u.country
    ,null as device
    ,u.install_source
    ,u.browser
    ,u.browser_version
    ,u.language
    ,t.level
    ,t.scene
    ,t.step as tutorial_step
    ,t.tutorial_type
    ,count(distinct t.user_key) as user_cnt
from  tmp_tutorial_step_first_time t
join custom_processed.fact_new_user u on t.user_key = u.user_key
where
   u.install_date >current_date-30
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
union all
select
     u.install_date
    ,u.app_id
    ,u.app_version
    ,u.os
    ,u.os_version
    ,u.country
    ,null as device
    ,u.install_source
    ,u.browser
    ,u.browser_version
    ,u.language
    ,1 as level
    ,u.scene
    ,'0' as step
    ,'Install' as tutorial_type
    ,count(distinct u.user_key)
from  custom_processed.fact_new_user u
where
    u.install_date >current_date-30
    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;
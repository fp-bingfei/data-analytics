DELETE FROM vegas.tutorial
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO vegas.tutorial
SELECT 
	app_id
	,user_id
	,coalesce(trunc(ts_pretty), '2016-01-01') as date
	,coalesce(trunc(install_ts), '2016-01-01') as install_date
	,country_code as country
	,coalesce(lang, 'EN') as lang
	,coalesce(os, 'Android') as os
	,level
	,coalesce(app_version, '0.0.0') as app_version
	,null as action
	,json_extract_path_text(d_c1, 'value') as tutorial_step
FROM vegas.catchall
WHERE event = 'tutorial'
AND user_id is not null
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM vegas.drive
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO vegas.drive
SELECT 
	app_id
	,user_id
	,coalesce(trunc(ts_pretty), '2016-01-01') as date
	,coalesce(trunc(install_ts), '2016-01-01') as install_date
	,country_code as country
	,coalesce(lang, 'EN') as lang
	,coalesce(os, 'Android') as os
	,level
	,coalesce(app_version, '0.0.0') as app_version
	,null as action
FROM vegas.catchall
WHERE event = 'drive'
AND user_id is not null
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM vegas.order
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO vegas.order
SELECT 
	app_id
	,user_id
	,coalesce(trunc(ts_pretty), '2016-01-01') as date
	,coalesce(trunc(install_ts), '2016-01-01') as install_date
	,country_code as country
	,coalesce(lang, 'EN') as lang
	,coalesce(os, 'Android') as os
	,level
	,coalesce(app_version, '0.0.0') as app_version
	,null as action
	,json_extract_path_text(d_c1, 'value')::INT as order_score
FROM vegas.catchall
WHERE event = 'order'
AND user_id is not null
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

DELETE FROM vegas.levelup
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;

INSERT INTO vegas.levelup
SELECT 
	app_id
	,user_id
	,coalesce(trunc(ts_pretty), '2016-01-01') as date
	,coalesce(trunc(install_ts), '2016-01-01') as install_date
	,country_code as country
	,coalesce(lang, 'EN') as lang
	,coalesce(os, 'Android') as os
	,coalesce(app_version, '0.0.0') as app_version
	,null as action
	,json_extract_path_text(d_c1, 'value') as levelup_type
	,json_extract_path_text(d_c2, 'value')::INT as levelup_from
	,json_extract_path_text(d_c3, 'value')::INT as levelup_to
FROM vegas.catchall
WHERE event = 'levelup'
AND user_id is not null
-- and ts_pretty >=
--     (
--         SELECT start_date
--         FROM   kpi_processed.init_start_date
--     )
;


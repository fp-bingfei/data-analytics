
drop table if exists tmp_infras;
CREATE temp table tmp_infras as
SELECT
date
,case 
when linkedaccountid = '131433655760' then 'fruitscoot'
when linkedaccountid = '135620544099' then 'poker'
when linkedaccountid = '147744843438' then 'waf'
when linkedaccountid = '266508744455' then 'devops'
when linkedaccountid = '289997590022' then 'ShineZone'
when linkedaccountid = '370884182004' then 'battlewarship'
when linkedaccountid = '376371647872' then 'dwtlw'
when linkedaccountid = '403045566686' then 'NLP'
when linkedaccountid = '424384843432' then 'farm'
when linkedaccountid = '431036357599' then 'royal'
when linkedaccountid = '457994678495' then 'royal'
when linkedaccountid = '464507519694' then 'gxs'
when linkedaccountid = '484933957969' then 'crazyplanets'
when linkedaccountid = '541015476938' then 'farm'
when linkedaccountid = '583300917620' then 'Infra'
when linkedaccountid = '614675119462' then 'VideoCommunity'
when linkedaccountid = '634033320744' then 'xiyou'
when linkedaccountid = '645037889100' then 'undergroundguardian'
when linkedaccountid = '677477847658' then 'lc'
when linkedaccountid = '683077249754' then 'Platform'
when linkedaccountid = '687657786659' then 'Funplus'
when linkedaccountid = '716504217123' then 'Slots'
when linkedaccountid = '735714818384' then 'ffs'
when linkedaccountid = '753756377410' then 'kuaiya'
when linkedaccountid = '838011746804' then 'dtl'
when linkedaccountid = '873210112193' then 'farm'
when linkedaccountid = '889096239409' then 'koa'
when linkedaccountid = '918819464745' then 'farm'
when linkedaccountid = '921287511715' then 'data'
when linkedaccountid = '924801593386' then 'ha'
when linkedaccountid = '941628076438' then 'ffs'
when linkedaccountid = '972926864295' then 'mt2'
when linkedaccountid = '670914317338' then 'loe'
--when linkedaccountid = '618191603327' then 'crazyplanets'
when linkedaccountid = '647268852127' then 'wartide'
when linkedaccountid = '860495783590' then 'maitaimadness'
when linkedaccountid = '956231806574' then 'sandigma'
when linkedaccountid = '069809866012' then 'inke'
when linkedaccountid = '575908059789' then 'vegas'
else 'others'
end as game
,sum(truecost) as amount
FROM db_usage.billing
where date >=  '2015-01-01'
GROUP BY 1,2;

drop table if exists tmp_revenue;
CREATE temp table tmp_revenue as
SELECT
date
,split_part(app_id, '.', 1) as game
,sum(revenue) as amount
FROM db_usage.revenue
where date >=  '2015-01-01'
GROUP BY 1,2;

drop table if exists tmp_marketing;
CREATE temp table tmp_marketing as
SELECT
start_date as date
--,case
--    when app = 'Family Farm Seaside' then 'ffs'
--    when app = 'Happy Acres' then 'ha'
--    when app = 'What a Farm' then 'waf'
--    when app = 'Battle Warships' then 'battlewarship'
--end
,case
when app = 'Barn Voyage' then 'waf'
when app = 'Battle Warships' then 'battlewarship'
when app = 'Citadel Realms' then 'loe'
when app = 'Dot Arena' then 'dtl'
when app = 'Family Farm' then 'farm'
when app = 'Family Farm Seaside' then 'ffs'
when app = 'Fruit Scoot' then 'fruitscoot'
when app = 'Galaxy Zero' then 'gxs'
when app = 'Happy Acres' then 'ha'
when app = 'King of Avalon' then 'koa'
when app = 'Kiss the Chef' then 'lc'
when app = 'League of Immortals' then 'xiyou'
when app = 'Mai Tai Madness' then 'maitaimadness'
when app = 'MT2' then 'mt2'
when app = 'Royal Story' then 'royal'
when app = 'Smash Island' then 'crazyplanets'
when app = 'Valiant Force' then '--'
when app = 'Wartide' then 'wartide'
when app = 'What a Farm' then 'waf'
end as game
,sum(case when adn_cost != 'None' then CAST(adn_cost as numeric) else 0 end) as amount
FROM raw_data.singular
where start_date >=  '2015-01-01'
GROUP BY 1,2;

delete from db_usage.profit_loss;
INSERT INTO db_usage.profit_loss
(
date
,game
,revenue
,marketing
,infras
,others
,net_revenue
)
SELECT
i.date
,i.game
,COALESCE(r.amount, 0)
,COALESCE(m.amount, 0)
,COALESCE(i.amount, 0)
,0
,COALESCE(r.amount, 0) - COALESCE(m.amount, 0) - COALESCE(i.amount, 0)
FROM
tmp_infras i left join tmp_marketing m on i.date = m.date and i.game = m.game
left join tmp_revenue r on i.date = r.date and i.game = r.game;
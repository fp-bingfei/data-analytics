drop table if exists db_usage.profit_loss; 
CREATE TABLE db_usage.profit_loss
(
	date DATE ENCODE delta,
	game VARCHAR(32) ENCODE lzo,
	revenue NUMERIC(20, 8),
	marketing NUMERIC(20, 8),
	infras NUMERIC(20, 8),
	others NUMERIC(20, 8),
	net_revenue NUMERIC(20, 8)
)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game
);

create table db_usage.revenue
(
	date DATE ENCODE delta,
	app_id VARCHAR(32) ENCODE lzo,
	revenue NUMERIC(20, 8)
)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game
);
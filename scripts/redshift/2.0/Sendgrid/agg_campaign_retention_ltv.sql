--- agg_sendgrid_retention_ltv -----------

DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (
    SELECT 1 AS day UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);

CREATE TEMP TABLE player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM 
kpi_processed.fact_dau_snapshot)
SELECT player_day,
d.app_id,
app_version,
install_date,
install_source,
install_subpublisher,
install_campaign,
install_creative_id,
device_alias,
os,
browser,
country,
language,
is_payer,
 case when fs.user_id is NOT null then true else false end AS 
email_flag,
count(distinct d.user_key) new_user_cnt from 
(select 
   p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.is_payer,
     d.user_id,
    d.user_key 
FROM kpi_processed.dim_user d, player_day p, last_date l, 
kpi_processed.fact_dau_snapshot dau
where d.install_date> DATEADD(day,-180,(select start_date from 
third_party.init_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
)d LEFT OUTER JOIN 
(SELECT distinct app_id,user_id 
,first_value(campaign_name ignore nulls) OVER (partition by 
app_id,user_id order by campaign_date desc
                                                  rows between unbounded 
preceding and unbounded following) as campaign_name FROM 
third_party.fact_sendgrid 
WHERE  delivered_ts is not null and bounce_ts is 
null )
fs
ON (d.app_id = fs.app_id and d.user_id = fs.user_id)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;



CREATE TEMP TABLE baseline AS
WITH last_date AS (SELECT max(date) AS date FROM 
kpi_processed.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.is_payer,
    case when fs.user_id is NOT null then true else false end AS 
email_flag,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 
else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM kpi_processed.fact_dau_snapshot d
 JOIN kpi_processed.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 LEFT OUTER JOIN 
(SELECT distinct app_id,user_id, md5(app_id||user_id) as user_key
,first_value(campaign_name ignore nulls) OVER (partition by 
app_id,user_id order by campaign_date desc
                                                  rows between unbounded 
preceding and unbounded following) as campaign_name FROM 
third_party.fact_sendgrid 
WHERE  delivered_ts is not null and bounce_ts is 
null )
fs
ON (d.app_id = fs.app_id and d.user_key = fs.user_key)
 WHERE
    u.install_date> DATEADD(day,-180,(select start_date from 
third_party.init_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;



DROP TABLE IF EXISTS third_party.agg_sendgrid_retention_ltv;
CREATE TABLE third_party.agg_sendgrid_retention_ltv AS
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.is_payer,
    pc.new_user_cnt,
    pc.email_flag,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube pc
LEFT JOIN baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0) AND 
    pc.email_flag = b.email_flag;


--------- agg_campaign_retention_ltv --------

create temp table tmp_users as
select  f.campaign_date
       ,f.campaign_name
       ,u.os
       ,u.country
       ,u.app_id as app_id
       ,u.install_source
       ,u.browser
       ,u.language
       ,u.is_payer
       ,count(distinct f.user_id) as user_cnt
from third_party.fact_sendgrid f join kpi_processed.dim_user u on 
f.app_id=u.app_id and f.user_id=u.user_id
group by 1,2,3,4,5,6,7,8,9;

--create temp table to get the last date in dau table

create temporary table last_date_cam
as
select max(date) as date
from kpi_processed.fact_dau_snapshot;


--create cube to calculate the retention

create temp table player_day_cube_cam 
as
select  pd.day as player_day
       ,nu.campaign_date
       ,nu.campaign_name
       ,nu.os
       ,nu.country
       ,nu.app_id
       ,nu.install_source
       ,nu.browser
       ,nu.language
       ,nu.is_payer
       ,nu.user_cnt
from   tmp_users nu
join   player_day pd
on 1=1
join   last_date_cam d
on 1=1
where  datediff('day',nu.campaign_date,d.date) >= pd.day;       

--create agg_campaign_retention table

drop table if exists third_party.agg_campaign_retention_ltv;
create table third_party.agg_campaign_retention_ltv as
select  cube.player_day
       ,cube.campaign_date
       ,cube.campaign_name
       ,cube.os
       ,cube.country
       ,cube.app_id
       ,cube.install_source
       ,cube.browser
       ,cube.language
       ,cube.is_payer
       ,cube.user_cnt
       ,coalesce(r.retained_user_cnt,0) as retained_user_cnt
       ,coalesce(r.revenue_usd,0) as revenue
from   player_day_cube_cam cube
left join
      (
        select   datediff('day',fu.campaign_date,du.date) as player_day
                ,fu.campaign_date
                ,u.os
                ,u.country
                ,u.app_id
                ,u.install_source as install_source
                ,u.browser
                ,u.language
                ,u.is_payer
                ,fu.campaign_name
                ,count(distinct du.user_key) as retained_user_cnt
                ,sum(du.revenue_usd) as revenue_usd
        from    kpi_processed.fact_dau_snapshot du
        join    (select user_id,app_id,campaign_name,campaign_date, md5
(app_id||user_id) as user_key from third_party.fact_sendgrid) fu
        on du.user_key=fu.user_key and du.app_id=fu.app_id
        join    kpi_processed.dim_user u
        on      du.user_key = u.user_key
        where   datediff('day',fu.campaign_date,du.date) in  (select day 
from player_day)
        group by 1,2,3,4,5,6,7,8,9,10                       
      )r
on  cube.player_day = r.player_day
and cube.campaign_date = r.campaign_date
and coalesce(cube.app_id,'') = coalesce(r.app_id,'')
and coalesce(cube.os,'') = coalesce(r.os,'') 
and coalesce(cube.country,'') = coalesce(r.country,'') 
and coalesce(cube.install_source,'') = coalesce(r.install_source,'')
and coalesce(cube.campaign_name,'') = coalesce(r.campaign_name,'')
and coalesce(cube.browser,'') = coalesce(r.browser,'')
and coalesce(cube.language,'') = coalesce(r.language,'')
and cube.is_payer = r.is_payer;
commit;
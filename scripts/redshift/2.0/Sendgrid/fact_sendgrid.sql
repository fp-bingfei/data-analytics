---update third_party.raw_sendgrid_daily set app_id='ha.th.prod' where app_id='ff2.th.prod';
---update third_party.raw_sendgrid_daily set app_id='ha.us.prod' where app_id='ff2.us.prod';

create temp table last_login as
select max(date) as login_date,s3_path, d.app_id,uid,snsid,email,campaign,dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') as delivered_ts
 from third_party.raw_sendgrid_daily d
left join kpi_processed.fact_dau_snapshot f on md5(d.app_id||d.snsid)=f.user_key and f.date<trunc(dateadd(SECOND,CAST(d.TIME AS INTEGER),'1970-01-01 00:00:00'))
where d.app_id like 'ffs%' and day>=(SELECT start_date FROM third_party.init_start_date) and event='delivered' 
group by 2,3,4,5,6,7,8;



CREATE temp TABLE fact_sendgrid_temp
AS
SELECT sg.app_id,
       campaign,
       TRUNC(delivered_ts) AS campaign_date,
       sg.uid,
       sg.snsid,
       email,
       sg.os,
       sg.device,
       sg.country,
       delivered_ts,
       bounce_ts,
       open_ts,
       click_ts,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.session_cnt ELSE 0 END) precamp_session_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.payment_cnt ELSE 0 END) precamp_purchase_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.revenue_usd ELSE 0 END) precamp_revenue_usd,
       MAX(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.level_end ELSE 1 END) AS precamp_level_end,
       MAX(dau.level_end) AS level_end,
       SUM(dau.session_cnt) total_session_cnt,
       SUM(dau.payment_cnt) total_purchase_cnt,
       SUM(dau.revenue_usd) total_revenue_usd,
       unsubscribe_ts,
       spamreport_ts,
       sg.level_start,
       cast ('2016-01-01' as date) as login_date,
              deferred_ts,
              send_ts,
              s3_path
FROM (SELECT s1.app_id,
             s1.campaign,
             s1.uid,
             s1.snsid,
             s1.email,
             du.os,
             du.device,
             du.country,
             du.user_key,
             l.level_start,
             s1.s3_path,
             s1.time AS delivered_ts,
             s2.time AS send_ts,
             s3.time AS open_ts,
             s4.time AS click_ts,
             s5.time AS unsubscribe_ts,
             s6.time AS spamreport_ts,
             s7.time as deferred_ts,
             s8.time as bounce_ts,
             ROW_NUMBER() OVER (PARTITION BY s1.campaign,s1.uid,s1.email,s1.event,TRUNC(s1.time) ORDER BY s3.time ASC) rnum
      FROM (SELECT snsid,
                   email,
                   category,
                   uid,
                   app_id,
                   dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                   ip,
                   event,
                   DAY,
                   campaign,
                   s3_path
            FROM third_party.raw_sendgrid_daily where app_id not like 'ffs%' and s3_path='old') s1
        LEFT OUTER JOIN (SELECT * FROM kpi_processed.dim_user) du
                ON (du.app_id = s1.app_id
               AND du.user_id = s1.uid)
        LEFT OUTER JOIN kpi_processed.fact_dau_snapshot l
                ON (l.user_key = md5(s1.app_id||s1.uid)
               AND l.date = trunc(s1.time))
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s2
                     ON (s1.app_id = s2.app_id
                    AND s1.campaign = s2.campaign
                    AND s1.uid = s2.uid
                    AND s1.email = s2.email
                    and s1.s3_path=s2.s3_path
                    AND s1.time <= nvl (s2.time,s1.time)
                    AND s2.event = 'send' )
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s3
                     ON (s1.app_id = s3.app_id
                    AND s1.campaign = s3.campaign
                    AND s1.uid = s3.uid
                    AND s1.email = s3.email
                    and s1.s3_path=s3.s3_path
                    AND s1.time <= nvl (s3.time,s1.time)
                    AND s3.event = 'open')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s4
                     ON (s1.app_id = s4.app_id
                    AND s1.campaign = s4.campaign
                    AND s1.uid = s4.uid
                    AND s1.email = s4.email
                    and s1.s3_path=s4.s3_path
                    AND s1.time <= nvl (s4.time,s1.time)
                    AND s4.event = 'click')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s5
                     ON (s1.app_id = s5.app_id
                    AND s1.campaign = s5.campaign
                    AND s1.uid = s5.uid
                    AND s1.email = s5.email
                    and s1.s3_path=s5.s3_path
                    AND s1.time <= nvl (s5.time,s1.time)
                    AND s5.event = 'unsubscribe')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s6
                     ON (s1.app_id = s6.app_id
                    AND s1.campaign = s6.campaign
                    AND s1.uid = s6.uid
                    AND s1.email = s6.email
                    and s1.s3_path=s6.s3_path
                    AND s1.time <= nvl (s6.time,s1.time)
                    AND s6.event = 'spamreport')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s7
                     ON (s1.app_id = s7.app_id
                    AND s1.campaign = s7.campaign
                    AND s1.uid = s7.uid
                    AND s1.email = s7.email
                    and s1.s3_path=s7.s3_path
                    AND s1.time <= nvl (s7.time,s1.time)
                    AND s7.event = 'deferred')  
         LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='old') s8
                     ON (s1.app_id = s8.app_id
                    AND s1.campaign = s8.campaign
                    AND s1.uid = s8.uid
                    AND s1.email = s8.email
                    and s1.s3_path=s8.s3_path
                    AND s1.time <= nvl (s8.time,s1.time)
                    AND s8.event = 'bounce')                     
      WHERE s1.event = 'delivered'
      AND   TRUNC(s1.time) >= (SELECT start_date FROM third_party.init_start_date)) sg
  LEFT OUTER JOIN (SELECT * FROM kpi_processed.fact_dau_snapshot) dau
               ON (sg.app_id = dau.app_id
              AND sg.user_key = dau.user_key
              AND dau.date <= (CURRENT_DATE- 1))
WHERE rnum = 1
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,22,23,24,25,26,27,28
ORDER BY app_id,
         campaign,
         campaign_date,
         delivered_ts;
         

DELETE
FROM third_party.fact_sendgrid USING fact_sendgrid_temp t
WHERE third_party.fact_sendgrid.app_id = t.app_id
AND   third_party.fact_sendgrid.campaign_name = t.campaign
AND   third_party.fact_sendgrid.campaign_date = t.campaign_date
and   third_party.fact_sendgrid.s3_path = t.s3_path
AND   third_party.fact_sendgrid.user_id = t.uid
AND   third_party.fact_sendgrid.email = t.email;


insert into third_party.fact_sendgrid
select * from fact_sendgrid_temp;

drop table fact_sendgrid_temp;

CREATE temp TABLE fact_sendgrid_temp_1
AS
SELECT sg.app_id,
       campaign,
       TRUNC(send_ts) AS campaign_date,
       sg.uid,
       sg.snsid,
       email,
       sg.os,
       sg.device,
       sg.country,
       delivered_ts,
       bounce_ts,
       open_ts,
       click_ts,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.session_cnt ELSE 0 END) precamp_session_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.payment_cnt ELSE 0 END) precamp_purchase_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.revenue_usd ELSE 0 END) precamp_revenue_usd,
       MAX(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.level_end ELSE 1 END) AS precamp_level_end,
       MAX(dau.level_end) AS level_end,
       SUM(dau.session_cnt) total_session_cnt,
       SUM(dau.payment_cnt) total_purchase_cnt,
       SUM(dau.revenue_usd) total_revenue_usd,
       unsubscribe_ts,
       spamreport_ts,
       sg.level_start,
       cast ('2016-01-01' as date) as login_date,
              deferred_ts,
              send_ts,
              s3_path
FROM (SELECT s1.app_id,
             s1.campaign,
             s1.uid,
             s1.snsid,
             s1.email,
             du.os,
             du.device,
             du.country,
             du.user_key,
             l.level_start,
             s1.s3_path,
             s1.time AS send_ts,
             s2.time AS delivered_ts,
             s3.time AS open_ts,
             s4.time AS click_ts,
             s5.time AS unsubscribe_ts,
             s6.time AS spamreport_ts,
             s7.time as deferred_ts,
             s8.time as bounce_ts,
             ROW_NUMBER() OVER (PARTITION BY s1.campaign,s1.uid,s1.email,s1.event,TRUNC(s1.time) ORDER BY s3.time ASC) rnum
      FROM (SELECT snsid,
                   email,
                   category,
                   uid,
                   app_id,
                   dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                   ip,
                   event,
                   DAY,
                   campaign,
                   s3_path
            FROM third_party.raw_sendgrid_daily where app_id not like 'ffs%' and s3_path='new') s1
        LEFT OUTER JOIN (SELECT * FROM kpi_processed.dim_user) du
                ON (du.app_id = s1.app_id
               AND du.user_id = s1.uid)
        LEFT OUTER JOIN kpi_processed.fact_dau_snapshot l
                ON (l.user_key = md5(s1.app_id||s1.uid)
               AND l.date = trunc(s1.time))
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s2
                     ON (s1.app_id = s2.app_id
                    AND s1.campaign = s2.campaign
                    AND s1.uid = s2.uid
                    AND s1.email = s2.email
                    and s1.s3_path=s2.s3_path
                    AND s1.time <= nvl (s2.time,s1.time)
                    AND s2.event = 'delivered')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s3
                     ON (s1.app_id = s3.app_id
                    AND s1.campaign = s3.campaign
                    AND s1.uid = s3.uid
                    AND s1.email = s3.email
                    and s1.s3_path=s3.s3_path
                    AND s1.time <= nvl (s3.time,s1.time)
                    AND s3.event = 'open' )
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s4
                     ON (s1.app_id = s4.app_id
                    AND s1.campaign = s4.campaign
                    AND s1.uid = s4.uid
                    AND s1.email = s4.email
                    and s1.s3_path=s4.s3_path
                    AND s1.time <= nvl (s4.time,s1.time)
                    AND s4.event = 'click' )
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s5
                     ON (s1.app_id = s5.app_id
                    AND s1.campaign = s5.campaign
                    AND s1.uid = s5.uid
                    AND s1.email = s5.email
                    and s1.s3_path=s5.s3_path
                    AND s1.time <= nvl (s5.time,s1.time)
                    AND s5.event = 'unsubscribe')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s6
                     ON (s1.app_id = s6.app_id
                    AND s1.campaign = s6.campaign
                    AND s1.uid = s6.uid
                    AND s1.email = s6.email
                    and s1.s3_path=s6.s3_path
                    AND s1.time <= nvl (s6.time,s1.time)
                    AND s6.event = 'spamreport')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s7
                     ON (s1.app_id = s7.app_id
                    AND s1.campaign = s7.campaign
                    AND s1.uid = s7.uid
                    AND s1.email = s7.email
                    and s1.s3_path=s7.s3_path
                    AND s1.time <= nvl (s7.time,s1.time)
                    AND s7.event = 'deferred')  
         LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where s3_path='new') s8
                     ON (s1.app_id = s8.app_id
                    AND s1.campaign = s8.campaign
                    AND s1.uid = s8.uid
                    AND s1.email = s8.email
                    and s1.s3_path=s8.s3_path
                    AND s1.time <= nvl (s8.time,s1.time)
                    AND s8.event = 'bounce')                     
      WHERE s1.event = 'send'
      AND   TRUNC(s1.time) >= (SELECT start_date FROM third_party.init_start_date)) sg
  LEFT OUTER JOIN (SELECT * FROM kpi_processed.fact_dau_snapshot) dau
               ON (sg.app_id = dau.app_id
              AND sg.user_key = dau.user_key
              AND dau.date <= (CURRENT_DATE- 1))
WHERE rnum = 1
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,22,23,24,25,26,27,28
ORDER BY app_id,
         campaign,
         campaign_date,
         delivered_ts;

         

DELETE
FROM third_party.fact_sendgrid USING fact_sendgrid_temp_1 t
WHERE third_party.fact_sendgrid.app_id = t.app_id
AND   third_party.fact_sendgrid.campaign_name = t.campaign
AND   third_party.fact_sendgrid.campaign_date = t.campaign_date
and   third_party.fact_sendgrid.s3_path = t.s3_path
AND   third_party.fact_sendgrid.user_id = t.uid
AND   third_party.fact_sendgrid.email = t.email;

insert into third_party.fact_sendgrid
select * from fact_sendgrid_temp_1;

drop table fact_sendgrid_temp_1;


delete from third_party.fact_sendgrid where app_id like 'ffs%' and campaign_date >= (SELECT start_date FROM third_party.init_start_date);




CREATE temp TABLE fact_sendgrid_temp_ffs
AS
SELECT sg.app_id,
       sg.campaign,
       TRUNC(sg.delivered_ts) AS campaign_date,
       sg.snsid,
       sg.uid,
       sg.email,
       sg.os,
       sg.device,
       sg.country,
       sg.delivered_ts,
       bounce_ts,
       open_ts,
       click_ts,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.session_cnt ELSE 0 END) precamp_session_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.payment_cnt ELSE 0 END) precamp_purchase_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.revenue_usd ELSE 0 END) precamp_revenue_usd,
       MAX(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.level_end ELSE 1 END) AS precamp_level_end,
       MAX(dau.level_end) AS level_end,
       SUM(dau.session_cnt) total_session_cnt,
       SUM(dau.payment_cnt) total_purchase_cnt,
       SUM(dau.revenue_usd) total_revenue_usd,
       unsubscribe_ts,
       spamreport_ts,
       sg.level_start,
       p.login_date,
       deferred_ts,
       send_ts,
       sg.s3_path
FROM (SELECT s1.app_id,
             s1.campaign,
             s1.uid,
             s1.snsid,
             s1.email,
             du.os,
             du.device,
             du.country,
             du.user_key,
             l.level_start,
             s1.s3_path,
             s1.time AS delivered_ts,
             s2.time AS send_ts,
             s3.time AS open_ts,
             s4.time AS click_ts,
             s5.time AS unsubscribe_ts,
             s6.time as spamreport_ts,
             s7.time as deferred_ts,
             s8.time as bounce_ts,
             ROW_NUMBER() OVER (PARTITION BY s1.campaign,s1.uid,s1.email,s1.event,TRUNC(s1.time) ORDER BY s3.time ASC) rnum
      FROM (SELECT snsid,
                   email,
                   category,
                   uid,
                   app_id,
                   dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                   ip,
                   event,
                   DAY,
                   campaign,
                   s3_path
            FROM third_party.raw_sendgrid_daily
            WHERE app_id LIKE 'ffs%' and s3_path='old') s1
        LEFT OUTER JOIN (SELECT * FROM kpi_processed.dim_user WHERE app_id LIKE 'ffs%') du
                ON (du.app_id = s1.app_id
               AND du.user_id = s1.snsid)
        LEFT OUTER JOIN (SELECT * FROM kpi_processed.fact_dau_snapshot WHERE app_id LIKE 'ffs%') l
                ON (l.user_key = md5(s1.app_id||s1.snsid)
               AND l.date = trunc(s1.time))
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily
                         WHERE app_id LIKE 'ffs%' and s3_path='old') s2
                     ON (s1.app_id = s2.app_id
                    AND s1.campaign = s2.campaign
                    AND s1.uid = s2.uid
                    AND s1.snsid = s2.snsid
                    AND s1.email = s2.email
                    and s1.s3_path=s2.s3_path
                    AND s1.time <= nvl (s2.time,s1.time)
                    AND s2.event = 'send')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily
                         WHERE app_id LIKE 'ffs%' and s3_path='old') s3
                     ON (s1.app_id = s3.app_id
                    AND s1.campaign = s3.campaign
                    AND s1.uid = s3.uid
                    AND s1.snsid = s3.snsid
                    AND s1.email = s3.email
                    and s1.s3_path=s3.s3_path
                    AND s1.time <= nvl (s3.time,s1.time)
                    AND s3.event = 'open')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily
                         WHERE app_id LIKE 'ffs%' and  s3_path='old') s4
                     ON (s1.app_id = s4.app_id
                    AND s1.campaign = s4.campaign
                    AND s1.uid = s4.uid
                    AND s1.snsid = s4.snsid
                    AND s1.email = s4.email
                    and s1.s3_path=s4.s3_path
                    AND s1.time <= nvl (s4.time,s1.time)
                    AND s4.event = 'click')
      LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where app_id LIKE 'ffs%' and s3_path='old') s5
                     ON (s1.app_id = s5.app_id
                    AND s1.campaign = s5.campaign
                    AND s1.uid = s5.uid
                    AND s1.email = s5.email
                    and s1.s3_path=s5.s3_path
                    AND s1.time <= nvl (s5.time,s1.time)
                    AND s5.event = 'unsubscribe')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where app_id LIKE 'ffs%' and s3_path='old') s6
                     ON (s1.app_id = s6.app_id
                    AND s1.campaign = s6.campaign
                    AND s1.uid = s6.uid
                    AND s1.email = s6.email
                    and s1.s3_path=s6.s3_path
                    AND s1.time <= nvl (s6.time,s1.time)
                    AND s6.event = 'spamreport')
                    LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where app_id LIKE 'ffs%' and s3_path='old') s7
                     ON (s1.app_id = s7.app_id
                    AND s1.campaign = s7.campaign
                    AND s1.uid = s7.uid
                    AND s1.email = s7.email
                    and s1.s3_path=s7.s3_path
                    AND s1.time <= nvl (s7.time,s1.time)
                    AND s7.event = 'deferred')
                    LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where app_id LIKE 'ffs%' and  s3_path='old') s8
                     ON (s1.app_id = s8.app_id
                    AND s1.campaign = s8.campaign
                    AND s1.uid = s8.uid
                    AND s1.email = s8.email
                    and s1.s3_path=s8.s3_path
                    AND s1.time <= nvl (s8.time,s1.time)
                    AND s8.event = 'bounce')
      WHERE s1.event = 'delivered'
      AND   TRUNC(s1.time) >= (SELECT start_date FROM third_party.init_start_date)) sg
  LEFT OUTER JOIN (SELECT *
                   FROM kpi_processed.fact_dau_snapshot
                   WHERE app_id LIKE 'ffs%') dau
               ON (sg.app_id = dau.app_id
              AND sg.user_key = dau.user_key
              AND dau.date <= (CURRENT_DATE- 1))
              left join (select login_date,app_id,uid,snsid,campaign,email,delivered_ts,s3_path from last_login where s3_path='old')p
        on sg.app_id=p.app_id 
        and sg.snsid=p.snsid
        and sg.campaign=p.campaign
        and sg.s3_path=p.s3_path
        and sg.delivered_ts=p.delivered_ts
        and sg.email=p.email
WHERE rnum = 1
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,22,23,24,25,26,27,28
ORDER BY app_id,
         campaign,
         campaign_date,
         delivered_ts;

DELETE
FROM third_party.fact_sendgrid USING fact_sendgrid_temp_ffs t
WHERE third_party.fact_sendgrid.app_id LIKE 'ffs%'
AND   third_party.fact_sendgrid.app_id = t.app_id
AND   third_party.fact_sendgrid.campaign_name = t.campaign
AND   third_party.fact_sendgrid.campaign_date = t.campaign_date
AND   third_party.fact_sendgrid.user_id = t.snsid
AND   third_party.fact_sendgrid.email = t.email;

INSERT INTO third_party.fact_sendgrid
SELECT *
FROM fact_sendgrid_temp_ffs where app_id like 'ffs%';


DROP TABLE fact_sendgrid_temp_ffs;

CREATE temp TABLE fact_sendgrid_temp_uid_null
AS
SELECT sg.app_id,
       campaign,
       TRUNC(delivered_ts) AS campaign_date,
       sg.uid,
       sg.snsid,
       email,
       sg.os,
       sg.device,
       sg.country,
       delivered_ts,
       bounce_ts,
       open_ts,
       click_ts,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.session_cnt ELSE 0 END) precamp_session_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.payment_cnt ELSE 0 END) precamp_purchase_cnt,
       SUM(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.revenue_usd ELSE 0 END) precamp_revenue_usd,
       MAX(CASE WHEN dau.date < TRUNC(sg.delivered_ts) THEN dau.level_end ELSE 1 END) AS precamp_level_end,
       MAX(dau.level_end) AS level_end,
       SUM(dau.session_cnt) total_session_cnt,
       SUM(dau.payment_cnt) total_purchase_cnt,
       SUM(dau.revenue_usd) total_revenue_usd,
       unsubscribe_ts,
       spamreport_ts,
       l.level_start,
       cast('2016-01-01' as date) as login_date,
       deferred_ts,
       send_ts,
       s3_path
FROM (SELECT s1.app_id,
             s1.campaign,
             du.user_id as uid,
             s1.snsid,
             s1.email,
             du.os,
             du.device,
             du.country,
             du.user_key,
             s1.time AS delivered_ts,
             s2.time AS send_ts,
             s3.time AS open_ts,
             s4.time AS click_ts,
             s5.time AS unsubscribe_ts,
             s6.time AS spamreport_ts,
             s7.time as deferred_ts,
             s8.time as bounce_ts,
             s1.s3_path,
             ROW_NUMBER() OVER (PARTITION BY s1.campaign,s1.uid,s1.email,s1.event,TRUNC(s1.time) ORDER BY s3.time ASC) rnum
      FROM (SELECT snsid,
                   email,
                   category,
                   uid,
                   app_id,
                   dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                   ip,
                   event,
                   DAY,
                   campaign,
                   s3_path
            FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s1
        inner JOIN (SELECT * FROM kpi_processed.dim_user) du
                ON (du.app_id = s1.app_id
               AND du.facebook_id = s1.snsid)

        
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s2
                     ON (s1.app_id = s2.app_id
                    AND s1.campaign = s2.campaign
                    AND s1.snsid = s2.snsid
                    AND s1.email = s2.email
                    and s1.s3_path=s2.s3_path
                    AND s1.time <= nvl (s2.time,s1.time)
                    AND s2.event = 'send')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s3
                     ON (s1.app_id = s3.app_id
                    AND s1.campaign = s3.campaign
                    AND s1.snsid = s3.snsid
                    AND s1.email = s3.email
                    and s1.s3_path=s3.s3_path
                    AND s1.time <= nvl (s3.time,s1.time)
                    AND s3.event = 'open')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s4
                     ON (s1.app_id = s4.app_id
                    AND s1.campaign = s4.campaign
                    AND s1.snsid = s4.snsid
                    AND s1.email = s4.email
                    and s1.s3_path=s4.s3_path
                    AND s1.time <= nvl (s4.time,s1.time)
                    AND s4.event = 'click')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s5
                     ON (s1.app_id = s5.app_id
                    AND s1.campaign = s5.campaign
                    AND s1.snsid = s5.snsid
                    AND s1.email = s5.email
                    and s1.s3_path=s5.s3_path
                    AND s1.time <= nvl (s5.time,s1.time)
                    AND s5.event = 'unsubscribe')
        LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s6
                     ON (s1.app_id = s6.app_id
                    AND s1.campaign = s6.campaign
                    AND s1.snsid = s6.snsid
                    AND s1.email = s6.email
                    and s1.s3_path=s6.s3_path
                    AND s1.time <= nvl (s6.time,s1.time)
                    AND s6.event = 'spamreport')
                    LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s7
                     ON (s1.app_id = s7.app_id
                    AND s1.campaign = s7.campaign
                    AND s1.snsid = s7.snsid
                    AND s1.email = s7.email
                    and s1.s3_path=s7.s3_path
                    AND s1.time <= nvl (s7.time,s1.time)
                    AND s7.event = 'deferred')
                    LEFT OUTER JOIN (SELECT snsid,
                                email,
                                category,
                                uid,
                                app_id,
                                dateadd(SECOND,CAST(TIME AS INTEGER),'1970-01-01 00:00:00') AS TIME,
                                ip,
                                event,
                                DAY,
                                campaign,
                                s3_path
                         FROM third_party.raw_sendgrid_daily where (uid is null or uid ='')) s8
                     ON (s1.app_id = s8.app_id
                    AND s1.campaign = s8.campaign
                    AND s1.snsid = s8.snsid
                    AND s1.email = s8.email
                    and s1.s3_path=s8.s3_path
                    AND s1.time <= nvl (s8.time,s1.time)
                    AND s8.event = 'bounce')
      WHERE s1.event = 'delivered'
      AND   TRUNC(s1.time) >= (SELECT start_date FROM third_party.init_start_date)) sg
  LEFT OUTER JOIN (SELECT * FROM kpi_processed.fact_dau_snapshot) dau
               ON (sg.app_id = dau.app_id
              AND sg.user_key = dau.user_key
              AND dau.date <= (CURRENT_DATE- 1))
 LEFT OUTER JOIN (SELECT * FROM kpi_processed.fact_dau_snapshot) l
                ON (l.user_key = sg.user_key
               AND l.date = trunc(sg.delivered_ts))
WHERE rnum = 1
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,22,23,24,25,26,27,28
ORDER BY app_id,
         campaign,
         campaign_date,
         delivered_ts;


DELETE
FROM third_party.fact_sendgrid USING fact_sendgrid_temp_uid_null t
WHERE third_party.fact_sendgrid.app_id = t.app_id
AND   third_party.fact_sendgrid.campaign_name = t.campaign
and   third_party.fact_sendgrid.s3_path = t.s3_path
AND   third_party.fact_sendgrid.campaign_date = t.campaign_date
AND   third_party.fact_sendgrid.snsid = t.snsid
AND   third_party.fact_sendgrid.email = t.email and third_party.fact_sendgrid.app_id not like 'ffs%' ;


insert into third_party.fact_sendgrid
select * from fact_sendgrid_temp_uid_null where app_id not like 'ffs%';

drop table fact_sendgrid_temp_uid_null;

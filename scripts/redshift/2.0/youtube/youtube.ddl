CREATE SCHEMA third_party ;
CREATE TABLE third_party.youtube_channel (
	date CHAR(8) ENCODE lzo,
	channel_id VARCHAR(32) ENCODE lzo,
	video_id VARCHAR(32) ENCODE lzo,
	live_or_on_demand VARCHAR(32) ENCODE lzo,
	subscribed_status VARCHAR(32) ENCODE lzo,
	country_code VARCHAR(4) ENCODE lzo,
	views BIGINT DEFAULT 0,
	comments BIGINT DEFAULT 0,
	likes BIGINT DEFAULT 0,
	dislikes BIGINT DEFAULT 0,
	shares BIGINT DEFAULT 0,
	watch_time_minutes NUMERIC(20, 8) DEFAULT 0,
	average_view_duration_seconds NUMERIC(20, 8) DEFAULT 0,
	average_view_duration_percentage NUMERIC(20, 8) DEFAULT 0,
	annotation_impressions NUMERIC(20, 8) DEFAULT 0,
	annotation_clickable_impressions NUMERIC(20, 8) DEFAULT 0,
	annotation_clicks NUMERIC(20, 8) DEFAULT 0,
	annotation_click_through_rate NUMERIC(20, 8) DEFAULT 0,
	annotation_closable_impressions NUMERIC(20, 8) DEFAULT 0,
	annotation_closes NUMERIC(20, 8) DEFAULT 0,
	annotation_close_rate NUMERIC(20, 8) DEFAULT 0,
	card_teaser_impressions NUMERIC(20, 8) DEFAULT 0,
	card_teaser_clicks NUMERIC(20, 8) DEFAULT 0,
	card_teaser_click_rate NUMERIC(20, 8) DEFAULT 0,
	card_impressions NUMERIC(20, 8) DEFAULT 0,
	card_clicks NUMERIC(20, 8) DEFAULT 0,
	card_click_rate NUMERIC(20, 8) DEFAULT 0,
	subscribers_gained NUMERIC(20, 8) DEFAULT 0,
	subscribers_lost NUMERIC(20, 8) DEFAULT 0,
	videos_added_to_playlists NUMERIC(20, 8) DEFAULT 0,
	videos_removed_from_playlists NUMERIC(20, 8) DEFAULT 0
) DISTSTYLE EVEN
SORTKEY
(
	date,
	channel_id,
	live_or_on_demand,
	subscribed_status
);

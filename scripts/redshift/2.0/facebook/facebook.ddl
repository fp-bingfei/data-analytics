CREATE TABLE third_party.facebook_insights
(
	date DATE,
	game VARCHAR(100) ENCODE lzo,
	page_fan_adds BIGINT,
	page_impression BIGINT,
	emotion BIGINT,
	storytellers BIGINT,
	negative_feedback BIGINT,
	page_fans BIGINT
	)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game
);

CREATE TABLE third_party.facebook_emotions
(
	date DATE,
	game VARCHAR(100) ENCODE lzo,
	likes BIGINT,
	loves BIGINT,
	wows BIGINT,
	hahas BIGINT,
	sorrys BIGINT,
	angers BIGINT
	)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game
);


CREATE TABLE third_party.facebook_post
(
	date DATE,
	game BIGINT,
	post_type VARCHAR(10) ENCODE lzo,
	post_message VARCHAR(500) ENCODE lzo,
	create_time TIMESTAMP,
	permalink VARCHAR(100) ENCODE lzo,
	post_impressions BIGINT,
	post_impressions_organic BIGINT,
	post_impressions_paid BIGINT,
	post_consumptions BIGINT,
	post_consumptions_unique BIGINT,
	post_negative_feeback BIGINT,
	likes BIGINT,
	comment BIGINT,
	share BIGINT,
	like_unique BIGINT,
	comment_unique BIGINT,
	share_unique BIGINT,
	emotion_like BIGINT,
	emotion_love BIGINT,
	emotion_wow BIGINT,
	emotion_haha BIGINT,
	emotion_sorry BIGINT,
	emotion_anger BIGINT
	)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game
);

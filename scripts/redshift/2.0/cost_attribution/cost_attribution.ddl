CREATE TABLE db_usage.billing
(
	LinkedAccountId CHAR(12) ENCODE lzo,
	ProductName VARCHAR(100) ENCODE lzo,
	UsageType VARCHAR(100) ENCODE lzo,
	Operation VARCHAR(100) ENCODE lzo,
	AvailabilityZone VARCHAR(30) ENCODE lzo,
	ReservedInstance VARCHAR(5) ENCODE lzo,
	Date DATE,
	UsageQuantity NUMERIC(25, 8),
	UnBlendCost NUMERIC(20, 8),
	Environment VARCHAR(30) ENCODE lzo,
	Project VARCHAR(30) ENCODE lzo,
	Role VARCHAR(30) ENCODE lzo,
	PayerAccountID CHAR(12) ENCODE lzo,
	PricingPlanId VARCHAR(50) ENCODE lzo,
	TrueCost NUMERIC(20, 8)
)
DISTSTYLE EVEN
SORTKEY
(
	LinkedAccountId,
	ProductName,
	UsageType,
	Operation,
	ReservedInstance,
	Project
);

CREATE TABLE db_usage.monthly_billing
(
	LinkedAccountId CHAR(12) ENCODE lzo,
	ProductName VARCHAR(100) ENCODE lzo,
	UsageType VARCHAR(100) ENCODE lzo,
	Operation VARCHAR(100) ENCODE lzo,
	AvailabilityZone VARCHAR(30) ENCODE lzo,
	ReservedInstance VARCHAR(5) ENCODE lzo,
	month CHAR(7) ENCODE lzo,
	UsageQuantity NUMERIC(25, 8),
	UnBlendCost NUMERIC(20, 8),
	Environment VARCHAR(30) ENCODE lzo,
	Project VARCHAR(30) ENCODE lzo,
	Role VARCHAR(30) ENCODE lzo,
	PayerAccountID CHAR(12) ENCODE lzo,
	PricingPlanId VARCHAR(50) ENCODE lzo,
	TrueCost NUMERIC(20, 8)
)
DISTSTYLE EVEN
SORTKEY
(
	LinkedAccountId,
	ProductName,
	UsageType,
	Operation,
	ReservedInstance,
	Project
);


CREATE TABLE db_usage.upfront_fee_mapping
(
	PayerAccountID CHAR(12) ENCODE lzo,
	PricingPlanId VARCHAR(50) ENCODE lzo,
	ProductName VARCHAR(100) ENCODE lzo,
	UsageType VARCHAR(100) ENCODE lzo,
	UpfrontFee NUMERIC(20, 8),
	monthly NUMERIC(20, 8),
	ReservedYears Integer,
	Hourly NUMERIC(20, 8)
)
DISTSTYLE EVEN
SORTKEY
(
	PayerAccountID,
	PricingPlanId,
	ProductName,
	UsageType
);

CREATE TABLE db_usage.raw_billing
(
	InvoiceID VARCHAR(200) ENCODE lzo,
	PayerAccountId CHAR(200) ENCODE lzo,
	LinkedAccountId CHAR(200) ENCODE lzo,
	RecordType VARCHAR(30) ENCODE lzo,
	RecordId VARCHAR(40) ENCODE lzo,
	ProductName VARCHAR(50) ENCODE lzo, 
	RateId VARCHAR(30) ENCODE lzo,
	SubscriptionId VARCHAR(100) ENCODE lzo,
	PricingPlanId VARCHAR(50) ENCODE lzo,
	UsageType VARCHAR(200) ENCODE lzo,
	Operation VARCHAR(50) ENCODE lzo,
	AvailabilityZone VARCHAR(30) ENCODE lzo,
	ReservedInstance VARCHAR(100) ENCODE lzo,
	ItemDescription VARCHAR(200) ENCODE lzo,
	UsageStartDate TIMESTAMP,
	UsageEndDate TIMESTAMP,
	UsageQuantity NUMERIC(25, 8),
	BlendedRate NUMERIC(20, 8),
	BlendedCost NUMERIC(20, 8),
	UnBlendedRate NUMERIC(20, 8),
	UnBlendCost NUMERIC(20, 8),
	ResourceId VARCHAR(200) ENCODE lzo,
	Environment VARCHAR(30) ENCODE lzo,
	Project VARCHAR(30) ENCODE lzo,
	Role VARCHAR(30) ENCODE lzo
)
DISTSTYLE EVEN
SORTKEY
(
	LinkedAccountId,
	ProductName,
	UsageType,
	Operation,
	AvailabilityZone,
	ReservedInstance,
	UsageStartDate,
	UsageEndDate,
	Project
);


CREATE TABLE db_usage.bi_games_share
(
date DATE,
game VARCHAR(20) ENCODE lzo, 
ProductName VARCHAR(100) ENCODE lzo,
AvailabilityZone VARCHAR(30) ENCODE lzo,
Size Integer,
TotalCost NUMERIC(20, 8),
IndividualCost NUMERIC(20, 8),
Role VARCHAR(30) ENCODE lzo
)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game,
	Role
);

CREATE TABLE db_usage.utilisation_rate
(
date DATE,
linkedaccountid CHAR(12) ENCODE lzo, 
ProductName VARCHAR(100) ENCODE lzo,
Zone VARCHAR(30) ENCODE lzo,
InstanceType VARCHAR(200) ENCODE lzo,
utilisation NUMERIC(20, 8)
)
DISTSTYLE EVEN
SORTKEY
(
	date,
	ProductName,
	InstanceType
);




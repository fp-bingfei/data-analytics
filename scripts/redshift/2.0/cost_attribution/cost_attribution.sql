--TODO 
--deal with invalid events
create temp table special_lines as
select * from db_usage.raw_data_s3
    WHERE position(',,' in csv_str) > 0;

delete from db_usage.raw_data_s3
    WHERE position(',,' in csv_str) > 0;

TRUNCATE TABLE db_usage.raw_billing;

INSERT INTO db_usage.raw_billing (
    InvoiceID,
    PayerAccountId,
    LinkedAccountId,
    RecordType,
    RecordId,
    ProductName,
    RateId,
    SubscriptionId,
    UsageType,
    Operation,
    ReservedInstance,
    ItemDescription,
    UsageStartDate,
    UsageEndDate,
    UsageQuantity,
    BlendedCost,
    UnBlendCost
    )
SELECT
TRIM('"' FROM split_part(csv_str,'",',1)),
TRIM('"' FROM split_part(csv_str,'",',2)),
TRIM('"' FROM split_part(csv_str,'",',3)),
TRIM('"' FROM split_part(csv_str,'",',4)),
TRIM('"' FROM split_part(csv_str,'",',5)),
TRIM('"' FROM split_part(csv_str,'",',6)),
TRIM('"' FROM split_part(csv_str,'",',7)),
TRIM('"' FROM split_part(csv_str,'",',8)),
TRIM(',"' FROM split_part(csv_str,'",',9)),
TRIM('"' FROM split_part(csv_str,'",',10)),
TRIM(',"' FROM split_part(csv_str,'",',11)),
TRIM('"' FROM split_part(csv_str,'",',12)),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',13)),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',14)),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',15)),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(TRIM(',"' FROM split_part(csv_str,'",',16)),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(TRIM(',"' FROM split_part(csv_str,'",',17)),''),'0') as NUMERIC(25, 8))
FROM special_lines
WHERE position('Amazon Elastic Compute Cloud' in csv_str) > 0 AND position('RunInstances' in csv_str) > 0
;

INSERT INTO db_usage.raw_billing (
    InvoiceID,
    PayerAccountId,
    LinkedAccountId,
    RecordType,
    RecordId,
    ProductName,
    RateId,
    ReservedInstance,
    ItemDescription,
    UsageStartDate,
    UsageEndDate,
    UsageQuantity,
    BlendedCost,
    UnBlendCost
    )
SELECT
TRIM('"' FROM split_part(csv_str,'",',1)),
TRIM('"' FROM split_part(csv_str,'",',2)),
TRIM('"' FROM split_part(csv_str,'",',3)),
TRIM('"' FROM split_part(csv_str,'",',4)),
TRIM('"' FROM split_part(csv_str,'",',5)),
TRIM('"' FROM split_part(csv_str,'",',6)),
TRIM('"' FROM split_part(csv_str,'",',7)),
TRIM(',,,,,"' FROM split_part(csv_str,'",',8)),
TRIM('"' FROM split_part(csv_str,'",',9)),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',10)),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',11)),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',12)),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(TRIM(',"' FROM split_part(csv_str,'",',13)),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(TRIM(',"' FROM split_part(csv_str,'",',14)),''),'0') as NUMERIC(25, 8))
FROM special_lines
WHERE position('AWS Support' in csv_str) > 0 and position('Prorated recurring fee' in csv_str) > 0
;

INSERT INTO db_usage.raw_billing (
    InvoiceID,
    PayerAccountId,
    LinkedAccountId,
    RecordType,
    RecordId,
    ProductName,
    RateId,
    ReservedInstance,
    ItemDescription,
    UsageStartDate,
    UsageEndDate,
    BlendedCost,
    UnBlendCost
    )
SELECT
TRIM('"' FROM split_part(csv_str,'",',1)),
TRIM('"' FROM split_part(csv_str,'",',2)),
TRIM('"' FROM split_part(csv_str,'",',3)),
TRIM('"' FROM split_part(csv_str,'",',4)),
TRIM('"' FROM split_part(csv_str,'",',5)),
TRIM('"' FROM split_part(csv_str,'",',6)),
TRIM('"' FROM split_part(csv_str,'",',7)),
TRIM(',,,,,"' FROM split_part(csv_str,'",',8)),
TRIM('"' FROM split_part(csv_str,'",',9)),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',10)),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(TRIM('"' FROM split_part(csv_str,'",',11)),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(TRIM(',,"' FROM split_part(csv_str,'",',12)),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(TRIM(',"' FROM split_part(csv_str,'",',13)),''),'0') as NUMERIC(25, 8))
FROM special_lines
WHERE position('AWS Support' in csv_str) > 0 and position('Prorated recurring fee' in csv_str) = 0
;


INSERT INTO db_usage.raw_billing (
    InvoiceID,
    PayerAccountId,
    LinkedAccountId,
    RecordType,
    RecordId,
    ProductName,
    RateId,
    SubscriptionId,
    PricingPlanId,
    UsageType,
    Operation,
    AvailabilityZone,
    ReservedInstance,
    ItemDescription,
    UsageStartDate,
    UsageEndDate,
    UsageQuantity,
    BlendedRate,
    BlendedCost,
    UnBlendedRate,
    UnBlendCost,
    ResourceId,
    Environment,
    Project,
    Role
    )
SELECT
TRIM('"' FROM split_part(csv_str,'","',1)),
split_part(csv_str,'","',2),
split_part(csv_str,'","',3),
split_part(csv_str,'","',4),
split_part(csv_str,'","',5),
split_part(csv_str,'","',6),
split_part(csv_str,'","',7),
split_part(csv_str,'","',8),
split_part(csv_str,'","',9),
split_part(csv_str,'","',10),
split_part(csv_str,'","',11),
split_part(csv_str,'","',12),
split_part(csv_str,'","',13),
split_part(csv_str,'","',14),
cast(COALESCE(NULLIF(split_part(csv_str,'","',15),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(split_part(csv_str,'","',16),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(split_part(csv_str,'","',17),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',18),''),'0') as NUMERIC(20, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',19),''),'0') as NUMERIC(20, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',20),''),'0') as NUMERIC(20, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',21),''),'0') as NUMERIC(20, 8)),
split_part(csv_str,'","',22),
split_part(csv_str,'","',23),
split_part(csv_str,'","',24),
TRIM('"' FROM split_part(csv_str,'","',25))
FROM db_usage.raw_data_s3
WHERE position('Sign up charge for subscription' in csv_str) = 0
and position('069809866012' in csv_str) = 0
;

INSERT INTO db_usage.raw_billing (
    InvoiceID,
    PayerAccountId,
    LinkedAccountId,
    RecordType,
    RecordId,
    ProductName,
    RateId,
    SubscriptionId,
    PricingPlanId,
    UsageType,
    Operation,
    AvailabilityZone,
    ReservedInstance,
    ItemDescription,
    UsageStartDate,
    UsageEndDate,
    UsageQuantity,
    BlendedRate,
    BlendedCost,
    UnBlendedRate,
    UnBlendCost,
    ResourceId
    )
SELECT
TRIM('"' FROM split_part(csv_str,'","',1)),
split_part(csv_str,'","',2),
split_part(csv_str,'","',3),
split_part(csv_str,'","',4),
split_part(csv_str,'","',5),
split_part(csv_str,'","',6),
split_part(csv_str,'","',7),
split_part(csv_str,'","',8),
split_part(csv_str,'","',9),
split_part(csv_str,'","',10),
split_part(csv_str,'","',11),
split_part(csv_str,'","',12),
split_part(csv_str,'","',13),
split_part(csv_str,'","',14),
cast(COALESCE(NULLIF(split_part(csv_str,'","',15),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(split_part(csv_str,'","',16),''),'1970-01-01 00:00:00') as TIMESTAMP),
cast(COALESCE(NULLIF(split_part(csv_str,'","',17),''),'0') as NUMERIC(25, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',18),''),'0') as NUMERIC(20, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',19),''),'0') as NUMERIC(20, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',18),''),'0') as NUMERIC(20, 8)),
cast(COALESCE(NULLIF(split_part(csv_str,'","',19),''),'0') as NUMERIC(20, 8)),
TRIM('"' FROM split_part(csv_str,'","',20))
FROM db_usage.raw_data_s3
WHERE position('Sign up charge for subscription' in csv_str) = 0
and position('069809866012' in csv_str) > 0
;

DELETE FROM db_usage.billing
WHERE date >= trunc(date_trunc('month', current_date - 5));

INSERT INTO db_usage.billing (
LinkedAccountId
,ProductName
,UsageType
,Operation
,AvailabilityZone
,ReservedInstance
,Date
,UsageQuantity
,UnBlendCost
,Environment
,Project
,Role
,PayerAccountId
,PricingPlanId
,trueCost
--,GameVersion
--,GameSize
)
SELECT
LinkedAccountId
,ProductName
,UsageType
,Operation
,AvailabilityZone
,ReservedInstance
,trunc(UsageStartDate) as Date
,SUM(UsageQuantity)
,SUM(UnBlendCost)
,Environment
,Project
,Role
,PayerAccountId
,PricingPlanId
,SUM(UnBlendCost)
--TODO gameVersion
--TODO gameSize
FROM db_usage.raw_billing
WHERE trunc(UsageStartDate) >= trunc(date_trunc('month', current_date - 5))
GROUP BY 1,2,3,4,5,6,7,10,11,12,13,14;


-- Update RDS, Redshift, EC2 Heavy Usage cost with upfront fee attributed to each month
update db_usage.billing
set trueCost = b.trueCost * (ufm.upfrontfee / ufm.reservedyears / 12 + ufm.monthly) / ufm.monthly
from db_usage.billing b join db_usage.upfront_fee_mapping ufm
ON ufm.pricingplanid = b.pricingplanid and ufm.productname = b.productname and ufm.payeraccountid = b.payeraccountid and ufm.usagetype = b.usagetype
where b.date >= trunc(date_trunc('month', current_date - 5));

-- Update RDS & Redshift daily reserved cost
update db_usage.billing
set trueCost = ufm.hourly * b.usagequantity
from db_usage.billing b join db_usage.upfront_fee_mapping ufm
    on b.productName = ufm.productName
    and b.pricingplanid = ufm.pricingplanid
where b.ReservedInstance = 'Y' AND b.trueCost = 0
AND b.date >= trunc(date_trunc('month', current_date - 5));

-------------------------
--BI cost attribution
--TODO add individual cost
--------------------------
DELETE FROM db_usage.bi_games_share
WHERE date >= trunc(date_trunc('month', current_date - 5));
--EMR/EC2
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    split_part(app_name, '_', 1) as game,
    sum(cnt) as size 
    FROM db_usage.s3_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND productName in ('Amazon Elastic Compute Cloud', 'Amazon Elastic MapReduce')
    GROUP BY 1,2,3,4) b
ON s.date = b.date ;
--redshift
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    db_name as game,
    sum(size) as size 
    FROM db_usage.cluster_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND ProductName = 'Amazon Redshift'
    GROUP BY 1,2,3,4) b
ON s.date = b.date 
where s.date < '2016-08-01';
--bicluster
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    db_name as game,
    sum(size) as size 
    FROM db_usage.cluster_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    and cluster = 'bicluster'
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND ProductName = 'Amazon Redshift' and role = 'bi-redshift-bicluster'
    GROUP BY 1,2,3,4) b
ON s.date = b.date 
where s.date >= '2016-08-01';
--bicluster-ffs
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    db_name as game,
    sum(size) as size 
    FROM db_usage.cluster_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    and cluster = 'bicluster-ffs'
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND ProductName = 'Amazon Redshift' and role = 'bi-redshift-ffs'
    GROUP BY 1,2,3,4) b
ON s.date = b.date 
where s.date >= '2016-08-01';
--bicluster-distribution
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    db_name as game,
    sum(size) as size 
    FROM db_usage.cluster_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    and cluster = 'bicluster-distribution'
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND ProductName = 'Amazon Redshift' and role = 'bi-redshift-distribution'
    GROUP BY 1,2,3,4) b
ON s.date = b.date 
where s.date >= '2016-08-01';
--custom-diandian
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    case when schema_name like 'custom%' then 'rs' else schema_name end as game,
    sum(size) as size 
    FROM db_usage.cluster_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    and cluster = 'custom-diandian'
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND ProductName = 'Amazon Redshift' and role = 'bi-redshift-diandian'
    GROUP BY 1,2,3,4) b
ON s.date = b.date 
where s.date >= '2016-08-01';
--custom-funplus
INSERT INTO db_usage.bi_games_share (
    date,
    game,
    ProductName,
    AvailabilityZone,
    Size,
    TotalCost,
    IndividualCost,
    Role
    )
SELECT 
s.date,
s.game,
b.ProductName,
b.availabilityZone,
s.size,
b.TotalCost,
s.size * 1.0 / sum(s.size) over (partition by s.date, b.productName, b.availabilityZone, b.role) * b.totalCost as individualCost,
b.Role
FROM 
    (SELECT 
    date,
    schema_name as game,
    sum(size) as size 
    FROM db_usage.cluster_usage
    WHERE date >= trunc(date_trunc('month', current_date - 5))
    and cluster = 'custom-funplus'
    GROUP BY 1,2
    ) s
JOIN 
    (SELECT
    date,
    ProductName,
    AvailabilityZone,
    Role,
    sum(trueCost) as TotalCost
    FROM db_usage.billing
    WHERE date >= trunc(date_trunc('month', current_date - 5)) AND
    Environment = 'bi-global' AND ProductName = 'Amazon Redshift' and role = 'bi-redshift-funplus'
    GROUP BY 1,2,3,4) b
ON s.date = b.date 
where s.date >= '2016-08-01';
---------------------------
--utilisation calculation
---------------------------
DELETE FROM db_usage.utilisation_rate
WHERE date >= trunc(date_trunc('month', current_date - 5));

INSERT INTO db_usage.utilisation_rate(
    date
    ,linkedaccountid
    ,productName
    ,zone
    ,instancetype
    ,utilisation
    )
select 
    cost.date as date,
    cost.linkedaccountid as linkedaccountid,
    cost.productname as productname,
    cost.zone as zone,
    cost.instancetype as instancetype,
    case
        when cost.usagequantity * 100.0 / upfront.usagequantity * cost.days > 100 then 100
        else cost.usagequantity * 100.0 / upfront.usagequantity * cost.days
    end as utilisation
from 
    (select 
        linkedaccountid
        ,productname
        ,split_part(usagetype, '-', 1) as zone
        ,split_part(usagetype, ':', 2) as instancetype
        ,date
        ,sum(usagequantity) as usagequantity
        ,date_part('day',last_day(date)) as days
    from db_usage.billing
    Where productname in ('Amazon Redshift', 'Amazon RDS Service') and reservedinstance = 'Y' and strpos(usagetype, 'HeavyUsage') = 0
        AND date >= trunc(date_trunc('month', current_date - 5))
    group by 1,2,3,4,5,7) cost
    join 
    (select 
        productname
        ,split_part(usagetype, '-', 1) as zone
        ,split_part(usagetype, ':', 2) as instancetype
        ,date
        ,sum(usagequantity) as usagequantity
    From db_usage.billing
    Where productname in ('Amazon Redshift', 'Amazon RDS Service') and reservedinstance = 'Y' and strpos(usagetype, 'HeavyUsage') > 0
        AND date >= trunc(date_trunc('month', current_date - 5))
    group by 1,2,3,4) upfront
    on cost.productname = upfront.productname
    and cost.zone = upfront.zone
    and cost.instancetype = upfront.instancetype
    and date_trunc('month',cost.date) = date_trunc('month', upfront.date)
Where cost.date >= trunc(date_trunc('month', current_date - 5));

---------------
--Monthly bill
---------------
TRUNCATE TABLE db_usage.monthly_billing;

INSERT INTO db_usage.monthly_billing (
LinkedAccountId
,ProductName
,UsageType
,Operation
,AvailabilityZone
,ReservedInstance
,Month
,UsageQuantity
,UnBlendCost
,Environment
,Project
,Role
,PayerAccountId
,PricingPlanId
,trueCost
)
SELECT
LinkedAccountId
,ProductName
,UsageType
,Operation
,AvailabilityZone
,ReservedInstance
,substring(Date, 1, 7)
,SUM(UsageQuantity)
,SUM(UnBlendCost)
,Environment
,Project
,Role
,PayerAccountId
,PricingPlanId
,SUM(trueCost)
--TODO gameVersion
--TODO gameSize
FROM db_usage.billing
GROUP BY 1,2,3,4,5,6,7,10,11,12,13,14;

-- delete everday usage because utilisation rate is not 100%
update db_usage.monthly_billing
set truecost = 0
where productname = 'Amazon Redshift' and reservedinstance = 'Y' and strpos(usagetype, 'HeavyUsage') = 0;
-- delete monthly usage because utilisation rate is 100%
update db_usage.monthly_billing
set truecost = 0
where productname = 'Amazon RDS Service' and reservedinstance = 'Y' and strpos(usagetype, 'HeavyUsage') > 0;

update db_usage.monthly_billing
set UnBlendCost = 0
where productname = 'Amazon Redshift' and reservedinstance = 'Y' and strpos(usagetype, 'HeavyUsage') = 0;

update db_usage.monthly_billing
set UnBlendCost = 0
where productname = 'Amazon RDS Service' and reservedinstance = 'Y' and strpos(usagetype, 'HeavyUsage') = 0;

INSERT INTO db_usage.monthly_billing (
Linkedaccountid
,ProductName
,UsageType
,Operation
,AvailabilityZone
,ReservedInstance
,Month
,UsageQuantity
,UnBlendCost
,Environment
,Project
,Role
,PayerAccountId
,PricingPlanId
,trueCost
)
SELECT
case
    --when game in ('bv', 'waf') then '147744843438'
    when game = 'daota' then '838011746804'
    when game = 'legendsawake' then '645037889100'
    when game in ('farm', 'farmeu') then '541015476938'
    when game in ('ff2', 'ff2pc', 'ha') then '924801593386'
    when game = 'ffs' then '735714818384'
    --when game = 'fruitscoot' then '131433655760'
    when game = 'gz' then '464507519694'
    when game = 'mt2' then '972926864295'
    when game = 'poker' then '135620544099'
    when game = 'rs' then '431036357599'
    when game = 'xy' then '634033320744'
    when game = 'littlechef' then '677477847658'
    --when game = 'battlewarship' then '370884182004'
    when game = 'dragonwar' then '889096239409'
    --when game = 'maitaimadness' then '860495783590'
    when game = 'wartide' then '647268852127'
    else '921287511715'
end as Linkedaccountid
,ProductName
,'BI Usage' as UsageType
,'BI Usage' as Operation
,AvailabilityZone
,null as ReservedInstance
,substring(Date, 1, 7)
,sum(0) as UsageQuantity
,sum(0) as UnBlendCost
,null as Environment
,null as Project
,Role
,null as PayerAccountId
,null as PricingPlanId
,sum(IndividualCost)
FROM db_usage.bi_games_share
WHERE date >= '2016-03-01'
GROUP BY 1,2,3,4,5,6,7,10,11,12,13,14;

INSERT INTO db_usage.monthly_billing (
Linkedaccountid
,ProductName
,UsageType
,Operation
,AvailabilityZone
,ReservedInstance
,Month
,UsageQuantity
,UnBlendCost
,Environment
,Project
,Role
,PayerAccountId
,PricingPlanId
,trueCost
)
SELECT
'921287511715' as Linkedaccountid
,ProductName
,'BI Usage' as UsageType
,'BI Usage' as Operation
,AvailabilityZone
,null as ReservedInstance
,substring(Date, 1, 7)
,sum(0) as UsageQuantity
,sum(0) as UnBlendCost
,null as Environment
,null as Project
,Role
,null as PayerAccountId
,null as PricingPlanId
,-1 * sum(IndividualCost)
FROM db_usage.bi_games_share
WHERE date >= '2016-03-01'
GROUP BY 1,2,3,4,5,6,7,10,11,12,13,14;

update db_usage.monthly_billing
set Linkedaccountid = 'Payment'
where Linkedaccountid = '583300917620' and project = 'payment';
update db_usage.monthly_billing
set Linkedaccountid = 'bv'
where Linkedaccountid = '147744843438' and project = 'bv';
CREATE TABLE third_party.fyber_data
(
	date Date,
	application_id INTEGER,
	application_name CHAR(100) ENCODE bytedict,
	ad_format VARCHAR(30) ENCODE bytedict,
	ad_network VARCHAR(50) ENCODE bytedict,
	country VARCHAR(4) ENCODE bytedict, 
	fills INTEGER,
	impressions INTEGER,
	completions INTEGER,
	revenue_usd NUMERIC(14, 4),
	ecpm_usd NUMERIC(14, 4)
)
DISTSTYLE EVEN
interleaved SORTKEY
(
	date,
	ad_network
);
------------------------------------------------
--upshot 2.0 fact_dau_snapshot.sql
------------------------------------------------
----- insert data to fact_dau_snapshot
DELETE FROM processed.fact_dau_snapshot
WHERE date >= 
           (
             select start_date 
             from processed.init_start_date
           );

insert into processed.fact_dau_snapshot
(
           id                  
           ,user_key            
           ,date                
           ,app_id              
           ,app_version         
           ,level_start         
           ,level_end           
           ,os                  
           ,os_version          
           ,device              
           ,browser             
           ,browser_version     
           ,country             
           ,language            
           ,is_new_user         
           ,is_payer            
           ,is_converted_today  
           ,revenue_usd         
           ,payment_cnt         
           ,session_cnt         
           ,playtime_sec 
           ,device_resolution  
           ,location           
           ,device_storage     
           ,network            
           ,carrier       
)
select     md5(s.app_id||s.date||s.user_id) as id
          ,s.user_key
          ,s.date
          ,s.app_id
          ,s.app_version
          ,s.level_start
          ,s.level_end
          ,s.os
          ,s.os_version
          ,s.device
          ,s.browser
          ,s.browser_version
          ,s.country
          ,s.language
          ,CASE                                               
                  WHEN s.date=s.install_date THEN           
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_new_user                            
              ,CASE                                         
                  WHEN s.date >= trunc(p.conversion_ts) THEN
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_payer                               
              ,CASE                                         
                  WHEN s.date = trunc(p.conversion_ts) THEN 
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_converted_today                     
          ,COALESCE(pd.revenue_usd,0) AS revenue_usd    
          ,COALESCE(pd.purchase_cnt,0) AS payment_cnt   
          ,s.session_cnt                                
          ,s.playtime_sec
          ,s.device_resolution  
          ,s.location           
          ,s.device_storage     
          ,s.network            
          ,s.carrier   
FROM      processed.tmp_user_daily_login s
LEFT JOIN processed.tmp_user_payment pd
ON        s.user_key=pd.user_key
AND       s.date=pd.date
LEFT JOIN
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
        FROM processed.tmp_user_payment
        GROUP BY 1
    ) AS p
ON        p.user_key=s.user_key
WHERE     s.date >= 
                 (
                   select start_date 
                   from processed.init_start_date
                 )
;

-- Attempt to backfill missing row on install date

insert into processed.fact_dau_snapshot
(
    id
    ,user_key
    ,date
    ,app_id
    ,app_version
    ,level_start
    ,level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,is_new_user
    ,is_payer
    ,is_converted_today
    ,revenue_usd
    ,payment_cnt
    ,session_cnt
    ,playtime_sec
    ,device_resolution  
    ,location           
    ,device_storage     
    ,network            
    ,carrier 
)
WITH missing_install_date AS
(
    SELECT    DISTINCT
              l.user_key
             ,l.install_date
    FROM      processed.tmp_user_daily_login l
    LEFT JOIN processed.fact_dau_snapshot i
    ON        l.user_key = i.user_key
    AND       l.install_date = i.date
    WHERE     l.install_date < (select start_date from processed.init_start_date) 
    AND       i.user_key is null
    UNION
    SELECT     DISTINCT
               l.user_key
              ,l.install_date
    FROM      processed.tmp_user_daily_login l
    LEFT JOIN processed.tmp_user_daily_login i
        ON    l.user_key = i.user_key
        AND   l.install_date = i.date
    WHERE     l.install_date >= (select start_date from processed.init_start_date)
      AND     i.user_key is null
)
SELECT
     MD5(user_key || install_date) AS id
    ,user_key
    ,install_date as date
    ,app_id
    ,app_version
    ,1 AS level_start
    ,level_start as level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,1 AS is_new_user
    ,0 AS is_payer
    ,0 AS is_converted_today
    ,0 AS revenue_usd
    ,0 AS purchase_cnt
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,device_resolution  
    ,location           
    ,device_storage     
    ,network            
    ,carrier 
FROM
    (
    SELECT
        d.*
        ,m.install_date
        ,row_number() over (partition by d.user_key order by d.date asc) as row
    FROM processed.fact_dau_snapshot d
    JOIN missing_install_date m
        ON  d.user_key = m.user_key
    WHERE d.date > m.install_date
    )  t
WHERE t.row=1
;

delete from processed.fact_dau_snapshot where date = current_date;
-------------------------------------------------
--upshot 2.0 dim_user.sql
-------------------------------------------------

DROP TABLE IF EXISTS tmp_dim_user;
CREATE TEMP TABLE tmp_dim_user AS
WITH
    last_info AS
    (
        SELECT
            *
            ,row_number() over (partition by user_key order by date desc) as row
        FROM processed.tmp_user_daily_login 
    )
    ,install_info AS
    (
        SELECT
             user_key
            ,language
            ,install_source
            ,country
            ,os
            ,device
            ,browser
            ,gender
        FROM processed.tmp_user_daily_login 
        where install_date = date
        
    )
    ,payment_info AS
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
            ,max(total_revenue_usd) as total_revenue_usd
            ,max(total_purchase_cnt) as total_purchase_cnt
        FROM processed.tmp_user_payment
        GROUP BY 1
    )

SELECT DISTINCT
  tu.user_key AS id
  ,tu.user_key
  ,tu.app_id
  ,tu.app_version
  ,tu.user_id
  ,COALESCE(du.facebook_id, tu.facebook_id) AS facebook_id
  ,tu.install_ts
  ,tu.install_date
  ,coalesce(ti.install_source, 'Organic') AS install_source
  ,null AS install_subpublisher
  ,null AS install_campaign
  ,coalesce(ti.language,du.install_language) AS install_language
  ,coalesce(ti.country, du.install_country) AS install_country
  ,coalesce(ti.os, du.install_os) AS install_os
  ,coalesce(ti.device, du.install_device) AS install_device
  ,coalesce(ti.browser, du.install_browser) AS install_browser
  ,coalesce(ti.gender, du.install_gender) AS install_gender
  ,tu.language
  ,tu.birthday
  ,tu.first_name
  ,tu.last_name
  ,tu.gender
  ,tu.country
  ,COALESCE (tu.email, du.email) AS email
  ,tu.os
  ,tu.os_version
  ,tu.device
  ,tu.browser
  ,tu.browser_version
  ,COALESCE (tu.last_ip, du.last_ip) AS last_ip
  ,COALESCE(tu.level_end, tu.level_start,du.level) AS level
  ,CASE WHEN COALESCE(tp.total_revenue_usd, 0) > 0 THEN 1 ELSE 0 END AS is_payer
  ,tp.conversion_ts
  ,COALESCE(tp.total_revenue_usd,0) AS revenue_usd
  ,COALESCE(tp.total_purchase_cnt,0) AS payment_cnt
  ,date(tu.last_login_ts) as last_login_date
  ,tu.device_resolution
  ,tu.location         
  ,tu.device_storage   
  ,tu.network          
  ,tu.carrier                        
FROM last_info tu
LEFT JOIN install_info ti
    ON ti.user_key=tu.user_key
LEFT JOIN processed.dim_user du
    ON tu.user_key = du.user_key
LEFT JOIN payment_info tp
    ON tp.user_key=tu.user_key
WHERE tu.row=1
;

DELETE FROM processed.dim_user
USING processed.tmp_user_daily_login d
WHERE processed.dim_user.user_key=d.user_key;

INSERT INTO processed.dim_user
(
   id                  
  ,user_key            
  ,app_id              
  ,app_version         
  ,user_id             
  ,facebook_id         
  ,install_ts          
  ,install_date        
  ,install_source      
  ,install_subpublisher
  ,install_campaign    
  ,install_language    
  ,install_country     
  ,install_os          
  ,install_device      
  ,install_browser     
  ,install_gender      
  ,language            
  ,birthday            
  ,first_name          
  ,last_name           
  ,gender              
  ,country             
  ,email               
  ,os                  
  ,os_version          
  ,device              
  ,browser             
  ,browser_version     
  ,last_ip             
  ,level               
  ,is_payer            
  ,conversion_ts       
  ,revenue_usd         
  ,payment_cnt         
  ,last_login_date     
  ,device_resolution   
  ,location            
  ,device_storage      
  ,network             
  ,carrier             
)
SELECT
   id                  
  ,user_key            
  ,app_id              
  ,app_version         
  ,user_id             
  ,facebook_id         
  ,install_ts          
  ,install_date        
  ,install_source      
  ,install_subpublisher
  ,install_campaign    
  ,install_language    
  ,install_country     
  ,install_os          
  ,install_device      
  ,install_browser     
  ,install_gender      
  ,language            
  ,birthday            
  ,first_name          
  ,last_name           
  ,gender              
  ,country             
  ,email               
  ,os                  
  ,os_version          
  ,device              
  ,browser             
  ,browser_version     
  ,last_ip             
  ,level               
  ,is_payer            
  ,conversion_ts       
  ,revenue_usd         
  ,payment_cnt         
  ,last_login_date     
  ,device_resolution   
  ,location            
  ,device_storage      
  ,network             
  ,carrier       
FROM tmp_dim_user
;

--delete from processed.dim_user where install_date is null;

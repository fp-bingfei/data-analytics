------------------------------------------------
--upshot 2.0 tmp_user_daily_login.sql
------------------------------------------------

delete from  processed.tmp_user_daily_login
where  date  >=  (
                    select start_date 
                    from   processed.init_start_date
                  );

insert into processed.tmp_user_daily_login
SELECT DISTINCT
            s.date_start AS date
           ,s.app_id
           ,s.user_key
           ,s.user_id
           ,last_value(s.ts_start ignore nulls)
               OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_login_ts
           ,last_value(s.facebook_id ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
           ,last_value(s.birthday ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
            ,last_value(s.first_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
            ,last_value(s.last_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
           ,substring(last_value(s.email ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_ts ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts 
                    THEN
                        u.install_ts
               ELSE
                   min(s.install_ts ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_ts
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_date ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN min(s.install_date ignore nulls)
                       OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                       ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date 
                    THEN
                       u.install_date
               ELSE
                   min(s.install_date ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_date
           ,last_value(s.app_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS app_version
           ,first_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS level_start
           ,last_value(s.level_end ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS level_end
           ,last_value(s.os ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os
           ,last_value(s.os_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os_version
           ,last_value(s.country ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS country
           ,last_value(s.ip ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ip
           ,last_value(s.install_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS install_source
           ,last_value(s.language ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS language
           ,last_value(s.gender ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
           ,last_value(s.device ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
           AS device
           ,last_value(s.browser ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser
           ,last_value(s.browser_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser_version
           ,sum(1)
               OVER (PARTITION BY s.date_start, s.user_key)
            AS session_cnt
           ,sum(playtime_sec)
               OVER (PARTITION BY s.date_start, s.user_key )
            AS playtime_sec
           ,last_value(s.device_resolution ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS device_resolution
           ,last_value(s.location ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS location
           ,last_value(s.device_storage ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS device_storage
           ,last_value(s.network ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS network
           ,last_value(s.carrier ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS carrier
FROM      processed.fact_session s
LEFT JOIN processed.dim_user u
ON        u.user_key=s.user_key
WHERE     s.date_start >= 
                  (
                    select start_date 
                    from   processed.init_start_date
                  );


-- Attempt to catch missing users from fact_new_user

INSERT INTO processed.tmp_user_daily_login
SELECT DISTINCT
     n.date_start
    ,n.app_id
    ,n.user_key
    ,n.user_id
    ,last_value(n.ts_start ignore nulls)
        OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,last_value(n.facebook_id ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
    ,last_value(n.birthday ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
     ,last_value(n.first_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
     ,last_value(n.last_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
    ,substring(last_value(n.email ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_ts ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(n.install_ts ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_date ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(n.install_date ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(n.app_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(n.level_start ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_start
    ,last_value(n.level_end ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(n.os ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(n.os_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(n.country ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(n.ip ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(n.install_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(n.language ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(n.gender ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
    ,last_value(n.device ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(n.browser ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(n.browser_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,1 AS session_cnt
    ,0 AS playtime_sec
FROM processed.fact_new_user n
LEFT JOIN processed.tmp_user_daily_login s
    ON  s.user_key=n.user_key
    AND s.date=n.date_start
LEFT JOIN processed.dim_user u
    ON  u.user_key=s.user_key
WHERE n.date_start >= 
           (
             select start_date 
             from   processed.init_start_date
            )
AND s.user_key is null
;

--update the user info where level_end < level_start
update processed.tmp_user_daily_login
set level_start = level_end
where level_end < level_start;


--update the install_ts info
create temp table tmp_user_last_day as
select t.*
from
    (
      select *
             ,row_number() OVER (partition by user_key order by date_start desc) as row
      from processed.fact_session ds
      where ds.install_date is not null
     ) t
where row = 1;

update processed.tmp_user_daily_login
set    install_date = t.install_date,
       install_ts = t.install_ts,
       device = t.device,
       country = t.country,
       language = t.language,
       browser = t.browser,
       browser_version = t.browser_version,
       os = t.os,
       os_version = t.os_version
from   tmp_user_last_day t
where  processed.tmp_user_daily_login.install_date is null 
and    processed.tmp_user_daily_login.user_key = t.user_key;


------------------------------------------------
--upshot 2.0 tmp_user_payment.sql
------------------------------------------------

delete from processed.tmp_user_payment
where date >=
          (
                    select start_date 
                    from   processed.init_start_date
          );

insert into processed.tmp_user_payment
select  DISTINCT
         user_key
        ,date
        ,min(r.ts) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)                      AS conversion_ts
        ,sum(revenue_usd) over (partition by r.user_key, date ORDER BY ts ASC rows between unbounded preceding and unbounded following)         AS revenue_usd
        ,sum(revenue_usd) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)               AS total_revenue_usd
        ,count(transaction_id) over (partition by r.user_key, date ORDER BY ts ASC rows between unbounded preceding and unbounded following )   AS purchase_cnt
        ,count(transaction_id) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)          AS total_purchase_cnt
FROM processed.fact_revenue r
where date >=
          (
                    select start_date 
                    from   processed.init_start_date
          )
;
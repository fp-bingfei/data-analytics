------------------------------------------------
--upshot 2.0 fact_session.sql
------------------------------------------------

delete from processed.fact_session
where date_start >= 
                 (
                    select start_date 
                    from   processed.init_start_date
                 );

insert into processed.fact_session
select     md5(s.app_id||s.user_id||s.session_id) as id
          ,s.app_id 
          ,s.app_version
          ,md5(s.app_id||s.user_id) as user_key
          ,s.user_id
          ,trunc(s.ts_pretty) as date_start
          ,trunc(e.ts_pretty) as date_end
          ,s.ts_pretty as ts_start
          ,e.ts_pretty as ts_end
          ,s.install_ts     
          ,trunc(s.install_ts) as install_date     
          ,s.session_id     
          ,s.facebook_id    
          ,s.install_source 
          ,s.os             
          ,s.os_version     
          ,s.browser        
          ,s.browser_version
          ,s.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,s.email          
          ,s.first_name     
          ,s.last_name      
          ,s.level as level_start
          ,null as level_end
          ,s.vip_level
          ,s.gender         
          ,s.birthday       
          ,s.ip             
          ,s.lang as language
          ,coalesce(e.session_length,0) as playtime_sec
          ,s.device_resolution  
          ,s.location      
          ,s.device_storage
          ,s.network       
          ,s.carrier       
from      processed.session_start s
left join processed.session_end e
on        s.session_id = e.session_id
and       s.app_id = e.app_id
and       s.user_id = e.user_id   
and       trunc(e.ts_pretty) >=
                 (
                    select start_date 
                    from   processed.init_start_date
                 )
left join processed.dim_country c
on        s.country_code = c.country_code
where     trunc(s.ts_pretty) >=
                 (
                    select start_date 
                    from   processed.init_start_date
                 );


------------------------------------------------
--upshot 2.0 fact_new_user.sql
------------------------------------------------

delete from processed.fact_new_user
where date_start >= 
                 (
                    select start_date 
                    from   processed.init_start_date
                 );


insert into  processed.fact_new_user
select     md5(u.app_id||u.user_id||u.session_id) as id
          ,u.app_id 
          ,u.app_version
          ,md5(u.app_id||u.user_id) as user_key
          ,u.user_id
          ,trunc(u.ts_pretty) as date_start
          ,null as date_end
          ,u.ts_pretty as ts_start
          ,null as ts_end
          ,u.install_ts     
          ,trunc(u.install_ts) as install_date     
          ,u.session_id     
          ,u.facebook_id    
          ,u.install_source 
          ,u.os             
          ,u.os_version     
          ,u.browser        
          ,u.browser_version
          ,u.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,u.email          
          ,u.first_name     
          ,u.last_name      
          ,u.level as level_start
          ,u.level as level_end
          ,u.vip_level
          ,u.gender         
          ,u.birthday       
          ,u.ip             
          ,u.lang as language
          ,device_resolution 
          ,location          
          ,device_storage    
          ,network           
          ,carrier           
from      processed.new_user u
left join processed.dim_country c
on        u.country_code = c.country_code
where     trunc(u.ts_pretty) >=
                 (
                    select start_date 
                    from   processed.init_start_date
                 );

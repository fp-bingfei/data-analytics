----------------------------------------------
--upshot 2.0 agg_kpi.sql
----------------------------------------------

--prepare the video_load data:

create temp table agg_video_load
as
select     d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as video_load_users
          ,sum(1) as video_load_cnt             
from    processed.video_load l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;


--prepare the video_capture data

create temp table agg_video_capture
as
select    
           d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as video_capture_users
          ,sum(1) as video_capture_cnt             
from    processed.video_capture l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;

--prepare the video_save data

create temp table agg_video_save
as
select    
           d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as video_save_users
          ,sum(1) as video_save_cnt             
from    processed.video_save l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;


--prepare the share_facebook data

create temp table agg_share_facebook
as
select    
           d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as share_facebook_users
          ,sum(1) as share_facebook_cnt             
from    processed.share_facebook l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;

--prepare the share_facebookmessenger data

create temp table agg_share_facebookmessenger
as
select    
           d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as share_facebookmessenger_users
          ,sum(1) as share_facebookmessenger_cnt             
from    processed.share_facebookmessenger l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;


--prepare the share_wechat data

create temp table agg_share_wechat
as
select    
           d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as share_wechat_users
          ,sum(1) as share_wechat_cnt             
from    processed.share_wechat l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;

--prepare the share_system_result data

create temp table agg_share_system_result
as
select    
           d.date 
	,d.app_id 
	,d.app_version 
	,EXTRACT(year FROM u.install_date) as install_year
          ,EXTRACT(month FROM u.install_date) as install_month
          ,EXTRACT(week FROM u.install_date) as install_week
	,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
          ,u.install_source
	,d.level_start 
	,d.browser 
	,d.browser_version 
	,d.device 
	,d.country 
	,d.os 
	,d.os_version 
	,d.language 
	,d.is_new_user 
	,d.is_payer 
	,d.device_resolution   
          ,d.location            
          ,d.device_storage      
          ,d.network             
          ,d.carrier 
          ,count(distinct md5(l.app_id||l.user_id)) as share_system_result_users
          ,sum(1) as share_system_result_cnt             
from    processed.share_system_result l
left join processed.fact_dau_snapshot d
on d.user_key = md5(l.app_id||l.user_id)
and date(l.ts_pretty) = d.date
join processed.dim_user u
on u.user_key = d.user_key
and d.date >=(select start_date from processed.init_start_date)
where trunc(l.ts_pretty) >=(select date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;

--prepare the kpi data
create temp table tmp_agg_kpi
as
select
      d.date
      ,d.app_id
      ,d.app_version
      ,EXTRACT(year FROM u.install_date) as install_year
      ,EXTRACT(month FROM u.install_date) as install_month
      ,EXTRACT(week FROM u.install_date) as install_week
      ,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
      ,u.install_source
      ,d.level_start
      ,d.browser
      ,d.browser_version
      ,d.device
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.is_new_user
      ,d.is_payer
      ,d.device_resolution 
      ,d.location          
      ,d.device_storage    
      ,d.network           
      ,d.carrier
      ,m.metric_name           
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
from processed.fact_dau_snapshot d
join processed.dim_user u on d.user_key=u.user_key
join processed.kpi_metric m
on 1 = 1
where date >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24;


--prepare video_load_cube
create temp table video_load_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(l.video_load_users,0) as video_load_users 
      ,coalesce(l.video_load_cnt,0) as video_load_cnt 
from tmp_agg_kpi k
left join agg_video_load l
on  k.date                 = l.date                     
and k.app_id               = l.app_id                   
and k.app_version          = l.app_version              
and k.install_year         = l.install_year             
and k.install_month        = l.install_month            
and k.install_week         = l.install_week             
and k.install_source_group = l.install_source_group     
and k.install_source       = l.install_source           
and k.level_start          = l.level_start                
and coalesce(k.browser,'Unknown') = coalesce(l.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(l.browser,'Unknown') 
and k.device               = l.device                   
and k.country              = l.country                  
and k.os                   = l.os                       
and k.os_version           = l.os_version               
and k.language             = l.language                 
and k.is_new_user          = l.is_new_user              
and k.is_payer             = l.is_payer                 
and k.device_resolution    = l.device_resolution        
and k.location             = l.location                 
and k.device_storage       = l.device_storage           
and k.network              = l.network                  
and k.carrier              = l.carrier 
where k.metric_name = 'video_load'
;

--prepare video_capture_cube

create temp table video_capture_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(c.video_capture_users,0) as video_capture_users 
      ,coalesce(c.video_capture_cnt,0) as video_capture_cnt 
from tmp_agg_kpi k
left join agg_video_capture c
on  k.date                 = c.date                     
and k.app_id               = c.app_id                   
and k.app_version          = c.app_version              
and k.install_year         = c.install_year             
and k.install_month        = c.install_month            
and k.install_week         = c.install_week             
and k.install_source_group = c.install_source_group     
and k.install_source       = c.install_source           
and k.level_start          = c.level_start               
and coalesce(k.browser,'Unknown') = coalesce(c.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(c.browser,'Unknown')         
and k.device               = c.device                   
and k.country              = c.country                  
and k.os                   = c.os                       
and k.os_version           = c.os_version               
and k.language             = c.language                 
and k.is_new_user          = c.is_new_user              
and k.is_payer             = c.is_payer                 
and k.device_resolution    = c.device_resolution        
and k.location             = c.location                 
and k.device_storage       = c.device_storage           
and k.network              = c.network                  
and k.carrier              = c.carrier 
where k.metric_name = 'video_capture'
;


--prepare video_save_cube

create temp table video_save_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(c.video_save_users,0) as video_save_users 
      ,coalesce(c.video_save_cnt,0) as video_save_cnt 
from tmp_agg_kpi k
left join agg_video_save c
on  k.date                 = c.date                     
and k.app_id               = c.app_id                   
and k.app_version          = c.app_version              
and k.install_year         = c.install_year             
and k.install_month        = c.install_month            
and k.install_week         = c.install_week             
and k.install_source_group = c.install_source_group     
and k.install_source       = c.install_source           
and k.level_start          = c.level_start               
and coalesce(k.browser,'Unknown') = coalesce(c.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(c.browser,'Unknown')         
and k.device               = c.device                   
and k.country              = c.country                  
and k.os                   = c.os                       
and k.os_version           = c.os_version               
and k.language             = c.language                 
and k.is_new_user          = c.is_new_user              
and k.is_payer             = c.is_payer                 
and k.device_resolution    = c.device_resolution        
and k.location             = c.location                 
and k.device_storage       = c.device_storage           
and k.network              = c.network                  
and k.carrier              = c.carrier 
where k.metric_name = 'video_save'
;

--prepare share_facebook_cube

create temp table share_facebook_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(c.share_facebook_users,0) as share_facebook_users 
      ,coalesce(c.share_facebook_cnt,0) as share_facebook_cnt 
from tmp_agg_kpi k
left join agg_share_facebook c
on  k.date                 = c.date                     
and k.app_id               = c.app_id                   
and k.app_version          = c.app_version              
and k.install_year         = c.install_year             
and k.install_month        = c.install_month            
and k.install_week         = c.install_week             
and k.install_source_group = c.install_source_group     
and k.install_source       = c.install_source           
and k.level_start          = c.level_start               
and coalesce(k.browser,'Unknown') = coalesce(c.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(c.browser,'Unknown')         
and k.device               = c.device                   
and k.country              = c.country                  
and k.os                   = c.os                       
and k.os_version           = c.os_version               
and k.language             = c.language                 
and k.is_new_user          = c.is_new_user              
and k.is_payer             = c.is_payer                 
and k.device_resolution    = c.device_resolution        
and k.location             = c.location                 
and k.device_storage       = c.device_storage           
and k.network              = c.network                  
and k.carrier              = c.carrier 
where k.metric_name = 'share_facebook'
;


--prepare share_facebookmessenger_cube

create temp table share_facebookmessenger_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(c.share_facebookmessenger_users,0) as share_facebookmessenger_users 
      ,coalesce(c.share_facebookmessenger_cnt,0) as share_facebookmessenger_cnt 
from tmp_agg_kpi k
left join agg_share_facebookmessenger c
on  k.date                 = c.date                     
and k.app_id               = c.app_id                   
and k.app_version          = c.app_version              
and k.install_year         = c.install_year             
and k.install_month        = c.install_month            
and k.install_week         = c.install_week             
and k.install_source_group = c.install_source_group     
and k.install_source       = c.install_source           
and k.level_start          = c.level_start               
and coalesce(k.browser,'Unknown') = coalesce(c.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(c.browser,'Unknown')         
and k.device               = c.device                   
and k.country              = c.country                  
and k.os                   = c.os                       
and k.os_version           = c.os_version               
and k.language             = c.language                 
and k.is_new_user          = c.is_new_user              
and k.is_payer             = c.is_payer                 
and k.device_resolution    = c.device_resolution        
and k.location             = c.location                 
and k.device_storage       = c.device_storage           
and k.network              = c.network                  
and k.carrier              = c.carrier 
where k.metric_name = 'share_facebookmessenger'
;


--prepare share_system_result_cube

create temp table share_system_result_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(c.share_system_result_users,0) as share_system_result_users 
      ,coalesce(c.share_system_result_cnt,0) as share_system_result_cnt 
from tmp_agg_kpi k
left join agg_share_system_result c
on  k.date                 = c.date                     
and k.app_id               = c.app_id                   
and k.app_version          = c.app_version              
and k.install_year         = c.install_year             
and k.install_month        = c.install_month            
and k.install_week         = c.install_week             
and k.install_source_group = c.install_source_group     
and k.install_source       = c.install_source           
and k.level_start          = c.level_start               
and coalesce(k.browser,'Unknown') = coalesce(c.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(c.browser,'Unknown')         
and k.device               = c.device                   
and k.country              = c.country                  
and k.os                   = c.os                       
and k.os_version           = c.os_version               
and k.language             = c.language                 
and k.is_new_user          = c.is_new_user              
and k.is_payer             = c.is_payer                 
and k.device_resolution    = c.device_resolution        
and k.location             = c.location                 
and k.device_storage       = c.device_storage           
and k.network              = c.network                  
and k.carrier              = c.carrier 
where k.metric_name = 'share_system_result'
;

--prepare share_wechat_cube

create temp table share_wechat_cube
as
select k.date                     
      ,k.app_id                   
      ,k.app_version              
      ,k.install_year             
      ,k.install_month            
      ,k.install_week             
      ,k.install_source_group     
      ,k.install_source           
      ,k.level_start                
      ,k.browser                  
      ,k.browser_version          
      ,k.device                   
      ,k.country                  
      ,k.os                       
      ,k.os_version               
      ,k.language                 
      ,k.is_new_user              
      ,k.is_payer                 
      ,k.device_resolution        
      ,k.location                 
      ,k.device_storage           
      ,k.network                  
      ,k.carrier                  
      ,k.new_user_cnt             
      ,k.dau_cnt                  
      ,k.new_payer_cnt             
      ,k.payer_today_cnt          
      ,k.payment_cnt              
      ,k.revenue_usd              
      ,k.session_cnt              
      ,k.playtime_sec             
      ,coalesce(c.share_wechat_users,0) as share_wechat_users 
      ,coalesce(c.share_wechat_cnt,0) as share_wechat_cnt 
from tmp_agg_kpi k
left join agg_share_wechat c
on  k.date                 = c.date                     
and k.app_id               = c.app_id                   
and k.app_version          = c.app_version              
and k.install_year         = c.install_year             
and k.install_month        = c.install_month            
and k.install_week         = c.install_week             
and k.install_source_group = c.install_source_group     
and k.install_source       = c.install_source           
and k.level_start          = c.level_start               
and coalesce(k.browser,'Unknown') = coalesce(c.browser,'Unknown')                
and coalesce(k.browser_version,'Unknown') = coalesce(c.browser,'Unknown')         
and k.device               = c.device                   
and k.country              = c.country                  
and k.os                   = c.os                       
and k.os_version           = c.os_version               
and k.language             = c.language                 
and k.is_new_user          = c.is_new_user              
and k.is_payer             = c.is_payer                 
and k.device_resolution    = c.device_resolution        
and k.location             = c.location                 
and k.device_storage       = c.device_storage           
and k.network              = c.network                  
and k.carrier              = c.carrier 
where k.metric_name = 'share_wechat'
;


delete from processed.agg_kpi
where date >=(select start_date from processed.init_start_date);


insert into processed.agg_kpi
select t1.date                     
      ,t1.app_id                   
      ,t1.app_version              
      ,t1.install_year             
      ,t1.install_month            
      ,t1.install_week             
      ,t1.install_source_group     
      ,t1.install_source           
      ,t1.level_start                
      ,t1.browser                  
      ,t1.browser_version          
      ,t1.device                   
      ,t1.country                  
      ,t1.os                       
      ,t1.os_version               
      ,t1.language                 
      ,t1.is_new_user              
      ,t1.is_payer                 
      ,t1.device_resolution        
      ,t1.location                 
      ,t1.device_storage           
      ,t1.network                  
      ,t1.carrier                  
      ,t1.new_user_cnt             
      ,t1.dau_cnt                  
      ,t1.new_payer_cnt             
      ,t1.payer_today_cnt          
      ,t1.payment_cnt              
      ,t1.revenue_usd              
      ,t1.session_cnt              
      ,t1.playtime_sec             
      ,t1.video_load_users 
      ,t1.video_load_cnt 
      ,t2.video_capture_users 
      ,t2.video_capture_cnt 
      ,t3.video_save_users 
      ,t3.video_save_cnt 
      ,t4.share_facebook_users 
      ,t4.share_facebook_cnt 
      ,t5.share_facebookmessenger_users 
      ,t5.share_facebookmessenger_cnt 
      ,t6.share_system_result_users 
      ,t6.share_system_result_cnt 
      ,t7.share_wechat_users 
      ,t7.share_wechat_cnt 
from video_load_cube t1
left join  video_capture_cube t2  
on  t1.date                 = t2.date                     
and t1.app_id               = t2.app_id                   
and t1.app_version          = t2.app_version              
and t1.install_year         = t2.install_year             
and t1.install_month        = t2.install_month            
and t1.install_week         = t2.install_week             
and t1.install_source_group = t2.install_source_group     
and t1.install_source       = t2.install_source           
and t1.level_start            = t2.level_start                
and coalesce(t1.browser,'Unknown') = coalesce(t2.browser,'Unknown')                
and coalesce(t1.browser_version,'Unknown') = coalesce(t2.browser,'Unknown')          
and t1.device               = t2.device                   
and t1.country              = t2.country                  
and t1.os                   = t2.os                       
and t1.os_version           = t2.os_version               
and t1.language             = t2.language                 
and t1.is_new_user          = t2.is_new_user              
and t1.is_payer             = t2.is_payer                 
and t1.device_resolution    = t2.device_resolution        
and t1.location             = t2.location                 
and t1.device_storage       = t2.device_storage           
and t1.network              = t2.network                  
and t1.carrier              = t2.carrier 
left join video_save_cube t3  
on  t1.date                 = t3.date                     
and t1.app_id               = t3.app_id                   
and t1.app_version          = t3.app_version              
and t1.install_year         = t3.install_year             
and t1.install_month        = t3.install_month            
and t1.install_week         = t3.install_week             
and t1.install_source_group = t3.install_source_group     
and t1.install_source       = t3.install_source           
and t1.level_start            = t3.level_start                
and coalesce(t1.browser,'Unknown') = coalesce(t3.browser,'Unknown')                
and coalesce(t1.browser_version,'Unknown') = coalesce(t3.browser,'Unknown')          
and t1.device               = t3.device                   
and t1.country              = t3.country                  
and t1.os                   = t3.os                       
and t1.os_version           = t3.os_version               
and t1.language             = t3.language                 
and t1.is_new_user          = t3.is_new_user              
and t1.is_payer             = t3.is_payer                 
and t1.device_resolution    = t3.device_resolution        
and t1.location             = t3.location                 
and t1.device_storage       = t3.device_storage           
and t1.network              = t3.network                  
and t1.carrier              = t3.carrier 
left join share_facebook_cube t4  
on  t1.date                 = t4.date                     
and t1.app_id               = t4.app_id                   
and t1.app_version          = t4.app_version              
and t1.install_year         = t4.install_year             
and t1.install_month        = t4.install_month            
and t1.install_week         = t4.install_week             
and t1.install_source_group = t4.install_source_group     
and t1.install_source       = t4.install_source           
and t1.level_start            = t4.level_start                
and coalesce(t1.browser,'Unknown') = coalesce(t4.browser,'Unknown')                
and coalesce(t1.browser_version,'Unknown') = coalesce(t4.browser,'Unknown')          
and t1.device               = t4.device                   
and t1.country              = t4.country                  
and t1.os                   = t4.os                       
and t1.os_version           = t4.os_version               
and t1.language             = t4.language                 
and t1.is_new_user          = t4.is_new_user              
and t1.is_payer             = t4.is_payer                 
and t1.device_resolution    = t4.device_resolution        
and t1.location             = t4.location                 
and t1.device_storage       = t4.device_storage           
and t1.network              = t4.network                  
and t1.carrier              = t4.carrier 
left join share_facebookmessenger_cube t5  
on  t1.date                 = t5.date                     
and t1.app_id               = t5.app_id                   
and t1.app_version          = t5.app_version              
and t1.install_year         = t5.install_year             
and t1.install_month        = t5.install_month            
and t1.install_week         = t5.install_week             
and t1.install_source_group = t5.install_source_group     
and t1.install_source       = t5.install_source           
and t1.level_start            = t5.level_start                
and coalesce(t1.browser,'Unknown') = coalesce(t5.browser,'Unknown')                
and coalesce(t1.browser_version,'Unknown') = coalesce(t5.browser,'Unknown')          
and t1.device               = t5.device                   
and t1.country              = t5.country                  
and t1.os                   = t5.os                       
and t1.os_version           = t5.os_version               
and t1.language             = t5.language                 
and t1.is_new_user          = t5.is_new_user              
and t1.is_payer             = t5.is_payer                 
and t1.device_resolution    = t5.device_resolution        
and t1.location             = t5.location                 
and t1.device_storage       = t5.device_storage           
and t1.network              = t5.network                  
and t1.carrier              = t5.carrier 
left join share_system_result_cube t6  
on  t1.date                 = t6.date                     
and t1.app_id               = t6.app_id                   
and t1.app_version          = t6.app_version              
and t1.install_year         = t6.install_year             
and t1.install_month        = t6.install_month            
and t1.install_week         = t6.install_week             
and t1.install_source_group = t6.install_source_group     
and t1.install_source       = t6.install_source           
and t1.level_start            = t6.level_start                
and coalesce(t1.browser,'Unknown') = coalesce(t6.browser,'Unknown')                
and coalesce(t1.browser_version,'Unknown') = coalesce(t6.browser,'Unknown')          
and t1.device               = t6.device                   
and t1.country              = t6.country                  
and t1.os                   = t6.os                       
and t1.os_version           = t6.os_version               
and t1.language             = t6.language                 
and t1.is_new_user          = t6.is_new_user              
and t1.is_payer             = t6.is_payer                 
and t1.device_resolution    = t6.device_resolution        
and t1.location             = t6.location                 
and t1.device_storage       = t6.device_storage           
and t1.network              = t6.network                  
and t1.carrier              = t6.carrier 
left join share_wechat_cube t7  
on  t1.date                 = t7.date                     
and t1.app_id               = t7.app_id                   
and t1.app_version          = t7.app_version              
and t1.install_year         = t7.install_year             
and t1.install_month        = t7.install_month            
and t1.install_week         = t7.install_week             
and t1.install_source_group = t7.install_source_group     
and t1.install_source       = t7.install_source           
and t1.level_start            = t7.level_start                
and coalesce(t1.browser,'Unknown') = coalesce(t7.browser,'Unknown')                
and coalesce(t1.browser_version,'Unknown') = coalesce(t7.browser,'Unknown')          
and t1.device               = t7.device                   
and t1.country              = t7.country                  
and t1.os                   = t7.os                       
and t1.os_version           = t7.os_version               
and t1.language             = t7.language                 
and t1.is_new_user          = t7.is_new_user              
and t1.is_payer             = t7.is_payer                 
and t1.device_resolution    = t7.device_resolution        
and t1.location             = t7.location                 
and t1.device_storage       = t7.device_storage           
and t1.network              = t7.network                  
and t1.carrier              = t7.carrier
;

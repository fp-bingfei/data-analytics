------------------------
--video action reports
------------------------

delete from processed.agg_video_action  where date >=(select start_date from processed.init_start_date); 
insert into processed.agg_video_action
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.video_load v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.video_capture v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.action_name
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.user_action v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.share_system_result v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.video_save v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.video_process_capture v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.video_process_load v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5
union all
select v.app_id
      ,date(v.ts_pretty)
      ,coalesce(u.country,'Unknown') as country
      ,coalesce(u.os,'Unknown') as os
      ,v.event
      ,count(distinct v.user_id) as user_cnt
      ,count(v.user_id) as action_cnt
from processed.video_process_output v
left join processed.dim_user u
on md5(v.app_id||v.user_id) = u.user_key
where date(ts_pretty) >=(select start_date from processed.init_start_date)
group by 1,2,3,4,5;
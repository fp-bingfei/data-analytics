----------------------------------------------------------
--processed.new_user
----------------------------------------------------------
delete from processed.new_user                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date);  

insert into processed.new_user 
select    event_id::varchar(128) 
         ,data_version::varchar(10) 
         ,app_id::varchar(64) 
         ,ts 
         ,ts_pretty
         ,event::varchar(64) 
         ,user_id::varchar(128)
         ,session_id::varchar(128) 
         ,app_version::varchar(64)
         ,gameserver_id::varchar(128) 
         ,os::varchar(64)
         ,os_version::varchar(64)
         ,browser::varchar(64) 
         ,browser_version::varchar(128) 
         ,idfa::varchar(128) 
         ,idfv::varchar(128) 
         ,gaid::varchar(128) 
         ,android_id::varchar(128)
         ,mac_address::varchar(128) 
         ,device::varchar(128) 
         ,ip::varchar(50) 
         ,country_code::varchar(10) 
         ,lang::varchar(16)
         ,level 
         ,vip_level 
         ,facebook_id::varchar(128) 
         ,gender::char(1) 
         ,first_name::varchar(64) 
         ,last_name::varchar(64) 
         ,birthday 
         ,email::varchar(256) 
         ,googleplus_id::varchar(128) 
         ,gamecenter_id::varchar(128) 
         ,install_ts 
         ,install_source::varchar(1024) 
         ,device_resolution::varchar(128)
         ,location::varchar(1024)
         ,device_storage::varchar(1024)
         ,network::varchar(1024)
         ,carrier::varchar(1024)
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          ,data_version 
          ,app_id 
          ,cast(ts as bigint) as ts 
          ,ts_pretty
          ,event 
          ,user_id 
          ,session_id 
          ,json_extract_path_text(properties,'app_version') as app_version
          ,json_extract_path_text(properties,'gameserver_id') as gameserver_id 
          ,json_extract_path_text(properties,'os') as os
          ,json_extract_path_text(properties,'os_version') as os_version
          ,json_extract_path_text(properties,'browser') as browser 
          ,json_extract_path_text(properties,'browser_version') as browser_version 
          ,json_extract_path_text(properties,'idfa') as idfa 
          ,json_extract_path_text(properties,'idfv') as idfv 
          ,json_extract_path_text(properties,'gaid') as gaid 
          ,json_extract_path_text(properties,'android_id') as android_id
          ,json_extract_path_text(properties,'mac_address') as mac_address 
          ,json_extract_path_text(properties,'device') as device 
          ,json_extract_path_text(properties,'ip') as ip 
          ,json_extract_path_text(properties,'country_code') as country_code 
          ,json_extract_path_text(properties,'lang') as lang
          ,cast(case when json_extract_path_text(properties,'level') = '' then '0'
                    else json_extract_path_text(properties,'level') end as int) as level 
          ,cast(case when json_extract_path_text(properties,'vip_level') = '' then '0'
                    else json_extract_path_text(properties,'vip_level') end as int) as vip_level
          ,json_extract_path_text(properties,'facebook_id') as facebook_id 
          ,json_extract_path_text(properties,'gender') as gender 
          ,json_extract_path_text(properties,'first_name') as first_name 
          ,json_extract_path_text(properties,'last_name') as last_name 
          ,cast(case when json_extract_path_text(properties,'birthday') = '' then null 
                    else json_extract_path_text(properties,'birthday') end as date) as birthday  
          ,json_extract_path_text(properties,'email') as email 
          ,json_extract_path_text(properties,'googleplus_id') as googleplus_id 
          ,json_extract_path_text(properties,'gamecenter_id') as gamecenter_id 
          ,cast(json_extract_path_text(properties,'install_ts_pretty') as timestamp) as install_ts 
          ,json_extract_path_text(properties,'install_source') as install_source 
          ,json_extract_path_text(properties,'device_resolution') as device_resolution
          ,json_extract_path_text(properties,'location') as location
          ,json_extract_path_text(properties,'device_storage') as  device_storage
          ,json_extract_path_text(properties,'network') as  network
          ,json_extract_path_text(properties,'carrier') as carrier
          ,row_number() over(partition by event_id) as rnum
           from      raw_events.events
           where date(ts_pretty) >= (select start_date from processed.init_start_date)
           and event = 'new_user'
)t
where t.rnum = 1
;

----------------------------------------------------------
--processed.session_start
----------------------------------------------------------
delete from processed.session_start                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date);  

insert into processed.session_start
select    event_id::varchar(128)  
         ,data_version::varchar(10) 
         ,app_id::varchar(64) 
         ,ts 
         ,ts_pretty
         ,event::varchar(64) 
         ,user_id::varchar(128)
         ,session_id::varchar(128) 
         ,app_version::varchar(64)
         ,gameserver_id::varchar(128) 
         ,os::varchar(64)
         ,os_version::varchar(64)
         ,browser::varchar(64) 
         ,browser_version::varchar(128) 
         ,idfa::varchar(128) 
         ,idfv::varchar(128) 
         ,gaid::varchar(128) 
         ,android_id::varchar(128)
         ,mac_address::varchar(128) 
         ,device::varchar(128) 
         ,ip::varchar(50) 
         ,country_code::varchar(10) 
         ,lang::varchar(16)
         ,level 
         ,vip_level 
         ,facebook_id::varchar(128) 
         ,gender::char(1) 
         ,first_name::varchar(64) 
         ,last_name::varchar(64) 
         ,birthday 
         ,email::varchar(256) 
         ,googleplus_id::varchar(128) 
         ,gamecenter_id::varchar(128) 
         ,install_ts 
         ,install_source::varchar(1024) 
         ,device_resolution::varchar(128)
         ,location::varchar(1024)
         ,device_storage::varchar(1024)
         ,network::varchar(1024)
         ,carrier::varchar(1024)
from      
(
    select     md5(app_id||event||ts||user_id||session_id) as event_id 
          ,data_version 
          ,app_id 
          ,cast(ts as bigint) as ts 
          ,ts_pretty
          ,event 
          ,user_id 
          ,session_id 
          ,json_extract_path_text(properties,'app_version') as app_version
          ,json_extract_path_text(properties,'gameserver_id') as gameserver_id 
          ,json_extract_path_text(properties,'os') as os
          ,json_extract_path_text(properties,'os_version') as os_version
          ,json_extract_path_text(properties,'browser') as browser 
          ,json_extract_path_text(properties,'browser_version') as browser_version 
          ,json_extract_path_text(properties,'idfa') as idfa 
          ,json_extract_path_text(properties,'idfv') as idfv 
          ,json_extract_path_text(properties,'gaid') as gaid 
          ,json_extract_path_text(properties,'android_id') as android_id
          ,json_extract_path_text(properties,'mac_address') as mac_address 
          ,json_extract_path_text(properties,'device') as device 
          ,json_extract_path_text(properties,'ip') as ip 
          ,json_extract_path_text(properties,'country_code') as country_code 
          ,json_extract_path_text(properties,'lang') as lang
          ,cast(case when json_extract_path_text(properties,'level') = '' then '0'
                    else json_extract_path_text(properties,'level') end as int) as level 
          ,cast(case when json_extract_path_text(properties,'vip_level') = '' then '0'
                    else json_extract_path_text(properties,'vip_level') end as int) as vip_level 
          ,json_extract_path_text(properties,'facebook_id') as facebook_id 
          ,json_extract_path_text(properties,'gender') as gender 
          ,json_extract_path_text(properties,'first_name') as first_name 
          ,json_extract_path_text(properties,'last_name') as last_name 
           ,cast(case when json_extract_path_text(properties,'birthday') = '' then null 
                    else json_extract_path_text(properties,'birthday') end as date) as birthday  
          ,json_extract_path_text(properties,'email') as email 
          ,json_extract_path_text(properties,'googleplus_id') as googleplus_id 
          ,json_extract_path_text(properties,'gamecenter_id') as gamecenter_id 
          ,cast(json_extract_path_text(properties,'install_ts_pretty') as timestamp) as install_ts 
          ,json_extract_path_text(properties,'install_source') as install_source 
          ,json_extract_path_text(properties,'device_resolution') as device_resolution
          ,json_extract_path_text(properties,'location') as location
          ,json_extract_path_text(properties,'device_storage') as  device_storage
          ,json_extract_path_text(properties,'network') as  network
          ,json_extract_path_text(properties,'carrier') as carrier
          ,row_number() over(partition by event_id) as rnum
    from      raw_events.events
    where  date(ts_pretty) >= (select start_date from processed.init_start_date)
    and event = 'session_start'
)t
where t.rnum = 1
;


----------------------------------------------------------
--processed.session_end
----------------------------------------------------------
----------------------------------------------------------
delete from processed.session_end                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.session_end
select    event_id::varchar(128) 
         ,data_version::VARCHAR(10)
         ,app_id::VARCHAR(64) 
         ,ts 
         ,ts_pretty
         ,event::VARCHAR(64) 
         ,user_id::VARCHAR(128) 
         ,session_id:: VARCHAR(128) 
         ,session_length
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,cast(case when position('.' in json_extract_path_text(properties,'session_length')) > 0 
          	       then substring(json_extract_path_text(properties,'session_length'),1,position('.' in json_extract_path_text(properties,'session_length'))-1) 
          	       when (json_extract_path_text(properties,'session_length') = '' or json_extract_path_text(properties,'session_length') is null) then '0'
          	       else json_extract_path_text(properties,'session_length') end as bigint) as session_length
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >= (select start_date from processed.init_start_date)
          and event = 'session_end'
)t
where t.rnum = 1
;

-----------------------------------------
--video_load
-----------------------------------------
delete from processed.video_load                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.video_load
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,video_dimension_height 
         ,video_length 
         ,video_dimension_width 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'video_dimension_height') as video_dimension_height 
	          ,json_extract_path_text(properties,'video_length') as video_length 
	          ,json_extract_path_text(properties,'video_dimension_width') as video_dimension_width 
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >= (select start_date from processed.init_start_date)
          and event = 'video_load'
)t
where t.rnum = 1
;


-----------------------------------------
--video_capture
-----------------------------------------
delete from processed.video_capture                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.video_capture
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,video_dimension_height 
         ,video_length 
         ,video_dimension_width 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'video_dimension_height') as video_dimension_height 
	          ,json_extract_path_text(properties,'video_length') as video_length 
	          ,json_extract_path_text(properties,'video_dimension_width') as video_dimension_width 
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >= (select start_date from processed.init_start_date) 
          and event = 'video_capture'
)t
where t.rnum = 1
;


-----------------------------------------
--user_action
-----------------------------------------
delete from processed.user_action                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.user_action
select    event_id 
         ,data_version 
         ,app_id 
         ,ts
         ,ts_pretty 
         ,event 
         ,user_id 
         ,session_id 
         ,action_name 
         ,country_code
from
(
    select    md5(app_id||event||ts||user_id||session_id||coalesce(json_extract_path_text(properties,'action_name'),'Unknown')) as event_id 
             ,data_version 
             ,app_id 
             ,cast(ts as bigint) as ts
             ,ts_pretty 
             ,event 
             ,user_id 
             ,session_id 
             ,json_extract_path_text(properties,'action_name') as action_name 
             ,json_extract_path_text(properties,'country_code') as country_code
             ,row_number() over(partition by event_id) as rnum
    from     raw_events.events
    where date(ts_pretty) >=(select start_date from processed.init_start_date)
    and event = 'user_action'
)t
where t.rnum =1
;

-----------------------------------------
--share_system_result
-----------------------------------------
delete from processed.share_system_result                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.share_system_result
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,share_system_service_name
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'share_system_service_name') as share_system_service_name 
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date) 
          and event = 'share_system_result'
)t
where t.rnum = 1
;

-----------------------------------------
--share_facebook
-----------------------------------------
delete from processed.share_facebook                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.share_facebook
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date) 
          and event = 'share_facebook'
)t
where t.rnum = 1
;

-----------------------------------------
--share_facebookmessenger
-----------------------------------------
delete from processed.share_facebookmessenger                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.share_facebookmessenger
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date)
          and event = 'share_facebookmessenger'
)t
where t.rnum = 1
;

-----------------------------------------
--video_save
-----------------------------------------
delete from processed.video_save                                      
where date(ts_pretty) >=(select start_date from processed.init_start_date); 


insert into processed.video_save
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,video_length  
         ,video_dimension_width 
         ,video_dimension_height 
         ,video_size 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'video_length') as video_length 
          	,json_extract_path_text(properties,'video_dimension_width') as video_dimension_width 
          	,json_extract_path_text(properties,'video_dimension_height') as video_dimension_height 
          	,json_extract_path_text(properties,'video_size') as video_size 
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date)
          and event = 'video_save'
)t
where t.rnum = 1
;


-----------------------------------------
--share_wechat
-----------------------------------------
delete from processed.share_wechat                                     
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.share_wechat
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id
          	,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date)
          and event = 'share_wechat'
)t
where t.rnum = 1
;


-----------------------------------------
--video_process_capture
-----------------------------------------
delete from processed.video_process_capture                                     
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.video_process_capture 
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,video_length::VARCHAR(128) 
         ,source_video_dimension_width::VARCHAR(128)  
         ,source_video_dimension_height::VARCHAR(128)  
         ,output_video_dimension_width::VARCHAR(128) 
         ,output_video_dimension_height::VARCHAR(128) 
         ,video_process_time::VARCHAR(128) 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'video_length') as video_length
          	,json_extract_path_text(properties,'source_video_dimension_width') as source_video_dimension_width
          	,json_extract_path_text(properties,'source_video_dimension_height') as source_video_dimension_height
          	,json_extract_path_text(properties,'output_video_dimension_width') as output_video_dimension_width
          	,json_extract_path_text(properties,'output_video_dimension_height') as output_video_dimension_height
          	,json_extract_path_text(properties,'video_process_time') as video_process_time
                    ,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date)
          and event = 'video_process_capture'
)t
where t.rnum = 1
;


-----------------------------------------
--video_process_load
-----------------------------------------
delete from processed.video_process_load                                     
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.video_process_load 
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,video_length::VARCHAR(128) 
         ,source_video_dimension_width::VARCHAR(128)  
         ,source_video_dimension_height::VARCHAR(128)  
         ,output_video_dimension_width::VARCHAR(128) 
         ,output_video_dimension_height::VARCHAR(128) 
         ,video_process_time::VARCHAR(128) 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'video_length') as video_length
          	,json_extract_path_text(properties,'source_video_dimension_width') as source_video_dimension_width
          	,json_extract_path_text(properties,'source_video_dimension_height') as source_video_dimension_height
          	,json_extract_path_text(properties,'output_video_dimension_width') as output_video_dimension_width
          	,json_extract_path_text(properties,'output_video_dimension_height') as output_video_dimension_height
          	,json_extract_path_text(properties,'video_process_time') as video_process_time
                    ,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date)
          and event = 'video_process_load'
)t
where t.rnum = 1
;


-----------------------------------------
--video_process_output
-----------------------------------------
delete from processed.video_process_output                                     
where date(ts_pretty) >=(select start_date from processed.init_start_date); 

insert into processed.video_process_output 
select    event_id 
         ,data_version 
         ,app_id 
         ,ts 
         ,ts_pretty
         ,event 
         ,user_id 
         ,session_id 
         ,video_length::varchar(128) 
         ,video_dimension_width::varchar(128) 
         ,video_dimension_height::varchar(128) 
         ,video_filter_id::varchar(128) 
         ,video_filter_name::varchar(128) 
         ,video_text_count::varchar(128) 
         ,video_soundrecorder_count::varchar(128) 
         ,video_music_count::varchar(128) 
         ,video_process_time::varchar(128) 
         ,video_size::varchar(128) 
from      
(
          select     md5(app_id||event||ts||user_id||session_id) as event_id 
          	,data_version 
          	,app_id 
          	,cast(ts as bigint) as ts 
          	,ts_pretty
          	,event 
          	,user_id 
          	,session_id 
          	,json_extract_path_text(properties,'video_length') as video_length
          	,json_extract_path_text(properties,'video_dimension_width') as video_dimension_width
          	,json_extract_path_text(properties,'video_dimension_height') as video_dimension_height
          	,json_extract_path_text(properties,'video_filter_id') as video_filter_id
          	,json_extract_path_text(properties,'video_filter_name') as video_filter_name
          	,json_extract_path_text(properties,'video_text_count') as video_text_count
          	,json_extract_path_text(properties,'video_soundrecorder_count') as video_soundrecorder_count
          	,json_extract_path_text(properties,'video_music_count') as video_music_count
          	,json_extract_path_text(properties,'video_process_time') as video_process_time
          	,json_extract_path_text(properties,'video_size') as video_size
                    ,row_number() over(partition by event_id) as rnum
          from      raw_events.events
          where date(ts_pretty) >=(select start_date from processed.init_start_date)
          and event = 'video_process_output'
)t
where t.rnum = 1
;
CREATE TABLE processed.new_user
(
	event_id VARCHAR(128) NOT NULL ENCODE bytedict,
	data_version VARCHAR(10) ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	gameserver_id VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(128) ENCODE lzo,
	browser VARCHAR(128) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	idfv VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	android_id VARCHAR(128) ENCODE lzo,
	mac_address VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	ip VARCHAR(50) ENCODE lzo,
	country_code VARCHAR(10) ENCODE lzo,
	lang VARCHAR(32) ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	facebook_id VARCHAR(128) ENCODE lzo,
	gender CHAR(1) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE lzo,
	googleplus_id VARCHAR(128) ENCODE lzo,
	gamecenter_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id,
	country_code
);


CREATE TABLE processed.session_start
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	gameserver_id VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(128) ENCODE lzo,
	browser VARCHAR(128) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	idfv VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	android_id VARCHAR(128) ENCODE lzo,
	mac_address VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	ip VARCHAR(50) ENCODE lzo,
	country_code VARCHAR(10) ENCODE lzo,
	lang VARCHAR(32) ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	facebook_id VARCHAR(128) ENCODE lzo,
	gender CHAR(1) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE lzo,
	googleplus_id VARCHAR(128) ENCODE lzo,
	gamecenter_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id,
	country_code
);



CREATE TABLE processed.session_end
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) ENCODE lzo,
	session_length BIGINT
)
SORTKEY
(
	app_id,
	user_id
);



CREATE TABLE processed.tmp_user_daily_login
(
	date DATE ENCODE delta DISTKEY,
	app_id VARCHAR(64) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	user_id VARCHAR(64) ENCODE lzo,
	last_login_ts TIMESTAMP ENCODE delta,
	facebook_id VARCHAR(50) ENCODE lzo,
	birthday DATE ENCODE delta,
	first_name VARCHAR(1024) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	email VARCHAR(1024) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	app_version VARCHAR(32) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	os VARCHAR(32) ENCODE lzo,
	os_version VARCHAR(100) ENCODE lzo,
	country VARCHAR(100) ENCODE lzo,
	last_ip VARCHAR(50) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	gender VARCHAR(16) ENCODE lzo,
	device VARCHAR(64) ENCODE lzo,
	browser VARCHAR(32) ENCODE lzo,
	browser_version VARCHAR(100) ENCODE lzo,
	session_cnt INTEGER,
	playtime_sec BIGINT,
	device_resolution VARCHAR(128),
	location VARCHAR(1024),
	device_storage VARCHAR(1024),
	network VARCHAR(1024),
	carrier VARCHAR(1024)
)
SORTKEY
(
	user_key,
	date,
	install_date
);

CREATE TABLE processed.tmp_user_payment
(
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	date DATE NOT NULL ENCODE delta,
	conversion_ts TIMESTAMP NOT NULL ENCODE delta,
	revenue_usd NUMERIC(15, 4),
	total_revenue_usd NUMERIC(15, 4),
	purchase_cnt BIGINT,
	total_purchase_cnt BIGINT
)
SORTKEY
(
	date,
	user_key
);

CREATE TABLE processed.fact_new_user
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date_start DATE NOT NULL ENCODE delta,
	date_end DATE ENCODE delta,
	ts_start TIMESTAMP NOT NULL ENCODE delta,
	ts_end TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	vip_level INTEGER,
	gender VARCHAR(1) ENCODE lzo,
	birthday DATE ENCODE delta,
	ip VARCHAR(50) NOT NULL ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	date_start,
	user_key,
	app_id,
	user_id
);

CREATE TABLE processed.fact_revenue
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date DATE NOT NULL ENCODE delta,
	ts TIMESTAMP NOT NULL ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	level INTEGER ENCODE lzo,
	vip_level INTEGER ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	ip VARCHAR(50) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	payment_processor VARCHAR(128) ENCODE lzo,
	iap_product_id VARCHAR(128) ENCODE lzo,
	iap_product_name VARCHAR(128) ENCODE lzo,
	iap_product_type VARCHAR(128) ENCODE lzo,
	currency VARCHAR(8) NOT NULL ENCODE lzo,
	revenue_amount NUMERIC(14, 4) NOT NULL,
	revenue_usd NUMERIC(14, 4) NOT NULL,
	transaction_id VARCHAR(128) NOT NULL ENCODE lzo,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	date,
	user_key,
	app_id,
	user_id
);

CREATE TABLE processed.fact_session
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date_start DATE NOT NULL ENCODE delta,
	date_end DATE ENCODE delta,
	ts_start TIMESTAMP NOT NULL ENCODE delta,
	ts_end TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	vip_level INTEGER,
	gender VARCHAR(1) ENCODE lzo,
	birthday DATE ENCODE delta,
	ip VARCHAR(50) NOT NULL ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	playtime_sec BIGINT,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	date_start,
	user_key,
	app_id,
	user_id
);

CREATE TABLE processed.fact_dau_snapshot
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	date DATE NOT NULL ENCODE delta,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_new_user INTEGER,
	is_payer INTEGER,
	is_converted_today INTEGER,
	revenue_usd NUMERIC(15, 4),
	payment_cnt INTEGER,
	session_cnt INTEGER,
	playtime_sec INTEGER,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	date,
	user_key,
	app_id,
	country
);

CREATE TABLE processed.dim_user
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_language VARCHAR(16) ENCODE lzo,
	install_country VARCHAR(64) ENCODE lzo,
	install_os VARCHAR(64) ENCODE lzo,
	install_device VARCHAR(128) ENCODE lzo,
	install_browser VARCHAR(64) ENCODE lzo,
	install_gender VARCHAR(10) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	birthday DATE ENCODE delta,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	gender VARCHAR(1) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	last_ip VARCHAR(50) ENCODE lzo,
	level INTEGER,
	is_payer INTEGER,
	conversion_ts TIMESTAMP ENCODE delta,
	revenue_usd NUMERIC(14, 4),
	payment_cnt INTEGER,
	last_login_date DATE,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	user_key,
	app_id,
	user_id
);

CREATE TABLE processed.agg_kpi
(
	date DATE NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	install_year VARCHAR(16) NOT NULL ENCODE lzo,
	install_month VARCHAR(16) NOT NULL ENCODE lzo,
	install_week VARCHAR(16) NOT NULL ENCODE lzo,
	install_source_group VARCHAR(1024) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	level INTEGER,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_new_user INTEGER,
	is_payer INTEGER,
	device_resolution VARCHAR(128) ENCODE lzo,
	location VARCHAR(1024) ENCODE lzo,
	device_storage VARCHAR(1024) ENCODE lzo,
	network VARCHAR(1024) ENCODE lzo,
	carrier VARCHAR(1024) ENCODE lzo,
	new_user_cnt INTEGER,
	dau_cnt INTEGER,
	new_payer_cnt INTEGER,
	payer_today_cnt INTEGER,
	payment_cnt INTEGER,
	revenue_usd NUMERIC(14, 4),
	session_cnt INTEGER,
	playtime_sec BIGINT,
	video_load_users INTEGER,
	video_load_cnt INTEGER,
	video_capture_users INTEGER,
	video_capture_cnt INTEGER,
	video_save_users INTEGER,
	video_save_cnt INTEGER,
	share_facebook_users INTEGER,
	share_facebook_cnt INTEGER,
	share_facebookmessenger_users INTEGER,
	share_facebookmessenger_cnt INTEGER,
	share_system_result_users INTEGER,
	share_system_result_cnt INTEGER,
	share_wechat_users INTEGER,
	share_wechat_cnt INTEGER
)
SORTKEY
(
	date,
	app_id,
	install_year,
	install_month,
	os,
	install_source,
	country,
	is_payer
);


CREATE TABLE processed.video_process_capture
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) NOT NULL ENCODE lzo,
	video_length VARCHAR(128) ENCODE lzo,
	source_video_dimension_width  VARCHAR(128) ENCODE lzo,
          source_video_dimension_height  VARCHAR(128) ENCODE lzo,
          output_video_dimension_width  VARCHAR(128) ENCODE lzo,
          output_video_dimension_height  VARCHAR(128) ENCODE lzo,
          video_process_time  VARCHAR(128) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id
);


CREATE TABLE processed.video_process_capture
(
	event_id VARCHAR(128)  ENCODE lzo,
	data_version VARCHAR(10)  ENCODE lzo,
	app_id VARCHAR(64)  ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP  ENCODE delta,
	event VARCHAR(64)  ENCODE lzo,
	user_id VARCHAR(128)  ENCODE lzo DISTKEY,
	session_id VARCHAR(128)  ENCODE lzo,
	video_length VARCHAR(128) ENCODE lzo,
	source_video_dimension_width  VARCHAR(128) ENCODE lzo,
          source_video_dimension_height  VARCHAR(128) ENCODE lzo,
          output_video_dimension_width  VARCHAR(128) ENCODE lzo,
          output_video_dimension_height  VARCHAR(128) ENCODE lzo,
          video_process_time  VARCHAR(128) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id
);

CREATE TABLE processed.video_process_load
(
	event_id VARCHAR(128)  ENCODE lzo,
	data_version VARCHAR(10)  ENCODE lzo,
	app_id VARCHAR(64)  ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP  ENCODE delta,
	event VARCHAR(64)  ENCODE lzo,
	user_id VARCHAR(128)  ENCODE lzo DISTKEY,
	session_id VARCHAR(128)  ENCODE lzo,
	video_length VARCHAR(128) ENCODE lzo,
          source_video_dimension_width  VARCHAR(128) ENCODE lzo,
          source_video_dimension_height VARCHAR(128) ENCODE lzo,
          output_video_dimension_width   VARCHAR(128) ENCODE lzo,
          output_video_dimension_height VARCHAR(128) ENCODE lzo,
          video_process_time  VARCHAR(128) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id
);


CREATE TABLE processed.video_process_output
(
	event_id VARCHAR(128)  ENCODE lzo,
	data_version VARCHAR(10)  ENCODE lzo,
	app_id VARCHAR(64)  ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP  ENCODE delta,
	event VARCHAR(64)  ENCODE lzo,
	user_id VARCHAR(128)  ENCODE lzo DISTKEY,
	session_id VARCHAR(128)  ENCODE lzo,
	video_length VARCHAR(128)  ENCODE lzo,
          video_dimension_width VARCHAR(128)  ENCODE lzo,
          video_dimension_height VARCHAR(128)  ENCODE lzo,
          video_filter_id VARCHAR(128)  ENCODE lzo,
          video_filter_name VARCHAR(128)  ENCODE lzo,
          video_text_count VARCHAR(128)  ENCODE lzo,
          video_soundrecorder_count VARCHAR(128)  ENCODE lzo,
          video_music_count VARCHAR(128)  ENCODE lzo,
          video_process_time VARCHAR(128)  ENCODE lzo,
          video_size VARCHAR(128)  ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id
);

CREATE TABLE processed.agg_video_action
(
	app_id VARCHAR(100) ENCODE bytedict,
	date DATE ENCODE delta,
	country VARCHAR(100) ENCODE bytedict,
	os VARCHAR(30) ENCODE bytedict,
	event VARCHAR(100) ENCODE bytedict,
	user_cnt BIGINT,
	action_cnt BIGINT
)
DISTSTYLE EVEN;

cp -f ~/workplace/analytics/scripts/redshift/2.0/kpi/*.sql ~/workplace/analytics/scripts/redshift/2.0/kpi_1game
cd ~/workplace/analytics/scripts/redshift/2.0/kpi_1game
rm -f sub_*.sql
for f in *.sql; do mv "$f" "sub_$f"; done
sed -i '' 's/--and /and /g' sub_*.sql 
sed -i '' 's/--where /where /g' sub_*.sql
cp -f ~/workplace/analytics/scripts/redshift/2.0/kpi_1game/sub_*.sql ~/Documents/kpi_1game/
 

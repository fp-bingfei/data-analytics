
--fruitscoot eas
create temp table temp_fruitscoot_payment_info as
select user_key
      ,1 is_payer
      ,min(ts) as conversion_ts
      ,max(ts) as last_payment_ts
      ,count(1) as payment_cnt 
from kpi_processed.fact_revenue 
where app_id like 'fruitscoot2%'
group by 1
;


delete from kpi_processed.eas_user_info where app like 'fruitscoot2%';
insert into kpi_processed.eas_user_info (
 app,
   uid,
   snsid,
   user_name,
   email,
   additional_email,
   install_source,
   install_ts,
   language,
   gender,
   level,
   is_payer,
   conversion_ts,
   last_payment_ts,
   payment_cnt,
   rc,
   coins,
   last_login_ts,
   country,
   funplus_id)
select u.app_id as app,
       cast (case when u.user_id = '' then '0' else u.user_id end as integer) as uid,
       u.facebook_id,
       u.first_name || ' ' || u.last_name as user_name,
       u.email as email,
       '' as additional_email,
       u.install_source,
       u.install_ts,
       u.language as language,
       u.gender,
       u.level,
       u.is_payer as is_payer,
       p.conversion_ts,
       p.last_payment_ts,
       nvl(p.payment_cnt, 0) as payment_cnt,
       0 as rc,
       0 as coins,
       u.last_login_date as last_login_ts,
       u.country_code,
       u.user_id
from (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country) u
left join temp_fruitscoot_payment_info p on u.user_key = p.user_key
where u.app_id like 'fruitscoot2%'
;

DROP TABLE IF EXISTS by_server.tmp_user_daily_login CASCADE;
CREATE TABLE by_server.tmp_user_daily_login
(
date              DATE,
app_id            VARCHAR(64),
user_key          VARCHAR(128),
user_id           VARCHAR(128),
last_login_ts     TIMESTAMP,
facebook_id       VARCHAR(128),
birthday          DATE,
first_name        VARCHAR(1024),
last_name         VARCHAR(64),
email             VARCHAR(1024),
install_ts        TIMESTAMP,
install_date      DATE,
app_version       VARCHAR(32),
level_start       INTEGER,
level_end         INTEGER,
os                VARCHAR(64),
os_version        VARCHAR(64),
country           VARCHAR(64),
last_ip           VARCHAR(64),
install_source    VARCHAR(1024),
language          VARCHAR(16),
gender            VARCHAR(16),
device            VARCHAR(128),
browser           VARCHAR(64),
browser_version   VARCHAR(128),
session_cnt       INTEGER,
playtime_sec      BIGINT,
scene             VARCHAR(32),
fb_source         VARCHAR(1024),
install_source_group        VARCHAR(1024),
last_ref          VARCHAR(1024),
gameserver_id     VARCHAR(128)

);

DROP TABLE IF EXISTS by_server.tmp_user_payment CASCADE;
CREATE TABLE by_server.tmp_user_payment
(
app_id            VARCHAR(64),
user_key          VARCHAR(128),
date              DATE,
conversion_ts     TIMESTAMP,
revenue_usd       NUMERIC(15,4),
total_revenue_usd NUMERIC(15,4),
purchase_cnt      BIGINT,
total_purchase_cnt      BIGINT,
scene             VARCHAR(32),
gameserver_id     VARCHAR(128)
);
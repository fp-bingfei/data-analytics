------------------------------------------------
--Data 2.0 tmp_user_daily_login.sql
------------------------------------------------

delete from  by_server.tmp_user_daily_login
where  date  >=  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
--and app_id like '_game_%'
;

insert into by_server.tmp_user_daily_login
SELECT DISTINCT
            s.date_start AS date
           ,s.app_id
           ,s.user_key
           ,s.user_id
           ,last_value(s.ts_start ignore nulls)
               OVER (PARTITION BY s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_login_ts
           ,last_value(s.facebook_id ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
           ,last_value(s.birthday ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
            ,last_value(s.first_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
            ,last_value(s.last_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
           ,substring(last_value(s.email ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_ts ignore nulls)
                        OVER (PARTITION BY s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following)  
               WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene, s.gameserver_id ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts
                     then u.install_ts  
               ELSE
                   min(s.install_ts ignore nulls)
                   OVER (PARTITION BY s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_ts
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_date ignore nulls)
                        OVER (PARTITION BY s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
               min(s.install_date ignore nulls)
                       OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                       ROWS BETWEEN unbounded preceding AND unbounded following) 
                WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene, s.gameserver_id ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date
                     then u.install_date    
               ELSE
                   min(s.install_date ignore nulls)
                   OVER (PARTITION BY s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_date
           ,last_value(s.app_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS app_version
           ,first_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS level_start
           ,nvl(last_value(s.level_end ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following),
                last_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following))
           AS level_end
           ,last_value(s.os ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os
           ,last_value(s.os_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os_version
           ,last_value(s.country ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS country
           ,last_value(s.ip ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ip
           ,last_value(s.install_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS install_source
           ,last_value(s.language ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS language
           ,last_value(s.gender ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
           ,last_value(s.device ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)::varchar(64)
           AS device
           ,last_value(s.browser ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser
           ,last_value(s.browser_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser_version
           ,sum(1)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene, s.gameserver_id)
            AS session_cnt
           ,sum(playtime_sec)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene, s.gameserver_id)
            AS playtime_sec
            ,s.scene
            ,last_value(s.fb_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS fb_source
            ,null as install_source_group
            ,last_value(s.last_ref ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ref
           ,last_value(s.gameserver_id ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key, s.gameserver_id ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gameserver_id
FROM  kpi_processed.fact_session s
LEFT JOIN by_server.dim_user u
ON        u.user_key=s.user_key
and       nvl(u.gameserver_id,'NA') = nvl(s.gameserver_id,'NA')
WHERE     s.date_start >= 
                  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
--and s.app_id like '_game_%'        
        ;
--update game server id when it is null 
update by_server.tmp_user_daily_login
set gameserver_id = 'NA'
where gameserver_id is null or gameserver_id='';
-- Attempt to catch missing users from fact_new_user

INSERT INTO by_server.tmp_user_daily_login
SELECT DISTINCT
     n.date_start
    ,n.app_id
    ,n.user_key
    ,n.user_id
    ,last_value(n.ts_start ignore nulls)
        OVER (PARTITION BY n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,last_value(n.facebook_id ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
    ,last_value(n.birthday ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
     ,last_value(n.first_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
     ,last_value(n.last_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
    ,substring(last_value(n.email ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_ts ignore nulls)
             OVER (PARTITION BY n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN n.app_id like 'royal%' and n.scene='2' then 
            min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    n.scene='Main' and min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene, n.gameserver_id ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(n.install_ts ignore nulls)
            OVER (PARTITION BY n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_date ignore nulls)
             OVER (PARTITION BY n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        when n.app_id like 'royal%' and n.scene='2' then
            min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN   n.scene='Main' and  min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene, n.gameserver_id ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(n.install_date ignore nulls)
            OVER (PARTITION BY n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(n.app_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(n.level_start ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_start
    ,last_value(n.level_end ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(n.os ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(n.os_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(n.country ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(n.ip ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(n.install_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(n.language ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(n.gender ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
    ,last_value(n.device ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(n.browser ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(n.browser_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,n.scene
    ,last_value(n.fb_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key, n.gameserver_id ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM by_server.fact_new_user n
LEFT JOIN by_server.tmp_user_daily_login s
    ON  s.user_key=n.user_key
    AND s.date=n.date_start
    AND s.gameserver_id=n.gameserver_id
LEFT JOIN by_server.dim_user u
    ON  u.user_key=s.user_key
    and u.gameserver_id = s.gameserver_id
WHERE n.date_start >= 
           (
             select start_date 
             from   kpi_processed.init_start_date
            )
           --and n.app_id like '_game_%'
AND s.user_key is null
;


                  
-- Attempt to catch missing users from fact_revenue

INSERT INTO by_server.tmp_user_daily_login
WITH revenue_session_cnt AS 
(
    SELECT  user_key
           ,date
           ,scene
           ,gameserver_id
           ,count(distinct session_id) as session_cnt
    FROM   by_server.fact_revenue
    WHERE  session_id is not null
    --and app_id like '_game_%'
    GROUP  BY 1,2,3,4
)
SELECT DISTINCT
     p.date
    ,p.app_id
    ,p.user_key
    ,p.user_id
    ,last_value(p.ts ignore nulls)
        OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,cast(null as varchar) AS facebook_id
    ,cast(null as date) AS birthday
    ,null AS  first_name
    ,null AS last_name
    ,cast(null as varchar) AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_ts ignore nulls)
             OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_ts ignore nulls)
                OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(p.install_ts ignore nulls)
            OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_date ignore nulls)
             OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_date ignore nulls)
                OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(p.install_date ignore nulls)
            OVER (PARTITION BY p.user_key, p.gameserver_id ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(p.app_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level
    ,last_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(p.os ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(p.os_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(p.country ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(p.ip ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(p.install_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(p.language ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,null AS gender
    ,last_value(p.device ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(p.browser ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(p.browser_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,COALESCE(c.session_cnt,1) AS session_cnt
    ,0 AS playtime_sec
    ,p.scene
    ,last_value(p.fb_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key, p.gameserver_id ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM by_server.fact_revenue p
LEFT JOIN revenue_session_cnt c
    ON  p.user_key=c.user_key
    AND p.date=c.date 
    and p.scene=c.scene
    and p.gameserver_id = c.gameserver_id
LEFT JOIN by_server.tmp_user_daily_login s
    ON  s.user_key=p.user_key
    AND s.date=p.date
    and s.gameserver_id = p.gameserver_id
LEFT JOIN by_server.dim_user u
    ON  u.user_key=s.user_key
    and u.gameserver_id = s.gameserver_id
WHERE p.date >= 
           (
             select start_date 
             from   kpi_processed.init_start_date
            )
    --and p.app_id like '_game_%'
AND s.user_key is null
;



--update the user info where level_end < level_start
update by_server.tmp_user_daily_login
set level_start = level_end
where level_end < level_start;

--update the install_ts info
create temp table tmp_user_last_day_byserver as
select t.*
from
    (
      select *
             ,row_number() OVER (partition by user_key, gameserver_id order by date_start desc) as row
      from kpi_processed.fact_session ds
      where date_start>=current_date-60
          and ds.install_date is not null 
      --and ds.app_id like '_game_%'
     ) t
where row = 1;

update by_server.tmp_user_daily_login
set    install_date = t.install_date,
       install_ts = t.install_ts,
       device = t.device,
       country = t.country,
       language = t.language,
       browser = t.browser,
       browser_version = t.browser_version,
       os = t.os,
       os_version = t.os_version
from   (select * from tmp_user_last_day_byserver where gameserver_id is not null or gameserver_id <>'') t
where  by_server.tmp_user_daily_login.install_date is null 
and    by_server.tmp_user_daily_login.user_key = t.user_key
and    by_server.tmp_user_daily_login.gameserver_id = t.gameserver_id
--and kpi_processed.tmp_user_daily_login.app_id like '_game_%'
;

--if we still have missing install_ts, remove
delete from by_server.tmp_user_daily_login where install_ts is NULL
--and app_id like '_game_%'
  ;
------------------------------------------------
--Data 2.0 tmp_user_payment.sql
------------------------------------------------

delete from by_server.tmp_user_payment
where date >=
          (
                    select start_date 
                    from   kpi_processed.init_start_date
          )
          --and app_id like '_game_%'
         ;


insert into by_server.tmp_user_payment
select app_id, a.user_key, a.date,conversion_ts, revenue_usd, total_revenue_usd, purchase_cnt, total_purchase_cnt, a.scene,a.gameserver_id
from
(select app_id, user_key, scene, date, gameserver_id,
sum(nvl(revenue_usd,0)) as revenue_usd,
count(1) as purchase_cnt
from by_server.fact_revenue
where date >=
          (
                    select start_date 
                    from   kpi_processed.init_start_date
          )
group by 1,2,3,4,5) a
left join
(select user_key, trunc(ts) as date, gameserver_id,
max(total_revenue_usd) as total_revenue_usd,
max(total_purchase_cnt) as total_purchase_cnt
from
--cumulative revenue until this date
(select user_key, ts, gameserver_id,sum(nvl(revenue_usd,0)) over (partition by user_key, gameserver_id order by ts rows unbounded preceding) as total_revenue_usd
  , count(1) over (partition by user_key, gameserver_id order by ts rows unbounded preceding) as total_purchase_cnt
from by_server.fact_revenue)
group by 1,2,3) b
on a.user_key = b.user_key
and a.date = b.date
and a.gameserver_id = b.gameserver_id
left join
(select user_key, gameserver_id,min(ts) as conversion_ts
from by_server.fact_revenue
group by 1,2) c
on a.user_key = c.user_key
and a.gameserver_id = c.gameserver_id;


create temp table device_table as 
select 
*
from
	(
	select 
	*
	,row_number() over(partition by app_id,funplus_id order by caff_add_time desc) as rk
	from raw_data.device_table
	)
where rk = 1
;


update kpi_processed.eas_user_info
set device_lang = d.lang,
    client_version = d.client_version,
    time_zone = d.timezone
from device_table d 
where 
	(
	kpi_processed.eas_user_info.device_lang is null or kpi_processed.eas_user_info.device_lang=''
	or kpi_processed.eas_user_info.client_version is null or kpi_processed.eas_user_info.client_version=''
	or kpi_processed.eas_user_info.time_zone is null or kpi_processed.eas_user_info.time_zone=''
	or (kpi_processed.eas_user_info.device_lang is not null and kpi_processed.eas_user_info.device_lang<>'' and kpi_processed.eas_user_info.device_lang<>d.lang)
	or (kpi_processed.eas_user_info.client_version is not null and kpi_processed.eas_user_info.client_version<>'' and kpi_processed.eas_user_info.client_version<>d.client_version)
	or (kpi_processed.eas_user_info.time_zone is not null and kpi_processed.eas_user_info.time_zone<>'' and kpi_processed.eas_user_info.time_zone<>d.timezone)
	)
	and kpi_processed.eas_user_info.app = d.app_id
	and nvl(kpi_processed.eas_user_info.uid,kpi_processed.eas_user_info.funplus_id) = d.funplus_id
;


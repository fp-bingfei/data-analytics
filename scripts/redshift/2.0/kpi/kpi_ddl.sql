
----- start_date

CREATE TABLE kpi_processed.init_start_date
(
	start_date DATE
)
DISTSTYLE EVEN;



------- session_start
CREATE TABLE kpi_processed.session_start
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) NOT NULL ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	gameserver_id VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(128) ENCODE lzo,
	browser VARCHAR(128) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	idfv VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	android_id VARCHAR(128) ENCODE lzo,
	mac_address VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	ip VARCHAR(15) ENCODE lzo,
	country_code VARCHAR(10) ENCODE lzo,
	lang VARCHAR(32) ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	facebook_id VARCHAR(128) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE lzo,
	googleplus_id VARCHAR(128) ENCODE lzo,
	gamecenter_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	scene VARCHAR(32) ENCODE lzo,
	fb_source VARCHAR(1024) ENCODE lzo,
	last_ref VARCHAR(1024)
)
SORTKEY
(
	app_id,
	user_id,
	country_code
);



------- session_end

CREATE TABLE kpi_processed.session_end
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) NOT NULL ENCODE lzo,
	session_length BIGINT,
	scene VARCHAR(32) ENCODE lzo,
	fb_source VARCHAR(1024) ENCODE lzo,
	level INTEGER
)
SORTKEY
(
	app_id,
	user_id
);

------- new_user

CREATE TABLE kpi_processed.new_user
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) NOT NULL ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	gameserver_id VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(128) ENCODE lzo,
	browser VARCHAR(128) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	idfv VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	android_id VARCHAR(128) ENCODE lzo,
	mac_address VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	ip VARCHAR(15) ENCODE lzo,
	country_code VARCHAR(10) ENCODE lzo,
	lang VARCHAR(32) ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	facebook_id VARCHAR(128) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE lzo,
	googleplus_id VARCHAR(128) ENCODE lzo,
	gamecenter_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	scene VARCHAR(32) ENCODE lzo,
	fb_source VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id,
	country_code
);

------- payment

CREATE TABLE kpi_processed.payment
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) NOT NULL ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	gameserver_id VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(128) ENCODE lzo,
	browser VARCHAR(128) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	idfv VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	android_id VARCHAR(128) ENCODE lzo,
	mac_address VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	ip VARCHAR(15) ENCODE lzo,
	country_code VARCHAR(10) ENCODE lzo,
	lang VARCHAR(32) ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	facebook_id VARCHAR(128) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE lzo,
	googleplus_id VARCHAR(128) ENCODE lzo,
	gamecenter_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	payment_processor VARCHAR(1024) ENCODE lzo,
	transaction_id VARCHAR(1024) ENCODE lzo,
	currency VARCHAR(128) ENCODE lzo,
	iap_product_id VARCHAR(1024) ENCODE lzo,
	iap_product_name VARCHAR(1024) ENCODE lzo,
	iap_product_type VARCHAR(1024) ENCODE lzo,
	amount INTEGER,
	scene VARCHAR(32),
	fb_source VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	app_id,
	user_id,
	country_code
);



------ tmp_user_daily_login

CREATE TABLE kpi_processed.tmp_user_daily_login
(
	date DATE ENCODE delta DISTKEY,
	app_id VARCHAR(64) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	user_id VARCHAR(64) ENCODE lzo,
	last_login_ts TIMESTAMP ENCODE delta,
	facebook_id VARCHAR(128) ENCODE lzo,
	birthday DATE ENCODE delta,
	first_name VARCHAR(1024) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	email VARCHAR(1024) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	app_version VARCHAR(32) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	os VARCHAR(32) ENCODE lzo,
	os_version VARCHAR(100) ENCODE lzo,
	country VARCHAR(100) ENCODE lzo,
	last_ip VARCHAR(64) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	language VARCHAR(8) ENCODE lzo,
	gender VARCHAR(16) ENCODE lzo,
	device VARCHAR(64) ENCODE lzo,
	browser VARCHAR(32) ENCODE lzo,
	browser_version VARCHAR(100) ENCODE lzo,
	session_cnt INTEGER,
	playtime_sec BIGINT,
	scene VARCHAR(32) DEFAULT 'Main'::character varying,
	fb_source VARCHAR(1024) ENCODE lzo,
	install_source_group VARCHAR(1024) ENCODE lzo,
	last_ref VARCHAR(1024) ENCODE lzo,
	gameserver_id VARCHAR(32) DEFAULT '' encode lzo,
)
SORTKEY
(
	user_key,
	date,
	install_date
);



------- tmp_user_payment

CREATE TABLE kpi_processed.tmp_user_payment
(
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	date DATE NOT NULL ENCODE delta,
	conversion_ts TIMESTAMP NOT NULL ENCODE delta,
	revenue_usd NUMERIC(15, 4),
	total_revenue_usd NUMERIC(15, 4),
	purchase_cnt BIGINT,
	total_purchase_cnt BIGINT,
	scene VARCHAR(32) DEFAULT 'Main'::character varying
)
SORTKEY
(
	date,
	user_key
);



-------- fact_session

CREATE TABLE kpi_processed.fact_session
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date_start DATE NOT NULL ENCODE delta,
	date_end DATE ENCODE delta,
	ts_start TIMESTAMP NOT NULL ENCODE delta,
	ts_end TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	vip_level INTEGER,
	gender VARCHAR(20) ENCODE lzo,
	birthday DATE ENCODE delta,
	ip VARCHAR(32) NOT NULL ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	playtime_sec BIGINT,
	scene VARCHAR(32) DEFAULT 'Main'::character varying,
	fb_source VARCHAR(1024),
	last_ref VARCHAR(1024),
	gameserver_id VARCHAR(32) DEFAULT '' encode lzo,
)
SORTKEY
(
	date_start,
	user_key,
	app_id,
	user_id
);


------- fact_revenue

CREATE TABLE kpi_processed.fact_revenue
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date DATE NOT NULL ENCODE delta,
	ts TIMESTAMP NOT NULL ENCODE delta,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	level INTEGER ENCODE lzo,
	vip_level INTEGER ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	ip VARCHAR(32) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	payment_processor VARCHAR(128) ENCODE lzo,
	iap_product_id VARCHAR(128) ENCODE lzo,
	iap_product_name VARCHAR(128) ENCODE lzo,
	iap_product_type VARCHAR(128) ENCODE lzo,
	currency VARCHAR(8) NOT NULL ENCODE lzo,
	revenue_amount NUMERIC(14, 4),
	revenue_usd NUMERIC(14, 4),
	transaction_id VARCHAR(128) ENCODE lzo,
	scene VARCHAR(32) ENCODE lzo,
	fb_source VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	date,
	user_key,
	app_id,
	user_id
);



-------- fact_new_user

CREATE TABLE kpi_processed.fact_new_user
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date_start DATE NOT NULL ENCODE delta,
	date_end DATE ENCODE delta,
	ts_start TIMESTAMP NOT NULL ENCODE delta,
	ts_end TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	vip_level INTEGER,
	gender VARCHAR(20) ENCODE lzo,
	birthday DATE ENCODE delta,
	ip VARCHAR(32) NOT NULL ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	scene VARCHAR(32) ENCODE lzo,
	fb_source VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	date_start,
	user_key,
	app_id,
	user_id
);

------- fact_dau_snapshot

CREATE TABLE kpi_processed.fact_dau_snapshot
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	date DATE NOT NULL ENCODE delta,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	level_start INTEGER,
	level_end INTEGER,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_new_user INTEGER,
	is_payer INTEGER,
	is_converted_today INTEGER,
	revenue_usd NUMERIC(15, 4),
	payment_cnt INTEGER,
	session_cnt INTEGER,
	playtime_sec INTEGER,
	scene VARCHAR(32) DEFAULT 'Main'::character varying,
	revenue_iap NUMERIC(14, 4),
	revenue_ads NUMERIC(14, 4),
	gameserver_id VARCHAR(32) DEFAULT '' encode lzo,
)
SORTKEY
(
	date,
	user_key,
	app_id,
	country
);

------- dim_user

CREATE TABLE kpi_processed.dim_user
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_language VARCHAR(16) ENCODE lzo,
	install_country VARCHAR(64) ENCODE lzo,
	install_os VARCHAR(64) ENCODE lzo,
	install_device VARCHAR(128) ENCODE lzo,
	install_browser VARCHAR(64) ENCODE lzo,
	install_gender VARCHAR(20) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	birthday DATE ENCODE delta,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	last_ip VARCHAR(32) ENCODE lzo,
	level INTEGER,
	is_payer INTEGER,
	conversion_ts TIMESTAMP ENCODE delta,
	revenue_usd NUMERIC(14, 4),
	payment_cnt INTEGER,
	last_login_date DATE,
	install_source_group VARCHAR(1024) ENCODE lzo,
	install_creative_id VARCHAR(500) ENCODE lzo,
	last_ref VARCHAR(1024) ENCODE lzo
)
SORTKEY
(
	user_key,
	app_id,
	user_id
);

------- currency

CREATE TABLE kpi_processed.currency
(
	id VARCHAR(30) NOT NULL,
	dt DATE NOT NULL,
	currency VARCHAR(10) NOT NULL,
	factor NUMERIC(15, 10) NOT NULL
)
DISTSTYLE EVEN;

ALTER TABLE kpi_processed.currency
ADD CONSTRAINT currency_pkey
PRIMARY KEY (id);


-------- dim_country

CREATE TABLE kpi_processed.dim_country
(
	country_code VARCHAR(2),
	country VARCHAR(50)
)
DISTSTYLE EVEN
SORTKEY
(
	country_code
);


---- agg_marketing_kpi

CREATE TABLE kpi_processed.agg_marketing_kpi
(
	app VARCHAR(32) NOT NULL ENCODE lzo,
	install_date DATE ENCODE delta,
	install_date_str VARCHAR(10) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	install_source_group VARCHAR(1024) ENCODE lzo,
	campaign VARCHAR(1024) ENCODE lzo,
	sub_publisher VARCHAR(512) ENCODE lzo,
	creative_id VARCHAR(512) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	os VARCHAR(32) ENCODE lzo,
	new_installs INTEGER DEFAULT 0,
	d1_new_installs INTEGER DEFAULT 0,
	d7_new_installs INTEGER DEFAULT 0,
	d30_new_installs INTEGER DEFAULT 0,
	d60_new_installs INTEGER DEFAULT 0,
	d90_new_installs INTEGER DEFAULT 0,
	d120_new_installs INTEGER DEFAULT 0,
	revenue NUMERIC(14, 4) DEFAULT 0,
	d1_revenue NUMERIC(14, 4) DEFAULT 0,
	d7_revenue NUMERIC(14, 4) DEFAULT 0,
	d30_revenue NUMERIC(14, 4) DEFAULT 0,
	d60_revenue NUMERIC(14, 4) DEFAULT 0,
	d90_revenue NUMERIC(14, 4) DEFAULT 0,
	d120_revenue NUMERIC(14, 4) DEFAULT 0,
	payers INTEGER DEFAULT 0,
	d1_payers INTEGER DEFAULT 0,
	d7_payers INTEGER DEFAULT 0,
	d30_payers INTEGER DEFAULT 0,
	d60_payers INTEGER DEFAULT 0,
	d90_payers INTEGER DEFAULT 0,
	d120_payers INTEGER DEFAULT 0,
	d1_retained INTEGER DEFAULT 0,
	d7_retained INTEGER DEFAULT 0,
	d30_retained INTEGER DEFAULT 0,
	d60_retained INTEGER DEFAULT 0,
	d90_retained INTEGER DEFAULT 0,
	d120_retained INTEGER DEFAULT 0,
	cost NUMERIC(14, 4) DEFAULT 0,
	d3_new_installs INTEGER DEFAULT 0,
	d3_revenue NUMERIC(14, 4) DEFAULT 0,
	d3_payers INTEGER DEFAULT 0,
	d3_retained INTEGER DEFAULT 0,
	install_month VARCHAR(10) DEFAULT ''::character varying,
	d15_new_installs INTEGER,
	d15_revenue NUMERIC(14, 4),
	d15_payers INTEGER,
	d15_retained INTEGER,
	d150_new_installs INTEGER,
	d150_revenue NUMERIC(14, 4),
	d150_payers INTEGER,
	d150_retained INTEGER,
	level INTEGER,
	level_u2 INTEGER DEFAULT 0,
	level_a2u5 INTEGER DEFAULT 0,
	level_a5u10 INTEGER DEFAULT 0,
	level_a10 INTEGER DEFAULT 0,
	session_cnt INTEGER DEFAULT 0
)
DISTSTYLE EVEN
SORTKEY
(
	install_date,
	os,
	install_source,
	install_source_group,
	country,
	campaign,
	sub_publisher,
	creative_id,
	app
);

------- agg_retention_ltv

CREATE TABLE kpi_processed.agg_retention_ltv
(
	player_day INTEGER,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	install_date DATE DISTKEY,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_creative_id VARCHAR(1),
	device_alias VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_payer INTEGER,
	new_user_cnt BIGINT,
	retained_user_cnt BIGINT,
	cumulative_revenue_usd NUMERIC(38, 4),
	new_payer_cnt BIGINT
);


------- agg_kpi

CREATE TABLE kpi_processed.agg_kpi
(
	date DATE NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	install_source_group VARCHAR(1024) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	level_end INTEGER,
	browser VARCHAR(64) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_new_user INTEGER,
	is_payer INTEGER,
	new_user_cnt INTEGER,
	dau_cnt INTEGER,
	newpayer_cnt INTEGER,
	payer_today_cnt INTEGER,
	payment_cnt INTEGER,
	revenue_usd NUMERIC(14, 4),
	session_cnt INTEGER,
	playtime_sec BIGINT,
	scene VARCHAR(32) DEFAULT 'Main'::character varying,
	revenue_iap NUMERIC(14, 4),
	revenue_ads NUMERIC(14, 4),
	gameserver_id VARCHAR(32) DEFAULT '' encode lzo,
)
SORTKEY
(
	app_id,
	install_source,
	is_payer
);

--------- adjust
CREATE TABLE ffs.adjust
(
	id VARCHAR(50) NOT NULL ENCODE lzo,
	adid VARCHAR(50) ENCODE lzo,
	android_id VARCHAR(50) ENCODE lzo,
	app_id VARCHAR(50) ENCODE lzo,
	country VARCHAR(10) ENCODE lzo,
	game VARCHAR(50) ENCODE lzo,
	idfa VARCHAR(50) ENCODE lzo,
	idfa_md5 VARCHAR(50) ENCODE lzo,
	ip_address VARCHAR(30) ENCODE lzo,
	mac_sha1 VARCHAR(50) ENCODE lzo,
	ts TIMESTAMP ENCODE delta,
	tracker VARCHAR(50) ENCODE lzo,
	tracker_name VARCHAR(500) ENCODE lzo,
	userid VARCHAR(50) ENCODE lzo DISTKEY,
	gaid VARCHAR(255) ENCODE lzo,
	device_name VARCHAR(255) ENCODE lzo,
	click_id VARCHAR(255) ENCODE lzo,
	os_name VARCHAR(255) ENCODE lzo,
	os_version VARCHAR(255) ENCODE lzo
)
SORTKEY
(
	ts,
	app_id
);


------- kpi_processed.fact_install_source
drop table if exists kpi_processed.fact_install_source;
CREATE TABLE kpi_processed.fact_install_source
(
	game VARCHAR(50) ENCODE lzo,
	userid VARCHAR(50) ENCODE lzo,
	user_key VARCHAR(32) ENCODE lzo,
	ts TIMESTAMP ENCODE delta,
	tracker_name VARCHAR(500) ENCODE lzo,
	install_source VARCHAR(500) ENCODE lzo,
	install_campaign VARCHAR(500) ENCODE lzo,
	install_subpublisher VARCHAR(500) ENCODE lzo,
	install_creative_id VARCHAR(500) ENCODE lzo
)
DISTSTYLE EVEN;




CREATE TABLE kpi_processed.agg_retention_ltv_2ndscene
(
	player_day INTEGER,
	app_id VARCHAR(64),
	app_version VARCHAR(32),
	install_date DATE DISTKEY,
	install_source VARCHAR(1024),
	install_subpublisher VARCHAR(1),
	install_campaign VARCHAR(1),
	install_creative_id VARCHAR(1),
	device_alias VARCHAR(128),
	os VARCHAR(64),
	browser VARCHAR(64),
	country VARCHAR(64),
	language VARCHAR(16),
	is_payer INTEGER,
	scene VARCHAR(32),
	new_user_cnt BIGINT,
	retained_user_cnt BIGINT,
	cumulative_revenue_usd NUMERIC(38, 4),
	new_payer_cnt BIGINT
);


------- Singular raw data
DROP TABLE IF EXISTS raw_events.singular CASCADE;

CREATE TABLE raw_events.singular
(
   adn_impressions  varchar(200),
   adn_campaign     varchar(500),
   end_date         date,
   country_field    varchar(30),
   source           varchar(500),
   app              varchar(200),
   start_date       date,
   adn_cost         varchar(200),
   custom_installs  varchar(200),
   os               varchar(30),
   custom_clicks    varchar(200)
)
SORTKEY
(
	start_date,
	app,
	source
);

COMMIT;

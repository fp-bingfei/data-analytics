

--ffs eas
create temp table temp_ffs_payment_info as
select uid as snsid, 1 is_payer, 
min(paid_time) as conversion_ts, max(paid_time) as last_payment_ts, count(1) as payment_cnt
from
raw_data.ffs_tbl_payment
where app like 'ffs%'
group by 1;

delete from kpi_processed.eas_user_info where app like 'ffs%';
insert into kpi_processed.eas_user_info (
 app,
   uid,
   snsid,
   user_name,
   email,
   additional_email,
   install_source,
   install_ts,
   language,
   gender,
   level,
   is_payer,
   conversion_ts,
   last_payment_ts,
   payment_cnt,
   rc,
   coins,
   last_login_ts,
   country)
select t.app as app,
    t.uid,
    t.snsid,
    t.username as user_name,
    nullif(t.email, 'NULL') as email,
    '' as additional_email,
    u.install_source,
    u.install_ts,
    nvl(t.language, u.language) as language,
    u.gender,
    t.level,
    nvl(p.is_payer, 0) as is_payer,
    p.conversion_ts,
    p.last_payment_ts,
    nvl(p.payment_cnt, 0) as payment_cnt,
    t.reward_points as rc,
    t.coins as coins,
    nvl(u.last_login_date, t.logintime) as last_login_ts,
    u.country_code
from raw_data.ffs_tbl_user t
left join temp_ffs_payment_info p on t.snsid=p.snsid
left join (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country)u  on t.snsid=u.user_id and u.app_id like 'ffs%';



--rs eas
create temp table temp_rs_payment_info as
select app, uid, snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from
(select app, uid, snsid, pay_ts as ts
from raw_data.rs_tbl_payment
union
select r.app_id as app
           ,r.user_id::bigint as uid
           ,u.facebook_id as snsid
           ,ts
     from kpi_processed.fact_revenue r
     join kpi_processed.dim_user u on r.app_id = u.app_id 
     and r.user_id = u.user_id
     and r.app_id like 'royal%'
     and u.app_id like 'royal%'
    ) t
group by 1,2,3;

delete from kpi_processed.eas_user_info where app like 'royal%';
insert into kpi_processed.eas_user_info (
 app,
   uid,
   snsid,
   user_name,
   email,
   additional_email,
   install_source,
   install_ts,
   language,
   gender,
   level,
   is_payer,
   conversion_ts,
   last_payment_ts,
   payment_cnt,
   rc,
   coins,
   last_login_ts,
   country)
select
    t.app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,'' as additional_email
    ,t.install_source
    ,t.install_ts
    ,coalesce(t.language, u.language) as language
    ,t.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,CAST(case when t.rc = 'NULL' then '0' else regexp_replace(t.rc, '\\.[0-9]+', '') end AS BIGINT) as rc
    ,CAST(case when t.coins = 'NULL' or position('E' in t.coins) >0 then '0' else regexp_replace(t.coins, '\\.[0-9]+', '') end AS BIGINT) as coins
    ,coalesce(u.last_login_date, case when t.install_ts > '2014-07-15' then t.install_ts else '2014-07-15' end) as last_login_ts,
    u.country_code
from raw_data.rs_tbl_user t
    left join temp_rs_payment_info p on t.app = p.app and t.uid = p.uid and t.snsid = p.snsid
    left join (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country) u on t.app = u.app_id and t.uid = u.user_id and t.snsid = u.facebook_id and u.app_id like 'royal%';





--ha_eas
create temp table temp_ha_payment_info as
select  app
       ,uid
       ,snsid
       ,min(ts) as conversion_ts
       ,max(ts) as last_payment_ts
       ,count(1) as payment_cnt
from
    (select u.app_id as app
           ,u.user_id as uid
           ,p.snsid as snsid
           ,paytime::timestamp as ts
     from raw_data.ha_tbl_payment p
     join kpi_processed.dim_user u on u.app_id = case when p.locale = 'th' then 'ha.th.prod'
                                                  else 'ha.us.prod' end
     and p.snsid = u.facebook_id
     and u.app_id like 'ha%'
     union
     select r.app_id as app
           ,r.user_id as uid
           ,u.facebook_id as snsid
           ,ts
     from kpi_processed.fact_revenue r
     join kpi_processed.dim_user u on r.app_id = u.app_id 
     and r.user_id = u.user_id
     and r.app_id like 'ha%'
     and u.app_id like 'ha%'
    ) t
group by 1,2,3;

delete from kpi_processed.eas_user_info where app like 'ha%';
insert into kpi_processed.eas_user_info
(
   app,
   uid,
   snsid,
   user_name,
   email,
   additional_email,
   install_source,
   install_ts,
   language,
   gender,
   level,
   is_payer,
   conversion_ts,
   last_payment_ts,
   payment_cnt,
   rc,
   coins,
   last_login_ts,
   sign_email,
   country
)
select
    'ha.us.prod' as app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,null as additional_email
    ,t.install_source as install_source
    ,t.install_ts as install_ts
    ,u.language as language
    ,u.gender
    ,t.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,t.rc as rc
    ,t.coins as coins
    ,dateadd(second, CAST(t.last_login_ts AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
    ,t.sign_email
    ,u.country_code
from raw_data.ha_tbl_user t
    left join temp_ha_payment_info p on p.app = 'ha.us.prod' and t.uid = p.uid and t.snsid = p.snsid
    left join (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country) u on u.app_id = 'ha.us.prod' and t.uid = u.user_id and t.snsid = u.facebook_id
where t.app = 'ff2.us.prod'
union all
select
    'ha.th.prod' as app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,null as additional_email
    ,t.install_source as install_source
    ,t.install_ts as install_ts
    ,u.language as language
    ,u.gender
    ,t.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,t.rc as rc
    ,t.coins as coins
    ,dateadd(second, CAST(t.last_login_ts AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
    ,t.sign_email
    ,u.country_code
from raw_data.ha_tbl_user t
    left join temp_ha_payment_info p on p.app = 'ha.th.prod' and t.uid = p.uid and t.snsid = p.snsid
    left join (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country) u on u.app_id = 'ha.th.prod' and t.uid = u.user_id and t.snsid = u.facebook_id 
where t.app = 'ff2.th.prod'
;



--farm eas
--Create temp table to get latest additional email address

create temp table personal_email_latest
as
select distinct   app_id 
          ,last_value(browser   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser
          ,last_value(browser_version   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser_version
          ,last_value(country_code   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as country_code
          ,event 
          ,last_value(install_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_source
          ,last_value(install_date   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_date
          ,last_value(ip   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as ip
          ,last_value(lang   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as lang
          ,last_value(level   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as level
          ,last_value(os   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os
          ,last_value(os_version   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os_version
          ,uid
          ,snsid
          ,last_value(fb_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as fb_source
          ,last_value(additional_email  ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following)  as additional_email
from kpi_processed.personal_info
;

-- TODO: For EAS report
create temp table temp_farm_payment_info as
select app, uid, snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from
(select p.app_id as app, u.user_id as uid, p.uid as snsid, paid_time as ts
from raw_data.farm_tbl_payment p
    join kpi_processed.dim_user u on p.app_id = u.app_id and p.uid = u.facebook_id
    and u.app_id like 'farm%'
union
select r.app_id as app, r.user_id as uid, u.facebook_id as snsid, ts
from kpi_processed.fact_revenue r
    join kpi_processed.dim_user u on r.app_id = u.app_id and r.user_id = u.user_id
    and r.app_id like 'farm%'
    and u.app_id like 'farm'
) t
group by 1,2,3;


delete from  kpi_processed.eas_user_info where app like 'farm%';
insert into kpi_processed.eas_user_info (
 app,
   uid,
   snsid,
   user_name,
   email,
   additional_email,
   install_source,
   install_ts,
   language,
   gender,
   level,
   is_payer,
   conversion_ts,
   last_payment_ts,
   payment_cnt,
   rc,
   coins,
   last_login_ts,
   country)
select
    t.app
    ,t.uid::integer
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,coalesce(pe.additional_email,'') as additional_email
    ,t.track_ref as install_source
    ,t.addtime::timestamp as install_ts
    ,u.language as language
    ,u.gender
    ,cast(t.level as int) as level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,CAST(t.reward_points AS BIGINT) as rc
    ,CAST(t.coins AS BIGINT) as coins
    ,dateadd(second, CAST(t.logintime AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
    ,u.country_code
from raw_data.farm_tbl_user t
    left join temp_farm_payment_info p on t.app = p.app and t.uid = p.uid and t.snsid = p.snsid
    left join (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country) u on t.app = u.app_id and t.uid = u.user_id and t.snsid = u.facebook_id and u.app_id like 'farm%'
    left join personal_email_latest pe on t.app=pe.app_id and t.uid=pe.uid and t.snsid=pe.snsid;

-------update null value in language column
update kpi_processed.eas_user_info 
set language = 'am'
where language is null and app = 'farm.us.prod';

update kpi_processed.eas_user_info 
set language = 'th'
where language is null and app = 'farm.th.prod';

update kpi_processed.eas_user_info 
set language = 'zh_TW'
where language is null and app = 'farm.tw.prod';

update kpi_processed.eas_user_info 
set language = 'de-DE'
where language is null and app = 'farm.de.prod';

update kpi_processed.eas_user_info 
set language = 'fr'
where language is null and app = 'farm.fr.prod';

update kpi_processed.eas_user_info 
set language = 'nl'
where language is null and app = 'farm.nl.prod';

update kpi_processed.eas_user_info 
set language = 'it'
where language is null and app = 'farm.it.prod';

update kpi_processed.eas_user_info 
set language = 'pl'
where language is null and app = 'farm.pl.prod';

update kpi_processed.eas_user_info 
set language = 'pt'
where language is null and app = 'farm.br.prod';

update kpi_processed.eas_user_info
set additional_email = 'elzbieta.zenowiew@wp.pl'
where snsid = '100001493084362'  and uid = '72360';



create temp table temp_cs_email
as
select app
      ,platform_uid
      ,email
from
      (select app
            ,platform_uid
            ,email
            ,row_number() over (partition by app,platform_uid  order by time_create desc) as rank
      from raw_data.ha_cs_email
      )t
where t.rank =1
union all
select app
      ,platform_uid
      ,email
from
      (select app
            ,platform_uid
            ,email
            ,row_number() over (partition by app,platform_uid  order by time_create desc) as rank
      from raw_data.rs_cs_email
      )t
where t.rank =1
union all
select app
      ,platform_uid
      ,email
from
      (select app
            ,platform_uid
            ,email
            ,row_number() over (partition by app,platform_uid  order by time_create desc) as rank
      from raw_data.farm_cs_email
      )t
where t.rank =1;


update kpi_processed.eas_user_info
set cs_email = temp_cs_email.email
from temp_cs_email
where kpi_processed.eas_user_info.app = temp_cs_email.app
and kpi_processed.eas_user_info.snsid = temp_cs_email.platform_uid;

--update helpshift email
create temp table temp_helpshift_email
as
select game
      ,user_id
      ,email
from
      (select game
            ,user_id
            ,email
            ,row_number() over (partition by game,user_id  order by date desc) as rank
      from raw_data.helpshift_email
      where game = 'FamilyFarm'
      )t
where t.rank =1;

update kpi_processed.eas_user_info
set help_shift_email = temp_helpshift_email.email
from temp_helpshift_email
where kpi_processed.eas_user_info.app like  'ffs%'
and kpi_processed.eas_user_info.snsid = temp_helpshift_email.user_id;

--warship eas
create temp table temp_warship_payment_info as
select user_key
      ,1 is_payer
      ,min(ts) as conversion_ts
      ,max(ts) as last_payment_ts
      ,count(1) as payment_cnt 
from kpi_processed.fact_revenue 
where app_id  ='battlewarship.global.prod'
group by 1
;

delete from kpi_processed.eas_user_info where app like 'battlewarship%';
insert into kpi_processed.eas_user_info (
 app,
   uid,
   snsid,
   user_name,
   email,
   additional_email,
   install_source,
   install_ts,
   language,
   gender,
   level,
   is_payer,
   conversion_ts,
   last_payment_ts,
   payment_cnt,
   rc,
   coins,
   last_login_ts,
   country,
   funplus_id)
select 'battlewarship.global.prod' app,
       cast (case when u.user_id = '' then '0' else u.user_id end as integer) as uid,
       u.facebook_id,
       u.first_name || ' ' || u.last_name as user_name,
       u.email as email,
       '' as additional_email,
       u.install_source,
       u.install_ts,
       u.language as language,
       u.gender,
       u.level,
       u.is_payer as is_payer,
       p.conversion_ts,
       p.last_payment_ts,
       nvl(p.payment_cnt, 0) as payment_cnt,
       0 as rc,
       0 as coins,
       u.last_login_date as last_login_ts,
       u.country_code,
       u.user_id
from (select u.*,c.country_code from kpi_processed.dim_user  u left join kpi_processed.dim_country c
on u.country=c.country) u
left join temp_warship_payment_info p on u.user_key = p.user_key
where u.app_id='battlewarship.global.prod';


-- poker eas
delete from raw_data.poker_tbl_user 
  where mid in(select distinct mid from raw_data.poker_tbl_user_daily);

insert into raw_data.poker_tbl_user
select * from raw_data.poker_tbl_user_daily;

delete from kpi_processed.eas_user_info where app like 'poker%';
insert into kpi_processed.eas_user_info (
  app,
  uid,
  snsid,
  user_name,
  email,
  additional_email,
  install_source,
  install_ts,
  language,
  gender,
  level,
  is_payer,
  conversion_ts,
  last_payment_ts,
  payment_cnt,
  rc,
  coins,
  last_login_ts,
  country
)
select 
  'poker.ar.prod' as app,
  mid::integer as uid,
  sitemid::varchar(100) as snsid,
  mnick as user_name,
  fullEmail::varchar(100) as email,
  email::varchar(100) as additional_email,
  '' as install_source,
  (TIMESTAMP 'epoch' + mtime * INTERVAL '1 Second ') as install_ts,
  lang as language,
  case when mgender='0' then 'male' when mgender='1' then 'female' else 'secret' end as gender,
  mexplevel::smallint as level,
  case when mpay::numeric(14,4)>0 then 1 else 0 end as is_payer,
  null as conversion_ts,
  null as last_payment_ts,
  null as payment_cnt,
  0 as rc,
  mmoney as coins,
  (TIMESTAMP 'epoch' + mactivetime * INTERVAL '1 Second ') as last_login_ts,
  mhometown::varchar(100) as country
from raw_data.poker_tbl_user;

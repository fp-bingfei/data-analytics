----------------------------------------------
--Data 2.0 agg_kpi.sql
--For kpi-funplus redshift cluster game, except diandian game
----------------------------------------------

delete from kpi_processed.agg_kpi
where date >=(select start_date from kpi_processed.init_start_date)
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi
(
	 date                       
	,app_id                     
	,app_version                             
	,install_source_group       
	,install_source             
	,level_end                  
	,browser                                             
	,country                    
	,os                                          
	,language                   
	,is_new_user                
	,is_payer
  ,gameserver_id                   
	,new_user_cnt               
	,dau_cnt                    
	,newpayer_cnt               
	,payer_today_cnt            
	,payment_cnt                
	,revenue_usd
	,session_cnt
          ,playtime_sec
          ,scene
          ,revenue_iap
          ,revenue_ads
          
)
select d.date
      ,d.app_id
      ,d.app_version
      ,case when u.app_id like 'farm%' then u.install_source_group
       else u.install_source end as install_source_group
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,d.is_new_user
      ,d.is_payer
      ,d.gameserver_id
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
      ,d.scene
      ,sum(coalesce(d.revenue_iap,0)) as revenue_iap
      ,sum(coalesce(d.revenue_ads,0)) as revenue_ads
from kpi_processed.fact_dau_snapshot d
join kpi_processed.dim_user u on d.user_key=u.user_key
where date >=(select start_date from kpi_processed.init_start_date)
--and d.app_id like '_game_%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,22;

--KF: kpi w install date, just for loe (special case)
delete from kpi_processed.agg_kpi_loe
where date >=(select start_date from kpi_processed.init_start_date);

insert into kpi_processed.agg_kpi_loe
(
   date                       
  ,app_id                     
  ,app_version                             
  ,install_source_group       
  ,install_source
  ,install_date
  ,level_end
  ,gameserver_id                  
  ,browser                                             
  ,country                    
  ,os                                          
  ,language                   
  ,is_new_user                
  ,is_payer
  ,scene
  ,new_user_cnt               
  ,dau_cnt                    
  ,newpayer_cnt               
  ,payer_today_cnt            
  ,payment_cnt                
  ,revenue_usd
  ,session_cnt
          ,playtime_sec
          ,revenue_iap
          ,revenue_ads
)
select d.date
      ,d.app_id
      ,d.app_version
      ,case when u.app_id like 'farm%' then u.install_source_group
       else u.install_source end as install_source_group
      ,u.install_source
      ,u.install_date
      ,d.level_end
      ,nvl(d.gameserver_id, '')    
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,d.is_new_user
      ,d.is_payer
    ,d.scene
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
      ,sum(coalesce(d.revenue_iap,0)) as revenue_iap
      ,sum(coalesce(d.revenue_ads,0)) as revenue_ads
from (select * from kpi_processed.fact_dau_snapshot 
where app_id like 'loe%') d
join kpi_processed.dim_user u on d.user_key=u.user_key
where date >=(select start_date from kpi_processed.init_start_date)
and (u.app_id != 'loe.global.prod' or u.level > 0)
--Remove level = 0 data from Citadel Realms, which are fake users
--and d.app_id like '_game_%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

----------------------------------------------
--Data 2.0 agg_retention_ltv.sql
----------------------------------------------

DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (
    SELECT 0 AS day UNION ALL
    SELECT 1 UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);

drop table if exists player_day_cube;
CREATE TEMP TABLE player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    --dau.gameserver_id,
    d.is_payer,
    count(distinct d.user_key) new_user_cnt
FROM kpi_processed.dim_user d, player_day p, last_date l, kpi_processed.fact_dau_snapshot dau
where
    d.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
    and dau.scene='Main'
    --and (d.app_id != 'loe.global.prod' or d.level > 0)
--Remove level = 0 data from Citadel Realms, which are fake users
--and d.app_id like '_game_%'
--group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

CREATE TEMP TABLE baseline AS
WITH last_date AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    --d.gameserver_id,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM kpi_processed.fact_dau_snapshot d
 JOIN kpi_processed.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 WHERE
    u.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
    and d.scene='Main'
    --and (u.app_id != 'loe.global.prod' or u.level > 0)
    --Remove level = 0 data from Citadel Realms, which are fake users
    --and d.app_id like '_game_%'
--group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;
  group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-- TODO: insert instead of create table

delete from kpi_processed.agg_retention_ltv
--where app_id like '_game_%'
;

insert into kpi_processed.agg_retention_ltv
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    --pc.gameserver_id,
    '' as gameserver_id,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube pc
LEFT JOIN baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    --COALESCE(pc.gameserver_id,'') = COALESCE(b.gameserver_id,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0);


--------------------------------------------------------------------------------------------------------------------------------------------
--FF tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate marketing tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

delete from kpi_processed.agg_marketing_kpi
--where app like '_game_%'
;

create temp table tmp_new_install_users as
select user_key
    ,app_id as app
    ,user_id as uid
    ,install_date
    ,case when (lower(install_campaign) like '%papaya%' and app_id like 'koa%') then 'Papaya_FB'
      else install_source end
     as install_source
    ,case when (install_source like 'Google%' and app_id like 'koa%') THEN 'Google Install Source'
          when (install_source like 'Youtube Installs' and app_id like 'koa%') THEN 'Google Install Source'
          when ((install_source in ('Off-Facebook Installs','Facebook Installs','Instagram Installs','Papaya_FB') or (lower(install_campaign) like '%papaya%')) and app_id like 'koa%') THEN 'Facebook Install Source'
          when (install_source like 'Poker_%' and app_id like 'poker%') THEN 'Facebook Install Source'
          when (install_source like 'FS_%' and app_id like 'fruitscoot%') THEN 'Facebook Install Source'
      else 'Other Install Source' end
     as install_source_group
    ,install_campaign as campaign
    ,install_subpublisher as sub_publisher
    ,install_creative_id as creative_id
    ,install_country as country
    ,install_os as os
    ,is_payer
from kpi_processed.dim_user
where app_id != 'loe.global.prod' or level > 0;
--Remove level = 0 data from Citadel Realms, which are fake users
--where app_id like '_game_%'
;


create temp table tmp_all_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
--where app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d1_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
    join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 1
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d3_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 3
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d7_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 7
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d30_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 30
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d60_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 60
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d90_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 90
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d120_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 120
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d1_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 1
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d3_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 3
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d7_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 7
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d30_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 30
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d60_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 60
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d90_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 90
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d120_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 120
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

insert into kpi_processed.agg_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_month
    ,install_source
    ,install_source_group
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os

    ,new_installs
    ,d1_new_installs
    ,d3_new_installs
    ,d7_new_installs
    ,d30_new_installs
    ,d60_new_installs
    ,d90_new_installs
    ,d120_new_installs

    ,revenue
    ,d1_revenue
    ,d3_revenue
    ,d7_revenue
    ,d30_revenue
    ,d60_revenue
    ,d90_revenue
    ,d120_revenue

    ,payers
    ,d1_payers
    ,d3_payers
    ,d7_payers
    ,d30_payers
    ,d60_payers
    ,d90_payers
    ,d120_payers

    ,d1_retained
    ,d3_retained
    ,d7_retained
    ,d30_retained
    ,d60_retained
    ,d90_retained
    ,d120_retained
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,substring(u.install_date, 1, 7) as install_month
    ,u.install_source::VARCHAR(128)
    ,u.install_source_group::VARCHAR(128)
    ,u.campaign::VARCHAR(128)
    ,u.sub_publisher
    ,u.creative_id
    ,u.country
    ,u.os

    ,count(1) as new_installs
    ,count(1) as d1_new_installs
    ,count(1) as d3_new_installs
    ,count(1) as d7_new_installs
    ,count(1) as d30_new_installs
    ,count(1) as d60_new_installs
    ,count(1) as d90_new_installs
    ,count(1) as d120_new_installs

    ,sum(case when r.revenue is null then 0 else r.revenue end) as revenue
    ,sum(case when d1_r.revenue is null then 0 else d1_r.revenue end) as d1_revenue
    ,sum(case when d3_r.revenue is null then 0 else d3_r.revenue end) as d3_revenue
    ,sum(case when d7_r.revenue is null then 0 else d7_r.revenue end) as d7_revenue
    ,sum(case when d30_r.revenue is null then 0 else d30_r.revenue end) as d30_revenue
    ,sum(case when d60_r.revenue is null then 0 else d60_r.revenue end) as d60_revenue
    ,sum(case when d90_r.revenue is null then 0 else d90_r.revenue end) as d90_revenue
    ,sum(case when d120_r.revenue is null then 0 else d120_r.revenue end) as d120_revenue

    ,sum(case when r.is_payer is null then 0 else r.is_payer end) as payers
    ,sum(case when d1_r.is_payer is null then 0 else d1_r.is_payer end) as d1_payers
    ,sum(case when d3_r.is_payer is null then 0 else d3_r.is_payer end) as d3_payers
    ,sum(case when d7_r.is_payer is null then 0 else d7_r.is_payer end) as d7_payers
    ,sum(case when d30_r.is_payer is null then 0 else d30_r.is_payer end) as d30_payers
    ,sum(case when d60_r.is_payer is null then 0 else d60_r.is_payer end) as d60_payers
    ,sum(case when d90_r.is_payer is null then 0 else d90_r.is_payer end) as d90_payers
    ,sum(case when d120_r.is_payer is null then 0 else d120_r.is_payer end) as d120_payers

    ,sum(case when d1_retained.retained is null then 0 else d1_retained.retained end) as d1_retained
    ,sum(case when d3_retained.retained is null then 0 else d3_retained.retained end) as d3_retained
    ,sum(case when d7_retained.retained is null then 0 else d7_retained.retained end) as d7_retained
    ,sum(case when d30_retained.retained is null then 0 else d30_retained.retained end) as d30_retained
    ,sum(case when d60_retained.retained is null then 0 else d60_retained.retained end) as d60_retained
    ,sum(case when d90_retained.retained is null then 0 else d90_retained.retained end) as d90_retained
    ,sum(case when d120_retained.retained is null then 0 else d120_retained.retained end) as d120_retained
from tmp_new_install_users u
    left join tmp_all_revenue r on u.user_key = r.user_key
    left join tmp_d1_revenue d1_r on u.user_key = d1_r.user_key
    left join tmp_d3_revenue d3_r on u.user_key = d3_r.user_key
    left join tmp_d7_revenue d7_r on u.user_key = d7_r.user_key
    left join tmp_d30_revenue d30_r on u.user_key = d30_r.user_key
    left join tmp_d60_revenue d60_r on u.user_key = d60_r.user_key
    left join tmp_d90_revenue d90_r on u.user_key = d90_r.user_key
    left join tmp_d120_revenue d120_r on u.user_key = d120_r.user_key

    left join tmp_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_d3_retained d3_retained on u.user_key = d3_retained.user_key
    left join tmp_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_d30_retained d30_retained on u.user_key = d30_retained.user_key
    left join tmp_d60_retained d60_retained on u.user_key = d60_retained.user_key
    left join tmp_d90_retained d90_retained on u.user_key = d90_retained.user_key
    left join tmp_d120_retained d120_retained on u.user_key = d120_retained.user_key
group by 1,2,3,4,5,6,7,8,9,10,11;

delete from kpi_processed.agg_marketing_kpi where install_date < '2014-09-01'
--and app like '_game_%'
    ;

update kpi_processed.agg_marketing_kpi
set d1_new_installs = 0,
    d1_retained = 0,
    d1_revenue = 0,
    d1_payers = 0
where install_date >= CURRENT_DATE - 1
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d3_new_installs = 0,
    d3_retained = 0,
    d3_revenue = 0,
    d3_payers = 0
where install_date >= CURRENT_DATE - 3
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d7_new_installs = 0,
    d7_retained = 0,
    d7_revenue = 0,
    d7_payers = 0
where install_date >= CURRENT_DATE - 7
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d30_new_installs = 0,
    d30_retained = 0,
    d30_revenue = 0,
    d30_payers = 0
where install_date >= CURRENT_DATE - 30
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d60_new_installs = 0,
    d60_retained = 0,
    d60_revenue = 0,
    d60_payers = 0
where install_date >= CURRENT_DATE - 60
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d90_new_installs = 0,
    d90_retained = 0,
    d90_revenue = 0,
    d90_payers = 0
where install_date >= CURRENT_DATE - 90
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d120_new_installs = 0,
    d120_retained = 0,
    d120_revenue = 0,
    d120_payers = 0
where install_date >= CURRENT_DATE - 120
--and app like '_game_%'
;


------agg monthly kpi-------------------------
delete from kpi_processed.agg_kpi_monthly
where datediff(month, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_monthly
select date, app_id, os, country, 
sum(is_new_user) as new_installs, 
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as mau,
sum(revenue_usd) as revenue
from
    (select a.*, u.app_id, u.os, u.country
    from
    (select date_trunc('month', date) as date, user_key,
    max(is_new_user) as is_new_user,
    max(is_converted_today) as is_new_payer,
    sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot
    where datediff(month, date, sysdate)<=2
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by 1,2,3,4;



------agg yearly kpi-------------------------
delete from kpi_processed.agg_kpi_yearly
where datediff(year, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_yearly
select date, app_id, os, country, 
sum(is_new_user) as new_installs, 
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as yau,
sum(revenue_usd) as revenue
from
    (select a.*, u.app_id, u.os, u.country
    from
    (select date_trunc('year', date) as date, user_key,
    max(is_new_user) as is_new_user,
    max(is_payer) as is_payer,
    max(is_converted_today) as is_new_payer,
    sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot
    where datediff(month, date, sysdate)<=2
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by 1,2,3,4;


------ agg weekly kpi -------------
delete from kpi_processed.agg_kpi_weekly
where datediff(week, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_weekly
select date, app_id, os, country, 
sum(is_new_user) as new_installs, 
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as wau,
sum(revenue_usd) as revenue
from
    (select a.*, u.app_id, u.os, u.country
    from
    (select date_trunc('week', date) as date, user_key,
    max(is_new_user) as is_new_user,
    max(is_converted_today) as is_new_payer,
    sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot
    where datediff(week, date, sysdate)<=2
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by 1,2,3,4;


----Marketing ROI reports----
create temp table temp_singular_mobile
as
select app
      ,start_date
      ,country_field
      ,os
      ,custom_installs
      ,adn_impressions
      ,custom_clicks
      ,adn_cost
      ,case
            -- Mappings for KOA
            when source = 'Aarki' then 'Aarki (Incent)'
            when source = 'Tapjoy' then 'Tapjoy (Incent)'
            when source = 'Fiksu' then 'Fiksu (Incent)'
            when source = 'NativeX' then 'NativeX (Incent)'
            when source = 'SupersonicAds' and position('nonincent' in lower(adn_campaign)) = 0 then 'SupersonicAds (Incent)'
            when source = 'SupersonicAds' and position('nonincent' in lower(adn_campaign)) > 0 then 'SupersonicAds'
            when source = 'Fyber' and position('nonincent' in lower(adn_campaign)) = 0 then 'Fyber (Incent)'
            when source = 'Fyber' and position('nonincent' in lower(adn_campaign)) > 0 then 'Fyber'
            when source = 'Glispa' and position('incent' in lower(adn_campaign)) > 0 then 'Glispa (Incent)'
            when source = 'Glispa' and position('incent' in lower(adn_campaign)) = 0 then 'Glispa'
            when source = 'Mobvista' and position('incent' in lower(adn_campaign)) > 0 then 'Mobvista (Incent)'
            when source = 'Mobvista' and position('incent' in lower(adn_campaign)) = 0 then 'Mobvista'
            when source = 'AdWords' and position('gdn' in lower(adn_campaign)) > 0 then 'Google Display Network'
            when source = 'AdWords' and position('src' in lower(adn_campaign)) > 0 then 'Google Search Network'
            when source = 'AdWords' and position('uac' in lower(adn_campaign)) > 0 then 'Google Universal App Campaigns'
            when source = 'AdWords' and position('vdo' in lower(adn_campaign)) > 0 then 'Youtube Installs'
            when source = 'AdWords' and position('admob' in lower(adn_campaign)) > 0 then 'Google AdMob'
            when source = 'Facebook' and position('_ins' in lower(adn_campaign)) > 0 then 'Instagram Installs'
            when source = 'Facebook' and position('papaya' in lower(adn_campaign)) > 0 then 'Papaya_FB'
            when source = 'Facebook' and (position('_ins' in lower(adn_campaign)) = 0  and position('papaya' in lower(adn_campaign)) = 0) then 'Facebook Installs'
            -- Copy from WAF
            when source = 'AppLift' and position('social' in lower(adn_campaign)) = 0 then 'AppLift'
            when source = 'AppLift' and position('social' in lower(adn_campaign)) > 0 then 'AppLift-Social Channels'
            when source = 'AppLift' and position('incent' in lower(adn_campaign)) > 0 then 'AppLift(incent)'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) > 0 then 'Adwords(Search)'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) = 0 then 'Adwords(Display)'
            when source = 'MassiveImpact' and position('myh' in lower(adn_campaign)) > 0 then 'MassiveImpact-Yahoo'
            when source = 'MassiveImpact' and position('mtr' in lower(adn_campaign)) > 0 then 'MassiveImpact-Tumblr'
            when source = 'MassiveImpact' and (position('myh' in lower(adn_campaign)) = 0  and position('mtr' in lower(adn_campaign)) = 0) then 'MassiveImpact-Twitter'
            when source = 'Facebook' and position('_ins_' in lower(adn_campaign)) > 0 then 'Instagram Installs'
            when source = 'Facebook' and position('_ins_' in lower(adn_campaign)) = 0 then 'Facebook Installs'
            when source = 'Motive Interactive' then 'Motive'
            when position('incent' in lower(adn_campaign)) >0 then source + '(incent)'
            else source
       end as source
from raw_events.singular
where app in ('Citadel Realms','King of Avalon');


-- install_source_map
create temp table install_source_mapping_mobile as
select distinct 'Marketing' as source
      ,case when app = 'loe.global.prod' then 'Citadel Realms'
            when app = 'koa.global.prod' then 'King of Avalon'
       else '' end as game
      ,install_source
      ,install_source as install_source_lower
from   kpi_processed.agg_marketing_kpi
where app in ('loe.global.prod','koa.global.prod')
union all
select distinct 'Singular' as source
      ,app as game
      ,source as install_source
      ,source as install_source_lower
from temp_singular_mobile;


update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\+', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\-', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\(', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\)', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, ' ', '');

update install_source_mapping_mobile
set install_source_lower = lower(install_source_lower)
;


--installs
create temp table installs_mobile as
select m.app
      ,m.install_date
      ,s.install_source_lower
      ,case when lower(os) = 'ios' then 'iOS'
             when lower(os) = 'android' then 'Android'
       else os end as os
      ,sum(new_installs) as new_installs
from kpi_processed.agg_marketing_kpi m
join install_source_mapping_mobile s
on   s.source = 'Marketing'
and s.install_source = m.install_source
and s.game = case when m.app = 'loe.global.prod' then 'Citadel Realms'
                  when m.app = 'koa.global.prod' then 'King of Avalon'
                  else 'Others' end
where m.app in ('loe.global.prod','koa.global.prod')
group by 1,2,3,4;


--cost
create temp table cost_mobile as
select c.app as app
      ,c.start_date as install_date
      ,s.install_source_lower
      ,c.os
      ,sum(cast (case when custom_installs = 'None' then '0' else custom_installs end as numeric)) as singular_installs
      ,sum(cast (case when adn_impressions = 'None' then '0' else adn_impressions end as numeric)) as adn_impressions
      ,sum(cast (case when custom_clicks = 'None' then '0' else custom_clicks end as numeric)) as clicks
      ,sum(cast(case when adn_cost = 'None' then '0' else adn_cost end as numeric(14,4))) as cost
from  temp_singular_mobile c
join  install_source_mapping_mobile s
on    s.source = 'Singular'
and   c.source = s.install_source
and   c.app = s.game
group by 1,2,3,4;


--install_cost
create temp table install_cost_mobile as
select i.install_date
      ,i.os
      ,i.app
      ,i.install_source_lower
      ,m.install_source
      ,coalesce(i.new_installs,0) as bi_installs
      ,coalesce(c.singular_installs,0) as singular_installs
      ,coalesce(c.adn_impressions,0) as adn_impressions
      ,coalesce(c.clicks,0) as clicks
      ,coalesce(c.cost,0) as cost
from installs_mobile i
join install_source_mapping_mobile m
on i.install_source_lower = m.install_source_lower
and m.source = 'Marketing'
and m.game = case when i.app = 'loe.global.prod' then 'Citadel Realms'
                  when i.app = 'koa.global.prod' then 'King of Avalon'
                  else 'Others' end
left join cost_mobile c
on i.os = c.os
and i.install_source_lower = c.install_source_lower
and i.install_date = c.install_date
and c.app = case when i.app = 'loe.global.prod' then 'Citadel Realms'
                 when i.app = 'koa.global.prod' then 'King of Avalon'
                 else 'Others' end;

drop table if exists kpi_processed.agg_marketing_kpi_singular;
create table kpi_processed.agg_marketing_kpi_singular
as
select  k.app
       ,k.install_date
       ,k.install_date_str
       ,k.install_source
       ,k.install_source_group
       ,'' as country
       ,k.os
       ,ict.bi_installs as bi_installs
       ,ict.singular_installs as singular_installs
       ,ict.adn_impressions as adn_impressions
       ,ict.clicks as clicks
       ,ict.cost as cost
       ,install_month
       ,sum(new_installs)  as new_installs
       ,sum(d1_new_installs) as d1_new_installs
       ,sum(d7_new_installs) as d7_new_installs
       ,sum(d15_new_installs) as d15_new_installs
       ,sum(d30_new_installs) as d30_new_installs
       ,sum(d60_new_installs)  as d60_new_installs
       ,sum(d90_new_installs) as d90_new_installs
       ,sum(d120_new_installs) as d120_new_installs
       ,sum(d150_new_installs) as d150_new_installs
       ,sum(revenue)   as revenue
       ,sum(d1_revenue)  as d1_revenue
       ,sum(d7_revenue) as d7_revenue
       ,sum(d15_revenue) as d15_revenue
       ,sum(d30_revenue) as d30_revenue
       ,sum(d60_revenue) as d60_revenue
       ,sum(d90_revenue) as d90_revenue
       ,sum(d120_revenue) as d120_revenue
       ,sum(d150_revenue) as d150_revenue
       ,sum(payers)  as payers
       ,sum(d1_payers)  as d1_payers
       ,sum(d7_payers) as d7_payers
       ,sum(d15_payers) as d15_payers
       ,sum(d30_payers) as d30_payers
       ,sum(d60_payers) as d60_payers
       ,sum(d90_payers) as d90_payers
       ,sum(d120_payers) as d120_payers
       ,sum(d150_payers) as d150_payers
       ,sum(d1_retained) as d1_retained
       ,sum(d7_retained)  as d7_retained
       ,sum(d15_retained)  as d15_retained
       ,sum(d30_retained) as d30_retained
       ,sum(d60_retained) as d60_retained
       ,sum(d90_retained)  as d90_retained
       ,sum(d120_retained)   as d120_retained
       ,sum(d150_retained)   as d150_retained
       ,sum(d3_new_installs)  as d3_new_installs
       ,sum(d3_revenue) as d3_revenue
       ,sum(d3_payers)  as d3_payers
       ,sum(d3_retained) as d3_retained
from  kpi_processed.agg_marketing_kpi k
left join install_cost_mobile ict
on k.app = ict.app
and k.install_source = ict.install_source
and k.install_date = ict.install_date
and ict.os = k.os
where k.app in ('loe.global.prod','koa.global.prod')
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;
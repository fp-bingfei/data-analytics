----------------------------------------------------------
--Data 2.0kpi_processed.new_user
----------------------------------------------------------


delete from kpi_processed.new_user
where ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
--and app_id like '_game_%'
;

insert into kpi_processed.new_user 
select    event_id::varchar(128)          
         ,data_version::varchar(10)      
         ,app_id::varchar(64)            
         ,ts               
         ,ts_pretty        
         ,event::varchar(64)             
         ,user_id::varchar(64)           
         ,session_id::varchar(128)       
         ,app_version::varchar(64)       
         ,gameserver_id::varchar(128)     
         ,case when lower(os) = 'ios' then 'iOS' when lower(os) = 'android' then 'Android' else os::varchar(64) end as os                
         ,regexp_substr(os_version, '[0-9]+\\.[0-9]+(\\.[0-9]+)?') as os_version       
         ,browser::varchar(32)           
         ,browser_version::varchar(64)   
         ,idfa::varchar(128)              
         ,idfv::varchar(128)              
         ,gaid::varchar(128)              
         ,android_id::varchar(128)        
         ,mac_address::varchar(128)       
         ,device::varchar(64)            
         ,ip::varchar(15)                
         ,country_code::varchar(10)      
         ,lang::varchar(8)             
         ,level            
         ,vip_level        
         ,facebook_id::varchar(128)       
         ,gender::varchar(16)            
         ,first_name::varchar(64)        
         ,last_name::varchar(64)         
         ,birthday         
         ,email::varchar(256)             
         ,googleplus_id::varchar(128)     
         ,gamecenter_id::varchar(128)     
         ,install_ts
         ,install_source::varchar(1024)
         ,scene::varchar(32)
         ,fb_source::varchar(1024)
from 
(
    select     md5(app_id||event||ts||user_id||session_id) as event_id 
              ,data_version 
              ,app_id 
              ,cast(ts as bigint) as ts 
              ,ts_pretty
              ,event 
              ,user_id 
              ,session_id 
              ,json_extract_path_text(properties,'app_version') as app_version
              ,json_extract_path_text(properties,'gameserver_id') as gameserver_id 
              ,json_extract_path_text(properties,'os') as os
              ,json_extract_path_text(properties,'os_version') as os_version
              ,json_extract_path_text(properties,'browser') as browser 
              ,json_extract_path_text(properties,'browser_version') as browser_version 
              ,json_extract_path_text(properties,'idfa') as idfa 
              ,json_extract_path_text(properties,'idfv') as idfv 
              ,json_extract_path_text(properties,'gaid') as gaid 
              ,json_extract_path_text(properties,'android_id') as android_id
              ,json_extract_path_text(properties,'mac_address') as mac_address 
              ,json_extract_path_text(properties,'device') as device 
              ,json_extract_path_text(properties,'ip') as ip 
              ,json_extract_path_text(properties,'country_code') as country_code 
              ,json_extract_path_text(properties,'lang') as lang
              ,nvl(nullif(json_extract_path_text(properties,'level'), '')::int, 0) as level 
              ,nvl(nullif(json_extract_path_text(properties,'vip_level'), '')::int, 0) as vip_level
              ,case when app_id like 'lc_patch%' then json_extract_path_text(properties,'fpid') else  json_extract_path_text(properties,'facebook_id') end as facebook_id 
              ,json_extract_path_text(properties,'gender') as gender 
              ,json_extract_path_text(properties,'first_name') as first_name 
              ,json_extract_path_text(properties,'last_name') as last_name
              ,case when json_extract_path_text(properties,'birthday') = '' then to_date('01/01/1900', 'MM/DD/YYYY')
                    when position('-' in json_extract_path_text(properties,'birthday')) > 0 then to_date(json_extract_path_text(properties,'birthday'), 'YYYY-MM-DD')
                    when position('-' in json_extract_path_text(properties,'birthday')) > 0 then to_date(json_extract_path_text(properties,'birthday'), 'MM/DD/YYYY')
                    else to_date('01/01/1900', 'MM/DD/YYYY') end as birthday
              ,json_extract_path_text(properties,'email') as email 
              ,json_extract_path_text(properties,'googleplus_id') as googleplus_id 
              ,json_extract_path_text(properties,'gamecenter_id') as gamecenter_id 
              ,cast(json_extract_path_text(properties,'install_ts_pretty') as timestamp) as install_ts 
              ,json_extract_path_text(properties,'install_source') as install_source 
              ,json_extract_path_text(properties,'scene') as scene 
              ,json_extract_path_text(properties,'fb_source') as fb_source 
              ,row_number() over(partition by app_id, event_id) as rnum
    from tmp_raw_events
    where event = 'new_user'
    and json_extract_path_text(properties,'install_ts_pretty') <> '' 
    --and app_id like '_game_%'
)t
where t.rnum = 1
;


update kpi_processed.new_user set scene='Main' where (scene='1' or scene is null or scene='') and ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
        --and app_id like '_game_%'
        ;

----------------------------------------------------------
--kpi_processed.session_end (No session_end for Farm)
----------------------------------------------------------


delete from kpi_processed.session_end
where ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
--and app_id like '_game_%'
;
        
insert into kpi_processed.session_end
select    event_id::varchar(128)        
         ,data_version::varchar(10)    
         ,app_id::varchar(64)          
         ,ts              
         ,ts_pretty      
         ,event::varchar(64)           
         ,user_id::varchar(64)         
         ,session_id::varchar(128)      
         ,session_length 
         ,scene::varchar(32)
         ,fb_source::varchar(1024)
         ,level
from
(
   select     md5(app_id||event||ts||user_id||session_id) as event_id 
             ,data_version 
             ,app_id 
             ,cast(ts as bigint) as ts 
             ,ts_pretty
             ,event 
             ,user_id 
             ,session_id 
             ,round(coalesce(nullif(json_extract_path_text(properties,'session_length'),''), '0'))::bigint as session_length
             ,json_extract_path_text(properties,'scene') as scene
             ,json_extract_path_text(properties,'fb_source') as fb_source
             ,nullif(json_extract_path_text(properties,'level'),'')::int as level
             ,row_number() over(partition by session_id order by ts desc) as rnum
   from      tmp_raw_events
   where event = 'session_end'
   --and app_id like '_game_%'
)t
where t.rnum = 1
;

update kpi_processed.session_end set scene='Main' where (scene='1' or scene is null or scene='') and ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
       --and app_id like '_game_%'
       ;
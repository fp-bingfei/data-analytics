-----------------------------------------------
--Data 2.0 Setup script
-----------------------------------------------
update kpi_processed.init_start_date                                                          
set start_date =  DATEADD(day, -3, CURRENT_DATE);

drop table if exists tmp_raw_events;

create table tmp_raw_events as
select *
from raw_events.events
where ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        );

delete from tmp_raw_events
where user_id is null or user_id = '' or app_id is null;

delete from tmp_raw_events
where ts_pretty>='2016-06-01' and app_id like 'loe%'
and json_extract_path_text(properties, 'level')='';


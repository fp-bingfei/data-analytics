--------------------------------------------------------------------------------------------------------------------------------------------
--FF tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate marketing tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

delete from kpi_processed.agg_marketing_kpi
--where app like '_game_%'
;

create temp table tmp_new_install_users as
select user_key
    ,app_id as app
    ,user_id as uid
    ,install_date
    ,case when (lower(install_campaign) like '%papaya%' and app_id like 'koa%') then 'Papaya_FB'
      else install_source end
     as install_source
    ,case when install_source in ('Untrusted Devices','Organic','null') then 'Untracked Install Source' when install_source in ('ManAtArm Project (Influencer)','KOA Community','Grubby','Ytop10_KR','Youtuber','Touch Arcade','SplitMetrics','SocialShare_0829','Roostr','Retargeting_0829','PreRegistrationKR_2','PreRegistrationKR','PreRegistrationJP','Prelaunch','PR_Famitsu','Pocket Gamer','Official Site','Games Radar (Review Sites Ads)','Email0725','EDM_Youtube','EDM_Round2','EDM_Funplus') then 'Non-perf&Special Campaigns' when install_source in ('Yeahmobi (Global)','Vungle ROW','Unity Ads ROW','Tapjoy ROW','Mobvista (Global)','Mars Media (Global)','Leadbolt ROW','AdColony ROW') then 'Global Campaign Source' when install_source in ('Zorka.Mobi','youAPPi','Yeahmobi','Unilead','StartApp','SharkGames','Shark Games','SevenGames Network','Secco Squared','PLYmedia','Mobvista','Mobisharks','mobihunter','Mobihunter','Matomy','Mars Media','LoopMe','Kokonut_myTarget','IconPeak','everyads','Dreambox (Affiliate)','Dreambox (Affiliate?','Dreambox','DataLead','Datalead','Clickky','ClickHeaven','Cheetah Mobile','Blind Ferret Media','Avazu','Appia','Adsup','Adbuddiz','Ad4Game','Actionpay','Zenna Apps','Viber','Sweetlabs','Session M','Septeni','RU influencer (Wakeapp)','myTarget(mail.ru)','Motive','Mobusi','Metaps Exchanger','MADUP','Liftoff(Septeni)','Leadbolt','Ironsource','Inmobi','HD_Video_player_001avazu','Glispa','FB_Play Game','DSNR Media Group (DMG)','DSNR Media Group (DMG Native)','DE influencer (Sevengames)','DAU-UP','Cyber Z','CrossChannel','Bee7','Baidu Union','AppLift','Appier','AdWays','Adinnovation','Vungle','UnityAds','Unity Ads','Twitter Installs','Tapjoy','SupersonicAds','Jungroup','iAd Installs','Helium','Fyber','Chartboost','BlueStacks','Appturbo','Applovin','AppLike','Apple Search Ads','Adcolony') then 'Other Install Source' when install_source in ('Tapjoy(CPE)','Nutzz Media','Hele (Incent)','Adways(CPE)','Unilead(Incent)','Tapjoy (Incent)','Tapjoy (CPE)','SupersonicAds (Incent)','Offerboard (Incent)','NativeX (Incent)','Mobvista (Incent)','Mobihunter(Incent)','Line (Incent)','Jump Ramp Games','Gree (Incent)','Glispa (Incent)','Fyber (Incent)','Fiksu (Incent)','Appdriver (Incent)','Appang (Incent)','Aarki (Incent)','113 (incent)') then 'Incent Install Source' when install_source in ('Adjust Organic') then 'Organic Install Source' when install_source in ('Hylink-AdMob','Youtube Installs','Wezonet_Google Admob','Wezonet','Google Video Campaign','Google Universal App Campaigns','Google Search Network','Google Organic Search','Google Display Network','Google AdWords Search','Google AdWords','Google AdMob','Google (unknown)') then 'Google Install Source' when install_source in ('Papaya_FB','Off-Facebook Installs','Instagram Installs','Facebook Installs','Clickkwala_FB','Baidu_FB') then 'Facebook Install Source' else 'Other Install Source' end as install_source_group
    ,install_campaign as campaign
    ,install_subpublisher as sub_publisher
    ,install_creative_id as creative_id
    ,install_country as country
    ,case when app_version = '1.9.2.17' or app_version = '2.0.0.20' then 'Amazon'
        else install_os end
     as os
    ,level
    ,is_payer
from kpi_processed.dim_user
where app_id != 'loe.global.prod' or level > 0;
--Remove level = 0 data from Citadel Realms, which are fake users
--where app_id like '_game_%'
;


create temp table tmp_all_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
--where app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d1_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
    join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 1
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d3_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 3
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d7_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 7
--and app_id like '_game_%'
group by d.user_key, d.app_id;
create temp table tmp_d15_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 15
--and app_id like '_game_%'
group by d.user_key, d.app_id;



create temp table tmp_d30_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 30
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d60_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 60
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d90_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 90
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d120_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 120
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d150_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 150
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;
create temp table tmp_d1_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 1
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d3_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 3
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d7_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 7
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;
create temp table tmp_d15_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 15
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d30_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 30
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d60_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 60
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d90_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 90
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d120_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 120
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d150_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 150
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;


insert into kpi_processed.agg_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_month
    ,install_source
    ,install_source_group
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os
    ,level

    ,new_installs
    ,d1_new_installs
    ,d3_new_installs
    ,d7_new_installs
    ,d15_new_installs
    ,d30_new_installs
    ,d60_new_installs
    ,d90_new_installs
    ,d120_new_installs
    ,d150_new_installs

    ,revenue
    ,d1_revenue
    ,d3_revenue
    ,d7_revenue
    ,d15_revenue
    ,d30_revenue
    ,d60_revenue
    ,d90_revenue
    ,d120_revenue
    ,d150_revenue

    ,payers
    ,d1_payers
    ,d3_payers
    ,d7_payers
    ,d15_payers
    ,d30_payers
    ,d60_payers
    ,d90_payers
    ,d120_payers
    ,d150_payers

    ,d1_retained
    ,d3_retained
    ,d7_retained
    ,d15_retained
    ,d30_retained
    ,d60_retained
    ,d90_retained
    ,d120_retained
    ,d150_retained
    ,level_u2
    ,level_a2u5
    ,level_a5u10
    ,level_a10
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,substring(u.install_date, 1, 7) as install_month
    ,u.install_source::VARCHAR(128)
    ,u.install_source_group::VARCHAR(128)
    ,u.campaign::VARCHAR(128)
    ,u.sub_publisher
    ,u.creative_id
    ,u.country
    ,u.os
    ,u.level

    ,count(1) as new_installs
    ,count(1) as d1_new_installs
    ,count(1) as d3_new_installs
    ,count(1) as d7_new_installs
    ,count(1) as d15_new_installs
    ,count(1) as d30_new_installs
    ,count(1) as d60_new_installs
    ,count(1) as d90_new_installs
    ,count(1) as d120_new_installs
    ,count(1) as d150_new_installs

    ,sum(case when r.revenue is null then 0 else r.revenue end) as revenue
    ,sum(case when d1_r.revenue is null then 0 else d1_r.revenue end) as d1_revenue
    ,sum(case when d3_r.revenue is null then 0 else d3_r.revenue end) as d3_revenue
    ,sum(case when d7_r.revenue is null then 0 else d7_r.revenue end) as d7_revenue
    ,sum(case when d15_r.revenue is null then 0 else d15_r.revenue end) as d15_revenue
    ,sum(case when d30_r.revenue is null then 0 else d30_r.revenue end) as d30_revenue
    ,sum(case when d60_r.revenue is null then 0 else d60_r.revenue end) as d60_revenue
    ,sum(case when d90_r.revenue is null then 0 else d90_r.revenue end) as d90_revenue
    ,sum(case when d120_r.revenue is null then 0 else d120_r.revenue end) as d120_revenue
    ,sum(case when d150_r.revenue is null then 0 else d150_r.revenue end) as d150_revenue

    ,sum(case when r.is_payer is null then 0 else r.is_payer end) as payers
    ,sum(case when d1_r.is_payer is null then 0 else d1_r.is_payer end) as d1_payers
    ,sum(case when d3_r.is_payer is null then 0 else d3_r.is_payer end) as d3_payers
    ,sum(case when d7_r.is_payer is null then 0 else d7_r.is_payer end) as d7_payers
    ,sum(case when d15_r.is_payer is null then 0 else d15_r.is_payer end) as d15_payers
    ,sum(case when d30_r.is_payer is null then 0 else d30_r.is_payer end) as d30_payers
    ,sum(case when d60_r.is_payer is null then 0 else d60_r.is_payer end) as d60_payers
    ,sum(case when d90_r.is_payer is null then 0 else d90_r.is_payer end) as d90_payers
    ,sum(case when d120_r.is_payer is null then 0 else d120_r.is_payer end) as d120_payers
    ,sum(case when d150_r.is_payer is null then 0 else d150_r.is_payer end) as d150_payers

    ,sum(case when d1_retained.retained is null then 0 else d1_retained.retained end) as d1_retained
    ,sum(case when d3_retained.retained is null then 0 else d3_retained.retained end) as d3_retained
    ,sum(case when d7_retained.retained is null then 0 else d7_retained.retained end) as d7_retained
    ,sum(case when d15_retained.retained is null then 0 else d15_retained.retained end) as d15_retained
    ,sum(case when d30_retained.retained is null then 0 else d30_retained.retained end) as d30_retained
    ,sum(case when d60_retained.retained is null then 0 else d60_retained.retained end) as d60_retained
    ,sum(case when d90_retained.retained is null then 0 else d90_retained.retained end) as d90_retained
    ,sum(case when d120_retained.retained is null then 0 else d120_retained.retained end) as d120_retained
    ,sum(case when d150_retained.retained is null then 0 else d150_retained.retained end) as d150_retained
    ,sum(case when u.level < 2 then 1 else 0 end) as level_u2
    ,sum(case when u.level >= 2 and u.level < 5 then 1 else 0 end) as level_a2u5
    ,sum(case when u.level >= 5 and u.level < 10 then 1 else 0 end) as level_a5u10
    ,sum(case when u.level >= 10 then 1 else 0 end) as level_a10
from tmp_new_install_users u
    left join tmp_all_revenue r on u.user_key = r.user_key
    left join tmp_d1_revenue d1_r on u.user_key = d1_r.user_key
    left join tmp_d3_revenue d3_r on u.user_key = d3_r.user_key
    left join tmp_d7_revenue d7_r on u.user_key = d7_r.user_key
    left join tmp_d15_revenue d15_r on u.user_key = d15_r.user_key
    left join tmp_d30_revenue d30_r on u.user_key = d30_r.user_key
    left join tmp_d60_revenue d60_r on u.user_key = d60_r.user_key
    left join tmp_d90_revenue d90_r on u.user_key = d90_r.user_key
    left join tmp_d120_revenue d120_r on u.user_key = d120_r.user_key
    left join tmp_d150_revenue d150_r on u.user_key = d150_r.user_key

    left join tmp_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_d3_retained d3_retained on u.user_key = d3_retained.user_key
    left join tmp_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_d15_retained d15_retained on u.user_key = d15_retained.user_key
    left join tmp_d30_retained d30_retained on u.user_key = d30_retained.user_key
    left join tmp_d60_retained d60_retained on u.user_key = d60_retained.user_key
    left join tmp_d90_retained d90_retained on u.user_key = d90_retained.user_key
    left join tmp_d120_retained d120_retained on u.user_key = d120_retained.user_key
    left join tmp_d150_retained d150_retained on u.user_key = d150_retained.user_key
group by 1,2,3,4,5,6,7,8,9,10,11,12;

delete from kpi_processed.agg_marketing_kpi where install_date < '2014-09-01'
--and app like '_game_%'
    ;

update kpi_processed.agg_marketing_kpi
set d1_new_installs = 0,
    d1_retained = 0,
    d1_revenue = 0,
    d1_payers = 0
where install_date >= CURRENT_DATE - 1
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d3_new_installs = 0,
    d3_retained = 0,
    d3_revenue = 0,
    d3_payers = 0
where install_date >= CURRENT_DATE - 3
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d7_new_installs = 0,
    d7_retained = 0,
    d7_revenue = 0,
    d7_payers = 0
where install_date >= CURRENT_DATE - 7
--and app like '_game_%'
;
update kpi_processed.agg_marketing_kpi
set d15_new_installs = 0,
    d15_retained = 0,
    d15_revenue = 0,
    d15_payers = 0
where install_date >= CURRENT_DATE - 15
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d30_new_installs = 0,
    d30_retained = 0,
    d30_revenue = 0,
    d30_payers = 0
where install_date >= CURRENT_DATE - 30
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d60_new_installs = 0,
    d60_retained = 0,
    d60_revenue = 0,
    d60_payers = 0
where install_date >= CURRENT_DATE - 60
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d90_new_installs = 0,
    d90_retained = 0,
    d90_revenue = 0,
    d90_payers = 0
where install_date >= CURRENT_DATE - 90
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d120_new_installs = 0,
    d120_retained = 0,
    d120_revenue = 0,
    d120_payers = 0
where install_date >= CURRENT_DATE - 120
--and app like '_game_%'
;


update kpi_processed.agg_marketing_kpi
set d150_new_installs = 0,
    d150_retained = 0,
    d150_revenue = 0,
    d150_payers = 0
where install_date >= CURRENT_DATE - 150
--and app like '_game_%'
;
------agg monthly kpi-------------------------
delete from kpi_processed.agg_kpi_monthly
where datediff(month, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_monthly
select date, app_id, os, country,
sum(is_new_user) as new_installs,
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as mau,
sum(revenue_usd) as revenue,
install_source_group,
install_source
from
    (select a.*, u.app_id, u.os, u.country,
        case when u.app_id like 'farm%' then u.install_source_group
           else u.install_source end as install_source_group,
        u.install_source
    from
    (select date_trunc('month', date) as date, user_key,
    max(is_new_user) as is_new_user,
    max(is_converted_today) as is_new_payer,
    sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot
    where datediff(month, date, sysdate)<=2
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by date,app_id,os,country,install_source_group,install_source;



------agg yearly kpi-------------------------
delete from kpi_processed.agg_kpi_yearly
where datediff(year, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_yearly
select date, app_id, os, country,
sum(is_new_user) as new_installs,
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as yau,
sum(revenue_usd) as revenue
from
    (select a.*, u.app_id, u.os, u.country
    from
    (select date_trunc('year', date) as date, user_key,
    max(is_new_user) as is_new_user,
    max(is_payer) as is_payer,
    max(is_converted_today) as is_new_payer,
    sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot
    where datediff(year, date, sysdate)<=2
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by 1,2,3,4;


------ agg weekly kpi -------------
delete from kpi_processed.agg_kpi_weekly
where date>=(select max(date) from kpi_processed.agg_kpi_weekly)
and date<=(select dateadd('day',7,max(date) ) from kpi_processed.agg_kpi_weekly)
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_weekly
select date, app_id, os, country,
sum(is_new_user) as new_installs,
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as wau,
sum(revenue_usd) as revenue,
install_source_group,
install_source
from
    (select a.*, u.app_id, u.os, u.country,
        case when u.app_id like 'farm%' then u.install_source_group
           else u.install_source end as install_source_group,
        u.install_source
    from
    (select
        date_trunc('week', date) as date,
        user_key,
        max(is_new_user) as is_new_user,
        max(is_converted_today) as is_new_payer,
        sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot
    where
      date>=(select dateadd('day',7,max(date) ) from kpi_processed.agg_kpi_weekly)
     and date<=(select dateadd('day',14,max(date) ) from kpi_processed.agg_kpi_weekly)
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by date,app_id,os,country,install_source_group,install_source;


----Marketing ROI reports----
create temp table temp_singular_mobile
as
select app
      ,start_date
      ,country_field
      ,os
      ,custom_installs
      ,adn_impressions
      ,custom_clicks
      ,adn_cost
      ,case
            -- Mappings for KOA
          --  when source = 'Aarki' then 'Aarki (Incent)'
          --  when source = 'Tapjoy' then 'Tapjoy (Incent)'
          --  when source = 'Fiksu' then 'Fiksu (Incent)'
          --  when source = 'NativeX' then 'NativeX (Incent)'
          --  when source = 'SupersonicAds' and position('nonincent' in lower(adn_campaign)) = 0 then 'SupersonicAds (Incent)'
          --  when source = 'SupersonicAds' and position('nonincent' in lower(adn_campaign)) > 0 then 'SupersonicAds'
          --  when source = 'Fyber' and position('nonincent' in lower(adn_campaign)) = 0 then 'Fyber (Incent)'
          --  when source = 'Fyber' and position('nonincent' in lower(adn_campaign)) > 0 then 'Fyber'
          --  when source = 'Glispa' and position('incent' in lower(adn_campaign)) > 0 then 'Glispa (Incent)'
          --  when source = 'Glispa' and position('incent' in lower(adn_campaign)) = 0 then 'Glispa'
          --  when source = 'Mobvista' and position('incent' in lower(adn_campaign)) > 0 then 'Mobvista (Incent)'
          --  when source = 'Mobvista' and position('incent' in lower(adn_campaign)) = 0 then 'Mobvista'
          --  when source = 'AdWords' and position('gdn' in lower(adn_campaign)) > 0 then 'Google Display Network'
          --  when source = 'AdWords' and position('src' in lower(adn_campaign)) > 0 then 'Google Search Network'
          --  when source = 'AdWords' and position('uac' in lower(adn_campaign)) > 0 then 'Google Universal App Campaigns'
          --  when source = 'AdWords' and position('vdo' in lower(adn_campaign)) > 0 then 'Youtube Installs'
          --  when source = 'AdWords' and position('admob' in lower(adn_campaign)) > 0 then 'Google AdMob'
          --  when source = 'Facebook' and position('_ins' in lower(adn_campaign)) > 0 then 'Instagram Installs'
          --  when source = 'Facebook' and position('papaya' in lower(adn_campaign)) > 0 then 'Papaya_FB'
          --  when source = 'Facebook' and (position('_ins' in lower(adn_campaign)) = 0  and position('papaya' in lower(adn_campaign)) = 0) then 'Facebook Installs'
            -- Copy from WAF
            when source = 'AppLift' and position('social' in lower(adn_campaign)) = 0 then 'AppLift'
            when source = 'AppLift' and position('social' in lower(adn_campaign)) > 0 then 'AppLift-Social Channels'
            when source = 'AppLift' and position('incent' in lower(adn_campaign)) > 0 then 'AppLift(incent)'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) > 0 then 'Adwords(Search)'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) = 0 then 'Adwords(Display)'
            when source = 'MassiveImpact' and position('myh' in lower(adn_campaign)) > 0 then 'MassiveImpact-Yahoo'
            when source = 'MassiveImpact' and position('mtr' in lower(adn_campaign)) > 0 then 'MassiveImpact-Tumblr'
            when source = 'MassiveImpact' and (position('myh' in lower(adn_campaign)) = 0  and position('mtr' in lower(adn_campaign)) = 0) then 'MassiveImpact-Twitter'
            when source = 'Facebook' and position('_ins_' in lower(adn_campaign)) > 0 then 'Instagram Installs'
            when source = 'Facebook' and position('_ins_' in lower(adn_campaign)) = 0 then 'Facebook Installs'
            when source = 'Motive Interactive' then 'Motive'
            when position('incent' in lower(adn_campaign)) >0 then source + '(incent)'
            else source
       end as source
from raw_events.singular
where app = 'Citadel Realms';
--'King of Avalon';


-- install_source_map
create temp table install_source_mapping_mobile as
select distinct 'Marketing' as source
      ,case when app = 'loe.global.prod' then 'Citadel Realms'
           -- when app = 'koa.global.prod' then 'King of Avalon'
       else '' end as game
      ,install_source
      ,install_source as install_source_lower
from   kpi_processed.agg_marketing_kpi
where app = 'loe.global.prod'--'koa.global.prod'
union all
select distinct 'Singular' as source
      ,app as game
      ,source as install_source
      ,source as install_source_lower
from temp_singular_mobile;


update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\+', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\-', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\(', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\)', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, ' ', '');

update install_source_mapping_mobile
set install_source_lower = lower(install_source_lower)
;


--installs
create temp table installs_mobile as
select m.app
      ,m.install_date
      ,s.install_source_lower
      ,case when lower(os) = 'ios' then 'iOS'
             when lower(os) = 'android' then 'Android'
       else os end as os
      ,sum(new_installs) as new_installs
from kpi_processed.agg_marketing_kpi m
join install_source_mapping_mobile s
on   s.source = 'Marketing'
and s.install_source = m.install_source
and s.game = case when m.app = 'loe.global.prod' then 'Citadel Realms'
                  --when m.app = 'koa.global.prod' then 'King of Avalon'
                  else 'Others' end
where m.app = 'loe.global.prod' --'koa.global.prod'
group by 1,2,3,4;


--cost
create temp table cost_mobile as
select c.app as app
      ,c.start_date as install_date
      ,s.install_source_lower
      ,c.os
      ,sum(cast (case when custom_installs = 'None' then '0' else custom_installs end as numeric)) as singular_installs
      ,sum(cast (case when adn_impressions = 'None' then '0' else adn_impressions end as numeric)) as adn_impressions
      ,sum(cast (case when custom_clicks = 'None' then '0' else custom_clicks end as numeric)) as clicks
      ,sum(cast(case when adn_cost = 'None' then '0' else adn_cost end as numeric(14,4))) as cost
from  temp_singular_mobile c
join  install_source_mapping_mobile s
on    s.source = 'Singular'
and   c.source = s.install_source
and   c.app = s.game
group by 1,2,3,4;


--install_cost
create temp table install_cost_mobile as
select i.install_date
      ,i.os
      ,i.app
      ,i.install_source_lower
      ,m.install_source
      ,coalesce(i.new_installs,0) as bi_installs
      ,coalesce(c.singular_installs,0) as singular_installs
      ,coalesce(c.adn_impressions,0) as adn_impressions
      ,coalesce(c.clicks,0) as clicks
      ,coalesce(c.cost,0) as cost
from installs_mobile i
join install_source_mapping_mobile m
on i.install_source_lower = m.install_source_lower
and m.source = 'Marketing'
and m.game = case when i.app = 'loe.global.prod' then 'Citadel Realms'
                  --when i.app = 'koa.global.prod' then 'King of Avalon'
                  else 'Others' end
left join cost_mobile c
on i.os = c.os
and i.install_source_lower = c.install_source_lower
and i.install_date = c.install_date
and c.app = case when i.app = 'loe.global.prod' then 'Citadel Realms'
                 --when i.app = 'koa.global.prod' then 'King of Avalon'
                 else 'Others' end;

delete from kpi_processed.agg_marketing_kpi_singular
where app not like 'koa%';

INSERT INTO kpi_processed.agg_marketing_kpi_singular
select  k.app
       ,k.install_date
       ,k.install_date_str
       ,k.install_source
       ,k.install_source_group
       ,k.os
       ,ict.bi_installs as bi_installs
       ,ict.singular_installs as singular_installs
       ,ict.adn_impressions as adn_impressions
       ,ict.clicks as clicks
       ,ict.cost as cost
       ,install_month
       ,sum(new_installs)  as new_installs
       ,sum(d1_new_installs) as d1_new_installs
       ,sum(d7_new_installs) as d7_new_installs
       ,sum(d15_new_installs) as d15_new_installs
       ,sum(d30_new_installs) as d30_new_installs
       ,sum(d60_new_installs)  as d60_new_installs
       ,sum(d90_new_installs) as d90_new_installs
       ,sum(d120_new_installs) as d120_new_installs
       ,sum(d150_new_installs) as d150_new_installs
       ,sum(revenue)   as revenue
       ,sum(d1_revenue)  as d1_revenue
       ,sum(d7_revenue) as d7_revenue
       ,sum(d15_revenue) as d15_revenue
       ,sum(d30_revenue) as d30_revenue
       ,sum(d60_revenue) as d60_revenue
       ,sum(d90_revenue) as d90_revenue
       ,sum(d120_revenue) as d120_revenue
       ,sum(d150_revenue) as d150_revenue
       ,sum(payers)  as payers
       ,sum(d1_payers)  as d1_payers
       ,sum(d7_payers) as d7_payers
       ,sum(d15_payers) as d15_payers
       ,sum(d30_payers) as d30_payers
       ,sum(d60_payers) as d60_payers
       ,sum(d90_payers) as d90_payers
       ,sum(d120_payers) as d120_payers
       ,sum(d150_payers) as d150_payers
       ,sum(d1_retained) as d1_retained
       ,sum(d7_retained)  as d7_retained
       ,sum(d15_retained)  as d15_retained
       ,sum(d30_retained) as d30_retained
       ,sum(d60_retained) as d60_retained
       ,sum(d90_retained)  as d90_retained
       ,sum(d120_retained)   as d120_retained
       ,sum(d150_retained)   as d150_retained
       ,sum(d3_new_installs)  as d3_new_installs
       ,sum(d3_revenue) as d3_revenue
       ,sum(d3_payers)  as d3_payers
       ,sum(d3_retained) as d3_retained
       ,'' as country
from  kpi_processed.agg_marketing_kpi k
left join install_cost_mobile ict
on k.app = ict.app
and k.install_source = ict.install_source
and k.install_date = ict.install_date
and ict.os = k.os
where k.app = 'loe.global.prod' --'koa.global.prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,52;
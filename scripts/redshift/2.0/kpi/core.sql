------------------------------------------------
--Data 2.0 fact_dau_snapshot.sql
------------------------------------------------
----- insert data to fact_dau_snapshot
DELETE FROM kpi_processed.fact_dau_snapshot
WHERE date >= 
           (
             select start_date 
             from kpi_processed.init_start_date
           )
           --and app_id like '_game_%'
           ;

insert into kpi_processed.fact_dau_snapshot
(
           id                  
           ,user_key            
           ,date                
           ,app_id              
           ,app_version         
           ,level_start         
           ,level_end           
           ,os                  
           ,os_version          
           ,device              
           ,browser             
           ,browser_version     
           ,country             
           ,language            
           ,is_new_user         
           ,is_payer            
           ,is_converted_today  
           ,revenue_usd         
           ,payment_cnt         
           ,session_cnt         
           ,playtime_sec  
           ,scene
           ,gameserver_id    
)
select     md5(s.app_id||s.date||s.user_id) as id
          ,s.user_key
          ,s.date
          ,s.app_id
          ,s.app_version
          ,s.level_start
          ,s.level_end
          ,s.os
          ,s.os_version
          ,s.device
          ,s.browser
          ,s.browser_version
          ,s.country
          ,s.language
          ,CASE                                               
                  WHEN s.date=s.install_date THEN           
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_new_user                            
              ,CASE                                         
                  WHEN s.date >= trunc(p.conversion_ts) THEN
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_payer                               
              ,CASE                                         
                  WHEN s.date = trunc(p.conversion_ts) THEN 
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_converted_today                     
          ,COALESCE(pd.revenue_usd,0) AS revenue_usd    
          ,COALESCE(pd.purchase_cnt,0) AS payment_cnt   
          ,s.session_cnt                                
          ,case
              when s.playtime_sec>1000000 then 1000000
              when s.playtime_sec<0 then 0
              else s.playtime_sec
           end as playtime_sec
           ,s.scene
           ,s.gameserver_id 
FROM      kpi_processed.tmp_user_daily_login s
LEFT JOIN kpi_processed.tmp_user_payment pd
ON        s.user_key=pd.user_key
AND       s.date=pd.date and s.scene=pd.scene
LEFT JOIN
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
        FROM kpi_processed.tmp_user_payment
        GROUP BY 1
    ) AS p
ON        p.user_key=s.user_key
WHERE     s.date >= 
                 (
                   select start_date 
                   from kpi_processed.init_start_date
                 )
                 --and s.app_id like '_game_%'
;

-- Attempt to backfill missing row on install date

insert into kpi_processed.fact_dau_snapshot
(
    id
    ,user_key
    ,date
    ,app_id
    ,app_version
    ,level_start
    ,level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,is_new_user
    ,is_payer
    ,is_converted_today
    ,revenue_usd
    ,payment_cnt
    ,session_cnt
    ,playtime_sec
    ,scene  
)
WITH missing_install_date AS
(
    SELECT    DISTINCT
              l.user_key
             ,l.install_date
    FROM      kpi_processed.tmp_user_daily_login l
    LEFT JOIN kpi_processed.fact_dau_snapshot i
    ON        l.user_key = i.user_key
    AND       l.install_date = i.date
    WHERE     l.install_date < (select start_date from kpi_processed.init_start_date)
    --and     l.app_id like '_game_%'
    AND       i.user_key is null
    UNION
    SELECT     DISTINCT
               l.user_key
              ,l.install_date
    FROM      kpi_processed.tmp_user_daily_login l
    LEFT JOIN kpi_processed.tmp_user_daily_login i
        ON    l.user_key = i.user_key
        AND   l.install_date = i.date
    WHERE     l.install_date >= (select start_date from kpi_processed.init_start_date)
    --and     l.app_id like '_game_%'
      AND     i.user_key is null
)
SELECT
     MD5(user_key || install_date) AS id
    ,user_key
    ,install_date as date
    ,app_id
    ,app_version
    ,1 AS level_start
    ,level_start as level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,1 AS is_new_user
    ,0 AS is_payer
    ,0 AS is_converted_today
    ,0 AS revenue_usd
    ,0 AS purchase_cnt
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,scene
FROM
    (
    SELECT
        d.*
        ,m.install_date
        ,row_number() over (partition by d.user_key order by d.date asc) as row
    FROM kpi_processed.fact_dau_snapshot d
    JOIN missing_install_date m
        ON  d.user_key = m.user_key
    WHERE d.date > m.install_date
    --and d.app_id like '_game_%'
    )  t
WHERE t.row=1
;


CREATE temp TABLE revenue_split
AS
SELECT SUM(CASE WHEN iap_product_id = 'trial' OR iap_product_id = 'flash_trial' THEN revenue_usd ELSE 0 END) AS revenue_ads,
       SUM(CASE WHEN iap_product_id <> 'trial' AND iap_product_id <> 'flash_trial' THEN revenue_usd ELSE 0 END) AS revenue_iap,
       user_key,
       app_id,
       DATE,
       scene
FROM kpi_processed.fact_revenue
WHERE app_id LIKE 'royal%'
GROUP BY 3,4, 5,6;

UPDATE kpi_processed.fact_dau_snapshot
   SET revenue_iap = s.revenue_iap,
       revenue_ads = s.revenue_ads
FROM revenue_split s
WHERE kpi_processed.fact_dau_snapshot.app_id = s.app_id
AND   kpi_processed.fact_dau_snapshot.date = s.date
AND   kpi_processed.fact_dau_snapshot.user_key = s.user_key
AND   kpi_processed.fact_dau_snapshot.scene = s.scene
AND   kpi_processed.fact_dau_snapshot.app_id LIKE 'royal%'
AND   kpi_processed.fact_dau_snapshot.date >= (SELECT start_date FROM kpi_processed.init_start_date);

delete from kpi_processed.fact_dau_snapshot where date >= current_date;
-------------------------------------------------
--Data 2.0 dim_user.sql
-------------------------------------------------

DROP TABLE IF EXISTS tmp_dim_user;
CREATE TEMP TABLE tmp_dim_user AS
WITH
    last_info AS
    (
        SELECT
            *
            ,row_number() over (partition by user_key order by date desc) as row
        FROM kpi_processed.tmp_user_daily_login
        where date >= 
                 (
                   select start_date 
                   from kpi_processed.init_start_date
                 )
        --and app_id like '_game_%' 
    )
    ,install_info AS
    (
        SELECT
             distinct user_key
            ,last_value(nullif(language,'') ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
            ,last_value(install_source ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
            ,last_value(install_source_group ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source_group
            ,last_value(country ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
            ,last_value(os ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
            ,last_value(device ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
            ,last_value(browser ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
            ,last_value(gender ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS gender
        FROM kpi_processed.tmp_user_daily_login 
        where install_date <= date
        and install_date > date - 15
        --and app_id like '_game_%' 
    )
    ,payment_info AS
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
            ,max(total_revenue_usd) as total_revenue_usd
            ,max(total_purchase_cnt) as total_purchase_cnt
        FROM kpi_processed.tmp_user_payment
        --where app_id like '_game_%'
        GROUP BY 1
    )

SELECT DISTINCT
  tu.user_key AS id
  ,tu.user_key
  ,tu.app_id
  ,tu.app_version
  ,tu.user_id
  ,COALESCE(du.facebook_id, tu.facebook_id) AS facebook_id
  ,case when tu.app_id like 'royal%' and tu.scene='2' then nvl(du.install_ts, tu.install_ts) else tu.install_ts end as install_ts
  ,case when tu.app_id like 'royal%' and tu.scene='2' then nvl(du.install_date, tu.install_date) else tu.install_date end as install_date
  ,ti.install_source_group
  ,coalesce(nullif(split_part(ti.install_source, '::', 1),''), 'Organic') AS install_source
  ,nullif(split_part(ti.install_source, '::', 3),'') AS install_subpublisher
  ,nullif(split_part(ti.install_source, '::', 2),'') AS install_campaign
  ,coalesce(ti.language,du.install_language) AS install_language
  ,coalesce(ti.country, du.install_country) AS install_country
  ,coalesce(ti.os, du.install_os) AS install_os
  ,coalesce(ti.device, du.install_device) AS install_device
  ,coalesce(ti.browser, du.install_browser) AS install_browser
  ,coalesce(ti.gender, du.install_gender) AS install_gender
  ,tu.language
  ,tu.birthday
  ,tu.first_name
  ,tu.last_name
  ,tu.gender
  ,tu.country
  ,COALESCE (tu.email, du.email) AS email
  ,tu.os
  ,tu.os_version
  ,tu.device
  ,tu.browser
  ,tu.browser_version
  ,COALESCE (tu.last_ip, du.last_ip) AS last_ip
  ,COALESCE(tu.level_end, tu.level_start,du.level) AS level
  ,CASE WHEN COALESCE(tp.total_revenue_usd, 0) > 0 THEN 1 ELSE 0 END AS is_payer
  ,tp.conversion_ts
  ,COALESCE(tp.total_revenue_usd,0) AS revenue_usd
  ,COALESCE(tp.total_purchase_cnt,0) AS payment_cnt
  ,date(tu.last_login_ts) as last_login_date
  ,nullif(split_part(ti.install_source, '::', 4),'') AS install_creative_id
  ,tu.last_ref
FROM last_info tu
LEFT JOIN install_info ti
    ON ti.user_key=tu.user_key
LEFT JOIN kpi_processed.dim_user du
    ON tu.user_key = du.user_key
LEFT JOIN payment_info tp
    ON tp.user_key=tu.user_key
WHERE tu.row=1
;

DELETE FROM kpi_processed.dim_user
where user_key in (
  select user_key from kpi_processed.tmp_user_daily_login 
  where date >= 
                 (
                   select start_date 
                   from kpi_processed.init_start_date
                 )
    )
--and app_id like '_game_%'
;

INSERT INTO kpi_processed.dim_user
(
   id                  
  ,user_key            
  ,app_id              
  ,app_version         
  ,user_id             
  ,facebook_id         
  ,install_ts          
  ,install_date        
  ,install_source      
  ,install_source_group
  ,install_subpublisher
  ,install_campaign    
  ,install_language    
  ,install_country     
  ,install_os          
  ,install_device      
  ,install_browser     
  ,install_gender      
  ,language            
  ,birthday            
  ,first_name          
  ,last_name           
  ,gender              
  ,country             
  ,email               
  ,os                  
  ,os_version          
  ,device              
  ,browser             
  ,browser_version     
  ,last_ip             
  ,level               
  ,is_payer            
  ,conversion_ts       
  ,revenue_usd         
  ,payment_cnt         
  ,last_login_date
  ,install_creative_id
  ,last_ref
)
SELECT
   id                  
  ,user_key            
  ,app_id              
  ,app_version         
  ,user_id             
  ,facebook_id         
  ,install_ts          
  ,install_date        
  ,case when install_source='appia' then 'Appia'
when install_source like 'app_page%' then 'app_page'
when install_source like 'book%ks' then 'bookmark'
when install_source like 'canvas_bookma%' then 'canvas_bookmark'
when install_source like 'easonboard2%' then 'easonboard2'
when install_source like 'easwelcome%' then 'easwelcome'
when install_source like 'search%' then 'search'
when install_source like 'shortcut%' then 'shortcut'
when install_source like 'sidebar_bo%' then 'sidebar_bookmark'
when install_source like 'timeline/' then 'timeline'
when install_source like 'ticker/' then 'ticker' 
else install_source 
end as install_source       
  ,install_source_group
  ,install_subpublisher
  ,install_campaign    
  ,install_language    
  ,install_country     
  ,install_os          
  ,install_device      
  ,install_browser     
  ,install_gender      
  ,language            
  ,birthday            
  ,first_name          
  ,last_name           
  ,gender              
  ,country             
  ,email               
  ,os                  
  ,os_version          
  ,device              
  ,browser             
  ,browser_version     
  ,last_ip             
  ,level               
  ,is_payer            
  ,conversion_ts       
  ,revenue_usd         
  ,payment_cnt
  ,last_login_date
  ,install_creative_id
  ,last_ref
FROM tmp_dim_user
;

--delete from kpi_processed.dim_user where install_date is null;

create temp table a as
SELECT trunc(ts_pretty) as date, md5(app_id||user_id) as user_key, user_id, app_id,
case
  when nvl(nullif(json_extract_path_text(properties,'scene'),''), '1') ='1' then 'Main'
  else json_extract_path_text(properties,'scene') end as scene,
min(nullif(json_extract_path_text(properties, 'level'),'')::int) as level_start,
max(nullif(json_extract_path_text(properties, 'level'),'')::int) as level_end,
max(ts_pretty) as last_login_date
FROM kpi.raw_events.events 
where ts_pretty>=(
                   select start_date 
                   from   kpi_processed.init_start_date
                ) 
and ts_pretty<CURRENT_DATE
group by 1,2,3,4,5;

create temp table b as
select date_start as date, user_key, app_id, scene, 
max(app_version) as app_version,
max(os) as os,
max(os_version) as os_version,
max(device) as device,
max(browser) as browser,
max(browser_version) as browser_version,
max(country) as country,
max(language) as language,
nvl(min(install_ts), date_start) as install_ts,
case
    when trunc(min(install_ts)) = date_start then 1
    else 0 end as is_new_user,
nvl(sum(playtime_sec), 0) as playtime_sec,
count(1) as session_cnt 
from kpi.kpi_processed.fact_session f 
where date_start>=(
                   select start_date 
                   from   kpi_processed.init_start_date
                ) 
and date_start<CURRENT_DATE
group by 1,2,3,4;

create temp table d as
select x.*, revenue_usd-revenue_ads as revenue_iap,
case
    when trunc(y.conversion_ts) = x.date then 1
    else 0 end as is_converted_today
from
(select date, app_id, user_key, scene,
1 as is_payer,
sum(revenue_usd) as revenue_usd,
SUM(case when iap_product_id in ('trial', 'flash_trial') THEN revenue_usd ELSE 0 END) AS revenue_ads,
count(1) as payment_cnt
from kpi_processed.fact_revenue
where date>=(
                   select start_date 
                   from   kpi_processed.init_start_date
                ) 
and date<CURRENT_DATE
and revenue_amount>0
group by 1,2,3,4) x
left join
(select app_id, user_key, min(ts) as conversion_ts
from kpi_processed.fact_revenue
where date<CURRENT_DATE
and revenue_amount>0
group by 1,2) y
on x.app_id = y.app_id
and x.user_key = y.user_key;

drop table if exists daily_snapshot_skeleton;
create temp table daily_snapshot_skeleton as
select a.*, b.app_version,
b.os,
b.os_version,
b.device,
b.browser,
b.browser_version,
b.country,
b.language,
b.install_ts,
b.is_new_user,
least(greatest(b.playtime_sec, 0), 24*3600) as playtime_sec,
b.session_cnt,
d.is_payer, d.revenue_usd, d.payment_cnt, d.is_converted_today, d.revenue_ads, d.revenue_iap
from 
a
left join
b
on a.date = b.date
and a.user_key = b.user_key
and a.app_id = b.app_id
and a.scene = b.scene
left join
d
on a.date = d.date
and a.user_key = d.user_key
and a.app_id = d.app_id
and a.scene = d.scene
where b.install_ts is not NULL;

delete from kpi_processed.optimize_fact_dau_snapshot
where date>=(
                   select start_date 
                   from   kpi_processed.init_start_date
                );

insert into kpi_processed.optimize_fact_dau_snapshot
(
	id,
	date, 
	user_key,  
	app_id, 
	scene, 
	level_start, 
	level_end, 
	app_version,
	os,
	os_version,
	device,
	browser,
	browser_version,
	country,
	language,
	is_new_user,
	playtime_sec,
	is_payer, 
	revenue_usd,
	revenue_ads,
	revenue_iap, 
	payment_cnt,
	session_cnt,
	is_converted_today
)
select
	md5(app_id||date||user_id) as id,
	date, 
	user_key, 
	app_id, 
	scene, 
	level_start, 
	level_end, 
	app_version,
	os,
	os_version,
	device,
	browser,
	browser_version,
	country,
	language,
	is_new_user,
	playtime_sec,
	is_payer, 
	revenue_usd,
	revenue_ads,
	revenue_iap, 
	payment_cnt,
	session_cnt,
	is_converted_today
from daily_snapshot_skeleton;

create temp table dim_user_skeleton as
select *
,nullif(split_part(install_source, '::', 3),'') AS install_subpublisher
,nullif(split_part(install_source, '::', 2),'') AS install_campaign
,nullif(split_part(install_source, '::', 4),'') AS install_creative_id
from
(select s.user_key, s.app_id, s.user_id, max(s.level_end) as level, 
trunc(max(s.last_login_date)) as last_login_date,
least(min(s.install_ts), max(u.install_ts)) as install_ts,
nvl(max(f.ip), max(u.last_ip)) as last_ip,
nvl(max(s.app_version), max(u.app_version)) as app_version,
nvl(max(s.os), max(u.os)) as os,
nvl(max(s.os_version), max(u.os_version)) as os_version,
nvl(max(s.device), max(u.device)) as device,
nvl(max(s.browser), max(u.browser)) as browser,
nvl(max(s.browser_version), max(u.browser_version)) as browser_version,
nvl(max(s.country), max(u.country)) as country,
nvl(max(s.language), max(u.language)) as language, 
nvl(max(f.facebook_id), max(u.facebook_id)) as facebook_id,
nvl(max(u.install_language), max(f.language)) as install_language,
nvl(max(u.install_country), max(f.country)) as install_country,
nvl(max(u.install_os), max(f.os)) as install_os,
nvl(max(u.install_device), max(f.device)) as install_device,
nvl(max(u.install_browser), max(f.browser)) as install_browser,
nvl(max(u.install_gender), max(f.gender)) as install_gender,
nvl(max(f.birthday), max(u.birthday)) as birthday,
nvl(max(f.first_name), max(u.first_name)) as first_name,
nvl(max(f.last_name), max(u.last_name)) as last_name,
nvl(max(f.gender), max(u.gender)) as gender,
nvl(max(f.email), max(u.email)) as email,
nvl(max(f.last_ref), max(u.last_ref)) as last_ref,
max(nvl(p.is_payer, 0)) as is_payer,
sum(p.revenue_usd) as revenue_usd,
count(p.revenue_amount) as payment_cnt,
min(p.ts) as conversion_ts,
case
    when lower(nvl(nullif(max(u.install_source), ''), 'organic'))!='organic' then max(f.install_source)
    else max(f.install_source) end as install_source
from
daily_snapshot_skeleton s
left join
(select * from kpi.kpi_processed.fact_session
where date_start >= 
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                ) 
and date_start<CURRENT_DATE) f
on s.user_key = f.user_key
and s.app_id = f.app_id
left join
(select *, 1 as is_payer
from kpi_processed.fact_revenue
where date<CURRENT_DATE and revenue_amount>0) p
on s.user_key = p.user_key
and s.app_id = p.app_id
left join 
kpi_processed.optimize_dim_user u
on s.user_key = u.user_key
and s.app_id = u.app_id
group by 1,2,3);

delete from kpi_processed.optimize_dim_user
where user_key in (select user_key from dim_user_skeleton);

insert into kpi_processed.optimize_dim_user
(
	id,
	user_key, 
	app_id, 
	app_version, 
	user_id, 
	facebook_id, 
	install_ts, 
	install_date, 
	install_source, 
	install_subpublisher, 
	install_campaign, 
	install_language, 
	install_country, 
	install_os, 
	install_device, 
	install_browser, 
	install_gender, 
	language, 
	birthday, 
	first_name, 
	last_name, 
	gender, 
	country, 
	email, 
	os, 
	os_version, 
	device, 
	browser, 
	browser_version, 
	last_ip, 
	level, 
	is_payer, 
	conversion_ts, 
	revenue_usd, 
	payment_cnt, 
	last_login_date,
	install_creative_id, 
	last_ref
)
select
	user_key as id,
	user_key, 
	app_id, 
	app_version, 
	user_id, 
	facebook_id, 
	install_ts, 
	trunc(install_ts) as install_date, 
	install_source, 
	install_subpublisher, 
	install_campaign, 
	install_language, 
	install_country, 
	install_os, 
	install_device, 
	install_browser, 
	install_gender, 
	language, 
	birthday, 
	first_name, 
	last_name, 
	gender, 
	country, 
	email, 
	os, 
	os_version, 
	device, 
	browser, 
	browser_version, 
	last_ip, 
	level, 
	is_payer, 
	conversion_ts, 
	revenue_usd, 
	payment_cnt, 
	last_login_date,
	install_creative_id, 
	last_ref 
from dim_user_skeleton;
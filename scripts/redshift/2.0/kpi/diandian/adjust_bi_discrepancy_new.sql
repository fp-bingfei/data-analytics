DELETE FROM marketing.adjust_bi_discrepancy where install_date >=dateadd(day,-1,current_date);
INSERT INTO marketing.adjust_bi_discrepancy
--/* adjust all data */--
with adjust_all as (
select 
    adid
    ,tracker_name 
    ,coalesce(split_part(tracker_name, '::', 1),'Unknown') as install_source
    ,coalesce(split_part(tracker_name, '::', 2),'Unknown') as campaign
    ,case
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') 
             then coalesce(split_part(tracker_name, '::', 3),'Unknown')
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') 
             then coalesce(split_part(tracker_name, '::', 4),'Unknown')
     end as sub_publisher
    ,coalesce(split_part(tracker_name, '::', 4),'Unknown') as creative_id
    ,fp_app_id as app_id
    ,coalesce(a.country,'Unknown') as country_code
    ,d.country
    ,date(installed_at) as install_date
    ,case
        when app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar') then 'Android'
        when app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350') then 'iOS'
        when app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
        else os_name
     end as os
     ,max(user_id) as user_id	
from adjust.raw_events	a 
left join kpi_processed.dim_country d
on a.country = lower(d.country_code)
where 
	(event  = 'install' or event = 'open')
	and installed_at>=dateadd(day,-1,current_date)
	and installed_at<current_date
group by 1,2,3,4,5,6,7,8,9,10,11
 ) 

--/* adjust vs BI */--

,adjust_bi as (
select 
	a.adid
	,a.user_id
	,a.tracker_name
	,case when a.install_source = 'Organic' then 'Adjust Organic' else a.install_source end as install_source
	,a.campaign
	,a.sub_publisher
	,a.creative_id
	,a.app_id
	,a.country_code
	,a.country
	,a.install_date
	,a.os
	,u.user_id as bi_user_id
	,coalesce(u.install_source,'Unknown') as bi_install_source
	,u.install_date as bi_install_date
from 
	(
	select * from adjust_all 
	where 
		user_id<>'' 
		and user_id is not null
	) a 
left join kpi_processed.dim_user u
	on a.user_id = u.user_id
	and a.app_id = u.app_id
)
-- /* calculations */ --


select 
	t0.* -- adjust all installs
--	,t1.all_bi_installs 
	,t2.adjust_installs_null_uid
	,t3.adjust_installs_in_bi
	,t4.adjust_installs_in_bi_mismatch_install_source
	,t5.adjust_installs_in_bi_mismatch_install_date
	,t6.adjust_installs_not_in_bi
from 
-- adjust all installs
(
select 
	install_date
	,app_id
	,country
	,country_code
	,case when install_source = 'Organic' then 'Adjust Organic' else install_source end as install_source
	,campaign
	,sub_publisher
	,creative_id
	,os
	,count(distinct adid) as all_adjust_installs
from adjust_all 
group by 1,2,3,4,5,6,7,8,9
) t0

--LEFT JOIN
-- bi all installs
-- /* cannot join with bi as for some channel adjust countries are quite different from bi countries given same user_id */
--(
--select 
--	install_date
--	,app_id
--	,coalesce(country,'Unknown') as country
--	,coalesce(install_source,'Unknown') as install_source
--	,coalesce(install_campaign,'Unknown') as campaign
--	,coalesce(install_subpublisher,'Unknown') as sub_publisher
--	,coalesce(install_creative_id,'Unknown') as creative_id
--	,case when os = 'iPhone OS' then 'iOS' else coalesce(os,'Unknown') end as os
--	,count(distinct user_id) as all_bi_installs
--from kpi_processed.dim_user
--where 
--	install_date>=dateadd(day,-1,current_date)
--	and install_date<current_date
--group by 1,2,3,4,5,6,7,8
--) t1
--ON 
--	t0.install_date = t1.install_date
--	and lower(replace(t0.install_source, '+' , ' ')) = lower(t1.install_source)
--	and lower(replace(t0.campaign, '+' , ' ')) = lower(t1.campaign)
--	and t0.sub_publisher = t1.sub_publisher
--	and t0.creative_id = t1.creative_id
--	and t0.app_id = t1.app_id
--	and t0.country = t1.country
--	and t0.os = t1.os

LEFT JOIN 
-- adjust install of null user_id
(
select 
	install_date
	,app_id
	,country
	,case when install_source = 'Organic' then 'Adjust Organic' else install_source end as install_source
	,campaign
	,sub_publisher
	,creative_id
	,os
	,count(distinct adid) as adjust_installs_null_uid
from adjust_all 
where (user_id ='' or user_id is null)
group by 1,2,3,4,5,6,7,8
) t2
ON 
	t0.install_date = t2.install_date
	and t0.install_source = t2.install_source
	and t0.campaign = t2.campaign
	and t0.sub_publisher = t2.sub_publisher
	and t0.creative_id = t2.creative_id
	and t0.app_id = t2.app_id
	and t0.country = t2.country
	and t0.os = t2.os

LEFT JOIN 
-- ajdust in bi installs
(
select 
	install_date
	,app_id
	,country
	,install_source
	,campaign
	,sub_publisher
	,creative_id
	,os
	,count(distinct adid) as adjust_installs_in_bi
from adjust_bi 
where bi_user_id is not null
group by 1,2,3,4,5,6,7,8
) t3 
ON 
	t0.install_date = t3.install_date
	and t0.install_source = t3.install_source
	and t0.campaign = t3.campaign
	and t0.sub_publisher = t3.sub_publisher
	and t0.creative_id = t3.creative_id
	and t0.app_id = t3.app_id
	and t0.country = t3.country
	and t0.os = t3.os

LEFT JOIN 
-- adjust in bi installs but mismatch install_source
(
select 
	install_date
	,app_id
	,country
	,install_source
	,campaign
	,sub_publisher
	,creative_id
	,os
	,count(distinct adid) as adjust_installs_in_bi_mismatch_install_source
from adjust_bi 
where 
	bi_user_id is not null 
	and lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) <> lower(bi_install_source)
group by 1,2,3,4,5,6,7,8
) t4
ON 
	t0.install_date = t4.install_date
	and t0.install_source = t4.install_source
	and t0.campaign = t4.campaign
	and t0.sub_publisher = t4.sub_publisher
	and t0.creative_id = t4.creative_id
	and t0.app_id = t4.app_id
	and t0.country = t4.country
	and t0.os = t4.os

LEFT JOIN 
-- adjust in bi installs but mismatch install_date
(
select 
	install_date
	,app_id
	,country
	,install_source
	,campaign
	,sub_publisher
	,creative_id
	,os
	,count(distinct adid) as adjust_installs_in_bi_mismatch_install_date
from adjust_bi 
where 
	bi_user_id is not null 
	and install_date <> bi_install_date
group by 1,2,3,4,5,6,7,8
) t5 
ON 
	t0.install_date = t5.install_date
	and t0.install_source = t5.install_source
	and t0.campaign = t5.campaign
	and t0.sub_publisher = t5.sub_publisher
	and t0.creative_id = t5.creative_id
	and t0.app_id = t5.app_id
	and t0.country = t5.country
	and t0.os = t5.os

LEFT JOIN 
-- adjust not in bi installs 
(
select 
	install_date
	,app_id
	,country
	,install_source
	,campaign
	,sub_publisher
	,creative_id
	,os
	,count(distinct adid) as adjust_installs_not_in_bi
from adjust_bi 
where 
	bi_user_id is null 
group by 1,2,3,4,5,6,7,8
) t6 
ON 
	t0.install_date = t6.install_date
	and t0.install_source = t6.install_source
	and t0.campaign = t6.campaign
	and t0.sub_publisher = t6.sub_publisher
	and t0.creative_id = t6.creative_id
	and t0.app_id = t6.app_id
	and t0.country = t6.country
	and t0.os = t6.os
;
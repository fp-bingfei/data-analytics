------------------------------------------------
--Data 2.0 fact_session.sql
------------------------------------------------
--fact_session

delete from kpi_processed.fact_session
where date_start >= 
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
                --and app_id like '_game_%'
                ;

CREATE TEMP TABLE tmp_fact_session AS
select   md5(s.app_id||s.user_id||s.session_id) as id
        ,s.app_id 
        ,s.app_version::varchar(32)
        ,md5(s.app_id||s.user_id) as user_key
        ,s.user_id
        ,trunc(s.ts_pretty) as date_start
        ,trunc(e.ts_pretty) as date_end
        ,s.ts_pretty as ts_start
        ,e.ts_pretty as ts_end
        ,s.install_ts     
        ,trunc(s.install_ts) as install_date     
        ,s.session_id     
        ,s.facebook_id    
        ,s.install_source 
        ,case when s.os is null then 'Unknown'
			when s.os='null' then 'Unknown'
			when s.os='' then 'Unknown'
			when s.os='ios' then 'iOS'
			when s.os='android' then 'Android'
			when s.os='windows' then 'Windows'
		else s.os end  as os     
        ,s.os_version     
        ,case when s.browser is null then 'Unknown'
			when s.browser='' then 'Unknown'
		else s.browser end as browser        
        ,s.browser_version
        ,s.device         
        ,coalesce(c.country,'Unknown')  as country      
        ,s.email          
        ,s.first_name     
        ,s.last_name      
        ,s.level as level_start
        ,e.level as level_end
        ,s.vip_level
        ,s.gender         
        ,s.birthday       
        ,s.ip             
        ,s.lang as language
        ,coalesce(e.session_length,0) as playtime_sec
        ,s.scene
        ,s.fb_source
        ,s.last_ref
        --,s.gameserver_id
from    kpi_processed.session_start s
left join kpi_processed.session_end e
on      s.session_id = e.session_id
and     s.app_id = e.app_id
and     s.user_id = e.user_id 
and     s.scene=e.scene  
and     e.ts_pretty >=
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
left join kpi_processed.dim_country c
on      s.country_code = c.country_code
where   s.ts_pretty >=
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
        --and s.app_id like '_game_%'
        ;

-- update playtime_sec for Royal Story
update tmp_fact_session
  set playtime_sec=DATEDIFF(second, ts_start, ts_end)
  where
      app_id like 'royal%'
      and ts_start >=
                 (
                    select start_date
                    from   kpi_processed.init_start_date
                 );

insert into kpi_processed.fact_session
select
	id
	,app_id
	,app_version
	,user_key
	,user_id
	,date_start
	,date_end
	,ts_start
	,ts_end
	,install_ts
	,install_date
	,session_id
	,facebook_id
	,install_source
	,os
	,os_version
	,browser
	,browser_version
	,device
	,country
	,email
	,first_name
	,last_name
	,level_start
	,level_end
	,vip_level
	,gender
	,birthday
	,ip
	,language
	,playtime_sec
	,scene
	,fb_source
	,last_ref
    --,gameserver_id
from tmp_fact_session;
                 
--fact_revenue
delete from kpi_processed.fact_revenue
where date >= 
                (
                   select start_date 
                   from   kpi_processed.init_start_date
                )
                --and app_id like '_game_%'
                ;

insert into kpi_processed.fact_revenue
select    distinct  md5(app_id||md5(p.app_id||p.user_id)||transaction_id) as id               
           ,p.app_id           
           ,p.app_version      
           ,md5(p.app_id||p.user_id) as user_key
           ,p.user_id
           ,trunc(p.ts_pretty) as date
           ,p.ts_pretty               
           ,p.install_ts               
           ,date(p.install_ts)               
           ,p.session_id       
           ,p.level            
           ,p.vip_level        
           ,case when p.os is null then 'Unknown'
when p.os='null' then 'Unknown'
when p.os='' then 'Unknown'
when p.os='ios' then 'iOS'
when p.os='android' then 'Android'
when p.os='windows' then 'Windows'
else p.os end  as os            
           ,p.os_version       
           ,p.device           
           ,case when p.browser is null then 'Unknown'
when p.browser='' then 'Unknown'
else p.browser end as browser           
           ,p.browser_version  
           ,coalesce(cy.country,'Unknown') as country          
           ,p.ip               
           ,p.install_source
           ,p.lang as language         
           ,p.payment_processor
           ,p.iap_product_id   
           ,p.iap_product_name 
           ,p.iap_product_type 
           ,p.currency         
           ,p.amount*1.0000/100 as revenue_amount   
           ,nvl(p.amount*1.0000*c.factor/100, 0) as revenue_usd      
           ,p.transaction_id  
           ,p.scene  
           ,p.fb_source
from       kpi_processed.payment p
left join  kpi_processed.currency c
on         p.currency = c.currency
and        trunc(p.ts_pretty) = c.dt
left join kpi_processed.dim_country cy
on        p.country_code = cy.country_code
where     p.ts_pretty >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
          --and p.app_id like '_game_%'
          ;
-- delete revenue of test users in prod env for KOA
delete from kpi_processed.fact_revenue
  where app_id = 'ffs.global.prod'
  and user_id in (select distinct user_id 
                    from kpi_processed.payment_test_users 
                    where app_id = 'ffs.global.prod'
                  )
;

--fact_new_user
delete from kpi_processed.fact_new_user
where date_start >= 
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
        --and app_id like '_game_%'
        ;


insert into kpi_processed.fact_new_user
select     md5(u.app_id||u.user_id||u.session_id) as id
          ,u.app_id 
          ,u.app_version::varchar(32)
          ,md5(u.app_id||u.user_id) as user_key
          ,u.user_id
          ,trunc(u.ts_pretty) as date_start
          ,null as date_end
          ,u.ts_pretty as ts_start
          ,null as ts_end
          ,u.install_ts     
          ,trunc(u.install_ts) as install_date     
          ,u.session_id     
          ,u.facebook_id    
          ,u.install_source 
          ,case when u.os is null then 'Unknown'
when u.os='null' then 'Unknown'
when u.os='' then 'Unknown'
when u.os='ios' then 'iOS'
when u.os='android' then 'Android'
when u.os='windows' then 'Windows'
else u.os end  as os           
          ,u.os_version     
          ,case when u.browser is null then 'Unknown'
when u.browser='' then 'Unknown'
else u.browser end as browser         
          ,u.browser_version
          ,u.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,u.email          
          ,u.first_name     
          ,u.last_name      
          ,u.level as level_start
          ,u.level as level_end
          ,u.vip_level
          ,u.gender         
          ,u.birthday       
          ,u.ip             
          ,u.lang as language
          ,u.scene
          ,u.fb_source
from      kpi_processed.new_user u
left join kpi_processed.dim_country c
on        u.country_code = c.country_code
where     u.ts_pretty >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
                 --and u.app_id like '_game_%'
                 ;

------------------------------------------------
--Data 2.0 tmp_user_daily_login.sql
------------------------------------------------

delete from  kpi_processed.tmp_user_daily_login
where  date  >=  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
--and app_id like '_game_%'
;

insert into kpi_processed.tmp_user_daily_login
SELECT DISTINCT
            s.date_start AS date
           ,s.app_id
           ,s.user_key
           ,s.user_id
           ,last_value(s.ts_start ignore nulls)
               OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_login_ts
           ,last_value(s.facebook_id ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
           ,last_value(s.birthday ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
            ,last_value(s.first_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
            ,last_value(s.last_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
           ,substring(last_value(s.email ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_ts ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following)  
               WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts
                     then u.install_ts  
               ELSE
                   min(s.install_ts ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_ts
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_date ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
               min(s.install_date ignore nulls)
                       OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                       ROWS BETWEEN unbounded preceding AND unbounded following) 
                WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date
                     then u.install_date    
               ELSE
                   min(s.install_date ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_date
           ,last_value(s.app_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS app_version
           ,first_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS level_start
           ,nvl(last_value(s.level_end ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following),
                last_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following))
           AS level_end
           ,last_value(s.os ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os
           ,last_value(s.os_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os_version
           ,last_value(s.country ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS country
           ,last_value(s.ip ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ip
           ,last_value(s.install_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS install_source
           ,last_value(s.language ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS language
           ,last_value(s.gender ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
           ,last_value(s.device ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)::varchar(64)
           AS device
           ,last_value(s.browser ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser
           ,last_value(s.browser_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser_version
           ,sum(1)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene)
            AS session_cnt
           ,sum(playtime_sec)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene )
            AS playtime_sec
            ,s.scene
            ,last_value(s.fb_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS fb_source
            ,null as install_source_group
            ,last_value(s.last_ref ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ref
           -- ,last_value(s.gameserver_id ignore nulls)
           --     OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
           --      ROWS BETWEEN unbounded preceding AND unbounded following)
           -- AS gameserver_id
FROM      kpi_processed.fact_session s
LEFT JOIN kpi_processed.dim_user u
ON        u.user_key=s.user_key
WHERE     s.date_start >= 
                  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
--and s.app_id like '_game_%'        
        ;

-- Attempt to catch missing users from fact_new_user

INSERT INTO kpi_processed.tmp_user_daily_login
SELECT DISTINCT
     n.date_start
    ,n.app_id
    ,n.user_key
    ,n.user_id
    ,last_value(n.ts_start ignore nulls)
        OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,last_value(n.facebook_id ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
    ,last_value(n.birthday ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
     ,last_value(n.first_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
     ,last_value(n.last_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
    ,substring(last_value(n.email ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_ts ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN n.app_id like 'royal%' and n.scene='2' then 
            min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    n.scene='Main' and min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(n.install_ts ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_date ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        when n.app_id like 'royal%' and n.scene='2' then
            min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN   n.scene='Main' and  min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(n.install_date ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(n.app_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(n.level_start ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_start
    ,last_value(n.level_end ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(n.os ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(n.os_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(n.country ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(n.ip ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(n.install_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(n.language ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(n.gender ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
    ,last_value(n.device ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(n.browser ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(n.browser_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,n.scene
    ,last_value(n.fb_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM kpi_processed.fact_new_user n
LEFT JOIN kpi_processed.tmp_user_daily_login s
    ON  s.user_key=n.user_key
    AND s.date=n.date_start
LEFT JOIN kpi_processed.dim_user u
    ON  u.user_key=s.user_key
WHERE n.date_start >= 
           (
             select start_date 
             from   kpi_processed.init_start_date
            )
           --and n.app_id like '_game_%'
AND s.user_key is null
;


                  
-- Attempt to catch missing users from fact_revenue

INSERT INTO kpi_processed.tmp_user_daily_login
WITH revenue_session_cnt AS 
(
    SELECT  user_key
           ,date
           ,scene
           ,count(distinct session_id) as session_cnt
    FROM   kpi_processed.fact_revenue
    WHERE  session_id is not null
    --and app_id like '_game_%'
    GROUP  BY 1,2,3
)
SELECT DISTINCT
     p.date
    ,p.app_id
    ,p.user_key
    ,p.user_id
    ,last_value(p.ts ignore nulls)
        OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,cast(null as varchar) AS facebook_id
    ,cast(null as date) AS birthday
    ,null AS  first_name
    ,null AS last_name
    ,cast(null as varchar) AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_ts ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_ts ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(p.install_ts ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_date ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_date ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(p.install_date ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(p.app_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level
    ,last_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(p.os ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(p.os_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(p.country ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(p.ip ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(p.install_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(p.language ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,null AS gender
    ,last_value(p.device ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(p.browser ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(p.browser_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,COALESCE(c.session_cnt,1) AS session_cnt
    ,0 AS playtime_sec
    ,p.scene
    ,last_value(p.fb_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM kpi_processed.fact_revenue p
LEFT JOIN revenue_session_cnt c
    ON  p.user_key=c.user_key
    AND p.date=c.date 
    and p.scene=c.scene
LEFT JOIN kpi_processed.tmp_user_daily_login s
    ON  s.user_key=p.user_key
    AND s.date=p.date
LEFT JOIN kpi_processed.dim_user u
    ON  u.user_key=s.user_key
WHERE p.date >= 
           (
             select start_date 
             from   kpi_processed.init_start_date
            )
    --and p.app_id like '_game_%'
AND s.user_key is null
;



--update the user info where level_end < level_start
update kpi_processed.tmp_user_daily_login
set level_start = level_end
where level_end < level_start;


--update the install_ts info
create temp table tmp_user_last_day as
select t.*
from
    (
      select *
             ,row_number() OVER (partition by user_key order by date_start desc) as row
      from kpi_processed.fact_session ds
      where date_start>=current_date-60
          and ds.install_date is not null 
      --and ds.app_id like '_game_%'
     ) t
where row = 1;

update kpi_processed.tmp_user_daily_login
set    install_date = t.install_date,
       install_ts = t.install_ts,
       device = t.device,
       country = t.country,
       language = t.language,
       browser = t.browser,
       browser_version = t.browser_version,
       os = t.os,
       os_version = t.os_version
from   tmp_user_last_day t
where  kpi_processed.tmp_user_daily_login.install_date is null 
and    kpi_processed.tmp_user_daily_login.user_key = t.user_key
--and kpi_processed.tmp_user_daily_login.app_id like '_game_%'
;

--if we still have missing install_ts, remove
delete from kpi_processed.tmp_user_daily_login where install_ts is NULL
--and app_id like '_game_%'
  ;


--create farm install source group based on their rules
create temp table tmp_install_source_group 
as
select user_key,
     case
     when (install_source like '%bookmark%' or fb_source like '%bookmark%') then 'bookmark'
     when (install_source like '%FF_%' or fb_source='ad' ) then 'ads'
     when install_source='search' or fb_source='search' or (install_source='ts' and fb_source='') or (install_source='br_tf' and fb_source='') then 'search'
     when fb_source='' and install_source='' then 'empty'
     when install_source='og_make' then 'og_make'
     when install_source='og_craft' then 'og_craft'
     when install_source='og_cook' then 'og_cook'
     when install_source='og_collect' then 'og_collect'
     when install_source='og_harvest' then 'og_harvest'
     when install_source='og_fill' then 'og_fill'
     when install_source='og_feed' then 'og_feed'
     when fb_source='appcenter_request' then 'appcenter_request'
     when (fb_source='feed_playing' or fb_source='ego') then 'mayLike'
     when fb_source='rightcolumn' or fb_source='hovercard' or fb_source='canvas_recommended' or fb_source like '%recommended%' or install_source like '%recommended%' then 'recommended'
     when install_source='appcenter' or fb_source='appcenter' then 'appCenter'
     when fb_source ='fbpage' or install_source='empty' or install_source like '%FP_%' then 'fanPage'
     when fb_source='timeline' then 'timeLine'
     when install_source='reminders' then 'reminders'
     when install_source='feed' or fb_source like '%feed%' then 'feed'
     when fb_source='request' or install_source='notif' then 'request'
     when install_source='' and fb_source='dialog' then 'dialog'
     when install_source='og_complete' then 'og_complete'
     when install_source='' and fb_source='canvas_featured' then 'canvas_featured'
     when install_source='' and fb_source='sidebar_featured' then 'sidebar_featured'
     when install_source='' and fb_source='appcenter_featured' then 'appcenter_featured'
     when (install_source ='ff15ar' or install_source='ff15tr' or install_source like '%YB-Z%') and position('YB-Z9B' in install_source)=0 and position('YB-Z14B' in install_source)=0 and position('YB-Z31B' in install_source) =0 and position('YB-Z12B' in install_source) =0 and position('YB-Z127B' in install_source) =0 and position('YB-Z128B' in install_source) =0 and position('YB-Z18B' in install_source) =0 and position('YB-Z125B' in install_source) =0 and position('YB-Z17B' in install_source) =0 and position('YB-Z16B' in install_source) =0 and position('YB-Z126B' in install_source) =0 then 'internal_promotion_banner'
     when (install_source='ff15ar' or position('YB-Z9B' in install_source)=1 or position('yb-z9b' in install_source)=1) then 'farm_ae'
     when (install_source='ff15tr' or position('YB-Z14B' in install_source)=1 or position('yb-z14b' in install_source)=1) then 'farm_tr'
     when (position('YB-Z31B' in install_source)=1  or position('yb-z31b' in install_source)=1) then 'farm_br'
     when install_source = 'notif' or fb_source = 'notification' then 'notification'
     when install_source = 'farm_ip' then 'farm_ip'
     when install_source = 'farm_es' then 'farm_es'
     when position('EAS' in install_source) = 1 then 'eas'
     when (position('YB-Z127B' in install_source)=1 or position('YB-Z12B' in install_source)=1 or position('yb-z127b' in install_source)=1 or position('yb-z12b' in install_source)=1) then 'farm_fr'
     when (position('YB-Z128B' in install_source)=1 or position('YB-Z18B' in install_source)=1 or position('yb-z128b' in install_source)=1 or position('yb-z18b' in install_source)=1) then 'farm_nl'
     when (position('YB-Z125B' in install_source)=1 or position('YB-Z17B' in install_source)=1 or position('yb-z125b' in install_source)=1 or position('yb-z17b' in install_source)=1) then 'farm_th'
     when (position('YB-Z126B' in install_source)=1 or position('YB-Z16B' in install_source)=1 or position('yb-z126b' in install_source)=1 or position('yb-z16b' in install_source)=1) then 'farm_de'
     else 'Others'
     end as install_src_group
from kpi_processed.tmp_user_daily_login
where date  >=  (
                    select start_date 
                    from   kpi_processed.init_start_date
                  )
and app_id like 'farm%';

update kpi_processed.tmp_user_daily_login
set install_source_group = t.install_src_group
from tmp_install_source_group t
where t.user_key = kpi_processed.tmp_user_daily_login.user_key;


------------------------------------------------
--Data 2.0 tmp_user_payment.sql
------------------------------------------------

delete from kpi_processed.tmp_user_payment
where date >=
          (
                    select start_date 
                    from   kpi_processed.init_start_date
          )
          --and app_id like '_game_%'
         ;

insert into kpi_processed.tmp_user_payment
select  DISTINCT
        app_id
        ,user_key
        ,date
        ,min(r.ts) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)                      AS conversion_ts
        ,sum(revenue_usd) over (partition by r.user_key, date,scene ORDER BY ts ASC rows between unbounded preceding and unbounded following)         AS revenue_usd
        ,sum(revenue_usd) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)               AS total_revenue_usd
        ,count(transaction_id) over (partition by r.user_key, date,scene ORDER BY ts ASC rows between unbounded preceding and unbounded following )   AS purchase_cnt
        ,count(transaction_id) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)          AS total_purchase_cnt
        ,scene
FROM kpi_processed.fact_revenue r
where date >=
          (
                    select start_date 
                    from   kpi_processed.init_start_date
          )
          --and app_id like '_game_%'
;

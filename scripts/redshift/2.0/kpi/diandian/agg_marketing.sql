--------------------------------------------------------------------------------------------------------------------------------------------
--Marketing tableau report tables
--Version 2.0
--Author Robin
/**
Description:
This script is generate marketing tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

delete from kpi_processed.agg_marketing_kpi
--where app like '_game_%'
;

-- update incorrect install date late than current for Smash Island(Crazy Planets)
-- add by Jia Ting 2016-04-14
update kpi_processed.dim_user
set install_date=date(a.ts)
from adjust a
where kpi_processed.dim_user.user_id = a.userid
    and kpi_processed.dim_user.app_id like 'crazyplanet%'
    and a.game like 'crazyplanet%'
    and kpi_processed.dim_user.install_date>=current_date;

-- update null install_os for Smash Island(Crazy Planets)
-- add by Jia Ting 2016-06-24
-- add Papaya install source group for RS, FF, HA
-- add by Qu Ying 2016-08-29
update kpi_processed.dim_user 
    set install_os=os 
    where  app_id like 'crazyplanet%'  
        and install_os is null ;

create temp table tmp_new_install_users as
select user_key
    ,app_id as app
    ,user_id as uid
    ,install_date
    ,install_source
    ,case when (install_source like 'FF_%' and app_id like 'farm%') THEN 'Facebook Install Source'
          when (install_source like 'HA_%' and app_id like 'ha%') THEN 'Facebook Install Source'
          when (install_source like 'RS_%' and app_id like 'royal%') THEN 'Facebook Install Source'
          when ((install_source like 'Papaya_%' or install_source like 'papaya_%') and app_id like 'farm%') THEN 'Papaya Install Source'
          when ((install_source like 'Papaya_%' or install_source like 'papaya_%') and app_id like 'ha%') THEN 'Papaya Install Source'
          when ((install_source like 'Papaya_%' or install_source like 'papaya_%') and app_id like 'royal%') THEN 'Papaya Install Source'
          when (install_source like 'Poker_%' and app_id like 'poker%') THEN 'Facebook Install Source'
      else 'Other Install Source' end
     as install_source_group
    ,install_campaign as campaign
    ,install_subpublisher as sub_publisher
    ,install_creative_id as creative_id
    ,install_country as country
    ,install_os as os
    ,level
    ,is_payer
from kpi_processed.dim_user
where install_date <= current_date 
--and app_id like '_game_%'
;

create temp table tmp_all_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
--where app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d1_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
    join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 1
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d3_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 3
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d7_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 7
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d15_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 15
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d30_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 30
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d60_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 60
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d90_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 90
--and app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d120_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 120
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d150_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 150
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d1_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 1
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d3_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 3
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d7_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 7
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d15_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 15
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d30_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 30
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d60_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 60
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d90_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 90
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d120_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 120
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_d150_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 150
--and d.app_id like '_game_%'
group by d.user_key, d.app_id;

create temp table tmp_session_cnt as
select d.user_key, d.app_id as app, sum(coalesce(d.session_cnt,0)) as session_cnt
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
group by d.user_key, d.app_id;

insert into kpi_processed.agg_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_month
    ,install_source
    ,install_source_group
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os
    ,level

    ,new_installs
    ,d1_new_installs
    ,d3_new_installs
    ,d7_new_installs
    ,d15_new_installs
    ,d30_new_installs
    ,d60_new_installs
    ,d90_new_installs
    ,d120_new_installs
    ,d150_new_installs

    ,revenue
    ,d1_revenue
    ,d3_revenue
    ,d7_revenue
    ,d15_revenue
    ,d30_revenue
    ,d60_revenue
    ,d90_revenue
    ,d120_revenue
    ,d150_revenue

    ,payers
    ,d1_payers
    ,d3_payers
    ,d7_payers
    ,d15_payers
    ,d30_payers
    ,d60_payers
    ,d90_payers
    ,d120_payers
    ,d150_payers

    ,d1_retained
    ,d3_retained
    ,d7_retained
    ,d15_retained
    ,d30_retained
    ,d60_retained
    ,d90_retained
    ,d120_retained
    ,d150_retained

    ,level_u2
    ,level_a2u5
    ,level_a5u10
    ,level_a10
    ,session_cnt
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,substring(u.install_date, 1, 7) as install_month
    ,u.install_source::VARCHAR(128)
    ,u.install_source_group::VARCHAR(128)
    ,u.campaign::VARCHAR(128)
    ,u.sub_publisher
    ,u.creative_id
    ,u.country
    ,u.os
    ,u.level

    ,count(1) as new_installs
    ,count(1) as d1_new_installs
    ,count(1) as d3_new_installs
    ,count(1) as d7_new_installs
    ,count(1) as d15_new_installs
    ,count(1) as d30_new_installs
    ,count(1) as d60_new_installs
    ,count(1) as d90_new_installs
    ,count(1) as d120_new_installs
    ,count(1) as d150_new_installs

    ,sum(case when r.revenue is null then 0 else r.revenue end) as revenue
    ,sum(case when d1_r.revenue is null then 0 else d1_r.revenue end) as d1_revenue
    ,sum(case when d3_r.revenue is null then 0 else d3_r.revenue end) as d3_revenue
    ,sum(case when d7_r.revenue is null then 0 else d7_r.revenue end) as d7_revenue
    ,sum(case when d15_r.revenue is null then 0 else d15_r.revenue end) as d15_revenue
    ,sum(case when d30_r.revenue is null then 0 else d30_r.revenue end) as d30_revenue
    ,sum(case when d60_r.revenue is null then 0 else d60_r.revenue end) as d60_revenue
    ,sum(case when d90_r.revenue is null then 0 else d90_r.revenue end) as d90_revenue
    ,sum(case when d120_r.revenue is null then 0 else d120_r.revenue end) as d120_revenue
    ,sum(case when d150_r.revenue is null then 0 else d150_r.revenue end) as d150_revenue

    ,sum(case when r.is_payer is null then 0 else r.is_payer end) as payers
    ,sum(case when d1_r.is_payer is null then 0 else d1_r.is_payer end) as d1_payers
    ,sum(case when d3_r.is_payer is null then 0 else d3_r.is_payer end) as d3_payers
    ,sum(case when d7_r.is_payer is null then 0 else d7_r.is_payer end) as d7_payers
    ,sum(case when d15_r.is_payer is null then 0 else d15_r.is_payer end) as d15_payers
    ,sum(case when d30_r.is_payer is null then 0 else d30_r.is_payer end) as d30_payers
    ,sum(case when d60_r.is_payer is null then 0 else d60_r.is_payer end) as d60_payers
    ,sum(case when d90_r.is_payer is null then 0 else d90_r.is_payer end) as d90_payers
    ,sum(case when d120_r.is_payer is null then 0 else d120_r.is_payer end) as d120_payers
    ,sum(case when d150_r.is_payer is null then 0 else d150_r.is_payer end) as d150_payers

    ,sum(case when d1_retained.retained is null then 0 else d1_retained.retained end) as d1_retained
    ,sum(case when d3_retained.retained is null then 0 else d3_retained.retained end) as d3_retained
    ,sum(case when d7_retained.retained is null then 0 else d7_retained.retained end) as d7_retained
    ,sum(case when d15_retained.retained is null then 0 else d15_retained.retained end) as d15_retained
    ,sum(case when d30_retained.retained is null then 0 else d30_retained.retained end) as d30_retained
    ,sum(case when d60_retained.retained is null then 0 else d60_retained.retained end) as d60_retained
    ,sum(case when d90_retained.retained is null then 0 else d90_retained.retained end) as d90_retained
    ,sum(case when d120_retained.retained is null then 0 else d120_retained.retained end) as d120_retained
    ,sum(case when d150_retained.retained is null then 0 else d150_retained.retained end) as d150_retained

    ,sum(case when u.level < 2 then 1 else 0 end) as level_u2
    ,sum(case when u.level >= 2 and u.level < 5 then 1 else 0 end) as level_a2u5
    ,sum(case when u.level >= 5 and u.level < 10 then 1 else 0 end) as level_a5u10
    ,sum(case when u.level >= 10 then 1 else 0 end) as level_a10
    ,sum(case when sc.session_cnt is null then 0 else sc.session_cnt end) as session_cnt
from tmp_new_install_users u
    left join tmp_all_revenue r on u.user_key = r.user_key
    left join tmp_d1_revenue d1_r on u.user_key = d1_r.user_key
    left join tmp_d3_revenue d3_r on u.user_key = d3_r.user_key
    left join tmp_d7_revenue d7_r on u.user_key = d7_r.user_key
    left join tmp_d15_revenue d15_r on u.user_key = d15_r.user_key
    left join tmp_d30_revenue d30_r on u.user_key = d30_r.user_key
    left join tmp_d60_revenue d60_r on u.user_key = d60_r.user_key
    left join tmp_d90_revenue d90_r on u.user_key = d90_r.user_key
    left join tmp_d120_revenue d120_r on u.user_key = d120_r.user_key
    left join tmp_d150_revenue d150_r on u.user_key = d150_r.user_key

    left join tmp_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_d3_retained d3_retained on u.user_key = d3_retained.user_key
    left join tmp_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_d15_retained d15_retained on u.user_key = d15_retained.user_key
    left join tmp_d30_retained d30_retained on u.user_key = d30_retained.user_key
    left join tmp_d60_retained d60_retained on u.user_key = d60_retained.user_key
    left join tmp_d90_retained d90_retained on u.user_key = d90_retained.user_key
    left join tmp_d120_retained d120_retained on u.user_key = d120_retained.user_key
    left join tmp_d150_retained d150_retained on u.user_key = d150_retained.user_key

    left join tmp_session_cnt sc on u.user_key = sc.user_key
group by 1,2,3,4,5,6,7,8,9,10,11,12;

delete from kpi_processed.agg_marketing_kpi where install_date < '2014-09-01'
--and app like '_game_%'
    ;

update kpi_processed.agg_marketing_kpi
set d1_new_installs = 0,
    d1_retained = 0,
    d1_revenue = 0,
    d1_payers = 0
where install_date >= CURRENT_DATE - 1
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d3_new_installs = 0,
    d3_retained = 0,
    d3_revenue = 0,
    d3_payers = 0
where install_date >= CURRENT_DATE - 3
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d7_new_installs = 0,
    d7_retained = 0,
    d7_revenue = 0,
    d7_payers = 0
where install_date >= CURRENT_DATE - 7
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d15_new_installs = 0,
    d15_retained = 0,
    d15_revenue = 0,
    d15_payers = 0
where install_date >= CURRENT_DATE - 15
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d30_new_installs = 0,
    d30_retained = 0,
    d30_revenue = 0,
    d30_payers = 0
where install_date >= CURRENT_DATE - 30
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d60_new_installs = 0,
    d60_retained = 0,
    d60_revenue = 0,
    d60_payers = 0
where install_date >= CURRENT_DATE - 60
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d90_new_installs = 0,
    d90_retained = 0,
    d90_revenue = 0,
    d90_payers = 0
where install_date >= CURRENT_DATE - 90
--and app like '_game_%'
;

update kpi_processed.agg_marketing_kpi
set d120_new_installs = 0,
    d120_retained = 0,
    d120_revenue = 0,
    d120_payers = 0
where install_date >= CURRENT_DATE - 120
--and app like '_game_%'
;


update kpi_processed.agg_marketing_kpi
set d150_new_installs = 0,
    d150_retained = 0,
    d150_revenue = 0,
    d150_payers = 0
where install_date >= CURRENT_DATE - 150
--and app like '_game_%'
;


update kpi_processed.agg_marketing_kpi set install_source = 'Facebook Installs' where install_source = 'Off-Facebook Installs' ;
update kpi_processed.agg_marketing_kpi set install_source = 'AppLift' where install_source = 'Applift';
update kpi_processed.agg_marketing_kpi set install_source = 'Cheetah Mobile' where install_source = 'Cheetah';
update kpi_processed.agg_marketing_kpi set install_source = 'Mobilda' where app like 'ffs%' and  install_source in ('Mars Media Group','MarsMediaGroup')  ;
update kpi_processed.agg_marketing_kpi set install_source = 'Appturbo' where app like 'ffs%' and lower(install_source) = 'app turbo';

update kpi_processed.agg_marketing_kpi set install_source = 'Aarki(incent)' where app like 'ffs%' and install_source = 'AArki(Incent)';
update kpi_processed.agg_marketing_kpi set install_source = 'Glispa(incent)' where app like 'ffs%' and install_source = 'Glispa(Incent)';
update kpi_processed.agg_marketing_kpi set install_source = 'Native(incent)' where app like 'ffs%' and install_source = 'NativeX (Incent)';
update kpi_processed.agg_marketing_kpi set install_source = 'Sponsorpay(incent)' where app like 'ffs%' and install_source = 'Sponsorpay(Incent)';

update kpi_processed.agg_marketing_kpi set install_source = 'Organic' where app like 'ffs%' and install_source = 'organic';
update kpi_processed.agg_marketing_kpi set install_source = 'Ripple' where app like 'ffs%' and install_source = 'RIpple';
update kpi_processed.agg_marketing_kpi set install_source = 'Supersonic(incent)' where app like 'ffs%' and install_source = 'Supersonic Ads (Incent)';
update kpi_processed.agg_marketing_kpi set install_source = 'AdAction' where app like 'ffs%' and install_source = 'AdAction Interactive';
update kpi_processed.agg_marketing_kpi set install_source = 'Google Adwords' where app like 'ffs%' and install_source like 'Google Adwords%';
update kpi_processed.agg_marketing_kpi set install_source = 'CP-FFS-FP' where app like 'crazyplanet%' and install_source = 'CP-FF-FP';

-------- RS 2nd scene retention and ltv

create temp table  rs_2ndscene_installs as  
SELECT n.app_id,
       n.user_id,
       n.user_key,
       n.app_version,
       n.install_source,
       trunc(n.install_ts) AS install_date,
       n.os AS install_os,
       n.os_version AS install_os_version,
       n.browser AS install_browser,
       n.browser_version AS install_browser_version,
       n.language AS install_language,
       n.country AS install_country,
       n.device AS install_device,
       n.scene,
       u.is_payer
FROM kpi_processed.fact_new_user n
  LEFT JOIN kpi_processed.dim_user u
         ON n.app_id = u.app_id
        AND n.user_id = u.user_id
WHERE n.app_id LIKE 'royal%'
AND   n.scene = '2';


CREATE TEMP TABLE player_day_cube_2ndscene AS
WITH last_dt AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    null as install_subpublisher,
    null as install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.is_payer,
    d.scene,
    count(distinct d.user_key) new_user_cnt
FROM rs_2ndscene_installs d, player_day p, last_dt l, kpi_processed.fact_dau_snapshot dau
where
    d.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date and dau.scene=d.scene
    and d.app_id like 'royal%' and d.scene='2'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

CREATE TEMP TABLE baseline_2ndscene AS
WITH last_dt AS (SELECT max(date) AS date FROM kpi_processed.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    null as install_subpublisher,
    null as install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt,
    d.scene
 FROM kpi_processed.fact_dau_snapshot d
 JOIN rs_2ndscene_installs u ON d.user_key=u.user_key and d.scene=u.scene
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_dt l on 1=1
 WHERE
    u.install_date> DATEADD(day,-360,(select start_date from kpi_processed.init_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day 
    and d.app_id like 'royal%' and d.scene='2'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,17;



delete from kpi_processed.agg_retention_ltv_2ndscene
--where app_id like '_game_%'
;

insert into kpi_processed.agg_retention_ltv_2ndscene
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.is_payer,
    pc.scene,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube_2ndscene pc
LEFT JOIN baseline_2ndscene b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0) and
    COALESCE(pc.scene,'') = COALESCE(b.scene,'')
    ;

-- For farm lapsed user facebook_id
-- Add by JiaTing 2015-10-26
truncate kpi_processed.tmp_lapsed_users;
insert into kpi_processed.tmp_lapsed_users
(
    app_id
    ,facebook_id
    ,install_date
    ,install_source
    ,browser
    ,language
    ,country
    ,os
    ,level
    ,is_payer
    ,last_login_date
    ,lapsed_days
)
select
    app_id
    ,facebook_id
    ,install_date
    ,install_source
    ,browser
    ,language
    ,country
    ,os
    ,level
    ,is_payer
    ,last_login_date
    ,trunc(CONVERT_TIMEZONE('UTC', CURRENT_DATE)) - trunc(last_login_date) as lapsed_days
from kpi_processed.dim_user
where level >= 1 and level <= 600 and app_id like 'farm%';


-- For royal story payment not into 2nd scene facebook ids
-- Add by JiaTing 2015-11-02
truncate kpi_processed.payment_user_royal;
insert into kpi_processed.payment_user_royal
select distinct
    p.app_id,
    p.facebook_id,
    p.level,
    p.scene,
    u.last_login_date
    ,last_value(p.ts_pretty ignore nulls)
    OVER (PARTITION BY p.facebook_id ORDER BY p.ts_pretty ASC
    ROWS BETWEEN unbounded preceding AND unbounded following)
    AS s_pretty
from kpi_processed.dim_user as u, kpi_processed.payment as p 
where 
    u.facebook_id = p.facebook_id 
    and p.app_id like 'royal%';
    
    
--------ha churned user by level----------------------------------------
create temp table tmp_dau_level
as
select date
      ,coalesce(level_end,level_start) as level
      ,count(distinct user_key) as user_cnt
from kpi_processed.fact_dau_snapshot
where app_id like 'ha%'
group by 1,2;


create temp table tmp_churned_user
as
select last_login_date+7 as churned_date
      ,level
      ,count(distinct user_key) as churned_cnt
from kpi_processed.dim_user where app_id like 'ha%'
and datediff(day,last_login_date,current_date-1)>=7
group by 1,2;



drop table if exists kpi_processed.ha_churned_user_by_level_d7;
create table kpi_processed.ha_churned_user_by_level_d7
as
select t1.churned_date
      ,t1.level
      ,t1.churned_cnt
      ,t2.user_cnt
from tmp_churned_user t1
left join tmp_dau_level t2
on t1.level = t2.level
and t1.churned_date = t2.date;

------agg monthly kpi-------------------------
delete from kpi_processed.agg_kpi_monthly
where datediff(month, date, sysdate)<=2
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_monthly
select date, app_id, os, country, 
sum(is_new_user) as new_installs, 
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as mau,
sum(revenue_usd) as revenue,
install_source_group,
install_source
from
    (select a.*, u.app_id, u.os, u.country,
        case when u.app_id like 'farm%' then u.install_source_group
           else u.install_source end as install_source_group,
        u.install_source
    from
    (select date_trunc('month', date) as date, user_key,
    max(is_new_user) as is_new_user,
    max(is_converted_today) as is_new_payer,
    sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot 
    where datediff(month, date, sysdate)<=2
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by date,app_id,os,country,install_source_group,install_source;

------ agg weekly kpi -------------
delete from kpi_processed.agg_kpi_weekly
where date>=(select max(date) from kpi_processed.agg_kpi_weekly)
and date<=(select dateadd('day',7,max(date) ) from kpi_processed.agg_kpi_weekly)
--and app_id like '_game_%'
;

insert into kpi_processed.agg_kpi_weekly
select date, app_id, os, country, 
sum(is_new_user) as new_installs, 
count(distinct case when revenue_usd>0 then user_key else NULL end) as payers,
sum(is_new_payer) as new_payers,
count(1) as wau,
sum(revenue_usd) as revenue,
install_source_group,
install_source
from
    (select a.*, u.app_id, u.os, u.country,
        case when u.app_id like 'farm%' then u.install_source_group
           else u.install_source end as install_source_group,
        u.install_source
    from
    (select 
        date_trunc('week', date) as date,
        user_key,
        max(is_new_user) as is_new_user,
        max(is_converted_today) as is_new_payer,
        sum(revenue_usd) as revenue_usd
    from kpi_processed.fact_dau_snapshot 
    where 
      date>=(select dateadd('day',7,max(date) ) from kpi_processed.agg_kpi_weekly)
     and date<=(select dateadd('day',14,max(date) ) from kpi_processed.agg_kpi_weekly)
    --and app_id like '_game_%'
    group by 1,2) a
    join
    kpi_processed.dim_user u
    on a.user_key = u.user_key)
group by date,app_id,os,country,install_source_group,install_source;

---------ffs marketing ROI-------------------------------------
--update FFS install source based on https://docs.google.com/spreadsheets/d/1usPcMrXnTLnt0CR7PwdIG1_KdbxfBspDJtMVhJ4V0xg/edit#gid=0

create temp table temp_singular
as
select app
      ,start_date
      ,country_field
      ,os
      ,custom_installs
      ,adn_impressions
      ,custom_clicks
      ,adn_cost
      ,case when source = 'MoBrain' then 'Headway'
            when source = 'Jump Ramp Games' then 'Jump Ramp'
            when source = 'Crunchie Media (InMedia)' then 'Crunchiemedia'
            when source = 'AppLift' and position('social' in lower(adn_campaign)) = 0 then 'Applift'
            when source = 'AppLift' and position('social' in lower(adn_campaign)) > 0 then 'Applift-social'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) > 0 then 'Google search'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) = 0 then 'Google Adwords'
            when source = 'Instagram' then 'Instagram Installs'
            when source = 'Facebook' and position('_ism_' in lower(adn_campaign)) > 0 then 'Instagram Installs'
            when source = 'Facebook' and position('sprinklr_' in lower(adn_campaign)) > 0 then 'Sprinklr'
            when source = 'Blind Ferret Media' then 'Blindferret'
            when source = 'Facebook' and position('socialclicks_' in lower(adn_campaign)) > 0 then 'Socialclicks'
            when source = 'Facebook' 
                   and position('socialclicks_' in lower(adn_campaign)) = 0 
                   and position('sprinklr_' in lower(adn_campaign)) = 0 
                   and position('_ism_' in lower(adn_campaign)) = 0 
                   then 'Facebook Installs'
            when source = 'Fyber' and (position('incent' in lower(adn_campaign)) > 0 or position('inccent' in lower(adn_campaign)) > 0) then 'Sponsorpay(incent)'
            when source = 'Fyber' and position('incent' in lower(adn_campaign)) = 0 and position('inccent' in lower(adn_campaign)) = 0 then 'Sponsorpay'
            when source = 'SupersonicAds' and position('incent' in lower(adn_campaign)) > 0 then 'Supersonic(incent)'
            when source = 'SupersonicAds' and position('incent' in lower(adn_campaign)) = 0 then 'Supersonic'
            else source
       end as source
from raw_data.singular
where app = 'Family Farm Seaside';


-- install_source_map
create temp table install_source_mapping as
select distinct 'Marketing' as source
      ,'Family Farm Seaside' as game 
      ,case when install_source = 'Facebook Installs' and position('sprinklr_' in lower(campaign)) > 0 then 'Sprinklr'
            when install_source = 'Facebook Installs' and position('socialclicks_' in lower(campaign)) > 0 then 'Socialclicks'
            when install_source = 'Instagram' then 'Instagram Installs'
            when install_source = 'Facebook Installs' and position('_ism_' in lower(campaign)) > 0 then 'Instagram Installs'
        else install_source end 
        as install_source
      ,case when install_source = 'Facebook Installs' and position('sprinklr_' in lower(campaign)) > 0 then 'Sprinklr'
            when install_source = 'Facebook Installs' and position('socialclicks_' in lower(campaign)) > 0 then 'Socialclicks'
            when install_source = 'Instagram' then 'Instagram Installs'
            when install_source = 'Facebook Installs' and position('_ism_' in lower(campaign)) > 0 then 'Instagram Installs'
        else install_source end 
        as install_source_lower
from   kpi_processed.agg_marketing_kpi
where app like 'ffs%'
union all 
select distinct 'Singular' as source
      ,app as game
      ,source as install_source
      ,source as install_source_lower
from temp_singular
where app = 'Family Farm Seaside';


update install_source_mapping
set install_source_lower = replace(install_source_lower, '\+', '');

update install_source_mapping
set install_source_lower = replace(install_source_lower, '\-', '');

update install_source_mapping
set install_source_lower = replace(install_source_lower, '\(', '');

update install_source_mapping
set install_source_lower = replace(install_source_lower, '\)', '');

update install_source_mapping
set install_source_lower = replace(install_source_lower, ' ', '');

update install_source_mapping
set install_source_lower = lower(install_source_lower)
;


--installs
create temp table installs as
select m.app
      ,m.install_date
      ,s.install_source_lower
      ,case when substring(substring(os,position('.' in os)+1,length(os)),position('.' in substring(os,position('.' in os)+1,length(os)))+1,length(substring(os,position('.' in os)+1,length(os)))) = 'iOS'
      then substring(substring(os,position('.' in os)+1,length(os)),position('.' in substring(os,position('.' in os)+1,length(os)))+1,length(substring(os,position('.' in os)+1,length(os))))
      else initcap(substring(substring(os,position('.' in os)+1,length(os)),position('.' in substring(os,position('.' in os)+1,length(os)))+1,length(substring(os,position('.' in os)+1,length(os))))) end as os
      ,sum(new_installs) as new_installs
from kpi_processed.agg_marketing_kpi m
join install_source_mapping s 
on   s.source = 'Marketing' 
and s.install_source = case when m.install_source = 'Facebook Installs' and position('sprinklr_' in lower(m.campaign)) > 0 then 'Sprinklr'
            when m.install_source = 'Facebook Installs' and position('socialclicks_' in lower(m.campaign)) > 0 then 'Socialclicks'
            when m.install_source = 'Instagram' then 'Instagram Installs'
            when m.install_source = 'Facebook Installs' and position('_ism_' in lower(m.campaign)) > 0 then 'Instagram Installs'
        else m.install_source end 
left join kpi_processed.dim_country d
on m.country = d.country
where m.app like 'ffs%'
group by 1,2,3,4;


--cost
create temp table cost as
select c.app as app
      ,c.start_date as install_date
      ,s.install_source_lower
      ,c.os
      ,sum(cast (case when custom_installs in ('', 'None') then '0' else custom_installs end as numeric)) as singular_installs
      ,sum(cast (case when adn_impressions in ('', 'None') then '0' else adn_impressions end as numeric)) as adn_impressions
      ,sum(cast (case when custom_clicks in ('', 'None') then '0' else custom_clicks end as numeric)) as clicks
      ,sum(cast(case when adn_cost in ('', 'None') then '0' else adn_cost end as numeric(14,4))) as cost
from  temp_singular c
join  install_source_mapping s 
on    s.source = 'Singular' 
and   c.source = s.install_source
and   c.app = s.game
group by 1,2,3,4;


--install_cost
create temp table install_cost as
select i.install_date
      ,i.os
      ,i.app
      ,i.install_source_lower
      ,m.install_source
      ,coalesce(i.new_installs,0) as bi_installs
      ,coalesce(c.singular_installs,0) as singular_installs
      ,coalesce(c.adn_impressions,0) as adn_impressions
      ,coalesce(c.clicks,0) as clicks
      ,coalesce(c.cost,0) as cost
from installs i
join install_source_mapping m
on i.install_source_lower = m.install_source_lower
and m.source = 'Marketing'
left join cost c
on i.os = c.os
and i.install_source_lower = c.install_source_lower
and i.install_date = c.install_date
and c.app = case when i.app = 'ffs.global.prod' then 'Family Farm Seaside' else 'Others' end;


---------ha marketing ROI-------------------------------------

drop table if exists kpi_processed.agg_marketing_kpi_singular;

create temp table install_source_singular_country as
select  app 
       ,start_date 
       ,adn_campaign 
       ,os 
       ,coalesce(t.country,'Unknown') as country
       ,source 
       ,sum(cast (case when custom_installs in ('', 'None') then '0' else custom_installs end as numeric)) as singular_installs
       ,sum(cast (case when adn_impressions in ('', 'None') then '0' else adn_impressions end as numeric)) as adn_impressions
       ,sum(cast (case when custom_clicks in ('', 'None') then '0' else custom_clicks end as numeric)) as clicks
       ,sum(cast(case when adn_cost in ('', 'None') then '0' else adn_cost end as numeric(14,4))) as cost
from raw_data.singular s
left join 
      (select dc.country_code
             ,dc.country
             ,c.alpha_3_code
        from kpi_processed.dim_country dc
        left join kpi_processed.dim_country_complete  c
        on dc.country_code = c.alpha_2_code
      )t
on s.country_field = t.alpha_3_code
group by 1,2,3,4,5,6
;

---------waf，warship marketing ROI-------------------------------------
--update WAF install source based on https://docs.google.com/spreadsheets/d/1usPcMrXnTLnt0CR7PwdIG1_KdbxfBspDJtMVhJ4V0xg/edit#gid=1255875480
create temp table temp_singular_mobile
as
select app
      ,start_date
      ,country_field
      ,os
      ,custom_installs
      ,adn_impressions
      ,custom_clicks
      ,adn_cost
      ,case when source = 'AppLift' and position('social' in lower(adn_campaign)) = 0 then 'AppLift'
            when source = 'AppLift' and position('social' in lower(adn_campaign)) > 0 then 'AppLift-Social Channels'
            when source = 'AppLift' and position('incent' in lower(adn_campaign)) > 0 then 'AppLift(incent)'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) > 0 then 'Adwords(Search)'
            when source = 'AdWords' and position('search' in lower(adn_campaign)) = 0 then 'Adwords(Display)'
            when source = 'MassiveImpact' and position('myh' in lower(adn_campaign)) > 0 then 'MassiveImpact-Yahoo'
            when source = 'MassiveImpact' and position('mtr' in lower(adn_campaign)) > 0 then 'MassiveImpact-Tumblr'
            when source = 'MassiveImpact' and (position('myh' in lower(adn_campaign)) = 0  and position('mtr' in lower(adn_campaign)) = 0) then 'MassiveImpact-Twitter'
            when source = 'Facebook' and position('_ins_' in lower(adn_campaign)) > 0 then 'Instagram Installs'
            when source = 'Facebook' and position('_ins_' in lower(adn_campaign)) = 0 then 'Facebook Installs'
            when source = 'Motive Interactive' then 'Motive'
            when position('incent' in lower(adn_campaign)) >0 then source + '(incent)'
            else source
       end as source
from raw_data.singular
where app in ('What a Farm','Battle Warships','Smash Island','Kiss the Chef','Sandigma');


-- install_source_map
create temp table install_source_mapping_mobile as
select distinct 'Marketing' as source
      ,case when app = 'waf.global.prod' then 'What a Farm' 
            when app = 'battlewarship.global.prod' then 'Battle Warships'
            when app = 'crazyplanets.global.prod' then 'Smash Island'
            when app = 'lc.global.prod' then 'Kiss the Chef'
            when app = 'sandigma.global.prod' then 'Sandigma'
            else '' end as game 
      ,install_source
      ,install_source as install_source_lower
from   kpi_processed.agg_marketing_kpi
where app in ('waf.global.prod','battlewarship.global.prod','crazyplanets.global.prod','lc.global.prod','sandigma.global.prod')
union all 
select distinct 'Singular' as source
      ,app as game
      ,source as install_source
      ,source as install_source_lower
from temp_singular_mobile;


update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\+', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\-', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\(', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, '\)', '');

update install_source_mapping_mobile
set install_source_lower = replace(install_source_lower, ' ', '');

update install_source_mapping_mobile
set install_source_lower = lower(install_source_lower)
;


--installs
create temp table installs_mobile as
select m.app
      ,m.install_date
      ,s.install_source_lower
      ,case when lower(os) = 'ios' then 'iOS'
             when lower(os) = 'android' then 'Android'
       else os end as os
      ,sum(new_installs) as new_installs
from kpi_processed.agg_marketing_kpi m
join install_source_mapping_mobile s 
on   s.source = 'Marketing' 
and s.install_source = m.install_source
and s.game = case when m.app = 'waf.global.prod' then 'What a Farm' 
                  when m.app = 'battlewarship.global.prod' then 'Battle Warships' 
                  when m.app = 'crazyplanets.global.prod' then 'Smash Island'
                  when m.app = 'lc.global.prod' then 'Kiss the Chef'
                  when m.app = 'sandigma.global.prod' then 'Sandigma'
                  else 'Others' end
where m.app in ('waf.global.prod','battlewarship.global.prod','crazyplanets.global.prod','lc.global.prod','sandigma.global.prod')
group by 1,2,3,4;


--cost
create temp table cost_mobile as
select c.app as app
      ,c.start_date as install_date
      ,s.install_source_lower
      ,c.os
      ,sum(cast (case when custom_installs in ('', 'None') then '0' else custom_installs end as numeric)) as singular_installs
      ,sum(cast (case when adn_impressions in ('', 'None') then '0' else adn_impressions end as numeric)) as adn_impressions
      ,sum(cast (case when custom_clicks in ('', 'None') then '0' else custom_clicks end as numeric)) as clicks
      ,sum(cast(case when adn_cost in ('', 'None') then '0' else adn_cost end as numeric(14,4))) as cost
from  temp_singular_mobile c
join  install_source_mapping_mobile s 
on    s.source = 'Singular' 
and   c.source = s.install_source
and   c.app = s.game
group by 1,2,3,4;


--install_cost
create temp table install_cost_mobile as
select i.install_date
      ,i.os
      ,i.app
      ,i.install_source_lower
      ,m.install_source
      ,coalesce(i.new_installs,0) as bi_installs
      ,coalesce(c.singular_installs,0) as singular_installs
      ,coalesce(c.adn_impressions,0) as adn_impressions
      ,coalesce(c.clicks,0) as clicks
      ,coalesce(c.cost,0) as cost
from installs_mobile i
join install_source_mapping_mobile m
on i.install_source_lower = m.install_source_lower
and m.source = 'Marketing'
and m.game = case when i.app = 'waf.global.prod' then 'What a Farm' 
                  when i.app = 'battlewarship.global.prod' then 'Battle Warships' 
                  when i.app = 'crazyplanets.global.prod' then 'Smash Island'
                  when i.app = 'lc.global.prod' then 'Kiss the Chef'
                  when i.app = 'sandigma.global.prod' then 'Sandigma'
                  else 'Others' end
left join cost_mobile c
on i.os = c.os
and i.install_source_lower = c.install_source_lower
and i.install_date = c.install_date
and c.app = case when i.app = 'waf.global.prod' then 'What a Farm' 
                 when i.app = 'battlewarship.global.prod' then 'Battle Warships' 
                 when i.app = 'crazyplanets.global.prod' then 'Smash Island'
                 when i.app = 'lc.global.prod' then 'Kiss the Chef'
                 when i.app = 'sandigma.global.prod' then 'Sandigma'
                 else 'Others' end;

drop table if exists kpi_processed.agg_marketing_kpi_singular;
create table kpi_processed.agg_marketing_kpi_singular
as
select  k.app                 
       ,k.install_date        
       ,k.install_date_str    
       ,case when k.install_source = 'Facebook Installs' and position('sprinklr_' in lower(k.campaign)) > 0 then 'Sprinklr'
            when k.install_source = 'Facebook Installs' and position('socialclicks_' in lower(k.campaign)) > 0 then 'Socialclicks'
            when k.install_source = 'Instagram' then 'Instagram Installs'
            when k.install_source = 'Facebook Installs' and position('_ism_' in lower(k.campaign)) > 0 then 'Instagram Installs'
        else k.install_source end 
        as install_source     
       ,k.install_source_group
       ,'' as country             
       ,k.os                  
       ,ic.bi_installs as bi_installs
       ,ic.singular_installs as singular_installs
       ,ic.adn_impressions as adn_impressions
       ,ic.clicks as clicks
       ,ic.cost as cost
       ,install_month       
       ,sum(new_installs)  as new_installs    
       ,sum(d1_new_installs) as d1_new_installs    
       ,sum(d7_new_installs) as d7_new_installs    
       ,sum(d15_new_installs) as d15_new_installs    
       ,sum(d30_new_installs) as d30_new_installs   
       ,sum(d60_new_installs)  as d60_new_installs  
       ,sum(d90_new_installs) as d90_new_installs   
       ,sum(d120_new_installs) as d120_new_installs  
       ,sum(d150_new_installs) as d150_new_installs  
       ,sum(revenue)   as revenue          
       ,sum(d1_revenue)  as d1_revenue        
       ,sum(d7_revenue) as d7_revenue        
       ,sum(d15_revenue) as d15_revenue        
       ,sum(d30_revenue) as d30_revenue        
       ,sum(d60_revenue) as d60_revenue        
       ,sum(d90_revenue) as d90_revenue        
       ,sum(d120_revenue) as d120_revenue       
       ,sum(d150_revenue) as d150_revenue       
       ,sum(payers)  as payers             
       ,sum(d1_payers)  as d1_payers         
       ,sum(d7_payers) as d7_payers          
       ,sum(d15_payers) as d15_payers          
       ,sum(d30_payers) as d30_payers         
       ,sum(d60_payers) as d60_payers         
       ,sum(d90_payers) as d90_payers         
       ,sum(d120_payers) as d120_payers        
       ,sum(d150_payers) as d150_payers        
       ,sum(d1_retained) as d1_retained        
       ,sum(d7_retained)  as d7_retained       
       ,sum(d15_retained)  as d15_retained       
       ,sum(d30_retained) as d30_retained       
       ,sum(d60_retained) as d60_retained       
       ,sum(d90_retained)  as d90_retained      
       ,sum(d120_retained)   as d120_retained    
       ,sum(d150_retained)   as d150_retained    
       ,sum(d3_new_installs)  as d3_new_installs   
       ,sum(d3_revenue) as d3_revenue         
       ,sum(d3_payers)  as d3_payers         
       ,sum(d3_retained) as d3_retained
from  kpi_processed.agg_marketing_kpi k
left join install_cost ic
on k.app = ic.app
and k.install_date = ic.install_date
and ic.install_source = case when k.install_source = 'Facebook Installs' and position('sprinklr_' in lower(k.campaign)) > 0 then 'Sprinklr'
            when k.install_source = 'Facebook Installs' and position('socialclicks_' in lower(k.campaign)) > 0 then 'Socialclicks'
            when k.install_source = 'Instagram' then 'Instagram Installs'
            when k.install_source = 'Facebook Installs' and position('_ism_' in lower(k.campaign)) > 0 then 'Instagram Installs'
        else k.install_source end 
and ic.os = case when substring(substring(k.os,position('.' in k.os)+1,length(k.os)),position('.' in substring(k.os,position('.' in k.os)+1,length(k.os)))+1,length(substring(k.os,position('.' in k.os)+1,length(k.os)))) = 'iOS'
      then substring(substring(k.os,position('.' in k.os)+1,length(k.os)),position('.' in substring(k.os,position('.' in k.os)+1,length(k.os)))+1,length(substring(k.os,position('.' in k.os)+1,length(k.os))))
      else initcap(substring(substring(k.os,position('.' in k.os)+1,length(k.os)),position('.' in substring(k.os,position('.' in k.os)+1,length(k.os)))+1,length(substring(k.os,position('.' in k.os)+1,length(k.os))))) end
where k.app like 'ffs%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13
union all
select  k.app                 
       ,k.install_date        
       ,k.install_date_str    
       ,k.install_source      
       ,k.install_source_group
       ,k.country             
       ,k.os                  
       ,sum(new_installs) as bi_installs
       ,ic.singular_installs as singular_installs
       ,ic.adn_impressions as adn_impressions
       ,ic.clicks as clicks
       ,ic.cost as cost
       ,install_month       
       ,sum(new_installs)  as new_installs    
       ,sum(d1_new_installs) as d1_new_installs    
       ,sum(d7_new_installs) as d7_new_installs    
       ,sum(d15_new_installs) as d15_new_installs    
       ,sum(d30_new_installs) as d30_new_installs   
       ,sum(d60_new_installs)  as d60_new_installs  
       ,sum(d90_new_installs) as d90_new_installs   
       ,sum(d120_new_installs) as d120_new_installs  
       ,sum(d150_new_installs) as d150_new_installs  
       ,sum(revenue)   as revenue          
       ,sum(d1_revenue)  as d1_revenue        
       ,sum(d7_revenue) as d7_revenue        
       ,sum(d15_revenue) as d15_revenue        
       ,sum(d30_revenue) as d30_revenue        
       ,sum(d60_revenue) as d60_revenue        
       ,sum(d90_revenue) as d90_revenue        
       ,sum(d120_revenue) as d120_revenue       
       ,sum(d150_revenue) as d150_revenue       
       ,sum(payers)  as payers             
       ,sum(d1_payers)  as d1_payers         
       ,sum(d7_payers) as d7_payers          
       ,sum(d15_payers) as d15_payers          
       ,sum(d30_payers) as d30_payers         
       ,sum(d60_payers) as d60_payers         
       ,sum(d90_payers) as d90_payers         
       ,sum(d120_payers) as d120_payers        
       ,sum(d150_payers) as d150_payers        
       ,sum(d1_retained) as d1_retained        
       ,sum(d7_retained)  as d7_retained       
       ,sum(d15_retained)  as d15_retained       
       ,sum(d30_retained) as d30_retained       
       ,sum(d60_retained) as d60_retained       
       ,sum(d90_retained)  as d90_retained      
       ,sum(d120_retained)   as d120_retained    
       ,sum(d150_retained)   as d150_retained    
       ,sum(d3_new_installs)  as d3_new_installs   
       ,sum(d3_revenue) as d3_revenue         
       ,sum(d3_payers)  as d3_payers         
       ,sum(d3_retained) as d3_retained
from  kpi_processed.agg_marketing_kpi k
left join install_source_singular_country ic
on ic.app = case when k.app like 'ha%' then 'Happy Acres' else 'Others' end
and k.install_date = ic.start_date
and ic.app = 'Happy Acres'
and k.install_source = ic.adn_campaign
and k.country = ic.country
where k.app like 'ha%'
group by 1,2,3,4,5,6,7,9,10,11,12,13
union all
select  k.app                 
       ,k.install_date        
       ,k.install_date_str    
       ,k.install_source    
       ,k.install_source_group
       ,'' as country             
       ,k.os                  
       ,ict.bi_installs as bi_installs
       ,ict.singular_installs as singular_installs
       ,ict.adn_impressions as adn_impressions
       ,ict.clicks as clicks
       ,ict.cost as cost
       ,install_month       
       ,sum(new_installs)  as new_installs    
       ,sum(d1_new_installs) as d1_new_installs    
       ,sum(d7_new_installs) as d7_new_installs    
       ,sum(d15_new_installs) as d15_new_installs    
       ,sum(d30_new_installs) as d30_new_installs   
       ,sum(d60_new_installs)  as d60_new_installs  
       ,sum(d90_new_installs) as d90_new_installs   
       ,sum(d120_new_installs) as d120_new_installs  
       ,sum(d150_new_installs) as d150_new_installs  
       ,sum(revenue)   as revenue          
       ,sum(d1_revenue)  as d1_revenue        
       ,sum(d7_revenue) as d7_revenue        
       ,sum(d15_revenue) as d15_revenue        
       ,sum(d30_revenue) as d30_revenue        
       ,sum(d60_revenue) as d60_revenue        
       ,sum(d90_revenue) as d90_revenue        
       ,sum(d120_revenue) as d120_revenue       
       ,sum(d150_revenue) as d150_revenue       
       ,sum(payers)  as payers             
       ,sum(d1_payers)  as d1_payers         
       ,sum(d7_payers) as d7_payers          
       ,sum(d15_payers) as d15_payers          
       ,sum(d30_payers) as d30_payers         
       ,sum(d60_payers) as d60_payers         
       ,sum(d90_payers) as d90_payers         
       ,sum(d120_payers) as d120_payers        
       ,sum(d150_payers) as d150_payers        
       ,sum(d1_retained) as d1_retained        
       ,sum(d7_retained)  as d7_retained       
       ,sum(d15_retained)  as d15_retained       
       ,sum(d30_retained) as d30_retained       
       ,sum(d60_retained) as d60_retained       
       ,sum(d90_retained)  as d90_retained      
       ,sum(d120_retained)   as d120_retained    
       ,sum(d150_retained)   as d150_retained    
       ,sum(d3_new_installs)  as d3_new_installs   
       ,sum(d3_revenue) as d3_revenue         
       ,sum(d3_payers)  as d3_payers         
       ,sum(d3_retained) as d3_retained
from  kpi_processed.agg_marketing_kpi k
left join install_cost_mobile ict
on k.app = ict.app
and k.install_source = ict.install_source
and k.install_date = ict.install_date
and ict.os = k.os
where k.app in ('waf.global.prod','battlewarship.global.prod','crazyplanets.global.prod','lc.global.prod','sandigma.global.prod')
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;
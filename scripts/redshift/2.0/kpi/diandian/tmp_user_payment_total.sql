DROP table if exists tmp_user_payment_total_fds; 
CREATE temp table tmp_user_payment_total_fds as 
-- latest revenue
select 
	app_id
	,user_key
	,max(date) as date
	,sum(revenue_usd) as revenue_usd
	,sum(payment_cnt) as purchase_cnt
from kpi_processed.fact_dau_snapshot
where 
	date > (select max(date) from kpi_processed.tmp_user_payment_total)
group by 1,2
;

-- users already in the table
DROP table if exists tmp_user_payment_total_t1;
CREATE temp table tmp_user_payment_total_t1 as 
select 
	a.user_key
	,a.date
	,a.revenue_usd + b.total_revenue_usd as total_revenue_usd
	,a.purchase_cnt + b.total_purchase_cnt as total_purchase_cnt
from 
tmp_user_payment_total_fds a 
left join kpi_processed.tmp_user_payment_total b 
on a.user_key = b.user_key
where b.user_key is not null 
;

-- users not in the table
DROP table if exists tmp_user_payment_total_t2;
CREATE temp table tmp_user_payment_total_t2 as 

select 
    a.app_id
	,a.user_key
	,a.date
	,a.revenue_usd as total_revenue_usd
	,a.purchase_cnt as total_purchase_cnt
from 
tmp_user_payment_total_fds a 
left join kpi_processed.tmp_user_payment_total b 
on a.user_key = b.user_key
where b.user_key is null
;

-- update total revenue for users who already in the table
UPDATE kpi_processed.tmp_user_payment_total
set total_revenue_usd = t1.total_revenue_usd,
    total_purchase_cnt = t1.total_purchase_cnt,
    date = t1.date
from tmp_user_payment_total_t1 t1
where t1.user_key = kpi_processed.tmp_user_payment_total.user_key;

INSERT INTO kpi_processed.tmp_user_payment_total
select * from tmp_user_payment_total_t2 t2;


-- update kpi_processed.dim_user
UPDATE kpi_processed.dim_user
set revenue_usd = t.total_revenue_usd,
    payment_cnt = t.total_purchase_cnt

from kpi_processed.tmp_user_payment_total t 
where t.user_key = kpi_processed.dim_user.user_key
;

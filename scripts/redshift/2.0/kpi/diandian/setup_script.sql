-----------------------------------------------
--Data 2.0 Setup script
-----------------------------------------------
update kpi_processed.init_start_date                                                          
set start_date =  DATEADD(day, -3, CURRENT_DATE);

delete from raw_events.events where app_id like 'lc.global%' and date(ts_pretty)>=
     (
          select start_date
          from   kpi_processed.init_start_date
                   );

-- delete from raw_events.events
-- where user_id is null and app_id is null;

-- DELETE FROM raw_events.events
-- WHERE user_id is null or user_id = ''
--     and date(ts_pretty) >=
--        (
--          select start_date
--          from   kpi_processed.init_start_date
--         );

drop table if exists tmp_raw_events;

create table tmp_raw_events as
select *
from raw_events.events
where ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        );

delete from tmp_raw_events
where user_id is null or user_id = '' or app_id is null;

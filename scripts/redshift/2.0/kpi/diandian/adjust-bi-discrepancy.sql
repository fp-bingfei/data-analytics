update kpi_processed.dim_user
    set install_source = 'Organic'
where app_id = 'crazyplanets.global.prod'
    and install_source = 'Adjust Organic';

drop table if exists adjust_bi_discrepancy;
create table adjust_bi_discrepancy as
select
    t1.install_date
    ,cast(t1.install_date as VARCHAR) as install_date_str
    ,t1.app_id
    ,t1.country
    ,t1.install_source
    ,t1.campaign
    ,t1.sub_publisher
    ,t1.creative_id
    ,t1.os
    ,t1.all_adjust_installs
    ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
    ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
    ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
    ,coalesce(t5.marketing_installs, 0) as marketing_installs
from
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,game as app_id
    ,country
    ,trunc(ts) as install_date
    ,case
        when app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,count(distinct adid) as all_adjust_installs
from adjust 
group by 1,2,3,4,5,6,7,8) t1
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,game as app_id
    ,country
    ,trunc(ts) as install_date
    ,case
        when app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,count(distinct adid) as empty_user_id_installs
from adjust
where userid = '' or userid is null
group by 1,2,3,4,5,6,7,8) t2
    on coalesce(t1.install_source,'Unknown') = coalesce(t2.install_source,'Unknown') and
       t1.install_date = t2.install_date and
       coalesce(t1.campaign,'Unknown') = coalesce(t2.campaign,'Unknown') and
       coalesce(t1.sub_publisher,'Unknown') = coalesce(t2.sub_publisher,'Unknown') and
       coalesce(t1.creative_id,'Unknown') = coalesce(t2.creative_id,'Unknown') and
       t1.app_id = t2.app_id and
       coalesce(t1.country,'Unknown') = coalesce(t2.country,'Unknown') and
       coalesce(t1.os,'Unknown') = coalesce(t2.os,'Unknown')
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,game as app_id
    ,a.country
    ,trunc(ts) as install_date
    ,case
        when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,count(distinct a.adid) in_bi_installs
from adjust a join kpi_processed.dim_user u on a.userid = u.user_id and u.app_id = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
where a.userid != '' and a.userid is not null and u.user_id is not null
group by 1,2,3,4,5,6,7,8) t3
    on coalesce(t1.install_source,'Unknown') = coalesce(t3.install_source,'Unknown') and
       t1.install_date = t3.install_date and
       coalesce(t1.campaign,'Unknown') = coalesce(t3.campaign,'Unknown') and
       coalesce(t1.sub_publisher,'Unknown') = coalesce(t3.sub_publisher,'Unknown') and
       coalesce(t1.creative_id,'Unknown') = coalesce(t3.creative_id,'Unknown') and
       t1.app_id = t3.app_id and
       coalesce(t1.country,'Unknown') = coalesce(t3.country,'Unknown') and
       coalesce(t1.os,'Unknown') = coalesce(t3.os,'Unknown')
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,game as app_id
    ,a.country
    ,trunc(ts) as install_date
    ,case
        when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,count(distinct a.adid) not_in_bi_installs
from adjust a left join kpi_processed.dim_user u on a.userid = u.user_id and u.app_id = a.game
where a.userid != '' and a.userid is not null and (u.user_id is null or lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) != lower(u.install_source) or trunc(a.ts) != u.install_date)
group by 1,2,3,4,5,6,7,8) t4
    on coalesce(t1.install_source,'Unknown') = coalesce(t4.install_source,'Unknown') and
       t1.install_date = t4.install_date and
       coalesce(t1.campaign,'Unknown') = coalesce(t4.campaign,'Unknown') and
       coalesce(t1.sub_publisher,'Unknown') = coalesce(t4.sub_publisher,'Unknown') and
       coalesce(t1.creative_id,'Unknown') = coalesce(t4.creative_id,'Unknown') and
       t1.app_id = t4.app_id and
       coalesce(t1.country,'Unknown') = coalesce(t4.country,'Unknown') and
       coalesce(t1.os,'Unknown') = coalesce(t4.os,'Unknown')
left join
(select 
    m.install_source
    , m.campaign
    , m.sub_publisher
    , m.creative_id
    , m.app as app_id
    , lower(c.country_code) as country
    , m.install_date
    , case
        when os like '%OS%' then 'iOS'
        when os like '%amazon%' then 'Amazon'
        else 'Android'
    end as os
    , sum(m.new_installs) as marketing_installs
from kpi_processed.agg_marketing_kpi m, kpi_processed.dim_country c
where m.country = c.country
group by 1,2,3,4,5,6,7,8
) t5
    on coalesce(nullif(t1.install_source, ''),'Unknown') = coalesce(nullif(t5.install_source, ''),'Unknown') and
       t1.install_date = t5.install_date and
       coalesce(nullif(t1.campaign, ''),'Unknown') = coalesce(nullif(t5.campaign, ''),'Unknown') and
       coalesce(nullif(t1.sub_publisher, ''),'Unknown') = coalesce(nullif(t5.sub_publisher, ''),'Unknown') and
       coalesce(nullif(t1.creative_id, ''),'Unknown') = coalesce(nullif(t5.creative_id, ''),'Unknown') and
       t1.app_id = t5.app_id and
       coalesce(nullif(t1.country, ''),'Unknown') = coalesce(nullif(t5.country, ''),'Unknown') and
       coalesce(nullif(t1.os, ''),'Unknown') = coalesce(nullif(t5.os, ''),'Unknown')
;

update adjust_bi_discrepancy
set install_source = replace(install_source, '+', ' ')
    ,campaign = replace(campaign, '+', ' ')
    ,sub_publisher = replace(sub_publisher, '+', ' ')
    ,creative_id = replace(creative_id, '+', ' ');
    

-- Remove duplicated records from adjust table
drop table if exists unique_adjust;
create table unique_adjust as
select *
from
(select *, row_number() over (partition by adid order by ts) as row_number
from adjust)  t
where row_number = 1;

update unique_adjust
set userid = a.userid
from adjust a
where unique_adjust.userid = '' and a.userid != '' and unique_adjust.adid = a.adid;

-- Fraud with same IP in one day
drop table if exists kpi_processed.adjust_ip_fraud;
create table kpi_processed.adjust_ip_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,count(1) as fraud_count
    ,count(distinct a.userid) as distinct_fraud_count
from
    unique_adjust a
    left join
    kpi_processed.dim_country c on a.country=lower(c.country_code)
where 
    (a.ip_address, date(a.ts))
    in 
    (
        select 
            ip_address
            ,date 
        from 
            (
                select 
                    ip_address
                    ,date(ts) as date
                    ,count(1) 
                from 
                    unique_adjust 
                where
                    tracker_name<>'Organic'
                    and date(ts)>=DATEADD(DAY, -90, current_date)
                group by 1,2
                having count(1)>=10
            )
    )
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -90, current_date)
group by 1,2,3,4,5,6,7,8,9;

-- Timestamp pattern of generating records
drop table if exists kpi_processed.adjust_ts_pattern;
create table kpi_processed.adjust_ts_pattern as
select
    *
from
    (
    select
        t.install_source
        ,t.campaign
        ,t.sub_publisher
        ,t.creative_id
        ,t.app_id
        ,t.country
        ,t.install_date
        ,t.os
        ,t.tslen - (LAG(t.tslen) OVER(PARTITION BY t.install_source ORDER BY t.install_source, t.tslen)) as tsdiff 
    from
        (
        select
            initcap(split_part(a.tracker_name, '::', 1)) as install_source
            ,split_part(a.tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(a.tracker_name, '::', 4) as creative_id
            ,a.game as app_id
            ,c.country
            ,trunc(ts) as install_date
            ,case
                when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
                when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
                when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
             end as os
            ,(extract(epoch from a.ts) - extract(epoch from DATEADD(DAY, -90, current_date))) as tslen
        from
            unique_adjust a
            left join
            kpi_processed.dim_country c on a.country=lower(c.country_code)
        where
            a.tracker_name<>'Organic'
            and date(a.ts)>=DATEADD(DAY, -90, current_date)
            and split_part(a.tracker_name, '::', 1) != ''
        ) t
    order by t.install_source, t.tslen
    )
where
    tsdiff is not null
    and tsdiff <= 200;

-- Fraud with same device model in one day
drop table if exists kpi_processed.adjust_device_fraud;
create table kpi_processed.adjust_device_fraud as
select 
        t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.app_id
        ,t1.country
        ,t1.device_name
        ,t1.install_date
        ,t1.os
        ,t1.device_count as device_count
        ,t2.device_count as device_total
        ,t1.device_count*1.00/t2.device_count as device_percent
from
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,a.device_name
        ,trunc(ts) as install_date
        ,case
            when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
            when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
            when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
         end as os
        ,count(1) as device_count
    from
        unique_adjust a
        left join
        kpi_processed.dim_country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.device_name != ''
    group by 1,2,3,4,5,6,7,8,9
    having count(1) >= 10
    ) t1
    left join
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,trunc(ts) as install_date
        ,case
            when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
            when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
            when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
         end as os
        ,count(1) as device_count
    from
        unique_adjust a
        left join
        kpi_processed.dim_country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.device_name != ''
    group by 1,2,3,4,5,6,7,8
    having count(1) >= 10
    ) t2
    on
        t1.install_source = t2.install_source
        and t1.campaign = t2.campaign
        and t1.sub_publisher = t2.sub_publisher
        and t1.creative_id = t2.creative_id
        and t1.app_id = t2.app_id
        and t1.country = t2.country
        and t1.install_date = t2.install_date
        and t1.os = t2.os
order by device_percent desc;

-- Adjust interval between install and first action 
drop table if exists kpi_processed.adjust_first_action;
create table kpi_processed.adjust_first_action as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,trunc(a.ts) as install_date
    ,case
        when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,a.userid
    ,a.tslen
    ,count(1) as session_cnt
    ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    ,max(s.level_end) as level_end
from
    (select
        adj.tracker_name
        ,adj.game
        ,adj.ts
        ,adj.app_id
        ,adj.userid
        ,adj.country
        ,u.user_key
        ,case
            when u.all_max_level = 1 then null
            else (extract(epoch from u.ts_end) - extract(epoch from u.ts_start))
         end as tslen
    from
        unique_adjust adj 
        left join 
        (select
            *
            ,row_number () over (partition by user_id order by ts_start) as rank
            ,max(level_end) over (partition by user_id) as all_max_level
        from 
            kpi_processed.fact_session
        where
            date_start >= DATEADD(DAY, -8, current_date) and date_start < DATEADD(DAY, -1, current_date)
            and install_date >= DATEADD(DAY, -8, current_date) and install_date < DATEADD(DAY, -1, current_date)
        ) u on adj.userid = u.user_id and u.app_id = adj.game and trunc(adj.ts) = u.install_date
    where adj.tracker_name<>'Organic'
        and split_part(adj.tracker_name, '::', 1) != ''
        and date(adj.ts)>=DATEADD(DAY, -8, current_date)
        and date(adj.ts)<DATEADD(DAY, -1, current_date)
        and adj.userid != ''
        and adj.userid is not null
        and u.user_id is not null
        and u.rank = 1
    ) a
    left join
    kpi_processed.dim_country c on a.country=lower(c.country_code)
    left join
    kpi_processed.fact_dau_snapshot s on a.user_key = s.user_key and a.game = s.app_id
        and s.date >= DATEADD(DAY, -8, current_date) and s.date < DATEADD(DAY, -1, current_date)
group by 1,2,3,4,5,6,7,8,9,10
;

-- Adjust country fraud
drop table if exists kpi_processed.adjust_country_fraud;
create table kpi_processed.adjust_country_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
        when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
        when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,count(1) as fraud_count
    ,count(distinct a.userid) as distinct_fraud_count
from
    unique_adjust a
    left join
    kpi_processed.dim_country c on a.country=lower(c.country_code)
where 
    charindex('facebook', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('google', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('youtube', lower(split_part(a.tracker_name, '::', 1))) = 0
    and (
        (a.game = 'waf.global.prod' and c.country not in ('United Kingdom', 'Germany'))
        or (a.game = 'loe.global.prod' and c.country not in ('United States', 'United Kingdom', 'Saudi Arabia', 'Canada', 'Philippines', 'Singapore', 'Australia'))
        or (a.game = 'ffs.global.prod' and c.country not in ('United States', 'Russian Federation','Germany','France','United Kingdom','Korea, Republic of','Japan','Canada','Taiwan','Australia')
           and c.country not in ('Netherlands','Norway','Switzerland','Austria','Sweden','Denmark','Ireland','Spain','Italy','Hong Kong')
           and c.country not in ('New Zealand','Singapore'))
        )
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -8, current_date)
    and date(a.ts)<DATEADD(DAY, -1, current_date)
group by 1,2,3,4,5,6,7,8,9
;

-- Adjust fraud monitor
drop table if exists kpi_processed.adjust_fraud_monitor;
create table kpi_processed.adjust_fraud_monitor as
select
    t1.install_source
    ,t1.sub_publisher
    ,t1.app_id
    ,trunc(DATEADD(DAY, -8, current_date)) as install_date
    ,t1.install_count
    ,t2.d1_retained*1.00/t2.d1_new_installs as d1_retention
    ,t2.d7_retained*1.00/t2.d7_new_installs as d7_retention
    ,t3.level1_percent
    ,t4.discrepancy
    ,t5.country_fraud_count*1.00/t1.install_count as country_discrepancy
from
    (
    select
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,a.game as app_id
        ,count(1) as install_count
    from
        unique_adjust a
    where
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -8, current_date)
        and date(a.ts)<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    having count(1) > 10
    ) as t1
    left join
    (
    select
        initcap(install_source) as install_source
        ,case
            when install_source like 'Google Adwords%' then creative_id
            else sub_publisher
         end as sub_publisher
        ,app
        ,sum(d1_retained) as d1_retained
        ,case
            when sum(d1_new_installs) = 0 then null
            when sum(d1_new_installs) <> 0 then sum(d1_new_installs)
         end as d1_new_installs
        ,sum(d7_retained) as d7_retained
        ,case
            when sum(d7_new_installs) = 0 then null
            when sum(d7_new_installs) <> 0 then sum(d7_new_installs)
         end as d7_new_installs
    from
        kpi_processed.agg_marketing_kpi
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t2
    on
        t2.install_source = case when lower(replace(t1.install_source, '+' , ' ')) = 'off-facebook installs' then 'Facebook Installs'
                                 --when lower(replace(t1.install_source, '+' , ' ')) = 'applift' then 'AppLift'
                                 when lower(replace(t1.install_source, '+' , ' ')) = 'cheetah' then 'Cheetah Mobile'
                                 when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) in ('mars media group','marsmediagroup') then 'Mobilda'
                                 when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'app turbo' then 'Appturbo'
                                 --when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'aarki(incent)' then 'Aarki(incent)'
                                 --when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'glispa(incent)' then 'Glispa(incent)'
                                 when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'nativex (incent)' then 'Native(Incent)'
                                 --when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'sponsorpay(incent)' then 'Sponsorpay(incent)'
                                 --when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'organic' then 'Organic'
                                 --when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'ripple' then 'Ripple'
                                 when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'supersonic ads (incent)' then 'Supersonic(Incent)'
                                 when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) = 'adaction interactive' then 'Adaction'
                                 when t2.app like 'ffs%' and lower(replace(t1.install_source, '+' , ' ')) like 'google adwords%' then 'Google Adwords'
                                 when t2.app like 'crazyplanet%' and lower(replace(t1.install_source, '+' , ' ')) = 'CP-FF-FP' then 'Cp-Ffs-Fp'
                                 else t1.install_source
                             end
        and t1.sub_publisher = t2.sub_publisher
        and t1.app_id = t2.app
    left join
    (
    select
        initcap(f1.install_source) as install_source
        ,f1.sub_publisher
        ,f1.app_id
        ,f1.level1_count*1.00/f2.total as level1_percent
    from
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as level1_count
        from
            kpi_processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
            and tslen is null
            and level_end = 1
            and active_days >= 2
        group by 1,2,3
        ) as f1
        left join
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as total
        from
            kpi_processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
        group by 1,2,3
        ) as f2
        on
            f1.install_source = f2.install_source
            and f1.sub_publisher = f2.sub_publisher
            and f1.app_id = f2.app_id
    ) as t3
    on
        t1.install_source = t3.install_source
        and t1.sub_publisher = t3.sub_publisher
        and t1.app_id = t3.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,abs((sum(all_adjust_installs) - sum(in_bi_installs))*1.00/sum(all_adjust_installs)) as discrepancy
    from
        adjust_bi_discrepancy
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t4
    on
        t1.install_source = t4.install_source
        and t1.sub_publisher = t4.sub_publisher
        and t1.app_id = t4.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,sum(fraud_count) as country_fraud_count
    from
        kpi_processed.adjust_country_fraud
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t5
    on
        t1.install_source = t5.install_source
        and t1.sub_publisher = t5.sub_publisher
        and t1.app_id = t5.app_id
where
    ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.2 and lower(t1.install_source) not in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.1 and lower(t1.install_source) in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or (t2.d1_retained*1.00/t2.d1_new_installs) > 0.6
    or t3.level1_percent > 0.1
    or t4.discrepancy > 0.2
    or (t5.country_fraud_count*1.00/t1.install_count) > 0.1
;

-- Blacklist
drop table if exists kpi_processed.blacklist;
create table kpi_processed.blacklist as
select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,ip_address
    ,country
    ,null as device_name
    ,sum(fraud_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,cast(null as numeric) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    kpi_processed.adjust_ip_fraud
group by 1,2,3,4,5,6
having sum(fraud_count) > 10

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,country
    ,device_name
    ,sum(device_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,sum(device_count)*1.00/sum(device_total) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    kpi_processed.adjust_device_fraud
group by 1,2,3,4,5,6,7
having 
    sum(device_count) > 10
    and sum(device_count)*1.00/sum(device_total) > 0.2

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,null as country
    ,null as device_name
    ,install_count
    ,d1_retention
    ,d7_retention
    ,level1_percent
    ,discrepancy
    ,cast(null as numeric) as device_percent
    ,country_discrepancy
from
    kpi_processed.adjust_fraud_monitor
;

-- Training Data
drop table if exists kpi_processed.training_data;
create table kpi_processed.training_data as
select
    t1.id
    ,t1.userid
    ,t1.app_id
    ,t1.install_source
    ,t1.sub_publisher
    ,t1.install_date
    ,t1.ip_address
    ,t1.country
    ,t1.device_name
    ,t1.os
    ,t1.os_version
    ,t1.level_end
    ,t1.session_cnt
    ,t1.purchase_cnt
    ,t1.active_days
    ,max(case
        when t1.level_end = 1 and t1.active_days >= 5 then 1
        else 0
     end) as flag1
    ,max(case
        when t1.level_end = 1 and t1.ip_address = t2.ip_address then 1
        else 0
     end) as flag2
    ,max(case
        when t1.level_end = 1 and t1.os = 'Android' and t1.device_name = t2.device_name then 1
        else 0
     end) as flag3
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.d1_retention < 0.05 then 1
        else 0
     end) as flag4
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.level1_percent > 0.4 then 1
        else 0
     end) as flag5
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.discrepancy > 0.4 then 1
        else 0
     end) as flag6
    ,max(case
        when t1.level_end = 1 and t2.country_discrepancy > 0.1 
            and charindex('facebook', lower(t1.install_source)) = 0
            and charindex('google', lower(t1.install_source)) = 0
            and charindex('youtube', lower(t1.install_source)) = 0
            and (
                (t1.app_id = 'waf.global.prod' and t1.country not in ('United Kingdom', 'Germany'))
                or (t1.app_id = 'loe.global.prod' and t1.country not in ('United States', 'United Kingdom', 'Saudi Arabia', 'Canada', 'Philippines', 'Singapore', 'Australia'))
                or (t1.app_id = 'ffs.global.prod' and t1.country not in ('United States', 'Russian Federation','Germany','France','United Kingdom','Korea, Republic of','Japan','Canada','Taiwan','Australia')
                   and t1.country not in ('Netherlands','Norway','Switzerland','Austria','Sweden','Denmark','Ireland','Spain','Italy','Hong Kong')
                   and t1.country not in ('New Zealand','Singapore'))
                ) then 1
        else 0
     end) as flag7
from
    (
    select
        adj.id
        ,adj.userid
        ,adj.app_id
        ,adj.install_source
        ,adj.sub_publisher
        ,adj.install_date
        ,adj.ip_address
        ,adj.country
        ,adj.device_name
        ,adj.os
        ,adj.os_version
        ,max(s.level_end) as level_end
        ,sum(s.session_cnt) as session_cnt
        ,sum(s.payment_cnt) as purchase_cnt
        ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    from
        (select
            u.user_key
            ,a.id
            ,a.userid
            ,a.game as app_id
            ,initcap(split_part(a.tracker_name, '::', 1)) as install_source
            ,case
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
             end as sub_publisher
            ,trunc(a.ts) as install_date
            ,a.ip_address
            ,c.country
            ,a.device_name
            ,case
                when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar', 'com.diandian.valiantforce') then 'Android'
                when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350','1106352070') then 'iOS'
                when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
             end as os
            ,a.os_version
         from
            unique_adjust a
            left join 
            kpi_processed.dim_user u on a.userid = u.user_id and a.game = u.app_id and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
            left join
            kpi_processed.dim_country c on a.country=lower(c.country_code)
         where
            a.tracker_name <> 'Organic'
            and date(a.ts) >= DATEADD(DAY, -8, current_date)
            and date(a.ts) < DATEADD(DAY, -1, current_date)
        ) adj
        left join
        kpi_processed.fact_dau_snapshot s on adj.user_key = s.user_key and adj.app_id = s.app_id and s.date >= DATEADD(DAY, -8, current_date) and s.date < DATEADD(DAY, -1, current_date)
    group by 1,2,3,4,5,6,7,8,9,10,11
    ) t1
    left join
    (
    select
        *
    from
        kpi_processed.blacklist b
    where
        b.install_date < DATEADD(DAY, -1, current_date)
    ) t2
    on
        t1.app_id = t2.app_id
        and t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
;

-- Fraud install duplicate userid (Android ID)
create temp table adjust_duplicated_users as
select
    trunc(a.ts) as install_date
    ,a.app_id
    ,a.country
    ,a.game
    ,initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,a.userid
    ,a.android_id
    ,a.gaid
    ,a.device_name
    ,a.os_name
    ,a.os_version
    ,a.ip_address
    ,a.click_id
from
    unique_adjust a
where a.userid in (
    select userid
    from unique_adjust
    where userid != ''
    group by 1
    having count(1) >= 10
    )
    and initcap(split_part(a.tracker_name, '::', 1)) <> 'Organic'
    and trunc(a.ts) >= DATEADD(DAY, -90, current_date)
;


drop table if exists kpi_processed.fraud_channel;
create table kpi_processed.fraud_channel as
select
    c.install_date
    ,c.app_id
    ,c.country
    ,c.game
    ,c.install_source
    ,c.sub_publisher
    ,c.userid
    ,coalesce(c.android_id, u.android_id) as android_id
    ,c.gaid
    ,c.device_name
    ,c.os_name
    ,c.os_version
    ,c.ip_address
    ,c.click_id
from
    adjust_duplicated_users c
    left join
    (
    select
        distinct app_id
        ,user_id
        ,last_value(android_id ignore nulls)
         OVER (PARTITION BY app_id,user_id ORDER BY ts_pretty ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
         AS android_id
    from
        kpi_processed.session_start
    where
        trunc(ts_pretty) >= DATEADD(DAY, -90, current_date)
    ) u on c.userid = u.user_id and c.game = u.app_id
;


update adjust_bi_discrepancy set install_source = 'AppLift' where app_id like 'waf%' and lower(install_source) like 'applift%';
update adjust_bi_discrepancy set install_source = 'Cheetah Mobile' where app_id like 'waf%' and install_source = 'Cheetah';
update adjust_bi_discrepancy set install_source = 'Facebook Installs' where app_id like 'waf%' and install_source = 'Off-Facebook Installs';
update adjust_bi_discrepancy set install_source = 'Aarki(incent)' where install_source = 'AArki(Incent)';
update adjust_bi_discrepancy set install_source = 'Appia' where install_source = 'appia';
update adjust_bi_discrepancy set install_source = 'Applift' where install_source = 'Applift-social';
update adjust_bi_discrepancy set install_source = 'Crosspromotion' where install_source = 'crosspromotion';
update adjust_bi_discrepancy set install_source = 'Glispa' where install_source = 'GLISPA' or install_source = 'Glispa_Row';
update adjust_bi_discrepancy set install_source = 'Glispa(incent)' where install_source = 'Glispa(Incent)';
update adjust_bi_discrepancy set install_source = 'Google Adwords' where install_source = 'Google Adwords Mobile Display';
update adjust_bi_discrepancy set install_source = 'Mars Media Group' where install_source = 'MarsMediaGroup';
update adjust_bi_discrepancy set install_source = 'Mdotm' where install_source = 'MdotM';
update adjust_bi_discrepancy set install_source = 'MobVista' where install_source like 'MobVista_%';
update adjust_bi_discrepancy set install_source = 'NativeX(incent)' where install_source like 'NativeX%cent)';
update adjust_bi_discrepancy set install_source = 'Ripple' where install_source = 'RIpple';
update adjust_bi_discrepancy set install_source = 'Sponsorpay(incent)' where install_source = 'Sponsorpay(Incent)';
update adjust_bi_discrepancy set install_source = 'Supersonic(incent)' where install_source = 'Supersonic Ads (Incent)';
update adjust_bi_discrepancy set install_source = 'Taptica' where install_source = 'taptica';
update adjust_bi_discrepancy set install_source = 'CP-FFS-FP' where install_source = 'CP-FF-FP';

update kpi_processed.dim_user
    set install_source = 'Adjust Organic'
from adjust
where adjust.userid = kpi_processed.dim_user.user_id
    and adjust.game = 'crazyplanets.global.prod'
    and lower(adjust.tracker_name) = 'organic'
    and adjust.game = kpi_processed.dim_user.app_id
    and kpi_processed.dim_user.install_source = 'Organic';

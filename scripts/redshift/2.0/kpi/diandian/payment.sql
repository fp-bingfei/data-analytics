----------------------------------------------------------
--kpi_processed.payment
----------------------------------------------------------
delete from kpi_processed.payment
where ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
       --and app_id like '_game_%'
       ;
        
insert into kpi_processed.payment   
select    event_id::varchar(128)          
         ,data_version::varchar(10)      
         ,app_id::varchar(64)            
         ,ts               
         ,ts_pretty        
         ,event::varchar(64)             
         ,user_id::varchar(64)           
         ,session_id::varchar(128)       
         ,app_version::varchar(32)       
         ,gameserver_id::varchar(128)     
         ,case when lower(os) = 'ios' then 'iOS' when lower(os) = 'android' then 'Android' else os::varchar(32) end as os                
         ,regexp_substr(os_version, '[0-9]+\\.[0-9]+(\\.[0-9]+)?') as os_version       
         ,browser::varchar(32)           
         ,browser_version::varchar(64)   
         ,idfa::varchar(128)              
         ,idfv::varchar(128)              
         ,gaid::varchar(128)              
         ,android_id::varchar(128)        
         ,mac_address::varchar(128)       
         ,device::varchar(64)            
         ,ip::varchar(15)                
         ,country_code::varchar(10)      
         ,lang::varchar(8)             
         ,level            
         ,vip_level        
         ,facebook_id::varchar(128)       
         ,gender::varchar(16)            
         ,first_name::varchar(64)        
         ,last_name::varchar(64)         
         ,birthday         
         ,email::varchar(256)             
         ,googleplus_id::varchar(128)     
         ,gamecenter_id::varchar(128)     
         ,install_ts       
         ,install_source::varchar(1024)     
         ,payment_processor::varchar(128)   
         ,transaction_id::varchar(128)      
         ,currency::varchar(8)            
         ,iap_product_id::varchar(128)      
         ,iap_product_name::varchar(128)    
         ,iap_product_type::varchar(128)    
         ,amount::float
         ,scene::varchar(32)
         ,fb_source::varchar(1024)           
from 
(
   select    md5(app_id||event||user_id||json_extract_path_text(properties,'transaction_id')) as event_id 
            ,data_version 
            ,app_id 
            ,cast(ts as bigint) as ts 
            ,ts_pretty
            ,event 
            ,user_id 
            ,nvl(session_id,'') as session_id 
            ,json_extract_path_text(properties,'app_version') as app_version
            ,json_extract_path_text(properties,'gameserver_id') as gameserver_id 
            ,json_extract_path_text(properties,'os') as os
            ,json_extract_path_text(properties,'os_version') as os_version
            ,json_extract_path_text(properties,'browser') as browser 
            ,json_extract_path_text(properties,'browser_version') as browser_version 
            ,json_extract_path_text(properties,'idfa') as idfa 
            ,json_extract_path_text(properties,'idfv') as idfv 
            ,json_extract_path_text(properties,'gaid') as gaid 
            ,json_extract_path_text(properties,'android_id') as android_id
            ,json_extract_path_text(properties,'mac_address') as mac_address 
            ,json_extract_path_text(properties,'device') as device 
            ,json_extract_path_text(properties,'ip') as ip 
            ,json_extract_path_text(properties,'country_code') as country_code 
            ,json_extract_path_text(properties,'lang') as lang
            ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'level'), '[0-9]+'), '')::int, 0) as level 
            ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'vip_level'), '[0-9]+'), '')::int, 0) as vip_level
            ,json_extract_path_text(properties,'facebook_id') as facebook_id 
            ,json_extract_path_text(properties,'gender') as gender 
            ,json_extract_path_text(properties,'first_name') as first_name 
            ,json_extract_path_text(properties,'last_name') as last_name 
            ,case when json_extract_path_text(properties,'birthday') = '' then to_date('01/01/1900', 'MM/DD/YYYY')
                  when position('-' in json_extract_path_text(properties,'birthday')) > 0 then to_date(json_extract_path_text(properties,'birthday'), 'YYYY-MM-DD')
                  when position('-' in json_extract_path_text(properties,'birthday')) > 0 then to_date(json_extract_path_text(properties,'birthday'), 'MM/DD/YYYY')
                  else to_date('01/01/1900', 'MM/DD/YYYY') end as birthday
            ,json_extract_path_text(properties,'email') as email 
            ,json_extract_path_text(properties,'googleplus_id') as googleplus_id 
            ,json_extract_path_text(properties,'gamecenter_id') as gamecenter_id 
            ,nullif(json_extract_path_text(properties,'install_ts_pretty'),'')::timestamp as install_ts 
            ,json_extract_path_text(properties,'install_source')  as install_source 
            ,json_extract_path_text(properties,'payment_processor') as payment_processor
            ,json_extract_path_text(properties,'transaction_id') as transaction_id
            ,json_extract_path_text(properties,'currency') as  currency
            ,json_extract_path_text(properties,'iap_product_id') as iap_product_id
            ,json_extract_path_text(properties,'iap_product_name') as iap_product_name
            ,json_extract_path_text(properties,'iap_product_type') as iap_product_type
            ,CASE
                WHEN ((app_id LIKE 'farm%' and json_extract_path_text(properties,'payment_processor') <> 'Sponsorpay' AND json_extract_path_text(properties,'payment_processor') <> 'Supersonic')
                        OR app_id LIKE 'ffs%' OR app_id LIKE 'ha%' OR app_id LIKE 'poker%' OR app_id LIKE 'royal%' OR app_id LIKE 'waf%') THEN nvl(NULLIF(json_extract_path_text(properties,'amount'),'')::FLOAT,0)
                ELSE nvl(NULLIF(json_extract_path_text(properties,'amount'),'')::FLOAT,0)*100
             END AS amount
            ,json_extract_path_text(properties,'scene') as scene  
            ,json_extract_path_text(properties,'fb_source') as fb_source 
            ,row_number() over(partition by app_id, event_id) as rnum
   from  tmp_raw_events
   where event = 'payment'
   and json_extract_path_text(properties,'install_ts_pretty') <> ''
   --and app_id like '_game_%'
)t
where t.rnum = 1  
;  
 
-- FFS CN sends currencies as RMB and APT, which are not in our currency tables.
update kpi_processed.payment
set currency='CNY'
where currency='RMB';

update kpi_processed.payment
set currency='USD'
where currency='APT';

------ update scene field
update kpi_processed.payment set scene='Main' where (scene='1' or scene is null or scene='') and ts_pretty >=
       (
         select start_date
         from   kpi_processed.init_start_date
        )
       --and app_id like '_game_%'
       ;
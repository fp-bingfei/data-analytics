-- Update adjust due to a bug in android
update adjust
set userid = u.user_id
from adjust.raw_events_unique as u
where u.adid = adjust.adid
and adjust.game = 'ffs.global.prod'
and u.fp_app_id = 'ffs.global.prod'
and u.os_name = 'android'
and u.event = 'open';

--update fact_install_source
create temp table g as
select game, userid, md5(game||userid) as user_key, ts, tracker_name, 
		split_part(tracker_name, '::', 1) as install_source, 
		nullif(split_part(tracker_name, '::', 2),'') as install_campaign, 
		nullif(split_part(tracker_name, '::', 3),'') as install_subpublisher,
		nullif(split_part(tracker_name, '::', 4),'') as install_creative_id
from
	(SELECT *, row_number() over (partition by game, userid order by ts) as rnum 
	FROM public.adjust where lower(tracker_name)!='organic' and userid!=''
	and md5(game || userid) not in (select md5(game || userid) from kpi_processed.fact_install_source)
	and tracker_name!=''
	and game like 'ffs%'
	)
where rnum=1;

insert into kpi_processed.fact_install_source
select * from g;

--update dim_user table
update kpi_processed.dim_user
set install_source = f.install_source,
    install_campaign = f.install_campaign,
    install_subpublisher =  f.install_subpublisher,
    install_creative_id = f.install_creative_id
from 
	(select * from kpi_processed.fact_install_source 
		where user_key in (select user_key from kpi_processed.tmp_user_daily_login)
	) f
where kpi_processed.dim_user.user_key = f.user_key
	and lower(kpi_processed.dim_user.install_source) in ('organic','','mobile')
	and kpi_processed.dim_user.app_id like 'ffs%';



-----update dim_user for lc_patch
--
--update kpi_processed.dim_user
--set install_source = f.install_source,
--    install_campaign = f.install_campaign,
--    install_subpublisher =  f.install_subpublisher,
--    install_creative_id = f.install_creative_id
--from
--	(select * from kpi_processed.fact_install_source
--		where game like 'lc%' and user_key in (select user_key from kpi_processed.tmp_user_daily_login)
--	) f
--where kpi_processed.dim_user.facebook_id = f.userid
--	and lower(kpi_processed.dim_user.install_source) in ('organic','','mobile') and kpi_processed.dim_user.app_id like 'lc_patch%' and f.game like 'lc%';
--
---- Separate Adjust Organic from Organic
--update kpi_processed.dim_user
--	set install_source = 'Adjust Organic'
--from adjust
--where adjust.userid = kpi_processed.dim_user.user_id
--	and adjust.game = 'crazyplanets.global.prod'
--	and lower(adjust.tracker_name) = 'organic'
--	and adjust.game = kpi_processed.dim_user.app_id
--	and kpi_processed.dim_user.install_source = 'Organic';
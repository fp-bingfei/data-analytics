--ffs.session_start
CREATE TABLE if not exists ffs.session_start
(
	event_id VARCHAR(128) NOT NULL ENCODE lzo,
	data_version VARCHAR(10) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP NOT NULL ENCODE delta,
	event VARCHAR(64) NOT NULL ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	session_id VARCHAR(128) NOT NULL ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	gameserver_id VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(128) ENCODE lzo,
	browser VARCHAR(128) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	idfv VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	android_id VARCHAR(128) ENCODE lzo,
	mac_address VARCHAR(128) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	ip VARCHAR(15) ENCODE lzo,
	country_code VARCHAR(10) ENCODE lzo,
	lang VARCHAR(32) ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	facebook_id VARCHAR(128) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE lzo,
	googleplus_id VARCHAR(128) ENCODE lzo,
	gamecenter_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	scene VARCHAR(32),
	fb_source VARCHAR(1024)
)
SORTKEY
(
	app_id,
	user_id,
	country_code
);

delete from ffs.session_start 
where datediff(day, ts_pretty, sysdate)<=3;

insert into ffs.session_start
select * from kpi_processed.session_start
where app_id like 'ffs%' and datediff(day, ts_pretty, sysdate)<=3;

--ffs.agg_kpi
CREATE TABLE if not exists ffs.agg_kpi
(
	date DATE NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	install_source_group VARCHAR(1024) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	level_end INTEGER,
	browser VARCHAR(64) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_new_user INTEGER,
	is_payer INTEGER,
	new_user_cnt INTEGER,
	dau_cnt INTEGER,
	newpayer_cnt INTEGER,
	payer_today_cnt INTEGER,
	payment_cnt INTEGER,
	revenue_usd NUMERIC(14, 4),
	session_cnt INTEGER,
	playtime_sec BIGINT,
	scene VARCHAR(32) DEFAULT 'Main'::character varying,
	revenue_iap NUMERIC(14, 4),
	revenue_ads NUMERIC(14, 4)
)
SORTKEY
(
	app_id,
	install_source,
	is_payer
);

delete from ffs.agg_kpi 
where datediff(day, date, sysdate)<=3;

insert into ffs.agg_kpi
select * from kpi_processed.agg_kpi
where app_id like 'ffs%' and datediff(day, date, sysdate)<=3;

--ffs.agg_retention_ltv
CREATE TABLE if not exists ffs.agg_retention_ltv
(
	player_day INTEGER,
	app_id VARCHAR(64) ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	install_date DATE DISTKEY,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_creative_id VARCHAR(1) ENCODE lzo,
	device_alias VARCHAR(128) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	is_payer INTEGER,
	new_user_cnt BIGINT,
	retained_user_cnt BIGINT,
	cumulative_revenue_usd NUMERIC(38, 4),
	new_payer_cnt BIGINT
);

truncate table ffs.agg_retention_ltv;

insert into ffs.agg_retention_ltv
select * from kpi_processed.agg_retention_ltv
where app_id like 'ffs%';

--ffs.fact_revenue
CREATE TABLE if not exists ffs.fact_revenue
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	date DATE NOT NULL ENCODE delta,
	ts TIMESTAMP NOT NULL ENCODE delta,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	session_id VARCHAR(128) ENCODE lzo,
	level INTEGER ENCODE lzo,
	vip_level INTEGER ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	ip VARCHAR(32) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	payment_processor VARCHAR(128) ENCODE lzo,
	iap_product_id VARCHAR(128) ENCODE lzo,
	iap_product_name VARCHAR(128) ENCODE lzo,
	iap_product_type VARCHAR(128) ENCODE lzo,
	currency VARCHAR(8) NOT NULL ENCODE lzo,
	revenue_amount NUMERIC(14, 4),
	revenue_usd NUMERIC(14, 4),
	transaction_id VARCHAR(128) ENCODE lzo,
	scene VARCHAR(32),
	fb_source VARCHAR(1024)
)
SORTKEY
(
	date,
	user_key,
	app_id,
	user_id
);

delete from ffs.fact_revenue 
where datediff(day, date, sysdate)<=3;

insert into ffs.fact_revenue
select * from kpi_processed.fact_revenue
where app_id like 'ffs%' and datediff(day, date, sysdate)<=3;

--ffs.dim_user
CREATE TABLE if not exists ffs.dim_user
(
	id VARCHAR(128) NOT NULL ENCODE lzo,
	user_key VARCHAR(128) NOT NULL ENCODE lzo DISTKEY,
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	facebook_id VARCHAR(128) ENCODE lzo,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	install_source VARCHAR(1024) ENCODE lzo,
	install_subpublisher VARCHAR(1024) ENCODE lzo,
	install_campaign VARCHAR(512) ENCODE lzo,
	install_language VARCHAR(16) ENCODE lzo,
	install_country VARCHAR(64) ENCODE lzo,
	install_os VARCHAR(64) ENCODE lzo,
	install_device VARCHAR(128) ENCODE lzo,
	install_browser VARCHAR(64) ENCODE lzo,
	install_gender VARCHAR(20) ENCODE lzo,
	language VARCHAR(16) ENCODE lzo,
	birthday DATE ENCODE delta,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	gender VARCHAR(20) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(128) ENCODE lzo,
	browser VARCHAR(64) ENCODE lzo,
	browser_version VARCHAR(128) ENCODE lzo,
	last_ip VARCHAR(32) ENCODE lzo,
	level INTEGER,
	is_payer INTEGER,
	conversion_ts TIMESTAMP ENCODE delta,
	revenue_usd NUMERIC(14, 4),
	payment_cnt INTEGER,
	last_login_date DATE,
	install_source_group VARCHAR(1024),
	install_creative_id VARCHAR(500)
)
SORTKEY
(
	user_key,
	app_id,
	user_id
);

truncate table ffs.dim_user;

insert into ffs.dim_user
select * from kpi_processed.dim_user
where app_id like 'ffs%';
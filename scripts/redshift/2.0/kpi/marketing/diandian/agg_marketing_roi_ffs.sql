DELETE FROM ffs.agg_marketing_roi;
INSERT INTO ffs.agg_marketing_roi
WITH
clean_config as (
			select
				*,
				datediff('day', created_at, next_created_at) as diff
			from
				(select
					os,
					install_source,
					country,
					pricing_model,
					sub_publisher,
					campaign,
					cpa,
					created_at,
					coalesce(lead(created_at,1) over (partition by os, install_source, country, sub_publisher, campaign order by created_at),created_at) as next_created_at
				from ffs.roi_cost
				order by 1,2,3,5,6)),
install_else as (select distinct *
				 from clean_config c
				 where 	c.pricing_model = 'all'
				    	and install_source = 'else'),
country_else as (select distinct *
				 from clean_config c
				 where 	c.pricing_model = 'all'
				    	and install_source <> 'else'
						and c.country <> 'all'
						and c.campaign = 'all'
						and c.sub_publisher = 'all') ,
country_all as (select distinct *
				 from clean_config c
				 where 	c.pricing_model = 'all'
				    	and install_source <> 'else'
						and c.country = 'all'
						and c.campaign = 'all'
						and c.sub_publisher = 'all'),
campaign_else as (select distinct *
				 from clean_config c
				 where 	c.pricing_model = 'all'
				    	and install_source <> 'else'
						--and c.country = 'all'
						and c.campaign <> 'all'
						and c.sub_publisher = 'all'),

/* Raw Marketing Table */
raw_table as (select app as app_id,
			   install_source,
				coalesce((case when os like '%iOS%' then 'iPhone OS'
			               when os like '%android%' then 'Android'
			               else os end),'null') as os,
			   coalesce(country, 'null') as country,
			   coalesce(campaign, 'null') as campaign,
			   coalesce(sub_publisher, 'null') as sub_publisher
		 from kpi_processed.agg_marketing_kpi
		 where app = 'ffs.global.prod'
		 --and install_source not like '%Organic%'
		 and install_date >= '2016-01-01'
		 group by 1,2,3,4,5,6),

/* Install Source: Install Source Scenario (install_source = 'else') */
install_else_list as (select distinct c.install_source from ffs.roi_cost c where c.install_source <> 'else'),
install_else_mapping as (select
					r.*,
					e.cpa,
					e.created_at,
					e.next_created_at,
					e.diff
					from raw_table as r
					left join install_else e
					on e.os = r.os
					and e.country = r.country
					where e.country <> 'else'
					and r.install_source not in (select install_source from install_else_list)
					group by 1,2,3,4,5,6,7,8,9,10),
install_else_else as (select
					r.*,
					e.cpa,
					e.created_at,
					e.next_created_at,
					e.diff
					from raw_table as r
					left join install_else e
					on e.os = r.os
					where e.country = 'else'
					and r.install_source not in (select install_source from install_else_list)
					and r.country not in (select distinct country from install_else where country <> 'else')
					group by 1,2,3,4,5,6,7,8,9,10),

/* Install Source Mapping: Coutnry Scenario (r.country = c.country) */
install_country_else as (select
						r.*,
						c.cpa,
						c.created_at,
						c.next_created_at,
						c.diff
					from raw_table as  r
					left join country_else as c
					on c.os = r.os
					and c.install_source = r.install_source
					where c.country = 'else'
					and r.country not in (select c.country from country_else c where c.country <> 'else'
											and c.os = r.os and c.install_source = r.install_source)
					group by 1,2,3,4,5,6,7,8,9,10),

/* Install Source Mapping: Coutnry Scenario (country = 'else') */
install_country_mapping as (select
						r.*,
						c.cpa,
						c.created_at,
						c.next_created_at,
						c.diff
					from raw_table as r
					join country_else as c
					on c.os = r.os
					and c.install_source = r.install_source
					and c.country = r.country
					where c.country <> 'else'
					group by 1,2,3,4,5,6,7,8,9,10),

/* Install Source Mapping: Coutnry Scenario (country = 'all') */
install_country_all as (select
						r.*,
						ca.cpa,
						ca.created_at,
						ca.next_created_at,
						ca.diff
					from raw_table as r
					join country_all as ca
					on ca.os = r.os
					and ca.install_source = r.install_source
					group by 1,2,3,4,5,6,7,8,9,10),

/* Install Source Mapping: Campaign Scenario (campaign = 'else') */
install_campaign_else_else as (select
						  r.*,
						  c.cpa,
						  c.created_at,
						  c.next_created_at,
						  c.diff
					from raw_table as r
					left join campaign_else as c
					on r.os = c.os
					and r.install_source = c.install_source
					where c.campaign = 'else'
						and c.country='else'
						and r.campaign not in (select distinct campaign from campaign_else c
											where c.os = r.os and r.install_source = c.install_source
											and r.country = c.country
											and c.campaign <> 'else'
											and c.country <> 'else')
						and r.country not in (select distinct campaign from campaign_else c
											where c.os = r.os and r.install_source = c.install_source
											and r.country = c.country
											and c.campaign <> 'else'
											and c.country <> 'else')),
install_campaign_else as (select
						  r.*,
						  c.cpa,
						  c.created_at,
						  c.next_created_at,
						  c.diff
					from raw_table as r
					left join campaign_else as c
					on r.os = c.os
					and r.install_source = c.install_source
					and r.country = c.country
					where c.campaign = 'else'
						and c.country <> 'else'
						and r.campaign not in (select distinct campaign from campaign_else c
											where c.os = r.os and r.install_source = c.install_source
											and r.country = c.country
											and c.campaign <> 'else'
											and c.country <> 'else')),
/* Install Source Mapping: Campaign Scenario (campaign <> 'else') */
install_campaign_mapping as (select
						  r.*,
						  c.cpa,
						  c.created_at,
						  c.next_created_at,
						  c.diff
					from raw_table as r
					join campaign_else as c
					on r.os = c.os
					and r.install_source = c.install_source
					and r.country = c.country
					and r.campaign = c.campaign
					where c.campaign <> 'else' and c.country <> 'else'),
/* Install Source Mapping Aggregation */
install as (
select * from install_else_mapping
union all
select * from install_else_else
union all
select * from install_country_mapping
union all
select * from install_country_all
union all
select * from install_country_else
union
select * from install_campaign_else_else
union all
select * from install_campaign_else
union all
select * from install_campaign_mapping),

/* Raw Adjust Table */
adjust as (select
      fp_app_id as app_id,
      event,
      trunc(installed_at) as install_date,
      case os_name when 'android' then 'Android' when 'ios' then 'iPhone OS' else os_name end as os,
      c.country_name as country,
      coalesce(split_part(tracker_name, '::', 1),'null') as install_source,
      coalesce(split_part(tracker_name, '::', 2),'null') as campaign,
      coalesce((case when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1))
            not in ('google adwords', 'google adwords mobile', 'google adwords mobile display')
      then split_part(tracker_name, '::', 3)
      when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1))
            in ('google adwords', 'google adwords mobile', 'google adwords mobile display')
      then split_part(tracker_name, '::', 4)
      end),'null')
      as sub_publisher,
      count(distinct adid) as cnt
  from adjust.raw_events a
  join kpi_processed.dim_country_complete c
  on c.alpha_2_code = UPPER(a.country)
  where fp_app_id = 'ffs.global.prod' and trunc(installed_at)>= '2016-11-10'
  group by 1,2,3,4,5,6,7,8),
/* Adjust Old System Data */
adjust_old as (
	select
		game as app_id,
		'install' as event,
		trunc(ts) as install_date,
		case os_name when 'android' then 'Android' when 'ios' then 'iPhone OS' else os_name end as os,
		c.country_name as country,
		coalesce(split_part(tracker_name, '::', 1),'null') as install_source,
		coalesce(split_part(tracker_name, '::', 2),'null') as campaign,
		coalesce((case when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1))
		    not in ('google adwords', 'google adwords mobile', 'google adwords mobile display')
		then split_part(tracker_name, '::', 3)
		when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1))
		    in ('google adwords', 'google adwords mobile', 'google adwords mobile display')
		then split_part(tracker_name, '::', 4)
		end),'null')
		as sub_publisher,
		count(distinct adid) as cnt
	from public.adjust a
	join kpi_processed.dim_country_complete c
	on c.alpha_2_code = UPPER(a.country)
	where game = 'ffs.global.prod' and (trunc(ts) between '2016-01-01' and '2016-11-09')
	group by 1,2,3,4,5,6,7,8),
/* Adjust Install Mapping */
/* Adjust Install Mapping: Raw Adjust Install Table */
adjust_install_raw as (
      select
          app_id,
          install_date,
          coalesce(os,'null') as os,
          case when country = '' then 'null' else country end as country,
          coalesce(install_source,'null') as install_source,
          case when campaign = '' then 'null' else campaign end as campaign,
          case when sub_publisher = '' then 'null' else sub_publisher end as sub_publisher,
          sum(cnt) as adjust_install
       from (select * from adjust where event='install'
      		union all
      		select * from adjust_old) as a
      group by 1,2,3,4,5,6,7),
/* Adjust Install Mapping: Mapping CPA */
adjust_install_agg as (
      select
          a.*,
          i.cpa,
          a.adjust_install*i.cpa as cost
      from adjust_install_raw a
      left join install i
      on i.app_id = a.app_id
      and i.os = a.os
      and i.install_source = a.install_source
      and i.country = a.country
      and i.campaign = a.campaign
      and i.sub_publisher = a.sub_publisher
      ),
/* AGG Marketing KPI Table */
kpi as (select
      app as app_id,
      install_date,
      install_source_group,
      coalesce((case when os like '%iOS%' then 'iPhone OS'
			               when os like '%android%' then 'Android'
			               else os end),'null') as os,
      install_source,
      coalesce(country,'null') as country,
      coalesce(sub_publisher,'null') as sub_publisher,
      coalesce(campaign,'null') as campaign,
      sum(new_installs) as new_installs,
      sum(d1_new_installs) as d1_new_installs,
      sum(d7_new_installs) as d7_new_installs,
      sum(d15_new_installs) as d15_new_installs,
      sum(d30_new_installs) as d30_new_installs,
      sum(d60_new_installs)  as d60_new_installs,
      sum(d90_new_installs) as d90_new_installs,
      sum(d120_new_installs) as d120_new_installs,
      sum(d150_new_installs) as d150_new_installs,
      sum(revenue)   as revenue,
      sum(d1_revenue)  as d1_revenue,
      sum(d7_revenue) as d7_revenue,
      sum(d15_revenue) as d15_revenue,
      sum(d30_revenue) as d30_revenue,
      sum(d60_revenue) as d60_revenue,
      sum(d90_revenue) as d90_revenue,
      sum(d120_revenue) as d120_revenue,
      sum(d150_revenue) as d150_revenue,
      sum(payers)  as payers,
      sum(d1_payers)  as d1_payers,
      sum(d7_payers) as d7_payers,
      sum(d15_payers) as d15_payers,
      sum(d30_payers) as d30_payers,
      sum(d60_payers) as d60_payers,
      sum(d90_payers) as d90_payers,
      sum(d120_payers) as d120_payers,
      sum(d150_payers) as d150_payers,
      sum(d1_retained) as d1_retained,
      sum(d7_retained)  as d7_retained,
      sum(d15_retained)  as d15_retained,
      sum(d30_retained) as d30_retained,
      sum(d60_retained) as d60_retained,
      sum(d90_retained)  as d90_retained,
      sum(d120_retained)   as d120_retained,
      sum(d150_retained)   as d150_retained,
      sum(d3_new_installs)  as d3_new_installs,
      sum(d3_revenue) as d3_revenue,
      sum(d3_payers)  as d3_payers,
      sum(d3_retained) as d3_retained
    from kpi_processed.agg_marketing_kpi
    where app like 'ffs%'
    group by 1,2,3,4,5,6,7,8),

/* ROI For Install Event */
/* For CPI Not Changed */
install_roi_nodiff as (
		select
			k.*,
			i.cpa
		from kpi k
		join (select * from install where diff=0) i
		on k.app_id = i.app_id
		and k.install_date >= i.created_at
		and k.os = i.os
		and k.install_source = i.install_source
		and k.country = i.country
		and k.campaign = i.campaign
		and k.sub_publisher = i.sub_publisher),
/* For CPI Changed */
install_roi_diff as (
		select
			k.*,
			i.cpa
		from kpi k
		join (select * from install where diff <> 0) i
		on k.app_id = i.app_id
		and (k.install_date >= i.created_at and k.install_date < i.next_created_at)
		and k.os = i.os
		and k.install_source = i.install_source
		and k.country = i.country
		and k.campaign = i.campaign
		and k.sub_publisher = i.sub_publisher),
install_roi as (
        select
            *
        from install_roi_nodiff
        union all
        select
            *
        from install_roi_diff),
/* ROI For Adust Install Event */
adjust_install_roi as (
    select
      i.*,
      0 as tutorial_count,
      coalesce(a.adjust_install,0) as adjust_install,
      'Adjust Install' as pricing_model
    from install_roi i
    left join adjust_install_raw a
    on i.app_id = a.app_id
    and i.install_date = a.install_date
    and i.os = a.os
    and i.install_source = a.install_source
    and i.country = a.country
    and i.campaign = a.campaign
    and i.sub_publisher = a.sub_publisher),
/* ROI For Singular Install */
singular_roi as (
    select
      app as app_id,
      install_date,
      install_source_group,
      coalesce((case when os like '%iOS%' then 'iPhone OS'
			               when os like '%android%' then 'Android'
			               else os end),'null') as os,
      install_source,
      country,
      'null' as sub_publisher,
      'null' as campaign,
      sum(bi_installs) as new_installs,
      sum(d1_new_installs) as d1_new_installs,
      sum(d7_new_installs) as d7_new_installs,
      sum(d15_new_installs) as d15_new_installs,
      sum(d30_new_installs) as d30_new_installs,
      sum(d60_new_installs)  as d60_new_installs,
      sum(d90_new_installs) as d90_new_installs,
      sum(d120_new_installs) as d120_new_installs,
      sum(d150_new_installs) as d150_new_installs,
      sum(revenue)   as revenue,
      sum(d1_revenue)  as d1_revenue,
      sum(d7_revenue) as d7_revenue,
      sum(d15_revenue) as d15_revenue,
      sum(d30_revenue) as d30_revenue,
      sum(d60_revenue) as d60_revenue,
      sum(d90_revenue) as d90_revenue,
      sum(d120_revenue) as d120_revenue,
      sum(d150_revenue) as d150_revenue,
      sum(payers)  as payers,
      sum(d1_payers)  as d1_payers,
      sum(d7_payers) as d7_payers,
      sum(d15_payers) as d15_payers,
      sum(d30_payers) as d30_payers,
      sum(d60_payers) as d60_payers,
      sum(d90_payers) as d90_payers,
      sum(d120_payers) as d120_payers,
      sum(d150_payers) as d150_payers,
      sum(d1_retained) as d1_retained,
      sum(d7_retained)  as d7_retained,
      sum(d15_retained)  as d15_retained,
      sum(d30_retained) as d30_retained,
      sum(d60_retained) as d60_retained,
      sum(d90_retained)  as d90_retained,
      sum(d120_retained)   as d120_retained,
      sum(d150_retained)   as d150_retained,
      sum(d3_new_installs)  as d3_new_installs,
      sum(d3_revenue) as d3_revenue,
      sum(d3_payers)  as d3_payers,
      sum(d3_retained) as d3_retained,
      sum(cost)/(case when sum(singular_installs)=0 then null else sum(singular_installs) end) as cpa,
      0 as tutorial_count,
      sum(singular_installs) as adjust_install,
      'Singular Install' as pricing_model
    from kpi_processed.agg_marketing_kpi_singular
    where app = 'ffs.global.prod'
    and install_source in ('Facebook Installs', 'Google Adwords', 'Google search')
    group by 1,2,3,4,5,6,7,8)
select * from adjust_install_roi where install_source not in ('Facebook Installs', 'Google Adwords', 'Google search')
union all
select * from singular_roi;
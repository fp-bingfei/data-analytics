DELETE FROM dragonwar.agg_marketing_roi;
INSERT INTO dragonwar.agg_marketing_roi
WITH
install_else as (select distinct app_id, os, install_source, country, cpa
				 from dragonwar.roi_cost c
				 where 	c.pricing_model = 'all'
				    	and install_source = 'else'),
country_else as (select distinct app_id, os, install_source, country, cpa
				 from dragonwar.roi_cost c
				 where 	c.pricing_model = 'all'
				    	and install_source <> 'else'
						and c.country <> 'all'
						and c.campaign = 'all'
						and c.sub_publisher = 'all') ,
country_all as (select distinct app_id, os, install_source, cpa from dragonwar.roi_cost c
																			where c.pricing_model = 'all'
																			and install_source <> 'else'
																			and c.country = 'all'
																			and c.campaign = 'all'
																			and c.sub_publisher = 'all'),
campaign_else as (select distinct app_id, os, install_source, country, campaign, cpa
				 from dragonwar.roi_cost c
				 where 	c.pricing_model = 'all'
				    	and install_source <> 'else'
						and c.country <> 'all'
						and c.campaign <> 'all'
						and c.sub_publisher = 'all'),

/* Raw Marketing Table */
raw_table as (select app as app_id,
			   install_source,
			   coalesce((case when lower(os) = 'ios' then 'iPhone OS' else os end),'null') as os,
			   coalesce(country, 'null') as country,
			   coalesce(campaign, 'null') as campaign,
			   coalesce(sub_publisher, 'null') as sub_publisher
		 from kpi_processed.agg_marketing_kpi
		 where app = 'koa.global.prod' and install_source not like '%Organic%'
		 group by 1,2,3,4,5,6),

/* Install Source: Install Source Scenario (install_source = 'else') */
install_else_list as (select distinct c.install_source from dragonwar.roi_cost c where c.install_source <> 'else'),
install_else_mapping as (select
					r.*,
					e.cpa
					from raw_table as r
					left join install_else e
					on e.os = r.os
					and e.country = r.country
					where e.country <> 'else'
					and r.install_source not in (select install_source from install_else_list)
					group by 1,2,3,4,5,6,7),
install_else_else as (select
					r.*,
					e.cpa
					from raw_table as r
					left join install_else e
					on e.os = r.os
					--and e.country = r.country
					where e.country = 'else'
					and r.install_source not in (select install_source from install_else_list)
					and r.country not in (select distinct country from install_else where country <> 'else')
					group by 1,2,3,4,5,6,7),

/* Install Source Mapping: Coutnry Scenario (r.country = c.country) */
install_country_else as (select
						r.*,
						c.cpa
					from raw_table as  r
					left join country_else as c
					on c.os = r.os
					and c.install_source = r.install_source
					where c.country = 'else'
					and r.country not in (select c.country from country_else c where c.country <> 'else'
											and c.os = r.os and c.install_source = r.install_source)
					group by 1,2,3,4,5,6,7),

/* Install Source Mapping: Coutnry Scenario (country = 'else') */
install_country_mapping as (select
						r.*,
						c.cpa
					from raw_table as r
					join country_else as c
					on c.os = r.os
					and c.install_source = r.install_source
					and c.country = r.country
					where c.country <> 'else'
					group by 1,2,3,4,5,6,7),

/* Install Source Mapping: Coutnry Scenario (country = 'all') */
install_country_all as (select
						r.*,
						ca.cpa
					from raw_table as r
					join country_all as ca
					on ca.os = r.os
					and ca.install_source = r.install_source
					group by 1,2,3,4,5,6,7),

/* Install Source Mapping: Campaign Scenario (campaign = 'else') */
install_campaign_else as (select
						  r.*,
						  c.cpa
					from raw_table as r
					left join campaign_else as c
					on r.os = c.os
					and r.install_source = c.install_source
					and r.country = c.country
					where c.campaign = 'else'
					and r.campaign not in (select campaign from campaign_else c
											where c.os = r.os and r.install_source = c.install_source
											and r.country = c.country and c.campaign <> 'else')
					),
/* Install Source Mapping: Campaign Scenario (campaign <> 'else') */
install_campaign_mapping as (select
						  r.*,
						  c.cpa
					from raw_table as r
					join campaign_else as c
					on r.os = c.os
					and r.install_source = c.install_source
					and r.country = c.country
					and r.campaign = c.campaign
					where c.campaign <> 'else')	,
/* Install Source Mapping Aggregation */
install as (
select * from install_else_mapping
union all
select * from install_else_else
union all
select * from install_country_mapping
union all
select * from install_country_all
union all
select * from install_country_else
union
select * from install_campaign_else
union all
select * from install_campaign_mapping),

/* Tutorial Mapping: Raw Tutorial Cost Table */
tut_raw_cost as (select
			fp_app_id as app_id,
			trunc(installed_at) as install_date,
			case os_name when 'android' then 'Android' when 'ios' then 'iPhone OS' else os_name end as os,
			c.country_name as country,
			split_part(tracker_name, '::', 1) as install_source,
			split_part(tracker_name, '::', 2) as campaign,
			case when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1))
						not in ('google adwords', 'google adwords mobile', 'google adwords mobile display')
			then split_part(tracker_name, '::', 3)
			when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1))
						in ('google adwords', 'google adwords mobile', 'google adwords mobile display')
			then split_part(tracker_name, '::', 4) end
			as sub_publisher,
			count(distinct adid) as tutorial_count
	from adjust.raw_events a
	join kpi_processed.dim_country_complete c
	on c.alpha_2_code = UPPER(a.country)
	where event = 'tutorial' and fp_app_id = 'koa.global.prod'
	group by 1,2,3,4,5,6,7),
/* Tutorial Mapping: Raw Tutorial Table */
tut_raw_table as (
			select
		          app_id,
		          os,
		          install_source,
		          country,
		          campaign,
		          sub_publisher
		     from tut_raw_cost
		     group by 1,2,3,4,5,6),
/* Tutorial Mapping: Config */
tut_config as (
		select
			distinct app_id,
			os,
			install_source,
			country,
			cpa
 		from dragonwar.roi_cost c
 		where c.pricing_model = 'Tutorial' and country <> 'else'),
/* Tutorial Mapping */
tut_cost as (
		select
			r.*,
			c.cpa
		from tut_raw_table as r
		join tut_config c
		on r.app_id = c.app_id
		and r.install_source = c.install_source
		and r.country = c.country
		group by 1,2,3,4,5,6,7),
tut_agg as (
		select
			r.*,
			c.cpa
		from tut_raw_cost as r
		join tut_cost as c
		on r.app_id = c.app_id
		and r.os = c.os
		and r.install_source = c.install_source
		and r.country = c.country
		and r.sub_publisher = c.sub_publisher
		and r.campaign = c.campaign
		),
/* AGG Marketing KPI Table */
kpi as (select
			app as app_id,
			install_date,
			install_source_group,
			coalesce((case when lower(os) = 'ios' then 'iPhone OS' else os end),'null') as os,
			install_source,
			coalesce(country,'null') as country,
			coalesce(sub_publisher,'null') as sub_publisher,
			coalesce(campaign,'null') as campaign,
			sum(new_installs) as new_installs,
			sum(revenue)   as revenue,
			sum(d1_revenue)  as d1_revenue,
			sum(d7_revenue) as d7_revenue,
			sum(d15_revenue) as d15_revenue,
			sum(d30_revenue) as d30_revenue,
			sum(d60_revenue) as d60_revenue,
			sum(d90_revenue) as d90_revenue,
			sum(d120_revenue) as d120_revenue,
			sum(d150_revenue) as d150_revenue
		from kpi_processed.agg_marketing_kpi
		where app like 'koa%'
		group by 1,2,3,4,5,6,7,8),
/* ROI For Install Event */
install_roi as (
		select
			k.*,
			0 as tutorial_count,
			'install' as pricing_model,
			i.cpa,
			i.cpa*k.new_installs as cost
		from kpi k
		join install i
		on k.app_id = i.app_id
		and k.os = i.os
		and k.install_source = i.install_source
		and k.country = i.country
		and k.campaign = i.campaign
		and k.sub_publisher = i.sub_publisher),
/* ROI For Tutorial Event */
tutorial_roi as (
		select
			k.*,
			t.tutorial_count,
			'tutorial' as pricing_model,
			t.cpa,
			t.cpa*t.tutorial_count as cost
		from kpi k
		join tut_agg t
		on  k.install_date = t.install_date
		and k.app_id = t.app_id
		and k.os = t.os
		and k.install_source = t.install_source
		and k.country = t.country
		and k.campaign = t.campaign
		and k.sub_publisher = t.sub_publisher
)
select * from install_roi
union all
select * from tutorial_roi;
drop table dragonwar.fraud_install_test;
create table dragonwar.fraud_install_test as
With
unique_adjust as (
    select
        distinct
        trunc(DATEADD(DAY, -8, current_date)) as install_date
        ,fp_app_id as app_id
        ,split_part(tracker_name, '::', 1) as install_source
        ,case
            when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
            when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
        end as sub_publisher
        ,ip_address
        ,adid
        ,country
        ,conversion_duration
        ,time_spent
    from adjust.raw_events
    where
    fp_app_id = 'koa.global.prod'
    and trunc(installed_at) between DATEADD(DAY, -8, current_date) and DATEADD(DAY, -1, current_date)
    and event = 'open'
    and split_part(tracker_name, '::', 1) <> 'Organic'
    )
/* Install Count */
,install_count as (
    select
        app_id
        ,install_source
        ,sub_publisher
        ,count(distinct adid) as install_count
    from unique_adjust
    group by 1,2,3
    having count(distinct adid) >= 50
    )
/* Retention Fraud */
/* Retention Calculation */
,ret as (
    select
        initcap(install_source) as install_source
        ,case
            when install_source like 'Google Adwords%' then creative_id
            else sub_publisher
         end as sub_publisher
        ,app as app_id
        ,sum(d1_retained)*1.00/(case when sum(d1_new_installs) = 0 then null
                            else sum(d1_new_installs) end )
         as d1_retention
        ,sum(d7_retained)*1.00/(case when sum(d7_new_installs) = 0 then null
                            else sum(d7_new_installs) end )
         as d7_retention
    from
        kpi_processed.agg_marketing_kpi
    where
        app = 'koa.global.prod'
        and install_date between DATEADD(DAY, -8, current_date) and DATEADD(DAY, -1, current_date)
    group by 1,2,3
    )
/* Retention Fraud */
,ret_fraud as (
	select
		*
	from ret
	where (d1_retention >= 0.45 and d1_retention is not null)
	or (d7_retention <= 0.05 and d7_retention is not null)
	)
/* IP Fraud: Installs with same IP > 100 */
,same_ip_tmp as (
    select
        app_id
        ,install_source
        ,sub_publisher
        ,ip_address
        ,count(1) as cnt
    from unique_adjust
    group by 1,2,3,4
    having count(1) > 100
    )
,same_ip as (
	select
		app_id
		,install_source
		,sub_publisher
		,count(ip_address) as ip_rep_count
	from same_ip_tmp
	group by 1,2,3
	)
/* Conversion Time Fraud */
,convert_time as (
    select
        app_id
        ,install_source
        ,sub_publisher
        ,count(distinct adid) as convert_fraud_count
    from unique_adjust
    where conversion_duration < 30
    group by 1,2,3
    having count(distinct adid) >= 50
    )
--/* Time Spent */
--,time_spent as (
--    select
--        app_id
--        ,install_source
--        ,sub_publisher
--        ,count(distinct adid) as ts_fraud_count
--    from unique_adjust
--    where time_spent < 60
--    group by 1,2,3
--    having count(distinct adid) >= 50
--    )
/* Device to 3 user id*/
,fraud_device_tmp as (
    select
        app_id
        ,install_source
        ,install_subpublisher as sub_publisher
        ,device_id
        ,count(distinct user_key) as cnt
    from kpi_processed.dim_user_by_device
    where
    app_id = 'koa.global.prod'
    and install_date between DATEADD(DAY, -8, current_date) and DATEADD(DAY, -1, current_date)
    and install_source not in ('Organic','Adjust Organic')
    and device_id <> ''
    group by 1,2,3,4
    having count(distinct user_key) > 3
    )
,fraud_device as (
	select
		app_id
		,install_source
		,sub_publisher
		,count(device_id) as device_fraud_count
	from fraud_device_tmp
	group by 1,2,3
	)
/* Level 1 Fraud */
,level_fraud as (
	select
        initcap(f1.install_source) as install_source
        ,f1.sub_publisher
        ,f1.app_id
        ,f1.level1_count*1.00/f2.total as level1_percent
    from
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as level1_count
        from
            kpi_processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
            and tslen is null
            and level_end = 1
            and active_days >= 2
        group by 1,2,3
        ) as f1
        left join
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as total
        from
            kpi_processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
        group by 1,2,3
        ) as f2
    	on  f1.install_source = f2.install_source
        and f1.sub_publisher = f2.sub_publisher
        and f1.app_id = f2.app_id
        where f1.level1_count*1.00/f2.total > 0.1
            )
,install_discrepancy as (
	select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,abs((sum(all_adjust_installs) - sum(in_bi_installs))*1.00/sum(all_adjust_installs)) as discrepancy
    from
        adjust_bi_discrepancy
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    having abs((sum(all_adjust_installs) - sum(in_bi_installs))*1.00/sum(all_adjust_installs)) >0.2
	)
,country_fraud_tmp as (
	select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,sum(fraud_count) as country_fraud_count
    from
        kpi_processed.adjust_country_fraud
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
	)
,country_fraud as (
	select
		i.install_source
		,i.sub_publisher
		,i.app_id
		,c.country_fraud_count*1.00/i.install_count as country_fraud_percent
	from install_count i
	left join country_fraud_tmp c
	on i.install_source = c.install_source
	and i.sub_publisher = c.sub_publisher
	and i.app_id = c.app_id
	where c.country_fraud_count*1.00/i.install_count > 0.1)
---------- Main Query ----------
select
		distinct i.*,
		trunc(DATEADD(DAY, -8, current_date)) as install_date,
		r.d1_retention as d1_retention,
		r.d7_retention as d7_retention,
		coalesce(s.ip_rep_count,0) as ip_rep_count,
		coalesce(c.convert_fraud_count,0) as convert_fraud_count,
		--coalesce(t.ts_fraud_count,0) as ts_fraud_count,
		coalesce(f.device_fraud_count,0) as device_fraud_count,
		l.level1_percent,
		d.discrepancy as install_discrepancy,
		cf.country_fraud_percent,
		case when r.d1_retention > 0.45 then 1 else 0 end as if_d1_retention,
		case when r.d7_retention < 0.05 then 1 else 0 end as if_d7_retention,
		case when s.ip_rep_count is null then 0 else 1 end as if_ip_replicate,
		case when c.convert_fraud_count is null then 0 else 1 end as if_convert_fraud,
		--case when t.ts_fraud_count is null then 0 else 1 end as if_ts_fraud,
		case when f.device_fraud_count is null then 0 else 1 end as if_device_fraud,
		case when l.level1_percent is null then 0 else 1 end as if_level1_fraud,
		case when d.discrepancy is null then 0 else 1 end as if_install_discrepancy,
		case when cf.country_fraud_percent is null then 0 else 1 end as if_country_fraud
	from install_count i
	left join ret_fraud as r
	on i.app_id = r.app_id
	and i.install_source = r.install_source
	and i.sub_publisher = r.sub_publisher
	left join same_ip s
	on i.app_id = s.app_id
	and i.install_source = s.install_source
	and i.sub_publisher = s.sub_publisher
	left join convert_time c
	on i.app_id = c.app_id
	and i.install_source = c.install_source
	and i.sub_publisher = c.sub_publisher
--	left join time_spent t
--	on i.app_id = t.app_id
--	and i.install_source = t.install_source
--	and i.sub_publisher = t.sub_publisher
	left join fraud_device f
	on i.app_id = f.app_id
	and i.install_source = f.install_source
	and i.sub_publisher = f.sub_publisher
	left join level_fraud l
	on i.app_id = l.app_id
	and i.install_source = l.install_source
	and i.sub_publisher = l.sub_publisher
	left join install_discrepancy d
	on i.app_id = d.app_id
	and i.install_source = d.install_source
	and i.sub_publisher = d.sub_publisher
	left join country_fraud cf
	on i.app_id = cf.app_id
	and i.install_source = cf.install_source
	and i.sub_publisher = cf.sub_publisher
	where i.sub_publisher <> ''
;

drop table dragonwar.fraud_install_summary;
create table dragonwar.fraud_install_summary as
select
    install_source,
    sub_publisher,
    install_date,
    sum(if_d1_retention+if_d7_retention+if_ip_replicate+if_convert_fraud+if_device_fraud+if_level1_fraud+if_install_discrepancy+if_country_fraud)*1.00/8 as fraud_prob,
    sum(install_count) as install_count,
    sum(d1_retention) as d1_retention,
    sum(d7_retention) as d7_retention,
    sum(ip_rep_count) as ip_rep_count,
    sum(convert_fraud_count) as convert_fraud_count,
    --sum(ts_fraud_count) as ts_fraud_count,
    sum(device_fraud_count) as device_fraud_count,
    sum(level1_percent) as level1_percent,
    sum(install_discrepancy) as install_discrepancy,
    sum(country_fraud_percent) as country_fraud_percent
from dragonwar.fraud_install_test
group by 1,2,3;

drop table dragonwar.fraud_by_install;
create table dragonwar.fraud_by_install as
with detail as (
select
    install_source,
    count(sub_publisher) as fraud_sub_publisher_count,
    sum(install_count) as total_install_count,
    sum(if_d1_retention)*1.00/count(sub_publisher) as d1_retention_fraud_percent,
    sum(if_d7_retention)*1.00/count(sub_publisher) as d7_retention_fraud_percent,
    sum(if_ip_replicate)*1.00/count(sub_publisher) as ip_replicate_fraud_percent,
    sum(if_convert_fraud)*1.00/count(sub_publisher) as convert_fraud_percent,
    --sum(if_ts_fraud)*1.00/count(sub_publisher) as ts_fraud_percent,
    sum(if_device_fraud)*1.00/count(sub_publisher) as device_fraud_percent,
    sum(if_level1_fraud)*1.00/count(sub_publisher) as level1_fraud_percent,
    sum(if_install_discrepancy)*1.00/count(sub_publisher) as install_discrepancy_percent,
    sum(if_country_fraud)*1.00/count(sub_publisher) as country_discrepancy_percent
from dragonwar.fraud_install_test t
where install_source not in ('Helium','Facebook Installs')
group by 1),
summary as (select
    install_source,
    install_date,
    avg(fraud_prob) as avg_fraud_prob,
    max(fraud_prob) as max_fraud_prob
from dragonwar.fraud_install_summary
where fraud_prob>0
and install_source not in ('Facebook Installs','Helium' )
group by 1,2),
unique_adjust as (
    select
        distinct fp_app_id as app_id
        ,split_part(tracker_name, '::', 1) as install_source
        ,case
            when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
            when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
        end as sub_publisher
    from adjust.raw_events
    where
    fp_app_id = 'koa.global.prod'
    and trunc(installed_at) between DATEADD(DAY, -8, current_date) and DATEADD(DAY, -1, current_date)
    and event = 'open'
    and split_part(tracker_name, '::', 1) <> 'Organic'
),
sub_pub as (
	select
		install_source,
		count(distinct sub_publisher) as total_subpub
	from unique_adjust
	group by 1)
select
    d.install_source,
    d.fraud_sub_publisher_count*1.00/p.total_subpub as fraud_subpub_percent,
    d.total_install_count,
    d.d1_retention_fraud_percent,
    d.d7_retention_fraud_percent,
    d.ip_replicate_fraud_percent,
    d.convert_fraud_percent,
    d.device_fraud_percent,
    d.level1_fraud_percent,
    d.install_discrepancy_percent,
    d.country_discrepancy_percent,
    s.install_date,
    s.avg_fraud_prob,
    s.max_fraud_prob
from detail d
join summary s
on d.install_source = s.install_source
join sub_pub p
on d.install_source = p.install_source;
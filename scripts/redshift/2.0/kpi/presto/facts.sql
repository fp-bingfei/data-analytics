------------------------------------------------
--Data 2.0 fact_session.sql
------------------------------------------------
--fact_session
delete from farm_pm.fact_session
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.fact_session
select     to_hex(md5(to_utf8(s.app_id||s.user_id||s.session_id))) as id
          ,s.app_id 
          ,s.app_version
          ,to_hex(md5(to_utf8(s.app_id||s.user_id))) as user_key
          ,s.user_id
          ,to_iso8601(date(s.ts_pretty)) as date_start
          ,to_iso8601(date(e.ts_pretty)) as date_end
          ,s.ts_pretty as ts_start
          ,e.ts_pretty as ts_end
          ,s.install_ts     
          ,to_iso8601(date(s.install_ts)) as install_date     
          ,s.session_id     
          ,s.facebook_id    
          ,s.install_source 
          ,case when s.os is null then 'Unknown'
                when s.os='null' then 'Unknown'
                when s.os='' then 'Unknown'
                when s.os='ios' then 'iOS'
                when s.os='android' then 'Android'
                when s.os='windows' then 'Windows'
                else s.os
           end  as os     
          ,s.os_version     
          ,case when s.browser is null then 'Unknown'
                when s.browser='' then 'Unknown'
                else s.browser
           end as browser        
          ,s.browser_version
          ,s.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,s.email          
          ,s.first_name     
          ,s.last_name      
          ,s.level as level_start
          ,e.level as level_end
          ,s.vip_level
          ,s.gender         
          ,s.birthday       
          ,s.ip             
          ,s.lang as language
          ,case when s.app_id like 'royal%' then DATE_DIFF('second', s.ts_pretty, e.ts_pretty)
                else coalesce(e.session_length,0)
           end as playtime_sec
          ,s.scene
          ,s.fb_source
          ,s.last_ref
          ,s.gameserver_id
          ,s.app
          ,s.dt
from      farm_pm.session_start s
          left join farm_pm.session_end e
          on        s.session_id = e.session_id
          and       s.app_id = e.app_id
          and       s.user_id = e.user_id 
          and       s.scene=e.scene
          left join farm_pm.dim_country c
          on        s.country_code = c.country_code
where     s.dt >= (
                    select start_date 
                    from   farm_pm.init_start_date
                  )
          --and s.app like '_game_%'
;


--fact_revenue
delete from farm_pm.fact_revenue
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.fact_revenue
select      to_hex(md5(to_utf8(p.app_id||p.user_id||p.transaction_id))) as id               
           ,p.app_id
           ,p.app_version
           ,to_hex(md5(to_utf8(p.app_id||p.user_id))) as user_key
           ,p.user_id
           ,to_iso8601(date(p.ts_pretty)) as date
           ,p.ts_pretty
           ,p.install_ts
           ,to_iso8601(date(p.install_ts))
           ,p.session_id
           ,p.level
           ,p.vip_level
           ,case when p.os is null then 'Unknown'
                 when p.os='null' then 'Unknown'
                 when p.os='' then 'Unknown'
                 when p.os='ios' then 'iOS'
                 when p.os='android' then 'Android'
                 when p.os='windows' then 'Windows'
                 else p.os
            end  as os
           ,p.os_version
           ,p.device
           ,case when p.browser is null then 'Unknown'
                 when p.browser='' then 'Unknown'
                 else p.browser
            end as browser
           ,p.browser_version
           ,coalesce(cy.country,'Unknown') as country
           ,p.ip
           ,p.install_source
           ,p.lang as language
           ,p.payment_processor
           ,p.iap_product_id
           ,p.iap_product_name
           ,p.iap_product_type
           ,p.currency
           ,p.amount*1.0000/100 as revenue_amount   
           ,coalesce(p.amount*1.0000*c.factor/100, 0) as revenue_usd      
           ,p.transaction_id
           ,p.scene
           ,p.fb_source
           ,p.app
           ,p.dt
from      farm_pm.payment p
          left join farm_pm.currency c
          on        p.currency = c.currency
          and       p.dt = c.dt
          left join farm_pm.dim_country cy
          on        p.country_code = cy.country_code
where     p.dt >= (
                    select start_date 
                    from   farm_pm.init_start_date
                  )
          --and p.app like '_game_%'
;


--fact_new_user
delete from farm_pm.fact_new_user
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.fact_new_user
select     to_hex(md5(to_utf8(u.app_id||u.user_id||u.session_id))) as id
          ,u.app_id
          ,u.app_version
          ,to_hex(md5(to_utf8(u.app_id||u.user_id))) as user_key
          ,u.user_id
          ,to_iso8601(date(u.ts_pretty)) as date_start
          ,cast(null as varchar) as date_end
          ,u.ts_pretty as ts_start
          ,cast(null as timestamp) as ts_end
          ,u.install_ts
          ,to_iso8601(date(u.install_ts)) as install_date
          ,u.session_id
          ,u.facebook_id
          ,u.install_source
          ,case when u.os is null then 'Unknown'
                when u.os='null' then 'Unknown'
                when u.os='' then 'Unknown'
                when u.os='ios' then 'iOS'
                when u.os='android' then 'Android'
                when u.os='windows' then 'Windows'
                else u.os
           end as os
          ,u.os_version   
          ,case when u.browser is null then 'Unknown'
                when u.browser='' then 'Unknown'
                else u.browser
           end as browser
          ,u.browser_version
          ,u.device
          ,coalesce(c.country,'Unknown')  as country
          ,u.email
          ,u.first_name
          ,u.last_name
          ,u.level as level_start
          ,u.level as level_end
          ,u.vip_level
          ,u.gender
          ,u.birthday
          ,u.ip
          ,u.lang as language
          ,u.scene
          ,u.fb_source
          ,u.app
          ,u.dt
from      farm_pm.new_user u
          left join farm_pm.dim_country c
          on        u.country_code = c.country_code
where     u.dt >= (
                    select start_date
                    from   farm_pm.init_start_date
                 )
          --and u.app like '_game_%'
;

----------------------------------------------------------
--Data 2.0farm_pm.new_user
----------------------------------------------------------

delete from farm_pm.new_user
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.new_user 
select    event_id
         ,data_version
         ,app_id
         ,ts               
         ,ts_pretty        
         ,event
         ,user_id
         ,session_id
         ,app_version
         ,gameserver_id
         ,case when lower(os) = 'ios' then 'iOS' when lower(os) = 'android' then 'Android' else os end as os
         ,os_version       
         ,browser
         ,browser_version
         ,idfa
         ,idfv
         ,gaid
         ,android_id
         ,mac_address
         ,device
         ,ip
         ,country_code
         ,lang
         ,level            
         ,vip_level        
         ,facebook_id
         ,gender
         ,first_name
         ,last_name
         ,birthday         
         ,email
         ,googleplus_id
         ,gamecenter_id
         ,install_ts
         ,install_source
         ,scene
         ,fb_source
         ,app
         ,dt
from 
(
    select     to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) as event_id
              ,data_version 
              ,app_id 
              ,cast(ts as bigint) as ts 
              ,cast(ts_pretty as timestamp) as ts_pretty
              ,event 
              ,user_id 
              ,session_id 
              ,properties['app_version'] as app_version
              ,properties['gameserver_id'] as gameserver_id 
              ,properties['os'] as os
              ,properties['os_version'] as os_version
              ,properties['browser'] as browser 
              ,properties['browser_version'] as browser_version 
              ,properties['idfa'] as idfa 
              ,properties['idfv'] as idfv 
              ,properties['gaid'] as gaid 
              ,properties['android_id'] as android_id
              ,properties['mac_address'] as mac_address 
              ,properties['device'] as device 
              ,properties['ip'] as ip 
              ,properties['country_code'] as country_code 
              ,properties['lang'] as lang
              ,cast(COALESCE(nullif(properties['level'], ''), '0') as bigint) as level 
              ,cast(COALESCE(nullif(properties['vip_level'], ''), '0') as bigint) as vip_level 
              ,properties['facebook_id'] as facebook_id 
              ,properties['gender'] as gender 
              ,properties['first_name'] as first_name 
              ,properties['last_name'] as last_name
              ,case when properties['birthday'] is null or properties['birthday']='' then '1900-01-01'
                    else properties['birthday']
               end as birthday
              ,properties['email'] as email 
              ,properties['googleplus_id'] as googleplus_id 
              ,properties['gamecenter_id'] as gamecenter_id 
              ,cast(properties['install_ts_pretty'] as timestamp) as install_ts 
              ,properties['install_source'] as install_source 
              ,case when properties['scene']='1' or properties['scene'] is null or properties['scene']='' then 'Main' 
                    else properties['scene']
               end as scene 
              ,properties['fb_source'] as fb_source 
              ,app
              ,dt
              ,row_number() over(partition by app_id, to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) order by ts desc) as rnum
    from      rs_1_1.raw_events_daily
    where     dt >= (
                      select start_date
                      from   farm_pm.init_start_date
                    )
    and event = 'newuser'
    and properties['install_ts_pretty'] <> '' 
    --and app like '_game_%'
)t
where t.rnum = 1
;

----------------------------------------------------------
--farm_pm.session_start
----------------------------------------------------------

delete from farm_pm.session_start
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.session_start
select    event_id
         ,data_version
         ,app_id
         ,ts
         ,ts_pretty
         ,event
         ,user_id
         ,session_id
         ,app_version
         ,gameserver_id
         ,case when lower(os) = 'ios' then 'iOS' when lower(os) = 'android' then 'Android' else os end as os
         ,os_version       
         ,browser
         ,browser_version
         ,idfa
         ,idfv
         ,gaid
         ,android_id
         ,mac_address
         ,device
         ,ip
         ,country_code
         ,lang
         ,level
         ,vip_level
         ,facebook_id
         ,gender
         ,first_name
         ,last_name
         ,birthday
         ,email
         ,googleplus_id
         ,gamecenter_id
         ,install_ts
         ,install_source
         ,scene
         ,fb_source
         ,last_ref
         ,app
         ,dt
from 
(
     select     to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) as event_id 
               ,data_version 
               ,app_id 
               ,cast(ts as bigint) as ts 
               ,cast(ts_pretty as timestamp) as ts_pretty
               ,event 
               ,user_id 
               ,session_id 
               ,properties['app_version'] as app_version
               ,properties['gameserver_id'] as gameserver_id 
               ,properties['os'] as os
               ,properties['os_version'] as os_version
               ,properties['browser'] as browser 
               ,properties['browser_version'] as browser_version 
               ,properties['idfa'] as idfa 
               ,properties['idfv'] as idfv 
               ,properties['gaid'] as gaid 
               ,properties['android_id'] as android_id
               ,properties['mac_address'] as mac_address 
               ,properties['device'] as device 
               ,properties['ip'] as ip 
               ,properties['country_code'] as country_code 
               ,properties['lang'] as lang
               ,cast(COALESCE(nullif(properties['level'], ''), '0') as bigint) as level 
               ,cast(COALESCE(nullif(properties['vip_level'], ''), '0') as bigint) as vip_level 
               ,properties['facebook_id'] as facebook_id 
               ,properties['gender'] as gender 
               ,properties['first_name'] as first_name 
               ,properties['last_name'] as last_name 
               ,case when properties['birthday'] is null or properties['birthday']='' then '1900-01-01'
                     else properties['birthday']
                end as birthday
               ,properties['email'] as email 
               ,properties['googleplus_id'] as googleplus_id 
               ,properties['gamecenter_id'] as gamecenter_id 
               ,cast(properties['install_ts_pretty'] as timestamp) as install_ts 
               ,properties['install_source'] as install_source    
               ,case when properties['scene']='1' or properties['scene'] is null or properties['scene']='' then 'Main' 
                     else properties['scene']
                end as scene 
               ,properties['fb_source'] as fb_source
               ,properties['last_ref'] as last_ref
               ,app
               ,dt
               ,row_number() over(partition by app_id, to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) order by ts desc) as rnum
     from      rs_1_1.raw_events_daily
     where     dt >=  (
                        select start_date
                        from   farm_pm.init_start_date
                      )
     and event = 'session_start'
     and properties['install_ts_pretty'] <> ''
     --and app like '_game_%'
)t
where t.rnum =1
;

----------------------------------------------------------
--farm_pm.session_end (No session_end for Farm)
----------------------------------------------------------

delete from farm_pm.session_end
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.session_end
select    event_id
         ,data_version
         ,app_id
         ,ts              
         ,ts_pretty      
         ,event
         ,user_id
         ,session_id
         ,session_length 
         ,scene
         ,fb_source
         ,level
         ,app
         ,dt
from
(
   select     to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) as event_id 
             ,data_version 
             ,app_id 
             ,cast(ts as bigint) as ts 
             ,cast(ts_pretty as timestamp) as ts_pretty
             ,event 
             ,user_id 
             ,session_id 
             ,cast(coalesce(nullif(properties['session_length'],''), '0') as bigint) as session_length
             ,case when properties['scene']='1' or properties['scene'] is null or properties['scene']='' then 'Main' 
                   else properties['scene']
              end as scene 
             ,properties['fb_source'] as fb_source
             ,cast(COALESCE(nullif(properties['level'], ''), '0') as bigint) as level 
             ,app
             ,dt
             ,row_number() over(partition by app_id, to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) order by ts desc) as rnum
   from      rs_1_1.raw_events_daily
   where     dt >= (
                     select start_date
                     from   farm_pm.init_start_date
                   )
   and event = 'session_end'
   --and app like '_game_%'
)t
where t.rnum = 1
;

----------------------------------------------------------
--farm_pm.payment
----------------------------------------------------------
delete from farm_pm.payment
where dt >= to_iso8601(DATE_ADD('day', -3, CURRENT_DATE))
--and app like '_game_%'
;

insert into farm_pm.payment   
select    event_id
         ,data_version
         ,app_id
         ,ts               
         ,ts_pretty        
         ,event
         ,user_id
         ,session_id
         ,app_version
         ,gameserver_id
         ,case when lower(os) = 'ios' then 'iOS' when lower(os) = 'android' then 'Android' else os end as os
         ,os_version       
         ,browser
         ,browser_version
         ,idfa
         ,idfv
         ,gaid
         ,android_id
         ,mac_address
         ,device
         ,ip
         ,country_code
         ,lang
         ,level            
         ,vip_level        
         ,facebook_id
         ,gender
         ,first_name
         ,last_name
         ,birthday         
         ,email
         ,googleplus_id
         ,gamecenter_id
         ,install_ts       
         ,install_source
         ,payment_processor
         ,transaction_id
         ,currency
         ,iap_product_id
         ,iap_product_name
         ,iap_product_type
         ,amount
         ,scene
         ,fb_source
         ,app
         ,dt
from 
(
   select    to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) as event_id 
            ,data_version 
            ,app_id 
            ,cast(ts as bigint) as ts 
            ,cast(ts_pretty as timestamp) as ts_pretty
            ,event 
            ,user_id 
            ,COALESCE(session_id,'') as session_id 
            ,properties['app_version'] as app_version
            ,properties['gameserver_id'] as gameserver_id 
            ,properties['os'] as os
            ,properties['os_version'] as os_version
            ,properties['browser'] as browser 
            ,properties['browser_version'] as browser_version 
            ,properties['idfa'] as idfa 
            ,properties['idfv'] as idfv 
            ,properties['gaid'] as gaid 
            ,properties['android_id'] as android_id
            ,properties['mac_address'] as mac_address 
            ,properties['device'] as device 
            ,properties['ip'] as ip 
            ,properties['country_code'] as country_code 
            ,properties['lang'] as lang
            ,cast(COALESCE(nullif(properties['level'], ''), '0') as bigint) as level 
            ,cast(COALESCE(nullif(properties['vip_level'], ''), '0') as bigint) as vip_level 
            ,properties['facebook_id'] as facebook_id 
            ,properties['gender'] as gender 
            ,properties['first_name'] as first_name 
            ,properties['last_name'] as last_name 
            ,case when properties['birthday'] is null or properties['birthday']='' then '1900-01-01'
                  else properties['birthday']
             end as birthday
            ,properties['email'] as email 
            ,properties['googleplus_id'] as googleplus_id 
            ,properties['gamecenter_id'] as gamecenter_id 
            ,cast(properties['install_ts_pretty'] as timestamp) as install_ts 
            ,properties['install_source']  as install_source 
            ,properties['payment_processor'] as payment_processor
            ,properties['transaction_id'] as transaction_id
            ,case when properties['currency']='RMB' then 'CNY'
                  when properties['currency']='APT' then 'USD'
                  else properties['currency']
             end as currency
            ,properties['iap_product_id'] as iap_product_id
            ,properties['iap_product_name'] as iap_product_name
            ,properties['iap_product_type'] as iap_product_type
            ,CASE
                WHEN (app_id LIKE 'farm%' OR app_id LIKE 'ffs%' OR app_id LIKE 'ha%' OR app_id LIKE 'poker%' OR app_id LIKE 'royal%' OR app_id LIKE 'waf%') THEN cast(COALESCE(NULLIF(properties['amount'],''),'0') as bigint)
                WHEN (app_id LIKE 'maitai%') THEN cast(COALESCE(NULLIF(properties['amount'],''),'0') as bigint) / 1000000
                ELSE cast(COALESCE(NULLIF(properties['amount'],''),'0') as bigint) * 100
             END AS amount
            ,case when properties['scene']='1' or properties['scene'] is null or properties['scene']='' then 'Main' 
                  else properties['scene']
             end as scene 
            ,properties['fb_source'] as fb_source 
            ,app
            ,dt
            ,row_number() over(partition by app_id, to_hex(md5(to_utf8(app_id||event||cast(ts as varchar)||user_id||session_id))) order by ts desc) as rnum
   from     rs_1_1.raw_events_daily
   where    dt >= (
                    select start_date
                    from   farm_pm.init_start_date
                  )
   and event = 'payment' and app not like 'fruitscoot2%'
   and properties['install_ts_pretty'] <> ''
   --and app like '_game_%'
)t
where t.rnum = 1
;

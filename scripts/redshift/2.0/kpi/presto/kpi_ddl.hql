------------------------------------------------
-- Hive scritps
-- farm_pm used for testing
------------------------------------------------
USE farm_pm;

----- start_date
DROP TABLE IF EXISTS init_start_date;
CREATE TABLE IF NOT EXISTS init_start_date
(
    start_date STRING
)
PARTITIONED BY (
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- session_start
DROP TABLE IF EXISTS session_start;
CREATE TABLE IF NOT EXISTS session_start
(
    event_id STRING,
    data_version STRING,
    app_id STRING,
    ts BIGINT,
    ts_pretty TIMESTAMP,
    event STRING,
    user_id STRING,
    session_id STRING,
    app_version STRING,
    gameserver_id STRING,
    os STRING,
    os_version STRING,
    browser STRING,
    browser_version STRING,
    idfa STRING,
    idfv STRING,
    gaid STRING,
    android_id STRING,
    mac_address STRING,
    device STRING,
    ip STRING,
    country_code STRING,
    lang STRING,
    level BIGINT,
    vip_level BIGINT,
    facebook_id STRING,
    gender STRING,
    first_name STRING,
    last_name STRING,
    birthday STRING,
    email STRING,
    googleplus_id STRING,
    gamecenter_id STRING,
    install_ts TIMESTAMP,
    install_source STRING,
    scene STRING,
    fb_source STRING,
    last_ref STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- session_end
DROP TABLE IF EXISTS session_end;
CREATE TABLE IF NOT EXISTS session_end
(
    event_id STRING,
    data_version STRING,
    app_id STRING,
    ts BIGINT,
    ts_pretty TIMESTAMP,
    event STRING,
    user_id STRING,
    session_id STRING,
    session_length BIGINT,
    scene STRING,
    fb_source STRING,
    level BIGINT
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- new_user
DROP TABLE IF EXISTS new_user;
CREATE TABLE IF NOT EXISTS new_user
(
    event_id STRING,
    data_version STRING,
    app_id STRING,
    ts BIGINT,
    ts_pretty TIMESTAMP,
    event STRING,
    user_id STRING,
    session_id STRING,
    app_version STRING,
    gameserver_id STRING,
    os STRING,
    os_version STRING,
    browser STRING,
    browser_version STRING,
    idfa STRING,
    idfv STRING,
    gaid STRING,
    android_id STRING,
    mac_address STRING,
    device STRING,
    ip STRING,
    country_code STRING,
    lang STRING,
    level BIGINT,
    vip_level BIGINT,
    facebook_id STRING,
    gender STRING,
    first_name STRING,
    last_name STRING,
    birthday STRING,
    email STRING,
    googleplus_id STRING,
    gamecenter_id STRING,
    install_ts TIMESTAMP,
    install_source STRING,
    scene STRING,
    fb_source STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- payment
DROP TABLE IF EXISTS payment;
CREATE TABLE IF NOT EXISTS payment
(
    event_id STRING,
    data_version STRING,
    app_id STRING,
    ts BIGINT,
    ts_pretty TIMESTAMP,
    event STRING,
    user_id STRING,
    session_id STRING,
    app_version STRING,
    gameserver_id STRING,
    os STRING,
    os_version STRING,
    browser STRING,
    browser_version STRING,
    idfa STRING,
    idfv STRING,
    gaid STRING,
    android_id STRING,
    mac_address STRING,
    device STRING,
    ip STRING,
    country_code STRING,
    lang STRING,
    level BIGINT,
    vip_level BIGINT,
    facebook_id STRING,
    gender STRING,
    first_name STRING,
    last_name STRING,
    birthday STRING,
    email STRING,
    googleplus_id STRING,
    gamecenter_id STRING,
    install_ts TIMESTAMP,
    install_source STRING,
    payment_processor STRING,
    transaction_id STRING,
    currency STRING,
    iap_product_id STRING,
    iap_product_name STRING,
    iap_product_type STRING,
    amount BIGINT,
    scene STRING,
    fb_source STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------ tmp_user_daily_login
DROP TABLE IF EXISTS tmp_user_daily_login;
CREATE TABLE IF NOT EXISTS tmp_user_daily_login
(
    date STRING,
    app_id STRING,
    user_key STRING,
    user_id STRING,
    last_login_ts TIMESTAMP,
    facebook_id STRING,
    birthday STRING,
    first_name STRING,
    last_name STRING,
    email STRING,
    install_ts TIMESTAMP,
    install_date STRING,
    app_version STRING,
    level_start BIGINT,
    level_end BIGINT,
    os STRING,
    os_version STRING,
    country STRING,
    last_ip STRING,
    install_source STRING,
    language STRING,
    gender STRING,
    device STRING,
    browser STRING,
    browser_version STRING,
    session_cnt BIGINT,
    playtime_sec BIGINT,
    scene STRING,
    fb_source STRING,
    install_source_group STRING,
    last_ref STRING,
    gameserver_id STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- tmp_user_payment
DROP TABLE IF EXISTS tmp_user_payment;
CREATE TABLE IF NOT EXISTS tmp_user_payment
(
    app_id STRING,
    user_key STRING,
    date STRING,
    conversion_ts TIMESTAMP,
    revenue_usd DOUBLE,
    total_revenue_usd DOUBLE,
    purchase_cnt BIGINT,
    total_purchase_cnt BIGINT,
    scene STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

-------- fact_session
DROP TABLE IF EXISTS fact_session;
CREATE TABLE IF NOT EXISTS fact_session
(
    id STRING,
    app_id STRING,
    app_version STRING,
    user_key STRING,
    user_id STRING,
    date_start STRING,
    date_end STRING,
    ts_start TIMESTAMP,
    ts_end TIMESTAMP,
    install_ts TIMESTAMP,
    install_date STRING,
    session_id STRING,
    facebook_id STRING,
    install_source STRING,
    os STRING,
    os_version STRING,
    browser STRING,
    browser_version STRING,
    device STRING,
    country STRING,
    email STRING,
    first_name STRING,
    last_name STRING,
    level_start BIGINT,
    level_end BIGINT,
    vip_level BIGINT,
    gender STRING,
    birthday STRING,
    ip STRING,
    language STRING,
    playtime_sec BIGINT,
    scene STRING,
    fb_source STRING,
    last_ref STRING,
    gameserver_id STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- fact_revenue
DROP TABLE IF EXISTS fact_revenue;
CREATE TABLE IF NOT EXISTS fact_revenue
(
    id STRING,
    app_id STRING,
    app_version STRING,
    user_key STRING,
    user_id STRING,
    date STRING,
    ts TIMESTAMP,
    install_ts TIMESTAMP,
    install_date STRING,
    session_id STRING,
    level BIGINT,
    vip_level BIGINT,
    os STRING,
    os_version STRING,
    device STRING,
    browser STRING,
    browser_version STRING,
    country STRING,
    ip STRING,
    install_source STRING,
    language STRING,
    payment_processor STRING,
    iap_product_id STRING,
    iap_product_name STRING,
    iap_product_type STRING,
    currency STRING,
    revenue_amount DOUBLE,
    revenue_usd DOUBLE,
    transaction_id STRING,
    scene STRING,
    fb_source STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

-------- fact_new_user
DROP TABLE IF EXISTS fact_new_user;
CREATE TABLE IF NOT EXISTS fact_new_user
(
    id STRING,
    app_id STRING,
    app_version STRING,
    user_key STRING,
    user_id STRING,
    date_start STRING,
    date_end STRING,
    ts_start TIMESTAMP,
    ts_end TIMESTAMP,
    install_ts TIMESTAMP,
    install_date STRING,
    session_id STRING,
    facebook_id STRING,
    install_source STRING,
    os STRING,
    os_version STRING,
    browser STRING,
    browser_version STRING,
    device STRING,
    country STRING,
    email STRING,
    first_name STRING,
    last_name STRING,
    level_start BIGINT,
    level_end BIGINT,
    vip_level BIGINT,
    gender STRING,
    birthday STRING,
    ip STRING,
    language STRING,
    scene STRING,
    fb_source STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- fact_dau_snapshot
DROP TABLE IF EXISTS fact_dau_snapshot;
CREATE TABLE IF NOT EXISTS fact_dau_snapshot
(
    id STRING,
    user_key STRING,
    date STRING,
    app_id STRING,
    app_version STRING,
    level_start BIGINT,
    level_end BIGINT,
    os STRING,
    os_version STRING,
    device STRING,
    browser STRING,
    browser_version STRING,
    country STRING,
    language STRING,
    is_new_user BIGINT,
    is_payer BIGINT,
    is_converted_today BIGINT,
    revenue_usd DOUBLE,
    payment_cnt BIGINT,
    session_cnt BIGINT,
    playtime_sec BIGINT,
    scene STRING,
    revenue_iap DOUBLE,
    revenue_ads DOUBLE,
    gameserver_id STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- dim_user
DROP TABLE IF EXISTS dim_user;
CREATE TABLE IF NOT EXISTS dim_user
(
    id STRING,
    user_key STRING,
    app_id STRING,
    app_version STRING,
    user_id STRING,
    facebook_id STRING,
    install_ts TIMESTAMP,
    install_date STRING,
    install_source STRING,
    install_subpublisher STRING,
    install_campaign STRING,
    install_language STRING,
    install_country STRING,
    install_os STRING,
    install_device STRING,
    install_browser STRING,
    install_gender STRING,
    language STRING,
    birthday STRING,
    first_name STRING,
    last_name STRING,
    gender STRING,
    country STRING,
    email STRING,
    os STRING,
    os_version STRING,
    device STRING,
    browser STRING,
    browser_version STRING,
    last_ip STRING,
    level BIGINT,
    is_payer BIGINT,
    conversion_ts TIMESTAMP,
    revenue_usd DOUBLE,
    payment_cnt BIGINT,
    last_login_date STRING,
    install_source_group STRING,
    install_creative_id STRING,
    last_ref STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- currency
DROP TABLE IF EXISTS currency;
CREATE TABLE IF NOT EXISTS currency
(
    id STRING,
    currency STRING,
    factor DOUBLE
)
PARTITIONED BY (
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

-------- dim_country
DROP TABLE IF EXISTS dim_country;
CREATE TABLE IF NOT EXISTS dim_country
(
    country_code STRING,
    country STRING
)
PARTITIONED BY (
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

---- agg_marketing_kpi
DROP TABLE IF EXISTS agg_marketing_kpi;
CREATE TABLE IF NOT EXISTS agg_marketing_kpi
(
    app_id STRING,
    install_date STRING,
    install_date_str STRING,
    install_source STRING,
    install_source_group STRING,
    campaign STRING,
    sub_publisher STRING,
    creative_id STRING,
    country STRING,
    os STRING,
    new_installs BIGINT,
    d1_new_installs BIGINT,
    d7_new_installs BIGINT,
    d30_new_installs BIGINT,
    d60_new_installs BIGINT,
    d90_new_installs BIGINT,
    d120_new_installs BIGINT,
    revenue DOUBLE,
    d1_revenue DOUBLE,
    d7_revenue DOUBLE,
    d30_revenue DOUBLE,
    d60_revenue DOUBLE,
    d90_revenue DOUBLE,
    d120_revenue DOUBLE,
    payers BIGINT,
    d1_payers BIGINT,
    d7_payers BIGINT,
    d30_payers BIGINT,
    d60_payers BIGINT,
    d90_payers BIGINT,
    d120_payers BIGINT,
    d1_retained BIGINT,
    d7_retained BIGINT,
    d30_retained BIGINT,
    d60_retained BIGINT,
    d90_retained BIGINT,
    d120_retained BIGINT,
    cost DOUBLE,
    d3_new_installs BIGINT,
    d3_revenue DOUBLE,
    d3_payers BIGINT,
    d3_retained BIGINT,
    install_month STRING,
    d15_new_installs BIGINT,
    d15_revenue DOUBLE,
    d15_payers BIGINT,
    d15_retained BIGINT,
    d150_new_installs BIGINT,
    d150_revenue DOUBLE,
    d150_payers BIGINT,
    d150_retained BIGINT,
    level BIGINT,
    level_u2 BIGINT,
    level_a2u5 BIGINT,
    level_a5u10 BIGINT,
    level_a10 BIGINT,
    session_cnt BIGINT
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- agg_retention_ltv
DROP TABLE IF EXISTS agg_retention_ltv;
CREATE TABLE IF NOT EXISTS agg_retention_ltv
(
    player_day BIGINT,
    app_id STRING,
    app_version STRING,
    install_date STRING,
    install_source STRING,
    install_subpublisher STRING,
    install_campaign STRING,
    install_creative_id STRING,
    device_alias STRING,
    os STRING,
    browser STRING,
    country STRING,
    language STRING,
    is_payer BIGINT,
    new_user_cnt BIGINT,
    retained_user_cnt BIGINT,
    cumulative_revenue_usd DOUBLE,
    new_payer_cnt BIGINT
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- agg_kpi
DROP TABLE IF EXISTS agg_kpi;
CREATE TABLE IF NOT EXISTS agg_kpi
(
    date STRING,
    app_id STRING,
    app_version STRING,
    install_source_group STRING,
    install_source STRING,
    level_end BIGINT,
    browser STRING,
    country STRING,
    os STRING,
    language STRING,
    is_new_user BIGINT,
    is_payer BIGINT,
    new_user_cnt BIGINT,
    dau_cnt BIGINT,
    newpayer_cnt BIGINT,
    payer_today_cnt BIGINT,
    payment_cnt BIGINT,
    revenue_usd DOUBLE,
    session_cnt BIGINT,
    playtime_sec BIGINT,
    scene STRING,
    revenue_iap DOUBLE,
    revenue_ads DOUBLE,
    gameserver_id STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

------- fact_install_source
DROP TABLE IF EXISTS fact_install_source;
CREATE TABLE IF NOT EXISTS fact_install_source
(
    game STRING,
    userid STRING,
    user_key STRING,
    ts TIMESTAMP,
    tracker_name STRING,
    install_source STRING,
    install_campaign STRING,
    install_subpublisher STRING,
    install_creative_id STRING
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

DROP TABLE IF EXISTS agg_retention_ltv_2ndscene;
CREATE TABLE IF NOT EXISTS agg_retention_ltv_2ndscene
(
    player_day BIGINT,
    app_id STRING,
    app_version STRING,
    install_date STRING,
    install_source STRING,
    install_subpublisher STRING,
    install_campaign STRING,
    install_creative_id STRING,
    device_alias STRING,
    os STRING,
    browser STRING,
    country STRING,
    language STRING,
    is_payer BIGINT,
    scene STRING,
    new_user_cnt BIGINT,
    retained_user_cnt BIGINT,
    cumulative_revenue_usd DOUBLE,
    new_payer_cnt BIGINT
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='')
;

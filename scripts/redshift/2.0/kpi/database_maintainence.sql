analyze raw_events.events;
analyze kpi_processed.session_start;
analyze kpi_processed.session_end;
analyze kpi_processed.new_user;
analyze kpi_processed.payment;
analyze kpi_processed.fact_session;
analyze kpi_processed.fact_revenue;
analyze kpi_processed.fact_new_user;
analyze kpi_processed.tmp_user_daily_login;
analyze kpi_processed.tmp_user_payment;
analyze kpi_processed.fact_dau_snapshot;
analyze kpi_Processed.dim_user;
analyze kpi_Processed.agg_kpi;


delete from raw_events.events where ts_pretty < '2016-10-01 00:00:00';
delete from kpi_processed.session_start where ts_pretty < '2016-10-01 00:00:00';
delete from kpi_processed.session_end where ts_pretty < '2016-10-01 00:00:00';
delete from kpi_processed.new_user where ts_pretty < '2016-10-01 00:00:00';
delete from kpi_processed.fact_session where ts_pretty < '2016-10-01 00:00:00';
delete from kpi_processed.fact_new_user where ts_pretty < '2016-10-01 00:00:00';

vacuum;
-----------------------------------------------
--Data 2.0 Setup script
-----------------------------------------------
update kpi_processed.init_start_date                                                          
set start_date =  DATEADD(day, -3, CURRENT_DATE);

delete from raw_events.events
where user_id is null and app_id is null;

delete from raw_events.events
where ts_pretty>='2016-06-01' and app_id like 'loe%'
and json_extract_path_text(properties, 'level')='';
CREATE TABLE third_party.ios_feature
(
	date DATE,
	country VARCHAR(10) ENCODE lzo,
	device_type VARCHAR(10) ENCODE lzo,
	label_name VARCHAR(100) ENCODE lzo,
	rank INTEGER,
	app_id VARCHAR(100) ENCODE lzo,

	iphone_downloads INTEGER,
	iphone_revenue INTEGER,
	ipad_downloads INTEGER,
	ipad_revenue INTEGER
)
DISTSTYLE EVEN
SORTKEY
(
	date,
	device_type,
	label_name
);

CREATE TABLE third_party.android_feature
(
	date DATE,
	country VARCHAR(10) ENCODE lzo,
	label_name VARCHAR(100) ENCODE lzo,
	rank INTEGER,
	app_id VARCHAR(100) ENCODE lzo,

	android_downloads INTEGER,
	android_revenue INTEGER
)
DISTSTYLE EVEN
SORTKEY
(
	date,
	label_name
);

CREATE TABLE third_party.app_info
(
	os VARCHAR(10) ENCODE lzo,
	app_id VARCHAR(100) ENCODE lzo,
	app_name VARCHAR(100) ENCODE lzo,
	publisher_name VARCHAR(100) ENCODE lzo,
	categories VARCHAR(100) ENCODE lzo
)
DISTSTYLE EVEN
SORTKEY
(
	os,
	app_id
);

delete from marketing.adjust_bi_install_ip_discrepancy where install_date>=DATEADD(DAY, -1, current_date) and install_date<current_date;
insert into marketing.adjust_bi_install_ip_discrepancy
with adjust_all as (
select * from
	(
	select 
	    adid
	    ,tracker_name 
	    ,coalesce(split_part(tracker_name, '::', 1),'Unknown') as install_source
	    ,coalesce(split_part(tracker_name, '::', 2),'Unknown') as campaign
	    ,case
	        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') 
	             then coalesce(split_part(tracker_name, '::', 3),'Unknown')
	        when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') 
	             then coalesce(split_part(tracker_name, '::', 4),'Unknown')
	     end as sub_publisher
	    ,coalesce(split_part(tracker_name, '::', 4),'Unknown') as creative_id
	    ,fp_app_id as app_id
	    ,coalesce(a.country,'Unknown') as country_code
	    ,d.country
	    ,date(installed_at) as install_date
	    ,case
	        when app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar') then 'Android'
	        when app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350') then 'iOS'
	        when app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
	        else os_name
	     end as os
	     ,case when (user_id = '' or user_id is null) then 'Unknown' else user_id end as user_id
	     ,ip_address
	     ,row_number() over(partition by fp_app_id,adid order by installed_at, created_at) as rk 
	from adjust.raw_events	a 
	left join kpi_processed.dim_country d
	on a.country = lower(d.country_code)
	where 
		(event  = 'install' or event = 'open')
		and installed_at>=dateadd(day,-1,current_date)
		and installed_at<current_date
	)
where rk=1
 ) 
,bi_install as (
select * 
from
	(
	select 
	app_id
	,user_id
	,ip
	,country
	,row_number() over(partition by app_id,user_id order by install_ts,ts_start) as rk 
	from kpi_processed.fact_session
	where 
		install_date>=dateadd(day,-2,current_date)
		and install_date<current_date
	) 
where rk=1
)
select 
	app_id
	,install_date
	,install_source
	,sub_publisher
	,os
	,ip_address as adjust_ip_address
	,country as adjust_country
	,bi_ip_address
	,bi_country
	,count(distinct adid) as adid_count
from
(
select 
	a.*
	,b.ip as bi_ip_address
	,b.country as bi_country
from adjust_all a 
left join bi_install b 
	on a.app_id = b.app_id
	and a.user_id = b.user_id 
)
group by 1,2,3,4,5,6,7,8,9
;

create table marketing.adjust_bi_install_ip_discrepancy (

app_id              varchar(64)
,install_date       date
,install_source     varchar(1024)
,sub_publisher      varchar(1024)
,os                 varchar(64)
,adjust_ip_address  varchar(128)
,adjust_country     varchar(128)
,bi_ip_address      varchar(128)
,bi_country         varchar(128)
,adid_count         integer
)
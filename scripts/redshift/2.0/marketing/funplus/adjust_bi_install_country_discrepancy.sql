delete from marketing.adjust_bi_install_country_discrepancy where install_date>=DATEADD(DAY, -90, current_date);
insert into marketing.adjust_bi_install_country_discrepancy

with temp as (
select   
    a.adid 
 --   ,a.userid
 --   ,split_part(a.tracker_name, '::', 2) as campaign
    ,initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,a.game as app_id
    ,c.country as adjust_country
    ,trunc(a.ts) as install_date
    ,case
        when a.app_id in ('com.funplus.familyfarm', 'com.funplus.familyfarmthai', 'com.funplus.whatafarm', 'com.funplus.warships', 'com.funplus.battlewarships', 'com.funplus.loe', 'com.diandian.smashisland', 'com.diandian.crazyplanets', 'com.funplus.ent.dragonwar', 'com.funplus.kingofavalon','air.com.coalaa.itexasar') then 'Android'
        when a.app_id in ('883531333', '539920547', '1050043274', '1070724230', '1102723222', '1074186704', '1058711755', '1084930849','640477350') then 'iOS'
        when a.app_id = '2fc36411ad654d9fb43afa7640026896' then 'Amazon'
     end as os
    ,b.country as bi_country
from
    (select * from public.unique_adjust) a
    left join kpi_processed.dim_country c 
      on a.country=lower(c.country_code)
    left join (select * from kpi_processed.dim_user) b 
      on a.game = b.app_id 
      and a.userid = b.user_id
where 
    date(a.ts)>=DATEADD(DAY, -90, current_date)
)
select 
    app_id
    , install_date
    , install_source
    , sub_publisher
    , os
    , adjust_country
    , bi_country
    , count(distinct adid) as adid_count
    --, count(distinct userid) as userid_count
from temp group by 1,2,3,4,5,6,7
;  
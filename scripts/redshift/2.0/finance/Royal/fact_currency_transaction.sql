
delete from finance.fact_currency_transaction where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date);


---------- insert paid rc and coins data (from raw_payment)
insert into finance.fact_currency_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
       ts_pretty,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,       
       nvl(nullif(json_extract_path_text(properties,'m_currency_received'), '')::int, 0)  AS currency_purchased_qty,
       cast(json_extract_path_text(properties,'amount') as numeric(14,4)) as amount_paid,
       CASE
               WHEN nvl(nullif(json_extract_path_text(properties,'m_currency_received'), '')::int, 0) > 0 
               THEN (nvl(nullif(json_extract_path_text(properties,'amount'), '')::float, 0)*c.factor)/100 else 0 end as usd_paid,
       json_extract_path_text(properties,'currency') as currency,
       0 as currency_used_qty,       
       json_extract_path_text(properties,'d_currency_received_type') as currency_type,
       session_id,
       json_extract_path_text(properties,'iap_product_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'iap_product_name') AS action
FROM finance.raw_payment t
       JOIN finance.currency c
    ON c.currency = json_extract_path_text (properties,'currency')
   AND TRUNC (ts_pretty) = c.dt and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date);

---- update transaction_type

update finance.fact_currency_transaction set currency_type='rc' where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and usd_paid>0 and currency_type='Rubies';

update finance.fact_currency_transaction set currency_type='coins' where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and usd_paid>0  and currency_type='ss_coins';

update finance.fact_currency_transaction set transaction_type='rc' where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and usd_paid>0  and transaction_type='Rubies';

update finance.fact_currency_transaction set transaction_type='coins' where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and usd_paid>0 and transaction_type='ss_coins';



----- insert awarded currency data (from raw_transaction)
insert into finance.fact_currency_transaction
    SELECT md5(app_id||event||user_id||session_id||ts) as id,
       ts_pretty,
       cast (ts as bigint) as ts,
       app_id,
       user_id,
       nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_currency_received'),seq.i),'m_currency_amount'), '')::bigint, 0) as currency_purchased_qty,
       0 as amount_paid,
       0 as usd_paid,
       Null as currency,
       0 as currency_used_qty,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_currency_received'),seq.i),'d_currency_type') AS currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type, 
       json_extract_path_text(properties,'data_version') AS data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and json_extract_path_text(properties,'d_transaction_type')='awarded' and seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_currency_received')) and
json_extract_path_text (properties,'c_currency_received')<>'';



--------- insert currency_spent data (from raw_transaction)
insert into finance.fact_currency_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
        ts_pretty,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,
       0 AS currency_purchased_qty,
       0 AS amount_paid,
       0 AS usd_paid,
       NULL AS currency,
       nvl(nullif(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_currency_spent'),seq.i),'m_currency_amount'), '')::bigint, 0) AS currency_used_qty,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_currency_spent'),seq.i),'d_currency_type') AS currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
       FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and json_extract_path_text(properties,'d_transaction_type')='currency_spent'
and seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_currency_spent')) and
json_extract_path_text (properties,'c_currency_spent')<>'';



/*----- insert awarded currency data (old data structure)
insert into finance.fact_currency_transaction
    SELECT md5(app_id||event||user_id||session_id||ts) as id,
       (TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') as ts_pretty_ms,
       cast (ts as bigint) as ts,
       app_id,
       user_id,
        nvl(nullif(json_extract_path_text (properties,'m_currency_received'), '')::bigint, 0) as currency_purchased_qty,
       0 as amount_paid,
       0 as usd_paid,
       Null as currency,
       0 as currency_used_qty,
       json_extract_path_text(properties,'d_currency_received_type') as currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type, 
       json_extract_path_text(properties,'data_version') AS data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
FROM finance.raw_transaction
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and json_extract_path_text(properties,'d_transaction_type')='awarded' and 
nvl(nullif(json_extract_path_text (properties,'m_currency_received'), '')::bigint, 0)>0;

--------- insert currency_spent data (old data structure)
insert into finance.fact_currency_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
       (TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') as ts_pretty_ms,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,
       0 AS currency_purchased_qty,
       0 AS amount_paid,
       0 AS usd_paid,
       NULL AS currency,
       nvl(nullif(json_extract_path_text (properties,'m_currency_spent'), '')::bigint, 0) as  currency_used_qty,
       json_extract_path_text(properties,'d_currency_spent_type') AS currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
       FROM finance.raw_transaction
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and json_extract_path_text(properties,'d_transaction_type')='currency_spent';*/



/* ------------------ insert rc history data (during first run only)

insert into  finance.fact_currency_transaction
    SELECT md5(t.app_id||'Existing'||t.user_id||'Unknown'||'1445213123000') as id,
       (TIMESTAMP 'epoch' + 1445213123000 * INTERVAL '0.001 Second ') as ts_pretty_ms,
       1445213123000 as ts,
       cast(replace(t.app_id,'"','') as varchar(64)) as app_id,
       cast(replace(t.user_id,'"','') as varchar(64)) as user_id,
       (coalesce(cast(replace(rc_out,'"','') as bigint),0)+ coalesce(rc,0)) AS currency_purchased_qty,
       coalesce(p.usd_paid*100,0) as amount_paid,
       coalesce(p.usd_paid,0) as usd_paid,
       'USD' as currency,
       0 as currency_used_qty,
       'rc' as currency_type,
       null as session_id,
       'Existing' AS transaction_type, 
       '1.1' AS data_version,
       null AS app_version,
       'Existing' AS action
FROM finance.rc_transaction_legacy t
left join (select sum(cast(replace(usd_paid,'"','') as decimal(14,2))) as usd_paid,app_id,user_id from finance.payers_legacy where iap_product_type='"rc"' group by 2,3) p on replace(t.app_id,'"','')=replace(p.app_id,'"','') and replace(t.user_id,'"','')=replace(p.user_id,'"','')
left join tbl_user_20151019 u on u.app=cast(replace(t.app_id,'"','') as varchar(64)) and u.uid=cast(replace(t.user_id,'"','') as varchar(64));

insert into  finance.fact_currency_transaction
SELECT md5(app_id||'Existing'||user_id||'Unknown'||1445249123000) as id,
       (TIMESTAMP 'epoch' + 1445249123000 * INTERVAL '0.001 Second ') as ts_pretty_ms,
       1445249123000 as ts,
       cast(replace(app_id,'"','') as varchar(64)) as app_id,
       cast(replace(user_id,'"','') as varchar(64)) as user_id,
       0 as  currency_purchased_qty,
       0 as amount_paid,
       0 as usd_paid,
       null as currency,
       coalesce(cast(replace(rc_out,'"','') as bigint),0) as currency_used_qty,
       'rc' as currency_type,
       null as session_id,
       'Existing' AS transaction_type, 
       '1.1' AS data_version,
       null AS app_version,
       'Existing' AS action
FROM finance.rc_transaction_legacy ;

------------  insert coins history data (during first run only)

insert into  finance.fact_currency_transaction
SELECT md5(t.user_id||'Existing'||t.app_id||'Unknown'||1445249123000) as id,
       (TIMESTAMP 'epoch' + 1445249123000 * INTERVAL '0.001 Second ') as ts_pretty_ms,
       1445249123000 as ts,
       cast(replace(t.user_id,'"','') as varchar(64)) as app_id,
       cast(replace(t.app_id,'"','') as varchar(64)) as user_id,
       coalesce(cast(replace(coins_in,'"','') as bigint),0) as  currency_purchased_qty,
       coalesce(p.usd_paid*100,0) as amount_paid,
       coalesce(p.usd_paid,0) as usd_paid,
       'USD' as currency,
       0 as currency_used_qty,
       'coins' as currency_type,
       null as session_id,
       'Existing' AS transaction_type, 
       '1.1' AS data_version,
       null AS app_version,
       'Existing' AS action
FROM finance.coins_transaction_legacy t
left join (select sum(cast(replace(usd_paid,'"','') as decimal(14,2))) as usd_paid,app_id,user_id from finance.payers_legacy where iap_product_type='"coins"' group by 2,3) p on replace(t.user_id,'"','')=replace(p.app_id,'"','') and replace(t.app_id,'"','')=replace(p.user_id,'"','');

insert into  finance.fact_currency_transaction
SELECT md5(t.user_id||'Existing'||t.app_id||'Unknown'||1445249123000) as id,
       (TIMESTAMP 'epoch' + 1445249123000 * INTERVAL '0.001 Second ') as ts_pretty_ms,
       1445249123000 as ts,
       cast(replace(t.user_id,'"','') as varchar(64)) as app_id,
       cast(replace(t.app_id,'"','') as varchar(64)) as user_id,
       0 as  currency_purchased_qty,
       0 as amount_paid,
       0 as usd_paid,
       null as currency,
       (coalesce(cast(replace(coins_in,'"','') as bigint),0)-coalesce(coins,0)) as currency_used_qty,
       'coins' as currency_type,
       null as session_id,
       'Existing' AS transaction_type, 
       '1.1' AS data_version,
       null AS app_version,
       'Existing' AS action
FROM finance.coins_transaction_legacy t
left join tbl_user_20151019 u on u.app=cast(replace(t.user_id,'"','') as varchar(64)) and u.uid=cast(replace(t.app_id,'"','') as varchar(64));
*/


delete from finance.fact_currency_transaction where currency_purchased_qty=0 and currency_used_qty=0;
delete from finance.fact_currency_transaction where action='loadPayment';

update finance.tmp_start_date set start_date='2015-10-19';

------- to avoid duplicate transactions group by ts_pretty
delete from finance.fact_currency_transaction_agg;

insert into finance.fact_currency_transaction_agg
select t.id,
ts_pretty,
ts,
t.app_id,
t.user_id,
sum(currency_purchased_qty) as currency_purchased_qty,
sum(amount_paid) as amount_paid,
sum(t.usd_paid) as usd_paid,
currency,
0 as currency_used_qty,
currency_type,
session_id,
transaction_type,
t.data_version,
t.app_version,
action from finance.fact_currency_transaction t
join processed.dim_user u on t.app_id=u.app_id and t.user_id=u.user_id
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and u.is_payer=1
group by 1,2,3,4,5,9,11,12,13,14,15,16;

insert into finance.fact_currency_transaction_agg
select t.id,
ts_pretty,
ts,
t.app_id,
t.user_id,
0 as currency_purchased_qty,
0 as amount_paid,
0 as usd_paid,
currency,
sum(currency_used_qty) as currency_used_qty,
currency_type,
session_id,
transaction_type,
t.data_version,
t.app_version,
action from finance.fact_currency_transaction t
join processed.dim_user u on t.app_id=u.app_id and t.user_id=u.user_id
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and u.is_payer=1
group by 1,2,3,4,5,9,11,12,13,14,15,16;


delete from finance.fact_currency_transaction_agg where currency_purchased_qty=0 and currency_used_qty=0;

update finance.fact_currency_transaction_agg set action='Unknown' where (action is Null or action='');

--- dim_currency_balance to get previous balance

CREATE temp TABLE latest_transaction
AS
SELECT 
       MAX(ts_pretty) AS ts_pretty,
       max(ts) as ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       currency_type
FROM finance.processed_currency_transaction where TRUNC(ts_pretty) < (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 7,8,9;

DROP TABLE finance.dim_currency_balance;

CREATE TABLE finance.dim_currency_balance 
AS
SELECT md5(p.app_id||p.user_id||l.ts) as id,
       l.ts_pretty AS updated_ts,
       l.ts,
       p.app_id,
       p.user_id,
       p.currency_type,
       p.currency_balance,
       p.usd_balance,
       p.cost_per_unit,
       l.currency_purchased_qty,
       l.currency_used_qty,
       l.usd_paid,
       l.usd_used
FROM (SELECT 
             date,
                   currency_balance,
                   usd_balance,
                   cost_per_unit,
                   currency_type,
                   app_id,
                   user_id,
                   ROW_NUMBER() OVER (PARTITION BY app_id,user_id,currency_type ORDER BY date DESC) AS RANK from finance.agg_currency_transaction)p
  JOIN latest_transaction l
    ON p.app_id = l.app_id
   AND l.user_id = p.user_id 
   and l.currency_type=p.currency_type
   where date < (SELECT start_date FROM finance.tmp_start_date) and p.rank=1;




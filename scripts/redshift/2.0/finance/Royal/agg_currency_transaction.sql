

--- delete empty records
DELETE
FROM finance.processed_currency_transaction
WHERE currency_purchased_qty = 0
AND   currency_used_qty = 0;

--update finance.processed_currency_transaction set usd_balance=0 where usd_balance>=-0.01 and usd_balance<=0.01;

--- to delete the transactions whose currency purchased is less than used
create temp table neg_bal as
select distinct app_id,user_id,currency_type from finance.processed_currency_transaction where currency_balance<0;


delete from finance.processed_currency_transaction where (app_id,user_id,currency_type) in (select app_id,user_id,currency_type from neg_bal);


----- delete duplicate records from finance.processed_currency_transaction
CREATE temp TABLE delete_dup
AS
SELECT COUNT(1),
       user_id,
       app_id,
       currency_type,
       ts_pretty,
       ts
FROM finance.processed_currency_transaction
WHERE TRUNC(ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 2,3,4,5,6
HAVING COUNT(1) > 1;


---- temp table for finance.agg_currency_transaction
CREATE temp TABLE last_transaction
AS
SELECT TRUNC(ts_pretty) AS DATE,
       MAX(ts_pretty) AS ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       currency_type
FROM finance.processed_currency_transaction
WHERE TRUNC(ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 1,7,8,9;

--- multiple transactions at same ts
create temp table dup_balance as
select app_id,user_id,ts_pretty,ts,currency_type from delete_dup where (app_id,user_id,currency_type,ts_pretty) in
(select app_id,user_id,currency_type,ts from last_transaction);


--- delete last N days of data
DELETE
FROM finance.agg_currency_transaction
WHERE DATE>= (SELECT start_date FROM finance.tmp_start_date);

create temp table payers as
select distinct app_id,user_id, conversion_ts,is_payer from processed.dim_user where app_id like 'royal%' and is_payer=1;

---- insert to finance.agg_currency_transaction
INSERT INTO finance.agg_currency_transaction
SELECT DATE,
       LAG(currency_balance,1) OVER (PARTITION BY app_id,user_id ORDER BY DATE) AS begin_currency_balance,
       currency_balance,
       LAG(usd_balance,1) OVER (PARTITION BY app_id,user_id ORDER BY DATE) AS begin_usd_balance,
       usd_balance,
       cost_per_unit,
       currency_type,
       app_id,
       user_id,
       currency_used_qty,
       currency_purchased_qty,
       usd_paid,
       usd_used
FROM (SELECT TRUNC(c.ts_pretty) AS DATE,
             currency_balance,
             usd_balance,
             cost_per_unit,
             c.currency_type,
             c.app_id,
             c.user_id,
             t.currency_used_qty,
             t.currency_purchased_qty,
             t.usd_paid,
             t.usd_used
      FROM (SELECT ts_pretty,
                   currency_balance,
                   usd_balance,
                   cost_per_unit,
                   currency_type,
                   app_id,
                   user_id,
                   ROW_NUMBER() OVER (PARTITION BY app_id,user_id,currency_type,TRUNC(ts_pretty) ORDER BY ts DESC) AS RANK
            FROM finance.processed_currency_transaction
 where TRUNC(ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)) c
        JOIN payers u
          ON c.user_id = u.user_id
         AND c.app_id = u.app_id
        JOIN last_transaction t
          ON t.app_id = c.app_id
         AND t.user_id = c.user_id
         AND t.currency_type = c.currency_type
         AND t.ts = c.ts_pretty
      WHERE c.rank = 1
      AND   u.is_payer = 1
      ) d;

delete from finance.agg_currency_transaction where date>= (SELECT start_date FROM finance.tmp_start_date) and (app_id,user_id,currency_type) in (select app_id,user_id,currency_type from dup_balance);


CREATE temp TABLE last_transaction_dup
AS
SELECT TRUNC(ts_pretty) AS DATE,
       MAX(ts_pretty) AS ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       b.app_id,
       b.user_id,
       b.currency_type
FROM finance.processed_currency_transaction b
join (select distinct app_id,user_id,currency_type from dup_balance) d on b.app_id=d.app_id and b.user_id=d.user_id
and b.currency_type=d.currency_type
WHERE TRUNC(b.ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 1,7,8,9;

---- in case of multiple transactions to avoid duplicates and insert latest balance for the day
INSERT INTO finance.agg_currency_transaction
SELECT DATE,
       LAG(currency_balance,1) OVER (PARTITION BY app_id,user_id ORDER BY DATE) AS begin_currency_balance,
       currency_balance,
       LAG(usd_balance,1) OVER (PARTITION BY app_id,user_id ORDER BY DATE) AS begin_usd_balance,
       usd_balance,
       cost_per_unit,
       currency_type,
       app_id,
       user_id,
       currency_used_qty,
       currency_purchased_qty,
       usd_paid,
       usd_used from 
(
SELECT
TRUNC(c.ts_pretty) AS DATE,
             currency_balance,
             usd_balance,
             cost_per_unit,
             c.currency_type,
             c.app_id,
             c.user_id,
             t.currency_used_qty,
             t.currency_purchased_qty,
             t.usd_paid,
             t.usd_used
FROM (SELECT ts_pretty,currency_balance,currency_purchased_qty,currency_used_qty,usd_balance,cost_per_unit,currency_type,
app_id,user_id,
row_number() over (partition by app_id,user_id,currency_type,trunc(ts_pretty) order by ts desc) as rank from finance.processed_currency_transaction where TRUNC(ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)
)c
JOIN payers u
          ON c.user_id = u.user_id
         AND c.app_id = u.app_id
JOIN last_transaction_dup t
          ON t.app_id = c.app_id
         AND t.user_id = c.user_id 
         and t.currency_type=c.currency_type
         AND t.ts = c.ts_pretty where u.is_payer=1 and c.currency_used_qty>0) d;

---- temp table to get month end balances
CREATE temp TABLE month_end_transaction
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       currency_type
FROM finance.processed_currency_transaction
WHERE datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
GROUP BY 1,2,8,9,10;

---- insert into finance.agg_currency_transaction_monthly
DELETE
FROM finance.agg_currency_transaction_monthly
WHERE month = datepart ('month',CURRENT_DATE)
AND   year = datepart ('year',CURRENT_DATE);

INSERT INTO finance.agg_currency_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.currency_type,
       c.currency_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.currency_purchased_qty,
       t.currency_used_qty,
       t.usd_paid,
       t.usd_used
FROM (SELECT ts_pretty,currency_balance,usd_balance,cost_per_unit,currency_type,app_id,user_id,
row_number() over (partition by app_id,user_id,currency_type,datepart('year',ts_pretty),datepart('month',ts_pretty) order by ts_pretty desc) as rank from finance.processed_currency_transaction
where datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE) )c
  JOIN payers u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN month_end_transaction t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id and t.currency_type=c.currency_type
   AND t.last_ts = c.ts_pretty
WHERE  u.is_payer = 1 and c.rank=1;

----------  in case of multiple transactions to avoid duplicates and insert latest balance for the month
CREATE temp TABLE delete_dup_monthly
AS
SELECT COUNT(1),
       user_id,
       app_id,
       currency_type,
       datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       ts_pretty,
       ts
FROM finance.processed_currency_transaction
where datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
GROUP BY 2,3,4,5,6,7,8
HAVING COUNT(1) > 1;


create temp table dup_balance_monthly as
select app_id,user_id,ts_pretty,ts,currency_type,month,year from delete_dup_monthly where (app_id,user_id,currency_type,ts_pretty,month,year) in
(select app_id,user_id,currency_type,last_ts,month,year from month_end_transaction);



delete from finance.agg_currency_transaction_monthly where month = datepart ('month',CURRENT_DATE)
AND   year = datepart ('year',CURRENT_DATE) and (app_id,user_id,currency_type) in (select app_id,user_id,currency_type from dup_balance_monthly);



CREATE temp TABLE last_transaction_monthly_dup
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       b.app_id,
       b.user_id,
       b.currency_type
FROM finance.processed_currency_transaction b
join (select distinct app_id,user_id,currency_type from dup_balance_monthly) d on b.app_id=d.app_id and b.user_id=d.user_id and b.currency_type=d.currency_type
WHERE datepart ('month',b.ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',b.ts_pretty) = datepart ('year',CURRENT_DATE)
GROUP BY 1,2,8,9,10;

INSERT INTO finance.agg_currency_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.currency_type,
       c.currency_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.currency_purchased_qty,
       t.currency_used_qty,
       t.usd_paid,
       t.usd_used
FROM (SELECT ts_pretty,currency_balance,usd_balance,currency_used_qty,currency_purchased_qty,cost_per_unit,
currency_type,app_id,user_id,
row_number() over (partition by app_id,user_id,currency_type,datepart('year',ts_pretty),datepart('month',ts_pretty) order by ts_pretty desc) as rank from finance.processed_currency_transaction
where datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE))c
  JOIN payers u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN last_transaction_monthly_dup t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id and t.currency_type=c.currency_type
   AND t.last_ts = c.ts_pretty
WHERE u.is_payer = 1 and c.currency_used_qty>0;


------ to insert new payers data

CREATE temp TABLE month_end_transaction_payer_today
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       p.app_id,
       p.user_id,
       p.currency_type
FROM finance.processed_currency_transaction p
join payers s on p.app_id=s.app_id and p.user_id=s.user_id
WHERE datepart ('month',ts_pretty) = datepart ('month',dateadd (month,-1,CURRENT_DATE))
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE) and trunc(conversion_ts)>=(select start_date from finance.tmp_start_date)
GROUP BY 1,2,8,9,10;


delete from finance.agg_currency_transaction_monthly using month_end_transaction_payer_today p
where finance.agg_currency_transaction_monthly.app_id=p.app_id and 
finance.agg_currency_transaction_monthly.user_id=p.user_id and
 finance.agg_currency_transaction_monthly.currency_type=p.currency_type and
finance.agg_currency_transaction_monthly.month=p.month and 
finance.agg_currency_transaction_monthly.year=p.year;


INSERT INTO finance.agg_currency_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.currency_type,
       c.currency_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.currency_purchased_qty,
       t.currency_used_qty,
       t.usd_paid,
       t.usd_used
FROM finance.processed_currency_transaction c
  JOIN payers u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN month_end_transaction_payer_today t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id and t.currency_type=c.currency_type
   AND t.last_ts = c.ts_pretty
WHERE u.is_payer = 1;



--- updating monthly_transaction with inactive players
CREATE temp TABLE player_update
AS
SELECT app_id,
       user_id,
       currency_type,
       0 AS currency_purchased_qty,
       0 AS currency_used_qty,
       currency_balance,
       0 AS usd_used,
       0 AS usd_paid,
       usd_balance
FROM finance.agg_currency_transaction_monthly
WHERE month = datepart ('month',dateadd (month,-1,CURRENT_DATE))
AND   (app_id,user_id,currency_type) NOT IN (SELECT app_id,
                                        user_id,currency_type
                                 FROM finance.agg_currency_transaction_monthly
                                 WHERE MONTH = datepart (MONTH,CURRENT_DATE));

INSERT INTO finance.agg_currency_transaction_monthly
SELECT app_id,
       user_id,
       currency_type,
       currency_balance,
       usd_balance,
       datepart(year,CURRENT_DATE) AS year,
       datepart(month,CURRENT_DATE) AS month,
       currency_purchased_qty,
       currency_used_qty,
       usd_paid,
       usd_used
FROM player_update;




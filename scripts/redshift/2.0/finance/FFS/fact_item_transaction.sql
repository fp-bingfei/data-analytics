-- update start date

---update finance.tmp_start_date set start_date = ?::DATE; 

----CREATE VIEW seq_0_to_10 AS (
    ---SELECT 0 AS i UNION ALL                                      
    ---SELECT 1 UNION ALL
    ---SELECT 2 UNION ALL    
    ---SELECT 3 UNION ALL
    ---SELECT 4 UNION ALL
    ---SELECT 5 UNION ALL
    ---SELECT 6 UNION ALL
    ---SELECT 7 UNION ALL
    ---SELECT 8 UNION ALL
    ---SELECT 9 UNION ALL  
    ---SELECT 10  
---);


--- insert into finance.fact_item_transaction


delete from finance.fact_item_transaction where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and event='payment';


/*----- insert item_used data
insert into finance.fact_item_transaction
    SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_class') AS item_class,   
       0 as item_purchased_qty, 
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'m_item_amount') as int) AS item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       0 as usd_paid,
       0 as cost_per_unit,
       data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action,
       Null as currency_type
       FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_used')) and json_extract_path_text (properties,'d_transaction_type')='item_used' and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;

-------- insert currency_spent and item_received data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class, 
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') as int) AS item_purchased_qty,
       0 as item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       cast(json_extract_path_text(properties,'m_currency_spent') as int)/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) as usd_paid,
       0 as cost_per_unit,
       data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action,       
       json_extract_path_text(properties,'d_currency_spent_type') as currency_type
         FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and json_extract_path_text (properties,'d_transaction_type')='currency_spent' and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;

-----insert awarded data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class, 
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') as int) AS item_purchased_qty,
       0 as item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       CAST(
               CASE WHEN json_extract_path_text (properties,'m_currency_spent') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'m_currency_spent')
               END AS integer
             )/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) AS usd_paid,
       0 as cost_per_unit,
       data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action,       
       json_extract_path_text(properties,'d_currency_spent_type') as currency_type
         FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and json_extract_path_text (properties,'d_transaction_type')='awarded' and json_extract_path_text (properties,'d_action')='user_register'
and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;

------ insert currency_spent and item_used data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_class') AS item_class, 
       0 as item_purchased_qty,
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'m_item_amount') as int) AS item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,          
       0 as usd_paid,
       0 as cost_per_unit,
       data_version,    
       json_extract_path_text(properties,'app_version') AS app_version,json_extract_path_text(properties,'d_action') AS action,
       Null as currency_type
    FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_used')) and json_extract_path_text (properties,'d_transaction_type')='currency_spent' and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;*/

----- insert payment data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       item_id,
       item_type,
       item_name,
       item_class,
       item_purchased_qty,
       item_used_qty,      
       transaction_type,
       (usd_paid*c.factor) as usd_paid,
       0 as cost_per_unit,
       data_version,
       app_version,
       action,       
       currency_type
       
FROM (SELECT app_id,
             data_version,
             event,
             user_id,
             session_id,
             CAST(ts AS BIGINT) AS ts,
             ts_pretty,
             json_extract_path_text(properties,'app_version') AS app_version,
             json_extract_path_text(properties,'currency') AS currency,
             NULL AS currency_type,
             (CASE
               WHEN CAST(json_extract_path_text (properties,'m_currency_received') AS INT) > 0 THEN 0
               ELSE (CAST(json_extract_path_text (properties,'amount') AS INTEGER)) / 100
             END)/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) AS usd_paid,
             json_extract_path_text(properties,'iap_product_type') AS transaction_type,
             json_extract_path_text(properties,'iap_product_name') AS action,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_id') AS item_id,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_type') AS item_type,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class,
             CAST(json_extract_path_text (JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') AS INT) AS item_purchased_qty,
             0 AS item_used_qty
      FROM finance.raw_payment,
           seq_0_to_10 AS seq
      WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
      ) t
  JOIN finance.currency c
    ON c.currency = t.currency
   AND TRUNC (t.ts_pretty) = c.dt;


-- delete inactive players
delete from finance.fact_item_transaction where item_purchased_qty=0 and item_used_qty=0;








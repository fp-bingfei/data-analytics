--- delete inactive players
DELETE
FROM processed.processed_consumable_item_transaction
WHERE item_purchased_qty = 0
AND   item_used_qty = 0;

--- delete players who started using items without purchasing any

create temp table first_transaction as
select p.user_id,p.app_id,p.item_id,ts_pretty,item_balance from processed.processed_consumable_item_transaction p
join (select app_id,user_id,item_id,min(ts_pretty) as ts from processed.processed_consumable_item_transaction group by 1,2,3)t
on p.app_id=t.app_id and p.user_id=t.user_id and p.ts_pretty=t.ts and p.item_id=t.item_id where item_balance<0;

DELETE
FROM processed.processed_consumable_item_transaction USING first_transaction d
WHERE processed.processed_consumable_item_transaction.app_id = d.app_id
AND   processed.processed_consumable_item_transaction.user_id = d.user_id
AND   processed.processed_consumable_item_transaction.item_id = d.item_id;

---- delete duplicate records

CREATE temp TABLE delete_dup
AS
SELECT COUNT(1),
       user_id,
       app_id,
       item_id,
       ts_pretty
FROM processed.processed_consumable_item_transaction
where item_id<>'booster_moves'
GROUP BY 2,3,4,5
HAVING COUNT(1) > 1;

DELETE
FROM processed.processed_consumable_item_transaction USING delete_dup d
WHERE processed.processed_consumable_item_transaction.app_id = d.app_id
AND   processed.processed_consumable_item_transaction.user_id = d.user_id
AND   processed.processed_consumable_item_transaction.item_id = d.item_id and processed.processed_consumable_item_transaction.item_id<>'booster_moves';



---- temp table for processed.agg_consumable_item_transaction


CREATE temp TABLE item_last_transaction
AS
SELECT TRUNC(ts_pretty) AS DATE,
       MAX(ts_pretty) AS ts,
       SUM(item_used_qty) AS item_used_qty,
       SUM(item_purchased_qty) AS item_purchased_qty,
       sum(case when transaction_type<>'currency_spent' and transaction_type<>'awarded' then usd_paid else 0 end) AS iap_usd_paid,
       sum(case when transaction_type='currency_spent' then usd_paid else 0 end) AS currency_usd_paid,
       sum(usd_paid) as total_usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class
FROM processed.processed_consumable_item_transaction
WHERE TRUNC(ts_pretty) >= (SELECT start_date FROM processed.tmp_start_date)
GROUP BY 1,9,10,11,12,13,14;

--- delete last N days of data
DELETE
FROM processed.agg_consumable_item_transaction
WHERE DATE>= (SELECT start_date FROM processed.tmp_start_date);

INSERT INTO processed.agg_consumable_item_transaction
SELECT DATE,
       LAG(item_balance,1) OVER (PARTITION BY app_id,user_id,item_id,item_name,item_type,item_class ORDER BY DATE) AS begin_item_balance,
       item_balance,
       LAG(usd_balance,1) OVER (PARTITION BY app_id,user_id,item_id,item_name,item_type,item_class ORDER BY DATE) AS begin_usd_balance,
       usd_balance,
       cost_per_unit,
       app_id,
       user_id,
       item_id,
       item_used_qty,
       item_purchased_qty,
       total_usd_paid ,
       usd_used,
       item_name,
       item_type,
       item_class,
       iap_usd_paid,
       currency_usd_paid
FROM (SELECT TRUNC(c.ts_pretty) AS DATE,
             item_balance,
             usd_balance,
             c.cost_per_unit,
             c.app_id,
             c.user_id,
             c.item_id,
             t.item_used_qty,
             t.item_purchased_qty,
             t.total_usd_paid,
             t.iap_usd_paid,
             t.currency_usd_paid,
             t.usd_used,
             c.item_name,
             c.item_type,
             c.item_class
      FROM processed.processed_consumable_item_transaction c
        JOIN processed.dim_user u
          ON c.user_id = u.user_id
         AND c.app_id = u.app_id
        JOIN item_last_transaction t
          ON t.app_id = c.app_id
         AND t.user_id = c.user_id
         AND t.ts = c.ts_pretty
         AND t.item_id = c.item_id 
         and t.item_name=c.item_name 
         and t.item_type=c.item_type 
         and t.item_class = c.item_class
      WHERE u.is_payer = 1
      AND   TRUNC(c.ts_pretty) >= (SELECT start_date FROM processed.tmp_start_date)
      ) d;



---- temp table to get month end balances
CREATE temp TABLE month_end_transaction
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(item_purchased_qty) AS item_purchased_qty,
       SUM(item_used_qty) AS item_used_qty,
       sum(case when transaction_type<>'currency_spent' and 
transaction_type<>'awarded' then usd_paid else 0 end) AS iap_usd_paid,
       sum(case when transaction_type='currency_spent' then usd_paid 
else 0 end) AS currency_usd_paid,
       sum(usd_paid) as total_usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class
FROM processed.processed_consumable_item_transaction
WHERE datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
GROUP BY 1,2,10,11,12,13,14,15;

DELETE
FROM processed.agg_consumable_item_transaction_monthly
WHERE month = datepart ('month',CURRENT_DATE)
AND   year = datepart ('year',CURRENT_DATE);

---- insert into processed.agg_consumable_item_transaction_monthly
INSERT INTO processed.agg_consumable_item_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.item_id,
       c.item_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.item_purchased_qty,
       t.item_used_qty,
       t.total_usd_paid,
       t.usd_used,
       c.item_name,
       c.item_type,
       c.item_class,
       t.iap_usd_paid,
       t.currency_usd_paid
FROM processed.processed_consumable_item_transaction c
  JOIN processed.dim_user u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN month_end_transaction t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id
   AND t.item_id = c.item_id
   AND t.item_name = c.item_name
   AND t.item_type = c.item_type
   AND t.item_class = c.item_class
   AND t.last_ts = c.ts_pretty
WHERE u.is_payer = 1
AND   datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
;


--- to delete the transactions whose item purchased is less than used
CREATE temp TABLE negative_bal
AS
SELECT SUM(item_purchased_qty) AS c_p,
       SUM(item_used_qty) AS c_u,
       SUM(usd_paid) AS usd_paid,
       app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class
FROM processed.fact_item_transaction where item_class='consumable'
GROUP BY 4,5,6,7,8,9
HAVING SUM(item_purchased_qty) < SUM(item_used_qty);

DELETE
FROM processed.processed_consumable_item_transaction USING negative_bal b
WHERE processed.processed_consumable_item_transaction.app_id = b.app_id
AND   processed.processed_consumable_item_transaction.user_id = b.user_id
AND   processed.processed_consumable_item_transaction.item_id = b.item_id
AND   processed.processed_consumable_item_transaction.item_name= b.item_name
AND   processed.processed_consumable_item_transaction.item_type = b.item_type
AND   processed.processed_consumable_item_transaction.item_class = b.item_class;

DELETE
FROM processed.agg_consumable_item_transaction USING negative_bal b
WHERE processed.agg_consumable_item_transaction.app_id = b.app_id
AND   processed.agg_consumable_item_transaction.user_id = b.user_id
AND   processed.agg_consumable_item_transaction.item_id = b.item_id
AND   processed.agg_consumable_item_transaction.item_name = b.item_name
AND   processed.agg_consumable_item_transaction.item_type = b.item_type
AND   processed.agg_consumable_item_transaction.item_class = b.item_class;

DELETE
FROM processed.agg_consumable_item_transaction_monthly USING negative_bal b
WHERE processed.agg_consumable_item_transaction_monthly.app_id = b.app_id
AND   processed.agg_consumable_item_transaction_monthly.user_id = b.user_id
AND   processed.agg_consumable_item_transaction_monthly.item_id = b.item_id
AND   processed.agg_consumable_item_transaction_monthly.item_name = b.item_name
AND   processed.agg_consumable_item_transaction_monthly.item_type = b.item_type
AND   processed.agg_consumable_item_transaction_monthly.item_class = b.item_class;

----  updating monthly_transaction with inactive players
CREATE temp TABLE player_update
AS
SELECT app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class,
       0 AS item_purchased_qty,
       0 AS item_used_qty,
       item_balance,
       0 AS usd_used,
       0 AS total_usd_paid,
       0 as iap_usd_paid,
       0 as currency_usd_paid,
       usd_balance
FROM processed.agg_consumable_item_transaction_monthly
WHERE month = datepart ('month',dateadd (month,-1,CURRENT_DATE))
AND   (app_id,user_id,item_id,item_name,item_type,item_class) NOT IN (SELECT app_id,
                                                user_id,
                                                item_id,item_name,item_type,item_class
                                         FROM processed.agg_consumable_item_transaction_monthly
                                         WHERE MONTH = datepart ('month',CURRENT_DATE));

INSERT INTO processed.agg_consumable_item_transaction_monthly
SELECT app_id,
       user_id,
       item_id,
       item_balance,
       usd_balance,
       datepart(year,CURRENT_DATE) AS year,
       datepart(month,CURRENT_DATE) AS month,
       item_purchased_qty,
       item_used_qty,
       total_usd_paid,
       usd_used,
       item_name,
       item_type,
       item_class,
       iap_usd_paid,
       currency_usd_paid
FROM player_update;



--- insert into processed.fact_currency_transaction

delete from processed.fact_currency_transaction where trunc(ts_pretty)>=(select start_date from processed.tmp_start_date);


insert into processed.fact_currency_transaction
    SELECT md5(app_id||event||user_id||session_id||ts) as id,
       (TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') as ts_pretty_ms,
       cast (ts as bigint) as ts,
       app_id,
       user_id,
        CAST(
               CASE WHEN json_extract_path_text (properties,'m_currency_received') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'m_currency_received')
               END AS integer
             ) AS currency_purchased_qty,
       0 as amount_paid,
       0 as usd_paid,
       Null as currency,
       0 as currency_used_qty,
       json_extract_path_text(properties,'d_currency_received_type') as currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type, 
       json_extract_path_text(properties,'data_version') AS data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
FROM raw_events.transaction
WHERE json_extract_path_text(properties,'d_transaction_type')='awarded' and trunc(ts_pretty)>=(select start_date from processed.tmp_start_date)
UNION ALL
SELECT md5(app_id||event||user_id||session_id||ts) as id,
       (TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') as ts_pretty_ms,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,
       0 AS currency_purchased_qty,
       0 AS amount_paid,
       0 AS usd_paid,
       NULL AS currency,
       CAST(json_extract_path_text (properties,'m_currency_spent') AS bigint) AS currency_used_qty,
       json_extract_path_text(properties,'d_currency_spent_type') AS currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
       FROM raw_events.transaction
WHERE json_extract_path_text(properties,'d_transaction_type')='currency_spent' and trunc(ts_pretty)>=(select start_date from processed.tmp_start_date)
UNION ALL
SELECT md5(app_id||event||user_id||session_id||ts) as id,
       (TIMESTAMP 'epoch' + ts::BIGINT * INTERVAL '0.001 Second ') as ts_pretty,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,       
       CAST(json_extract_path_text (properties,'m_currency_received') as bigint) AS currency_purchased_qty,
       cast(json_extract_path_text(properties,'amount') as numeric(14,4)) as amount_paid,
       CASE
               WHEN CAST(json_extract_path_text (properties,'m_currency_received') AS INT) > 0 
               THEN (CAST(json_extract_path_text (properties,'amount') AS INTEGER)*c.factor)/100 else 0 end as usd_paid,
       json_extract_path_text(properties,'currency') as currency,
       0 as currency_used_qty,       
       json_extract_path_text(properties,'d_currency_received_type') as currency_type,
       session_id,
       json_extract_path_text(properties,'iap_product_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'iap_product_name') AS action
FROM raw_events.payment t
       JOIN processed.currency c
    ON c.currency = json_extract_path_text (properties,'currency')
   AND TRUNC (ts_pretty) = c.dt and trunc(ts_pretty)>=(select start_date from processed.tmp_start_date);


update processed.fact_currency_transaction set currency_purchased_qty=100 where transaction_type='starter_pack';


--- dim_currency_balance to get previous balance

CREATE temp TABLE latest_transaction
AS
SELECT MAX(ts_pretty) AS ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id
FROM processed.processed_currency_transaction where TRUNC(ts_pretty) < (SELECT start_date FROM processed.tmp_start_date)
GROUP BY 6,7;

DROP TABLE processed.dim_currency_balance;

CREATE TABLE processed.dim_currency_balance 
AS
SELECT p.id,
       p.ts_pretty AS updated_ts,
       p.ts,
       p.app_id,
       p.user_id,
       p.currency_type,
       p.currency_balance,
       p.usd_balance,
       p.cost_per_unit,
       l.currency_purchased_qty,
       l.currency_used_qty,
       l.usd_paid,
       l.usd_used
FROM processed.processed_currency_transaction p
  JOIN latest_transaction l
    ON p.app_id = l.app_id
   AND l.user_id = p.user_id
   AND p.ts_pretty = l.ts where TRUNC(ts_pretty) < (SELECT start_date FROM processed.tmp_start_date);


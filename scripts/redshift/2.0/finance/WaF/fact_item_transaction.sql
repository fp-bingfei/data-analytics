-- update start date

---update finance.tmp_start_date set start_date = ?::DATE; 

----CREATE VIEW seq_0_to_10 AS (
    ---SELECT 0 AS i UNION ALL                                      
    ---SELECT 1 UNION ALL
    ---SELECT 2 UNION ALL    
    ---SELECT 3 UNION ALL
    ---SELECT 4 UNION ALL
    ---SELECT 5 UNION ALL
    ---SELECT 6 UNION ALL
    ---SELECT 7 UNION ALL
    ---SELECT 8 UNION ALL
    ---SELECT 9 UNION ALL  
    ---SELECT 10  
---);


--- insert into finance.fact_item_transaction


delete from finance.fact_item_transaction where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and event='payment';


/*----- insert item_used data
insert into finance.fact_item_transaction
    SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_class') AS item_class,   
       0 as item_purchased_qty, 
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'m_item_amount') as int) AS item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       0 as usd_paid,
       0 as cost_per_unit,
       data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action,
       Null as currency_type
       FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_used')) and json_extract_path_text (properties,'d_transaction_type')='item_used' and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;

-------- insert currency_spent and item_received data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class, 
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') as int) AS item_purchased_qty,
       0 as item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       cast(json_extract_path_text(properties,'m_currency_spent') as int)/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) as usd_paid,
       0 as cost_per_unit,
       data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action,       
       json_extract_path_text(properties,'d_currency_spent_type') as currency_type
         FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and json_extract_path_text (properties,'d_transaction_type')='currency_spent' and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;

-----insert awarded data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class, 
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') as int) AS item_purchased_qty,
       0 as item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       CAST(
               CASE WHEN json_extract_path_text (properties,'m_currency_spent') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'m_currency_spent')
               END AS integer
             )/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) AS usd_paid,
       0 as cost_per_unit,
       data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action,       
       json_extract_path_text(properties,'d_currency_spent_type') as currency_type
         FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and json_extract_path_text (properties,'d_transaction_type')='awarded' and json_extract_path_text (properties,'d_action')='user_register'
and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;

------ insert currency_spent and item_used data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_id') AS item_id,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_type') AS item_type,  
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_name') AS item_name,
       json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'d_item_class') AS item_class, 
       0 as item_purchased_qty,
       cast(json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text (properties,'c_items_used'),seq.i),'m_item_amount') as int) AS item_used_qty,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,          
       0 as usd_paid,
       0 as cost_per_unit,
       data_version,    
       json_extract_path_text(properties,'app_version') AS app_version,json_extract_path_text(properties,'d_action') AS action,
       Null as currency_type
    FROM finance.raw_transaction,
     seq_0_to_10 AS seq
WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_used')) and json_extract_path_text (properties,'d_transaction_type')='currency_spent' and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
;*/

----- insert payment data
insert into finance.fact_item_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
    ts_pretty,       
    cast (ts as bigint) as ts,
       app_id,
       user_id,
       session_id,
       event,
       item_id,
       item_type,
       item_name,
       item_class,
       item_purchased_qty,
       item_used_qty,      
       transaction_type,
       (usd_paid*c.factor) as usd_paid,
       0 as cost_per_unit,
       data_version,
       app_version,
       action,       
       currency_type
       
FROM (SELECT app_id,
             data_version,
             event,
             user_id,
             session_id,
             CAST(ts AS BIGINT) AS ts,
             ts_pretty,
             json_extract_path_text(properties,'app_version') AS app_version,
             json_extract_path_text(properties,'currency') AS currency,
             NULL AS currency_type,
             (CASE
               WHEN CAST(json_extract_path_text (properties,'m_currency_received') AS INT) > 0 THEN 0
               ELSE (CAST(json_extract_path_text (properties,'amount') AS INTEGER)) / 100
             END)/JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) AS usd_paid,
             json_extract_path_text(properties,'iap_product_type') AS transaction_type,
             json_extract_path_text(properties,'iap_product_name') AS action,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_id') AS item_id,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_type') AS item_type,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_name') AS item_name,
             json_extract_path_text(JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'d_item_class') AS item_class,
             CAST(json_extract_path_text (JSON_EXTRACT_ARRAY_ELEMENT_TEXT (json_extract_path_text (properties,'c_items_received'),seq.i),'m_item_amount') AS INT) AS item_purchased_qty,
             0 AS item_used_qty
      FROM finance.raw_payment,
           seq_0_to_10 AS seq
      WHERE seq.i < JSON_ARRAY_LENGTH (json_extract_path_text (properties,'c_items_received')) and trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
      ) t
  JOIN finance.currency c
    ON c.currency = t.currency
   AND TRUNC (t.ts_pretty) = c.dt;


-- delete inactive players
delete from finance.fact_item_transaction where item_purchased_qty=0 and item_used_qty=0;

/*--- update usd_paid using dim_currency_balance
create temp table update_usd_paid as select t.app_id,t.user_id,t.currency_type,t.ts_pretty, t.usd_paid*coalesce(d.cost_per_unit,0) as usd_paid, t.event,t.session_id from finance.fact_item_transaction t left join finance.agg_currency_transaction d on t.app_id=d.app_id and t.user_id=d.user_id and trunc(t.ts_pretty)=d.date and t.currency_type=d.currency_type where trunc(t.ts_pretty)>=(select start_date from finance.tmp_start_date) and event<>'payment';

update finance.fact_item_transaction set usd_paid=c.usd_paid from update_usd_paid c where finance.fact_item_transaction.app_id=c.app_id and finance.fact_item_transaction.user_id=c.user_id and finance.fact_item_transaction.ts_pretty=c.ts_pretty and finance.fact_item_transaction.session_id=c.session_id and finance.fact_item_transaction.currency_type=c.currency_type and finance.fact_item_transaction.event=c.event and trunc(finance.fact_item_transaction.ts_pretty)>=(select start_date from finance.tmp_start_date) and finance.fact_item_transaction.event<>'payment';

update finance.fact_item_transaction set cost_per_unit=usd_paid/item_purchased_qty where item_purchased_qty>0;
	
update finance.fact_item_transaction set currency_type = 'NULL' WHERE (currency_type = '' OR currency_type IS NULL);

-- agg to avoid duplicates within same ts

delete from finance.fact_item_transaction_agg;
insert into finance.fact_item_transaction_agg
select id,
ts_pretty,
ts,
app_id,
user_id,
session_id,
event,
item_id,
item_type,
item_name,
item_class,
sum(item_purchased_qty) as item_purchased_qty,
0 as item_used_qty,
transaction_type,
sum(usd_paid) as usd_paid,
0 as cost_per_unit,
data_version,
app_version,
action, 
currency_type
from finance.fact_item_transaction where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,14,17,18,19,20;


insert into finance.fact_item_transaction_agg
select id,
ts_pretty,
ts,
app_id,
user_id,
session_id,
event,
item_id,
item_type,
item_name,
item_class,
0 as item_purchased_qty,
sum(item_used_qty) as item_used_qty,
transaction_type,
0 as usd_paid,
0 as cost_per_unit,
data_version,
app_version,
action, 
currency_type
from finance.fact_item_transaction where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,14,17,18,19,20;


update finance.fact_item_transaction_agg set cost_per_unit=usd_paid/item_purchased_qty where item_purchased_qty>0;
    

--- dim_consumable_item_balance by batch_id

CREATE temp TABLE latest_transaction
 AS
 SELECT MAX(ts_pretty) AS ts_pretty,
        SUM(item_used_qty) AS item_used_qty,
        SUM(item_purchased_qty) AS item_purchased_qty,
        SUM(usd_paid) AS usd_paid,
        SUM(usd_used) AS usd_used,
        app_id,
        user_id,
        item_id,
        item_type,
        item_name,
        item_class,
        currency_type,
        batch_id
FROM finance.processed_consumable_item_transaction
where TRUNC(ts_pretty) < (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 6,7,8,9,10,11,12,13;
 


DROP TABLE if exists finance.dim_consumable_item_balance;
CREATE TABLE finance.dim_consumable_item_balance 
 AS
SELECT p.id,
        p.ts_pretty AS updated_ts,
        p.batch_id,
        p.app_id,
        p.user_id,
        p.item_id,
        p.item_type,
        p.item_name,
        p.item_class,
        p.currency_type,
        p.batch_item_balance,
        p.batch_usd_balance
FROM finance.processed_consumable_item_transaction p
   JOIN latest_transaction l
     ON p.app_id = l.app_id
    AND l.user_id = p.user_id
    AND p.ts_pretty = l.ts_pretty
    AND l.item_id = p.item_id
   AND l.item_type=p.item_type
   AND l.item_name=p.item_name
   AND l.item_class=p.item_class
   AND l.currency_type=p.currency_type
   AND l.batch_id = p.batch_id where TRUNC(p.ts_pretty) < (SELECT start_date FROM finance.tmp_start_date);

*/
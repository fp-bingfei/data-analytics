------ payers data
create temp table payers as
select app_id,user_id,
install_date,
country,
is_payer, conversion_ts from processed.dim_user where is_payer=1;


------ agg_durable_item_transaction

delete from finance.agg_durable_item_transaction where date>=(select start_date from finance.tmp_start_date);

insert into finance.agg_durable_item_transaction 
SELECT TRUNC(ts_pretty) AS DATE,
       t.app_id,
       user_id,
       t.install_date,
       trunc(conversion_ts) as conversion_date,
       t.country,
       item_id,
       item_type,
       item_name,
       item_class,
       SUM(item_purchased_qty) AS item_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       l.predict AS lt_days,
       dateadd(day,l.predict,t.install_date) AS expiration_date,
       CASE
         WHEN TRUNC(ts_pretty) < dateadd (day,l.predict,t.install_date) THEN SUM(item_purchased_qty) / datediff ('day',TRUNC(ts_pretty),dateadd (day,l.predict,t.install_date))::numeric(14,4)
         ELSE SUM(item_purchased_qty)
       END AS item_used_qty_daily,
       CASE
         WHEN TRUNC(ts_pretty) < dateadd (day,l.predict,t.install_date) THEN SUM(usd_paid) / datediff ('day',TRUNC(ts_pretty),dateadd (day,l.predict,t.install_date))::numeric(14,4)
         ELSE SUM(usd_paid)::numeric(14,4)
       END AS usd_used_daily,
       transaction_type,
       action from (select ts_pretty, 
       md5(t.app_id||t.user_id) as user_key,
       t.app_id,
       t.user_id,
       u.install_date,
       u.conversion_ts,
       u.country,
       item_id,
       item_type,
       item_name,
       item_class,
       item_purchased_qty,
       usd_paid,
       transaction_type,
       action
FROM finance.fact_item_transaction t
  JOIN payers u
    ON t.app_id = u.app_id
   AND t.user_id = u.user_id
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)  and 
item_class = 'durable' and u.is_payer=1
)t
left join finance.user_life_time l on  t.user_key=l.user_key
GROUP BY 1,2,3,4,5,6,7,8,9,10,13,14,17,18;
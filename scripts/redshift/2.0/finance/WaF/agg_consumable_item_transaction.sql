--- delete inactive players
DELETE
FROM finance.processed_consumable_item_transaction
WHERE item_purchased_qty = 0
AND   item_used_qty = 0;


--- to delete the transactions whose item purchased is less than used
create temp table neg_bal as
select distinct app_id,user_id,item_id,item_name,item_type,item_class from finance.processed_consumable_item_transaction where item_balance<0;


delete from finance.processed_consumable_item_transaction where (app_id,user_id,item_id,item_name,item_type,item_class) in (select app_id,user_id,item_id,item_name,item_type,item_class from neg_bal);


---- delete duplicate records

CREATE temp TABLE delete_dup
AS
SELECT COUNT(1),
       user_id,
       app_id,
       item_id,
       item_name,
       item_type,
       item_class,
       ts_pretty
FROM finance.processed_consumable_item_transaction
GROUP BY 2,3,4,5,6,7,8
HAVING COUNT(1) > 1;

DELETE
FROM finance.processed_consumable_item_transaction USING delete_dup d
WHERE finance.processed_consumable_item_transaction.app_id = d.app_id
AND   finance.processed_consumable_item_transaction.user_id = d.user_id
AND   finance.processed_consumable_item_transaction.item_id = d.item_id
AND   finance.processed_consumable_item_transaction.item_name = d.item_name
AND   finance.processed_consumable_item_transaction.item_type = d.item_type
AND   finance.processed_consumable_item_transaction.item_class = d.item_class;



---- temp table for finance.agg_consumable_item_transaction

CREATE temp TABLE item_last_transaction
AS
SELECT TRUNC(ts_pretty) AS DATE,
       MAX(ts_pretty) AS ts,
       SUM(item_used_qty) AS item_used_qty,
       SUM(item_purchased_qty) AS item_purchased_qty,
       sum(case when transaction_type<>'currency_spent' and transaction_type<>'awarded' then usd_paid else 0 end) AS iap_usd_paid,
       sum(case when transaction_type='currency_spent' then usd_paid else 0 end) AS currency_usd_paid,
       sum(usd_paid) as total_usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class
FROM finance.processed_consumable_item_transaction
WHERE TRUNC(ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 1,9,10,11,12,13,14;

----- payer data

create temp table payers as
select app_id,user_id, 1 as is_payer, min(ts_pretty) as conversion_ts from finance.raw_payment group by 1,2;




--- delete last N days of data
DELETE
FROM finance.agg_consumable_item_transaction
WHERE DATE>= (SELECT start_date FROM finance.tmp_start_date);

INSERT INTO finance.agg_consumable_item_transaction
SELECT DATE,
       LAG(item_balance,1) OVER (PARTITION BY app_id,user_id,item_id,item_name,item_type,item_class ORDER BY DATE) AS begin_item_balance,
       item_balance,
       LAG(usd_balance,1) OVER (PARTITION BY app_id,user_id,item_id,item_name,item_type,item_class ORDER BY DATE) AS begin_usd_balance,
       usd_balance,
       cost_per_unit,
       app_id,
       user_id,
       item_id,
       item_used_qty,
       item_purchased_qty,
       total_usd_paid ,
       usd_used,
       item_name,
       item_type,
       item_class,
       iap_usd_paid,
       currency_usd_paid
FROM (SELECT TRUNC(c.ts_pretty) AS DATE,
             item_balance,
             usd_balance,
             c.cost_per_unit,
             c.app_id,
             c.user_id,
             c.item_id,
             t.item_used_qty,
             t.item_purchased_qty,
             t.total_usd_paid,
             t.iap_usd_paid,
             t.currency_usd_paid,
             t.usd_used,
             c.item_name,
             c.item_type,
             c.item_class
      FROM finance.processed_consumable_item_transaction c
        JOIN payers u
          ON c.user_id = u.user_id
         AND c.app_id = u.app_id
        JOIN item_last_transaction t
          ON t.app_id = c.app_id
         AND t.user_id = c.user_id
         AND t.ts = c.ts_pretty
         AND t.item_id = c.item_id 
         and t.item_name=c.item_name 
         and t.item_type=c.item_type 
         and t.item_class = c.item_class
      WHERE u.is_payer = 1
      AND   TRUNC(c.ts_pretty) >= (SELECT start_date FROM finance.tmp_start_date)
      ) d;



---- temp table to get month end balances
CREATE temp TABLE month_end_transaction
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(item_purchased_qty) AS item_purchased_qty,
       SUM(item_used_qty) AS item_used_qty,
       sum(case when transaction_type<>'currency_spent' and 
transaction_type<>'awarded' then usd_paid else 0 end) AS iap_usd_paid,
       sum(case when transaction_type='currency_spent' then usd_paid 
else 0 end) AS currency_usd_paid,
       sum(usd_paid) as total_usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class
FROM finance.processed_consumable_item_transaction
WHERE datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
GROUP BY 1,2,10,11,12,13,14,15;

DELETE
FROM finance.agg_consumable_item_transaction_monthly
WHERE month = datepart ('month',CURRENT_DATE)
AND   year = datepart ('year',CURRENT_DATE);

---- insert into finance.agg_consumable_item_transaction_monthly
INSERT INTO finance.agg_consumable_item_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.item_id,
       c.item_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.item_purchased_qty,
       t.item_used_qty,
       t.total_usd_paid,
       t.usd_used,
       c.item_name,
       c.item_type,
       c.item_class,
       t.iap_usd_paid,
       t.currency_usd_paid
FROM finance.processed_consumable_item_transaction c
  JOIN payers u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN month_end_transaction t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id
   AND t.item_id = c.item_id
   AND t.item_name = c.item_name
   AND t.item_type = c.item_type
   AND t.item_class = c.item_class
   AND t.last_ts = c.ts_pretty
WHERE u.is_payer = 1
AND   datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
;

------ to insert new payers data

CREATE temp TABLE month_end_transaction_payer_today
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(item_purchased_qty) AS item_purchased_qty,
       SUM(item_used_qty) AS item_used_qty,
       sum(case when transaction_type<>'currency_spent' and 
transaction_type<>'awarded' then usd_paid else 0 end) AS iap_usd_paid,
       sum(case when transaction_type='currency_spent' then usd_paid 
else 0 end) AS currency_usd_paid,
       sum(usd_paid) as total_usd_paid,
       SUM(usd_used) AS usd_used,
       p.app_id,
       p.user_id,
       item_id,
       item_name,
       item_type,
       item_class
FROM finance.processed_consumable_item_transaction p
join payers s on p.app_id=s.app_id and p.user_id=s.user_id
WHERE datepart ('month',ts_pretty) = datepart ('month',dateadd (month,-1,CURRENT_DATE))
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE) and trunc(conversion_ts)>=(select start_date from finance.tmp_start_date)
GROUP BY 1,2,10,11,12,13,14,15;

DELETE
FROM finance.agg_consumable_item_transaction_monthly
using month_end_transaction_payer_today p
where finance.agg_consumable_item_transaction_monthly.app_id=p.app_id and 
finance.agg_consumable_item_transaction_monthly.user_id=p.user_id and 
finance.agg_consumable_item_transaction_monthly.item_id=p.item_id and 
finance.agg_consumable_item_transaction_monthly.item_name=p.item_name and 
finance.agg_consumable_item_transaction_monthly.item_type=p.item_type and 
finance.agg_consumable_item_transaction_monthly.month=p.month and 
finance.agg_consumable_item_transaction_monthly.year=p.year;

---- insert into finance.agg_consumable_item_transaction_monthly
INSERT INTO finance.agg_consumable_item_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.item_id,
       c.item_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.item_purchased_qty,
       t.item_used_qty,
       t.total_usd_paid,
       t.usd_used,
       c.item_name,
       c.item_type,
       c.item_class,
       t.iap_usd_paid,
       t.currency_usd_paid
FROM finance.processed_consumable_item_transaction c
  JOIN payers u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN month_end_transaction_payer_today t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id
   AND t.item_id = c.item_id
   AND t.item_name = c.item_name
   AND t.item_type = c.item_type
   AND t.item_class = c.item_class
   AND t.last_ts = c.ts_pretty
WHERE u.is_payer = 1
;



----  updating monthly_transaction with inactive players
CREATE temp TABLE player_update
AS
SELECT app_id,
       user_id,
       item_id,
       item_name,
       item_type,
       item_class,
       0 AS item_purchased_qty,
       0 AS item_used_qty,
       item_balance,
       0 AS usd_used,
       0 AS total_usd_paid,
       0 as iap_usd_paid,
       0 as currency_usd_paid,
       usd_balance
FROM finance.agg_consumable_item_transaction_monthly
WHERE month = datepart ('month',dateadd (month,-1,CURRENT_DATE))
AND   (app_id,user_id,item_id,item_name,item_type,item_class) NOT IN (SELECT app_id,
                                                user_id,
                                                item_id,item_name,item_type,item_class
                                         FROM finance.agg_consumable_item_transaction_monthly
                                         WHERE MONTH = datepart ('month',CURRENT_DATE));

INSERT INTO finance.agg_consumable_item_transaction_monthly
SELECT app_id,
       user_id,
       item_id,
       item_balance,
       usd_balance,
       datepart(year,CURRENT_DATE) AS year,
       datepart(month,CURRENT_DATE) AS month,
       item_purchased_qty,
       item_used_qty,
       total_usd_paid,
       usd_used,
       item_name,
       item_type,
       item_class,
       iap_usd_paid,
       currency_usd_paid
FROM player_update;







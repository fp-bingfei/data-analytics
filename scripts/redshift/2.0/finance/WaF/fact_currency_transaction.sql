----- delete from fact_currency_transaction
delete from finance.fact_currency_transaction where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date) and usd_paid>0;

/*----- insert using raw_transaction and raw_payment
----  awarded data
insert into finance.fact_currency_transaction
    SELECT md5(app_id||event||user_id||session_id||ts) as id,
       ts_pretty,
       cast (ts as bigint) as ts,
       app_id,
       user_id,
       nvl(nullif(json_extract_path_text(properties,'d_currency_received'), '')::int, 0)  AS currency_purchased_qty,
       0 as amount_paid,
       0 as usd_paid,
       Null as currency,
       0 as currency_used_qty,
       json_extract_path_text(properties,'d_currency_received_type') as currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type, 
       json_extract_path_text(properties,'data_version') AS data_version,
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
FROM finance.raw_transaction
WHERE json_extract_path_text(properties,'d_transaction_type')='awarded' and trunc(ts_pretty)>=(select start_date from 
finance.tmp_start_date);

----- currency_spent data
insert into finance.fact_currency_transaction
SELECT md5(app_id||event||user_id||session_id||ts) as id,
       ts_pretty,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,
       0 AS currency_purchased_qty,
       0 AS amount_paid,
       0 AS usd_paid,
       NULL AS currency,
       nvl(nullif(json_extract_path_text(properties,'d_currency_spent'), '')::int, 0) AS currency_used_qty,
       json_extract_path_text(properties,'d_currency_spent_type') AS currency_type,
       session_id,
       json_extract_path_text(properties,'d_transaction_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'d_action') AS action
       FROM finance.raw_transaction
WHERE json_extract_path_text(properties,'d_transaction_type')='currency_spent' and trunc(ts_pretty)>=(select start_date 
from finance.tmp_start_date);*/

------  payment data
insert into finance.fact_currency_transaction
SELECT id,
       ts_pretty,
       ts,
       app_id,
       user_id,
       coalesce(p.amount,t.currency_purchased_qty) as currency_purchased_qty,
       amount_paid,
       usd_paid,
       t.currency,
       currency_used_qty,
       coalesce(p.currency,t.currency_type) as currency_type,
       session_id,
       transaction_type,
       data_version,
       app_version,
       action
       from (select
       md5(app_id||event||user_id||session_id||ts) as id,
       ts_pretty,
       CAST(ts AS bigint) AS ts,
       app_id,
       user_id,       
       nvl(nullif(json_extract_path_text(properties,'m_currency_received'), '')::int, 0)  AS currency_purchased_qty,
       cast(json_extract_path_text(properties,'amount') as numeric(14,4)) as amount_paid,
       CASE
               WHEN CAST(json_extract_path_text (properties,'m_currency_received') AS INT) > 0 
               THEN (CAST(json_extract_path_text (properties,'amount') AS INTEGER)*c.factor)/100 else 0 end as usd_paid,
       json_extract_path_text(properties,'currency') as currency,
       0 as currency_used_qty,       
       json_extract_path_text(properties,'d_currency_received_type') as currency_type,
       session_id,
       json_extract_path_text(properties,'iap_product_type') AS transaction_type,
       json_extract_path_text(properties,'data_version') AS data_version,       
       json_extract_path_text(properties,'app_version') AS app_version,
       json_extract_path_text(properties,'iap_product_name') AS action,
       row_number() over(partition by app_id,user_id,json_extract_path_text(properties,'transaction_id') order by ts) as 
rnum
FROM finance.raw_payment p JOIN finance.currency c
    ON c.currency = json_extract_path_text(properties,'currency')
   AND TRUNC (p.ts_pretty) = c.dt where trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)) t
   left join finance.package_list p on t.currency_type=p.package
      where t.rnum=1;
      



delete from finance.fact_currency_transaction where currency_purchased_qty=0 and currency_used_qty=0;

update finance.fact_currency_transaction set currency_type='rc' where currency_type='reward_points';commit;

/*----- grouping together multiple transactions within same ts

delete from finance.fact_currency_transaction_agg;

insert into finance.fact_currency_transaction_agg
select id,
ts_pretty,
ts,
app_id,
user_id,
sum(currency_purchased_qty) as currency_purchased_qty,
sum(amount_paid) as amount_paid,
sum(usd_paid) as usd_paid,
currency,
0 as currency_used_qty,
currency_type,
session_id,
transaction_type,
data_version,
app_version,
action from finance.fact_currency_transaction
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
group by 1,2,3,4,5,9,11,12,13,14,15,16;

insert into finance.fact_currency_transaction_agg
select id,
ts_pretty,
ts,
app_id,
user_id,
0 as currency_purchased_qty,
0 as amount_paid,
0 as usd_paid,
currency,
sum(currency_used_qty) as currency_used_qty,
currency_type,
session_id,
transaction_type,
data_version,
app_version,
action from finance.fact_currency_transaction
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)
group by 1,2,3,4,5,9,11,12,13,14,15,16;


delete from finance.fact_currency_transaction_agg where currency_purchased_qty=0 and currency_used_qty=0;

update finance.fact_currency_transaction_agg set action='Unknown' where (action is Null or action='');




CREATE temp TABLE latest_transaction
AS
SELECT MAX(ts_pretty) AS ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id,
       currency_type
FROM finance.processed_currency_transaction where TRUNC(ts_pretty) < (SELECT start_date FROM finance.tmp_start_date)
GROUP BY 6,7,8;


DROP TABLE finance.dim_currency_balance;

CREATE TABLE finance.dim_currency_balance 
AS
SELECT p.id,
       p.ts_pretty AS updated_ts,
       p.ts,
       p.app_id,
       p.user_id,
       p.currency_type,
       p.currency_balance,
       p.usd_balance,
       p.cost_per_unit,
       l.currency_purchased_qty,
       l.currency_used_qty,
       l.usd_paid,
       l.usd_used
FROM finance.processed_currency_transaction p
  JOIN latest_transaction l
    ON p.app_id = l.app_id
   AND l.user_id = p.user_id and p.currency_type=l.currency_type
   AND p.ts_pretty = l.ts where TRUNC(ts_pretty) < (SELECT start_date FROM finance.tmp_start_date);

*/



CREATE TABLE finance.agg_consumable_item_transaction
(
	date DATE,
	begin_item_balance BIGINT,
	item_balance BIGINT,
	begin_usd_balance NUMERIC(14, 4),
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	app_id VARCHAR(64),
	user_id VARCHAR(100) DISTKEY,
	item_id VARCHAR(32),
	item_used_qty BIGINT,
	item_purchased_qty BIGINT,
	total_usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4),
	item_name VARCHAR(64),
	item_type VARCHAR(64),
	item_class VARCHAR(32),
	iap_usd_paid NUMERIC(14, 4),
	currency_usd_paid NUMERIC(14, 4)
)
SORTKEY
(
	app_id,
	date
);


CREATE TABLE finance.agg_consumable_item_transaction_monthly
(
	app_id VARCHAR(64),
	user_id VARCHAR(100) DISTKEY,
	item_id VARCHAR(32),
	item_balance BIGINT,
	usd_balance NUMERIC(14, 4),
	year INTEGER,
	month INTEGER,
	item_purchased_qty BIGINT,
	item_used_qty BIGINT,
	total_usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4),
	item_name VARCHAR(64),
	item_type VARCHAR(64),
	item_class VARCHAR(32),
	iap_usd_paid NUMERIC(14, 4),
	currency_usd_paid NUMERIC(14, 4)
)
SORTKEY
(
	app_id,
	year,
	month
);


CREATE TABLE finance.agg_currency_transaction
(
	date DATE,
	begin_currency_balance INTEGER,
	currency_balance INTEGER,
	begin_usd_balance NUMERIC(14, 4),
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	currency_type VARCHAR(32),
	app_id VARCHAR(64) ENCODE bytedict,
	user_id VARCHAR(100) ENCODE bytedict DISTKEY,
	currency_used_qty BIGINT,
	currency_purchased_qty BIGINT,
	usd_paid NUMERIC(20, 4),
	usd_used NUMERIC(20, 4)
)
SORTKEY
(
	app_id,
	date
);


CREATE TABLE finance.agg_currency_transaction_monthly
(
	app_id VARCHAR(64),
	user_id VARCHAR(100) DISTKEY,
	currency_type VARCHAR(32),
	currency_balance INTEGER,
	usd_balance NUMERIC(14, 4),
	year INTEGER,
	month INTEGER,
	currency_purchased_qty BIGINT,
	currency_used_qty BIGINT,
	usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4)
)
SORTKEY
(
	app_id,
	year,
	month
);


CREATE TABLE finance.agg_durable_item_transaction
(
	date DATE,
	app_id VARCHAR(64),
	user_id VARCHAR(64),
	install_date DATE,
	conversion_date DATE,
	install_country VARCHAR(64),
	item_id VARCHAR(64),
	item_type VARCHAR(64),
	item_name VARCHAR(64),
	item_class VARCHAR(32),
	item_purchased_qty INTEGER,
	usd_paid NUMERIC(14, 4),
	lt_days INTEGER,
	expiration_date DATE,
	item_used_qty_daily NUMERIC(14, 4),
	usd_used_daily NUMERIC(14, 4),
	transaction_type VARCHAR(64),
	action VARCHAR(64)
)
DISTSTYLE EVEN;


CREATE TABLE finance.dim_consumable_item_balance
(
	id VARCHAR(128),
	updated_ts TIMESTAMP,
	batch_id BIGINT,
	app_id VARCHAR(64),
	user_id VARCHAR(64),
	item_id VARCHAR(64),
	item_type VARCHAR(64),
	item_name VARCHAR(64),
	item_class VARCHAR(32),
	currency_type VARCHAR(64),
	batch_item_balance BIGINT,
	batch_usd_balance NUMERIC(14, 4)
)
DISTSTYLE EVEN;


CREATE TABLE finance.dim_currency_balance
(
	id VARCHAR(128),
	updated_ts TIMESTAMP,
	ts BIGINT,
	app_id VARCHAR(64),
	user_id VARCHAR(64),
	currency_type VARCHAR(64),
	currency_balance BIGINT,
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	currency_purchased_qty BIGINT,
	currency_used_qty BIGINT,
	usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4)
)
DISTSTYLE EVEN;


CREATE TABLE finance.fact_currency_transaction
(
	id VARCHAR(100) ENCODE bytedict,
	ts_pretty TIMESTAMP,
	ts BIGINT,
	app_id VARCHAR(64) ENCODE bytedict,
	user_id VARCHAR(64) ENCODE bytedict DISTKEY,
	currency_purchased_qty BIGINT,
	amount_paid NUMERIC(14, 4),
	usd_paid NUMERIC(14, 4),
	currency VARCHAR(16) ENCODE bytedict,
	currency_used_qty BIGINT,
	currency_type VARCHAR(64) ENCODE bytedict,
	session_id VARCHAR(128) ENCODE bytedict,
	transaction_type VARCHAR(64) ENCODE bytedict,
	data_version VARCHAR(16) ENCODE bytedict,
	app_version VARCHAR(64) ENCODE bytedict,
	action VARCHAR(64) ENCODE bytedict
)
SORTKEY
(
	app_id,
	ts_pretty
);




CREATE TABLE finance.fact_item_transaction
(
	id VARCHAR(128) ENCODE bytedict,
	ts_pretty TIMESTAMP ENCODE delta,
	ts BIGINT,
	app_id VARCHAR(64) ENCODE bytedict,
	user_id VARCHAR(64) ENCODE bytedict DISTKEY,
	session_id VARCHAR(128) ENCODE bytedict,
	event VARCHAR(64) ENCODE bytedict,
	item_id VARCHAR(64) ENCODE bytedict,
	item_type VARCHAR(64) ENCODE bytedict,
	item_name VARCHAR(64) ENCODE bytedict,
	item_class VARCHAR(32) ENCODE bytedict,
	item_purchased_qty INTEGER,
	item_used_qty INTEGER,
	transaction_type VARCHAR(64) ENCODE bytedict,
	usd_paid NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	data_version VARCHAR(16) ENCODE bytedict,
	app_version VARCHAR(64) ENCODE bytedict,
	action VARCHAR(64) ENCODE bytedict,
	currency_type VARCHAR(64) ENCODE bytedict
)
SORTKEY
(
	app_id,
	ts_pretty
);


CREATE TABLE finance.processed_consumable_item_transaction
(
	id VARCHAR(128) ENCODE bytedict,
	ts_pretty TIMESTAMP,
	batch_id BIGINT,
	app_id VARCHAR(64) ENCODE bytedict,
	user_id VARCHAR(64) ENCODE bytedict DISTKEY,
	session_id VARCHAR(128) ENCODE bytedict,
	transaction_type VARCHAR(64) ENCODE bytedict,
	item_id VARCHAR(64) ENCODE bytedict,
	item_type VARCHAR(64) ENCODE bytedict,
	item_name VARCHAR(64) ENCODE bytedict,
	item_class VARCHAR(32) ENCODE bytedict,
	item_purchased_qty INTEGER,
	usd_paid NUMERIC(14, 4),
	item_used_qty INTEGER,
	usd_used NUMERIC(14, 4),
	batch_item_balance BIGINT,
	batch_usd_balance NUMERIC(14, 4),
	item_balance BIGINT,
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	action VARCHAR(64) ENCODE bytedict,
	currency_type VARCHAR(64) ENCODE bytedict
)
SORTKEY
(
	app_id,
	ts_pretty
);


CREATE TABLE finance.processed_currency_transaction
(
	id VARCHAR(128) ENCODE bytedict,
	ts_pretty TIMESTAMP,
	ts BIGINT,
	app_id VARCHAR(64) ENCODE bytedict,
	user_id VARCHAR(64) ENCODE bytedict DISTKEY,
	currency_purchased_qty BIGINT,
	usd_paid NUMERIC(14, 4),
	currency_used_qty BIGINT,
	currency_type VARCHAR(64) ENCODE bytedict,
	session_id VARCHAR(128) ENCODE bytedict,
	transaction_type VARCHAR(64) ENCODE bytedict,
	usd_used NUMERIC(14, 4),
	currency_balance BIGINT,
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	action VARCHAR(64) ENCODE bytedict
)
SORTKEY
(
	app_id,
	ts_pretty
);


CREATE TABLE finance.raw_payment
(
	data_version VARCHAR(10),
	app_id VARCHAR(64),
	ts VARCHAR(20),
	ts_pretty TIMESTAMP,
	user_id VARCHAR(128),
	session_id VARCHAR(128),
	event VARCHAR(128),
	properties VARCHAR(20000) ENCODE lzo,
	collections VARCHAR(20000)
)
DISTSTYLE EVEN;


CREATE TABLE finance.raw_transaction
(
	data_version VARCHAR(10) ENCODE bytedict,
	app_id VARCHAR(64) ENCODE lzo,
	ts VARCHAR(20) ENCODE lzo,
	ts_pretty TIMESTAMP ENCODE delta,
	user_id VARCHAR(128) ENCODE lzo DISTKEY,
	session_id VARCHAR(128) ENCODE lzo,
	event VARCHAR(128) ENCODE bytedict,
	properties VARCHAR(20000) ENCODE lzo,
	collections VARCHAR(20000) ENCODE lzo
)
SORTKEY
(
	app_id,
	ts_pretty,
	event
);


CREATE TABLE finance.tmp_start_date
(
	start_date TIMESTAMP
)
DISTSTYLE EVEN;


CREATE TABLE finance.user_lt
(
	app_id VARCHAR(64) ENCODE lzo,
	install_country VARCHAR(64) ENCODE bytedict,
	conversion_year INTEGER,
	conversion_month INTEGER,
	lt_days INTEGER
)
DISTSTYLE EVEN
SORTKEY
(
	conversion_year,
	conversion_month
);









CREATE OR REPLACE VIEW public.seq_0_to_10 AS 
((((((((( SELECT 0 AS i
UNION ALL 
 SELECT 1 AS i)
UNION ALL 
 SELECT 2 AS i)
UNION ALL 
 SELECT 3 AS i)
UNION ALL 
 SELECT 4 AS i)
UNION ALL 
 SELECT 5 AS i)
UNION ALL 
 SELECT 6 AS i)
UNION ALL 
 SELECT 7 AS i)
UNION ALL 
 SELECT 8 AS i)
UNION ALL 
 SELECT 9 AS i)
UNION ALL 
 SELECT 10 AS i;



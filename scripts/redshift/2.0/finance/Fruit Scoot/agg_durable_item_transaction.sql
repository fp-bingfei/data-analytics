

------ payers data
create temp table payers as
select app_id,user_id,
trunc(cast(json_extract_path_text(properties,'install_ts_pretty') as timestamp)) as install_date,
json_extract_path_text(properties,'country_code') as country,
1 as is_payer, min(ts_pretty) as conversion_ts from finance.raw_payment group by 1,2,3,4;


------ agg_durable_item_transaction

delete from finance.agg_durable_item_transaction where date>=(select start_date from finance.tmp_start_date);

insert into finance.agg_durable_item_transaction 
SELECT TRUNC(ts_pretty) AS DATE,
       t.app_id,
       user_id,
       install_date,
       trunc(conversion_ts) as conversion_date,
       t.country,
       item_id,
       item_type,
       item_name,
       item_class,
       SUM(item_purchased_qty) AS item_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       coalesce(l.lt_days,180) AS lt_days,
       dateadd(day,coalesce(l.lt_days,180),trunc(conversion_ts)) AS expiration_date,
       CASE
         WHEN TRUNC(ts_pretty) < dateadd (day,coalesce(l.lt_days,180),trunc(conversion_ts)) THEN SUM(item_purchased_qty) / datediff ('day',TRUNC(ts_pretty),dateadd (day,coalesce(l.lt_days,180),trunc(conversion_ts)))::numeric(14,4)
         ELSE SUM(item_purchased_qty)
       END AS item_used_qty_daily,
       CASE
         WHEN TRUNC(ts_pretty) < dateadd (day,coalesce(l.lt_days,180),trunc(conversion_ts)) THEN SUM(usd_paid) / datediff ('day',TRUNC(ts_pretty),dateadd (day,coalesce(l.lt_days,180),trunc(conversion_ts)))::numeric(14,4)
         ELSE SUM(usd_paid)::numeric(14,4)
       END AS usd_used_daily,
       transaction_type,
       action from (select ts_pretty,
       t.app_id,
       t.user_id,
       u.install_date,
       u.conversion_ts,
       u.country,
       item_id,
       item_type,
       item_name,
       item_class,
       item_purchased_qty,
       usd_paid,
       transaction_type,
       action
FROM finance.fact_item_transaction t
  JOIN payers u
    ON t.app_id = u.app_id
   AND t.user_id = u.user_id
WHERE trunc(ts_pretty)>=(select start_date from finance.tmp_start_date)  and 
item_class = 'durable' and u.is_payer=1
)t
left join finance.user_lt l on t.app_id=l.app_id and t.country=l.install_country and datepart('month',conversion_ts)=l.conversion_month
and datepart('year',conversion_ts)=l.conversion_year
GROUP BY 1,2,3,4,5,6,7,8,9,10,13,14,17,18;

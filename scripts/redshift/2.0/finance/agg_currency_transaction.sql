

--- delete empty records
DELETE
FROM processed.processed_currency_transaction
WHERE currency_purchased_qty = 0
AND   currency_used_qty = 0;

update processed.processed_currency_transaction set usd_balance=0 where usd_balance>=-0.01 and usd_balance<=0.01;

--- delete users who starts spending without purchasing or getting free --acorns
create temp table first_transaction as
select p.user_id,p.app_id,ts_pretty,currency_balance from processed.processed_currency_transaction p
join (select app_id,user_id,min(ts_pretty) as ts from processed.processed_currency_transaction group by 1,2)t
on p.app_id=t.app_id and p.user_id=t.user_id and p.ts_pretty=t.ts where currency_balance<0;

DELETE
FROM processed.processed_currency_transaction USING first_transaction d
WHERE processed.processed_currency_transaction.app_id = d.app_id
AND   processed.processed_currency_transaction.user_id = d.user_id;

----- delete duplicate records from processed.processed_currency_transaction
CREATE temp TABLE delete_dup
AS
SELECT COUNT(1),
       user_id,
       app_id,
       ts_pretty
FROM processed.processed_currency_transaction
GROUP BY 2,3,4
HAVING COUNT(1) > 1;

DELETE
FROM processed.processed_currency_transaction USING delete_dup d
WHERE processed.processed_currency_transaction.app_id = d.app_id
AND   processed.processed_currency_transaction.user_id = d.user_id;

---- temp table for processed.agg_currency_transaction
CREATE temp TABLE last_transaction
AS
SELECT TRUNC(ts_pretty) AS DATE,
       MAX(ts_pretty) AS ts,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id
FROM processed.processed_currency_transaction
WHERE TRUNC(ts_pretty) >= (SELECT start_date FROM processed.tmp_start_date)
GROUP BY 1,7,8;

--- delete last N days of data
DELETE
FROM processed.agg_currency_transaction
WHERE DATE>= (SELECT start_date FROM processed.tmp_start_date);

---- insert to processed.agg_currency_transaction
INSERT INTO processed.agg_currency_transaction
SELECT DATE,
       LAG(currency_balance,1) OVER (PARTITION BY app_id,user_id ORDER BY DATE) AS begin_currency_balance,
       currency_balance,
       LAG(usd_balance,1) OVER (PARTITION BY app_id,user_id ORDER BY DATE) AS begin_usd_balance,
       usd_balance,
       cost_per_unit,
       currency_type,
       app_id,
       user_id,
       currency_used_qty,
       currency_purchased_qty,
       usd_paid,
       usd_used
FROM (SELECT TRUNC(c.ts_pretty) AS DATE,
             currency_balance,
             usd_balance,
             cost_per_unit,
             c.currency_type,
             c.app_id,
             c.user_id,
             t.currency_used_qty,
             t.currency_purchased_qty,
             t.usd_paid,
             t.usd_used
      FROM processed.processed_currency_transaction c
        JOIN processed.dim_user u
          ON c.user_id = u.user_id
         AND c.app_id = u.app_id
        JOIN last_transaction t
          ON t.app_id = c.app_id
         AND t.user_id = c.user_id
         AND t.ts = c.ts_pretty
      WHERE u.is_payer = 1
      AND   TRUNC(c.ts_pretty) >= (SELECT start_date FROM processed.tmp_start_date)) d;

---- temp table to get month end balances
CREATE temp TABLE month_end_transaction
AS
SELECT datepart('year',ts_pretty) AS year,
       datepart('month',ts_pretty) AS month,
       MAX(ts_pretty) AS last_ts,
       SUM(currency_purchased_qty) AS currency_purchased_qty,
       SUM(currency_used_qty) AS currency_used_qty,
       SUM(usd_paid) AS usd_paid,
       SUM(usd_used) AS usd_used,
       app_id,
       user_id
FROM processed.processed_currency_transaction
WHERE datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE)
GROUP BY 1,2,8,9;

---- insert into processed.agg_currency_transaction_monthly
DELETE
FROM processed.agg_currency_transaction_monthly
WHERE month = datepart ('month',CURRENT_DATE)
AND   year = datepart ('year',CURRENT_DATE);

INSERT INTO processed.agg_currency_transaction_monthly
SELECT c.app_id,
       c.user_id,
       c.currency_type,
       c.currency_balance,
       c.usd_balance,
       t.year,
       t.month,
       t.currency_purchased_qty,
       t.currency_used_qty,
       t.usd_paid,
       t.usd_used
FROM processed.processed_currency_transaction c
  JOIN processed.dim_user u
    ON c.user_id = u.user_id
   AND c.app_id = u.app_id
  JOIN month_end_transaction t
    ON t.app_id = c.app_id
   AND t.user_id = c.user_id
   AND t.last_ts = c.ts_pretty
WHERE u.is_payer = 1
AND   datepart ('month',ts_pretty) = datepart ('month',CURRENT_DATE)
AND   datepart ('year',ts_pretty) = datepart ('year',CURRENT_DATE);

--- to delete the transactions whose currency purchased is less than used
CREATE temp TABLE negative_bal
AS
SELECT SUM(currency_purchased_qty) AS c_p,
       SUM(currency_used_qty) AS c_u,
       SUM(usd_paid) AS usd_paid,
       app_id,
       user_id
FROM processed.fact_currency_transaction
GROUP BY 4,
         5
HAVING SUM(currency_purchased_qty) < SUM(currency_used_qty);

DELETE
FROM processed.processed_currency_transaction USING negative_bal b
WHERE processed.processed_currency_transaction.app_id = b.app_id
AND   processed.processed_currency_transaction.user_id = b.user_id;

DELETE
FROM processed.agg_currency_transaction USING negative_bal b
WHERE processed.agg_currency_transaction.app_id = b.app_id
AND   processed.agg_currency_transaction.user_id = b.user_id;

DELETE
FROM processed.agg_currency_transaction_monthly USING negative_bal b
WHERE processed.agg_currency_transaction_monthly.app_id = b.app_id
AND   processed.agg_currency_transaction_monthly.user_id = b.user_id;

--- updating monthly_transaction with inactive players
CREATE temp TABLE player_update
AS
SELECT app_id,
       user_id,
       0 AS currency_purchased_qty,
       0 AS currency_used_qty,
       currency_balance,
       0 AS usd_used,
       0 AS usd_paid,
       usd_balance
FROM processed.agg_currency_transaction_monthly
WHERE month = datepart ('month',dateadd (month,-1,CURRENT_DATE))
AND   (app_id,user_id) NOT IN (SELECT app_id,
                                        user_id
                                 FROM processed.agg_currency_transaction_monthly
                                 WHERE MONTH = datepart (MONTH,CURRENT_DATE));

INSERT INTO processed.agg_currency_transaction_monthly
SELECT app_id,
       user_id,
       'acorns' AS currency_type,
       currency_balance,
       usd_balance,
       datepart(year,CURRENT_DATE) AS year,
       datepart(month,CURRENT_DATE) AS month,
       currency_purchased_qty,
       currency_used_qty,
       usd_paid,
       usd_used
FROM player_update;


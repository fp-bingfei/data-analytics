--Create the raw_events schema and tables
CREATE SCHEMA IF NOT EXISTS raw_events;

DROP TABLE IF EXISTS raw_events.payment CASCADE;

CREATE TABLE raw_events.payment 
(
  data_version   VARCHAR(10) ENCODE bytedict,
  app_id         VARCHAR(64) ENCODE lzo,
  ts             VARCHAR(20) ENCODE lzo,
  ts_pretty      TIMESTAMP ENCODE delta,
  user_id        VARCHAR(128) ENCODE lzo,
  session_id     VARCHAR(128) ENCODE lzo,
  event          VARCHAR(128) ENCODE bytedict,
  properties     VARCHAR(20000) ENCODE lzo,
  collections    VARCHAR(20000) ENCODE lzo
)
DISTKEY (user_id) SORTKEY (app_id,ts_pretty,event);

DROP TABLE IF EXISTS raw_events.transaction;

CREATE TABLE raw_events.transaction 
(
  data_version   VARCHAR(10) ENCODE bytedict,
  app_id         VARCHAR(64) ENCODE lzo,
  ts             VARCHAR(20) ENCODE lzo,
  ts_pretty      TIMESTAMP ENCODE delta,
  user_id        VARCHAR(128) ENCODE lzo,
  session_id     VARCHAR(128) ENCODE lzo,
  event          VARCHAR(128) ENCODE bytedict,
  properties     VARCHAR(20000) ENCODE lzo,
  collections    VARCHAR(20000) ENCODE lzo
)
DISTKEY (user_id) SORTKEY (app_id,ts_pretty,event);


--Create the processed schema and tables
CREATE SCHEMA IF NOT exists processed;

DROP TABLE IF EXISTS processed.currency;

CREATE TABLE processed.currency 
(
  id         VARCHAR(30) NOT NULL ENCODE LZO,
  dt         DATE NOT NULL ENCODE DELTA,
  currency   VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  factor     NUMERIC(15,10) NOT NULL ENCODE LZO,
  PRIMARY KEY (id)
)
SORTKEY (dt);

DROP TABLE IF EXISTS processed.dim_user;

CREATE TABLE processed.dim_user 
(
  id                     VARCHAR(128) NOT NULL ENCODE LZO,
  user_key               VARCHAR(128) NOT NULL ENCODE LZO,
  app_id                 VARCHAR(64) NOT NULL ENCODE LZO,
  app_version            VARCHAR(32) ENCODE LZO,
  user_id                VARCHAR(128) NOT NULL ENCODE LZO,
  facebook_id            VARCHAR(128) ENCODE LZO,
  install_ts             TIMESTAMP NOT NULL ENCODE DELTA,
  install_date           DATE NOT NULL ENCODE DELTA,
  install_source         VARCHAR(1024) ENCODE LZO,
  install_subpublisher   VARCHAR(1024) ENCODE LZO,
  install_campaign       VARCHAR(512) ENCODE LZO,
  install_language       VARCHAR(16) ENCODE LZO,
  install_country        VARCHAR(64) ENCODE LZO,
  install_os             VARCHAR(64) ENCODE LZO,
  install_device         VARCHAR(128) ENCODE LZO,
  install_browser        VARCHAR(64) ENCODE LZO,
  install_gender         VARCHAR(20) ENCODE LZO,
  LANGUAGE VARCHAR(16) ENCODE LZO,
  birthday               DATE ENCODE DELTA,
  first_name             VARCHAR(64) ENCODE LZO,
  last_name              VARCHAR(64) ENCODE LZO,
  gender                 VARCHAR(20) ENCODE LZO,
  country                VARCHAR(64) ENCODE LZO,
  email                  VARCHAR(256) ENCODE LZO,
  os                     VARCHAR(64) ENCODE LZO,
  os_version             VARCHAR(64) ENCODE LZO,
  device                 VARCHAR(128) ENCODE LZO,
  browser                VARCHAR(64) ENCODE LZO,
  browser_version        VARCHAR(128) ENCODE LZO,
  last_ip                VARCHAR(32) ENCODE LZO,
  level                  INTEGER,
  is_payer               INTEGER,
  conversion_ts          TIMESTAMP ENCODE DELTA,
  revenue_usd            NUMERIC(14,4),
  payment_cnt            INTEGER,
  last_login_date        DATE
)
DISTKEY (user_key) SORTKEY (user_key,app_id,user_id);


CREATE TABLE processed.fact_currency_transaction
( id varchar(100) ENCODE bytedict,
ts_pretty timestamp,
ts bigint,
app_id varchar(64) ENCODE BYTEDICT,
user_id varchar(64) ENCODE bytedict,
currency_purchased_qty bigint,
amount_paid numeric(14,4),
usd_paid numeric(14,4),
currency varchar(16) ENCODE bytedict,
currency_used_qty bigint,
currency_type varchar(64) ENCODE bytedict,
session_id varchar(128) ENCODE bytedict,
transaction_type varchar(64) ENCODE bytedict,
data_version varchar(16) ENCODE bytedict,
app_version varchar(64) ENCODE bytedict,
action varchar(64) ENCODE BYTEDICT
) DISTKEY (user_id) SORTKEY (app_id,ts_pretty);


CREATE TABLE processed.fact_item_transaction
( id varchar(128) ENCODE bytedict,
ts_pretty timestamp ENCODE DELTA,
ts bigint,
app_id varchar(64) encode bytedict,
user_id varchar(64) ENCODE bytedict,
session_id varchar(128) ENCODE bytedict,
event varchar(64) encode bytedict,
item_id varchar(64) ENCODE bytedict,
item_type varchar(64) ENCODE bytedict,
item_name varchar(64) ENCODE bytedict,
item_class varchar(32) ENCODE bytedict,
item_purchased_qty int,
item_used_qty int,
transaction_type varchar(64) ENCODE bytedict,
usd_paid numeric(14,4),
cost_per_unit numeric(14,4),
data_version varchar(16) ENCODE bytedict,
app_version varchar(64) ENCODE bytedict,
action varchar(64) ENCODE bytedict,
currency_type varchar(64) ENCODE bytedict
) DISTKEY (user_id) SORTKEY (app_id,ts_pretty);

CREATE TABLE processed.processed_consumable_item_transaction
( id varchar(128) encode bytedict,
ts_pretty timestamp ,
batch_id bigint,
app_id varchar(64) ENCODE bytedict,
user_id varchar(64) ENCODE bytedict,
session_id varchar(128) ENCODE bytedict,
transaction_type varchar(64) ENCODE bytedict,
item_id varchar(64) ENCODE bytedict,
item_type varchar(64) ENCODE bytedict,
item_name varchar(64) ENCODE bytedict,
item_class varchar(32) ENCODE bytedict,
item_purchased_qty int,
usd_paid numeric(14,4),
item_used_qty int,
usd_used numeric(14,4),
batch_item_balance bigint,
batch_usd_balance numeric(14,4),
item_balance bigint,
usd_balance numeric(14,4),
cost_per_unit numeric(14,4),
action varchar(64) ENCODE bytedict,
currency_type varchar(64) ENCODE bytedict
) DISTKEY (user_id) SORTKEY (app_id,ts_pretty);

CREATE TABLE processed.processed_currency_transaction
( id varchar(128) encode bytedict,
ts_pretty timestamp ,
ts bigint,
app_id varchar(64) ENCODE bytedict,
user_id varchar(64) ENCODE bytedict,
currency_purchased_qty bigint,
usd_paid numeric(14,4),
currency_used_qty bigint,
currency_type varchar(64) ENCODE bytedict,
session_id varchar(128) ENCODE bytedict,
transaction_type varchar(64) ENCODE bytedict,
usd_used numeric(14,4),
currency_balance bigint,
usd_balance numeric(14,4),
cost_per_unit numeric(14,4),
action varchar(64) encode bytedict
) DISTKEY (user_id) SORTKEY (app_id,ts_pretty);



CREATE TABLE processed.agg_currency_transaction
(
	date DATE,
	begin_currency_balance INTEGER,
	currency_balance INTEGER,
	begin_usd_balance NUMERIC(14, 4),
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	currency_type VARCHAR(32),
	app_id VARCHAR(64) encode bytedict,
	user_id VARCHAR(100) encode bytedict,
	currency_used_qty BIGINT,
	currency_purchased_qty BIGINT,
	usd_paid NUMERIC(20, 4),
	usd_used NUMERIC(20, 4)
)
DISTKEY (user_id) SORTKEY (app_id,date);

CREATE TABLE processed.agg_currency_transaction_monthly
(
	app_id VARCHAR(64),
	user_id VARCHAR(100),
	currency_type VARCHAR(32),
	currency_balance INTEGER,
	usd_balance NUMERIC(14, 4),
	year INTEGER,
	month INTEGER,
	currency_purchased_qty BIGINT,
	currency_used_qty BIGINT,
	usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4)
)
DISTKEY (user_id) SORTKEY (app_id,year,month);

CREATE TABLE processed.agg_consumable_item_transaction
(
	date DATE,
	begin_item_balance BIGINT,
	item_balance BIGINT,
	begin_usd_balance NUMERIC(14, 4),
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	app_id VARCHAR(64),
	user_id VARCHAR(100),
	item_id VARCHAR(32),
	item_used_qty BIGINT,
	item_purchased_qty BIGINT,
	usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4)
)
DISTKEY (user_id) SORTKEY (app_id,date);

CREATE TABLE  processed.agg_consumable_item_transaction_monthly
(
	app_id VARCHAR(64),
	user_id VARCHAR(100),
	item_id VARCHAR(32),
	item_balance BIGINT,
	usd_balance NUMERIC(14, 4),
	year INTEGER,
	month INTEGER,
	item_purchased_qty BIGINT,
	item_used_qty BIGINT,
	usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4)
)
DISTKEY (user_id) SORTKEY (app_id,year,month);

CREATE TABLE processed.dim_currency_balance
(
	id VARCHAR(100),
	updated_ts TIMESTAMP,
	ts INTEGER,
	app_id VARCHAR(64),
	user_id VARCHAR(64),
	currency_type VARCHAR(32),
	currency_balance INTEGER,
	usd_balance NUMERIC(14, 4),
	cost_per_unit NUMERIC(14, 4),
	currency_purchased_qty BIGINT,
	currency_used_qty BIGINT,
	usd_paid NUMERIC(38, 4),
	usd_used NUMERIC(38, 4)
)
DISTKEY (user_id) SORTKEY (app_id,user_id);


CREATE TABLE  processed.dim_consumable_item_balance
(
	id VARCHAR(100),
	updated_ts TIMESTAMP,
	batch_id INTEGER,
	app_id VARCHAR(64),
	user_id VARCHAR(128),
	item_id VARCHAR(64),
	item_type VARCHAR(64),
	item_name varchar(64),
	item_class VARCHAR(32),
	currency_type varchar(32),
	batch_item_balance BIGINT,
	batch_usd_balance NUMERIC(14, 4)
)
DISTKEY (user_id) SORTKEY (app_id,user_id);


CREATE TABLE processed.user_lt 
(
  app_id             VARCHAR(64) encode lzo,
  install_country    VARCHAR(64) encode bytedict,
  conversion_year    INTEGER,
  conversion_month   INTEGER,
  lt_days            INTEGER
)
SORTKEY (conversion_year,conversion_month);


CREATE TABLE processed.dim_country
(
   country_code  varchar(2),
   country       varchar(50)
)
SORTKEY(country_code);
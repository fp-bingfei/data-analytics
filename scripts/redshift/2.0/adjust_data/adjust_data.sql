SELECT 
	COALESCE(k.date, c.date) as date
	,COALESCE(k.game, c.game) as game
	,COALESCE(k.tracker, c.tracker) as tracker
	,COALESCE(k.campaign, c.campaign) as campaign
	,COALESCE(k.adgroup, c.adgroup) as adgroup
	,k.clicks
	,k.installs

	,c.d0_cohort_size
	,c.d0_retained_users
	,c.d0_sessions
	,c.d0_time_spent
	,c.d1_cohort_size
	,c.d1_retained_users
	,c.d1_sessions
	,c.d1_time_spent
	,c.d3_cohort_size
	,c.d3_retained_users
	,c.d3_sessions
	,c.d3_time_spent
	,c.d7_cohort_size
	,c.d7_retained_users
	,c.d7_sessions
	,c.d7_time_spent
	,c.d15_cohort_size
	,c.d15_retained_users
	,c.d15_sessions
	,c.d15_time_spent
	,c.d30_cohort_size
	,c.d30_retained_users
	,c.d30_sessions
	,c.d30_time_spent
FROM
(	SELECT *
	FROM adjust_data.kpis
	WHERE game like 'WAF%'
) k
FULL OUTER JOIN 
(	SELECT *
	FROM adjust_data.cohorts
	WHERE game like 'WAF%'
) c
ON k.date = c.date
	and k.game = c.game
	and k.tracker = c.tracker
	and k.campaign = c.campaign
	and k.adgroup = c.adgroup


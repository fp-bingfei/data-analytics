CREATE TABLE adjust_data.kpis
(
	date DATE,
	game VARCHAR(100) ENCODE lzo,
	tracker VARCHAR(100) ENCODE lzo,
	campaign VARCHAR(200) ENCODE lzo,
	adgroup VARCHAR(200) ENCODE lzo,
	clicks BIGINT,
	installs INTEGER
	)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game,
	tracker
);

CREATE TABLE adjust_data.cohorts
(
	date DATE,
	game VARCHAR(100) ENCODE lzo,
	tracker VARCHAR(100) ENCODE lzo,
	campaign VARCHAR(200) ENCODE lzo,
	adgroup VARCHAR(200) ENCODE lzo,

	d0_cohort_size INTEGER,
	d0_retained_users INTEGER,
	d0_sessions INTEGER,
	d0_time_spent BIGINT,
	
	d1_cohort_size INTEGER,
	d1_retained_users INTEGER,
	d1_sessions INTEGER,
	d1_time_spent BIGINT,

	d3_cohort_size INTEGER,
	d3_retained_users INTEGER,
	d3_sessions INTEGER,
	d3_time_spent BIGINT,

	d7_cohort_size INTEGER,
	d7_retained_users INTEGER,
	d7_sessions INTEGER,
	d7_time_spent BIGINT,

	d15_cohort_size INTEGER,
	d15_retained_users INTEGER,
	d15_sessions INTEGER,
	d15_time_spent BIGINT,

	d30_cohort_size INTEGER,
	d30_retained_users INTEGER,
	d30_sessions INTEGER,
	d30_time_spent BIGINT
	)
DISTSTYLE EVEN
SORTKEY
(
	date,
	game,
	tracker
);
--We shouldn't update install_info for lifetime, so just ignore any new info 
INSERT INTO adjust.primary_events
SELECT DISTINCT
r.adid,
r.event,
r.app_id,
r.app_version,
r.device_name,
r.os_name,
r.os_version,
r.idfa,
r.idfv,
r.gaid,
r.android_id,
r.ip_address,
r.country,
r.city,
r.lang,
r.tracker,
r.tracker_name,
r.rejection_reason,
r.click_id,
r.click_time,
r.engagement_time,
r.installed_at,
r.created_at,
r.received_at,
r.timezone,
r.time_spent,
r.game,
r.user_id
FROM adjust.raw_events r
LEFT JOIN adjust.primary_events p
ON r.adid = p.adid
AND r.game = p.game
AND r.event = p.event
WHERE p.adid is null;

--Yet we should record tracker_name change, so insert those data into a different table to monitor suspicious installs
--We need to review this table periodically, if not, a user might keep entering this table with many duplicated/similar records
--We should then probably update the primary_events table with this new info
INSERT INTO adjust.usual_suspects
SELECT DISTINCT
r.adid,
r.event,
r.app_id,
r.app_version,
r.device_name,
r.os_name,
r.os_version,
r.idfa,
r.idfv,
r.gaid,
r.android_id,
r.ip_address,
r.country,
r.city,
r.lang,
r.tracker,
r.tracker_name,
p.tracker_name as old_tracker_name,
r.rejection_reason,
r.click_id,
r.click_time,
r.engagement_time,
r.installed_at,
p.installed_at as old_installed_at,
r.created_at,
r.received_at,
r.timezone,
r.time_spent,
r.game,
r.user_id
FROM adjust.raw_events r
LEFT JOIN adjust.primary_events p
ON r.adid = p.adid
AND r.game = p.game
AND r.event = p.event
WHERE r.tracker_name != p.tracker_name
OR r.installed_at != p.installed_at;

--Need to change the strategy of update install source in dim_user later.
CREATE SCHEMA adjust;

CREATE TABLE adjust.raw_events
(
	--basic
	adid VARCHAR(64) encode lzo,
	app_id VARCHAR(64) encode lzo,
	app_version VARCHAR(32) encode bytedict,
	--device
	device_name VARCHAR(64) encode lzo,
	device_type VARCHAR(32) encode lzo,
	os_name VARCHAR(32) encode lzo,
	os_version VARCHAR(32) encode bytedict,
	idfa VARCHAR(64) encode lzo,
	idfv VARCHAR(64) encode lzo,
	gps_adid VARCHAR(64) encode lzo,
	android_id VARCHAR(64) encode lzo,
	sdk_version VARCHAR(32) encode bytedict,
	--location
	ip_address VARCHAR(15) encode lzo,
	country VARCHAR(2) encode bytedict,
	city VARCHAR(64) encode lzo,
	language VARCHAR(2) encode bytedict,
	isp VARCHAR(64) encode lzo,
	--marketing
	tracker VARCHAR(6) encode lzo,
	tracker_name VARCHAR(1024) encode lzo,
	last_tracker_name VARCHAR(1024) encode lzo,
	rejection_reason VARCHAR(64) encode lzo,
	click_id VARCHAR(300) encode lzo,
	search_term VARCHAR(128) encode lzo,
	referrer VARCHAR(300) encode lzo,
	--times
	click_time timestamp,
	engagement_time timestamp,
	installed_at timestamp,
	created_at timestamp,
	received_at timestamp  encode runlength,
	timezone VARCHAR(8) encode bytedict,
	conversion_duration INT encode MOSTLY16,
	time_spent INT encode delta32k,
	last_time_spent INT encode delta,
	lifetime_session_count INT encode delta32k,
	--custom
	event VARCHAR(16) encode lzo,
	fp_app_id VARCHAR(64) encode lzo,
	user_id VARCHAR(64) encode lzo
)
interleaved SORTKEY (
	event,
	fp_app_id
);


--To be fixed
CREATE TABLE adjust.primary_events
(
	--basic
	adid VARCHAR(50) distkey,
	event VARCHAR(16),
	app_id VARCHAR(64),
	app_version VARCHAR(32),
	--device
	device_name VARCHAR(32),
	os_name VARCHAR(32),
	os_version VARCHAR(32),
	idfa VARCHAR(64),
	idfv VARCHAR(64),
	gaid VARCHAR(64),
	android_id VARCHAR(64),
	--location
	ip_address VARCHAR(15),
	country VARCHAR(2),
	city VARCHAR(64),
	lang VARCHAR(2),
	--marketing
	tracker VARCHAR(6),
	tracker_name VARCHAR(1024),
	rejection_reason VARCHAR(64),
	click_id VARCHAR(64),
	--times
	click_time timestamp,
	engagement_time timestamp,
	installed_at timestamp,
	created_at timestamp,
	received_at timestamp,
	timezone VARCHAR(8),
	time_spent INT,
	--custom
	game VARCHAR(64),
	user_id VARCHAR(64)
)
SORTKEY (
	game,
	received_at,
	installed_at
);

CREATE TABLE adjust.usual_suspects
(
	--basic
	adid VARCHAR(50) distkey,
	event VARCHAR(16),
	app_id VARCHAR(64),
	app_version VARCHAR(32),
	--device
	device_name VARCHAR(32),
	os_name VARCHAR(32),
	os_version VARCHAR(32),
	idfa VARCHAR(64),
	idfv VARCHAR(64),
	gaid VARCHAR(64),
	android_id VARCHAR(64),
	--location
	ip_address VARCHAR(15),
	country VARCHAR(2),
	city VARCHAR(64),
	lang VARCHAR(2),
	--marketing
	tracker VARCHAR(6),
	tracker_name VARCHAR(1024),
	old_tracker_name VARCHAR(1024),
	rejection_reason VARCHAR(64),
	click_id VARCHAR(64),
	--times
	click_time timestamp,
	engagement_time timestamp,
	installed_at timestamp,
	old_installed_at timestamp,
	created_at timestamp,
	received_at timestamp,
	timezone VARCHAR(8),
	time_spent INT,
	--custom
	game VARCHAR(64),
	user_id VARCHAR(64)
)
SORTKEY (
	game,
	installed_at
);

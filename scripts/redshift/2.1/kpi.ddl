CREATE SCHEMA shared;

CREATE TABLE shared.init_start_date
(
	start_date DATE
)
;

CREATE TABLE shared.fact_session
(
	id VARCHAR(32) NOT NULL ENCODE lzo,

	--app attributes
	app_id VARCHAR(64) ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	session_id VARCHAR(128) ENCODE lzo,

	--user attributes
	user_key VARCHAR(32) NOT NULL ENCODE lzo, --distKey?
	user_id VARCHAR(128) NOT NULL ENCODE lzo, 
	level_start INTEGER,
	level_end INTEGER,
	vip_level INTEGER,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	gender VARCHAR(1),
	birthday VARCHAR(10),

	--times
	event_date DATE NOT NULL ENCODE delta, --date the session_start
	-- date_end DATE ENCODE delta, --?
	ts_start TIMESTAMP NOT NULL ENCODE delta,
	ts_end TIMESTAMP NOT NULL ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	-- install_date DATE NOT NULL ENCODE delta, --?
	
	--device
	os VARCHAR(32) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(32) ENCODE lzo,
	browser_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(64) ENCODE lzo,
	idfa VARCHAR(36) ENCODE lzo,
	idfv VARCHAR(36) ENCODE lzo,
	gaid VARCHAR(36) ENCODE lzo,
	android_id VARCHAR(36) ENCODE lzo,
	mac_address VARCHAR(64) ENCODE lzo,
	
	--location
	ip VARCHAR(39) NOT NULL ENCODE lzo,
	country_code VARCHAR(2),
	lang VARCHAR(16) ENCODE lzo,

	--attribution
	facebook_id VARCHAR(128) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	install_source VARCHAR(512) ENCODE lzo,
	last_ref VARCHAR(256), -- in farm it's called fb_source

	--segment
	scene INTEGER,
	gameserver_id INTEGER,

	playtime_sec BIGINT

)
SORTKEY(
	event_date,
	app_id
	--user_key
);

CREATE TABLE shared.fact_revenue
(
	id VARCHAR(32) NOT NULL ENCODE lzo,

	--app attributes
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	session_id VARCHAR(128) ENCODE lzo,

	--user attributes
	user_key VARCHAR(32) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	-- first_name VARCHAR(64) ENCODE lzo,
	-- last_name VARCHAR(64) ENCODE lzo,
	-- gender VARCHAR(1),
	-- birthday VARCHAR(10),

	--times
	event_date DATE NOT NULL ENCODE delta,
	ts TIMESTAMP NOT NULL ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,

	--device
	os VARCHAR(32) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(32) ENCODE lzo,
	browser_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(64) ENCODE lzo,
	idfa VARCHAR(36) ENCODE lzo,
	idfv VARCHAR(36) ENCODE lzo,
	gaid VARCHAR(36) ENCODE lzo,
	android_id VARCHAR(36) ENCODE lzo,
	mac_address VARCHAR(64) ENCODE lzo,

	--location
	ip VARCHAR(39) NOT NULL ENCODE lzo,
	country_code VARCHAR(2), -- country code?
	lang VARCHAR(16) ENCODE lzo,

	--attribution
	facebook_id VARCHAR(128) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	install_source VARCHAR(512) ENCODE lzo,
	last_ref VARCHAR(256), -- in farm it's called fb_source, and only pc game has it

	--segment
	scene INTEGER,
	gameserver_id INTEGER,

	--revenue
	payment_processor VARCHAR(16),
	iap_product_id VARCHAR(64),
	iap_product_name VARCHAR(64),
	iap_product_type VARCHAR(64),
	currency VARCHAR(3),
	revenue_amount numeric(14,4),
	revenue_usd numeric(14,4),
	transaction_id VARCHAR(128),

	charged_amount numeric(14,4),
	buyer_forex_rate numeric(10,8),
	buyer_currency VARCHAR(3)
)
SORTKEY
(
	event_date,
	app_id
);

CREATE TABLE shared.fact_new_user
(
	id VARCHAR(32) NOT NULL ENCODE lzo,

	--app attributes
	app_id VARCHAR(64) NOT NULL ENCODE lzo,
	app_version VARCHAR(32) ENCODE lzo,
	session_id VARCHAR(128) ENCODE lzo,

	--user attributes
	user_key VARCHAR(32) NOT NULL ENCODE lzo DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE lzo,
	level INTEGER,
	vip_level INTEGER,
	first_name VARCHAR(64) ENCODE lzo,
	last_name VARCHAR(64) ENCODE lzo,
	gender VARCHAR(1),
	birthday VARCHAR(10),

	--times
	event_date DATE NOT NULL ENCODE delta,
	ts TIMESTAMP NOT NULL ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,

	--device
	os VARCHAR(32) ENCODE lzo,
	os_version VARCHAR(64) ENCODE lzo,
	browser VARCHAR(32) ENCODE lzo,
	browser_version VARCHAR(64) ENCODE lzo,
	device VARCHAR(64) ENCODE lzo,
	idfa VARCHAR(36) ENCODE lzo,
	idfv VARCHAR(36) ENCODE lzo,
	gaid VARCHAR(36) ENCODE lzo,
	android_id VARCHAR(36) ENCODE lzo,
	mac_address VARCHAR(64) ENCODE lzo,

	--location
	ip VARCHAR(39) NOT NULL ENCODE lzo,
	country_code VARCHAR(2), -- country code?
	lang VARCHAR(16) ENCODE lzo,

	--attribution
	facebook_id VARCHAR(128) ENCODE lzo,
	email VARCHAR(256) ENCODE lzo,
	install_source VARCHAR(512) ENCODE lzo,
	last_ref VARCHAR(256), -- in farm it's called fb_source, and only farm has it

	--segment
	scene INTEGER,
	gameserver_id INTEGER,
)
SORTKEY
(
	event_date,
	app_id
);



CREATE SCHEMA raw_events;
CREATE TABLE raw_events.events
(
	data_version VARCHAR(8) ENCODE lzo,
	app_id VARCHAR(64) ENCODE lzo,
	ts BIGINT ENCODE delta32k,
	ts_pretty TIMESTAMP ENCODE delta,
	user_id VARCHAR(128) ENCODE lzo,
	session_id VARCHAR(128) ENCODE lzo,
	event VARCHAR(32) ENCODE lzo,
	properties VARCHAR(3000) ENCODE lzo,
	collections VARCHAR(2000) ENCODE lzo
)
DISTSTYLE EVEN
SORTKEY
(
	ts_pretty,
	event,
	app_id
);

CREATE TABLE raw_events.tmp_events
(
	data_version VARCHAR(8) ENCODE lzo,
	app_id VARCHAR(64) ENCODE lzo,
	ts BIGINT ENCODE delta32k,
	ts_pretty TIMESTAMP ENCODE delta,
	user_id VARCHAR(128) ENCODE lzo,
	session_id VARCHAR(128) ENCODE lzo,
	event VARCHAR(32) ENCODE lzo,
	properties VARCHAR(20000) ENCODE lzo,
	collections VARCHAR(2000) ENCODE lzo
)
;

CREATE SCHEMA kpi_by_fpid;

CREATE TABLE;




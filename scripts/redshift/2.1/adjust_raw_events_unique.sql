
insert into adjust.raw_events_unique 
with new_user as (
select
    adid,
	app_id,
	app_version,
	device_name,
	device_type,
	os_name,
	os_version,
	idfa,
	idfv,
	gps_adid,
	android_id,
	sdk_version,
	ip_address,
	country,
	city,
	language,
	isp,
	tracker,
	tracker_name,
	last_tracker_name,
	rejection_reason,
	click_id,
	search_term,
	referrer,
	click_time,
	engagement_time,
	installed_at,
	created_at,
	received_at,
	timezone,
	conversion_duration,
	time_spent,
	last_time_spent,
	lifetime_session_count,
	event,
	fp_app_id,
	user_id
from
(
select 
	*
	,row_number() over(partition by fp_app_id,adid,event order by installed_at,created_at) as rk
from adjust.raw_events
where 
	installed_at>=dateadd(day,-1,current_date)
	and installed_at<current_date
)
where rk=1
)

select 
a.*
from new_user a 
left join adjust.raw_events_unique b 
	on a.fp_app_id = b.fp_app_id
	and a.adid = b.adid
	and a.event = b.event 
where b.adid is null
;


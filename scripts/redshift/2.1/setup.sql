-----------------------------------------------
--Data 2.1 Setup script
-----------------------------------------------

update kpi_processed.init_start_date                                                          
set start_date =  DATEADD(day, -3, CURRENT_DATE);

delete from raw_events.events
where user_id is null and app_id is null;
------------------------------------------------
--Data 2.1 fact_tables
------------------------------------------------

--Hard coded part in table, and use join

-- New user
DELETE FROM kpi_processed.fact_new_user
 WHERE event_date >=
       (SELECT start_date
          FROM kpi_processed.init_start_date)
   -- AND app_id LIKE '_game_%'
;


INSERT INTO kpi_processed.fact_new_user
SELECT DISTINCT id

       ,app_id::VARCHAR(64)
       ,app_version::VARCHAR(32)
       ,sesssion_id::VARCHAR(128)

       ,user_key::VARCHAR(32)
       ,user_id::VARCHAR(128)
       ,level
       ,vip_level
       ,first_name::VARCHAR(64)
       ,last_name::VARCHAR(64)
       ,gender::VARCHAR(1)
       ,birthday::VARCHAR(10)

       ,event_date
       ,ts
       ,install_ts

       ,os::VARCHAR(32) -- format??
       ,os_version::VARCHAR(64) -- format??
       ,browser::VARCHAR(32)
       ,browser_version::VARCHAR(64)
       ,device::VARCHAR(64)
       ,idfa::VARCHAR(36)
       ,idfv::VARCHAR(36)
       ,gaid::VARCHAR(36)
       ,android_id::VARCHAR(36)
       ,mac_address::VARCHAR(64)

       ,ip::VARCHAR(39)
       ,country_code::VARCHAR(2)
       ,lang::VARCHAR(16)

       ,facebook_id::VARCHAR(128)
       ,email::VARCHAR(256)
       ,install_source::VARCHAR(512)
       ,last_ref::VARCHAR(256)

       ,scene
       ,gameserver_id

  FROM (SELECT  MD5(app_id||user_id) AS id
               ,app_id
               ,json_extract_path_text(properties,'app_version') AS app_version
               ,session_id
               ,MD5(app_id||user_id)
               ,user_id
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'level'), '[0-9]+'), '')::INTEGER, 0) AS level
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'vip_level'), '[0-9]+'), '')::INTEGER, 0) AS vip_level
               ,json_extract_path_text(properties,'first_name') AS first_name
               ,json_extract_path_text(properties,'last_name') AS last_name
               ,json_extract_path_text(properties,'gender') as gender
               ,json_extract_path_text(properties,'birthday') AS birthday
               ,DATE(ts_pretty) AS event_date
               ,ts_pretty
               ,CAST(json_extract_path_text(properties,'install_ts_pretty') as TIMESTAMP) as install_ts
               ,CASE
                WHEN json_extract_path_text(properties,'os') is null
                     OR lower(json_extract_path_text(properties,'os')) in ('', 'null') THEN 'Unknown'
                WHEN lower(json_extract_path_text(properties,'os')) in ('ios', 'iphone os', 'iphone', 'ipad') THEN 'iOS'
                WHEN lower(json_extract_path_text(properties,'os')) = 'android' THEN 'Android'
                WHEN lower(json_extract_path_text(properties,'os')) = 'windows' THEN 'Windows'
                ELSE json_extract_path_text(properties,'os')
                END AS os
               ,json_extract_path_text(properties,'os_version') AS os_version
               ,CASE
                WHEN json_extract_path_text(properties,'browser') is null
                     OR json_extract_path_text(properties,'browser') = '' THEN 'Unknown' 
                ELSE json_extract_path_text(properties,'browser')
                END AS browser
               ,json_extract_path_text(properties,'browser_version') AS browser_version
               ,json_extract_path_text(properties,'device') AS device
               ,json_extract_path_text(properties,'idfa') AS idfa
               ,json_extract_path_text(properties,'idfv') AS idfv
               ,json_extract_path_text(properties,'gaid') AS gaid
               ,json_extract_path_text(properties,'android_id') AS android_id
               ,json_extract_path_text(properties,'mac_address') AS mac_address
               ,json_extract_path_text(properties,'ip') AS ip
               ,nvl(json_extract_path_text(properties,'country_code'), '--') AS country_code
               ,json_extract_path_text(properties,'lang') AS lang
               ,CASE
                WHEN app_id = 'lc_patch.global.prod' THEN json_extract_path_text(properties,'fpid')
                ELSE json_extract_path_text(properties,'facebook_id')
                END AS facebook_id
               ,json_extract_path_text(properties,'email') AS email
               ,json_extract_path_text(properties,'install_source') AS install_source
               ,nvl(nullif(json_extract_path_text(properties,'fb_source'), ''), json_extract_path_text(properties,'last_ref')) AS last_ref
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'scene'), '[0-9]+'), '')::INTEGER, 1) AS scene
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'gameserver_id'), '[0-9]+'), '')::INTEGER, 0) AS gameserver_id
               ,row_number() over(partition by id ORDER BY ts_pretty ASC) AS rnum
          FROM raw_events.events
         WHERE ts_pretty >=
               (SELECT start_date 
                  FROM kpi_processed.init_start_date)
           AND event = 'new_user'
           -- AND app_id LIKE '_game_%'
           AND json_extract_path_text(properties,'install_ts_pretty') <> ''
       ) AS tmp
 WHERE tmp.rnum = 1
;


-- Payment

DELETE FROM kpi_processed.fact_revenue
 WHERE event_date >=
       (SELECT start_date
          FROM kpi_processed.init_start_date)
   -- AND app_id LIKE '_game_%'
;


INSERT INTO kpi_processed.fact_revenue
SELECT DISTINCT id

       ,app_id::VARCHAR(64)
       ,app_version::VARCHAR(32)
       ,sesssion_id::VARCHAR(128)

       ,user_key::VARCHAR(32)
       ,user_id::VARCHAR(128)
       ,level
       ,vip_level
       -- ,first_name::VARCHAR(64)
       -- ,last_name::VARCHAR(64)
       -- ,gender::VARCHAR(1)
       -- ,birthday::VARCHAR(10)

       ,event_date
       ,ts
       ,install_ts

       ,os::VARCHAR(32) -- format??
       ,os_version::VARCHAR(64) -- format??
       ,browser::VARCHAR(32)
       ,browser_version::VARCHAR(64)
       ,device::VARCHAR(64)
       ,idfa::VARCHAR(36)
       ,idfv::VARCHAR(36)
       ,gaid::VARCHAR(36)
       ,android_id::VARCHAR(36)
       ,mac_address::VARCHAR(64)

       ,ip::VARCHAR(39)
       ,country_code::VARCHAR(2)
       ,lang::VARCHAR(16)

       ,facebook_id::VARCHAR(128)
       ,email::VARCHAR(256)
       ,install_source::VARCHAR(512)
       ,last_ref::VARCHAR(256)

       ,scene
       ,gameserver_id

       ,payment_processor::VARCHAR(16)
       ,iap_product_id::VARCHAR(64)
       ,iap_product_name::VARCHAR(64)
       ,iap_product_type::VARCHAR(64)
       ,p.currency::VARCHAR(3)
       ,p.amount * 1.0000 / 100 AS revenue_amount
       ,nvl(p.amount * 1.0000 * c.factor / 100, 0) AS revenue_usd
       ,transaction_id::VARCHAR(128)

        -- TO BE INSERT in other pipeline
        -- ,charged_amount numeric(14,4)
        -- ,buyer_forex_rate numeric(10,8)
        -- ,buyer_currency VARCHAR(3)

  FROM (SELECT  MD5(app_id||user_id||json_extract_path_text(properties,'transaction_id')) AS id
               ,app_id
               ,json_extract_path_text(properties,'app_version') AS app_version
               ,session_id
               ,MD5(app_id||user_id)
               ,user_id
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'level'), '[0-9]+'), '')::INTEGER, 0) AS level
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'vip_level'), '[0-9]+'), '')::INTEGER, 0) AS vip_level
               -- ,json_extract_path_text(properties,'first_name') AS first_name
               -- ,json_extract_path_text(properties,'last_name') AS last_name
               -- ,json_extract_path_text(properties,'gender') as gender
               -- ,json_extract_path_text(properties,'birthday') AS birthday
               ,DATE(ts_pretty) AS event_date
               ,ts_pretty
               ,CAST(json_extract_path_text(properties,'install_ts_pretty') as TIMESTAMP) as install_ts
               ,CASE
                WHEN json_extract_path_text(properties,'os') is null
                     OR lower(json_extract_path_text(properties,'os')) in ('', 'null') THEN 'Unknown'
                WHEN lower(json_extract_path_text(properties,'os')) in ('ios', 'iphone os', 'iphone', 'ipad') THEN 'iOS'
                WHEN lower(json_extract_path_text(properties,'os')) = 'android' THEN 'Android'
                WHEN lower(json_extract_path_text(properties,'os')) = 'windows' THEN 'Windows'
                ELSE json_extract_path_text(properties,'os')
                END AS os
               ,json_extract_path_text(properties,'os_version') AS os_version
               ,CASE
                WHEN json_extract_path_text(properties,'browser') is null
                     OR json_extract_path_text(properties,'browser') = '' THEN 'Unknown'
                ELSE json_extract_path_text(properties,'browser')
                END AS browser
               ,json_extract_path_text(properties,'browser_version') AS browser_version
               ,json_extract_path_text(properties,'device') AS device
               ,json_extract_path_text(properties,'idfa') AS idfa
               ,json_extract_path_text(properties,'idfv') AS idfv
               ,json_extract_path_text(properties,'gaid') AS gaid
               ,json_extract_path_text(properties,'android_id') AS android_id
               ,json_extract_path_text(properties,'mac_address') AS mac_address
               ,json_extract_path_text(properties,'ip') AS ip
               ,nvl(json_extract_path_text(properties,'country_code'), '--') AS country_code
               ,json_extract_path_text(properties,'lang') AS lang
               ,CASE
                WHEN app_id = 'lc_patch.global.prod' THEN json_extract_path_text(properties,'fpid')
                ELSE json_extract_path_text(properties,'facebook_id')
                END AS facebook_id
               ,json_extract_path_text(properties,'email') AS email
               ,json_extract_path_text(properties,'install_source') AS install_source
               ,nvl(nullif(json_extract_path_text(properties,'fb_source'), ''), json_extract_path_text(properties,'last_ref')) AS last_ref
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'scene'), '[0-9]+'), '')::INTEGER, 1) AS scene
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'gameserver_id'), '[0-9]+'), '')::INTEGER, 0) AS gameserver_id
               ,json_extract_path_text(properties,'payment_processor') AS payment_processor
               ,json_extract_path_text(properties,'iap_product_id') AS iap_product_id
               ,json_extract_path_text(properties,'iap_product_name') AS iap_product_name
               ,json_extract_path_text(properties,'iap_product_type') AS iap_product_type
               ,json_extract_path_text(properties,'currency') AS currency
               ,CASE
                WHEN ((app_id IN ('farm.ae.prod', 'farm.br.prod', 'farm.de.prod', 'farm.fr.prod', 'farm.hyves.prod',
                                  'farm.it.prod', 'farm.nk.prod', 'farm.nl.prod', 'farm.pl.prod', 'farm.plingaplay.prod',
                                  'farm.spil.prod', 'farm.th.prod', 'farm.tw.prod', 'farm.us.prod', 'farm.vz.prod',
                                  'farm.y8.prod')
                              AND json_extract_path_text(properties,'payment_processor') != 'Sponsorpay'
                              AND json_extract_path_text(properties,'payment_processor') != 'Supersonic')
                      OR app_id IN ('ffs.amazon.prod', 'ffs.cn.prod', 'ffs.flexion.prod', 'ffs.global.prod',
                                    'ffs.tango.prod', 'ffs.th.prod', 'ha.th.prod', 'ha.us.prod', 'poker.ar.prod',
                                    'royal.ae.prod', 'royal.de.prod','royal.fr.prod', 'royal.nl.prod',
                                    'royal.plinga.prod', 'royal.spil.prod', 'royal.th.prod', 'royal.us.prod')
                      ) THEN nvl(NULLIF(regexp_substr(json_extract_path_text(properties,'amount'), '[0-9]+(\\.[0-9]+)?'),'')::FLOAT, 0)
                ELSE nvl(NULLIF(regexp_substr(json_extract_path_text(properties,'amount'), '[0-9]+(\\.[0-9]+)?'),'')::FLOAT, 0) * 100
                END AS amount
               ,json_extract_path_text(properties,'transaction_id') AS transaction_id
               ,row_number() over(partition by id ORDER BY ts_pretty ASC) AS rnum
          FROM raw_events.events
         WHERE ts_pretty >=
               (SELECT start_date
                  FROM kpi_processed.init_start_date)
           AND event = 'payment'
           -- AND app_id LIKE '_game_%'
           AND json_extract_path_text(properties,'install_ts_pretty') <> ''
       ) AS tmp
       LEFT JOIN (SELECT DISTINCT  DATE(dt) --maybe another DDL
                                  ,currency
                                  ,factor
                       FROM kpi_processed.currency
                       WHERE DATE(dt) >=
                             (SELECT start_date
                                FROM kpi_processed.init_start_date)
                 ) AS c
       ON tmp.event_date = DATE(c.dt) --maybe another DDL
          AND tmp.currency = c.currency
 WHERE tmp.rnum = 1
;


-- Session

DELETE FROM kpi_processed.fact_session
 WHERE event_date >=
       (SELECT start_date
          FROM kpi_processed.init_start_date)
   -- AND app_id LIKE '_game_%'
;


INSERT INTO kpi_processed.fact_session
SELECT  DISTINCT ss.id

       ,app_id::VARCHAR(64)
       ,app_version::VARCHAR(32)
       ,sesssion_id::VARCHAR(128)

       ,user_key::VARCHAR(32)
       ,user_id::VARCHAR(128)
       ,ss.level as level_start
       ,se.level as level_end
       ,vip_level
       ,first_name::VARCHAR(64)
       ,last_name::VARCHAR(64)
       ,gender::VARCHAR(1)
       ,birthday::VARCHAR(10)

       ,event_date
       ,ss.ts_pretty as ts_start
       ,se.ts_pretty as ts_end
       ,install_ts

       ,os::VARCHAR(32) -- format??
       ,os_version::VARCHAR(64) -- format??
       ,browser::VARCHAR(32)
       ,browser_version::VARCHAR(64)
       ,device::VARCHAR(64)
       ,idfa::VARCHAR(36)
       ,idfv::VARCHAR(36)
       ,gaid::VARCHAR(36)
       ,android_id::VARCHAR(36)
       ,mac_address::VARCHAR(64)

       ,ip::VARCHAR(39)
       ,country_code::VARCHAR(2)
       ,lang::VARCHAR(16)

       ,facebook_id::VARCHAR(128)
       ,email::VARCHAR(256)
       ,install_source::VARCHAR(512)
       ,last_ref::VARCHAR(256)

       ,ss.scene as scene
       ,gameserver_id
       ,coalese(se.session_length, 0) AS playtime_sec

  FROM (SELECT  MD5(app_id||user_id||session_id) AS id
               ,app_id
               ,json_extract_path_text(properties,'app_version') AS app_version
               ,session_id
               ,MD5(app_id||user_id)
               ,user_id
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'level'), '[0-9]+'), '')::INTEGER, 0) AS level
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'vip_level'), '[0-9]+'), '')::INTEGER, 0) AS vip_level
               ,json_extract_path_text(properties,'first_name') AS first_name
               ,json_extract_path_text(properties,'last_name') AS last_name
               ,json_extract_path_text(properties,'gender') as gender
               ,json_extract_path_text(properties,'birthday') AS birthday
               ,DATE(ts_pretty) AS event_date
               ,ts_pretty
               ,CAST(json_extract_path_text(properties,'install_ts_pretty') as TIMESTAMP) as install_ts
               ,CASE
                WHEN json_extract_path_text(properties,'os') is null
                     OR lower(json_extract_path_text(properties,'os')) in ('', 'null') THEN 'Unknown'
                WHEN lower(json_extract_path_text(properties,'os')) in ('ios', 'iphone os', 'iphone', 'ipad') THEN 'iOS'
                WHEN lower(json_extract_path_text(properties,'os')) = 'android' THEN 'Android'
                WHEN lower(json_extract_path_text(properties,'os')) = 'windows' THEN 'Windows'
                ELSE json_extract_path_text(properties,'os')
                END AS os
               ,json_extract_path_text(properties,'os_version') AS os_version
               ,CASE
                WHEN json_extract_path_text(properties,'browser') is null
                     OR json_extract_path_text(properties,'browser') = '' THEN 'Unknown'
                ELSE json_extract_path_text(properties,'browser')
                END AS browser
               ,json_extract_path_text(properties,'browser_version') AS browser_version
               ,json_extract_path_text(properties,'device') AS device
               ,json_extract_path_text(properties,'idfa') AS idfa
               ,json_extract_path_text(properties,'idfv') AS idfv
               ,json_extract_path_text(properties,'gaid') AS gaid
               ,json_extract_path_text(properties,'android_id') AS android_id
               ,json_extract_path_text(properties,'mac_address') AS mac_address
               ,json_extract_path_text(properties,'ip') AS ip
               ,nvl(json_extract_path_text(properties,'country_code'), '--') AS country_code
               ,json_extract_path_text(properties,'lang') AS lang
               ,CASE
                WHEN app_id = 'lc_patch.global.prod' THEN json_extract_path_text(properties,'fpid')
                ELSE json_extract_path_text(properties,'facebook_id')
                END AS facebook_id
               ,json_extract_path_text(properties,'email') AS email
               ,json_extract_path_text(properties,'install_source') AS install_source
               ,nvl(nullif(json_extract_path_text(properties,'fb_source'), ''), json_extract_path_text(properties,'last_ref')) AS last_ref
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'scene'), '[0-9]+'), '')::INTEGER, 1) AS scene
               ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'gameserver_id'), '[0-9]+'), '')::INTEGER, 0) AS gameserver_id
               ,row_number() over(partition by id ORDER BY ts_pretty DESC) AS rnum
          FROM raw_events.events
         WHERE ts_pretty >=
               (SELECT start_date
                  FROM kpi_processed.init_start_date)
           AND event = 'session_start'
           -- AND app_id LIKE '_game_%'
           AND json_extract_path_text(properties,'install_ts_pretty') <> ''
       ) AS ss
       LEFT JOIN (SELECT  MD5(app_id||user_id||session_id) AS id
                         ,ts_pretty
                         ,round(nvl(nullif(json_extract_path_text(properties,'session_length'),''), '0'))::BIGINT AS session_length
                         ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'scene'), '[0-9]+'), '')::INTEGER, 1) AS scene
                         ,nvl(nullif(regexp_substr(json_extract_path_text(properties,'level'), '[0-9]+'), '')::int, 0) as level
                         ,row_number() over (partition by id ORDER BY ts_pretty ASC) AS rnum
                    FROM raw_events.events
                   WHERE ts_pretty >=
                         (SELECT start_date
                            FROM kpi_processed.init_start_date)
                     AND event = 'session_end'
                     -- AND app_id LIKE '_game_%'
                 ) AS se
       ON ss.id = se.id
          AND ss.scene = se.scene
          AND ss.rnum = se.rnum
 WHERE tmp.rnum = 1
;



--the scene issue????

update tmp_session_start set install_ts=u.install_ts from kpi_processed.new_user u
where tmp_session_start.app_id=u.app_id
and tmp_session_start.user_id=u.user_id
and tmp_session_start.scene=u.scene
and tmp_session_start.app_id like 'royal%'
and tmp_session_start.scene='2';
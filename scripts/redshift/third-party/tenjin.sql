CREATE SCHEMA IF NOT EXISTS raw_data;

-- DROP TABLE IF EXISTS raw_data.tenjin;

CREATE TABLE IF NOT EXISTS raw_data.tenjin (
	date						DATE NOT NULL ENCODE DELTA,
	app_id						VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	ad_network					VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	tenjin_application_name		VARCHAR(128) NOT NULL ENCODE BYTEDICT,
	tenjin_app_id				VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	tenjin_campaign_id			VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	campaign_name				VARCHAR(128) NOT NULL ENCODE BYTEDICT,
	remote_campaign_id			VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	impressions					BIGINT,
	clicks						BIGINT,
	downloads					BIGINT,
  	amount        				DECIMAL(14,4) DEFAULT 0,
	store_id					VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	bundle_id					VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	platform					VARCHAR(32) NOT NULL ENCODE BYTEDICT
) SORTKEY(date, app_id) ;

DELETE FROM raw_data.tenjin WHERE date >= '${START_DATE}' AND date <= '${END_DATE}';

copy raw_data.tenjin from 's3://com.funplus.datawarehouse/third-party/tenjin/copy/${RPT_DATE}/${COST_IMPORT_JOB_APP}.tsv.gz' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL;

commit;

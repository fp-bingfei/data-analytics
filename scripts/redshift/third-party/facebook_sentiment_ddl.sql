CREATE TABLE third_party.facebook_sentiment(
  status_id varchar(100) NOT NULL PRIMARY KEY ENCODE BYTEDICT, 
  status_message varchar(MAX) NOT NULL ENCODE BYTEDICT, 
  status_author varchar(100) ENCODE BYTEDICT, 
  link_name varchar(100) ENCODE BYTEDICT, 
  status_type varchar(100) NOT NULL ENCODE BYTEDICT,  
  status_link varchar(100) ENCODE BYTEDICT, 
  status_published varchar(100) ENCODE BYTEDICT, 
  num_reactions int, 
  num_comments int,
  num_shares int,
  num_likes int,
  num_loves int,
  num_wows  int,
  num_hahas int,
  num_sads  int,
  num_angrys int,
  group_id bigint,
  sentiment_compound  numeric(15,4),
  sentiment_neg numeric(15,4),
  sentiment_neu numeric(15,4),
  sentiment_pos numeric(15,4))
  DISTKEY(status_id)
  SORTKEY(status_id,status_published,group_id);

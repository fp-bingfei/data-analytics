use farm_1_1;


SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};


--------------------------------------------------
--- Submarine events
--------------------------------------------------
-- submarine
INSERT OVERWRITE TABLE raw_submarine_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    coins,
    duration,
    greenery_level,
    input,
    output,
    npc,
    rc,
    returnTs,
    sailTs,
    sailors,
    speedup,
    sub,
    water_level,
    timestamp,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_submarine_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- WaterLevelUp
INSERT OVERWRITE TABLE raw_waterlevelup_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    expansion_size_from, 
    greenery_bal, 
    greenery_in, 
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level, 
    new_water_level, 
    old_water_level, 
    os,
    os_version,
    rc_bal, 
    rc_get,
    snsid,
    uid,
    water_exp,
    water_level,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_waterlevelup_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- ThirdSceneExpand
INSERT OVERWRITE TABLE raw_thirdsceneexpand_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty, 
    browser,
    browser_version,
    coins_bal double,
    country_code,
    event,
    expansion_size_from, 
    expansion_size_to, 
    expansion_top_size_from, 
    greenery_bal, 
    install_source,
    install_ts,
    install_ts_pretty, 
    ip,
    lang,
    level, 
    os,
    os_version,
    rc_bal, 
    snsid,
    uid,
    waterLevel,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_thirdsceneexpand_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- Guild events
--------------------------------------------------
-- GuildInfo
INSERT OVERWRITE TABLE raw_guildinfo_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    is_new,
    guildid,
    name,
    cover,
    type,
    required_level,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildinfo_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildMembership
INSERT OVERWRITE TABLE raw_guildmembership_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    memberid,
    status,
    is_self,
    member_count,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildmembership_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildShop
INSERT OVERWRITE TABLE raw_guildshop_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    itemid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildshop_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildMapReward
INSERT OVERWRITE TABLE raw_guildmapreward_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    rewardid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildmapreward_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildDonation
INSERT OVERWRITE TABLE raw_guilddonation_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    topicid,
    itemid,
    item_num,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guilddonation_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildDonationReward
INSERT OVERWRITE TABLE raw_guilddonationreward_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    topicid,
    rewardid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guilddonationreward_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildDonationUnlock
INSERT OVERWRITE TABLE raw_guilddonationunlock_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    topicid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guilddonationunlock_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildLevelUp
INSERT OVERWRITE TABLE raw_guildlevelup_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    guild_level,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildlevelup_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildVouchers
INSERT OVERWRITE TABLE raw_guildvouchers_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    voucher_type,
    voucher_in,
    voucher_out,
    action,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildvouchers_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- DIY events
--------------------------------------------------
-- DIYMaterial
INSERT OVERWRITE TABLE raw_diymaterial_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    material_id,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_diymaterial_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- DIYVoucher
INSERT OVERWRITE TABLE raw_diyvoucher_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    voucher_id,
    voucher_qty,
    is_buy,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_diyvoucher_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- DIYReward
INSERT OVERWRITE TABLE raw_diyreward_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    reward_id,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_diyreward_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- Expand Status events
--------------------------------------------------
INSERT OVERWRITE TABLE raw_expand_status_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    1st_ranch,
    1st_yard,
    2nd,
    2nd_ranch,
    2nd_yard,
    3rd,
    3rd_water,
    3rd_ranch,
    neighbor_num,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_expand_status_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- Dinner Party events
--------------------------------------------------
-- Dinner Party make
INSERT OVERWRITE TABLE raw_dinner_party_make_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    dish_id,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_dinner_party_make_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- Dinner Party eat
INSERT OVERWRITE TABLE raw_dinner_party_eat_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    dish_id,
    invited_friend,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_dinner_party_eat_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- Dinner Party reward
INSERT OVERWRITE TABLE raw_dinner_party_reward_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    reward_id,
    first_time,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_dinner_party_reward_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- daily DAU events
--------------------------------------------------

INSERT OVERWRITE TABLE raw_dau_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    fb_source_last,
    track_ref_last,
    rc_bal,
    new_cash1,
    new_cash2,
    new_cash3,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_dau_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- daily vip events
--------------------------------------------------

INSERT OVERWRITE TABLE raw_vip_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    uid,
    os,
    country_code,
    lang,
    ip,
    os_version,
    browser_version,
    snsid,
    fb_source,
    install_ts_pretty,
    level,
    install_ts,
    event,
    browser,
    install_source,
    properties,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_vip_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- daily 4th scene events
--------------------------------------------------

-- 4th_scene_login
INSERT OVERWRITE TABLE raw_4th_scene_login_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    first_login,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_login_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- 4th_scene_currency
INSERT OVERWRITE TABLE raw_4th_scene_currency_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    action_type,
    chef_points_in,
    chef_points_out,
    chef_points_bal,
    silver_coins_in,
    silver_coins_out,
    silver_coins_bal,
    reputation_in,
    reputation_out,
    reputation_bal,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_currency_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- 4th_scene_action
INSERT OVERWRITE TABLE raw_4th_scene_action_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    put_on_4th_scene,
    cost_type,
    cost_num,
    decoration_id,
    reputation,
    building,
    item_id,
    item_num,
    finish_with_rc,
    production_time,
    from_num,
    to_num,
    material,
    upgrade_material,
    sell_time,
    dish_id,
    recipe,
    sell_silver_coins,
    sell_chef_points,
    product_id,
    material_id,
    needs,
    request_to_friend,
    request_to_neighbor,
    parent_id,
    use_num,
    direction,
    detail,
    limit_from,
    limit_to,
    type,
    currency,
    currency_num,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_action_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- 4th_scene_level_up
INSERT OVERWRITE TABLE raw_4th_scene_level_up_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    previous_level,
    current_level,
    reward_info,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_level_up_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- fact tables
insert overwrite table agg_4th_scene_login partition(app,dt)
select dt
      ,key
      ,browser
      ,browser_version
      ,coalesce(c.country,'Unknown') as country  
      ,to_date(install_ts_pretty) as install_date
      ,lang
      ,level
      ,os
      ,os_version
      ,count(distinct case when first_login = 1 then md5(concat(app,uid)) else null end) as first_login_cnt
      ,count(distinct md5(concat(app,uid))) as login_user_cnt
      ,count(md5(concat(app,uid))) as login_cnt
      ,app
      ,dt
from raw_4th_scene_login_daily r
left join dim_country c
on c.country_code = r.country_code WHERE dt = '${hiveconf:rpt_date}'
group by  to_date(ts_pretty) 
      ,key
      ,browser
      ,browser_version
      ,coalesce(c.country,'Unknown')
      ,to_date(install_ts_pretty)
      ,lang
      ,level
      ,os
      ,os_version
      ,app
      ,dt
;



insert overwrite table agg_4th_scene_currency_transaction partition(app,dt)
select dt
      ,key
      ,browser
      ,browser_version
      ,coalesce(c.country,'Unknown') as country
      ,lang
      ,level
      ,os
      ,os_version
      ,action
      ,action_type
      ,sum(case when chef_points_in is null then 0 else chef_points_in end) as chefs_point_in
      ,sum(case when chef_points_out is null then 0 else chef_points_out end) as chefs_point_out
      ,sum(case when silver_coins_in is null then 0 else silver_coins_in end) as silver_coins_in
      ,sum(case when silver_coins_out is null then 0 else silver_coins_out end) as silver_coins_out
      ,sum(case when reputation_in is null then 0 else reputation_in end) as reputation_in
      ,sum(case when reputation_out is null then 0 else reputation_out end) as reputation_out
      ,app
      ,dt
from raw_4th_scene_currency_daily r
left join dim_country c
on c.country_code = r.country_code WHERE dt = '${hiveconf:rpt_date}'
group by  to_date(ts_pretty)
      ,key
      ,browser
      ,browser_version
      ,coalesce(c.country,'Unknown')
      ,level
      ,lang
      ,os
      ,os_version
      ,action
      ,action_type
      ,app
      ,dt
;



--------------------------------------------------
--- daily handprint events
--------------------------------------------------

INSERT OVERWRITE TABLE raw_handprint_daily PARTITION (app, dt)
SELECT 
 key,
 ts,
 ts_pretty,
 browser,
 browser_version,
 country_code,
 event,
 install_source,
 install_ts,
 install_ts_pretty,
 ip,
 lang,
 level,
 os,
 os_version,
 snsid,
 uid,
 fb_source,
 action,
 handprint_in,
 handprint_qty,
 handprint_level,
 reward_type,
 reward_item,
 app,
 '${RPT_DATE}' dt
FROM farm_1_1.raw_handprint_seq
WHERE 
 (
  (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
  OR
  (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
 ) 
 AND 
 to_date(ts_pretty) = '${hiveconf:rpt_date}'
;


--------------------------------------------------
--- Insert agg_handprint data
--------------------------------------------------

INSERT OVERWRITE TABLE agg_handprint PARTITION (app, dt)
select h.key
,to_date(h.ts_pretty) as date
,h.snsid
,h.uid 
,h.browser
,coalesce(c.country,'Unknown') as country
,h.install_source
,to_date(h.install_ts_pretty) as install_date
,h.action
,h.handprint_level
,sum(h.handprint_in) as handprint_in
,h.app
,h.dt
from raw_handprint_daily h
left join dim_country c
on h.country_code = c.country_code
where h.dt = '${hiveconf:rpt_date}'
group by    h.key
,to_date(h.ts_pretty)
,h.snsid
,h.uid 
,h.browser
,coalesce(c.country,'Unknown')
,h.install_source
,to_date(h.install_ts_pretty)
,h.action
,h.handprint_level
,h.app
,h.dt
;
--------------------------------------------------
--- trade orders
--------------------------------------------------

insert OVERWRITE TABLE fact_tradeorders PARTITION (app, dt)
select  to_date(ts_pretty) as date
 ,to_date(install_ts_pretty) as install_date
 ,level
 ,action
 ,experience
 ,reward_coins
 ,special_num
 ,special_item
,sum(exchange_num_1)  as exchange_num_1
 ,exchange_item_1
 ,sum(exchange_num_2)  as exchange_num_2
 ,exchange_item_2
 ,sum(exchange_num_3)  as exchange_num_3
 ,exchange_item_3
 ,count(1) as times,
app, 
dt
from raw_tradeorders_daily
where dt='${hiveconf:rpt_date}'
group by  to_date(ts_pretty)
 ,to_date(install_ts_pretty)
 ,level
 ,action
 ,experience
 ,reward_coins
 ,special_num
 ,special_item
 ,exchange_item_1
 ,exchange_item_2
 ,exchange_item_3, app, dt
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_tradeorders DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_fact_tradeorders PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT
date
 ,install_date
 ,level
 ,action
 ,experience
 ,reward_coins
 ,special_num
 ,special_item
 ,exchange_num_1
 ,exchange_item_1
 ,exchange_num_2
 ,exchange_item_2
 ,exchange_num_3
 ,exchange_item_3
 ,times
 ,app
FROM fact_tradeorders
where dt='${hiveconf:rpt_date}';


--------------------------------------------------
--- copy to redshift - copy_agg_4th_scene
--------------------------------------------------

ALTER TABLE copy_agg_4th_scene_login DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

insert overwrite table copy_agg_4th_scene_login partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select  date  
       ,app_id 
       ,browser 
       ,brower_version 
       ,country 
       ,install_date  
       ,language  
       ,level 
       ,os 
       ,os_version  
       ,first_login_cnt 
       ,login_user_cnt 
       ,login_cnt 
from agg_4th_scene_login 
where dt='${hiveconf:rpt_date}'
;


ALTER TABLE copy_agg_4th_scene_currency_transaction DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

insert overwrite table copy_agg_4th_scene_currency_transaction partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select  date  
       ,app_id 
       ,browser 
       ,brower_version
       ,country 
       ,language  
       ,level 
       ,os 
       ,os_version  
       ,action
       ,action_type
       ,chef_points_in 
       ,chef_points_out 
       ,silver_coins_in 
       ,silver_coins_out 
       ,reputation_in 
       ,reputation_out 
from agg_4th_scene_currency_transaction 
where dt='${hiveconf:rpt_date}'
;


ALTER TABLE copy_4th_scene_level_up_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

insert overwrite table copy_4th_scene_level_up_daily partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select 
    key
    ,ts_pretty
    ,browser
    ,browser_version
    ,country_code
    ,install_source
    ,install_ts_pretty
    ,ip
    ,lang
    ,level
    ,os
    ,os_version
    ,snsid
    ,uid
    ,fb_source
    ,previous_level
    ,current_level
    ,reward_info 
from raw_4th_scene_level_up_daily 
where dt='${hiveconf:rpt_date}'
;

ALTER TABLE copy_4th_scene_login_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

insert overwrite table copy_4th_scene_login_daily partition (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select  distinct key
       ,date
       ,browser
       ,browser_version
       ,country_code
       ,install_source
       ,install_date
       ,ip
       ,language
       ,level
       ,os
       ,os_version
       ,snsid 
       ,uid 
       ,fb_source
       ,first_login
from
     (
      select  key 
             ,to_date(ts_pretty) as date 
             ,first_value(browser) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as browser
             ,first_value(browser_version) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as browser_version
             ,first_value(country_code) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as country_code
             ,first_value(install_source) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as install_source
             ,first_value(to_date(install_ts_pretty)) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as install_date
             ,first_value(ip) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as ip
             ,first_value(lang) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as language
             ,first_value(level) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as level
             ,first_value(os) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as os
             ,first_value(os_version) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as os_version
             ,snsid 
             ,uid 
             ,first_value(fb_source) over(partition by key, uid, to_date(ts_pretty) order by ts_pretty desc
              rows between unbounded preceding AND unbounded following) as fb_source
             ,first_value(first_login) over(partition by key, uid, to_date(ts_pretty) order by first_login desc
              rows between unbounded preceding AND unbounded following) as first_login
      from raw_4th_scene_login_daily
      where dt='${hiveconf:rpt_date}'
     )t
;

--------------------------------------------------
--- copy to redshift - agg_handprint
--------------------------------------------------

ALTER TABLE copy_agg_handprint DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


insert overwrite table copy_agg_handprint partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select  key as app_id 
,date
,snsid
,uid
,browser 
,country 
,install_source 
,install_date 
,action 
,handprint_level 
,handprint_in 
from agg_handprint where dt='${hiveconf:rpt_date}';

ALTER TABLE copy_personal_info_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_personal_info_daily PARTITION(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
SELECT 
  MD5(concat(app,uid,ts)) as id,
  key as app_id,
  ts_pretty as ts,
  to_date(ts_pretty) as date,
  browser,
  browser_version,
  country_code,
  event,
  install_source,
  install_ts_pretty as install_ts,
  to_date(install_ts_pretty) as install_date,
  ip,
  lang,
  level,
  os,
  os_version,
  snsid,
  uid,
  fb_source,
  additional_email
FROM 
  raw_personal_info_daily
WHERE 
 dt='${hiveconf:rpt_date}';



--ALTER TABLE copy_raw_marketcratefinish_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

--INSERT OVERWRITE TABLE copy_raw_marketcratefinish_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
--SELECT
--uid, 
--key, 
--ts_pretty,
--country_code, 
--snsid, 
--level, 
--item_id, 
--order_points_get, 
--item_quantity, 
--assist
--FROM 
-- raw_marketcratefinish_daily WHERE dt='${hiveconf:rpt_date}'
--;


--ALTER TABLE copy_raw_marketcrateneedhelp_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


--INSERT OVERWRITE TABLE copy_raw_marketcrateneedhelp_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
--SELECT
--  uid, 
-- key, 
--  ts_pretty,
--  country_code, 
--  snsid, 
--  level, 
--  item_id, 
--  item_quantity
--  FROM 
--raw_marketcrateneedhelp_daily  WHERE  dt='${hiveconf:rpt_date}'
--  ;


ALTER TABLE copy_raw_marketexchange_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_raw_marketexchange_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
  uid, 
  key, 
  ts_pretty,
  country_code, 
  snsid, 
  level, 
  item_id, 
  item_quantity, 
  new_cash1_cost,
  new_cash2_cost,
  new_cash3_cost,
  new_cash1_left, 
  new_cash2_left,  
  new_cash3_left  
  FROM 
raw_marketexchange_daily
WHERE dt='${hiveconf:rpt_date}'
;


ALTER TABLE copy_raw_marketorderbegin_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_raw_marketorderbegin_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
  uid, 
  key, 
  ts_pretty,
  country_code, 
  snsid, 
  level, 
  order_points, 
  time_left,
  new_cash1,
  new_cash2,
  new_cash3,
  crates,
  rc_cost, 
  rc_bal
  FROM 
raw_marketorderbegin_daily
WHERE  dt='${hiveconf:rpt_date}'
;



ALTER TABLE copy_raw_marketorderfinish_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_raw_marketorderfinish_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
  uid, 
  key, 
  ts_pretty,
  country_code, 
  snsid, 
  level, 
  order_points_get, 
  item_quantity, 
  time_left,
  new_cash1_get,
  new_cash2_get,
  new_cash3_get,
  crates
  FROM 
raw_marketorderfinish_daily 
WHERE dt='${hiveconf:rpt_date}'
;



ALTER TABLE copy_raw_marketordernew_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


 INSERT OVERWRITE TABLE copy_raw_marketordernew_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
  uid, 
  key, 
  ts_pretty,
  country_code, 
  snsid, 
  level, 
  order_points, 
  time_left,
  new_cash1,
  new_cash2,
  new_cash3,
  crates,
  rc_cost, 
  rc_bal
  FROM 
raw_marketordernew_daily
WHERE dt='${hiveconf:rpt_date}'
;




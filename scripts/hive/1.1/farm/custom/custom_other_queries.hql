use farm_1_1;


SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;


--------------------------------------------------
--- Notif events
--------------------------------------------------
INSERT OVERWRITE TABLE agg_notif PARTITION (app, dt)
select
    s.date
    ,s.app_id
    ,s.notif_name
    ,s.total_send
    ,c.total_click
    ,(c.total_click*1.00/s.total_send) as ctr
    ,s.app_id as app
    ,s.date as dt
from
    (select
        to_date(ts_pretty) as date
        ,app_id
        ,properties['notif_name'] as notif_name
        ,count(*) as total_send
    from
        raw_notif_send_daily
    where
        to_date(ts_pretty) >= date_sub('${RPT_DATE}', 7)
    group by
        to_date(ts_pretty)
        ,app_id
        ,properties['notif_name']
    ) s
    left join
    (select
        to_date(rs.ts_pretty) as date
        ,rs.app_id
        ,rs.properties['notif_name'] as notif_name
        ,count(*) as total_click
    from
        raw_notif_send_daily rs
        join
        raw_notif_click_daily rc
        on
            rs.app_id = rc.app_id
            and lower(rs.properties['notif_id']) = lower(rc.properties['notif_id'])
            and rs.properties['snsid'] = rc.properties['snsid']
    where
        to_date(rs.ts_pretty) >= date_sub('${RPT_DATE}', 7)
    group by
        to_date(rs.ts_pretty)
        ,rs.app_id
        ,rs.properties['notif_name']
    ) c
    on
        s.date = c.date
        and s.app_id = c.app_id
        and s.notif_name = c.notif_name
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

--------------------------------------------------
--- Notif events
--------------------------------------------------
ALTER TABLE copy_agg_notif DROP IF EXISTS PARTITION (app='all');

insert overwrite table copy_agg_notif partition(app='all',dt='${RPT_DATE}')
select  date
       ,app_id
       ,notif_name
       ,total_send
       ,total_click
       ,ctr
from agg_notif 
where dt >= date_sub('${RPT_DATE}', 7)
;

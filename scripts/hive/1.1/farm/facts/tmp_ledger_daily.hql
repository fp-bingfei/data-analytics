----------------------------------
-- Query to generate tmp_ledger_daily
----------------------------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;



SET rpt_date = ${RPT_DATE};
set rpt_app = ${RPT_APP};


ALTER TABLE tmp_ledger_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE tmp_ledger_daily PARTITION (app, dt)
select dt AS date_start, app AS app_name, md5(concat(app, uid)) AS user_key, uid
, max(coins_bal) as coins_bal, max(coins_in) as coins_in, max(coins_out) as coins_out
, max(rc_bal) AS rc_bal, max(rc_in) as rc_in, max(rc_out) as rc_out,max(coins_flag), max(rc_flag), app, dt FROM
(
select * from (select app, dt, uid, first_value(coins_bal) over (partition by app, dt, uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as coins_bal,  sum(coins_in) over (partition by app,dt,uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS coins_in, sum(coins_out) over (partition by app,dt,uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS coins_out, 0 as rc_bal, 0 as rc_in, 0 as rc_out,1 as coins_flag,0 as rc_flag, row_number() over (partition by app,dt,uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rownum
from raw_coins_transaction_daily where  dt='${hiveconf:rpt_date}')cb where rownum = 1
UNION ALL
select * from (select app, dt, uid, 0 as coins_bal, 0 as coins_in, 0 as coins_out, first_value(rc_bal) over (partition by app, dt, uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as rc_bal, sum(rc_in) over (partition by app,dt,uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rc_in, sum(rc_out) over (partition by app,dt,uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rc_out,0 as coins_flag,1 as rc_flag, row_number() over (partition by app,dt,uid order by ts_pretty desc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS rownum
from raw_rc_transaction_daily where dt='${hiveconf:rpt_date}') rb where rownum = 1
) tmp
group by dt, app, md5(concat(app, uid)), uid;



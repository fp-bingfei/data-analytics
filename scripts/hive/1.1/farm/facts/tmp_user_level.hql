
---------create a temp table from fact_levelup which gives levelup_ts, level_start and level_end from the fact_levelup table -------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;



--set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};


--ALTER TABLE tmp_user_level DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE tmp_user_level PARTITION (app, dt)
select levelup_date,
user_key,
app_id,
max(levelup_ts) levelup_ts,
min(previous_level) level_start,
max(current_level) level_end,
app,
dt
from fact_levelup where dt ='${hiveconf:rpt_date}'
group by levelup_date,app_id,user_key, app, dt
;


----------------------------------
-- Query to generate fact_levelup
----------------------------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;



set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
--set rpt_geo=${RPT_GEO};
set rpt_all_app=${RPT_ALL_APP};

INSERT OVERWRITE TABLE fact_levelup PARTITION (app,dt)
select md5(concat(l.key, l.uid,l.level,l.ts)) AS id, md5(concat(l.key, l.uid)) AS user_key, l.key as app_id, l.uid as app_user_id
, null as session_id, COALESCE(lag(l.level) over (partition by l.key, l.uid order by l.level),l.level - 1) AS previous_level
, to_date(COALESCE(lag(l.ts_pretty) over (partition by l.key, l.uid order by l.level), prev.levelup_ts, l.install_ts_pretty)) AS previous_levelup_date
, COALESCE(lag(l.ts_pretty) over (partition by l.key, l.uid order by l.level), prev.levelup_ts, l.install_ts_pretty) AS previous_levelup_ts
, l.level, l.dt AS levelup_date, l.ts_pretty AS levelup_ts, l.browser, l.browser_version, l.os, l.os_version
, null as device, l.coins_bal as coins_wallet, l.coins_get as coins_in
, l.country_code, l.ip, l.lang AS language, l.rc_bal as rc_wallet, l.rc_get as rc_in
, null as locale, null as ab_experiment, null as ab_variant, l.app, l.dt
from 
(
select * from (
select *, row_number() over (partition by app, uid, level order by ts_pretty asc) rnum 
from raw_levelup_daily where dt ='${hiveconf:rpt_date}'
) X where rnum = 1
) l
left outer join 
(select app, app_user_id, install_ts, levelup_ts from farm_1_1.dim_user where dt = '${hiveconf:rpt_date_d1}')
prev on (l.uid = prev.app_user_id AND l.app = prev.app);

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_levelup DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_fact_levelup PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select
id, user_key, app_id, app_user_id, session_id, previous_level, previous_levelup_date, previous_levelup_ts, current_level, levelup_date, levelup_ts, browser, browser_version, 
os, os_version,  device, coin_wallet, coins_in, country_code, ip, language, rc_wallet, rc_in, locale, ab_experiment, ab_variant 
from fact_levelup where dt='${hiveconf:rpt_date}';


------- CREATE TABLES IF NOT EXISTS --------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;



set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_date_plus1_nohyphen = ${RPT_DATE_PLUS1_NOHYPHEN};

-- Single temp_user_geo table for all the locales
CREATE EXTERNAL TABLE IF NOT EXISTS temp_user_geo(
uid STRING,
snsid STRING,
email STRING,
level INT,
experience INT,
coins INT,
reward_points INT,
new_cash1 INT,
new_cash2 INT,
new_cash3 INT,
order_points INT,
time_points INT,
op INT,
gas INT,
lottery_coins INT,
size_x INT,
size_y INT,
top_map_size INT,
max_work_area_size INT,
work_area_size INT,
addtime timestamp,
logintime timestamp,
loginip STRING,
status INT,
continuous_day INT,
point INT,
love_points INT,
feed_status STRING,
track_ref STRING,
extra_info STRING,
fish_op STRING,
name STRING,
picture STRING,
loginnum INT,
note STRING,
fb_source STRING,
pay_times INT,
water_exp INT,
water_level INT,
greenery INT,
sign_points INT,
fb_source_last STRING,
track_ref_last STRING
) PARTITIONED BY (app STRING, dt STRING)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION "s3://com.funplusgame.bidata/etl/farm/"
TBLPROPERTIES('serialization.null.format'='');

-- Add partitions for all the apps
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.us.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/us/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.de.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/de/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.fr.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/fr/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.it.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/it/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.tw.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/tw/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.nl.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/nl/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.pl.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/pl/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.br.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/br/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';
ALTER TABLE temp_user_geo ADD IF NOT EXISTS PARTITION (app='farm.th.prod', dt='${hiveconf:rpt_date}') LOCATION 's3://com.funplusgame.bidata/etl/farm/th/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/';


CREATE TABLE IF NOT EXISTS temp_user_geo_np(
user_id string, 
uid string, 
snsid string, 
email string,
user_name string,
addtime timestamp, 
logintime timestamp, 
loginip string, 
status int,
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
TBLPROPERTIES ('serialization.null.format'='');

INSERT OVERWRITE TABLE temp_user_geo_np
SELECT
md5(concat(
if(uid is null, "uid", uid),
if(snsid is null, "snsid", snsid),
if(email is null, "email", email)
)) as user_id,
uid,
snsid,
email,
name as user_name,
addtime,
logintime,
loginip,
status,
app,
dt
FROM
(select  uid, snsid, email, name, addtime, logintime, loginip, status, row_number() over (partition by app,uid order by logintime desc) AS rnum, app, dt
FROM temp_user_geo WHERE dt='${hiveconf:rpt_date}' AND uid <> 'uid')tbl where rnum = 1;

CREATE EXTERNAL TABLE IF NOT EXISTS user_geo(
user_id string, 
uid string, 
snsid string, 
email string,
user_name string,
addtime timestamp, 
logintime timestamp, 
loginip string, 
status int)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/others/user_geo'
TBLPROPERTIES ('serialization.null.format'='');


--- Querys ----

INSERT OVERWRITE TABLE user_geo PARTITION (app, dt)
SELECT
user_id,
uid,
snsid,
email,
user_name,
addtime,
logintime,
loginip,
status,
app,
dt
FROM temp_user_geo_np WHERE dt='${hiveconf:rpt_date}';
 

 -- Drop Old partitions in temp table --
 
ALTER TABLE temp_user_geo DROP IF EXISTS PARTITION (dt < '${hiveconf:rpt_date}');
ALTER TABLE user_geo DROP IF EXISTS PARTITION (dt < '${hiveconf:rpt_date}');

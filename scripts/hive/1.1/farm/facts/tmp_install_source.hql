use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;



set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};



INSERT OVERWRITE TABLE tmp_install_source PARTITION (app,dt)
select
app_id,
user_key,
uid,
case

when (install_source like '%bookmark%' or fb_source like '%bookmark%') then 'bookmark'

when (install_source like '%FF_%' or fb_source='ad' ) then 'ads'

when install_source='search' or fb_source='search' or (install_source='ts' and fb_source='') or (install_source='br_tf' and fb_source='') then 'search'

when fb_source='' and install_source='' then 'empty'

when install_source=='og_make' then 'og_make'

when install_source='og_craft' then 'og_craft'

when install_source='og_cook' then 'og_cook'

when install_source='og_collect' then 'og_collect'

when install_source='og_harvest' then 'og_harvest'

when install_source='og_fill' then 'og_fill'

when install_source='og_feed' then 'og_feed'

when fb_source='appcenter_request' then 'appcenter_request'

when (fb_source='feed_playing' or fb_source='ego') then 'mayLike'

when fb_source='rightcolumn' or fb_source='hovercard' or fb_source='canvas_recommended' or fb_source like '%recommended%' or install_source like '%recommended%' then 'recommended'

when install_source='appcenter' or fb_source='appcenter' then 'appCenter'

when fb_source ='fbpage' or install_source='empty' or install_source like '%FP_%' then 'fanPage'

when fb_source='timeline' then 'timeLine'

when install_source='reminders' then 'reminders'

when install_source='feed' or fb_source like '%feed%' then 'feed'

when fb_source='request' or install_source='notif' then 'request'

when install_source='' and fb_source='dialog' then 'dialog'

when install_source='og_complete' then 'og_complete'

when install_source='' and fb_source='canvas_featured' then 'canvas_featured'

when install_source='' and fb_source='sidebar_featured' then 'sidebar_featured'

when install_source='' and fb_source='appcenter_featured' then 'appcenter_featured'

when (install_source ='ff15ar' or install_source='ff15tr' or install_source like '%YB-Z%') and instr(install_source,'YB-Z9B')==0 and instr(install_source,'YB-Z14B')==0 and instr(install_source,'YB-Z31B') ==0 and instr(install_source,'YB-Z12B') ==0 and instr(install_source,'YB-Z127B') ==0 and instr(install_source,'YB-Z128B') ==0 and instr(install_source,'YB-Z18B') ==0 and instr(install_source,'YB-Z125B') ==0 and instr(install_source,'YB-Z17B') ==0 and instr(install_source,'YB-Z16B') ==0 and instr(install_source,'YB-Z126B') ==0 then 'internal_promotion_banner'

when (install_source='ff15ar' or instr(install_source,'YB-Z9B')==1 or instr(install_source,'yb-z9b')==1) then 'farm_ae'

when (install_source='ff15tr' or instr(install_source,'YB-Z14B')==1 or instr(install_source,'yb-z14b')==1) then 'farm_tr'

when (instr(install_source,'YB-Z31B')==1 or instr(install_source,'yb-z31b')==1) then 'farm_br'

when (instr(install_source,'YB-Z127B')==1 or instr(install_source,'YB-Z12B')==1 or instr(install_source,'yb-z127b')==1 or instr(install_source,'yb-z12b')==1) then 'farm_fr'


when (instr(install_source,'YB-Z128B')==1 or instr(install_source,'YB-Z18B')==1 or instr(install_source,'yb-z128b')==1 or instr(install_source,'yb-z18b')==1) then 'farm_nl'

when (instr(install_source,'YB-Z125B')==1 or instr(install_source,'YB-Z17B')==1 or instr(install_source,'yb-z125b')==1 or instr(install_source,'yb-z17b')==1) then 'farm_th'

when (instr(install_source,'YB-Z126B')==1 or instr(install_source,'YB-Z16B')==1 or instr(install_source,'yb-z126b')==1 or instr(install_source,'yb-z16b')==1) then 'farm_de'

else 'Others'

end as install_src,
app,
dt

from tmp_user_daily_login where  dt='${hiveconf:rpt_date}';




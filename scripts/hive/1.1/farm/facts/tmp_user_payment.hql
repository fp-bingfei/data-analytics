-------- payment daily script ------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;




set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};

INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app, dt)
select 
p.user_key,
p.app_id,
p.date,
coalesce(d.conversion_ts,p.conversion_ts) conversion_ts,
revenue_usd,
purchase_cnt,
coalesce(to_date(p.last_payment_ts),d.last_payment_date) last_payment_date,
p.app,
p.date
from (select app_id, user_key, date, min(ts) conversion_ts, sum(revenue_usd) revenue_usd, count(ts) purchase_cnt, max(ts) last_payment_ts,app from fact_revenue 
where dt='${hiveconf:rpt_date}'
group by app_id, user_key, date,app )p
left outer join (select * from farm_1_1.dim_user where dt='${hiveconf:rpt_date_d1}')d
on p.user_key=d.user_key and p.app=d.app;



use farm_1_1;


DROP TABLE raw_coins_transaction_seq;

CREATE EXTERNAL TABLE `raw_coins_transaction_seq`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `action` string COMMENT 'from deserializer', 
  `action_detail` string COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `coins_bal` double COMMENT 'from deserializer', 
  `coins_in` double COMMENT 'from deserializer', 
  `coins_out` double COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `location` string COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer')
PARTITIONED BY ( 
  `app` string, 
  `year` int, 
  `month` int, 
  `day` int, 
  `hour` int)
  ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
  WITH SERDEPROPERTIES (
  "mapping.ts" = "@ts",
  "mapping.ts_pretty" ="@ts_pretty",
  "mapping.key"="@key"
  )
STORED AS SEQUENCEFILE
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_seq/coins_transaction'
  TBLPROPERTIES('serialization.null.format'='');

  DROP TABLE raw_levelup_seq;


  CREATE EXTERNAL TABLE `raw_levelup_seq`(
    `key` string COMMENT 'from deserializer', 
    `ts` string COMMENT 'from deserializer', 
    `ts_pretty` timestamp COMMENT 'from deserializer', 
    `browser` string COMMENT 'from deserializer', 
    `browser_version` string COMMENT 'from deserializer', 
    `coins_bal` string COMMENT 'from deserializer', 
    `coins_get` bigint COMMENT 'from deserializer', 
    `country_code` string COMMENT 'from deserializer', 
    `event` string COMMENT 'from deserializer', 
    `install_source` string COMMENT 'from deserializer', 
    `install_ts` string COMMENT 'from deserializer', 
    `install_ts_pretty` timestamp COMMENT 'from deserializer', 
    `ip` string COMMENT 'from deserializer', 
    `lang` string COMMENT 'from deserializer', 
    `level` bigint COMMENT 'from deserializer', 
    `os` string COMMENT 'from deserializer', 
    `os_version` string COMMENT 'from deserializer', 
    `rc_bal` bigint COMMENT 'from deserializer', 
    `rc_get` string COMMENT 'from deserializer', 
    `snsid` string COMMENT 'from deserializer', 
    `uid` string COMMENT 'from deserializer')
  PARTITIONED BY ( 
    `app` string, 
    `year` int, 
    `month` int, 
    `day` int, 
    `hour` int)
  ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
  STORED AS SEQUENCEFILE
  LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/events_seq/levelup'
    TBLPROPERTIES('serialization.null.format'='');


    DROP TABLE raw_newuser_seq;


CREATE EXTERNAL TABLE `raw_newuser_seq`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `gender` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `device` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer')
PARTITIONED BY ( 
  `app` string, 
  `year` int, 
  `month` int, 
  `day` int, 
  `hour` int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
STORED AS SEQUENCEFILE
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_seq/newuser'
TBLPROPERTIES (
  'serialization.null.format'='');

  DROP TABLE raw_payment_seq;
  
  
  
  CREATE EXTERNAL TABLE `raw_payment_seq`(
    `key` string COMMENT 'from deserializer', 
    `ts` string COMMENT 'from deserializer', 
    `ts_pretty` timestamp COMMENT 'from deserializer', 
    `amount` double COMMENT 'from deserializer', 
    `browser` string COMMENT 'from deserializer', 
    `browser_version` string COMMENT 'from deserializer', 
    `coins_bal` double COMMENT 'from deserializer', 
    `coins_in` bigint COMMENT 'from deserializer', 
    `country_code` string COMMENT 'from deserializer', 
    `currency` string COMMENT 'from deserializer', 
    `event` string COMMENT 'from deserializer', 
    `install_source` string COMMENT 'from deserializer', 
    `install_ts` string COMMENT 'from deserializer', 
    `install_ts_pretty` timestamp COMMENT 'from deserializer', 
    `ip` string COMMENT 'from deserializer', 
    `is_gift` bigint COMMENT 'from deserializer', 
    `lang` string COMMENT 'from deserializer', 
    `level` bigint COMMENT 'from deserializer', 
    `os` string COMMENT 'from deserializer', 
    `os_version` string COMMENT 'from deserializer', 
    `payment_processor` string COMMENT 'from deserializer', 
    `product_id` string COMMENT 'from deserializer', 
    `product_name` string COMMENT 'from deserializer', 
    `product_type` string COMMENT 'from deserializer', 
    `raw` struct<amount:double,cash_type:string,currency:string,from_uid:string,gameamount:string,gift:string,gift_id:string,level:bigint,paid_time:string,status:bigint,type:string,uid:string> COMMENT 'from deserializer', 
    `rc_bal` bigint COMMENT 'from deserializer', 
    `rc_in` bigint COMMENT 'from deserializer', 
    `snsid` string COMMENT 'from deserializer', 
    `transaction_id` string COMMENT 'from deserializer', 
    `uid` string COMMENT 'from deserializer', 
    `fb_source` string COMMENT 'from deserializer')
  PARTITIONED BY ( 
    `app` string, 
    `year` int, 
    `month` int, 
    `day` int, 
    `hour` int)
  ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
  STORED AS SEQUENCEFILE
  LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/events_seq/payment'
  TBLPROPERTIES (
    'serialization.null.format'=''); 

    DROP TABLE raw_personal_info_seq;
  
  
  CREATE EXTERNAL TABLE `raw_personal_info_seq`(
    `key` string COMMENT 'from deserializer', 
    `ts` string COMMENT 'from deserializer', 
    `ts_pretty` timestamp COMMENT 'from deserializer', 
    `browser` string COMMENT 'from deserializer', 
    `browser_version` string COMMENT 'from deserializer', 
    `country_code` string COMMENT 'from deserializer', 
    `event` string COMMENT 'from deserializer', 
    `install_source` string COMMENT 'from deserializer', 
    `install_ts` string COMMENT 'from deserializer', 
    `install_ts_pretty` timestamp COMMENT 'from deserializer', 
    `ip` string COMMENT 'from deserializer', 
    `lang` string COMMENT 'from deserializer', 
    `level` bigint COMMENT 'from deserializer', 
    `os` string COMMENT 'from deserializer', 
    `os_version` string COMMENT 'from deserializer', 
    `snsid` string COMMENT 'from deserializer', 
    `uid` string COMMENT 'from deserializer', 
    `fb_source` string COMMENT 'from deserializer', 
    `additional_email` string COMMENT 'from deserializer')
  PARTITIONED BY ( 
    `app` string, 
    `year` int, 
    `month` int, 
    `day` int, 
    `hour` int)
  ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
  STORED AS SEQUENCEFILE
  LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/events_seq/personal_info'
  TBLPROPERTIES (
      'serialization.null.format'=''); 
  

      DROP TABLE raw_quest_seq;
 
 
 
 CREATE EXTERNAL TABLE `raw_quest_seq`(
   `key` string COMMENT 'from deserializer', 
   `ts` string COMMENT 'from deserializer', 
   `ts_pretty` timestamp COMMENT 'from deserializer', 
   `action` string COMMENT 'from deserializer', 
   `browser` string COMMENT 'from deserializer', 
   `browser_version` string COMMENT 'from deserializer', 
   `country_code` string COMMENT 'from deserializer', 
   `event` string COMMENT 'from deserializer', 
   `install_source` string COMMENT 'from deserializer', 
   `install_ts` string COMMENT 'from deserializer', 
   `install_ts_pretty` timestamp COMMENT 'from deserializer', 
   `ip` string COMMENT 'from deserializer', 
   `lang` string COMMENT 'from deserializer', 
   `level` bigint COMMENT 'from deserializer', 
   `os` string COMMENT 'from deserializer', 
   `os_version` string COMMENT 'from deserializer', 
   `quest_id` string COMMENT 'from deserializer', 
   `quest_type` string COMMENT 'from deserializer', 
   `snsid` string COMMENT 'from deserializer', 
   `task_id` string COMMENT 'from deserializer', 
   `uid` string COMMENT 'from deserializer', 
   `rc_in` string COMMENT 'from deserializer', 
   `rc_out` string COMMENT 'from deserializer', 
   `rc_bal` string COMMENT 'from deserializer', 
   `coins_in` double COMMENT 'from deserializer', 
   `coins_out` double COMMENT 'from deserializer', 
   `coins_bal` double COMMENT 'from deserializer')
 PARTITIONED BY ( 
   `app` string, 
   `year` int, 
   `month` int, 
   `day` int, 
   `hour` int)
 ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
 STORED AS SEQUENCEFILE
 LOCATION
   's3://com.funplus.datawarehouse/farm_1_1/events_seq/quest'
 TBLPROPERTIES (
   'serialization.null.format'=''); 
 
 
   DROP TABLE raw_rc_transaction_seq;
   
   
   CREATE EXTERNAL TABLE `raw_rc_transaction_seq`(
     `key` string COMMENT 'from deserializer', 
     `ts` string COMMENT 'from deserializer', 
     `ts_pretty` timestamp COMMENT 'from deserializer', 
     `action` string COMMENT 'from deserializer', 
     `action_detail` string COMMENT 'from deserializer', 
     `browser` string COMMENT 'from deserializer', 
     `browser_version` string COMMENT 'from deserializer', 
     `country_code` string COMMENT 'from deserializer', 
     `event` string COMMENT 'from deserializer', 
     `install_source` string COMMENT 'from deserializer', 
     `install_ts` string COMMENT 'from deserializer', 
     `install_ts_pretty` timestamp COMMENT 'from deserializer', 
     `ip` string COMMENT 'from deserializer', 
     `lang` string COMMENT 'from deserializer', 
     `level` bigint COMMENT 'from deserializer', 
     `location` string COMMENT 'from deserializer', 
     `os` string COMMENT 'from deserializer', 
     `os_version` string COMMENT 'from deserializer', 
     `rc_bal` bigint COMMENT 'from deserializer', 
     `rc_in` bigint COMMENT 'from deserializer', 
     `rc_out` bigint COMMENT 'from deserializer', 
     `snsid` string COMMENT 'from deserializer', 
     `uid` string COMMENT 'from deserializer')
   PARTITIONED BY ( 
     `app` string, 
     `year` int, 
     `month` int, 
     `day` int, 
     `hour` int)
   ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
   STORED AS SEQUENCEFILE
   LOCATION
     's3://com.funplus.datawarehouse/farm_1_1/events_seq/rc_transaction'
   TBLPROPERTIES (
     'serialization.null.format'=''
 );
 
 DROP TABLE raw_session_start_seq;
  
 
 CREATE EXTERNAL TABLE `raw_session_start_seq`(
   `key` string COMMENT 'from deserializer', 
   `ts` string COMMENT 'from deserializer', 
   `ts_pretty` timestamp COMMENT 'from deserializer', 
   `browser` string COMMENT 'from deserializer', 
   `browser_version` string COMMENT 'from deserializer', 
   `country_code` string COMMENT 'from deserializer', 
   `event` string COMMENT 'from deserializer', 
   `gender` string COMMENT 'from deserializer', 
   `install_source` string COMMENT 'from deserializer', 
   `install_ts` string COMMENT 'from deserializer', 
   `install_ts_pretty` timestamp COMMENT 'from deserializer', 
   `ip` string COMMENT 'from deserializer', 
   `lang` string COMMENT 'from deserializer', 
   `level` bigint COMMENT 'from deserializer', 
   `os` string COMMENT 'from deserializer', 
   `os_version` string COMMENT 'from deserializer', 
   `snsid` string COMMENT 'from deserializer', 
   `uid` string COMMENT 'from deserializer', 
   `device` string COMMENT 'from deserializer', 
   `fb_source` string COMMENT 'from deserializer')
 PARTITIONED BY ( 
   `app` string, 
   `year` int, 
   `month` int, 
   `day` int, 
   `hour` int)
 ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
 STORED AS SEQUENCEFILE
 LOCATION
   's3://com.funplus.datawarehouse/farm_1_1/events_seq/session_start'
 TBLPROPERTIES (
   'serialization.null.format'='');
    
   DROP TABLE raw_tradeorders_seq;
	
 
   CREATE EXTERNAL TABLE `raw_tradeorders_seq`(
     `key` string COMMENT 'from deserializer', 
     `ts` string COMMENT 'from deserializer', 
     `ts_pretty` timestamp COMMENT 'from deserializer', 
     `browser` string COMMENT 'from deserializer', 
     `browser_version` string COMMENT 'from deserializer', 
     `country_code` string COMMENT 'from deserializer', 
     `event` string COMMENT 'from deserializer', 
     `gender` string COMMENT 'from deserializer', 
     `install_source` string COMMENT 'from deserializer', 
     `install_ts` string COMMENT 'from deserializer', 
     `install_ts_pretty` timestamp COMMENT 'from deserializer', 
     `ip` string COMMENT 'from deserializer', 
     `lang` string COMMENT 'from deserializer', 
     `level` bigint COMMENT 'from deserializer', 
     `os` string COMMENT 'from deserializer', 
     `os_version` string COMMENT 'from deserializer', 
     `snsid` string COMMENT 'from deserializer', 
     `uid` string COMMENT 'from deserializer', 
     `device` string COMMENT 'from deserializer', 
     `action` string COMMENT 'from deserializer', 
     `experience` string COMMENT 'from deserializer', 
     `reward_coins` bigint COMMENT 'from deserializer', 
     `special_num` bigint COMMENT 'from deserializer', 
     `special_item` bigint COMMENT 'from deserializer', 
     `exchange_num_1` bigint COMMENT 'from deserializer', 
     `exchange_item_1` bigint COMMENT 'from deserializer', 
     `exchange_num_2` bigint COMMENT 'from deserializer', 
     `exchange_item_2` bigint COMMENT 'from deserializer', 
     `exchange_num_3` bigint COMMENT 'from deserializer', 
     `exchange_item_3` bigint COMMENT 'from deserializer')
   PARTITIONED BY ( 
     `app` string, 
     `year` int, 
     `month` int, 
     `day` int, 
     `hour` int)
   ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
   STORED AS SEQUENCEFILE
   LOCATION
     's3://com.funplus.datawarehouse/farm_1_1/events_seq/TradeOrders'
   TBLPROPERTIES (
     'serialization.null.format'='');
 
  
 
 CREATE EXTERNAL TABLE `raw_tutorial_seq`(
   `key` string COMMENT 'from deserializer', 
   `ts` string COMMENT 'from deserializer', 
   `ts_pretty` timestamp COMMENT 'from deserializer', 
   `browser` string COMMENT 'from deserializer', 
   `browser_version` string COMMENT 'from deserializer', 
   `country_code` string COMMENT 'from deserializer', 
   `event` string COMMENT 'from deserializer', 
   `install_source` string COMMENT 'from deserializer', 
   `install_ts` string COMMENT 'from deserializer', 
   `install_ts_pretty` timestamp COMMENT 'from deserializer', 
   `ip` string COMMENT 'from deserializer', 
   `lang` string COMMENT 'from deserializer', 
   `level` bigint COMMENT 'from deserializer', 
   `os` string COMMENT 'from deserializer', 
   `os_version` string COMMENT 'from deserializer', 
   `snsid` string COMMENT 'from deserializer', 
   `step` bigint COMMENT 'from deserializer', 
   `uid` string COMMENT 'from deserializer')
 PARTITIONED BY ( 
   `app` string, 
   `year` int, 
   `month` int, 
   `day` int, 
   `hour` int)
 ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
     STORED AS SEQUENCEFILE
 LOCATION
   's3://com.funplus.datawarehouse/farm_1_1/events_seq/tutorial'
 TBLPROPERTIES (
     'serialization.null.format'='');


CREATE EXTERNAL TABLE `raw_marketcratefinish_seq`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points_get` bigint COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer', 
  `assist` string COMMENT 'from deserializer',
  `new_cash_3_get` string COMMENT 'from deserializer',
  `adv_point_get` string COMMENT 'from deserializer')
PARTITIONED BY ( 
  `app` string, 
  `year` int, 
  `month` int, 
  `day` int, 
  `hour` int)
   ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
 STORED AS SEQUENCEFILE
 LOCATION
   's3://com.funplus.datawarehouse/farm_1_1/events_seq/MarketCrateFinish'
 TBLPROPERTIES (
   'serialization.null.format'='');


		CREATE EXTERNAL TABLE `raw_marketcrateneedhelp_seq`(
	 `key` string COMMENT 'from deserializer', 
	 `ts` string COMMENT 'from deserializer', 
	 `ts_pretty` timestamp COMMENT 'from deserializer', 
	 `browser` string COMMENT 'from deserializer', 
	 `browser_version` string COMMENT 'from deserializer', 
	 `country_code` string COMMENT 'from deserializer', 
	 `event` string COMMENT 'from deserializer', 
	 `install_source` string COMMENT 'from deserializer', 
	 `install_ts` string COMMENT 'from deserializer', 
	 `install_ts_pretty` timestamp COMMENT 'from deserializer', 
	 `ip` string COMMENT 'from deserializer', 
	 `lang` string COMMENT 'from deserializer', 
	 `level` bigint COMMENT 'from deserializer', 
	 `os` string COMMENT 'from deserializer', 
	 `os_version` string COMMENT 'from deserializer', 
	 `uid` string COMMENT 'from deserializer', 
	 `snsid` string COMMENT 'from deserializer', 
	 `item_id` string COMMENT 'from deserializer', 
	 `fb_source` string COMMENT 'from deserializer', 
	 `item_quantity` bigint COMMENT 'from deserializer',
	 `is_guild` string COMMENT 'from deserializer')
		PARTITIONED BY ( 
	 `app` string, 
	 `year` int, 
	 `month` int, 
	 `day` int, 
	 `hour` int)
	  ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
		WITH SERDEPROPERTIES (
		"mapping.ts" = "@ts",
		"mapping.ts_pretty" ="@ts_pretty",
		"mapping.key"="@key"
		) 
	STORED AS SEQUENCEFILE
	LOCATION
	  's3://com.funplus.datawarehouse/farm_1_1/events_seq/MarketCrateNeedHelp'
	TBLPROPERTIES (
	  'serialization.null.format'='');


	  CREATE EXTERNAL TABLE `raw_marketexchange_seq`(
	    `key` string COMMENT 'from deserializer', 
	    `ts` string COMMENT 'from deserializer', 
	    `ts_pretty` timestamp COMMENT 'from deserializer', 
	    `browser` string COMMENT 'from deserializer', 
	    `browser_version` string COMMENT 'from deserializer', 
	    `country_code` string COMMENT 'from deserializer', 
	    `event` string COMMENT 'from deserializer', 
	    `install_source` string COMMENT 'from deserializer', 
	    `install_ts` string COMMENT 'from deserializer', 
	    `install_ts_pretty` timestamp COMMENT 'from deserializer', 
	    `ip` string COMMENT 'from deserializer', 
	    `lang` string COMMENT 'from deserializer', 
	    `level` bigint COMMENT 'from deserializer', 
	    `os` string COMMENT 'from deserializer', 
	    `os_version` string COMMENT 'from deserializer', 
	    `uid` string COMMENT 'from deserializer', 
	    `snsid` string COMMENT 'from deserializer', 
	    `item_id` string COMMENT 'from deserializer', 
	    `fb_source` string COMMENT 'from deserializer', 
	    `item_quantity` bigint COMMENT 'from deserializer',
	    `new_cash1_cost` bigint COMMENT 'from deserializer',
	    `new_cash2_cost` bigint COMMENT 'from deserializer',
	    `new_cash3_cost` bigint COMMENT 'from deserializer',
	    `new_cash1_left` bigint COMMENT 'from deserializer', 
	    `new_cash2_left` bigint COMMENT 'from deserializer',  
	    `new_cash3_left` bigint COMMENT 'from deserializer'     
	    )
	  PARTITIONED BY ( 
	    `app` string, 
	    `year` int, 
	    `month` int, 
	    `day` int, 
	    `hour` int)
	     ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
	  WITH SERDEPROPERTIES (
	  "mapping.ts" = "@ts",
	  "mapping.ts_pretty" ="@ts_pretty",
	  "mapping.key"="@key"
	  ) 
	   STORED AS SEQUENCEFILE
	   LOCATION
	     's3://com.funplus.datawarehouse/farm_1_1/events_seq/MarketExchange'
	   TBLPROPERTIES (
	     'serialization.null.format'='');
 
 
		 CREATE EXTERNAL TABLE `raw_marketorderfinish_seq`(
		   `key` string COMMENT 'from deserializer', 
		   `ts` string COMMENT 'from deserializer', 
		   `ts_pretty` timestamp COMMENT 'from deserializer', 
		   `browser` string COMMENT 'from deserializer', 
		   `browser_version` string COMMENT 'from deserializer', 
		   `country_code` string COMMENT 'from deserializer', 
		   `event` string COMMENT 'from deserializer', 
		   `install_source` string COMMENT 'from deserializer', 
		   `install_ts` string COMMENT 'from deserializer', 
		   `install_ts_pretty` timestamp COMMENT 'from deserializer', 
		   `ip` string COMMENT 'from deserializer', 
		   `lang` string COMMENT 'from deserializer', 
		   `level` bigint COMMENT 'from deserializer', 
		   `os` string COMMENT 'from deserializer', 
		   `os_version` string COMMENT 'from deserializer', 
		   `uid` string COMMENT 'from deserializer', 
		   `snsid` string COMMENT 'from deserializer', 
		   `item_id` string COMMENT 'from deserializer', 
		   `fb_source` string COMMENT 'from deserializer', 
		   `order_points_get` bigint COMMENT 'from deserializer', 
		   `item_quantity` bigint COMMENT 'from deserializer',
		   `time_left` bigint COMMENT 'from deserializer',
		   `new_cash1_get` bigint COMMENT 'from deserializer',
		   `new_cash2_get` bigint COMMENT 'from deserializer',
		   `new_cash3_get` bigint COMMENT 'from deserializer',
		   `crates` MAP<string, bigint> COMMENT 'from deserializer'
		     )
		 PARTITIONED BY ( 
		   `app` string, 
		   `year` int, 
		   `month` int, 
		   `day` int, 
		   `hour` int)
		    ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
		 WITH SERDEPROPERTIES (
		 "mapping.ts" = "@ts",
		 "mapping.ts_pretty" ="@ts_pretty",
		 "mapping.key"="@key"
		 ) 
		  STORED AS SEQUENCEFILE
		  LOCATION
		    's3://com.funplus.datawarehouse/farm_1_1/events_seq/MarketOrderFinish'
		  TBLPROPERTIES (
		    'serialization.null.format'='');
		
CREATE EXTERNAL TABLE `raw_marketorderbegin_seq`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1` bigint COMMENT 'from deserializer',
  `new_cash2` bigint COMMENT 'from deserializer',
  `new_cash3` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer',
  `rc_cost` double COMMENT 'from deserializer', 
  `rc_bal` double COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
  `app` string, 
  `year` int, 
  `month` int, 
  `day` int, 
  `hour` int)
   ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
 STORED AS SEQUENCEFILE
 LOCATION
   's3://com.funplus.datawarehouse/farm_1_1/events_seq/MarketOrderBegin'
 TBLPROPERTIES (
   'serialization.null.format'='');


--------------



CREATE EXTERNAL TABLE `raw_marketordernew_seq`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1` bigint COMMENT 'from deserializer',
  `new_cash2` bigint COMMENT 'from deserializer',
  `new_cash3` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer',
   `rc_cost` double COMMENT 'from deserializer', 
  `rc_bal` double COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
  `app` string, 
  `year` int, 
  `month` int, 
  `day` int, 
  `hour` int)
   ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
) 
 STORED AS SEQUENCEFILE
 LOCATION
   's3://com.funplus.datawarehouse/farm_1_1/events_seq/MarketOrderNew'
 TBLPROPERTIES (
   'serialization.null.format'='');
   
   

DROP TABLE IF EXISTS raw_handprint_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_handprint_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    action   STRING,
    handprint_in   BIGINT,
    handprint_qty   BIGINT,
    handprint_level   BIGINT,
    reward_type STRING,
    reward_item STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/Handprint'
TBLPROPERTIES('serialization.null.format'='');


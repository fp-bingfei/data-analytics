USE farm_1_1;

-- 4th_scene_login
DROP TABLE IF EXISTS raw_4th_scene_login_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_4th_scene_login_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    action   STRING,
    first_login   BIGINT
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/4th_scene_login'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_4th_scene_login_daily;

CREATE EXTERNAL TABLE raw_4th_scene_login_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    action string, 
    first_login bigint
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_4th_scene_login_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- 4th_scene_currency
DROP TABLE IF EXISTS raw_4th_scene_currency_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_4th_scene_currency_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    action   STRING,
    action_type   STRING,
    chef_points_in   BIGINT,
    chef_points_out   BIGINT,
    chef_points_bal   BIGINT,
    silver_coins_in   BIGINT,
    silver_coins_out   BIGINT,
    silver_coins_bal   BIGINT,
    reputation_in   BIGINT,
    reputation_out   BIGINT,
    reputation_bal   BIGINT
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/4th_scene_currency'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_4th_scene_currency_daily;

CREATE EXTERNAL TABLE raw_4th_scene_currency_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    action string, 
    action_type string,
    chef_points_in bigint,
    chef_points_out bigint,
    chef_points_bal bigint,
    silver_coins_in bigint,
    silver_coins_out bigint,
    silver_coins_bal bigint,
    reputation_in bigint,
    reputation_out bigint,
    reputation_bal bigint
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_4th_scene_currency_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- 4th_scene_action
DROP TABLE IF EXISTS raw_4th_scene_action_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_4th_scene_action_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    action   STRING,
    put_on_4th_scene   STRING,
    cost_type   STRING,
    cost_num   BIGINT,
    decoration_id   STRING,
    reputation   BIGINT,
    building   STRING,
    item_id   STRING,
    item_num   BIGINT,
    finish_with_rc   STRING,
    production_time   BIGINT,
    from_num   BIGINT,
    to_num   BIGINT,
    material   STRING,
    upgrade_material   STRING,
    sell_time   BIGINT,
    dish_id   STRING,
    recipe   STRING,
    sell_silver_coins   BIGINT,
    sell_chef_points   BIGINT,
    product_id   STRING,
    material_id   STRING,
    needs   BIGINT,
    request_to_friend   BIGINT,
    request_to_neighbor   BIGINT,
    parent_id   STRING,
    use_num   BIGINT,
    direction   STRING,
    detail   STRING,
    limit_from   BIGINT,
    limit_to   BIGINT,
    type   STRING,
    currency   STRING,
    currency_num   BIGINT
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key",
    "mapping.limit_from"= "from",
    "mapping.limit_to"= "to"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/4th_scene_action'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_4th_scene_action_daily;

CREATE EXTERNAL TABLE raw_4th_scene_action_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    action string, 
    put_on_4th_scene string,
    cost_type string,
    cost_num bigint,
    decoration_id string,
    reputation bigint,
    building string,
    item_id string,
    item_num bigint,
    finish_with_rc string,
    production_time bigint,
    from_num bigint,
    to_num bigint,
    material string,
    upgrade_material string,
    sell_time bigint,
    dish_id string,
    recipe string,
    sell_silver_coins bigint,
    sell_chef_points bigint,
    product_id string,
    material_id string,
    needs bigint,
    request_to_friend bigint,
    request_to_neighbor bigint,
    parent_id string,
    use_num bigint,
    direction string,
    detail string,
    limit_from bigint,
    limit_to bigint,
    type string,
    currency string,
    currency_num bigint
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_4th_scene_action_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- 4th_scene_level_up
DROP TABLE IF EXISTS raw_4th_scene_level_up_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_4th_scene_level_up_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    previous_level   STRING,
    current_level   BIGINT,
    reward_info   STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/4th_scene_level_up'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_4th_scene_level_up_daily;

CREATE EXTERNAL TABLE raw_4th_scene_level_up_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    previous_level string,
    current_level bigint,
    reward_info string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_4th_scene_level_up_daily'
TBLPROPERTIES ('serialization.null.format'='');


DROP TABLE IF EXISTS agg_4th_scene_login;

CREATE EXTERNAL TABLE IF NOT EXISTS agg_4th_scene_login
(
  date  STRING,
  app_id STRING,
  browser STRING,
  brower_version STRING,
  country STRING,
  install_date  STRING,  
  language  STRING,
  level BIGINT,
  os STRING,
  os_version  STRING,
  first_login_cnt BIGINT,
  login_user_cnt BIGINT,
  login_cnt BIGINT
)
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_4th_scene_login'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS agg_4th_scene_currency_transaction;

CREATE EXTERNAL TABLE IF NOT EXISTS agg_4th_scene_currency_transaction
(
  date  STRING,
  app_id STRING,
  browser STRING,
  brower_version STRING,
  country STRING,
  language  STRING,
  level BIGINT,
  os STRING,
  os_version  STRING,
  action STRING,
  action_type STRING,
  chef_points_in BIGINT,
  chef_points_out BIGINT,
  silver_coins_in BIGINT,
  silver_coins_out BIGINT,
  reputation_in BIGINT,
  reputation_out BIGINT
)
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_4th_scene_currency_transaction'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS copy_agg_4th_scene_login;

CREATE TABLE IF NOT EXISTS copy_agg_4th_scene_login
(
  date  STRING,
  app_id STRING,
  browser STRING,
  brower_version STRING,
  country STRING,
  install_date  STRING,  
  language  STRING,
  level BIGINT,
  os STRING,
  os_version  STRING,
  first_login_cnt BIGINT,
  login_user_cnt BIGINT,
  login_cnt BIGINT
)
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_4th_scene_login'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS copy_agg_4th_scene_currency_transaction;

CREATE TABLE IF NOT EXISTS copy_agg_4th_scene_currency_transaction
(
  date  STRING,
  app_id STRING,
  browser STRING,
  brower_version STRING,
  country STRING,
  language  STRING,
  level BIGINT,
  os STRING,
  os_version  STRING,
  action STRING,
  action_type STRING,
  chef_points_in BIGINT,
  chef_points_out BIGINT,
  silver_coins_in BIGINT,
  silver_coins_out BIGINT,
  reputation_in BIGINT,
  reputation_out BIGINT
)
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_4th_scene_currency_transaction'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS copy_4th_scene_level_up_daily;

CREATE TABLE IF NOT EXISTS copy_4th_scene_level_up_daily (
    key string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    install_source string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    previous_level string,
    current_level bigint,
    reward_info string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/4th_scene_level_up_daily'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE copy_4th_scene_login_daily (
    app_id string, 
    date string, 
    browser string, 
    browser_version string, 
    country_code string, 
    install_source string, 
    install_date string, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string,  
    first_login bigint
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/4th_scene_login_daily'
TBLPROPERTIES ('serialization.null.format'='');

-- Redshift
CREATE TABLE processed.raw_4th_scene_level_up_daily (
    app_id VARCHAR(64) NOT NULL ENCODE bytedict,
    ts_pretty timestamp ENCODE delta, 
    browser VARCHAR(32) ENCODE bytedict,
    browser_version VARCHAR(64) ENCODE bytedict,
    country_code VARCHAR(16) ENCODE bytedict,
    install_source VARCHAR(1024) ENCODE bytedict, 
    install_ts_pretty timestamp ENCODE delta, 
    ip VARCHAR(64) ENCODE bytedict,
    lang VARCHAR(8) ENCODE bytedict,
    level bigint, 
    os VARCHAR(32) ENCODE bytedict,
    os_version VARCHAR(32) ENCODE bytedict,
    snsid VARCHAR(50) ENCODE bytedict,
    uid VARCHAR(64) NOT NULL ENCODE bytedict,
    fb_source VARCHAR(300) ENCODE bytedict,
    previous_level VARCHAR(30) ENCODE bytedict,
    current_level bigint,
    reward_info VARCHAR(1024) NOT NULL ENCODE bytedict
);

CREATE TABLE processed.raw_4th_scene_login_daily
(
   app_id           varchar(64)      NOT NULL,
   date             date,
   browser          varchar(32),
   browser_version  varchar(64),
   country_code     varchar(16),
   install_source   varchar(1024),
   install_date     date,
   ip               varchar(64),
   language         varchar(8),
   level            bigint,
   os               varchar(32),
   os_version       varchar(32),
   snsid            varchar(50),
   uid              varchar(64),
   fb_source        varchar(1024),
   first_login      bigint
);

CREATE TABLE processed.raw_4th_scene_login_daily
(
   app_id           varchar(64)      NOT NULL,
   date             date,
   browser          varchar(32),
   browser_version  varchar(64),
   country_code     varchar(16),
   install_source   varchar(1024),
   install_date     date,
   ip               varchar(64),
   language         varchar(8),
   level            bigint,
   os               varchar(32),
   os_version       varchar(32),
   snsid            varchar(50),
   uid              varchar(64),
   fb_source        varchar(1024),
   first_login      bigint
);


CREATE TABLE processed.dim_4th_scene_level
(
   app_id           varchar(64)      NOT NULL,
   user_key         varchar(64)      NOT NULL,
   browser          varchar(32),
   browser_version  varchar(64),
   country     varchar(200),
   install_source   varchar(1024),
   install_date     date,
   ip               varchar(64),
   language         varchar(8),
   scene_level            bigint,
   os               varchar(32),
   os_version       varchar(32),
   snsid            varchar(50),
   uid              varchar(64),
   fb_source        varchar(1024),
   latest_login_date date
);
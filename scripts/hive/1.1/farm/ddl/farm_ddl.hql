CREATE EXTERNAL TABLE `fact_session`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date_start` string,
  `date_end` string,
  `ts_start` timestamp,
  `ts_end` timestamp,
  `install_ts` timestamp,
  `install_date` string,
  `session_id` string,
  `facebook_id` string,
  `install_source` string,
  `os` string,
  `os_version` string,
  `browser` string,
  `browser_version` string,
  `device` string,
  `country_code` string,
  `level_start` int,
  `level_end` int,
  `gender` string,
  `birthday` string,
  `email` string,
  `ip` string,
  `language` string,
  `locale` string,
  `rc_wallet_start` int,
  `rc_wallet_end` int,
  `coin_wallet_start` bigint,
  `coin_wallet_end` bigint,
  `ab_experiment` string,
  `ab_variant` string,
  `session_length_sec` int,
  `idfa` string,
  `idfv` string,
  `gaid` string,
  `mac_address` string,
  `android_id` string,
  `fb_source` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/processed/fact_session'
TBLPROPERTIES (
  'serialization.null.format'='');



CREATE EXTERNAL TABLE `fact_revenue`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date` string,
  `ts` timestamp,
  `install_ts` timestamp,
  `install_date` string,
  `session_id` string,
  `level` int,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `install_source` string,
  `ip` string,
  `language` string,
  `locale` string,
  `ab_experiment` string,
  `ab_variant` string,
  `coin_wallet` bigint,
  `rc_wallet` int,
  `payment_processor` string,
  `product_id` string,
  `product_name` string,
  `product_type` string,
  `coins_in` bigint,
  `rc_in` int,
  `currency` string,
  `revenue_amount` double,
  `revenue_usd` double,
  `transaction_id` string,
  `idfa` string,
  `idfv` string,
  `gaid` string,
  `mac_address` string,
  `android_id` string,
  `fb_source` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/processed/fact_revenue'
TBLPROPERTIES (
  'serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS fact_levelup(
id string COMMENT 'id is md5(concat(app_name, app_user_id,current_level,levelup_ts)', 
user_key string, 
app_id string COMMENT 'app_id is the same as app which is being used as partition key', 
app_user_id string, 
session_id string, 
previous_level smallint, 
previous_levelup_date string, 
previous_levelup_ts timestamp, 
current_level smallint, 
levelup_date string, 
levelup_ts timestamp, 
browser string, 
browser_version string, 
os string, 
os_version string, 
device string, 
coin_wallet bigint, 
coins_in bigint, 
country_code string, 
ip string, 
language string, 
rc_wallet bigint, 
rc_in bigint, 
locale string, 
ab_experiment string, 
ab_variant string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/processed/fact_levelup'
TBLPROPERTIES (
'serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS fact_sendgrid(
app_id string, 
campaign_name string, 
campaign_date string, 
delivered_per_campaign_date int, 
category string, 
app_user_id string, 
snsid string, 
email string, 
language string, 
os string, 
browser string, 
country string, 
delivered_ts string, 
bounce_ts string, 
open_ts string, 
open_daydiff int, 
click_ts string, 
click_daydiff int, 
precamp_session_cnt bigint, 
precamp_purchase_cnt int, 
precamp_revenue_usd double, 
precamp_level_end int, 
level_end int, 
total_session_cnt bigint, 
total_purchase_cnt int, 
total_revenue_usd double)
PARTITIONED BY ( 
app string, 
campaign_dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/processed/fact_sendgrid'
TBLPROPERTIES ('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS fact_tradeorders(
date string, 
install_date string, 
level bigint, 
action string, 
experience string, 
reward_coins bigint, 
special_num bigint, 
special_item bigint, 
exchange_num_1 bigint, 
exchange_item_1 bigint, 
exchange_num_2 bigint, 
exchange_item_2 bigint, 
exchange_num_3 bigint, 
exchange_item_3 bigint, 
times bigint)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/processed/fact_tradeorders'
TBLPROPERTIES ('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS fact_ledger(
id string, 
date string, 
ts timestamp, 
app_id string, 
app_user_id string, 
user_key string, 
session_id string, 
level int, 
action_type string, 
action string, 
received_id string, 
received_name string, 
received_type string, 
received_amount int, 
spent_id string, 
spent_name string, 
spent_type string, 
spent_amount int)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/processed/fact_ledger_test'
TBLPROPERTIES ('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS agg_daily_ledger(
  id string, 
  date string, 
  app_id string, 
  level int, 
  os string, 
  device string, 
  browser string, 
  country string, 
  language string, 
  action_type string, 
  action string, 
  received_id string, 
  received_name string, 
  received_type string, 
  received_amount bigint, 
  spent_id string, 
  spent_name string, 
  spent_type string, 
  spent_amount bigint)
PARTITIONED BY ( 
  app string, 
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/processed/agg_daily_ledger_test'
TBLPROPERTIES ('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS user_geo(
  user_id string, 
  uid string, 
  snsid string, 
  email string, 
  user_name string, 
  addtime timestamp, 
  logintime timestamp, 
  loginip string, 
  status int)
PARTITIONED BY ( 
  app string, 
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/others/user_geo'
TBLPROPERTIES ('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS dim_action(
action_type string, 
action string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/others/dim_action'
TBLPROPERTIES (
'serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS dim_country(
country_code string, 
country string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/common/dim_country'
TBLPROPERTIES (
'serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS dim_install_source(
id string, 
user_key string, 
app_name string, 
uid int, 
install_date string, 
install_source_event_raw string, 
install_source_event string, 
install_source_3rdparty_raw string, 
install_source_3rdparty string, 
install_source_raw string, 
install_source string, 
campaign string, 
sub_publisher string)
PARTITIONED BY ( 
app string, 
year int, 
month int, 
day int)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/dim_install_source'
TBLPROPERTIES (
'serialization.null.format'='');


CREATE  TABLE `tmp_install_source`(
  `app_id` string,
  `user_key` string,
  `app_user_id` string,
  `install_source` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES (
  'serialization.null.format'='');




CREATE EXTERNAL TABLE `dim_user`(
  `id` string,
  `user_key` string,
  `app_id` string,
  `app_user_id` string,
  `facebook_id` string,
  `install_ts` timestamp,
  `install_date` string,
  `install_source` string,
  `install_subpublisher` string,
  `install_campaign` string,
  `install_language` string,
  `install_locale` string,
  `install_country_code` string,
  `install_country` string,
  `install_os` string,
  `install_device` string,
  `install_device_alias` string,
  `install_browser` string,
  `install_gender` string,
  `install_age` string,
  `language` string,
  `locale` string,
  `birthday` string,
  `gender` string,
  `country_code` string,
  `country` string,
  `os` string,
  `os_version` string,
  `device` string,
  `device_alias` string,
  `browser` string,
  `browser_version` string,
  `app_version` string,
  `level` int,
  `levelup_ts` timestamp,
  `ab_experiment` string,
  `ab_variant` string,
  `is_payer` int,
  `conversion_ts` timestamp,
  `total_revenue_usd` decimal(12,4),
  `payment_cnt` int,
  `last_login_ts` timestamp,
  `coin_wallet` double,
  `coins_in` bigint,
  `coins_out` bigint,
  `rc_wallet` double,
  `rc_in` bigint,
  `rc_out` bigint,
  `email` string,
  `last_ip` string,
  `user_name` string,
  `last_payment_date` string,
  `install_source_display` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/processed/dim_user'
TBLPROPERTIES (
  'serialization.null.format'='');



CREATE EXTERNAL TABLE `fact_dau_snapshot`(
  `id` string,
  `user_key` string,
  `date` string,
  `app_id` string,
  `app_version` string,
  `level_start` int,
  `level_end` int,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `country` string,
  `language` string,
  `ab_experiment` string,
  `ab_variant` string,
  `is_new_user` smallint,
  `rc_in` int,
  `coins_in` bigint,
  `rc_out` int,
  `coins_out` bigint,
  `rc_wallet` int,
  `coin_wallet` bigint,
  `is_payer` smallint,
  `is_converted_today` smallint,
  `revenue_usd` double,
  `payment_cnt` int,
  `session_cnt` int,
  `playtime_sec` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/processed/fact_dau_snapshot'
TBLPROPERTIES (
  'serialization.null.format'='');




CREATE EXTERNAL TABLE IF NOT EXISTS agg_kpi(
id string, 
date string, 
app_id string, 
app_version string, 
install_year string, 
install_month string, 
install_week string, 
install_source string, 
level_end int, 
browser string, 
browser_version string, 
device_alias string, 
country string, 
os string, 
os_version string, 
language string, 
is_new_user int, 
is_payer int, 
new_user_cnt int, 
dau_cnt int, 
newpayer_cnt int, 
payer_today_cnt int, 
payment_cnt int, 
revenue_usd double, 
session_cnt int, 
session_length_sec int)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/processed/agg_kpi'
TBLPROPERTIES ('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS agg_tutorial(
  install_date string, 
  app_id string, 
  os string, 
  os_version string, 
  country string, 
  install_source string, 
  browser string, 
  browser_version string, 
  language string, 
  step int, 
  user_cnt bigint)
PARTITIONED BY ( 
  app string, 
  install_dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/processed/agg_tutorial'
TBLPROPERTIES ('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS agg_quest(
install_date string, 
app_id string, 
os string, 
os_version string, 
country string, 
install_source string, 
browser string, 
browser_version string, 
language string, 
quest_id string, 
quest_type string, 
task_id string, 
action string, 
user_cnt bigint, 
rc_out bigint)
PARTITIONED BY ( 
app string, 
install_dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/processed/agg_quest'
TBLPROPERTIES ( 'serialization.null.format'='');

CREATE  TABLE `tmp_user_daily_login`(
  `date_start` string,
  `user_key` string,
  `device_key` string,
  `app_id` string,
  `uid` string,
  `install_ts` timestamp,
  `install_date` string,
  `birthday` string,
  `app_version` string,
  `level_start` int,
  `level_end` int,
  `os` string,
  `os_version` string,
  `country_code` string,
  `country` string,
  `last_ip` string,
  `install_source` string,
  `language` string,
  `locale` string,
  `gender` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `ab_experiment` string,
  `ab_variant` string,
  `session_cnt` int,
  `last_login_ts` timestamp,
  `fb_source` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_daily_login'
TBLPROPERTIES (
  'serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_user_level(
date string,
user_key string,
app_id string,
levelup_ts timestamp,
level_start int,
level_end int)
PARTITIONED BY (
app string,
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_level'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_user_payment(
  user_key string, 
  app_id string, 
  date string, 
  conversion_ts timestamp, 
  revenue_usd double, 
  purchase_cnt int, 
  last_payment_date string)
PARTITIONED BY ( 
  app string, 
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_payment'
TBLPROPERTIES ('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_agg_iap(
date string, 
app_id string, 
app_version string, 
level int, 
install_date string, 
install_source string, 
country string, 
os string, 
ab_experiment string, 
ab_variant string, 
conversion_purchase int, 
product_id string, 
product_type string, 
revenue_usd double, 
purchase_cnt int, 
purchase_user_cnt int)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/agg_iap'
TBLPROPERTIES (
'serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_user_snsid(
app_id string, 
app_user_id string, 
facebook_id string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_snsid'
TBLPROPERTIES (
'serialization.null.format'='');


CREATE  TABLE `copy_dim_user`(
  `id` string,
  `user_key` string,
  `app_id` string,
  `app_user_id` string,
  `facebook_id` string,
  `install_ts` timestamp,
  `install_date` string,
  `install_source` string,
  `install_subpublisher` string,
  `install_campaign` string,
  `install_language` string,
  `install_locale` string,
  `install_country_code` string,
  `install_country` string,
  `install_os` string,
  `install_device` string,
  `install_device_alias` string,
  `install_browser` string,
  `install_gender` string,
  `install_age` string,
  `language` string,
  `locale` string,
  `birthday` string,
  `gender` string,
  `country_code` string,
  `country` string,
  `os` string,
  `os_version` string,
  `device` string,
  `device_alias` string,
  `browser` string,
  `browser_version` string,
  `app_version` string,
  `level` int,
  `levelup_ts` timestamp,
  `ab_experiment` string,
  `ab_variant` string,
  `is_payer` int,
  `conversion_ts` timestamp,
  `total_revenue_usd` decimal(12,4),
  `payment_cnt` int,
  `last_login_ts` timestamp,
  `coin_wallet` double,
  `coins_in` bigint,
  `coins_out` bigint,
  `rc_wallet` double,
  `rc_in` bigint,
  `rc_out` bigint,
  `email` string,
  `last_ip` string,
  `user_name` string,
  `last_payment_date` string,
  `install_source_display` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/copy/dim_user'
TBLPROPERTIES (
  'serialization.null.format'='');



CREATE  TABLE `copy_fact_session`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date_start` string,
  `date_end` string,
  `ts_start` timestamp,
  `ts_end` timestamp,
  `install_ts` timestamp,
  `install_date` string,
  `session_id` string,
  `facebook_id` string,
  `install_source` string,
  `os` string,
  `os_version` string,
  `browser` string,
  `browser_version` string,
  `device` string,
  `country_code` string,
  `level_start` int,
  `level_end` int,
  `gender` string,
  `birthday` string,
  `email` string,
  `ip` string,
  `language` string,
  `locale` string,
  `rc_wallet_start` int,
  `rc_wallet_end` int,
  `coin_wallet_start` bigint,
  `coin_wallet_end` bigint,
  `ab_experiment` string,
  `ab_variant` string,
  `session_length_sec` int,
  `idfa` string,
  `idfv` string,
  `gaid` string,
  `mac_address` string,
  `android_id` string,
  `fb_source` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/copy/fact_session'
TBLPROPERTIES (
  'serialization.null.format'='');



CREATE  TABLE `copy_fact_revenue`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date` string,
  `ts` timestamp,
  `install_ts` timestamp,
  `install_date` string,
  `session_id` string,
  `level` int,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `install_source` string,
  `ip` string,
  `language` string,
  `locale` string,
  `ab_experiment` string,
  `ab_variant` string,
  `coin_wallet` bigint,
  `rc_wallet` int,
  `payment_processor` string,
  `product_id` string,
  `product_name` string,
  `product_type` string,
  `coins_in` bigint,
  `rc_in` int,
  `currency` string,
  `revenue_amount` double,
  `revenue_usd` double,
  `transaction_id` string,
  `idfa` string,
  `idfv` string,
  `gaid` string,
  `mac_address` string,
  `android_id` string,
  `fb_source` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/copy/fact_revenue'
TBLPROPERTIES (
  'serialization.null.format'='',
  'transient_lastDdlTime'='1431539706');
  
  
  CREATE EXTERNAL TABLE `copy_fact_tradeorders`(
    `date` string, 
    `install_date` string, 
    `level` bigint, 
    `action` string, 
    `experience` string, 
    `reward_coins` bigint, 
    `special_num` bigint, 
    `special_item` bigint, 
    `exchange_num_1` bigint, 
    `exchange_item_1` bigint, 
    `exchange_num_2` bigint, 
    `exchange_item_2` bigint, 
    `exchange_num_3` bigint, 
    `exchange_item_3` bigint, 
    `times` bigint, 
    `app_id` string)
  PARTITIONED BY ( 
    `app` string, 
    `dt` string)
  ROW FORMAT DELIMITED 
    FIELDS TERMINATED BY '\t' 
  STORED AS TEXTFILE
  LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/copy/fact_tradeorders'
  TBLPROPERTIES (
    'serialization.null.format'='');

--------------------------------------------------
--- Notif events
--------------------------------------------------
CREATE EXTERNAL TABLE IF NOT EXISTS agg_notif (
    date string, 
    app_id string, 
    notif_name string, 
    total_send bigint, 
    total_click bigint, 
    ctr double)
PARTITIONED BY ( 
    app string, 
    dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/processed/agg_notif'
TBLPROPERTIES ('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS `copy_agg_notif` (
    `date` string, 
    `app_id` string, 
    `notif_name` string, 
    `total_send` bigint, 
    `total_click` bigint,  
    `ctr` double)
PARTITIONED BY ( 
    `app` string, 
    `dt` string)
ROW FORMAT DELIMITED 
    FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/copy/agg_notif'
TBLPROPERTIES (
    'serialization.null.format'='');

-- Run in Redshift
DROP TABLE IF EXISTS processed.agg_notif CASCADE;
CREATE TABLE processed.agg_notif
(
   date             date,
   app_id           varchar(64),
   notif_name       varchar(255),
   total_send       bigint,
   total_click      bigint,
   ctr              numeric(14,4)
);

COMMIT;

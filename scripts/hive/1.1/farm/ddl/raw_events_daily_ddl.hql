USE farm_1_1;

DROP TABLE IF EXISTS raw_session_start_daily;


CREATE EXTERNAL TABLE raw_session_start_daily(
key string, 
ts string, 
ts_pretty timestamp, 
browser string, 
browser_version string, 
country_code string, 
event string, 
gender string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
lang string, 
level bigint, 
os string, 
os_version string, 
snsid string, 
uid string, 
device string,
fb_source string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_session_start_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_payment_daily;


CREATE EXTERNAL TABLE raw_payment_daily(
key string, 
ts string, 
ts_pretty timestamp, 
amount double, 
browser string, 
browser_version string, 
coins_bal double, 
coins_in bigint, 
country_code string, 
currency string, 
event string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
is_gift bigint, 
lang string, 
level bigint, 
os string, 
os_version string, 
payment_processor string, 
product_id string, 
product_name string, 
product_type string, 
raw struct<amount:double,cash_type:string,currency:string,from_uid:string,gameamount:string,gift:string,gift_id:string,level:bigint,paid_time:string,status:bigint,type:string,uid:string>, 
rc_bal bigint, 
rc_in bigint, 
snsid string, 
transaction_id string, 
uid string,
fb_source string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_payment_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_newuser_daily;


CREATE EXTERNAL TABLE raw_newuser_daily(
key string, 
ts string, 
ts_pretty timestamp, 
browser string, 
browser_version string, 
country_code string, 
event string, 
gender string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
lang string, 
level bigint, 
os string, 
os_version string, 
snsid string, 
uid string, 
device string,
fb_source string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_newuser_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_levelup_daily;


CREATE EXTERNAL TABLE raw_levelup_daily(
key string, 
ts string, 
ts_pretty timestamp, 
browser string, 
browser_version string, 
coins_bal string, 
coins_get bigint, 
country_code string, 
event string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
lang string, 
level bigint, 
os string, 
os_version string, 
rc_bal bigint, 
rc_get string, 
snsid string, 
uid string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_levelup_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_tutorial_daily;


CREATE EXTERNAL TABLE raw_tutorial_daily(                                                                                                                                                                         key string,                                                                                                                                                                   ts string,                                                                                                                                                                    ts_pretty timestamp,                                                                                                                                                          browser string,                                                                                                                            
browser_version string, 
country_code string, 
event string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
lang string, 
level bigint, 
os string, 
os_version string, 
snsid string, 
step bigint, 
uid string)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_tutorial_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_quest_daily;


CREATE EXTERNAL TABLE raw_quest_daily(
key string, 
ts string, 
ts_pretty timestamp, 
action string, 
browser string, 
browser_version string, 
country_code string, 
event string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
lang string, 
level bigint, 
os string, 
os_version string, 
quest_id string, 
quest_type string, 
snsid string, 
task_id string, 
uid string, 
rc_in string, 
rc_out string, 
rc_bal string, 
coins_in double, 
coins_out double, 
coins_bal double)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_quest_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_tradeorders_daily;


CREATE EXTERNAL TABLE raw_tradeorders_daily(
key string, 
ts string, 
ts_pretty timestamp, 
browser string, 
browser_version string, 
country_code string, 
event string, 
gender string, 
install_source string, 
install_ts string, 
install_ts_pretty timestamp, 
ip string, 
lang string, 
level bigint, 
os string, 
os_version string, 
snsid string, 
uid string, 
device string, 
action string, 
experience string, 
reward_coins bigint, 
special_num bigint, 
special_item bigint, 
exchange_num_1 bigint, 
exchange_item_1 bigint, 
exchange_num_2 bigint, 
exchange_item_2 bigint, 
exchange_num_3 bigint, 
exchange_item_3 bigint)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_tradeorders_daily'
TBLPROPERTIES (
'serialization.null.format'='');

DROP TABLE IF EXISTS raw_marketcratefinish_daily;


CREATE EXTERNAL TABLE `raw_marketcratefinish_daily`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points_get` bigint COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer', 
  `assist` boolean COMMENT 'from deserializer',
  `new_cash_3_get` string COMMENT 'from deserializer',
  `adv_point_get` string COMMENT 'from deserializer')
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_marketcratefinish_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_marketcrateneedhelp_daily;


CREATE EXTERNAL TABLE `raw_marketcrateneedhelp_daily`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer',
  `is_guild` string COMMENT 'from deserializer')
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_marketcrateneedhelp_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_marketexchange_daily;


CREATE EXTERNAL TABLE `raw_marketexchange_daily`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer',
  `new_cash1_cost` bigint COMMENT 'from deserializer',
  `new_cash2_cost` bigint COMMENT 'from deserializer',
  `new_cash3_cost` bigint COMMENT 'from deserializer',
  `new_cash1_left` bigint COMMENT 'from deserializer', 
  `new_cash2_left` bigint COMMENT 'from deserializer',  
  `new_cash3_left` bigint COMMENT 'from deserializer'     
  )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_marketexchange_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);
   
DROP TABLE IF EXISTS raw_marketorderfinish_daily;
   
   
 
CREATE EXTERNAL TABLE `raw_marketorderfinish_daily`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points_get` bigint COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer',
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1_get` bigint COMMENT 'from deserializer',
  `new_cash2_get` bigint COMMENT 'from deserializer',
  `new_cash3_get` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_marketorderfinish_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS raw_marketorderbegin_daily;


  CREATE EXTERNAL TABLE `raw_marketorderbegin_daily`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1` bigint COMMENT 'from deserializer',
  `new_cash2` bigint COMMENT 'from deserializer',
  `new_cash3` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer',
  `rc_cost` double COMMENT 'from deserializer', 
  `rc_bal` double COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_marketorderbegin_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);


DROP TABLE IF EXISTS raw_marketordernew_daily;


CREATE EXTERNAL TABLE `raw_marketordernew_daily`(
  `key` string COMMENT 'from deserializer', 
  `ts` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer', 
  `browser` string COMMENT 'from deserializer', 
  `browser_version` string COMMENT 'from deserializer', 
  `country_code` string COMMENT 'from deserializer', 
  `event` string COMMENT 'from deserializer', 
  `install_source` string COMMENT 'from deserializer', 
  `install_ts` string COMMENT 'from deserializer', 
  `install_ts_pretty` timestamp COMMENT 'from deserializer', 
  `ip` string COMMENT 'from deserializer', 
  `lang` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `os` string COMMENT 'from deserializer', 
  `os_version` string COMMENT 'from deserializer', 
  `uid` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `fb_source` string COMMENT 'from deserializer', 
  `order_points` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1` bigint COMMENT 'from deserializer',
  `new_cash2` bigint COMMENT 'from deserializer',
  `new_cash3` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer',
  `rc_cost` double COMMENT 'from deserializer', 
  `rc_bal` double COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_marketordernew_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);  


DROP TABLE IF EXISTS raw_handprint_daily;

CREATE EXTERNAL TABLE raw_handprint_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    action string, 
    handprint_in bigint, 
    handprint_qty bigint, 
    handprint_level bigint, 
    reward_type string, 
    reward_item string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_handprint_daily'
TBLPROPERTIES ('serialization.null.format'='');

CREATE EXTERNAL TABLE `raw_personal_info_daily`(
  `key` string,
  `ts` string,
  `ts_pretty` timestamp,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `event` string,
  `install_source` string,
  `install_ts` string,
  `install_ts_pretty` timestamp,
  `ip` string,
  `lang` string,
  `level` bigint,
  `os` string,
  `os_version` string,
  `snsid` string,
  `uid` string,
  `fb_source` string,
  `additional_email` string)
  PARTITIONED BY (
    `app` string,
    `dt` string)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_personal_info_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);




---------------
-- Copy
---------------

DROP TABLE IF EXISTS copy_raw_marketcratefinish_daily;


CREATE TABLE `copy_raw_marketcratefinish_daily`(
  `uid` string COMMENT 'from deserializer', 
  `key` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer',
  `country_code` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `order_points_get` bigint COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer', 
  `assist` boolean COMMENT 'from deserializer',
  `new_cash_3_get` string COMMENT 'from deserializer',
  `adv_point_get` string COMMENT 'from deserializer')
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketcratefinish_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS copy_raw_marketcrateneedhelp_daily;

CREATE TABLE `copy_raw_marketcrateneedhelp_daily`(
  `uid` string COMMENT 'from deserializer', 
  `key` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer',
  `country_code` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer')
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketcrateneedhelp_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);


DROP TABLE IF EXISTS copy_raw_marketexchange_daily;


CREATE TABLE `copy_raw_marketexchange_daily`(
  `uid` string COMMENT 'from deserializer', 
  `key` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer',
  `country_code` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `item_id` string COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer', 
  `new_cash1_cost` bigint COMMENT 'from deserializer',
  `new_cash2_cost` bigint COMMENT 'from deserializer',
  `new_cash3_cost` bigint COMMENT 'from deserializer',
  `new_cash1_left` bigint COMMENT 'from deserializer', 
  `new_cash2_left` bigint COMMENT 'from deserializer',  
  `new_cash3_left` bigint COMMENT 'from deserializer'     
  )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketexchange_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);
   
 
DROP TABLE IF EXISTS copy_raw_marketorderfinish_daily;
   
 
CREATE TABLE `copy_raw_marketorderfinish_daily`(
  `uid` string COMMENT 'from deserializer', 
  `key` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer',
  `country_code` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `order_points_get` bigint COMMENT 'from deserializer', 
  `item_quantity` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1_get` bigint COMMENT 'from deserializer',
  `new_cash2_get` bigint COMMENT 'from deserializer',
  `new_cash3_get` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketorderfinish_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);

DROP TABLE IF EXISTS copy_raw_marketorderbegin_daily;


  CREATE TABLE `copy_raw_marketorderbegin_daily`(
  `uid` string COMMENT 'from deserializer', 
  `key` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer',
  `country_code` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `order_points` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1` bigint COMMENT 'from deserializer',
  `new_cash2` bigint COMMENT 'from deserializer',
  `new_cash3` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer',
  `rc_cost` double COMMENT 'from deserializer', 
  `rc_bal` double COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketorderbegin_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);


DROP TABLE IF EXISTS copy_raw_marketordernew_daily;



CREATE TABLE `copy_raw_marketordernew_daily`(
  `uid` string COMMENT 'from deserializer', 
  `key` string COMMENT 'from deserializer', 
  `ts_pretty` timestamp COMMENT 'from deserializer',
  `country_code` string COMMENT 'from deserializer', 
  `snsid` string COMMENT 'from deserializer', 
  `level` bigint COMMENT 'from deserializer', 
  `order_points` bigint COMMENT 'from deserializer', 
  `time_left` bigint COMMENT 'from deserializer',
  `new_cash1` bigint COMMENT 'from deserializer',
  `new_cash2` bigint COMMENT 'from deserializer',
  `new_cash3` bigint COMMENT 'from deserializer',
  `crates` MAP<string, bigint> COMMENT 'from deserializer',
  `rc_cost` double COMMENT 'from deserializer', 
  `rc_bal` double COMMENT 'from deserializer'
    )
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketordernew_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);  


CREATE TABLE IF NOT EXISTS copy_personal_info_daily (
    `id` string,
    `app_id` string,
    `ts` timestamp,
    `date` string,
    `browser` string,
    `browser_version` string,
    `country_code` string,
    `event` string,
    `install_source` string,
    `install_ts` timestamp,
    `install_date` string,
    `ip` string,
    `lang` string,
    `level` int,
    `os` string,
    `os_version` string,
    `snsid` string,
    `uid` int,
    `fb_source` string,
    `additional_email` string
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/personal_info_daily'
TBLPROPERTIES('serialization.null.format'='');

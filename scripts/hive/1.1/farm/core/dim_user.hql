-----------------------------
-- Query to generate dim_user
-----------------------------

use farm_1_1;


SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_all_app=${RPT_ALL_APP}; 





INSERT OVERWRITE TABLE dim_user PARTITION(app,dt)
select distinct 
tu.user_key AS id,
tu.user_key,
tu.app as app_id,
tu.uid as app_user_id,
COALESCE(du.facebook_id, tus.facebook_id) AS facebook_id,
COALESCE(du.install_ts,tu.install_ts) as install_ts,
COALESCE(du.install_date, tu.install_date) as install_date,
split(tu.install_source, '::')[0] AS install_source,
split(tu.install_source, '::')[1] AS install_subpublisher,
split(tu.install_source, '::')[2] AS install_campaign,
case when tu.date_start = tu.install_date then tu.language else du.install_language end AS install_language,
case when tu.date_start = tu.install_date then tu.locale else du.install_locale end AS install_locale,
case when tu.date_start = tu.install_date then tu.country_code else du.install_country_code end AS install_country_code,
case when tu.date_start = tu.install_date then tu.country else du.install_country end AS install_country,
case when tu.date_start = tu.install_date then tu.os else du.install_os end AS install_os,
case when tu.date_start = tu.install_date then tu.device else du.install_device end AS install_device,
case when tu.date_start = tu.install_date then NULL else du.install_device_alias end AS install_device_alias,
case when tu.date_start = tu.install_date then tu.browser else du.install_browser end AS install_browser,
case when tu.date_start = tu.install_date then tu.gender else du.install_gender end AS install_gender,
case when tu.date_start = tu.install_date then datediff(tu.install_date, tu.birthday) else du.install_age end AS install_age,
tu.language,
tu.locale,
tu.birthday,
tu.gender,
tu.country_code,
tu.country,
tu.os,
tu.os_version,
tu.device,
NULL AS device_alias,
tu.browser,
tu.browser_version,
NULL AS app_version,
COALESCE(tl.level_end, tu.level_end, tu.level_start) AS level,
COALESCE(tl.levelup_ts,du.levelup_ts) AS levelup_ts,
tu.ab_experiment,
tu.ab_variant,
CASE WHEN COALESCE(du.is_payer,0) = 1 THEN 1 WHEN COALESCE(tp.purchase_cnt, 0) > 0 THEN 1 ELSE 0 END AS is_payer,
COALESCE (du.conversion_ts, tp.conversion_ts) as conversion_ts,
(COALESCE(tp.revenue_usd,0) + COALESCE(du.total_revenue_usd, 0)) AS total_revenue_usd,
(COALESCE(tp.purchase_cnt,0) + COALESCE(du.payment_cnt, 0)) AS payment_cnt,
tu.last_login_ts,
if(coins_flag=1, tld.coins_bal,du.coin_wallet) coin_wallet,
if(coins_flag=1, tld.coins_in,du.coins_in) coins_in  ,
if(coins_flag=1, tld.coins_out,du.coins_out) coins_out ,
if(rc_flag=1, tld.rc_bal,du.rc_wallet) rc_wallet  ,
if(rc_flag=1, tld.rc_in,du.rc_in) rc_in ,
if(rc_flag=1, tld.rc_out,du.rc_out) rc_out,
COALESCE (u.email, du.email) AS email,
COALESCE (tu.last_ip, du.last_ip) AS last_ip,
COALESCE(u.user_name,du.user_name) as user_name,
COALESCE(tp.last_payment_date, du.last_payment_date) as last_payment_date,
COALESCE(tis.install_source,du.install_source_display) as install_source_display,
tu.app,
'${hiveconf:rpt_date}' AS dt
FROM (SELECT * FROM tmp_user_daily_login WHERE dt = '${hiveconf:rpt_date}') tu 
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE dt = '${hiveconf:rpt_date_d1}') 
du on (tu.user_key = du.user_key and du.app = tu.app )
LEFT OUTER JOIN (SELECT app, dt, user_key, purchase_cnt, conversion_ts, revenue_usd, last_payment_date FROM tmp_user_payment WHERE dt = '${hiveconf:rpt_date}') 
tp ON (tu.user_key = tp.user_key and tu.app = tp.app )
LEFT OUTER JOIN (SELECT app, dt, user_key, install_source FROM tmp_install_source WHERE dt = '${hiveconf:rpt_date}') 
tis ON (tu.user_key = tis.user_key and tu.app=tis.app )
LEFT OUTER JOIN (SELECT app, dt, user_key, level_end, levelup_ts FROM tmp_user_level WHERE dt = '${hiveconf:rpt_date}') 
tl ON ( tu.user_key = tl.user_key and tu.app = tl.app )
LEFT OUTER JOIN (SELECT app, dt, uid, coins_bal, coins_in, coins_out, rc_bal, rc_in, rc_out,coins_flag,rc_flag FROM tmp_ledger_daily WHERE dt = '${hiveconf:rpt_date}')
tld on (tu.uid = tld.uid and tu.app = tld.app)
LEFT OUTER JOIN (SELECT dt, uid, user_name, email, app FROM user_geo WHERE dt = '${hiveconf:rpt_date}') 
u on (tu.uid = u.uid and tu.app = u.app)
LEFT OUTER JOIN (SELECT app, dt, app_user_id, facebook_id FROM tmp_user_snsid WHERE dt = '${hiveconf:rpt_date}') 
tus ON (tu.uid = tus.app_user_id and tu.app = tus.app)
UNION ALL
SELECT distinct du.id,du.user_key,du.app_id,du.app_user_id,du.facebook_id,du.install_ts,du.install_date
, du.install_source,du.install_subpublisher,du.install_campaign,du.install_language,du.install_locale
,du.install_country_code,du.install_country,du.install_os,du.install_device,du.install_device_alias,du.install_browser
,du.install_gender,du.install_age,du.language,du.locale,du.birthday,du.gender,du.country_code,du.country,du.os
,du.os_version,du.device,du.device_alias,du.browser,du.browser_version,du.app_version,du.level,du.levelup_ts
,du.ab_experiment,du.ab_variant,du.is_payer,du.conversion_ts,du.total_revenue_usd,du.payment_cnt,du.last_login_ts
,du.coin_wallet, du.coins_in, du.coins_out, du.rc_wallet, du.rc_in, du.rc_out, COALESCE(u.email, du.email) AS email, du.last_ip
,COALESCE(u.user_name, du.user_name) AS user_name,du.last_payment_date,du.install_source_display, du.app, '${hiveconf:rpt_date}' AS dt
FROM farm_1_1.dim_user du 
  LEFT OUTER JOIN (SELECT app, dt, uid, user_key FROM tmp_user_daily_login WHERE dt = '${hiveconf:rpt_date}')
  tu ON(tu.app = du.app and tu.user_key = du.user_key )
  LEFT OUTER JOIN (SELECT dt, uid, user_name, email, app FROM user_geo WHERE dt = '${hiveconf:rpt_date}') 
  u ON (du.app_user_id = u.uid and du.app = u.app)
WHERE du.dt='${hiveconf:rpt_date_d1}' AND tu.user_key IS NULL;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
SET hive.input.format=org.apache.hadoop.hive.ql.io.CombineHiveInputFormat;
  
ALTER TABLE copy_dim_user DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

INSERT OVERWRITE TABLE copy_dim_user PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT id, user_key, app_id, app_user_id, facebook_id, install_ts , install_date, install_source
, install_subpublisher, install_campaign, install_language, install_locale, install_country_code, install_country
, install_os, install_device, install_device_alias, install_browser, install_gender, install_age, language, locale
, birthday, gender, country_code, country, os, os_version, device, device_alias, browser, browser_version, app_version
, level, levelup_ts, ab_experiment, ab_variant, is_payer, conversion_ts, total_revenue_usd, payment_cnt, last_login_ts
, coin_wallet, coins_in, coins_out, rc_wallet, rc_in, rc_out, email, last_ip, user_name, last_payment_date,install_source_display
FROM dim_user WHERE dt='${hiveconf:rpt_date}';

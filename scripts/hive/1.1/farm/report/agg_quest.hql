----------------------------------
-- Query to generate agg_quest
----------------------------------

use farm_1_1;


SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET rpt_app = ${RPT_APP};
SET rpt_date = ${RPT_DATE};
SET rpt_date_d1 = ${RPT_DATE_D1};
SET rpt_date_d30 = ${RPT_DATE_D30};
SET rpt_date_d60 = ${RPT_DATE_D60};
SET rpt_date_d180 = ${RPT_DATE_D180};
SET rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};

INSERT OVERWRITE TABLE agg_quest PARTITION(app,install_dt) 
select du.install_date, du.app_id, du.os, du.os_version, du.country, du.install_source, du.browser
, du.browser_version, du.language
, rq.quest_id, rq.quest_type, rq.task_id, rq.action
, count(1) AS user_cnt 
, rq.rc_out
,du.app, du.install_date as install_dt
FROM 
(
    SELECT app, uid, quest_id, quest_type, task_id, action, rc_out FROM (
    SELECT  app, uid, quest_id, quest_type, task_id, action, rc_out
    , row_number() over (partition by app, uid, quest_id, quest_type, task_id, action order by ts_pretty DESC) rnum 
    FROM raw_quest_daily WHERE to_date(install_ts_pretty) > '${hiveconf:rpt_date_d60}'  AND dt > '${hiveconf:rpt_date_d60}' AND dt <= '${hiveconf:rpt_date}'
    ) t WHERE rnum = 1
) rq
JOIN
(
    select app_user_id, app_id, install_date, app, os, os_version, country, install_source
    , browser, browser_version, language 
    FROM dim_user WHERE dt = '${hiveconf:rpt_date}' 
    AND install_date <= '${hiveconf:rpt_date}' AND install_date > '${hiveconf:rpt_date_d60}'
) du 
ON (du.app = rq.app AND du.app_user_id = rq.uid)
GROUP BY du.install_date, du.app, du.app_id, du.os, du.os_version, du.country, du.install_source, du.browser
, du.browser_version, du.language, rq.quest_id, rq.quest_type, rq.task_id, rq.action
, rq.rc_out
UNION ALL
select install_date, app_id, os, os_version, country, install_source, browser, browser_version, language
, 0 AS quest_id, 0 AS quest_type, 0 AS task_id, 0 AS action, count(1) AS user_cnt , 0 rc_out
,app, install_date as install_dt
FROM dim_user WHERE dt = '${hiveconf:rpt_date}' 
    AND install_date <= '${hiveconf:rpt_date}' AND install_date > '${hiveconf:rpt_date_d60}'
GROUP BY  install_date, app, app_id, os, os_version, country, install_source, browser, browser_version, language ;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_quest DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

INSERT OVERWRITE TABLE copy_agg_quest PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT install_date,
app_id,
os,
os_version,
country,  
install_source,
browser,
browser_version,
language,
quest_id,
quest_type,
task_id,
action,
user_cnt, 
rc_out
FROM agg_quest 
WHERE 
install_dt <= '${hiveconf:rpt_date}'
AND install_dt > '${hiveconf:rpt_date_d60}';

----------------------------------
-- Query to generate agg_tutorial
----------------------------------

use farm_1_1;


SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;


--ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_d30 = ${RPT_DATE_D30};
set rpt_date_d60 = ${RPT_DATE_D60};
set rpt_date_d180 = ${RPT_DATE_D180};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};

INSERT OVERWRITE TABLE agg_tutorial PARTITION(app,install_dt) 
select du.install_date, du.app_id, du.os, du.os_version, du.country, du.install_source, du.browser
, du.browser_version, du.language
, rt.step
, count(*) AS user_cnt, du.app
, du.install_date as install_dt
FROM 
(
SELECT DISTINCT app, uid, step FROM raw_tutorial_daily WHERE to_date(install_ts_pretty) > '${hiveconf:rpt_date_d60}'  AND dt > '${hiveconf:rpt_date_d60}' AND dt <= '${hiveconf:rpt_date}'
) rt
INNER JOIN
(
select app_user_id, app_id, install_date, app, os, os_version, country, install_source, 
browser, browser_version, language
FROM dim_user WHERE dt = '${hiveconf:rpt_date}' 
AND install_date <= '${hiveconf:rpt_date}' AND install_date > '${hiveconf:rpt_date_d60}'
) du 
ON (du.app = rt.app AND du.app_user_id = rt.uid)
GROUP BY du.install_date, du.app, du.app_id, du.os, du.os_version, du.country, du.install_source, du.browser
, du.browser_version, du.language
, rt.step
UNION ALL
SELECT
install_date,
app_id,
os,
os_version,
country,  
install_source,
browser,
browser_version,
language,
0 AS step,
count(*) AS user_cnt, app
, install_date as install_dt
FROM dim_user
WHERE  dt = '${hiveconf:rpt_date}' 
AND install_date <= '${hiveconf:rpt_date}' AND install_date > '${hiveconf:rpt_date_d60}'
GROUP BY install_date, app_id, app, os, os_version, country, install_source
, browser, browser_version, language
distribute by install_date sort by install_date, app_id, os, os_version, country, install_source
, browser, browser_version, language;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_tutorial DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');
  
INSERT OVERWRITE TABLE copy_agg_tutorial PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT 
install_date,
app_id,
os,
os_version,
country,  
install_source,
browser,
browser_version,
language,
step,
user_cnt
FROM agg_tutorial
WHERE 
install_dt <= '${hiveconf:rpt_date}'
AND install_dt > '${hiveconf:rpt_date_d60}';

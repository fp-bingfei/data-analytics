use farm_1_1;


SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_d30 = ${RPT_DATE_D30};
set rpt_date_d60 = ${RPT_DATE_D60};
set rpt_date_d180 = ${RPT_DATE_D180};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};

INSERT OVERWRITE TABLE agg_kpi PARTITION (app,dt)
select MD5(concat(COALESCE(t.date,''),COALESCE(t.app_id,''),COALESCE(t.app_version,''),COALESCE(t.install_year,''),COALESCE(t.install_month,''),COALESCE(t.install_week,''),COALESCE(t.install_source,''),COALESCE(level_end,0),
COALESCE(t.browser,''),COALESCE(t.browser_version,''),COALESCE(t.device_alias,''),COALESCE(t.country,''),COALESCE(t.os,''),COALESCE(t.os_version,''),COALESCE(t.language,''),COALESCE(t.is_new_user,''),COALESCE(t.is_payer,'0')))
,t.date
,t.app_id
,t.app_version
,t.install_year
,t.install_month
,t.install_week
,t.install_source
,t.level_end
,t.browser
,t.browser_version
,t.device_alias
,t.country
,t.os
,t.os_version
,t.language
,t.is_new_user
,t.is_payer
,sum(t.new_user_cnt)
,sum(t.dau_cnt)
,sum(t.newpayer_cnt)
,sum(t.payer_today_cnt)
,sum(t.payment_cnt)
,sum(t.revenue_usd)
,sum(t.session_cnt)
,0
,t.app
,t.dt from
(
select d.date
,d.app_id
,d.app_version
,year(u.install_date)  install_year
,month(u.install_date) install_month
,weekofyear(u.install_date) install_week
,u.install_source_display as install_source
,d.level_end
,d.browser
,d.browser_version
,u.device_alias
,d.country
,d.os
,d.os_version
,d.language
,d.is_new_user
,d.is_payer
,sum(d.is_new_user) as new_user_cnt
,count(d.user_key) as dau_cnt
,sum(d.is_converted_today) as newpayer_cnt
,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
,sum(d.payment_cnt) as payment_cnt
,sum(d.revenue_usd) as revenue_usd
,sum(d.session_cnt) as session_cnt
,0
,d.app
,d.dt
from (select * from fact_dau_snapshot where dt >='${hiveconf:rpt_date_d30}' and dt <='${hiveconf:rpt_date}' ) d
Left outer join (select * from dim_user where dt='${hiveconf:rpt_date}') u
on d.user_key = u.user_key and d.app=u.app
group by d.date,d.app_id,d.app,d.dt,d.app_version,u.install_source_display,year(u.install_date),month(u.install_date),weekofyear(u.install_date),d.os,d.os_version,d.browser,d.browser_version,u.device_alias,d.country,d.language,d.is_new_user,d.is_payer,d.level_end
UNION ALL
select
u.install_date as date
,u.app_id
,u.app_version
,year(u.install_date)  install_year
,month(u.install_date) install_month
,weekofyear(u.install_date) install_week
,u.install_source_display as install_source
,null as level_end
,u.browser
,u.browser_version
,u.device_alias
,u.country
,u.os
,u.os_version
,u.language
,1 as is_new_user
,u.is_payer
,count(u.user_key) as new_user_cnt
,count(u.user_key) as dau_cnt
,0 as newpayer_cnt
,0 as payer_today_cnt
,0 as payment_cnt
,0 as revenue_usd
,0 as session_cnt
,0
,u.app
,u.install_date as dt
from (select *  from dim_user WHERE dt='${hiveconf:rpt_date}'
and install_date>='${hiveconf:rpt_date_d30}' and install_date<='${hiveconf:rpt_date}') u
LEFT OUTER JOIN
(SELECT user_key, dt, app FROM fact_dau_snapshot WHERE dt >='${hiveconf:rpt_date_d30}' and dt <='${hiveconf:rpt_date}' ) f
ON  u.install_date = f.dt and u.user_key = f.user_key WHERE f.dt is NULL
group by
u.install_date
,u.app_id
,u.app_version
,year(u.install_date)
,month(u.install_date)
,weekofyear(u.install_date)
,u.install_source_display
,u.browser
,u.browser_version
,u.device_alias
,u.country
,u.os
,u.os_version
,u.language
,u.is_payer
,u.app
)t
group by t.date
,t.app_id
,t.app_version
,t.install_year
,t.install_month
,t.install_week
,t.install_source
,t.level_end
,t.browser
,t.browser_version
,t.device_alias
,t.country
,t.os
,t.os_version
,t.language
,t.is_new_user
,t.is_payer
,t.app
,t.dt;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_kpi DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


insert overwrite table copy_agg_kpi partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') SELECT
id, date, app_id, app_version, install_year, install_month, install_week, install_source, level_end, browser, browser_version, device_alias, country, os, os_version, language, is_new_user, is_payer, new_user_cnt, dau_cnt, newpayer_cnt, payer_today_cnt, payment_cnt, revenue_usd, session_cnt, session_length_sec 
from agg_kpi where  dt >='${hiveconf:rpt_date_d30}' and dt <='${hiveconf:rpt_date}';
 	 



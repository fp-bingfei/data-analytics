
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;



set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};

INSERT OVERWRITE TABLE raw_marketorderfinish_daily PARTITION (app, dt)
SELECT
key, 
ts, 
ts_pretty, 
browser, 
browser_version, 
country_code, 
event, 
install_source, 
install_ts, 
install_ts_pretty, 
ip, 
lang, 
level, 
os, 
os_version, 
uid, 
snsid, 
item_id, 
fb_source, 
order_points_get, 
item_quantity,
time_left,
new_cash1_get,
new_cash2_get,
new_cash3_get,
crates,
app,
'${RPT_DATE}' dt
FROM farm_1_1.raw_marketorderfinish_seq 
WHERE 
(
(year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day})
OR
(year=${hiveconf:rpt_year_plus1} AND month = ${hiveconf:rpt_month_plus1} AND day = ${hiveconf:rpt_day_plus1})
) AND 
to_date(ts_pretty) = '${hiveconf:rpt_date}' 
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

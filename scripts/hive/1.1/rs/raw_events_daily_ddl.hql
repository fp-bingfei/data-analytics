CREATE EXTERNAL TABLE IF NOT EXISTS raw_events_daily (
    data_version string,
    app_id string,
    ts string,
    ts_pretty string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >
)
PARTITIONED BY (
  event string,
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/rs_1_1/events_daily/'
TBLPROPERTIES (
  'serialization.null.format'=''
);
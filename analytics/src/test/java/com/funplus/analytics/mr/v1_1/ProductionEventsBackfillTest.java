package com.funplus.analytics.mr.v1_1;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by chunzeng on 12/24/2014.
 */
public class ProductionEventsBackfillTest {

    MapDriver<LongWritable, Text, Text, Text> mapDriver;
    ReduceDriver<Text, Text, NullWritable, Text> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, Text, NullWritable, Text> mapReduceDriver;

    @Before
    public void setUp() {
    	ProductionEventsBackfill.Map mapper = new ProductionEventsBackfill.Map();
    	ProductionEventsBackfill.Reduce reducer = new ProductionEventsBackfill.Reduce();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapJson() throws IOException, JSONException {
        String inputJson = "{\"amount\":799,\"product_type\":\"currency\",\"transaction_id\":\"26280084\",\"payment_processor\":\"windowsstore\",\"product_name\":\"rc35\",\"currency\":\"USD\",\"cash_type\":\"rc\",\"coins_in\":0,\"rc_in\":35,\"is_gift\":0,\"pay_times\":1,\"reward_package\":\"\",\"product_id\":\"com.funplus.familyfarm.rc35\",\"lang\":\"en_US\",\"uid\":18058,\"os\":\"ffs.dev.wp\",\"level\":20,\"ip\":\"54.74.61.165\",\"snsid\":\"e55c2a0ab61902917cd447f3e21a677a\",\"os_version\":\"\",\"track_ref\":\"\",\"addtime\":\"2014-10-26 04:03:33\",\"device\":\"Nokia\",\"rc_left\":3,\"coins_left\":1784,\"install_source\":\"\",\"install_ts\":1414296213,\"event\":\"payment\",\"GameVersion\":\"ffs-wp\",\"@key\":\"ffs.wp.prod\",\"@ts\":\"1417393626\"}";
        String outputJson = "b969e511-2065-428b-8bc2-5b1644637d36\tffs.wp.prod\t2014-12-01 08:27:06\t18058\te55c2a0ab61902917cd447f3e21a677a\t2014-10-26 12:03:33\t\t\t54.74.61.165\t\t\tffs.dev.wp\t\tpayment\t{\"product_id\":\"com.funplus.familyfarm.rc35\",\"product_name\":\"rc35\",\"cash_type\":\"rc\",\"lang\":\"en_US\",\"currency\":\"USD\",\"amount\":799,\"addtime\":\"2014-10-26 04:03:33\",\"level\":20,\"GameVersion\":\"ffs-wp\",\"payment_processor\":\"windowsstore\",\"track_ref\":\"\",\"is_gift\":0,\"coins_in\":0,\"pay_times\":1,\"rc_in\":35,\"product_type\":\"currency\",\"reward_package\":\"\",\"transaction_id\":\"26280084\",\"rc_left\":3,\"coins_left\":1784,\"device\":\"Nokia\"}";
        mapDriver.withInput(new LongWritable(), new Text(new JSONObject(inputJson).toString()));
        //mapDriver.withOutput(new Text("2014/12/01/08/part"), new Text(outputJson));
        //mapDriver.runTest();
        List<Pair<Text, Text>> results = mapDriver.run();
        assertEquals(results.get(0).getFirst().toString(), "2014/12/01/08/part");
        assertEquals(results.get(0).getSecond().toString().length(), outputJson.length());
        assertEquals(results.get(0).getSecond().toString().substring(36), outputJson.substring(36));
    }
}
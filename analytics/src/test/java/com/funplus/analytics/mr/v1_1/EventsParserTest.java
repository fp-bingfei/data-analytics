package com.funplus.analytics.mr.v1_1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

/**
 * Created by balaji on 10/17/14.
 */

//todo: more units test need to be added.
public class EventsParserTest {

    MapDriver<LongWritable, Text, Text, Text> mapDriver;
    ReduceDriver<Text, Text, NullWritable, Text> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, Text, NullWritable, Text> mapReduceDriver;

    @Before
    public void setUp() {
        EventsParser.Map mapper = new EventsParser.Map();
        EventsParser.Reduce reducer = new EventsParser.Reduce();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
        setUpConfiguration();
    }


    @Test
    public void testMapperForValidData() throws IOException, JSONException {
        String inputJson = "{\"uid\":\"30672\",\"snsid\":\"7f1cf8ffffd550e18e51ed259dd14ab0\",\"install_ts\":1413216324,\"install_source\":\"\",\"os\":\"ios\",\"os_version\":\"7.1\",\"device\":\"iPad2,2\",\"ip\":\"\",\"lang\":\"en_US\",\"level\":16,\"abid\":[{\"test\":\"NewConstructionPartCostTF2\",\"variant\":4}],\"action\":\"finish_npc_order\",\"action_detail\":\"5300001\",\"location\":\"NpcOrder\",\"event\":\"npc_order\",\"coins\":\"22\",\"xp\":\"1\",\"npc\":\"5300001\",\"time\":1413241731,\"@key\":\"bv.global.testflight\",\"@ts\":\"1413241231\"}";
        String outputJson = "{\"uid\":\"30672\",\"npc\":\"5300001\",\"os\":\"ios\",\"@key\":\"bv.global.testflight\",\"location\":\"NpcOrder\",\"@ts\":\"1413241231\",\"abid\":[{\"test\":\"NewConstructionPartCostTF2\",\"variant\":4}],\"xp\":\"1\",\"@ts_pretty\":\"2014-10-13 16:00:31\",\"country_code\":\"\",\"lang\":\"en_US\",\"os_version\":\"7.1\",\"ip\":\"\",\"snsid\":\"7f1cf8ffffd550e18e51ed259dd14ab0\",\"install_ts_pretty\":\"2014-10-13 09:05:24\",\"time\":1413241731,\"install_ts\":1413216324,\"level\":16,\"coins\":\"22\",\"event\":\"npc_order\",\"action_detail\":\"5300001\",\"device\":\"iPad2,2\",\"action\":\"finish_npc_order\",\"install_source\":\"\"}";
        mapDriver.withInput(new LongWritable(), new Text(inputJson));
        mapDriver.withOutput(new Text("npc_order"), new Text(outputJson));
        mapDriver.runTest();
    }
    
    @Test
    public void testMapperForNoTabData() throws IOException, JSONException {
    	BufferedReader br = null;
    	String sCurrentLine = null;
    	String inputJson = null;
		try {
			br = new BufferedReader(new FileReader("resources/testnotab"));
 
			while ((sCurrentLine = br.readLine()) != null) {
				inputJson = sCurrentLine;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}
        String outputJson = "{\"uid\":10783946,\"os\":\"Windows 7\",\"@key\":\"farm.us.prod\",\"location\":\"Storage\",\"@ts\":\"1417327194\",\"item_out\":1,\"@ts_pretty\":\"2014-11-29 21:59:54\",\"country_code\":\"\",\"lang\":\"am\",\"os_version\":\"\",\"ip\":\"\",\"browser_version\":\"33.0.1750\",\"snsid\":\"100005370640350\",\"install_ts_pretty\":\"2013-03-22 06:05:33\",\"install_ts\":1363957533,\"level\":51,\"item_name\":\"Mario's Sausage Pizza\",\"item_id\":52016,\"item_class\":\"c\",\"event\":\"item_transaction\",\"browser\":\"Chrome\",\"action\":\"sold\",\"install_source\":\"\",\"item_type\":\"products\"}";
        mapDriver.withInput(new LongWritable(), new Text(inputJson));
        mapDriver.withOutput(new Text("item_transaction"), new Text(outputJson));
        mapDriver.runTest();
    }
    
    @Test
    public void testMapperForDataTypeMismatch() throws IOException, JSONException {
        String inputJson = "{\"uid\":\"2036\",\"os\":\"ios\",\"@key\":\"bv.global.testflight\",\"coins_in\":\"78\",\"location\":\"BoardOrder\",\"@ts\":\"1414434800\",\"coins_bal\":1123,\"abid\":[],\"lang\":\"en_US\",\"os_version\":\"7.1.2\",\"ip\":\"\",\"snsid\":\"a73136dfb01dc4286ddc0fc025eeef0c\",\"install_ts\":1398894372,\"level\":31,\"event\":\"coins_transaction\",\"action_detail\":\"\",\"device\":\"iPad3,1\",\"action\":\"reward_b_order\",\"install_source\":\"\"}";
        String outputJson = "{\"uid\":\"2036\",\"os\":\"ios\",\"error\":\"VALIDATION_FAILED\",\"errorDetails\":[\"coins_in instance type (string) does not match any allowed primitive type (allowed: [integer,number])\"],\"@key\":\"bv.global.testflight\",\"coins_in\":\"78\",\"location\":\"BoardOrder\",\"@ts\":\"1414434800\",\"coins_bal\":1123,\"abid\":[],\"lang\":\"en_US\",\"os_version\":\"7.1.2\",\"ip\":\"\",\"snsid\":\"a73136dfb01dc4286ddc0fc025eeef0c\",\"level\":31,\"install_ts\":1398894372,\"action_detail\":\"\",\"event\":\"coins_transaction\",\"action\":\"reward_b_order\",\"device\":\"iPad3,1\",\"install_source\":\"\"}";
        mapDriver.withInput(new LongWritable(), new Text(new JSONObject(inputJson).toString()));
        mapDriver.withOutput(new Text("invalid"), new Text(outputJson));
        mapDriver.runTest();
    }

    @Test
    public void testMapperForUnwantedProperties() throws IOException, JSONException {
        String inputJson = "{\"uid\":\"30227\",\"snsid\":\"21ab9f3b40d5b45f80507ba6c1839c7f\",\"install_ts\":1407768141,\"install_source\":\"\",\"os\":\"android\",\"os_version\":\"4.4.2\",\"device\":\"SM-G900T\",\"ip\":\"\",\"lang\":\"en_US\",\"level\":34,\"abid\":[],\"action\":\"retrieve_data\",\"action_detail\":\"\",\"location\":\"\",\"event\":\"session_start\",\"@key\":\"bv.global.testflight\",\"@ts\":\"1414429559\",\"test1\":\"value1\",\"test2\":\"value2\"}";
        String outputJson = "{\"uid\":\"30227\",\"os\":\"android\",\"error\":\"VALIDATION_FAILED\",\"errorDetails\":[\"object instance has properties which are not allowed by the schema: [test1,test2]\"],\"@key\":\"bv.global.testflight\",\"location\":\"\",\"@ts\":\"1414429559\",\"abid\":[],\"lang\":\"en_US\",\"test2\":\"value2\",\"os_version\":\"4.4.2\",\"ip\":\"\",\"snsid\":\"21ab9f3b40d5b45f80507ba6c1839c7f\",\"test1\":\"value1\",\"install_ts\":1407768141,\"level\":34,\"event\":\"session_start\",\"action_detail\":\"\",\"device\":\"SM-G900T\",\"action\":\"retrieve_data\",\"install_source\":\"\"}";
        mapDriver.withInput(new LongWritable(), new Text(new JSONObject(inputJson).toString()));
        mapDriver.withOutput(new Text("invalid"), new Text(outputJson));
        mapDriver.runTest();
    }

    @Test
    public void testMapperForNoSchema() throws IOException, JSONException {
        String inputJson = "{\"uid\":\"2036\",\"event\":\"test\"}";
        String outputJson = "{\"uid\":\"2036\",\"error\":\"SCHEMA_NOT_FOUND\",\"event\":\"test\"}";
        mapDriver.withInput(new LongWritable(), new Text(new JSONObject(inputJson).toString()));
        mapDriver.withOutput(new Text("invalid"), new Text(outputJson));
        mapDriver.runTest();
    }

    // @Test
    public void testMapReduceForValidData() throws IOException, JSONException {
        Reducer.Context mockContext = mock(Reducer.Context.class);
        when(mockContext.getJobName()).thenReturn("testJob");
        String inputJson = "{\"uid\":\"30227\",\"snsid\":\"21ab9f3b40d5b45f80507ba6c1839c7f\",\"install_ts\":1407768141,\"install_source\":\"\",\"os\":\"android\",\"os_version\":\"4.4.2\",\"device\":\"SM-G900T\",\"ip\":\"\",\"lang\":\"en_US\",\"level\":34,\"abid\":[],\"action\":\"add_plant\",\"action_detail\":\"52\",\"location\":\"Plots\",\"event\":\"coins_transaction\",\"coins_out\":5,\"coins_bal\":239997,\"@key\":\"bv.global.testflight\",\"@ts\":\"1414429666\"}";
        String outputJson = "{\"uid\":\"30672\",\"npc\":\"5300001\",\"os\":\"ios\",\"@key\":\"bv.global.testflight\",\"location\":\"NpcOrder\",\"@ts\":\"1413241231\",\"abid\":[{\"test\":\"NewConstructionPartCostTF2\",\"variant\":4}],\"xp\":\"1\",\"@ts_pretty\":\"2014-10-13 16:00:31\",\"country_code\":\"\",\"lang\":\"en_US\",\"os_version\":\"7.1\",\"ip\":\"\",\"snsid\":\"7f1cf8ffffd550e18e51ed259dd14ab0\",\"install_ts_pretty\":\"2014-10-13 09:05:24\",\"time\":1413241731,\"install_ts\":1413216324,\"level\":16,\"coins\":\"22\",\"event\":\"npc_order\",\"action_detail\":\"5300001\",\"device\":\"iPad2,2\",\"action\":\"finish_npc_order\",\"install_source\":\"\"}";
        mapReduceDriver.withInput(new LongWritable(), new Text(new JSONObject(inputJson).toString()));
        mapReduceDriver.withOutput(NullWritable.get(), new Text(outputJson));
        mapReduceDriver.runTest();
    }


    //@Test
    public void testCounter(){

    }


    private void setUpConfiguration() {
        Map<String, String>  settings = generateSetting();
        setConfigurationInDriver(mapDriver.getConfiguration(), settings);
        setConfigurationInDriver(reduceDriver.getConfiguration(), settings);
        setConfigurationInDriver(mapReduceDriver.getConfiguration(), settings);

    }

    private Map generateSetting() {
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("fs.s3n.awsAccessKeyId", "AKIAJRKDNC52OAINDWKA");
        settings.put("fs.s3n.awsSecretAccessKey", "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");
        settings.put("app", "bv");
        settings.put("jobName", "bv");
        settings.put("specVersion", "test");
        settings.put("outputBasePathFormat", "/usr/bv/temp/%s/2014/10/10/10");
        settings.put("s3BucketName", "s3n://com.funplusgame.bidata");
        settings.put("s3SchemaPath", "/dev/schema/%s/%s/");
        return settings;
    }

    private void setConfigurationInDriver(Configuration configuration, Map<String,String> settings){
        for(String key : settings.keySet()){
            configuration.set(key, settings.get(key));
        }
    }


}

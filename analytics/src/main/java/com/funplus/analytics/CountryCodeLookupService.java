package com.funplus.analytics;

import com.maxmind.geoip.Country;
import com.maxmind.geoip.LookupService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by balaji on 10/16/14.
 */
public class CountryCodeLookupService {

    private static final String GEO_IP_DATABASE = "GeoIP.dat";

    private static final Logger LOGGER = Logger.getLogger(CountryCodeLookupService.class);


    private static LookupService LOOKUP_SERVICE = null;

    private CountryCodeLookupService(){
        try {
            LOOKUP_SERVICE = new LookupService(GEO_IP_DATABASE,
                    LookupService.GEOIP_MEMORY_CACHE);
        } catch (IOException e) {
            //do nothing

            LOGGER.warn(String.format("Exception occurred when setting up CountryCodeLookupService - %s", e.getMessage()));
        }
    }


    private static class CountryCodeLookupServiceHolder {
        public static final CountryCodeLookupService INSTANCE = new CountryCodeLookupService();
    }

    public static CountryCodeLookupService getInstance() {
        return CountryCodeLookupServiceHolder.INSTANCE;
    }

    /**
     * Provides the country code for the given ip
     * @param ip
     * @return
     */
    public String getCountryCode(String ip){
        String countryCode = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(ip)) {
            Country country = LOOKUP_SERVICE.getCountry(ip);
            if (null != country) {
                countryCode = country.getCode();
            }
        }

        return countryCode;
    }


    /**
     * Utility method w.r.t. country code to include country code in output json
     * @param countryCodeKey
     * @param outputJson
     */
    public void includeCountryCodeInJson(String countryCodeKey, JSONObject outputJson) {
        String ip = StringUtils.EMPTY;
        try {
            ip = outputJson.getString("ip");
        } catch (Exception e){
            //do nothing
        }
        outputJson.put(countryCodeKey, getCountryCode(ip));
    }

}

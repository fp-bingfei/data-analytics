package com.funplus.analytics;

/**
 * Created by balaji on 10/14/14.
 */

import com.funplus.analytics.validation.JsonError;
import com.maxmind.geoip.Country;
import com.maxmind.geoip.LookupService;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.json.JSONObject;
import org.mortbay.log.Log;

import java.io.PrintWriter;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Reusable utility code goes here.
 */

public class Utilities {

    private static final Object LOCK = new Object();

    private static final String GEOIPDATABASE = "GeoIP.dat";
    private static final String GLOBAL = "global";
    private static LookupService LOOKUP_SERVICE = null;
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String ERROR = "error";
    private static final String ERROR_DETAILS = "errorDetails";


    /**
     * Provides the country code for the given ip
     * If no match or exception default("") is returned
     * @param ip
     * @return
     */
    /**
     * Use CountryCodeLookupService
     */
    @Deprecated
    public static String getCountryCodeForIP(String ip) {
        String countryCode = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(ip)) {

            Country country = null;
            try {
                if(null == LOOKUP_SERVICE){
                    synchronized (LOCK) {
                        if (null == LOOKUP_SERVICE) {
                            LOOKUP_SERVICE = new LookupService(GEOIPDATABASE,
                                    LookupService.GEOIP_MEMORY_CACHE);
                        }
                    }
                }
                country = LOOKUP_SERVICE.getCountry(ip);
                if (null != country) {
                    countryCode = country.getCode();
                }
            } catch (Exception e) {
                //do nothing
            } finally {
                try {
                    LOOKUP_SERVICE.close();
                } catch (Exception e) {
                }
            }

        }

        return countryCode;
    }

    /**
     * Converts the timestamp to given date format
     * @param timeStamp
     * @param format
     * @return
     */
    public static String getFormattedDateForTimestamp(int timeStamp, String format){
        Date unixDate = new Date((long) timeStamp * 1000);
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
        return dateFormatter.format(unixDate);
    }



    /**
     * Converts the timestamp to given date format
     * @param timeStamp
     * @return
     */
    public static String getDefaultFormattedDateForTimestamp(int timeStamp){
        return getFormattedDateForTimestamp(timeStamp, DEFAULT_DATE_FORMAT);
    }

    /**
     * Utility to validate given fieldname value from input jsonObject with inValidValue data.
     * @param jsonObject
     * @param fieldName
     * @param inValidValue
     * @return
     */
    public static boolean validate(JSONObject jsonObject, String fieldName, String inValidValue){
        try {
            String jsonValue = String.valueOf(jsonObject.get(fieldName));
            return !inValidValue.equals(jsonValue);
        }
        catch (Exception e){
            // return false for exception
            return Boolean.FALSE;
        }
    }


    /**
     * Get filename from the given path
     * Last index is the filename
     * @param pathString
     * @return
     */
    public static String getFileNameFromPath(String pathString){
        String fileName = StringUtils.EMPTY;
        if(StringUtils.isNotBlank(pathString)){
            String[] filePathFields = StringUtils.split(pathString, '/');
            fileName = filePathFields[filePathFields.length - 1];
        }
        return fileName;
    }


    /**
     * Generates the schema dir path to be scanned for json schemas.
     * By default global is included
     * @param schemaFormat
     * @param specVersion
     * @param app
     * @return
     */
    public static Path[] generateSchemaPathList(String schemaFormat, String specVersion, String app){
        List<Path> pathList = new ArrayList<Path>();
        pathList.add(new Path(String.format(schemaFormat, specVersion, GLOBAL)));
        if(StringUtils.isNotBlank(app)){
            pathList.add(new Path(String.format(schemaFormat, specVersion, app)));
        }
        return pathList.toArray(new Path[pathList.size()]);
    }

    /**
     * Include data from JsonError in JsonObject
     * @param outputJson
     * @param jsonError
     */
    public static void includeErrorDataInJson(JSONObject outputJson, JsonError jsonError){
        if(null != jsonError) {
            outputJson.put(ERROR, jsonError.getError());
            List<String> detailMessages = jsonError.getDetailMessages();
            if(null != detailMessages){
                for(String message : detailMessages){
                    outputJson.append(ERROR_DETAILS, message);
                }
            }
        }
    }

    public static void includePrettyDateInJson(String inputKey, String outputKey, JSONObject outputJson, String format){
        String prettyDate = StringUtils.EMPTY;
        try{
            prettyDate = getFormattedDateForTimestamp(outputJson.getInt(inputKey), format);
        }
        catch (Exception e){}
        outputJson.put(outputKey, prettyDate);

    }

    public static void includeDefaultPrettyDateInJson(String inputKey, JSONObject outputJson){
        String outputKey = new StringBuilder(inputKey).append("_").append("pretty").toString();
        includePrettyDateInJson(inputKey, outputKey, outputJson, DEFAULT_DATE_FORMAT);
    }

    public static JSONObject renameJSONStringKey(String renameFrom, String renameTo, JSONObject json) {
    	//if target key already exists - do nothing
    	if (json.has(renameTo)) {
    		return json;
    	}
    	if (json.has(renameFrom)) {
    		String value = json.optString(renameFrom,"");
    		json.remove(renameFrom);
    		json.put(renameTo, value);
    	}
    	return json;
    }

    public static String getDateFormatConverted(String inputDateString, String fromFormat, String toFormat) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat(fromFormat);
        Date inputDate = inputFormat.parse(inputDateString);
        SimpleDateFormat outputFormat = new SimpleDateFormat(toFormat);
        return outputFormat.format(inputDate);
    }



    public static void writeCounters(Job job, String rawTempPath) {


        String tempBucketName = rawTempPath.substring(0, StringUtils.ordinalIndexOf(rawTempPath, "/", 3));

        try {
            Configuration conf = job.getConfiguration();
            Counters counters = job.getCounters();
            FileSystem tempFileSystem = FileSystem.get(new URI(tempBucketName),conf);
            FSDataOutputStream fsDataOutputStream = tempFileSystem.create(new Path(rawTempPath + "counters.txt"));
            PrintWriter writer = new PrintWriter(fsDataOutputStream);

            for (CounterGroup group : counters) {
                if (group.getDisplayName().equals("EventsParserJob")) {
                    for (Counter counter : group) {
                        writer.write(counter.getDisplayName() + "\t" + counter.getValue() + "\n");
                    }
                }
            }

            writer.close();
            fsDataOutputStream.close();

        } catch (Exception e) {
            Log.info("something is wrong in writeCounters!!");
        }
    }

}

package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.json.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;

import com.maxmind.geoip.LookupService;

/*
 * Converts adjust JSON to TSV
 */
public class AdjustTSV extends Configured implements Tool {
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;

	private final static String[] KEYARRAY = new String[] { 
		"@id",
		"adid", 
		"android_id", 
		"app_id",
		"country",
		"game",
		"idfa",
		"idfa_md5",
		"ip_address",
		"mac_sha1",
		"timestamp",
		"tracker",
		"tracker_name",
		"userid"
 };
	// sentinel value for invalid integers
	private final static int MISSEDINT = -1;
	// empty string for empty fields
	private final static String NULLSTRING = "";
	// tab character
	private final static char TAB = '\t';

	public static class Map extends
			Mapper<LongWritable, Text, IntWritable, Text> {
		
		public Random rng;
		public int numReducers;
		public IntWritable[] reducerKeys;
		
		// construct the random number generator and set the number of reducers
		public void setup(Context context) {
			rng = new Random();
			numReducers = context.getNumReduceTasks();
			reducerKeys = new IntWritable[numReducers];
			for (int i = 0; i < numReducers; i++) {
				reducerKeys[i] = new IntWritable(i);
			}
		}
		
		// doesn't use the LongWritable key
		// returns (random number from 0 to NUMSPLITS - 1, Text constructed from JSON)
		// or (-1, Text with invalid properties length)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			
			//generate id
			String id = DigestUtils.md5Hex(value.toString());

			//transform legacy format to include @id in JSON
			JSONObject jsonLine = new JSONObject(value.toString());
			jsonLine.put("@id", id);
			
			ArrayList<String> tsvFields = getJSONFields(jsonLine);
			
			if (tsvFields != null) {
					context.write(reducerKeys[rng.nextInt(numReducers)], new Text(makeSBLine(tsvFields).toString()));
			}
		}

		private static ArrayList<String> getJSONFields(JSONObject jsonLine) {
			ArrayList<String> fieldKeys = new ArrayList<String>();
			fieldKeys.addAll(Arrays.asList(KEYARRAY));
			ArrayList<String> jsonFields = new ArrayList<String>();
			for (int i = 0; i < fieldKeys.size(); i++) {
				String key = fieldKeys.get(i);
				if (key.equals("timestamp")) {
					int ts = jsonLine.optInt(key);
					Date unixDate = new Date((long) ts * 1000);
					SimpleDateFormat dateFormatter = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					String tsvDate = dateFormatter.format(unixDate);
					jsonFields.add(tsvDate);
				}
				else {
					jsonFields.add(jsonLine.optString(key).replaceAll("\n", ""));
				}
			}
			 
			return jsonFields;
		}

		// wrapper for getting a string from json, returns empty string on
		// failure
		private static String getJSONString(JSONObject jsonLine, String key) {
			return getJSONString(jsonLine, key, "");
		}

		private static String getJSONString(JSONObject jsonLine, String key,
				String errMessage) {
			String value;
			try {
				value = jsonLine.getString(key);
			} catch (Exception e) {
				System.out.println("missed a field:" + errMessage);
				value = NULLSTRING;
			}
			return value;
		}

		// wrapper for getting an int from json, returns a sentinel on failure
		private static int getJSONInt(JSONObject jsonLine, String key) {
			return getJSONInt(jsonLine, key, "");
		}

		private static int getJSONInt(JSONObject jsonLine, String key,
				String errMessage) {
			int value;
			try {
				value = jsonLine.getInt(key);
			} catch (Exception e) {
				System.out.println("missed an int:" + errMessage);
				value = MISSEDINT;
			}
			return value;
		}

		// generates the tab-delimited StringBuilder out of the fields
		private static StringBuilder makeSBLine(ArrayList<String> fields) {
			StringBuilder sb = new StringBuilder(fields.get(0));
			for (int i = 1; i < fields.size(); i++) {
				sb.append(TAB);
				sb.append(fields.get(i));
			}
			return sb;
		}
	}
	
	// outputs just the tab-delimited line
	public static class Reduce extends
			Reducer<IntWritable, Text, NullWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text t : values) {
				context.write(BLANK, t);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new AdjustTSV(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {
		Job job = new Job(super.getConf());
		job.setJarByClass(AdjustTSV.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextOutputFormat.setCompressOutput(job, true);
		TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

}

package com.funplus.analytics.mr.v1_2;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.log.Log;

import com.funplus.analytics.CountryCodeLookupService;
import com.funplus.analytics.Utilities;
import com.funplus.analytics.validation.JsonError;
import com.funplus.analytics.validation.JsonSchemaValidation;
import com.funplus.analytics.validation.JsonSchemaValidatorFactory;

/**
 * Created by chunzeng on 01/13/2015.
 */

public class EventsParser extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(EventsParser.class);

    private static final String INVALID_EVENT = "invalid";
    private static final String INVALID_JSON = "invalidJson";
    private static final String COUNTRY_CODE = "country_code";
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";
    private static final String DEFAULT_SCHEMA = "default";

    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {

        private JsonSchemaValidatorFactory jsonSchemaValidatorFactory = null;
        private CountryCodeLookupService  countryCodeLookupService = null;
        private String specVersion = null;

        @Override
        protected void setup(Context context) throws IOException {

            //setup factory
            jsonSchemaValidatorFactory = JsonSchemaValidatorFactory.getInstance();
            jsonSchemaValidatorFactory.bootstrap(context.getConfiguration());

            //setup Country code service
            countryCodeLookupService = CountryCodeLookupService.getInstance();

            specVersion = context.getConfiguration().get("specVersion");
        }

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            JSONObject outputJson = null, invalidJson = null;
            JsonError jsonError = null;
            String eventName = null;
            String schemaValidatorKey = null;
            try {
                outputJson = new JSONObject(value.toString());

                try {
                    eventName = outputJson.getString("event");
                    schemaValidatorKey = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, eventName);
                    String defaultSchema = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, DEFAULT_SCHEMA);
                    jsonError = jsonSchemaValidatorFactory.validate(schemaValidatorKey, value.toString(), defaultSchema);
                } catch (Exception e) {
                    //do nothing
                    jsonError = new JsonError(JsonSchemaValidation.PROCESSING_ERROR.toString(), e.getMessage());
                }
                //check for error if available the input json is invalid
                if (null != jsonError) {
                    // Append invalid event
                	invalidJson = new JSONObject();
                	if (eventName != null) {
                		invalidJson.put("event", eventName);
                	} else {
                		invalidJson.put("event", INVALID_EVENT);
                	}
                	eventName = INVALID_EVENT;
                    Utilities.includeErrorDataInJson(invalidJson, jsonError);
                    invalidJson.put(INVALID_JSON, value.toString());
                } else {
                    //include CountryCode && prettyDate to output json
                    Utilities.includeDefaultPrettyDateInJson("ts", outputJson);
                    JSONObject propertiesJson = outputJson.getJSONObject("properties");
                    countryCodeLookupService.includeCountryCodeInJson(COUNTRY_CODE, propertiesJson);
                    Utilities.includeDefaultPrettyDateInJson("install_ts", propertiesJson);
                }
                //LOGGER.debug(String.format("%s --> %s", eventName, outputJson));
            } catch (JSONException e) {
                LOGGER.error(String.format("Error in json -> %s -> %s", e.getMessage(), value.toString()));
                // Append invalid event
                invalidJson = new JSONObject();
                invalidJson.put("event", INVALID_EVENT);
                eventName = INVALID_EVENT;
                jsonError = new JsonError(JsonSchemaValidation.JSON_EXCEPTION.toString(), e.getMessage());
                Utilities.includeErrorDataInJson(invalidJson, jsonError);
                invalidJson.put(INVALID_JSON, value.toString());
            }

            // Put all events on warehouse
            if (invalidJson == null) {
                context.write(new Text(eventName), new Text(StringUtils.remove(outputJson.toString(), "\\t")));
            } else {
                context.write(new Text(eventName), new Text(StringUtils.remove(invalidJson.toString(), "\\t")));
            }
        }
    }

    
    public static class Reduce extends
            Reducer<Text, Text, NullWritable, Text> {

        private MultipleOutputs<NullWritable, Text> multipleOutputs = null;
        private String outputBasePathFormat = null;

        @Override
        protected void setup(Context context) throws IOException {
            //setup multiple output
            multipleOutputs = new MultipleOutputs<NullWritable, Text>(context);
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
        }

        public void reduce(Text key, Iterable<Text> values,
                           Context context) throws IOException, InterruptedException {

            String eventName = key.toString();
            String outputPath = String.format(outputBasePathFormat, eventName);
            for (Text text : values) {
//            	if (!eventName.equals(INVALID_EVENT)) {
//                	// event name and ts
//                	JSONObject outputJson = new JSONObject(text.toString());
//                	int ts = outputJson.getInt("ts");
//                	String year = Utilities.getFormattedDateForTimestamp(ts, "yyyy");
//                	String month = Utilities.getFormattedDateForTimestamp(ts, "MM");
//                	String day = Utilities.getFormattedDateForTimestamp(ts, "dd");
//                	String hour = Utilities.getFormattedDateForTimestamp(ts, "HH");
//                	outputPath = outputPath.replaceFirst("year=\\d+/", "year=" + year + "/")
//                			.replaceFirst("month=\\d+/", "month=" + month + "/")
//                			.replaceFirst("day=\\d+/", "day=" + day + "/")
//                			.replaceFirst("hour=\\d+/", "hour=" + hour + "/");
//                }
                context.getCounter(context.getJobName(), eventName).increment(1);
                multipleOutputs.write(NullWritable.get(), text, outputPath);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            multipleOutputs.close();
        }
    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int rc = ToolRunner.run(conf, new EventsParser(), args);
        System.exit(rc);
    }


    @Override
    public int run(String[] args) throws Exception {
        Log.info("MapReduce job Started");

        try {

            String inputPath = args[0];
            String outputPath = args[1];
            String tempPath = args[2];
            String app = args[3];
            String specVersion = args[4];
            String s3SchemaBucket = args[5];
            String s3SchemaPath = args[6];

            LOGGER.info(String.format("inputPath(%s) outputBasePath(%s) tempPath(%s) app(%s) specVersion(%s) s3SchemaBucket(%s) s3SchemaPath(%s)",
                    inputPath, outputPath, tempPath, app, specVersion, s3SchemaBucket, s3SchemaPath));

            Configuration conf = super.getConf();
            conf.set(OUTPUT_BASE_PATH_FORMAT, outputPath);
            conf.set("app", app);
            conf.set("specVersion", specVersion);
            conf.set("s3SchemaBucket", s3SchemaBucket);
            conf.set("s3SchemaPath", s3SchemaPath);

            Job job = new Job(conf, "EventsParserJob");
            job.setJarByClass(EventsParser.class);

            job.setMapperClass(Map.class);
            job.setReducerClass(Reduce.class);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);

            SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK);
            SequenceFileOutputFormat.setCompressOutput(job, true);
            SequenceFileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(tempPath));
            Boolean result = job.waitForCompletion(true);

            Counters counters = job.getCounters();
            writeCounters(conf, counters, tempPath);

            return result ? 0 : 1;
        } finally {
            Log.info("MapReduce job Completed");
        }
    }

    public void writeCounters(Configuration conf, Counters counters, String rawTempPath) {

        String tempBucketName = rawTempPath.substring(0, StringUtils.ordinalIndexOf(rawTempPath, "/", 3));

        try {

            FileSystem tempFileSystem = FileSystem.get(new URI(tempBucketName),conf);
            FSDataOutputStream fsDataOutputStream = tempFileSystem.create(new Path(rawTempPath + "counters.txt"));
            PrintWriter writer = new PrintWriter(fsDataOutputStream);

            for (CounterGroup group : counters) {
                if (group.getDisplayName().equals("EventsParserJob")) {
                    for (Counter counter : group) {
                        writer.write(counter.getDisplayName() + "\t" + counter.getValue() + "\n");
                    }
                }
            }

            writer.close();
            fsDataOutputStream.close();

        } catch (Exception e) {
            Log.info("something is wrong in writeCounters!!");
        }
        return;
    }
}

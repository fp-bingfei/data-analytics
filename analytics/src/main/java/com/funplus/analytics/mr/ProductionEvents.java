package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.maxmind.geoip.LookupService;

import com.funplus.analytics.Utilities;


/*
 * Takes input from multiple locales and converts them to the desired TSV format
 */
public class ProductionEvents extends Configured implements Tool {
	
	private static final Logger LOGGER = Logger.getLogger(ProductionEvents.class);
	
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;

	// game keywords; if the locale argument is one of these, process within
	// the appropriate file system
	private final static String RSNAME = "royal";
	private final static String FARMNAME = "farm";
	private final static String POKERNAME = "poker";
	private final static String DAOTANAME = "daota";
	// keyword for using all locales in a game
	private final static String ALLLOCALES = "all";
	// list of locale names
	private final static String[] RSLOCALES = { "royal.ae.prod",
			"royal.de.prod", "royal.fr.prod", "royal.nl.prod", "royal.th.prod",
			"royal.spil.prod", "royal.us.prod", "royal.plinga.prod" };
	private final static String[] FARMLOCALES = { "farm.ae.prod",
			"farm.br.prod", "farm.de.prod", "farm.fr.prod", "farm.it.prod",
			"farm.nl.prod", "farm.pl.prod", "farm.th.prod", "farm.tr.prod",
			"farm.tw.prod", "farm.us.prod" };
	private final static String[] POKERLOCALES = { "pkr.ar.prod" };
	private final static String[] DAOTALOCALES = { "dtl.zh.prod" };
	private final static String[] KEYARRAY = new String[] { "@key", "@ts",
			"uid", "snsid", "install_ts", "install_src", "ip", "browser",
			"browser_version", "os", "os_version", "event" };
	// paths are [fluentdPath or pokerPath] + locale + DIRPATH1 + date
	// path to directory before locale for fluentd games: royal, farm
	private final static String fluentdPath = "s3://com.funplusgame.bidata/fluentd/";
	// path to directory before locale for poker
	private final static String pokerPath = "s3://com.funplus.poker/poker/events/";
	// path to directory before locale for daota
	private final static String daotaPath = "s3://com.funplus.daota/events/";
	// path to directory after locale before date
	private final static String DIRPATH1 = "/";
	// hardcoded path to geoip database
	private final static String GEOIPDATABASE = "GeoIP.dat";
	// sentinel value for invalid integers
	private final static int MISSEDINT = -1;
	// empty string for empty fields
	private final static String NULLSTRING = "";
	// tab character
	private final static char TAB = '\t';

	// output: tab-separated app ts uid snsid install_ts install_src
	// country_code ip browser browser_version os os_version name {properties}
	public static class Map extends
			Mapper<LongWritable, Text, IntWritable, Text> {
		
		public Random rng;
		public int numReducers;
		public IntWritable[] reducerKeys;
		
		// construct the random number generator and set the number of reducers
		public void setup(Context context) {
			rng = new Random();
			numReducers = context.getNumReduceTasks();
			reducerKeys = new IntWritable[numReducers];
			for (int i = 0; i < numReducers; i++) {
				reducerKeys[i] = new IntWritable(i);
			}
		}
		
		// doesn't use the LongWritable key
		// returns (random number from 0 to NUMSPLITS - 1, Text constructed from JSON)
		// or (-1, Text with invalid properties length)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			try {
				JSONObject jsonLine = new JSONObject(value.toString());
				ArrayList<String> tsvFields = getJSONFields(jsonLine);
				if (tsvFields != null) {
				    context.getCounter(context.getJobName(), tsvFields.get(tsvFields.size() - 2)).increment(1);
				    if (!tsvFields.get(tsvFields.size() - 2).equals("SpecialDelivery")) {
				        context.write(reducerKeys[rng.nextInt(numReducers)], new Text(makeSBLine(tsvFields).toString()));
				    }
				}
				else {
				    context.getCounter(context.getJobName(), "invalid").increment(1);
				}
			} catch(JSONException e) {
            	LOGGER.error(String.format("Invalid JSON - %s", value.toString()));
            }
		}

		private final static SimpleDateFormat dateFormatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");

		// get all of the relevant fields from the json and return a String[]
		// @key @ts uid snsid install_ts install_src country_code ip device os
		// os_version name {properties}
		// (properties = the json line without the previous fields)
		// returns null if missing all of app, uid, snsid, timestamp
		private static ArrayList<String> getJSONFields(JSONObject jsonLine) {
			// list of keys in the json
			JSONObject tempJSONLine = new JSONObject(jsonLine.toString());
			ArrayList<String> fieldKeys = new ArrayList<String>();
			fieldKeys.addAll(Arrays.asList(KEYARRAY));
			ArrayList<String> jsonFields = new ArrayList<String>();
			for (int i = 0; i < fieldKeys.size(); i++) {
				String key = fieldKeys.get(i);
				if (key.equals("uid")) {
					int uid = getJSONInt(jsonLine, key);
					if (uid == MISSEDINT) {
						return null;
					} else {
						jsonFields.add(Integer.toString(uid));
					}
				} else if (key.equals("install_ts") || key.equals("@ts")) {
					int ts = getJSONInt(jsonLine, key);
					if (ts == MISSEDINT) {
						if (key.equals("@ts")) {
							return null;
						}
						jsonFields.add("");
					} else {
						Date unixDate = new Date((long) ts * 1000);
						String tsvDate = dateFormatter.format(unixDate);
						jsonFields.add(tsvDate);
					}
				} else if (key.equals("ip")) {
					String ip = getJSONString(jsonLine, key);
					if (!ip.equals(NULLSTRING)) {
						try {
							LookupService ls = new LookupService(GEOIPDATABASE,
									LookupService.GEOIP_MEMORY_CACHE);
							jsonFields.add(ls.getCountry(ip).getCode());
						} catch (Exception e) {
							jsonFields.add("");
						}
						jsonFields.add(ip);
					} else {
						jsonFields.add("");
					}
				} else {
					String val = "";
					if (key.equals("install_src")) {
						val = getJSONString(jsonLine, "install_src");
						if (val.equals(NULLSTRING)) {
							val = getJSONString(jsonLine, "install_source");
						}
					} else {
						val = getJSONString(jsonLine, key);
						if (val.equals(NULLSTRING)) {
							if (key.equals("snsid") || key.equals("app")
									|| key.equals("event")) {
								return null;
							}
						}
					}
					jsonFields.add(val);
				}
				try {
					jsonLine.remove(key);
				} catch (Exception e) {
					System.out.println("didn't find " + key + " in line "
							+ tempJSONLine.toString());
				}
			}
			jsonFields.add(0, UUID.randomUUID().toString()); // add uuid to
																// beginning
			// run through remaining values in jsonLine, remove quotes
			Iterator<String> jsonKeyIter = jsonLine.keys();
			ArrayList<String> modifyKeys = new ArrayList<String>();
			while (jsonKeyIter.hasNext()) {
				String nextKey = jsonKeyIter.next();
				String nextString = jsonLine.optString(nextKey);
				if (!nextString.equals("") && nextString.indexOf("\"") != -1) {
					modifyKeys.add(nextKey);
				}
			}
			for (String key : modifyKeys) {
				String val = jsonLine.optString(key);
				val = val.replaceAll("\"", "");
				jsonLine.remove(key);
				jsonLine.put(key, val);
			}
			String jsonString = jsonLine.toString();
			jsonFields.add(jsonString); // add remaining values to the end
			return jsonFields;
		}

		// wrapper for getting a string from json, returns empty string on
		// failure
		private static String getJSONString(JSONObject jsonLine, String key) {
			return getJSONString(jsonLine, key, "");
		}

		private static String getJSONString(JSONObject jsonLine, String key,
				String errMessage) {
			String value;
			try {
				value = jsonLine.getString(key);
			} catch (Exception e) {
				System.out.println("missed a field:" + errMessage);
				value = NULLSTRING;
			}
			return value;
		}

		// wrapper for getting an int from json, returns a sentinel on failure
		private static int getJSONInt(JSONObject jsonLine, String key) {
			return getJSONInt(jsonLine, key, "");
		}

		private static int getJSONInt(JSONObject jsonLine, String key,
				String errMessage) {
			int value;
			try {
				value = jsonLine.getInt(key);
			} catch (Exception e) {
				System.out.println("missed an int:" + errMessage);
				value = MISSEDINT;
			}
			return value;
		}

		// generates the tab-delimited StringBuilder out of the fields
		private static StringBuilder makeSBLine(ArrayList<String> fields) {
			StringBuilder sb = new StringBuilder(fields.get(0));
			for (int i = 1; i < fields.size(); i++) {
				sb.append(TAB);
				sb.append(fields.get(i));
			}
			return sb;
		}
	}
	
	// outputs just the tab-delimited line
	public static class Reduce extends
			Reducer<IntWritable, Text, NullWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text t : values) {
				context.write(BLANK, t);
			}
		}
	}

	// construct the path out of the inputDateString, specific locale
	private Path makePath(String gamePath, String inputDateString, String locale) {
		return new Path(gamePath + locale + DIRPATH1 + inputDateString);
	}

	// make an arraylist of all locales for a game
	private ArrayList<Path> getAllLocaleInputs(String gamePath, String inputDateString,
			String[] gameLocales) {
		ArrayList<Path> inputPaths = new ArrayList<Path>();
		for (String locale : gameLocales) {
			inputPaths.add(makePath(gamePath, inputDateString, locale));
		}
		return inputPaths;
	}

	// return a path to the locale's data folder
	private ArrayList<Path> getInputPaths(String app, String date, String locale) {
		String inputDateString = makeInputDate(date);
		ArrayList<Path> inputPaths = new ArrayList<Path>();
		if (locale.equals(ALLLOCALES)) {
			if (app.equals(RSNAME)) {
				inputPaths = getAllLocaleInputs(fluentdPath, inputDateString, RSLOCALES);
			} else if (app.equals(FARMNAME)) {
				inputPaths = getAllLocaleInputs(fluentdPath, inputDateString, FARMLOCALES);
			} else if (app.equals(POKERNAME)) {
				inputPaths = getAllLocaleInputs(pokerPath, inputDateString, POKERLOCALES);
			} else if (app.equals(DAOTANAME)) {
				inputPaths = getAllLocaleInputs(daotaPath, inputDateString, DAOTALOCALES);
			}
		} else {
			if (app.equals(POKERNAME)) {
				inputPaths.add(makePath(pokerPath, inputDateString, locale));
			} else if (app.equals(DAOTANAME)) {
				inputPaths.add(makePath(daotaPath, inputDateString, locale));
			} else {
				inputPaths.add(makePath(fluentdPath, inputDateString, locale));
			}
		}
		return inputPaths;
	}

	// given a date in YYYYMMDDHH format, splits with slashes for the
	// appropriate file path
	private String makeInputDate(String inputTime) {
		String[] inputFields = new String[4];
		inputFields[0] = inputTime.substring(0, 4);
		inputFields[1] = inputTime.substring(4, 6);
		inputFields[2] = inputTime.substring(6, 8);
		inputFields[3] = inputTime.substring(8);
		String dateString = "";
		for (String field : inputFields) {
			dateString += field;
			dateString += '/';
		}
		return dateString;
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new ProductionEvents(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {
		Job job = new Job(super.getConf(), "EventsParserJob");
		job.setJarByClass(ProductionEvents.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		/*
		 * args[0]: output path args[1]: app name args[2]: [locale] args[3]:
		 * date as YYYYMMDDHH
		 */

		FileOutputFormat.setOutputPath(job, new Path(args[0]));

		ArrayList<Path> inputPaths;
		String app = args[1];
		String locale;
		String timestamp;
		if (args.length >= 3) {
			locale = args[2];
			timestamp = args[3];
		} else {
			locale = ALLLOCALES;
			timestamp = args[2];
		}
		inputPaths = getInputPaths(app, timestamp, locale);
		for (Path p : inputPaths) {
			FileInputFormat.addInputPath(job, p);
		}
		Boolean result = job.waitForCompletion(true);
		if (args[0].contains("hourly")){
		    String tempPath = args[0].replaceFirst("hourly", "scratch");
		    Utilities.writeCounters(job, tempPath);
		}

		return result ? 0 : 1;
	}

}

package com.funplus.analytics.mr;

import com.funplus.analytics.Utilities;
import com.funplus.analytics.mr.combined.CombinedFileWritable;
import com.funplus.analytics.mr.combined.CombinedInputFormat;
import com.maxmind.geoip.LookupService;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.CombineFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Takes input from multiple locales and converts them to the desired TSV format
 */
public class ProductionEventsDotaBackfill extends Configured implements Tool {
	private static final Logger LOGGER = Logger.getLogger(ProductionEventsDotaBackfill.class);
	private final static NullWritable BLANK = NullWritable.get();
	private static final String LOAD_HOUR = "load_hour";
	private static Configuration conf;

	private final static String[] KEYARRAY = new String[] { "@key", "@ts",
			"uid", "snsid", "install_ts", "install_src", "ip", "browser",
			"browser_version", "os", "os_version", "event" };
	// hardcoded path to geoip database
	private final static String GEOIPDATABASE = "GeoIP.dat";
	// sentinel value for invalid integers
	private final static int MISSEDINT = -1;
	// empty string for empty fields
	private final static String NULLSTRING = "";
	// tab character
	private final static char TAB = '\t';

	private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";


	// output: tab-separated app ts uid snsid install_ts install_src
	// country_code ip browser browser_version os os_version name {properties}
	public static class Map extends
			Mapper<LongWritable, Text, NullWritable, Text> {

		public Random rng;
		public int numReducers;
		public IntWritable[] reducerKeys;
		public String loadHour;

		private MultipleOutputs multipleOutputs = null;
		private String outputBasePathFormat = null;

		// construct the random number generator and set the number of reducers
		public void setup(Context context) {
			rng = new Random();
			numReducers = context.getNumReduceTasks();
			reducerKeys = new IntWritable[numReducers];
			for (int i = 0; i < numReducers; i++) {
				reducerKeys[i] = new IntWritable(i);
			}
			loadHour = context.getConfiguration().get(LOAD_HOUR);

			//setup multiple output
			multipleOutputs = new MultipleOutputs(context);
			outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
		}

		// doesn't use the LongWritable key
		// returns (random number from 0 to NUMSPLITS - 1, Text constructed from JSON)
		// or (-1, Text with invalid properties length)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			try {
				JSONObject jsonLine = new JSONObject(value.toString());
	
				String event = jsonLine.getString("event");
				if (event.equals("payment")) {
					Pattern pattern = Pattern.compile("\"transaction_id\":\\s?([^,\\s}]*)");
					Matcher matcher = pattern.matcher(value.toString());
					if (matcher.find()) {
						String transaction_id = matcher.group(1);
						jsonLine.remove("transaction_id");
						jsonLine.put("transaction_id", transaction_id);
					}
				}
	
				ArrayList<String> tsvFields = getJSONFields(jsonLine);
				// add load hour to beginning
				if (!loadHour.equals("ignore")) {
					tsvFields.add(0, loadHour);
				}
				if (tsvFields != null) {
					if (!tsvFields.get(tsvFields.size() - 2).equals("SpecialDelivery")) {
						String outputPath;
						if (!loadHour.equals("ignore")) {
							outputPath = generateOutputPath(tsvFields.get(3));
						} else {
							outputPath = generateOutputPath(tsvFields.get(2));
						}
						multipleOutputs.write(NullWritable.get(), new Text(makeSBLine(tsvFields).toString()), outputPath);
					}
				}
			} catch(JSONException e) {
            	LOGGER.error(String.format("Invalid JSON - %s", value.toString()));
            }
		}

		@Override
		protected void cleanup(Mapper.Context context) throws IOException, InterruptedException {
			multipleOutputs.close();
		}

		private String generateOutputPath(String timeStamp){
			StringBuilder builder = new StringBuilder(outputBasePathFormat);
			String year = "unknown";
			String day = "unknown";
			String month = "unknown";
			String hour = "unknown";
			try{
				year = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "yyyy");
				month = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "MM");
				day = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "dd");
				hour = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "HH");
			}
			catch (Exception e){
				System.out.print("error in ts : " + e.getMessage());
			}
			builder.append(year).append("/")
					.append(month).append("/")
					.append(day).append("/")
					.append(hour).append("/").append("part");
			return builder.toString();
		}


		// get all of the relevant fields from the json and return a String[]
		// @key @ts uid snsid install_ts install_src country_code ip device os
		// os_version name {properties}
		// (properties = the json line without the previous fields)
		// returns null if missing all of app, uid, snsid, timestamp
		private static ArrayList<String> getJSONFields(JSONObject jsonLine) {
			// list of keys in the json
			JSONObject tempJSONLine = new JSONObject(jsonLine.toString());
			ArrayList<String> fieldKeys = new ArrayList<String>();
			fieldKeys.addAll(Arrays.asList(KEYARRAY));
			ArrayList<String> jsonFields = new ArrayList<String>();
			for (int i = 0; i < fieldKeys.size(); i++) {
				String key = fieldKeys.get(i);
				if (key.equals("uid")) {
					int uid = getJSONInt(jsonLine, key);
					if (uid == MISSEDINT) {
						return null;
					} else {
						jsonFields.add(Integer.toString(uid));
					}
				} else if (key.equals("install_ts") || key.equals("@ts")) {
					int ts = getJSONInt(jsonLine, key);
					if (ts == MISSEDINT) {
						if (key.equals("@ts")) {
							return null;
						}
						jsonFields.add("");
					} else {
						Date unixDate = new Date((long) ts * 1000);
						SimpleDateFormat dateFormatter = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						String tsvDate = dateFormatter.format(unixDate);
						jsonFields.add(tsvDate);
					}
				} else if (key.equals("ip")) {
					String ip = getJSONString(jsonLine, key);
					if (!ip.equals(NULLSTRING)) {
						try {
							LookupService ls = new LookupService(GEOIPDATABASE,
									LookupService.GEOIP_MEMORY_CACHE);
							jsonFields.add(ls.getCountry(ip).getCode());
						} catch (Exception e) {
							jsonFields.add("");
						}
						jsonFields.add(ip);
					} else {
						jsonFields.add("");
						jsonFields.add("");
					}
				} else {
					String val = "";
					if (key.equals("install_src")) {
						val = getJSONString(jsonLine, "install_src");
						if (val.equals(NULLSTRING)) {
							val = getJSONString(jsonLine, "install_source");
						}
					} else {
						val = getJSONString(jsonLine, key);
						if (val.equals(NULLSTRING)) {
							if (key.equals("snsid") || key.equals("app")
									|| key.equals("event")) {
								return null;
							}
						}
					}
					jsonFields.add(val.replaceAll("\n", ""));
				}
				try {
					jsonLine.remove(key);
				} catch (Exception e) {
					System.out.println("didn't find " + key + " in line "
							+ tempJSONLine.toString());
				}
			}
			jsonFields.add(0, UUID.randomUUID().toString()); // add uuid to
																// beginning
			// run through remaining values in jsonLine, remove quotes
			Iterator<String> jsonKeyIter = jsonLine.keys();
			ArrayList<String> modifyKeys = new ArrayList<String>();
			while (jsonKeyIter.hasNext()) {
				String nextKey = jsonKeyIter.next();
				String nextString = jsonLine.optString(nextKey);
				if (!nextString.equals("") && nextString.indexOf("\"") != -1) {
					modifyKeys.add(nextKey);
				}
			}
			for (String key : modifyKeys) {
				String val = jsonLine.optString(key);
				val = val.replaceAll("\"", "");
				jsonLine.remove(key);
				jsonLine.put(key, val);
			}
			String jsonString = jsonLine.toString();
			if (jsonString.length() > 19000) {
				return null;
			}
			jsonFields.add(jsonString); // add remaining values to the end
			return jsonFields;
		}

		// wrapper for getting a string from json, returns empty string on
		// failure
		private static String getJSONString(JSONObject jsonLine, String key) {
			return getJSONString(jsonLine, key, key);
		}

		private static String getJSONString(JSONObject jsonLine, String key,
											String errMessage) {
			String value;
			try {
				value = jsonLine.getString(key);
			} catch (Exception e) {
				System.out.println("missed a field: " + errMessage);
				value = NULLSTRING;
			}
			return value;
		}

		// wrapper for getting an int from json, returns a sentinel on failure
		private static int getJSONInt(JSONObject jsonLine, String key) {
			return getJSONInt(jsonLine, key, "");
		}

		private static int getJSONInt(JSONObject jsonLine, String key,
									  String errMessage) {
			int value;
			try {
				value = jsonLine.getInt(key);
			} catch (Exception e) {
				System.out.println("missed an int:" + errMessage);
				value = MISSEDINT;
			}
			return value;
		}

		// generates the tab-delimited StringBuilder out of the fields
		private static StringBuilder makeSBLine(ArrayList<String> fields) {
			StringBuilder sb = new StringBuilder(fields.get(0));
			for (int i = 1; i < fields.size(); i++) {
				sb.append(TAB);
				sb.append(fields.get(i));
			}
			return sb;
		}
	}

	// outputs just the tab-delimited line
	/*public static class Reduce extends
			Reducer<IntWritable, Text, NullWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text t : values) {
				context.write(BLANK, t);
			}
		}
	}*/

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new ProductionEventsDotaBackfill(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {

		conf.set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
		conf.set(OUTPUT_BASE_PATH_FORMAT, args[2]);
		conf.set("mapred.max.split.size", "64000000");
		// Set load hour
		String loadHour = args[3];
        conf.set(LOAD_HOUR, loadHour);

		Job job = new Job(super.getConf());
		job.setJarByClass(ProductionEventsDotaBackfill.class);

		job.setMapperClass(Map.class);
		//job.setReducerClass(Reduce.class);
		job.setNumReduceTasks(0);

		job.setMapOutputKeyClass(NullWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		//job.setOutputFormatClass(TextOutputFormat.class);
		LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);


		TextOutputFormat.setCompressOutput(job, true);
		TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

}

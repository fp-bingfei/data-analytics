package com.funplus.analytics.mr.combined;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.CombineFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.CombineFileRecordReader;
import org.apache.hadoop.mapreduce.lib.input.CombineFileSplit;

import java.io.IOException;

/**
 * Created by balaji on 12/12/14.
 */

public class CombinedInputFormat extends CombineFileInputFormat<CombinedFileWritable, Text> {

    @Override
    public RecordReader<CombinedFileWritable,Text> createRecordReader(InputSplit split,
                       TaskAttemptContext context) throws IOException {
        return new  CombineFileRecordReader<CombinedFileWritable, Text>((CombineFileSplit)split, context,
                CombinedFileRecordReader.class);
    }

    public CombinedInputFormat(){
        super();
        setMaxSplitSize(16000000); // 64 MB, default block size on hadoop
    }


    @Override
    protected boolean isSplitable(JobContext context, Path file){
        return false;
    }

}

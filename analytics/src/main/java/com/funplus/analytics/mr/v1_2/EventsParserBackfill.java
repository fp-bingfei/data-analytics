package com.funplus.analytics.mr.v1_2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedPartitioner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.log.Log;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import com.funplus.analytics.CountryCodeLookupService;
import com.funplus.analytics.Utilities;
import com.funplus.analytics.validation.JsonError;
import com.funplus.analytics.validation.JsonSchemaValidation;
import com.funplus.analytics.validation.JsonSchemaValidatorFactory;

/**
 * Created by Zhenxuan on 03/04/2015.
 */

public class EventsParserBackfill extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(EventsParserBackfill.class);

    private static final String INVALID_EVENT = "invalid";
    private static final String INVALID_JSON = "invalidJson";
    private static final String COUNTRY_CODE = "country_code";
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";
    private static final String OUTPUT_DATE = "outputDate";
    private static final String DEFAULT_SCHEMA = "default";

    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {

        private JsonSchemaValidatorFactory jsonSchemaValidatorFactory = null;
        private CountryCodeLookupService  countryCodeLookupService = null;
        private String specVersion = null;
        private String outputBasePathFormat = null;
        private String outputDate = null;
        private String inputPathString = null;

        @Override
        protected void setup(Context context) throws IOException {

            //setup factory
            jsonSchemaValidatorFactory = JsonSchemaValidatorFactory.getInstance();
            jsonSchemaValidatorFactory.bootstrap(context.getConfiguration());

            //setup Country code service
            countryCodeLookupService = CountryCodeLookupService.getInstance();

            specVersion = context.getConfiguration().get("specVersion");
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
            outputDate = context.getConfiguration().get(OUTPUT_DATE); 

            inputPathString = ((FileSplit) context.getInputSplit()).getPath().toString();
            
        }

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            JSONObject outputJson = null, invalidJson = null;
            JsonError jsonError = null;
            String eventName = null;
            String schemaValidatorKey = null;
            try {
                outputJson = new JSONObject(value.toString());

                try {
                    eventName = outputJson.getString("event");
                    schemaValidatorKey = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, eventName);
                    String defaultSchema = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, DEFAULT_SCHEMA);
                    jsonError = jsonSchemaValidatorFactory.validate(schemaValidatorKey, value.toString(), defaultSchema);
                } catch (Exception e) {
                    //do nothing
                    jsonError = new JsonError(JsonSchemaValidation.PROCESSING_ERROR.toString(), e.getMessage());
                }
                //check for error if available the input json is invalid
                if (null != jsonError) {
                    // Append invalid event
                	invalidJson = new JSONObject();
                	if (eventName != null) {
                		invalidJson.put("event", eventName);
                	} else {
                		invalidJson.put("event", INVALID_EVENT);
                	}
                	eventName = INVALID_EVENT;
                    Utilities.includeErrorDataInJson(invalidJson, jsonError);
                    invalidJson.put(INVALID_JSON, value.toString());
                } else {
                    //include CountryCode && prettyDate to output json
                    Utilities.includeDefaultPrettyDateInJson("ts", outputJson);
                    JSONObject propertiesJson = outputJson.getJSONObject("properties");
                    countryCodeLookupService.includeCountryCodeInJson(COUNTRY_CODE, propertiesJson);
                    Utilities.includeDefaultPrettyDateInJson("install_ts", propertiesJson);
                }
                //LOGGER.debug(String.format("%s --> %s", eventName, outputJson));
            } catch (JSONException e) {
                LOGGER.error(String.format("Error in json -> %s -> %s", e.getMessage(), value.toString()));
                // Append invalid event
                invalidJson = new JSONObject();
                invalidJson.put("event", INVALID_EVENT);
                eventName = INVALID_EVENT;
                jsonError = new JsonError(JsonSchemaValidation.JSON_EXCEPTION.toString(), e.getMessage());
                Utilities.includeErrorDataInJson(invalidJson, jsonError);
                invalidJson.put(INVALID_JSON, value.toString());
            }

            // Put all events on warehouse
            if (invalidJson == null) {
            	// generate map key
                String mapKey = generateMapKey(eventName, outputJson);
                String outputPath = new StringBuilder(outputBasePathFormat)
                        .append(mapKey).toString();
                context.write(new Text(outputPath), new Text(StringUtils.remove(outputJson.toString(), "\\t")));
            } else {
                String outputPath = new StringBuilder(outputBasePathFormat)
                		.append(eventName).append(outputDate).toString();
                context.write(new Text(outputPath), new Text(StringUtils.remove(invalidJson.toString(), "\\t")));
            }
        }

        private String generateMapKey(String eventName, JSONObject outputJson){
            StringBuilder builder = new StringBuilder(eventName).append("/")
                    .append("app=").append(outputJson.getString("app_id")).append("/");
            
            String[] inputPathInfo = null;
            String hour = "unknown";
            String day = "unknown";
            String month = "unknown";
            String year = "unknown";
            
            try {
            	inputPathInfo = inputPathString.split("/");
            	hour = inputPathInfo[inputPathInfo.length - 2];
            	day = inputPathInfo[inputPathInfo.length - 3];
            	month = inputPathInfo[inputPathInfo.length - 4];
            	year = inputPathInfo[inputPathInfo.length - 5];
            } catch (Exception e) {
            	//do nothing for now
            }
            
            builder.append("year=").append(year).append("/")
            		.append("month=").append(month).append("/")
            		.append("day=").append(day).append("/")
            		.append("hour=").append(hour).append("/").append("part");
            
            return builder.toString(); 

        }
    }

    
    public static class Reduce extends
            Reducer<Text, Text, NullWritable, Text> {

        private MultipleOutputs<NullWritable, Text> multipleOutputs = null;
        private String outputBasePathFormat = null;

        @Override
        protected void setup(Context context) throws IOException {
            //setup multiple output
            multipleOutputs = new MultipleOutputs<NullWritable, Text>(context);
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
        }

        public void reduce(Text key, Iterable<Text> values,
                           Context context) throws IOException, InterruptedException {

            String outputPath = key.toString();
            for (Text text : values) {
                multipleOutputs.write(NullWritable.get(), text, outputPath);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            multipleOutputs.close();
        }

    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int rc = ToolRunner.run(conf, new EventsParserBackfill(), args);
        System.exit(rc);
    }


    @Override
    public int run(String[] args) throws Exception {
        Log.info("MapReduce job Started");

        try {

            String inputPath = args[0];
            String outputPath = args[1];
            String outputDate = args[2];
            String tempPath = args[3];
            String app = args[4];
            String specVersion = args[5];
            String s3SchemaBucket = args[6];
            String s3SchemaPath = args[7];

            LOGGER.info(String.format("inputPath(%s) outputBasePath(%s) outputDate(%s) tempPath(%s) app(%s) specVersion(%s) s3SchemaBucket(%s) s3SchemaPath(%s)",
                    inputPath, outputPath, outputDate, tempPath, app, specVersion, s3SchemaBucket, s3SchemaPath));

            Configuration conf = super.getConf();
            conf.set(OUTPUT_BASE_PATH_FORMAT, outputPath);
            conf.set(OUTPUT_DATE, outputDate);
            conf.set("app", app);
            conf.set("specVersion", specVersion);
            conf.set("s3SchemaBucket", s3SchemaBucket);
            conf.set("s3SchemaPath", s3SchemaPath);

            Job job = new Job(conf, "EventsParserBackfillJob");
            job.setJarByClass(EventsParserBackfill.class);

            job.setMapperClass(Map.class);
            job.setReducerClass(Reduce.class);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
 
            SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK);
            SequenceFileOutputFormat.setCompressOutput(job, true);
            SequenceFileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);
            
            String refinedInputPaths = getInputPath(conf, inputPath, tempPath);
            if (refinedInputPaths.length() == 0) {
            	return 0;
            }
            FileInputFormat.addInputPaths(job, refinedInputPaths);
            
            tempPath = tempPath + "/backfill";
            FileOutputFormat.setOutputPath(job, new Path(tempPath));

            return job.waitForCompletion(true) ? 0 : 1;
        } finally {
            Log.info("MapReduce job Completed");
        }
    }
    
    public String getInputPath(Configuration conf, String rawInputPath, String rawTempPath) {
		Log.info("In reducing input");

		FileSystem inputFileSystem = null;
		FileSystem tempFileSystem = null;
		FileStatus[] inputStatuses = null;
		FileStatus[] tempStatuses = null;
		String inputBucketName = rawInputPath.substring(0,StringUtils.ordinalIndexOf(rawInputPath, "/", 3));
		String tempBucketName = rawTempPath.substring(0, StringUtils.ordinalIndexOf(rawTempPath, "/", 3));
		
		List<Path> rawInputPathList = new ArrayList<Path>();
		rawInputPathList.add(new Path(rawInputPath));
		Path[] inputPathList = rawInputPathList.toArray((new Path[rawInputPathList.size()])) ;
		
		List<Path> rawTempPathList = new ArrayList<Path>();
		rawTempPathList.add(new Path(rawTempPath));
		Path[] tempPathList = rawTempPathList.toArray((new Path[rawTempPathList.size()])) ;
		
		StringBuilder refinedInputPaths = new StringBuilder();

		try {
			inputFileSystem = FileSystem.get(new URI(inputBucketName), conf);
			tempFileSystem = FileSystem.get(new URI(tempBucketName), conf);
			inputStatuses = inputFileSystem.listStatus(inputPathList);
			tempStatuses = tempFileSystem.listStatus(tempPathList);
			
			String[] inputPaths = new String[inputStatuses.length];
			List<String> inputHour = new ArrayList<String>();

			for (int i = 0; i < inputStatuses.length; i++) {
				inputPaths[i] = inputStatuses[i].getPath().toString();
				String[] inputInfo = inputPaths[i].split("/");
				inputHour.add(inputInfo[inputInfo.length - 1]);
			}
			
			String[] tempPaths = new String[tempStatuses.length];
			List<String> tempHour = new ArrayList<String>();

			for (int i = 0; i < tempStatuses.length; i++){
				tempPaths[i] = tempStatuses[i].getPath().toString();
				String[] tempInfo = tempPaths[i].split("=");
				tempHour.add(tempInfo[tempInfo.length - 1]);
			}

			List<String> newHours = new ArrayList<String>(inputHour); 
			newHours.removeAll(tempHour);
			
			if (newHours.size() > 0) {
				for (String newHour : newHours) {
					int index = inputHour.indexOf(newHour);
					refinedInputPaths.append(inputPaths[index].toString()).append(",");
				}
				refinedInputPaths.deleteCharAt(refinedInputPaths.length()-1);
			}
		} catch (URISyntaxException e) {
			throw new RuntimeException(String.format("URISyntaxException - %s", e.getMessage()));
		} catch (IOException e){
			// throw new RuntimeException(String.format("IOException - %s", e.getMessage()));
			Log.info(String.format("IOException - %s", e.getMessage()));
			for (FileStatus inputStatus : inputStatuses) {
				refinedInputPaths.append(inputStatus.getPath().toString()).append(",");
			}
			refinedInputPaths.deleteCharAt(refinedInputPaths.length()-1);
			return refinedInputPaths.toString();
			} finally {
			try {
				inputFileSystem.close();
				tempFileSystem.close();
			} catch(Exception e) {
				//what to do?
			}
		}
		return refinedInputPaths.toString();
	}
  
}
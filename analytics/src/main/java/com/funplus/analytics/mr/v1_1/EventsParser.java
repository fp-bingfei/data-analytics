package com.funplus.analytics.mr.v1_1;

import com.funplus.analytics.CountryCodeLookupService;
import com.funplus.analytics.Utilities;
import com.funplus.analytics.validation.JsonError;
import com.funplus.analytics.validation.JsonSchemaValidation;
import com.funplus.analytics.validation.JsonSchemaValidatorFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.partition.KeyFieldBasedPartitioner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.mortbay.log.Log;

import java.io.*;

/**
 * Created by balaji on 10/16/14.
 */

public class EventsParser extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(EventsParser.class);

    private static final String INVALID_EVENT = "invalid";
    private static final String COUNTRY_CODE = "country_code";
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";
    private static final String DEFAULT_SCHEMA = "default";


    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {

        private JsonSchemaValidatorFactory jsonSchemaValidatorFactory = null;
        private CountryCodeLookupService  countryCodeLookupService = null;
        private String specVersion = null;

        @Override
        protected void setup(Context context) throws IOException {

            //setup factory
            jsonSchemaValidatorFactory = JsonSchemaValidatorFactory.getInstance();
            jsonSchemaValidatorFactory.bootstrap(context.getConfiguration());

            //setup Country code service
            countryCodeLookupService = CountryCodeLookupService.getInstance();

            specVersion = context.getConfiguration().get("specVersion");


        }


        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            JSONObject outputJson = null;
            JsonError jsonError = null;
            String eventName = null;
            String schemaValidatorKey = null;
            try{
                outputJson = new JSONObject(value.toString());
            }
            catch (Exception e){
                LOGGER.error(String.format("Error in json -> %s -> %s", e.getMessage(), value.toString()));
                e.printStackTrace();
                throw new InterruptedIOException(e.getMessage());
            }
            try {
                eventName = outputJson.getString("event");
                schemaValidatorKey = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, eventName);
                String defaultSchema = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, DEFAULT_SCHEMA);
                jsonError = jsonSchemaValidatorFactory.validate(schemaValidatorKey, value.toString(), defaultSchema);
            } catch (Exception e) {
                //do nothing
                jsonError = new JsonError(JsonSchemaValidation.PROCESSING_ERROR.toString(), e.getMessage());
            }
            //check for error if available the input json is invalid
            if (null != jsonError) {
                eventName = INVALID_EVENT;
                Utilities.includeErrorDataInJson(outputJson, jsonError);
            }
            else {
                //include CountryCode && prettyDate to output json
                countryCodeLookupService.includeCountryCodeInJson(COUNTRY_CODE, outputJson);
                Utilities.includeDefaultPrettyDateInJson("@ts", outputJson);
                Utilities.includeDefaultPrettyDateInJson("install_ts", outputJson);
            }

           
            context.write(new Text(eventName), new Text(StringUtils.remove(outputJson.toString(), "\\t")));
            //LOGGER.debug(String.format("%s --> %s", eventName, outputJson));
        }


    }


    public static class Reduce extends
            Reducer<Text, Text, NullWritable, Text> {

        private MultipleOutputs multipleOutputs = null;
        private String outputBasePathFormat = null;

        @Override
        protected void setup(Context context) throws IOException {
            //setup multiple output
            multipleOutputs = new MultipleOutputs(context);
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
        }

        public void reduce(Text key, Iterable<Text> values,
                           Context context) throws IOException, InterruptedException {

            String eventName = key.toString();
            String outputPath = String.format(outputBasePathFormat, eventName);
            for (Text text : values) {
                context.getCounter(context.getJobName(), eventName).increment(1);
                multipleOutputs.write( NullWritable.get(), text, outputPath);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            multipleOutputs.close();
        }

    }

    public static class CustomPartitioner extends KeyFieldBasedPartitioner<Text, Text> {

        @Override
        public int getPartition(Text key, Text value, int numReduceTasks) {
           int distributionKey = key.toString().hashCode() % numReduceTasks;
           int numDistribution = getConf().getInt("numDistribution", 0);
            if(numDistribution != 0){
                distributionKey = (int) (Math.random() * numDistribution);
            }
           return Math.abs(distributionKey);
        }
    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int rc = ToolRunner.run(conf, new EventsParser(), args);
        System.exit(rc);
    }


    @Override
    public int run(String[] args) throws Exception {
        Log.info("MapReduce job Started");

        try {

            String inputPath = args[0];
            String outputPath = args[1];
            String tempPath = args[2];
            String app = args[3];
            String specVersion = args[4];
            String s3SchemaBucket = args[5];
            String s3SchemaPath = args[6];

            LOGGER.info(String.format("inputPath(%s) outputBasePath(%s) tempPath(%s) app(%s) specVersion(%s) s3SchemaBucket(%s) s3SchemaPath(%s)",
                    inputPath, outputPath, tempPath, app, specVersion, s3SchemaBucket, s3SchemaPath));

            Configuration conf = super.getConf();
            conf.set(OUTPUT_BASE_PATH_FORMAT, outputPath);
            conf.set("app", app);
            conf.set("specVersion", specVersion);
            conf.set("s3SchemaBucket", s3SchemaBucket);
            conf.set("s3SchemaPath", s3SchemaPath);
            if(args.length == 8){
                conf.set("numDistribution", args[7]);
            }

            Job job = new Job(conf, "EventsParserJob");
            job.setJarByClass(EventsParser.class);

            job.setMapperClass(Map.class);
            job.setReducerClass(Reduce.class);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            LazyOutputFormat.setOutputFormatClass(job, SequenceFileOutputFormat.class);


            SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
            SequenceFileOutputFormat.setCompressOutput(job, true);
            SequenceFileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(tempPath));

            Boolean result = job.waitForCompletion(true);

            Utilities.writeCounters(job , tempPath);

            return result ? 0 : 1;
        } finally {
            Log.info("MapReduce job Completed");
        }

    }
}

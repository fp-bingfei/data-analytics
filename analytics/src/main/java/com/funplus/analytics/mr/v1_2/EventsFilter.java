package com.funplus.analytics.mr.v1_2;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.log.Log;

/**
 * Created by chunzeng on 02/03/2015.
 */

public class EventsFilter extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(EventsFilter.class);

    private static final String FILTER_KEY = "filterKey";
    private static final String FILTER_VALUE = "filterValue";
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";

    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {
    	
    	private String filterKey = null;
    	private String filterValue = null;

    	@Override
        protected void setup(Context context) throws IOException {
    		filterKey = context.getConfiguration().get(FILTER_KEY);
    		filterValue = context.getConfiguration().get(FILTER_VALUE);
        }
    	
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            JSONObject outputJson = null;
            String eventName = null;
            try {
                outputJson = new JSONObject(value.toString());
                eventName = outputJson.getString(filterKey);
                if (eventName.equals(filterValue)) {
		            context.write(new Text(eventName), new Text(StringUtils.remove(outputJson.toString(), "\\t")));
		            //LOGGER.debug(String.format("%s --> %s", eventName, outputJson));
                }
	        } catch (Exception e) {
                LOGGER.error(String.format("Error in json -> %s -> %s", e.getMessage(), value.toString()));
            }
        }
    }

    
    public static class Reduce extends
            Reducer<Text, Text, NullWritable, Text> {

        private MultipleOutputs<NullWritable, Text> multipleOutputs = null;
        private String outputBasePathFormat = null;

        @Override
        protected void setup(Context context) throws IOException {
            //setup multiple output
            multipleOutputs = new MultipleOutputs<NullWritable, Text>(context);
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
        }

        public void reduce(Text key, Iterable<Text> values,
                           Context context) throws IOException, InterruptedException {

            String eventName = key.toString();
            String outputPath = String.format(outputBasePathFormat, eventName);
            for (Text text : values) {
                //context.getCounter(context.getJobName(), eventName).increment(1);
                multipleOutputs.write(NullWritable.get(), text, outputPath);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            multipleOutputs.close();
        }

    }


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int rc = ToolRunner.run(conf, new EventsFilter(), args);
        System.exit(rc);
    }


    @SuppressWarnings("deprecation")
	@Override
    public int run(String[] args) throws Exception {
        Log.info("MapReduce job Started");

        try {

            String inputPath = args[0];
            String outputPath = args[1];
            String tempPath = args[2];
            String filterKey = args[3];
            String filterValue = args[4];

            LOGGER.info(String.format("inputPath(%s) outputBasePath(%s) tempPath(%s) filterKey(%s) filterValue(%s)",
                    inputPath, outputPath, tempPath, filterKey, filterValue));

            Configuration conf = super.getConf();
            conf.set(OUTPUT_BASE_PATH_FORMAT, outputPath);
            conf.set(FILTER_KEY, filterKey);
            conf.set(FILTER_VALUE, filterValue);

            Job job = new Job(conf, "EventsFilterJob");
            job.setJarByClass(EventsFilter.class);

            job.setMapperClass(Map.class);
            job.setReducerClass(Reduce.class);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
 
            SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK);
            SequenceFileOutputFormat.setCompressOutput(job, true);
            SequenceFileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(tempPath));

            return job.waitForCompletion(true) ? 0 : 1;
        } finally {
            Log.info("MapReduce job Completed");
        }

    }
}

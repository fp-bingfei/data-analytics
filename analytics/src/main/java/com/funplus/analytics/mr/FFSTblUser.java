package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.json.JSONObject;

import com.maxmind.geoip.LookupService;

/*
 * Takes input from multiple locales and converts them to the desired TSV format
 */
public class FFSTblUser extends Configured implements Tool {
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;

	private final static String[] KEYARRAY = new String[] { "@key", "@ts",
			"uid", "snsid", "install_ts", "install_src", "ip", "browser",
			"browser_version", "os", "os_version", "event" };
	// hardcoded path to geoip database
	private final static String GEOIPDATABASE = "GeoIP.dat";
	// sentinel value for invalid integers
	private final static int MISSEDINT = -1;
	// empty string for empty fields
	private final static String NULLSTRING = "";
	// tab character
	private final static char TAB = '\t';
	
	 

	// output: tab-separated app ts uid snsid install_ts install_src
	// country_code ip browser browser_version os os_version name {properties}
	public static class Map extends
			Mapper<LongWritable, Text, IntWritable, Text> {
		
		public Random rng;
		public int numReducers;
		public IntWritable[] reducerKeys;
		
		// construct the random number generator and set the number of reducers
		public void setup(Context context) {
			rng = new Random();
			numReducers = context.getNumReduceTasks();
			reducerKeys = new IntWritable[numReducers];
			for (int i = 0; i < numReducers; i++) {
				reducerKeys[i] = new IntWritable(i);
			}
		}
		
		// doesn't use the LongWritable key
		// returns (random number from 0 to NUMSPLITS - 1, Text constructed from JSON)
		// or (-1, Text with invalid properties length)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			
			if (value.toString().indexOf("snsid")!= -1) {
				return;
			}
			String[] data = (value.toString()).split("\t");
			if (data.length != 39) {
				return;
			}
			
			ArrayList<String> tsvFields = new ArrayList<String>();
			//app
			tsvFields.add("ffs.global.prod");
			//uid
			tsvFields.add(data[0]);
			//snsid
			tsvFields.add(data[1]);
			//install_ts
			tsvFields.add(data[17]);
			//trackref
			tsvFields.add(data[25]);
			//os
			if (data[30] != null & !data[30].equals("")) {
				String os = isAppleDevice(data[30]) ? "ios" : "android";
				tsvFields.add(os);
			}
			else {
				tsvFields.add("");
			}
			//system_version
			tsvFields.add(data[31]);
			//device_model
			tsvFields.add(data[30]);
			//level
			tsvFields.add(data[3]);
			//language
			tsvFields.add(data[33]);
			
			
			//uid     snsid   email   level   experience      coins   reward_points   op      gas     power   last_recover_time       lottery_coins   size_x  size_y  top_map_size    max_work_area_size      
			//work_area_size  addtime logintime       loginip status  continuous_day  point   love_points     feed_status     track_ref 25       extra_info      username        pic     
			//display_id      device_model30    system_version  app_version_number      language        matchmaking     removed order_points    max_batch_num   product
			
			if (tsvFields != null) {
				context.write(reducerKeys[rng.nextInt(numReducers)], new Text(makeSBLine(tsvFields).toString()));
			}
			
		}

		private static boolean isAppleDevice(String device) {
			if (device.equals("x86_64")) {
				return true;
			}
			if (device.length() < 4) {
				return false;
			}
			if (device.length()>=4) {
				String subs = device.substring(0, 4);
				if (subs.equals("iPad") || subs.equals("iPod")) {
					return true;
				}
			}
			if (device.length()>=6) {
				String subs = device.substring(0, 6);
				if (subs.equals("iPhone")) {
					return true;
				}
			}
			return false;
		}
		
		// generates the tab-delimited StringBuilder out of the fields
		private static StringBuilder makeSBLine(ArrayList<String> fields) {
			StringBuilder sb = new StringBuilder(fields.get(0));
			for (int i = 1; i < fields.size(); i++) {
				sb.append(TAB);
				sb.append(fields.get(i));
			}
			return sb;
		}
	}
	
	// outputs just the tab-delimited line
	public static class Reduce extends
			Reducer<IntWritable, Text, NullWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text t : values) {
				context.write(BLANK, t);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new FFSTblUser(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {
		Job job = new Job(super.getConf());
		job.setJarByClass(FFSTblUser.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		TextOutputFormat.setCompressOutput(job, true);
		TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

}

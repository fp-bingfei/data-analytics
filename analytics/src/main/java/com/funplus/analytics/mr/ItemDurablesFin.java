package com.funplus.analytics.mr;


import java.io.IOException;
import java.util.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.util.*;


public class ItemDurablesFin extends Configured implements Tool {

    public static class globalVars {

        private String amt;
        private String uid;

        public String getAmt() {
            return amt;
        }

        public void setAmt(String amt) {
            this.amt = amt;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }
    }

    //todo:not sure whether it is right functionality
    public static String createPaymentRec(String rec, String pAmt) {

        int rc_get=0;
        int rc_spend=0;
        int rc_left=0;

        double amt=0.00;
        double pAmt_bal=0.00;
        double amt_bal=0.00;
        double amt_dec=0.00;
        double avg_cost=0.00;

        String[] pymtRec = rec.split("\t");
        ArrayList newRec = new ArrayList(Arrays.asList(pymtRec));

        rc_left = Integer.parseInt(pymtRec[8]);
        rc_get = Integer.parseInt(pymtRec[6]);
        amt = Double.parseDouble(pymtRec[10]); //usd_amt

        pAmt_bal = Double.parseDouble(pAmt);
        amt_bal = amt + pAmt_bal - amt_dec;

        if ((rc_left+rc_get)==0)
        {
            avg_cost = 0.0;
        } else
        {
            avg_cost = amt_bal / (rc_left + rc_get);
        }

        DecimalFormat f = new DecimalFormat("##.##");
        newRec.add(f.format(amt_dec));
        newRec.add(f.format(amt_bal));
        newRec.add(f.format(avg_cost));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long ts = Long.parseLong(newRec.get(2).toString());
        Date d = new Date(ts * 1000);
        newRec.set(2, sdf.format(d));

        if (!newRec.get(5).toString().equals("")) {
            long ins_ts = Long.parseLong(newRec.get(5).toString());
            Date d1 = new Date(ins_ts * 1000);
            newRec.set(5, sdf.format(d1));
        }

        newRec.set(8, rc_left + rc_get);
        newRec.set(13, newRec.get(13).toString().toLowerCase());

        newRec.remove(0);

        String result0 = newRec.toString().replaceAll(",\\s+", ",");
        String result = result0.substring(1, result0.length()-1);

        return result;
    }

    //todo:not sure whether it is right functionality
    public static String createRCRec(String rec, String pAmt) {
        String[] rcRec = rec.split("\t");
        ArrayList<String> newrcRec = new ArrayList(Arrays.asList(rcRec));

        int rc_get=0;
        int rc_spend=0;
        int rc_left=0;
        double amt=0.0;
        double pAmt_bal=0.0;
        double amt_bal = 0.0;
        double amt_dec=0.0;
        double avg_cost=0.0;

        if (rcRec[6].toString().equals("0"))
        {
            //RC spend or rc_cost;
            rc_spend = Integer.parseInt(rcRec[7].toString());
            rc_left = Integer.parseInt(rcRec[8].toString());

            pAmt_bal = Double.parseDouble(pAmt.toString());
            amt_dec = rc_spend*(pAmt_bal/(rc_left+rc_spend));
            amt_bal = pAmt_bal - amt_dec;
            if (rc_left==0)
            {
                avg_cost = 0.0;
            } else
            {
                avg_cost = amt_bal / rc_left;
            }
        }
        else
        {
            // RC get
            rc_left = Integer.parseInt(rcRec[8]);
            rc_get = Integer.parseInt(rcRec[6]);
            amt_dec = 0.0;
			/* if (rc_left == rc_get)
			{
				amt_bal = 0.0;
			} else {
				//amt_bal = (Double.parseDouble(pAmt)) + (rc_get * ( Double.parseDouble(pAmt) / (rc_left - rc_get)));
				amt_bal = (Double.parseDouble(pAmt));
			}
				*/
            amt_bal = (Double.parseDouble(pAmt));

            if (rc_left==0)
            {

                avg_cost = 0.0;
            } else {
                avg_cost = amt_bal / rc_left;
            }
        }

        DecimalFormat f = new DecimalFormat("##.##");
        newrcRec.add(f.format(amt_dec));
        newrcRec.add(f.format(amt_bal));
        newrcRec.add(f.format(avg_cost));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long ts = Long.parseLong(newrcRec.get(2).toString());
        Date d = new Date(ts * 1000);
        newrcRec.set(2, sdf.format(d));

        if (!newrcRec.get(5).toString().equals("")) {
            long ins_ts = Long.parseLong(newrcRec.get(5).toString());
            Date d1 = new Date(ins_ts * 1000);
            newrcRec.set(5, sdf.format(d1));
        }

        newrcRec.remove(0);

        String result0 = newrcRec.toString().replaceAll(",\\s+", ",");
        String result = result0.substring(1, result0.length()-1);

        return result;
    }

	public static String createDurableStats(String rec) throws ParseException
	{
		int item_in=0;
		double item_cost=0.0;
		int ap=0;
		
		int reportDays = 0;
		int daysUsed = 0;
		double qtyUsed = 0;
		double valueUsed = 0.0;
		double qtyRemaining = 0.0;
		double valueRemaining = 0.0;
		
		String[] itemDurableRec = rec.split("\t");
		ArrayList newRec = new ArrayList(Arrays.asList(itemDurableRec));
		
		item_in = Integer.parseInt(itemDurableRec[7]);
		
		item_cost = Integer.parseInt(itemDurableRec[11]);
		ap = Integer.parseInt(itemDurableRec[15]);
		
		//Days used;
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		long ts = Long.parseLong(itemDurableRec[3]);
		Date eventDate = new Date(ts*1000);
		Date today = new Date();
		long datediff = today.getTime() - eventDate.getTime();
		reportDays = (int)(datediff/(1000 * 60 * 60 * 24));
		daysUsed = reportDays;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		newRec.set(3, sdf.format(eventDate));

		//qtyUsed;
		if(ap==0) {
			qtyUsed = 0;
		} else {
			//qtyUsed = item_in * (daysUsed/ap);
			qtyUsed = (item_in*daysUsed/ap);
		}
		
		//valueUsed;
		if(item_in==0) {
			valueUsed = 0;
		} else {
			//valueUsed = (item_cost/item_in) * qtyUsed;
			valueUsed = (item_cost * qtyUsed/item_in) ;
		}
		
		//qtyRemaining;
		if(ap==0) {
			qtyRemaining = 0;
		} else {
			//qtyRemaining = item_in * (1 - daysUsed/ap);
			qtyRemaining = item_in - (item_in*daysUsed/ap);
		}
		
		//valueRemaining;
		if(item_in==0) {
			valueRemaining = 0;
		} else {
			//valueRemaining = (item_cost/item_in) * qtyRemaining;
			valueRemaining = (item_cost * qtyRemaining/item_in);
		}		
		
		DecimalFormat f = new DecimalFormat("##.##");
		newRec.add(f.format(qtyUsed));
		newRec.add(f.format(valueUsed));
		newRec.add(f.format(qtyRemaining));
		newRec.add(f.format(valueRemaining));

		String result0 = newRec.toString().replaceAll(",\\s+", ",");
		String result = result0.substring(1, result0.length()-1);
		
		return result;
	}
	
	public static String createDurableStats_item_out(String rec) throws ParseException
	{
		int item_out=0;
		double item_cost=0.0;
		int ap=0;
		
		int reportDays = 0;
		int daysUsed = 0;
		double qtyUsed = 0;
		double valueUsed = 0.0;
		double qtyRemaining = 0.0;
		double valueRemaining = 0.0;
		
		String[] itemDurableRec = rec.split("\t");
		ArrayList newRec = new ArrayList(Arrays.asList(itemDurableRec));
		
		item_out = Integer.parseInt(itemDurableRec[8]);
		
		item_cost = Integer.parseInt(itemDurableRec[9]);
		ap = Integer.parseInt(itemDurableRec[15]);
		
		//Days used;
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		long ts = Long.parseLong(itemDurableRec[3]);
		Date eventDate = new Date(ts*1000);
		Date today = new Date();
		long datediff = today.getTime() - eventDate.getTime();
		reportDays = (int)(datediff/(1000 * 60 * 60 * 24));
		daysUsed = reportDays;
		//System.out.println(daysUsed);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		newRec.set(3, sdf.format(eventDate));

		//qtyUsed;
		if(ap==0) {
			qtyUsed = 0;
		} else {
			//qtyUsed = item_in * (daysUsed/ap);
			qtyUsed = (item_out*daysUsed/ap);
		}
		
		//valueUsed;
		if(item_out==0) {
			valueUsed = 0;
		} else {
			//valueUsed = (item_cost/item_in) * qtyUsed;
			valueUsed = (item_cost * qtyUsed/item_out) ;
		}
		
		//qtyRemaining;
		if(ap==0) {
			qtyRemaining = 0;
		} else {
			//qtyRemaining = item_in * (1 - daysUsed/ap);
			qtyRemaining = item_out - (item_out*daysUsed/ap);
		}
		
		//valueRemaining;
		if(item_out==0) {
			valueRemaining = 0;
		} else {
			//valueRemaining = (item_cost/item_in) * qtyRemaining;
			valueRemaining = (item_cost * qtyRemaining/item_out);
		}		
		
		DecimalFormat f = new DecimalFormat("##.##");
		newRec.add(f.format(qtyUsed));
		newRec.add(f.format(valueUsed));
		newRec.add(f.format(qtyRemaining));
		newRec.add(f.format(valueRemaining));

		String result0 = newRec.toString().replaceAll(",\\s+", ",");
		String result = result0.substring(1, result0.length()-1);
		
		return result;
	}


	public static class RCMap extends Mapper<LongWritable, Text, NullWritable, Text> {

		public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException{

			String line = new String();

			line = value.toString();

			if(line.length() > 1) {
				context.write(NullWritable.get(), new Text(line));
			}
		}
	}

	public static class RCReduce extends Reducer<NullWritable, Text, NullWritable, Text> {

		public void reduce(NullWritable key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {

				Text reduceStr0 = new Text();
				globalVars pVars = new globalVars();

				String rcEvent = new String();
				String pymtEvent = new String();
				String[] eventRec = new String[14];

				Iterator<Text> iterator = values.iterator();
				while (iterator.hasNext()) {
					reduceStr0 = iterator.next();
					String reduceStr = reduceStr0.toString();
					eventRec = reduceStr.split("\t");
					String cUid = eventRec[3];
				
					if(!pVars.getUid().equals(cUid))
					{
						pVars.setAmt("0");
					}

					if (eventRec[13].toString().equals("Payment"))
					{
						pymtEvent = createPaymentRec(reduceStr, pVars.getAmt());
						String[] new_pymtEvent = pymtEvent.split(",");
						pVars.setAmt(new_pymtEvent[14]);
					}
					else if (eventRec[13].toString().equals("rc_transaction"))
					{
						if(eventRec[6].toString().equals("0")) // rc_get = 0
						{
							rcEvent = createRCRec(reduceStr, pVars.getAmt());
							String[] new_rcEvent = rcEvent.split(",");
							pVars.setAmt(new_rcEvent[14]);
						}
						else
						{   
							//rc_spend = 0
							rcEvent = createRCRec(reduceStr, pVars.getAmt());
							String[] new_rcEvent = rcEvent.split(",");
							pVars.setAmt(new_rcEvent[14]);
						}
					}

					pVars.setUid(cUid);
				if(eventRec[13].toString().equals("rc_transaction"))
				{
					context.write(NullWritable.get(), new Text(rcEvent));
					//context.write(NullWritable.get(), new Text(reduceStr));
				}else{
					context.write(NullWritable.get(), new Text(pymtEvent));
					//context.write(NullWritable.get(), new Text(reduceStr));
				}
				}
			}
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new RanchCashFin(), args);

	}

	@Override
		public int run(String[] args) throws Exception {

			// When implementing tool
			Configuration conf = this.getConf();

			// Create job
			Job job = new Job(conf, "ranchcashfin");
			job.setJarByClass(RanchCashFin.class);


			FileInputFormat.addInputPath(job, new Path(args[0]));
			job.setInputFormatClass(TextInputFormat.class);
			FileOutputFormat.setOutputPath(job, new Path(args[1]));        
			job.setOutputFormatClass(TextOutputFormat.class);

			job.setMapperClass(RanchCashFin.RCMap.class);
			job.setReducerClass(RanchCashFin.RCReduce.class);

			// Specify key / value
			//job.setOutputKeyClass(LongWritable.class);
			//job.setOutputValueClass(Text.class);

			// Mapper Class output
			job.setMapOutputKeyClass(NullWritable.class);
			job.setMapOutputValueClass(Text.class);
			//Reducer Class Output
			job.setOutputKeyClass(NullWritable.class);
			job.setOutputValueClass(Text.class);

			// Execute job and return status
			return job.waitForCompletion(true) ? 0 : 1;
		} 
}


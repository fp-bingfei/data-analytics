package com.funplus.analytics.mr.v1_2;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.log.Log;

import parquet.avro.AvroParquetOutputFormat;
import parquet.hadoop.metadata.CompressionCodecName;

import com.funplus.analytics.CountryCodeLookupService;
import com.funplus.analytics.Utilities;
import com.funplus.analytics.validation.JsonError;
import com.funplus.analytics.validation.JsonSchemaValidation;
import com.funplus.analytics.validation.JsonSchemaValidatorFactory;

/**
 * Created by chunzeng on 02/06/2015.
 */

public class WriteParquetFromJson extends Configured implements Tool {
	
	private static final Logger LOGGER = Logger.getLogger(WriteParquetFromJson.class);
	
	private static final String INVALID_EVENT = "invalid";
    private static final String COUNTRY_CODE = "country_code";
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";
    private static final String DEFAULT_SCHEMA = "v1_2_default";
    private static final String AVRO_OUTPUT_SCHEMA = "schema_path";
    
	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		
		private JsonSchemaValidatorFactory jsonSchemaValidatorFactory = null;
        private CountryCodeLookupService  countryCodeLookupService = null;
        private String specVersion = null;
		
		@Override
		protected void setup(Context context) throws IOException {
            //setup factory
            jsonSchemaValidatorFactory = JsonSchemaValidatorFactory.getInstance();
            jsonSchemaValidatorFactory.bootstrap(context.getConfiguration());

            //setup Country code service
            countryCodeLookupService = CountryCodeLookupService.getInstance();

            specVersion = context.getConfiguration().get("specVersion");
        }
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            JSONObject outputJson = null;
            JsonError jsonError = null;
            String eventName = null;
            String schemaValidatorKey = null;
            try {
                outputJson = new JSONObject(value.toString());
                
	            try {
	                eventName = outputJson.getString("event");
	                schemaValidatorKey = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, eventName);
	                jsonError = jsonSchemaValidatorFactory.validate(schemaValidatorKey, value.toString(), DEFAULT_SCHEMA);
	            } catch (Exception e) {
	                //do nothing
	                jsonError = new JsonError(JsonSchemaValidation.PROCESSING_ERROR.toString(), e.getMessage());
	            }
	            //check for error if available the input json is invalid
	            if (null != jsonError) {
	                eventName = INVALID_EVENT;
	                Utilities.includeErrorDataInJson(outputJson, jsonError);
	            } else {
	                //include CountryCode && prettyDate to output json
	                countryCodeLookupService.includeCountryCodeInJson(COUNTRY_CODE, outputJson);
	                Utilities.includeDefaultPrettyDateInJson("@ts", outputJson);
	                Utilities.includeDefaultPrettyDateInJson("install_ts", outputJson);
	            }
	            
	            context.write(new Text(eventName), new Text(StringUtils.remove(outputJson.toString(), "\\t")));
	            //LOGGER.debug(String.format("%s --> %s", eventName, outputJson));
	        } catch (JSONException e) {
                LOGGER.error(String.format("Error in json -> %s -> %s", e.getMessage(), value.toString()));
            }
        }
	}
	
	public static class Reduce extends
    	Reducer<Text, Text, Void, GenericRecord> {
		
		private MultipleOutputs<Void, GenericRecord> multipleOutputs = null;
		private String outputBasePathFormat = null;
		private Schema avroSchema = null;
		
		@Override
		protected void setup(Context context) throws IOException {
		    //setup multiple output
		    multipleOutputs = new MultipleOutputs<Void, GenericRecord>(context);
		    outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
		    
		    try {
				String schemaPath = context.getConfiguration().get(AVRO_OUTPUT_SCHEMA);
				FileSystem fs = FileSystem.get(new URI(schemaPath), context.getConfiguration());
			    InputStream in = fs.open(new Path(schemaPath));
			    avroSchema = new Schema.Parser().parse(in);
			    fs.close();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		@Override
		public void reduce(Text key, Iterable<Text> values,
		                   Context context) throws IOException, InterruptedException {
		    String eventName = key.toString();
		    String outputPath = String.format(outputBasePathFormat, eventName);
		    for (Text text : values) {
		    	try {
		    		// create record
			        JSONObject outputJson = new JSONObject(text.toString());
			        GenericRecord datum = new GenericData.Record(avroSchema);
			        // invalid events
//			        try {
//				        if (outputJson.get("error") != null) {
//				        	datum.put("error", outputJson.get("error").toString());
//				        	if (outputJson.get("errorDetails") != null) {
//				        		datum.put("errorDetails", outputJson.get("errorDetails").toString());
//				        	}
//				        }
//			        } catch (JSONException e) {
//			        	// ignore
//			        }
			        datum.put("bi_version", outputJson.get("bi_version").toString());
			        datum.put("app_id", outputJson.get("app_id").toString());
			        try {
			        	datum.put("ts", outputJson.getInt("ts"));
			        } catch (JSONException e) {
			        	// set current time
			        	datum.put("ts", new Date().getTime());
			        }
	                datum.put("event", outputJson.get("event").toString());
	                datum.put("user_id", outputJson.get("user_id").toString());
	                datum.put("session_id", outputJson.get("session_id").toString());
	                // properties
	                JSONObject propertiesJson = outputJson.getJSONObject("properties");
	                if (propertiesJson != null) {
	                	String[] names = JSONObject.getNames(propertiesJson);
		                HashMap<String, String> propertiesMap = new HashMap<String, String>(names.length);
		                for (int i = 0; i < names.length; i++) {
		                	if (names[i].length() > 0) {
		                		propertiesMap.put(names[i], propertiesJson.get(names[i]).toString());
		                	}
		                }
		                datum.put("properties", propertiesMap);
	                }
	                // collections
	                JSONObject collectionsJson = outputJson.getJSONObject("collections");
	                if (collectionsJson != null) {
	                	String[] fieldNames = JSONObject.getNames(collectionsJson);
		                HashMap<String, ArrayList<HashMap<String, String>>> collectionsMap = 
		                		new HashMap<String, ArrayList<HashMap<String, String>>>(fieldNames.length);
		                for (int i = 0; i < fieldNames.length; i++) {
		                	if (fieldNames[i].length() > 0) {
			                	JSONArray jsonArray = collectionsJson.getJSONArray(fieldNames[i]);
			                	ArrayList<HashMap<String, String>> fieldArray = 
			                			new ArrayList<HashMap<String, String>>(jsonArray.length());
			                	for (int j = 0; j < jsonArray.length(); j++) {
			                		JSONObject itemJson = jsonArray.getJSONObject(j);
			                		String[] itemNames = JSONObject.getNames(itemJson);
			                		for (int k = 0; k < itemNames.length; k++) {
			                			if (itemNames[k].length() > 0) {
				                			HashMap<String, String> item = new HashMap<String, String>();
				                			item.put(itemNames[k], itemJson.get(itemNames[k]).toString());
				                			fieldArray.add(item);
			                			}
			                		}
			                	}
			                	collectionsMap.put(fieldNames[i], fieldArray);
		                	}
		                }
		                datum.put("collections", collectionsMap);
	                }
	                
			        multipleOutputs.write((Void)null, datum, outputPath);
		    	} catch (Exception e) {
	                //LOGGER.error(String.format("Error in json -> %s -> %s", e.getMessage(), text.toString()));
		    		throw new RuntimeException(e);
	            }
		    }
		}
		
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
		    multipleOutputs.close();
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		Configuration conf = new Configuration();
		int rc = ToolRunner.run(conf, new WriteParquetFromJson(), args);
		System.exit(rc);
	}
	
	@Override
	public int run(String[] args) throws Exception {
		Log.info("MapReduce job Started");

        try {
        	String inputPath = args[0];
            String outputPath = args[1];
            String tempPath = args[2];
            String schemaPath = args[3];	// path to Avro schema file (.avsc)
            String app = args[4];
            String specVersion = args[5];
            String s3SchemaBucket = args[6];
            String s3SchemaPath = args[7];
            
            LOGGER.info(String.format("inputPath(%s) outputBasePath(%s) tempPath(%s) schemaPath(%s) app(%s) specVersion(%s) s3SchemaBucket(%s) s3SchemaPath(%s)",
                    inputPath, outputPath, tempPath, schemaPath, app, specVersion, s3SchemaBucket, s3SchemaPath));
            
	        Job job = new Job(getConf(), "WriteParquetFromJson");
	        job.setJarByClass(getClass());
	        Configuration conf = job.getConfiguration();
	        conf.set(OUTPUT_BASE_PATH_FORMAT, outputPath);
	        conf.set(AVRO_OUTPUT_SCHEMA, schemaPath);
            conf.set("app", app);
            conf.set("specVersion", specVersion);
            conf.set("s3SchemaBucket", s3SchemaBucket);
            conf.set("s3SchemaPath", s3SchemaPath);
            
	        // read in the Avro schema
	        FileSystem fs = FileSystem.get(new URI(schemaPath), conf);
	        InputStream in = fs.open(new Path(schemaPath));
	        Schema avroSchema = new Schema.Parser().parse(in);
	        fs.close();
	        
	        job.setMapperClass(Map.class);
	        job.setReducerClass(Reduce.class);
	        
	        job.setMapOutputKeyClass(Text.class);
	        job.setMapOutputValueClass(Text.class);
	        job.setOutputKeyClass(NullWritable.class);
	        job.setOutputValueClass(GenericRecord.class);
	        
	        // point to input data
	        FileInputFormat.addInputPath(job, new Path(inputPath));
	        job.setInputFormatClass(TextInputFormat.class);
	        
	        // set the output format
	        job.setOutputFormatClass(AvroParquetOutputFormat.class);
	        AvroParquetOutputFormat.setOutputPath(job, new Path(tempPath));
	        AvroParquetOutputFormat.setSchema(job, avroSchema);
	        AvroParquetOutputFormat.setCompression(job, CompressionCodecName.SNAPPY);
	        AvroParquetOutputFormat.setCompressOutput(job, true);
	        
	        // set a large block size to ensure a single row group.
	        AvroParquetOutputFormat.setBlockSize(job, 256 * 1024 * 1024);
	        
	        return job.waitForCompletion(true) ? 0 : 1;
        } finally {
            Log.info("MapReduce job Completed");
        }
	}
}

package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;

import org.json.JSONObject;
import com.maxmind.geoip.LookupService;

/*
 * Takes input from multiple locales and converts them to the desired TSV format
 */
public class RSUserToTSVMulti extends Configured implements Tool {
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;

	private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	// list of locale names
	private final static String[] LOCALES = {"ae","de", "fr", "nl", "th", "us"};
	// path to directory before locale
	private final static String DIRPATH0 = "s3://com.funplusgame.bidata/etl/rs/";
	// path to directory after locale
	private final static String DIRPATH1 = "/mongodb/user/";
	// index of locale on split filepath string array
	private final static int LOCALEINDEX = 5;
	// hardcoded path to geoip database
	private final static String GEOIPDATABASE = "GeoIP.dat";
	private final static int MISSEDINT = -1; // sentinel value for invalid integers

	// empty string for empty fields
	private final static String NULLSTRING = "NULL";

	// tab character
	private final static char TAB = '	';
	
	public static class Map extends
			Mapper<LongWritable, Text, NullWritable, Text> {

		// doesn't use the LongWritable
		// returns (BLANK , Text constructed from JSON)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

			Path filePath = ((FileSplit) context.getInputSplit()).getPath();
			// System.out.println(filePath.toString() + " is the filepath");
			String[] splitPath = filePath.toString().split("/");
			String line = value.toString();
			JSONObject jsonLine = new JSONObject(line);
			String app, uid, snsid, ip, install_source, install_tsString, os, os_version, countryCode, levelString, device, language, cashString, coinsString, email, gender, name, age;
			int level, install_ts, cash, coins;
			
			String localeName = splitPath[LOCALEINDEX];
			app = "royal."+localeName+".prod";
			uid = "0";
			snsid = getJsonString(jsonLine, "uid", "uid");
			install_source = getJsonString(jsonLine, "ref");
			email = getJsonString(jsonLine, "email");
                        gender = getJsonString(jsonLine, "gender");
                        name = getJsonString(jsonLine, "first_name");
			age = "";
			if (install_source.length() > 100) {
				install_source = install_source.substring(0, 100);
			}
			while (install_source.indexOf('\\') != -1) {
				install_source = install_source.substring(0, install_source.length() - 2);
			}
			install_ts = getJsonInt(jsonLine, "create_time");
			if (install_ts != MISSEDINT) {
				Date tsDate = new Date((long) install_ts * 1000);
				install_tsString = dateFormatter.format(tsDate);
				// install_tsString = Integer.toString(install_ts);
			} else {
				install_tsString = NULLSTRING;
			}
			
			// no fields for os, os_version
			os = NULLSTRING;
			os_version = NULLSTRING;
			
			ip = getJsonString(jsonLine, "last_ip", "ip");
			if (!ip.equals(NULLSTRING)) {
				LookupService ls = new LookupService(GEOIPDATABASE,
						LookupService.GEOIP_MEMORY_CACHE);
				countryCode = ls.getCountry(ip).getCode();
			} else {
				countryCode = NULLSTRING;
			}
			level = getJsonInt(jsonLine, "level");
			if (level != MISSEDINT) {
				levelString = Integer.toString(level);
			} else {
				levelString = NULLSTRING; 
			}
				//cash RC	
			cash = getJsonInt(jsonLine, "cash");
			if (cash != MISSEDINT) {
				cashString = Integer.toString(cash);
			} else {
				cashString = NULLSTRING; 
			}

			coins = getJsonInt(jsonLine, "coins");
			if (coins != MISSEDINT) {
				coinsString = Integer.toString(coins);
			} else {
				coinsString = NULLSTRING; 
			}

			// no field for device
			device = NULLSTRING;
			language = getJsonString(jsonLine, "lang");

			StringBuilder newline = makeSBLine(app, uid, snsid, install_source, install_tsString, os, os_version, countryCode, levelString, device, language, cashString, coinsString, email, gender, name, age);
			context.write(BLANK, new Text(newline.toString()));
		}
	}
	
	// wrapper for getting a string from json, returns empty string on failure
	private static String getJsonString(JSONObject jsonLine, String key) {
		return getJsonString(jsonLine, key, "");
	}

	private static String getJsonString(JSONObject jsonLine, String key,
			String errMessage) {
		String value;
		try {
			value = jsonLine.getString(key);
		} catch (Exception e) {
			System.out.println("missed a field:" + errMessage);
			value = NULLSTRING;
		}
		return value;
	}
	
	// wrapper for getting an int from json, returns a sentinel on failure
	private static int getJsonInt(JSONObject jsonLine, String key) {
		return getJsonInt(jsonLine, key, "");
	}
	
	private static int getJsonInt(JSONObject jsonLine, String key,
			String errMessage) {
		int value;
		try {
			value = jsonLine.getInt(key);
		} catch (Exception e) {
			System.out.println("missed an int:" + errMessage);
			value = MISSEDINT;
		}
		return value;
	}
	
	// generates the tab-delimited StringBuilder out of the fields
	private static StringBuilder makeSBLine (String... fields) {
		StringBuilder sb = new StringBuilder(fields[0]);
		for (int i = 1; i < fields.length; i++) {
			sb.append(TAB);
			sb.append(fields[i]);
		}
		return sb;
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new RSUserToTSVMulti(), args);
		System.exit(rc);
	}

	private Path[] getInputPaths(String arg) {
		Path[] inputPaths = new Path[LOCALES.length];
		for (int i = 0; i < LOCALES.length; i++) {
			inputPaths[i] = new Path(DIRPATH0 + LOCALES[i] + DIRPATH1 + arg);
		}
		return inputPaths;
	}
	
	@Override
	public int run(String[] args) throws Exception {
		@SuppressWarnings("deprecation")
		Job job = new Job(super.getConf());
		job.setJarByClass(RSUserToTSVMulti.class);

		job.setMapperClass(Map.class);
		// job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(NullWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		for (Path p : getInputPaths(args[0])) {
			FileInputFormat.addInputPath(job, p);
		}
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

}

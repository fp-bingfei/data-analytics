package com.funplus.analytics.mr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import org.json.JSONArray;
import org.json.JSONObject;

public class KochavaProcessing {
	// file location for where to stage the data on EC2
	private final static String STAGING0 = "/mnt/data/jars/kochava_staging/";
	// first part of the kochava API URL, before the start/end dates
	private final static String kochavaURL0 = "http://control.kochava.com/v1/cpi/get_installs?";
	// second part of the URL, with all other fields
	private final static String kochavaURL1 = "&rtn_device_id_type=android_id,android_md5,android_sha1,imei,imei_md5,imei_sha1,udid,udid_md5,udid_sha1,idfa,idfv,Kochava_device_id,odin,mac&kochava_app_id=";
	private final static String kochavaURL2 = "&api_key=7EB29A90-DA76-4300-A055-8A56C83BED43&account=ummair.waheed&rtn_custom_id=fpid";
	// list of all the relevant keys in the JSON
	private final static String[] objectKeys = { "campaign_id", "campaign_name", "click_date", "country_code", "creative", "install_date", "network_name", "site_id", "android_id","android_md5","android_sha1","imei","imei_md5","imei_sha1","udid","udid_md5","udid_sha1","idfa","idfv","Kochava_device_id","odin","mac", "fpid" };
	
	public static void main(String[] args) {
		// String appID = args[2];
		// need to implement passing in multiple app id's for different games
		// also, make one output .gz per id
		// and move everything into a single folder, 
		String startDate = args[0];
		String endDate = args[1];
		String gameName = args[2];
		// String appID = args[2];
		// need to implement passing in multiple app id's for different games
		// also, make one output .gz per id
		// and move everything into a single folder, appropriate for a directory redshiftcopy
		// ffs global android: koffsandroidglobal183752d4f76e0e31d
		// dt global android: kodt-zh-android53cd4597a4060
		// dt_zh global android: kodt-zh53bafba8da48e
		// url: http://control.kochava.com/v1/cpi/get_installs?start_date=08-08-2014&end_date=08-08-2014&rtn_device_id_type=android_id,android_md5,android_sha1,imei,imei_md5,imei_sha1,udid,udid_md5,udid_sha1,idfa,idfv,Kochava_device_id,odin,mac&kochava_app_id=koffsandroidglobal183752d4f76e0e31d&api_key=7EB29A90-DA76-4300-A055-8A56C83BED43&account=ummair.waheed&rtn_custom_id=fpid
		for (int argAppID = 3; argAppID < args.length; argAppID++) {
			String appID = args[argAppID];
			String filePath = makeFilePath(STAGING0, gameName, startDate);
			String kochavaURL = kochavaURL0 + "start_date=" + makeDashedDate(startDate) + "&end_date=" + makeDashedDate(endDate) + kochavaURL1 + appID + kochavaURL2;
			String fileName = startDate + "_" + appID + "_kochava.gz";
			File dir = new File(filePath);
			dir.mkdirs();
			File f = new File(filePath + fileName);
			try {
				GZIPOutputStream gzipWriter = new GZIPOutputStream(new FileOutputStream(f, false));
				JSONArray kochavaJSON = readURLJSON(kochavaURL);
				for (int i = 0; i < kochavaJSON.length(); i++) {
					JSONObject obj = kochavaJSON.getJSONObject(i);
					String TSVLine = makeTSV(obj);
					TSVLine = UUID.randomUUID() + "\t" + TSVLine;
					for (char TSVChar : TSVLine.toCharArray()) {
						gzipWriter.write((int) TSVChar);
					}
				}
				gzipWriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public static String makeFilePath(String... elements) {
		String path = "";
		for (String e : elements) {
			path += e;
			path += "/";
		}
		return path;
	}
	public static String makeTSV(JSONObject obj) {
		String TSVLine = obj.optString(objectKeys[0]);
		for (int i = 1; i < objectKeys.length; i++) {
			TSVLine += '\t';
			TSVLine += obj.optString(objectKeys[i]);
		}
		TSVLine += '\n';
		return TSVLine;
	}
	public static String makeDashedDate(String date) {
		String DASH = "-";
		String year = date.substring(0,4);
		String month = date.substring(4,6);
		String day = date.substring(6,8);
		return month + DASH + day + DASH + year;
	}
	// read the text at a URL into a JSON object
	public static JSONArray readURLJSON(String URLName) throws Exception {
		URL kochavaURL = new URL(URLName);
		InputStream kochavaStream = kochavaURL.openStream();
		int nextCharInt = kochavaStream.read();
		StringBuilder fileChars = new StringBuilder(); 
		while (nextCharInt != -1) {
			char nextChar = (char)(nextCharInt);
			if (nextChar != '\n') {
				fileChars.append(nextChar);
			}
			nextCharInt = kochavaStream.read();
		}
		return new JSONArray(fileChars.toString());
	}
}

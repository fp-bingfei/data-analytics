package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;

import org.json.JSONObject;
import org.json.JSONException;

import com.maxmind.geoip.LookupService;

public class MRDates extends Configured implements Tool {
	// private final static IntWritable ONE = new IntWritable(1);
	private final static NullWritable BLANK = NullWritable.get();
	
	// hardcoded local path to the GeoIP database
	//private final static String GEOIPDATABASE = "s3://com.funplusgame.emr/job/GeoIP.dat";
	//private final static String GEOIPDATABASE = "~/mr_jobs/lib/GeoIP.dat";
	//private final static String GEOIPDATABASE = "home/hadoop/mr_jobs/lib/GeoIP.dat";
	private final static String GEOIPDATABASE = "GeoIP.dat";
	
	public static class Map extends Mapper<LongWritable, Text, NullWritable, Text> {

		// doesn't use the LongWritable
		// returns (Null, Text constructed from JSON)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			String fileName = ((FileSplit) context.getInputSplit()).getPath()
					.getName();
			// System.out.println("i think the filename is " + fileName);
			String line = value.toString();
			// System.out.println("processing: ***" + line + "***");
			if (line.trim().equals("")) {
				return;
			}
			String[] tokens = line.split(",", 3);

			String gameName = tokens[0];
			String timestamp = tokens[1];
			// System.out.println("game name: " + gameName + "    timestamp: " + timestamp);
			JSONObject jsonLine = null;

			// get the country code
			try {
				jsonLine = new JSONObject(tokens[2]);
				jsonLine.put("@key", gameName);
				jsonLine.put("@ts", timestamp);
				String ipString = jsonLine.getString("ip");
				LookupService ls = new LookupService(GEOIPDATABASE,
						LookupService.GEOIP_MEMORY_CACHE);
				String countryCode = ls.getCountry(ipString).getCode();
				jsonLine.put("country", countryCode);
			} catch (JSONException e) {
				System.out.println("error in the line ***" + jsonLine.toString() + "***");
			}

			// get the time and convert it
			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			try {
				Date addtime = date.parse(jsonLine.getString("addtime"));
				jsonLine.put("addtime", addtime.getTime() / 1000);
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}

			// get the event from the filename, convert it to lower case, and
			// append it
			// assumes the filename is in the format "[stuff][digit]_[event]_[digit][stuff]"
			String[] eventTokens = fileName.split("[\\d_][_\\d]");
			String event = eventTokens[1];
			event = event.toLowerCase();

			try {
				jsonLine.put("event", event);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			context.write(BLANK, new Text(jsonLine.toString()));
		}
	}

	public static class Reduce extends Reducer<NullWritable, Text, NullWritable, Text> {

		// output just the json line
		public void reduce(NullWritable key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			for (Text val : values) {
				context.write(BLANK, val);
			}
		}
	}

	public static void main(String[] args) throws Exception {
    for (String str : args) {
      System.out.println(str);
    }
		Configuration conf = new Configuration();
		int rc = ToolRunner.run(conf, new MRDates(), args);
		System.exit(rc);

	}

	@Override
	public int run(String[] args) throws Exception {
		
		@SuppressWarnings("deprecation")
		Job job = new Job(this.getConf(), "MRDates");
		job.setJarByClass(MRDates.class);
		

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(NullWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}
}

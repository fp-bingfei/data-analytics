package com.funplus.analytics.mr.v1_1;

import java.io.IOException;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.log.Log;

import com.funplus.analytics.Utilities;


/**
 * Created by chunzeng on 12/24/2014.
 */

public class ProductionEventsBackfill extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(ProductionEventsBackfill.class);

    // sentinel value for invalid integers
    private final static String MISSED = "-1";
    // blank key
    private final static NullWritable BLANK = NullWritable.get();
    private static Configuration conf;

    private final static String[] OUTPUT_FIELDS = new String[] { "id", "@key", "@ts",
            "uid", "snsid", "install_ts", "install_source", "country_code", "ip", "browser",
            "browser_version", "os", "os_version", "event", "properties" };
    
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";

    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            try {
            	JSONObject jsonObject = new JSONObject(value.toString());
            
	            //rename incorrect json keys to match spec for downstream processing 
	            jsonObject = preProcessJSON(jsonObject);
	            
	            if(validateInputData(jsonObject)){
	                String[] outputFields = getOutputFields(jsonObject, OUTPUT_FIELDS);
	
	                //set properties field value - if > 5000, remove action_detail attribute
	                if (jsonObject.toString().length() > 5000) {
	                	if (jsonObject.has("action_detail")) {
	                		jsonObject.remove("action_detail");
	                	}
	                }
	                outputFields[OUTPUT_FIELDS.length - 1] = jsonObject.toString();
	                //write to context
	                // map key is hourly output path
	                String mapKey = generateOutputPath(outputFields[2]);
	                context.write(new Text(mapKey), new Text(StringUtils.join(outputFields, '\t')));
	            }
	            else{
	                LOGGER.warn(String.format("Validation failed for input data - %s", jsonObject));
	            }
            }
            catch(JSONException e) {
            	LOGGER.error(String.format("Invalid JSON - %s", value.toString()));
            }
        }

        /**
         * Construct the String array for the given output fields using input json object
         * @param jsonObject
         * @param outputFieldNames
         * @return
         */
        private String[] getOutputFields(JSONObject jsonObject, String[] outputFieldNames) {

            String[] outputArray = new String[outputFieldNames.length];
            int columnIndex = 0;
            for (String outputFieldName : outputFieldNames) {
                try {
                    // handle special cases for fields - ps, @ts, country_code, properties
                    if ("id".equals(outputFieldName)) {
                        outputArray[columnIndex] = UUID.randomUUID().toString();
                    } else if ("@ts".equals(outputFieldName) || "install_ts".equals(outputFieldName)) {
                        int timeStamp = jsonObject.getInt(outputFieldName);
                        outputArray[columnIndex] = String.valueOf(Utilities.getDefaultFormattedDateForTimestamp(timeStamp));
                    } else if ("country_code".equals(outputFieldName)) {
                        outputArray[columnIndex] = Utilities.getCountryCodeForIP(jsonObject.getString("ip"));
                    } else if ("properties".equals(outputFieldName)) {
                        //do nothing will be handled lately
                    } else {
                        outputArray[columnIndex] = String.valueOf(jsonObject.get(outputFieldName));
                    }
                } catch (Exception e) {
                    //reset outputArray to empty string
                    /*LOGGER.warn(String.format("Exception caught when processing data(%s) for fieldName(%s)",
                            jsonObject.toString(), outputFieldName));*/
                    outputArray[columnIndex] = StringUtils.EMPTY;
                }
                jsonObject.remove(outputFieldName);
                columnIndex++;
            }
	        return outputArray;
	
	    }


        /**
         * Validates fields - app, uid, snsid, timestamp to ensure that it does not have invalid data
         * @param jsonObject
         * @return
         */
	    private boolean validateInputData(JSONObject jsonObject){
	        return Utilities.validate(jsonObject, "snsid", StringUtils.EMPTY)
	                && Utilities.validate(jsonObject, "@key", StringUtils.EMPTY)
	                && Utilities.validate(jsonObject, "event", StringUtils.EMPTY)
	                && Utilities.validate(jsonObject, "@ts", MISSED)
	                && Utilities.validate(jsonObject, "uid", MISSED);
	    }
	    
	    /**
	     * Pre-processes the JSONObject for mismatched attribute names.
	     * e.g. install_src -> install_source
	     * @param jsonObject
	     * @return
	     */
	    private JSONObject preProcessJSON(JSONObject jsonObject) {
	    	JSONObject json = Utilities.renameJSONStringKey("event_name", "event", jsonObject);
	    	json = Utilities.renameJSONStringKey("install_src", "install_source", json);
	    	json = Utilities.renameJSONStringKey("track_ref", "install_source", json);
	    	json = Utilities.renameJSONStringKey("addtime", "install_ts", json);
	    	json = Utilities.renameJSONStringKey("browserVersion", "browser_version", json);
	    	return json;
	    }
    
	    /**
	     * Generate the output path using date information
	     * 
	     * @param timeStamp
	     * @return
	     */
	    private String generateOutputPath(String timeStamp){
			StringBuilder builder = new StringBuilder();
			String year = "unknown";
			String day = "unknown";
			String month = "unknown";
			String hour = "unknown";
			try{
				year = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "yyyy");
				month = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "MM");
				day = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "dd");
				hour = Utilities.getDateFormatConverted(timeStamp, "yyyy-MM-dd HH:mm:ss", "HH");
			}
			catch (Exception e){
				System.out.print("error in ts : " + e.getMessage());
			}
			builder.append(year).append("/")
					.append(month).append("/")
					.append(day).append("/")
					.append(hour).append("/").append("part");
			return builder.toString();
		}

	}

    
    // outputs just the tab-delimited line
 	public static class Reduce extends
 			Reducer<Text, Text, NullWritable, Text> {

 		private MultipleOutputs<NullWritable, Text> multipleOutputs = null;
 		private String outputBasePathFormat = null;

 		@Override
 		protected void setup(Context context) throws IOException {
 			//setup multiple output
 			multipleOutputs = new MultipleOutputs<NullWritable, Text>(context);
 			outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
 		}

 		public void reduce(Text key, Iterable<Text> values,
 				Context context) throws IOException, InterruptedException {
 			// write to hourly output path
 			String outputPath = new StringBuilder(outputBasePathFormat)
 					.append(key.toString()).toString();
 			for (Text t : values) {
 				multipleOutputs.write(BLANK, t, outputPath);
 			}
 		}

 		@Override
 		protected void cleanup(Context context) throws IOException, InterruptedException {
 			multipleOutputs.close();
 		}

 	}
 	

    public static void main(String[] args) throws Exception {
        conf = new Configuration();
        int rc = ToolRunner.run(conf, new ProductionEventsBackfill(), args);
        System.exit(rc);
    }



    @Override
    public int run(String[] args) throws Exception {

        Log.info("MapReduce job Started");

        try {
        	// job name
            String jobName = args[0];
            // input path
            String inputPath = args[1];
            // daily output path
            String outputPath = args[2];
            // hourly output path
            conf.set(OUTPUT_BASE_PATH_FORMAT, args[3]);
            
            Job job = new Job(super.getConf(), jobName);
            job.setJarByClass(ProductionEventsBackfill.class);

            job.setMapperClass(Map.class);
            job.setReducerClass(Reduce.class);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            TextOutputFormat.setCompressOutput(job, true);
            TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);
            
            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(outputPath));

            return job.waitForCompletion(true) ? 0 : 1;
        }
        finally {
            Log.info("MapReduce job Completed");
        }

    }
}

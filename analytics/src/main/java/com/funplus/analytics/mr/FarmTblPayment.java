package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit; 
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;

//import com.maxmind.geoip.LookupService;

/*
 * Takes input from multiple locales and converts them to the desired TSV format
 */
public class FarmTblPayment extends Configured implements Tool {
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;

	// output: tab-separated app ts uid snsid install_ts install_src
	// country_code ip browser browser_version os os_version name {properties}
	public static class Map extends
			Mapper<LongWritable, Text, IntWritable, Text> {
		private Random rng;
		private final static SimpleDateFormat dateFormatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");

		public void setup(Context context) {
			rng = new Random();
		}

		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

			String inLine = value.toString();
			String[] tokenArr = inLine.split("\t");

			if (tokenArr[1].equals("uid") || tokenArr.length < 13) {
				return;
			}
			ArrayList<String> tokenList = new ArrayList<String>();
			tokenList.add(UUID.randomUUID().toString());
			for (int i = 0; i < tokenArr.length; i++) {
				tokenList.add(tokenArr[i].trim());
			}

			if (tokenList.size()==15) {
				tokenList.remove(11);
			}
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	                long ts = Long.parseLong(tokenList.get(9));
        	        Date d = new Date(ts * 1000);
                	tokenList.set(9, sdf.format(d));
			
			Path filePath = ((FileSplit) context.getInputSplit()).getPath();
			String filePathString = ((FileSplit) context.getInputSplit()).getPath().toString();
			tokenList.add(filePathString);
			
			StringBuilder outLine = new StringBuilder();
			outLine.append(tokenList.get(0));
			for (int i = 0; i < tokenList.size(); i++) {
				outLine.append("\t");
				outLine.append(tokenList.get(i).trim());
			}

			context.write(new IntWritable(rng.nextInt()),
					new Text(outLine.toString()));
		}
	}

	// outputs just the tab-delimited line
	public static class Reduce extends
			Reducer<IntWritable, Text, NullWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text t : values) {
				context.write(BLANK, t);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new FarmTblPayment(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {
		Job job = new Job(super.getConf());
		job.setJarByClass(FarmTblPayment.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		//FileOutputFormat.setOutputPath(job, new Path(args[1]));
		//FileInputFormat.addInputPath(job, new Path(args[0]));
		
		//job.setNumReduceTasks(1);
		/*
		 * args[0]: output path args[1]: date as YYYYMMDD
		 */

		FileOutputFormat.setOutputPath(job, new Path(args[0]));
		String date = args[1];

		String[] locales = { "ae", "br", "de", "fr", "it", "nl", "pl", "th",
				"tr", "tw", "us" };
		String INPUTDIR0 = "s3://com.funplusgame.bidata/etl/farm/";
		String INPUTDIR1 = "/mysql/tbl_payments/";
		for (String locale : locales) {
			String inputPath = INPUTDIR0 + locale + INPUTDIR1 + date;
			String inputPath2 = "\"" + inputPath.trim() + "\"";
			FileInputFormat.addInputPath(job, new Path(inputPath));
		}

		return job.waitForCompletion(true) ? 0 : 1;
	}

}

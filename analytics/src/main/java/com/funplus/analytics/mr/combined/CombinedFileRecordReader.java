package com.funplus.analytics.mr.combined;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.CombineFileSplit;
import org.apache.hadoop.util.LineReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by balaji on 12/12/14.
 */

public class CombinedFileRecordReader extends RecordReader<CombinedFileWritable, Text> {

    private Configuration conf;

    private long startOffset;
    private long end;
    private long pos;
    private FileSystem fileSystem;
    private Path path;
    private Path tempPath;
    private CombinedFileWritable key = new CombinedFileWritable();
    private Text value;
    private long cLength; // compressed file length
    private FSDataInputStream fileIn;
    private LineReader reader;

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context)
            throws IOException {
        // not initialized
    }

    public CombinedFileRecordReader(CombineFileSplit split,
                                    TaskAttemptContext context, Integer index) throws IOException {

        conf = context.getConfiguration();
        path = split.getPath(index);

        boolean isCompressed = isSplitCompressed();
        if (isCompressed) {
            createDecompressedTemp();
        }
        fileSystem = path.getFileSystem(conf);
        startOffset = split.getOffset(index);

        if(isCompressed){
            end = startOffset + cLength;
        }else{
            end = startOffset + split.getLength(index);
            tempPath = path;
        }

        boolean skipFirstLine = false;
        fileIn = fileSystem.open(tempPath);
        if(isCompressed) {
            fileSystem.deleteOnExit(tempPath);
        }

        if (startOffset != 0) {
            skipFirstLine = true;
            --startOffset;
            fileIn.seek(startOffset);
        }
        reader = new LineReader(fileIn);
        if (skipFirstLine) {
            startOffset += reader.readLine(new Text(), 0,
                    (int)Math.min((long)Integer.MAX_VALUE, end - startOffset));
        }
        pos = startOffset;

    }



    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        if (key.fileName== null) {
            key = new CombinedFileWritable();
            key.fileName = tempPath.getName();
        }
        key.offset = pos;
        if (value == null) {
            value = new Text();
        }
        int newSize = 0;
        if (pos < end) {
            newSize = reader.readLine(value);
            pos += newSize;
        }
        if (newSize == 0) {
            key = null;
            value = null;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public CombinedFileWritable getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        if (startOffset == end) {
            return 0.0f;
        } else {
            return Math.min(1.0f, (pos - startOffset) / (float)
                    (end - startOffset));
        }
    }

    @Override
    public void close() throws IOException {

    }


    /**
     * For compressed file create decompressed file and get its length
     * @throws IOException
     */
    private void createDecompressedTemp() throws IOException{

        CompressionCodecFactory factory = new CompressionCodecFactory(conf);
        CompressionCodec codec = factory.getCodec(path);

        if (codec == null) {
            System.err.println("No Codec Found For " + path);
            System.exit(1);
        }

        String outputUri =
                CompressionCodecFactory.removeSuffix(path.toString(),
                        codec.getDefaultExtension());
        tempPath = new Path(outputUri);

        InputStream in = null;
        OutputStream out = null;
        fileSystem = path.getFileSystem(conf);

        try {
            in = codec.createInputStream(fileSystem.open(path));
            out = fileSystem.create(tempPath);
            IOUtils.copyBytes(in, out, conf);
        } finally {
            IOUtils.closeStream(in);
            IOUtils.closeStream(out);
            cLength = fileSystem.getFileStatus(tempPath).getLen();
        }
    }

    /**
     * Check whether the input file is compressed.
     * @return
     */
    private boolean isSplitCompressed(){

        CompressionCodecFactory factory = new CompressionCodecFactory(conf);
        CompressionCodec codec = factory.getCodec(path);

        if (codec == null)
            return false;
        else
            return true;

    }


}

package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;


public class SplytToTSV extends Configured implements Tool {
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;
	private final static String EXPERIMENTS = "experiments";
	private final static String DATA = "data";
	private final static String VARIANTS = "variants";

	public static class Map extends
			Mapper<LongWritable, Text, NullWritable, Text> {

		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			
			Configuration conf = context.getConfiguration();
			String app = conf.get("app.namespace");
			String type = conf.get("file.type");
			String env = conf.get("env");
			
			if (type.equals(EXPERIMENTS)) {
				ArrayList<String> fields = parseTSVRecord(value.toString());
				if (fields == null) {
					return;
				}
				System.out.println(fields.toString());
				//generate MD5 of app,exid,env
				StringBuilder surrogate = new StringBuilder();
				surrogate.append(app).append(fields.get(0)).append(env);
				String id = DigestUtils.md5Hex(surrogate.toString());
				
				//format timestamp 1407917146.1183
				String start_ts = getFormattedTimestamp(fields.get(4));
				String end_ts = getFormattedTimestamp(fields.get(5));
				
				ArrayList<String> tsvFields = new ArrayList<String>(fields);
				tsvFields.add(0,id);
				tsvFields.add(1,app);
				tsvFields.add(start_ts);
				tsvFields.add(end_ts);
				tsvFields.add(env);
				
				if (tsvFields != null) {
					context.write(BLANK, new Text(writeTSVRecord(tsvFields)));
				}
			}
			else if (type.equals(VARIANTS)) {
				ArrayList<String> fields = parseTSVRecord(value.toString());
				if (fields == null) {
					return;
				}
				
				//generate MD5 of app,exid,env
				StringBuilder surrogate = new StringBuilder();
				surrogate.append(app).append(fields.get(0)).append(fields.get(1)).append(env);
				String id = DigestUtils.md5Hex(surrogate.toString());
				
				ArrayList<String> tsvFields = new ArrayList<String>(fields);
				tsvFields.add(0,id);
				tsvFields.add(1,app);
				tsvFields.add(env);
				
				if (tsvFields != null) {
					context.write(BLANK, new Text(writeTSVRecord(tsvFields)));
				}
			}
			else if (type.equals(DATA)) {
				ArrayList<String> fields = parseTSVRecord(value.toString());
				if (fields == null) {
					return;
				}
				
				//generate MD5 of app,exid,user_id,env
				StringBuilder surrogate = new StringBuilder();
				surrogate.append(app).append(fields.get(0)).append(fields.get(1)).append(fields.get(2)).append(fields.get(3)).append(env);
				String id = DigestUtils.md5Hex(surrogate.toString());
				
				//format timestamp 1407917146.1183
				String first_assignment_ts = getFormattedTimestamp(fields.get(3));
				
				ArrayList<String> tsvFields = new ArrayList<String>(fields);
				tsvFields.add(0,id);
				tsvFields.add(1,app);
				tsvFields.add(first_assignment_ts);
				tsvFields.add(env);
				if (tsvFields != null) {
					context.write(BLANK, new Text(writeTSVRecord(tsvFields)));
				}
			}
			else {
				return;
			}
		}
	};
	
	private static ArrayList<String> parseTSVRecord(String str) {
		if (str == null) {
			return null;
		}
		ArrayList<String> outputRecord = new ArrayList<String>();
		String[] fields = str.split("\t");
		for (String field : fields) {
			outputRecord.add(StringUtils.strip(field, "\""));
		}
		return outputRecord;
	}

	private static String writeTSVRecord(ArrayList<String> fields) {		
		return (fields != null) ? StringUtils.join(fields,'\t') : "";
	}

	private static String getFormattedTimestamp(String ts) {
        String val = "";
        try {
			Double f = Double.parseDouble(ts);
			if (f > 0) {
		        long timestamp = (long)(f.doubleValue()*1000);
		        Date unixDate=new Date(timestamp);
				SimpleDateFormat dateFormatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				val = dateFormatter.format(unixDate);
			}
        }
        catch(NumberFormatException e) {
        	System.out.println("Error formatting date: " + ts);
        }
        return val;
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new SplytToTSV(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = super.getConf();
		conf.set("file.type", args[2]);
		conf.set("app.namespace", args[3]);
		conf.set("env", args[4]); // e.g. prod or test
		Job job = new Job(conf);
		
		job.setJarByClass(SplytToTSV.class);
		job.setMapperClass(Map.class);

		job.setMapOutputKeyClass(NullWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextOutputFormat.setCompressOutput(job, true);
		TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		return job.waitForCompletion(true) ? 0 : 1; 
	}

}

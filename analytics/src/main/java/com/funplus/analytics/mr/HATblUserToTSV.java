package com.funplus.analytics.mr;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.apache.commons.codec.digest.DigestUtils;

import com.maxmind.geoip.LookupService;

/*
 * Converts adjust JSON to TSV
 */
public class HATblUserToTSV extends Configured implements Tool {
	private static final Logger LOGGER = Logger.getLogger(HATblUserToTSV.class);
	private final static NullWritable BLANK = NullWritable.get();
	private static Configuration conf;

	private final static String[] KEYARRAY = new String[] { 
		"@id",
		"@app", 
		"_id",//uid 
		"uid",//snsid
		"ref",//install_source
		"create_time",//install_ts
		"os",
		"os_version",
		"@country_code",//sns.country
		"level",
		"device",
		"language",
		"browser",
		"browser_version",
		"cash",//rc
		"coins",
		"email",
		"@gender",//sns.gender
		"first_name",
		"age",
		"last_login_time",//last_login_ts
		"sign_email",//sign_email
		"birthday", //birthday 03/26/1987
	};
	
	// sentinel value for invalid integers
	private final static int MISSEDINT = -1;
	// empty string for empty fields
	private final static String NULLSTRING = "";
	private final static JSONObject NULLJSONOBJECT = null;
	// tab character
	private final static char TAB = '\t';
	private static String appID = null;

	public static class Map extends
			Mapper<LongWritable, Text, IntWritable, Text> {
		
		public Random rng;
		public int numReducers;
		public IntWritable[] reducerKeys;
		
		// construct the random number generator and set the number of reducers
		public void setup(Context context) {
			appID = context.getConfiguration().get("app");
			rng = new Random();
			numReducers = context.getNumReduceTasks();
			reducerKeys = new IntWritable[numReducers];
			for (int i = 0; i < numReducers; i++) {
				reducerKeys[i] = new IntWritable(i);
			}
		}
		
		// doesn't use the LongWritable key
		// returns (random number from 0 to NUMSPLITS - 1, Text constructed from JSON)
		// or (-1, Text with invalid properties length)
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			try {
				JSONObject jsonLine = new JSONObject(value.toString());
				
				if (!isValid(jsonLine)) {
					return;
				}
				
				ArrayList<String> tsvFields = getJSONFields(jsonLine);
				if (tsvFields != null) {
						context.write(reducerKeys[rng.nextInt(numReducers)], new Text(makeSBLine(tsvFields).toString()));
				}
			} catch (Exception e) {
				LOGGER.error(String.format("Invalid JSON - %s", value.toString()));
			}
		}
	}
	
	private static boolean isValid(JSONObject jsonLine) {
		String snsid;
		int uid;
		int install_ts;
		try {
			snsid = ((JSONObject)jsonLine.get("uid")).getString("$numberLong");
		} catch (Exception e) {
			snsid = getJSONString(jsonLine, "uid");
		}
		try {
			uid = ((JSONObject)jsonLine.get("_id")).getInt("$numberLong");
		}catch (Exception e) {
			uid = getJSONInt(jsonLine, "_id");
		}
		try {
			install_ts = ((JSONObject)jsonLine.get("create_time")).getInt("$numberLong");
		} catch (Exception e) {
			install_ts = getJSONInt(jsonLine, "create_time");
		}
		
		if (snsid == NULLSTRING || uid == MISSEDINT || install_ts == MISSEDINT) {
			return false;
		}
		return true;
	}
		
	private static ArrayList<String> getJSONFields(JSONObject jsonLine) {
		ArrayList<String> fieldKeys = new ArrayList<String>();
		fieldKeys.addAll(Arrays.asList(KEYARRAY));
		ArrayList<String> jsonFields = new ArrayList<String>();
		for (int i = 0; i < fieldKeys.size(); i++) {
			String key = fieldKeys.get(i);
			
			if (key.equals("@id")) {
				String snsid;
				int uid;
				try {
					snsid = ((JSONObject)jsonLine.get("uid")).getString("$numberLong");

				} catch (Exception e) {
					snsid = getJSONString(jsonLine,"uid");
				}
				try {
					uid = ((JSONObject)jsonLine.get("_id")).getInt("$numberLong");
				} catch (Exception e) {
					uid = getJSONInt(jsonLine, "_id");
				}
				StringBuilder sb = new StringBuilder();
				sb.append(appID).append(uid).append(snsid);
				String id = DigestUtils.md5Hex(sb.toString());
				jsonFields.add(id);
			}
			else if (key.equals("@app")) {
				jsonFields.add(appID);
			}
			else if (key.equals("_id")) {
				int uid;
				try {
					uid = ((JSONObject)jsonLine.get("_id")).getInt("$numberLong");
				} catch (Exception e) {
					uid = getJSONInt(jsonLine, "_id");
				}
				jsonFields.add(String.valueOf(uid));
			}
			else if (key.equals("create_time")) {
				int ts;
				try {
					ts = ((JSONObject)jsonLine.get("create_time")).getInt("$numberLong");
				} catch (Exception e) {
					ts = getJSONInt(jsonLine,"create_time");
				}
				Date unixDate = new Date((long) ts * 1000);
				SimpleDateFormat dateFormatter = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				String tsvDate = dateFormatter.format(unixDate);
				jsonFields.add(tsvDate);
			}
			else if (key.equals("@country_code")) {
				String country = "";
				JSONObject sns = getJSONObject(jsonLine,"sns");
				if (sns != NULLJSONOBJECT) {
					country = getJSONString(sns, "country");
				}
				jsonFields.add(country);
			}
			else if (key.equals("level")) {
				int level;
				try {
					level = ((JSONObject)jsonLine.get("level")).getInt("$numberLong");
				} catch (Exception e) {
					level = getJSONInt(jsonLine, "level");
				}
				jsonFields.add(String.valueOf(level));
			}
			else if (key.equals("cash")) {
				String cash;
				try {
					cash = ((JSONObject)jsonLine.get("cash")).optString("$numberLong");
				} catch (Exception e) {
					cash = jsonLine.optString("cash");
				}
				jsonFields.add(cash);
			}
			else if (key.equals("coins")) {
				String coins;
				try {
					coins = ((JSONObject)jsonLine.get("coins")).optString("$numberLong");
				} catch (Exception e) {
					coins = jsonLine.optString("coins");
				}
				jsonFields.add(coins);

			}
			else if (key.equals("last_login_time")) {
				String last_login_time;
				try {
					last_login_time = ((JSONObject)jsonLine.get("last_login_time")).optString("$numberLong");
				} catch (Exception e) {
					last_login_time = jsonLine.optString("last_login_time");
				}
				jsonFields.add(last_login_time);
			}
			else if (key.equals("@gender")) {
				String gender = "";
				JSONObject sns = getJSONObject(jsonLine,"sns");
				if (sns != NULLJSONOBJECT) {
					int genderInt;
					try {
						genderInt = ((JSONObject)jsonLine.get("gender")).getInt("$numberLong");
					} catch (Exception e) {
						genderInt = getJSONInt(sns, "gender");
					}
					if (genderInt != MISSEDINT) {
						if (genderInt == 1) {
							gender = "M";
						}
						else if (genderInt == 2) {
							gender = "F";
						}
						else {
						    gender = "UN";
						}
					}
				}
				jsonFields.add(gender);
			}
			else if (key.equals("age")) {
				String coins;
				try {
					coins = ((JSONObject)jsonLine.get("age")).optString("$numberLong");
				} catch (Exception e) {
					coins = jsonLine.optString("age");
				}
				jsonFields.add(coins);

			}
			else {
				jsonFields.add(jsonLine.optString(key).replaceAll("\n", "").replaceAll("\\\\", ""));
			}
		}
		 
		return jsonFields;
	}

	// wrapper for getting a string from json, returns empty string on
	// failure
	private static String getJSONString(JSONObject jsonLine, String key) {
		return getJSONString(jsonLine, key, "");
	}

	private static String getJSONString(JSONObject jsonLine, String key,
			String errMessage) {
		String value;
		try {
			value = jsonLine.getString(key);
		} catch (Exception e) {
			System.out.println("missed a field:" + errMessage);
			value = NULLSTRING;
		}
		return value;
	}

	// wrapper for getting an int from json, returns a sentinel on failure
	private static int getJSONInt(JSONObject jsonLine, String key) {
		return getJSONInt(jsonLine, key, "");
	}

	private static int getJSONInt(JSONObject jsonLine, String key,
			String errMessage) {
		int value;
		try {
			value = jsonLine.getInt(key);
		} catch (Exception e) {
			System.out.println("missed an int:" + errMessage);
			value = MISSEDINT;
		}
		return value;
	}

	private static JSONObject getJSONObject(JSONObject jsonLine, String key,
			String errMessage) {
		JSONObject value;
		try {
			value = (JSONObject)jsonLine.get(key);
		} catch (Exception e) {
			System.out.println("missed a field:" + errMessage);
			value = NULLJSONOBJECT;
		}
		return value;
	}

	// wrapper for getting an JSONObject from json, returns a sentinel on failure
	private static JSONObject getJSONObject(JSONObject jsonLine, String key) {
		return getJSONObject(jsonLine, key, "");
	}

	
	// generates the tab-delimited StringBuilder out of the fields
	private static StringBuilder makeSBLine(ArrayList<String> fields) {
		StringBuilder sb = new StringBuilder(fields.get(0));
		for (int i = 1; i < fields.size(); i++) {
			sb.append(TAB);
			sb.append(fields.get(i));
		}
		return sb;
	}
	

	
	// outputs just the tab-delimited line
	public static class Reduce extends
			Reducer<IntWritable, Text, NullWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			for (Text t : values) {
				context.write(BLANK, t);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		conf = new Configuration();
		int rc = ToolRunner.run(conf, new HATblUserToTSV(), args);
		System.exit(rc);
	}

	@Override
	public int run(String[] args) throws Exception {
		String app = args[2];
        Configuration conf = super.getConf();
        conf.set("app", app);
		Job job = new Job(super.getConf());
		job.setJarByClass(HATblUserToTSV.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextOutputFormat.setCompressOutput(job, true);
		TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		return job.waitForCompletion(true) ? 0 : 1;
	}

}

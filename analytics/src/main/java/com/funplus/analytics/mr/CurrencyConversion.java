package com.funplus.analytics.mr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

import org.json.JSONArray;
import org.json.JSONObject;


public class CurrencyConversion {
	public static final String CURRENCYURL = "http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json"; 
	public static final String TSVLOCATION_0 = "/mnt/data/jars/currency_staging/";
	public static final String TSVLOCATION_1 = "_currency.gz";
	
	private final static SimpleDateFormat dateInputFormatter = new SimpleDateFormat("yyyyMMdd");
	private final static SimpleDateFormat dateOutputFormatter = new SimpleDateFormat("yyyy-MM-dd");
	
	// read the text at a URL into a JSON object
	public static JSONObject readURLJSON(String URLName) throws Exception {
		URL currencyURL = new URL(URLName);
		InputStream currencyStream = currencyURL.openStream();
		int nextCharInt = currencyStream.read();
		StringBuilder fileChars = new StringBuilder(); 
		while (nextCharInt != -1) {
			char nextChar = (char)(nextCharInt);
			if (nextChar != '\n') {
				fileChars.append(nextChar);
			}
			nextCharInt = currencyStream.read();
		}
		return new JSONObject(fileChars.toString());
	}
	
	// returns whether this is a USD conversion or not
	public static boolean isUSDConversion(JSONObject fields) {
		return fields.getString("name").substring(0, 3).equals("USD");
	}

	// given a string in YYYYMMDD format, returns a properly formatted date-time string
	public static String getDateTime(String date) {
		try {
			Date inputDate = dateInputFormatter.parse(date);
			return dateOutputFormatter.format(inputDate);
		} catch (ParseException e) {
			return "";
		}
	}
	
	// returns a string of the hash of two strings
	public static String getBigHashCode(String s1, String s2) {
		return Integer.toString((s1 + s2).hashCode());
	}
	
	// given the JSONObject containing the field, returns the other currency in a USD conversion
	public static String getOtherCurrency(JSONObject fields) {
		String name = null;
		try {
			name = fields.getString("name").substring(4, 7);
		}
		catch (Exception e){
//			System.out.println(fields);
			name = fields.getString("name");
		}
		return name;
	}
	
	// given the JSONObject containing the field, returns the conversion factor as a string
	public static String getConversionFactor(JSONObject fields) {
		return String.format("%f", (1.0 / fields.getDouble("price")));
	}
	
	// given the needed fields, splits them with tabs, returns the final TSV string
	public static String getTSVLine(String hash, String dateTime, String otherCurrency, String conversionFactor) {
		return hash + "	" + dateTime + "	" + otherCurrency + "	" + conversionFactor + "\n";
	}
	
	public static void main(String[] args) {
		JSONObject JSON = new JSONObject();
		try {
			JSON = readURLJSON(CURRENCYURL);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("exiting");
			System.exit(1);
		}
		JSONArray currencyInfo = JSON.getJSONObject("list").getJSONArray("resources");
		
		try {
			File targetTSV = new File(TSVLOCATION_0 + args[0] + TSVLOCATION_1);
			GZIPOutputStream gzipWriter = new GZIPOutputStream(new FileOutputStream(targetTSV, false));
			for (int i = 0; i < currencyInfo.length(); i++) {
				JSONObject currencyFields = currencyInfo.getJSONObject(i).getJSONObject("resource").getJSONObject("fields");
				if (isUSDConversion(currencyFields)) {
					String date = getDateTime(args[0]);
					String currencyName = getOtherCurrency(currencyFields);
					String id = getBigHashCode(date, currencyName);
					String tsvLine = getTSVLine(id, getDateTime(args[0]), getOtherCurrency(currencyFields), getConversionFactor(currencyFields));
					for (char tsvChar : tsvLine.toCharArray()) {
						gzipWriter.write((int) tsvChar);
					}
				}
			}
			gzipWriter.close();
		} catch (IOException e) {
			e.printStackTrace(); 
			System.out.println("write failed, exiting");
			System.exit(1);
		}
		
	}
}


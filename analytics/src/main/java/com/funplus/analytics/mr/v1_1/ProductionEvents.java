package com.funplus.analytics.mr.v1_1;

import com.funplus.analytics.Utilities;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.mortbay.log.Log;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.UUID;


/**
 * Created by balaji on 10/14/14.
 */

public class ProductionEvents extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(ProductionEvents.class);

    // sentinel value for invalid integers
    private final static String MISSED = "-1";

    private final static String[] OUTPUT_FIELDS = new String[] { "id", "@key", "@ts",
            "uid", "snsid", "install_ts", "install_source", "country_code", "ip", "browser",
            "browser_version", "os", "os_version", "event", "properties" };


    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            try {
            	JSONObject jsonObject = new JSONObject(value.toString());
            
	            //rename incorrect json keys to match spec for downstream processing 
	            jsonObject = preProcessJSON(jsonObject);
	            
	            if(validateInputData(jsonObject)){
	                String[] outputFields = getOutputFields(jsonObject, OUTPUT_FIELDS);
	
	                //set properties field value - if > 5000, remove action_detail attribute
	                if (jsonObject.toString().length() > 5000) {
	                	if (jsonObject.has("action_detail")) {
	                		jsonObject.remove("action_detail");
	                	}
	                }
	                outputFields[OUTPUT_FIELDS.length - 1] = jsonObject.toString();
	                //write to context
	                context.getCounter(context.getJobName(), outputFields[OUTPUT_FIELDS.length - 2]).increment(1);
	                context.write(new Text(StringUtils.join(outputFields, '\t')), null);
	            }
	            else{
	                context.getCounter(context.getJobName(), "invalid").increment(1);
	                LOGGER.warn(String.format("Validation failed for input data - %s", jsonObject));
	            }
            }
            catch(JSONException e) {
            	LOGGER.error(String.format("Invalid JSON - %s", value.toString()));
            }
        }

        /**
         * Construct the String array for the given output fields using input json object
         * @param jsonObject
         * @param outputFieldNames
         * @return
         */
        private String[] getOutputFields(JSONObject jsonObject, String[] outputFieldNames){

            String[] outputArray = new String[outputFieldNames.length];
            int columnIndex = 0;
            for (String outputFieldName : outputFieldNames) {
                try {
                    // handle special cases for fields - ps, @ts, country_code, properties
                    if ("id".equals(outputFieldName)) {
                        outputArray[columnIndex] = UUID.randomUUID().toString();
                    } else if ("@ts".equals(outputFieldName) || "install_ts".equals(outputFieldName)) {
                    	try {
	                        int timeStamp = jsonObject.getInt(outputFieldName);
	                        outputArray[columnIndex] = String.valueOf(Utilities.getDefaultFormattedDateForTimestamp(timeStamp));
                    	} catch (JSONException e) {
                    		// Fix the date issue
                    		String timeStamp = jsonObject.getString(outputFieldName);
                        	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                        	outputArray[columnIndex] = String.valueOf(Utilities.getDefaultFormattedDateForTimestamp((int)(df.parse(timeStamp).getTime()/1000)));
                    	}
                    } else if ("country_code".equals(outputFieldName)) {
                        outputArray[columnIndex] = Utilities.getCountryCodeForIP(jsonObject.getString("ip"));
                    } else if ("properties".equals(outputFieldName)) {
                        //do nothing will be handled lately
                    } else {
                        outputArray[columnIndex] = String.valueOf(jsonObject.get(outputFieldName));
                    }
                } catch (Exception e) {
                    //reset outputArray to empty string
                    /*LOGGER.warn(String.format("Exception caught when processing data(%s) for fieldName(%s)",
                            jsonObject.toString(), outputFieldName));*/
                    outputArray[columnIndex] = StringUtils.EMPTY;
                }
                jsonObject.remove(outputFieldName);
                columnIndex++;
            }
        return outputArray;

    }


        /**
         * Validates fields - app, uid, snsid, timestamp to ensure that it does not have invalid data
         * @param jsonObject
         * @return
         */
    private boolean validateInputData(JSONObject jsonObject){
        return Utilities.validate(jsonObject, "snsid", StringUtils.EMPTY)
                && Utilities.validate(jsonObject, "@key", StringUtils.EMPTY)
                && Utilities.validate(jsonObject, "event", StringUtils.EMPTY)
                && Utilities.validate(jsonObject, "@ts", MISSED)
                && Utilities.validate(jsonObject, "uid", MISSED);
    }
    
    /**
     * Pre-processes the JSONObject for mismatched attribute names.
     * e.g. install_src -> install_source
     * @param jsonObject
     * @return
     */
    private JSONObject preProcessJSON(JSONObject jsonObject) {
    	JSONObject json = Utilities.renameJSONStringKey("event_name", "event", jsonObject);
    	json = Utilities.renameJSONStringKey("install_src", "install_source", json);
    	json = Utilities.renameJSONStringKey("track_ref", "install_source", json);
    	json = Utilities.renameJSONStringKey("addtime", "install_ts", json);
    	json = Utilities.renameJSONStringKey("browserVersion", "browser_version", json);
    	return json;
    }


}


    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int rc = ToolRunner.run(conf, new ProductionEvents(), args);
        System.exit(rc);
    }



    @Override
    public int run(String[] args) throws Exception {

        Log.info("MapReduce job Started");

        try {

            String jobName = args[0];
            String inputPath = args[1];
            String outputPath = args[2];
            
            Job job = new Job(super.getConf(), "EventsParserJob");
            job.setJarByClass(ProductionEvents.class);

            job.setMapperClass(Map.class);
            //job.setReducerClass(Reduce.class);

            // Sets reducer tasks to 0
            job.setNumReduceTasks(0);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            TextOutputFormat.setCompressOutput(job, true);
            TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);
            
            FileInputFormat.addInputPath(job, new Path(inputPath));
            FileOutputFormat.setOutputPath(job, new Path(outputPath));

            Boolean result = job.waitForCompletion(true);

            if (outputPath.contains("hourly")){
                String tempPath = outputPath.replaceFirst("hourly","scratch");
                Utilities.writeCounters(job, tempPath);
            }

            return result ? 0 : 1;
        }
        finally {
            Log.info("MapReduce job Completed");
        }

    }
}

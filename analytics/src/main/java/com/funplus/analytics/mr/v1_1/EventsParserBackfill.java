
package com.funplus.analytics.mr.v1_1;

import com.funplus.analytics.CountryCodeLookupService;
import com.funplus.analytics.Utilities;
import com.funplus.analytics.validation.JsonError;
import com.funplus.analytics.validation.JsonSchemaValidation;
import com.funplus.analytics.validation.JsonSchemaValidatorFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.mortbay.log.Log;

import java.io.IOException;

/**
 * Created by balaji on 10/16/14.
 */

public class EventsParserBackfill extends Configured implements Tool {

    private static final Logger LOGGER = Logger.getLogger(EventsParserBackfill.class);

    private static final String INVALID_EVENT = "invalid";
    private static final String COUNTRY_CODE = "country_code";
    private static final String OUTPUT_BASE_PATH_FORMAT = "outputBasePathFormat";



    public static class Map extends
            Mapper<LongWritable, Text, Text, Text> {

        private JsonSchemaValidatorFactory jsonSchemaValidatorFactory = null;
        private CountryCodeLookupService  countryCodeLookupService = null;
        private String specVersion = null;
        //private MultipleOutputs multipleOutputs = null;
        private String outputBasePathFormat = null;
        String outputPath = null;


        @Override
        protected void setup(Context context) throws IOException {

            //setup factory
            jsonSchemaValidatorFactory = JsonSchemaValidatorFactory.getInstance();
            jsonSchemaValidatorFactory.bootstrap(context.getConfiguration());

            //setup Country code service
            countryCodeLookupService = CountryCodeLookupService.getInstance();

            specVersion = context.getConfiguration().get("specVersion");
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);

        }


        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            JSONObject outputJson = null;
            JsonError jsonError = null;
            String eventName = null;
            String schemaValidatorKey = null;
            try{
                outputJson = new JSONObject(value.toString());
                try {
                    eventName = outputJson.getString("event");
                    schemaValidatorKey = jsonSchemaValidatorFactory.generateSchemaValidatorKey(specVersion, eventName);
                    jsonError = jsonSchemaValidatorFactory.validate(schemaValidatorKey, value.toString());
                } catch (Exception e) {
                    //do nothing
                    jsonError = new JsonError(JsonSchemaValidation.PROCESSING_ERROR.toString(), e.getMessage());
                }
                //check for error if available the input json is invalid
                if (null != jsonError) {
                    eventName = INVALID_EVENT;
                    Utilities.includeErrorDataInJson(outputJson, jsonError);
                }
                else {
                    //include CountryCode && prettyDate to output json
                    countryCodeLookupService.includeCountryCodeInJson(COUNTRY_CODE, outputJson);
                    Utilities.includeDefaultPrettyDateInJson("@ts", outputJson);
                    Utilities.includeDefaultPrettyDateInJson("install_ts", outputJson);
                }

                String mapKey = generateMapKey(eventName, outputJson);
                outputPath = new StringBuilder(outputBasePathFormat)
                        .append(mapKey).toString();
            }
            catch (Exception e){

                LOGGER.error(String.format("Error in json -&gt; %s -&gt; %s", e.getMessage(), value.toString()));
            }
            context.write(new Text(outputPath), new Text(StringUtils.remove(outputJson.toString(), "\\t")));
            //LOGGER.info(String.format("%s --&gt; %s", outputPath, outputJson));
        }

        private String generateMapKey(String eventName, JSONObject outputJson){
            StringBuilder builder = new StringBuilder(eventName).append("/")
                    .append("app=").append(outputJson.get("@key")).append("/");
            Integer ts = null;
            String year = "unknown";
            String day = "unknown";
            String month = "unknown";
            String hour = "unknown";
            try{
                ts = outputJson.getInt("@ts");
                year = Utilities.getFormattedDateForTimestamp(ts, "yyyy");
                month = Utilities.getFormattedDateForTimestamp(ts, "MM");
                day = Utilities.getFormattedDateForTimestamp(ts, "dd");
                hour = Utilities.getFormattedDateForTimestamp(ts,"HH");
            }
            catch (Exception e){}
            builder.append("year=").append(year).append("/")
                    .append("month=").append(month).append("/")
                    .append("day=").append(day).append("/")
                    .append("hour=").append(hour).append("/").append("part");
            return builder.toString();
            //accept_neighbor/app=bv.global.prod/year=2014/month=11/day=03/hour=03/
            //npc_order/app=bv.global.testflight/year=2014/month=10/day=13/hour=16
        }

    }


    public static class Reduce extends
            Reducer<Text, Text, NullWritable, Text> {

        private MultipleOutputs multipleOutputs = null;
        private String outputBasePathFormat = null;

        @Override
        protected void setup(Context context) throws IOException {
            //setup multiple output
            multipleOutputs = new MultipleOutputs(context);
            outputBasePathFormat = context.getConfiguration().get(OUTPUT_BASE_PATH_FORMAT);
        }

        public void reduce(Text key, Iterable<Text> values,
                           Context context) throws IOException, InterruptedException {

            String outputPath = key.toString();
            for (Text text : values) {
                multipleOutputs.write(NullWritable.get(), text, outputPath);
            }
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            multipleOutputs.close();
        }

    }




    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        int rc = ToolRunner.run(conf, new EventsParserBackfill(), args);
        System.exit(rc);
    }


    @Override
    public int run(String[] args) throws Exception {
        Log.info("MapReduce job Started");

        try {

            String inputBasePath = args[0];
            String outputPath = args[1];
            String tempPath = args[2];
            String app = args[3];
            String specVersion = args[4];
            String s3SchemaBucket = args[5];
            String s3SchemaPath = args[6];
            // String baseDate = args[7];
            // String daysBack = args[8];

            LOGGER.info(String.format("inputBasePath(%s) outputBasePath(%s) tempPath(%s) app(%s) specVersion(%s) s3SchemaBucket(%s) s3SchemaPath(%s)",
                    inputBasePath, outputPath, tempPath, app, specVersion, s3SchemaBucket, s3SchemaPath));

            Configuration conf = super.getConf();
            conf.set(OUTPUT_BASE_PATH_FORMAT, outputPath);
            conf.set("app", app);
            conf.set("specVersion", specVersion);
            conf.set("s3SchemaBucket", s3SchemaBucket);
            conf.set("s3SchemaPath", s3SchemaPath);
            // for recursive
            conf.set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
            if (args.length == 8) {
                conf.set("numDistribution", args[7]);
            }
            Job job = new Job(conf, "EventsParserBackfillJob");
            job.setJarByClass(EventsParserBackfill.class);

            job.setMapperClass(Map.class);
            job.setReducerClass(Reduce.class);
            //job.setNumReduceTasks(0);

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);

            job.setInputFormatClass(TextInputFormat.class);
            LazyOutputFormat.setOutputFormatClass(job, SequenceFileOutputFormat.class);


            SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
            SequenceFileOutputFormat.setCompressOutput(job, true);
            SequenceFileOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

            FileInputFormat.addInputPath(job, new Path(inputBasePath));
            FileOutputFormat.setOutputPath(job, new Path(tempPath));

            return job.waitForCompletion(true) ? 0 : 1;
        } finally {
            Log.info("MapReduce job Completed");
        }

    }
}
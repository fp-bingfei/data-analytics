package com.funplus.analytics.mr;

import java.io.IOException;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.*;
import java.util.StringTokenizer;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.util.*;
import org.apache.hadoop.io.compress.*;

public class ItemFlowFin extends Configured implements Tool {

	public static class globalVars { 

		public String pCummItemBal="0";
		public String pLinkedListItem = "0";
		public String pItemid = "0";
		public String pUid="0";
		public String pUnitPrice = "0.0";
		public String pItemUsedPrice = "0.0";

		public String getCummItemBal() { 
			return pCummItemBal; 
		} 
		public void setCummItemBal(String x) { 
			this.pCummItemBal = x; 
		} 

		public String getLinkedListItem() { 
			return pLinkedListItem; 
		} 
		public void setLinkedListItem(String x) { 
			this.pLinkedListItem = x; 
		} 
		public String getItemid() { 
			return pItemid; 
		} 

		public void setItemid(String y) { 
			this.pItemid = y; 
		} 

		public String getUid() { 
			return pUid; 
		} 

		public void setUid(String y) { 
			this.pUid = y; 
		} 

		public String getUnitPrice() { 
			return pUnitPrice; 
		} 

		public void setUnitPrice(String y) { 
			this.pUnitPrice = y; 
		}

		public String getItemUsedPrice() { 
			return pItemUsedPrice; 
		} 

		public void setItemUsedPrice(String y) { 
			this.pItemUsedPrice = y; 
		}

	}

	public static String createPurchaseRec(String rec, String pCummItemBal) {

		int item_in=0;
		double item_cost=0.0;
		int item_out=0;

		int pCummItemBal_val = 0;
		int item_bal = 0;
		double unit_cost = 0.0;
		double RevRecog = 0.0;
		String[] itemRec = rec.split("\t");
		ArrayList newRec = new ArrayList(Arrays.asList(itemRec));

		item_in = Integer.parseInt(itemRec[7]);

		item_cost = Double.parseDouble(itemRec[11]);
		item_out = Integer.parseInt(itemRec[8]);

		pCummItemBal_val = Integer.parseInt(pCummItemBal);

		// Calculate unit cost;
		if (item_in > 0) {
			unit_cost = item_cost / item_in ;
		}else {
			unit_cost = 0.0;
		}

		item_bal = pCummItemBal_val + item_in ;

		RevRecog = item_out * unit_cost;

		DecimalFormat f = new DecimalFormat("##.##");
		newRec.add(f.format(item_bal));
		newRec.add(f.format(unit_cost));
		newRec.add(f.format(RevRecog));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long ts = Long.parseLong(newRec.get(3).toString());
                Date d = new Date(ts * 1000);
                newRec.set(3, sdf.format(d));

		String result0 = newRec.toString().replaceAll(",\\s+", ",");
		String result = result0.substring(1, result0.length()-1);

		return result;
	}

	public static String createItemUsedRec(String rec, String pItemBal, String pUnitPrice) {

		int item_in=0;
		double item_cost=0.0;
		int item_out=0;

		int pItemBal_val = 0;
		int item_bal = 0;
		double unit_cost = 0.0;
		double RevRecog = 0.0;
		String[] itemRec = rec.split("\t");
		ArrayList newRec = new ArrayList(Arrays.asList(itemRec));

		item_in = 0;
		item_cost = 0.0;

		item_out = Integer.parseInt(itemRec[8]);

		pItemBal_val = Integer.parseInt(pItemBal);
		unit_cost = Double.parseDouble(pUnitPrice);
		//unit_cost = 0.0;
		item_bal = pItemBal_val - item_out ;
		//RevRecog = 0.0;
		RevRecog = item_out * unit_cost;
		DecimalFormat f = new DecimalFormat("##.##");
		newRec.add(f.format(item_bal));
		newRec.add(f.format(unit_cost));
		newRec.add(f.format(RevRecog));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long ts = Long.parseLong(newRec.get(3).toString());
                Date d = new Date(ts * 1000);
                newRec.set(3, sdf.format(d));

		String result0 = newRec.toString().replaceAll(",\\s+", ",");
		String result = result0.substring(1, result0.length()-1);

		return result;
	}

	public static String createPartialItemUsedRec(String rec, String pItemBal, int pLeftOverItemUsedBal, String pUnitPrice) {

		int item_in=0;
		double item_cost=0.0;
		int item_out=0;

		int pItemBal_val = 0;
		int pLeftOverItemUsedBal_val = 0;
		int cummItemBal = 0;
		double unit_cost = 0.0;
		double RevRecog = 0.0;
		String[] itemRec = rec.split("\t");

		ArrayList newRec = new ArrayList(Arrays.asList(itemRec));
		globalVars pVars = new globalVars();
		item_in = 0;

		item_cost = 0.0;
		item_out = pLeftOverItemUsedBal;
		pItemBal_val = Integer.parseInt(pItemBal);
		pLeftOverItemUsedBal_val = pLeftOverItemUsedBal;
		unit_cost = Double.parseDouble(pUnitPrice);
		//unit_cost = 0.0;
		cummItemBal = pItemBal_val - item_out ;

		//RevRecog = 0.0;
		RevRecog = item_out * unit_cost;

		newRec.set(8, item_out);

		DecimalFormat f = new DecimalFormat("##.##");
		newRec.add(f.format(cummItemBal));
		newRec.add(f.format(unit_cost));
		newRec.add(f.format(RevRecog));
		
		if(!newRec.get(3).toString().contains("-")) {	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                long ts = Long.parseLong(newRec.get(3).toString());
                Date d = new Date(ts * 1000);
                newRec.set(3, sdf.format(d));
		}

		String result0 = newRec.toString().replaceAll(",\\s+", ",");
		String result = result0.substring(1, result0.length()-1);

		return result;
	}

	public static class ItemFlowMap extends Mapper<LongWritable, Text, NullWritable, Text> {

		public void map(LongWritable key, Text value, Context context ) throws IOException, InterruptedException{

			String line = new String();

			line = value.toString();

			if(line.length() > 1) {
				context.write(NullWritable.get(), new Text(line));
			}
		}
	}

	public static class ItemFlowReduce extends Reducer<NullWritable, Text, NullWritable, Text> {

		public void reduce(NullWritable key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {

				Text reduceStr0 = new Text(); 
				globalVars pVars = new globalVars();

				LinkedList<Integer> itemUsedBalLL = new LinkedList<Integer>();
				String[] new_partialItemUsedEvent = new String[18];
				String[] new_ItemUsedEvent = new String[18];
				int pLeftOverItemUsedBal = 0;
				int pItemBalBucket = 0;
				String pUnitPrice = "0.0";

				Iterator<Text> iterator = values.iterator();
				while (iterator.hasNext()) {
					reduceStr0 = iterator.next();
					String reduceStr = reduceStr0.toString();
					String[] itemRec = reduceStr.split("\t");

					String cUid = itemRec[1]; //snsid
					String cItemid = itemRec[2]; //item_id

					if(!pVars.getItemid().equals(cItemid) || !pVars.getUid().equals(cUid))
					{
						pVars.setCummItemBal("0");
						pVars.setLinkedListItem("0");
						pVars.setItemid("0");
					}

					if ( !((itemRec[5].toString().equals("sold")) || (itemRec[5].toString().equals("used")) || (itemRec[5].toString().equals("gifted")) )) {

						String purchaseEvent = createPurchaseRec(reduceStr, pVars.getCummItemBal());
						String[] new_purchaseEvent = purchaseEvent.split(",");
						pVars.setCummItemBal(new_purchaseEvent[15]); //cum item bal
						int pCummItemBal = Integer.parseInt(pVars.getCummItemBal());
						int item_used_diff = pCummItemBal - Integer.parseInt(new_purchaseEvent[7]); // cum item bal - item_in
						pVars.setLinkedListItem(Integer.toString(item_used_diff));
						pVars.setUnitPrice(new_purchaseEvent[16]); //unit price

						if(item_used_diff > 0) {
							pVars.setItemUsedPrice(pVars.getItemUsedPrice());
						}
						context.write(NullWritable.get(), new Text(purchaseEvent));
					} else {
						int pCummItemBal =  Integer.parseInt(pVars.getCummItemBal());
						pLeftOverItemUsedBal = Integer.parseInt(pVars.getLinkedListItem());
						int item_out = Integer.parseInt(itemRec[8].toString()); //item_out column
						pUnitPrice = pVars.getItemUsedPrice();
						if(pCummItemBal > 0) { //if this is true then player has some credit to play
							if (item_out > pLeftOverItemUsedBal && pLeftOverItemUsedBal != 0) {
								while (pLeftOverItemUsedBal > 0) {

									String partialItemUsedEvent = createPartialItemUsedRec(reduceStr, pVars.getCummItemBal(), pLeftOverItemUsedBal, pUnitPrice);

									//Update Cumulative item balance;
									new_partialItemUsedEvent = partialItemUsedEvent.split(",");
									pVars.setCummItemBal(new_partialItemUsedEvent[15]);

									//Update LeftOverItemUsedBal
									int item_diff = item_out - pLeftOverItemUsedBal;

									pVars.setLinkedListItem(Integer.toString(item_diff));										
									pLeftOverItemUsedBal = Integer.parseInt(pVars.getLinkedListItem());
									//pVars.setItemUsedPrice(new_partialItemUsedEvent[8]);

									pUnitPrice = pVars.getUnitPrice();

									item_out = item_diff;

									//Update ITEM Rec and Pass;
									ArrayList newLine = new ArrayList(Arrays.asList(new_partialItemUsedEvent));
									newLine.remove(newLine.size()-1);
									newLine.remove(newLine.size()-1);
									newLine.remove(newLine.size()-1);
									String line0 = newLine.toString().replaceAll(",\\s+", "\t");
									reduceStr = line0.substring(1, line0.length()-1);

									//System.out.println(partialItemUsedEvent);
									context.write(NullWritable.get(), new Text(partialItemUsedEvent));
								}

							} else {
								if (Integer.parseInt(pVars.getLinkedListItem()) > 0 ) {
									pUnitPrice = pVars.getItemUsedPrice();
								} else {
									pUnitPrice = pVars.getUnitPrice();	
								}

								String itemUsedEvent = createItemUsedRec(reduceStr, pVars.getCummItemBal(), pUnitPrice);
								String[] new_itemUsedEvent = itemUsedEvent.split(",");
								pVars.setCummItemBal(new_itemUsedEvent[15]);
								pVars.setItemUsedPrice(new_itemUsedEvent[16]);

								//System.out.println(itemUsedEvent);

								// Update leftOver Item Balance;
								int item_out_diff = 0;
								if (Integer.parseInt(pVars.getLinkedListItem()) != 0) {
									item_out_diff =  Integer.parseInt(pVars.getLinkedListItem()) - Integer.parseInt(new_itemUsedEvent[8]);
								}
								pVars.setLinkedListItem(Integer.toString(item_out_diff));
								context.write(NullWritable.get(), new Text(itemUsedEvent));

							}
						}	

					}
					pVars.setItemid(cItemid);
					pVars.setUid(cUid);
				}
			}
	}

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new ItemFlowFin(), args);

	}

	@Override
		public int run(String[] args) throws Exception {

			// When implementing tool
			Configuration conf = this.getConf();

			// Create job
			Job job = new Job(conf, "itemflowfin");
			job.setJarByClass(ItemFlowFin.class);


			FileInputFormat.addInputPath(job, new Path(args[0]));
			job.setInputFormatClass(TextInputFormat.class);
			FileOutputFormat.setOutputPath(job, new Path(args[1]));        
			job.setOutputFormatClass(TextOutputFormat.class);

			job.setMapperClass(ItemFlowFin.ItemFlowMap.class);
			job.setReducerClass(ItemFlowFin.ItemFlowReduce.class);

			// Specify key / value
			//job.setOutputKeyClass(LongWritable.class);
			//job.setOutputValueClass(Text.class);

			//set num of reducers;
			//job.setNumReduceTasks(1);
			// Mapper Class output
			job.setMapOutputKeyClass(NullWritable.class);
			job.setMapOutputValueClass(Text.class);
			//Reducer Class Output
			job.setOutputKeyClass(NullWritable.class);
			job.setOutputValueClass(Text.class);

			// compress to gzip file
			job.setOutputFormatClass(TextOutputFormat.class);
			TextOutputFormat.setCompressOutput(job, true);
			TextOutputFormat.setOutputCompressorClass(job, GzipCodec.class);

			// Execute job and return status
			return job.waitForCompletion(true) ? 0 : 1;
		} 
}


package com.funplus.analytics.thirdparty.constants;

/**
 * Created by balaji on 10/8/14.
 */
public class KochavaConstants {

    public static final String USAGE = "USAGE: java jar jarname KochavaProcessing startDate endDate outputDir appNamespace appId1 [appId2 .. appIdn]";

    public static final String[] FIELDS = { "campaign_id", "campaign_name", "click_date", "country_code", "creative", "install_date", "network_name", "site_id", "android_id","android_md5","android_sha1","imei","imei_md5","imei_sha1","udid","udid_md5","udid_sha1","idfa","idfv","Kochava_device_id","odin","mac", "fpid" };

    public static final String DIR_SEPERATOR = "/";
    public static final String FILE_SEPERATOR = "_";
    public static final String FILE_SUFFIX = "_kochava.gz";

    public static final String URL = "http://control.kochava.com/v1/cpi/get_installs?";

    public static final String URL_PARAMS = "&rtn_device_id_type=android_id,android_md5,android_sha1,imei,imei_md5,imei_sha1,udid,udid_md5,udid_sha1,idfa,idfv,Kochava_device_id,odin,mac&api_key=7EB29A90-DA76-4300-A055-8A56C83BED43&account=ummair.waheed&rtn_custom_id=fpid";

    public static final String START_DATE_URL_PARAM = "start_date=";
    public static final String END_DATE_URL_PARAM = "&end_date=";
    public static final String APPID_URL_PARAM = "&kochava_app_id=";

    public static final String INPUT_PARAMS = " - startDate(%s), endDate(%s), outputDir(%s), appNamespace(%s), appIds(%s)";
    public static final String PROCESS_STARTED = "INFO: Kochava processing started" + INPUT_PARAMS;
    public static final String PROCESS_COMPLETED = "INFO: Kochava processing completed" + INPUT_PARAMS;
    public static final String PROCESS_FAILED = "ERROR: Kochava processing failed (%s)";


}

package com.funplus.analytics.thirdparty;


import com.funplus.analytics.thirdparty.constants.KochavaConstants;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

/**
 * Created by balaji on 10/8/14.
 */


/**
 * This class get campaign information from kochava third party tracking
 * Input data is in JSON which is converted to TSV and then compressed to gz format.
 * Sample URI for output -
 * http://control.kochava.com/v1/cpi/get_installs?start_date=2014-09-15&end_date=2014-09-15
 * &kochava_app_id=kodt-zh53bafba8da48e
 * &rtn_device_id_type=android_id,android_md5,android_sha1,imei,imei_md5,imei_sha1,udid,udid_md5,udid_sha1,idfa,idfv,Kochava_device_id,odin,mac
 * &api_key=7EB29A90-DA76-4300-A055-8A56C83BED43&account=ummair.waheed&rtn_custom_id=fpid
 */
public class KochavaProcessing {

    public static void main(String[] args) {

        if(args.length < 5){
            usage();
            System.exit(1);
        }
        KochavaProcessing processing = new KochavaProcessing();
        try {
            List<String> argsList = Arrays.asList(args);
            System.out.println(String.format(KochavaConstants.PROCESS_STARTED,
                    argsList.get(0), argsList.get(1), argsList.get(2), argsList.get(3), argsList.subList(4, argsList.size())));
            processing.run(args);
            System.out.println(String.format(KochavaConstants.PROCESS_COMPLETED,
                    argsList.get(0), argsList.get(1), argsList.get(2), argsList.get(3), argsList.subList(4, argsList.size())));
        }
        catch (IOException e){
            System.out.println(String.format(KochavaConstants.PROCESS_FAILED, e.getMessage()));
            System.exit(1);
        }
    }

    /**
     * Validates the user input parameter for the processing.
     * Multiple APP id is supported. Please check usage.
     */
    private static void usage(){
        System.out.println(KochavaConstants.USAGE);
    }


    /**
     * Process the Kochana data for the given input params
     * Write the data into output directory
     * @param args
     * @throws IOException
     */
    private void run(String... args) throws IOException {
        String startDate = args[0];
        String endDate = args[1];
        String outputDir = args[2];
        String appNamespace = args[3];

        String dirPath = new StringBuilder(outputDir)
                .append(KochavaConstants.DIR_SEPERATOR).toString();
        FileUtils.forceMkdir(new File(dirPath));

        for(int argsIndex = 4; argsIndex < args.length; argsIndex++){
            String appId = args[argsIndex];
            String apiURL = getApiURL(startDate, endDate, appId);
            String fileOutputPath = new StringBuilder(dirPath).append(startDate).append(KochavaConstants.FILE_SEPERATOR)
                    .append(appId).append(KochavaConstants.FILE_SUFFIX).toString();
            System.out.println(String.format("INFO: Kochava processing - fileOutputPath(%s)", fileOutputPath));
            writeToFile(apiURL, fileOutputPath, appNamespace);
        }
    }

    /**
     * Construct Kochava API URL for given params
     * @param startDate
     * @param endDate
     * @param appId
     * @return
     */
    private String getApiURL(String startDate, String endDate, String appId){
        String apiURL = new StringBuilder(KochavaConstants.URL)
                .append(KochavaConstants.START_DATE_URL_PARAM).append(startDate)
                .append(KochavaConstants.END_DATE_URL_PARAM).append(endDate)
                .append(KochavaConstants.APPID_URL_PARAM).append(appId)
                .append(KochavaConstants.URL_PARAMS).toString();
        System.out.println(String.format("INFO: Kochava processing - API url(%s)", apiURL));
        return apiURL;
    }

    /**
     * Read data from API and write to GZIP format
     * @param apiURL
     * @param fileOutputPath
     * @throws IOException
     */
    private void writeToFile(String apiURL, String fileOutputPath, String appNamespace) throws IOException {
        File f = new File(fileOutputPath);
        FileOutputStream outputStream = null;
        OutputStreamWriter writer = null;
        try {
            outputStream = new FileOutputStream(f, false);
            writer = new OutputStreamWriter(new GZIPOutputStream(outputStream));
            JSONArray jsonArray = getJsonArray(apiURL);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String tsvLine = getDataInTsv(obj, appNamespace);
                for (char tsvChar : tsvLine.toCharArray()) {
                    writer.write((int) tsvChar);
                }
            }
        }
        finally {
            try{ writer.close();} catch (Exception e){}
            try{ outputStream.close();} catch (Exception e){}
        }
    }

    /**
     * Construct JSONArray from the API data.
     * @param url
     * @return
     * @throws IOException
     */
    private JSONArray getJsonArray(String url) throws IOException {

        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = new URL(url).openStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder text = new StringBuilder();
            String line = bufferedReader.readLine();
            while (line != null) {
                text.append(line + "\n");
                line = bufferedReader.readLine();
            }
            return new JSONArray(text.toString());
        }
        finally {
            try{ bufferedReader.close(); } catch (Exception e){};
            try{ inputStream.close(); } catch (Exception e){};
        }
    }


    /**
     * Convert to TSV format for given json object.
     * @param object
     * @return
     */
    private String getDataInTsv(JSONObject object, String appNamespace){
        StringBuilder tsvLine = new StringBuilder(UUID.randomUUID().toString())
                .append('\t').append(appNamespace);
        for (int i = 0; i < KochavaConstants.FIELDS.length; i++) {
            tsvLine.append('\t');
            tsvLine.append(object.optString(KochavaConstants.FIELDS[i]));
        }
        tsvLine.append('\n');
        return tsvLine.toString();
    }

}

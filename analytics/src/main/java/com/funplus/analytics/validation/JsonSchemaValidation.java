package com.funplus.analytics.validation;

/**
 * Created by balaji on 10/17/14.
 */

public enum JsonSchemaValidation {

    SCHEMA_NOT_FOUND,
    PROCESSING_ERROR,
    IO_ERROR,
    JSON_EXCEPTION,
    VALIDATION_FAILED;

}

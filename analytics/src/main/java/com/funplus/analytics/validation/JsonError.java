package com.funplus.analytics.validation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by balaji on 10/17/14.
 */
public class JsonError {

    private String error;
    private List<String> detailMessages;

    public JsonError(String error) {
        this.error = error;
        detailMessages = new ArrayList<String>();
    }

    public JsonError(String error, String detailMessage) {
        this.error = error;
        if(null == this.getDetailMessages()){
            detailMessages = new ArrayList<String>();
        }
        getDetailMessages().add(detailMessage);
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<String> getDetailMessages() {
        return detailMessages;
    }

    public void setDetailMessages(List<String> detailMessages) {
        this.detailMessages = detailMessages;
    }

    @Override
    public String toString() {
        return "JsonError{" +
                "error='" + error + '\'' +
                ", detailMessages=" + detailMessages +
                '}';
    }
}

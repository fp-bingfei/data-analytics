package com.funplus.analytics.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.funplus.analytics.Utilities;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by balaji on 10/16/14.
 */


public class JsonSchemaValidatorFactory {

    private static final Logger LOGGER = Logger.getLogger(JsonSchemaValidatorFactory.class);

    private static Map<String, JsonSchema> registeredSchemas = null;

    private JsonSchemaValidatorFactory(){
        registeredSchemas = new HashMap<String, JsonSchema>();
    }


    private static class JsonSchemaValidatorFactoryHolder {
        public static final JsonSchemaValidatorFactory INSTANCE = new JsonSchemaValidatorFactory();
    }

    public static JsonSchemaValidatorFactory getInstance() {
        return JsonSchemaValidatorFactoryHolder.INSTANCE;
    }

    /**
     * Scan the given paths and load the json schemas available to factory
     * @param conf
     */
    public void bootstrap(Configuration conf) {
        FileSystem fileSystem = null;
        FileStatus[] fileStatuses = null;
        String s3BucketName = conf.get("s3BucketName","s3://com.funplusgame.bidata");
        String s3SchemaPath = conf.get("s3SchemaPath");
        String specVersion = conf.get("specVersion");
        String app = conf.get("app");
        Path[] paths = Utilities.generateSchemaPathList(s3SchemaPath, specVersion, app);

        try{
            fileSystem = FileSystem.get(new URI(s3BucketName), conf);
            fileStatuses = fileSystem.listStatus(paths);
            for(FileStatus fileStatus : fileStatuses) {
                registerSchema(fileSystem, fileStatus.getPath(), specVersion);
            }
            LOGGER.info(String.format("Total schema registered - %d", registeredSchemas.size()));
            if(registeredSchemas.size() == 0){
                LOGGER.error("Registered schema cannot be empty");
                throw new RuntimeException("Registered schema cannot be empty");
            }
        } catch (URISyntaxException e) {
            throw new RuntimeException(String.format("URISyntaxException - %s", e.getMessage()));
        } catch (IOException e) {
            throw new RuntimeException(String.format("IOException - %s", e.getMessage()));
        } finally {
            try {
                fileSystem.close();
            } catch (Exception e){
                //do nothing
            }
        }
    }

    /**
     * Inhouse method to read json schema from the filesystem and register it to factory
     * @param fileSystem
     * @param path
     * @param specVersion
     */
    private void registerSchema(FileSystem fileSystem, Path path, String specVersion){
        DataInputStream in = null;
        BufferedReader br = null;
        JsonSchemaFactory jsonSchemaFactory = null;
        JsonNode jsonSchemaNode = null;
        JsonSchema jsonSchema = null;
        String eventName = FilenameUtils.removeExtension(Utilities.getFileNameFromPath(path.toString()));
        String schemaValidatorKey = generateSchemaValidatorKey(specVersion, eventName);
        StringBuilder outputBuilder = new StringBuilder();
        try{
            LOGGER.info(String.format("Schema setup started for schemaValidatorKey(%s) and schemaPath(%s)", schemaValidatorKey, path.toString()));
            in = fileSystem.open(path);
            br = new BufferedReader(new InputStreamReader(in));
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                outputBuilder.append(sCurrentLine);
            }
            LOGGER.debug("json --> " + outputBuilder.toString());
            jsonSchemaFactory = JsonSchemaFactory.byDefault();
            jsonSchemaNode = JsonLoader.fromString(outputBuilder.toString());
            jsonSchema = jsonSchemaFactory.getJsonSchema(jsonSchemaNode);
            registeredSchemas.put(schemaValidatorKey , jsonSchema);
            LOGGER.info(String.format("Schema setup completed for schemaValidatorKey(%s) and schemaPath(%s)", schemaValidatorKey, path.toString()));

        } catch (Exception e){
            LOGGER.error(String.format("Schema setup failed for schemaValidatorKey(%s) and schemaPath(%s) - %s", schemaValidatorKey, path.toString(), e.getMessage()));
            throw new RuntimeException(String.format("Error occurred during schema setup for schemaValidatorKey(%s) - %s",
                    schemaValidatorKey, e.getMessage()));
        }

    }

    /**
     * Validate the given jsonString against the event schema
     * @param schemaValidatorKey
     * @param inputJsonString
     * @return
     */
    public JsonError validate(String schemaValidatorKey, String inputJsonString){
        return validate(schemaValidatorKey, inputJsonString, null);
    }

    /**
     * Validate the given jsonString against the event schema
     * @param schemaValidatorKey
     * @param inputJsonString
     * @param defaultSchema
     * @return
     */
    public JsonError validate(String schemaValidatorKey, String inputJsonString, String defaultSchema){
        JsonNode inputJsonNode = null;
        JsonSchema jsonSchema = null;
        ProcessingReport report;
        try {
            jsonSchema = registeredSchemas.get(schemaValidatorKey);
            if(null == jsonSchema){
                if (defaultSchema == null || defaultSchema == "") {
                    return new JsonError(JsonSchemaValidation.SCHEMA_NOT_FOUND.toString());
                } else {
                    jsonSchema = registeredSchemas.get(defaultSchema);
                }
            }
            inputJsonNode = JsonLoader.fromString(inputJsonString);
            report = jsonSchema.validate(inputJsonNode);
            if(!report.isSuccess()){
                return constructJsonErrorForValidationFailed(report);
            }
        } catch (IOException e) {
            return new JsonError(JsonSchemaValidation.IO_ERROR.toString(), e.getMessage());
        } catch (ProcessingException e) {
            return new JsonError(JsonSchemaValidation.PROCESSING_ERROR.toString(), e.getMessage());
        }
        return  null;
    }

    /**
     * Generate schema object key based on given input
     * @param specVersion
     * @param eventName
     * @return
     */
    public String generateSchemaValidatorKey(String specVersion, String eventName){
        return new StringBuilder(specVersion).append("_").append(eventName).toString();
    }

    /**
     * Construct json Error for Validation Failed.
     * Need Special cases got user friendly message
     * @param report
     * @return
     */
    private JsonError constructJsonErrorForValidationFailed(ProcessingReport report) {
        JsonError jsonError = new JsonError(JsonSchemaValidation.VALIDATION_FAILED.toString());
        Iterator<ProcessingMessage> iterator = report.iterator();
        while (iterator.hasNext()){
            StringBuilder detailMessageBuilder = new StringBuilder();
            ProcessingMessage processingMessage = iterator.next();
            JsonNode instanceNode = processingMessage.asJson().get("instance");
            if(null != instanceNode){
                JsonNode pointerNode = instanceNode.get("pointer");
                if(null != pointerNode){
                    String pointerText = pointerNode.asText();
                    if(StringUtils.isNotBlank(pointerText)) {
                        detailMessageBuilder.append(StringUtils.replaceOnce(pointerText, "/","")).append(" ");
                    }
                }
            }
            detailMessageBuilder.append(processingMessage.getMessage());
            jsonError.getDetailMessages().add(StringUtils.remove(detailMessageBuilder.toString(), "\""));
        }
        return jsonError;
    }


}

-- TODO: history data
CREATE TABLE aws_spend_raw
(
  date                          DATE NOT NULL ENCODE DELTA,
  aws_key_management_service    DECIMAL(14,4) DEFAULT 0,
  amazon_kinesis                DECIMAL(14,4) DEFAULT 0,
  amazon_workspaces             DECIMAL(14,4) DEFAULT 0,
  cloudwatch                    DECIMAL(14,4) DEFAULT 0,
  data_pipeline                 DECIMAL(14,4) DEFAULT 0,
  dynamodb                      DECIMAL(14,4) DEFAULT 0,
  ebs                           DECIMAL(14,4) DEFAULT 0,
  ec2                           DECIMAL(14,4) DEFAULT 0,
  ec2_instance                  DECIMAL(14,4) DEFAULT 0,
  eip                           DECIMAL(14,4) DEFAULT 0,
  emr                           DECIMAL(14,4) DEFAULT 0,
  rds                           DECIMAL(14,4) DEFAULT 0,
  redshift                      DECIMAL(14,4) DEFAULT 0,
  s3                            DECIMAL(14,4) DEFAULT 0,
  ses                           DECIMAL(14,4) DEFAULT 0,
  simpledb                      DECIMAL(14,4) DEFAULT 0,
  sns                           DECIMAL(14,4) DEFAULT 0,
  sqs                           DECIMAL(14,4) DEFAULT 0
)
  DISTKEY(date)
  SORTKEY(date);

CREATE TABLE tab_aws_spend
(
  date                          DATE NOT NULL ENCODE DELTA,
  service                       VARCHAR(64) ENCODE BYTEDICT,
  cost_usd                      DECIMAL(14,4) DEFAULT 0
)
  DISTKEY(date)
  SORTKEY(date);

INSERT INTO tab_aws_spend
SELECT
    date
    ,'aws_key_management_service' as service
    ,aws_key_management_service as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'amazon_kinesis' as service
    ,amazon_kinesis as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'amazon_workspaces' as service
    ,amazon_workspaces as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'cloudwatch' as service
    ,cloudwatch as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'data_pipeline' as service
    ,data_pipeline as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'dynamodb' as service
    ,dynamodb as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'ebs' as service
    ,ebs as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'ec2' as service
    ,ec2 as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'ec2_instance' as service
    ,ec2_instance as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'eip' as service
    ,eip as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'emr' as service
    ,emr as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'rds' as service
    ,rds as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'redshift' as service
    ,redshift as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'s3' as service
    ,s3 as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'ses' as service
    ,ses as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'simpledb' as service
    ,simpledb as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'sns' as service
    ,sns as cost_usd
FROM aws_spend_raw
UNION
SELECT
    date
    ,'sqs' as service
    ,sqs as cost_usd
FROM aws_spend_raw;

-- TODO: data after 2015-03-01
CREATE TABLE aws_spend_raw_0301
(
  date                          DATE NOT NULL ENCODE DELTA,
  s3                            DECIMAL(14,4) DEFAULT 0,
  ebs                           DECIMAL(14,4) DEFAULT 0,
  ec2                           DECIMAL(14,4) DEFAULT 0,
  ec2_instance                  DECIMAL(14,4) DEFAULT 0,
  amazon_kinesis                DECIMAL(14,4) DEFAULT 0,
  cloudwatch                    DECIMAL(14,4) DEFAULT 0,
  data_pipeline                 DECIMAL(14,4) DEFAULT 0,
  dynamodb                      DECIMAL(14,4) DEFAULT 0,
  emr                           DECIMAL(14,4) DEFAULT 0,
  rds                           DECIMAL(14,4) DEFAULT 0,
  redshift                      DECIMAL(14,4) DEFAULT 0,
  ses                           DECIMAL(14,4) DEFAULT 0,
  simpledb                      DECIMAL(14,4) DEFAULT 0,
  sns                           DECIMAL(14,4) DEFAULT 0
)
  DISTKEY(date)
  SORTKEY(date);

truncate table aws_spend_raw_0301;
copy aws_spend_raw_0301
from 's3://com.funplus.bitest/robin/aws_cost.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t'
IGNOREHEADER 1;

DELETE FROM tab_aws_spend WHERE DATE >= '2015-03-01';

INSERT INTO tab_aws_spend
SELECT
    date
    ,'s3' as service
    ,s3 as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'ebs' as service
    ,ebs as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'ec2' as service
    ,ec2 as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'ec2_instance' as service
    ,ec2_instance as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'amazon_kinesis' as service
    ,amazon_kinesis as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'cloudwatch' as service
    ,cloudwatch as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'data_pipeline' as service
    ,data_pipeline as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'dynamodb' as service
    ,dynamodb as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'emr' as service
    ,emr as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'rds' as service
    ,rds as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'redshift' as service
    ,redshift as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'ses' as service
    ,ses as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'simpledb' as service
    ,simpledb as cost_usd
FROM aws_spend_raw_0301
UNION
SELECT
    date
    ,'sns' as service
    ,sns as cost_usd
FROM aws_spend_raw_0301;

select *
from aws_spend_raw_0301
limit 100;

select *
from aws_spend_raw
limit 100;
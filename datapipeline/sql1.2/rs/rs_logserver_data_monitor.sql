-- drop table if exists processed.raw_data_s3_test;
-- create table processed.raw_data_s3_test
-- (
-- 	json_str          VARCHAR(20000) ENCODE LZO
-- );

truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.ae.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.de.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.fr.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.nl.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.plinga.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.spil.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/royal.us.prod/2015/06/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

-- drop table if exists processed.events_raw_test;
-- CREATE TABLE processed.events_raw_test
-- (
-- 	load_hour           TIMESTAMP ENCODE DELTA,
-- 	id                  VARCHAR(64) ENCODE LZO,
-- 	app                 VARCHAR(64) ENCODE BYTEDICT,
-- 	ts                  TIMESTAMP ENCODE DELTA,
-- 	ts_str              VARCHAR(16) ENCODE LZO,
-- 	uid                 INTEGER,
-- 	uid_str             VARCHAR(32) ENCODE LZO,
-- 	snsid               VARCHAR(64) ENCODE LZO,
-- 	install_ts          TIMESTAMP ENCODE DELTA,
-- 	install_ts_str      VARCHAR(16) ENCODE LZO,
-- 	install_source      VARCHAR(1024) ENCODE BYTEDICT,
-- 	country_code        VARCHAR(16) ENCODE BYTEDICT,
-- 	ip                  VARCHAR(16) ENCODE LZO,
-- 	browser             VARCHAR(32) ENCODE BYTEDICT,
-- 	browser_version     VARCHAR(32) ENCODE BYTEDICT,
-- 	os                  VARCHAR(32) ENCODE BYTEDICT,
-- 	os_version          VARCHAR(64) ENCODE BYTEDICT,
-- 	event               VARCHAR(32) ENCODE BYTEDICT,
-- 	properties          VARCHAR(4096) ENCODE LZO
-- )
--   DISTKEY(snsid)
--   SORTKEY(event, ts);
--
-- truncate table processed.events_raw_test;

insert into processed.events_raw_test
(
    load_hour
    ,id
    ,app
    ,ts_str
    ,uid_str
    ,snsid
    ,install_ts_str
    ,install_source
    ,country_code
    ,ip
    ,browser
    ,browser_version
    ,os
    ,os_version
    ,event
    ,properties
)
select
    '2015-06-01 00:00:00' as load_hour
    ,md5(json_str) as id
    ,json_extract_path_text(json_str, '@key') as app
    ,json_extract_path_text(json_str, '@ts') as ts_string
    ,json_extract_path_text(json_str, 'uid') as uid_str
    ,substring(json_extract_path_text(json_str, 'snsid'), 1, 64) as snsid
    ,json_extract_path_text(json_str, 'install_ts') as install_ts_str
    ,json_extract_path_text(json_str, 'install_src') as install_src
    ,'--' as country_code
    ,json_extract_path_text(json_str, 'ip') as ip
    ,json_extract_path_text(json_str, 'browser') as browser
    ,json_extract_path_text(json_str, 'browser_version') as browser_version
    ,json_extract_path_text(json_str, 'os') as os
    ,substring(json_extract_path_text(json_str, 'os_version'), 1, 64) as os_version
    ,json_extract_path_text(json_str, 'event') as event
    ,json_str as properties
from processed.raw_data_s3_test;

update processed.events_raw_test
set ts = dateadd(second, CAST(ts_str AS INTEGER), '1970-01-01 00:00:00')
where ts_str != '' and load_hour = '2015-06-01 00:00:00';

update processed.events_raw_test
set install_ts = dateadd(second, CAST(install_ts_str AS INTEGER), '1970-01-01 00:00:00')
where install_ts_str != '' and load_hour = '2015-06-01 00:00:00';

update processed.events_raw_test
set uid = CAST(uid_str AS INTEGER)
where uid_str != '' and load_hour = '2015-06-01 00:00:00';



drop table if exists fluentd_data_rows;
create table fluentd_data_rows as
select app, event, trunc(ts) as date, extract(hour from ts) as hour, count(1) as count
from events
where trunc(ts) >= '2015-05-08'
group by 1,2,3,4;

drop table if exists logserver_data_rows;
create table logserver_data_rows as
select app, event, trunc(ts) as date, extract(hour from ts) as hour, count(1) as count
from processed.events_raw_test
where trunc(ts) >= '2015-05-08'
group by 1,2,3,4;

-- drop table if exists fluentd_logserver_data_rows;
-- create table fluentd_logserver_data_rows as
-- select l.app, l.event, l.date, l.hour, l.count as logserver_count, f.count as fluentd_count
-- from logserver_data_rows l
--     join fluentd_data_rows f on l.app = f.app and l.event = f.event and l.date = f.date and l.hour = f.hour;

create temp table cube as
select distinct l.app, l.event, l.date, l.hour
from logserver_data_rows l
union
select distinct f.app, f.event, f.date, f.hour
from fluentd_data_rows f;


drop table if exists fluentd_logserver_data_rows;
create table fluentd_logserver_data_rows as
select c.app, c.event, c.date, c.hour, coalesce(l.count, 0) as logserver_count, coalesce(f.count, 0) as fluentd_count
from cube c
    left join logserver_data_rows l on c.app = l.app and c.event = l.event and c.date = l.date and c.hour = l.hour
    left join fluentd_data_rows f on c.app = f.app and c.event = f.event and c.date = f.date and c.hour = f.hour;



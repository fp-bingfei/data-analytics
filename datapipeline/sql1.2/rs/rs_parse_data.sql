-- Dota history data

-- import data from S3 directly
-- drop table if exists processed.raw_data_s3;
-- create table processed.raw_data_s3
-- (
-- 	json_str          VARCHAR(20000) ENCODE LZO,
-- 	load_hour         TIMESTAMP ENCODE DELTA
-- )
--   SORTKEY(load_hour);

drop table if exists processed.raw_data_s3_test;
create table processed.raw_data_s3_test
(
	json_str          VARCHAR(20000) ENCODE LZO
);

truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

-- drop table if exists processed.events_raw_test;
-- CREATE TABLE processed.events_raw_test
-- (
-- 	load_hour           TIMESTAMP ENCODE DELTA,
-- 	id                  VARCHAR(64) ENCODE LZO,
-- 	app                 VARCHAR(64) ENCODE BYTEDICT,
-- 	ts                  TIMESTAMP ENCODE DELTA,
-- 	ts_str              VARCHAR(16) ENCODE LZO,
-- 	uid                 INTEGER,
-- 	uid_str             VARCHAR(32) ENCODE LZO,
-- 	snsid               VARCHAR(64) ENCODE LZO,
-- 	install_ts          TIMESTAMP ENCODE DELTA,
-- 	install_ts_str      VARCHAR(16) ENCODE LZO,
-- 	install_source      VARCHAR(1024) ENCODE BYTEDICT,
-- 	country_code        VARCHAR(16) ENCODE BYTEDICT,
-- 	ip                  VARCHAR(16) ENCODE LZO,
-- 	browser             VARCHAR(32) ENCODE BYTEDICT,
-- 	browser_version     VARCHAR(32) ENCODE BYTEDICT,
-- 	os                  VARCHAR(32) ENCODE BYTEDICT,
-- 	os_version          VARCHAR(64) ENCODE BYTEDICT,
-- 	event               VARCHAR(32) ENCODE BYTEDICT,
-- 	properties          VARCHAR(4096) ENCODE LZO
-- )
--   DISTKEY(snsid)
--   SORTKEY(event, ts);
--
-- truncate table processed.events_raw_test;

insert into processed.events_raw_test
(
    load_hour
    ,id
    ,app
    ,ts_str
    ,uid_str
    ,snsid
    ,install_ts_str
    ,install_source
    ,country_code
    ,ip
    ,browser
    ,browser_version
    ,os
    ,os_version
    ,event
    ,properties
)
select
    '2015-05-08 00:00:00' as load_hour
    ,md5(json_str) as id
    ,json_extract_path_text(json_str, '@key') as app
    ,json_extract_path_text(json_str, '@ts') as ts_string
    ,json_extract_path_text(json_str, 'uid') as uid_str
    ,json_extract_path_text(json_str, 'snsid') as snsid
    ,json_extract_path_text(json_str, 'install_ts') as install_ts_str
    ,json_extract_path_text(json_str, 'install_src') as install_src
    ,'--' as country_code
    ,json_extract_path_text(json_str, 'ip') as ip
    ,json_extract_path_text(json_str, 'browser') as browser
    ,json_extract_path_text(json_str, 'browser_version') as browser_version
    ,json_extract_path_text(json_str, 'os') as os
    ,json_extract_path_text(json_str, 'os_version') as os_version
    ,json_extract_path_text(json_str, 'event') as event
    ,json_str as properties
from processed.raw_data_s3_test;

update processed.events_raw_test
set ts = dateadd(second, CAST(ts_str AS INTEGER), '1970-01-01 00:00:00')
where ts_str != '';

update processed.events_raw_test
set install_ts = dateadd(second, CAST(install_ts_str AS INTEGER), '1970-01-01 00:00:00')
where install_ts_str != '';

update processed.events_raw_test
set uid = CAST(uid_str AS INTEGER)
where uid_str != '';




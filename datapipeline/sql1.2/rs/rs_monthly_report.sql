--------------------------------------------------------------------------------------------------------------------------------------------
--RS monthly report
--Version 1.2
--Author Robin
/**
Description:
This script is generate the tables for monthly report.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

-- Report type #1
-- 1) MAU
-- 2) Avg DAU
-- 3) Days Played Per month
-- 4) Revenue, APRDAU, ARPPU
-- 5) D1, D7, D30 Retention
-- 6) D30, D60, D90 LTV


-- Date table
-- year, month_name, days_of_month, date
create temp table tmp_month_days as
select
    extract(year from du.date) as year
    ,extract(month from du.date) as month_num
    ,case
      when extract(month from du.date) = 1	then 'January'
      when extract(month from du.date) = 2	then 'February'
      when extract(month from du.date) = 3	then 'March'
      when extract(month from du.date) = 4	then 'April'
      when extract(month from du.date) = 5	then 'May'
      when extract(month from du.date) = 6	then 'June'
      when extract(month from du.date) = 7	then 'July'
      when extract(month from du.date) = 8	then 'August'
      when extract(month from du.date) = 9	then 'September'
      when extract(month from du.date) = 10	then 'October'
      when extract(month from du.date) = 11	then 'November'
      when extract(month from du.date) = 12	then 'December'
    end as month_name
    ,count(distinct date) as month_days
from processed.fact_dau_snapshot du
group by 1,2;

-- User info table
-- app, uid, country, os, install_source_group, install_date
create temp table tmp_user_info as
select
    u.user_key
    ,u.app
    ,u.uid
    ,u.os
    ,u.country
    ,u.install_source_group as install_source
    ,u.install_date
    ,u.is_payer
    ,u.conversion_ts
from processed.dim_user u;

-- partly fact_dau_snapshot table to process
-- year, month_num, date, user_key, app, uid, install_date, revenue
create temp table tmp_dau_snapshot_monthly_report as
select
    extract(year from du.date) as year
    ,extract(month from du.date) as month_num
    ,du.date
    ,du.user_key
    ,du.app
    ,du.uid
    ,du.install_date
    ,case when du.date = du.install_date then 1 else 0 end as new_user
    ,case when revenue_usd_iap > 0 then 1 else 0 end as is_day_payer
    ,is_converted_today as is_new_payer
    ,du.revenue_usd as revenue
from processed.fact_dau_snapshot du
where du.date >= '2014-08-01';

-- User aggregation info
-- year, month_num, user_key, app, uid, played_days, revenue
create temp table tmp_agg_month_user_play as
select
    year
    ,month_num
    ,user_key
    ,app
    ,uid
    ,count(date) as play_days
    ,max(new_user) as new_user
    ,sum(is_day_payer) as month_payers
    ,max(is_new_payer) as is_new_payer
    ,sum(revenue) as revenue
from tmp_dau_snapshot_monthly_report
group by 1,2,3,4,5;

-- Merge all infos from different table
drop table if exists processed.tab_monthly_report;
create table processed.tab_monthly_report as
select
    mu.year
    ,mu.month_num
    ,md.month_name as month
    ,md.month_days
    ,ui.app
    ,ui.os
    ,ui.country
    ,ui.install_source
    ,ui.is_payer
    ,mu.new_user
    ,sum(new_user) as new_installs
    ,sum(month_payers) as month_payers
    ,sum(is_new_payer) as new_payers
    ,count(mu.user_key) as mau
    ,sum(mu.play_days) as month_dau
    ,sum(mu.play_days) as play_days
    ,sum(mu.revenue) as revenue
from tmp_agg_month_user_play mu
    join tmp_user_info ui on mu.user_key = ui.user_key
    join tmp_month_days md on mu.year = md.year and mu.month_num = md.month_num
group by 1,2,3,4,5,6,7,8,9,10;

delete from processed.tab_monthly_report where year = extract(year from current_date) and month_num = extract(month from current_date);

-- Retention (D1, D7, D30)
drop table if exists processed.tab_monthly_retention;
create table processed.tab_monthly_retention as
select
    md.year
	,md.month_num
	,md.month_name as month
	,r.app
	,r.os
	,r.country
	,r.install_source_group as install_source
	,sum(case when r.retention_days = 1 then new_users else 0 end) as d1_new_users
	,sum(case when r.retention_days = 7 then new_users else 0 end) as d7_new_users
	,sum(case when r.retention_days = 30 then new_users else 0 end) as d30_new_users
	,sum(case when r.retention_days = 1 then retained else 0 end) as d1_retained
	,sum(case when r.retention_days = 7 then retained else 0 end) as d7_retained
	,sum(case when r.retention_days = 30 then retained else 0 end) as d30_retained
from processed.agg_retention r
	join tmp_month_days md on extract(year from r.install_date) = md.year and extract(month from r.install_date) = md.month_num
where r.install_date >= '2014-08-01'
group by 1,2,3,4,5,6,7;

delete from processed.tab_monthly_retention where year = extract(year from current_date) and month_num = extract(month from current_date);

-- LTV (D30, D60, D90)
drop table if exists processed.tab_monthly_ltv;
create table processed.tab_monthly_ltv as
select
    md.year
	,md.month_num
	,md.month_name as month
	,l.app
	,l.os
	,l.country
	,l.install_source_group as install_source
	,sum(case when l.ltv_days = 30 then l.users else 0 end) as d30_users
	,sum(case when l.ltv_days = 60 then l.users else 0 end) as d60_users
	,sum(case when l.ltv_days = 90 then l.users else 0 end) as d90_users
	,sum(case when l.ltv_days = 30 then l.revenue else 0 end) as d30_revenue
	,sum(case when l.ltv_days = 60 then l.revenue else 0 end) as d60_revenue
	,sum(case when l.ltv_days = 90 then l.revenue else 0 end) as d90_revenue
from processed.agg_ltv l
	join tmp_month_days md on extract(year from l.install_date) = md.year and extract(month from l.install_date) = md.month_num
where l.install_date >= '2014-08-01'
group by 1,2,3,4,5,6,7;

delete from processed.tab_monthly_ltv where year = extract(year from current_date) and month_num = extract(month from current_date);

-- TODO: payer profile
/*
drop table tmp_payer_profile;
create table tmp_payer_profile as
select date, user_id, p.usd_adjusted as revenue
from processed.raw_revenue_iap iap join processed.ref_product_id p on iap.item = p.product_id
where iap.is_failed is null
union all
select date, user_id, p.usd_adjusted as revenue
from processed.raw_revenue_iab iab join processed.ref_product_id p on iab.item = p.product_id;

drop table processed.tab_payer_profile;
create table processed.tab_payer_profile as
select t.*, row_number() over (partition by year, month order by revenue desc) as row
from
(select extract(year from date) as year, md.month_name as month, user_id, sum(revenue) as revenue
from tmp_payer_profile t
    join tmp_month_days md on extract(year from date) = md.year and extract(month from date) = md.month_num
group by extract(year from date), extract(month from date), md.month_name, user_id) t;

alter table processed.tab_payer_profile add column payer_count integer;
alter table processed.tab_payer_profile add column payer_profile varchar(16);

drop table tmp_payer_count;
create table tmp_payer_count as
select year, month, count(1) as payer_count
from processed.tab_payer_profile
group by year, month;

update processed.tab_payer_profile
set payer_count = t.payer_count
from tmp_payer_count t
where processed.tab_payer_profile.year = t.year and processed.tab_payer_profile.month = t.month;

update processed.tab_payer_profile
set payer_profile = 'Whale'
where row / cast(payer_count as decimal) <= 0.1;

update processed.tab_payer_profile
set payer_profile = 'Dolphin'
where row / cast(payer_count as decimal) > 0.1 and row / cast(payer_count as decimal) <= 0.5;

update processed.tab_payer_profile
set payer_profile = 'Minnow'
where row / cast(payer_count as decimal) > 0.5;
*/

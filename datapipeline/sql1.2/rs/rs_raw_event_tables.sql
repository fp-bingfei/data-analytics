--------------------------------------------------------------------------------------------------------------------------------------------
--RS session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

-- TODO: check the data
-- drop table rs.processed.tab_spiderman_capture;
-- create table rs.processed.tab_spiderman_capture (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number_old" BIGINT NOT NULL DEFAULT 0,
--   "row_number_new" BIGINT NOT NULL DEFAULT 0,
--   "row_number_diff" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);

-- drop table if exists rs.processed.tab_spiderman_capture_history;
-- create table rs.processed.tab_spiderman_capture_history (
--   "record_date" date NOT NULL,
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(record_date, event, date, hour);

truncate table rs.processed.tab_spiderman_capture;
drop table rs.processed.tab_spiderman_capture_old;
alter table rs.processed.tab_spiderman_capture_new rename to tab_spiderman_capture_old;

create table rs.processed.tab_spiderman_capture_new (
  "event" varchar(32) NOT NULL,
  "date" date NOT NULL,
  "hour" SMALLINT NOT NULL,
  "row_number" BIGINT NOT NULL
)
  DISTKEY(event)
  SORTKEY(event, date, hour);

insert into rs.processed.tab_spiderman_capture_new
select event, trunc(ts) as date, extract(hour from ts) as hour, count(1)
from rs.public.events
group by event, trunc(ts), extract(hour from ts);

insert into rs.processed.tab_spiderman_capture
select n.event, n.date, n.hour, case when o.row_number is null then 0 else o.row_number end as row_number_old,
    n.row_number as row_number_new
from rs.processed.tab_spiderman_capture_new n left join rs.processed.tab_spiderman_capture_old o
    on n.event = o.event and n.date = o.date and n.hour = o.hour
union
select o.event, o.date, o.hour, o.row_number as row_number_old,
    case when n.row_number is null then 0 else n.row_number end as row_number_new
from rs.processed.tab_spiderman_capture_old o left join rs.processed.tab_spiderman_capture_new n
    on n.event = o.event and n.date = o.date and n.hour = o.hour;

update rs.processed.tab_spiderman_capture
set row_number_diff = row_number_new - row_number_old;

delete from rs.processed.tab_spiderman_capture_history where record_date = CURRENT_DATE;
insert into rs.processed.tab_spiderman_capture_history
(
    record_date
    ,event
    ,date
    ,hour
    ,row_number
)
select
    CURRENT_DATE as record_date
    ,event
    ,date
    ,hour
    ,row_number
from rs.processed.tab_spiderman_capture_new;

-- TODO: fact tables
----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------
-- insert data to be processed into tmp_fact_session
create temp table tmp_fact_session as
select  trunc(ts) as date_start
       ,MD5(app||uid) as user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,trunc(install_ts) AS install_date
     ,install_source
       ,CASE WHEN json_extract_path_text (properties,'session_id') = '' THEN NULL ELSE json_extract_path_text(properties,'session_id') END as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_start
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_end
       ,os
       ,os_version
       ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
       ,ip
       ,json_extract_path_text(properties,'langs') AS language
       ,json_extract_path_text(properties,'device') AS device
       ,browser
       ,browser_version
       ,json_extract_path_text(properties,'ab_data') as ab_test
       ,0 as session_length
from   public.events
where  event in ('newuser', 'session_start')
and    trunc(ts) >= (
                     select raw_data_start_date
                     from rs.processed.tmp_start_date
                    );

create temp table session_end as
select MD5(app||uid) as user_key
       ,app
       ,uid
       ,snsid
       ,json_extract_path_text(properties,'session_id') as session_id
       ,max(ts) as ts_end
       ,max(CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer)) as level_end
from   public.events
where  event = 'session_active'
and    trunc(ts) >= (
                     select raw_data_start_date
                     from rs.processed.tmp_start_date
                    )
and    json_extract_path_text(properties,'session_id') != ''
group by 2,3,4,5;

update tmp_fact_session
set ts_end = s.ts_end,
    level_end = s.level_end
from session_end s
where tmp_fact_session.user_key = s.user_key and tmp_fact_session.session_id = s.session_id;

update tmp_fact_session
set session_length = DATEDIFF('second', ts_start, ts_end);

delete from tmp_fact_session where level_end > 200;

--delete the historical data in case repeat loading
delete from  processed.fact_session
where  date_start >= (
                     select raw_data_start_date
                     from rs.processed.tmp_start_date
                );

-- insert into processed.fact_session
insert into processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,install_date
     ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,browser
       ,browser_version
       ,rc_bal_start
       ,rc_bal_end
       ,coin_bal_start
       ,coin_bal_end
       ,ab_test
       ,ab_variant
       ,session_length
)
select
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,install_date
     ,install_source
       ,null as app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,browser
       ,browser_version
       ,null as rc_bal_start
       ,null as rc_bal_end
       ,null as coin_bal_start
       ,null as coin_bal_end
       ,ab_test
       ,null as ab_variant
       ,session_length
from tmp_fact_session;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------
-- insert data to be processed into tmp_fact_revenue
create temp table tmp_fact_revenue as
select  trunc(ts) as date
        ,MD5(app||uid) as user_key
        ,app
        ,uid
        ,snsid
        ,ts
        ,CAST(
               CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'level')
               END AS integer
             ) AS level
        ,json_extract_path_text(properties,'payment_processor') AS payment_processor
        ,json_extract_path_text(properties,'product_id') AS product_id
        ,json_extract_path_text(properties,'product_name') AS product_name
        ,json_extract_path_text(properties,'product_type') AS product_type
        ,CAST(CASE WHEN split_part(json_extract_path_text(properties,'coins_in'), '.', 1) = '' THEN '0' ELSE split_part(json_extract_path_text(properties,'coins_in'), '.', 1) END AS integer) as coins_in
        ,CAST(CASE WHEN split_part(json_extract_path_text(properties,'rc_in'), '.', 1) = '' THEN '0' ELSE split_part(json_extract_path_text(properties,'rc_in'), '.', 1) END AS integer) as rc_in
        ,json_extract_path_text(properties,'currency') AS currency
        ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL)/100::DECIMAL(14,4) AS amount
        ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL)/100::DECIMAL(14,4) AS usd
        ,0::DECIMAL(14,4) AS usd_iap
        ,0::DECIMAL(14,4) AS usd_ads
        ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    public.events r
where   event = 'payment'
and     trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                     );

update tmp_fact_revenue
set amount = amount * 100
where app = 'royal.plinga.prod' and ts <= '2015-06-19 03:09:19';

update tmp_fact_revenue
set currency = 'EUR',
    amount = amount / 40
where currency = 'EGB';

update tmp_fact_revenue
set currency = 'USD',
    amount = amount / 7
where currency = 'WCn';

delete from tmp_fact_revenue where level > 200;

update tmp_fact_revenue
set usd = amount * case when c.factor is null then 0 else c.factor end
from public.currency c
where tmp_fact_revenue.currency = c.currency and tmp_fact_revenue.date = c.dt;

update tmp_fact_revenue
set usd_iap = usd
where product_id not in ('trial', 'flash_trial');

update tmp_fact_revenue
set usd_ads = usd
where product_id in ('trial', 'flash_trial');

--delete the historical data in case repeat loading
delete from  processed.fact_revenue
where  date >= (
                     select raw_data_start_date
                     from   rs.processed.tmp_start_date
                );

-- insert into processed.fact_revenue
insert into processed.fact_revenue
(
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,ts
        ,level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,usd_iap
        ,usd_ads
        ,transaction_id
)
select
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,ts
        ,level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,usd_iap
        ,usd_ads
        ,transaction_id
from tmp_fact_revenue;

create temp table tmp_duplicated_transaction_id as
select transaction_id, count(1)
from processed.fact_revenue
group by transaction_id
having count(1) > 1;

create temp table tmp_duplicated_transaction_id_rows as
select t.*, row_number() over (partition by transaction_id order by ts) row_number
from
(select r.*
from processed.fact_revenue r join tmp_duplicated_transaction_id d on r.transaction_id = d.transaction_id) t;

create temp table tmp_duplicated_transaction_id_first_row as
select *
from tmp_duplicated_transaction_id_rows
where row_number = 1;

alter table tmp_duplicated_transaction_id_first_row drop column row_number;

delete
from processed.fact_revenue
using tmp_duplicated_transaction_id d
where processed.fact_revenue.transaction_id = d.transaction_id;

insert into processed.fact_revenue
select *
from tmp_duplicated_transaction_id_first_row;

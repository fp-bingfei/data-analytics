-- TODO: rc transaction
delete from rs.processed.rc_transaction
where date >=(
                select raw_data_start_date
                from rs.processed.tmp_start_date
             );

insert into rs.processed.rc_transaction
(
    date
    ,transaction_type
    ,app
    ,app_version
    ,os
    ,country
    ,install_source
    ,install_source_group
    ,browser
    ,level
    ,ab_test
    ,ab_variant
    ,is_payer
    ,location
    ,action
    ,action_detail
    ,rc_in
    ,rc_out
    ,user_cnt
)
select
    trunc(e.ts)
    ,null as transaction_type
    ,e.app
    ,null as app_version
    ,null as os
    ,u.country
    ,null as install_source
    ,u.install_source_group
    ,null as browser
    ,cast(json_extract_path_text(e.properties,'level') as smallint) as level
    ,null as ab_test
    ,null as ab_variant
    ,d.is_payer
    ,json_extract_path_text(e.properties,'location') as location
    ,json_extract_path_text(e.properties,'action') as action
    ,json_extract_path_text(e.properties,'action_detail') as action_detail
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_in')='' then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT)) as rc_in
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_out')='' then '0' else json_extract_path_text(e.properties,'rc_out') end as BIGINT)) as rc_out
    ,count(distinct u.user_key) as user_cnt
from events e
    join processed.dim_user u on MD5(e.app||e.uid) = u.user_key
    join processed.fact_dau_snapshot d on trunc(e.ts) = d.date and MD5(e.app||e.uid) = d.user_key
where e.event = 'rc_transaction' and u.is_tester = 0
and     trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                     )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

update rs.processed.rc_transaction
set transaction_type =
    case
        when rc_in > 0 then 'rc_in'
        when rc_out > 0 then 'rc_out'
        else 'Unknown'
    end
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              );

delete from rs.processed.rc_transaction where level > 200;
delete from rs.processed.rc_transaction where date = CURRENT_DATE;

-- create table rs.processed.rc_transaction_toolsEditUser as
-- select *
-- from rs.processed.rc_transaction
-- where action = 'toolsEditUser';

delete
from rs.processed.rc_transaction
where action = 'toolsEditUser';

-- create table rs.processed.rc_transaction_buyHolidayCountDown as
-- select *
-- from rs.processed.rc_transaction
-- where action = 'buyHolidayCountDown' and rc_in > 0;

delete
from rs.processed.rc_transaction
where action = 'buyHolidayCountDown' and rc_in > 0;

-- create table rs.processed.rc_transaction_sendPaidGiftAction as
-- select *
-- from rs.processed.rc_transaction
-- where action = 'sendPaidGiftAction' and rc_in > 0;

delete
from rs.processed.rc_transaction
where action = 'sendPaidGiftAction' and rc_in > 0;

-- rc_transaction for every user
delete from rs.processed.rc_transaction_user
where date >=(
                select raw_data_start_date
                from rs.processed.tmp_start_date
             );

insert into rs.processed.rc_transaction_user
(
    date
    ,transaction_type
    ,user_key
    ,app
    ,app_version
    ,os
    ,country
    ,install_source
    ,install_source_group
    ,browser
    ,level
    ,ab_test
    ,ab_variant
    ,is_payer
    ,location
    ,action
    ,action_detail
    ,rc_in
    ,rc_out
)
select
    trunc(e.ts)
    ,null as transaction_type
    ,u.user_key
    ,e.app
    ,null as app_version
    ,null as os
    ,u.country
    ,null as install_source
    ,u.install_source_group
    ,null as browser
    ,cast(json_extract_path_text(e.properties,'level') as smallint) as level
    ,null as ab_test
    ,null as ab_variant
    ,d.is_payer
    ,json_extract_path_text(e.properties,'location') as location
    ,json_extract_path_text(e.properties,'action') as action
    ,json_extract_path_text(e.properties,'action_detail') as action_detail
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_in')='' then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT)) as rc_in
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_out')='' then '0' else json_extract_path_text(e.properties,'rc_out') end as BIGINT)) as rc_out
from events e
    join processed.dim_user u on MD5(e.app||e.uid) = u.user_key
    join processed.fact_dau_snapshot d on trunc(e.ts) = d.date and MD5(e.app||e.uid) = d.user_key
where e.event = 'rc_transaction' and u.is_tester = 0
and     trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                     )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;

update rs.processed.rc_transaction_user
set transaction_type =
    case
        when rc_in > 0 then 'rc_in'
        when rc_out > 0 then 'rc_out'
        else 'Unknown'
    end
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              );

delete from rs.processed.rc_transaction_user where level > 200;
delete from rs.processed.rc_transaction_user where date = CURRENT_DATE;

-- TODO: merge small action_detail
-- create temp table rc_transaction_action_detail_rank as
-- select *, row_number() over (partition by transaction_type, action order by rc_in desc, rc_out desc) as rank
-- from
-- (select transaction_type, action, action_detail, sum(rc_in) as rc_in, sum(rc_out) as rc_out
-- from rs.processed.rc_transaction
-- where CURRENT_DATE - date <= 30
-- group by transaction_type, action, action_detail) t;
--
-- update rs.processed.rc_transaction
-- set action_detail = r.action || ' : Other'
-- from rc_transaction_action_detail_rank r
-- where rs.processed.rc_transaction.transaction_type = r.transaction_type and
--     rs.processed.rc_transaction.action = r.action and
--     rs.processed.rc_transaction.action_detail = r.action_detail and
--  

-- coins_transaction for every user
delete from rs.processed.coins_transaction_user
where date >=(
                select raw_data_start_date
                from rs.processed.tmp_start_date
             );

insert into rs.processed.coins_transaction_user
(
    date
    ,transaction_type
    ,user_key
    ,app
    ,app_version
    ,os
    ,country
    ,install_source
    ,install_source_group
    ,browser
    ,level
    ,ab_test
    ,ab_variant
    ,is_payer
    ,location
    ,action
    ,action_detail
    ,coins_in
    ,coins_out
)
select
    trunc(e.ts)
    ,null as transaction_type
    ,u.user_key
    ,e.app
    ,null as app_version
    ,null as os
    ,u.country
    ,null as install_source
    ,u.install_source_group
    ,null as browser
    ,cast(json_extract_path_text(e.properties,'level') as smallint) as level
    ,null as ab_test
    ,null as ab_variant
    ,d.is_payer
    ,json_extract_path_text(e.properties,'location')::varchar(64) as location
    ,json_extract_path_text(e.properties,'action')::varchar(64) as action
    ,json_extract_path_text(e.properties,'action_detail')::varchar(256) as action_detail
    ,sum(cast(case when json_extract_path_text(e.properties,'coins_in')='' then '0' else json_extract_path_text(e.properties,'coins_in') end as BIGINT)) as coins_in
    ,sum(cast(case when json_extract_path_text(e.properties,'coins_out')='' then '0' else json_extract_path_text(e.properties,'coins_out') end as BIGINT)) as coins_out
from events e
    join processed.dim_user u on MD5(e.app||e.uid) = u.user_key
    join processed.fact_dau_snapshot d on trunc(e.ts) = d.date and MD5(e.app||e.uid) = d.user_key
where e.event = 'coins_transaction' and u.is_tester = 0
and     trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                     )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;

update rs.processed.coins_transaction_user
set transaction_type =
    case
        when coins_in > 0 then 'coins_in'
        when coins_out > 0 then 'coins_out'
        else 'Unknown'
    end
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              );

delete from rs.processed.coins_transaction_user where level > 200;
delete from rs.processed.coins_transaction_user where date = CURRENT_DATE;


-- TODO: item transaction
-- aggregation on item_transaction data
delete from rs.processed.agg_events_raw_item_transaction
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              );

insert into rs.processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,controller
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action')::varchar(64) as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'controller')::varchar(64) as controller
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction'
and     trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                     )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;

-- table for Item Transaction report
delete from rs.processed.item_transaction
where date >=(
                select raw_data_start_date
                from rs.processed.tmp_start_date
             );

insert into rs.processed.item_transaction
(
    date
    ,transaction_type
    ,app
    ,app_version
    ,os
    ,country
    ,install_source
    ,install_source_group
    ,browser
    ,level_range
    ,ab_test
    ,ab_variant
    ,is_payer
    ,location
    ,action
    ,action_detail
    ,controller
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,user_cnt
)
select
    i.date
    ,null as transaction_type
    ,i.app
    ,null as app_version
    ,null as os
    ,u.country
    ,null as install_source
    ,u.install_source_group
    ,null as browser
    ,cast(i.level - ((i.level - 1) % 10) as varchar) || ' ~ ' || cast(i.level - ((i.level - 1) % 10) + 9 as varchar) as level_range
    ,i.ab_test
    ,i.ab_variant
    ,d.is_payer
    ,i.location
    ,i.action
    ,i.action_detail
    ,i.controller
    ,i.item_type
    ,i.item_class
    ,i.item_name
    ,i.item_id
    ,sum(i.item_in * count) as  item_in
    ,sum(i.item_out * count) as item_out
    ,count(distinct u.user_key) as user_cnt
from processed.agg_events_raw_item_transaction i
    join processed.dim_user u on MD5(i.app||i.uid) = u.user_key
    join processed.fact_dau_snapshot d on i.date = d.date and MD5(i.app||i.uid) = d.user_key
where u.is_tester = 0 and i.level <= 200
and   i.date >= (
                select raw_data_start_date
                from rs.processed.tmp_start_date
              )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21;

update rs.processed.item_transaction
set transaction_type =
    case
        when item_in > 0 then 'item_in'
        when item_out > 0 then 'item_out'
        else 'Unknown'
    end
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              );

delete from rs.processed.item_transaction where date = CURRENT_DATE;

-- TODO: quests
delete from processed.fact_quests
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              );

insert into processed.fact_quests
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,snsid
    ,level
    ,quest_id
    ,action
    ,task_id
    ,rc_out
    ,coins_in
)
select
    trunc(ts) as date
    ,ts
    ,MD5(app||uid) as user_key
    ,app
    ,uid
    ,snsid
    ,cast(json_extract_path_text(properties,'level') as smallint) as level
    ,json_extract_path_text(properties,'quest_id') as quest_id
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'task_id') as task_id
    ,case when json_extract_path_text(properties,'rc_out') = '' then 0 else cast(json_extract_path_text(properties,'rc_out') as INTEGER) end as rc_out
    ,case when json_extract_path_text(properties,'coins_in') = '' then 0 else cast(json_extract_path_text(properties,'coins_in') as INTEGER) end as coins_in
from events
where event = 'quest' and
    trunc(ts) >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
                 );

delete
from processed.fact_quests
using processed.dim_user u
where processed.fact_quests.user_key = u.user_key and u.is_tester != 0;

-- remove duplicated records for quest
create temp table fact_quests_duplicated_records as
select user_key, quest_id, action, task_id, count(1) as count
from processed.fact_quests
group by 1,2,3,4
having count(1) > 1;

delete
from processed.fact_quests
using fact_quests_duplicated_records d
where processed.fact_quests.user_key = d.user_key;

-- create agg_quests
create temp table quests_start_finish as
select
    cast(s.date as VARCHAR) as date
    ,s.date as date_start
    ,f.date as date_finish
    ,s.user_key
    ,s.app
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,u.install_date
    ,u.last_login_ts
    ,s.level
    ,s.quest_id
    ,s.ts as ts_start
    ,f.ts as ts_finish
    ,f.coins_in
from
(select *
from processed.fact_quests
where action = 'start_quest') s
left join
processed.dim_user u on s.user_key = u.user_key
left join
(select *
from processed.fact_quests
where action = 'finish_quest') f
on s.quest_id = f.quest_id and s.user_key = f.user_key;

delete from quests_start_finish where date_start < '2014-08-01';
delete from quests_start_finish where date_start = CURRENT_DATE;
delete from quests_start_finish where ts_start > ts_finish;
delete from quests_start_finish where country is null;
delete from quests_start_finish where level > 200;

drop table if exists processed.agg_quests;
create table processed.agg_quests as
select
    date
    ,date_start
    ,date_finish
    ,app
    ,country
    ,install_source
    ,install_source_group
    ,install_date
    ,trunc(last_login_ts) as last_login_date
    ,level
    ,quest_id
    ,count(1) as start_count
    ,sum(case when ts_finish is null then 0 else 1 end) as finish_count
    ,sum(coalesce(datediff(minutes, ts_start, ts_finish), 0)) as duration_minutes
from quests_start_finish
group by 1,2,3,4,5,6,7,8,9,10,11;

-- create skip_quests
drop table if exists processed.agg_skip_quests;
create table processed.agg_skip_quests as
select
    q.date
    ,q.app
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,u.install_date
    ,trunc(u.last_login_ts) as last_login_date
    ,q.level
    ,q.quest_id
    ,q.task_id
    ,count(1) as skip_task_count
    ,sum(q.rc_out) as rc_out
from processed.fact_quests q
    join processed.dim_user u on q.user_key = u.user_key
where action = 'skip_task'
group by 1,2,3,4,5,6,7,8,9,10;

-- TODO: lapsed_users
truncate table processed.lapsed_users;
insert into processed.lapsed_users
(
    app
    ,app_version
    ,os
    ,country
    ,install_date
    ,install_source
    ,install_source_group
    ,browser
    ,level
    ,ab_test
    ,ab_variant
    ,is_payer
    ,last_login_date
    ,lapsed_days
    ,user_cnt
)
select
    app
    ,null as app_version
    ,null as os
    ,country
    ,install_date
    ,install_source
    ,install_source_group
    ,null as browser
    ,level
    ,null as ab_test
    ,null as ab_variant
    ,is_payer
    ,trunc(last_login_ts) as last_login_date
    ,trunc(CONVERT_TIMEZONE('UTC', CURRENT_DATE)) - trunc(last_login_ts) as lapsed_days
    ,count(distinct user_key) as user_cnt
from processed.dim_user
where level >= 1 and level <= 200
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

-- lapsed_users_snsid
-- for RS export lapsed user snsid requirement
truncate table processed.lapsed_users_snsid;
insert into processed.lapsed_users_snsid
(
    app
    ,snsid
    ,install_date
    ,install_source
    ,install_source_group
    ,browser
    ,country
    ,language
    ,os
    ,level
    ,is_payer
    ,last_login_ts
    ,lapsed_days
)
select
    app
    ,snsid
    ,install_date
    ,install_source
    ,install_source_group
    ,browser
    ,country
    ,language
    ,os
    ,level
    ,is_payer
    ,last_login_ts
    ,trunc(CONVERT_TIMEZONE('UTC', CURRENT_DATE)) - trunc(last_login_ts) as lapsed_days
from processed.dim_user
where level >= 1 and level <= 200;
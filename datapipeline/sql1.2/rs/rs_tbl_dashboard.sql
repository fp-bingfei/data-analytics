--------------------------------------------------------------------------------------------------------------------------------------------
--RS tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate kpi,retention and LTV tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_kpi
---------------------------------------------------------------------------------------------------------------------------------------------- 

------delete the historical data in case repeat loading 
truncate table processed.agg_kpi;

insert into processed.agg_kpi
(
      date 
      ,app 
      ,app_version
      ,install_source
      ,install_source_group
      ,level_range
      ,browser
      ,country          
      ,os
      ,language
      ,is_payer
      ,ab_test
      ,is_whale
      ,new_installs
      ,dau
      ,new_payers
      ,today_payers
      ,revenue
      ,revenue_iap
      ,revenue_ads
      ,session_cnt
      ,playtime_sec
) 
select  date 
      ,d.app 
      ,d.app_version
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar)
      ,d.browser
      ,d.country          
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.is_whale
      ,sum(d.is_new_user) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd_iap > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_ads) as revenue_ads
      ,sum(d.session_cnt) as session_cnt
      ,sum(d.playtime_sec) as playtime_sec
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= '2014-07-16' and u.is_tester = 0
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_retention
---------------------------------------------------------------------------------------------------------------------------------------------- 

-- create temp table for player days

create temp table player_day as
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day
union all
select 120 as day
union all
select 150 as day
union all
select 180 as day;


-- create temp table for new users in each install_date
create temp table tmp_new_users as
select  u.install_date
       ,u.os
       ,u.browser
       ,u.country
       ,u.app
       ,u.install_source
       ,u.install_source_group
       ,u.is_payer 
       ,count(u.user_key) as new_users
from  processed.dim_user u
where u.install_date >= '2014-07-16' and u.is_tester = 0
group by 1,2,3,4,5,6,7,8;

-- create temp table to get the last date in dau table.
create temp table last_date as
select max(date) as last_date
from processed.fact_dau_snapshot;

-- create cube to calculate the retention
create temp table player_day_cube as
select rd.day as retention_days
      ,nu.install_date
      ,nu.os
      ,nu.browser
      ,nu.country
      ,nu.app
      ,nu.install_source
      ,nu.install_source_group
      ,nu.is_payer
      ,nu.new_users
from tmp_new_users nu 
join player_day rd on 1 = 1
join last_date d on 1=1
where DATEDIFF('day', nu.install_date, d.last_date) >= rd.day;

-- insert into agg_retention table
truncate table processed.agg_retention;
insert into processed.agg_retention
(
  retention_days
  ,install_date
  ,os
  ,browser
  ,country
  ,app
  ,install_source
  ,install_source_group
  ,is_payer
  ,new_users
  ,retained
)
select
           cube.retention_days
          ,cube.install_date
          ,cube.os
          ,cube.browser
          ,cube.country
          ,cube.app
          ,cube.install_source
          ,cube.install_source_group
          ,cube.is_payer
          ,cube.new_users
          ,coalesce(r.retained, 0) as retained
from      player_day_cube cube
left join
          (
             SELECT   DATEDIFF('day', du.install_date, du.date) AS retention_days
                     ,u.install_date
                     ,u.os
                     ,u.browser
                     ,u.country
                     ,u.app
                     ,u.install_source
                     ,u.install_source_group
                     ,u.is_payer
                     ,count(distinct du.user_key) as retained
             FROM    processed.fact_dau_snapshot du 
             join    processed.dim_user u on du.user_key = u.user_key
             where   DATEDIFF('day', du.install_date, du.date) in
                                     (
                                       select day
                                       from player_day
                                      )
             group by 1,2,3,4,5,6,7,8,9
          ) r
on  cube.retention_days = r.retention_days
    and cube.install_date = r.install_date
    and cube.os = r.os
    and cube.browser = r.browser
    and cube.country = r.country
    and cube.app = r.app
    and cube.install_source = r.install_source
    and cube.install_source_group = r.install_source_group
    and cube.is_payer = r.is_payer;


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_ltv
----------------------------------------------------------------------------------------------------------------------------------------------

-- create ltv cube table
create temp table agg_ltv_cube as
SELECT      d.day as ltv_days
           ,trunc(u.install_ts) as install_date
           ,u.os
           ,u.browser
           ,u.country
           ,u.app
           ,u.install_source
           ,u.install_source_group
           ,u.is_payer
           ,u.user_key
FROM        processed.dim_user u
     join   player_day d
       on   DATEDIFF('day', u.install_date, (select last_date from last_date)) >= d.day
where u.install_date >= '2014-07-16' and u.is_tester = 0;

-- insert into ltv table
truncate table processed.agg_ltv;
insert into processed.agg_ltv
(
  ltv_days
  ,install_date
  ,os
  ,browser
  ,country
  ,app
  ,install_source
  ,install_source_group
  ,is_payer
  ,users
  ,revenue
)
SELECT
            c.ltv_days
           ,c.install_date
           ,c.os
           ,c.browser
           ,c.country
           ,c.app
           ,c.install_source
           ,c.install_source_group
           ,c.is_payer
           ,count(distinct c.user_key) as users
           ,sum(COALESCE(du.revenue_usd, 0)) as revenue
FROM        agg_ltv_cube c
left join   processed.fact_dau_snapshot du
       on   c.user_key = du.user_key and DATEDIFF('day', c.install_date, du.date) <= c.ltv_days
group by 1,2,3,4,5,6,7,8,9;

delete from processed.agg_ltv where install_date < '2014-07-16';

delete from processed.agg_ltv
using last_date d
where DATEDIFF('day', install_date, d.last_date) < ltv_days;

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.tab_iap (iap report)
----------------------------------------------------------------------------------------------------------------------------------------------
delete from processed.agg_iap
where date >=(
                select start_date
                from rs.processed.tmp_start_date
             );

insert into processed.agg_iap
(
    date
    ,app
    ,date_str
    ,date_week_str
    ,date_month_str
    ,level
    ,install_date
    ,install_source
    ,install_source_group
    ,os
    ,country
    ,ab_test
    ,ab_variant
    ,conversion_purchase
    ,product_type
    ,product_id
    ,revenue_usd
    ,purchase_cnt
    ,purchase_user_cnt
)
SELECT
    r.date
    ,r.app
    ,regexp_replace( substring(r.date, 1, 10), '-', '/' ) as date_str
    ,regexp_replace( substring(r.date - extract(dayofweek from r.date), 1, 10), '-', '/' ) as date_week_str
    ,regexp_replace( substring(r.date - extract(day from r.date) + 1, 1, 7), '-', '/' ) as date_month_str
    ,r.level
    ,u.install_date
    ,u.install_source
    ,u.install_source_group
    ,s.os
    ,s.country
    ,s.ab_test
    ,s.ab_variant
    ,case when u.conversion_ts = r.ts then 1 else 0 end as conversion_purchase
    ,case when product_type = 'Coins' then 'coins' else product_type end as product_type
    ,product_id
    ,sum(usd) as revenue_usd
    ,count(1) as purchase_cnt
    ,count(distinct r.user_key) as purchase_user_cnt
FROM processed.fact_revenue r
    join processed.fact_dau_snapshot s on r.user_key = s.user_key and r.date = s.date
    join processed.dim_user u on u.user_key = r.user_key
where r.date >= (select start_date from rs.processed.tmp_start_date) and u.is_tester = 0
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

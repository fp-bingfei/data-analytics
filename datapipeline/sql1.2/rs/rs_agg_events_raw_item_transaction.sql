-- drop table if exists processed.agg_events_raw_item_transaction;
-- CREATE TABLE processed.agg_events_raw_item_transaction
-- (
--   app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
--   date                  DATE NOT NULL ENCODE DELTA,
--   uid                   INTEGER NOT NULL,
--   snsid                 VARCHAR(64) NOT NULL ENCODE LZO,
--
--   level                 SMALLINT,
--   ab_test               VARCHAR(64) ENCODE BYTEDICT,
--   ab_variant            VARCHAR(8) ENCODE BYTEDICT,
--
--   location              VARCHAR(64) ENCODE BYTEDICT,
--   action                VARCHAR(64) ENCODE BYTEDICT,
--   action_detail         VARCHAR(256) ENCODE BYTEDICT,
--   item_type             VARCHAR(64) ENCODE BYTEDICT,
--   item_class            VARCHAR(64) ENCODE BYTEDICT,
--   item_name             VARCHAR(64) ENCODE BYTEDICT,
--   item_id               VARCHAR(64) ENCODE BYTEDICT,
--
--   item_in               INTEGER,
--   item_out              INTEGER,
--   count                 INTEGER
-- )
--   DISTKEY(uid)
--   SORTKEY(date, app, uid, snsid, level, ab_test, location, action, action_detail, item_type,
--         item_class, item_name, item_id);

-- TODO: process history data

-- TODO: trunc(ts) < '2014-08-01'
insert into processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction' and trunc(ts) < '2014-08-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

-- TODO: trunc(ts) >= '2014-08-01' and trunc(ts) < '2014-09-01'
insert into processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction' and trunc(ts) >= '2014-08-01' and trunc(ts) < '2014-09-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

-- TODO: trunc(ts) >= '2014-09-01' and trunc(ts) < '2014-10-01'
insert into processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction' and trunc(ts) >= '2014-09-01' and trunc(ts) < '2014-10-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

-- TODO: trunc(ts) >= '2014-10-01' and trunc(ts) < '2014-11-01'
insert into processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction' and trunc(ts) >= '2014-10-01' and trunc(ts) < '2014-11-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

-- TODO: trunc(ts) >= '2014-11-01' and trunc(ts) < '2014-12-01'
insert into processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction' and trunc(ts) >= '2014-11-01' and trunc(ts) < '2014-12-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

-- TODO: trunc(ts) >= '2014-12-01' and trunc(ts) < '2015-01-01'
insert into processed.agg_events_raw_item_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,level
    ,ab_test
    ,ab_variant
    ,location
    ,action
    ,action_detail
    ,item_type
    ,item_class
    ,item_name
    ,item_id
    ,item_in
    ,item_out
    ,count
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'ab_data') as ab_test
    ,null as ab_variant
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,json_extract_path_text(properties,'item_type') as item_type
    ,json_extract_path_text(properties,'item_class') as item_class
    ,json_extract_path_text(properties,'item_name') as item_name
    ,json_extract_path_text(properties,'item_id') as item_id
    ,cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as BIGINT) as item_in
    ,cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as BIGINT) as item_out
    ,count(1) as count
from events
where event = 'item_transaction' and trunc(ts) >= '2014-12-01' and trunc(ts) < '2015-01-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;


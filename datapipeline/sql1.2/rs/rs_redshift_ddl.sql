-- raw_data.raw_sendgrid_daily
CREATE  TABLE raw_data.raw_sendgrid_daily (
  snsid varchar(32),
  email varchar(128),
  category varchar(128),
  uid varchar(64),
  app_id varchar(64),
  time timestamp,
  ip varchar(32),
  event varchar(32),
  day date,
  campaign varchar(128)
  )
  distkey(uid)
  sortkey(app_id, day, campaign);


  --  processed.fact_sendgrid
create table processed.fact_sendgrid (
app_id varchar(64),
campaign_name varchar(128),
campaign_date date,
delivered_per_campaign_date integer,
app_user_id varchar(64),
snsid varchar(64),
email varchar(128),
os varchar(32),
device varchar(32),
country varchar(64),
delivered_ts timestamp,
bounce_ts timestamp,
open_ts timestamp,
open_daydiff integer,
click_ts timestamp,
click_daydiff integer,
precamp_session_cnt integer,
precamp_purchase_cnt integer,
precamp_revenue_usd numeric(10,4),
precamp_level_end integer,
level_end integer,
total_session_cnt integer,
total_purchase_cnt integer,
total_revenue_usd numeric(10,4)
) 
distkey (app_user_id)
sortkey(campaign_name, campaign_date, app_user_id, email);


-- processed.agg_sendgrid_retention
create table processed.agg_sendgrid_retention (
install_date date,
app_id varchar(32),
retention_days integer,
language varchar(32),
os varchar(32),
browser varchar(32),
country varchar(64),
install_source varchar(256),
is_payer integer,
email_flag boolean,
user_cnt integer)
distkey (install_date)
sortkey(install_date, app_id, retention_days);
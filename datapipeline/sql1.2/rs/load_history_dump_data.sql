drop table if exists processed.tbl_payment_s3_temp;
create table processed.tbl_payment_s3_temp
(
	json_str          VARCHAR(20000) ENCODE LZO
);

drop table if exists processed.tbl_payment_s3;
create table processed.tbl_payment_s3
(
	load_date         DATE NOT NULL ENCODE DELTA,
	app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
	json_str          VARCHAR(20000) ENCODE LZO
);

drop table if exists processed.tbl_payment_temp;
CREATE TABLE processed.tbl_payment_temp
(
  load_date         DATE NOT NULL ENCODE DELTA,
  app               VARCHAR(64),
  uid               VARCHAR(64),
  snsid             VARCHAR(64),
  pay_ts            VARCHAR(64),
  log_ts            VARCHAR(64),
  track_ref         VARCHAR(256),
  type              VARCHAR(64),
  item              VARCHAR(64),
  currency          VARCHAR(64),
  amount            VARCHAR(64),
  game_amount       VARCHAR(64),
  tid               VARCHAR(64),
  id                VARCHAR(64),
  status            VARCHAR(64)
)
  DISTKEY(load_date)
  SORTKEY(load_date);

drop table if exists processed.tbl_payment_temp_invalid;
CREATE TABLE processed.tbl_payment_temp_invalid as
select *
from processed.tbl_payment_temp
limit 0;

drop table if exists processed.tbl_payment;
CREATE TABLE processed.tbl_payment
(
  load_date         DATE NOT NULL ENCODE DELTA,
  date              DATE NOT NULL ENCODE DELTA,
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  pay_ts            TIMESTAMP ENCODE DELTA,
  log_ts            TIMESTAMP ENCODE DELTA,
  track_ref         VARCHAR(256) ENCODE BYTEDICT,
  type              VARCHAR(64) ENCODE BYTEDICT,
  item              VARCHAR(64) ENCODE BYTEDICT,
  currency          VARCHAR(64) ENCODE BYTEDICT,
  amount            DECIMAL(14,4) DEFAULT 0,
  game_amount       DECIMAL(14,4) DEFAULT 0,
  tid               VARCHAR(64),
  id                VARCHAR(64),
  status            INTEGER
)
  DISTKEY(date)
  SORTKEY(load_date, date, app, user_key, uid, pay_ts);

-- TODO: Copy data from S3
truncate table processed.tbl_payment_s3;

-- royal.de.prod
-- royal.nl.prod
-- royal.ae.prod
-- royal.spil.prod      *** release on 20141008
-- royal.us.prod
-- royal.th.prod
-- royal.fr.prod

-- 20140801 de
truncate table processed.tbl_payment_s3_temp;
copy processed.tbl_payment_s3_temp
from 's3://com.funplusgame.bidata/etl/rs/de/mongodb/payment/20140801/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into processed.tbl_payment_s3
select
    '2014-08-01' as load_date
    ,'royal.de.prod' as app
    ,json_str
from processed.tbl_payment_s3_temp;

-- 20140801 nl
truncate table processed.tbl_payment_s3_temp;
copy processed.tbl_payment_s3_temp
from 's3://com.funplusgame.bidata/etl/rs/nl/mongodb/payment/20140801/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into processed.tbl_payment_s3
select
    '2014-08-01' as load_date
    ,'royal.nl.prod' as app
    ,json_str
from processed.tbl_payment_s3_temp;

-- 20140801 ae
truncate table processed.tbl_payment_s3_temp;
copy processed.tbl_payment_s3_temp
from 's3://com.funplusgame.bidata/etl/rs/ae/mongodb/payment/20140801/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into processed.tbl_payment_s3
select
    '2014-08-01' as load_date
    ,'royal.ae.prod' as app
    ,json_str
from processed.tbl_payment_s3_temp;

-- 20140801 us
truncate table processed.tbl_payment_s3_temp;
copy processed.tbl_payment_s3_temp
from 's3://com.funplusgame.bidata/etl/rs/us/mongodb/payment/20140801/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into processed.tbl_payment_s3
select
    '2014-08-01' as load_date
    ,'royal.us.prod' as app
    ,json_str
from processed.tbl_payment_s3_temp;

-- 20140801 th
truncate table processed.tbl_payment_s3_temp;
copy processed.tbl_payment_s3_temp
from 's3://com.funplusgame.bidata/etl/rs/th/mongodb/payment/20140801/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into processed.tbl_payment_s3
select
    '2014-08-01' as load_date
    ,'royal.th.prod' as app
    ,json_str
from processed.tbl_payment_s3_temp;

-- 20140801 fr
truncate table processed.tbl_payment_s3_temp;
copy processed.tbl_payment_s3_temp
from 's3://com.funplusgame.bidata/etl/rs/fr/mongodb/payment/20140801/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into processed.tbl_payment_s3
select
    '2014-08-01' as load_date
    ,'royal.fr.prod' as app
    ,json_str
from processed.tbl_payment_s3_temp;

-- {
--     "_id": {
--         "$oid": "50e20f3d8ce4f6580b03082c"
--     },
--     "amount": 20000,
--     "currency": "FBC",
--     "gameamount": 110,
--     "item": "cash",
--     "logTime": 1356992317,
--     "payTime": "1356992316",
--     "snsid": "100000129897180",
--     "status": 1,
--     "tid": "3786794",
--     "trackRef": "self",
--     "type": "diffPayData",
--     "uid": 346
-- }

-- TODO: Parsing data from json string
truncate table processed.tbl_payment_temp;

insert into processed.tbl_payment_temp
select
    load_date
    ,app
    ,json_extract_path_text(json_str, 'uid') as uid
    ,json_extract_path_text(json_str, 'snsid') as snsid
    ,json_extract_path_text(json_str, 'payTime') as pay_ts
    ,json_extract_path_text(json_str, 'logTime') as log_ts
    ,json_extract_path_text(json_str, 'trackRef') as track_ref
    ,json_extract_path_text(json_str, 'type') as type
    ,json_extract_path_text(json_str, 'item') as item
    ,json_extract_path_text(json_str, 'currency') as currency
    ,json_extract_path_text(json_str, 'amount') as amount
    ,json_extract_path_text(json_str, 'gameamount') as game_amount
    ,json_extract_path_text(json_str, 'tid') as tid
    ,json_extract_path_text(json_extract_path_text(json_str, '_id'), '$oid') as id
    ,json_extract_path_text(json_str, 'status') as status
from processed.tbl_payment_s3;

-- TODO: Remove invalid data
truncate table processed.tbl_payment_temp_invalid;

insert into processed.tbl_payment_temp_invalid
select *
from processed.tbl_payment_temp
where snsid = '';

delete from processed.tbl_payment_temp where snsid = '';

insert into processed.tbl_payment_temp_invalid
select *
from processed.tbl_payment_temp
where uid = '';

delete from processed.tbl_payment_temp where uid = '';

-- TODO: Insert into tbl_payment
truncate table processed.tbl_payment;

insert into processed.tbl_payment
select
    load_date
    ,trunc(dateadd(second, CAST(pay_ts AS INTEGER), '1970-01-01 00:00:00')) as date
    ,MD5('royal.us.prod' || CAST(uid AS INTEGER)) as user_key
    ,app
    ,CAST(uid AS INTEGER) as uid
    ,snsid
    ,dateadd(second, CAST(pay_ts AS INTEGER), '1970-01-01 00:00:00') as pay_ts
    ,dateadd(second, CAST(log_ts AS INTEGER), '1970-01-01 00:00:00') as log_ts
    ,track_ref
    ,type
    ,item
    ,currency
    ,CAST(amount AS DECIMAL(14,4)) as amount
    ,CAST(game_amount AS DECIMAL(14,4)) as game_amount
    ,tid
    ,id
    ,CAST(status AS INTEGER) as status
from processed.tbl_payment_temp;

delete from processed.tbl_payment where status = 0;
-- delete from processed.tbl_payment where pay_ts >= '2014-07-16';


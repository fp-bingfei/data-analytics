drop table if exists processed.fact_quests;
CREATE TABLE processed.fact_quests
(
  date                  DATE NOT NULL ENCODE DELTA,
  ts                    TIMESTAMP ENCODE DELTA,
  user_key              VARCHAR(50) NOT NULL ENCODE LZO,
  app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                   INTEGER NOT NULL,
  snsid                 VARCHAR(64) NOT NULL ENCODE LZO,
  level                 SMALLINT,
  quest_id              VARCHAR(64) ENCODE BYTEDICT,
  action                VARCHAR(64) ENCODE BYTEDICT,
  task_id               VARCHAR(256) ENCODE BYTEDICT,
  rc_out                INTEGER,
  coins_in              INTEGER
)
  DISTKEY(user_key)
  SORTKEY(user_key, quest_id, date);


insert into processed.fact_quests
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,snsid
    ,level
    ,quest_id
    ,action
    ,task_id
    ,rc_out
    ,coins_in
)
select
    trunc(ts) as date
    ,ts
    ,MD5(app||uid) as user_key
    ,app
    ,uid
    ,snsid
    ,cast(json_extract_path_text(properties,'level') as smallint) as level
    ,json_extract_path_text(properties,'quest_id') as quest_id
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'task_id') as task_id
    ,case when json_extract_path_text(properties,'rc_out') = '' then 0 else cast(json_extract_path_text(properties,'rc_out') as INTEGER) end as rc_out
    ,case when json_extract_path_text(properties,'coins_in') = '' then 0 else cast(json_extract_path_text(properties,'coins_in') as INTEGER) end as coins_in
from events
where event = 'quest' and trunc(ts) >= '2014-07-16' and trunc(ts) < '2015-04-01';



-- remove duplicated records for quest
select distinct action
from processed.fact_quests;

drop table if exists processed.fact_quests_duplicated_records;
create table processed.fact_quests_duplicated_records as
select user_key, quest_id, action, task_id, count(1) as count
from processed.fact_quests
group by 1,2,3,4
order by count(1) desc;

select count, count(distinct user_key)
from processed.fact_quests_duplicated_records
group by 1
order by 1;

select quest_id, count(1), count(distinct user_key)
from
(select user_key, quest_id, count(1)
from processed.fact_quests
where action = 'start_quest'
group by 1,2) t
group by 1
order by 1;

delete from processed.fact_quests_duplicated_records where count = 1;

delete
from processed.fact_quests
using processed.fact_quests_duplicated_records d
where processed.fact_quests.user_key = d.user_key;

-- create agg_quests
drop table if exists quests_start_finish;
create table quests_start_finish as
select
    cast(s.date as VARCHAR) as date
    ,s.date as date_start
    ,f.date as date_finish
    ,s.user_key
    ,s.app
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,u.last_login_ts
    ,s.level
    ,s.quest_id
    ,s.ts as ts_start
    ,f.ts as ts_finish
    ,f.coins_in
from
(select *
from processed.fact_quests
where action = 'start_quest') s
left join
processed.dim_user u on s.user_key = u.user_key
left join
(select *
from processed.fact_quests
where action = 'finish_quest') f
on s.quest_id = f.quest_id and s.user_key = f.user_key;

delete from quests_start_finish where date_start < '2014-08-01';
delete from quests_start_finish where date_start = CURRENT_DATE;
delete from quests_start_finish where ts_start > ts_finish;
delete from quests_start_finish where country is null;
delete from quests_start_finish where level > 200;

drop table if exists processed.agg_quests;
create table processed.agg_quests as
select
    date
    ,date_start
    ,date_finish
    ,app
    ,country
    ,install_source
    ,install_source_group
    ,trunc(last_login_ts) as last_login_date
    ,level
    ,quest_id
    ,count(1) as start_count
    ,sum(case when ts_finish is null then 0 else 1 end) as finish_count
    ,sum(coalesce(datediff(minutes, ts_start, ts_finish), 0)) as duration_minutes
from quests_start_finish
group by 1,2,3,4,5,6,7,8,9,10;



-- create skip_quests
drop table if exists processed.agg_skip_quests;
create table processed.agg_skip_quests as
select
    q.date
    ,q.app
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,q.level
    ,q.quest_id
    ,q.task_id
    ,count(1) as skip_task_count
    ,sum(q.rc_out) as rc_out
from processed.fact_quests q
    join processed.dim_user u on q.user_key = u.user_key
where action = 'skip_task'
group by 1,2,3,4,5,6,7,8;

unload ('select * from processed.agg_quests;')
to 's3://com.funplus.bitest/rs/quests_data/agg_quests_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.agg_skip_quests;')
to 's3://com.funplus.bitest/rs/quests_data/agg_skip_quests_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

-- TODO: TEST: unload events data to S3 and copy data from S3
unload ('select * from events where trunc(ts) < \'2014-09-01\';')
to 's3://com.funplus.bitest/rs/events_raw/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
DELIMITER '^';

copy public.events1
from 's3://com.funplus.bitest/rs/events_raw/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
DELIMITER '^'
MAXERROR 1000;

-- TODO: Import missing payment data
CREATE TABLE public.events_payment_0511
(
	id              VARCHAR(64) ENCODE LZO,
	app             VARCHAR(64) ENCODE BYTEDICT,
	ts              TIMESTAMP ENCODE DELTA,
	uid             INTEGER,
	snsid           VARCHAR(64) ENCODE LZO,
	install_ts      TIMESTAMP ENCODE DELTA,
	install_source  VARCHAR(1024) ENCODE BYTEDICT,
	country_code    VARCHAR(16) ENCODE BYTEDICT,
	ip              VARCHAR(16) ENCODE LZO,
	browser         VARCHAR(32) ENCODE BYTEDICT,
	browser_version VARCHAR(32) ENCODE BYTEDICT,
	os              VARCHAR(32) ENCODE BYTEDICT,
	os_version      VARCHAR(64) ENCODE BYTEDICT,
	event           VARCHAR(32) ENCODE BYTEDICT,
	properties      VARCHAR(4096) ENCODE LZO
)
  DISTKEY(uid)
  SORTKEY(event, ts);


truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplusgame.bidata/logserver/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
GZIP
MAXERROR 1000;

insert into public.events_payment_0511
select
    md5(json_str) as id
    ,json_extract_path_text(json_str, '@key') as app
    ,dateadd(second, CAST(json_extract_path_text(json_str, '@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,CAST(json_extract_path_text(json_str, 'uid') AS INTEGER) as uid
    ,json_extract_path_text(json_str, 'snsid') as snsid
    ,dateadd(second, CAST(json_extract_path_text(json_str, 'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,json_extract_path_text(json_str, 'install_src') as install_src
    ,'--' as country_code
    ,json_extract_path_text(json_str, 'ip') as ip
    ,json_extract_path_text(json_str, 'browser') as browser
    ,json_extract_path_text(json_str, 'browser_version') as browser_version
    ,json_extract_path_text(json_str, 'os') as os
    ,json_extract_path_text(json_str, 'os_version') as os_version
    ,json_extract_path_text(json_str, 'event') as event
    ,json_str as properties
from processed.raw_data_s3_test
where json_extract_path_text(json_str, 'event') = 'payment';

insert into events
select *
from events_payment_0511;

select t.*, r.*
from events_payment_0511 t
	left join processed.fact_revenue r on r.transaction_id = json_extract_path_text(t.properties,'transaction_id')
where r.uid is null;

grant select on table processed.eas_user_info to "zhao.liu";

-- TODO: Check data difference between fluentd and logserver
drop table if exists fluentd_data_rows;
create table fluentd_data_rows as
select app, event, trunc(ts) as date, extract(hour from ts) as hour, count(1) as count
from events
where trunc(ts) >= '2015-05-08'
group by 1,2,3,4;

drop table if exists logserver_data_rows;
create table logserver_data_rows as
select app, event, trunc(ts) as date, extract(hour from ts) as hour, count(1) as count
from processed.events_raw_test
where trunc(ts) >= '2015-05-08'
group by 1,2,3,4;

drop table if exists fluentd_logserver_data_rows;
create table fluentd_logserver_data_rows as
select l.app, l.event, l.date, l.hour, l.count as logserver_count, f.count as fluentd_count
from logserver_data_rows l
    join fluentd_data_rows f on l.app = f.app and l.event = f.event and l.date = f.date and l.hour = f.hour;


-- TODO: Load history events data
create table tmp_events as
select *
from events
limit 0;

copy tmp_events
from 's3://com.funplusgame.bidata/events_raw/rs/session_start/2015/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '^'
MAXERROR 1000;

select *
from   tmp_events
where  event in ('newuser', 'session_start', 'session_active') and json_extract_path_text(properties,'session_id') = '133964241420166198' and trunc(ts) >= '2015-01-02' and trunc(ts) <= '2015-01-18'
order by ts;

drop table tmp_events;


select * from pg_user;
create user "rs_pm" with password 'QowNr%icA03hsOGe';

create schema rs_pm;

grant usage on schema rs_pm to "rs_pm";
grant create on schema rs_pm to "rs_pm";
grant all on all tables in schema rs_pm to "rs_pm";

grant usage on schema processed to "rs_pm";
grant select on all tables in schema processed to "rs_pm";

grant usage on schema public to "rs_pm";
grant select on all tables in schema public to "rs_pm";

revoke all on all tables in schema processed from rs_pm;
revoke all on all tables in schema public from rs_pm;

-- TODO: Check quest issue
drop table if exists invest_reactive_users;
create table invest_reactive_users as
select user_key, min(date) as reactive_date, min(quest_id) as quest_id
from processed.fact_quests
where date >= '2015-06-04' and action = 'start_quest' and (quest_id = 'Solomon4_1_03@1' or quest_id = 'Solomon4_1_03@2')
group by 1
having count(distinct quest_id) = 1;

select count(1) from invest_reactive_users;
select count(1) from invest_reactive_finish_task_users;

drop table if exists invest_reactive_finish_task_users;
create table invest_reactive_finish_task_users as
select user_key, max(date) as finish_date, min(quest_id) as quest_id
from processed.fact_quests
where date >= '2015-06-04' and action = 'finish_quest' and (quest_id = 'Solomon4_1_03@1' or quest_id = 'Solomon4_1_03@2')
group by 1
having count(distinct quest_id) = 1;

select *
from invest_reactive_users i1
	join invest_reactive_finish_task_users i2 on i1.user_key = i2.user_key and i1.quest_id != i2.quest_id;

select *
from processed.fact_quests
where user_key in ('240f3bcab2de91264eacd936da7934ed','fbb07a73884b97cc6912383a2f26522b','a03e109cd1cbc11caf4005a670e21a98','86d2409c30c02b28f22cb84641b8e3df','5359c3c3a993a241bb1941abce2c1b2a')
	and quest_id like 'Solomon4_1_03@%'
order by user_key, ts;

'royal.ae.prod',3729421
'royal.fr.prod',912700
'royal.fr.prod',89800
'royal.us.prod',18777162
'royal.ae.prod',4786703

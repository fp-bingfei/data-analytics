-- tutorial report
delete from processed.agg_tutorial
where install_date >=(
                select start_date
                from rs.processed.tmp_start_date
             );

create temp table tmp_tutorial_step as
select MD5(app||uid) as user_key
        ,app
        ,uid
        ,snsid
        ,json_extract_path_text(properties,'step') as step
		,cast(json_extract_path_text(properties,'step') as decimal) as step_number
        ,json_extract_path_text(properties,'abid') as tutorial_type
        ,min(ts) as min_ts
from events
where event = 'tutorial' and
      trunc(ts) >=
            (
                select start_date
                from rs.processed.tmp_start_date
            )
group by 1,2,3,4,5,6,7;

create temp table tmp_tutorial_step_first_time as
select *,
	first_value(min_ts) over (partition by user_key order by step_number rows between unbounded preceding and unbounded following) as pre_min_ts
from tmp_tutorial_step;

insert into processed.agg_tutorial
(
    install_date
    ,app
    ,os
    ,country
    ,install_source
    ,install_source_group
    ,browser
    ,browser_version
    ,tutorial_type
    ,step
    ,step_number
    ,tutorial_installs
)
select
     u.install_date
    ,u.app
    ,s.os
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,s.browser
    ,s.browser_version
    ,t.tutorial_type
    ,t.step
    ,t.step_number
    ,count(distinct t.user_key) as tutorial_installs
from tmp_tutorial_step_first_time t
        join processed.dim_user u on t.user_key = u.user_key
        join processed.fact_dau_snapshot s on t.user_key = s.user_key
where u.install_date >=
            (
                select start_date
                from rs.processed.tmp_start_date
            )
      and u.is_tester = 0
group by 1,2,3,4,5,6,7,8,9,10,11;













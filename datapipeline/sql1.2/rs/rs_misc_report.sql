-- TODO: last ref
drop table if exists processed.events_session_start;
create table processed.events_session_start as
select
    app
    ,ts
    ,uid
    ,snsid
    ,os
    ,json_extract_path_text(properties, 'last_ref') as last_ref
    ,properties
from events
where event = 'session_start' and trunc(ts) >= '2015-04-13';

delete from processed.events_session_start where trunc(ts) = CURRENT_DATE;

delete from processed.agg_last_ref
where date >= (
                  select raw_data_start_date
                  from processed.tmp_start_date
            );
insert into processed.agg_last_ref 
select
    trunc(s.ts) as date
    ,u.app
    ,u.country
    ,u.language
    ,u.is_payer
    ,s.last_ref
    ,coalesce(g.install_source_group, 'others') as install_source_group
    ,count(distinct u.user_key) as dau
from processed.events_session_start s
    join processed.dim_user u on s.app = u.app and s.uid = u.uid
    left join processed.ref_install_source_group g on s.last_ref = g.install_source
where u.is_tester = 0
      and trunc(s.ts)>= (
                  select raw_data_start_date
                  from processed.tmp_start_date
            )
group by 1,2,3,4,5,6,7;

-- TODO: cheating_list
delete from processed.cheating_list
where date >= (
                    select raw_data_start_date
                    from processed.tmp_start_date
              );

insert into processed.cheating_list
select
    trunc(e.ts) as date
    ,e.app
    ,e.snsid
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_in')='' then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT)) as rc_in
from events e
    join processed.dim_user u on MD5(e.app||e.uid) = u.user_key
where e.event = 'rc_transaction' and u.is_tester = 0
and     trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                     )
group by 1,2,3
having sum(cast(case when json_extract_path_text(e.properties,'rc_in')='' then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT)) > 1000;

create temp table cheater_revenue as
select
    date
    ,app
    ,snsid
    ,sum(usd) as revenue_usd
from processed.fact_revenue
where date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
              )
group by 1,2,3;

update processed.cheating_list
set revenue_usd = c.revenue_usd
from cheater_revenue c
where processed.cheating_list.date = c.date and
      processed.cheating_list.app = c.app and
      processed.cheating_list.snsid = c.snsid;


-- TODO: AB test result analysis
drop table if exists processed.dim_user_ab_test;
create table processed.dim_user_ab_test as
select distinct 'd20_inactive_level_5_10 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_5_10_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_5_10 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_5_10_snsid_b t on u.snsid = t.snsid
union

select distinct 'd20_inactive_level_11_15 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_11_15_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_11_15 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_11_15_snsid_b t on u.snsid = t.snsid

union
select distinct 'd20_inactive_level_16_20 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_16_20_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_16_20 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_16_20_snsid_b t on u.snsid = t.snsid

union
select distinct 'd20_inactive_level_21_30 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_21_30_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_21_30 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_21_30_snsid_b t on u.snsid = t.snsid

union
select distinct 'd20_inactive_level_31_plus A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_31_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_31_plus B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_31_snsid_b t on u.snsid = t.snsid;


-- create agg_kpi_ab_test
drop table if exists processed.agg_kpi_ab_test;
create table processed.agg_kpi_ab_test as
select  date
      ,cast(date as VARCHAR) as date_str
      ,d.app
      ,d.app_version
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar) as level_range
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.is_whale
      ,u.ab_group
      ,sum(d.is_new_user) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd_iap > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_ads) as revenue_ads
      ,sum(d.session_cnt) as session_cnt
      ,sum(d.playtime_sec) as playtime_sec
from processed.fact_dau_snapshot d
    join processed.dim_user_ab_test u on d.user_key = u.user_key
where d.date >= '2015-05-07'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

-- TODO: Online time distribution
drop table if exists processed.online_time_distribution;
create table processed.online_time_distribution as
select date
      ,d.app
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar) as level_range
      ,d.country
      ,d.language
      ,u.is_payer
      ,u.is_whale
      ,(playtime_sec / 600) * 10 as time_interval_start
--       ,cast((playtime_sec / 600) * 10 as varchar) || ' ~ ' || cast((playtime_sec / 600) * 10 + 10 as varchar) as online_time_range
      ,cast((playtime_sec / 60) as varchar) as online_time_range
      ,count(1) as users
from processed.fact_dau_snapshot d
	join processed.dim_user u on d.user_key = u.user_key
where date >= '2015-01-01' and playtime_sec > 0
group by 1,2,3,4,5,6,7,8,9,10,11;

-- TODO: Re-active user report
drop table if exists reactive_users;
create temp table reactive_users as
select user_key, min(date) as reactive_date
from processed.fact_quests
where date >= '2015-07-15' and action = 'start_quest' and quest_id = 'ReturnDay1_1_1'
group by 1;

drop table if exists reactive_finish_task_users;
create temp table reactive_finish_task_users as
select user_key, max(date) as finish_date
from processed.fact_quests
where date >= '2015-07-15' and action = 'finish_quest' and quest_id = 'ReturnDay1_1_6'
group by 1;

drop table if exists reactive_users_dau_snapshot;
create temp table reactive_users_dau_snapshot as
select d.*, r.reactive_date,
    case when f.finish_date is not null and d.date >= f.finish_date then 1 else 0 end as is_finish_quest
from processed.fact_dau_snapshot d
    join reactive_users r on d.user_key = r.user_key and d.date >= r.reactive_date
    left join reactive_finish_task_users f on d.user_key = f.user_key;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_reactive_kpi
----------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists processed.agg_reactive_kpi;
create table processed.agg_reactive_kpi as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar) as level_range
      ,d.country
      ,d.os
      ,d.language
      ,d.is_finish_quest
      ,d.is_payer
      ,u.is_whale
      ,sum(case when d.date = d.reactive_date then 1 else 0 end) as new_active_users
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd_iap > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_ads) as revenue_ads
      ,sum(d.session_cnt) as session_cnt
      ,sum(d.playtime_sec) as playtime_sec
from reactive_users_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
where date >= '2015-07-15' and u.is_tester = 0
group by 1,2,3,4,5,6,7,8,9,10,11,12;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_reactive_retention
----------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists player_day;
create temp table player_day as
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day
union all
select 120 as day
union all
select 150 as day
union all
select 180 as day;

drop table if exists tmp_new_reactive_users;
create temp table tmp_new_reactive_users as
select  r.reactive_date
       ,u.os
       ,u.browser
       ,u.country
       ,u.app
       ,u.install_source
       ,u.install_source_group
       ,u.is_payer
       ,count(u.user_key) as new_reactive_users
from  processed.dim_user u
    join reactive_users r on u.user_key = r.user_key
where r.reactive_date >= '2015-07-15' and u.is_tester = 0
group by 1,2,3,4,5,6,7,8;

-- create temp table to get the last date in dau table.
drop table if exists last_date;
create temp table last_date as
select max(date) as last_date
from processed.fact_dau_snapshot;

-- create cube to calculate the retention
drop table if exists player_day_cube;
create temp table player_day_cube as
select rd.day as retention_days
      ,nu.reactive_date
      ,nu.os
      ,nu.browser
      ,nu.country
      ,nu.app
      ,nu.install_source
      ,nu.install_source_group
      ,nu.is_payer
      ,nu.new_reactive_users
from tmp_new_reactive_users nu
    join player_day rd on 1 = 1
    join last_date d on 1=1
where DATEDIFF('day', nu.reactive_date, d.last_date) >= rd.day;

-- insert into agg_reactive_retention table
drop table if exists processed.agg_reactive_retention;
create table processed.agg_reactive_retention as
select
           cube.retention_days
          ,cube.reactive_date
          ,cube.os
          ,cube.browser
          ,cube.country
          ,cube.app
          ,cube.install_source
          ,cube.install_source_group
          ,cube.is_payer
          ,cube.new_reactive_users
          ,coalesce(r.retained, 0) as retained
from      player_day_cube cube
left join
          (
             SELECT   DATEDIFF('day', du.reactive_date, du.date) AS retention_days
                     ,du.reactive_date
                     ,u.os
                     ,u.browser
                     ,u.country
                     ,u.app
                     ,u.install_source
                     ,u.install_source_group
                     ,u.is_payer
                     ,count(distinct du.user_key) as retained
             FROM    reactive_users_dau_snapshot du
             join    processed.dim_user u on du.user_key = u.user_key
             where   DATEDIFF('day', du.reactive_date, du.date) in
                                     (
                                       select day
                                       from player_day
                                      )
             group by 1,2,3,4,5,6,7,8,9
          ) r
on  cube.retention_days = r.retention_days
    and cube.reactive_date = r.reactive_date
    and cube.os = r.os
    and cube.browser = r.browser
    and cube.country = r.country
    and cube.app = r.app
    and cube.install_source = r.install_source
    and cube.install_source_group = r.install_source_group
    and cube.is_payer = r.is_payer;


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_reactive_ltv
----------------------------------------------------------------------------------------------------------------------------------------------
-- create ltv cube table
drop table if exists agg_reactive_ltv_cube;
create temp table agg_reactive_ltv_cube as
SELECT d.day as ltv_days
      ,r.reactive_date
      ,u.os
      ,u.browser
      ,u.country
      ,u.app
      ,u.install_source
      ,u.install_source_group
      ,u.is_payer
      ,u.user_key
FROM processed.dim_user u
    join reactive_users r on u.user_key = r.user_key
    join player_day d on DATEDIFF('day', r.reactive_date, (select last_date from last_date)) >= d.day
where r.reactive_date >= '2015-07-15' and u.is_tester = 0;

-- insert into ltv table
drop table if exists processed.agg_reactive_ltv;
create table processed.agg_reactive_ltv as
SELECT
    c.ltv_days
    ,c.reactive_date
    ,c.os
    ,c.browser
    ,c.country
    ,c.app
    ,c.install_source
    ,c.install_source_group
    ,c.is_payer
    ,count(distinct c.user_key) as users
    ,sum(COALESCE(du.revenue_usd, 0)) as revenue
FROM        agg_reactive_ltv_cube c
left join   reactive_users_dau_snapshot du
       on   c.user_key = du.user_key and DATEDIFF('day', c.reactive_date, du.date) <= c.ltv_days
group by 1,2,3,4,5,6,7,8,9;

delete from processed.agg_reactive_ltv where reactive_date < '2015-07-15';

delete from processed.agg_reactive_ltv
using last_date d
where DATEDIFF('day', reactive_date, d.last_date) < ltv_days;

-- TODO: Feed report
delete from processed.fact_feed
where date >=(
                select raw_data_start_date
                from rs.processed.tmp_start_date
             );

insert into processed.fact_feed
(
    app
    ,date
    ,uid
    ,snsid
    ,user_key
    ,level
    ,location
    ,action
    ,action_detail
    ,rc_in
    ,rc_out
)
select
    app
    ,trunc(ts) as date
    ,uid
    ,snsid
    ,MD5(app||uid) as user_key
    ,cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as smallint) as level
    ,json_extract_path_text(properties,'location') as location
    ,json_extract_path_text(properties,'action') as action
    ,json_extract_path_text(properties,'action_detail') as action_detail
    ,cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as BIGINT) as rc_in
    ,cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') end as BIGINT) as rc_out
from events
where event='feed' and
      trunc(ts) >= (
                        select raw_data_start_date
                        from rs.processed.tmp_start_date
                   );

delete from processed.agg_feed
where date >=(
                select raw_data_start_date
                from rs.processed.tmp_start_date
             );

insert into processed.agg_feed
(
    app
    ,date
    ,level
    ,country
    ,install_source
    ,install_source_group
    ,is_payer
    ,location
    ,clicks
    ,sends
    ,clickers
    ,senders
)
select
    f.app
    ,f.date
    ,f.level
    ,u.country
    ,u.install_source
    ,u.install_source_group
    ,u.is_payer
    ,f.location
    ,sum(case when f.action = 'click' then 1 else 0 end) as clicks
    ,sum(case when f.action = 'send' then 1 else 0 end) as sends
    ,count(distinct case when f.action = 'click' then f.user_key else null end) as clickers
    ,count(distinct case when f.action = 'send' then f.user_key else null end) as senders
from processed.fact_feed f
    join processed.dim_user u on f.user_key = u.user_key
where u.is_tester = 0 and f.level <= 200
and   f.date >= (
                    select raw_data_start_date
                    from rs.processed.tmp_start_date
                )
group by 1,2,3,4,5,6,7,8;

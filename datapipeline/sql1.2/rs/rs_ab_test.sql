-- TODO: snsid for d3 active
-- get snsid for d3 active
drop table if exists processed.payer_snsid_d3_active;
create table processed.payer_snsid_d3_active as
select snsid, max(trunc(last_login_ts)) as last_login_ts
from processed.dim_user
where is_payer = 1 and app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod')
group by snsid
having CURRENT_DATE - max(trunc(last_login_ts)) <= 3 and count(1) = 1;

-- split snsid for d3 active into 2 parts
drop table if exists processed.payer_snsid_d3_active_a;
create table processed.payer_snsid_d3_active_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.payer_snsid_d3_active) t
where rank % 2 = 1;

drop table if exists processed.payer_snsid_d3_active_b;
create table processed.payer_snsid_d3_active_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.payer_snsid_d3_active) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.payer_snsid_d3_active_a_us;
create table processed.payer_snsid_d3_active_a_us as
select a.snsid
from processed.payer_snsid_d3_active_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.us.prod';

drop table if exists processed.payer_snsid_d3_active_a_de;
create table processed.payer_snsid_d3_active_a_de as
select a.snsid
from processed.payer_snsid_d3_active_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.de.prod';

drop table if exists processed.payer_snsid_d3_active_a_fr;
create table processed.payer_snsid_d3_active_a_fr as
select a.snsid
from processed.payer_snsid_d3_active_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.fr.prod';

drop table if exists processed.payer_snsid_d3_active_a_nl;
create table processed.payer_snsid_d3_active_a_nl as
select a.snsid
from processed.payer_snsid_d3_active_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.payer_snsid_d3_active_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0319/d3_active_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP;

unload ('select * from processed.payer_snsid_d3_active_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0319/d3_active_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP;

unload ('select * from processed.payer_snsid_d3_active_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0319/d3_active_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP;

unload ('select * from processed.payer_snsid_d3_active_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0319/d3_active_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP;


-- TODO: snsid for d3 inactive
-- get snsid for d3 inactive
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.payer_snsid_d3_inactive;
create table processed.payer_snsid_d3_inactive as
select u.snsid, max(trunc(last_login_ts)) as last_login_ts
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where is_payer = 1 and app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod')
group by 1
having CURRENT_DATE - max(trunc(last_login_ts)) > 3 and count(1) = 1;

-- split snsid for d3 inactive into 2 parts
drop table if exists processed.payer_snsid_d3_inactive_a;
create table processed.payer_snsid_d3_inactive_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.payer_snsid_d3_inactive) t
where rank % 2 = 1;

drop table if exists processed.payer_snsid_d3_inactive_b;
create table processed.payer_snsid_d3_inactive_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.payer_snsid_d3_inactive) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.payer_snsid_d3_inactive_a_us;
create table processed.payer_snsid_d3_inactive_a_us as
select a.snsid
from processed.payer_snsid_d3_inactive_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.us.prod';

drop table if exists processed.payer_snsid_d3_inactive_a_de;
create table processed.payer_snsid_d3_inactive_a_de as
select a.snsid
from processed.payer_snsid_d3_inactive_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.de.prod';

drop table if exists processed.payer_snsid_d3_inactive_a_fr;
create table processed.payer_snsid_d3_inactive_a_fr as
select a.snsid
from processed.payer_snsid_d3_inactive_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.fr.prod';

drop table if exists processed.payer_snsid_d3_inactive_a_nl;
create table processed.payer_snsid_d3_inactive_a_nl as
select a.snsid
from processed.payer_snsid_d3_inactive_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.is_payer = 1 and u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.payer_snsid_d3_inactive_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0410/d3_inactive_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.payer_snsid_d3_inactive_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0410/d3_inactive_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.payer_snsid_d3_inactive_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0410/d3_inactive_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.payer_snsid_d3_inactive_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0410/d3_inactive_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: AB test result analysis
drop table if exists processed.dim_user_ab_test;
create table processed.dim_user_ab_test as
select distinct 'D3 Active Payer A' as ab_group, u.*
from processed.dim_user u
    join processed.payer_snsid_d3_active_a p on u.snsid = p.snsid
union
select distinct 'D3 Active Payer B' as ab_group, u.*
from processed.dim_user u
    join processed.payer_snsid_d3_active_b p on u.snsid = p.snsid
union
select distinct 'D3 Inactive Payer A' as ab_group, u.*
from processed.dim_user u
    join processed.payer_snsid_d3_inactive_a p on u.snsid = p.snsid
union
select distinct 'D3 Inactive Payer B' as ab_group, u.*
from processed.dim_user u
    join processed.payer_snsid_d3_inactive_b p on u.snsid = p.snsid;


-- create agg_kpi_ab_test
drop table if exists processed.agg_kpi_ab_test;
create table processed.agg_kpi_ab_test as
select  date
      ,cast(date as VARCHAR) as date_str
      ,d.app
      ,d.app_version
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar) as level_range
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.is_whale
      ,u.ab_group
      ,sum(d.is_new_user) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd_iap > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_ads) as revenue_ads
      ,sum(d.session_cnt) as session_cnt
      ,sum(d.playtime_sec) as playtime_sec
from processed.fact_dau_snapshot d
    join processed.dim_user_ab_test u on d.user_key = u.user_key
where d.date >= '2015-03-10'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

-- TODO: 2015-04-24
-- TODO: snsid for 6天登陆过4天没登陆的非付费玩家
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.nonpayer_d4_d6_active_snsid;
create table processed.nonpayer_d4_d6_active_snsid as
select u.snsid, u.last_login_ts
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where is_payer = 0 and
    app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 4 and
    CURRENT_DATE - trunc(u.last_login_ts) <= 6;

-- split snsid into 2 parts
drop table if exists processed.nonpayer_d4_d6_active_snsid_a;
create table processed.nonpayer_d4_d6_active_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.nonpayer_d4_d6_active_snsid) t
where rank % 2 = 1;

drop table if exists processed.nonpayer_d4_d6_active_snsid_b;
create table processed.nonpayer_d4_d6_active_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.nonpayer_d4_d6_active_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.nonpayer_d4_d6_active_snsid_a_us;
create table processed.nonpayer_d4_d6_active_snsid_a_us as
select a.snsid
from processed.nonpayer_d4_d6_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.nonpayer_d4_d6_active_snsid_a_de;
create table processed.nonpayer_d4_d6_active_snsid_a_de as
select a.snsid
from processed.nonpayer_d4_d6_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.nonpayer_d4_d6_active_snsid_a_fr;
create table processed.nonpayer_d4_d6_active_snsid_a_fr as
select a.snsid
from processed.nonpayer_d4_d6_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.nonpayer_d4_d6_active_snsid_a_nl;
create table processed.nonpayer_d4_d6_active_snsid_a_nl as
select a.snsid
from processed.nonpayer_d4_d6_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.nonpayer_d4_d6_active_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d4_d6_active_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.nonpayer_d4_d6_active_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d4_d6_active_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.nonpayer_d4_d6_active_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d4_d6_active_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.nonpayer_d4_d6_active_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d4_d6_active_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;


-- TODO: snsid for 10天登陆过7天没登陆的非付费玩家
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.nonpayer_d7_d10_active_snsid;
create table processed.nonpayer_d7_d10_active_snsid as
select u.snsid, u.last_login_ts
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where is_payer = 0 and
    app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 7 and
    CURRENT_DATE - trunc(u.last_login_ts) <= 10;

-- split snsid into 2 parts
drop table if exists processed.nonpayer_d7_d10_active_snsid_a;
create table processed.nonpayer_d7_d10_active_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.nonpayer_d7_d10_active_snsid) t
where rank % 2 = 1;

drop table if exists processed.nonpayer_d7_d10_active_snsid_b;
create table processed.nonpayer_d7_d10_active_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.nonpayer_d7_d10_active_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.nonpayer_d7_d10_active_snsid_a_us;
create table processed.nonpayer_d7_d10_active_snsid_a_us as
select a.snsid
from processed.nonpayer_d7_d10_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.nonpayer_d7_d10_active_snsid_a_de;
create table processed.nonpayer_d7_d10_active_snsid_a_de as
select a.snsid
from processed.nonpayer_d7_d10_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.nonpayer_d7_d10_active_snsid_a_fr;
create table processed.nonpayer_d7_d10_active_snsid_a_fr as
select a.snsid
from processed.nonpayer_d7_d10_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.nonpayer_d7_d10_active_snsid_a_nl;
create table processed.nonpayer_d7_d10_active_snsid_a_nl as
select a.snsid
from processed.nonpayer_d7_d10_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.nonpayer_d7_d10_active_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d7_d10_active_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.nonpayer_d7_d10_active_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d7_d10_active_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.nonpayer_d7_d10_active_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d7_d10_active_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.nonpayer_d7_d10_active_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0424/nonpayer_d7_d10_active_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;


-- TODO: snsid for 60天登录过4天没登陆过的所有付费玩家
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.payer_d4_d60_active_snsid;
create table processed.payer_d4_d60_active_snsid as
select u.snsid, u.last_login_ts
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where is_payer = 1 and
    app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 4 and
    CURRENT_DATE - trunc(u.last_login_ts) <= 60;

-- split snsid into 2 parts
drop table if exists processed.payer_d4_d60_active_snsid_a;
create table processed.payer_d4_d60_active_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.payer_d4_d60_active_snsid) t
where rank % 2 = 1;

drop table if exists processed.payer_d4_d60_active_snsid_b;
create table processed.payer_d4_d60_active_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.payer_d4_d60_active_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.payer_d4_d60_active_snsid_a_us;
create table processed.payer_d4_d60_active_snsid_a_us as
select a.snsid
from processed.payer_d4_d60_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.payer_d4_d60_active_snsid_a_de;
create table processed.payer_d4_d60_active_snsid_a_de as
select a.snsid
from processed.payer_d4_d60_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.payer_d4_d60_active_snsid_a_fr;
create table processed.payer_d4_d60_active_snsid_a_fr as
select a.snsid
from processed.payer_d4_d60_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.payer_d4_d60_active_snsid_a_nl;
create table processed.payer_d4_d60_active_snsid_a_nl as
select a.snsid
from processed.payer_d4_d60_active_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.payer_d4_d60_active_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0424/payer_d4_d60_active_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.payer_d4_d60_active_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0424/payer_d4_d60_active_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.payer_d4_d60_active_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0424/payer_d4_d60_active_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.payer_d4_d60_active_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0424/payer_d4_d60_active_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: AB test result analysis
drop table if exists processed.dim_user_ab_test;
create table processed.dim_user_ab_test as
select distinct 'nonpayer_d4_d6_active A' as ab_group, u.*
from processed.dim_user u
    join processed.nonpayer_d4_d6_active_snsid_a t on u.snsid = t.snsid
union
select distinct 'nonpayer_d4_d6_active B' as ab_group, u.*
from processed.dim_user u
    join processed.nonpayer_d4_d6_active_snsid_b t on u.snsid = t.snsid
union
select distinct 'nonpayer_d7_d10_active A' as ab_group, u.*
from processed.dim_user u
    join processed.nonpayer_d7_d10_active_snsid_a t on u.snsid = t.snsid
union
select distinct 'nonpayer_d7_d10_active B' as ab_group, u.*
from processed.dim_user u
    join processed.nonpayer_d7_d10_active_snsid_b t on u.snsid = t.snsid
union
select distinct 'payer_d4_d60_active A' as ab_group, u.*
from processed.dim_user u
    join processed.payer_d4_d60_active_snsid_a t on u.snsid = t.snsid
union
select distinct 'payer_d4_d60_active B' as ab_group, u.*
from processed.dim_user u
    join processed.payer_d4_d60_active_snsid_b t on u.snsid = t.snsid;


-- create agg_kpi_ab_test
drop table if exists processed.agg_kpi_ab_test;
create table processed.agg_kpi_ab_test as
select  date
      ,cast(date as VARCHAR) as date_str
      ,d.app
      ,d.app_version
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar) as level_range
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.is_whale
      ,u.ab_group
      ,sum(d.is_new_user) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd_iap > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_ads) as revenue_ads
      ,sum(d.session_cnt) as session_cnt
      ,sum(d.playtime_sec) as playtime_sec
from processed.fact_dau_snapshot d
    join processed.dim_user_ab_test u on d.user_key = u.user_key
where d.date >= '2015-03-10'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

-- TODO: 2015-05-07
-- TODO: snsid for 20天没有登陆过，level 5-10
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.d20_inactive_level_5_10_snsid;
create table processed.d20_inactive_level_5_10_snsid as
select u.snsid, u.last_login_ts, u.level
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 20 and
    level >= 5 and level <= 10;

-- split snsid into 2 parts
drop table if exists processed.d20_inactive_level_5_10_snsid_a;
create table processed.d20_inactive_level_5_10_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_5_10_snsid) t
where rank % 2 = 1;

drop table if exists processed.d20_inactive_level_5_10_snsid_b;
create table processed.d20_inactive_level_5_10_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_5_10_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.d20_inactive_level_5_10_snsid_a_us;
create table processed.d20_inactive_level_5_10_snsid_a_us as
select a.snsid
from processed.d20_inactive_level_5_10_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.d20_inactive_level_5_10_snsid_a_de;
create table processed.d20_inactive_level_5_10_snsid_a_de as
select a.snsid
from processed.d20_inactive_level_5_10_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.d20_inactive_level_5_10_snsid_a_fr;
create table processed.d20_inactive_level_5_10_snsid_a_fr as
select a.snsid
from processed.d20_inactive_level_5_10_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.d20_inactive_level_5_10_snsid_a_nl;
create table processed.d20_inactive_level_5_10_snsid_a_nl as
select a.snsid
from processed.d20_inactive_level_5_10_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.d20_inactive_level_5_10_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_5_10_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_5_10_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_5_10_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_5_10_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_5_10_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_5_10_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_5_10_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: snsid for 20天没有登陆过，level 11-15
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.d20_inactive_level_11_15_snsid;
create table processed.d20_inactive_level_11_15_snsid as
select u.snsid, u.last_login_ts, u.level
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 20 and
    level >= 11 and level <= 15;

-- split snsid into 2 parts
drop table if exists processed.d20_inactive_level_11_15_snsid_a;
create table processed.d20_inactive_level_11_15_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_11_15_snsid) t
where rank % 2 = 1;

drop table if exists processed.d20_inactive_level_11_15_snsid_b;
create table processed.d20_inactive_level_11_15_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_11_15_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.d20_inactive_level_11_15_snsid_a_us;
create table processed.d20_inactive_level_11_15_snsid_a_us as
select a.snsid
from processed.d20_inactive_level_11_15_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.d20_inactive_level_11_15_snsid_a_de;
create table processed.d20_inactive_level_11_15_snsid_a_de as
select a.snsid
from processed.d20_inactive_level_11_15_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.d20_inactive_level_11_15_snsid_a_fr;
create table processed.d20_inactive_level_11_15_snsid_a_fr as
select a.snsid
from processed.d20_inactive_level_11_15_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.d20_inactive_level_11_15_snsid_a_nl;
create table processed.d20_inactive_level_11_15_snsid_a_nl as
select a.snsid
from processed.d20_inactive_level_11_15_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.d20_inactive_level_11_15_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_11_15_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_11_15_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_11_15_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_11_15_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_11_15_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_11_15_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_11_15_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: snsid for 20天没有登陆过，level 16-20
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.d20_inactive_level_16_20_snsid;
create table processed.d20_inactive_level_16_20_snsid as
select u.snsid, u.last_login_ts, u.level
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 20 and
    level >= 16 and level <= 20;

-- split snsid into 2 parts
drop table if exists processed.d20_inactive_level_16_20_snsid_a;
create table processed.d20_inactive_level_16_20_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_16_20_snsid) t
where rank % 2 = 1;

drop table if exists processed.d20_inactive_level_16_20_snsid_b;
create table processed.d20_inactive_level_16_20_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_16_20_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.d20_inactive_level_16_20_snsid_a_us;
create table processed.d20_inactive_level_16_20_snsid_a_us as
select a.snsid
from processed.d20_inactive_level_16_20_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.d20_inactive_level_16_20_snsid_a_de;
create table processed.d20_inactive_level_16_20_snsid_a_de as
select a.snsid
from processed.d20_inactive_level_16_20_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.d20_inactive_level_16_20_snsid_a_fr;
create table processed.d20_inactive_level_16_20_snsid_a_fr as
select a.snsid
from processed.d20_inactive_level_16_20_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.d20_inactive_level_16_20_snsid_a_nl;
create table processed.d20_inactive_level_16_20_snsid_a_nl as
select a.snsid
from processed.d20_inactive_level_16_20_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.d20_inactive_level_16_20_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_16_20_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_16_20_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_16_20_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_16_20_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_16_20_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_16_20_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_16_20_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: snsid for 20天没有登陆过，level 21-30
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.d20_inactive_level_21_30_snsid;
create table processed.d20_inactive_level_21_30_snsid as
select u.snsid, u.last_login_ts, u.level
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 20 and
    level >= 21 and level <= 30;

-- split snsid into 2 parts
drop table if exists processed.d20_inactive_level_21_30_snsid_a;
create table processed.d20_inactive_level_21_30_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_21_30_snsid) t
where rank % 2 = 1;

drop table if exists processed.d20_inactive_level_21_30_snsid_b;
create table processed.d20_inactive_level_21_30_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_21_30_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.d20_inactive_level_21_30_snsid_a_us;
create table processed.d20_inactive_level_21_30_snsid_a_us as
select a.snsid
from processed.d20_inactive_level_21_30_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.d20_inactive_level_21_30_snsid_a_de;
create table processed.d20_inactive_level_21_30_snsid_a_de as
select a.snsid
from processed.d20_inactive_level_21_30_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.d20_inactive_level_21_30_snsid_a_fr;
create table processed.d20_inactive_level_21_30_snsid_a_fr as
select a.snsid
from processed.d20_inactive_level_21_30_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.d20_inactive_level_21_30_snsid_a_nl;
create table processed.d20_inactive_level_21_30_snsid_a_nl as
select a.snsid
from processed.d20_inactive_level_21_30_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.d20_inactive_level_21_30_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_21_30_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_21_30_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_21_30_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_21_30_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_21_30_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_21_30_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_21_30_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: snsid for 20天没有登陆过，level 31+
drop table if exists unique_user_snsid;
create temp table unique_user_snsid as
select snsid
from processed.dim_user
group by 1
having count(1) = 1;

drop table if exists processed.d20_inactive_level_31_snsid;
create table processed.d20_inactive_level_31_snsid as
select u.snsid, u.last_login_ts, u.level
from processed.dim_user u
    join unique_user_snsid s on u.snsid = s.snsid
where app in ('royal.us.prod', 'royal.de.prod', 'royal.fr.prod', 'royal.nl.prod') and
    CURRENT_DATE - trunc(u.last_login_ts) > 20 and
    level >= 31;

-- split snsid into 2 parts
drop table if exists processed.d20_inactive_level_31_snsid_a;
create table processed.d20_inactive_level_31_snsid_a as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_31_snsid) t
where rank % 2 = 1;

drop table if exists processed.d20_inactive_level_31_snsid_b;
create table processed.d20_inactive_level_31_snsid_b as
select snsid
from
(select *, row_number() over (order by snsid) as rank
from processed.d20_inactive_level_31_snsid) t
where rank % 2 = 0;

-- split into different apps
drop table if exists processed.d20_inactive_level_31_snsid_a_us;
create table processed.d20_inactive_level_31_snsid_a_us as
select a.snsid
from processed.d20_inactive_level_31_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.us.prod';

drop table if exists processed.d20_inactive_level_31_snsid_a_de;
create table processed.d20_inactive_level_31_snsid_a_de as
select a.snsid
from processed.d20_inactive_level_31_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.de.prod';

drop table if exists processed.d20_inactive_level_31_snsid_a_fr;
create table processed.d20_inactive_level_31_snsid_a_fr as
select a.snsid
from processed.d20_inactive_level_31_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.fr.prod';

drop table if exists processed.d20_inactive_level_31_snsid_a_nl;
create table processed.d20_inactive_level_31_snsid_a_nl as
select a.snsid
from processed.d20_inactive_level_31_snsid_a a
    join processed.dim_user u on a.snsid = u.snsid
where u.app = 'royal.nl.prod';

-- unload data to s3
unload ('select * from processed.d20_inactive_level_31_snsid_a_us')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_31_snsid_a_us_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_31_snsid_a_de')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_31_snsid_a_de_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_31_snsid_a_fr')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_31_snsid_a_fr_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

unload ('select * from processed.d20_inactive_level_31_snsid_a_nl')
to 's3://com.funplus.bitest/rs/ab_test_0507/d20_inactive_level_31_snsid_a_nl_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP
ALLOWOVERWRITE;

-- TODO: AB test result analysis
drop table if exists processed.dim_user_ab_test;
create table processed.dim_user_ab_test as
select distinct 'd20_inactive_level_5_10 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_5_10_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_5_10 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_5_10_snsid_b t on u.snsid = t.snsid
union

select distinct 'd20_inactive_level_11_15 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_11_15_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_11_15 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_11_15_snsid_b t on u.snsid = t.snsid

union
select distinct 'd20_inactive_level_16_20 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_16_20_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_16_20 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_16_20_snsid_b t on u.snsid = t.snsid

union
select distinct 'd20_inactive_level_21_30 A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_21_30_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_21_30 B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_21_30_snsid_b t on u.snsid = t.snsid

union
select distinct 'd20_inactive_level_31_plus A' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_31_snsid_a t on u.snsid = t.snsid
union
select distinct 'd20_inactive_level_31_plus B' as ab_group, u.*
from processed.dim_user u
    join processed.d20_inactive_level_31_snsid_b t on u.snsid = t.snsid;


-- create agg_kpi_ab_test
drop table if exists processed.agg_kpi_ab_test;
create table processed.agg_kpi_ab_test as
select  date
      ,cast(date as VARCHAR) as date_str
      ,d.app
      ,d.app_version
      ,u.install_source
      ,u.install_source_group
      ,cast(d.level_end - ((d.level_end - 1) % 10) as varchar) || ' ~ ' || cast(d.level_end - ((d.level_end - 1) % 10) + 9 as varchar) as level_range
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.is_whale
      ,u.ab_group
      ,sum(d.is_new_user) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd_iap > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_ads) as revenue_ads
      ,sum(d.session_cnt) as session_cnt
      ,sum(d.playtime_sec) as playtime_sec
from processed.fact_dau_snapshot d
    join processed.dim_user_ab_test u on d.user_key = u.user_key
where d.date >= '2015-05-07'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;









BEGIN TRANSACTION;

DELETE FROM processed.agg_sendgrid_retention WHERE install_date > (current_date - 180);


INSERT INTO processed.agg_sendgrid_retention
SELECT install_date, app, retention_days, language
  ,os
  ,browser
  ,country
  ,campaign_name
  ,install_source
  ,is_payer
  , email_flag
  , count(*)
    FROM (
  select
   du.install_date   
   ,du.app
   ,datediff(day, du.install_date, fds.date) AS retention_days
   ,du.uid
  ,du.language 
  ,du.os
  ,du.browser
  ,du.country
  ,du.install_source
  ,du.is_payer
  , case when fs.app_user_id is NOT null then campaign_name  else 'No Email' end AS campaign_name
  , case when fs.app_user_id is NOT null then true else false end AS email_flag
FROM
(
  SELECT install_date
   ,app
   ,user_key
   ,uid   
  ,language
  ,os
  ,browser
  ,country
  ,install_source
  ,is_payer FROM
processed.dim_user x  WHERE 
install_date > (current_date - 180) AND install_date <= (current_date - 1))
du
INNER JOIN 
(select app, user_key, date from processed.fact_dau_snapshot y
  WHERE date > (current_date - 180) AND  date <= (current_date - 1))
fds ON (du.app = fds.app AND du.user_key = fds.user_key and du.install_date <= fds.date)
LEFT OUTER JOIN 
(SELECT distinct app_id, app_user_id, 
first_value(campaign_name ignore nulls) OVER (partition by app_id,app_user_id order by campaign_date desc
                                                  rows between unbounded preceding and unbounded following) as campaign_name FROM processed.fact_sendgrid 
WHERE campaign_date >(current_date - 180) AND campaign_date <= (current_date - 1) AND delivered_ts is not null and bounce_ts is null )
fs
ON (du.app = fs.app_id and du.uid = fs.app_user_id)
) ASR GROUP BY install_date, app, retention_days, language
  ,os
  ,browser
  ,country
  ,install_source
  ,is_payer
  ,campaign_name
  , email_flag;

COMMIT;

---------------------- agg_sendgrid_ltv table --------------------------

------- create temp table for player days
create temp table tmp_player_day as
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day
union all
select 120 as day
union all
select 150 as day
union all
select 180 as day;

--------- create temp table to get the last date in dau table.
create temp table tmp_last_date as
select max(date) as last_date
from processed.fact_dau_snapshot;

--------- create ltv cube table
create temp table tmp_agg_ltv_cube as
SELECT      d.day as ltv_days
           ,trunc(u.install_ts) as install_date
           ,u.os
           ,u.browser
           ,u.country
           ,u.app
           ,u.install_source
           ,u.install_source_group
           ,u.is_payer
           ,u.user_key
           ,case when fs.app_user_id is NOT null then campaign_name  else 'No Email' end AS campaign_name
           ,case when fs.app_user_id is NOT null then true else false end AS email_flag
FROM        processed.dim_user u
join   tmp_player_day d
       on   DATEDIFF('day', u.install_date, (select last_date from tmp_last_date)) >= d.day
LEFT OUTER JOIN 
(SELECT distinct app_id, app_user_id, 
first_value(campaign_name ignore nulls) OVER (partition by app_id,app_user_id order by campaign_date desc
                                                  rows between unbounded preceding and unbounded following) as campaign_name FROM processed.fact_sendgrid 
WHERE campaign_date >(current_date - 180) AND campaign_date <= (current_date - 1) AND delivered_ts is not null and bounce_ts is null )
fs
ON (u.app = fs.app_id and u.uid = fs.app_user_id)
where u.install_date > (current_date - 180) AND u.install_date <= (current_date - 1) and u.is_tester = 0;

-------- insert into ltv table

DELETE FROM processed.agg_sendgrid_ltv WHERE install_date > (current_date - 180);

insert into processed.agg_sendgrid_ltv
(
  ltv_days
  ,install_date
  ,os
  ,browser
  ,country
  ,app_id
  ,install_source
  ,install_source_group
  ,campaign_name
  ,is_payer
  ,email_flag
  ,users
  ,revenue
)
SELECT
            c.ltv_days
           ,c.install_date
           ,c.os
           ,c.browser
           ,c.country
           ,c.app
           ,c.install_source
           ,c.install_source_group
           ,c.campaign_name
           ,c.is_payer
           ,c.email_flag
           ,count(distinct c.user_key) as users
           ,sum(COALESCE(du.revenue_usd, 0)) as revenue
FROM        tmp_agg_ltv_cube c
left join   processed.fact_dau_snapshot du
       on   c.user_key = du.user_key and DATEDIFF('day', c.install_date, du.date) <= c.ltv_days
group by 1,2,3,4,5,6,7,8,9,10,11;



delete from processed.agg_sendgrid_ltv
using tmp_last_date d
where DATEDIFF('day', install_date, d.last_date) < ltv_days;



create temp table player_day_cam as                                                                                        
     SELECT 1 AS day UNION ALL                                                                                        
     SELECT 2 UNION ALL                                                                                               
     SELECT 3 UNION ALL                                                                                               
     SELECT 4 UNION ALL                                                                                               
     SELECT 5 UNION ALL                                                                                               
     SELECT 6 UNION ALL                                                                                               
     SELECT 7 UNION ALL                                                                                               
     SELECT 14 UNION ALL                                                                                              
     SELECT 15 UNION ALL                                                                                              
     SELECT 21 UNION ALL                                                                                              
     SELECT 28 UNION ALL                                                                                              
     SELECT 30 UNION ALL                                                                                              
     SELECT 45 UNION ALL                                                                                              
     SELECT 60 UNION ALL                                                                                              
     SELECT 90 UNION ALL                                                                                              
     SELECT 120 UNION ALL                                                                                             
     SELECT 150 UNION ALL                                                                                             
     SELECT 180 UNION ALL                                                                                             
     SELECT 210 UNION ALL                                                                                             
     SELECT 240 UNION ALL                                                                                             
     SELECT 270 UNION ALL                                                                                             
     SELECT 300 UNION ALL                                                                                             
     SELECT 330 UNION ALL                                                                                             
     SELECT 360                                                                                                       
; 



--create temp table for users in each campaign_date
create temp table tmp_users as
select  f.campaign_date
       ,f.campaign_name
       ,u.os
       ,u.country
       ,u.app as app_id
       ,u.install_source as install_source
       ,u.browser
       ,u.language
       ,u.is_payer
       ,count(distinct f.app_user_id) as user_cnt
from processed.fact_sendgrid f left join processed.dim_user u on f.app_id=u.app and f.app_user_id=u.uid
group by 1,2,3,4,5,6,7,8,9;

--create temp table to get the last date in dau table.
create temporary table last_date_cam
as
select max(date) as date
from processed.fact_dau_snapshot;


--create cube to calculate the retention
create temp table player_day_cube_cam 
as
select  pd.day as player_day
       ,nu.campaign_date
       ,nu.campaign_name
       ,nu.os
       ,nu.country
       ,nu.app_id
       ,nu.install_source
       ,nu.browser
       ,nu.language
       ,nu.is_payer
       ,nu.user_cnt
from   tmp_users nu
join   player_day_cam pd
on 1=1
join   last_date_cam d
on 1=1
where  datediff('day',nu.campaign_date,d.date) >= pd.day;       

--create agg_campaign_retention table

drop table processed.agg_campaign_retention;
create table processed.agg_campaign_retention as
select  cube.player_day
       ,cube.campaign_date
       ,cube.campaign_name
       ,cube.os
       ,cube.country
       ,cube.app_id
       ,cube.install_source
       ,cube.browser
       ,cube.language
       ,cube.is_payer
       ,cube.user_cnt
       ,coalesce(r.retained_user_cnt,0) as retained_user_cnt
from   player_day_cube_cam cube
left join
      (
        select   datediff('day',fu.campaign_date,du.date) as player_day
                ,fu.campaign_date
                ,u.os
                ,u.country
                ,u.app as app_id
                ,u.install_source as install_source
                ,u.browser
                ,u.language
                ,u.is_payer
                ,fu.campaign_name
                ,count(distinct du.user_key) as retained_user_cnt
        from    processed.fact_dau_snapshot du
        join    (select app_user_id,app_id,campaign_name,campaign_date, md5(app_id||app_user_id) as user_key from processed.fact_sendgrid) fu
        on du.user_key=fu.user_key and du.app=fu.app_id and du.uid=fu.app_user_id
        join    processed.dim_user u
        on      du.user_key = u.user_key
        where   datediff('day',fu.campaign_date,du.date) in  (select day from player_day_cam)
        group by 1,2,3,4,5,6,7,8,9,10                       
      )r
on  cube.player_day = r.player_day
and cube.campaign_date = r.campaign_date
and coalesce(cube.app_id,'') = coalesce(r.app_id,'')
and coalesce(cube.os,'') = coalesce(r.os,'') 
and coalesce(cube.country,'') = coalesce(r.country,'') 
and coalesce(cube.install_source,'') = coalesce(r.install_source,'')
and coalesce(cube.campaign_name,'') = coalesce(r.campaign_name,'')
and coalesce(cube.browser,'') = coalesce(r.browser,'')
and coalesce(cube.language,'') = coalesce(r.language,'')
and cube.is_payer = r.is_payer;


--create agg_campaign_ltv table

drop table processed.agg_campaign_ltv;
create table processed.agg_campaign_ltv as
select  cube.player_day
       ,cube.campaign_date
       ,cube.campaign_name
       ,cube.os
       ,cube.country
       ,cube.app_id
       ,cube.install_source
       ,cube.browser
       ,cube.language
       ,cube.is_payer
       ,cube.user_cnt
       ,coalesce(r.revenue,0) as revenue
from   player_day_cube_cam cube
left join
      (
        select   datediff('day',fu.campaign_date,du.date) as player_day
                ,fu.campaign_date
                ,u.os
                ,u.country
                ,u.app as app_id
                ,u.install_source as install_source
                ,u.browser
                ,u.language
                ,u.is_payer
                ,fu.campaign_name
                ,sum(revenue_usd) as revenue
        from    processed.fact_dau_snapshot du
        join    (select app_user_id,app_id,campaign_name,campaign_date, md5(app_id||app_user_id) as user_key from processed.fact_sendgrid) fu
        on du.user_key=fu.user_key and du.app=fu.app_id and du.uid=fu.app_user_id
        join    processed.dim_user u
        on      du.user_key = u.user_key
        where   datediff('day',fu.campaign_date,du.date) in  (select day from player_day_cam)
        group by 1,2,3,4,5,6,7,8,9,10                       
      )r
on  cube.player_day = r.player_day
and cube.campaign_date = r.campaign_date
and coalesce(cube.app_id,'') = coalesce(r.app_id,'')
and coalesce(cube.os,'') = coalesce(r.os,'') 
and coalesce(cube.country,'') = coalesce(r.country,'') 
and coalesce(cube.install_source,'') = coalesce(r.install_source,'')
and coalesce(cube.campaign_name,'') = coalesce(r.campaign_name,'')
and coalesce(cube.browser,'') = coalesce(r.browser,'')
and coalesce(cube.language,'') = coalesce(r.language,'')
and cube.is_payer = r.is_payer;




COMMIT;

END TRANSACTION;




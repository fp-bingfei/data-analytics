--------------------------------------------------------------------------------------------------------------------------------------------
--RS fact dau snapshot
--Version 1.2
--Author Robin
/**
Description:
This script is generate dim_user tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_user
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table temp_dim_user_latest
(
  user_key                  VARCHAR(50) NOT NULL ENCODE LZO,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  user_name                 VARCHAR(256) ENCODE LZO,
  email                     VARCHAR(256) ENCODE LZO,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(16) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4) DEFAULT 0,
  payment_cnt               INTEGER DEFAULT 0,
  rc_wallet                 BIGINT DEFAULT 0,
  coin_wallet               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA,
  is_tester                 SMALLINT DEFAULT 0,
  is_whale                  SMALLINT DEFAULT 0
)  
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);


insert into temp_dim_user_latest
(
          user_key 
         ,uid 
         ,snsid 
         ,install_ts
         ,install_date
         ,app 
         ,language 
         ,birth_date 
         ,gender 
         ,app_version
         ,level
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,is_payer
)
select    user_key 
         ,uid 
         ,snsid 
         ,install_ts
         ,trunc(install_ts)
         ,app 
         ,language 
         ,null as birth_date 
         ,null as gender 
         ,null as app_version
         ,level_end
         ,os 
         ,os_version 
         ,t.country_code
         ,coalesce(c.country,'Unknown') as country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
from  (
         select *
                ,row_number() over (partition by user_key order by date desc) as row
         from processed.fact_dau_snapshot
         where date >= (
                        select start_date
                        from rs.processed.tmp_start_date
                     )
      )t
left join processed.dim_country c on c.country_code=t.country_code
where t.row = 1;

-- create temp table to get the revenue for each user till now + conversion
create temp table temp_user_first_payment as
select u.user_key
       ,min(ts) as conversion_ts
from temp_dim_user_latest u
join processed.fact_revenue r on r.user_key=u.user_key
where usd_iap > 0
group by 1;

create temp table temp_user_total_revenue as
select u.user_key
       ,sum(usd) as revenue
       ,count(1) as payment_cnt
from temp_dim_user_latest u
join processed.fact_revenue r on r.user_key=u.user_key
where usd > 0
group by 1;

update temp_dim_user_latest
set conversion_ts = t.conversion_ts
from temp_user_first_payment t
where temp_dim_user_latest.user_key = t.user_key;

update temp_dim_user_latest
set total_revenue_usd = t.revenue,
    payment_cnt = t.payment_cnt
from temp_user_total_revenue t
where temp_dim_user_latest.user_key = t.user_key;

-- update install source
update temp_dim_user_latest
set install_source = substring(f.install_source, 1, 128),
    campaign = f.campaign,
    sub_publisher =  f.sub_publisher,
    creative_id = f.creative_id
from processed.fact_user_install_source f
where temp_dim_user_latest.user_key = f.user_key;

-- update install source group
update temp_dim_user_latest
set install_source_group = i.install_source_group
from processed.ref_install_source_group i
where temp_dim_user_latest.install_source = i.install_source;

update temp_dim_user_latest
set install_source_group = 'bookmark'
where install_source_group = '' and install_source like '%bookmark%';

update temp_dim_user_latest
set install_source_group = 'marketing'
where install_source_group = '' and (install_source like 'C%' or install_source like 'RS%');

update temp_dim_user_latest
set install_source_group = 'new'
where temp_dim_user_latest.install_source_group = '';

-- create temp table temp_user_last_login
create temp table temp_user_last_login as
select user_key, max(ts_start) as last_login_ts
from processed.fact_session
group by 1;


update temp_dim_user_latest
set last_login_ts = t.last_login_ts
from temp_user_last_login t
where temp_dim_user_latest.user_key = t.user_key;

-- update is_tester
update temp_dim_user_latest
set is_tester = 1
from processed.test_ids t
where temp_dim_user_latest.snsid = t.snsid;

-- update is_whale
update temp_dim_user_latest
set is_whale = 1
from processed.whale_ids w
where temp_dim_user_latest.snsid = w.snsid;

-- delete old user status in dim_user
delete from processed.dim_user
where user_key in
(
 select user_key from temp_dim_user_latest
);

-- insert the new status of users
insert into processed.dim_user
(
  user_key
  ,app
  ,uid
  ,snsid
  ,install_ts
  ,install_date
  ,install_source
  ,install_source_group
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,payment_cnt
  ,last_login_ts
  ,is_tester
  ,is_whale
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,install_ts
  ,install_date
  ,install_source
  ,install_source_group
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,payment_cnt
  ,last_login_ts
  ,is_tester
  ,is_whale
from temp_dim_user_latest;

-- TODO: For EAS report
update processed.dim_user
set email = case when u.email = 'NULL' then '' else u.email end
    ,gender = u.gender
    ,user_name = u.name
    ,rc_wallet = CAST(case when rc = 'NULL' then '0' else regexp_replace(rc, '\\.[0-9]+', '') end AS BIGINT)
    ,coin_wallet = CAST(case when coins = 'NULL' then '0' else regexp_replace(coins, '\\.[0-9]+', '') end AS BIGINT)
from tbl_user u
where processed.dim_user.app = u.app and processed.dim_user.uid = u.uid and processed.dim_user.snsid = u.snsid;

create temp table payment_info as
select app, uid, snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from
(select app, uid, snsid, pay_ts as ts
from processed.tbl_payment
union
select app, uid, snsid, ts
from processed.fact_revenue) t
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    t.app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,'' as additional_email
    ,t.install_source
    ,t.install_ts
    ,coalesce(t.language, u.language) as language
    ,t.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,CAST(case when t.rc = 'NULL' then '0' else regexp_replace(t.rc, '\\.[0-9]+', '') end AS BIGINT) as rc
    ,CAST(case when t.coins = 'NULL' then '0' else regexp_replace(t.coins, '\\.[0-9]+', '') end AS BIGINT) as coins
    ,coalesce(u.last_login_ts, case when t.install_ts > '2014-07-15' then t.install_ts else '2014-07-15' end) as last_login_ts
from tbl_user t
    left join payment_info p on t.app = p.app and t.uid = p.uid and t.snsid = p.snsid
    left join processed.dim_user u on t.app = u.app and t.uid = u.uid and t.snsid = p.snsid;


-------------------------------------------------
--RS user install source
--Version 1.2
--Author Robin
/**
Description:
This script get user install source
**/
-------------------------------------------------
CREATE TEMP TABLE tmp_fact_user_install_source
(
  user_key                  VARCHAR(64) NOT NULL ENCODE LZO,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  install_date              DATE ENCODE DELTA,
  install_source_event_raw  VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw        VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source            VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign, sub_publisher, creative_id);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get the users whose install source should be updated
------------------------------------------------------------------------------------------------------------------------
insert into tmp_fact_user_install_source (user_key, app, uid, snsid, install_date)
select distinct md5(app || uid), app, uid, snsid, install_date
from rs.processed.fact_session
where date_start >= (select start_date from rs.processed.tmp_start_date);

-- delete users whose install source have been updated
delete
from tmp_fact_user_install_source
using rs.processed.fact_user_install_source t2
where tmp_fact_user_install_source.user_key = t2.user_key and
      tmp_fact_user_install_source.snsid = t2.snsid;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get first no 'Organic' install source from adjust
------------------------------------------------------------------------------------------------------------------------;

------------------------------------------------------------------------------------------------------------------------
--Step 3. Get first no 'Organic' install source from event data.
--For history data, we can only get install source from event data
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_event_install_source as
select user_key, app, uid, snsid,
    first_value(install_source ignore nulls) over (partition by app, uid, snsid order by ts_start rows between unbounded preceding and unbounded following) as install_source
from rs.processed.fact_session
where date_start >= (select start_date from rs.processed.tmp_start_date) and
      lower(install_source) != 'organic'
order by app, uid, snsid;

update tmp_fact_user_install_source
set install_source_event_raw = t.install_source,
    install_source_event = t.install_source
from tmp_user_event_install_source t
where tmp_fact_user_install_source.user_key = t.user_key and
      tmp_fact_user_install_source.snsid = t.snsid;

------------------------------------------------------------------------------------------------------------------------
--Step 4. Normalize install_source_event and install_source_adjust from adjust and event data
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_event = split_part(install_source_event_raw, '::', 1);

update tmp_fact_user_install_source
set install_source_event = replace(install_source_event, '\"', '');

update tmp_fact_user_install_source
set install_source_event = 'Organic'
where lower(install_source_event) = 'organic';

update tmp_fact_user_install_source
set install_source_adjust = split_part(install_source_adjust_raw, '::', 1);

------------------------------------------------------------------------------------------------------------------------
--Step 5. Update install_source, install_source_adjust have higher priority
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_raw = install_source_adjust_raw,
    install_source = install_source_adjust
where install_source = 'Organic' and install_source_adjust != '';

update tmp_fact_user_install_source
set install_source_raw = install_source_event_raw,
    install_source = install_source_event
where install_source = 'Organic' and install_source_event != '';

------------------------------------------------------------------------------------------------------------------------
--Step 6. Normalize install_source,
-- Such as mapping both ‘Adperio’ and ‘AdPerio’ to ‘AdPerio’, mapping both 'App Turbo’ and 'App+Turbo’ to 'App Turbo’
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_ref_install_source_map as
select distinct install_source as install_source_raw, lower(install_source) as install_source_lower, install_source
from tmp_fact_user_install_source;

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\+', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\-', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, ' ', '');

insert into tmp_ref_install_source_map
select *
from rs.processed.ref_install_source_map;

create temp table tmp_install_source_map as
select install_source_lower, install_source
from
(select *, row_number () over (partition by install_source_lower order by install_source) as rank
from tmp_ref_install_source_map) t
where t.rank = 1;

update tmp_ref_install_source_map
set install_source = t.install_source
from tmp_install_source_map t
where tmp_ref_install_source_map.install_source_lower = t.install_source_lower;

truncate table rs.processed.ref_install_source_map;
insert into rs.processed.ref_install_source_map
select distinct install_source_raw, install_source_lower, install_source
from tmp_ref_install_source_map;

update tmp_fact_user_install_source
set install_source = t.install_source
from rs.processed.ref_install_source_map t
where tmp_fact_user_install_source.install_source = t.install_source_raw;

-- update campaign
update tmp_fact_user_install_source
set campaign = split_part(install_source_raw, '::', 2);

-- update sub_publisher
update tmp_fact_user_install_source
set sub_publisher = split_part(install_source_raw, '::', 3);

delete
from tmp_fact_user_install_source
where install_source = 'Organic';

insert into rs.processed.fact_user_install_source
(
  user_key
  ,app
  ,uid
  ,snsid
  ,install_date
  ,install_source_event_raw
  ,install_source_event
  ,install_source_adjust_raw
  ,install_source_adjust
  ,install_source_raw
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,install_date
  ,install_source_event_raw
  ,install_source_event
  ,install_source_adjust_raw
  ,install_source_adjust
  ,install_source_raw
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
from tmp_fact_user_install_source;


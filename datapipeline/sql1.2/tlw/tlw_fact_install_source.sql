-------------------------------------------------
--The Last War user install source
--Version 1.2
--Author Robin
/**
Description:
This script get user install source
**/
-------------------------------------------------
CREATE TEMP TABLE tmp_fact_user_install_source
(
  user_key                      VARCHAR(64) NOT NULL ENCODE LZO,
  app                           VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                           INTEGER NOT NULL,
  snsid                         VARCHAR(64) NOT NULL ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_appsflyer_raw  VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_appsflyer      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_mat_raw        VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_mat            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid, install_date, install_source_appsflyer, install_source_mat, install_source, campaign, sub_publisher, creative_id);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get the users whose install source should be updated
------------------------------------------------------------------------------------------------------------------------
insert into tmp_fact_user_install_source (user_key, app, uid, snsid)
select distinct md5(app || uid), app, uid, ''
from tlw.processed.fact_session
where date_start >= (select start_date from tlw.processed.tmp_start_date);

-- delete users whose install source have been updated
delete
from tmp_fact_user_install_source
using tlw.processed.fact_user_install_source t2
where tmp_fact_user_install_source.user_key = t2.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get first no 'Organic' install source from mat.
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_mat_install_source as
select user_id as uid,
    first_value(publisher_name ignore nulls) over (partition by user_id order by created rows between unbounded preceding and unbounded following) as install_source
from tlw.public.mat
where lower(publisher_name) != 'organic';

update tmp_fact_user_install_source
set install_source_mat_raw = t.install_source,
    install_source_mat = t.install_source
from tmp_user_mat_install_source t
where tmp_fact_user_install_source.uid = t.uid;

create temp table tmp_user_mat_install_source_by_device as
select distinct s.uid,
    first_value(publisher_name ignore nulls) over (partition by s.uid order by created rows between unbounded preceding and unbounded following) as install_source
from processed.fact_session s join mat m
        on lower(regexp_replace(s.idfa, '-', '')) = lower(regexp_replace(m.ios_ifa, '-', ''))
where s.date_start >= (select start_date from tlw.processed.tmp_start_date) and s.idfa != '' and lower(m.publisher_name) != 'organic';

update tmp_fact_user_install_source
set install_source_mat_raw = t.install_source,
    install_source_mat = t.install_source
from tmp_user_mat_install_source_by_device t
where tmp_fact_user_install_source.uid = t.uid;
------------------------------------------------------------------------------------------------------------------------
--Step 3. Get first no 'Organic' install source from appsflyer.
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_appsflyer_install_source as
select user_id as uid,
    first_value(media_source ignore nulls) over (partition by user_id order by install_time rows between unbounded preceding and unbounded following) as install_source
from tlw.public.appsflyer;

update tmp_fact_user_install_source
set install_source_appsflyer_raw = t.install_source,
    install_source_appsflyer = t.install_source
from tmp_user_appsflyer_install_source t
where tmp_fact_user_install_source.uid = t.uid;

create temp table tmp_user_appsflyer_install_source_by_device as
select distinct s.uid,
    first_value(media_source ignore nulls) over (partition by s.uid order by install_time rows between unbounded preceding and unbounded following) as install_source
from processed.fact_session s join appsflyer m
        on lower(regexp_replace(s.idfa, '-', '')) = lower(regexp_replace(m.idfa, '-', ''))
where s.date_start >= (select start_date from tlw.processed.tmp_start_date) and s.idfa != '' and lower(m.media_source) != 'organic';

update tmp_fact_user_install_source
set install_source_appsflyer_raw = t.install_source,
    install_source_appsflyer = t.install_source
from tmp_user_appsflyer_install_source_by_device t
where tmp_fact_user_install_source.uid = t.uid;
------------------------------------------------------------------------------------------------------------------------
--Step 4. Normalize install_source_appsflyer and install_source_mat from appsflyer and mat data
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_appsflyer = split_part(install_source_appsflyer_raw, '::', 1);

update tmp_fact_user_install_source
set install_source_appsflyer = replace(install_source_appsflyer, '\"', '');

update tmp_fact_user_install_source
set install_source_appsflyer = 'Organic'
where lower(install_source_appsflyer) = 'organic';

update tmp_fact_user_install_source
set install_source_mat = split_part(install_source_mat_raw, '::', 1);

update tmp_fact_user_install_source
set install_source_mat = replace(install_source_mat, '\"', '');

update tmp_fact_user_install_source
set install_source_mat = 'Organic'
where lower(install_source_mat) = 'organic';

------------------------------------------------------------------------------------------------------------------------
--Step 5. Update install_source, install_source_appsflyer have higher priority
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_raw = install_source_appsflyer_raw,
    install_source = install_source_appsflyer
where install_source = 'Organic' and install_source_appsflyer != '';


update tmp_fact_user_install_source
set install_source_raw = install_source_mat_raw,
    install_source = install_source_mat
where install_source = 'Organic' and install_source_mat != '';

------------------------------------------------------------------------------------------------------------------------
--Step 6. Normalize install_source,
-- Such as mapping both ‘Adperio’ and ‘AdPerio’ to ‘AdPerio’, mapping both 'App Turbo’ and 'App+Turbo’ to 'App Turbo’
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_ref_install_source_map as
select distinct install_source as install_source_raw, lower(install_source) as install_source_lower, install_source
from tmp_fact_user_install_source;

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\+', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\-', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, ' ', '');

insert into tmp_ref_install_source_map
select *
from tlw.processed.ref_install_source_map;

create temp table tmp_install_source_map as
select install_source_lower, install_source
from
(select *, row_number () over (partition by install_source_lower order by install_source) as rank
from tmp_ref_install_source_map) t
where t.rank = 1;

update tmp_ref_install_source_map
set install_source = t.install_source
from tmp_install_source_map t
where tmp_ref_install_source_map.install_source_lower = t.install_source_lower;

truncate table tlw.processed.ref_install_source_map;
insert into tlw.processed.ref_install_source_map
select distinct install_source_raw, install_source_lower, install_source
from tmp_ref_install_source_map;

update tmp_fact_user_install_source
set install_source = t.install_source
from tlw.processed.ref_install_source_map t
where tmp_fact_user_install_source.install_source = t.install_source_raw;

delete
from tmp_fact_user_install_source
where install_source = 'Organic';

insert into tlw.processed.fact_user_install_source
(
  user_key
  ,app
  ,uid
  ,snsid
  ,install_date
  ,install_source_appsflyer_raw
  ,install_source_appsflyer
  ,install_source_mat_raw
  ,install_source_mat
  ,install_source_raw
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,install_date
  ,install_source_appsflyer_raw
  ,install_source_appsflyer
  ,install_source_mat_raw
  ,install_source_mat
  ,install_source_raw
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
from tmp_fact_user_install_source;


--------------------------------------------------------------------------------------------------------------------------------------------
--The Last War session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

-- TODO: raw data
----------------------------------------------------------------------------------------------------------------------------------------------
-- extract data from json
----------------------------------------------------------------------------------------------------------------------------------------------
delete from  processed.events_raw
where  load_hour >= (
                     select raw_data_start_date
                     from tlw.processed.tmp_start_date
                );

insert into processed.events_raw
select
    load_hour
    ,json_extract_path_text(json_str, 'event') as event
    ,trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) as date
    ,dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str, 'app_id') as app
    ,CAST(json_extract_path_text(json_str, 'user_id') AS INTEGER) as uid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gameserver_id') as server
    ,dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,'--' as country_code
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'level') AS INTEGER) as level
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'ip') as ip
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os') as os
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os_version') as os_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'app_version') as app_version
    ,json_extract_path_text(json_str, 'session_id') as session_id
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'idfa') as idfa
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'mac_address') as mac_address
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device') as device
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'lang') as lang
    ,json_str as json_str
    ,json_extract_path_text(json_str, 'properties') as properties
    ,json_extract_path_text(json_str, 'collections') as collections
    ,md5(json_str) as md5
from processed.raw_data_s3
where  load_hour >= (
                     select raw_data_start_date
                     from tlw.processed.tmp_start_date
                );

-- ip_country_map
create temp table ip_address as
select distinct
    ip,
    cast(split_part(ip,'.',1) AS BIGINT) * 16777216
    + cast(split_part(ip,'.',2) AS BIGINT) * 65536
    + cast(split_part(ip,'.',3) AS BIGINT) * 256
    + cast(split_part(ip,'.',4) AS BIGINT) as ip_int
from processed.events_raw
where load_hour >= (
                     select raw_data_start_date
                     from tlw.processed.tmp_start_date
                    )
and   ip != ''
and   ip != 'unknown';

create temp table ip_country_map as
select
     i.ip
    ,i.ip_int
    ,c.country_iso_code as country_code
from ip_address i
    join processed.ip_integer_range_block r on i.ip_int >= network_start_integer and i.ip_int <= network_last_integer
    join processed.geoip_country c on r.geoname_id = c.geoname_id;

update processed.events_raw
set country_code = i.country_code
from ip_country_map i
where processed.events_raw.ip = i.ip
and   load_hour >= (
                     select raw_data_start_date
                     from tlw.processed.tmp_start_date
                   );

-- TODO: check the data
-- drop table if exists tlw.processed.tab_spiderman_capture;
-- create table tlw.processed.tab_spiderman_capture (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number_old" BIGINT NOT NULL DEFAULT 0,
--   "row_number_new" BIGINT NOT NULL DEFAULT 0,
--   "row_number_diff" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);
--
-- drop table if exists tlw.processed.tab_spiderman_capture_history;
-- create table tlw.processed.tab_spiderman_capture_history (
--   "record_date" date NOT NULL,
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(record_date, event, date, hour);

truncate table tlw.processed.tab_spiderman_capture;
drop table tlw.processed.tab_spiderman_capture_old;
alter table tlw.processed.tab_spiderman_capture_new rename to tab_spiderman_capture_old;

create table tlw.processed.tab_spiderman_capture_new (
  "event" varchar(32) NOT NULL,
  "date" date NOT NULL,
  "hour" SMALLINT NOT NULL,
  "row_number" BIGINT NOT NULL
)
  DISTKEY(event)
  SORTKEY(event, date, hour);

insert into tlw.processed.tab_spiderman_capture_new
select event, trunc(ts) as date, extract(hour from ts) as hour, count(1)
from tlw.processed.events_raw
group by event, trunc(ts), extract(hour from ts);

insert into tlw.processed.tab_spiderman_capture
select n.event, n.date, n.hour, case when o.row_number is null then 0 else o.row_number end as row_number_old,
    n.row_number as row_number_new
from tlw.processed.tab_spiderman_capture_new n left join tlw.processed.tab_spiderman_capture_old o
    on n.event = o.event and n.date = o.date and n.hour = o.hour
union
select o.event, o.date, o.hour, o.row_number as row_number_old,
    case when n.row_number is null then 0 else n.row_number end as row_number_new
from tlw.processed.tab_spiderman_capture_old o left join tlw.processed.tab_spiderman_capture_new n
    on n.event = o.event and n.date = o.date and n.hour = o.hour;

update tlw.processed.tab_spiderman_capture
set row_number_diff = row_number_new - row_number_old;

delete from tlw.processed.tab_spiderman_capture_history where record_date = CURRENT_DATE;
insert into tlw.processed.tab_spiderman_capture_history
(
    record_date
    ,event
    ,date
    ,hour
    ,row_number
)
select
    CURRENT_DATE as record_date
    ,event
    ,date
    ,hour
    ,row_number
from tlw.processed.tab_spiderman_capture_new;

-- TODO: fact tables
----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_session
where  date_start >= (
                     select raw_data_start_date
                     from tlw.processed.tmp_start_date
                );

create temp table tmp_fact_session as
select *
from   processed.events_raw
where  event in ('newuser', 'session_start')
and    date >= (
                    select raw_data_start_date
                    from tlw.processed.tmp_start_date
               );

insert into processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,player_key
       ,server
       ,player_id
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,mac_address
       ,browser
       ,browser_version
       ,rc_bal_start
       ,rc_bal_end
       ,coin_bal_start
       ,coin_bal_end
       ,ab_test
       ,ab_variant
       ,session_length
)
select  date as date_start
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,null as snsid
       ,MD5(server || uid) AS player_key
       ,server
       ,null AS player_id
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,null as install_source
       ,app_version as app_version
       ,session_id as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,level as level_start
       ,level as level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,lang AS language
       ,device AS device
       ,idfa AS idfa
       ,mac_address AS mac_address
       ,null as browser
       ,null as browser_version
       ,null as rc_bal_start
       ,null as rc_bal_end
       ,null as coin_bal_start
       ,null as coin_bal_end
       ,null as ab_test
       ,null as ab_variant
       ,null as session_length
from   tmp_fact_session;


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_revenue
where  date >= (
                     select raw_data_start_date
                     from   tlw.processed.tmp_start_date
                );

create temp table tmp_fact_revenue as
select *
from    processed.events_raw
where   event = 'payment'
and     date >= (
                    select raw_data_start_date
                    from tlw.processed.tmp_start_date
                );


insert into processed.fact_revenue
(
       date
       ,user_key
       ,app
       ,uid
       ,snsid
       ,player_key
       ,server
       ,player_id
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts
       ,level
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,mac_address
       ,browser
       ,browser_version
       ,ab_test
       ,ab_variant

       ,payment_processor
       ,product_id
       ,product_name
       ,product_type
       ,coins_in
       ,rc_in
       ,currency
       ,amount
       ,usd
       ,transaction_id
)
select  date
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,null as snsid
       ,MD5(server || uid) AS player_key
       ,server
       ,null AS player_id
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,null as install_source
       ,app_version as app_version
       ,session_id as session_id
       ,ts
       ,level
       ,os
       ,os_version
       ,country_code
       ,ip
       ,lang AS language
       ,device AS device
       ,idfa AS idfa
       ,mac_address AS mac_address
       ,null as browser
       ,null as browser_version
       ,null as ab_test
       ,null as ab_variant

       ,json_extract_path_text(properties,'payment_processor') AS payment_processor
       ,json_extract_path_text(properties,'iap_product_id') AS product_id
       ,json_extract_path_text(properties,'iap_product_name') AS product_name
       ,json_extract_path_text(properties,'product_type') AS product_type
       ,CAST(
               CASE WHEN json_extract_path_text (properties,'coins_in') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'coins_in')
               END AS integer
             ) AS coins_in
       ,CAST(
               CASE WHEN json_extract_path_text (properties,'rc_in') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'rc_in')
               END AS integer
             ) AS rc_in
       ,json_extract_path_text(properties,'currency') AS currency
       ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4))/100 AS amount
       ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4))/100 AS usd
       ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    tmp_fact_revenue;

create temp table tmp_duplicated_transaction_id as
select transaction_id, count(1)
from processed.fact_revenue
group by transaction_id
having count(1) > 1;

create temp table tmp_duplicated_transaction_id_rows as
select t.*, row_number() over (partition by transaction_id order by ts) rank
from
(select r.*
from processed.fact_revenue r join tmp_duplicated_transaction_id d on r.transaction_id = d.transaction_id) t;

create temp table tmp_duplicated_transaction_id_first_row as
select *
from tmp_duplicated_transaction_id_rows
where rank = 1;

alter table tmp_duplicated_transaction_id_first_row drop column rank;

delete
from processed.fact_revenue
using tmp_duplicated_transaction_id d
where processed.fact_revenue.transaction_id = d.transaction_id;

insert into processed.fact_revenue
select *
from tmp_duplicated_transaction_id_first_row;

update processed.fact_revenue
set usd = amount * case when c.factor is null then 1 else c.factor end
from public.currency c
where processed.fact_revenue.currency = c.currency and processed.fact_revenue.date = c.dt;

-- daily_level
delete from  processed.daily_level
where  date >= (
                     select raw_data_start_date
                     from   tlw.processed.tmp_start_date
                );

create temp table player_level as
select trunc(ts) as date
        ,MD5(app || uid) as user_key
        ,app
        ,uid
        ,MD5(server || uid) AS player_key
        ,server
        ,null as player_id
        ,level
from processed.events_raw
where date >= (
                    select raw_data_start_date
                    from tlw.processed.tmp_start_date
              );

insert into processed.daily_level
(
    date
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,min_level
    ,max_level
)
select
    date
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,min(level) as min_level
    ,max(level) as max_level
from player_level
group by 1,2,3,4,5,6;

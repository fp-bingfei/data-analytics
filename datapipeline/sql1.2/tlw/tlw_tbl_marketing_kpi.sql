--------------------------------------------------------------------------------------------------------------------------------------------
--TLW tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate marketing tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

truncate table tlw.processed.tab_marketing_kpi;

create temp table tmp_new_install_users as
select user_key, app, uid, install_date, install_source, campaign, sub_publisher, creative_id, country, os, is_payer
from tlw.processed.dim_user;

create temp table tmp_install_users_d3_payers as
select user_key, app, uid, max(is_payer) as d3_payer
from tlw.processed.fact_dau_snapshot
where date - install_date <= 3
group by user_key, app, uid;

create temp table tmp_install_users_revenue as
select user_key, app, uid, sum(revenue_usd) as revenue
from tlw.processed.fact_dau_snapshot
group by user_key, app, uid;

create temp table tmp_install_users_d7_revenue as
select user_key, app, uid, sum(revenue_usd) as revenue
from tlw.processed.fact_dau_snapshot
where date - install_date <= 7
group by user_key, app, uid;

create temp table tmp_install_users_d30_revenue as
select user_key, app, uid, sum(revenue_usd) as revenue
from tlw.processed.fact_dau_snapshot
where date - install_date <= 30
group by user_key, app, uid;

create temp table tmp_install_users_d90_revenue as
select user_key, app, uid, sum(revenue_usd) as revenue
from tlw.processed.fact_dau_snapshot
where date - install_date <= 90
group by user_key, app, uid;

create temp table tmp_install_users_d1_retained as
select user_key, app, uid, 1 as d1_retained
from tlw.processed.fact_dau_snapshot
where date - install_date = 1
group by user_key, app, uid;

create temp table tmp_install_users_d7_retained as
select user_key, app, uid, 1 as d7_retained
from tlw.processed.fact_dau_snapshot
where date - install_date = 7
group by user_key, app, uid;

create temp table tmp_install_users_d15_retained as
select user_key, app, uid, 1 as d15_retained
from tlw.processed.fact_dau_snapshot
where date - install_date = 15
group by user_key, app, uid;

create temp table tmp_install_users_d30_retained as
select user_key, app, uid, 1 as d30_retained
from tlw.processed.fact_dau_snapshot
where date - install_date = 30
group by user_key, app, uid;

insert into tlw.processed.tab_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_source
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os
    ,new_installs
    ,new_installs_d1
    ,new_installs_d3
    ,new_installs_d7
    ,new_installs_d15
    ,new_installs_d30
    ,new_installs_d90
    ,revenue
    ,d7_revenue
    ,d30_revenue
    ,d90_revenue
    ,payers
    ,d3_payers
    ,d1_retained
    ,d7_retained
    ,d15_retained
    ,d30_retained
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,u.install_source
    ,u.campaign
    ,u.sub_publisher
    ,u.creative_id
    ,u.country
    ,u.os
    ,count(1) as new_installs
    ,count(1) as new_installs_d1
    ,count(1) as new_installs_d3
    ,count(1) as new_installs_d7
    ,count(1) as new_installs_d15
    ,count(1) as new_installs_d30
    ,count(1) as new_installs_d90
    ,sum(case when revenue.revenue is null then 0 else revenue.revenue end) as revenue
    ,sum(case when d7_revenue.revenue is null then 0 else d7_revenue.revenue end) as d7_revenue
    ,sum(case when d30_revenue.revenue is null then 0 else d30_revenue.revenue end) as d30_revenue
    ,sum(case when d90_revenue.revenue is null then 0 else d90_revenue.revenue end) as d90_revenue
    ,sum(case when u.is_payer is null then 0 else u.is_payer end) as payers
    ,sum(case when d3p.d3_payer is null then 0 else d3p.d3_payer end) as d3_payers
    ,sum(case when d1_retained.d1_retained is null then 0 else d1_retained.d1_retained end) as d1_retained
    ,sum(case when d7_retained.d7_retained is null then 0 else d7_retained.d7_retained end) as d7_retained
    ,sum(case when d15_retained.d15_retained is null then 0 else d15_retained.d15_retained end) as d15_retained
    ,sum(case when d30_retained.d30_retained is null then 0 else d30_retained.d30_retained end) as d30_retained
from tmp_new_install_users u
    left join tmp_install_users_d3_payers d3p on u.user_key = d3p.user_key
    left join tmp_install_users_revenue revenue on u.user_key = revenue.user_key
    left join tmp_install_users_d7_revenue d7_revenue on u.user_key = d7_revenue.user_key
    left join tmp_install_users_d30_revenue d30_revenue on u.user_key = d30_revenue.user_key
    left join tmp_install_users_d90_revenue d90_revenue on u.user_key = d90_revenue.user_key
    left join tmp_install_users_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_install_users_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_install_users_d15_retained d15_retained on u.user_key = d15_retained.user_key
    left join tmp_install_users_d30_retained d30_retained on u.user_key = d30_retained.user_key
group by 1,2,3,4,5,6,7,8,9;

delete from tlw.processed.tab_marketing_kpi where install_date < '2015-01-29';

update tlw.processed.tab_marketing_kpi
set new_installs_d1 = 0,
    d1_retained = 0
where install_date >= CURRENT_DATE - 1;

update tlw.processed.tab_marketing_kpi
set new_installs_d3 = 0,
    d3_payers = 0
where install_date >= CURRENT_DATE - 3;

update tlw.processed.tab_marketing_kpi
set new_installs_d7 = 0,
    d7_retained = 0,
    d7_revenue = 0
where install_date >= CURRENT_DATE - 7;

update tlw.processed.tab_marketing_kpi
set new_installs_d15 = 0,
    d15_retained = 0
where install_date >= CURRENT_DATE - 15;

update tlw.processed.tab_marketing_kpi
set new_installs_d30 = 0,
    d30_retained = 0,
    d30_revenue = 0
where install_date >= CURRENT_DATE - 30;

update tlw.processed.tab_marketing_kpi
set new_installs_d90 = 0,
    d90_revenue = 0
where install_date >= CURRENT_DATE - 90;
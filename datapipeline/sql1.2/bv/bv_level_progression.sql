drop table processed.tab_level_progression;
create table processed.tab_level_progression as
with this_date as (select max(date(install_ts)) as date from bv.public.app_user)
select
  install_date,
  lu.level,
  u.level as current_level,
  u.country,
  u.os,
  u.install_source,
  u.is_payer,
  lu.date_start as level_up_date,
  datediff(day,u.login_ts,t.date) as last_seen_daycnt,
  datediff(day, u.install_ts,lu.ts_start) as time_to_reach_daycnt,
  count(distinct lu.user_key) as users
from processed.fact_level_up lu
join processed.dim_user u on lu.user_key=u.user_key
join this_date t on 1=1
where u.install_date>='2014-09-01'
group by 1,2,3,4,5,6,7,8,9,10;

CREATE TABLE if not exists processed.tab_level_dropoff
(
  install_date              DATE ENCODE DELTA,
  level                     integer,
  current_level             integer,
  app                       VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  is_payer                  SMALLINT ENCODE BYTEDICT,
  is_churned_3days          smallint default 0,
  is_churned_7days          smallint default 0,
  is_churned_14days         smallint default 0,
  is_churned_21days         smallint default 0,
  is_churned_30days         smallint default 0,
  user_cnt                  INTEGER DEFAULT 0
)
DISTKEY(install_date)
SORTKEY(install_date,level,current_level, app, os, country, install_source);

truncate processed.tab_level_dropoff;

insert into processed.tab_level_dropoff
with this_date as (select max(install_date) as date from processed.dim_user)
select
  u.install_date,
  l.level,
  u.level as current_level,
  u.app,
  u.os,
  u.country,
  u.install_source,
  u.is_payer,
  case when l.level=u.level and datediff(day,u.login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days,
  case when l.level=u.level and datediff(day,u.login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days,
  case when l.level=u.level and datediff(day,u.login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days,
  case when l.level=u.level and datediff(day,u.login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days,
  case when l.level=u.level and datediff(day,u.login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days,
  count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join this_date t on 1=1
where install_date>='2014-09-01' and install_date<t.date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

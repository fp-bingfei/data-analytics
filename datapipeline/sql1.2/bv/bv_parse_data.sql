---- import data from S3 directly
--drop table if exists processed.raw_data_s3;
--create table processed.raw_data_s3
--(
--	json_str          VARCHAR(20000) ENCODE LZO
--);

--truncate table processed.raw_data_s3;
--
--copy processed.raw_data_s3
--from 's3://com.funplusgame.bidata/fluentd/bv.global.testflight/2015'
--CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
--delimiter '\t'
--IGNOREBLANKLINES
--GZIP
--MAXERROR 1000;
--
--drop table if exists processed.events_raw_1_2;
--CREATE TABLE processed.events_raw_1_2
--(
--	id VARCHAR(50) NOT NULL ENCODE lzo,
--	app VARCHAR(50) NOT NULL ENCODE lzo,
--	ts TIMESTAMP NOT NULL ENCODE delta,
--	uid INTEGER NOT NULL,
--	snsid VARCHAR(100) NOT NULL ENCODE lzo DISTKEY,
--	install_ts TIMESTAMP ENCODE bytedict,
--	install_source VARCHAR(500) ENCODE lzo,
--	country_code VARCHAR(30) ENCODE bytedict,
--	ip VARCHAR(30) ENCODE lzo,
--	browser VARCHAR(30) ENCODE bytedict,
--	browser_version VARCHAR(30) ENCODE bytedict,
--	os VARCHAR(30) ENCODE bytedict,
--	os_version VARCHAR(30) ENCODE lzo,
--	event VARCHAR(50) NOT NULL ENCODE lzo,
--	properties VARCHAR(20000) ENCODE lzo,
--	collections VARCHAR(20000) ENCODE lzo,
--	bi_version varchar(3)
--)
--SORTKEY
--(
--	ts
--);

delete from public.events_raw
where trunc(ts) >= (select start_date from bv.processed.tmp_start_date) and bi_version='1.2';


insert into public.events_raw
(id,app,ts,snsid,uid,install_ts,install_source,os,os_version,event,properties,collections,bi_version,ip)
select distinct
	md5(json_str) as id
 	,json_extract_path_text(json_str, 'app_id') as app
 	,(TIMESTAMP 'epoch' + json_extract_path_text (json_str,'ts')::BIGINT*INTERVAL '1 Second') as ts
 	,json_extract_path_text(json_str, 'snsid')
 	,CAST(json_extract_path_text(json_str, 'user_id') AS INTEGER) as uid
 	,case when json_extract_path_text (json_str, 'properties','install_ts')!='' then (TIMESTAMP 'epoch' + json_extract_path_text (json_str, 'properties','install_ts')::BIGINT*INTERVAL '1 Second') else null end as install_ts
 	,json_extract_path_text(json_str, 'properties', 'install_source') as install_source
 	,json_extract_path_text(json_str, 'properties', 'os') as os
    ,json_extract_path_text(json_str, 'properties', 'os_version') as os_version
 	,json_extract_path_text(json_str, 'event') as event
    ,json_extract_path_text(json_str, 'properties') as properties
    ,json_extract_path_text(json_str, 'collections') as collections
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text(json_str, 'properties','ip') as ip
from processed.raw_data_s3
where
    length(json_extract_path_text (json_str,'ts')) =10 and
    load_hour>=trunc((TIMESTAMP 'epoch' + json_extract_path_text (json_str,'ts')::BIGINT*INTERVAL '1 Second')) and
    json_extract_path_text(json_str, 'bi_version')='1.2' and
    json_extract_path_text(json_str, 'app_id')='bv.global.prod' and
    trunc((TIMESTAMP 'epoch' + json_extract_path_text (json_str,'ts')::BIGINT*INTERVAL '1 Second')) >= (select start_date from bv.processed.tmp_start_date);

-- ip_country_map
create temp table ip_address as
select distinct ip, cast(0 AS BIGINT) as ip_int
from public.events_raw
where
    trunc(ts) >= (select start_date from bv.processed.tmp_start_date) and
    ip!='' and
    bi_version='1.2';

delete
from ip_address
using processed.ref_ip_country_map m
where ip_address.ip = m.ip;

update ip_address
set ip_int = cast(split_part(ip,'.',1) AS BIGINT) * 16777216
              + cast(split_part(ip,'.',2) AS BIGINT) * 65536
              + cast(split_part(ip,'.',3) AS BIGINT) * 256
              + cast(split_part(ip,'.',4) AS BIGINT);

insert into processed.ref_ip_country_map
(
    ip
    ,ip_int
    ,country_code
)
select
     i.ip
    ,i.ip_int
    ,c.country_iso_code as country_code
from ip_address i
    left join bv.geoip.blocks r on i.ip_int >= network_start_integer and i.ip_int <= network_last_integer
    left join bv.geoip.locations c on r.geoname_id = c.geoname_id;

update public.events_raw
set country_code = i.country_code
from processed.ref_ip_country_map i
where
 events_raw.ip = i.ip and
 events_raw.bi_version='1.2' and
 trunc(ts) >= (select start_date from bv.processed.tmp_start_date);



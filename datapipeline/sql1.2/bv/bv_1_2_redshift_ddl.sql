CREATE  TABLE processed.fact_session(
  id varchar(100) NOT NULL PRIMARY KEY ENCODE BYTEDICT, 
  app_id varchar(100) NOT NULL ENCODE BYTEDICT, 
  app_version varchar(64) ENCODE BYTEDICT, 
  user_key varchar(100) ENCODE BYTEDICT, 
  app_user_id varchar(100) NOT NULL ENCODE BYTEDICT, 
  snsid varchar(100) ENCODE BYTEDICT, 
  date_start date ENCODE DELTA, 
  date_end date ENCODE DELTA, 
  ts_start timestamp ENCODE DELTA, 
  ts_end timestamp ENCODE DELTA, 
  install_ts timestamp ENCODE DELTA, 
  install_date date ENCODE DELTA, 
  session_id varchar(156) ENCODE BYTEDICT, 
  facebook_id varchar(128) ENCODE BYTEDICT, 
  install_source varchar(1024) ENCODE BYTEDICT, 
  os varchar(100) ENCODE BYTEDICT, 
  os_version varchar(100) ENCODE BYTEDICT, 
  browser varchar(100) ENCODE BYTEDICT, 
  browser_version varchar(100) ENCODE BYTEDICT, 
  device varchar(100) ENCODE BYTEDICT, 
  country_code varchar(10) ENCODE BYTEDICT, 
  level_start int, 
  level_end int, 
  gender varchar(32) ENCODE BYTEDICT, 
  birthday date ENCODE DELTA, 
  email varchar(500) ENCODE BYTEDICT, 
  ip varchar(100) ENCODE BYTEDICT, 
  language varchar(16) ENCODE BYTEDICT, 
  locale varchar(16) ENCODE BYTEDICT, 
  rc_wallet_start int, 
  rc_wallet_end int, 
  coin_wallet_start bigint, 
  coin_wallet_end bigint, 
  ab_experiment varchar(256) ENCODE BYTEDICT, 
  ab_variant varchar(256) ENCODE BYTEDICT, 
  session_length_sec bigint, 
  idfa varchar(156) ENCODE BYTEDICT, 
  idfv varchar(156) ENCODE BYTEDICT, 
  gaid varchar(156) ENCODE BYTEDICT, 
  mac_address varchar(156) ENCODE BYTEDICT,
  android_id varchar(156) ENCODE BYTEDICT)
  DISTKEY(user_key)
  SORTKEY(user_key,date_start,app_user_id,snsid,app_id);
  

CREATE  TABLE processed.fact_revenue
(
    id varchar(100) NOT NULL PRIMARY KEY ENCODE BYTEDICT, 
    app_id varchar(100) NOT NULL ENCODE BYTEDICT, 
    app_version varchar(64) ENCODE BYTEDICT, 
    user_key varchar(100) ENCODE BYTEDICT, 
    app_user_id varchar(100) NOT NULL ENCODE BYTEDICT, 
    date date ENCODE DELTA, 
    ts timestamp ENCODE DELTA, 
    install_ts timestamp ENCODE DELTA, 
    install_date date ENCODE DELTA, 
    session_id varchar(256) ENCODE BYTEDICT, 
    level int, 
    os varchar(100) ENCODE BYTEDICT, 
    os_version varchar(100) ENCODE BYTEDICT, 
    device varchar(100) ENCODE BYTEDICT, 
    browser varchar(100) ENCODE BYTEDICT, 
    browser_version varchar(100) ENCODE BYTEDICT, 
    country_code varchar(10) ENCODE BYTEDICT, 
    install_source varchar(1024) ENCODE BYTEDICT, 
    ip varchar(100) ENCODE BYTEDICT, 
    language varchar(16) ENCODE BYTEDICT, 
    locale varchar(16) ENCODE BYTEDICT, 
    ab_experiment varchar(256) ENCODE BYTEDICT, 
    ab_variant varchar(256) ENCODE BYTEDICT, 
    coin_wallet bigint, 
    rc_wallet int, 
    payment_processor varchar(128) ENCODE BYTEDICT, 
    product_id varchar(100) ENCODE BYTEDICT, 
    product_name varchar(100) ENCODE BYTEDICT, 
    product_type varchar(100) ENCODE BYTEDICT, 
    coins_in bigint, 
    rc_in int, 
    currency varchar(32) ENCODE BYTEDICT, 
    revenue_amount numeric(15,4), 
    revenue_usd numeric(15,4), 
    transaction_id varchar(156) ENCODE BYTEDICT, 
    idfa varchar(200) ENCODE BYTEDICT, 
    idfv varchar(200) ENCODE BYTEDICT, 
    gaid varchar(200) ENCODE BYTEDICT, 
    mac_address varchar(200) ENCODE BYTEDICT,
    android_id  varchar(200)	ENCODE BYTEDICT  
)
DISTKEY(user_key)
  SORTKEY(date, ts, user_key, app_user_id,os,country_code);


CREATE TABLE processed.fact_levelup 
(
id  varchar(100) NOT NULL ENCODE BYTEDICT,
user_key  varchar(100) ENCODE BYTEDICT,
app_id  varchar(100) NOT NULL ENCODE BYTEDICT,
app_version varchar(64) ENCODE BYTEDICT,
app_user_id  varchar(100) NOT NULL ENCODE BYTEDICT,
session_id varchar(256) ENCODE BYTEDICT,
previous_level  INT,
previous_levelup_date  date ENCODE DELTA,
previous_levelup_ts  TIMESTAMP ENCODE DELTA,
current_level  INT,
levelup_date date ENCODE DELTA,
levelup_ts TIMESTAMP ENCODE DELTA,
browser varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
os varchar(100) ENCODE BYTEDICT,
os_version varchar(100) ENCODE BYTEDICT,
device varchar(100) ENCODE BYTEDICT,
country_code varchar(10) ENCODE BYTEDICT,
ip varchar(100) ENCODE BYTEDICT,
language varchar(16) ENCODE BYTEDICT,
locale varchar(32) ENCODE BYTEDICT,
ab_experiment varchar(256) ENCODE BYTEDICT,
ab_variant varchar(256) ENCODE BYTEDICT
)
distkey(user_key)
  sortkey(user_key,current_level,levelup_date);


CREATE  TABLE processed.fact_dau_snapshot(
  id varchar(100) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  date date NOT NULL ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT,
  app_version varchar(100) ENCODE BYTEDICT,
  level_start int,
  level_end int,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  country_code varchar(10) ENCODE BYTEDICT,
  country varchar(100) ENCODE BYTEDICT,
  language varchar(16) ENCODE BYTEDICT,
  ab_experiment varchar(256) ENCODE BYTEDICT,
  ab_variant varchar(256) ENCODE BYTEDICT,
  is_new_user int,
  rc_in int,
  coins_in bigint,
  rc_out int,
  coins_out bigint,
  rc_wallet int,
  coin_wallet bigint,
  is_payer int,
  is_converted_today int,
  revenue_usd numeric(15,4),
  payment_cnt int,
  session_cnt int,
  playtime_sec int)
DISTKEY(user_key)
   SORTKEY(date, user_key, app_id, country_code);



CREATE  TABLE  processed.dim_user
(
  id  varchar(100) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT,
  app_user_id  varchar(100) NOT NULL ENCODE BYTEDICT,
  snsid varchar(100) ENCODE BYTEDICT,
  facebook_id varchar(200) ENCODE BYTEDICT,
  install_ts  TIMESTAMP ENCODE DELTA,
  install_date  date ENCODE DELTA,
  install_source  varchar(1024) ENCODE BYTEDICT,
  install_subpublisher  varchar(1024) ENCODE BYTEDICT,
  install_campaign  varchar(500) ENCODE BYTEDICT,
  install_language  varchar(16) ENCODE BYTEDICT,
  install_locale  varchar(32) ENCODE BYTEDICT,
  install_country_code  varchar(16) ENCODE BYTEDICT,
  install_country  varchar(100) ENCODE BYTEDICT,
  install_os  varchar(100) ENCODE BYTEDICT,
  install_device  varchar(100) ENCODE BYTEDICT,
  install_device_alias  varchar(100) ENCODE BYTEDICT,
  install_browser  varchar(100) ENCODE BYTEDICT,
  install_gender  varchar(32) ENCODE BYTEDICT,
  install_age  varchar(10) ENCODE BYTEDICT,
  language  varchar(16) ENCODE BYTEDICT,
  locale varchar(32) ENCODE BYTEDICT,
  birthday  date ENCODE DELTA,
  gender varchar(32) ENCODE BYTEDICT,
  country_code  varchar(16) ENCODE BYTEDICT,
  country  varchar(100) ENCODE BYTEDICT,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device  varchar(100) ENCODE BYTEDICT,
  device_alias  varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  app_version varchar(64) ENCODE BYTEDICT,
  level INT,
  levelup_ts TIMESTAMP ENCODE DELTA,
  ab_experiment varchar(256) ENCODE BYTEDICT,
  ab_variant varchar(256) ENCODE BYTEDICT,
  is_payer INT ,
  conversion_ts TIMESTAMP ENCODE DELTA,
  total_revenue_usd NUMERIC(15,4) ,
  payment_cnt INT,
  last_login_ts TIMESTAMP ENCODE DELTA,
  email varchar(500) ENCODE BYTEDICT,
  last_ip varchar(100) ENCODE BYTEDICT
)  
DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);



CREATE TABLE processed.fact_tutorial
(
id varchar(100) NOT NULL ENCODE BYTEDICT,
app_id varchar(100) NOT NULL ENCODE BYTEDICT,
app_version varchar(64) ENCODE BYTEDICT,
user_key varchar(100) ENCODE BYTEDICT,
app_user_id varchar(100) NOT NULL ENCODE BYTEDICT,
date date ENCODE DELTA,
ts         timestamp ENCODE DELTA,
session_id varchar(256) ENCODE BYTEDICT,
os varchar(100) ENCODE BYTEDICT,
os_version varchar(100) ENCODE BYTEDICT,
device varchar(100) ENCODE BYTEDICT,
browser varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
country_code varchar(16) ENCODE BYTEDICT,
ip varchar(100) ENCODE BYTEDICT,
language varchar(16) ENCODE BYTEDICT,
locale varchar(32) ENCODE BYTEDICT,
level int,
tutorial_step   int,
tutorial_step_desc  varchar(100) ENCODE BYTEDICT
)DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);
;


CREATE  TABLE processed.fact_mission(
  id varchar(100) ENCODE BYTEDICT,
  mission_id varchar(100) ENCODE BYTEDICT,
  app_id varchar(100) ENCODE BYTEDICT,
  app_version varchar(32) ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  app_user_id varchar(100) ENCODE BYTEDICT,
  date date ENCODE DELTA,
  ts timestamp,
  session_id varchar(256) ENCODE BYTEDICT,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  country_code varchar(32) ENCODE BYTEDICT,
  ip varchar(100) ENCODE BYTEDICT,
  language varchar(32) ENCODE BYTEDICT,
  mission_start_ts timestamp,
  mission_status int,
  level int)
DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id)
;

create  table processed.fact_mission_objective
(
id  varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id  varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
objective_id  varchar(100) ENCODE BYTEDICT,
objective_name  varchar(100) ENCODE BYTEDICT,
objective_type  varchar(100) ENCODE BYTEDICT,
objective_amount  int,
objective_amount_remaining  int
);

create  table processed.fact_mission_parameter
(
id  varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id  varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
parameter_name  varchar(100) ENCODE BYTEDICT,
parameter_value int
)
;



create  table processed.fact_mission_statistic
(
id  varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id  varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
statistic_name  varchar(100) ENCODE BYTEDICT,
statistic_value int
)
;

create  table processed.fact_player_resources
(
id varchar(100),
app_id varchar(100),
event varchar(32),
date date,
ts timestamp,
resource_id varchar(100),
resource_name varchar(100),
resource_type varchar(100),
resource_amount int
)
distkey(id)
sortkey(app_id,event,date,ts)
;

CREATE  TABLE raw_data.raw_adjust_daily
(
  adid  varchar(200) ENCODE BYTEDICT,
  userid varchar(100) ENCODE BYTEDICT,
  game varchar(100) ENCODE BYTEDICT,
  tracker  varchar(300) ENCODE BYTEDICT,
  tracker_name varchar(1024) ENCODE BYTEDICT,
  app_id  varchar(100) ENCODE BYTEDICT,
  ip_address  varchar(100) ENCODE BYTEDICT,
  idfa  varchar(200) ENCODE BYTEDICT,
  android_id varchar(200) ENCODE BYTEDICT,
  mac_sha1  varchar(200) ENCODE BYTEDICT,
  idfa_md5  varchar(200) ENCODE BYTEDICT,
  country  varchar(100) ENCODE BYTEDICT,
  timestamp  timestamp ENCODE DELTA,
  mac_md5  varchar(200) ENCODE BYTEDICT,
  gps_adid  varchar(200) ENCODE BYTEDICT,
  device_name  varchar(100) ENCODE BYTEDICT,
  os_name  varchar(100) ENCODE BYTEDICT,
  os_version  varchar(100) ENCODE BYTEDICT
)  ;


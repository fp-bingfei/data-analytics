--------------------------------------------------------------------------------------------------------------------------------------------
--BV fact dau snapshot
--Version 1.2
--Author yanyu
/**
Discription:
This script is generate dim_user tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_user
----------------------------------------------------------------------------------------------------------------------------------------------      
            
create temp table temp_dim_user_install_latest
(
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  install_ts timestamp ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  subpublisher_source varchar(500) DEFAULT '' ENCODE BYTEDICT,
  campaign_source varchar(100) DEFAULT '' ENCODE BYTEDICT,
  language varchar(20) default 'Unknown' ENCODE BYTEDICT,
  birth_date date ENCODE DELTA,
  gender varchar(10) ENCODE BYTEDICT,
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT,
  device  VARCHAR(64) ENCODE BYTEDICT,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(32) ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None'
)
DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);
truncate temp_dim_user_install_latest;

insert into temp_dim_user_install_latest
(
          user_key
         ,uid
         ,snsid
         ,install_ts
         ,install_date
         ,app
         ,language
         ,birth_date
         ,gender
         ,app_version
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,ab_test
         ,ab_variant
)
select    user_key
         ,uid
         ,snsid
         ,install_ts
         ,trunc(install_ts)
         ,app
         ,language
         ,null as birth_date
         ,null as gender
         ,null as app_version
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,ab_test
         ,ab_variant
from processed.fact_dau_snapshot d
where date >= (select start_date from bv.processed.tmp_start_date) and date=install_date;

-- missing install info

create temp table tmp_user_missing_install_os as
select distinct
         d.user_key
         ,first_value(d.os ignore nulls) OVER (partition by d.user_key order by date desc
                                                  rows between unbounded preceding and unbounded following) as os
         ,first_value(d.os_version ignore nulls) OVER (partition by d.user_key order by date desc
                                                  rows between unbounded preceding and unbounded following) as os_version
from processed.fact_dau_snapshot d
join temp_dim_user_install_latest u on u.user_key=d.user_key
where datediff(day,u.install_date,d.date)<=7 and u.os='Unknown' and d.os!='Unknown';

create temp table tmp_user_missing_install_country as
select distinct
         d.user_key
         ,first_value(d.country_code ignore nulls) OVER (partition by d.user_key order by date desc
                                                  rows between unbounded preceding and unbounded following) as country_code
         ,first_value(d.country ignore nulls) OVER (partition by d.user_key order by date desc
                                                  rows between unbounded preceding and unbounded following) as country
from processed.fact_dau_snapshot d
join temp_dim_user_install_latest u on u.user_key=d.user_key
where datediff(day,u.install_date,d.date)<=7 and u.country='Unknown' and d.country!='Unknown';

update temp_dim_user_install_latest
set os=m.os,os_version=m.os_version
from tmp_user_missing_install_os m
where m.user_key=temp_dim_user_install_latest.user_key;

update temp_dim_user_install_latest
set country=m.country,country_code=m.country_code
from tmp_user_missing_install_country m
where m.user_key=temp_dim_user_install_latest.user_key;


------update install source------------------------------
update temp_dim_user_install_latest
set install_source = f.install_source, subpublisher_source =  f.subpublisher_source, campaign_source = f.campaign_source
from processed.fact_user_install_source f
where temp_dim_user_install_latest.user_key = f.user_key
;

-----delete old user status in dim_user_install
delete from processed.dim_user_install
where user_key in
(
 select user_key from temp_dim_user_install_latest
);

-----insert the new status of users
insert into processed.dim_user_install
select * from temp_dim_user_install_latest;
-- start date table
-- drop table bv.processed.tmp_start_date;
-- create table bv.processed.tmp_start_date as
-- select DATEADD(day, -7, CURRENT_DATE) as start_date;

update processed.tmp_start_date
set start_date = DATEADD(day, -7, ?::DATE);
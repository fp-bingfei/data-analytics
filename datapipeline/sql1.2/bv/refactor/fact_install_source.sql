DROP TABLE IF EXISTS tmp_fact_user_install_source;

CREATE TEMP TABLE tmp_fact_user_install_source
(
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app_id               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_user_id                VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_event_raw      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event          VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign_source                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  subpublisher_source                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(user_key)
  SORTKEY(app_id, user_key,app_user_id, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign_source, subpublisher_source);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get the users whose install source should be updated
------------------------------------------------------------------------------------------------------------------------
insert into tmp_fact_user_install_source (user_key, app_id, app_user_id, snsid, install_date)
select distinct user_key, app_id, app_user_id, snsid, install_date
from processed.fact_session
where date_start >= (select start_date from processed.tmp_start_date);

-- delete users whose install source have been updated
delete
from tmp_fact_user_install_source
using processed.fact_user_install_source t2
where tmp_fact_user_install_source.app_id = t2.app_id and
      tmp_fact_user_install_source.app_user_id = t2.app_user_id and
      tmp_fact_user_install_source.snsid = t2.snsid and
      tmp_fact_user_install_source.install_date = t2.install_date;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get first no 'Organic' install source from adjust
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_install_source as
select game as app_id, userid as snsid,
    first_value(tracker_name ignore nulls) over (partition by game, userid order by timestamp rows between unbounded preceding and unbounded following) as install_source
from raw_data.raw_adjust_daily
where lower(tracker_name) != 'organic'
order by game, userid;

update tmp_fact_user_install_source
set install_source_adjust_raw = t.install_source,
    install_source_adjust = t.install_source
from tmp_user_install_source t
where tmp_fact_user_install_source.app_id = t.app_id and
      tmp_fact_user_install_source.snsid = t.snsid;

------------------------------------------------------------------------------------------------------------------------
--Step 3. Get first no 'Organic' install source from event data.
--For history data, we can only get install source from event data
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_event_install_source as
select app_id, app_user_id, snsid,
    first_value(install_source ignore nulls) over (partition by app_id, app_user_id, snsid order by ts_start rows between unbounded preceding and unbounded following) as install_source
from processed.fact_session
where date_start >= (select start_date from processed.tmp_start_date) and
      lower(install_source) != 'organic'
order by app_id, app_user_id, snsid;

update tmp_fact_user_install_source
set install_source_event_raw = t.install_source,
    install_source_event = t.install_source
from tmp_user_event_install_source t
where tmp_fact_user_install_source.app_id = t.app_id and
      tmp_fact_user_install_source.app_user_id = t.app_user_id and
      tmp_fact_user_install_source.snsid = t.snsid;

------------------------------------------------------------------------------------------------------------------------
--Step 4. Normalize install_source_event and install_source_adjust from adjust and event data
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_event = split_part(install_source_event_raw, '::', 1);

update tmp_fact_user_install_source
set install_source_event = replace(install_source_event, '\"', '');

update tmp_fact_user_install_source
set install_source_event = 'Organic'
where lower(install_source_event) = 'organic';

update tmp_fact_user_install_source
set install_source_adjust = split_part(install_source_adjust_raw, '::', 1);

------------------------------------------------------------------------------------------------------------------------
--Step 5. Update install_source, install_source_adjust have higher priority
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_raw = install_source_adjust_raw,
    install_source = install_source_adjust
where install_source = 'Organic' and install_source_adjust != '';

update tmp_fact_user_install_source
set install_source_raw = install_source_event_raw,
    install_source = install_source_event
where install_source = 'Organic' and install_source_event != '';

------------------------------------------------------------------------------------------------------------------------
--Step 6. Normalize install_source,
-- Such as mapp_iding both ‘Adperio’ and ‘AdPerio’ to ‘AdPerio’, mapp_iding both 'App Turbo’ and 'App+Turbo’ to 'App Turbo’
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_ref_install_source_map as
select distinct install_source as install_source_raw, lower(install_source) as install_source_lower, install_source
from tmp_fact_user_install_source;

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\+', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\-', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, ' ', '');

insert into tmp_ref_install_source_map
select *
from processed.ref_install_source_map;

create temp table tmp_install_source_map as
select install_source_lower, install_source
from
(select *, row_number () over (partition by install_source_lower order by install_source) as rank
from tmp_ref_install_source_map) t
where t.rank = 1;

update tmp_ref_install_source_map
set install_source = t.install_source
from tmp_install_source_map t
where tmp_ref_install_source_map.install_source_lower = t.install_source_lower;

truncate table processed.ref_install_source_map;
insert into processed.ref_install_source_map
select distinct install_source_raw, install_source_lower, install_source
from tmp_ref_install_source_map;

update tmp_fact_user_install_source
set install_source = t.install_source
from processed.ref_install_source_map t
where tmp_fact_user_install_source.install_source = t.install_source_raw;

-- update campaign
update tmp_fact_user_install_source
set campaign_source = split_part(install_source_raw, '::', 2);

-- update sub_publisher
update tmp_fact_user_install_source
set subpublisher_source = split_part(install_source_raw, '::', 3)
where lower(install_source) not in ('google adwords', 'google+adwords+mobile', 'google+adwords+mobile+display');

update tmp_fact_user_install_source
set subpublisher_source = split_part(install_source_raw, '::', 4)
where lower(install_source) in ('google adwords', 'google+adwords+mobile', 'google+adwords+mobile+display');

-- Sprinklr
update tmp_fact_user_install_source
set install_source = 'Sprinklr'
where lower(campaign_source) like 'sprinklr%';

-- Mars Technologies
update processed.fact_user_install_source
set install_source = 'Mars+Media+Group'
where install_source= 'Mars Technologies';

delete
from tmp_fact_user_install_source
where install_source = 'Organic';

insert into processed.fact_user_install_source
(
    user_key
    ,app_id
    ,app_user_id
    ,snsid
    ,install_date
    ,install_source_event_raw
    ,install_source_event
    ,install_source_adjust_raw
    ,install_source_adjust
    ,install_source_raw
    ,install_source
    ,campaign_source
    ,subpublisher_source
)
select
    user_key
    ,app_id
    ,app_user_id
    ,snsid
    ,install_date
    ,install_source_event_raw
    ,install_source_event
    ,install_source_adjust_raw
    ,install_source_adjust
    ,install_source_raw
    ,install_source
    ,campaign_source
    ,subpublisher_source
from tmp_fact_user_install_source;
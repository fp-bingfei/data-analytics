drop table if exists economy;
CREATE temp table economy
(
  id varchar(128) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE LZO,
  level integer,
  event varchar(100)  ENCODE LZO,
  date date NOT NULL ENCODE DELTA,
  ts timestamp ENCODE DELTA,
  transaction_type varchar(100)  ENCODE LZO,
  detail varchar(100)  ENCODE LZO,
  in_id varchar(128)  ENCODE BYTEDICT,
  out_id varchar(128)   ENCODE BYTEDICT,
  in_amount integer,
  out_amount integer
)
DISTKEY(date)
SORTKEY(id,event,in_id,out_id);

INSERT INTO economy
SELECT
    f.id
    ,f.user_key
    ,f.level
    ,f.event
    ,f.date
    ,f.ts
    ,CASE
        when f.event='payment'
            then 'IAP'
        when f.action_type is null
            then f.event
        else
            f.action_type
     end as action_type
    ,coalesce(t.item_target_id,'N/A') as detail
    ,resource_received_id AS in_id
    ,NULL as out_id
    ,sum(resource_received_amount) AS in_amount
    ,0 as out_amount
FROM processed.fact_resources_received f
left join processed.fact_items_target t on t.id=f.id
WHERE f.date>= (select start_date from bv.processed.tmp_start_date) and resource_received_id in ('998','997')
group by 1,2,3,4,5,6,7,8,9,10

UNION ALL

SELECT
    f.id
    ,f.user_key
    ,f.level
    ,f.event
    ,f.date
    ,f.ts
    ,CASE
        when f.event='payment'
            then 'IAP'
        when f.action_type is null
            then f.event
        else
            f.action_type
     end as action_type
    ,coalesce(t.item_target_id,'N/A') as detail
    ,NULL AS in_id
    ,resource_spent_id as out_id
    ,sum(0) AS in_amount
    ,sum(resource_spent_amount) as out_amount
FROM processed.fact_resources_spent f
left join processed.fact_items_target t on t.id=f.id
WHERE f.date>= (select start_date from bv.processed.tmp_start_date) and resource_spent_id in ('998','997')
group by 1,2,3,4,5,6,7,8,9,10

UNION ALL

SELECT
    f.id
    ,f.user_key
    ,f.level
    ,f.event
    ,f.date
    ,f.ts
    ,CASE
        when f.event='payment'
            then 'IAP'
        when f.action_type is null
            then f.event
        else
            f.action_type
     end as action_type
    ,coalesce(t.item_target_id,'N/A') as detail
    ,null AS in_id
    ,f.item_spent_id as out_id
    ,sum(0) AS in_amount
    ,sum(item_spent_amount) as out_amount
FROM processed.fact_items_spent f
left join processed.fact_items_target t
    ON t.id=f.id
LEFT JOIN processed.fact_resources_spent rs
    ON rs.id=f.id
LEFT JOIN processed.fact_resources_received rr
    ON rr.id=f.id
WHERE f.date>= (select start_date from bv.processed.tmp_start_date)
  AND (rs.resource_spent_id in ('998','997') OR rr.resource_received_id in ('998','997'))
group by 1,2,3,4,5,6,7,8,9,10

UNION ALL

SELECT
    f.id
    ,f.user_key
    ,f.level
    ,f.event
    ,f.date
    ,f.ts
    ,CASE
        when f.event='payment'
            then 'IAP'
        when f.action_type is null
            then f.event
        else
            f.action_type
     end as action_type
    ,coalesce(t.item_target_id,'N/A') as detail
    ,f.item_received_id AS in_id
    ,null as out_id
    ,sum(f.item_received_amount) AS in_amount
    ,sum(0) as out_amount
FROM processed.fact_items_received f
left join processed.fact_items_target t
    ON t.id=f.id
LEFT JOIN processed.fact_resources_spent rs
    ON rs.id=f.id
LEFT JOIN processed.fact_resources_received rr
    ON rr.id=f.id
WHERE f.date>= (select start_date from bv.processed.tmp_start_date)
  AND (rs.resource_spent_id in ('998','997') OR rr.resource_received_id in ('998','997'))
group by 1,2,3,4,5,6,7,8,9,10
;

-- update detail column for transaction event

drop table if exists in_out_count;
CREATE temp table in_out_count(
  id varchar(128) NOT NULL ENCODE BYTEDICT,
  ins integer,
  outs integer,
  out_id varchar(128)  ENCODE BYTEDICT,
  out_amount integer,
  in_id varchar(128)  ENCODE BYTEDICT,
  in_amount integer
)
SORTKEY(id,outs,ins);

INSERT INTO in_out_count
select
    id,
    sum(case when in_id is not null then 1 else 0 end) as ins,
    sum(case when out_id is not null then 1 else 0 end) as outs,
    max(out_id) as out_id,max(out_amount) as out_amount,
    max(in_id) as in_id,max(in_amount) as in_amount
from economy
group by 1;

update economy
set out_id=t.out_id, in_id=t.in_id, in_amount=t.in_amount, out_amount=t.out_amount
from in_out_count t
where economy.id=t.id  and outs=1 and ins=1;

update economy
set detail=t.out_id
from in_out_count t
where  detail='N/A' and event ='transaction' and economy.id=t.id and outs=1 and ins>1 and economy.in_id is null;

update economy
set detail=t.in_id
from in_out_count t
where detail='N/A' and  event ='transaction' and economy.id=t.id and outs>1 and ins=1 and economy.out_id is null;


-- insert events into ledger

delete from processed.fact_ledger where  date>= (select start_date from processed.tmp_start_date) ;

INSERT INTO processed.fact_ledger
select distinct
    date,
    ts,
    t.user_key,
    'bv.global.prod',
    cast(u.app_user_id as integer),
    u.snsid,
    t.level,
    transaction_type,
    coalesce(i.item_name,in_id) as in_name,
    coalesce(i.item_type,'Unknown') as in_type,
    coalesce(in_amount,0),
    coalesce(o.item_name,out_id) as out_name,
    coalesce(o.item_type,'Unknown') as out_type,
    coalesce(out_amount,0),
    coalesce(d.item_name,detail)
from economy t
join processed.dim_user u on u.user_key=t.user_key
left join processed.dim_item i on i.item_id=t.in_id
left join processed.dim_item o on o.item_id=t.out_id
left join processed.dim_item d on d.item_id=t.detail
where  date(t.ts) >= (select start_date from processed.tmp_start_date);

-- DAU cube

drop table if exists temp_ledger_cube_level_bin;
create temp table temp_ledger_cube_level_bin (
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  detail varchar(180) default null
)DISTKEY(date)
  SORTKEY(date,level_bin);

insert into temp_ledger_cube_level_bin
select distinct date,ceiling((level / 5::real))*5 as level_bin,transaction_type,in_name,in_type,out_name,out_type,detail
from processed.fact_ledger
where  date >= (select start_date from processed.tmp_start_date);

drop table if exists temp_ledger_cube_all;
create temp table temp_ledger_cube_all (
  date DATE ENCODE DELTA,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  detail varchar(180) default null
)DISTKEY(date)
  SORTKEY(date);

insert into temp_ledger_cube_all
select distinct date,transaction_type,in_name,in_type,out_name,out_type,detail
from temp_ledger_cube_level_bin;

drop table if exists tmp_agg_dau_level_bin;
create  table tmp_agg_dau_level_bin (
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  detail varchar(180) default null,
  user_cnt integer default 0
) DISTKEY(date)
  SORTKEY(date,level_bin);

insert into tmp_agg_dau_level_bin
select
        d.date,
        ceiling((l.level / 5::real))*5 as level_bin,
        cube.transaction_type,
        cube.in_name,
        cube.in_type,
        cube.out_name,
        cube.out_type,
        cube.detail,
        count(distinct d.user_key)
FROM  processed.fact_dau_snapshot d
join (select distinct level from processed.dim_user) l on l.level between d.level_start and d.level_end
join temp_ledger_cube_level_bin cube on cube.date=d.date and cube.level_bin=ceiling((l.level / 5::real))*5
where  d.date >= (select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8
union all
select
        d.date,
        0 as level_bin,
        cube.transaction_type,
        cube.in_name,
        cube.in_type,
        cube.out_name,
        cube.out_type,
        cube.detail,
        count(distinct d.user_key)
FROM  processed.fact_dau_snapshot d
join temp_ledger_cube_all cube on cube.date=d.date
where d.date  >= (select start_date from processed.tmp_start_date)
group by 1,3,4,5,6,7,8;

-- Aggregated daily ledger by binned levels

--delete the historical data in case repeat loading


delete from processed.agg_daily_ledger
where date >=(select start_date from processed.tmp_start_date);

insert into processed.agg_daily_ledger
with agg_ledger as (
SELECT
        l.date,
        ceiling((l.level / 5::real))*5 as level_bin,
        l.transaction_type,
        l.in_name,
        l.in_type,
        l.out_name,
        l.out_type,
        l.detail,
        sum(l.in_amount) as in_amount,
        sum(l.out_amount) as out_amount,
        count(distinct l.user_key) as  purchaser_cnt
FROM  processed.fact_ledger l
where  l.date  >= (select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8
)
SELECT
        d.date,
        d.level_bin,
        d.transaction_type,
        d.in_name,
        d.in_type,
        d.out_name,
        d.out_type,
        d.detail,
        coalesce(l.in_amount,0) as in_amount,
        coalesce(l.out_amount,0) as out_amount,
        coalesce(l.purchaser_cnt,0) as  purchaser_cnt,
        coalesce(d.user_cnt,0) as user_cnt
FROM  tmp_agg_dau_level_bin d
left join  agg_ledger l
on
l.date=d.date and d.level_bin=l.level_bin and
d.transaction_type=l.transaction_type and
coalesce(d.in_name,'')=coalesce(l.in_name,'') and d.in_type=l.in_type and
coalesce(d.out_name,'')=coalesce(l.out_name,'') and d.out_type=l.out_type and
d.detail=l.detail
where d.level_bin!=0
union all
SELECT
        d.date,
        0,
        d.transaction_type,
        d.in_name,
        d.in_type,
        d.out_name,
        d.out_type,
        d.detail,
        coalesce(sum(l.in_amount),0) as in_amount,
        coalesce(sum(l.out_amount),0) as out_amount,
        coalesce(sum(l.purchaser_cnt),0) as  purchaser_cnt,
        coalesce(max(d.user_cnt),0) as user_cnt
FROM  tmp_agg_dau_level_bin d
left join  agg_ledger l
on
l.date=d.date and
d.transaction_type=l.transaction_type and
coalesce(d.in_name,'')=coalesce(l.in_name,'') and d.in_type=l.in_type and
coalesce(d.out_name,'')=coalesce(l.out_name,'') and d.out_type=l.out_type and
d.detail=l.detail
where d.level_bin=0
group by 1,3,4,5,6,7,8;
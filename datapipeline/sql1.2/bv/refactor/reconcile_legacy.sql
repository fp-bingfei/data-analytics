-- fact_session

delete from processed.fact_session
where date_start<'2015-03-14' ;


insert into processed.fact_session
(
    id
    ,app_id
    ,user_key
    ,app_user_id
    ,snsid
    ,date_start
    ,ts_start
    ,install_ts
    ,install_date
    ,install_source
    ,os
    ,os_version
    ,country_code
    ,level_start
    ,ip
    ,language
)
select
    md5(app || 'session_start' || uid || ts_start) as id
    ,app as app_id
    ,md5(app||uid) as user_key
    ,uid as app_user_id
    ,snsid
    ,date_start
    ,ts_start
    ,install_ts
    ,install_date
    ,install_source
    ,os
    ,os_version
    ,country_code
    ,level_start
    ,ip
    ,language
from legacy.fact_session
where app='bv.global.prod' and date_start<'2015-03-14';

-- fact_revenue

delete from processed.fact_revenue
where date<'2015-03-14';


insert into processed.fact_revenue
(
    id
    ,app_id
    ,user_key
    ,app_user_id
    ,os
    ,country_code
    ,date
    ,ts
    ,level
    ,payment_processor
    ,product_id
    ,product_name
    ,product_type
    ,currency
    ,revenue_amount
    ,revenue_usd
    ,transaction_id
)
select
    md5(app || 'payment' || uid || ts) as id
    ,app as app_id
    ,md5(app||uid) as user_key
    ,uid as app_user_id
    ,os
    ,country_code
    ,date
    ,ts
    ,level
    ,payment_processor
    ,product_id
    ,product_name
    ,product_type
    ,currency
    ,null as revenue_currency
    ,usd as revenue_usd
    ,transaction_id
from legacy.fact_revenue
where app='bv.global.prod' and date<'2015-03-14';

-- fact_levelup

delete from processed.fact_levelup
where levelup_date<'2015-03-14';


insert into processed.fact_levelup
(
    id
    ,app_id
    ,user_key
    ,app_user_id
    ,previous_level
    ,current_level
    ,levelup_date
    ,levelup_ts
)
select
    md5(app || 'level_up' || uid || ts_start) as id
    ,app as app_id
    ,md5(app||uid) as user_key
    ,uid as app_user_id
    ,level-1
    ,level-1 as previous_levelup_date
    ,date_start as levelup_date
    ,ts_start as levelup_ts
from legacy.fact_level_up
where app='bv.global.prod' and date_start<'2015-03-14';;

-- adjust


delete from raw_data.raw_adjust_daily
where timestamp<'2015-03-14';

insert into raw_data.raw_adjust_daily
(
adid,
userid,
game,
app_id,
idfa,
idfa_md5,
android_id,
mac_sha1,
ip_address,
tracker,
tracker_name,
country,
timestamp
)
select
adid,
userid,
game,
app_id,
idfa,
idfa_md5,
android_id,
mac_sha1,
ip_address,
tracker,
tracker_name,
country,
ts
from legacy.adjust
where ts<'2015-03-14';

-- agg_tutorial

delete from processed.agg_tutorial
where install_date<='2015-03-14';

insert into processed.agg_tutorial
select
    app
    ,null
    ,1
    ,install_date
    ,install_source
    ,country
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,language
    ,step
    ,step
    ,1
    ,user_cnt
from legacy.agg_tutorial
where install_date<='2015-03-14';

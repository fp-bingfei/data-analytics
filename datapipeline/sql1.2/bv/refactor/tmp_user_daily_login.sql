
TRUNCATE processed.tmp_user_daily_login;

INSERT INTO processed.tmp_user_daily_login
SELECT DISTINCT
    s.date_start AS date
    ,s.app_id
    ,s.user_key
    ,s.app_user_id
    ,s.snsid
    ,last_value(s.ts_start ignore nulls)
        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,last_value(s.facebook_id ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS facebook_id
    ,last_value(s.birthday ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS birthday
    ,last_value(s.email ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(s.install_ts ignore nulls)
             OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(s.install_ts ignore nulls)
                OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(s.install_ts ignore nulls)
            OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(s.install_date ignore nulls)
             OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(s.install_date ignore nulls)
                OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(s.install_date ignore nulls)
            OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(s.app_version ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(s.level_start ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_start
    ,last_value(s.level_end ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(s.os ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(s.os_version ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(s.country_code ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country_code
    ,last_value(s.ip ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(s.install_source ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(s.language ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(s.ab_experiment ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS ab_experiment
     ,last_value(s.ab_variant ignore nulls)
        OVER (PARTITION BY s.ab_variant, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS ab_variant
    ,last_value(s.locale ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS locale
    ,last_value(s.gender ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS gender
    ,last_value(s.device ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(s.browser ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(s.browser_version ignore nulls)
        OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,sum(1)
        OVER (PARTITION BY s.date_start, s.user_key)
     AS session_cnt
    ,sum(session_length_sec)
        OVER (PARTITION BY s.date_start, s.user_key )
    AS playtime_sec
FROM processed.fact_session s
LEFT JOIN processed.dim_user u
    ON  u.user_key=s.user_key
WHERE s.date_start >= (select start_date from processed.tmp_start_date)
;

-- Attempt to catch missing users from fact_revenue

INSERT INTO processed.tmp_user_daily_login
WITH revenue_session_cnt AS (
    SELECT
        user_key
        ,date
        ,count(distinct session_id) as session_cnt
    FROM processed.fact_revenue
    WHERE session_id is not null
    GROUP BY 1,2
)
SELECT DISTINCT
    p.date
    ,p.app_id
    ,p.user_key
    ,p.app_user_id
    ,null as snsid
    ,last_value(p.ts ignore nulls)
        OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,cast(null as varchar) AS facebook_id
    ,cast(null as date) AS birthday
    ,cast(null as varchar) AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_ts ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_ts ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(p.install_ts ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_date ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_date ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(p.install_date ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(p.app_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level
    ,last_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(p.os ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(p.os_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(p.country_code ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country_code
    ,last_value(p.ip ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(p.install_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(p.language ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(p.ab_experiment ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS ab_experiment
     ,last_value(p.ab_variant ignore nulls)
        OVER (PARTITION BY p.ab_variant, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS ab_variant
    ,last_value(p.locale ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS locale
    ,null AS gender
    ,last_value(p.device ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(p.browser ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(p.browser_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,COALESCE(c.session_cnt,1) AS session_cnt
    ,cast(null as int) AS playtime_sec
FROM processed.fact_revenue p
LEFT JOIN revenue_session_cnt c
    ON  p.user_key=c.user_key
    AND p.date=c.date
LEFT JOIN processed.tmp_user_daily_login s
    ON  s.user_key=p.user_key
    AND s.date=p.date
 LEFT JOIN processed.dim_user u
    ON  u.user_key=s.user_key
WHERE p.date >= (select start_date from processed.tmp_start_date)
  AND s.user_key is null
;

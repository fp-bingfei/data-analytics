--------------------------------------------------------------------------------------------------------------------------------------------
--BV fact dau snapshot
--Version 1.2
--Author yanyu
/**
Discription:
This script is generate dim_user tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_user
----------------------------------------------------------------------------------------------------------------------------------------------      
            
create temp table temp_dim_user_latest
(
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  install_ts timestamp ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  subpublisher_source varchar(500) DEFAULT 'Unknown' ENCODE BYTEDICT,
  campaign_source varchar(100) DEFAULT 'Unknown' ENCODE BYTEDICT,
  language varchar(20) default 'Unknown' ENCODE BYTEDICT,
  birth_date date ENCODE DELTA,
  gender varchar(10) default 'Unknown' ENCODE BYTEDICT,
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  device  VARCHAR(64) ENCODE BYTEDICT default 'Unknown',
  browser VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  browser_version VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  app_version varchar(20) ENCODE LZO,
  level integer,
  is_payer integer default 0,
  conversion_ts timestamp ENCODE DELTA,
  total_revenue_usd decimal(12,4) ENCODE BYTEDICT default 0,
  login_ts timestamp ENCODE DELTA
)  
DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);
truncate temp_dim_user_latest;
  
insert into temp_dim_user_latest
(
          user_key 
         ,uid 
         ,snsid 
         ,install_ts
         ,install_date
         ,app 
         ,language 
         ,birth_date 
         ,gender 
         ,app_version  
         ,level 
         ,os 
         ,os_version 
         ,country_code
         ,country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
)
select    user_key 
         ,uid 
         ,snsid 
         ,install_ts
         ,trunc(install_ts)
         ,app 
         ,language 
         ,null as birth_date 
         ,null as gender 
         ,null as app_version  
         ,level_end
         ,os 
         ,os_version 
         ,t.country_code
         ,t.country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
from  (
         select *
                ,row_number() over (partition by user_key order by date desc) as row
         from processed.fact_dau_snapshot
         where date >= (select start_date from bv.processed.tmp_start_date)
      )t
where t.row = 1;
        
--------create temp table to get the revenue for each user till now + conversion
create temp table temp_user_payment as
select user_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
from processed.fact_revenue
where user_key in (select distinct user_key from temp_dim_user_latest)
group by 1;

update temp_dim_user_latest
set total_revenue_usd = t.revenue,conversion_ts = t.conversion_ts
from temp_user_payment t
where temp_dim_user_latest.user_key = t.user_key;


------update install source------------------------------
update temp_dim_user_latest set install_source = f.install_source, subpublisher_source =  f.subpublisher_source, campaign_source = f.campaign_source
from processed.fact_user_install_source f
where temp_dim_user_latest.user_key = f.user_key
;

------create temp table temp_user_login
create temp table temp_user_login as
select user_key
       ,max(ts_start) as login_ts
from processed.fact_session
where user_key in (select distinct user_key from temp_dim_user_latest)
group by 1;

update temp_dim_user_latest
set login_ts = t.login_ts
from temp_user_login t
where temp_dim_user_latest.user_key = t.user_key;

-----delete old user status in dim_user
delete from processed.dim_user
where user_key in
(
 select user_key from temp_dim_user_latest
);

-----insert the new status of users
insert into processed.dim_user
select * from temp_dim_user_latest;

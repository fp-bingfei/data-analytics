--------------------------------------------------------------------------------------------------------------------------------------------
--BV session and payment
--Version 1.2
--Author yanyu
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading 
delete from  processed.fact_session
where  date_start >= (
                     select start_date 
                     from   bv.processed.tmp_start_date
                );
                    
insert into processed.fact_session
(
        date_start
       ,user_key 
       ,uid 
       ,snsid
       ,install_ts
       ,install_date
	   ,install_source
       ,app 
       ,app_version 
       ,session_id 
       ,ts_start 
       ,ts_end 
       ,level_start 
       ,level_end 
       ,os 
       ,os_version 
       ,country_code 
       ,ip 
       ,language 
       ,device   
       ,browser 
       ,browser_version 
       ,rc_bal_start 
       ,rc_bal_end 
       ,coin_bal_start 
       ,coin_bal_end 
       ,ab_test 
       ,ab_variant 
       ,session_length_sec

)
select  trunc(ts) as date_start
       ,MD5(app||uid) as user_key 
       ,uid 
       ,snsid
       ,case
            when install_ts is null and bi_version!='1.2' then CAST(json_extract_path_text (properties,'addtime') AS timestamp)
            else install_ts
        end as install_ts
       ,trunc(
        case
            when install_ts is null and bi_version!='1.2' then CAST(json_extract_path_text (properties,'addtime') AS timestamp)
            else install_ts
        end
        ) AS install_date
	   ,install_source
       ,app 
       ,null as app_version 
       ,null as session_id 
       ,ts as ts_start 
       ,ts as ts_end 
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_start 
       ,null as level_end 
       ,case when lower(right(os, 3)) = 'ios' then 'iOS'
             when lower(right(os, 7)) = 'android' then 'Android'
             when os='' and substring(json_extract_path_text(properties,'device'), 1, 2) = 'iP' then 'iOS'
             else 'Unknown'
         end as os
       ,case when os_version='' or os_version is null then 'Unknown' end as os_version
       ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
       ,case when ip='' or ip is null then 'Unknown' else ip end as ip
       ,case when json_extract_path_text(properties,'lang')='' then 'Unknown' else coalesce(json_extract_path_text(properties,'lang'),'Unknown') end AS language
       ,case when json_extract_path_text(properties,'device')='' then 'Unknown' else coalesce(json_extract_path_text(properties,'device'),'Unknown') end AS device
       ,'Unknown' as browser
       ,'Unknown' as browser_version
       ,null as rc_bal_start 
       ,null as rc_bal_end 
       ,null as coin_bal_start 
       ,null as coin_bal_end 
       ,'None' as ab_test
       ,'None' as ab_variant
       ,null as session_length_sec
from   public.events_raw 
where   trunc(ts) >= (select start_date from   bv.processed.tmp_start_date) and
        ((bi_version='1.2' and event='session_start') or ( bi_version='1.1' and event in ('login', 'newuser', 'session_start')));

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

-- Insert data < 2014-09-01


-- insert into processed.fact_revenue
-- (
--          date
--         ,user_key
--         ,uid
--         ,snsid
--         ,app
--         ,ts
--         ,level
--         ,os
--         ,country_code
--         ,payment_processor
--         ,product_id
--         ,product_name
--         ,product_type
--         ,coins_in
--         ,rc_in
--         ,currency
--         ,amount
--         ,usd
--         ,transaction_id
--         ,properties
-- )
-- select  trunc(ts) as date
--         ,MD5(e.app||u.uid)
--         ,cast (u.uid as integer) as uid
--         ,e.uid as snsid
--         ,e.app
--         ,e.ts
--         ,CAST(
--                CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0'
--                     ELSE json_extract_path_text(properties,'level')
--                END AS integer
--              ) AS level
--         ,case when lower(right(os, 3)) = 'ios' then 'iOS'
--              when lower(right(os, 7)) = 'android' then 'Android'
--              when os='' and substring(json_extract_path_text(properties,'device'), 1, 2) = 'iP' then 'iOS'
--              else 'Unknown'
--          end as os
--         ,case when u.country_code='' or u.country_code is null or u.country_code=' ' then '--' else u.country_code end as country_code
--         ,'' AS payment_processor
--         ,json_extract_path_text(properties,'product_id') AS product_id
--         ,'' AS product_name
--         ,'' AS product_type
--         ,0 AS coins_in
--         ,0 AS rc_in
--         ,'' AS currency
--         ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS amount
--         ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS usd
--         ,null AS transaction_id
--         ,properties
-- from    public.events e
-- join    public.app_user u on u.snsid=e.uid
-- where   trunc(ts)<'2014-09-01' and name = 'payment';

--delete the historical data in case repeat loading 
delete from  processed.fact_revenue
where  date >= (
                     select start_date 
                     from   bv.processed.tmp_start_date
                );

insert into processed.fact_revenue
(
         date              
        ,user_key          
        ,uid               
        ,snsid             
        ,app               
        ,ts                
        ,level
        ,os
        ,country_code
        ,payment_processor
        ,product_id        
        ,product_name      
        ,product_type
        ,coins_in          
        ,rc_in             
        ,currency          
        ,amount
        ,usd
        ,transaction_id           
        ,properties       
)
select  trunc(ts) as date              
        ,MD5(app||uid)          
        ,uid               
        ,snsid             
        ,app               
        ,ts                
        ,CAST(
               CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' 
                    ELSE json_extract_path_text(properties,'level') 
               END AS integer
             ) AS level
        ,case when lower(right(os, 3)) = 'ios' then 'iOS'
             when lower(right(os, 7)) = 'android' then 'Android'
             when os='' and substring(json_extract_path_text(properties,'device'), 1, 2) = 'iP' then 'iOS'
             else 'Unknown'
         end as os
        ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
        ,json_extract_path_text(properties,'payment_processor') AS payment_processor
        ,case when bi_version='1.2' then json_extract_path_text(properties,'iap_product_id') else json_extract_path_text(properties,'product_id') end AS product_id
        ,case when bi_version='1.2' then json_extract_path_text(properties,'iap_product_name') else json_extract_path_text(properties,'product_name') end AS product_name
        ,case when bi_version='1.2' then json_extract_path_text(properties,'iap_product_type') else json_extract_path_text(properties,'product_type') end AS product_type
        ,case when bi_version='1.2' then null else CAST(json_extract_path_text(properties,'coins_in') AS INTEGER) end AS coins_in
        ,case when bi_version='1.2' then null else CAST(json_extract_path_text(properties,'rc_in') AS INTEGER) end AS rc_in
        ,case when json_extract_path_text(properties,'currency')='' then json_extract_path_text(properties,'curreny') else json_extract_path_text(properties,'currency') end AS currency
        ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS amount
        ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS usd
        ,CAST(json_extract_path_text(properties,'transaction_id') AS VARCHAR) AS transaction_id
        ,properties        
from    public.events_raw
where   event = 'payment' 
and     trunc(ts) >= (
                        select start_date 
                        from bv.processed.tmp_start_date
                     );  
----------------------------------------------------------------------------------------------------------------------------------------------
--processed.fact_level_up
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_level_up
where  date_start >= (select start_date from bv.processed.tmp_start_date);

insert into processed.fact_level_up
(
          user_key
          ,uid
          ,snsid
          ,app
          ,level
          ,ts_start
          ,date_start
          ,ts_end
          ,date_end
)
select  MD5(app||uid) as user_key
        ,uid
        ,snsid
        ,app
        ,CASE
          WHEN json_extract_path_text(properties,'to') ~ '^[0-9]+' then cast(json_extract_path_text (properties,'to') AS integer)
          when json_extract_path_text(properties,'from_level') ~ '^[0-9]+' then cast(json_extract_path_text(properties,'from_level') AS integer)+1
          END AS level
        ,ts
        ,trunc(ts) as date
        ,lead(ts) over (partition by MD5(app||uid) order by ts asc) as ts_end
        ,trunc(lead(ts) over (partition by user_key order by ts asc)) as date_end
from bv.public.events_raw
where trunc(ts) >= (select start_date from bv.processed.tmp_start_date) and
      event='level_up' and
      (json_extract_path_text(properties,'to') ~ '^[0-9]+' or json_extract_path_text(properties,'from_level') ~ '^[0-9]+') and
      json_extract_path_text(properties,'from_level')!='0'
union
select  user_key
        ,uid
        ,snsid
        ,app
        ,1 as level
        ,min(ts_start) as ts
        ,min(date_start) as date
        ,null
        ,null
from processed.fact_session
where date_start >= (select start_date from bv.processed.tmp_start_date) and install_date=date_start
group by 1,2,3,4,5;

-- Update end level for rows inserted previously and level 1 fake level up

create temp table tmp_next_level as
select n.user_key,n.level,n.ts_start,n.date_start
from processed.fact_level_up l
join processed.fact_level_up n on n.level=l.level+1 and l.user_key=n.user_key
where l.ts_end is null and n.date_start >= (select start_date from bv.processed.tmp_start_date);

update processed.fact_level_up
set ts_end=tmp_next_level.ts_start, date_end=tmp_next_level.date_start
from tmp_next_level
where fact_level_up.user_key=tmp_next_level.user_key and tmp_next_level.level=fact_level_up.level+1;
-- tutorial report
delete from processed.agg_tutorial
where install_date >=(
                select start_date
                from bv.processed.tmp_start_date
             );

create temp table tmp_tutorial_step as
select MD5(app||uid) as user_key
        ,app
        ,uid
        ,snsid
		,case
		    when bi_version='1.2' then cast(json_extract_path_text(properties,'tutorial_step') as integer)
		    else cast(json_extract_path_text(properties,'step') as integer)
		 end as step
        ,min(ts) as min_ts
from events_raw
where event = 'tutorial' and
      trunc(ts) >=
            (
                select start_date
                from bv.processed.tmp_start_date
            )
group by 1,2,3,4,5;

insert into processed.agg_tutorial
(
    install_date
    ,app
    ,os
    ,os_version
    ,country
    ,install_source
    ,device
    ,browser
    ,browser_version
    ,language
    ,step
    ,user_cnt
)
select
     u.install_date
    ,u.app
    ,u.os
    ,u.os_version
    ,u.country
    ,u.install_source
    ,u.device
    ,u.browser
    ,u.browser_version
    ,u.language
    ,t.step
    ,count(distinct u.user_key)
from  tmp_tutorial_step t
join processed.dim_user_install u on t.user_key = u.user_key
where u.install_date >=
            (
                select start_date
                from bv.processed.tmp_start_date
            )
group by 1,2,3,4,5,6,7,8,9,10,11
union
select
     u.install_date
    ,u.app
    ,u.os
    ,u.os_version
    ,u.country
    ,u.install_source
    ,u.device
    ,u.browser
    ,u.browser_version
    ,u.language
    ,0 as step
    ,count(distinct u.user_key)
from  processed.dim_user_install u
where u.install_date >=
            (
                select start_date
                from bv.processed.tmp_start_date
            )
group by 1,2,3,4,5,6,7,8,9,10,11;













--------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------
--BV fact dau snapshot
--Version 1.2
--Author yanyu
/**
Discription:
This script is generate kpi,rentention and LTV tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.agg_kpi
---------------------------------------------------------------------------------------------------------------------------------------------- 

------delete the historical data in case repeat loading 
delete from processed.agg_kpi
where date >=(
                        select start_date 
                        from bv.processed.tmp_start_date
                     ); 

insert into processed.agg_kpi
(
      date 
      ,app 
      ,app_version 
      ,install_date
      ,install_source 
      ,subpublisher_source 
      ,campaign_source
      ,level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,is_new_user
      ,is_payer
      ,is_converted_today
      ,metric_name
      ,metric_value
      ,metric_freq
)
select  date
      ,d.app
      ,d.app_version
      ,d.install_date
      ,u.install_source
      ,u.subpublisher_source
      ,u.campaign_source
      ,d.level_end
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.country_code
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_test
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      , d.is_converted_today  
      ,'DAU' as metric_name
      ,count(d.user_key) as metric_value
      ,0 as metric_freq
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from bv.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25
union all
select  date
      ,d.app
      ,d.app_version
      ,d.install_date
      ,u.install_source
      ,u.subpublisher_source
      ,u.campaign_source
      ,d.level_end
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.country_code
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_test
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      , d.is_converted_today  
      ,'Revenue' as metric_name
      ,sum(revenue_usd) as metric_value
      ,0 as metric_freq
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from bv.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25
union all
select  date
      ,d.app
      ,d.app_version
      ,d.install_date
      ,u.install_source
      ,u.subpublisher_source
      ,u.campaign_source
      ,d.level_end
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.country_code
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_test
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      ,d.is_converted_today
      ,'Session' as metric_name
      ,0 as metric_value
      ,sum(session_cnt) as metric_freq
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from bv.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24
union all
select  date
      ,d.app
      ,d.app_version
      ,d.install_date
      ,u.install_source
      ,u.subpublisher_source
      ,u.campaign_source
      ,d.level_end
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.country_code
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_test
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      , d.is_converted_today  
      ,'New Installs' as metric_name
      ,sum(is_new_user) as metric_value
      ,0 as metric_freq
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from bv.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25
union all
select  date
      ,d.app
      ,d.app_version
      ,d.install_date
      ,u.install_source
      ,u.subpublisher_source
      ,u.campaign_source
      ,d.level_end
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.country_code
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_test
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      , d.is_converted_today  
      ,'New Payers' as metric_name
      ,sum(is_converted_today) as metric_value
      ,0 as metric_freq
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from bv.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25
union all
select  date
      ,d.app
      ,d.app_version
      ,d.install_date
      ,u.install_source
      ,u.subpublisher_source
      ,u.campaign_source
      ,d.level_end
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.country_code
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_test
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      , d.is_converted_today  
      ,'Payers' as metric_name
      ,sum(case when revenue_usd>0 then 1 else 0 end) as metric_value
      ,0 as metric_freq
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from bv.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25;


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.agg_retention
----------------------------------------------------------------------------------------------------------------------------------------------

--create temp table for player days

create temp table player_day as
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 15 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day;


--create temp table for new users in each install_date
create temp table tmp_new_users as
select  u.install_date
       ,u.os
       ,u.country
       ,u.app
       ,u.install_source
       ,u.subpublisher_source
       ,u.campaign_source
       ,l.is_payer
       ,u.ab_test
       ,u.ab_variant
       ,count(u.user_key) as new_user_cnt
from processed.dim_user_install u
join processed.dim_user l on l.user_key=u.user_key
where u.install_date>'2014-09-01'
group by 1,2,3,4,5,6,7,8,9,10;

--create temp table to get the last date in dau table.
create temporary table last_date
as
select max(date) as date
from processed.fact_dau_snapshot;

-- player cubes

create temp table player_day_cube as
select nu.*, rd.day as player_day
from tmp_new_users nu
join player_day rd on 1 = 1
join last_date d on 1=1
where DATEDIFF('day', nu.install_date, d.date) >= rd.day;

--create agg_retention table

drop table processed.agg_retention;
create table processed.agg_retention as
--create cube to calculate the retention
with retention_base as (
 SELECT   DATEDIFF('day', u.install_date, du.date) AS player_day
         ,u.install_date
         ,u.os
         ,u.app
         ,u.country
         ,u.install_source
         ,u.subpublisher_source
         ,u.campaign_source
         ,l.is_payer
         ,u.ab_test
         ,u.ab_variant
         ,count(distinct du.user_key) as retained_user_cnt
 FROM    processed.fact_dau_snapshot du
 join    processed.dim_user_install u on du.user_key = u.user_key
 join    processed.dim_user l on l.user_key=u.user_key
 where u.install_date>'2014-09-01' and DATEDIFF('day', u.install_date, du.date) in (select day from player_day)
 group by 1,2,3,4,5,6,7,8,9,10,11
)
select     cube.install_date
          ,cube.player_day
          ,cube.os
          ,cube.country
          ,cube.app
          ,cube.install_source
          ,cube.subpublisher_source
          ,cube.campaign_source
          ,cube.is_payer
          ,cube.ab_test
          ,cube.ab_variant
          ,cube.new_user_cnt
          ,coalesce(retained_user_cnt,0) as retained_user_cnt
from      player_day_cube cube
left join retention_base r
on  cube.player_day = r.player_day
and cube.install_date = r.install_date
and cube.app = r.app
and cube.os = r.os 
and cube.country = r.country 
and cube.install_source = r.install_source
and cube.subpublisher_source= r.subpublisher_source
and cube.campaign_source = r.campaign_source
and cube.is_payer = r.is_payer
and cube.ab_test = r.ab_test
and cube.ab_variant = r.ab_variant;

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.agg_ltv
----------------------------------------------------------------------------------------------------------------------------------------------

--create ltv table

drop table processed.agg_ltv;
create table processed.agg_ltv as
with ltv_base as (
SELECT    pd.day as player_day
          ,u.install_date
          ,u.os
          ,u.app
          ,u.country
          ,u.install_source
          ,u.subpublisher_source
          ,u.campaign_source
          ,1 as is_payer
          ,u.ab_test
          ,u.ab_variant
          ,sum(r.usd) as revenue_usd
FROM      processed.fact_revenue r
join      processed.dim_user_install u on r.user_key = u.user_key
join      last_date ld on 1=1
join      player_day pd on  DATEDIFF('day', u.install_date, r.date)<=pd.day
where     u.install_date>'2014-09-01' and DATEDIFF('day', u.install_date, ld.date) >= pd.day
group by 1,2,3,4,5,6,7,8,9,10,11
)
select     cube.install_date
          ,cube.player_day
          ,cube.os
          ,cube.country
          ,cube.app
          ,cube.install_source
          ,cube.subpublisher_source
          ,cube.campaign_source
          ,cube.is_payer
          ,cube.ab_test
          ,cube.ab_variant
          ,cube.new_user_cnt
          ,coalesce(revenue_usd,0) as revenue_usd
from      player_day_cube cube
left join ltv_base l
on  cube.player_day = l.player_day
and cube.install_date = l.install_date
and cube.app = l.app
and cube.os = l.os
and cube.country = l.country
and cube.install_source = l.install_source
and cube.subpublisher_source= l.subpublisher_source
and cube.campaign_source = l.campaign_source
and cube.is_payer = l.is_payer
and cube.ab_test = l.ab_test
and cube.ab_variant = l.ab_variant;

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.tab_iap (iap report)
----------------------------------------------------------------------------------------------------------------------------------------------

drop table processed.tab_iap;
create table processed.tab_iap as
SELECT  r.date
        ,r.level
        ,u.install_date as install_date
        ,u.install_source as install_source
        ,s.country
        ,r.os
        ,s.ab_test
        ,s.ab_variant
        ,case when u.conversion_ts=r.ts then 1 else 0 end as conversion_purchase
		    ,product_id
		    ,product_type
		    ,sum(usd) as revenue_usd
		    ,count(1) as purchase_cnt
FROM processed.fact_revenue r
join processed.fact_dau_snapshot s on r.user_key=s.user_key and r.date=s.date
join processed.dim_user u on u.user_key=r.user_key
group by 1,2,3,4,5,6,7,8,9,10,11;



truncate processed.tab_marketing_kpi;

create temporary table last_date as
select max(date) as date
from processed.fact_dau_snapshot;

create temp table tmp_install_users_d3_payers as
select user_key, max(is_payer) as d3_payer
from processed.fact_dau_snapshot
where (install_date <= (select date from last_date)-4) and datediff(day,install_date,date)<=3
group by user_key;

create temp table tmp_install_users_d7_revenue as
select user_key, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where install_date <= ((select date from last_date)-8) and datediff(day,install_date,date)<=7
group by user_key;

create temp table tmp_install_users_d30_revenue as
select user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
where (install_date <= (select date from last_date)-30) and datediff(day,install_date,date)<=30
group by user_key;

create temp table tmp_install_users_d90_revenue as
select user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where (install_date <= (select date from last_date)-91) and datediff(day,install_date,date)<=90
group by user_key;

create temp table tmp_install_users_d1_retained_users as
select user_key,  1 as d1_retained_user_cnt
from processed.fact_dau_snapshot
where date - install_date = 1
group by user_key;

create temp table tmp_install_users_d7_retained_users as
select user_key,  1 as d7_retained_user_cnt
from processed.fact_dau_snapshot
where date - install_date = 7
group by user_key;

create temp table tmp_install_users_d15_retained_users as
select user_key,  1 as d15_retained_user_cnt
from processed.fact_dau_snapshot
where date - install_date = 15
group by user_key;

create temp table tmp_install_users_d30_retained_users as
select user_key,  1 as d30_retained_user_cnt
from processed.fact_dau_snapshot
where date - install_date = 30
group by user_key;

insert into processed.tab_marketing_kpi
(app, install_date, install_source, campaign_source, subpublisher_source, country, os, new_user_cnt,d1_new_user_cnt,d3_new_user_cnt,d7_new_user_cnt,d15_new_user_cnt,d30_new_user_cnt,d90_new_user_cnt, revenue_usd, d7_revenue_usd, d30_revenue_usd, d90_revenue_usd,
payer_cnt, d3_payer_cnt, d1_retained_user_cnt, d7_retained_user_cnt, d15_retained_user_cnt, d30_retained_user_cnt)
select
    u.app,
    u.install_date,
    u.install_source,
    u.campaign_source,
    u.subpublisher_source,
    u.country,
    u.os,
    count(1) as new_user_cnt,
    sum (case when u.install_date <= ld.date-2 then 1 else 0 end) as d1_new_user_cnt,
    sum (case when u.install_date <= ld.date-4 then 1 else 0 end) as d3_new_user_cnt,
    sum (case when u.install_date <= ld.date-8 then 1 else 0 end) as d7_new_user_cnt,
    sum (case when u.install_date <= ld.date-16 then 1 else 0 end) as d15_new_user_cnt,
    sum (case when u.install_date <= ld.date-31 then 1 else 0 end) as d30_new_user_cnt,
    sum (case when u.install_date <= ld.date-91 then 1 else 0 end) as d90_new_user_cnt,
    sum(coalesce(l.total_revenue_usd,0)) as revenue_usd,
    sum(coalesce(d7_revenue.revenue,0)) as d7_revenue_usd,
    sum(coalesce(d30_revenue.revenue,0))  as d30_revenue_usd,
    sum(coalesce(d90_revenue.revenue,0))  as d90_revenue_usd,
    sum(l.is_payer) as payer_cnt,
    sum(coalesce(d3p.d3_payer,0)) as d3_payer_cnt,
    sum(coalesce(d1_retained_users.d1_retained_user_cnt,0)) as d1_retained_user_cnt,
    sum(coalesce(d7_retained_users.d7_retained_user_cnt,0)) as d7_retained_user_cnt,
    sum(coalesce(d15_retained_users.d15_retained_user_cnt,0)) as d15_retained_user_cnt,
    sum(coalesce(d30_retained_users.d30_retained_user_cnt,0)) as d30_retained_user_cnt
from processed.dim_user_install u
join processed.dim_user l on l.user_key=u.user_key
join (select date from last_date) ld on 1=1
    left join tmp_install_users_d3_payers d3p on u.user_key = d3p.user_key
    left join tmp_install_users_d7_revenue d7_revenue on u.user_key = d7_revenue.user_key
    left join tmp_install_users_d30_revenue d30_revenue on u.user_key = d30_revenue.user_key
    left join tmp_install_users_d90_revenue d90_revenue on u.user_key = d90_revenue.user_key
    left join tmp_install_users_d1_retained_users d1_retained_users on u.user_key = d1_retained_users.user_key
    left join tmp_install_users_d7_retained_users d7_retained_users on u.user_key = d7_retained_users.user_key
    left join tmp_install_users_d15_retained_users d15_retained_users on u.user_key = d15_retained_users.user_key
    left join tmp_install_users_d30_retained_users d30_retained_users on u.user_key = d30_retained_users.user_key
where u.install_date <= (select date from last_date)-1
group by 1,2,3,4,5,6,7;



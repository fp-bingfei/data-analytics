--------------------------------------------------------------------------------------------------------------------------------------------
--BV Economy reports
--Version 1.2
--Author Jeff
/**
Description:
This script is generate the economy reports
**/
---------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.fact_ledger
----------------------------------------------------------------------------------------------------------------------------------------------
--delete the historical data in case repeat loading 
delete from processed.fact_ledger where  date>= (select start_date from processed.tmp_start_date) ;

-- insert rc transaction

INSERT INTO processed.fact_ledger
select
  trunc(ts),
  ts,
  MD5(app||uid) as user_key,
  app,
  cast(uid as integer),
  snsid,
  cast(json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') as integer) AS level,
  json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action'),
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') END AS integer) >0 then 'Rc' else coalesce(item_name,json_extract_path_text(properties,'action_detail')) end as in_name,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') END AS integer) >0 then 'Resource' else coalesce(i.item_type,'Unknown') end as in_type,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') END AS integer) >0 then CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') END AS integer)  else 1 end as in_amount,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') END AS integer) >0 then 'Rc' else coalesce(item_name,json_extract_path_text(properties,'action_detail')) end as out_name,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') END AS integer) >0 then 'Resource' else coalesce(i.item_type,'Unknown') end as out_type,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') END AS integer) >0 then CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') END AS integer)  else 1 end as out_amount,
  ''
from public.events_raw t
left join processed.dim_item i on i.item_id=json_extract_path_text(properties,'action_detail')
where date(t.ts) >=(select start_date from processed.tmp_start_date) and
      event='rc_transaction' AND length(json_extract_path_text(properties,'action_detail'))<=2000 and
      json_extract_path_text(regexp_replace(properties, '\\\\.', ''), 'level') ~ '^[0-9]+';

--insert missing rc from iap

INSERT INTO processed.fact_ledger
select
  date,
  ts,
  MD5(app||uid) as user_key,
  app,
  cast(uid as integer),
  snsid,
  level,
  'IAP',
  initcap(p.type) as in_name,
  'Resource' as in_type,
  coalesce(qty,0) as in_amount,
  r.product_id as out_name,
  'IAP' as out_type,
  1  as out_amount,
  ''
from processed.fact_revenue r
left join processed.dim_product p on r.product_id =p.product_id
where date(r.ts) >=(select start_date from processed.tmp_start_date) and qty>0;

-- insert coins transaction

INSERT INTO processed.fact_ledger
select
  trunc(ts),
  ts,
  MD5(app||uid) as user_key,
  app,
  cast(uid as integer),
  snsid,
  cast(json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') as integer)  as level,
  json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action'),
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') END AS integer)>0 then 'Coins' else coalesce(item_name,SUBSTRING(json_extract_path_text(properties,'action_detail'),0,32)) end as in_name,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') END AS integer)>0 then 'Resource' else coalesce(i.item_type,'Unknown') end as in_type,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') END AS integer)>0 then CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') END AS integer) else 1 end as in_amount,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') END AS integer)>0 then 'Coins' else coalesce(item_name,SUBSTRING(json_extract_path_text(properties,'action_detail'),0,32)) end as out_name,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') END AS integer)>0 then 'Resource' else coalesce(i.item_type,'Unknown') end as out_type,
  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') END AS integer)>0 then CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') END AS integer) else 1 end as out_amount,
  ''
from public.events_raw t
left join processed.dim_item i on i.item_id=json_extract_path_text(properties,'action_detail')
where date(t.ts) >=(select start_date from processed.tmp_start_date) and event='coins_transaction' and
      length(json_extract_path_text(properties,'action_detail'))<=2000 and
      json_extract_path_text(regexp_replace(properties, '\\\\.', ''), 'level') ~ '^[0-9]+';

-- Explode collections for 1.2 events
DROP VIEW IF EXISTS seq_0_to_10;
CREATE VIEW  seq_0_to_10 AS (
    SELECT 0 AS i UNION ALL
    SELECT 1 UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 8 UNION ALL
    SELECT 9 UNION ALL
    SELECT 10
);

-- Temporary tables for exploding collections

CREATE TEMP TABLE resources_received AS (
    SELECT id,
            md5(app || uid) as user_key, uid, snsid,app,
           json_extract_path_text(properties, 'level') AS level,
           event,
           ts,
           case event
            when 'transaction' then json_extract_path_text(properties,'transaction_type')
            when 'timer' then json_extract_path_text(properties,'timer_type')
            when 'payment' then 'IAP'
            when 'item_actioned' then json_extract_path_text(properties,'action_type')
            when 'mission' then json_extract_path_text(properties,'mission_type')
            when 'gift_received' then 'gift'
            when 'level_up' then 'level_up'
            when 'achievement' then 'achievement'
           end as transaction_type,
           json_extract_path_text(collections, 'resources_received') as resources_received,
           case
            when event='timer' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            when event='achievement' then json_extract_path_text(properties,'achievement_id')
            when event='payment' then json_extract_path_text(properties,'iap_product_id')
            when event='item_actioned' and json_extract_path_text(collections,'items_target')!='' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            else 'N/A'
           end as detail
    FROM events_raw
    WHERE bi_version='1.2' and len(json_extract_path_text(collections, 'resources_received'))>0 and trunc(ts)>= (select start_date from bv.processed.tmp_start_date)
);

CREATE TEMP TABLE resources_spent AS (
    SELECT  id,
            md5(app || uid) as user_key, uid, snsid,app,
           json_extract_path_text(properties, 'level') AS level,
           event,
           ts,
           case event
            when 'transaction' then json_extract_path_text(properties,'transaction_type')
            when 'timer' then json_extract_path_text(properties,'timer_type')
            when 'payment' then 'IAP'
            when 'item_actioned' then json_extract_path_text(properties,'action_type')
            when 'mission' then json_extract_path_text(properties,'mission_type')
            when 'gift_received' then 'gift'
            when 'level_up' then 'level_up'
            when 'achievement' then 'achievement'
           end as transaction_type,
           json_extract_path_text(collections, 'resources_spent') as resources_spent,
          case
            when event='timer' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            when event='achievement' then json_extract_path_text(properties,'achievement_id')
            when event='payment' then json_extract_path_text(properties,'iap_product_id')
            when event='item_actioned' and json_extract_path_text(collections,'items_target')!='' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            else 'N/A'
           end as detail
    FROM events_raw
    WHERE bi_version='1.2' and len(json_extract_path_text(collections, 'resources_spent')) > 0 and trunc(ts)>= (select start_date from bv.processed.tmp_start_date)
);

CREATE TEMP TABLE items_received AS (
    SELECT  id,
            md5(app || uid) as user_key, uid, snsid,app,
           json_extract_path_text(properties, 'level') AS level,
           event,
           ts,
           case event
            when 'transaction' then json_extract_path_text(properties,'transaction_type')
            when 'timer' then json_extract_path_text(properties,'timer_type')
            when 'payment' then 'IAP'
            when 'item_actioned' then json_extract_path_text(properties,'action_type')
            when 'mission' then json_extract_path_text(properties,'mission_type')
            when 'gift_received' then 'gift'
            when 'level_up' then 'level_up'
            when 'achievement' then 'achievement'
           end as transaction_type,
           json_extract_path_text(collections, 'items_received') as items_received,
          case
            when event='timer' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            when event='achievement' then json_extract_path_text(properties,'achievement_id')
            when event='payment' then json_extract_path_text(properties,'iap_product_id')
            when event='item_actioned' and json_extract_path_text(collections,'items_target')!='' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            else 'N/A'
           end as detail
    FROM events_raw
    WHERE bi_version='1.2' and len(json_extract_path_text(collections, 'items_received')) > 0 and trunc(ts)>= (select start_date from bv.processed.tmp_start_date)
);

CREATE TEMP TABLE items_spent AS (
    SELECT id,
           md5(app || uid) as user_key, uid, snsid,app,
           json_extract_path_text(properties, 'level') AS level,
           event,
           ts,
           case event
            when 'transaction' then json_extract_path_text(properties,'transaction_type')
            when 'timer' then json_extract_path_text(properties,'timer_type')
            when 'payment' then 'IAP'
            when 'item_actioned' then json_extract_path_text(properties,'action_type')
            when 'mission' then json_extract_path_text(properties,'mission_type')
            when 'gift_received' then 'gift'
            when 'level_up' then 'level_up'
            when 'achievement' then 'achievement'
           end as transaction_type,
           json_extract_path_text(collections, 'items_spent') as items_spent,
          case
            when event='timer' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            when event='achievement' then json_extract_path_text(properties,'achievement_id')
            when event='payment' then json_extract_path_text(properties,'iap_product_id')
            when event='item_actioned' and json_extract_path_text(collections,'items_target')!='' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections,'items_target'),0),'item_id')
            else 'N/A'
           end as detail
    FROM events_raw
    WHERE bi_version='1.2' and len(json_extract_path_text(collections, 'items_spent')) > 0 and trunc(ts)>= (select start_date from bv.processed.tmp_start_date)
);

drop table if exists economy;
CREATE temp TABLE economy AS (
    SELECT id,user_key,uid,snsid, level,app,  event, ts, transaction_type,
    json_extract_path_text(json_extract_array_element_text(resources_received, seq.i), 'resource_id') AS in_id,
    sum(cast(json_extract_path_text(json_extract_array_element_text(resources_received, seq.i), 'resource_amount') as integer)) AS in_amount,
    NULL as out_id,
    NULL as out_amount,
    detail
    FROM resources_received, seq_0_to_10 AS seq
    WHERE seq.i < JSON_ARRAY_LENGTH(resources_received)
    group by 1,2,3,4,5,6,7,8,9,in_id,out_id,detail
    UNION ALL
    SELECT id,user_key,uid,snsid, level,app,  event, ts, transaction_type,
    NULL AS in_id,
    NULL AS in_amount,
    json_extract_path_text(json_extract_array_element_text(resources_spent, seq.i), 'resource_id') as out_id,
    sum(cast(json_extract_path_text(json_extract_array_element_text(resources_spent, seq.i), 'resource_amount') as integer)) AS out_amount,
    detail
    FROM resources_spent, seq_0_to_10 AS seq
    WHERE seq.i < JSON_ARRAY_LENGTH(resources_spent)
    group by 1,2,3,4,5,6,7,8,9,in_id,out_id,detail
    UNION ALL
    SELECT id,user_key,uid,snsid, level,app,  event, ts, transaction_type,
    json_extract_path_text(json_extract_array_element_text(items_received, seq.i), 'item_id') AS in_id,
    sum(cast(json_extract_path_text(json_extract_array_element_text(items_received, seq.i), 'item_amount') as integer)) AS in_amount,
    NULL as out_id,
    NULL as out_amount,
    detail
    FROM items_received, seq_0_to_10 AS seq
    WHERE seq.i < JSON_ARRAY_LENGTH(items_received)
    group by 1,2,3,4,5,6,7,8,9,in_id,out_id,detail
    UNION ALL
    SELECT id,user_key,uid,snsid, level,app,  event, ts, transaction_type,
    NULL AS in_id,
    NULL AS in_amount,
    json_extract_path_text(json_extract_array_element_text(items_spent, seq.i), 'item_id') as out_id,
    sum(cast(json_extract_path_text(json_extract_array_element_text(items_spent, seq.i), 'item_amount') as integer)) AS out_amount,
    detail
    FROM items_spent, seq_0_to_10 AS seq
    WHERE seq.i < JSON_ARRAY_LENGTH(items_spent)
   group by 1,2,3,4,5,6,7,8,9,in_id,out_id,detail
);

drop table if exists in_out_count;
create temp table in_out_count as
select
    id,
    sum(case when in_id is not null then 1 else 0 end) as ins,
    sum(case when out_id is not null then 1 else 0 end) as outs,
    max(out_id) as out_id,max(out_amount) as out_amount,
    max(in_id) as in_id,max(in_amount) as in_amount
from economy
group by 1;

update economy
set out_id=t.out_id, in_id=t.in_id, in_amount=t.in_amount, out_amount=t.out_amount
from in_out_count t
where economy.id=t.id  and outs=1 and ins=1;

update economy
set detail=t.out_id
from in_out_count t
where  detail='N/A' and event not in ('timer','achievement','mission','payment') and economy.id=t.id and outs=1 and ins>1 and economy.in_id is null;

update economy
set detail=t.in_id
from in_out_count t
where detail='N/A' and event not in ('timer','achievement','mission','payment') and economy.id=t.id and outs>1 and ins=1 and economy.out_id is null;

drop table if exists processed.economy_1_2;
create table processed.economy_1_2 as
select distinct * from economy;

-- insert 1.2 events

INSERT INTO processed.fact_ledger
select
    trunc(ts),
    ts,
    user_key,
    app,
    cast(uid as integer),
    snsid,
    cast(level as integer),
    transaction_type,
    coalesce(i.item_name,in_id) as in_name,
    coalesce(i.item_type,'Unknown') as in_type,
    coalesce(in_amount,0),
    coalesce(o.item_name,out_id) as out_name,
    coalesce(o.item_type,'Unknown') as out_type,
    coalesce(out_amount,0),
    coalesce(d.item_name,detail)
from processed.economy_1_2 t
left join processed.dim_item i on i.item_id=t.in_id
left join processed.dim_item o on o.item_id=t.out_id
left join processed.dim_item d on d.item_id=t.detail
where  date(t.ts) >= (select start_date from processed.tmp_start_date) and (in_id in ('998','997') or out_id in ('998','997'));

-- DAU cube

drop table if exists temp_ledger_cube_level_bin;
create temp table temp_ledger_cube_level_bin (
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  detail varchar(180) default null
)DISTKEY(date)
  SORTKEY(date,level_bin);

insert into temp_ledger_cube_level_bin
select distinct date,ceiling((level / 5::real))*5 as level_bin,transaction_type,in_name,in_type,out_name,out_type,detail
from processed.fact_ledger
where  date >= (select start_date from processed.tmp_start_date);

drop table if exists temp_ledger_cube_all;
create temp table temp_ledger_cube_all (
  date DATE ENCODE DELTA,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  detail varchar(180) default null
)DISTKEY(date)
  SORTKEY(date);

insert into temp_ledger_cube_all
select distinct date,transaction_type,in_name,in_type,out_name,out_type,detail
from temp_ledger_cube_level_bin;

drop table if exists tmp_agg_dau_level_bin;
create  table tmp_agg_dau_level_bin (
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT null ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT null ENCODE BYTEDICT,
  detail varchar(180) default null,
  user_cnt integer default 0
) DISTKEY(date)
  SORTKEY(date,level_bin);

insert into tmp_agg_dau_level_bin
select
        d.date,
        ceiling((l.level / 5::real))*5 as level_bin,
        cube.transaction_type,
        cube.in_name,
        cube.in_type,
        cube.out_name,
        cube.out_type,
        cube.detail,
        count(distinct d.user_key)
FROM  processed.fact_dau_snapshot d
join (select distinct level from processed.dim_user) l on l.level between d.level_start and d.level_end
join temp_ledger_cube_level_bin cube on cube.date=d.date and cube.level_bin=ceiling((l.level / 5::real))*5
where  d.date >= (select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8
union all
select
        d.date,
        0 as level_bin,
        cube.transaction_type,
        cube.in_name,
        cube.in_type,
        cube.out_name,
        cube.out_type,
        cube.detail,
        count(distinct d.user_key)
FROM  processed.fact_dau_snapshot d
join temp_ledger_cube_all cube on cube.date=d.date
where d.date  >= (select start_date from processed.tmp_start_date)
group by 1,3,4,5,6,7,8;

-- Aggregated daily ledger by binned levels

--delete the historical data in case repeat loading


delete from processed.agg_daily_ledger
where date >=(select start_date from processed.tmp_start_date);

insert into processed.agg_daily_ledger
with agg_ledger as (
SELECT
        l.date,
        ceiling((l.level / 5::real))*5 as level_bin,
        l.transaction_type,
        l.in_name,
        l.in_type,
        l.out_name,
        l.out_type,
        l.detail,
        sum(l.in_amount) as in_amount,
        sum(l.out_amount) as out_amount,
        count(distinct l.user_key) as  purchaser_cnt
FROM  processed.fact_ledger l
where  l.date  >= (select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8
)
SELECT
        d.date,
        d.level_bin,
        d.transaction_type,
        d.in_name,
        d.in_type,
        d.out_name,
        d.out_type,
        d.detail,
        coalesce(l.in_amount,0) as in_amount,
        coalesce(l.out_amount,0) as out_amount,
        coalesce(l.purchaser_cnt,0) as  purchaser_cnt,
        coalesce(d.user_cnt,0) as user_cnt
FROM  tmp_agg_dau_level_bin d
left join  agg_ledger l
on
l.date=d.date and d.level_bin=l.level_bin and
d.transaction_type=l.transaction_type and
coalesce(d.in_name,'')=coalesce(l.in_name,'') and d.in_type=l.in_type and
coalesce(d.out_name,'')=coalesce(l.out_name,'') and d.out_type=l.out_type and
d.detail=l.detail
where d.level_bin!=0
union all
SELECT
        d.date,
        0,
        d.transaction_type,
        d.in_name,
        d.in_type,
        d.out_name,
        d.out_type,
        d.detail,
        coalesce(sum(l.in_amount),0) as in_amount,
        coalesce(sum(l.out_amount),0) as out_amount,
        coalesce(sum(l.purchaser_cnt),0) as  purchaser_cnt,
        coalesce(max(d.user_cnt),0) as user_cnt
FROM  tmp_agg_dau_level_bin d
left join  agg_ledger l
on
l.date=d.date and
d.transaction_type=l.transaction_type and
coalesce(d.in_name,'')=coalesce(l.in_name,'') and d.in_type=l.in_type and
coalesce(d.out_name,'')=coalesce(l.out_name,'') and d.out_type=l.out_type and
d.detail=l.detail
where d.level_bin=0
group by 1,3,4,5,6,7,8;
--
------------------------------------------------------------------------------------------------------------------------------------------------
----processed.fact_item_transaction
------------------------------------------------------------------------------------------------------------------------------------------------
--
----delete the historical data in case repeat loading
--delete from processed.fact_item_transaction where date >= ( select start_date from processed.tmp_start_date);
--
--INSERT INTO processed.fact_item_transaction
--select
--  trunc(ts),
--  ts,
--  MD5(app||uid) as user_key,
--  app,
--  cast(uid as integer),
--  snsid,
--  cast(json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') as integer) AS level,
--  json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action'),
--  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') END AS integer) >0 then coalesce(item_name,null) else 'N/A' end as in_name,
--  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') END AS integer) >0 then coalesce(i.item_type,null) else 'N/A' end as in_type,
--  CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') END AS integer) as in_amount,
--  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') END AS integer) >0 then coalesce(item_name,null) else 'N/A' end as out_name,
--  case when CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') END AS integer) >0 then  coalesce(i.item_type,null) else 'N/A' end as out_type,
--  CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') END AS integer) as out_amount
--from public.events_raw t
--left join processed.dim_item i on i.item_id=json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_id')
--where date(t.ts)>'2014-10-15' and date(t.ts) >= (select start_date from processed.tmp_start_date) and
--      event='item_transaction' and
--      json_extract_path_text(regexp_replace(properties, '\\\\.', ''), 'level') ~ '^[0-9]+';
--
--truncate tmp_agg_dau_level_bin;
--
--insert into tmp_agg_dau_level_bin
--select
--        d.date,
--        ceiling((l.level / 5::real))*5 as level_bin,
--        ab_test,
--        ab_variant,
--        cube.transaction_type,
--        cube.in_name,
--        cube.in_type,
--        cube.out_name,
--        cube.out_type,
--        count(distinct d.user_key)
--FROM  processed.fact_dau_snapshot d
--join (select distinct level from processed.dim_user) l on l.level between d.level_start and d.level_end
--join (
--  select distinct ceiling((level / 5::real))*5 as level_bin,date,transaction_type,in_name,in_type,out_name,out_type
--  from processed.fact_item_transaction
--  where date>'2014-10-15' and date>= (select start_date from processed.tmp_start_date)
--  ) cube on cube.level_bin=ceiling((l.level / 5::real))*5 and cube.date=d.date
--where d.date>'2014-10-15' and d.date>= (select start_date from processed.tmp_start_date)
--group by 1,2,3,4,5,6,7,8,9
--union
--select
--        d.date,
--        0 as level_bin,
--        ab_test,
--        ab_variant,
--        cube.transaction_type,
--        cube.in_name,
--        cube.in_type,
--        cube.out_name,
--        cube.out_type,
--        count(distinct d.user_key)
--FROM  processed.fact_dau_snapshot d
--join (
--  select distinct date,transaction_type,in_name,in_type,out_name,out_type
--  from processed.fact_item_transaction
--  where date>= (select start_date from processed.tmp_start_date)
--  ) cube on cube.date=d.date
--where d.date>'2014-10-15' and d.date>= (select start_date from processed.tmp_start_date)
--group by 1,3,4,5,6,7,8,9;
--
---- Aggregated daily item transactions by binned levels
--
----delete the historical data in case repeat loading
--delete from processed.agg_item_transaction where date >= ( select start_date from processed.tmp_start_date);
--
--insert into processed.agg_item_transaction
--with agg_ledger as (
--SELECT
--        l.date,
--        ceiling((l.level / 5::real))*5 as level_bin,
--        s.ab_test,
--        s.ab_variant,
--        l.transaction_type,
--        l.in_name,
--        l.in_type,
--        l.out_name,
--        l.out_type,
--        sum(l.in_amount) as in_amount,
--        sum(l.out_amount) as out_amount,
--        count(distinct l.user_key) as  purchaser_cnt
--FROM  processed.fact_item_transaction l
--join  processed.fact_dau_snapshot s on l.user_key=s.user_key and s.date=l.date
--where l.date>'2014-10-15' and l.date>= (select start_date from processed.tmp_start_date)
--group by 1,2,3,4,5,6,7,8,9
--)
--SELECT
--        d.date,
--        d.level_bin,
--        d.ab_test,
--        d.ab_variant,
--        d.transaction_type,
--        d.in_name,
--        d.in_type,
--        d.out_name,
--        d.out_type,
--        coalesce(l.in_amount,0) as in_amount,
--        coalesce(l.out_amount,0) as out_amount,
--        coalesce(purchaser_cnt,0) as  purchaser_cnt,
--        coalesce(d.user_cnt,0) as user_cnt
--FROM  tmp_agg_dau_level_bin d
--left join  agg_ledger l
--on
--l.date=d.date and d.level_bin=l.level_bin and
--d.transaction_type=l.transaction_type and
--d.in_name=l.in_name and d.in_type=l.in_type and
--d.out_name=l.out_name and d.out_type=l.out_type and
--d.ab_test=l.ab_test and d.ab_variant=l.ab_variant
--where d.level_bin!=0
--union
--SELECT
--        d.date,
--        0,
--        d.ab_test,
--        d.ab_variant,
--        d.transaction_type,
--        d.in_name,
--        d.in_type,
--        d.out_name,
--        d.out_type,
--        coalesce(sum(l.in_amount),0) as in_amount,
--        coalesce(sum(l.out_amount),0) as out_amount,
--        coalesce(sum(purchaser_cnt),0) as  purchaser_cnt,
--        coalesce(max(d.user_cnt),0) as user_cnt
--FROM  tmp_agg_dau_level_bin d
--left join  agg_ledger l
--on
--l.date=d.date and
--d.transaction_type=l.transaction_type and
--d.in_name=l.in_name and d.in_type=l.in_type and
--d.out_name=l.out_name and d.out_type=l.out_type and
--d.ab_test=l.ab_test and d.ab_variant=l.ab_variant
--where d.level_bin=0
--group by 1,3,4,5,6,7,8,9;
--
------------------------------------------------------------------------------------------------------------------------------------------------
---- Ledger by level
-----------------------------------------------------------------r-------------------------------------------------------------------------------
--
--drop table if exists tmp_agg_dau_level;
--create table tmp_agg_dau_level as
--with this_date as (select max(install_date) as date from processed.dim_user)
--select
--    u.install_date,
--    l.level,
--    u.level as current_level,
--    cube.transaction_type,
--    cube.in_name,
--    cube.in_type,
--    cube.out_name,
--    cube.out_type,
--    count(distinct u.user_key) as user_cnt
--from processed.dim_user u
--join (select distinct level from processed.dim_user) l on (u.level>l.level or u.level=50)
--join
--(
--  select distinct level,transaction_type,in_name,in_type,out_name,out_type
--  from processed.fact_ledger
--) cube on cube.level=l.level
--join this_date t on 1=1
--where install_date>'2014-10-15'
--group by 1,2,3,4,5,6,7,8;
--
--drop table if exists processed.agg_level_ledger;
--create table processed.agg_level_ledger as
--with agg_ledger as (
--SELECT
--    u.install_date,
--    ledger.level,
--    u.level as current_level,
--    ledger.transaction_type,
--    ledger.in_name,
--    ledger.in_type,
--    ledger.out_name,
--    ledger.out_type,
--    sum(ledger.in_amount) as in_amount,
--    sum(ledger.out_amount) as out_amount,
--    sum(ledger.in_amount) over
--        (partition by   u.install_date,ledger.transaction_type,
--                        ledger.in_name,ledger.in_type,
--                        ledger.out_name, ledger.out_type
--         order by ledger.level asc
--         rows unbounded preceding) as running_in_amount,
--    sum(ledger.in_amount) over
--        (partition by   u.install_date,ledger.transaction_type,
--                        ledger.in_name,ledger.in_type,
--                        ledger.out_name, ledger.out_type
--         order by ledger.level asc
--         rows unbounded preceding) as running_in_amount,
--    sum(ledger.out_amount) as out_amount,
--    count(distinct ledger.user_key) as  purchaser_cnt
--FROM  processed.fact_ledger ledger
--join  processed.dim_user u on ledger.user_key=u.user_key
--where install_date>'2014-10-15' and (ledger.level<u.level or u.level=50)
--group by 1,2,3,4,5,6,7,8
--)
--SELECT
--    d.install_date,
--    d.level,
--    d.current_level,
--    d.transaction_type,
--    d.in_name,
--    d.in_type,
--    d.out_name,
--    d.out_type,
--    coalesce(l.in_amount,0) as in_amount,
--    coalesce(l.out_amount,0) as out_amount,
--    coalesce(purchaser_cnt,0) as  purchaser_cnt,
--    coalesce(d.user_cnt,0) as user_cnt
--FROM  tmp_agg_dau_level d
--
--left join  agg_ledger l
--on
--l.install_date=d.install_date and
--d.level=l.level and d.current_level=l.current_level and
--d.transaction_type=l.transaction_type and
--d.in_name=l.in_name and d.in_type=l.in_type and
--d.out_name=l.out_name and d.out_type=l.out_type;
--
--create temp table fact_level_balance as
--with level_ledger as (
--select
--       u.user_key,
--       l.level,
--       sum(case when in_name='Coins' then ledger.in_amount else 0 end) as coins_received,
--       sum(case when out_name='Coins' then ledger.in_amount else 0 end) as coins_spent,
--       max(ts) as ts
--FROM processed.dim_user u
--join (select distinct level from processed.dim_user) l on (u.level>l.level or u.level=50)
--left join processed.fact_ledger ledger on ledger.user_key=u.user_key and ledger.level=l.level
--where
--    install_date>'2014-10-15' and
--    datediff('day',u.install_date,CURRENT_DATE)<=60 and
--    (in_name='Coins' or out_name='Coins') and u.uid=1217909
--group by 1,2 order by 1,2
--)
--select
--    user_key,
--    level,
--    coins_received,
--    coins_spent,
--    sum(coins_received) over (partition by user_key order by level asc  rows unbounded preceding) as cumulative_coins_received,
--    sum(coins_spent) over (partition by user_key order by level asc  rows unbounded preceding) as cumulative_coins_spent,
--    sum(coins_received) over (partition by user_key order by level asc  rows unbounded preceding) - sum(coins_spent) over (partition by user_key order by level asc rows unbounded preceding) as coins_balance,
--    ts
--from level_ledger
--order by 2;
--
--create table jeff.agg_level_coins_balance as
--select
--    level,
--    sum(coins_received) as total_coins_received,
--    sum(coins_spent) as total_coins_spent,
--    sum(cumulative_coins_received) as total_cumulative_coins_received,
--    sum(cumulative_coins_spent) as total_cumulative_coins_spent,
--    sum(coins_balance) as total_coins_balance,
--    count(distinct user_key) as user_cnt,
--    sum(coins_balance)/count(distinct user_key) as avg_coins_balance
--from fact_level_balance
--group by 1;

-- TODO: Discrepancy between Adjust and BI
drop table if exists processed.adjust_bi_discrepancy;
create table processed.adjust_bi_discrepancy as
select
    t5.install_date
    ,t5.install_date_str
    ,t5.app_id
    ,c.country
    ,t5.install_source
    ,t5.campaign
    ,t5.sub_publisher
    ,t5.creative_id
    ,t5.os
    ,t5.all_adjust_installs
    ,t5.empty_user_id_installs
    ,t5.in_bi_installs
    ,t5.not_in_bi_installs
from
    (select
        t1.install_date
        ,cast(t1.install_date as VARCHAR) as install_date_str
        ,t1.app_id
        ,t1.country
        ,t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.os
        ,t1.all_adjust_installs
        ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
        ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
        ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
    from
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,country
            ,trunc(ts) as install_date
            ,case
        	    when app_id in ('com.funplus.xiyou') then 'Android'
        	    when app_id = '964009302' then 'iOS'
            end as os
            ,count(1) as all_adjust_installs
        from unique_adjust
        group by 1,2,3,4,5,6,7,8) t1
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,country
            ,trunc(ts) as install_date
            ,case
        	    when app_id in ('com.funplus.xiyou') then 'Android'
        	    when app_id = '964009302' then 'iOS'
            end as os
            ,count(1) as empty_user_id_installs
        from unique_adjust
        where userid = '' or userid is null
        group by 1,2,3,4,5,6,7,8) t2
            on t1.install_source = t2.install_source and
               t1.install_date = t2.install_date and
               t1.campaign = t2.campaign and
               t1.sub_publisher = t2.sub_publisher and
               t1.creative_id = t2.creative_id and
               t1.app_id = t2.app_id and
               t1.country = t2.country and
               t1.os = t2.os
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,a.country
            ,trunc(ts) as install_date
            ,case
        	    when app_id in ('com.funplus.xiyou') then 'Android'
        	    when app_id = '964009302' then 'iOS'
            end as os
            ,count(1) in_bi_installs
        from unique_adjust a join processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        where a.userid != '' and a.userid is not null and u.uid is not null
        group by 1,2,3,4,5,6,7,8) t3
            on t1.install_source = t3.install_source and
               t1.install_date = t3.install_date and
               t1.campaign = t3.campaign and
               t1.sub_publisher = t3.sub_publisher and
               t1.creative_id = t3.creative_id and
               t1.app_id = t3.app_id and
               t1.country = t3.country and
               t1.os = t3.os
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,a.country
            ,trunc(ts) as install_date
            ,case
        	    when app_id in ('com.funplus.xiyou') then 'Android'
        	    when app_id = '964009302' then 'iOS'
            end as os
            ,count(1) not_in_bi_installs
        from unique_adjust a left join processed.dim_user u on a.userid = u.uid
        where a.userid != '' and a.userid is not null and (u.uid is null or lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) != lower(u.install_source) or trunc(a.ts) != u.install_date)
        group by 1,2,3,4,5,6,7,8) t4
            on t1.install_source = t4.install_source and
               t1.install_date = t4.install_date and
               t1.campaign = t4.campaign and
               t1.sub_publisher = t4.sub_publisher and
               t1.creative_id = t4.creative_id and
               t1.app_id = t4.app_id and
               t1.country = t4.country and
               t1.os = t4.os
    ) t5
    left join
    public.country c
    on t5.country=lower(c.country_code);


-- Fraud with same IP in one day
drop table if exists processed.adjust_ip_fraud;
create table processed.adjust_ip_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when app_id in ('com.funplus.xiyou') then 'Android'
        when app_id = '964009302' then 'iOS'
     end as os
    ,count(1) as fraud_count
    ,count(distinct u.uid) as distinct_fraud_count
from
    unique_adjust a 
    left join 
    processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
    left join
    public.country c on a.country=lower(c.country_code)
where 
    (a.ip_address, date(a.ts))
    in 
    (
        select 
            ip_address
            ,date 
        from 
            (
                select 
                    ip_address
                    ,date(ts) as date
                    ,count(1) 
                from 
                    unique_adjust 
                where
                    tracker_name<>'Organic'
                    and date(ts)>=DATEADD(DAY, -90, current_date)
                    and userid != ''
                    and userid is not null
                group by 1,2
                having count(1)>=10
            )
    )
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -90, current_date)
    and a.userid != ''
    and a.userid is not null
    and u.uid is not null
group by 1,2,3,4,5,6,7,8,9;

-- Timestamp pattern of generating records
drop table if exists processed.adjust_ts_pattern;
create table processed.adjust_ts_pattern as
select
    *
from
    (
    select
        t.install_source
        ,t.campaign
        ,t.sub_publisher
        ,t.creative_id
        ,t.app_id
        ,t.country
        ,t.install_date
        ,t.os
        ,t.tslen - (LAG(t.tslen) OVER(PARTITION BY t.install_source ORDER BY t.install_source, t.tslen)) as tsdiff 
    from
        (
        select
            initcap(split_part(a.tracker_name, '::', 1)) as install_source
            ,split_part(a.tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(a.tracker_name, '::', 4) as creative_id
            ,a.game as app_id
            ,c.country
            ,trunc(ts) as install_date
            ,case
                when app_id in ('com.funplus.xiyou') then 'Android'
                when app_id = '964009302' then 'iOS'
             end as os
            ,(extract(epoch from a.ts) - extract(epoch from DATEADD(DAY, -90, current_date))) as tslen
        from
            unique_adjust a 
            left join 
            processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
            left join
            public.country c on a.country=lower(c.country_code)
        where
            a.tracker_name<>'Organic'
            and date(a.ts)>=DATEADD(DAY, -90, current_date)
            and a.userid != ''
            and a.userid is not null
            and u.uid is not null
            and split_part(a.tracker_name, '::', 1) != ''
        ) t
    order by t.install_source, t.tslen
    )
where
    tsdiff is not null
    and tsdiff <= 200;

-- Fraud with same device model in one day
drop table if exists processed.adjust_device_fraud;
create table processed.adjust_device_fraud as
select 
        t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.app_id
        ,t1.country
        ,t1.device_name
        ,t1.install_date
        ,t1.os
        ,t1.distinct_device_count as device_count
        ,t2.distinct_device_count as device_total
        ,t1.distinct_device_count*1.00/t2.distinct_device_count as device_percent
from
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,a.device_name
        ,trunc(ts) as install_date
        ,case
            when app_id in ('com.funplus.xiyou') then 'Android'
            when app_id = '964009302' then 'iOS'
         end as os
        ,count(distinct u.uid) as distinct_device_count
    from
        unique_adjust a 
        left join 
        processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        left join
        public.country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.userid != ''
        and a.userid is not null
        and a.device_name != ''
        and u.uid is not null
    group by 1,2,3,4,5,6,7,8,9
    having count(distinct u.uid) >= 10
    ) t1
    left join
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,trunc(ts) as install_date
        ,case
            when app_id in ('com.funplus.xiyou') then 'Android'
            when app_id = '964009302' then 'iOS'
         end as os
        ,count(distinct u.uid) as distinct_device_count
    from
        unique_adjust a 
        left join 
        processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        left join
        public.country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.userid != ''
        and a.userid is not null
        and a.device_name != ''
        and u.uid is not null
    group by 1,2,3,4,5,6,7,8
    having count(distinct u.uid) >= 10
    ) t2
    on
        t1.install_source = t2.install_source
        and t1.campaign = t2.campaign
        and t1.sub_publisher = t2.sub_publisher
        and t1.creative_id = t2.creative_id
        and t1.app_id = t2.app_id
        and t1.country = t2.country
        and t1.install_date = t2.install_date
        and t1.os = t2.os
order by device_percent desc;

-- Adjust interval between install and first action 
drop table if exists processed.adjust_first_action;
create table processed.adjust_first_action as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,trunc(a.ts) as install_date
    ,case
        when app_id in ('com.funplus.xiyou') then 'Android'
        when app_id = '964009302' then 'iOS'
     end as os
    ,a.userid
    ,a.tslen
    ,count(1) as session_cnt
    ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    ,max(s.level_end) as level_end
from
    (select
        *
        ,case
            when u.all_max_level = 1 then null
            else (extract(epoch from u.ts_end) - extract(epoch from u.ts_start))
         end as tslen
    from
        unique_adjust adj 
        left join 
        (select
            *
            ,row_number () over (partition by uid order by ts_start) as rank
            ,max(level_end) over (partition by uid) as all_max_level
        from 
            processed.fact_session
        where
            date_start>=DATEADD(DAY, -90, current_date)
            and install_date>=DATEADD(DAY, -90, current_date)
        ) u on adj.userid = u.uid and trunc(adj.ts) = u.install_date
    where adj.tracker_name<>'Organic'
        and split_part(adj.tracker_name, '::', 1) != ''
        and date(adj.ts)>=DATEADD(DAY, -90, current_date)
        and adj.userid != ''
        and adj.userid is not null
        and u.uid is not null
        and u.rank = 1
    ) a
    left join
    public.country c on a.country=lower(c.country_code)
    left join
    processed.fact_dau_snapshot s on a.userid = s.uid and trunc(a.ts) = s.install_date
        and s.date >= DATEADD(DAY, -90, current_date) and s.install_date >= DATEADD(DAY, -90, current_date)
group by 1,2,3,4,5,6,7,8,9,10
;

-- Adjust country fraud
drop table if exists processed.adjust_country_fraud;
create table processed.adjust_country_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when app_id in ('com.funplus.xiyou') then 'Android'
        when app_id = '964009302' then 'iOS'
     end as os
    ,count(1) as fraud_count
    ,count(distinct u.uid) as distinct_fraud_count
from
    unique_adjust a 
    left join 
    processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
    left join
    public.country c on a.country=lower(c.country_code)
where 
    charindex('facebook', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('google', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('youtube', lower(split_part(a.tracker_name, '::', 1))) = 0
    and c.country not in ('Hong Kong', 'Macao', 'Malaysia', 'Singapore')
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -8, current_date)
    and date(a.ts)<DATEADD(DAY, -1, current_date)
    and a.userid != ''
    and a.userid is not null
    and u.uid is not null
group by 1,2,3,4,5,6,7,8,9
;

-- Adjust fraud monitor
drop table if exists processed.adjust_fraud_monitor;
create table processed.adjust_fraud_monitor as
select
    t1.install_source
    ,t1.sub_publisher
    ,t1.app_id
    ,trunc(DATEADD(DAY, -8, current_date)) as install_date
    ,t1.install_count
    ,t2.d1_retained*1.00/t2.d1_new_installs as d1_retention
    ,t2.d7_retained*1.00/t2.d7_new_installs as d7_retention
    ,t3.level1_percent
    ,t4.discrepancy
    ,t5.country_fraud_count*1.00/t1.install_count as country_discrepancy
from
    (
    select
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,a.game as app_id
        ,count(distinct u.uid) as install_count
    from
        unique_adjust a
        left join
        processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
    where
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -8, current_date)
        and date(a.ts)<DATEADD(DAY, -1, current_date)
        and a.userid != ''
        and a.userid is not null
        and u.uid is not null
    group by 1,2,3
    having count(distinct u.uid) > 10
    ) as t1
    left join
    (
    select
        initcap(install_source) as install_source
        ,case
            when install_source like 'Google Adwords%' then creative_id
            else sub_publisher
         end as sub_publisher
        ,app
        ,sum(d1_retained) as d1_retained
        ,case
            when sum(d1_new_installs) = 0 then null
            when sum(d1_new_installs) <> 0 then sum(d1_new_installs)
         end as d1_new_installs
        ,sum(d7_retained) as d7_retained
        ,case
            when sum(d7_new_installs) = 0 then null
            when sum(d7_new_installs) <> 0 then sum(d7_new_installs)
         end as d7_new_installs
    from
        processed.tab_marketing_kpi
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t2
    on
        t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
    left join
    (
    select
        initcap(f1.install_source) as install_source
        ,f1.sub_publisher
        ,f1.app_id
        ,f1.level1_count*1.00/f2.total as level1_percent
    from
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as level1_count
        from
            processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
            and tslen is null
            and level_end = 1
            and active_days >= 2
        group by 1,2,3
        ) as f1
        left join
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as total
        from
            processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
        group by 1,2,3
        ) as f2
        on
            f1.install_source = f2.install_source
            and f1.sub_publisher = f2.sub_publisher
            and f1.app_id = f2.app_id
    ) as t3
    on
        t1.install_source = t3.install_source
        and t1.sub_publisher = t3.sub_publisher
        and t1.app_id = t3.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,abs((sum(all_adjust_installs) - sum(in_bi_installs))*1.00/sum(all_adjust_installs)) as discrepancy
    from
        processed.adjust_bi_discrepancy
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t4
    on
        t1.install_source = t4.install_source
        and t1.sub_publisher = t4.sub_publisher
        and t1.app_id = t4.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,sum(distinct_fraud_count) as country_fraud_count
    from
        processed.adjust_country_fraud
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t5
    on
        t1.install_source = t5.install_source
        and t1.sub_publisher = t5.sub_publisher
        and t1.app_id = t5.app_id
where
    ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.2 and lower(t1.install_source) not in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.1 and lower(t1.install_source) in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or (t2.d1_retained*1.00/t2.d1_new_installs) > 0.6
    or t3.level1_percent > 0.1
    or t4.discrepancy > 0.2
    or (t5.country_fraud_count*1.00/t1.install_count) > 0.1
;

-- Blacklist
drop table if exists processed.blacklist;
create table processed.blacklist as
select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,ip_address
    ,country
    ,null as device_name
    ,sum(distinct_fraud_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,cast(null as numeric) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    processed.adjust_ip_fraud
group by 1,2,3,4,5,6
having sum(distinct_fraud_count) > 10

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,country
    ,device_name
    ,sum(device_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,sum(device_count)*1.00/sum(device_total) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    processed.adjust_device_fraud
group by 1,2,3,4,5,6,7
having 
    sum(device_count) > 10
    and sum(device_count)*1.00/sum(device_total) > 0.2

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,null as country
    ,null as device_name
    ,install_count
    ,d1_retention
    ,d7_retention
    ,level1_percent
    ,discrepancy
    ,cast(null as numeric) as device_percent
    ,country_discrepancy
from
    processed.adjust_fraud_monitor
;

-- Training Data
drop table if exists processed.training_data;
create table processed.training_data as
select
    t1.user_key
    ,t1.uid
    ,t1.app_id
    ,t1.install_source
    ,t1.sub_publisher
    ,t1.install_date
    ,t1.ip_address
    ,t1.country
    ,t1.device_name
    ,t1.os
    ,t1.os_version
    ,t1.level_end
    ,t1.session_cnt
    ,t1.purchase_cnt
    ,t1.active_days
    ,max(case
        when t1.level_end = 1 and t1.active_days >= 5 then 1
        else 0
     end) as flag1
    ,max(case
        when t1.level_end = 1 and t1.ip_address = t2.ip_address then 1
        else 0
     end) as flag2
    ,max(case
        when t1.level_end = 1 and t1.os = 'Android' and t1.device_name = t2.device_name then 1
        else 0
     end) as flag3
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.d1_retention < 0.05 then 1
        else 0
     end) as flag4
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.level1_percent > 0.4 then 1
        else 0
     end) as flag5
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.discrepancy > 0.4 then 1
        else 0
     end) as flag6
    ,max(case
        when t1.level_end = 1 and t2.country_discrepancy > 0.1 
            and charindex('facebook', lower(t1.install_source)) = 0
            and charindex('google', lower(t1.install_source)) = 0
            and charindex('youtube', lower(t1.install_source)) = 0
            and t1.country not in ('Hong Kong', 'Macao', 'Malaysia', 'Singapore') then 1
        else 0
     end) as flag7
from
    (
    select
        u.user_key
        ,u.uid
        ,a.game as app_id
        ,initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,trunc(a.ts) as install_date
        ,a.ip_address
        ,c.country
        ,a.device_name
        ,u.os
        ,u.os_version
        ,max(s.level_end) as level_end
        ,sum(s.session_cnt) as session_cnt
        ,sum(s.purchase_cnt) as purchase_cnt
        ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    from
        public.unique_adjust a
        left join 
        processed.dim_user u on a.userid = u.uid and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        left join
        public.country c on a.country=lower(c.country_code)
        left join
        processed.fact_dau_snapshot s on a.userid = s.uid and trunc(a.ts) = s.install_date and s.date >= DATEADD(DAY, -8, current_date) and s.date < DATEADD(DAY, -1, current_date)
    where 
        a.tracker_name <> 'Organic'
        and date(a.ts) >= DATEADD(DAY, -8, current_date)
        and date(a.ts) < DATEADD(DAY, -1, current_date)
        and a.userid != ''
        and a.userid is not null
        and u.uid is not null
    group by 1,2,3,4,5,6,7,8,9,10,11
    ) t1
    left join
    (
    select
        *
    from
        processed.blacklist b
    where
        b.install_date < DATEADD(DAY, -1, current_date)
    ) t2
    on
        t1.app_id = t2.app_id
        and t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
;


-- TODO: For EAS report
create temp table payment_info as
select app, uid, '' as snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from processed.fact_revenue
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    u.app
    ,u.uid
    ,u.os
    ,u.server
    ,'' as snsid
    ,'' as user_name
    ,'' as email
    ,'' as additional_email
    ,u.install_source
    ,u.install_ts
    ,u.language
    ,u.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,u.total_rc_in as rc
    ,0 as coins
    ,u.last_login_ts as last_login_ts
from processed.dim_user u
    left join payment_info p on u.app = p.app and u.uid = p.uid;


-- TODO: figure out fraud install source or sub_publisher
create temp table adjust_duplicated_users as
select userid, count(1) as install_count
from unique_adjust
where userid != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel;
create table processed.fraud_channel as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d on a.userid = d.userid;

-- login source report
delete from processed.login_source
where date >= current_date - 1;

insert into processed.login_source
select t1.date
    ,t1.social_type
    ,t1.country
    ,t1.dau_count as dau_count
    ,t2.total_cnt as total_cnt
    ,t1.level as level
from
(select date
    ,social_type
    ,country
    ,level
    ,count(1) as dau_count
from 
(select current_date - 1 as date
        ,case 
            when s.social_type is not null then 'facebook'
            when s.social_type is null and a.email_address is not null then 'Email'
            else 'quick_login'
            end as social_type
        ,case 
            when u.country in ('Malaysia','Hong Kong','Singapore') then u.country 
            else 'others'
            end as country
        ,s.fpid as fpid
        ,u.level_end as level
    from processed.fact_dau_snapshot u
    left join processed.social_id s
        on u.uid = s.fpid
    left join processed.account a
        on u.uid = a.fpid
    where u.date = current_date - 1) 
    group by 1,2,3,4
) t1
join 
(select date
    ,social_type
    ,country
    ,level
    ,count(1) as total_cnt
from 
(select current_date - 1 as date
        ,case 
            when s.social_type is not null then 'facebook'
            when s.social_type is null and a.email_address is not null then 'Email'
            else 'quick_login'
            end as social_type
        ,case 
            when u.country in ('Malaysia','Hong Kong','Singapore') then u.country 
            else 'others'
            end as country
        ,s.fpid as fpid
        ,u.level
    from processed.dim_user u
    left join processed.social_id s
        on u.uid = s.fpid
    left join processed.account a
        on u.uid = a.fpid
    where u.install_date <= current_date - 1) 
    group by 1,2,3,4
) t2
on t1.date = t2.date
    and t1.social_type = t2.social_type
    and t1.country = t2.country
    and t1.level = t2.level;

--------------------------------------------------------------------------------------------------------------------------------------------
--The XY session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------
-- TODO: raw data
----------------------------------------------------------------------------------------------------------------------------------------------
-- extract data from json
----------------------------------------------------------------------------------------------------------------------------------------------
delete from xy.processed.events_raw
where  load_hour >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                );

insert into xy.processed.events_raw
(
    load_hour
    ,event
    ,date
    ,ts
    ,app
    ,uid
    ,server
    ,install_ts
    ,country_code
    ,level
    ,vip_level
    ,ip
    ,os
    ,os_version
    ,app_version
    ,session_id
    ,bi_version
    ,idfa
    ,gaid
    ,android_id
    ,mac_address
    ,device
    ,lang
    ,json_str
    ,properties
    ,md5
)
select
    load_hour
    ,json_extract_path_text(json_str, 'event') as event
    ,trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) as date
    ,dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str, 'app_id') as app
    ,CAST(json_extract_path_text(json_str, 'user_id') AS INTEGER) as uid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gameserver_id') as server
    ,dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,'--' as country_code
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'level') AS INTEGER) as level
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'vip_level') as INTEGER) as vip_level
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'ip') as ip
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os') as os
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os_version') as os_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'app_version') as app_version
    ,json_extract_path_text(json_str, 'session_id') as session_id
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'idfa') as idfa
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gaid') as gaid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'android_id') as android_id
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'mac_address') as mac_address
    ,case
        when char_length(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device')) > 128
        then substring(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device') from 1 for 127)
        else json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device')
        end as device
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'lang') as lang
    ,json_str as json_str
    ,json_extract_path_text(json_str, 'properties') as properties
    ,md5(json_str) as md5
from xy.processed.raw_data_s3
where  load_hour >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                )
and json_extract_path_text(json_str, 'user_id') != ''
and regexp_substr(json_extract_path_text(json_str, 'user_id'), '[a-z]+') = ''
and load_hour >= '2015-08-13';


-- ip_country_map
create temp table ip_address as
select distinct
    ip,
    cast(split_part(ip,'.',1) AS BIGINT) * 16777216
    + cast(split_part(ip,'.',2) AS BIGINT) * 65536
    + cast(split_part(ip,'.',3) AS BIGINT) * 256
    + cast(split_part(ip,'.',4) AS BIGINT) as ip_int
from xy.processed.events_raw
where load_hour >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                    )
and   ip != ''
and   ip != 'unknown';

create temp table ip_country_map as
select
     i.ip
    ,i.ip_int
    ,c.country_iso_code as country_code
from ip_address i
    join xy.processed.ip_integer_range_block r on i.ip_int >= network_start_integer and i.ip_int <= network_last_integer
    join xy.processed.geoip_country c on r.geoname_id = c.geoname_id;

update xy.processed.events_raw
set country_code = i.country_code
from ip_country_map i
where xy.processed.events_raw.ip = i.ip
and   load_hour >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                   );

-- TODO: check the data
-- drop table if exists xy.processed.tab_spiderman_capture;
-- create table xy.processed.tab_spiderman_capture (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number_old" BIGINT NOT NULL DEFAULT 0,
--   "row_number_new" BIGINT NOT NULL DEFAULT 0,
--   "row_number_diff" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);
--
-- drop table if exists xy.processed.tab_spiderman_capture_history;
-- create table xy.processed.tab_spiderman_capture_history (
--   "record_date" date NOT NULL,
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(record_date, event, date, hour);

truncate table xy.processed.tab_spiderman_capture;
drop table xy.processed.tab_spiderman_capture_old;
alter table xy.processed.tab_spiderman_capture_new rename to tab_spiderman_capture_old;

create table xy.processed.tab_spiderman_capture_new
(
  app         VARCHAR(32) NOT NULL,
  event       VARCHAR(32) NOT NULL,
  date        DATE NOT NULL,
  hour        SMALLINT NOT NULL,
  row_number  BIGINT NOT NULL
)
  SORTKEY(app, event, date, hour);

insert into xy.processed.tab_spiderman_capture_new
select app, event, trunc(ts) as date, extract(hour from ts) as hour, count(1)
from xy.processed.events_raw
group by 1,2,3,4;

insert into xy.processed.tab_spiderman_capture
select n.app, n.event, n.date, n.hour, case when o.row_number is null then 0 else o.row_number end as row_number_old,
    n.row_number as row_number_new
from xy.processed.tab_spiderman_capture_new n left join xy.processed.tab_spiderman_capture_old o
    on n.app = o.app and n.event = o.event and n.date = o.date and n.hour = o.hour
union
select o.app, o.event, o.date, o.hour, o.row_number as row_number_old,
    case when n.row_number is null then 0 else n.row_number end as row_number_new
from xy.processed.tab_spiderman_capture_old o left join xy.processed.tab_spiderman_capture_new n
    on n.app = o.app and n.event = o.event and n.date = o.date and n.hour = o.hour;

update xy.processed.tab_spiderman_capture
set row_number_diff = row_number_new - row_number_old;

delete from xy.processed.tab_spiderman_capture_history where record_date = CURRENT_DATE;
insert into xy.processed.tab_spiderman_capture_history
(
    record_date
    ,app
    ,event
    ,date
    ,hour
    ,row_number
)
select
    CURRENT_DATE as record_date
    ,app
    ,event
    ,date
    ,hour
    ,row_number
from xy.processed.tab_spiderman_capture_new;

-- TODO: fact tables
----------------------------------------------------------------------------------------------------------------------------------------------
-- xy.processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table tmp_fact_session as
select trunc(ts) as date_start
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,null as snsid
       ,MD5(server || uid) AS player_key
       ,server
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,null as install_source
       ,app_version as app_version
       ,session_id as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,level as level_start
       ,level as level_end
       ,vip_level as vip_level_start
       ,vip_level as vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,lang AS language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,0 as session_length
from   xy.processed.events_raw
where  event in ('newuser', 'session_start')
and    trunc(ts) >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                    );

create temp table session_end as
select MD5(app||uid) as user_key
       ,app
       ,uid
       ,session_id
       ,max(ts) as ts_end
       ,max(level) as level_end
       ,max(vip_level) as vip_level_end
from   xy.processed.events_raw
where  event = 'session_end'
and    trunc(ts) >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                    )
and    session_id != ''
group by 2,3,4;

update tmp_fact_session
set ts_end = s.ts_end,
    level_end = s.level_end,
    vip_level_end = s.vip_level_end
from session_end s
where tmp_fact_session.user_key = s.user_key and tmp_fact_session.session_id = s.session_id;

update tmp_fact_session
set session_length = DATEDIFF('second', ts_start, ts_end);

--delete the historical data in case repeat loading
delete from  xy.processed.fact_session
where  date_start >= (
                     select raw_data_start_date
                     from xy.processed.tmp_start_date
                );

-- insert into xy.processed.fact_session
insert into xy.processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,player_key
       ,server
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,vip_level_start
       ,vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,session_length
)
select
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,player_key
       ,server
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,vip_level_start
       ,vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,session_length
from tmp_fact_session;

----------------------------------------------------------------------------------------------------------------------------------------------
-- xy.processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  xy.processed.fact_revenue
where  date >= (
                     select raw_data_start_date
                     from   xy.processed.tmp_start_date
                );

create temp table tmp_fact_revenue as
select *
from    xy.processed.events_raw
where   event = 'payment'
and     date >= (
                    select raw_data_start_date
                    from xy.processed.tmp_start_date
                );


insert into xy.processed.fact_revenue
(
       date
       ,user_key
       ,app
       ,uid
       ,snsid
       ,player_key
       ,server
       ,player_id
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts
       ,level
       ,vip_level
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address

       ,payment_processor
       ,product_id
       ,product_name
       ,product_type
       ,coins_in
       ,rc_in
       ,currency
       ,amount
       ,usd
       ,transaction_id
)
select  date
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,null as snsid
       ,MD5(server || uid) AS player_key
       ,server AS server
       ,null AS player_id
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,null as install_source
       ,app_version as app_version
       ,session_id as session_id
       ,ts
       ,level
       ,vip_level
       ,os
       ,os_version
       ,country_code
       ,ip
       ,lang AS language
       ,device AS device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address

       ,json_extract_path_text(properties,'payment_processor') AS payment_processor
       ,json_extract_path_text(properties,'iap_product_id') AS product_id
       ,json_extract_path_text(properties,'iap_product_name') AS product_name
       ,json_extract_path_text(properties,'product_type') AS product_type
       ,CAST(
               CASE WHEN json_extract_path_text (properties,'coins_in') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'coins_in')
               END AS INTEGER
             ) AS coins_in
       ,CAST(
                CASE WHEN regexp_substr(regexp_substr(json_extract_path_text(properties,'iap_product_id'),'stones[0-9]+'), '[0-9]+') = '' THEN '0'
                     ELSE regexp_substr(regexp_substr(json_extract_path_text(properties,'iap_product_id'),'stones[0-9]+'), '[0-9]+')
                END AS INTEGER
            ) AS rc_in
       ,json_extract_path_text(properties,'currency') AS currency
       ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4))/100 AS amount
       ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4))/100 AS usd
       ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    tmp_fact_revenue;

create temp table tmp_duplicated_transaction_id as
select transaction_id, count(1)
from xy.processed.fact_revenue
where transaction_id != ''
group by transaction_id
having count(1) > 1;

create temp table tmp_duplicated_transaction_id_rows as
select t.*, row_number() over (partition by transaction_id order by ts) rank
from
(select r.*
from xy.processed.fact_revenue r join tmp_duplicated_transaction_id d on r.transaction_id = d.transaction_id) t;

create temp table tmp_duplicated_transaction_id_first_row as
select *
from tmp_duplicated_transaction_id_rows
where rank = 1;

alter table tmp_duplicated_transaction_id_first_row drop column rank;

delete
from xy.processed.fact_revenue
using tmp_duplicated_transaction_id d
where xy.processed.fact_revenue.transaction_id = d.transaction_id;

insert into xy.processed.fact_revenue
select *
from tmp_duplicated_transaction_id_first_row;

update xy.processed.fact_revenue
set usd = amount * case when c.factor is null then 0 else c.factor end
from public.currency c
where xy.processed.fact_revenue.currency = c.currency and xy.processed.fact_revenue.date = c.dt;

update xy.processed.fact_revenue
set payment_processor = 'appstore'
where transaction_id like 'funplus_sdk-ios%';

update xy.processed.fact_revenue
set payment_processor = 'googleplay'
where transaction_id like 'funplus_sdk-ad%';

update xy.processed.fact_revenue
set usd_iap = usd
where payment_processor in ('appstore','googleplay') and
    date >= (
                select raw_data_start_date
                from   xy.processed.tmp_start_date
            );

update xy.processed.fact_revenue
set usd_3rd = usd
where payment_processor not in ('appstore','googleplay') and
    date >= (
                select raw_data_start_date
                from   xy.processed.tmp_start_date
            );


-- daily_level
delete from  xy.processed.daily_level
where  date >= (
                     select raw_data_start_date
                     from   xy.processed.tmp_start_date
                );

create temp table player_level as
select trunc(ts) as date
        ,MD5(app || uid) as user_key
        ,app
        ,uid
        ,MD5(server || uid) AS player_key
        ,server AS server
        ,level
        ,vip_level
from xy.processed.events_raw
where date >= (
                    select raw_data_start_date
                    from xy.processed.tmp_start_date
              );


insert into xy.processed.daily_level
(
    date
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,min_level
    ,max_level
    ,min_vip_level
    ,max_vip_level
)
select
    date
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,min(level) as min_level
    ,max(level) as max_level
    ,min(vip_level) as min_vip_level
    ,max(vip_level) as max_vip_level
from player_level
group by 1,2,3,4,5,6;

-- TODO: Remove duplicated records from adjust table
drop table if exists unique_adjust;
create table unique_adjust as
select *
from
(select *, row_number() over (partition by adid order by ts) as row_number
from adjust)  t
where row_number = 1;

update unique_adjust
set userid = a.userid
from adjust a
where unique_adjust.userid = '' and a.userid != '' and unique_adjust.adid = a.adid;

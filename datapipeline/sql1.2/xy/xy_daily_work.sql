-- create new database account
select * from pg_user;

create schema xy_pm;

create user "xy_pm" with password 'PY2sS7DNVFAvcut1';

grant usage on schema xy_pm to guoguang;
grant create on schema xy_pm to guoguang;
grant all on all tables in schema xy_pm to guoguang;

grant usage on schema processed to guoguang;
grant select on all tables in schema processed to guoguang;

grant usage on schema public to guoguang;
grant select on all tables in schema public to guoguang;

revoke all on all tables in schema processed from xy_pm;
revoke all on all tables in schema public from xy_pm;

-- TODO: get detailed info of fraud installs
create temp table fraud_installs_uid as
select
    app_id
    ,country
    ,game
    ,install_source
	,sub_publisher
	,userid
	,count(1) as installs
from processed.fraud_channel
where install_source != 'Organic' and install_date >= '2015-08-13' and install_date < '2015-08-31'
group by 1,2,3,4,5,6
having count(1) >= 10;

select distinct a.*
from
(select *
from fraud_installs_uid) t
join unique_adjust a on t.userid = a.userid
where t.install_source = 'Mobvista+(incent)' and t.userid = a.userid and split_part(a.tracker_name, '::', 1) = 'Mobvista+(incent)'
order by split_part(a.tracker_name, '::', 1),
    split_part(a.tracker_name, '::', 3), userid, ts;


-- TODO: get retargeting list
-- level >= 5, uid, IDFA, GAID 
drop table if exists processed.re_targeting_compaign_level_5;
create table processed.re_targeting_compaign_level_5 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.level >= 5 and (idfa != '' or gaid != '');


unload ('select * from processed.re_targeting_compaign_level_5;')
to 's3://com.funplus.bitest/xy/retargeting/re_targeting_compaign_level_5_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


-- data validation for xiyou idfa*uid
drop table if exists processed.uid_idfa_cross_validation;
create table processed.uid_idfa_cross_validation as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.level >= 5 and (idfa != '' or gaid != '');

select count(1) from processed.uid_idfa_cross_validation
select count(1) from (select distinct idfa from processed.uid_idfa_cross_validation)
select count(1) from (select distinct uid from processed.uid_idfa_cross_validation)
select count(1) from (select idfa from processed.uid_idfa_cross_validation group by idfa having count(uid) > 5 );
select count(1) from (select uid from processed.uid_idfa_cross_validation group by uid having count(idfa) > 5 );

--marketing table ddl
CREATE TABLE processed.tab_marketing_kpi
(
	app VARCHAR(32) NOT NULL ENCODE lzo,
	install_date DATE ENCODE delta,
	install_date_str VARCHAR(10) ENCODE lzo,
	install_source VARCHAR(1024) ENCODE lzo,
	campaign VARCHAR(1024) ENCODE lzo,
	sub_publisher VARCHAR(512) ENCODE lzo,
	creative_id VARCHAR(512) ENCODE lzo,
	country VARCHAR(64) ENCODE lzo,
	os VARCHAR(32) ENCODE lzo,
	level INTEGER DEFAULT 0,
	new_installs INTEGER DEFAULT 0,
	d1_new_installs INTEGER DEFAULT 0,
	d3_new_installs INTEGER DEFAULT 0,
	d7_new_installs INTEGER DEFAULT 0,
	d30_new_installs INTEGER DEFAULT 0,
	d60_new_installs INTEGER DEFAULT 0,
	d90_new_installs INTEGER DEFAULT 0,
	d120_new_installs INTEGER DEFAULT 0,
	d150_new_installs INTEGER DEFAULT 0,
	revenue NUMERIC(14, 4) DEFAULT 0,
	d1_revenue NUMERIC(14, 4) DEFAULT 0,
	d3_revenue NUMERIC(14, 4) DEFAULT 0,
	d7_revenue NUMERIC(14, 4) DEFAULT 0,
	d30_revenue NUMERIC(14, 4) DEFAULT 0,
	d60_revenue NUMERIC(14, 4) DEFAULT 0,
	d90_revenue NUMERIC(14, 4) DEFAULT 0,
	d120_revenue NUMERIC(14, 4) DEFAULT 0,
	d150_revenue NUMERIC(14, 4) DEFAULT 0,
	payers INTEGER DEFAULT 0,
	d1_payers INTEGER DEFAULT 0,
	d3_payers INTEGER DEFAULT 0,
	d7_payers INTEGER DEFAULT 0,
	d30_payers INTEGER DEFAULT 0,
	d60_payers INTEGER DEFAULT 0,
	d90_payers INTEGER DEFAULT 0,
	d120_payers INTEGER DEFAULT 0,
	d150_payers INTEGER DEFAULT 0,
	d1_retained INTEGER DEFAULT 0,
	d3_retained INTEGER DEFAULT 0,
	d7_retained INTEGER DEFAULT 0,
	d30_retained INTEGER DEFAULT 0,
	d60_retained INTEGER DEFAULT 0,
	d90_retained INTEGER DEFAULT 0,
	d120_retained INTEGER DEFAULT 0,
	d150_retained INTEGER DEFAULT 0,
	cost NUMERIC(14, 4) DEFAULT 0
)
DISTSTYLE EVEN
SORTKEY
(
	install_date,
	os,
	install_source,
	country,
	campaign,
	sub_publisher,
	creative_id,
	app,
	level
);




-------------------------------------------------
--The XY user install source
--Version 1.2
--Author Robin
/**
Description:
This script get user install source
**/
-------------------------------------------------
CREATE TEMP TABLE tmp_fact_user_install_source
(
  user_key                      VARCHAR(32) NOT NULL ENCODE LZO,
  app                           VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                           INTEGER NOT NULL,
  snsid                         VARCHAR(64) DEFAULT '' ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(uid)
  SORTKEY(user_key, app, uid, snsid, install_date, install_source_adjust, install_source, campaign, sub_publisher);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get the users whose install source should be updated
------------------------------------------------------------------------------------------------------------------------
insert into tmp_fact_user_install_source (user_key, app, uid, install_date)
select md5(app || uid), app, uid, min(install_date) as install_date
from xy.processed.fact_session
where date_start >= (select start_date from xy.processed.tmp_start_date)
group by 1,2,3;

-- delete users whose install source have been updated
delete
from tmp_fact_user_install_source
using xy.processed.fact_user_install_source t2
where tmp_fact_user_install_source.app = t2.app and
      tmp_fact_user_install_source.uid = t2.uid;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get first no 'Organic' install source from adjust
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_install_source as
select game as app, userid as uid,
    first_value(tracker_name ignore nulls) over (partition by game, userid order by ts rows between unbounded preceding and unbounded following) as install_source
from xy.public.adjust
where lower(tracker_name) != 'organic'
order by game, userid;

update tmp_fact_user_install_source
set install_source_adjust_raw = t.install_source,
    install_source_adjust = t.install_source
from tmp_user_install_source t
where tmp_fact_user_install_source.uid = t.uid;

------------------------------------------------------------------------------------------------------------------------
--Step 3. Normalize install_source_adjust from adjust data
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_adjust = split_part(install_source_adjust_raw, '::', 1);

------------------------------------------------------------------------------------------------------------------------
--Step 4. Update install_source, install_source_adjust have higher priority
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_raw = install_source_adjust_raw,
    install_source = install_source_adjust
where install_source = 'Organic' and install_source_adjust != '';

update tmp_fact_user_install_source
set install_source = 'Tapjoy(incent)'
where install_source = 'Tapjoy';

------------------------------------------------------------------------------------------------------------------------
--Step 5. Normalize install_source,
-- Such as mapping both ‘Adperio’ and ‘AdPerio’ to ‘AdPerio’, mapping both 'App Turbo’ and 'App+Turbo’ to 'App Turbo’
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_ref_install_source_map as
select distinct install_source as install_source_raw, lower(install_source) as install_source_lower, install_source
from tmp_fact_user_install_source;

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\+', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\-', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, ' ', '');

insert into tmp_ref_install_source_map
select *
from xy.processed.ref_install_source_map;

create temp table tmp_install_source_map as
select install_source_lower, install_source
from
(select *, row_number () over (partition by install_source_lower order by install_source) as rank
from tmp_ref_install_source_map) t
where t.rank = 1;

update tmp_ref_install_source_map
set install_source = t.install_source
from tmp_install_source_map t
where tmp_ref_install_source_map.install_source_lower = t.install_source_lower;

truncate table xy.processed.ref_install_source_map;
insert into xy.processed.ref_install_source_map
select distinct install_source_raw, install_source_lower, install_source
from tmp_ref_install_source_map;

update tmp_fact_user_install_source
set install_source = t.install_source
from xy.processed.ref_install_source_map t
where tmp_fact_user_install_source.install_source = t.install_source_raw;

-- update campaign
update tmp_fact_user_install_source
set campaign = split_part(install_source_raw, '::', 2);

-- update sub_publisher
update tmp_fact_user_install_source
set sub_publisher = split_part(install_source_raw, '::', 3);

-- update creative_id
update tmp_fact_user_install_source
set creative_id = split_part(install_source_raw, '::', 4);

delete
from tmp_fact_user_install_source
where install_source = 'Organic';

insert into xy.processed.fact_user_install_source
(
    user_key
    ,app
    ,uid
    ,snsid
    ,install_date
    ,install_source_adjust_raw
    ,install_source_adjust
    ,install_source_raw
    ,install_source
    ,campaign
    ,sub_publisher
    ,creative_id
)
select
    user_key
    ,app
    ,uid
    ,snsid
    ,install_date
    ,install_source_adjust_raw
    ,install_source_adjust
    ,install_source_raw
    ,install_source
    ,campaign
    ,sub_publisher
    ,creative_id
from tmp_fact_user_install_source;



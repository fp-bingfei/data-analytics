-- start date table
-- drop table processed.tmp_start_date;
-- create table processed.tmp_start_date as
-- select DATEADD(day, -7, CURRENT_DATE) as start_date;

update processed.tmp_start_date
set start_date = DATEADD(day, -15, ?::DATE);
COMMIT;

-- Delete Test Data
--delete from processed.dim_user where app_id not in ('fruitscoot.global.dev', 'fruitscoot.global.prod');
--delete from processed.fact_dau_snapshot where app_id not in ('fruitscoot.global.dev', 'fruitscoot.global.prod');
--delete from processed.fact_session where app_id not in ('fruitscoot.global.dev', 'fruitscoot.global.prod');
--delete from processed.fact_levelup where app_id not in ('fruitscoot.global.dev', 'fruitscoot.global.prod');
--delete from processed.fact_revenue where app_id not in ('fruitscoot.global.dev', 'fruitscoot.global.prod');
--delete from processed.agg_tutorial where app_id not in ('fruitscoot.global.dev', 'fruitscoot.global.prod');

--delete from processed.dim_user where app_id ='fruitscoot.global.prod' and app_version<'0.3.0';
--delete from processed.fact_dau_snapshot where app_id ='fruitscoot.global.prod' and app_version<'0.3.0';
--delete from processed.fact_session where app_id ='fruitscoot.global.prod' and app_version<'0.3.0';
--delete from processed.fact_revenue where app_id ='fruitscoot.global.prod' and app_version<'0.3.0';
--delete from processed.agg_tutorial where app_id ='fruitscoot.global.prod'and app_version<'0.3.0';

-- app_user_id='141' app_version is mostly 0.2.1 except on 2015-03-04 where the app_version=0.3.0.4
-- , that causes inconsistent app_version in dim_user and fact_dau_snapshot. 
-- And delete above (app_version<'0.3.0') only delete the one in dim_user but not in fact_dau_snapshot.
delete from processed.fact_dau_snapshot where user_key = 'bb15872f9fe0da7b410f741f582af741' and user_key not in (select user_key from processed.dim_user where app_user_id='141');
commit;

-- clean up OS

update processed.fact_session set os='Android' where os='android';
update processed.fact_revenue set os='Android' where os='android';
update processed.fact_levelup set os='Android' where os='android';
update processed.fact_dau_snapshot set os='Android' where os='android';
update processed.dim_user set os='Android' where os='android';
update processed.fact_tutorial set os='Android' where os='android';
update processed.fact_transaction set os='Android' where os='android';
update processed.fact_mission set os='Android' where os='android';
update processed.fact_message_sent set os='Android' where os='android';
update processed.fact_accept_help set os='Android' where os='android';

update processed.fact_session set os='iOS' where os='ios';
update processed.fact_revenue set os='iOS' where os='ios';
update processed.fact_levelup set os='iOS' where os='ios';
update processed.fact_dau_snapshot set os='iOS' where os='ios';
update processed.dim_user set os='iOS' where os='ios';
update processed.fact_tutorial set os='iOS' where os='ios';
update processed.fact_transaction set os='iOS' where os='ios';
update processed.fact_mission set os='iOS' where os='ios';
update processed.fact_message_sent set os='iOS' where os='ios';
update processed.fact_accept_help set os='iOS' where os='ios';

update processed.fact_revenue set os='Windows' where os='windows';
update processed.fact_levelup set os='Windows' where os='windows';
update processed.fact_dau_snapshot set os='Windows' where os='windows';
update processed.dim_user set os='Windows' where os='windows';
update processed.fact_revenue set os='Windows' where os='windows';
update processed.fact_tutorial set os='Windows' where os='windows';
update processed.fact_transaction set os='Windows' where os='windows';
update processed.fact_mission set os='Windows' where os='windows';
update processed.fact_message_sent set os='Windows' where os='windows';
update processed.fact_accept_help set os='Windows' where os='windows';


-- Dedup fact_mission

DROP TABLE IF EXISTS fact_mission_dedup;
CREATE TEMP TABLE fact_mission_dedup AS
SELECT *, row_number() over (partition by id,mission_status order by ts) as row
FROM processed.fact_mission
WHERE date >=(select start_date from processed.tmp_start_date);

DELETE FROM processed.fact_mission
USING fact_mission_dedup d
where processed.fact_mission.date >=(select start_date from processed.tmp_start_date) and processed.fact_mission.id=d.id and d.row>1;

INSERT INTO processed.fact_mission
select  id,mission_id, app_id, app_version, user_key, app_user_id, date, ts, session_id, os, os_version, device, browser, browser_version, country_code, ip, language, mission_start_ts, mission_status, level
FROM fact_mission_dedup
WHERE row=2;



--- delete from dim_user with wrong install dates

delete from processed.dim_user where trunc(last_login_ts)<install_date;

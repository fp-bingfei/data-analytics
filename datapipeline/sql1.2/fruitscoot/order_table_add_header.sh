date=`date +%Y%m%d`
rm -rf /tmp/order_table
mkdir -p /tmp/order_table
cd /tmp/order_table
hadoop fs -copyToLocal s3://com.funplus.analyst/fruitscoot/export/order_table/*.csv
hadoop fs -copyToLocal s3://com.funplus.analyst/fruitscoot/export/order_table/order_table_challenge_header.csv
hadoop fs -copyToLocal s3://com.funplus.analyst/fruitscoot/export/order_table/${date}/noheader/${date}.csv.000.gz
hadoop fs -copyToLocal s3://com.funplus.analyst/fruitscoot/export/order_table/${date}/noheader/order_attempt_${date}.csv.000.gz
hadoop fs -copyToLocal s3://com.funplus.analyst/fruitscoot/export/order_table/${date}/noheader/order_challenge_${date}.csv.000.gz
gunzip ${date}.csv.000.gz order_attempt_${date}.csv.000.gz order_challenge_${date}.csv.000.gz
cat order_table_header.csv ${date}.csv.000 > new_${date}.csv.000
cat order_table_header.csv order_attempt_${date}.csv.000 > new_order_attempt_${date}.csv.000
cat order_table_challenge_header.csv order_challenge_${date}.csv.000 > new_order_challenge_${date}.csv.000
gzip new_${date}.csv.000
gzip new_order_attempt_${date}.csv.000
gzip new_order_challenge_${date}.csv.000
hdfs dfs -rm -r -f s3://com.funplus.analyst/fruitscoot/export/order_table/${date}/header
hdfs dfs -mkdir -p s3://com.funplus.analyst/fruitscoot/export/order_table/${date}/header
hadoop fs -copyFromLocal new_* s3://com.funplus.analyst/fruitscoot/export/order_table/${date}/header/
rm -rf /tmp/order_table

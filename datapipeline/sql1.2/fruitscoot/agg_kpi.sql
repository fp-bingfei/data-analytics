delete from processed.agg_kpi
where date >=(select start_date from processed.tmp_start_date);

--agg_kpi_ab table is identical to agg_kpi, except that it also breaks down by ab_test info.
--when ab_experiment=NULL in agg_kpi_ab, it is exactly the same as agg_kpi

insert into processed.agg_kpi
(
	id
	,date 
	,app_id
	,app_version
	,install_year 
	,install_month
	,install_week
	,install_source
	,level_end
	,browser
	,browser_version
	,device_alias
	,country
	,os
	,os_version
	--,language
	,ab_experiment
	,ab_variant
	,is_new_user
	,is_payer
	,new_user_cnt
	,dau_cnt
	,newpayer_cnt
	,payer_today_cnt
	,payment_cnt
	,revenue_usd
	,session_cnt
        ,session_length_sec
)
select
      MD5(d.date||COALESCE(d.app_id,'')||COALESCE(d.app_version,'')||EXTRACT(year FROM u.install_date)||
      EXTRACT(month FROM u.install_date)||EXTRACT(week FROM u.install_date) ||COALESCE(u.install_source,'')||
      COALESCE(level_end,0)||COALESCE(d.browser,'')||COALESCE(d.browser_version,'')||COALESCE(d.device,'')||
      COALESCE(d.country,'')||COALESCE(d.os,'')||COALESCE(d.os_version,'')||COALESCE(d.language,'')||
      COALESCE(d.is_new_user,0)||COALESCE(d.is_payer,0)) as id
      ,d.date
      ,d.app_id
      ,d.app_version
      ,EXTRACT(year FROM u.install_date) as install_year
      ,EXTRACT(month FROM u.install_date) as install_month
      ,EXTRACT(week FROM u.install_date) as install_week
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.browser_version
      ,d.device
      ,d.country
      ,d.os
      ,d.os_version
      --,d.language
      ,d.ab_experiment
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as session_length_sec
from processed.fact_dau_snapshot d
join processed.dim_user u on d.user_key=u.user_key
where date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19;

delete from processed.agg_kpi_ab
where date >=(select start_date from processed.tmp_start_date);


create temp table fb_connected as 
select user_key,max(is_facebook_connected) as is_facebook_connected from processed.fact_dau_snapshot group by user_key;


insert into processed.agg_kpi_ab
(
      id
      ,date 
      ,app_id
      ,app_version
      ,install_year 
      ,install_month
      ,install_week
      ,install_source
      ,level_end
      ,browser
      ,browser_version
      ,device_alias
      ,country
      ,os
      ,os_version
      --,language
      ,ab_experiment
      ,ab_variant
      ,is_new_user
      ,is_payer
      ,is_facebook_connected
      ,new_user_cnt
      ,dau_cnt
      ,newpayer_cnt
      ,payer_today_cnt
      ,payment_cnt
      ,revenue_usd
      ,session_cnt
        ,session_length_sec
        ,install_campaign
)
select
      MD5(d.date||COALESCE(d.app_id,'')||COALESCE(d.app_version,'')||EXTRACT(year FROM u.install_date)||
      EXTRACT(month FROM u.install_date)||EXTRACT(week FROM u.install_date) ||COALESCE(u.install_source,'')||COALESCE(u.install_campaign,'')||
      COALESCE(level_end,0)||COALESCE(d.browser,'')||COALESCE(d.browser_version,'')||COALESCE(d.device,'')||
      COALESCE(d.country,'')||COALESCE(d.os,'')||COALESCE(d.os_version,'')||COALESCE(d.language,'')||
      COALESCE(d.is_new_user,0)||COALESCE(d.is_payer,0)||coalesce(f.is_facebook_connected,0)) as id
      ,d.date
      ,d.app_id
      ,d.app_version
      ,EXTRACT(year FROM u.install_date) as install_year
      ,EXTRACT(month FROM u.install_date) as install_month
      ,EXTRACT(week FROM u.install_date) as install_week
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.browser_version
      ,d.device
      ,d.country
      ,d.os
      ,d.os_version
      --,d.language
      ,c.ab_experiment
      ,c.ab_variant
      ,d.is_new_user
      ,d.is_payer
      ,f.is_facebook_connected
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as session_length_sec
      ,u.install_campaign
from processed.fact_dau_snapshot d
join processed.dim_user u on d.user_key=u.user_key
left join fb_connected f on d.user_key=f.user_key
left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on d.user_key = c.user_key
where date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,29;

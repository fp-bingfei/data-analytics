---------------
-- Order table
---------------
CREATE TABLE IF NOT EXISTS processed.order_table
(
	id VARCHAR(100),
	mission_id VARCHAR(100),
	app_id VARCHAR(100),
	app_version VARCHAR(32),
	user_key VARCHAR(100),
	app_user_id VARCHAR(100),
	date DATE,
	ts TIMESTAMP,
	session_id VARCHAR(256),
	os VARCHAR(100),
	os_version VARCHAR(100),
	device VARCHAR(100),
	browser VARCHAR(100),
	browser_version VARCHAR(100),
	country_code VARCHAR(32),
	ip VARCHAR(100),
	language VARCHAR(32),
	mission_start_ts TIMESTAMP,
	mission_status INTEGER,
	level INTEGER,
	initial_moves BIGINT,
	points_2_stars BIGINT,
	points_3_stars BIGINT,
	frozen_fruits_rate BIGINT,
	butterfly_number BIGINT,
	moves_remaining BIGINT,
	moves_used BIGINT,
	moves_buy BIGINT,
	points_final BIGINT,
	points_initial BIGINT,
	no_possible_moves BIGINT,
	order_status VARCHAR(1000)
);

DELETE FROM processed.order_table WHERE date >=current_date-3;

DROP TABLE IF EXISTS TEMP_MISSION_ORDER_STATUS;
CREATE TEMPORARY TABLE TEMP_MISSION_ORDER_STATUS (id VARCHAR(50), order_status VARCHAR(1000));
INSERT INTO TEMP_MISSION_ORDER_STATUS
SELECT id,
       CASE
           WHEN order_status_10 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9|| ', ' ||order_status_10
           WHEN order_status_9 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9
           WHEN order_status_8 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8
           WHEN order_status_7 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7
           WHEN order_status_6 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6
           WHEN order_status_5 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5
           WHEN order_status_4 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4
           WHEN order_status_3 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3
           WHEN order_status_2 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2
           WHEN order_status_1 IS NOT NULL THEN order_status_1
       END AS order_status
FROM
  (SELECT DISTINCT *
   FROM
     (SELECT id,
             nth_value(order_status,1)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_1,
             nth_value(order_status,2)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_2,
             nth_value(order_status,3)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_3,
             nth_value(order_status,4)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_4,
             nth_value(order_status,5)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_5,
             nth_value(order_status,6)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_6,
             nth_value(order_status,7)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_7,
             nth_value(order_status,8)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_8,
             nth_value(order_status,9)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_9,
             nth_value(order_status,10)
      IGNORE NULLS OVER (PARTITION BY id
                         ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_10
      FROM
        (SELECT id,
                objective_name,
                objective_name|| ':' ||objective_amount::INTEGER- objective_amount_remaining::INTEGER|| '/' ||objective_amount AS order_status
         FROM
           (SELECT DISTINCT *
            FROM processed.fact_mission_objective
            where date >=current_date-3 and app_id='fruitscoot.global.prod'))
      WHERE order_status != ''));

 ---------------------------
-------- Fact_mission -----
---------------------------

DELETE FROM processed.order_table WHERE date >=current_date-3;

INSERT INTO processed.order_table
SELECT DISTINCT *
FROM
  (SELECT m.id,
          m.mission_id,
          m.app_id,
          m.app_version,
          m.user_key,
          m.app_user_id,
          m.date,
          m.ts,
          m.session_id,
          m.os,
          m.os_version,
          m.device,
          m.browser,
          m.browser_version,
          m.country_code,
          m.ip,
          m.language,
          m.mission_start_ts,
          m.mission_status,
          m.level,
          initial_moves AS initial_moves,
          points_2_stars AS points_2_stars,
          points_3_stars AS points_3_stars,
          frozen_fruits_rate AS frozen_fruits_rate,
          butterfly_number AS butterfly_number,
          moves_remaining AS moves_remaining,
          moves_used AS moves_used,
          moves_buy AS moves_buy,
          points_final AS points_final,
          points_initial AS points_initial,
          no_possible_moves AS no_possible_moves,
          o.order_status
   FROM processed.fact_mission m
   LEFT JOIN
     (SELECT id,
             SUM(CASE parameter_name WHEN 'initial_moves' THEN parameter_value::INTEGER ELSE NULL END) AS initial_moves,
             SUM(CASE parameter_name WHEN 'points_2_stars' THEN parameter_value::INTEGER ELSE NULL END) AS points_2_stars,
             SUM(CASE parameter_name WHEN 'points_3_stars' THEN parameter_value::INTEGER ELSE NULL END) AS points_3_stars,
             SUM(CASE parameter_name WHEN 'frozen_fruits_rate' THEN parameter_value::INTEGER ELSE NULL END) AS frozen_fruits_rate,
             SUM(CASE parameter_name WHEN 'butterfly_number' THEN parameter_value::INTEGER ELSE NULL END) AS butterfly_number
      FROM processed.fact_mission_parameter
      WHERE date >=current_date-3 and app_id='fruitscoot.global.prod'
      GROUP BY id) mp
   ON mp.id=m.id
   LEFT JOIN
     (SELECT id,
             SUM(CASE statistic_name WHEN 'moves_remaining' THEN statistic_value::INTEGER ELSE NULL END) AS moves_remaining,
             SUM(CASE statistic_name WHEN 'moves_used' THEN statistic_value::INTEGER ELSE NULL END) AS moves_used,
             SUM(CASE statistic_name WHEN 'moves_buy' THEN statistic_value::INTEGER ELSE NULL END) AS moves_buy,
             SUM(CASE statistic_name WHEN 'points_final' THEN statistic_value::INTEGER ELSE NULL END) AS points_final,
             SUM( CASE statistic_name WHEN 'points_initial' THEN statistic_value::INTEGER ELSE NULL END) AS points_initial,
             SUM(CASE statistic_name WHEN 'no_possible_moves' THEN statistic_value::INTEGER ELSE NULL END) AS no_possible_moves
      FROM processed.fact_mission_statistic
      where date >=current_date-3 and app_id='fruitscoot.global.prod'
      GROUP BY id
      ) ms
   ON ms.id=m.id
   LEFT JOIN TEMP_MISSION_ORDER_STATUS o ON o.id=m.id
     WHERE m.date >=current_date-3
     and m.app_id='fruitscoot.global.prod'
  );

---------------
-- Order table: only keep data before first completion
---------------
CREATE TABLE IF NOT EXISTS processed.order_table_attempt
(
	id VARCHAR(100),
	mission_id VARCHAR(100),
	app_id VARCHAR(100),
	app_version VARCHAR(32),
	user_key VARCHAR(100),
	app_user_id VARCHAR(100),
	date DATE,
	ts TIMESTAMP,
	session_id VARCHAR(256),
	os VARCHAR(100),
	os_version VARCHAR(100),
	device VARCHAR(100),
	browser VARCHAR(100),
	browser_version VARCHAR(100),
	country_code VARCHAR(32),
	ip VARCHAR(100),
	language VARCHAR(32),
	mission_start_ts TIMESTAMP,
	mission_status INTEGER,
	level INTEGER,
	initial_moves BIGINT,
	points_2_stars BIGINT,
	points_3_stars BIGINT,
	frozen_fruits_rate BIGINT,
	butterfly_number BIGINT,
	moves_remaining BIGINT,
	moves_used BIGINT,
	moves_buy BIGINT,
	points_final BIGINT,
	points_initial BIGINT,
	no_possible_moves BIGINT,
	order_status VARCHAR(1000)
);

DELETE FROM processed.order_table_attempt WHERE date >=current_date-3;

INSERT INTO processed.order_table_attempt
select
    o.*
from processed.order_table o
join processed.fact_levelup l
    on  l.user_key=o.user_key
    and l.current_level=cast(o.mission_id as integer)+1
where o.date >=current_date-3
  AND o.ts<=l.levelup_ts
;
---------------
-- Order table: only keep data for golden challenge
---------------
CREATE TABLE IF NOT EXISTS processed.order_table_challenge
(
  id VARCHAR(100),
  mission_id VARCHAR(100),
  mission_name varchar(100),
  app_id VARCHAR(100),
  app_version VARCHAR(32),
  user_key VARCHAR(100),
  app_user_id VARCHAR(100),
  date DATE,
  ts TIMESTAMP,
  session_id VARCHAR(256),
  os VARCHAR(100),
  os_version VARCHAR(100),
  device VARCHAR(100),
  browser VARCHAR(100),
  browser_version VARCHAR(100),
  country_code VARCHAR(32),
  ip VARCHAR(100),
  language VARCHAR(32),
  mission_start_ts TIMESTAMP,
  mission_status INTEGER,
  level INTEGER,
  initial_moves BIGINT,
  points_2_stars BIGINT,
  points_3_stars BIGINT,
  frozen_fruits_rate BIGINT,
  butterfly_number BIGINT,
  moves_remaining BIGINT,
  moves_used BIGINT,
  moves_buy BIGINT,
  points_final BIGINT,
  points_initial BIGINT,
  no_possible_moves BIGINT,
  order_status VARCHAR(1000),
  mission_type VARCHAR(32)

);

DELETE FROM processed.order_table_challenge WHERE date >=current_date-3;

INSERT INTO processed.order_table_challenge
select
o.id,
o.mission_id,
c.mission_name,
o.app_id,	
o.app_version,	
o.user_key,	
o.app_user_id,	
o.date,	
o.ts,	
o.session_id,	
o.os,	
o.os_version,	
o.device,	
o.browser,	
o.browser_version,	
o.country_code,	
o.ip,
o.language,	
o.mission_start_ts,	
o.mission_status,
o.level	,	
o.initial_moves,	
o.points_2_stars,	
o.points_3_stars,	
o.frozen_fruits_rate,	
o.butterfly_number,	
o.moves_remaining,	
o.moves_used,	
o.moves_buy,	
o.points_final	,	
o.points_initial,	
o.no_possible_moves,	
o.order_status	,	
c.mission_type
from processed.order_table o
join processed.challenge_raw c
    on  c.id=o.id
where o.date >=current_date-3 and c.mission_type='golden_challenge';
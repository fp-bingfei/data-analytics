delete from processed.economy where date >=(select start_date from processed.tmp_start_date);

insert into processed.economy
select
app_id	,
app_version,	
date	,
os	,
os_version,	
device	,
lang	,
level	,
country_code	,
'facebook_reward' as transaction_type,
sum(coalesce(fb_reward,0)+coalesce(reward_acorns,0)) as acorns_in,	
0 as acorns_out from processed.facebook_reward where date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9;	

insert into processed.economy
select
app_id	,
app_version,	
date	,
os	,
os_version,	
device	,
lang	,
level	,
country_code	,
'daily_reward' as transaction_type,
sum(resource_amount) as acorns_in,	
0 as acorns_out from processed.fact_daily_reward where date >=(select start_date from processed.tmp_start_date) and resource_id='acorns'
group by 1,2,3,4,5,6,7,8,9;

insert into processed.economy
select
app_id	,
app_version,	
date	,
os	,
os_version,	
device	,
language	,
level	,
country_code	,
item_name as transaction_type,
sum(acorn_in) as acorns_in,	
sum(acorn_out) as acorns_out from processed.fact_transaction where date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10;


insert into processed.economy
select
f.app_id	,
f.app_version,	
f.date	,
f.os	,
f.os_version,	
f.device	,
f.lang	,
f.level	,
f.country_code	,
mission_type as transaction_type,
sum(f.resource_amount) as acorns_in,	
0 as acorns_out from processed.fact_mission_resources_received f
join processed.fact_mission m on f.id=m.id where m.date >=(select start_date from processed.tmp_start_date) and resource_id='acorns'
group by 1,2,3,4,5,6,7,8,9,10;


insert into processed.economy
select
f.app_id ,
m.app_version,	
f.date	,
m.os	,
m.os_version,	
m.device	,
m.language	,
m.level	,
m.country_code	,
mission_type as transaction_type,
0 as acorns_in,	
sum(f.resource_amount) as acorns_out from processed.fact_mission_resources_spent f
join processed.fact_mission m on f.id=m.id where m.date >=(select start_date from processed.tmp_start_date) and resource_id='acorns'
group by 1,2,3,4,5,6,7,8,9,10;


insert into processed.economy
select
f.app_id	,
f.app_version,	
f.date	,
r.os	,
r.os_version,	
r.device	,
r.language	,
f.level	,
r.country_code	,
r.product_type as transaction_type,
sum(f.resource_amount) as acorns_in,	
0 as acorns_out from processed.fact_payment_resources_received f
join processed.fact_revenue r on f.id=r.id where f.resource_id='acorns'
group by 1,2,3,4,5,6,7,8,9,10;



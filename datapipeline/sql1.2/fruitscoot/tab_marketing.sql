create temp table last_date as
select max(date) as date
from processed.fact_dau_snapshot;


delete from processed.tab_marketing_kpi
using last_date d
where datediff('day',install_date,d.date) <= 120;

create temp table tmp_install_users_d3_payers as
select d.user_key, max(d.is_payer) as d3_payer
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date -3 and datediff(day,u.install_date,d.date)<=3
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d7_revenue as
select d.user_key, sum(d.revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-7 and datediff(day,u.install_date,d.date)<=7
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d30_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-30 and datediff(day,u.install_date,d.date)<=30
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d90_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-90 and datediff(day,u.install_date,d.date)<=90
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d120_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-120 and datediff(day,u.install_date,d.date)<=120
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d1_retained_users as
select d.user_key,  1 as d1_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where d.date - u.install_date = 1
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d7_retained_users as
select d.user_key,  1 as d7_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on u.user_key = d.user_key
join last_date l
on 1=1
where d.date - u.install_date = 7
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d15_retained_users as
select d.user_key,  1 as d15_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on u.user_key = d.user_key
join last_date l
on 1=1
where d.date - u.install_date = 15
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d30_retained_users as
select d.user_key,  1 as d30_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where d.date - u.install_date = 30
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

insert into processed.tab_marketing_kpi
(app_id, app_version,install_date, install_source, install_campaign, install_subpublisher, install_country, install_os, new_user_cnt,d1_new_user_cnt
  ,d3_new_user_cnt,d7_new_user_cnt,d15_new_user_cnt,d30_new_user_cnt,d90_new_user_cnt,d120_new_user_cnt, revenue_usd, d7_revenue_usd, d30_revenue_usd, d90_revenue_usd, d120_revenue_usd,
payer_cnt, d3_payer_cnt, d1_retained_user_cnt, d7_retained_user_cnt, d15_retained_user_cnt, d30_retained_user_cnt)
select
    u.app_id,
    d.app_version,
    u.install_date,
    coalesce(u.install_source,'Unknown') as install_source,
    u.install_campaign,
    u.install_subpublisher,
    case when u.install_country = '' or u.install_country is null then 'Unknown' else u.install_country end,
    case when u.install_os = '' or u.install_os is null then 'Unknown' else u.install_os end,
    count(1) as new_user_cnt,
    sum (case when u.install_date <= ld.date-1 then 1 else 0 end) as d1_new_user_cnt,
    sum (case when u.install_date <= ld.date-3 then 1 else 0 end) as d3_new_user_cnt,
    sum (case when u.install_date <= ld.date-7 then 1 else 0 end) as d7_new_user_cnt,
    sum (case when u.install_date <= ld.date-15 then 1 else 0 end) as d15_new_user_cnt,
    sum (case when u.install_date <= ld.date-30 then 1 else 0 end) as d30_new_user_cnt,
    sum (case when u.install_date <= ld.date-90 then 1 else 0 end) as d90_new_user_cnt,
    sum (case when u.install_date <= ld.date-120 then 1 else 0 end) as d120_new_user_cnt,
    sum(coalesce(u.total_revenue_usd,0)) as revenue_usd,
    sum(coalesce(d7_revenue.revenue,0)) as d7_revenue_usd,
    sum(coalesce(d30_revenue.revenue,0))  as d30_revenue_usd,
    sum(coalesce(d90_revenue.revenue,0))  as d90_revenue_usd,
    sum(coalesce(d120_revenue.revenue,0))  as d120_revenue_usd,
    sum(u.is_payer) as payer_cnt,
    sum(coalesce(d3p.d3_payer,0)) as d3_payer_cnt,
    sum(coalesce(d1_retained_users.d1_retained_user_cnt,0)) as d1_retained_user_cnt,
    sum(coalesce(d7_retained_users.d7_retained_user_cnt,0)) as d7_retained_user_cnt,
    sum(coalesce(d15_retained_users.d15_retained_user_cnt,0)) as d15_retained_user_cnt,
    sum(coalesce(d30_retained_users.d30_retained_user_cnt,0)) as d30_retained_user_cnt
from processed.dim_user u
join processed.fact_dau_snapshot d on u.user_key=d.user_key and u.install_date=d.date
join (select date from last_date) ld on 1=1
    left join tmp_install_users_d3_payers d3p on u.user_key = d3p.user_key
    left join tmp_install_users_d7_revenue d7_revenue on u.user_key = d7_revenue.user_key
    left join tmp_install_users_d30_revenue d30_revenue on u.user_key = d30_revenue.user_key
    left join tmp_install_users_d90_revenue d90_revenue on u.user_key = d90_revenue.user_key
    left join tmp_install_users_d120_revenue d120_revenue on u.user_key = d120_revenue.user_key
    left join tmp_install_users_d1_retained_users d1_retained_users on u.user_key = d1_retained_users.user_key
    left join tmp_install_users_d7_retained_users d7_retained_users on u.user_key = d7_retained_users.user_key
    left join tmp_install_users_d15_retained_users d15_retained_users on u.user_key = d15_retained_users.user_key
    left join tmp_install_users_d30_retained_users d30_retained_users on u.user_key = d30_retained_users.user_key
where u.install_date <= (select date from last_date)
and datediff('day',u.install_date,ld.date) <= 120
group by 1,2,3,4,5,6,7,8;



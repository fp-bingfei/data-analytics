DROP VIEW IF EXISTS player_day;
CREATE VIEW  player_day AS (
    SELECT 1 AS day UNION ALL
    SELECT 2 UNION ALL
    SELECT 3 UNION ALL
    SELECT 4 UNION ALL
    SELECT 5 UNION ALL
    SELECT 6 UNION ALL
    SELECT 7 UNION ALL
    SELECT 14 UNION ALL
    SELECT 15 UNION ALL
    SELECT 21 UNION ALL
    SELECT 28 UNION ALL
    SELECT 30 UNION ALL
    SELECT 45 UNION ALL
    SELECT 60 UNION ALL
    SELECT 90 UNION ALL
    SELECT 120 UNION ALL
    SELECT 150 UNION ALL
    SELECT 180 UNION ALL
    SELECT 210 UNION ALL
    SELECT 240 UNION ALL
    SELECT 270 UNION ALL
    SELECT 300 UNION ALL
    SELECT 330 UNION ALL
    SELECT 360
);

drop table if exists player_day_cube;
CREATE TEMP TABLE player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM processed.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device_alias as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.ab_experiment,
    d.ab_variant,
    d.is_payer,
    count(distinct d.user_key) new_user_cnt
FROM processed.dim_user d, player_day p, last_date l, processed.fact_dau_snapshot dau
where
    d.install_date> DATEADD(day,-360,(SELECT start_date FROM processed.tmp_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

CREATE TEMP TABLE baseline AS
WITH last_date AS (SELECT max(date) AS date FROM processed.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device_alias as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.ab_experiment,
    u.ab_variant,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM processed.fact_dau_snapshot d
 JOIN processed.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 WHERE
    u.install_date> DATEADD(day,-360,(SELECT start_date FROM processed.tmp_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

-- TODO: insert instead of create table

DROP TABLE IF EXISTS processed.agg_retention_ltv;
CREATE TABLE processed.agg_retention_ltv AS
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.ab_experiment,
    pc.ab_variant,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube pc
LEFT JOIN baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.ab_experiment,'') = COALESCE(b.ab_experiment,'') AND
    COALESCE(pc.ab_variant,'') = COALESCE(b.ab_variant,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0);




---drop table if exists player_day_cube;
create temp table fb_connected as 
select user_key,max(is_facebook_connected) as is_facebook_connected from processed.fact_dau_snapshot group by user_key;

create temp table app_version as
select d.user_key,d.app_version,u.install_date from processed.fact_dau_snapshot d left join processed.dim_user u on d.user_key=u.user_key where u.install_date=d.date;


CREATE TEMP TABLE player_day_cube_ab AS
WITH last_date AS (SELECT max(date) AS date FROM processed.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    a.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device_alias as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    c.ab_experiment,
    c.ab_variant,
    d.is_payer,
    f.is_facebook_connected,
    count(distinct d.user_key) new_user_cnt
FROM processed.dim_user d
JOIN processed.fact_dau_snapshot u ON d.user_key=u.user_key
left join fb_connected f on d.user_key=f.user_key
left join app_version a on d.user_key=a.user_key
 JOIN player_day p ON DATEDIFF('day', d.install_date, u.date)<=p.day
 join last_date l on 1=1
left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on d.user_key = c.user_key
where
    d.install_date> DATEADD(day,-360,(SELECT start_date FROM processed.tmp_start_date)) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    u.user_key= d.user_key and u.date=d.install_date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;


CREATE TEMP TABLE baseline_ab AS
WITH last_date AS (SELECT max(date) AS date FROM processed.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    a.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device_alias as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    c.ab_experiment,
    c.ab_variant,
    u.is_payer,
    f.is_facebook_connected,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM processed.fact_dau_snapshot d
 left join fb_connected f on d.user_key=f.user_key
 left join app_version a on d.user_key=a.user_key
 JOIN processed.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on d.user_key = c.user_key
WHERE
    u.install_date> DATEADD(day,-360,(SELECT start_date FROM processed.tmp_start_date)) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;

-- TODO: insert instead of create table

DROP TABLE IF EXISTS processed.agg_retention_ltv_ab;
CREATE TABLE processed.agg_retention_ltv_ab AS
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.ab_experiment,
    pc.ab_variant,
    pc.is_payer,
    pc.is_facebook_connected,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM player_day_cube_ab pc
LEFT JOIN baseline_ab b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.ab_experiment,'') = COALESCE(b.ab_experiment,'') AND
    COALESCE(pc.ab_variant,'') = COALESCE(b.ab_variant,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0) and 
    COALESCE(pc.is_facebook_connected,0) = COALESCE(b.is_facebook_connected,0);

update processed.agg_retention_ltv_ab set os='Windows' where os='windows';
update processed.agg_retention_ltv_ab set os='iOS' where os='ios';
update processed.agg_retention_ltv_ab set os='Android' where os='android';

grant select on all tables in schema processed to "fsteam";
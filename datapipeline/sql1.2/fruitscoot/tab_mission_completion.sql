drop table if exists mission_attempt;
create temp table mission_attempt as
SELECT
    s.user_key
    ,s.mission_id
    ,max(s.date) as date
    ,count(distinct s.mission_start_ts) as attempt_cnt
FROM processed.fact_mission s
join processed.fact_levelup l
    on l.user_key=s.user_key and l.current_level=cast(s.mission_id as integer)+1
left join processed.fact_mission e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_id=e.mission_id and
        e.user_key=s.user_key and
        e.mission_status!=s.mission_status
where
    s.mission_status=0 and
    s.ts<l.levelup_ts
group by 1,2
;

drop table if exists attempt_to_pass;
create temp table attempt_to_pass as
SELECT
    m.user_key
	,m.mission_id
	,m.date
	,d.app_id
	,d.app_version
	,MEDIAN(attempt_cnt) over (partition by m.date,m.mission_id,d.app_id,d.app_version) as attempt_median
	,PERCENTILE_CONT(0.25) within group (order by attempt_cnt) over (partition by m.date,m.mission_id,d.app_id,d.app_version) as attempt_q1
	,PERCENTILE_CONT(0.75) within group (order by attempt_cnt) over (partition by m.date,m.mission_id,d.app_id,d.app_version) as attempt_q3
FROM mission_attempt m
join processed.fact_dau_snapshot d on d.user_key=m.user_key and m.date=d.date
join processed.dim_user u ON u.user_key=m.user_key
;

drop table if exists processed.tab_mission_attempt;
create  table processed.tab_mission_attempt as
SELECT
	p.mission_id
	,p.date
	,p.app_id
	,p.app_version
	,c.ab_experiment
    ,c.ab_variant
    ,max(attempt_median) as attempt_median
    ,max(attempt_q1) as attempt_q1
    ,max(attempt_q3) as attempt_q3
    ,sum(attempt_cnt) as attempt_cnt
	,count(distinct p.user_key) as user_cnt
FROM attempt_to_pass p
join mission_attempt m on m.user_key=p.user_key and m.mission_id=p.mission_id
left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on p.user_key = c.user_key
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6;

create temp table moves_used as
SELECT
    m.id
	,m.mission_id
	,MEDIAN(statistic_value) over (partition by m.date,m.mission_id,d.app_id,d.app_version,m.mission_status) as moves_used_median
	,PERCENTILE_CONT(0.25) within group (order by statistic_value) over (partition by m.date,m.mission_id,d.app_id,d.app_version,m.mission_status) as moves_used_q1
	,PERCENTILE_CONT(0.75) within group (order by statistic_value) over (partition by m.date,m.mission_id,d.app_id,d.app_version,m.mission_status) as moves_used_q3
FROM processed.fact_mission  m
join processed.fact_mission_statistic st
    ON st.id=m.id and statistic_name='moves_used'
join processed.fact_dau_snapshot d
    on d.user_key=m.user_key and m.date=d.date
join processed.dim_user u
    ON u.user_key=m.user_key
where
    m.mission_status!=0
;

drop table if exists processed.fact_mission_completion;
create table processed.fact_mission_completion as
SELECT
	s.mission_id
	,d.date
	,d.app_id
	,d.app_version
	,c.ab_experiment
    ,c.ab_variant
	,case e.mission_status
	    when 0      then 'started'
	    when 10     then 'completed'
	    when -10    then 'failed'
	    when -20    then 'abandoned'
	    else 'missing end status'
	 end as mission_status_end
	,max(moves_used_median) as moves_used_median
	,max(moves_used_q1) as moves_used_q1
	,max(moves_used_q3) as moves_used_q3
	,count(distinct s.id) as mission_cnt
	,count(distinct s.user_key) as user_cnt
FROM processed.fact_mission s
join processed.fact_dau_snapshot d
    on d.user_key=s.user_key and s.date=d.date
join processed.dim_user u
    ON u.user_key=s.user_key
join processed.fact_mission e
    on  s.mission_start_ts=e.mission_start_ts and
        s.mission_id=e.mission_id and
        e.user_key=s.user_key and
        e.mission_status!=s.mission_status
join moves_used m
    ON m.id=e.id
left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on s.user_key = c.user_key
where
    s.mission_status=0
group by 1,2,3,4,5,6,7
;

drop table if exists leveling_speed;
create temp table leveling_speed as
SELECT
    lu.user_key
	,d.app_id
	,d.app_version
    ,lu.current_level as level
    ,u.level as current_level
    ,u.is_payer
    ,datediff(day,u.install_date,lu.levelup_date) as levelup_time_daycnt
    ,MEDIAN(datediff(day,u.install_date,lu.levelup_date)) over (partition by d.app_id,d.app_version,lu.current_level,u.level,u.is_payer) as median
    ,PERCENTILE_CONT(0.25) within group (order by datediff(day,u.install_date,lu.levelup_date)) over (partition by d.app_id,d.app_version,lu.current_level,u.level,u.is_payer) as q1
    ,PERCENTILE_CONT(0.75) within group (order by datediff(day,u.install_date,lu.levelup_date)) over (partition by d.app_id,d.app_version,lu.current_level,u.level,u.is_payer) as q3
from processed.fact_levelup lu
join processed.dim_user u on lu.user_key=u.user_key
join processed.fact_dau_snapshot d on d.user_key=lu.user_key and lu.levelup_date=d.date
where u.app_id='fruitscoot.global.prod'
;

drop table if exists processed.tab_leveling_speed;
CREATE TABLE processed.tab_leveling_speed as
SELECT
	app_id
	,app_version
	,level
	,current_level
	,is_payer
	,sum(levelup_time_daycnt) as total_levelup_time_daycnt
	,max(median) as median_levelup_time_daycnt
	,max(q1) as q1_levelup_time_daycnt
	,max(q3) as q3_levelup_time_daycnt
	,count(distinct user_key) as user_cnt
FROM leveling_speed
where levelup_time_daycnt>0
group by 1,2,3,4,5
;

truncate processed.tab_level_churn;
insert into processed.tab_level_churn
with this_date as (select max(date) as date from processed.fact_dau_snapshot)
select
  u.app_id
  ,u.app_version
  ,l.level
  ,u.level as current_level
  ,u.install_date
  ,u.install_source
  ,u.country
  ,u.os
  ,u.browser
  ,u.language
  ,u.is_payer
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join this_date t on 1=1
where app_id='fruitscoot.global.prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;


create temp table fb_connected as 
select user_key,max(is_facebook_connected) as is_facebook_connected from processed.fact_dau_snapshot group by user_key;



truncate table processed.tab_level_churn_ab;
insert into processed.tab_level_churn_ab
with this_date as (select max(date) as date from processed.fact_dau_snapshot)
select
  u.app_id
  ,u.app_version
  ,l.level
  ,u.level as current_level
  ,u.install_date
  ,u.install_source
  ,c.ab_experiment 
  ,c.ab_variant 
  ,u.country
  ,u.os
  ,u.browser
  ,u.language
  ,u.is_payer
  ,f.is_facebook_connected
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=1 then 1 else 0 end as is_churned_1days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days
  ,case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days
  ,count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
left join fb_connected f on u.user_key = f.user_key
join this_date t on 1=1
 left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on u.user_key = c.user_key
where app_id='fruitscoot.global.prod'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20;

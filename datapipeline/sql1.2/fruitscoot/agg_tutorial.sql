delete from processed.agg_tutorial
where install_date >=(select start_date from processed.tmp_start_date);

insert into processed.agg_tutorial
SELECT
	d.app_id
	,d.app_version
	,t.level
	,u.install_date
	,u.install_source
	,d.country
	,t.os
	,t.os_version
	,d.device
	,t.browser
	,t.browser_version
	,d.language
	,t.tutorial_step
	,t.tutorial_step_desc
	,case when s.tutorial_step is null then 1 else 0 end as is_required
	,count(distinct t.user_key) as user_cnt
FROM processed.fact_tutorial t
join processed.fact_dau_snapshot d on d.user_key=t.user_key and t.date=d.date
join processed.dim_user u ON u.user_key=t.user_key
left join processed.dim_tutorial_required_step s on s.tutorial_step=t.tutorial_step
where
    u.install_date >=(select start_date from processed.tmp_start_date) and
    (t.tutorial_step!=1300 or (t.tutorial_step=1300 and t.tutorial_step_desc='14_Boy_11_Next'))
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
union
SELECT
	d.app_id
	,d.app_version
	,1 as level
	,u.install_date
	,u.install_source
	,d.country
	,d.os
	,d.os_version
	,d.device
	,d.browser
	,d.browser_version
	,d.language
	,0 as tutorial_step
	,'New Users' as tutorial_step_desc
	,1 as is_required
	,count(u.user_key) as user_cnt
FROM processed.dim_user u
join processed.fact_dau_snapshot d on d.user_key=u.user_key and u.install_date=d.date
where
    u.install_date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;


--------- agg_tutorial_ab
create temp table fb_connected as 
select user_key,max(is_facebook_connected) as is_facebook_connected from processed.fact_dau_snapshot group by user_key;

create temp table app_version as
select d.user_key,d.app_version,u.install_date from processed.fact_dau_snapshot d left join processed.dim_user u on d.user_key=u.user_key where u.install_date=d.date;

delete from processed.agg_tutorial_ab
where install_date >=(select start_date from processed.tmp_start_date);

insert into processed.agg_tutorial_ab
SELECT
	d.app_id
	,a.app_version
	,t.level
	,u.install_date
	,u.install_source
	,c.ab_experiment
	,c.ab_variant
	,d.country
	,t.os
	,t.os_version
	,d.device
	,t.browser
	,t.browser_version
	,d.language
	,u.is_payer
	,f.is_facebook_connected
	,t.tutorial_step
	,t.tutorial_step_desc
	,case when s.tutorial_step is null then 1 else 0 end as is_required
	,count(distinct t.user_key) as user_cnt
	,t.bundle
FROM processed.fact_tutorial t
join processed.fact_dau_snapshot d on d.user_key=t.user_key and t.date=d.date
left join app_version a on t.user_key=a.user_key
join processed.dim_user u ON u.user_key=t.user_key
left join processed.dim_tutorial_required_step s on s.tutorial_step=t.tutorial_step
left join fb_connected f on t.user_key=f.user_key
left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on t.user_key = c.user_key
where
    u.install_date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21
union
SELECT
	d.app_id
	,a.app_version
	,1 as level
	,u.install_date
	,u.install_source
	,c.ab_experiment
	,c.ab_variant
	,d.country
	,d.os
	,d.os_version
	,d.device
	,d.browser
	,d.browser_version
	,d.language
	,u.is_payer
	,f.is_facebook_connected
	,0 as tutorial_step
	,'New Users' as tutorial_step_desc
	,1 as is_required
	,count(u.user_key) as user_cnt
	,'New Users' as bundle
FROM processed.dim_user u
join processed.fact_dau_snapshot d on d.user_key=u.user_key and u.install_date=d.date
left join app_version a on u.user_key=a.user_key
left join fb_connected f on u.user_key=f.user_key
left join 
      (
         (SELECT user_key,
                 ab_experiment,
                 ab_variant
          FROM
            (SELECT *,
                    row_number() over (partition BY user_key, ab_experiment
                                       ORDER BY date DESC) AS rank
            FROM processed.fact_mission_abtest)
            WHERE rank=1)
       UNION
         (SELECT DISTINCT user_key,
                          NULL,
                          NULL
          FROM processed.fact_mission_abtest)
      ) c
      on u.user_key = c.user_key
where
    u.install_date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21;



delete from processed.agg_tutorial_ab where install_date>current_date;
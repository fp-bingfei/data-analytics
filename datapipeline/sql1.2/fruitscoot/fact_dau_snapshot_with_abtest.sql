
SELECT DISTINCT
    id,
    user_key,
    date,
    app_id,
    app_version,
    level_start,
    level_end,
    os,
    os_version,
    device,
    browser,
    browser_version,
    country_code,
    country,
    language,
    is_new_user,
    resource_amount,
    received_resource_amount,
    is_payer,
    is_converted_today,
    revenue_usd,
    payment_cnt,
    session_cnt,
    playtime_sec,
    is_facebook_connected,
    is_facebook_connected_today,
    ab_experiment,
    ab_variant
FROM (
    SELECT
        d.id,
        d.user_key,
        d.date,
        d.app_id,
        d.app_version,
        d.level_start,
        d.level_end,
        d.os,
        d.os_version,
        d.device,
        d.browser,
        d.browser_version,
        d.country_code,
        d.country,
        d.language,
        d.is_new_user,
        d.resource_amount,
        d.received_resource_amount,
        d.is_payer,
        d.is_converted_today,
        d.revenue_usd,
        d.payment_cnt,
        d.session_cnt,
        d.playtime_sec,
        d.is_facebook_connected,
        d.is_facebook_connected_today,
        a.ab_experiment,
        a.ab_variant 
    FROM 
        processed.fact_dau_snapshot d
    LEFT JOIN
        (SELECT * FROM processed.fact_mission_abtest WHERE date > '2015-05-20') a
    ON
        d.user_key = a.user_key
    WHERE
        d.date = '2015-05-25'
    ) t;
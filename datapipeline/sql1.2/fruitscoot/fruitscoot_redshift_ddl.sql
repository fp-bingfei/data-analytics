CREATE table processed.fact_session(
  id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
  app_id varchar(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE BYTEDICT,
  user_key varchar(100) NOT NULL ENCODE LZO,
  app_user_id varchar(64) NOT NULL ENCODE LZO,
  date_start date NOT NULL ENCODE DELTA,
  date_end date ENCODE DELTA,
  ts_start timestamp ENCODE DELTA,
  ts_end timestamp ENCODE DELTA,
  install_ts timestamp ENCODE DELTA,
  install_date date ENCODE DELTA,
  session_id varchar(50) ENCODE LZO,
  facebook_id varchar(50) ENCODE LZO,
  install_source varchar(1024) ENCODE BYTEDICT,
  os varchar(30) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  browser varchar(32) ENCODE BYTEDICT,
  browser_version varchar(64) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  country_code varchar(16) ENCODE BYTEDICT,
  level_start int,
  level_end int,
  gender varchar(16) ENCODE BYTEDICT,
  birthday date,
  email varchar(256) ENCODE BYTEDICT,
  ip varchar(64) ENCODE BYTEDICT,
  language varchar(32) ENCODE BYTEDICT,
  locale varchar(32) ENCODE BYTEDICT,
  life_wallet bigint,,
  ab_experiment varchar(1024) ENCODE BYTEDICT,
  ab_variant varchar(1024) ENCODE BYTEDICT,
  session_length_sec int,
  idfa varchar(128),
  idfv varchar(128),
  gaid varchar(128),
  mac_address varchar(128),
  android_id varchar(128))
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, app_id, app_user_id);
  


CREATE TABLE processed.fact_revenue(
  id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
  app_id varchar(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(32) ENCODE BYTEDICT,
  user_key varchar(100) NOT NULL ENCODE LZO,
  app_user_id varchar(64) ENCODE LZO,
  date date NOT NULL ENCODE DELTA, 
  ts timestamp ENCODE DELTA,
  install_ts timestamp ENCODE DELTA,
  install_date date ENCODE DELTA,
  session_id varchar(100) ENCODE LZO,
  level int,
  os varchar(50) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  browser varchar(32) ENCODE BYTEDICT,
  browser_version varchar(64) ENCODE BYTEDICT,
  country_code varchar(16) ENCODE BYTEDICT,
  install_source varchar(1024) ENCODE BYTEDICT,
  ip varchar(32) ENCODE BYTEDICT,
  language varchar(32) ENCODE BYTEDICT,
  locale varchar(32) ENCODE BYTEDICT,
  ab_experiment varchar(1024) ENCODE BYTEDICT,
  ab_variant varchar(1024) ENCODE BYTEDICT,
  payment_processor varchar(64) ENCODE BYTEDICT,
  product_id varchar(100) ENCODE BYTEDICT,
  product_name varchar(100) ENCODE BYTEDICT,
  product_type varchar(100) ENCODE BYTEDICT,
  currency varchar(20) ENCODE BYTEDICT,
  revenue_currency numeric(14,4),
  revenue_usd numeric(14,4),
  transaction_id varchar(256) ENCODE BYTEDICT,
  idfa varchar(128) ENCODE BYTEDICT,
  idfv varchar(128) ENCODE BYTEDICT,
  gaid varchar(128) ENCODE BYTEDICT,
  mac_address varchar(128) ENCODE BYTEDICT,
  android_id varchar(128) ENCODE BYTEDICT,
  life_wallet int
  )DISTKEY(user_key)
  SORTKEY(date, user_key, app_id, app_user_id)

;


CREATE TABLE processed.fact_levelup(
  id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
  user_key varchar(64) NOT NULL ENCODE LZO,
  app_id varchar(64) NOT NULL ENCODE BYTEDICT,
  app_user_id varchar(64) ENCODE LZO,
  session_id varchar(50) ENCODE LZO,
  previous_level smallint,
  previous_levelup_date date ENCODE DELTA,
  previous_levelup_ts timestamp ENCODE DELTA,
  current_level smallint,
  levelup_date date ENCODE DELTA,
  levelup_ts timestamp ENCODE DELTA,
  browser varchar(32) ENCODE BYTEDICT,
  browser_version varchar(64) ENCODE BYTEDICT,
  os varchar(32) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(64) ENCODE BYTEDICT,
  country_code varchar(16) ENCODE BYTEDICT,
  ip varchar(32) ENCODE BYTEDICT,
  language varchar(32) ENCODE BYTEDICT,
  life_wallet numeric(14,4),
  locale varchar(32) ENCODE BYTEDICT,
  ab_experiment varchar(1024) ENCODE BYTEDICT,
  ab_variant varchar(1024) ENCODE BYTEDICT
  )
  distkey(user_key)
  sortkey(user_key,current_level,levelup_date);


CREATE TABLE processed.fact_dau_snapshot (
   id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   user_key varchar(64) NOT NULL ENCODE LZO,
   date date NOT NULL ENCODE DELTA,
   app_id varchar(64) ENCODE BYTEDICT,
   app_version varchar(20) ENCODE BYTEDICT,
   level_start  int,
   level_end  int,
   os varchar(32) ENCODE BYTEDICT,
   os_version varchar(100) ENCODE BYTEDICT,
   device varchar(64) ENCODE BYTEDICT,
   browser varchar(32) ENCODE BYTEDICT,
   browser_version varchar(64) ENCODE BYTEDICT,
   country_code varchar(16) ENCODE BYTEDICT,
   country varchar(100) ENCODE BYTEDICT,
   language varchar(8) ENCODE BYTEDICT,
   ab_experiment varchar(1024) ENCODE BYTEDICT,
   ab_variant varchar(1024) ENCODE BYTEDICT,
   is_new_user  smallint,
   resource_amount numeric(14,4),
   received_resource_amount numeric(14,4),
   is_payer  smallint,
   is_converted_today  smallint,
   revenue_usd  numeric(14,4),
   payment_cnt  int,
   session_cnt  int,
   playtime_sec  int,
   is_facebook_connected int,
   is_facebook_connected_today int)
   DISTKEY(user_key)
   SORTKEY(date, user_key, app_id, country_code);


CREATE  TABLE  processed.dim_user (
   id   varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   user_key   varchar(64) NOT NULL ENCODE LZO,
   app_id   varchar(64) NOT NULL ENCODE BYTEDICT,
   app_user_id   varchar(64) ENCODE LZO,
   facebook_id   varchar(50) ENCODE BYTEDICT,
   install_ts  timestamp ENCODE DELTA,
   install_date   date ENCODE DELTA,
   install_source   varchar(1024) ENCODE BYTEDICT,
   install_subpublisher   varchar(500) ENCODE BYTEDICT,
   install_campaign   varchar(128) ENCODE BYTEDICT,
   install_language   varchar(8) ENCODE BYTEDICT,
   install_locale   varchar(8) ENCODE BYTEDICT,
   install_country_code   varchar(16) ENCODE BYTEDICT,
   install_country   varchar(100) ENCODE BYTEDICT,
   install_os   varchar(32) ENCODE BYTEDICT,
   install_device   varchar(64) ENCODE BYTEDICT,
   install_device_alias   varchar(64) ENCODE BYTEDICT,
   install_browser   varchar(32) ENCODE BYTEDICT,
   install_gender   varchar(16) ENCODE BYTEDICT,
   install_age   varchar(20) ENCODE BYTEDICT,
   language   varchar(8) ENCODE BYTEDICT,
   locale   varchar(8) ENCODE BYTEDICT,
   birthday   date ENCODE DELTA,
   gender   varchar(16) ENCODE BYTEDICT,
   country_code   varchar(16) ENCODE BYTEDICT,
   country   varchar(100) ENCODE BYTEDICT,
   os   varchar(32) ENCODE BYTEDICT,
   os_version   varchar(100) ENCODE BYTEDICT,
   device   varchar(64) ENCODE BYTEDICT,
   device_alias   varchar(64) ENCODE BYTEDICT,
   browser   varchar(32) ENCODE BYTEDICT,
   browser_version   varchar(64) ENCODE BYTEDICT,
   app_version   varchar(20) ENCODE BYTEDICT,
   level  int,
   levelup_ts  timestamp ENCODE DELTA,
   ab_experiment   varchar(1024) ENCODE BYTEDICT,
   ab_variant   varchar(1024) ENCODE BYTEDICT,
   is_payer  int,
   conversion_ts  timestamp ENCODE DELTA,
   total_revenue_usd  numeric(14,4),
   payment_cnt  int,
   last_login_ts  timestamp,
   email   varchar(500) ENCODE BYTEDICT,
   last_ip   varchar(64) ENCODE BYTEDICT
)
   DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);





create table processed.agg_kpi
(
    id varchar(100) primary key ENCODE BYTEDICT
	,date date ENCODE DELTA
	,app_id varchar(100) ENCODE BYTEDICT
	,app_version varchar(50) ENCODE BYTEDICT
	,install_year  varchar(100) ENCODE BYTEDICT
	,install_month varchar(100) ENCODE BYTEDICT
	,install_week varchar(100) ENCODE BYTEDICT
	,install_source varchar(1024) ENCODE BYTEDICT
	,level_end int ENCODE BYTEDICT
	,browser varchar(100) ENCODE BYTEDICT
	,browser_version varchar(100) ENCODE BYTEDICT
	,device_alias varchar(100) ENCODE BYTEDICT
	,country varchar(100) ENCODE BYTEDICT
	,os varchar(100) ENCODE BYTEDICT
	,os_version varchar(100) ENCODE BYTEDICT
	,language varchar(32) ENCODE BYTEDICT
	,ab_experiment varchar(128) ENCODE BYTEDICT
	,ab_variant varchar(128) ENCODE BYTEDICT
	,is_new_user int
	,is_payer int
	,new_user_cnt int
	,dau_cnt int
	,newpayer_cnt int
	,payer_today_cnt int
	,payment_cnt int
	,revenue_usd numeric(14,4)
	,session_cnt int
    ,session_length_sec bigint
);



create table processed.agg_iap
( date date ENCODE DELTA,
app_id varchar(100) ENCODE BYTEDICT,
app_version varchar(50) ENCODE BYTEDICT,
level int,
install_date date ENCODE DELTA,
install_source varchar(1024) ENCODE BYTEDICT,
country varchar(100) ENCODE BYTEDICT,
os varchar(50) ENCODE BYTEDICT,
browser varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
language varchar(32) ENCODE BYTEDICT,
is_conversion_purchase int,
product_id varchar(128) ENCODE BYTEDICT,
product_type varchar(128) ENCODE BYTEDICT,
revenue_usd numeric(14,4),
purchase_cnt bigint,
purchase_user_cnt bigint
);


create table processed.tab_marketing_kpi
(
app_id varchar(100) ENCODE BYTEDICT,
app_version varchar(50) ENCODE BYTEDICT,
install_date date ENCODE DELTA,
install_source varchar(1024) ENCODE BYTEDICT, 
install_campaign varchar(1024) ENCODE BYTEDICT, 
install_subpublisher varchar(1024) ENCODE BYTEDICT, 
install_country varchar(100) ENCODE BYTEDICT, 
install_os varchar(100) ENCODE BYTEDICT, 
new_user_cnt int,
d1_new_user_cnt int ,
d3_new_user_cnt int,
d7_new_user_cnt int,
d15_new_user_cnt int,
d30_new_user_cnt int,
d90_new_user_cnt int,
d120_new_user_cnt int,
revenue_usd numeric(14,4), 
d7_revenue_usd numeric(14,4) , 
d30_revenue_usd numeric(14,4), 
d90_revenue_usd numeric(14,4), 
d120_revenue_usd numeric(14,4),
payer_cnt int ,
d3_payer_cnt int, 
d1_retained_user_cnt int, 
d7_retained_user_cnt int, 
d15_retained_user_cnt int, 
d30_retained_user_cnt int
 );
 

create table processed.fact_tutorial
(id	     varchar(100) ENCODE BYTEDICT,
app_id	 varchar(64) ENCODE BYTEDICT,
app_version	varchar(32) ENCODE BYTEDICT,
user_key	 varchar(100) ENCODE BYTEDICT,
app_user_id	varchar(100) ENCODE BYTEDICT,
date	        date ENCODE DELTA,
ts	       timestamp,
session_id	varchar(128) ENCODE BYTEDICT,
os	        varchar(64) ENCODE BYTEDICT,
os_version    varchar(100) ENCODE BYTEDICT,
device varchar(100) ENCODE BYTEDICT,
browser	  varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
country_code	varchar(16) ENCODE BYTEDICT,
ip	        varchar(100) ENCODE BYTEDICT,
language varchar(32) ENCODE BYTEDICT,
locale	    varchar(32) ENCODE BYTEDICT,
life_wallet int,
level int,
tutorial_step	  int,
tutorial_step_desc  varchar(100) ENCODE BYTEDICT
)DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);
;



CREATE  TABLE processed.fact_mission(
  id varchar(100) ENCODE BYTEDICT,
  mission_id varchar(100) ENCODE BYTEDICT,
  app_id varchar(100) ENCODE BYTEDICT,
  app_version varchar(32) ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  app_user_id varchar(100) ENCODE BYTEDICT,
  date date ENCODE DELTA,
  ts timestamp,
  session_id varchar(256) ENCODE BYTEDICT,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  country_code varchar(32) ENCODE BYTEDICT,
  ip varchar(100) ENCODE BYTEDICT,
  language varchar(32) ENCODE BYTEDICT,
  mission_start_ts timestamp,
  mission_status int,
  level int)
DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id)
;

create  table processed.fact_mission_objective
(
id	varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id	varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
objective_id	varchar(100) ENCODE BYTEDICT,
objective_name	varchar(100) ENCODE BYTEDICT,
objective_type	varchar(100) ENCODE BYTEDICT,
objective_amount	int,
objective_amount_remaining	int
);

create  table processed.fact_mission_parameter
(
id	varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id	varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
parameter_name	varchar(100) ENCODE BYTEDICT,
parameter_value	int
)
;



create  table processed.fact_mission_statistic
(
id	varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id	varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
statistic_name	varchar(100) ENCODE BYTEDICT,
statistic_value	int
)
;

create  table processed.fact_player_resources
(
id varchar(100),
app_id varchar(100),
event varchar(32),
date date,
ts timestamp,
resource_id varchar(100),
resource_name varchar(100),
resource_type varchar(100),
resource_amount int
)
distkey(id)
sortkey(app_id,event,date,ts)
;

CREATE TABLE raw_data.raw_adjust_daily
(
  adid  varchar(48),
  userid varchar(32),
  game varchar(32),
  tracker  varchar(32),
  tracker_name varchar(100),
  app_id  varchar(32),
  ip_address  varchar(32),
  idfa  varchar(64),
  android_id varchar(64),
  mac_sha1  varchar(64),
  idfa_md5  varchar(64),
  country  varchar(64),
  timestamp  timestamp,
  mac_md5  varchar(64),
  gps_adid  varchar(64),
  device_name  varchar(64),
  os_name  varchar(32),
  os_version varchar(32) 
)  
distkey(userid)
sortkey(game,app_id,country,tracker_name,userid);

CREATE TABLE if not exists processed.tab_level_churn
(
    app_id varchar(100) ENCODE BYTEDICT,
    app_version varchar(50) ENCODE BYTEDICT,
    level int,
    current_level int,
    install_date date ENCODE DELTA,
    install_source varchar(1024) ENCODE BYTEDICT,
    country varchar(100) ENCODE BYTEDICT,
    os varchar(50) ENCODE BYTEDICT,
    browser varchar(100) ENCODE BYTEDICT,
    language varchar(32) ENCODE BYTEDICT,
    is_payer                  smallint default 0,
    is_churned_1days          smallint default 0,
    is_churned_3days          smallint default 0,
    is_churned_7days          smallint default 0,
    is_churned_14days         smallint default 0,
    is_churned_21days         smallint default 0,
    is_churned_30days         smallint default 0,
    user_cnt                  INTEGER DEFAULT 0
)
DISTKEY(install_date);

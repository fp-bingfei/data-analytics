----------------------------
-- Blacklist
----------------------------
create table if not exists processed.blacklist (
    app_id varchar(50) ENCODE LZO,
    install_source varchar(500) ENCODE LZO,
    sub_publisher varchar(500) ENCODE LZO,
    install_date DATE ENCODE delta,
    ip_address varchar(50) ENCODE LZO,
    country varchar(50) ENCODE LZO,
    device_name varchar(255) ENCODE LZO,
    install_count bigint,
    d1_retention numeric(38,17),
    d7_retention numeric(38,17),
    level1_percent  numeric(38,17),
    discrepancy numeric(38,17),
    device_percent  numeric(38,17)
);

insert into processed.blacklist 
select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,ip_address
    ,country
    ,null as device_name
    ,sum(distinct_fraud_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,cast(null as numeric) as device_percent
from
    processed.adjust_ip_fraud
group by 1,2,3,4,5,6
having sum(distinct_fraud_count) > 20

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,country
    ,device_name
    ,sum(device_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,sum(device_count)*1.00/sum(device_total) as device_percent
from
    processed.adjust_device_fraud
group by 1,2,3,4,5,6,7
having 
    sum(device_count) > 20
    and sum(device_count)*1.00/sum(device_total) > 0.2

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,null as country
    ,null as device_name
    ,install_count
    ,d1_retention
    ,d7_retention
    ,level1_percent
    ,discrepancy
    ,cast(null as numeric) as device_percent
from
    processed.adjust_fraud_monitor
;

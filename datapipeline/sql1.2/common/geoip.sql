-- Create schema
CREATE SCHEMA IF NOT EXISTS geoip;

-- Create table blocks
DROP TABLE IF EXISTS geoip.blocks;
CREATE TABLE  geoip.blocks (
    network VARCHAR(50) NOT NULL,
    network_start_ip VARCHAR(50) NOT NULL,
    network_last_ip VARCHAR(50) NOT NULL,
    network_start_integer BIGINT NOT NULL,
    network_last_integer BIGINT NOT NULL,
    geoname_id INTEGER,
    registered_country_geoname_id INTEGER,
    represented_country_geoname_id INTEGER,
    is_anonymous_proxy INTEGER,
    is_satellite_provider INTEGER,
    PRIMARY KEY (network_start_integer, network_last_integer)
);

-- Create table locations
DROP TABLE IF EXISTS geoip.locations;
CREATE TABLE  geoip.locations (
    geoname_id INTEGER NOT NULL,
    locale_code CHAR(2) NOT NULL,
    continent_code CHAR(2) NOT NULL,
    continent_name VARCHAR(20) NOT NULL,
    country_iso_code CHAR(2),
    country_name VARCHAR(50),
    PRIMARY KEY (geoname_id)
);

-- Import data from S3
COPY geoip.blocks
FROM 's3://com.funplusgame.bidata/dev/geoip2/GeoIP2-Country-CSV_20150127/GeoIP2-Country-Blocks-IPv4_converted.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER ',' IGNOREHEADER 1 CSV;

COPY geoip.locations
FROM 's3://com.funplusgame.bidata/dev/geoip2/GeoIP2-Country-CSV_20150127/GeoIP2-Country-Locations-en.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=FTTVxv9SngsUztHSEuXIdoBODYFRDnHic309+AmI'
DELIMITER ',' IGNOREHEADER 1 CSV;

-- Query Example
SELECT country_iso_code, country_name 
FROM 
    (SELECT 
        geoname_id 
    FROM geoip.blocks 
    WHERE network_start_integer <= (SPLIT_PART('38.88.217.70', '.', 1) * 16777216::bigint + SPLIT_PART('38.88.217.70', '.', 2) * 65536::bigint + SPLIT_PART('38.88.217.70', '.', 3) * 256::bigint + SPLIT_PART('38.88.217.70', '.', 4)::bigint) 
        AND network_last_integer >= (SPLIT_PART('38.88.217.70', '.', 1) * 16777216::bigint + SPLIT_PART('38.88.217.70', '.', 2) * 65536::bigint + SPLIT_PART('38.88.217.70', '.', 3) * 256::bigint + SPLIT_PART('38.88.217.70', '.', 4)::bigint) 
    ) AS t 
    LEFT JOIN geoip.locations AS l 
    ON t.geoname_id = l.geoname_id;

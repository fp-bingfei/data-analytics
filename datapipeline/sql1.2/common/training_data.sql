----------------------------
-- Training Data
----------------------------
create table if not exists processed.training_data (
    user_key varchar(50) ENCODE LZO,
    snsid varchar(64) ENCODE LZO,
    app_id varchar(64) ENCODE LZO,
    install_source varchar(500) ENCODE LZO,
    sub_publisher varchar(500) ENCODE LZO,
    install_date DATE ENCODE delta,
    ip_address varchar(50) ENCODE LZO,
    country varchar(50) ENCODE LZO,
    device_name varchar(255) ENCODE LZO,
    os varchar(30) ENCODE LZO,
    os_version varchar(100) ENCODE LZO,
    level_end smallint,
    session_cnt integer,
    purchase_cnt integer,
    active_days integer,
    flag smallint
);

insert into processed.training_data 
select
    t1.user_key
    ,t1.snsid
    ,t1.app_id
    ,t1.install_source
    ,t1.sub_publisher
    ,t1.install_date
    ,t1.ip_address
    ,t1.country
    ,t1.device_name
    ,t1.os
    ,t1.os_version
    ,t1.level_end
    ,t1.session_cnt
    ,t1.purchase_cnt
    ,t1.active_days
    ,max(case
        when t1.level_end = 1 and t1.active_days >= 5 then 1
        when t1.ip_address = t2.ip_address then 1
        when t1.os = 'Android' and t1.device_name = t2.device_name then 1
        when lower(t1.install_source) not like '%incent%' and t2.d1_retention < 0.2 and t2.level1_percent > 0.2 and t2.discrepancy > 0.2 then 1
        else 0 
     end) as flag
from
    (
    select
        u.user_key
        ,u.snsid
        ,a.game as app_id
        ,u.install_source
        ,u.sub_publisher
        ,trunc(a.ts) as install_date
        ,a.ip_address
        ,u.country
        ,a.device_name
        ,u.os
        ,u.os_version
        ,avg(s.level_end) as level_end
        ,avg(s.session_cnt) as session_cnt
        ,sum(s.purchase_cnt) as purchase_cnt
        ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    from
        public.unique_adjust a
        left join 
        processed.dim_user u on a.userid = u.snsid and a.game = u.app
        left join
        processed.fact_dau_snapshot s on a.userid = s.snsid and a.game = s.app
    where 
        a.tracker_name <> 'Organic'
        and date(a.ts) >= DATEADD(DAY, -15, current_date)
        and date(a.ts) < DATEADD(DAY, -1, current_date)
        and a.userid != ''
        and u.snsid is not null
    group by 1,2,3,4,5,6,7,8,9,10,11
    ) t1
    left join
    (
    select
        *
    from
        processed.blacklist b
    where
        b.install_date < DATEADD(DAY, -1, current_date)
    ) t2
    on
        t1.app_id = t2.app_id
        and t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
;

DROP TABLE IF EXISTS processed.tmp_user_level;

CREATE TABLE processed.tmp_user_level AS
SELECT
    levelup_date
    ,user_key
    ,app_id
    ,max(levelup_ts)        AS levelup_ts
    ,min(previous_level)    AS level_start
    ,max(current_level)     AS level_end
FROM processed.fact_levelup
WHERE levelup_date >= (select start_date from processed.tmp_start_date)
  AND current_level != 0
GROUP BY 1,2,3
;
delete from processed.agg_kpi
where date >=(select start_date from processed.tmp_start_date);

insert into processed.agg_kpi
(
	id
	,date 
	,app_id
	,app_version
	,install_year 
	,install_month
	,install_week
	,install_source
	,level_end
	,browser
	,browser_version
	,device_alias
	,device_resolution 
          ,network 
          ,location 
          ,carrier 
          ,device_storage 
	,country
	,os
	,os_version
	,language
	,ab_experiment
	,ab_variant
	,is_new_user
	,is_payer
	,new_user_cnt
	,dau_cnt
	,newpayer_cnt
	,payer_today_cnt
	,payment_cnt
	,revenue_usd
	,session_cnt
        ,session_length_sec
)
select
      MD5(d.date||COALESCE(d.app_id,'')||COALESCE(d.app_version,'')||EXTRACT(year FROM u.install_date)||
      EXTRACT(month FROM u.install_date)||EXTRACT(week FROM u.install_date) ||COALESCE(u.install_source,'')||
      COALESCE(level_end,0)||COALESCE(d.browser,'')||COALESCE(d.browser_version,'')||COALESCE(d.device,'')||
      COALESCE(d.country,'')||COALESCE(d.os,'')||COALESCE(d.os_version,'')||COALESCE(d.language,'')||
      COALESCE(d.is_new_user,0)||COALESCE(d.is_payer,0)) as id
      ,d.date
      ,d.app_id
      ,d.app_version
      ,EXTRACT(year FROM u.install_date) as install_year
      ,EXTRACT(month FROM u.install_date) as install_month
      ,EXTRACT(week FROM u.install_date) as install_week
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.browser_version
      ,d.device
      ,d.device_resolution 
      ,d.network 
      ,d.location 
      ,d.carrier 
      ,d.device_storage 
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.ab_experiment
      ,d.ab_variant
      ,d.is_new_user
      ,d.is_payer
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as session_length_sec
from processed.fact_dau_snapshot d
join processed.dim_user u on d.user_key=u.user_key
where date >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25;



----------------------
--video action by dau
----------------------
drop table if exists processed.agg_video_action_by_dau;
create table processed.agg_video_action_by_dau
as
select  v.app_id
       ,v.date
       ,v.action 
       ,v.user_cnt
       ,v.action_cnt
       ,coalesce(c.country,'Unknown') as country
       ,v.os
from processed.agg_video_action v
left join processed.dim_country c
on v.country_code = c.country_code
;


--------------------
--video action detail
--------------------

drop table if exists processed.agg_video_action_detail_report;
create table processed.agg_video_action_detail_report
as
select  t1.app_id 
       ,t1.date DATE
       ,coalesce(t2.country,'Unknown') as country
       ,t1.device_resolution 
       ,t1.network 
       ,t1.location 
       ,t1.carrier 
       ,t1.device_storage 
       ,t1.os 
       ,t1.device 
       ,t1.action 
       ,t1.action_detail 
       ,case when t1.action_detail = 'size'
        then 
         (case when to_number(t1.attribute,'9999D999') >=0   and to_number(t1.attribute,'9999D999') <=0.5 then '[0,0.5]'
               when to_number(t1.attribute,'9999D999') >0.5 and to_number(t1.attribute,'9999D999') <=1 then '(0.5,1]'
               when to_number(t1.attribute,'9999D999') >1   and to_number(t1.attribute,'9999D999') <=1.5 then '(1,1.5]'
               when to_number(t1.attribute,'9999D999') >1.5 and to_number(t1.attribute,'9999D999') <=2 then '(1.5,2]'
               when to_number(t1.attribute,'9999D999') >2   and to_number(t1.attribute,'9999D999') <=3 then '(2,3]'
               when to_number(t1.attribute,'9999D999') >3 then '(3,]'
           else t1.attribute end)
        else t1.attribute
        end as attribute
       ,t1.attribute_cnt
from processed.agg_video_action_detail t1
left join processed.dim_country t2
on t1.country_code=t2.country_code;

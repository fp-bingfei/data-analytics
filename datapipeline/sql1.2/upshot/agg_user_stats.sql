
-----------------------------------
--agg user status
-----------------------------------
delete from  processed.agg_user_stats
where date = '${date}';

insert into processed.agg_user_stats
select '${date}'
      ,count(case when date = '${date}' then d.user_key else null end) as dau
      ,count(distinct case when date between '${date}'-7 and '${date}' then d.user_key else null end) as wau
      ,count(distinct case when date between '${date}'-30 and '${date}' then d.user_key else null end) as mau
from  processed.fact_dau_snapshot d
join processed.dim_user u 
on d.user_key=u.user_key
and date >= '${date}' -30;
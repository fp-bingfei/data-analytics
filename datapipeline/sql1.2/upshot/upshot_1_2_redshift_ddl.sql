CREATE TABLE processed.fact_revenue
(
	id VARCHAR(100) NOT NULL ENCODE bytedict,
	app_id VARCHAR(100) NOT NULL ENCODE bytedict,
	app_version VARCHAR(64) ENCODE bytedict,
	user_key VARCHAR(100) ENCODE bytedict DISTKEY,
	app_user_id VARCHAR(100) NOT NULL ENCODE bytedict,
	date DATE ENCODE delta,
	ts TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	session_id VARCHAR(256) ENCODE bytedict,
	level INTEGER,
	os VARCHAR(100) ENCODE bytedict,
	os_version VARCHAR(100) ENCODE bytedict,
	device VARCHAR(100) ENCODE bytedict,
	device_resolution varchar(300) ENCODE BYTEDICT,
          network varchar(300) ENCODE BYTEDICT,
          location varchar(1000) ENCODE BYTEDICT,
          carrier varchar(300) ENCODE BYTEDICT,
          device_storage varchar(300) ENCODE BYTEDICT,
	browser VARCHAR(100) ENCODE bytedict,
	browser_version VARCHAR(100) ENCODE bytedict,
	country_code VARCHAR(10) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	ip VARCHAR(100) ENCODE bytedict,
	language VARCHAR(16) ENCODE bytedict,
	locale VARCHAR(16) ENCODE bytedict,
	ab_experiment VARCHAR(256) ENCODE bytedict,
	ab_variant VARCHAR(256) ENCODE bytedict,
	coin_wallet BIGINT,
	rc_wallet INTEGER,
	payment_processor VARCHAR(128) ENCODE bytedict,
	product_id VARCHAR(100) ENCODE bytedict,
	product_name VARCHAR(100) ENCODE bytedict,
	product_type VARCHAR(100) ENCODE bytedict,
	coins_in BIGINT,
	rc_in INTEGER,
	currency VARCHAR(32) ENCODE bytedict,
	revenue_amount NUMERIC(15, 4),
	revenue_usd NUMERIC(15, 4),
	transaction_id VARCHAR(156) ENCODE bytedict,
	idfa VARCHAR(200) ENCODE bytedict,
	idfv VARCHAR(200) ENCODE bytedict,
	gaid VARCHAR(200) ENCODE bytedict,
	mac_address VARCHAR(200) ENCODE bytedict,
	android_id VARCHAR(200) ENCODE bytedict
)
SORTKEY
(
	date,
	ts,
	user_key,
	app_user_id,
	os,
	country_code
);


CREATE TABLE processed.fact_session
(
	id VARCHAR(100) NOT NULL ENCODE bytedict,
	app_id VARCHAR(100) NOT NULL ENCODE bytedict,
	app_version VARCHAR(64) ENCODE bytedict,
	user_key VARCHAR(100) ENCODE bytedict DISTKEY,
	app_user_id VARCHAR(100) NOT NULL ENCODE bytedict,
	date_start DATE ENCODE delta,
	date_end DATE ENCODE delta,
	ts_start TIMESTAMP ENCODE delta,
	ts_end TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	session_id VARCHAR(156) ENCODE bytedict,
	facebook_id VARCHAR(128) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	os VARCHAR(100) ENCODE bytedict,
	os_version VARCHAR(100) ENCODE bytedict,
	browser VARCHAR(100) ENCODE bytedict,
	browser_version VARCHAR(100) ENCODE bytedict,
	device VARCHAR(100) ENCODE bytedict,
	device_resolution varchar(300) ENCODE BYTEDICT,
          network varchar(300) ENCODE BYTEDICT,
          location varchar(1000) ENCODE BYTEDICT,
          carrier varchar(300) ENCODE BYTEDICT,
          device_storage varchar(300) ENCODE BYTEDICT,
	country_code VARCHAR(10) ENCODE bytedict,
	level_start INTEGER,
	level_end INTEGER,
	first_name VARCHAR(200) ENCODE bytedict,
	gender VARCHAR(32) ENCODE bytedict,
	birthday DATE ENCODE delta,
	email VARCHAR(500) ENCODE bytedict,
	ip VARCHAR(100) ENCODE bytedict,
	language VARCHAR(16) ENCODE bytedict,
	locale VARCHAR(16) ENCODE bytedict,
	rc_wallet_start INTEGER,
	rc_wallet_end INTEGER,
	coin_wallet_start BIGINT,
	coin_wallet_end BIGINT,
	ab_experiment VARCHAR(256) ENCODE bytedict,
	ab_variant VARCHAR(256) ENCODE bytedict,
	session_length_sec BIGINT,
	idfa VARCHAR(156) ENCODE bytedict,
	idfv VARCHAR(156) ENCODE bytedict,
	gaid VARCHAR(156) ENCODE bytedict,
	mac_address VARCHAR(156) ENCODE bytedict,
	android_id VARCHAR(156) ENCODE bytedict
)
SORTKEY
(
	user_key,
	date_start,
	app_user_id,
	app_id
);



CREATE  TABLE processed.fact_dau_snapshot(
  id varchar(100) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  date date NOT NULL ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT,
  app_version varchar(100) ENCODE BYTEDICT,
  level_start int,
  level_end int,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  device_resolution varchar(300) ENCODE BYTEDICT,
  network varchar(300) ENCODE BYTEDICT,
  location varchar(1000) ENCODE BYTEDICT,
  carrier varchar(300) ENCODE BYTEDICT,
  device_storage varchar(300) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  country_code varchar(10) ENCODE BYTEDICT,
  country varchar(100) ENCODE BYTEDICT,
  language varchar(16) ENCODE BYTEDICT,
  ab_experiment varchar(256) ENCODE BYTEDICT,
  ab_variant varchar(256) ENCODE BYTEDICT,
  is_new_user int,
  rc_in int,
  coins_in bigint,
  rc_out int,
  coins_out bigint,
  rc_wallet int,
  coin_wallet bigint,
  is_payer int,
  is_converted_today int,
  revenue_usd numeric(15,4),
  payment_cnt int,
  session_cnt int,
  playtime_sec int)
DISTKEY(user_key)
   SORTKEY(date, user_key, app_id, country_code);



CREATE  TABLE  processed.dim_user
(
  id  varchar(100) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT,
  app_user_id  varchar(100) NOT NULL ENCODE BYTEDICT,
  snsid varchar(100) ENCODE BYTEDICT,
  facebook_id varchar(200) ENCODE BYTEDICT,
  install_ts  TIMESTAMP ENCODE DELTA,
  install_date  date ENCODE DELTA,
  install_source  varchar(1024) ENCODE BYTEDICT,
  install_subpublisher  varchar(1024) ENCODE BYTEDICT,
  install_campaign  varchar(500) ENCODE BYTEDICT,
  install_language  varchar(16) ENCODE BYTEDICT,
  install_locale  varchar(32) ENCODE BYTEDICT,
  install_country_code  varchar(16) ENCODE BYTEDICT,
  install_country  varchar(100) ENCODE BYTEDICT,
  install_os  varchar(100) ENCODE BYTEDICT,
  install_device  varchar(100) ENCODE BYTEDICT,
  install_device_alias  varchar(100) ENCODE BYTEDICT,
  install_device_resolution varchar(300) ENCODE BYTEDICT,
  install_network varchar(300) ENCODE BYTEDICT,
  install_location varchar(1000) ENCODE BYTEDICT,
  install_carrier varchar(300) ENCODE BYTEDICT,
  install_device_storage varchar(300) ENCODE BYTEDICT,
  install_browser  varchar(100) ENCODE BYTEDICT,
  install_gender  varchar(32) ENCODE BYTEDICT,
  install_age  varchar(10) ENCODE BYTEDICT,
  language  varchar(16) ENCODE BYTEDICT,
  locale varchar(32) ENCODE BYTEDICT,
  birthday  date ENCODE DELTA,
  gender varchar(32) ENCODE BYTEDICT,
  country_code  varchar(16) ENCODE BYTEDICT,
  country  varchar(100) ENCODE BYTEDICT,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device  varchar(100) ENCODE BYTEDICT,
  device_alias  varchar(100) ENCODE BYTEDICT,
  device_resolution varchar(300) ENCODE BYTEDICT,
  network varchar(300) ENCODE BYTEDICT,
  location varchar(1000) ENCODE BYTEDICT,
  carrier varchar(300) ENCODE BYTEDICT,
  device_storage varchar(300) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  app_version varchar(64) ENCODE BYTEDICT,
  level INT,
  levelup_ts TIMESTAMP ENCODE DELTA,
  ab_experiment varchar(256) ENCODE BYTEDICT,
  ab_variant varchar(256) ENCODE BYTEDICT,
  is_payer INT ,
  conversion_ts TIMESTAMP ENCODE DELTA,
  total_revenue_usd NUMERIC(15,4) ,
  payment_cnt INT,
  last_login_ts TIMESTAMP ENCODE DELTA,
  email varchar(500) ENCODE BYTEDICT,
  last_ip varchar(100) ENCODE BYTEDICT
)  
DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);

CREATE TABLE processed.tmp_user_daily_login
(
	date DATE ENCODE delta DISTKEY,
	app_id VARCHAR(64) ENCODE bytedict,
	user_key VARCHAR(100) ENCODE bytedict,
	app_user_id VARCHAR(64) ENCODE bytedict,
	snsid VARCHAR(64) ENCODE bytedict,
	last_login_ts TIMESTAMP ENCODE delta,
	facebook_id VARCHAR(50) ENCODE bytedict,
	birthday DATE ENCODE delta,
	email VARCHAR(256) ENCODE bytedict,
	install_ts TIMESTAMP ENCODE delta,
	install_date DATE ENCODE delta,
	app_version VARCHAR(32) ENCODE bytedict,
	level_start INTEGER,
	level_end INTEGER,
	os VARCHAR(32) ENCODE bytedict,
	os_version VARCHAR(100) ENCODE bytedict,
	country_code VARCHAR(16) ENCODE bytedict,
	last_ip VARCHAR(64) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	language VARCHAR(8) ENCODE bytedict,
	locale VARCHAR(8) ENCODE bytedict,
	ab_experiment VARCHAR(128) ENCODE bytedict,
	ab_variant VARCHAR(128) ENCODE bytedict,
	gender VARCHAR(16) ENCODE bytedict,
	device VARCHAR(64) ENCODE bytedict,
	device_resolution varchar(300) ENCODE BYTEDICT,
          network varchar(300) ENCODE BYTEDICT,
          location varchar(1000) ENCODE BYTEDICT,
          carrier varchar(300) ENCODE BYTEDICT,
          device_storage varchar(300) ENCODE BYTEDICT,
	browser VARCHAR(32) ENCODE bytedict,
	browser_version VARCHAR(100) ENCODE bytedict,
	session_cnt INTEGER,
	playtime_sec BIGINT
)
SORTKEY
(
	user_key,
	date,
	install_date
);


CREATE TABLE processed.tmp_user_payment
(
	user_key VARCHAR(100),
	date DATE,
	conversion_ts TIMESTAMP,
	revenue_usd NUMERIC(15, 4),
	total_revenue_usd NUMERIC(15, 4),
	purchase_cnt BIGINT,
	total_purchase_cnt BIGINT
)
DISTSTYLE EVEN;

create table processed.agg_kpi
(
    id varchar(100) primary key ENCODE BYTEDICT
	,date date ENCODE DELTA
	,app_id varchar(100) ENCODE BYTEDICT
	,app_version varchar(50) ENCODE BYTEDICT
	,install_year  varchar(100) ENCODE BYTEDICT
	,install_month varchar(100) ENCODE BYTEDICT
	,install_week varchar(100) ENCODE BYTEDICT
	,install_source varchar(1024) ENCODE BYTEDICT
	,level_end int ENCODE BYTEDICT
	,browser varchar(100) ENCODE BYTEDICT
	,browser_version varchar(100) ENCODE BYTEDICT
	,device_alias varchar(100) ENCODE BYTEDICT
	,device_resolution varchar(300) ENCODE BYTEDICT
          ,network varchar(300) ENCODE BYTEDICT
          ,location varchar(1000) ENCODE BYTEDICT
          ,carrier varchar(300) ENCODE BYTEDICT
          ,device_storage varchar(300) ENCODE BYTEDICT
	,country varchar(100) ENCODE BYTEDICT
	,os varchar(100) ENCODE BYTEDICT
	,os_version varchar(100) ENCODE BYTEDICT
	,language varchar(32) ENCODE BYTEDICT
	,ab_experiment varchar(128) ENCODE BYTEDICT
	,ab_variant varchar(128) ENCODE BYTEDICT
	,is_new_user int 
	,is_payer int
	,new_user_cnt int
	,dau_cnt int
	,newpayer_cnt int
	,payer_today_cnt int
	,payment_cnt int 
	,revenue_usd numeric(14,4)
	,session_cnt int
    ,session_length_sec bigint
);


create table processed.agg_user_stats
(
  date date encode delta,
  dau bigint,
  wau bigint,
  mau bigint
);


create table processed.agg_video_action
(
 app_id varchar(100) ENCODE BYTEDICT
 ,date date ENCODE DELTA
 ,country_code varchar(100) ENCODE BYTEDICT
 ,os varchar(30) ENCODE BYTEDICT
 ,action varchar(100) ENCODE BYTEDICT
 ,user_cnt bigint
 ,action_cnt bigint
);

CREATE TABLE  processed.agg_video_action_detail
( 
    app_id VARCHAR(100)  ENCODE bytedict, 
    date DATE ENCODE bytedict,
    country_code VARCHAR(10) ENCODE bytedict, 
    device_resolution VARCHAR(300) ENCODE bytedict,
    network VARCHAR(300) ENCODE bytedict,
    location VARCHAR(1000) ENCODE bytedict,
    carrier VARCHAR(300) ENCODE bytedict,
    device_storage VARCHAR(300) ENCODE bytedict,
    os VARCHAR(100) ENCODE bytedict,
    device VARCHAR(100) ENCODE bytedict,
    action VARCHAR(100) ENCODE bytedict,
    action_detail VARCHAR(100) ENCODE bytedict,
    attribute VARCHAR(100) ENCODE bytedict,
    attribute_cnt bigint
)
SORTKEY
(
	date,
	app_id,
	country_code
);
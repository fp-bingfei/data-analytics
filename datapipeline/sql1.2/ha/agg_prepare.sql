--start date TABLE
--drop TABLE IF EXISTS processed.tmp_start_date;
--create TABLE processed.tmp_start_date as
--SELECT DATEADD(day, -7, CURRENT_DATE) as start_date;

update processed_new.tmp_start_date
set start_date =  DATEADD(day, -7, CURRENT_DATE);

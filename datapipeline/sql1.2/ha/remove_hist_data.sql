-----------------------------------------------
--before delete and drop ff2pc 10.99T
--after delete and drop ff2pc 6.57T

--remove events_raw data:
delete from events_raw where event ='transaction';
delete from events_raw where event ='marketstand';
delete from events_raw where event ='onlinetime';
delete from events_raw where event ='inventoryUsed';
delete from events_raw where event ='loading';
delete from events_raw where event ='logintimes';
delete from events_raw where event ='order';
delete from events_raw where event ='tutorial';
delete from events_raw where event ='dau';
delete from events_raw where event ='globalEvent';
delete from events_raw where event ='levelup';
delete from events_raw where event ='rank';
delete from events_raw where event ='cashBuy';
delete from events_raw where event ='payment';

vacuum delete only events_raw;
analyze events_raw;


--drop unused table
drop table item_transaction;
drop table rpt_item;
drop table rpt_loading;
drop table rpt_tutorial;

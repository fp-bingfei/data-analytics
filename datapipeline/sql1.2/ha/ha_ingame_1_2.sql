--------------------------------------------------------
--special offer start
--------------------------------------------------------

delete from processed_new.agg_special_offer 
where date >=(select start_date from processed_new.tmp_start_date);
insert into processed_new.agg_special_offer
(
    app_id 
   ,app_version 
   ,date 
   ,os 
   ,os_version 
   ,browser 
   ,browser_version 
   ,country 
   ,language 
   ,level 
   ,package 
   ,schedule 
   ,price 
   ,qualified_players  
   ,shown_players  
   ,unique_clikers  
   ,icon_clicks  
   ,purchased_players  
)

select  f.app_id
       ,f.app_version
       ,f.date
       ,f.os
       ,f.os_version
       ,f.browser
       ,f.browser_version
       ,coalesce(c.country,'Unknown') as country
       ,f.language
       ,f.level
       ,f.package
       ,f.schedule
       ,f.price
       ,count(distinct case when f.action = 'qualify' then f.user_key else null end) as qualified_players
       ,count(distinct case when f.action = 'show' then f.user_key else null end) as shown_players
       ,count(distinct case when f.action = 'click' then f.user_key else null end) as unique_clikers
       ,count(case when f.action = 'click' then 1 else null end) as icon_clicks
       ,count(case when f.action = 'purchase' then user_key else null end) as purchased_players
from   processed_new.fact_special_offer f
left join processed_new.dim_country c
on f.country_code = c.country_code where f.date>=(select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

--------------------------------------------------------
--special offer end
--------------------------------------------------------

--------------------------------------------------------
--load step start
--------------------------------------------------------
drop table if exists processed_new.agg_load_step_report;
create table processed_new.agg_load_step_report
as
select t1.app_id 
      ,t1.date 
      ,t1.load_step 
      ,coalesce(t2.country,'Unkonwn') as country
      ,t1.load_step_desc 
      ,t1.app_version 
      ,t1.level
      ,t1.browser_version 
      ,t1.language 
      ,t1.os 
      ,t1.browser 
      ,t1.os_version 
      ,t1.user_cnt 
from processed_new.agg_load_step t1
left join processed_new.dim_country t2
on t1.country_code = t2.country_code
;

--------------------------------------------------------
--load step end
--------------------------------------------------------

--------------------------------------------------------
--Boardorder start
--------------------------------------------------------
delete from processed_new.boardorder
where date >= (select start_date from processed_new.tmp_start_date);


insert into processed_new.boardorder
select t.app
      ,t.install_source
      ,coalesce(c.country,'Unknown') as country
      ,t.date
      ,t.level
      ,sum(t.coins_get) as coins_get
      ,sum(t.xp_get) as xp_get
      ,sum(case when t.action = 'finish' then 1 else 0 end) as finish_boardorder
      ,sum(case when t.action = 'discard' then 1 else 0 end) as discard_boardorder
      ,sum(case when t.action = 'skip_cooldown' then 1 else 0 end) as skip_cooldown
      ,sum(case when t.action = 'skip_cooldown' then t.rc_spend else 0 end) as rc_spend_skip_cooldown
      ,sum(case when t.action = 'finish' and t.rc_spend >0 then t.rc_spend else 0 end) as finish_by_spend_rc
      ,sum(case when t.action = 'finish' then t.rc_spend else 0 end) as rc_spend_finish_order
from
     ( 
          select app,      
                 install_source,
                 country_code,
                 trunc(ts) date,
                 cast(case when json_extract_path_text(properties,'coins_get')='' then '0' else json_extract_path_text
                 (properties,'coins_get') end as integer) as coins_get,
                 cast(case when json_extract_path_text(properties,'xp_get')='' then '0' else json_extract_path_text(properties,'xp_get') 
                 end as integer) as xp_get,
                 cast(case when json_extract_path_text(properties,'rc_get')='' then '0' else json_extract_path_text(properties,'rc_get') 
                 end as integer) as rc_get,
                 cast(case when json_extract_path_text(properties,'coins_spend')='' then '0' else json_extract_path_text
                 (properties,'coins_spend') end as integer) as coins_spend,
                 cast(case when json_extract_path_text(properties,'xp_spend')='' then '0' else json_extract_path_text
                 (properties,'xp_spend') end as integer) as xp_spend,
                 cast(case when json_extract_path_text(properties,'rc_spend')='' then '0' else json_extract_path_text
                 (properties,'rc_spend') end as integer) as rc_spend,
                 json_extract_path_text(properties,'action') as action,
                 json_extract_path_text(properties,'level') as level,
                 json_extract_path_text(properties,'abtest') as abtest,
                 json_extract_path_text(properties,'locale') as locale
          from events_raw
          where event='boardOrder' 
          and date(ts) >= (
                             select start_date from processed_new.tmp_start_date
                          )
     )t
left join processed_new.dim_country c
on t.country_code = c.country_code
group by 1,2,3,4,5;
--------------------------------------------------------
--Boardorder end
--------------------------------------------------------
delete from processed_new.agg_boatorder
where datediff('day', date, current_date)<=3;

insert into processed_new.agg_boatorder
select * from
(select date, app_id, action_type as action, count(distinct user_key) as u
FROM ff2pc.processed_new.fact_resources_received
where event='transaction'
and action_type like 'BoatOrder_CabinReward%'
and datediff('day', date, current_date)<=3
group by 1,2,3)
union all
(select date, app_id, mission_id as action, count(distinct user_key) as u
from processed_new.fact_mission 
where mission_id='boat_discard'
and datediff('day', date, current_date)<=3
group by 1,2,3);
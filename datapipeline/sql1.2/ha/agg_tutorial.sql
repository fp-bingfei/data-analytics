delete from processed_new.agg_tutorial
where install_date >=(select start_date from processed_new.tmp_start_date);

insert into processed_new.agg_tutorial
SELECT
	d.app_id
	,d.app_version
	,t.level
	,u.install_date
	,u.install_source
	,d.country
	,t.os
	,t.os_version
	,d.device
	,t.browser
	,t.browser_version
	,d.language
	,t.tutorial_step
	,coalesce(t.tutorial_step_desc,cast(tutorial_step as varchar))
	,1 as is_required
	,count(distinct t.user_key) as user_cnt
FROM processed_new.fact_tutorial t
join processed_new.fact_dau_snapshot d on d.user_key=t.user_key and t.date=d.date
join processed_new.dim_user u ON u.user_key=t.user_key
where
    u.install_date >=(select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
union
SELECT
	d.app_id
	,d.app_version
	,1 as level
	,u.install_date
	,u.install_source
	,d.country
	,d.os
	,d.os_version
	,d.device
	,d.browser
	,d.browser_version
	,d.language
	,0 as tutorial_step
	,'New Users' as tutorial_step_desc
	,1 as is_required
	,count(u.user_key) as user_cnt
FROM processed_new.dim_user u
join processed_new.fact_dau_snapshot d on d.user_key=u.user_key and u.install_date=d.date
where
    u.install_date >=(select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;
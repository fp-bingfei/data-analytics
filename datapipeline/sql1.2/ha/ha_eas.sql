-- TODO: For EAS report
create temp table payment_info as
select  app
       ,uid
       ,snsid
       ,min(ts) as conversion_ts
       ,max(ts) as last_payment_ts
       ,count(1) as payment_cnt
from
    (select u.app_id as app
           ,u.app_user_id as uid
           ,p.snsid as snsid
           ,paytime::timestamp as ts
     from tbl_payment p
     join processed_new.dim_user u on u.app_id = case when p.locale = 'th' then 'ha.th.prod'
                                                  else 'ha.us.prod' end
     and p.snsid = u.facebook_id
     union
     select r.app_id as app
           ,r.app_user_id as uid
           ,u.facebook_id as snsid
           ,ts
     from processed_new.fact_revenue r
     join processed_new.dim_user u on r.app_id = u.app_id 
     and r.app_user_id = u.app_user_id
    ) t
group by 1,2,3;
truncate table processed_new.eas_user_info;
insert into processed_new.eas_user_info
select
    'ha.us.prod' as app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,null as additional_email
    ,t.install_source as install_source
    ,t.install_ts as install_ts
    ,u.language as language
    ,u.gender
    ,t.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,t.rc as rc
    ,t.coins as coins
    ,dateadd(second, CAST(t.last_login_ts AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
from tbl_user t
    left join payment_info p on p.app = 'ha.us.prod' and t.uid = p.uid and t.snsid = p.snsid
    left join processed_new.dim_user u on u.app_id = 'ha.us.prod' and t.uid = u.app_user_id and t.snsid = u.facebook_id
union all
select
    'ha.th.prod' as app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,null as additional_email
    ,t.install_source as install_source
    ,t.install_ts as install_ts
    ,u.language as language
    ,u.gender
    ,t.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,t.rc as rc
    ,t.coins as coins
    ,dateadd(second, CAST(t.last_login_ts AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
from tbl_user_th t
    left join payment_info p on p.app = 'ha.th.prod' and t.uid = p.uid and t.snsid = p.snsid
    left join processed_new.dim_user u on u.app_id = 'ha.th.prod' and t.uid = u.app_user_id and t.snsid = u.facebook_id    
;



DELETE FROM processed_new.fact_dau_snapshot
WHERE date >= (select start_date from processed_new.tmp_start_date);


insert into processed_new.fact_dau_snapshot
(
    id
    ,user_key
    ,date
    ,app_id
    ,app_version
    ,level_start
    ,level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country_code
    ,country
    ,language
    ,ab_experiment
    ,ab_variant
    ,is_new_user
    ,is_payer
    ,is_converted_today
    ,revenue_usd
    ,payment_cnt
    ,session_cnt
    ,playtime_sec
)
SELECT
    MD5(s.user_key || s.date) AS id
    ,s.user_key
    ,s.date
    ,s.app_id
    ,s.app_version
    ,COALESCE(s.level_start, l.level_start) AS level_start
    ,COALESCE(s.level_end,l.level_end,s.level_start) AS level_end
    ,lower(s.os)
    ,s.os_version
    ,s.device
    ,lower(s.browser)
    ,s.browser_version
    ,s.country_code
    ,c.country
    ,s.language
    ,s.ab_experiment
    ,s.ab_variant
    ,CASE
        WHEN s.date=s.install_date THEN
            1
        ELSE
            0
    END AS is_new_user
    ,CASE
        WHEN s.date >= trunc(p.conversion_ts) THEN
            1
        ELSE
            0
    END AS is_payer
    ,CASE
        WHEN s.date = trunc(p.conversion_ts) THEN
            1
        ELSE
            0
    END AS is_converted_today
    ,COALESCE(pd.revenue_usd,0) AS revenue_usd
    ,COALESCE(pd.purchase_cnt,0) AS payment_cnt
    ,s.session_cnt
    ,s.playtime_sec
FROM processed_new.tmp_user_daily_login s
LEFT JOIN processed_new.tmp_user_payment pd
    ON  s.user_key=pd.user_key
    AND s.date=pd.date
LEFT JOIN processed_new.tmp_user_level l
    ON  s.user_key=l.user_key
    AND s.date=l.levelup_date
LEFT JOIN processed_new.dim_country c
    ON  c.country_code=s.country_code
LEFT JOIN
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
        FROM processed_new.tmp_user_payment
        GROUP BY 1
    ) AS p
    ON p.user_key=s.user_key
WHERE s.date >= (select start_date from processed_new.tmp_start_date)
;

-- Attempt to backfill missing row on install date

insert into processed_new.fact_dau_snapshot
(
    id
    ,user_key
    ,date
    ,app_id
    ,app_version
    ,level_start
    ,level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country_code
    ,country
    ,language
    ,ab_experiment
    ,ab_variant
    ,is_new_user
    ,is_payer
    ,is_converted_today
    ,revenue_usd
    ,payment_cnt
    ,session_cnt
    ,playtime_sec
)
WITH missing_install_date AS
(
    SELECT DISTINCT
        l.user_key
        ,l.install_date
    FROM processed_new.tmp_user_daily_login l
    LEFT JOIN processed_new.fact_dau_snapshot i
        ON  l.user_key = i.user_key
        AND l.install_date = i.date
    WHERE l.install_date < (select start_date from processed_new.tmp_start_date) 
      AND i.user_key is null
    UNION
    SELECT DISTINCT
        l.user_key
        ,l.install_date
    FROM processed_new.tmp_user_daily_login l
    LEFT JOIN processed_new.tmp_user_daily_login i
        ON  l.user_key = i.user_key
        AND l.install_date = i.date
    WHERE l.install_date >= (select start_date from processed_new.tmp_start_date)
      AND i.user_key is null
)
SELECT
     MD5(user_key || install_date) AS id
    ,user_key
    ,install_date as date
    ,app_id
    ,app_version
    ,1 AS level_start
    ,level_start as level_end
    ,lower(os)
    ,os_version
    ,device
    ,lower(browser)
    ,browser_version
    ,country_code
    ,country
    ,language
    ,ab_experiment
    ,ab_variant
    ,1 AS is_new_user
    ,0 AS is_payer
    ,0 AS is_converted_today
    ,0 AS revenue_usd
    ,0 AS purchase_cnt
    ,1 AS session_cnt
    ,NULL AS playtime_sec
FROM
    (
    SELECT
        d.*
        ,m.install_date
        ,row_number() over (partition by d.user_key order by d.date asc) as row
    FROM processed_new.fact_dau_snapshot d
    JOIN missing_install_date m
        ON  d.user_key = m.user_key
    WHERE d.date > m.install_date
    )  t
WHERE t.row=1
;

delete from processed_new.agg_iap
where date >=(select start_date from processed_new.tmp_start_date);

insert into processed_new.agg_iap
SELECT
	r.date
	,d.app_id
	,d.app_version
	,r.level
	,u.install_date
	,u.install_source
	,d.country
	,r.os
	,d.browser
	,d.browser_version
	,d.language
    ,case when trunc(u.conversion_ts)=r.date then 1 else 0 end as is_conversion_purchase
	,product_id
	,product_type
	,sum(r.revenue_usd) as revenue_usd
	,count(1) as purchase_cnt
	,count(distinct r.user_key) as purchase_user_cnt
FROM processed_new.fact_revenue r
join processed_new.fact_dau_snapshot d on d.user_key=r.user_key and r.date=d.date
join processed_new.dim_user u ON u.user_key=r.user_key
where r.date >= (select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;
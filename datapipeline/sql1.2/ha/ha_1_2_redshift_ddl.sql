CREATE  TABLE processed_new.fact_session(
  id varchar(100) NOT NULL PRIMARY KEY ENCODE BYTEDICT, 
  app_id varchar(100) NOT NULL ENCODE BYTEDICT, 
  app_version varchar(64) ENCODE BYTEDICT, 
  user_key varchar(100) ENCODE BYTEDICT, 
  app_user_id varchar(100) NOT NULL ENCODE BYTEDICT,  
  date_start date ENCODE DELTA, 
  date_end date ENCODE DELTA, 
  ts_start timestamp ENCODE DELTA, 
  ts_end timestamp ENCODE DELTA, 
  install_ts timestamp ENCODE DELTA, 
  install_date date ENCODE DELTA, 
  session_id varchar(156) ENCODE BYTEDICT, 
  facebook_id varchar(128) ENCODE BYTEDICT, 
  install_source varchar(1024) ENCODE BYTEDICT, 
  os varchar(100) ENCODE BYTEDICT, 
  os_version varchar(100) ENCODE BYTEDICT, 
  browser varchar(100) ENCODE BYTEDICT, 
  browser_version varchar(100) ENCODE BYTEDICT, 
  device varchar(100) ENCODE BYTEDICT, 
  country_code varchar(10) ENCODE BYTEDICT, 
  level_start int, 
  level_end int, 
  first_name varchar(200) ENCODE BYTEDICT, 
  gender varchar(32) ENCODE BYTEDICT, 
  birthday date ENCODE DELTA, 
  email varchar(500) ENCODE BYTEDICT, 
  ip varchar(100) ENCODE BYTEDICT, 
  language varchar(16) ENCODE BYTEDICT, 
  locale varchar(16) ENCODE BYTEDICT, 
  rc_wallet_start int, 
  rc_wallet_end int, 
  coin_wallet_start bigint, 
  coin_wallet_end bigint, 
  ab_experiment varchar(256) ENCODE BYTEDICT, 
  ab_variant varchar(256) ENCODE BYTEDICT, 
  session_length_sec bigint, 
  idfa varchar(156) ENCODE BYTEDICT, 
  idfv varchar(156) ENCODE BYTEDICT, 
  gaid varchar(156) ENCODE BYTEDICT, 
  mac_address varchar(156) ENCODE BYTEDICT,
  android_id varchar(156) ENCODE BYTEDICT)
  DISTKEY(user_key)
  SORTKEY(user_key,date_start,app_user_id,app_id);
  
CREATE TABLE  processed_new.fact_new_user(
    id varchar(128) not null primary key encode bytedict, 
    app_id varchar(64) encode bytedict, 
    app_version varchar(32) encode bytedict, 
    user_key varchar(128) encode bytedict, 
    app_user_id varchar(128) encode bytedict, 
    install_ts timestamp, 
    install_date date, 
    session_id varchar(128) encode bytedict, 
    facebook_id varchar(128) encode bytedict, 
    install_source varchar(1024) encode bytedict, 
    os varchar(128) encode bytedict, 
    os_version varchar(128) encode bytedict, 
    browser varchar(128) encode bytedict, 
    browser_version varchar(128) encode bytedict, 
    device varchar(128) encode bytedict, 
    country_code varchar(16) encode bytedict, 
    level int, 
    first_name varchar(128) encode bytedict, 
    gender varchar(16) encode bytedict, 
    birthday date, 
    email varchar(256) encode bytedict, 
    ip varchar(128) encode bytedict, 
    language varchar(32) encode bytedict, 
    locale varchar(32) encode bytedict, 
    ab_experiment varchar(128) encode bytedict, 
    ab_variant varchar(128) encode bytedict, 
    idfa varchar(128) encode bytedict, 
    idfv varchar(128) encode bytedict, 
    gaid varchar(128) encode bytedict, 
    mac_address varchar(128) encode bytedict, 
    android_id varchar(128) encode bytedict
)
DISTKEY(user_key)
SORTKEY(user_key,install_date,app_user_id,app_id);
  

CREATE  TABLE processed_new.fact_revenue
(
    id varchar(100) NOT NULL PRIMARY KEY ENCODE BYTEDICT, 
    app_id varchar(100) NOT NULL ENCODE BYTEDICT, 
    app_version varchar(64) ENCODE BYTEDICT, 
    user_key varchar(100) ENCODE BYTEDICT, 
    app_user_id varchar(100) NOT NULL ENCODE BYTEDICT, 
    date date ENCODE DELTA, 
    ts timestamp ENCODE DELTA, 
    install_ts timestamp ENCODE DELTA, 
    install_date date ENCODE DELTA, 
    session_id varchar(256) ENCODE BYTEDICT, 
    level int, 
    os varchar(100) ENCODE BYTEDICT, 
    os_version varchar(100) ENCODE BYTEDICT, 
    device varchar(100) ENCODE BYTEDICT, 
    browser varchar(100) ENCODE BYTEDICT, 
    browser_version varchar(100) ENCODE BYTEDICT, 
    country_code varchar(10) ENCODE BYTEDICT, 
    install_source varchar(1024) ENCODE BYTEDICT, 
    ip varchar(100) ENCODE BYTEDICT, 
    language varchar(16) ENCODE BYTEDICT, 
    locale varchar(16) ENCODE BYTEDICT, 
    ab_experiment varchar(256) ENCODE BYTEDICT, 
    ab_variant varchar(256) ENCODE BYTEDICT, 
    coin_wallet bigint, 
    rc_wallet int, 
    payment_processor varchar(128) ENCODE BYTEDICT, 
    product_id varchar(100) ENCODE BYTEDICT, 
    product_name varchar(100) ENCODE BYTEDICT, 
    product_type varchar(100) ENCODE BYTEDICT, 
    coins_in bigint, 
    rc_in int, 
    currency varchar(32) ENCODE BYTEDICT, 
    revenue_amount numeric(15,4), 
    revenue_usd numeric(15,4), 
    transaction_id varchar(156) ENCODE BYTEDICT, 
    idfa varchar(200) ENCODE BYTEDICT, 
    idfv varchar(200) ENCODE BYTEDICT, 
    gaid varchar(200) ENCODE BYTEDICT, 
    mac_address varchar(200) ENCODE BYTEDICT,
    android_id  varchar(200)  ENCODE BYTEDICT  
)
DISTKEY(user_key)
  SORTKEY(date, ts, user_key, app_user_id,os,country_code);


CREATE TABLE processed_new.fact_levelup 
(
id  varchar(100) NOT NULL ENCODE BYTEDICT,
user_key  varchar(100) ENCODE BYTEDICT,
app_id  varchar(100) NOT NULL ENCODE BYTEDICT,
app_version varchar(64) ENCODE BYTEDICT,
app_user_id  varchar(100) NOT NULL ENCODE BYTEDICT,
session_id varchar(256) ENCODE BYTEDICT,
previous_level  INT,
previous_levelup_date  date ENCODE DELTA,
previous_levelup_ts  TIMESTAMP ENCODE DELTA,
current_level  INT,
levelup_date date ENCODE DELTA,
levelup_ts TIMESTAMP ENCODE DELTA,
browser varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
os varchar(100) ENCODE BYTEDICT,
os_version varchar(100) ENCODE BYTEDICT,
device varchar(100) ENCODE BYTEDICT,
country_code varchar(10) ENCODE BYTEDICT,
ip varchar(100) ENCODE BYTEDICT,
language varchar(16) ENCODE BYTEDICT,
locale varchar(32) ENCODE BYTEDICT,
ab_experiment varchar(256) ENCODE BYTEDICT,
ab_variant varchar(256) ENCODE BYTEDICT
)
distkey(user_key)
  sortkey(user_key,current_level,levelup_date);


CREATE  TABLE processed_new.fact_dau_snapshot(
  id varchar(100) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  date date NOT NULL ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT,
  app_version varchar(100) ENCODE BYTEDICT,
  level_start int,
  level_end int,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  country_code varchar(10) ENCODE BYTEDICT,
  country varchar(100) ENCODE BYTEDICT,
  language varchar(16) ENCODE BYTEDICT,
  ab_experiment varchar(256) ENCODE BYTEDICT,
  ab_variant varchar(256) ENCODE BYTEDICT,
  is_new_user int,
  rc_in int,
  coins_in bigint,
  rc_out int,
  coins_out bigint,
  rc_wallet int,
  coin_wallet bigint,
  is_payer int,
  is_converted_today int,
  revenue_usd numeric(15,4),
  payment_cnt int,
  session_cnt int,
  playtime_sec int)
DISTKEY(user_key)
   SORTKEY(date, user_key, app_id, country_code);



CREATE  TABLE  processed_new.dim_user
(
  id  varchar(100) NOT NULL ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT,
  app_user_id  varchar(100) NOT NULL ENCODE BYTEDICT,
  snsid varchar(100) ENCODE BYTEDICT,
  facebook_id varchar(200) ENCODE BYTEDICT,
  install_ts  TIMESTAMP ENCODE DELTA,
  install_date  date ENCODE DELTA,
  install_source  varchar(1024) ENCODE BYTEDICT,
  install_subpublisher  varchar(1024) ENCODE BYTEDICT,
  install_campaign  varchar(500) ENCODE BYTEDICT,
  install_language  varchar(16) ENCODE BYTEDICT,
  install_locale  varchar(32) ENCODE BYTEDICT,
  install_country_code  varchar(16) ENCODE BYTEDICT,
  install_country  varchar(100) ENCODE BYTEDICT,
  install_os  varchar(100) ENCODE BYTEDICT,
  install_device  varchar(100) ENCODE BYTEDICT,
  install_device_alias  varchar(100) ENCODE BYTEDICT,
  install_browser  varchar(100) ENCODE BYTEDICT,
  install_gender  varchar(32) ENCODE BYTEDICT,
  install_age  varchar(10) ENCODE BYTEDICT,
  language  varchar(16) ENCODE BYTEDICT,
  locale varchar(32) ENCODE BYTEDICT,
  birthday  date ENCODE DELTA,
  gender varchar(32) ENCODE BYTEDICT,
  country_code  varchar(16) ENCODE BYTEDICT,
  country  varchar(100) ENCODE BYTEDICT,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device  varchar(100) ENCODE BYTEDICT,
  device_alias  varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  app_version varchar(64) ENCODE BYTEDICT,
  level INT,
  levelup_ts TIMESTAMP ENCODE DELTA,
  ab_experiment varchar(256) ENCODE BYTEDICT,
  ab_variant varchar(256) ENCODE BYTEDICT,
  is_payer INT ,
  conversion_ts TIMESTAMP ENCODE DELTA,
  total_revenue_usd NUMERIC(15,4) ,
  payment_cnt INT,
  last_login_ts TIMESTAMP ENCODE DELTA,
  email varchar(500) ENCODE BYTEDICT,
  last_ip varchar(100) ENCODE BYTEDICT
)  
DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);



CREATE TABLE processed_new.fact_tutorial
(
id varchar(100) NOT NULL ENCODE BYTEDICT,
app_id varchar(100) NOT NULL ENCODE BYTEDICT,
app_version varchar(64) ENCODE BYTEDICT,
user_key varchar(100) ENCODE BYTEDICT,
app_user_id varchar(100) NOT NULL ENCODE BYTEDICT,
date date ENCODE DELTA,
ts         timestamp ENCODE DELTA,
session_id varchar(256) ENCODE BYTEDICT,
os varchar(100) ENCODE BYTEDICT,
os_version varchar(100) ENCODE BYTEDICT,
device varchar(100) ENCODE BYTEDICT,
browser varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
country_code varchar(16) ENCODE BYTEDICT,
ip varchar(100) ENCODE BYTEDICT,
language varchar(16) ENCODE BYTEDICT,
locale varchar(32) ENCODE BYTEDICT,
level int,
tutorial_step   int,
tutorial_step_desc  varchar(100) ENCODE BYTEDICT
)DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);
;


CREATE  TABLE processed_new.fact_mission(
  id varchar(100) ENCODE BYTEDICT,
  mission_id varchar(100) ENCODE BYTEDICT,
  app_id varchar(100) ENCODE BYTEDICT,
  app_version varchar(32) ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT,
  app_user_id varchar(100) ENCODE BYTEDICT,
  date date ENCODE DELTA,
  ts timestamp,
  session_id varchar(256) ENCODE BYTEDICT,
  os varchar(100) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(100) ENCODE BYTEDICT,
  browser varchar(100) ENCODE BYTEDICT,
  browser_version varchar(100) ENCODE BYTEDICT,
  country_code varchar(32) ENCODE BYTEDICT,
  ip varchar(100) ENCODE BYTEDICT,
  language varchar(32) ENCODE BYTEDICT,
  mission_start_ts timestamp,
  mission_status int,
  level int)
DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id)
;

create  table processed_new.fact_mission_objective
(
id  varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id  varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
objective_id  varchar(100) ENCODE BYTEDICT,
objective_name  varchar(100) ENCODE BYTEDICT,
objective_type  varchar(100) ENCODE BYTEDICT,
objective_amount  int,
objective_amount_remaining  int
);

create  table processed_new.fact_mission_parameter
(
id  varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id  varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
parameter_name  varchar(100) ENCODE BYTEDICT,
parameter_value int
)
;



create  table processed_new.fact_mission_statistic
(
id  varchar(100) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
app_id  varchar(100) ENCODE BYTEDICT,
date date ENCODE DELTA,
statistic_name  varchar(100) ENCODE BYTEDICT,
statistic_value int
)
;

create  table processed_new.fact_player_resources
(
id varchar(100),
app_id varchar(100),
event varchar(32),
date date,
ts timestamp,
resource_id varchar(100),
resource_name varchar(100),
resource_type varchar(100),
resource_amount int
)
distkey(id)
sortkey(app_id,event,date,ts)
;

CREATE  TABLE raw_data.raw_adjust_daily
(
  adid  varchar(200) ENCODE BYTEDICT,
  userid varchar(100) ENCODE BYTEDICT,
  game varchar(100) ENCODE BYTEDICT,
  tracker  varchar(300) ENCODE BYTEDICT,
  tracker_name varchar(1024) ENCODE BYTEDICT,
  app_id  varchar(100) ENCODE BYTEDICT,
  ip_address  varchar(100) ENCODE BYTEDICT,
  idfa  varchar(200) ENCODE BYTEDICT,
  android_id varchar(200) ENCODE BYTEDICT,
  mac_sha1  varchar(200) ENCODE BYTEDICT,
  idfa_md5  varchar(200) ENCODE BYTEDICT,
  country  varchar(100) ENCODE BYTEDICT,
  timestamp  timestamp ENCODE DELTA,
  mac_md5  varchar(200) ENCODE BYTEDICT,
  gps_adid  varchar(200) ENCODE BYTEDICT,
  device_name  varchar(100) ENCODE BYTEDICT,
  os_name  varchar(100) ENCODE BYTEDICT,
  os_version  varchar(100) ENCODE BYTEDICT
)  ;


CREATE TABLE if not exists processed_new.tab_level_churn
(
    app_id varchar(100) ENCODE BYTEDICT,
    app_version varchar(50) ENCODE BYTEDICT,
    level int,
    current_level int,
    install_date date ENCODE DELTA,
    install_source varchar(1024) ENCODE BYTEDICT,
    country varchar(100) ENCODE BYTEDICT,
    os varchar(50) ENCODE BYTEDICT,
    browser varchar(100) ENCODE BYTEDICT,
    language varchar(32) ENCODE BYTEDICT,
    is_payer                  smallint default 0,
    is_churned_1days          smallint default 0,
    is_churned_3days          smallint default 0,
    is_churned_7days          smallint default 0,
    is_churned_14days         smallint default 0,
    is_churned_21days         smallint default 0,
    is_churned_30days         smallint default 0,
    user_cnt                  INTEGER DEFAULT 0
)
DISTKEY(install_date);


CREATE TABLE processed_new.tmp_user_daily_login
(
  date date ENCODE DELTA,
  app_id VARCHAR(64) ENCODE BYTEDICT,
  user_key VARCHAR(64) ENCODE BYTEDICT,
  app_user_id VARCHAR(64) ENCODE BYTEDICT,
    snsid varchar(64)  ENCODE BYTEDICT,
  last_login_ts TIMESTAMP ENCODE DELTA,
  facebook_id VARCHAR(50) ENCODE BYTEDICT,
  birthday date ENCODE DELTA,
  email varchar(256) ENCODE BYTEDICT,
  install_ts TIMESTAMP ENCODE DELTA,
  install_date date ENCODE DELTA,
  app_version VARCHAR(32) ENCODE BYTEDICT,
  level_start INTEGER,
  level_end INTEGER,
  os VARCHAR(32) ENCODE BYTEDICT,
  os_version VARCHAR(100) ENCODE BYTEDICT,
  country_code VARCHAR(16) ENCODE BYTEDICT,
  last_ip VARCHAR(64) ENCODE BYTEDICT,
  install_source VARCHAR(1024) ENCODE BYTEDICT,
  language VARCHAR(8) ENCODE BYTEDICT,
  locale VARCHAR(8) ENCODE BYTEDICT,
    ab_experiment varchar(128) ENCODE BYTEDICT,
  ab_variant varchar(128) ENCODE BYTEDICT,
  gender VARCHAR(16) ENCODE BYTEDICT,
  device VARCHAR(64) ENCODE BYTEDICT,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(100) ENCODE BYTEDICT,
  session_cnt INTEGER,
  playtime_sec BIGINT
)
    DISTKEY(date)
    SORTKEY(user_key,date,install_date);

create table processed_new.fact_ledger (
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64)  ENCODE LZO,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0,
  detail varchar(180) default null
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);
  
  
  

CREATE TABLE processed_new.agg_daily_ledger
(
	date DATE DISTKEY,
	level int,
	transaction_type VARCHAR(64),
	in_name VARCHAR(256),
	in_type VARCHAR(256),
	out_name VARCHAR(256),
	out_type VARCHAR(256),
	detail VARCHAR(180),
	in_amount BIGINT,
	out_amount BIGINT
)
SORTKEY
(
	date,
	level,
	transaction_type,
	in_type,
	in_name,
	out_type,
	out_name,
	detail
);

  

CREATE TABLE processed_new.fact_user_install_source
(
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app_id               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_user_id                VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_event_raw      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event          VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign_source                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  subpublisher_source                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(user_key)
  SORTKEY(app_id, user_key,app_user_id, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign_source, subpublisher_source);

CREATE TABLE processed_new.ref_install_source_map
(
  install_source_raw VARCHAR(1024) DISTKEY,
  install_source_lower VARCHAR(1024),
  install_source VARCHAR(1024)
)
SORTKEY
(
  install_source_raw,
  install_source_lower,
  install_source
);


CREATE TABLE processed_new.agg_tutorial
(
  app_id VARCHAR(64) ENCODE bytedict,
  app_version VARCHAR(50) ENCODE bytedict,
  level INTEGER,
  install_date DATE ENCODE delta,
  install_source VARCHAR(1024) ENCODE bytedict,
  country VARCHAR(100) ENCODE bytedict,
  os VARCHAR(64) ENCODE bytedict,
  os_version VARCHAR(64) ENCODE bytedict,
  device VARCHAR(100) ENCODE bytedict,
  browser VARCHAR(64) ENCODE bytedict,
  browser_version VARCHAR(100) ENCODE bytedict,
  language VARCHAR(32) ENCODE bytedict,
  tutorial_step INTEGER,
  tutorial_step_desc VARCHAR(100) ENCODE bytedict,
  is_required SMALLINT,
  user_cnt INTEGER
)
DISTSTYLE EVEN;




-- raw_data.raw_sendgrid_daily
CREATE  TABLE raw_data.raw_sendgrid_daily (
  snsid varchar(32),
  email varchar(64),
  category varchar(128),
  uid varchar(64),
  app_id varchar(64),
  time timestamp,
  ip varchar(32),
  event varchar(32),
  day date,
  campaign varchar(128)
  )
  distkey(uid)
  sortkey(app_id, day, campaign);
  
--  processed_new.fact_sendgrid
create table processed_new.fact_sendgrid (
app_id varchar(64),
campaign_name varchar(128),
campaign_date date,
delivered_per_campaign_date integer,
app_user_id varchar(64),
snsid varchar(64),
email varchar(128),
os varchar(32),
device varchar(32),
country varchar(64),
delivered_ts timestamp,
bounce_ts timestamp,
open_ts timestamp,
open_daydiff integer,
click_ts timestamp,
click_daydiff integer,
precamp_session_cnt integer,
precamp_purchase_cnt integer,
precamp_revenue_usd numeric(10,4),
precamp_level_end integer,
level_end integer,
total_session_cnt integer,
total_purchase_cnt integer,
total_revenue_usd numeric(10,4)
) 
distkey (app_user_id)
sortkey(campaign_name, campaign_date, app_user_id, email);

-- processed_new.agg_sendgrid_retention
create table processed_new.agg_sendgrid_retention (
install_date date,
app_id varchar(32),
retention_days integer,
language varchar(32),
os varchar(32),
browser varchar(32),
country varchar(64),
install_source varchar(256),
is_payer integer,
email_flag boolean,
user_cnt integer)
distkey (install_date)
sortkey(install_date, app_id, retention_days);

CREATE TABLE processed_new.agg_kpi
(
	id VARCHAR(100) NOT NULL ENCODE bytedict,
	date DATE ENCODE delta,
	app_id VARCHAR(100) ENCODE bytedict,
	app_version VARCHAR(50) ENCODE bytedict,
	install_year VARCHAR(100) ENCODE bytedict,
	install_month VARCHAR(100) ENCODE bytedict,
	install_week VARCHAR(100) ENCODE bytedict,
	install_source_group VARCHAR(1024) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	level_end INTEGER ENCODE bytedict,
	browser VARCHAR(100) ENCODE bytedict,
	browser_version VARCHAR(100) ENCODE bytedict,
	device_alias VARCHAR(100) ENCODE bytedict,
	country VARCHAR(100) ENCODE bytedict,
	os VARCHAR(100) ENCODE bytedict,
	os_version VARCHAR(100) ENCODE bytedict,
	language VARCHAR(32) ENCODE bytedict,
	ab_experiment VARCHAR(128) ENCODE bytedict,
	ab_variant VARCHAR(128) ENCODE bytedict,
	is_new_user INTEGER,
	is_payer INTEGER,
	new_user_cnt INTEGER,
	dau_cnt INTEGER,
	newpayer_cnt INTEGER,
	payer_today_cnt INTEGER,
	payment_cnt INTEGER,
	revenue_usd NUMERIC(14, 4),
	session_cnt INTEGER,
	session_length_sec BIGINT
)
DISTSTYLE EVEN;



create table processed_new.tab_marketing_kpi
(
app_id varchar(100) ENCODE BYTEDICT,
app_version varchar(50) ENCODE BYTEDICT,
install_date date ENCODE DELTA,
install_source varchar(1024) ENCODE BYTEDICT, 
install_campaign varchar(1024) ENCODE BYTEDICT, 
install_subpublisher varchar(1024) ENCODE BYTEDICT, 
install_country varchar(100) ENCODE BYTEDICT, 
install_os varchar(100) ENCODE BYTEDICT, 
new_user_cnt int,
d1_new_user_cnt int ,
d3_new_user_cnt int,
d7_new_user_cnt int,
d15_new_user_cnt int,
d30_new_user_cnt int,
d90_new_user_cnt int,
d120_new_user_cnt int,
revenue_usd numeric(14,4), 
d7_revenue_usd numeric(14,4) , 
d30_revenue_usd numeric(14,4), 
d90_revenue_usd numeric(14,4), 
d120_revenue_usd numeric(14,4),
payer_cnt int ,
d3_payer_cnt int, 
d1_retained_user_cnt int, 
d7_retained_user_cnt int, 
d15_retained_user_cnt int, 
d30_retained_user_cnt int
 );
 
create table processed_new.agg_iap
( date date ENCODE DELTA,
app_id varchar(100) ENCODE BYTEDICT,
app_version varchar(50) ENCODE BYTEDICT,
level int,
install_date date ENCODE DELTA,
install_source varchar(1024) ENCODE BYTEDICT,
country varchar(100) ENCODE BYTEDICT,
os varchar(50) ENCODE BYTEDICT,
browser varchar(100) ENCODE BYTEDICT,
browser_version varchar(100) ENCODE BYTEDICT,
language varchar(32) ENCODE BYTEDICT,
is_conversion_purchase int,
product_id varchar(128) ENCODE BYTEDICT,
product_type varchar(128) ENCODE BYTEDICT,
revenue_usd numeric(14,4),
purchase_cnt bigint,
purchase_user_cnt bigint
);



CREATE TABLE if not exists processed_new.tab_level_churn
(
    app_id varchar(100) ENCODE BYTEDICT,
    app_version varchar(50) ENCODE BYTEDICT,
    level int,
    current_level int,
    install_date date ENCODE DELTA,
    install_source varchar(1024) ENCODE BYTEDICT,
    country varchar(100) ENCODE BYTEDICT,
    os varchar(50) ENCODE BYTEDICT,
    browser varchar(100) ENCODE BYTEDICT,
    language varchar(32) ENCODE BYTEDICT,
    is_payer                  smallint default 0,
    is_churned_1days          smallint default 0,
    is_churned_3days          smallint default 0,
    is_churned_7days          smallint default 0,
    is_churned_14days         smallint default 0,
    is_churned_21days         smallint default 0,
    is_churned_30days         smallint default 0,
    user_cnt                  INTEGER DEFAULT 0
)
DISTKEY(install_date);

CREATE TABLE processed_new.fact_items_received
(
	id VARCHAR(100) ENCODE lzo DISTKEY,
	app_id VARCHAR(100) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	level INTEGER,
	event VARCHAR(32) ENCODE lzo,
	date DATE ENCODE delta,
	ts TIMESTAMP ENCODE delta,
	action_type VARCHAR(100) ENCODE lzo,
	item_received_id VARCHAR(100) ENCODE lzo,
	item_received_name VARCHAR(100),
	item_received_type VARCHAR(100),
	item_received_class VARCHAR(32),
	item_received_amount INTEGER ENCODE runlength
)
SORTKEY
(
	app_id,
	event,
	date,
	ts
);


CREATE TABLE processed_new.fact_items_spent
(
	id VARCHAR(100) ENCODE lzo DISTKEY,
	app_id VARCHAR(100) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	level INTEGER,
	event VARCHAR(32) ENCODE lzo,
	date DATE ENCODE delta,
	ts TIMESTAMP ENCODE delta,
	action_type VARCHAR(100) ENCODE lzo,
	item_spent_id VARCHAR(100) ENCODE lzo,
	item_spent_name VARCHAR(100),
	item_spent_type VARCHAR(100),
	item_spent_class VARCHAR(32),
	item_spent_amount INTEGER ENCODE delta
)
SORTKEY
(
	app_id,
	event,
	date,
	ts
);



CREATE TABLE processed_new.fact_items_target
(
	id VARCHAR(100) ENCODE lzo DISTKEY,
	app_id VARCHAR(100) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	level INTEGER,
	event VARCHAR(32) ENCODE lzo,
	date DATE ENCODE delta,
	ts TIMESTAMP ENCODE delta,
	action_type VARCHAR(100) ENCODE lzo,
	item_target_id VARCHAR(100) ENCODE lzo,
	item_target_name VARCHAR(100),
	item_target_type VARCHAR(100),
	item_target_class VARCHAR(32),
	item_target_amount INTEGER ENCODE runlength
)
SORTKEY
(
	app_id,
	event,
	date,
	ts
);

CREATE TABLE processed_new.fact_resources_received
(
	id VARCHAR(100) ENCODE lzo DISTKEY,
	app_id VARCHAR(100) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	level INTEGER,
	event VARCHAR(32) ENCODE lzo,
	date DATE ENCODE delta,
	ts TIMESTAMP ENCODE delta,
	action_type VARCHAR(100) ENCODE lzo,
	resource_received_id VARCHAR(100) ENCODE lzo,
	resource_received_name VARCHAR(100),
	resource_received_type VARCHAR(100),
	resource_received_amount INTEGER ENCODE bytedict
)
SORTKEY
(
	app_id,
	event,
	date,
	ts
);


CREATE TABLE processed_new.fact_resources_spent
(
	id VARCHAR(100) ENCODE lzo DISTKEY,
	app_id VARCHAR(100) ENCODE lzo,
	user_key VARCHAR(100) ENCODE lzo,
	level INTEGER,
	event VARCHAR(32) ENCODE lzo,
	date DATE ENCODE delta,
	ts TIMESTAMP ENCODE delta,
	action_type VARCHAR(100) ENCODE lzo,
	resource_spent_id VARCHAR(100) ENCODE lzo,
	resource_spent_name VARCHAR(100),
	resource_spent_type VARCHAR(100),
	resource_spent_amount INTEGER ENCODE bytedict
)
SORTKEY
(
	app_id,
	event,
	date,
	ts
);


CREATE  TABLE processed_new.fact_special_offer(
  id varchar(100) NOT NULL PRIMARY KEY ENCODE BYTEDICT,
  app_id varchar(100) NOT NULL ENCODE BYTEDICT, 
  app_version varchar(64) ENCODE BYTEDICT,
  user_key varchar(100) ENCODE BYTEDICT, 
  app_user_id varchar(100) NOT NULL ENCODE BYTEDICT,
  date date ENCODE DELTA, 
  ts timestamp ENCODE DELTA, 
  session_id varchar(156) ENCODE BYTEDICT, 
  os varchar(100) ENCODE BYTEDICT,
  os_version  varchar(100) ENCODE BYTEDICT, 
  browser varchar(100) ENCODE BYTEDICT, 
  browser_version varchar(100) ENCODE BYTEDICT, 
  country_code varchar(10) ENCODE BYTEDICT, 
  ip varchar(100) ENCODE BYTEDICT, 
  language varchar(16) ENCODE BYTEDICT,
  locale  varchar(16) ENCODE BYTEDICT, 
  level     int,
  package varchar(100) ENCODE BYTEDICT,
  schedule varchar(100) ENCODE BYTEDICT,
  price varchar(100) ENCODE BYTEDICT,
  action varchar(100) ENCODE BYTEDICT
  )
  DISTKEY(user_key)
  SORTKEY(user_key,date,app_user_id,app_id);
;


CREATE TABLE processed_new.agg_special_offer
(
          
   app_id VARCHAR(100) NOT NULL ENCODE bytedict,
   app_version VARCHAR(64) ENCODE bytedict,
   date DATE ENCODE delta,
   os VARCHAR(100) ENCODE bytedict,
   os_version VARCHAR(100) ENCODE bytedict,
   browser VARCHAR(100) ENCODE bytedict,
   browser_version VARCHAR(100) ENCODE bytedict,
   country VARCHAR(64) ENCODE bytedict,
   language VARCHAR(16) ENCODE bytedict,
   level INTEGER,
   package VARCHAR(100) ENCODE bytedict,
   schedule VARCHAR(100) ENCODE bytedict,
   price VARCHAR(100) ENCODE bytedict,
   qualified_players  integer,
   shown_players  integer,
   unique_clikers  integer,
   icon_clicks  integer,
   purchased_players  integer
)
SORTKEY
(
	user_key,
	date,
	app_id
);


create table processed_new.agg_load_step
(
  app_id VARCHAR(100) NOT NULL ENCODE bytedict,
  date date encode delta,
  load_step bigint,
  country_code VARCHAR(10) ENCODE bytedict,
  load_step_desc VARCHAR(40) ENCODE bytedict,
  app_version VARCHAR(20) ENCODE bytedict,
  level bigint,
  browser_version VARCHAR(100) ENCODE bytedict,
  language VARCHAR(16) ENCODE bytedict,
  os VARCHAR(100) ENCODE bytedict,
  browser VARCHAR(100) ENCODE bytedict,
  os_version VARCHAR(100) ENCODE bytedict,
  user_cnt bigint
 ) 
 SORTKEY
(
	date,
	app_id,
	country_code
);
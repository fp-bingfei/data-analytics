--todo: need checkpoints instead of reparsing all history

DROP TABLE IF EXISTS processed_new.tmp_user_payment;
CREATE TABLE processed_new.tmp_user_payment AS
SELECT DISTINCT
	user_key
    ,date
    ,min(r.ts) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)                      AS conversion_ts
    ,sum(revenue_usd) over (partition by r.user_key, date ORDER BY ts ASC rows between unbounded preceding and unbounded following)         AS revenue_usd
    ,sum(revenue_usd) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)               AS total_revenue_usd
    ,count(transaction_id) over (partition by r.user_key, date ORDER BY ts ASC rows between unbounded preceding and unbounded following )   AS purchase_cnt
    ,count(transaction_id) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)          AS total_purchase_cnt
FROM processed_new.fact_revenue r
;

delete from processed_new.agg_material
where date >=(select start_date from processed_new.tmp_start_date);

insert into processed_new.agg_material
-- material received
select 
app_id,
date,
level as player_level,
'material_receive' as material_type,
action_type as material_action_type,
item_received_id as item_id,
item_received_name as item_name,
item_received_type as item_type,
item_received_class as item_class,
item_received_level as item_level,
sum(item_received_amount) as item_amount,
count(distinct user_key) as user_count
from
-- distinct data: raw data contains duplications
(select
id,app_id,user_key,level,event,date,ts,action_type,item_received_id,item_received_name,item_received_type,item_received_class,item_received_amount,item_received_level
from processed_new.fact_items_received
where event = 'transaction'
and action_type in ('DailyBonus_Collect','Buy_Roadsideshop','gallery_reward','Lucky_Wheel','Market_Buy','Cash_Buy','Workshop_Collect','loot_harvestCrop','loot_completeOrder','loot_clearRecamlation','loot_collectTree','loot_openTreasure','loot_openMineChest','loot_fishLoot01','lucky_parcel_free','lucky_parcel_buy','levelup_reward','visitor_metal','loot_openSurpriseBag_extra','Bonus_option','special_offer','balloon_gifts','News_Buy','guild_request','swap_meet')
and date >= (select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14)
group by 1,2,3,4,5,6,7,8,9,10
union all
-- material spent
select 
app_id,
date,
level as player_level,
'material_spent' as material_type,
action_type as material_action_type,
item_spent_id as item_id,
item_spent_name as item_name,
item_spent_type as item_type,
item_spent_class as item_class,
item_spent_level as item_level,
sum(item_spent_amount) as item_amount,
count(distinct user_key) as user_count
from
-- distinct data: raw data contains duplications
(select
id,app_id,user_key,level,event,date,ts,action_type,item_spent_id,item_spent_name,item_spent_type,item_spent_class,item_spent_amount,item_spent_level
from processed_new.fact_items_spent
where event in ('transaction','timer')
and action_type in ('Material_Start','Machine_Start','Market_Sell','building_start','land_expand','machine_update','building_update','machine_start')
and date >= (select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14
)
group by 1,2,3,4,5,6,7,8,9,10;
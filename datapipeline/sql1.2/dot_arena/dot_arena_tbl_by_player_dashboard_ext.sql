delete from processed.agg_player_kpi_ext where date = dateadd('day',-1,current_date);
insert into processed.agg_player_kpi_ext

with last_login_info_1 as (
select
	uid
	,server
	,count(*)
from
	(
	select
	*
	,rank() over(partition by uid order by session_cnt desc, install_ts) as rk2
	from
		(
		select 
			a.*
			,rank() over(partition by uid order by date desc) as rk
		from processed.fact_player_dau_snapshot a 
		where date < dateadd('day',-1,current_date)
		)
	where rk=1
	)
where rk2=1
group by 1,2
)
, last_login_info_2 as (
select 
	a.uid
	,a.server
	,b.min_install_date
from last_login_info_1 a 
left join 
	(select
		uid
		,min(date) as min_install_date
	from processed.fact_player_dau_snapshot
	where 
		date < dateadd('day',-1,current_date)
		and is_new_player =1
	group by 1
	) b 
on a.uid = b.uid 
)

select 
	d.date
	,d.server
	,d.app
	,d.app_version
	,u.install_source
	,d.browser
	,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
	,d.os
	,d.language
	,u.is_payer
	,d.ab_test
	,'vip_' || cast(d.vip_level as varchar) as vip_level
	,case when (d.is_new_player=1 and datediff(day,d.date,l.min_install_date)>=0) or (d.is_new_player=1 and l.min_install_date is null) then 'new_player' 
	      else 'return_player' end as player_type
	,l.server as last_login_server
	,count(distinct case when d.is_new_player = 1 then d.player_key else null end) as new_players
	,count(distinct d.player_key) as dau
	,count(distinct case when d.is_converted_today = 1 then d.player_key else null end) as new_payers 
	,count(distinct case when d.revenue_usd>0 then d.player_key else null end) as today_payers
	,sum(d.revenue_usd) as revenue
	,sum(d.revenue_usd_iap) as revenue_iap
	,sum(d.revenue_usd_3rd) as revenue_3rd 
	,sum(d.session_cnt) as session_cnt
from processed.fact_player_dau_snapshot d 
join processed.dim_player u 
on d.player_key = u.player_key
left join last_login_info_2 l 
on d.uid = l.uid
where 
	d.date = dateadd('day',-1,current_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14
;

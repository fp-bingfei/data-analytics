-- Dota history data

-- import data from S3 directly
-- drop table if exists processed.raw_data_s3;
-- create table processed.raw_data_s3
-- (
-- 	json_str          VARCHAR(20000) ENCODE LZO,
-- 	load_hour         TIMESTAMP ENCODE DELTA
-- )
--   SORTKEY(load_hour);

drop table if exists processed.raw_data_s3_test;
create table processed.raw_data_s3_test
(
	json_str          VARCHAR(20000) ENCODE LZO
);

truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.daota/events/dtl.zh.prod/2015/05/11/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

-- drop table if exists processed.events_raw_test;
-- CREATE TABLE processed.events_raw_test
-- (
-- 	load_hour           TIMESTAMP ENCODE DELTA,
-- 	event               VARCHAR(32) ENCODE BYTEDICT,
-- 	date                DATE NOT NULL ENCODE DELTA,
-- 	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
-- 	app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
-- 	uid                 INTEGER NOT NULL,
-- 	server              VARCHAR(16) ENCODE BYTEDICT,
-- 	install_ts          TIMESTAMP ENCODE DELTA,
-- 	country_code        VARCHAR(8) ENCODE BYTEDICT,
-- 	level               SMALLINT,
-- 	viplevel            VARCHAR(32) ENCODE BYTEDICT,
-- 	ip                  VARCHAR(16) ENCODE LZO,
-- 	os                  VARCHAR(32) ENCODE BYTEDICT,
-- 	os_version          VARCHAR(64) ENCODE BYTEDICT,
-- 	app_version         VARCHAR(32) ENCODE BYTEDICT,
-- 	session_id          VARCHAR(32) ENCODE LZO,
-- 	bi_version          VARCHAR(32) ENCODE BYTEDICT,
-- 	idfa                VARCHAR(64) ENCODE LZO,
-- 	gaid                VARCHAR(64) ENCODE LZO,
-- 	android_id          VARCHAR(64) ENCODE LZO,
-- 	mac_address         VARCHAR(64) ENCODE LZO,
-- 	device              VARCHAR(32) ENCODE BYTEDICT,
-- 	lang                VARCHAR(32) ENCODE BYTEDICT,
-- 	json_str            VARCHAR(1024) ENCODE LZO,
-- 	properties          VARCHAR(1024) ENCODE LZO,
-- 	md5                 VARCHAR(32) ENCODE LZO
-- )
--   SORTKEY(load_hour, event, date, ts);

truncate table processed.events_raw_test;

insert into processed.events_raw_test
select
    load_hour
    ,json_extract_path_text(json_str, 'event') as event
    ,trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) as date
    ,dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str, 'app_id') as app
    ,CAST(json_extract_path_text(json_str, 'user_id') AS INTEGER) as uid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gameserver_id') as server
    ,case
            when json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') = '' then dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')
            else dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00')
     end as install_ts
    ,'--' as country_code
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'level') AS INTEGER) as level
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'viplevel') as viplevel
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'ip') as ip
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os') as os
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os_version') as os_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'app_version') as app_version
    ,json_extract_path_text(json_str, 'session_id') as session_id
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'idfa') as idfa
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gaid') as gaid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'android_id') as android_id
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'mac_address') as mac_address
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device') as device
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'lang') as lang
    ,json_str as json_str
    ,json_extract_path_text(json_str, 'properties') as properties
    ,md5(json_str) as md5
from processed.raw_data_s3_test;




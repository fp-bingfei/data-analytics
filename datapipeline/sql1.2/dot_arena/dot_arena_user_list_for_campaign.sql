-- TODO: re_target_campaign_payer
drop table if exists processed.re_target_campaign_payer;
create table processed.re_target_campaign_payer as
select d.*, u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and u.last_login_ts <= '2015-03-18';

unload ('select * from processed.re_target_campaign_payer;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_payer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_target_campaign_nonpayer_0318
drop table if exists processed.re_target_campaign_nonpayer_0318;
create table processed.re_target_campaign_nonpayer_0318 as
select d.*, u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 0 and u.last_login_ts >= '2015-03-10' and u.last_login_ts <= '2015-03-18';

unload ('select * from processed.re_target_campaign_nonpayer_0318;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_nonpayer_0318_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: 1 week later track 看谁回来了
drop table if exists processed.re_target_campaign_return_payer;
create table processed.re_target_campaign_return_payer as
select
    p.uid as funplus_id
    ,p.server
    ,p.player_id
    ,p.last_login_ts
    ,p.level
    ,case when p.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then p.country else 'Other' end as country
    ,count(1) as play_days
    ,sum(d.revenue_usd) as revenue
from
processed.fact_player_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_payer) t
        on d.uid = t.funplus_id
    join processed.dim_player p
        on d.player_key = p.player_key
where d.date > '2015-03-18'
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6;

unload ('select * from processed.re_target_campaign_return_payer;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_return_payer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


drop table if exists processed.re_target_campaign_return_nonpayer;
create table processed.re_target_campaign_return_nonpayer as
select
    p.uid as funplus_id
    ,p.server
    ,p.player_id
    ,p.last_login_ts
    ,p.level
    ,case when p.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then p.country else 'Other' end as country
    ,count(1) as play_days
    ,sum(d.revenue_usd) as revenue
from
processed.fact_player_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_nonpayer_0318) t
        on d.uid = t.funplus_id
    join processed.dim_player p
        on d.player_key = p.player_key
where d.date > '2015-03-18'
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6;

unload ('select * from processed.re_target_campaign_return_nonpayer;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_return_nonpayer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_target_campaign_payer 0506
drop table if exists processed.re_target_campaign_payer_0506;
create table processed.re_target_campaign_payer_0506 as
select d.*, u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 7;

unload ('select * from processed.re_target_campaign_payer_0506;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_payer_0506_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting A/B testing list
drop table if exists processed.re_target_campaign_ab_test_payer_0525;
create table processed.re_target_campaign_ab_test_payer_0525 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 7 and
    (google_aid != '' or idfa != '');

drop table if exists processed.re_target_campaign_ab_test_payer_0525_funplus_id;
create table processed.re_target_campaign_ab_test_payer_0525_funplus_id as
select distinct funplus_id
from processed.re_target_campaign_ab_test_payer_0525;

drop table if exists processed.re_target_campaign_ab_test_payer_0525_funplus_id_a;
create table processed.re_target_campaign_ab_test_payer_0525_funplus_id_a as
select *
from
(select *, row_number() over (order by funplus_id) as row_number
from processed.re_target_campaign_ab_test_payer_0525_funplus_id) t
where row_number % 2 = 1;

drop table if exists processed.re_target_campaign_ab_test_payer_0525_funplus_id_b;
create table processed.re_target_campaign_ab_test_payer_0525_funplus_id_b as
select *
from
(select *, row_number() over (order by funplus_id) as row_number
from processed.re_target_campaign_ab_test_payer_0525_funplus_id) t
where row_number % 2 = 0;

drop table if exists processed.re_target_campaign_ab_test_payer_0525_a;
create table processed.re_target_campaign_ab_test_payer_0525_a as
select r.*
from processed.re_target_campaign_ab_test_payer_0525_funplus_id_a a
	join processed.re_target_campaign_ab_test_payer_0525 r on a.funplus_id = r.funplus_id;

drop table if exists processed.re_target_campaign_ab_test_payer_0525_b;
create table processed.re_target_campaign_ab_test_payer_0525_b as
select r.*
from processed.re_target_campaign_ab_test_payer_0525_funplus_id_b b
	join processed.re_target_campaign_ab_test_payer_0525 r on b.funplus_id = r.funplus_id;

unload ('select * from processed.re_target_campaign_ab_test_payer_0525_a;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_ab_test_payer_0525_a_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from processed.re_target_campaign_ab_test_payer_0525_b;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_ab_test_payer_0525_b_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: 30day active payer list
drop table if exists processed.d30_active_payer_0527;
create table processed.d30_active_payer_0527 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) <= 30 and
    (google_aid != '' or idfa != '');

unload ('select * from processed.d30_active_payer_0527;')
to 's3://com.funplus.bitest/dota/lookalike_campaign/lookalike_campaign_d30_active_payer_0527_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting A/B testing list （30天未登录)
drop table if exists processed.re_target_campaign_ab_test_payer_0605;
create table processed.re_target_campaign_ab_test_payer_0605 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

drop table if exists processed.re_target_campaign_ab_test_payer_0605_funplus_id;
create table processed.re_target_campaign_ab_test_payer_0605_funplus_id as
select distinct funplus_id
from processed.re_target_campaign_ab_test_payer_0605;

drop table if exists processed.re_target_campaign_ab_test_payer_0605_funplus_id_a;
create table processed.re_target_campaign_ab_test_payer_0605_funplus_id_a as
select *
from
(select *, row_number() over (order by funplus_id) as row_number
from processed.re_target_campaign_ab_test_payer_0605_funplus_id) t
where row_number % 2 = 1;

drop table if exists processed.re_target_campaign_ab_test_payer_0605_funplus_id_b;
create table processed.re_target_campaign_ab_test_payer_0605_funplus_id_b as
select *
from
(select *, row_number() over (order by funplus_id) as row_number
from processed.re_target_campaign_ab_test_payer_0605_funplus_id) t
where row_number % 2 = 0;

drop table if exists processed.re_target_campaign_ab_test_payer_0605_a;
create table processed.re_target_campaign_ab_test_payer_0605_a as
select r.*
from processed.re_target_campaign_ab_test_payer_0605_funplus_id_a a
	join processed.re_target_campaign_ab_test_payer_0605 r on a.funplus_id = r.funplus_id;

drop table if exists processed.re_target_campaign_ab_test_payer_0605_b;
create table processed.re_target_campaign_ab_test_payer_0605_b as
select r.*
from processed.re_target_campaign_ab_test_payer_0605_funplus_id_b b
	join processed.re_target_campaign_ab_test_payer_0605 r on b.funplus_id = r.funplus_id;

unload ('select * from processed.re_target_campaign_ab_test_payer_0605_a;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_ab_test_payer_0605_a_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from processed.re_target_campaign_ab_test_payer_0605_b;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_ab_test_payer_0605_b_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0619;
create table processed.re_target_campaign_d30_inactive_payer_0619 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0619;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0703;
create table processed.re_target_campaign_d30_inactive_payer_0703 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0703;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0703_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0728;
create table processed.re_target_campaign_d30_inactive_payer_0728 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0728;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0728_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists processed.re_target_campaign_d30_inactive_nonpayer_0728;
create table processed.re_target_campaign_d30_inactive_nonpayer_0728 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 0 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and trunc(u.last_login_ts) != install_date and
    (google_aid != '' or idfa != '');

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0728
using processed.dim_user_max_level_server u
where processed.re_target_campaign_d30_inactive_nonpayer_0728.funplus_id = u.uid and u.level < 65;


unload ('select * from processed.re_target_campaign_d30_inactive_nonpayer_0728;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_nonpayer_0728_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: lookalike_campaign (30day active payer list)
-- iOS
drop table if exists processed.lookalike_campaign_d30_active_payer_0731_iOS;
create table processed.lookalike_campaign_d30_active_payer_0731_iOS as
select distinct upper(d.idfa) as idfa
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) <= 30 and idfa != '';

unload ('select * from processed.lookalike_campaign_d30_active_payer_0731_iOS;')
to 's3://com.funplus.bitest/dota/lookalike_campaign/lookalike_campaign_d30_active_payer_0731_iOS_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- Android
drop table if exists processed.lookalike_campaign_d30_active_payer_0731_Android;
create table processed.lookalike_campaign_d30_active_payer_0731_Android as
select distinct upper(d.google_aid) as google_aid
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) <= 30 and google_aid != '';

unload ('select * from processed.lookalike_campaign_d30_active_payer_0731_Android;')
to 's3://com.funplus.bitest/dota/lookalike_campaign/lookalike_campaign_d30_active_payer_0731_Android_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0806;
create table processed.re_target_campaign_d30_inactive_payer_0806 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0806
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0806.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0806;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0806_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists processed.re_target_campaign_d30_inactive_nonpayer_0806;
create table processed.re_target_campaign_d30_inactive_nonpayer_0806 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 0 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and trunc(u.last_login_ts) != install_date and
    (google_aid != '' or idfa != '');

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0806
using processed.dim_user_max_level_server u
where processed.re_target_campaign_d30_inactive_nonpayer_0806.funplus_id = u.uid and u.level < 65;

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0806
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_nonpayer_0806.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_nonpayer_0806;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_nonpayer_0806_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0812;
create table processed.re_target_campaign_d30_inactive_payer_0812 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0812
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0812.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0812;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0812_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists processed.re_target_campaign_d30_inactive_nonpayer_0812;
create table processed.re_target_campaign_d30_inactive_nonpayer_0812 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
	u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
	join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 0 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and trunc(u.last_login_ts) != install_date and
    (google_aid != '' or idfa != '');

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0812
using processed.dim_user_max_level_server u
where processed.re_target_campaign_d30_inactive_nonpayer_0812.funplus_id = u.uid and u.level < 65;

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0812
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_nonpayer_0812.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_nonpayer_0812;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_nonpayer_0812_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0821;
create table processed.re_target_campaign_d30_inactive_payer_0821 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0821
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0821.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0821;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0821_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists processed.re_target_campaign_d30_inactive_nonpayer_0821;
create table processed.re_target_campaign_d30_inactive_nonpayer_0821 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 0 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and trunc(u.last_login_ts) != install_date and
    (google_aid != '' or idfa != '');

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0821
using processed.dim_user_max_level_server u
where processed.re_target_campaign_d30_inactive_nonpayer_0821.funplus_id = u.uid and u.level < 65;

delete
from processed.re_target_campaign_d30_inactive_nonpayer_0821
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_nonpayer_0821.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_nonpayer_0821;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_nonpayer_0821_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0827;
create table processed.re_target_campaign_d30_inactive_payer_0827 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0827
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0827.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0827;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0827_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0925;
create table processed.re_target_campaign_d30_inactive_payer_0925 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0925
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0925.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0925;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0925_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1008;
create table processed.re_target_campaign_d30_inactive_payer_1008 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1008
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1008.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1008;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1008_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1023;
create table processed.re_target_campaign_d30_inactive_payer_1023 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1023
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1023.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1023;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1023_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1030;
create table processed.re_target_campaign_d30_inactive_payer_1030 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1030
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1030.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1030;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1030_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1106;
create table processed.re_target_campaign_d30_inactive_payer_1106 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1106
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1106.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1106;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1106_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1113;
create table processed.re_target_campaign_d30_inactive_payer_1113 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1113
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1113.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1113;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1113_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1120;
create table processed.re_target_campaign_d30_inactive_payer_1120 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1120
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1120.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1120;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1120_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1127;
create table processed.re_target_campaign_d30_inactive_payer_1127 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1127
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1127.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1127;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1127_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1204;
create table processed.re_target_campaign_d30_inactive_payer_1204 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1204
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1204.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1204;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1204_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （level = 90)
drop table if exists processed.re_target_campaign_level_e90_1211;
create table processed.re_target_campaign_level_e90_1211 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.level, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where level = 90 and (google_aid != '' or idfa != '');

unload ('select * from processed.re_target_campaign_level_e90_1211;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_level_e90_1211_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1218;
create table processed.re_target_campaign_d30_inactive_payer_1218 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1218
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1218.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1218;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1218_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1225;
create table processed.re_target_campaign_d30_inactive_payer_1225 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1225
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1225.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1225;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1225_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_1231;
create table processed.re_target_campaign_d30_inactive_payer_1231 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_1231
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_1231.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_1231;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_1231_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0107;
create table processed.re_target_campaign_d30_inactive_payer_0107 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0107
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0107.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0107;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0107_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0115;
create table processed.re_target_campaign_d30_inactive_payer_0115 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0115
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0115.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0115;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0115_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0122;
create table processed.re_target_campaign_d30_inactive_payer_0122 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0122
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0122.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0122;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0122_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0129;
create table processed.re_target_campaign_d30_inactive_payer_0129 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0129
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0129.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0129;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0129_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0204;
create table processed.re_target_campaign_d30_inactive_payer_0204 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0204
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0204.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0204;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0204_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0218;
create table processed.re_target_campaign_d30_inactive_payer_0218 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0218
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0218.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0218;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0218_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0225;
create table processed.re_target_campaign_d30_inactive_payer_0225 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0225
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0225.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0225;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0225_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0303;
create table processed.re_target_campaign_d30_inactive_payer_0303 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0303
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0303.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0303;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0303_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0310;
create table processed.re_target_campaign_d30_inactive_payer_0310 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0310
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0310.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0310;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0310_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0317;
create table processed.re_target_campaign_d30_inactive_payer_0317 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0317
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0317.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0317;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0317_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0325;
create table processed.re_target_campaign_d30_inactive_payer_0325 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0325
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0325.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0325;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0325_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0331;
create table processed.re_target_campaign_d30_inactive_payer_0331 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0331
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0331.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0331;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0331_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0407;
create table processed.re_target_campaign_d30_inactive_payer_0407 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0407
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0407.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0407;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0407_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0414;
create table processed.re_target_campaign_d30_inactive_payer_0414 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0414
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0414.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0414;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0414_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0421;
create table processed.re_target_campaign_d30_inactive_payer_0421 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0421
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0421.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0421;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0421_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0428;
create table processed.re_target_campaign_d30_inactive_payer_0428 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0428
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0428.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0428;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0428_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0506;
create table processed.re_target_campaign_d30_inactive_payer_0506 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0506
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0506.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0506;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0506_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list （30天未登录)
drop table if exists processed.re_target_campaign_d30_inactive_payer_0512;
create table processed.re_target_campaign_d30_inactive_payer_0512 as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid,
    u.country, u.install_source, u.total_revenue_usd, u.last_login_ts
from processed.dim_user u
    join processed.user_device_list d on u.uid = d.funplus_id
where u.is_payer = 1 and CURRENT_DATE - trunc(u.last_login_ts) > 30 and
    (google_aid != '' or idfa != '');

create temp table users_login_in_30days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 30;

delete
from processed.re_target_campaign_d30_inactive_payer_0512
using users_login_in_30days u
where processed.re_target_campaign_d30_inactive_payer_0512.funplus_id = u.uid;

unload ('select * from processed.re_target_campaign_d30_inactive_payer_0512;')
to 's3://com.funplus.bitest/dota/re_target/re_target_campaign_d30_inactive_payer_0512_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
-- TODO: set start date
-- start date table
-- drop table daota.processed.tmp_start_date;
-- create table daota.processed.tmp_start_date as
-- select DATEADD(day, -7, CURRENT_DATE) as start_date, DATEADD(day, -3, CURRENT_DATE) as raw_data_start_date;

-- update daota.processed.tmp_start_date
-- set start_date = '2014-08-04';

update daota.processed.tmp_start_date
set start_date = DATEADD(day, -3, CURRENT_DATE);

update daota.processed.tmp_start_date
set raw_data_start_date = DATEADD(day, -2, CURRENT_DATE);

-- TODO: keep the history for agg_kpi table
delete from daota.processed.agg_kpi_history where record_date = CURRENT_DATE;
insert into daota.processed.agg_kpi_history
(
      record_date
      ,date
      ,country
      ,os
      ,new_installs
      ,dau
      ,new_payers
      ,today_payers
      ,revenue
      ,session_cnt
)
select CURRENT_DATE as record_date
      ,date
      ,case when country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then country else 'Other' end as country
      ,os
      ,sum(new_installs) as new_installs
      ,sum(dau) as dau
      ,sum(new_payers) as new_payers
      ,sum(today_payers) as today_payers
      ,sum(revenue) as revenue
      ,sum(session_cnt) as session_cnt
from daota.processed.agg_kpi d
group by 2,3,4;

-- TODO: Discrepancy between Adjust and BI
drop table if exists processed.adjust_bi_discrepancy;
create table processed.adjust_bi_discrepancy as
select
    t5.install_date
    ,t5.install_date_str
    ,t5.app_id
    ,c.country
    ,t5.install_source
    ,t5.campaign
    ,t5.sub_publisher
    ,t5.creative_id
    ,t5.os
    ,t5.all_adjust_installs
    ,t5.empty_user_id_installs
    ,t5.in_bi_installs
    ,t5.not_in_bi_installs
from
    (select
        t1.install_date
        ,cast(t1.install_date as VARCHAR) as install_date_str
        ,t1.app_id
        ,t1.country
        ,t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.os
        ,t1.all_adjust_installs
        ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
        ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
        ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
    from
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.zh.daota' then 'Android'
                when app_id = '896830109' then 'iOS'
            end as os
            ,count(1) as all_adjust_installs
        from unique_adjust
        group by 1,2,3,4,5,6,7,8) t1
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.zh.daota' then 'Android'
                when app_id = '896830109' then 'iOS'
            end as os
            ,count(1) as empty_user_id_installs
        from unique_adjust
        where userid = '' or userid is null
        group by 1,2,3,4,5,6,7,8) t2
            on t1.install_source = t2.install_source and
               t1.install_date = t2.install_date and
               t1.campaign = t2.campaign and
               t1.sub_publisher = t2.sub_publisher and
               t1.creative_id = t2.creative_id and
               t1.app_id = t2.app_id and
               t1.country = t2.country and
               t1.os = t2.os
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,a.country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.zh.daota' then 'Android'
                when app_id = '896830109' then 'iOS'
            end as os
            ,count(1) in_bi_installs
        from unique_adjust a join processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        where a.userid != '' and a.userid is not null and u.uid is not null
        group by 1,2,3,4,5,6,7,8) t3
            on t1.install_source = t3.install_source and
               t1.install_date = t3.install_date and
               t1.campaign = t3.campaign and
               t1.sub_publisher = t3.sub_publisher and
               t1.creative_id = t3.creative_id and
               t1.app_id = t3.app_id and
               t1.country = t3.country and
               t1.os = t3.os
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,a.country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.zh.daota' then 'Android'
                when app_id = '896830109' then 'iOS'
            end as os
            ,count(1) not_in_bi_installs
        from unique_adjust a left join processed.dim_user u on a.userid = u.uid and u.app = a.game
        where a.userid != '' and a.userid is not null and (u.uid is null or lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) != lower(u.install_source) or trunc(a.ts) != u.install_date)
        group by 1,2,3,4,5,6,7,8) t4
            on t1.install_source = t4.install_source and
               t1.install_date = t4.install_date and
               t1.campaign = t4.campaign and
               t1.sub_publisher = t4.sub_publisher and
               t1.creative_id = t4.creative_id and
               t1.app_id = t4.app_id and
               t1.country = t4.country and
               t1.os = t4.os
    ) t5
    left join
    public.country c
    on t5.country=lower(c.country_code);


-- Fraud with same IP in one day
drop table if exists processed.adjust_ip_fraud;
create table processed.adjust_ip_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when app_id = 'com.funplus.zh.daota' then 'Android'
        when app_id = '896830109' then 'iOS'
     end as os
    ,count(1) as fraud_count
    ,count(distinct u.uid) as distinct_fraud_count
from
    unique_adjust a 
    left join 
    processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
    left join
    public.country c on a.country=lower(c.country_code)
where 
    (a.ip_address, date(a.ts))
    in 
    (
        select 
            ip_address
            ,date 
        from 
            (
                select 
                    ip_address
                    ,date(ts) as date
                    ,count(1) 
                from 
                    unique_adjust 
                where
                    tracker_name<>'Organic'
                    and date(ts)>=DATEADD(DAY, -90, current_date)
                    and userid != ''
                    and userid is not null
                group by 1,2
                having count(1)>=10
            )
    )
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -90, current_date)
    and a.userid != ''
    and a.userid is not null
    and u.uid is not null
group by 1,2,3,4,5,6,7,8,9;

-- Timestamp pattern of generating records
drop table if exists processed.adjust_ts_pattern;
create table processed.adjust_ts_pattern as
select
    *
from
    (
    select
        t.install_source
        ,t.campaign
        ,t.sub_publisher
        ,t.creative_id
        ,t.app_id
        ,t.country
        ,t.install_date
        ,t.os
        ,t.tslen - (LAG(t.tslen) OVER(PARTITION BY t.install_source ORDER BY t.install_source, t.tslen)) as tsdiff 
    from
        (
        select
            initcap(split_part(a.tracker_name, '::', 1)) as install_source
            ,split_part(a.tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(a.tracker_name, '::', 4) as creative_id
            ,a.game as app_id
            ,c.country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.zh.daota' then 'Android'
                when app_id = '896830109' then 'iOS'
             end as os
            ,(extract(epoch from a.ts) - extract(epoch from DATEADD(DAY, -90, current_date))) as tslen
        from
            unique_adjust a 
            left join 
            processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
            left join
            public.country c on a.country=lower(c.country_code)
        where
            a.tracker_name<>'Organic'
            and date(a.ts)>=DATEADD(DAY, -90, current_date)
            and a.userid != ''
            and a.userid is not null
            and u.uid is not null
            and split_part(a.tracker_name, '::', 1) != ''
        ) t
    order by t.install_source, t.tslen
    )
where
    tsdiff is not null
    and tsdiff <= 200;

-- Fraud with same device model in one day
drop table if exists processed.adjust_device_fraud;
create table processed.adjust_device_fraud as
select 
        t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.app_id
        ,t1.country
        ,t1.device_name
        ,t1.install_date
        ,t1.os
        ,t1.distinct_device_count as device_count
        ,t2.distinct_device_count as device_total
        ,t1.distinct_device_count*1.00/t2.distinct_device_count as device_percent
from
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,a.device_name
        ,trunc(ts) as install_date
        ,case
            when app_id = 'com.funplus.zh.daota' then 'Android'
            when app_id = '896830109' then 'iOS'
         end as os
        ,count(distinct u.uid) as distinct_device_count
    from
        unique_adjust a 
        left join 
        processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        left join
        public.country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.userid != ''
        and a.userid is not null
        and a.device_name != ''
        and u.uid is not null
    group by 1,2,3,4,5,6,7,8,9
    having count(distinct u.uid) >= 10
    ) t1
    left join
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,trunc(ts) as install_date
        ,case
            when app_id = 'com.funplus.zh.daota' then 'Android'
            when app_id = '896830109' then 'iOS'
         end as os
        ,count(distinct u.uid) as distinct_device_count
    from
        unique_adjust a 
        left join 
        processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        left join
        public.country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.userid != ''
        and a.userid is not null
        and a.device_name != ''
        and u.uid is not null
    group by 1,2,3,4,5,6,7,8
    having count(distinct u.uid) >= 10
    ) t2
    on
        t1.install_source = t2.install_source
        and t1.campaign = t2.campaign
        and t1.sub_publisher = t2.sub_publisher
        and t1.creative_id = t2.creative_id
        and t1.app_id = t2.app_id
        and t1.country = t2.country
        and t1.install_date = t2.install_date
        and t1.os = t2.os
order by device_percent desc;

-- Adjust interval between install and first action 
drop table if exists processed.adjust_first_action;
create table processed.adjust_first_action as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,trunc(a.ts) as install_date
    ,case
        when app_id = 'com.funplus.zh.daota' then 'Android'
        when app_id = '896830109' then 'iOS'
     end as os
    ,a.userid
    ,a.tslen
    ,count(1) as session_cnt
    ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    ,max(s.level_end) as level_end
from
    (select
        *
        ,case
            when u.all_max_level = 1 then null
            else (extract(epoch from u.ts_end) - extract(epoch from u.ts_start))
         end as tslen
    from
        unique_adjust adj 
        left join 
        (select
            *
            ,row_number () over (partition by uid order by ts_start) as rank
            ,max(level_end) over (partition by uid) as all_max_level
        from 
            processed.fact_session
        where
            date_start>=DATEADD(DAY, -90, current_date)
            and install_date>=DATEADD(DAY, -90, current_date)
        ) u on adj.userid = u.uid and u.app = adj.game and trunc(adj.ts) = u.install_date
    where adj.tracker_name<>'Organic'
        and split_part(adj.tracker_name, '::', 1) != ''
        and date(adj.ts)>=DATEADD(DAY, -90, current_date)
        and adj.userid != ''
        and adj.userid is not null
        and u.uid is not null
        and u.rank = 1
    ) a
    left join
    public.country c on a.country=lower(c.country_code)
    left join
    processed.fact_dau_snapshot s on a.userid = s.uid and a.game = s.app and trunc(a.ts) = s.install_date
        and s.date >= DATEADD(DAY, -90, current_date) and s.install_date >= DATEADD(DAY, -90, current_date)
group by 1,2,3,4,5,6,7,8,9,10
;

-- Adjust country fraud
drop table if exists processed.adjust_country_fraud;
create table processed.adjust_country_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when app_id = 'com.funplus.zh.daota' then 'Android'
        when app_id = '896830109' then 'iOS'
     end as os
    ,count(1) as fraud_count
    ,count(distinct u.uid) as distinct_fraud_count
from
    unique_adjust a 
    left join 
    processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
    left join
    public.country c on a.country=lower(c.country_code)
where 
    charindex('facebook', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('google', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('youtube', lower(split_part(a.tracker_name, '::', 1))) = 0
    and c.country not in ('Indonesia', 'Malaysia', 'Philippines', 'Singapore', 'Thailand')
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -8, current_date)
    and date(a.ts)<DATEADD(DAY, -1, current_date)
    and a.userid != ''
    and a.userid is not null
    and u.uid is not null
group by 1,2,3,4,5,6,7,8,9
;

-- Adjust fraud monitor
drop table if exists processed.adjust_fraud_monitor;
create table processed.adjust_fraud_monitor as
select
    t1.install_source
    ,t1.sub_publisher
    ,t1.app_id
    ,trunc(DATEADD(DAY, -8, current_date)) as install_date
    ,t1.install_count
    ,t2.d1_retained*1.00/t2.d1_new_installs as d1_retention
    ,t2.d7_retained*1.00/t2.d7_new_installs as d7_retention
    ,t3.level1_percent
    ,t4.discrepancy
    ,t5.country_fraud_count*1.00/t1.install_count as country_discrepancy
from
    (
    select
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,a.game as app_id
        ,count(distinct u.uid) as install_count
    from
        unique_adjust a
        left join
        processed.dim_user u on a.userid = u.uid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
    where
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -8, current_date)
        and date(a.ts)<DATEADD(DAY, -1, current_date)
        and a.userid != ''
        and a.userid is not null
        and u.uid is not null
    group by 1,2,3
    having count(distinct u.uid) > 20
    ) as t1
    left join
    (
    select
        initcap(install_source) as install_source
        ,case
            when install_source like 'Google Adwords%' then creative_id
            else sub_publisher
         end as sub_publisher
        ,app
        ,sum(d1_retained) as d1_retained
        ,case
            when sum(d1_new_installs) = 0 then null
            when sum(d1_new_installs) <> 0 then sum(d1_new_installs)
         end as d1_new_installs
        ,sum(d7_retained) as d7_retained
        ,case
            when sum(d7_new_installs) = 0 then null
            when sum(d7_new_installs) <> 0 then sum(d7_new_installs)
         end as d7_new_installs
    from
        processed.tab_marketing_kpi
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t2
    on
        t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
        and t1.app_id = t2.app
    left join
    (
    select
        initcap(f1.install_source) as install_source
        ,f1.sub_publisher
        ,f1.app_id
        ,f1.level1_count*1.00/f2.total as level1_percent
    from
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as level1_count
        from
            processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
            and tslen is null
            and level_end = 1
            and active_days >= 2
        group by 1,2,3
        ) as f1
        left join
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as total
        from
            processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
        group by 1,2,3
        ) as f2
        on
            f1.install_source = f2.install_source
            and f1.sub_publisher = f2.sub_publisher
            and f1.app_id = f2.app_id
    ) as t3
    on
        t1.install_source = t3.install_source
        and t1.sub_publisher = t3.sub_publisher
        and t1.app_id = t3.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,abs((sum(all_adjust_installs) - sum(in_bi_installs))*1.00/sum(all_adjust_installs)) as discrepancy
    from
        processed.adjust_bi_discrepancy
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t4
    on
        t1.install_source = t4.install_source
        and t1.sub_publisher = t4.sub_publisher
        and t1.app_id = t4.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,sum(distinct_fraud_count) as country_fraud_count
    from
        processed.adjust_country_fraud
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t5
    on
        t1.install_source = t5.install_source
        and t1.sub_publisher = t5.sub_publisher
        and t1.app_id = t5.app_id
where
    ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.2 and lower(t1.install_source) not in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.1 and lower(t1.install_source) in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or (t2.d1_retained*1.00/t2.d1_new_installs) > 0.6
    or t3.level1_percent > 0.1
    or t4.discrepancy > 0.4
    or (t5.country_fraud_count*1.00/t1.install_count) > 0.1
;

-- Blacklist
drop table if exists processed.blacklist;
create table processed.blacklist as
select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,ip_address
    ,country
    ,null as device_name
    ,sum(distinct_fraud_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,cast(null as numeric) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    processed.adjust_ip_fraud
group by 1,2,3,4,5,6
having sum(distinct_fraud_count) > 10

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,country
    ,device_name
    ,sum(device_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,sum(device_count)*1.00/sum(device_total) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    processed.adjust_device_fraud
group by 1,2,3,4,5,6,7
having 
    sum(device_count) > 10
    and sum(device_count)*1.00/sum(device_total) > 0.2

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,null as country
    ,null as device_name
    ,install_count
    ,d1_retention
    ,d7_retention
    ,level1_percent
    ,discrepancy
    ,cast(null as numeric) as device_percent
    ,country_discrepancy
from
    processed.adjust_fraud_monitor
;

-- Training Data
drop table if exists processed.training_data;
create table processed.training_data as
select
    t1.user_key
    ,t1.uid
    ,t1.app_id
    ,t1.install_source
    ,t1.sub_publisher
    ,t1.install_date
    ,t1.ip_address
    ,t1.country
    ,t1.device_name
    ,t1.os
    ,t1.os_version
    ,t1.level_end
    ,t1.session_cnt
    ,t1.purchase_cnt
    ,t1.active_days
    ,max(case
        when t1.level_end = 1 and t1.active_days >= 5 then 1
        else 0
     end) as flag1
    ,max(case
        when t1.level_end = 1 and t1.ip_address = t2.ip_address then 1
        else 0
     end) as flag2
    ,max(case
        when t1.level_end = 1 and t1.os = 'Android' and t1.device_name = t2.device_name then 1
        else 0
     end) as flag3
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.d1_retention < 0.05 then 1
        else 0
     end) as flag4
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.level1_percent > 0.4 then 1
        else 0
     end) as flag5
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.discrepancy > 0.4 then 1
        else 0
     end) as flag6
    ,max(case
        when t1.level_end = 1 and t2.country_discrepancy > 0.1 
            and charindex('facebook', lower(t1.install_source)) = 0
            and charindex('google', lower(t1.install_source)) = 0
            and charindex('youtube', lower(t1.install_source)) = 0
            and t1.country not in ('Indonesia', 'Malaysia', 'Philippines', 'Singapore', 'Thailand') then 1
        else 0
     end) as flag7
from
    (
    select
        u.user_key
        ,u.uid
        ,a.game as app_id
        ,initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,trunc(a.ts) as install_date
        ,a.ip_address
        ,c.country
        ,a.device_name
        ,u.os
        ,u.os_version
        ,max(s.level_end) as level_end
        ,sum(s.session_cnt) as session_cnt
        ,sum(s.purchase_cnt) as purchase_cnt
        ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    from
        public.unique_adjust a
        left join 
        processed.dim_user u on a.userid = u.uid and a.game = u.app and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = lower(u.install_source) and trunc(a.ts) = u.install_date
        left join
        public.country c on a.country=lower(c.country_code)
        left join
        processed.fact_dau_snapshot s on a.userid = s.uid and a.game = s.app and trunc(a.ts) = s.install_date and s.date >= DATEADD(DAY, -8, current_date) and s.date < DATEADD(DAY, -1, current_date)
    where 
        a.tracker_name <> 'Organic'
        and date(a.ts) >= DATEADD(DAY, -8, current_date)
        and date(a.ts) < DATEADD(DAY, -1, current_date)
        and a.userid != ''
        and a.userid is not null
        and u.uid is not null
    group by 1,2,3,4,5,6,7,8,9,10,11
    ) t1
    left join
    (
    select
        *
    from
        processed.blacklist b
    where
        b.install_date < DATEADD(DAY, -1, current_date)
    ) t2
    on
        t1.app_id = t2.app_id
        and t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
;


-- TODO: Get device info
drop table processed.user_device_list;
create table processed.user_device_list as
select userid as funplus_id, android_id, '' as imei, '' as google_aid, idfa, '' as mac_address
from adjust
union
select user_id as funplus_id, '' as android_id, '' as imei, google_aid, ios_ifa as idfa, mac_address
from mat
union
select fpid as funplus_id, android_id, imei, '' as google_aid, idfa, mac as mac_address
from kochava;

delete
from processed.user_device_list
where funplus_id = '' or funplus_id is null;

-- TODO: For EAS report
create temp table payment_info as
select app, uid, '' as snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from processed.fact_revenue
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    u.app
    ,u.uid
    ,u.os
    ,u.server
    ,'' as snsid
    ,'' as user_name
    ,'' as email
    ,'' as additional_email
    ,u.install_source
    ,u.install_ts
    ,u.language
    ,u.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,u.total_rc_in as rc
    ,0 as coins
    ,u.last_login_ts as last_login_ts
from processed.dim_user u
    left join payment_info p on u.app = p.app and u.uid = p.uid;

-- TODO: figure out fraud install source or sub_publisher
create temp table adjust_duplicated_users as
select userid, count(1) as install_count
from unique_adjust
where userid != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel;
create table processed.fraud_channel as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d on a.userid = d.userid;

-- TODO: re-targeting campaign
-- re-targeting campaign KPI for 0728
drop table if exists processed.retarget_date_0728_payer;
create table processed.retarget_date_0728_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0728) r on d.uid = r.funplus_id
where date >= '2015-07-28'
group by 1;

drop table if exists processed.retarget_date_0728_nonpayer;
create table processed.retarget_date_0728_nonpayer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_nonpayer_0728) r on d.uid = r.funplus_id
where date >= '2015-07-28'
group by 1;

create temp table login_30_days_users as
select distinct uid
from processed.fact_dau_snapshot
where date >= '2015-07-01' and date < '2015-07-28';

delete
from processed.retarget_date_0728_payer
using login_30_days_users d
where processed.retarget_date_0728_payer.uid = d.uid;

delete
from processed.retarget_date_0728_nonpayer
using login_30_days_users d
where processed.retarget_date_0728_nonpayer.uid = d.uid;

drop table if exists processed.agg_kpi_retarget_0728_payer;
create table processed.agg_kpi_retarget_0728_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0728_payer rd on d.uid = rd.uid
where date >= '2015-07-28'
group by 1,2,3,4,5,6,7,8,9,10,11;

drop table if exists processed.agg_kpi_retarget_0728_nonpayer;
create table processed.agg_kpi_retarget_0728_nonpayer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0728_nonpayer rd on d.uid = rd.uid
where date >= '2015-07-28'
group by 1,2,3,4,5,6,7,8,9,10,11;


-- re-targeting campaign KPI for 0806
drop table if exists processed.retarget_date_0806_payer;
create table processed.retarget_date_0806_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0806) r on d.uid = r.funplus_id
where date >= '2015-08-06'
group by 1;

drop table if exists processed.retarget_date_0806_nonpayer;
create table processed.retarget_date_0806_nonpayer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_nonpayer_0806) r on d.uid = r.funplus_id
where date >= '2015-08-06'
group by 1;

drop table if exists processed.agg_kpi_retarget_0806_payer;
create table processed.agg_kpi_retarget_0806_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0806_payer rd on d.uid = rd.uid
where date >= '2015-08-06'
group by 1,2,3,4,5,6,7,8,9,10,11;

drop table if exists processed.agg_kpi_retarget_0806_nonpayer;
create table processed.agg_kpi_retarget_0806_nonpayer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0806_nonpayer rd on d.uid = rd.uid
where date >= '2015-08-06'
group by 1,2,3,4,5,6,7,8,9,10,11;


-- re-targeting campaign KPI for 0812
drop table if exists processed.retarget_date_0812_payer;
create table processed.retarget_date_0812_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0812) r on d.uid = r.funplus_id
where date >= '2015-08-12'
group by 1;

drop table if exists processed.retarget_date_0812_nonpayer;
create table processed.retarget_date_0812_nonpayer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_nonpayer_0812) r on d.uid = r.funplus_id
where date >= '2015-08-12'
group by 1;

drop table if exists processed.agg_kpi_retarget_0812_payer;
create table processed.agg_kpi_retarget_0812_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0812_payer rd on d.uid = rd.uid
where date >= '2015-08-12'
group by 1,2,3,4,5,6,7,8,9,10,11;

drop table if exists processed.agg_kpi_retarget_0812_nonpayer;
create table processed.agg_kpi_retarget_0812_nonpayer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0812_nonpayer rd on d.uid = rd.uid
where date >= '2015-08-12'
group by 1,2,3,4,5,6,7,8,9,10,11;


-- re-targeting campaign KPI for 0821
drop table if exists processed.retarget_date_0821_payer;
create table processed.retarget_date_0821_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0821) r on d.uid = r.funplus_id
where date >= '2015-08-21'
group by 1;

drop table if exists processed.retarget_date_0821_nonpayer;
create table processed.retarget_date_0821_nonpayer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_nonpayer_0821) r on d.uid = r.funplus_id
where date >= '2015-08-21'
group by 1;

drop table if exists processed.agg_kpi_retarget_0821_payer;
create table processed.agg_kpi_retarget_0821_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0821_payer rd on d.uid = rd.uid
where date >= '2015-08-21'
group by 1,2,3,4,5,6,7,8,9,10,11;

drop table if exists processed.agg_kpi_retarget_0821_nonpayer;
create table processed.agg_kpi_retarget_0821_nonpayer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0821_nonpayer rd on d.uid = rd.uid
where date >= '2015-08-21'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0827, only payer
drop table if exists processed.retarget_date_0827_payer;
create table processed.retarget_date_0827_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0827) r on d.uid = r.funplus_id
where date >= '2015-08-27'
group by 1;

drop table if exists processed.agg_kpi_retarget_0827_payer;
create table processed.agg_kpi_retarget_0827_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0827_payer rd on d.uid = rd.uid
where date >= '2015-08-27'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0902, only payer
drop table if exists processed.retarget_date_0902_payer;
create table processed.retarget_date_0902_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0902) r on d.uid = r.funplus_id
where date >= '2015-09-02'
group by 1;

drop table if exists processed.agg_kpi_retarget_0902_payer;
create table processed.agg_kpi_retarget_0902_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0902_payer rd on d.uid = rd.uid
where date >= '2015-09-02'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0911, only payer
drop table if exists processed.retarget_date_0911_payer;
create table processed.retarget_date_0911_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0911) r on d.uid = r.funplus_id
where date >= '2015-09-11'
group by 1;

drop table if exists processed.agg_kpi_retarget_0911_payer;
create table processed.agg_kpi_retarget_0911_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0911_payer rd on d.uid = rd.uid
where date >= '2015-09-11'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0925, only payer
drop table if exists processed.retarget_date_0925_payer;
create table processed.retarget_date_0925_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0925) r on d.uid = r.funplus_id
where date >= '2015-09-25'
group by 1;

drop table if exists processed.agg_kpi_retarget_0925_payer;
create table processed.agg_kpi_retarget_0925_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0925_payer rd on d.uid = rd.uid
where date >= '2015-09-25'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1008, only payer
drop table if exists processed.retarget_date_1008_payer;
create table processed.retarget_date_1008_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1008) r on d.uid = r.funplus_id
where date >= '2015-10-08'
group by 1;

drop table if exists processed.agg_kpi_retarget_1008_payer;
create table processed.agg_kpi_retarget_1008_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1008_payer rd on d.uid = rd.uid
where date >= '2015-10-08'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1023, only payer
drop table if exists processed.retarget_date_1023_payer;
create table processed.retarget_date_1023_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1023) r on d.uid = r.funplus_id
where date >= '2015-10-23'
group by 1;

drop table if exists processed.agg_kpi_retarget_1023_payer;
create table processed.agg_kpi_retarget_1023_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1023_payer rd on d.uid = rd.uid
where date >= '2015-10-23'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1030, only payer
drop table if exists processed.retarget_date_1030_payer;
create table processed.retarget_date_1030_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1030) r on d.uid = r.funplus_id
where date >= '2015-10-30'
group by 1;

drop table if exists processed.agg_kpi_retarget_1030_payer;
create table processed.agg_kpi_retarget_1030_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1030_payer rd on d.uid = rd.uid
where date >= '2015-10-30'
group by 1,2,3,4,5,6,7,8,9,10,11;


-- re-targeting campaign KPI for 1106, only payer
drop table if exists processed.retarget_date_1106_payer;
create table processed.retarget_date_1106_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1106) r on d.uid = r.funplus_id
where date >= '2015-11-06'
group by 1;

drop table if exists processed.agg_kpi_retarget_1106_payer;
create table processed.agg_kpi_retarget_1106_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1106_payer rd on d.uid = rd.uid
where date >= '2015-11-06'
group by 1,2,3,4,5,6,7,8,9,10,11;


-- re-targeting campaign KPI for 1113, only payer
drop table if exists processed.retarget_date_1113_payer;
create table processed.retarget_date_1113_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1113) r on d.uid = r.funplus_id
where date >= '2015-11-13'
group by 1;

drop table if exists processed.agg_kpi_retarget_1113_payer;
create table processed.agg_kpi_retarget_1113_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1113_payer rd on d.uid = rd.uid
where date >= '2015-11-13'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1120, only payer
drop table if exists processed.retarget_date_1120_payer;
create table processed.retarget_date_1120_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1120) r on d.uid = r.funplus_id
where date >= '2015-11-20'
group by 1;

drop table if exists processed.agg_kpi_retarget_1120_payer;
create table processed.agg_kpi_retarget_1120_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1120_payer rd on d.uid = rd.uid
where date >= '2015-11-20'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1127, only payer
drop table if exists processed.retarget_date_1127_payer;
create table processed.retarget_date_1127_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1127) r on d.uid = r.funplus_id
where date >= '2015-11-27'
group by 1;

drop table if exists processed.agg_kpi_retarget_1127_payer;
create table processed.agg_kpi_retarget_1127_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1127_payer rd on d.uid = rd.uid
where date >= '2015-11-27'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1204, only payer
drop table if exists processed.retarget_date_1204_payer;
create table processed.retarget_date_1204_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1204) r on d.uid = r.funplus_id
where date >= '2015-12-04'
group by 1;

drop table if exists processed.agg_kpi_retarget_1204_payer;
create table processed.agg_kpi_retarget_1204_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1204_payer rd on d.uid = rd.uid
where date >= '2015-12-04'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1211, only level = 90
drop table if exists processed.retarget_date_1211_level_e90;
create table processed.retarget_date_1211_level_e90 as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_level_e90_1211) r on d.uid = r.funplus_id
where date >= '2015-12-11'
group by 1;

drop table if exists processed.agg_kpi_retarget_1211_level_e90;
create table processed.agg_kpi_retarget_1211_level_e90 as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1211_level_e90 rd on d.uid = rd.uid
where date >= '2015-12-11'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1218, only payer
drop table if exists processed.retarget_date_1218_payer;
create table processed.retarget_date_1218_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1218) r on d.uid = r.funplus_id
where date >= '2015-12-18'
group by 1;

drop table if exists processed.agg_kpi_retarget_1218_payer;
create table processed.agg_kpi_retarget_1218_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1218_payer rd on d.uid = rd.uid
where date >= '2015-12-18'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1225, only payer
drop table if exists processed.retarget_date_1225_payer;
create table processed.retarget_date_1225_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1225) r on d.uid = r.funplus_id
where date >= '2015-12-25'
group by 1;

drop table if exists processed.agg_kpi_retarget_1225_payer;
create table processed.agg_kpi_retarget_1225_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1225_payer rd on d.uid = rd.uid
where date >= '2015-12-25'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 1231, only payer
drop table if exists processed.retarget_date_1231_payer;
create table processed.retarget_date_1231_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_1231) r on d.uid = r.funplus_id
where date >= '2015-12-31'
group by 1;

drop table if exists processed.agg_kpi_retarget_1231_payer;
create table processed.agg_kpi_retarget_1231_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1231_payer rd on d.uid = rd.uid
where date >= '2015-12-31'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0107, only payer
drop table if exists processed.retarget_date_0107_payer;
create table processed.retarget_date_0107_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0107) r on d.uid = r.funplus_id
where date >= '2016-01-07'
group by 1;

drop table if exists processed.agg_kpi_retarget_0107_payer;
create table processed.agg_kpi_retarget_0107_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0107_payer rd on d.uid = rd.uid
where date >= '2016-01-07'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0115, only payer
drop table if exists processed.retarget_date_0115_payer;
create table processed.retarget_date_0115_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0115) r on d.uid = r.funplus_id
where date >= '2016-01-15'
group by 1;

drop table if exists processed.agg_kpi_retarget_0115_payer;
create table processed.agg_kpi_retarget_0115_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0115_payer rd on d.uid = rd.uid
where date >= '2016-01-15'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0122, only payer
drop table if exists processed.retarget_date_0122_payer;
create table processed.retarget_date_0122_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0122) r on d.uid = r.funplus_id
where date >= '2016-01-22'
group by 1;

drop table if exists processed.agg_kpi_retarget_0122_payer;
create table processed.agg_kpi_retarget_0122_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0122_payer rd on d.uid = rd.uid
where date >= '2016-01-22'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0129, only payer
drop table if exists processed.retarget_date_0129_payer;
create table processed.retarget_date_0129_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0129) r on d.uid = r.funplus_id
where date >= '2016-01-29'
group by 1;

drop table if exists processed.agg_kpi_retarget_0129_payer;
create table processed.agg_kpi_retarget_0129_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0129_payer rd on d.uid = rd.uid
where date >= '2016-01-29'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0204, only payer
drop table if exists processed.retarget_date_0204_payer;
create table processed.retarget_date_0204_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0204) r on d.uid = r.funplus_id
where date >= '2016-02-04'
group by 1;

drop table if exists processed.agg_kpi_retarget_0204_payer;
create table processed.agg_kpi_retarget_0204_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0204_payer rd on d.uid = rd.uid
where date >= '2016-02-04'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0218, only payer
drop table if exists processed.retarget_date_0218_payer;
create table processed.retarget_date_0218_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0218) r on d.uid = r.funplus_id
where date >= '2016-02-18'
group by 1;

drop table if exists processed.agg_kpi_retarget_0218_payer;
create table processed.agg_kpi_retarget_0218_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0218_payer rd on d.uid = rd.uid
where date >= '2016-02-18'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0225, only payer
drop table if exists processed.retarget_date_0225_payer;
create table processed.retarget_date_0225_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0225) r on d.uid = r.funplus_id
where date >= '2016-02-25'
group by 1;

drop table if exists processed.agg_kpi_retarget_0225_payer;
create table processed.agg_kpi_retarget_0225_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0225_payer rd on d.uid = rd.uid
where date >= '2016-02-25'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0303, only payer
drop table if exists processed.retarget_date_0303_payer;
create table processed.retarget_date_0303_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0303) r on d.uid = r.funplus_id
where date >= '2016-03-03'
group by 1;

drop table if exists processed.agg_kpi_retarget_0303_payer;
create table processed.agg_kpi_retarget_0303_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0303_payer rd on d.uid = rd.uid
where date >= '2016-03-03'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0310, only payer
drop table if exists processed.retarget_date_0310_payer;
create table processed.retarget_date_0310_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0310) r on d.uid = r.funplus_id
where date >= '2016-03-10'
group by 1;

drop table if exists processed.agg_kpi_retarget_0310_payer;
create table processed.agg_kpi_retarget_0310_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0310_payer rd on d.uid = rd.uid
where date >= '2016-03-10'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0317, only payer
drop table if exists processed.retarget_date_0317_payer;
create table processed.retarget_date_0317_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0317) r on d.uid = r.funplus_id
where date >= '2016-03-17'
group by 1;

drop table if exists processed.agg_kpi_retarget_0317_payer;
create table processed.agg_kpi_retarget_0317_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0317_payer rd on d.uid = rd.uid
where date >= '2016-03-17'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0325, only payer
drop table if exists processed.retarget_date_0325_payer;
create table processed.retarget_date_0325_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0325) r on d.uid = r.funplus_id
where date >= '2016-03-25'
group by 1;

drop table if exists processed.agg_kpi_retarget_0325_payer;
create table processed.agg_kpi_retarget_0325_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0325_payer rd on d.uid = rd.uid
where date >= '2016-03-25'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0331, only payer
drop table if exists processed.retarget_date_0331_payer;
create table processed.retarget_date_0331_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0331) r on d.uid = r.funplus_id
where date >= '2016-03-31'
group by 1;

drop table if exists processed.agg_kpi_retarget_0331_payer;
create table processed.agg_kpi_retarget_0331_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0331_payer rd on d.uid = rd.uid
where date >= '2016-03-31'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0407, only payer
drop table if exists processed.retarget_date_0407_payer;
create table processed.retarget_date_0407_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0407) r on d.uid = r.funplus_id
where date >= '2016-04-07'
group by 1;

drop table if exists processed.agg_kpi_retarget_0407_payer;
create table processed.agg_kpi_retarget_0407_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0407_payer rd on d.uid = rd.uid
where date >= '2016-04-07'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0414, only payer
drop table if exists processed.retarget_date_0414_payer;
create table processed.retarget_date_0414_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0414) r on d.uid = r.funplus_id
where date >= '2016-04-14'
group by 1;

drop table if exists processed.agg_kpi_retarget_0414_payer;
create table processed.agg_kpi_retarget_0414_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0414_payer rd on d.uid = rd.uid
where date >= '2016-04-14'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0421, only payer
drop table if exists processed.retarget_date_0421_payer;
create table processed.retarget_date_0421_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0421) r on d.uid = r.funplus_id
where date >= '2016-04-21'
group by 1;

drop table if exists processed.agg_kpi_retarget_0421_payer;
create table processed.agg_kpi_retarget_0421_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0421_payer rd on d.uid = rd.uid
where date >= '2016-04-21'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0428, only payer
drop table if exists processed.retarget_date_0428_payer;
create table processed.retarget_date_0428_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0428) r on d.uid = r.funplus_id
where date >= '2016-04-28'
group by 1;

drop table if exists processed.agg_kpi_retarget_0428_payer;
create table processed.agg_kpi_retarget_0428_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0428_payer rd on d.uid = rd.uid
where date >= '2016-04-28'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0506, only payer
drop table if exists processed.retarget_date_0506_payer;
create table processed.retarget_date_0506_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0506) r on d.uid = r.funplus_id
where date >= '2016-05-06'
group by 1;

drop table if exists processed.agg_kpi_retarget_0506_payer;
create table processed.agg_kpi_retarget_0506_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0506_payer rd on d.uid = rd.uid
where date >= '2016-05-06'
group by 1,2,3,4,5,6,7,8,9,10,11;

-- re-targeting campaign KPI for 0512, only payer
drop table if exists processed.retarget_date_0512_payer;
create table processed.retarget_date_0512_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct funplus_id from processed.re_target_campaign_d30_inactive_payer_0512) r on d.uid = r.funplus_id
where date >= '2016-05-12'
group by 1;

drop table if exists processed.agg_kpi_retarget_0512_payer;
create table processed.agg_kpi_retarget_0512_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0512_payer rd on d.uid = rd.uid
where date >= '2016-05-12'
group by 1,2,3,4,5,6,7,8,9,10,11;

--union
drop table if exists processed.agg_kpi_retarget_nonpayer;
create table processed.agg_kpi_retarget_nonpayer as
select '2015-08-21' as retarget_date, *
from processed.agg_kpi_retarget_0821_nonpayer
union
select '2015-08-12' as retarget_date, *
from processed.agg_kpi_retarget_0812_nonpayer
union
select '2015-08-06' as retarget_date, *
from processed.agg_kpi_retarget_0806_nonpayer
union
select '2015-07-28' as retarget_date, *
from processed.agg_kpi_retarget_0728_nonpayer;

drop table if exists processed.agg_kpi_retarget_payer;
create table processed.agg_kpi_retarget_payer as
select '2016-05-12' as retarget_date, *
from processed.agg_kpi_retarget_0512_payer
union
select '2016-05-06' as retarget_date, *
from processed.agg_kpi_retarget_0506_payer
union
select '2016-04-28' as retarget_date, *
from processed.agg_kpi_retarget_0428_payer
union
select '2016-04-21' as retarget_date, *
from processed.agg_kpi_retarget_0421_payer
union
select '2016-04-14' as retarget_date, *
from processed.agg_kpi_retarget_0414_payer
union
select '2016-04-07' as retarget_date, *
from processed.agg_kpi_retarget_0407_payer
union
select '2016-03-31' as retarget_date, *
from processed.agg_kpi_retarget_0331_payer
union
select '2016-03-25' as retarget_date, *
from processed.agg_kpi_retarget_0325_payer
union
select '2016-03-17' as retarget_date, *
from processed.agg_kpi_retarget_0317_payer
union
select '2016-03-10' as retarget_date, *
from processed.agg_kpi_retarget_0310_payer
union
select '2016-03-03' as retarget_date, *
from processed.agg_kpi_retarget_0303_payer
union
select '2016-02-25' as retarget_date, *
from processed.agg_kpi_retarget_0225_payer
union
select '2016-02-18' as retarget_date, *
from processed.agg_kpi_retarget_0218_payer
union
select '2016-02-04' as retarget_date, *
from processed.agg_kpi_retarget_0204_payer
union
select '2016-01-29' as retarget_date, *
from processed.agg_kpi_retarget_0129_payer
union
select '2016-01-22' as retarget_date, *
from processed.agg_kpi_retarget_0122_payer
union
select '2016-01-15' as retarget_date, *
from processed.agg_kpi_retarget_0115_payer
union
select '2016-01-07' as retarget_date, *
from processed.agg_kpi_retarget_0107_payer
union
select '2015-12-31' as retarget_date, *
from processed.agg_kpi_retarget_1231_payer
union
select '2015-12-25' as retarget_date, *
from processed.agg_kpi_retarget_1225_payer
union
select '2015-12-18' as retarget_date, *
from processed.agg_kpi_retarget_1218_payer
union
select '2015-12-11' as retarget_date, *
from processed.agg_kpi_retarget_1211_level_e90
union
select '2015-12-04' as retarget_date, *
from processed.agg_kpi_retarget_1204_payer
union
select '2015-11-27' as retarget_date, *
from processed.agg_kpi_retarget_1127_payer
union
select '2015-11-20' as retarget_date, *
from processed.agg_kpi_retarget_1120_payer
union
select '2015-11-13' as retarget_date, *
from processed.agg_kpi_retarget_1113_payer
union
select '2015-11-06' as retarget_date, *
from processed.agg_kpi_retarget_1106_payer
union
select '2015-10-30' as retarget_date, *
from processed.agg_kpi_retarget_1030_payer
union
select '2015-10-23' as retarget_date, *
from processed.agg_kpi_retarget_1023_payer
union
select '2015-10-08' as retarget_date, *
from processed.agg_kpi_retarget_1008_payer
union
select '2015-09-25' as retarget_date, *
from processed.agg_kpi_retarget_0925_payer
union
select '2015-09-11' as retarget_date, *
from processed.agg_kpi_retarget_0911_payer
union
select '2015-09-02' as retarget_date, *
from processed.agg_kpi_retarget_0902_payer
union
select '2015-08-27' as retarget_date, *
from processed.agg_kpi_retarget_0827_payer
union
select '2015-08-21' as retarget_date, *
from processed.agg_kpi_retarget_0821_payer
union
select '2015-08-12' as retarget_date, *
from processed.agg_kpi_retarget_0812_payer
union
select '2015-08-06' as retarget_date, *
from processed.agg_kpi_retarget_0806_payer
union
select '2015-07-28' as retarget_date, *
from processed.agg_kpi_retarget_0728_payer;

--------- paytimes
drop table if exists processed.pay_time;
create table processed.pay_time as
select r.app,
r.date,
r.user_key,
ts,
r.uid,
r.snsid,
r.level,
r.server,
r.payment_processor,
r.product_id,
r.product_name,
r.product_type,
r.usd,
u.install_source,
u.country,
usd_iap,
usd_3rd,
rank() over  (partition by r.app,r.user_key order by ts asc) as payment_time from processed.fact_revenue r
join processed.dim_user u on r.user_key=u.user_key
union all
select app,
install_date as date,
user_key,
install_ts as ts,
uid,
snsid,
0 as level,
server,
null as payment_processor,
null as product_id,
null as product_name,
null as product_type,
0 as usd,
install_source,
country,
0 as usd_iap,
0 as usd_3rd,
0 as payment_time from processed.dim_user
;

grant usage on schema processed to "gz_pm";
grant select on all tables in schema processed to "gz_pm";

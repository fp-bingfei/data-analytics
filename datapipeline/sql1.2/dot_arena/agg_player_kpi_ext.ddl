create table processed.agg_player_kpi_ext 
-- add field "player_type" to figure out if it is return playe or totally new player
( 
date                date
,server             varchar(8)
,app                varchar(32)
,app_version        varchar(32)
,install_source     varchar(128)
,browser            varchar(32)
,country            varchar(64)
,os                 varchar(32)
,language           varchar(32)
,is_payer           smallint
,ab_test            varchar(64)
,vip_level          varchar(16)
,player_type        varchar(32)
,last_login_server  varchar(8)
,new_players        integer
,dau                integer
,new_payers         integer
,today_payers       integer
,revenue            numeric(14,4)
,revenue_iap        numeric(14,4)
,revenue_3rd        numeric(14,4)
,session_cnt        integer
)
;
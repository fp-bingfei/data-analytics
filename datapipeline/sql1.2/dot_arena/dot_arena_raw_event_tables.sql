--------------------------------------------------------------------------------------------------------------------------------------------
--Dot_Arena session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

-- TODO: check the data
-- drop table if exists daota.processed.tab_spiderman_capture;
-- create table daota.processed.tab_spiderman_capture (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number_old" BIGINT NOT NULL DEFAULT 0,
--   "row_number_new" BIGINT NOT NULL DEFAULT 0,
--   "row_number_diff" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);
--
-- drop table if exists daota.processed.tab_spiderman_capture_history;
-- create table daota.processed.tab_spiderman_capture_history (
--   "record_date" date NOT NULL,
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(record_date, event, date, hour);

truncate table daota.processed.tab_spiderman_capture;
drop table daota.processed.tab_spiderman_capture_old;
alter table daota.processed.tab_spiderman_capture_new rename to tab_spiderman_capture_old;

create table daota.processed.tab_spiderman_capture_new (
  "event" varchar(32) NOT NULL,
  "date" date NOT NULL,
  "hour" SMALLINT NOT NULL,
  "row_number" BIGINT NOT NULL
)
  DISTKEY(event)
  SORTKEY(event, date, hour);

insert into daota.processed.tab_spiderman_capture_new
select event, trunc(ts) as date, extract(hour from ts) as hour, count(1)
from daota.public.events
group by event, trunc(ts), extract(hour from ts);

insert into daota.processed.tab_spiderman_capture
select n.event, n.date, n.hour, case when o.row_number is null then 0 else o.row_number end as row_number_old,
    n.row_number as row_number_new
from daota.processed.tab_spiderman_capture_new n left join daota.processed.tab_spiderman_capture_old o
    on n.event = o.event and n.date = o.date and n.hour = o.hour
union
select o.event, o.date, o.hour, o.row_number as row_number_old,
    case when n.row_number is null then 0 else n.row_number end as row_number_new
from daota.processed.tab_spiderman_capture_old o left join daota.processed.tab_spiderman_capture_new n
    on n.event = o.event and n.date = o.date and n.hour = o.hour;

update daota.processed.tab_spiderman_capture
set row_number_diff = row_number_new - row_number_old;

delete from daota.processed.tab_spiderman_capture_history where record_date = CURRENT_DATE;
insert into daota.processed.tab_spiderman_capture_history
(
    record_date
    ,event
    ,date
    ,hour
    ,row_number
)
select
    CURRENT_DATE as record_date
    ,event
    ,date
    ,hour
    ,row_number
from daota.processed.tab_spiderman_capture_new;

-- TODO: fact tables
----------------------------------------------------------------------------------------------------------------------------------------------
-- daota.processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  daota.processed.fact_session
where  date_start >= (
                     select start_date
                     from daota.processed.tmp_start_date
                );

create temp table tmp_fact_session as
select *
from   public.events
where  event in ('newuser', 'session_start')
and    trunc(ts) >= (
                     select start_date
                     from daota.processed.tmp_start_date
                    );

update tmp_fact_session
set app = 'dtl.zh.prod'
where app != 'dtl.zh.prod';

insert into daota.processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,player_key
       ,server
       ,player_id
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,browser
       ,browser_version
       ,rc_bal_start
       ,rc_bal_end
       ,coin_bal_start
       ,coin_bal_end
       ,ab_test
       ,ab_variant
       ,session_length
)
select  trunc(ts) as date_start
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,snsid
       ,MD5(app||split_part(snsid, '_', 1)||CAST(split_part(snsid, '_', 2) AS integer)) AS player_key
       ,split_part(snsid, '_', 1) AS server
       ,CAST(split_part(snsid, '_', 2) AS integer) AS player_id
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,install_source
       ,null as app_version
       ,null as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_start
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_end
       ,os
       ,os_version
       ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
       ,ip
       ,json_extract_path_text(properties,'lang') AS language
       ,substring(json_extract_path_text(properties,'device'), 1, 64) AS device
       ,json_extract_path_text(properties,'idfa') AS idfa
       ,json_extract_path_text(properties,'gaid') AS gaid
       ,json_extract_path_text(properties,'android_id') AS android_id
       ,json_extract_path_text(properties,'mac_address') AS mac_address
       ,browser
       ,browser_version
       ,null as rc_bal_start
       ,null as rc_bal_end
       ,null as coin_bal_start
       ,null as coin_bal_end
       ,null as ab_test
       ,null as ab_variant
       ,null as session_length
from   tmp_fact_session;

delete from daota.processed.fact_session where server = '60001';

----------------------------------------------------------------------------------------------------------------------------------------------
-- daota.processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  daota.processed.fact_revenue
where  date >= (
                     select start_date
                     from   daota.processed.tmp_start_date
                );

create temp table tmp_fact_revenue as
select *
from    public.events
where   event = 'payment'
and     trunc(ts) >= (
                        select start_date
                        from daota.processed.tmp_start_date
                     );

update tmp_fact_revenue
set app = 'dtl.zh.prod'
where app != 'dtl.zh.prod';

insert into daota.processed.fact_revenue
(
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,player_key
        ,server
        ,player_id
        ,ts
        ,level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,transaction_id
)
select  trunc(ts) as date
        ,MD5(app || uid)
        ,app
        ,uid
        ,snsid
        ,MD5(app||split_part(snsid, '_', 1)||CAST(split_part(snsid, '_', 2) AS integer)) AS player_key
        ,split_part(snsid, '_', 1) AS server
        ,CAST(split_part(snsid, '_', 2) AS integer) AS player_id
        ,ts
        ,CAST(
               CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'level')
               END AS integer
             ) AS level
        ,json_extract_path_text(properties,'payment_processor') AS payment_processor
        ,json_extract_path_text(properties,'product_id') AS product_id
        ,json_extract_path_text(properties,'product_name') AS product_name
        ,json_extract_path_text(properties,'product_type') AS product_type
        ,0 AS coins_in
        ,CAST(json_extract_path_text(properties,'rc_in') AS INTEGER) AS rc_in
        ,case when json_extract_path_text(properties,'currency') = '' then 'CNY' else json_extract_path_text(properties,'currency') end AS currency
        ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4)) AS amount
        ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4)) AS usd
        ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    tmp_fact_revenue;

update daota.processed.fact_revenue
set rc_in = p.rc_in,
    currency = 'USD',
    amount = p.amount,
    usd = p.amount
from daota.processed.ref_app_third_product p
where daota.processed.fact_revenue.date >= '2015-05-15' and
    daota.processed.fact_revenue.product_id = p.product_id and
    daota.processed.fact_revenue.rc_in = 0 and
    currency = 'CNY';
--just for the bingbingbi issue
update  daota.processed.fact_revenue
set product_id = 'com.funplus.zh.daota.bingbingbi',
  product_name = 'com.funplus.zh.daota.bingbingbi'
where amount = 118;
  
delete from daota.processed.fact_revenue where product_id = 'com.zeptolab.ctrbonus.superpower1';
delete from daota.processed.fact_revenue where date < (select min(dt) from public.currency);
delete from daota.processed.fact_revenue where server = '60001';
delete from daota.processed.fact_revenue where product_id = 'false';

update daota.processed.fact_revenue
set rc_in = CAST(regexp_replace(product_id, '[^0-9]*','') AS INTEGER)
where payment_processor != 'app_third' and product_id != 'com.funplus.zh.daota.bingbingbi';

update daota.processed.fact_revenue
set rc_in = p.rc_in,
    currency = p.currency,
    amount = p.amount
from daota.processed.dim_third_part_payment_product p
where daota.processed.fact_revenue.payment_processor = 'app_third' and daota.processed.fact_revenue.product_id = p.product_id;

update daota.processed.fact_revenue
set rc_in = 2178
where product_id = 'com.funplus.zh.daota.diamonds1980' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 2178
where product_id = 'com.funplus.zh.daota.diamonds1980first' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 330
where product_id = 'com.funplus.zh.daota.diamonds300' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 330
where product_id = 'com.funplus.zh.daota.diamonds300first' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 3608
where product_id = 'com.funplus.zh.daota.diamonds3280' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 66
where product_id = 'com.funplus.zh.daota.diamonds60' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 7128
where product_id = 'com.funplus.zh.daota.diamonds6480' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 7128
where product_id = 'com.funplus.zh.daota.diamonds6480first' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 1078
where product_id = 'com.funplus.zh.daota.diamonds980' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 330
where product_id = 'com.funplus.zh.daota.monthlycard300' and date >= '2015-08-06';

update daota.processed.fact_revenue
set rc_in = 330
where product_id = 'com.funplus.zh.daota.submonthlycard300' and date >= '2015-08-06';

update daota.processed.fact_revenue
set vip_exp_get = rc_in;

-- double vip_exp_get in promotion period
update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-05-23 21:00:00' and ts < '2015-05-30 21:00:00' and
    server >= '12001' and server <= '12052';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-06-06 21:00:00' and ts < '2015-06-13 21:00:00' and
    server >= '12001' and server <= '12054';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-06-20 21:00:00' and ts < '2015-06-27 21:00:00' and
    server >= '12001' and server <= '12056';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-04 21:00:00' and ts < '2015-07-11 21:00:00' and
    server >= '12001' and server <= '12058';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-18 21:00:00' and ts < '2015-07-25 21:00:00' and
    server >= '12001' and server <= '12060';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-31 21:00:00' and ts < '2015-08-31 21:00:00';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-05-28 03:00:00' and ts < '2015-06-04 03:00:00' and
    server = '12053';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-06-11 03:00:00' and ts < '2015-06-14 03:00:00' and
    server = '12055';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-06-18 03:00:00' and ts < '2015-06-21 03:00:00' and
    server = '12056';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-06-25 03:00:00' and ts < '2015-07-01 03:00:00' and
    server = '12057';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-02 03:00:00' and ts < '2015-07-08 03:00:00' and
    server = '12058';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-09 03:00:00' and ts < '2015-07-15 03:00:00' and
    server = '12059';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-16 03:00:00' and ts < '2015-07-22 03:00:00' and
    server = '12060';

update daota.processed.fact_revenue
set vip_exp_get = rc_in * 2
where ts >= '2015-07-28 03:00:00' and ts < '2015-08-03 03:00:00' and
    server = '12061';

-- remove duplicated records by transaction_id
create temp table tmp_duplicated_transaction_id as
select transaction_id, count(1)
from daota.processed.fact_revenue
group by transaction_id
having count(1) > 1;

create temp table tmp_duplicated_transaction_id_rows as
select t.*, row_number() over (partition by transaction_id order by ts) rank
from
(select r.*
from daota.processed.fact_revenue r join tmp_duplicated_transaction_id d on r.transaction_id = d.transaction_id) t;

create temp table tmp_duplicated_transaction_id_first_row as
select *
from tmp_duplicated_transaction_id_rows
where rank = 1;

alter table tmp_duplicated_transaction_id_first_row drop column rank;

delete
from daota.processed.fact_revenue
using tmp_duplicated_transaction_id d
where daota.processed.fact_revenue.transaction_id = d.transaction_id;

insert into daota.processed.fact_revenue
select *
from tmp_duplicated_transaction_id_first_row;

update daota.processed.fact_revenue
set usd = amount * case when c.factor is null then 0 else c.factor end
from public.currency c
where daota.processed.fact_revenue.currency = c.currency and daota.processed.fact_revenue.date = c.dt;

insert into daota.processed.fact_revenue
(
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,player_key
        ,server
        ,player_id
        ,ts
        ,level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,transaction_id
)
select
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,player_key
        ,server
        ,player_id
        ,ts
        ,level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,transaction_id
from daota.processed.fact_revenue_3rd_history
where date >= (
                select start_date
                from   daota.processed.tmp_start_date
              );

update daota.processed.fact_revenue
set usd_iap = usd
where payment_processor != 'app_third' and
    date >= (
                select start_date
                from   daota.processed.tmp_start_date
            );

update daota.processed.fact_revenue
set usd_3rd = usd
where payment_processor = 'app_third' and
    date >= (
                select start_date
                from   daota.processed.tmp_start_date
            );

-- daily_level
delete from  daota.processed.daily_level
where  date >= (
                     select start_date
                     from   daota.processed.tmp_start_date
                );

create temp table player_level as
select trunc(ts) as date
        ,MD5(app || uid) as user_key
        ,app
        ,uid
        ,snsid
        ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level
from events
where trunc(ts) >= (
                        select start_date
                        from daota.processed.tmp_start_date
                   );

insert into daota.processed.daily_level
(
    date
    ,user_key
    ,app
    ,uid
    ,snsid
    ,min_level
    ,max_level
)
select
    date
    ,user_key
    ,app
    ,uid
    ,snsid
    ,min(level) as min_level
    ,max(level) as max_level
from player_level
group by 1,2,3,4,5;

-- TODO: Remove duplicated records from adjust table
drop table if exists unique_adjust;
create table unique_adjust as
select *
from
(select *, row_number() over (partition by adid order by ts) as row_number
from adjust)  t
where row_number = 1;

update unique_adjust
set userid = a.userid
from adjust a
where unique_adjust.userid = '' and a.userid != '' and unique_adjust.adid = a.adid;

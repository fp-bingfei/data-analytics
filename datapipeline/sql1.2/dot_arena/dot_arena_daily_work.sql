-- TODO: check revenue
drop table if exists processed.revenue_android_siyuan;
CREATE TABLE processed.revenue_android_siyuan
(
  order_number         VARCHAR(128) ENCODE LZO,
  date                 DATE ENCODE DELTA,
  ts                   VARCHAR(16) ENCODE LZO,
  status               VARCHAR(128) ENCODE BYTEDICT,
  device               VARCHAR(128) ENCODE BYTEDICT,
  product_title        VARCHAR(128) ENCODE BYTEDICT,
  product_id           VARCHAR(128) ENCODE BYTEDICT,
  product_type         VARCHAR(128) ENCODE BYTEDICT,
  sku_id               VARCHAR(128) ENCODE LZO,
  currency             VARCHAR(128) ENCODE BYTEDICT,
  item_price           VARCHAR(128) ENCODE BYTEDICT,
  tax                  VARCHAR(128) ENCODE BYTEDICT,
  charge_amount        VARCHAR(128) ENCODE BYTEDICT,
  city                 VARCHAR(128) ENCODE BYTEDICT,
  buyer_status         VARCHAR(256) ENCODE LZO,
  post_code            VARCHAR(128) ENCODE BYTEDICT,
  country              VARCHAR(16) ENCODE BYTEDICT
)
  DISTKEY(order_number)
  SORTKEY(order_number);

copy processed.revenue_android_siyuan
from 's3://com.funplus.bitest/dota/check_revenue/salesreport_201505.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
CSV
IGNOREHEADER 1;


select *
from daota.pg_catalog.stl_load_errors
limit 100;

select *
from processed.revenue_android_siyuan
limit 100;

drop table if exists processed.revenue_android_siyuan_normalized;
create table processed.revenue_android_siyuan_normalized as
select
	date,
	dateadd(second, CAST(ts AS INTEGER), '1970-01-01 00:00:00') as ts,
	sku_id as product_id,
	country,
	item_price,
	charge_amount,
	s.currency,
	cast(regexp_replace(s.item_price, ',', '') as decimal(14,4)) * case when c.factor is null then 1 else c.factor end as usd,
	order_number
from processed.revenue_android_siyuan s
	join currency c on s.currency = c.currency and s.date = c.dt
where product_id = 'com.funplus.zh.daota';

select date, sum(usd)
from processed.revenue_android_siyuan_normalized
group by 1
limit 100;

select count(distinct order_number), count(1)
from processed.revenue_android_siyuan_normalized
limit 100;

select count(1)
from processed.fact_revenue r
where r.ts >= '2015-05-01' and r.payment_processor = 'app_android';

select r.*
from processed.fact_revenue r
	left join processed.revenue_android_siyuan_normalized s on substring(r.transaction_id, 24, 16) = s.order_number
where r.ts >= '2015-05-01' and r.payment_processor = 'app_android'	and s.order_number is null;

select r.date, count(1), sum(r.usd)
from processed.fact_revenue r
	left join processed.revenue_android_siyuan_normalized s on substring(r.transaction_id, 24, 16) = s.order_number
where r.ts >= '2015-05-01' and r.payment_processor = 'app_android'	and s.order_number is null
group by 1
order by 1;

select r.uid, count(1), sum(r.usd)
from processed.fact_revenue r
	left join processed.revenue_android_siyuan_normalized s on substring(r.transaction_id, 24, 16) = s.order_number
where r.ts >= '2015-05-01' and r.payment_processor = 'app_android'	and s.order_number is null
group by 1
order by 1;

select *
from processed.revenue_android_siyuan_normalized s
	left join processed.fact_revenue r on substring(r.transaction_id, 24, 16) = s.order_number
where r.transaction_id is null;

select count(1)
from processed.revenue_android_siyuan_normalized;

select r.ts, s.ts, r.amount, r.currency, r.usd, s.currency, s.item_price, s.usd
from processed.fact_revenue r
	join processed.revenue_android_siyuan_normalized s on substring(r.transaction_id, 24, 16) = s.order_number
limit 100;

select count(1)
from processed.fact_revenue r
	join processed.revenue_android_siyuan_normalized s on substring(r.transaction_id, 24, 16) = s.order_number;

select r.ts, s.ts, r.currency, r.usd, s.currency, s.item_price
from processed.fact_revenue r
	join processed.revenue_android_siyuan_normalized s on substring(r.transaction_id, 24, 16) = s.order_number
	join currency c on s.currency = c.currency and s.date = c.dt;

insert into processed.ref_app_third_product (product_id, rc_in, amount)
values
    ('10201',35   ,0.62  ),
    ('10202',45   ,0.775 ),
    ('10203',90   ,1.55  ),
    ('10241',105  ,1.86  ),
    ('10204',170  ,2.79  ),
    ('10205',190  ,3.1   ),
    ('10206',290  ,4.65  ),
    ('10242',380  ,6.20  ),
    ('10207',580  ,9.3   ),
    ('10208',1000 ,15.5  ),
    ('10209',2160 ,31    ),
    ('10210',4740 ,62    ),
    ('10211',7180 ,93    ),
    ('10212',8460 ,108.5 ),
    ('10213',12160,155   ),
    ('10214',60   ,0.99  ),
    ('10215',120  ,1.99  ),
    ('10216',180  ,2.99  ),
    ('10217',300  ,4.99  ),
    ('10218',620  ,9.99  ),
    ('10219',1320 ,19.99 ),
    ('10220',2090 ,29.99 ),
    ('10221',3690 ,49.99 ),
    ('10222',7700 ,99.99 ),
    ('10223',170  ,2.87  ),
    ('10224',350  ,5.75  ),
    ('10225',530  ,8.62  ),
    ('10226',720  ,11.5  ),
    ('10240',785  ,12.50 ),
    ('10227',920  ,14.37 ),
    ('10228',2000 ,28.74 ),
    ('10229',4390 ,57.49 ),
    ('10230',6660 ,86.23 ),
    ('10231',11280,143.72),
    ('10232',240  ,4     ),
    ('10233',490  ,8     ),
    ('10234',1030 ,16    ),
    ('10235',1600 ,24    ),
    ('10236',2230 ,32    ),
    ('10237',2890 ,40    ),
    ('10238',3560 ,48    ),
    ('10239',6160 ,80    ),
    ('10243',50   ,0.76  ),
    ('10244',60   ,1.03  ),
    ('10245',80   ,1.38  ),
    ('10246',120  ,1.53  ),
    ('10247',140  ,1.72  ),
    ('10248',200  ,2.68  ),
    ('10249',300  ,3.82  ),
    ('10250',400  ,4.97  ),
    ('10251',600  ,7.65  ),
    ('10252',1000 ,13.38 ),
    ('10253',1200 ,15.29 ),
    ('10254',1500 ,19.11 ),
    ('10255',1800 ,22.94 ),
    ('10256',3000 ,38.23 ),
    ('10257',4200 ,53.38 ),
    ('10258',6200 ,76.26 );

-- create new database account
select * from pg_user;

create schema dota_pm;

grant usage on schema dota_pm to guoguang;
grant create on schema dota_pm to guoguang;
grant all on all tables in schema dota_pm to guoguang;

grant usage on schema processed to guoguang;
grant select on all tables in schema processed to guoguang;

grant usage on schema public to guoguang;
grant select on all tables in schema public to guoguang;

revoke all on all tables in schema processed from guoguang;
revoke all on all tables in schema public from guoguang;

--multiple mappings idfa*uid 
drop table if exists processed.uid_idfa_cross_validation;
create table processed.uid_idfa_cross_validation as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid
from processed.dim_user u
  join processed.user_device_list d on u.uid = d.funplus_id
where u.level >= 5 and (idfa != '' or google_aid != '');

select * from processed.fact_session limit 100;
select count(1) from processed.uid_idfa_cross_validation
select count(1) from (select distinct idfa from processed.uid_idfa_cross_validation)
select count(1) from (select distinct funplus_id from processed.uid_idfa_cross_validation)
select count(1) from (select idfa from processed.uid_idfa_cross_validation group by idfa having count(funplus_id) > 5 );
select count(1) from (select funplus_id from processed.uid_idfa_cross_validation group by funplus_id having count(idfa) > 5 );

--Somoto uid checking
select a.adid as device_id, a.ip_address as ip, date(a.ts) as install_date, a.country, a.userid::varchar as adjust_uid, d.uid::varchar as fp_uid
from adjust a
    left outer join processed.dim_user d on a.userid = d.uid
where tracker_name like '%Somoto%' ;


--Users who played the game more than 5 times in the last 30 days
create temp table 30days5active as 
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid
FROM
  (select user_key, max(uid) as uid from processed.fact_dau_snapshot where date > CURRENT_DATE - 30 group by user_key having count(1) > 5) f 
  join processed.user_device_list d on f.uid = d.funplus_id
where  (google_aid != '' or idfa != '');

unload ('select * from 30days5active;')
to 's3://com.funplus.bitest/dota/LAL/30days5active_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


--Users who purchased in the past 30 days
create temp table purchasedin30days as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid
FROM
  (select user_key, max(uid) as uid from processed.fact_dau_snapshot where date > CURRENT_DATE - 30 group by user_key having sum(revenue_usd) > 0) f 
  join processed.user_device_list d on f.uid = d.funplus_id
where  (google_aid != '' or idfa != '');
unload ('select * from purchasedin30days;')
to 's3://com.funplus.bitest/dota/LAL/purchasedin30days_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--Users who purchased in the past 90 days
create temp table purchasedin90days as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid
FROM
  (select user_key, max(uid) as uid from processed.fact_dau_snapshot where date > CURRENT_DATE - 90 group by user_key having sum(revenue_usd) > 0) f 
  join processed.user_device_list d on f.uid = d.funplus_id
where  (google_aid != '' or idfa != '');
unload ('select * from purchasedin90days;')
to 's3://com.funplus.bitest/dota/LAL/purchasedin90days_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--Top 10K users in the last 30 days (by level)
create temp table top10klast30days as
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid
FROM
  (select user_key, max(uid) as uid from processed.fact_dau_snapshot where date > CURRENT_DATE - 30 group by user_key order by max(level_end) desc limit 10000) f 
  join processed.user_device_list d on f.uid = d.funplus_id
where  (google_aid != '' or idfa != '');
unload ('select * from top10klast30days;')
to 's3://com.funplus.bitest/dota/LAL/top10klast30days_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--Users who reached level 65 in the game
create temp table levelabove65 as 
select distinct d.funplus_id, upper(d.idfa) as idfa, upper(d.google_aid) as google_aid
FROM
  (select user_key, uid from processed.dim_user where level >= 65) f 
  join processed.user_device_list d on f.uid = d.funplus_id
where  (google_aid != '' or idfa != '');
unload ('select * from levelabove65;')
to 's3://com.funplus.bitest/dota/re_target/levelabove65_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

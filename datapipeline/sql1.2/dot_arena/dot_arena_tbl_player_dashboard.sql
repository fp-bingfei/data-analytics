--------------------------------------------------------------------------------------------------------------------------------------------
--Dot_Arena tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate kpi,retention and LTV tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_player_kpi
---------------------------------------------------------------------------------------------------------------------------------------------- 

------delete the historical data in case repeat loading 
truncate table processed.agg_player_kpi;

insert into processed.agg_player_kpi
(
      date
      ,server
      ,app 
      ,app_version 
      ,install_source
      ,browser
      ,country
      ,os
      ,language
      ,is_payer
      ,ab_test
      ,vip_level
      ,new_players
      ,dau
      ,new_payers
      ,today_payers
      ,revenue
      ,revenue_iap
      ,revenue_3rd
      ,session_cnt
) 
select  date 
      ,d.server
      ,d.app
      ,d.app_version 
      ,u.install_source
      ,d.browser
      ,case when d.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then d.country else 'Other' end as country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,'vip_' || cast(d.vip_level as varchar) as vip_level
      ,sum(d.is_new_player) as new_players
      ,count(d.player_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(d.revenue_usd_iap) as revenue_iap
      ,sum(d.revenue_usd_3rd) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_player_dau_snapshot d
join processed.dim_player u
on d.player_key = u.player_key
where date >= '2014-08-01'
group by 1,2,3,4,5,6,7,8,9,10,11,12;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_player_retention
---------------------------------------------------------------------------------------------------------------------------------------------- 

-- create temp table for player days
drop table if exists player_day;
create temp table player_day as
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day
union all
select 120 as day;


-- create temp table for new users in each install_date
create temp table tmp_new_player as
select  u.install_date
       ,u.server
       ,u.os
       ,u.country
       ,u.app
       ,u.install_source
       ,u.is_payer
       ,u.vip_level
       ,count(u.player_key) as new_players
from  processed.dim_player u
where u.install_date >= '2014-08-01'
group by 1,2,3,4,5,6,7,8;

-- create temp table to get the last date in dau table.
drop table if exists last_date;
create temporary table last_date as
select max(date) as last_date
from processed.fact_player_dau_snapshot;

-- create cube to calculate the retention
drop table if exists player_day_cube;
create temp table player_day_cube as
select rd.day as retention_days
      ,nu.install_date
      ,nu.server
      ,nu.os
      ,nu.country
      ,nu.app
      ,nu.install_source
      ,nu.is_payer
      ,nu.vip_level
      ,nu.new_players
from tmp_new_player nu
join player_day rd on 1 = 1
join last_date d on 1=1
where DATEDIFF('day', nu.install_date, d.last_date) >= rd.day;

-- insert into agg_player_retention table
truncate table processed.agg_player_retention;
insert into processed.agg_player_retention
(
  retention_days
  ,install_date
  ,server
  ,os
  ,country
  ,app
  ,install_source
  ,is_payer
  ,vip_level
  ,new_players
  ,retained
)
select
           cube.retention_days
          ,cube.install_date
          ,cube.server
          ,cube.os
          ,case when cube.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then cube.country else 'Other' end as country
          ,cube.app
          ,cube.install_source
          ,cube.is_payer
          ,cube.vip_level
          ,cube.new_players
          ,coalesce(r.retained, 0) as retained
from      player_day_cube cube
left join
          (
             SELECT   DATEDIFF('day', du.install_date, du.date) AS retention_days
                     ,u.install_date
                     ,u.server
                     ,u.os
                     ,u.country
                     ,u.app
                     ,u.install_source
                     ,u.is_payer
                     ,u.vip_level
                     ,count(distinct du.player_key) as retained
             FROM    processed.fact_player_dau_snapshot du
             join    processed.dim_player u on du.player_key = u.player_key
             where   DATEDIFF('day', du.install_date, du.date) in
                                     (
                                       select day
                                       from player_day
                                      )
             group by 1,2,3,4,5,6,7,8,9
          ) r
on  cube.retention_days = r.retention_days
    and cube.install_date = r.install_date
    and cube.server = r.server
    and cube.os = r.os
    and cube.country = r.country
    and cube.app = r.app
    and cube.install_source = r.install_source
    and cube.is_payer = r.is_payer
    and cube.vip_level = r.vip_level;


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.agg_player_ltv
----------------------------------------------------------------------------------------------------------------------------------------------

-- create ltv cube table
create temp table agg_player_ltv_cube as
SELECT      d.day as ltv_days
           ,trunc(u.install_ts) as install_date
           ,u.server
           ,u.os
           ,u.country
           ,u.app
           ,u.install_source
           ,u.is_payer
           ,u.vip_level
           ,u.player_key
FROM        processed.dim_player u
     join   player_day d
       on   DATEDIFF('day', u.install_date, (select last_date from last_date)) >= d.day;

-- insert into agg_player_ltv table
truncate table processed.agg_player_ltv;
insert into processed.agg_player_ltv
(
  ltv_days
  ,install_date
  ,server
  ,os
  ,country
  ,app
  ,install_source
  ,is_payer
  ,vip_level
  ,players
  ,revenue
)
SELECT
            c.ltv_days
           ,c.install_date
           ,c.server
           ,c.os
           ,case when c.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then c.country else 'Other' end as country
           ,c.app
           ,c.install_source
           ,c.is_payer
           ,c.vip_level
           ,count(distinct c.player_key) as players
           ,sum(COALESCE(du.revenue_usd, 0)) as revenue
FROM        agg_player_ltv_cube c
left join   processed.fact_player_dau_snapshot du
       on   c.player_key = du.player_key and DATEDIFF('day', c.install_date, du.date) <= c.ltv_days
group by 1,2,3,4,5,6,7,8,9;

delete from processed.agg_player_ltv where install_date < '2014-08-01';

delete from processed.agg_player_ltv
using last_date d
where DATEDIFF('day', install_date, d.last_date) < ltv_days;

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.tab_iap (iap report)
----------------------------------------------------------------------------------------------------------------------------------------------
delete from processed.agg_player_iap
where date >=(
                select start_date
                from daota.processed.tmp_start_date
             );

insert into processed.agg_player_iap
(
    date
    ,app
    ,date_str
    ,date_week_str
    ,date_month_str
    ,level
    ,install_date
    ,install_source
    ,server
    ,os
    ,country
    ,ab_test
    ,ab_variant
    ,conversion_purchase
    ,product_type
    ,product_id
    ,revenue_usd
    ,purchase_cnt
    ,purchase_user_cnt
)
SELECT
    r.date
    ,r.app
    ,regexp_replace( substring(r.date, 1, 10), '-', '/' ) as date_str
    ,regexp_replace( substring(r.date - extract(dayofweek from r.date), 1, 10), '-', '/' ) as date_week_str
    ,regexp_replace( substring(r.date - extract(day from r.date) + 1, 1, 7), '-', '/' ) as date_month_str
    ,r.level
    ,u.install_date as install_date
    ,u.install_source as install_source
    ,s.server
    ,s.os
    ,case when s.country in ('Malaysia', 'Singapore', 'Thailand', 'Philippines', 'Indonesia') then s.country else 'Other' end as country
    ,s.ab_test
    ,s.ab_variant
    ,case when u.conversion_ts = r.ts then 1 else 0 end as conversion_purchase
    ,product_type
    ,product_id
    ,sum(usd) as revenue_usd
    ,count(1) as purchase_cnt
    ,count(distinct r.player_key) as purchase_user_cnt
FROM processed.fact_revenue r
    join processed.fact_player_dau_snapshot s on r.player_key = s.player_key and r.date = s.date
    join processed.dim_player u on u.player_key = r.player_key
where r.date >= (select start_date from daota.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16;


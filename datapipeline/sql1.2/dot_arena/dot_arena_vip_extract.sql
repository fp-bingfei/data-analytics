-- Extract Vip15 Top up information
select t1.uid as funplus_id, t1.server, t1.player_id, t1.vip_level,
t1.level, t1.country, t1.os, t1.install_source, t2.total_rc_in
from
(select *
from processed.dim_user_max_level_server
where vip_level = 'vip_15') t1
join
(select user_key, sum(rc_in) as total_rc_in
from processed.fact_revenue
where date <= '2015-02-08'
group by user_key
having sum(rc_in) >= 150000) t2
on t1.user_key = t2.user_key;

-- Extract Vip11 Top up information
select t1.uid as funplus_id, t1.server, t1.player_id, t1.vip_level,
t1.level, t1.country, t1.os, t1.install_source, t2.total_rc_in
from
(select *
from processed.dim_user_max_level_server
where vip_level != 'vip_0') t1
join
(select user_key, sum(rc_in) as total_rc_in
from processed.fact_revenue
where date <= '2015-02-15'
group by user_key
having sum(rc_in) >= 15000) t2
on t1.user_key = t2.user_key;


-- TODO: 2015-03-25: Extract Vip15 palyers Top up information
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_rc_in
from
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-03-25 16:00:00'
group by 1,2,3
having sum(rc_in) >= 150000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

-- Part 3: Information extract - After Promo
-- 1. Top information for VIP 15 - between 29/3/2015 05:00 ~ 5/4/2015 05:00
-- above 10000 diamonds and above 30000 diamonds

create temp table promotion_users as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-04-04 21:00:00'
group by 1,2,3
having sum(rc_in) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-03-28 21:00:00' and r.ts < '2015-04-04 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;

-- Extract payers data btw 3/8/2015 and current
create table export_data as
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_rc_in
from
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts >= '2015-03-07 16:00:00'
group by 1,2,3) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

unload ('select * from export_data;')
to 's3://com.funplus.bitest/dota/payer/payer_list_0308_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
DELIMITER '\t'
GZIP;

drop table export_data;

-- TODO: Extract all VIP14 and VIP15 players' Basic information Cut of 23/4/2015 23:59
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_rc_in
from
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-04-23 16:00:00'
group by 1,2,3
having sum(rc_in) >= 80000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

-- TODO: 2015-05-04
-- Part 3: Information extract - After Promo
-- 1. Top information for VIP 15 - between 26/4/2015 05:00 ~ 3/5/2015 05:00
-- above 10000 diamonds and above 30000 diamonds

create temp table promotion_users as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-02 21:00:00'
group by 1,2,3
having sum(rc_in) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-04-25 21:00:00' and r.ts < '2015-05-02 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;


-- TODO: 2015-05-21
-- Part 2: Information extract - Before Promo.
-- Extract all VIP14 and VIP15 players' Basic information Cut of 21/5/2015 23:59
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_rc_in
from
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-21 16:00:00'
group by 1,2,3
having sum(rc_in) >= 80000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;


-- TODO: 请抓一下5月24日早上05:00（包括05:00）之前成为VIP11-15的全部玩家的ID。请根据账号ID抓，不根据Funplus ID抓。
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_rc_in
from
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts <= '2015-05-23 21:00:00'
group by 1,2,3
having sum(rc_in) >= 15000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

-- TODO: 2015-06-01
-- Part 3: Information extract - After Promo
-- 1. Top information for VIP 15 - between 24/5/2015 05:00 ~ 31/5/2015 05:00
-- above 10000 diamonds and above 30000 diamonds
create temp table promotion_users_old as
select r.uid, r.server, r.player_id,
    sum(case when r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00' then rc_in / 2 else rc_in end) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id,
    sum(case when ts >= '2015-05-23 21:00:00' and ts < '2015-05-30 21:00:00' then rc_in / 2 else rc_in end) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-30 21:00:00'
group by 1,2,3
having sum(case when ts >= '2015-05-23 21:00:00' and ts < '2015-05-30 21:00:00' then rc_in / 2 else rc_in end) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00'
group by 1,2,3;

create temp table promotion_users_new as
select r.uid, r.server, r.player_id,
    sum(case when r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00' then rc_in / 2 else rc_in end) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-30 21:00:00'
group by 1,2,3
having sum(rc_in) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00'
group by 1,2,3;

drop table if exists promotion_users_old_30000;
create table promotion_users_old_30000 as
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users_old t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select count(1) from promotion_users_old_30000;

drop table if exists promotion_users_old_10000;
create table promotion_users_old_10000 as
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users_old t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;

select count(1) from promotion_users_old_10000;

drop table if exists promotion_users_new_30000;
create table promotion_users_new_30000 as
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users_new t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select count(1) from promotion_users_new_30000;

drop table if exists promotion_users_new_10000;
create table promotion_users_new_10000 as
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users_new t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;

select count(1) from promotion_users_new_10000;

drop table if exists promotion_users_diff_30000;
create table promotion_users_diff_30000 as
select t1.*
from promotion_users_new_30000 t1
    left join promotion_users_old_30000 t2 on t1.funplus_id = t2.funplus_id and t1.server = t2.server and t1.player_id = t2.player_id
where t2.funplus_id is null;

drop table if exists promotion_users_diff_10000;
create table promotion_users_diff_10000 as
select t1.*
from promotion_users_new_10000 t1
    left join promotion_users_old_10000 t2 on t1.funplus_id = t2.funplus_id and t1.server = t2.server and t1.player_id = t2.player_id
where t2.funplus_id is null;


-- TODO: 请抓一下5月24日早上5:01（不包括5:01）之前不是VIP11或以上，在5月24日早上5:01（包括5:01）--5月31日早上5:00（包括5:00）成为VIP11或以上的全部玩家账号ID。
create temp table vip_11_player_0523 as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-23 21:00:00'
group by 1,2,3
having sum(rc_in) >= 15000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00'
group by 1,2,3;

create temp table vip_11_player_0530 as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-30 21:00:00'
group by 1,2,3
having sum(rc_in) >= 15000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00'
group by 1,2,3;

create temp table vip_11_player_0530_new as
select r.uid, r.server, r.player_id, sum(r.rc_in * 2) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id,
    sum(case when ts >= '2015-05-23 21:00:00' and ts < '2015-05-30 21:00:00' then rc_in * 2 else rc_in end) as total_rc_in
from processed.fact_revenue
where ts < '2015-05-30 21:00:00'
group by 1,2,3
having sum(case when ts >= '2015-05-23 21:00:00' and ts < '2015-05-30 21:00:00' then rc_in * 2 else rc_in end) >= 15000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-05-23 21:00:00' and r.ts < '2015-05-30 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, v1.promotion_rc_in
from vip_11_player_0530 v1
    left join vip_11_player_0523 v2 on v1.uid = v2.uid and v1.server = v2.server and v1.player_id = v2.player_id
    join processed.dim_player p on v1.uid = p.uid and v1.server = p.server and v1.player_id = p.player_id
where v2.uid is null;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, v1.promotion_rc_in
from vip_11_player_0530_new v1
    left join vip_11_player_0523 v2 on v1.uid = v2.uid and v1.server = v2.server and v1.player_id = v2.player_id
    join processed.dim_player p on v1.uid = p.uid and v1.server = p.server and v1.player_id = p.player_id
where v2.uid is null;

select t1.*
from
(select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, v1.promotion_rc_in
from vip_11_player_0530_new v1
    left join vip_11_player_0523 v2 on v1.uid = v2.uid and v1.server = v2.server and v1.player_id = v2.player_id
    join processed.dim_player p on v1.uid = p.uid and v1.server = p.server and v1.player_id = p.player_id
where v2.uid is null) t1
    left join
(select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, v1.promotion_rc_in
from vip_11_player_0530 v1
    left join vip_11_player_0523 v2 on v1.uid = v2.uid and v1.server = v2.server and v1.player_id = v2.player_id
    join processed.dim_player p on v1.uid = p.uid and v1.server = p.server and v1.player_id = p.player_id
where v2.uid is null) t2 on t1.funplus_id = t2.funplus_id and t1.server = t2.server and t1.player_id = t2.player_id
where t2.funplus_id is null;

-- TODO: 2015-06-18
-- Part 2: Information extract - Before Promo.
-- Extract basic information for VIP 14 and 15 - Part 2. Cut of 18/6/2015 23:59
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_vip_exp_get, t.total_rc_in
from
(select uid, server, player_id, sum(vip_exp_get) as total_vip_exp_get, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-06-18 16:00:00'
group by 1,2,3
having sum(vip_exp_get) >= 80000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

-- TODO: Server 56， 6月18日 11am-2pm期间 付费超过25RMB的玩家 ID
select uid as funplus_id, server, player_id, currency, sum(amount) as revenue_rmb
from processed.fact_revenue
where server = '12056' and ts >= '2015-06-18 03:00:00' and ts <= '2015-06-18 06:00:00'
group by 1,2,3,4
having sum(amount) > 25;

-- TODO: 2015-06-27
-- Part 3: Information extract - After Promo
-- 1. Top information for VIP 15 - between 21/6/2015 05:00 ~ 28/6/2015 05:00
-- above 10000 diamonds and above 30000 diamonds
create temp table promotion_users as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(vip_exp_get) as total_vip_exp_get
from processed.fact_revenue
where ts < '2015-06-27 21:00:00'
group by 1,2,3
having sum(vip_exp_get) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-06-20 21:00:00' and r.ts < '2015-06-27 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;

-- TODO: 2015-07-17
-- Part 2: Information extract - Before Promo.
-- Extract basic information for VIP 14 and 15 - Part 2. Cut of 16/7/2015 23:59
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_vip_exp_get, t.total_rc_in
from
(select uid, server, player_id, sum(vip_exp_get) as total_vip_exp_get, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-07-16 16:00:00'
group by 1,2,3
having sum(vip_exp_get) >= 80000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

-- TODO: 2015-07-25
-- Part 3: Information extract - After Promo
-- 1. Top information for VIP 15 - between 19/7/2015 05:00 ~ 26/7/2015 05:00
-- above 10000 diamonds and above 30000 diamonds
create temp table promotion_users as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(vip_exp_get) as total_vip_exp_get
from processed.fact_revenue
where ts < '2015-07-25 21:00:00'
group by 1,2,3
having sum(vip_exp_get) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-07-18 21:00:00' and r.ts < '2015-07-25 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;

-- TODO: 2015-08-14
-- Part 2: Information extract - Before Promo.
-- Extract basic information for VIP 14 and 15 - Part 2. Cut of 13/8/2015 23:59
select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.total_vip_exp_get, t.total_rc_in
from
(select uid, server, player_id, sum(vip_exp_get) as total_vip_exp_get, sum(rc_in) as total_rc_in
from processed.fact_revenue
where ts < '2015-08-13 16:00:00'
group by 1,2,3
having sum(vip_exp_get) >= 80000) t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id;

-- TODO: 2015-08-25
-- Part 3: Information extract - After Promo
-- 1. Top information for VIP 15 - between 16/8/2015 05:00 ~ 23/8/2015 05:00
-- above 10000 diamonds and above 30000 diamonds
create temp table promotion_users as
select r.uid, r.server, r.player_id, sum(r.rc_in) as promotion_rc_in
from processed.fact_revenue r
join
(select uid, server, player_id, sum(vip_exp_get) as total_vip_exp_get
from processed.fact_revenue
where ts < '2015-08-22 21:00:00'
group by 1,2,3
having sum(vip_exp_get) >= 150000) t
on r.uid = t.uid and r.server = t.server and r.player_id = t.player_id
where r.ts >= '2015-08-15 21:00:00' and r.ts < '2015-08-22 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;


-- TODO: 2015-09-21
-- Part 3: Information extract - After Promo
-- 1. Top information f- between UTC 2015/09/12 21:00 － 2015/09/19 21:00
-- above 10000 diamonds and above 30000 diamonds

create temp table promotion_users as
select uid, server, player_id, sum(rc_in) as promotion_rc_in
from processed.fact_revenue 
where ts >= '2015-09-12 21:00:00' and ts < '2015-09-19 21:00:00'
group by 1,2,3;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 30000;

select p.uid as funplus_id, p.server, p.player_id, p.vip_level,
p.level, p.country, p.os, p.install_source, t.promotion_rc_in
from promotion_users t
join processed.dim_player p on t.uid = p.uid and t.server = p.server and t.player_id = p.player_id
where t.promotion_rc_in >= 10000 and t.promotion_rc_in < 30000;

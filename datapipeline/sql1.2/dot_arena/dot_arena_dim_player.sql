--------------------------------------------------------------------------------------------------------------------------------------------
--Dot_Arena dim player
--Version 1.2
--Author Robin
/**
Description:
This script is generate dim_player tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_player
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table temp_dim_player_latest
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  player_key                VARCHAR(32) NOT NULL ENCODE LZO,
  server                    VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  player_id                 INTEGER NOT NULL,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT ,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_revenue_usd_iap     DECIMAL(14,4),
  total_revenue_usd_3rd     DECIMAL(14,4),
  total_rc_in               INTEGER DEFAULT 0,
  total_vip_exp_get         INTEGER DEFAULT 0,
  vip_level                 VARCHAR(16) ENCODE BYTEDICT,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(player_key)
SORTKEY(player_key, app, server, player_id);


insert into temp_dim_player_latest
(
          user_key
         ,app
         ,uid 
         ,snsid
         ,player_key
         ,server
         ,player_id
         ,language
         ,birth_date 
         ,gender 
         ,app_version
         ,level
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,is_payer
)
select    user_key 
         ,app
         ,uid
         ,snsid
         ,player_key
         ,server
         ,player_id
         ,language 
         ,null as birth_date 
         ,null as gender 
         ,null as app_version
         ,level_end
         ,os 
         ,os_version 
         ,country_code
         ,country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
from  (
         select *
                ,row_number() over (partition by player_key order by date desc) as row
         from processed.fact_player_dau_snapshot
         where date >= (
                        select start_date
                        from daota.processed.tmp_start_date
                     )
      )t
where t.row = 1;

-- create temp table to get the min install_ts
create temp table temp_player_min_install_ts as
select s.player_key, min(s.install_ts) as install_ts
from processed.fact_player_dau_snapshot s
where date >= (
                    select start_date
                    from daota.processed.tmp_start_date
              )
group by s.player_key;

update temp_dim_player_latest
set install_ts = t.install_ts,
    install_date = trunc(t.install_ts)
from temp_player_min_install_ts t
where temp_dim_player_latest.player_key = t.player_key;

-- create temp table to get the revenue for each player till now + conversion + rc_in + vip level
create temp table temp_player_last_payment as
select u.player_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
       ,sum(usd_iap) as revenue_iap
       ,sum(usd_3rd) as revenue_3rd
       ,sum(rc_in) as rc_in
       ,sum(vip_exp_get) as vip_exp_get
from temp_dim_player_latest u
join processed.fact_revenue r on r.player_key=u.player_key
where r.date < CURRENT_DATE
group by 1;

update temp_dim_player_latest
set total_revenue_usd = t.revenue,
    total_revenue_usd_iap = t.revenue_iap,
    total_revenue_usd_3rd = t.revenue_3rd,
    total_rc_in = t.rc_in,
    total_vip_exp_get = t.vip_exp_get,
    conversion_ts = t.conversion_ts
from temp_player_last_payment t
where temp_dim_player_latest.player_key = t.player_key;

update temp_dim_player_latest
set vip_level =
        case
            when total_vip_exp_get < 10 then 'vip_0'
            when total_vip_exp_get >= 10 and total_vip_exp_get < 100 then 'vip_1'
            when total_vip_exp_get >= 100 and total_vip_exp_get < 300 then 'vip_2'
            when total_vip_exp_get >= 300 and total_vip_exp_get < 500 then 'vip_3'
            when total_vip_exp_get >= 500 and total_vip_exp_get < 1000 then 'vip_4'
            when total_vip_exp_get >= 1000 and total_vip_exp_get < 2000 then 'vip_5'
            when total_vip_exp_get >= 2000 and total_vip_exp_get < 3000 then 'vip_6'
            when total_vip_exp_get >= 3000 and total_vip_exp_get < 5000 then 'vip_7'
            when total_vip_exp_get >= 5000 and total_vip_exp_get < 7000 then 'vip_8'
            when total_vip_exp_get >= 7000 and total_vip_exp_get < 10000 then 'vip_9'
            when total_vip_exp_get >= 10000 and total_vip_exp_get < 15000 then 'vip_10'
            when total_vip_exp_get >= 15000 and total_vip_exp_get < 20000 then 'vip_11'
            when total_vip_exp_get >= 20000 and total_vip_exp_get < 40000 then 'vip_12'
            when total_vip_exp_get >= 40000 and total_vip_exp_get < 80000 then 'vip_13'
            when total_vip_exp_get >= 80000 and total_vip_exp_get < 150000 then 'vip_14'
            when total_vip_exp_get >= 150000 then 'vip_15'
        end;

-- update install source
update temp_dim_player_latest
set install_source = f.install_source,
    campaign = f.campaign,
    sub_publisher =  f.sub_publisher,
    creative_id = f.creative_id
from processed.fact_user_install_source f
where temp_dim_player_latest.user_key = f.user_key;

-- create temp table temp_player_last_login
create temp table temp_player_last_login as
select player_key, max(ts_start) as last_login_ts
from processed.fact_session
group by 1;


update temp_dim_player_latest
set last_login_ts = t.last_login_ts
from temp_player_last_login t
where temp_dim_player_latest.player_key = t.player_key;

-- delete old player status in dim_player
delete from processed.dim_player
where player_key in
(
 select player_key from temp_dim_player_latest
);

-- insert the new status of players
insert into processed.dim_player
(
  user_key
  ,app
  ,uid
  ,snsid
  ,player_key
  ,server
  ,player_id
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,total_revenue_usd_iap
  ,total_revenue_usd_3rd
  ,total_rc_in
  ,total_vip_exp_get
  ,vip_level
  ,last_login_ts
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,player_key
  ,server
  ,player_id
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,total_revenue_usd_iap
  ,total_revenue_usd_3rd
  ,total_rc_in
  ,total_vip_exp_get
  ,vip_level
  ,last_login_ts
from temp_dim_player_latest;

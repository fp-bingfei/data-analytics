-- user distribution
drop table processed.dim_user_last_login_server;
create table processed.dim_user_last_login_server as
select *
from
(select *, row_number() over (partition by uid order by last_login_ts desc) as row_number
from processed.dim_player) t
where t.row_number = 1;

drop table processed.dim_user_max_level_server;
create table processed.dim_user_max_level_server as
select *
from
(select *, row_number() over (partition by uid order by level desc, last_login_ts desc) as row_number
from processed.dim_player) t
where t.row_number = 1;

update processed.dim_user_last_login_server
set vip_level = u.vip_level,
    is_payer = u.is_payer
from processed.dim_user u
where processed.dim_user_last_login_server.uid = u.uid;

update processed.dim_user_max_level_server
set vip_level = u.vip_level,
    is_payer = u.is_payer
from processed.dim_user u
where processed.dim_user_max_level_server.uid = u.uid;



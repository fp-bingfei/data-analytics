--Create temp table to get latest additional email address

create temp table personal_email_latest
as
select distinct   app_id 
          ,last_value(browser   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser
          ,last_value(browser_version   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser_version
          ,last_value(country_code   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as country_code
          ,event 
          ,last_value(install_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_source
          ,last_value(install_date   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_date
          ,last_value(ip   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as ip
          ,last_value(lang   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as lang
          ,last_value(level   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as level
          ,last_value(os   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os
          ,last_value(os_version   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os_version
          ,uid
          ,snsid
          ,last_value(fb_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as fb_source
          ,last_value(additional_email  ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following)  as additional_email
from processed.personal_info
;

-- TODO: For EAS report
create temp table payment_info as
select app, uid, snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from
(select p.app_id as app, u.app_user_id as uid, p.uid as snsid, paid_time as ts
from tbl_payment p
    join processed.dim_user u on p.app_id = u.app_id and p.uid = u.facebook_id
union
select r.app_id as app, r.app_user_id as uid, u.facebook_id as snsid, ts
from processed.fact_revenue r
    join processed.dim_user u on r.app_id = u.app_id and r.app_user_id = u.app_user_id
) t
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    t.app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,coalesce(pe.additional_email,'') as additional_email
    ,t.track_ref as install_source
    ,t.addtime as install_ts
    ,u.language as language
    ,u.gender
    ,cast(t.level as int)
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,CAST(t.reward_points AS BIGINT) as rc
    ,CAST(t.coins AS BIGINT) as coins
    ,dateadd(second, CAST(t.logintime AS INTEGER), '1970-01-01 00:00:00') as last_login_ts
from tbl_user t
    left join payment_info p on t.app = p.app and t.uid = p.uid and t.snsid = p.snsid
    left join processed.dim_user u on t.app = u.app_id and t.uid = u.app_user_id and t.snsid = u.facebook_id
    left join personal_email_latest pe on t.app=pe.app_id and t.uid=pe.uid and t.snsid=pe.snsid;

-------update null value in language column
update processed.eas_user_info 
set language = 'am'
where language is null and app = 'farm.us.prod';

update processed.eas_user_info 
set language = 'th'
where language is null and app = 'farm.th.prod';

update processed.eas_user_info 
set language = 'zh_TW'
where language is null and app = 'farm.tw.prod';

update processed.eas_user_info 
set language = 'de-DE'
where language is null and app = 'farm.de.prod';

update processed.eas_user_info 
set language = 'fr'
where language is null and app = 'farm.fr.prod';

update processed.eas_user_info 
set language = 'nl'
where language is null and app = 'farm.nl.prod';

update processed.eas_user_info 
set language = 'it'
where language is null and app = 'farm.it.prod';

update processed.eas_user_info 
set language = 'pl'
where language is null and app = 'farm.pl.prod';

update processed.eas_user_info 
set language = 'pt'
where language is null and app = 'farm.br.prod';

update processed.eas_user_info
set additional_email = 'elzbieta.zenowiew@wp.pl'
where snsid = '100001493084362'  and uid = '72360';

-- raw_data.raw_sendgrid_daily
CREATE  TABLE raw_data.raw_sendgrid_daily (
  snsid varchar(32),
  email varchar(64),
  category varchar(128),
  uid varchar(64),
  app_id varchar(64),
  time timestamp,
  ip varchar(32),
  event varchar(32),
  day date,
  campaign varchar(128)
  )
  distkey(uid)
  sortkey(app_id, day, campaign);


--  processed_new.fact_sendgrid
create table processed.fact_sendgrid (
app_id varchar(64),
campaign_name varchar(128),
campaign_date date,
delivered_per_campaign_date integer,
app_user_id varchar(64),
snsid varchar(64),
email varchar(128),
os varchar(32),
device varchar(32),
country varchar(64),
delivered_ts timestamp,
bounce_ts timestamp,
open_ts timestamp,
open_daydiff integer,
click_ts timestamp,
click_daydiff integer,
precamp_session_cnt integer,
precamp_purchase_cnt integer,
precamp_revenue_usd numeric(10,4),
precamp_level_end integer,
level_end integer,
total_session_cnt integer,
total_purchase_cnt integer,
total_revenue_usd numeric(10,4)
) 
distkey (app_user_id)
sortkey(campaign_name, campaign_date, app_user_id, email);


create table processed.agg_sendgrid_retention (
install_date date,
app_id varchar(32),
retention_days integer,
language varchar(32),
os varchar(32),
browser varchar(32),
country varchar(128),
install_source varchar(256),
is_payer integer,
email_flag boolean,
user_cnt integer)
distkey (install_date)
sortkey(install_date, app_id, retention_days);

create table processed.personal_info
(
  id varchar(100) ENCODE lzo DISTKEY,
  app_id varchar(30) ENCODE lzo,
  ts timestamp ENCODE delta,
  date date ENCODE delta,
  browser varchar(50) ENCODE lzo,
  browser_version varchar(50) ENCODE lzo,
  country_code varchar(20) ENCODE lzo,
  event varchar(50) ENCODE lzo,
  install_source varchar(20) ENCODE lzo,
  install_ts timestamp ENCODE delta,
  install_date date ENCODE delta,
  ip varchar(20) ENCODE lzo,
  lang varchar(20) ENCODE lzo,
  level int ENCODE lzo,
  os varchar(20) ENCODE lzo,
  os_version varchar(20) ENCODE lzo,
  snsid varchar(20) ENCODE lzo,
  uid varchar(30) ENCODE lzo,
  fb_source varchar(30) ENCODE lzo,
  additional_email varchar(20) ENCODE lzo
)
SORTKEY
(
	app_id,
	event,
	date,
	ts
);

CREATE TABLE processed.eas_user_info
(
	app VARCHAR(50),
	uid VARCHAR(200),
	snsid VARCHAR(200),
	user_name VARCHAR(200),
	email VARCHAR(200),
	additional_email varchar(20),
	install_source VARCHAR(200),
	install_ts VARCHAR(200),
	language VARCHAR(8),
	gender VARCHAR(16),
	level VARCHAR(200),
	is_payer INTEGER,
	conversion_ts TIMESTAMP,
	last_payment_ts TIMESTAMP,
	payment_cnt BIGINT,
	rc BIGINT,
	coins BIGINT,
	last_login_ts VARCHAR(200)
)
DISTSTYLE EVEN;
drop table processed.tab_iap; 
create table processed.tab_iap as
SELECT  date(p.ts) as date
        ,p.app
        ,p.level
        ,date(u.install_ts) as install_date
        ,u.install_source as install_source
        ,c.country
        ,p.os
        ,'' as ab_test
        ,'' as ab_variant
        ,case when date(u.conversion_ts)=date(p.ts) then 1 else 0 end as conversion_purchase
		    ,product_id
		    ,product_type
		    ,sum(amount) as revenue_usd
		    ,count(1) as purchase_cnt
FROM payment p
left join dau d on p.app=d.app and p.snsid=d.snsid and p.uid = d.uid and date(p.ts)=d.dt
left join app_user u on  p.app=u.app and p.snsid=u.snsid and p.uid = u.uid
left join country c on p.country_code = c.country_code
group by 1,2,3,4,5,6,7,8,9,10,11,12;
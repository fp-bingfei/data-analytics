-- MERGE/UpSert raw_data.raw_sendgrid_daily table's last 28 days' data into fact_sendgrid table

begin transaction;

create temp table _fact_sendgrid_temp_ as
select sg.app_id, campaign, trunc(delivered_ts) as campaign_date
, count(*) over (partition by campaign, trunc(delivered_ts)) delivered_per_campaign_date
, sg.uid, sg.snsid, email
, sg.os, sg.device, sg.country
, delivered_ts
, bounce_ts
, open_ts
, open_daydiff
, click_ts
, click_daydiff
, sum(case when dau.date < trunc(sg.delivered_ts) then dau.session_cnt else 0 end) precamp_session_cnt
, sum(case when dau.date < trunc(sg.delivered_ts) then dau.payment_cnt else 0 end) precamp_purchase_cnt
, sum(case when dau.date < trunc(sg.delivered_ts) then dau.revenue_usd else 0 end) precamp_revenue_usd
, max(case when dau.date < trunc(sg.delivered_ts) then dau.level_end else 1 end) AS precamp_level_end
, max(dau.level_end) AS level_end
, sum(dau.session_cnt) total_session_cnt, sum(dau.payment_cnt) total_purchase_cnt, sum(dau.revenue_usd) total_revenue_usd
 from (
select s1.app_id, s1.campaign, s1.uid, s1.snsid, s1.email
, du.os, du.device, du.country, du.user_key
, s1.time as delivered_ts, s2.time as bounce_ts
, s3.time as open_ts
, case when s3.time is not null then datediff(day, s1.time, s3.time) else null  end open_daydiff
, s4.time as click_ts
, case when s4.time is not null then datediff(day, s1.time, s4.time) else null  end click_daydiff
, row_number() over (partition by s1.campaign, s1.uid, s1.email, s1.event, trunc(s1.time) order by s3.time asc) rnum
from raw_data.raw_sendgrid_daily s1 
inner join processed.dim_user du on (du.app_id = s1.app_id and du.app_user_id = s1.uid)
left outer join raw_data.raw_sendgrid_daily s2
on (s1.app_id = s2.app_id and s1.campaign = s2.campaign and s1.uid = s2.uid and s1.email = s2.email and s1.time <= nvl(s2.time, s1.time) and s2.event= 'bounce')
left outer join raw_data.raw_sendgrid_daily s3
on (s1.app_id = s3.app_id and s1.campaign = s3.campaign and s1.uid = s3.uid and s1.email = s3.email and s1.time <= nvl(s3.time, s1.time) and s3.event= 'open')
left outer join raw_data.raw_sendgrid_daily s4
on (s1.app_id = s4.app_id and s1.campaign = s4.campaign and s1.uid = s4.uid and s1.email = s4.email and s1.time <= nvl(s4.time, s1.time) and s4.event= 'click')
where s1.event = 'delivered' and  trunc(s1.time) > (current_date - 7) 
) sg
left outer join processed.fact_dau_snapshot dau on (sg.app_id = dau.app_id and sg.user_key = dau.user_key and dau.date <= (current_date - 1) )
where rnum =1
group by 1,2,3,5,6,7,8,9,10,11,12,13,14,15,16
order by  app_id, campaign, campaign_date, delivered_ts;


delete from processed.fact_sendgrid 
using _fact_sendgrid_temp_ t 
where fact_sendgrid.app_id = t.app_id and fact_sendgrid.campaign_name = t.campaign and fact_sendgrid.app_user_id = t.uid and fact_sendgrid.email = t.email;

insert into processed.fact_sendgrid
select * from _fact_sendgrid_temp_;

drop table _fact_sendgrid_temp_;

end transaction;

---------------------------------
--FF tutorial report
--Version 1.2
--Author yanyu
/**
Discription:
This script is generate agg_tutorial tables.
**/
---------------------------------

-- tutorial report

delete from processed.agg_tutorial
where install_date >=(
                select start_date
                from processed.tmp_start_date
             );
             


create temp table tmp_tutorial_step as
select MD5(app||uid) as user_key
        ,app
        ,uid
        ,snsid
        ,step
        ,min(ts) as min_ts
from raw_tutorial
where trunc(ts_pretty) >=
            (
                select start_date
                from processed.tmp_start_date
            )
group by 1,2,3,4,5;

insert into processed.agg_tutorial
(
    install_date
    ,app
    ,os
    ,os_version
    ,country
    ,install_source
    ,browser
    ,browser_version
    ,language
    ,step
    ,user_cnt
)
select
     trunc(u.install_ts) as install_date
    ,u.app
    ,u.os
    ,u.os_version
    ,c.country
    ,u.install_source
    ,u.browser
    ,u.browser_version
    ,u.language
    ,cast(t.step as int) as step
    ,count(distinct md5(u.app||u.uid))
from  tmp_tutorial_step t
join app_user u 
on t.user_key = md5(u.app||u.uid)
join country c
on   u.country_code = c.country_code
where trunc(u.install_ts) >=
            (
                select start_date
                from processed.tmp_start_date
            )
group by 1,2,3,4,5,6,7,8,9,10
union all
select
     trunc(u.install_ts) as install_date
    ,u.app
    ,u.os
    ,u.os_version
    ,c.country
    ,u.install_source
    ,u.browser
    ,u.browser_version
    ,u.language
    ,0 as step
    ,count(distinct md5(app||uid))
from  app_user u
join  country c
on   u.country_code = c.country_code
where trunc(u.install_ts) >=
            (
                select start_date
                from processed.tmp_start_date
            )
group by 1,2,3,4,5,6,7,8,9,10;



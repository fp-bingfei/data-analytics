truncate processed.tab_level_dropoff;
insert into processed.tab_level_dropoff
with this_date as (select max(install_date) as date from processed.dim_user)
select
  u.install_date,
  l.level,
  u.level as current_level,
  u.app_id,
  u.install_os,
  u.install_country,
  u.install_source,
  u.is_payer,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days,
  count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join this_date t on 1=1
where install_date<t.date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;



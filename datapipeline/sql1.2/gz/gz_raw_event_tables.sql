--------------------------------------------------------------------------------------------------------------------------------------------
--Galaxy Zero session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

-- TODO: raw data
----------------------------------------------------------------------------------------------------------------------------------------------
-- extract data from json
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.events_raw
where  load_hour >= (
                        select raw_data_start_date
                        from gz.processed.tmp_start_date
                    );

create temp table tmp_events_raw as
select
    load_hour
    ,md5(json_str) as id
    ,json_extract_path_text(json_str,'@key') AS app
    ,dateadd(second, CAST(json_extract_path_text(json_str,'@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str,'uid')::varchar(32) AS uid
    ,CAST(json_extract_path_text(json_str,'snsid') AS VARCHAR(64)) AS snsid
    ,case
        when json_extract_path_text(json_str,'install_ts') = '' then NULL
        else dateadd(second, CAST(json_extract_path_text(json_str,'install_ts') AS INTEGER), '1970-01-01 00:00:00')
    end as install_ts
    ,json_extract_path_text(json_str,'install_source') AS install_source
    ,'--' AS country_code
    ,split_part(json_extract_path_text(json_str,'ip'), ',', 1) AS ip
    ,json_extract_path_text (json_str,'level') AS level
  ,json_extract_path_text (json_str,'vipLevel') AS vip_level
  ,CAST(json_extract_path_text(json_str,'lang') AS VARCHAR(16)) AS language
  ,CAST(json_extract_path_text(json_str,'device') AS VARCHAR(64)) AS device
    ,json_extract_path_text(json_str,'os') AS os
    ,CAST(json_extract_path_text(json_str,'os_version') AS VARCHAR(64)) AS os_version
    ,CAST(json_extract_path_text(json_str,'event') AS VARCHAR(64)) AS event
    ,json_str as properties
from gz.processed.raw_data_s3
where load_hour >= (
                     select raw_data_start_date
                     from gz.processed.tmp_start_date
                   )
and   json_extract_path_text(json_str,'uid') not like '%null' and
    json_extract_path_text(json_str,'uid') != '';

insert into tmp_events_raw
(
  load_hour
  ,id
  ,app
  ,ts
  ,uid
  ,snsid
  ,install_ts
  ,install_source
  ,country_code
  ,ip
  ,level
  ,vip_level
  ,language
  ,device
  ,os
  ,os_version
  ,event
  ,properties
)
select
    load_hour
    ,md5(json_str) as id
    ,json_extract_path_text(json_str,'@key') AS app
    ,dateadd(second, CAST(json_extract_path_text(json_str,'@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str,'uid')::varchar(32) AS uid
    ,CAST(json_extract_path_text(json_str,'snsid') AS VARCHAR(64)) AS snsid
    ,case
        when json_extract_path_text(json_str,'install_ts') = '' then NULL
        else dateadd(second, CAST(json_extract_path_text(json_str,'install_ts') AS INTEGER), '1970-01-01 00:00:00')
    end as install_ts
    ,json_extract_path_text(json_str,'install_source') AS install_source
    ,'--' AS country_code
    ,split_part(json_extract_path_text(json_str,'ip'), ',', 1) AS ip
    ,json_extract_path_text (json_str,'level') AS level
  ,json_extract_path_text (json_str,'vipLevel') AS vip_level
  ,CAST(json_extract_path_text(json_str,'lang') AS VARCHAR(16)) AS language
  ,CAST(json_extract_path_text(json_str,'device') AS VARCHAR(64)) AS device
    ,json_extract_path_text(json_str,'os') AS os
    ,CAST(json_extract_path_text(json_str,'os_version') AS VARCHAR(64)) AS os_version
    ,CAST(json_extract_path_text(json_str,'event') AS VARCHAR(64)) AS event
    ,json_str
from gz.processed.raw_data_s3_ar
where load_hour >= (
                     select raw_data_start_date
                     from gz.processed.tmp_start_date
                   )
and   json_extract_path_text(json_str,'uid') not like '%null' and
    json_extract_path_text(json_str,'uid') != '';

-- ip_country_map
create temp table ip_address as
select distinct
    ip,
    cast(split_part(ip,'.',1) AS BIGINT) * 16777216
    + cast(split_part(ip,'.',2) AS BIGINT) * 65536
    + cast(split_part(ip,'.',3) AS BIGINT) * 256
    + cast(split_part(ip,'.',4) AS BIGINT) as ip_int
from tmp_events_raw
where load_hour >= (
                     select raw_data_start_date
                     from gz.processed.tmp_start_date
                    )
and   ip != ''
and   ip != 'unknown'
and   ip != 'null';

create temp table ip_country_map as
select
     i.ip
    ,i.ip_int
    ,c.country_iso_code as country_code
from ip_address i
    join gz.processed.ip_integer_range_block r on i.ip_int >= network_start_integer and i.ip_int <= network_last_integer
    join gz.processed.geoip_country c on r.geoname_id = c.geoname_id;

update tmp_events_raw
set country_code = i.country_code
from ip_country_map i
where tmp_events_raw.ip = i.ip
and   load_hour >= (
                     select raw_data_start_date
                     from gz.processed.tmp_start_date
                   );

insert into gz.processed.events_raw
SELECT * from tmp_events_raw
;

-- TODO: check the data
-- drop table if exists gz.processed.tab_spiderman_capture;
-- create table gz.processed.tab_spiderman_capture (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number_old" BIGINT NOT NULL DEFAULT 0,
--   "row_number_new" BIGINT NOT NULL DEFAULT 0,
--   "row_number_diff" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);
--
-- drop table if exists gz.processed.tab_spiderman_capture_history;
-- create table gz.processed.tab_spiderman_capture_history (
--   "record_date" date NOT NULL,
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(record_date, event, date, hour);

-- truncate table gz.processed.tab_spiderman_capture;
-- drop table gz.processed.tab_spiderman_capture_old;
-- alter table gz.processed.tab_spiderman_capture_new rename to tab_spiderman_capture_old;

-- create table gz.processed.tab_spiderman_capture_new (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);

-- insert into gz.processed.tab_spiderman_capture_new
-- select event, trunc(ts) as date, extract(hour from ts) as hour, count(1)
-- from gz.processed.events_raw
-- group by event, trunc(ts), extract(hour from ts);

-- insert into gz.processed.tab_spiderman_capture
-- select n.event, n.date, n.hour, case when o.row_number is null then 0 else o.row_number end as row_number_old,
--     n.row_number as row_number_new
-- from gz.processed.tab_spiderman_capture_new n left join gz.processed.tab_spiderman_capture_old o
--     on n.event = o.event and n.date = o.date and n.hour = o.hour
-- union
-- select o.event, o.date, o.hour, o.row_number as row_number_old,
--     case when n.row_number is null then 0 else n.row_number end as row_number_new
-- from gz.processed.tab_spiderman_capture_old o left join gz.processed.tab_spiderman_capture_new n
--     on n.event = o.event and n.date = o.date and n.hour = o.hour;

-- update gz.processed.tab_spiderman_capture
-- set row_number_diff = row_number_new - row_number_old;

-- delete from gz.processed.tab_spiderman_capture_history where record_date = CURRENT_DATE;
-- insert into gz.processed.tab_spiderman_capture_history
-- (
--     record_date
--     ,event
--     ,date
--     ,hour
--     ,row_number
-- )
-- select
--     CURRENT_DATE as record_date
--     ,event
--     ,date
--     ,hour
--     ,row_number
-- from gz.processed.tab_spiderman_capture_new;

-- TODO: fact tables
----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists tmp_fact_session;
create temp table tmp_fact_session as
select  trunc(ts) as date_start
       ,MD5(app||uid) as user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,trunc(install_ts) AS install_date
     ,install_source
       ,CASE WHEN json_extract_path_text (properties,'session_id') = '' THEN NULL ELSE json_extract_path_text(properties,'session_id') END as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) as level_start
       ,CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) as level_end
       ,CAST(CASE WHEN vip_level = '' THEN '0' ELSE vip_level END AS integer) as vip_level_start
       ,CAST(CASE WHEN vip_level = '' THEN '0' ELSE vip_level END AS integer) as vip_level_end
       ,os
       ,os_version
       ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
       ,ip
       ,language
       ,device
       ,json_extract_path_text(properties,'duraction') as duration
       ,0 as session_length
from   tmp_events_raw
where  event in ('newuser', 'session_start')
;

create temp table session_end as
select MD5(app||uid) as user_key
       ,app
       ,uid
       ,snsid
       ,json_extract_path_text(properties,'session_id') as session_id
       ,max(ts) as ts_end
       ,max(CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer)) as level_end
       ,max(CAST(CASE WHEN vip_level = '' THEN '0' ELSE vip_level END AS integer)) as vip_level_end
from   tmp_events_raw
where  event = 'session_end'
and    json_extract_path_text(properties,'session_id') != ''
group by 2,3,4,5;

update tmp_fact_session
set ts_end = s.ts_end,
    level_end = s.level_end,
    vip_level_end = s.vip_level_end
from session_end s
where tmp_fact_session.user_key = s.user_key and tmp_fact_session.session_id = s.session_id;

update tmp_fact_session
set session_length = DATEDIFF('second', ts_start, ts_end);

--delete the historical data in case repeat loading
delete from  gz.processed.fact_session
where  date_start >= (
                     select raw_data_start_date
                     from gz.processed.tmp_start_date
                );

-- insert into gz.processed.fact_session
insert into gz.processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,install_date
     ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,vip_level_start
       ,vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,session_length
)
select
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,install_date
     ,install_source
       ,null as app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,case when level_start >= 32766 then 32766 else level_start end
       ,case when level_end >= 32766 then 32766 else level_end end
       ,vip_level_start 
       ,vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,session_length
from tmp_fact_session;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  gz.processed.fact_revenue
where  date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
                );

create temp table tmp_fact_revenue as
select *
from    tmp_events_raw
where   event = 'payment'
;

insert into gz.processed.fact_revenue
(
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,player_key
        ,server
        ,player_id
        ,ts
        ,level
        ,vip_level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,transaction_id
)
select  trunc(ts) as date
        ,MD5(app || uid)
        ,app
        ,uid
        ,snsid
        ,null AS player_key
        ,null AS server
        ,null AS player_id
        ,ts
        ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end
        ,CAST(CASE WHEN vip_level = '' THEN '0' ELSE vip_level END AS integer) as vip_level
        ,json_extract_path_text(properties,'payment_processor') AS payment_processor
        ,json_extract_path_text(properties,'product_id') AS product_id
        ,json_extract_path_text(properties,'product_name') AS product_name
        ,json_extract_path_text(properties,'product_type') AS product_type
        ,CAST(
              case
                    when json_extract_path_text(properties,'coins_in') = '' then '0'
                    else json_extract_path_text(properties,'coins_in')
              end
        AS INTEGER) AS coins_in
        ,CAST(
              case
                    when json_extract_path_text(properties,'rc_in') = '' then '0'
                    else json_extract_path_text(properties,'rc_in')
              end
        AS INTEGER) AS rc_in
        ,case when json_extract_path_text(properties,'currency') = '' then 'USD' else json_extract_path_text(properties,'currency') end AS currency
        ,CAST(json_extract_path_text(properties,'amount')/100 AS DECIMAL(14,4)) AS amount
        ,CAST(json_extract_path_text(properties,'amount')/100 AS DECIMAL(14,4)) AS usd
        ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    tmp_fact_revenue;

update gz.processed.fact_revenue
set amount = 4.99,
    usd = 4.99
where product_id = 'com.funplus.tf.diamonds300'
and   date >= (
                select raw_data_start_date
                from gz.processed.tmp_start_date
              );

update gz.processed.fact_revenue
set amount = 9.99,
    usd = 9.99
where product_id = 'com.funplus.tf.diamonds650'
and   date >= (
                select raw_data_start_date
                from gz.processed.tmp_start_date
              );

update gz.processed.fact_revenue
set amount = 19.99,
    usd = 19.99
where product_id = 'com.funplus.tf.diamonds1400'
and   date >= (
                select raw_data_start_date
                from gz.processed.tmp_start_date
              );

update gz.processed.fact_revenue
set amount = 49.99,
    usd = 49.99
where product_id = 'com.funplus.tf.diamonds3600'
and   date >= (
                select raw_data_start_date
                from gz.processed.tmp_start_date
              );

update gz.processed.fact_revenue
set amount = 99.99,
    usd = 99.99
where product_id = 'com.funplus.tf.diamonds7500'
and   date >= (
                select raw_data_start_date
                from gz.processed.tmp_start_date
              );

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.daily_level
----------------------------------------------------------------------------------------------------------------------------------------------
delete from  gz.processed.daily_level
where  date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
                );

create temp table player_level as
select trunc(ts) as date
        ,MD5(app || uid) as user_key
        ,app
        ,uid
        ,snsid
        ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
from tmp_events_raw
;

insert into gz.processed.daily_level
(
    date
    ,user_key
    ,app
    ,uid
    ,snsid
    ,min_level
    ,max_level
)
select
    date
    ,user_key
    ,app
    ,uid
    ,snsid
    ,min(level) as min_level
    ,max(level) as max_level
from player_level
group by 1,2,3,4,5;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.rc_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.rc_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

create temp table tmp_rc_transaction as
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as integer) as vip_level
             ,os
             ,abs(cast(case when json_extract_path_text(properties,'rc_in') = '' then '0'
                        else json_extract_path_text(properties,'rc_in') end as int)) as rc_in
             ,abs(cast(case when json_extract_path_text(properties,'rc_out') = '' then '0'
                        else json_extract_path_text(properties,'rc_out') end as int)) as rc_out
             ,json_extract_path_text(properties,'action') as action
             ,cast(json_extract_path_text(properties,'rc_bal') as int) as rc_bal
             ,cast(json_extract_path_text(properties,'is_gift') as int) as is_gift
             ,json_extract_path_text(properties,'location') as location
             ,json_extract_path_text(properties,'install_src') as install_source
             ,device
             ,json_extract_path_text(properties,'lang') as lang
from         tmp_events_raw
where        event = 'rc_transaction'
;

-- split StoreCrate
create temp table tmp_rc_transaction_StoreCrate as
select *
from tmp_rc_transaction
where action = 'StoreCrate';

delete
from tmp_rc_transaction
where action = 'StoreCrate';

update tmp_rc_transaction_StoreCrate
set action = 'StoreUltraCrate'
where rc_out = 2500;

insert into tmp_rc_transaction
select *
from tmp_rc_transaction_StoreCrate;

-- split EndlessModeSettlement
create temp table tmp_rc_transaction_EndlessModeRevive as
select *
from tmp_rc_transaction
where action = 'EndlessModeSettlement';

delete
from tmp_rc_transaction
where action = 'EndlessModeSettlement';

update tmp_rc_transaction_EndlessModeRevive
set action = 'EndlessModeRevive';

create temp table tmp_rc_transaction_EndlessModeBomb as
select *
from tmp_rc_transaction_EndlessModeRevive
where (rc_out / 10) % 2 = 1;

update tmp_rc_transaction_EndlessModeBomb
set rc_out = 10,
    action = 'EndlessModeBomb';

update tmp_rc_transaction_EndlessModeRevive
set rc_out = rc_out - 10
where (rc_out / 10) % 2 = 1;

insert into tmp_rc_transaction
select *
from tmp_rc_transaction_EndlessModeRevive;

insert into tmp_rc_transaction
select *
from tmp_rc_transaction_EndlessModeBomb;

insert into gz.processed.rc_transaction
select *
from tmp_rc_transaction;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.coins_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.coins_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.coins_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as integer) as vip_level
             ,os
             ,cast(case when json_extract_path_text(properties,'coins_in') = '' then '0'
                        else json_extract_path_text(properties,'coins_in') end as int) as coins_in
             ,cast(case when json_extract_path_text(properties,'coins_out') = '' then '0'
                        else json_extract_path_text(properties,'coins_out') end as int) as coins_out
             ,json_extract_path_text(properties,'action') as action
             ,cast(case when json_extract_path_text(properties,'coins_bal') = '' then '0'
                        else json_extract_path_text(properties,'coins_bal') end as int) as coins_bal
             ,cast(case when json_extract_path_text(properties,'is_gift')= '' then '0'
                        else json_extract_path_text(properties,'is_gift') end as int) as is_gift
             ,json_extract_path_text(properties,'location') as location
             ,json_extract_path_text(properties,'install_src') as install_source
             ,device
             ,language
from         tmp_events_raw
where        event = 'coins_transaction'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.item_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.item_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.item_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as int) as vip_level
             ,os
             ,cast(case when json_extract_path_text(properties,'rc_out') = '' then '0'
                        else json_extract_path_text(properties,'rc_out') end as int) as rc_out
             ,cast(case when json_extract_path_text(properties,'coins_out') = '' then '0'
                        else json_extract_path_text(properties,'coins_out') end as int) as coin_out
             ,json_extract_path_text(properties,'action') as action
             ,json_extract_path_text(properties,'location') as location
             ,json_extract_path_text(properties,'install_src') as install_source
             ,device
             ,language
             ,cast(case when json_extract_path_text(properties,'item_in') = '' then '0'
                        else json_extract_path_text(properties,'item_in') end as int) as item_in
             ,cast(case when json_extract_path_text(properties,'item_out') = '' then '0'
                        else json_extract_path_text(properties,'item_out') end as int) as item_out
             ,json_extract_path_text(properties,'item_id') as item_id
             ,json_extract_path_text(properties,'item_name') as item_name
             ,json_extract_path_text(properties,'item_type') as item_type
             ,json_extract_path_text(properties,'item_class') as item_class
             ,json_extract_path_text(properties,'gifted_by') as gifted_by
             ,json_extract_path_text(properties,'gifted_to') as gifted_to
from         tmp_events_raw
where        event = 'item_transaction'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.pve_progress
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.pve_progress
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.pve_progress
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as int) as vip_level
             ,os
             ,device
             ,language
             ,json_extract_path_text(properties,'pveid') as pveid
             ,json_extract_path_text(properties,'pvelocation') as pvelocation
from         tmp_events_raw
where        event = 'PVEProgress'
and trunc(ts) >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.equip_intensify_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.equip_intensify_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.equip_intensify_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as int) as vip_level
             ,os
             ,device
             ,language
             ,json_extract_path_text(properties,'equipid') as equipid
             ,json_extract_path_text(properties,'equipname') as equipname
             ,cast(case when json_extract_path_text(properties,'equiplevel') = '' then '0'
                    else json_extract_path_text(properties,'equiplevel') end as integer) as equiplevel
             ,cast(case when json_extract_path_text(properties,'equipexp') = '' then '0'
                    else json_extract_path_text(properties,'equipexp') end as integer) as equipexp
             ,cast(case when json_extract_path_text(properties,'afterequipexp') = '' then '0'
                    else  json_extract_path_text(properties,'afterequipexp') end as integer) as afterequipexp
             ,cast(case when json_extract_path_text(properties,'afterequiplevel')= '' then '0'
                   else json_extract_path_text(properties,'afterequiplevel') end as integer) as afterequiplevel
             ,json_extract_path_text(properties,'sequence') as sequence
from         tmp_events_raw
where        event = 'Equip_Intensify_Flow'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.equip_advance_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.equip_advance_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.equip_advance_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as int) as vip_level
             ,os
             ,device
             ,language
             ,json_extract_path_text(properties,'equipid') as equipid
             ,json_extract_path_text(properties,'equipname') as equipname
             ,cast(case when json_extract_path_text(properties,'star') = '' then '0'
                    else json_extract_path_text(properties,'star') end as integer) as star
             ,cast(case when json_extract_path_text(properties,'afterstar') = '' then '0'
                    else json_extract_path_text(properties,'afterstar') end as integer) as afterstar
             ,json_extract_path_text(properties,'sequence') as sequence
from         tmp_events_raw
where        event = 'Equip_Intensify_Flow'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.sns_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.sns_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.sns_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
             ,cast(case when vip_level = '' then '0' else vip_level end as int) as vip_level
             ,os
             ,device
             ,language
             ,json_extract_path_text(properties,'snstype') as snstype
             ,cast(case when json_extract_path_text(properties,'count') = '' then '0'
                    else json_extract_path_text(properties,'count') end as integer) as count
             ,cast(case when json_extract_path_text(properties,'recnum') = '' then '0'
                    else json_extract_path_text(properties,'recnum') end as integer) as recnum
             ,cast(case when json_extract_path_text(properties,'actoropenid') = '' then '0'
                  else json_extract_path_text(properties,'actoropenid') end as integer) as actoropenid
from         tmp_events_raw
where        event = 'SNS_transaction'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.roundflow_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.roundflow_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.roundflow_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,level
             ,vip_level
             ,os
             ,device
             ,language
             ,json_extract_path_text(properties,'pveid') as pveid
             ,json_extract_path_text(properties,'pvelocation') as pvelocation
             ,cast(json_extract_path_text(properties,'result') as int) as result
             ,cast(json_extract_path_text(properties,'roundtime')  as int) as roundtime
             ,cast(json_extract_path_text(properties,'roundscore')  as int) as roundscore
             ,cast(case when json_extract_path_text(properties,'rank') = '' then '0'
                  else json_extract_path_text(properties,'rank') end as int) as rank
             ,cast(json_extract_path_text(properties,'gold')  as int) as gold
             ,json_extract_path_text(properties,'battletype') as battletype
             ,json_extract_path_text(properties,'battleid') as battleid
from         tmp_events_raw
where        event = 'RoundFlow'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.energyflow_transaction
----------------------------------------------------------------------------------------------------------------------------------------------
delete from gz.processed.energyflow_transaction
where date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
              );

insert into gz.processed.energyflow_transaction
select        trunc(ts) as date
             ,ts
             ,app
             ,uid
             ,snsid
             ,md5(app||uid) as user_key
             ,trunc(install_ts) as install_date
             ,country_code
             ,level
             ,vip_level
             ,os
             ,device
             ,language
             ,json_extract_path_text(properties,'reason') as reason
             ,json_extract_path_text(properties,'pveid') as pveid
             ,json_extract_path_text(properties,'pvelocation') as pvelocation
             ,cast(case when json_extract_path_text(properties,'energy_in') = '' then '0'
                  else json_extract_path_text(properties,'energy_in') end as int) as energy_in
             ,cast(case when json_extract_path_text(properties,'energy_out') = '' then '0'
                  else json_extract_path_text(properties,'energy_out') end as int) as energy_out
             ,cast(case when json_extract_path_text(properties,'afterenergy') = '' then '0'
                  else json_extract_path_text(properties,'afterenergy') end as int) as afterenergy
from         tmp_events_raw
where        event = 'EnergyFlow'
;

----------------------------------------------------------------------------------------------------------------------------------------------
-- gz.processed.fact_custom_event
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table custom_event as
select
    trunc(ts) as date
    ,MD5(app||uid) as user_key
    ,app
    ,uid
    ,snsid
    ,ts
    ,case when CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) >= 32766 then 32766 else CAST(CASE WHEN level = '' THEN '0' ELSE level END AS integer) end as level
    ,CAST(CASE WHEN vip_level = '' THEN '0' ELSE vip_level END AS integer) as vip_level
    ,json_extract_path_text (properties,'type') as type
    ,json_extract_path_text (properties,'eventdesc') as eventdesc
    ,json_extract_path_text (properties,'eventid') as eventid
    ,json_extract_path_text (properties,'process') as process
    ,json_extract_path_text (properties,'maintype') as maintype
    ,json_extract_path_text (properties,'device') as device
    ,json_extract_path_text (properties,'isdone') as isdone
    ,row_number() over (partition by trunc(ts), user_key, maintype, type, eventid order by ts desc) as rank
from   tmp_events_raw
where  event in ('Custom Event', 'EventAnalysis')
;

delete from  gz.processed.fact_custom_event
where  date >= (
                     select raw_data_start_date
                     from   gz.processed.tmp_start_date
                );

insert into gz.processed.fact_custom_event
(
    date
    ,user_key
    ,app
    ,uid
    ,snsid
    ,ts
    ,level
    ,vip_level
    ,type
    ,eventdesc
    ,eventid
    ,process
    ,maintype
    ,device
    ,isdone
    ,rank
)
select
    date
    ,user_key
    ,app
    ,uid
    ,snsid
    ,ts
    ,level
    ,vip_level
    ,type
    ,eventdesc
    ,eventid
    ,process
    ,maintype
    ,device::varchar(128)
    ,isdone
    ,rank
from   custom_event
where rank = 1;

-- TODO: Remove duplicated records from adjust table
-- drop table if exists unique_adjust;
-- create table unique_adjust as
-- select *
-- from
-- (select *, row_number() over (partition by adid order by ts) as row_number
-- from adjust)  t
-- where row_number = 1;

-- update unique_adjust
-- set userid = a.userid
-- from adjust a
-- where unique_adjust.userid = '' and a.userid != '' and unique_adjust.adid = a.adid;

-- GZ history data

-- drop table if exists processed.raw_data_s3;
-- create table processed.raw_data_s3
-- (
-- 	json_str          VARCHAR(20000) ENCODE LZO,
-- 	load_hour         TIMESTAMP ENCODE DELTA
-- )
--   SORTKEY(load_hour);

drop table if exists processed.raw_data_s3_test;
create table processed.raw_data_s3_test
(
	json_str          VARCHAR(20000) ENCODE LZO
);

drop table if exists processed.events_raw_test;
CREATE TABLE processed.events_raw_test
(
	load_hour           TIMESTAMP ENCODE DELTA,
	id                  VARCHAR(64) NOT NULL ENCODE LZO,
	app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
	uid                 VARCHAR(32) NOT NULL ENCODE LZO,
	snsid               VARCHAR(64) NOT NULL ENCODE LZO,
	install_ts          TIMESTAMP ENCODE DELTA,
	install_source      VARCHAR(256) ENCODE BYTEDICT,
	country_code        VARCHAR(8) ENCODE BYTEDICT,
	ip                  VARCHAR(16) ENCODE LZO,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	os_version          VARCHAR(64) ENCODE BYTEDICT,
	event               VARCHAR(64) ENCODE BYTEDICT,
	properties          VARCHAR(1024) ENCODE LZO
)
  DISTKEY(uid)
  SORTKEY(load_hour, event, ts);

truncate table processed.events_raw_test;

-- TODO: load 2014-12 data
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.ae.prod/2015/06/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

insert into processed.events_raw_test
select
    '2014-12-30 00:00:00' as load_hour
    ,md5(properties) as id
    ,json_extract_path_text(properties,'@key') AS app
    ,dateadd(second, CAST(json_extract_path_text(properties,'@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(properties,'uid') AS uid
    ,json_extract_path_text(properties,'snsid') AS snsid
    ,dateadd(second, CAST(json_extract_path_text(properties,'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,json_extract_path_text(properties,'install_source') AS install_source
    ,null AS country_code
    ,json_extract_path_text(properties,'ip') AS ip
    ,json_extract_path_text(properties,'os') AS os
    ,json_extract_path_text(properties,'os_version') AS os_version
    ,json_extract_path_text(properties,'event') AS event
    ,properties
from processed.raw_data_s3_test;

-- TODO: load 2015-01 data
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.global.prod/2015/01/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

insert into processed.events_raw_test
select
    '2015-01-01 00:00:00' as load_hour
    ,md5(properties) as id
    ,json_extract_path_text(properties,'@key') AS app
    ,dateadd(second, CAST(json_extract_path_text(properties,'@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(properties,'uid') AS uid
    ,json_extract_path_text(properties,'snsid') AS snsid
    ,dateadd(second, CAST(json_extract_path_text(properties,'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,json_extract_path_text(properties,'install_source') AS install_source
    ,null AS country_code
    ,json_extract_path_text(properties,'ip') AS ip
    ,json_extract_path_text(properties,'os') AS os
    ,json_extract_path_text(properties,'os_version') AS os_version
    ,json_extract_path_text(properties,'event') AS event
    ,properties
from processed.raw_data_s3_test;

-- TODO: load 2015-02 data
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.global.prod/2015/02/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

insert into processed.events_raw_test
select
    '2015-02-01 00:00:00' as load_hour
    ,md5(properties) as id
    ,json_extract_path_text(properties,'@key') AS app
    ,dateadd(second, CAST(json_extract_path_text(properties,'@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(properties,'uid') AS uid
    ,json_extract_path_text(properties,'snsid') AS snsid
    ,dateadd(second, CAST(json_extract_path_text(properties,'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,json_extract_path_text(properties,'install_source') AS install_source
    ,null AS country_code
    ,json_extract_path_text(properties,'ip') AS ip
    ,json_extract_path_text(properties,'os') AS os
    ,json_extract_path_text(properties,'os_version') AS os_version
    ,json_extract_path_text(properties,'event') AS event
    ,properties
from processed.raw_data_s3_test
where json_extract_path_text(properties,'uid') not like '%null';

-- TODO: check data
select extract(hour from load_hour), extract(hour from ts), count(1)
from processed.events_raw_test
where trunc(ts) = '2015-01-19'
group by 1,2
order by 1 desc,2 desc;

select trunc(ts), count(1)
from events
group by 1
order by 1;

-- events_raw_test
insert into gz.processed.events_raw_test
(
	load_hour
	,id
	,app
	,ts
	,uid
	,snsid
	,install_ts
	,install_source
	,country_code
	,ip
	,level
	,vip_level
	,language
	,device
	,os
	,os_version
	,event
	,properties
)
select
    '2015-06-24 00:00:00' as load_hour
    ,md5(json_str) as id
    ,json_extract_path_text(json_str,'@key') AS app
    ,dateadd(second, CAST(json_extract_path_text(json_str,'@ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str,'uid') AS uid
    ,json_extract_path_text(json_str,'snsid') AS snsid
    ,case
        when json_extract_path_text(json_str,'install_ts') != '' then dateadd(second, CAST(json_extract_path_text(json_str,'install_ts') AS INTEGER), '1970-01-01 00:00:00')
        else null
    end as install_ts
    ,json_extract_path_text(json_str,'install_source') AS install_source
    ,'--' AS country_code
    ,json_extract_path_text(json_str,'ip') AS ip
    ,json_extract_path_text (json_str,'level') AS level
	,json_extract_path_text (json_str,'viplevel') AS vip_level
	,CAST(json_extract_path_text(json_str,'lang') AS VARCHAR(64)) AS language
	,CAST(json_extract_path_text(json_str,'device') AS VARCHAR(64)) AS device
    ,json_extract_path_text(json_str,'os') AS os
    ,json_extract_path_text(json_str,'os_version') AS os_version
    ,json_extract_path_text(json_str,'event') AS event
    ,json_str
from gz.processed.raw_data_s3_test;








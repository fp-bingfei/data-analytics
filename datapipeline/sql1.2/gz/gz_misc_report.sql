-- TODO: Discrepancy between Adjust and BI
drop table if exists processed.adjust_bi_discrepancy;
create table processed.adjust_bi_discrepancy as
select
    t1.install_date
    ,cast(t1.install_date as VARCHAR) as install_date_str
    ,t1.install_source
    ,t1.campaign
    ,t1.sub_publisher
    ,t1.os
    ,t1.all_adjust_installs
    ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
    ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
    ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
from
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.funplus.fr.galaxystorm', 'com.funplus.tf') then 'Android'
	    when app_id = '922338625' then 'iOS'
    end as os
    ,count(1) as all_adjust_installs
from unique_adjust
group by 1,2,3,4,5) t1
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.funplus.fr.galaxystorm', 'com.funplus.tf') then 'Android'
	    when app_id = '922338625' then 'iOS'
    end as os
    ,count(1) as empty_user_id_installs
from unique_adjust
where userid = ''
group by 1,2,3,4,5) t2
    on t1.install_source = t2.install_source and
       t1.install_date = t2.install_date and
       t1.campaign = t2.campaign and
       t1.sub_publisher = t2.sub_publisher and
       t1.os = t2.os
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.funplus.fr.galaxystorm', 'com.funplus.tf') then 'Android'
	    when app_id = '922338625' then 'iOS'
    end as os
    ,count(1) in_bi_installs
from unique_adjust a join processed.dim_user u on a.userid = u.uid
where a.userid != ''
group by 1,2,3,4,5) t3
    on t1.install_source = t3.install_source and
       t1.install_date = t3.install_date and
       t1.campaign = t3.campaign and
       t1.sub_publisher = t3.sub_publisher and
       t1.os = t3.os
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.funplus.fr.galaxystorm', 'com.funplus.tf') then 'Android'
	    when app_id = '922338625' then 'iOS'
    end as os
    ,count(1) not_in_bi_installs
from unique_adjust a left join processed.dim_user u on a.userid = u.uid
where a.userid != '' and u.uid is null
group by 1,2,3,4,5) t4
    on t1.install_source = t4.install_source and
       t1.install_date = t4.install_date and
       t1.campaign = t4.campaign and
       t1.sub_publisher = t4.sub_publisher and
       t1.os = t4.os;


-- TODO: For EAS report
create temp table payment_info as
select app, uid, '' as snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from processed.fact_revenue
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    u.app
    ,substring(u.uid, 2, len(u.uid)) as uid
    ,u.os
    ,u.server
    ,'' as snsid
    ,'' as user_name
    ,'' as email
    ,'' as additional_email
    ,u.install_source
    ,u.install_ts
    ,u.language
    ,u.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,u.total_rc_in as rc
    ,0 as coins
    ,u.last_login_ts as last_login_ts
from processed.dim_user u
    left join payment_info p on u.app = p.app and u.uid = p.uid;

create temp table temp_helpshift_email
as
select game
,user_id
,email
from
(select game
    ,user_id
    ,email
    ,row_number() over (partition by game,user_id  order by date desc) as rank
    from raw_data.helpshift_email
    where game = 'Galaxy Zero'
)t
where t.rank =1;

update processed.eas_user_info 
set help_shift_email = temp_helpshift_email.email
from temp_helpshift_email
where processed.eas_user_info.uid = temp_helpshift_email.user_id;

-- TODO: figure out fraud install source or sub_publisher
create temp table adjust_duplicated_users as
select userid, count(1) as install_count
from unique_adjust
where userid != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel;
create table processed.fraud_channel as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,split_part(tracker_name, '::', 3) as sub_publisher
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d on a.userid = d.userid;

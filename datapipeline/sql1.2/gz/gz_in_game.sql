---------------------------------------------------------------------------------------------------------------------------------------
--GS In-Game reports script
--Version v_1.2
--Author Yanyu
---------------------------------------------------------------------------------------------------------------------------------------

---------------------
--GS tbl_rc_reports

---------------------

create temp table tmp_tbl_rc
as
select   r.date
        ,r.app
        ,r.user_key
        ,r.install_date
        ,case when lower(r.os) = 'ios' then 'iOS' 
              when lower(r.os) = 'android' then 'Android'
            else 'Unknown' end as os  
        ,d.country
        ,r.level
        ,r.viplevel
        ,r.action
        ,sum(r.rc_in) as rc_in
        ,sum(r.rc_out) as rc_out
        ,count(distinct case when is_gift=1 then gifted_to else null end) as invite_num
from    processed.rc_transaction r
join    
       (
         select  date
                ,user_key
                ,country
         from  processed.fact_dau_snapshot
         where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
       )d
on      r.date = d.date
and     r.user_key = d.user_key
where r.date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
group   by 1,2,3,4,5,6,7,8,9;



delete from processed.agg_rc_stats
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );




insert into processed.agg_rc_stats
select      date
           ,app
           ,install_date
           ,country
           ,viplevel
           ,os
           ,level
           ,action
           ,sum(rc_in) as rc_in
           ,sum(rc_out) as rc_out
           ,sum(invite_num) as invite_num
from       tmp_tbl_rc
group by 1,2,3,4,5,6,7,8
;
            
---------------------
--GS tbl_coin_reports

---------------------


create temp table tmp_tbl_coins
as
select   c.date
        ,c.app
        ,c.user_key
        ,c.install_date
        ,case when lower(c.os) = 'ios' then 'iOS' 
              when lower(c.os) = 'android' then 'Android'
            else 'Unknown' end as os  
        ,d.country
        ,c.level
        ,c.viplevel
        ,c.action
        ,sum(c.coins_in) as coins_in
        ,sum(c.coins_out) as coins_out
        ,count(distinct case when is_gift=1 then gifted_to else null end) as invite_num
from    processed.coins_transaction c
join    
       (
         select  date
                ,user_key
                ,country
         from  processed.fact_dau_snapshot
         where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
       )d
on      c.date = d.date
and     c.user_key = d.user_key
where c.date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
group   by 1,2,3,4,5,6,7,8,9;



delete from processed.agg_coins_stats
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );




insert into processed.agg_coins_stats
select      date
           ,app
           ,install_date
           ,country
           ,viplevel
           ,os
           ,level
           ,action
           ,sum(coins_in) as coins_in
           ,sum(coins_out) as coins_out
           ,sum(invite_num) as invite_num
from       tmp_tbl_coins
group by 1,2,3,4,5,6,7,8
;
            
---------------------
--GS tbl_item_reports

---------------------

delete from processed.agg_item_stats
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );
              
insert into processed.agg_item_stats
select  i.date
       ,i.app as app
       ,i.install_date as install_date
       ,case when lower(i.os) = 'ios' then 'iOS' 
             when lower(i.os) = 'android' then 'Android'
            else 'Unknown' end as os
       ,i.level
       ,i.viplevel
       ,d.country
       ,case when i.rc_out >0 then 'rc' 
             when i.coins_out>0 then 'coin'
        else 'Non-consume' end as resource
       ,i.item_name
       ,i.item_class
       ,i.action
       ,i.location
       ,sum(i.item_in) as item_in
       ,abs(sum(i.item_out)) as item_out
       ,sum(i.rc_out) as rc_out
       ,sum(i.coins_out) as coins_out
from   processed.item_transaction i
join (
       select date
              ,user_key
              ,country
       from   processed.fact_dau_snapshot
       where  date >= (
                     select start_date 
                     from   processed.tmp_start_date
                      )
      )d
on i.user_key = d.user_key
and i.date = d.date
where i.date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
group by 1,2,3,4,5,6,7,8,9,10,11,12;


---------------------------------
--GS tbl_equip_intensify_reports

---------------------------------
   

--1.processed.agg_tbl_equip_intensify_times

create temp table tmp_tbl_intensify_times
as
select  e.date
       ,e.app
       ,case when lower(e.os) = 'ios' then 'iOS'
             when lower(e.os) = 'android' then 'Android'
            else 'Unknown' end as os
       ,e.user_key
       ,e.install_date
       ,d.country
       ,e.level as level
       ,e.viplevel as viplevel
       ,count(*) as intensify_times
from   processed.equip_intensify_transaction e
join   (
         select date
                ,user_key
                ,country
         from    processed.fact_dau_snapshot
         where date >= (
                     select start_date 
                     from   processed.tmp_start_date
                       )   
        )d
on     e.user_key = d.user_key
and    e.date = d.date
where e.date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
group by 1,2,3,4,5,6,7,8;




delete from processed.agg_tbl_equip_intensify_times
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );

insert into  processed.agg_tbl_equip_intensify_times
select  date
       ,app
       ,os
       ,install_date
       ,country
       ,level 
       ,viplevel
       ,sum(intensify_times) as  intensify_times     
from   tmp_tbl_intensify_times
group by 1,2,3,4,5,6,7
;
       

--2.processed.agg_tbl_equip_intensify_num

create temp table tmp_tbl_intensify_num
as
select  e.date
       ,e.app
       ,case when lower(e.os) = 'ios' then 'iOS'
             when lower(e.os) = 'android' then 'Android'
            else 'Unknown' end as os 
       ,e.user_key
       ,e.install_date
       ,d.country
       ,e.level as level
       ,e.viplevel as viplevel
       ,equipid
       ,equipname
       ,count(*) as intensify_num
from   processed.equip_intensify_transaction e
join   (
        select date
               ,user_key
               ,country
        from   processed.fact_dau_snapshot
        where  date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
        )d
on e.user_key = d.user_key
and e.date = d.date
where e.date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
group by 1,2,3,4,5,6,7,8,9,10              
;



 
delete from processed.agg_tbl_equip_intensify_num  
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
                );
                

                   
insert into processed.agg_tbl_equip_intensify_num  
select  date
       ,app
       ,os
       ,install_date
       ,country
       ,level
       ,viplevel
       ,equipid
       ,equipname
       ,sum(intensify_num) as  intensify_num
from   tmp_tbl_intensify_num
where  date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
group by 1,2,3,4,5,6,7,8,9;



---------------------------------
--GS tbl_equip_advance_reports

---------------------------------
   


--1.processed.agg_tbl_equip_advance_times

create temp table tmp_tbl_advance_times
as
select  e.date
       ,e.app
       ,case when lower(e.os) = 'ios' then 'iOS'
             when lower(e.os) = 'android' then 'Android'
            else 'Unknown' end as os
       ,e.user_key
       ,e.install_date
       ,d.country
       ,e.level as level
       ,e.viplevel as viplevel
       ,count(*) as advance_times
from   processed.equip_advance_transaction e
join   (
         select date
                ,user_key
                ,country
         from    processed.fact_dau_snapshot
         where date >= (
                     select start_date 
                     from   processed.tmp_start_date
                       )   
        )d
on     e.user_key = d.user_key
and    e.date = d.date
where e.date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
group by 1,2,3,4,5,6,7,8;


delete from processed.agg_tbl_equip_advance_times
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );

insert into  processed.agg_tbl_equip_advance_times
select  date
       ,app
       ,os
       ,install_date
       ,country
       ,level 
       ,viplevel
       ,sum(advance_times) as  advance_times     
from   tmp_tbl_advance_times
group by 1,2,3,4,5,6,7
;
       

--2.processed.agg_tbl_equip_advance_num

create temp table tmp_tbl_advance_num
as
select  e.date
       ,e.app
       ,case when lower(e.os) = 'ios' then 'iOS'
             when lower(e.os) = 'android' then 'Android'
            else 'Unknown' end as os 
       ,e.user_key
       ,e.install_date
       ,d.country
       ,e.level as level
       ,e.viplevel as viplevel
       ,equipid
       ,equipname
       ,count(*) as advance_num
from   processed.equip_advance_transaction e
join   (
        select date
               ,user_key
               ,country
        from   processed.fact_dau_snapshot
        where  date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
        )d
on e.user_key = d.user_key
and e.date = d.date
where e.date >= (
                     select start_date 
                     from   processed.tmp_start_date
              )
group by 1,2,3,4,5,6,7,8,9,10              
;


delete from processed.agg_tbl_equip_advance_num  
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
                 ;
                

                   
insert into processed.agg_tbl_equip_advance_num  
select  date
       ,app
       ,os
       ,install_date
       ,country
       ,level
       ,viplevel
       ,equipid
       ,equipname
       ,sum(advance_num) as  advance_num
from   tmp_tbl_advance_num
where  date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
group by 1,2,3,4,5,6,7,8,9;



---------------------
--GS tbl_pve_reports

---------------------
delete from processed.pve_progress_latest;

insert into processed.pve_progress_latest
select  t.ts
       ,t.app
       ,t.uid
       ,t.snsid
       ,t.user_key
       ,t.install_date
       ,coalesce(u.country,'Unknown') as country
       ,t.level
       ,t.viplevel
       ,case when lower(t.os) = 'ios' then 'iOS' when lower(t.os) = 'android' then 'Android' else 'Unknown' end as os
       ,t.device
       ,t.lang
       ,t.pveid
       ,t.pvelocation
       ,coalesce(u.last_login_ts) as latest_login_ts
from 
       (select ts
              ,app
              ,uid
              ,snsid
              ,user_key
              ,install_date
              ,country_code
              ,level
              ,viplevel
              ,os
              ,device
              ,lang
              ,pveid
              ,pvelocation
              ,row_number() over(partition by user_key order by ts desc) as rank
       from   processed.pve_progress
       )t                               
left join processed.dim_user u
on u.user_key = t.user_key       
where t.rank =1;       

---------------------------------
--GS tbl_roundflow_reports

---------------------------------
delete from processed.tbl_roundflow_user
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
                );
                
insert into processed.tbl_roundflow_user
select   r.date 
        ,r.app 
        ,r.user_key 
        ,r.install_date
        ,d.country
        ,cast(case when r.level = '' then '0' else r.level end as int) as level
        ,cast(case when r.viplevel = '' then '0' else r.viplevel end as int) as viplevel
        ,case when lower(r.os) = 'ios' then 'iOS'
              when lower(r.os) = 'android' then 'Android'
            else 'Unknown' end as os 
        ,r.device 
        ,r.pveid 
        ,r.pvelocation 
        ,r.result 
        ,r.roundtime 
        ,r.roundscore 
        ,r.rank 
        ,r.gold 
        ,r.battletype 
        ,r.battleid 
from   processed.roundflow_transaction r
join   (
         select date
                ,user_key
                ,country
         from   processed.fact_dau_snapshot
         where date >= (
                        select start_date 
                        from   processed.tmp_start_date
                       )
         )d
on     r.user_key = d.user_key
and    r.date = d.date
where r.date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
;

          

---------------------------------
--GS tbl_energyflow_reports

---------------------------------


create temp table tmp_tbl_energy_user
as
select   r.date 
        ,r.app 
        ,r.user_key 
        ,r.install_date
        ,d.country
        ,r.level 
        ,r.viplevel 
        ,case when lower(r.os) = 'ios' then 'iOS'
              when lower(r.os) = 'android' then 'Android'
            else 'Unknown' end as os 
        ,r.device 
        ,r.reason
        ,r.pveid
        ,r.pvelocation
        ,r.energy_in
        ,r.energy_out
        ,r.afterenergy
from   processed.energyflow_transaction r
join   (
         select date
                ,user_key
                ,country
         from   processed.fact_dau_snapshot
         where date >= (
                        select start_date 
                        from   processed.tmp_start_date
                       )
         )d
on     r.user_key = d.user_key
and    r.date = d.date
where r.date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
;

           
      
           
delete from processed.agg_tbl_energyflow
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );          
           
           
insert into processed.agg_tbl_energyflow
select  date 
       ,app 
       ,install_date
       ,country
       ,level 
       ,viplevel 
       ,os 
       ,device 
       ,pveid 
       ,pvelocation 
       ,reason
       ,count(distinct user_key) as user_num
       ,sum(energy_in) as energy_in
       ,sum(energy_out) as energy_out
from   (  
        select  date 
               ,app 
               ,user_key
               ,install_date
               ,country
               ,level 
               ,viplevel 
               ,os 
               ,device 
               ,pveid 
               ,pvelocation 
               ,reason
               ,sum(energy_in) as energy_in 
               ,sum(energy_out) as energy_out
        from    tmp_tbl_energy_user
        group by 1,2,3,4,5,6,7,8,9,10,11,12   
       )t
group by 1,2,3,4,5,6,7,8,9,10,11;
       
      
---------------------------------
--GS tbl_sns_reports

---------------------------------



create temp table tmp_tbl_sns_user_num
as
select  e.date
       ,e.app
       ,case when lower(e.os) = 'ios' then 'iOS'
             when lower(e.os) = 'android' then 'Android' 
            else 'Unknown' end as os
       ,e.user_key
       ,e.install_date
       ,d.country
       ,e.snstype
       ,e.level as level
       ,e.viplevel as viplevel
       ,count(*) as sns_user_num
from   processed.sns_transaction e
join   (
         select date
                ,user_key
                ,country
         from   processed.fact_dau_snapshot
         where date >= (
                        select start_date 
                        from   processed.tmp_start_date
                       )
         )d
on     e.user_key = d.user_key
and    e.date = d.date
where e.date >= (
                     select start_date 
                     from   processed.tmp_start_date
                )
group by 1,2,3,4,5,6,7,8,9;



delete from processed.agg_tbl_sns_user_num
where date >= (
                     select start_date 
                     from   processed.tmp_start_date
              );

insert into  processed.agg_tbl_sns_user_num
select  date
       ,app
       ,os
       ,install_date
       ,country
       ,level 
       ,viplevel
       ,snstype
       ,sum(sns_user_num) as  sns_user_num      
from   tmp_tbl_sns_user_num
group by 1,2,3,4,5,6,7,8;

-- Custom Event
drop table if exists processed.agg_custom_event;
create table processed.agg_custom_event as
select
    e.date
    ,u.app
    ,u.os
    ,u.country
    ,u.install_source
    ,u.install_date
    ,u.language
    ,d.is_payer
    ,case when d.revenue_usd > 0 then 1 else 0 end as is_payer_today
    ,e.maintype
    ,e.type
    ,e.eventid
    ,e.eventdesc
    ,e.process
    ,e.isdone
    ,count(distinct e.user_key) as users
from processed.fact_custom_event e
    join processed.dim_user u on e.user_key = u.user_key
    join processed.fact_dau_snapshot d on e.date = d.date and e.user_key = d.user_key
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

drop table if exists processed.agg_custom_event_dau;
create table processed.agg_custom_event_dau as
select
    date
    ,u.app
    ,u.os
    ,u.country
    ,u.install_source
    ,u.install_date
    ,u.language
    ,count(distinct d.user_key) as users
from processed.fact_dau_snapshot d join processed.dim_user u on d.user_key = u.user_key
group by 1,2,3,4,5,6,7;

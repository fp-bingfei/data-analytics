-- TODO: check new installs
create table processed.check_data_installs as
select uid, ts
from processed.events_raw
limit 0;

copy processed.check_data_installs from 's3://com.funplus.bitest/gz/funplusid_time.txt' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' DELIMITER '\t';

select *
from processed.check_data_installs c
	left join processed.dim_user u on 'i' + c.uid = u.uid
where u.uid is null;

select u.os, sum(r.usd)
from processed.fact_revenue r
	join processed.dim_user u on r.uid = u.uid
group by 1;


-- TODO: check revenue
drop table if exists processed.check_data_revenue_ios;
CREATE TABLE processed.check_data_revenue_ios
(
  id                VARCHAR(64) ENCODE LZO,
  app_id            VARCHAR(32) ENCODE BYTEDICT,
  uid               VARCHAR(32) ENCODE LZO,
  rmoney            DECIMAL(14,4) DEFAULT 0,
  uuid              VARCHAR(64) ENCODE LZO,
  package_type      VARCHAR(32) ENCODE LZO,
  ts                TIMESTAMP ENCODE DELTA,
  currency          VARCHAR(32) ENCODE BYTEDICT
)
  SORTKEY(uid);

copy processed.check_data_revenue_ios
from 's3://com.funplus.bitest/gz/check_data/gz_ios.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
CSV
IGNOREHEADER 1;


drop table if exists processed.check_data_revenue_android;
CREATE TABLE processed.check_data_revenue_android
(
  id                VARCHAR(64) ENCODE LZO,
  app_id            VARCHAR(32) ENCODE BYTEDICT,
  uid               VARCHAR(32) ENCODE LZO,
  rmoney            DECIMAL(14,4) DEFAULT 0,
  uuid              VARCHAR(64) ENCODE LZO,
  package_type      VARCHAR(32) ENCODE LZO,
  ts                TIMESTAMP ENCODE DELTA,
  currency          VARCHAR(32) ENCODE BYTEDICT
)
  SORTKEY(uid);

copy processed.check_data_revenue_android
from 's3://com.funplus.bitest/gz/check_data/gz_googleplay.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
CSV
IGNOREHEADER 1;

-- Android: compare data
select a.uid, r.uid, a.ts, r.ts, a.app_id, r.app, a.rmoney, r.usd, a.currency, r.currency
from processed.check_data_revenue_android a
	left join processed.fact_revenue r on a.id = r.transaction_id
limit 100;

select a.*
from processed.check_data_revenue_android a
	left join processed.fact_revenue r on a.id = r.transaction_id
where r.uid is null;

select r.*
from processed.fact_revenue r
	left join processed.check_data_revenue_android a on a.id = r.transaction_id
where a.uid is null;

-- iOS: compare data
select i.uid, r.uid, i.ts, r.ts, i.app_id, r.app, i.rmoney, r.usd, i.currency, r.currency
from processed.check_data_revenue_ios i
	left join processed.fact_revenue r on i.id = r.transaction_id
limit 100;

select i.*
from processed.check_data_revenue_ios i
	left join processed.fact_revenue r on i.id = r.transaction_id
where r.uid is null;

select r.*
from processed.fact_revenue r
	left join processed.check_data_revenue_ios i on i.id = r.transaction_id
where i.uid is null;

----------------------------------------------------------------------------------------------------------------------------------------------
-- TODO: import data
----------------------------------------------------------------------------------------------------------------------------------------------
-- 2015-04-09
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.global.prod/2015/04/09/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

delete from processed.raw_data_s3 where load_hour = '2015-04-09 00:00:00';

insert into processed.raw_data_s3
select json_str, '2015-04-09 00:00:00' as load_hour
from processed.raw_data_s3_test;

--  2015-04-10
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.global.prod/2015/04/10/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

delete from processed.raw_data_s3 where load_hour = '2015-04-10 00:00:00';

insert into processed.raw_data_s3
select json_str, '2015-04-10 00:00:00' as load_hour
from processed.raw_data_s3_test;

-- 2015-04-11
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.global.prod/2015/04/11/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

delete from processed.raw_data_s3 where load_hour = '2015-04-11 00:00:00';

insert into processed.raw_data_s3
select json_str, '2015-04-11 00:00:00' as load_hour
from processed.raw_data_s3_test;

-- 2015-04-12
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.galaxystorm/events/gxs.global.prod/2015/04/12/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

delete from processed.raw_data_s3 where load_hour = '2015-04-12 00:00:00';

insert into processed.raw_data_s3
select json_str, '2015-04-12 00:00:00' as load_hour
from processed.raw_data_s3_test;

-- create new database account
select * from pg_user;
create user "gz_pm" with password '';

create schema gz_pm;

grant usage on schema gz_pm to "siyuan.zhang";
grant create on schema gz_pm to "siyuan.zhang";
grant all on all tables in schema gz_pm to "siyuan.zhang";

grant usage on schema processed to "siyuan.zhang";
grant select on all tables in schema processed to "siyuan.zhang";

grant usage on schema public to "siyuan.zhang";
grant select on all tables in schema public to "siyuan.zhang";

revoke all on all tables in schema processed from gz_pm;
revoke all on all tables in schema public from gz_pm;

grant usage on schema processed to "zhao.liu";
grant select on table processed.eas_user_info to "zhao.liu";

grant usage on schema public to gz_pm;
grant select on all tables in schema public to gz_pm;

grant usage on schema processed to gz_pm;
grant select on all tables in schema processed to gz_pm;

-- test account
create table gz_pm.test as
select *
from processed.events_raw
limit 100;

set session authorization gz_pm;

select current_user;

select * from processed.agg_kpi limit 100;




-- TODO: set start date
-- start date table
-- drop table gz.processed.tmp_start_date;
-- create table gz.processed.tmp_start_date as
-- select DATEADD(day, -7, CURRENT_DATE) as start_date, DATEADD(day, -3, CURRENT_DATE) as raw_data_start_date;

-- update gz.processed.tmp_start_date
-- set start_date = '2014-12-30';

update gz.processed.tmp_start_date
set start_date = DATEADD(day, -3, CURRENT_DATE);

update gz.processed.tmp_start_date
set raw_data_start_date = DATEADD(day, -1, CURRENT_DATE);

-- TODO: keep the history for agg_kpi table
delete from gz.processed.agg_kpi_history where record_date = CURRENT_DATE;
insert into gz.processed.agg_kpi_history
(
      record_date
      ,date
      ,country
      ,os
      ,new_installs
      ,dau
      ,new_payers
      ,today_payers
      ,revenue
      ,session_cnt
)
select CURRENT_DATE as record_date
      ,date
      ,country
      ,os
      ,sum(new_installs) as new_installs
      ,sum(dau) as dau
      ,sum(new_payers) as new_payers
      ,sum(today_payers) as today_payers
      ,sum(revenue) as revenue
      ,sum(session_cnt) as session_cnt
from gz.processed.agg_kpi d
group by 2,3,4;

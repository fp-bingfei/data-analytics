-- TODO: Import missing data
drop table processed.fact_session_missing_data;
CREATE TABLE processed.fact_session_missing_data
(
  date_start            TIMESTAMP ENCODE DELTA,
  user_key              VARCHAR(32) ENCODE LZO,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  uid                   INTEGER,
  snsid                 VARCHAR(32) DEFAULT '' ENCODE LZO,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          TIMESTAMP ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts_start              TIMESTAMP ENCODE DELTA,
  ts_end                TIMESTAMP ENCODE DELTA,
  level_start           SMALLINT,
  level_end             SMALLINT,
  vip_level_start       SMALLINT,
  vip_level_end         SMALLINT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(128) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(16) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(64) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  rc_bal_start          INTEGER,
  rc_bal_end            INTEGER,
  coin_bal_start        INTEGER,
  coin_bal_end          INTEGER,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  session_length        INTEGER
)
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, player_key, app, uid, server, player_id);

truncate table processed.fact_session_missing_data;
copy processed.fact_session_missing_data
from 's3://com.funplus.bitest/mt2/missing_data/fact_session.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\,'
IGNOREHEADER 1;

drop table if exists processed.fact_revenue_missing_data;
CREATE TABLE processed.fact_revenue_missing_data
(
  date                  TIMESTAMP ENCODE DELTA,
  user_key              VARCHAR(32) ENCODE LZO,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  uid                   INTEGER,
  snsid                 VARCHAR(32) DEFAULT '' ENCODE LZO,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          TIMESTAMP ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts                    TIMESTAMP ENCODE DELTA,
  level                 SMALLINT,
  vip_level             SMALLINT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(128) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(16) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(64) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,

  payment_processor     VARCHAR(32) ENCODE BYTEDICT,
  product_id            VARCHAR(64) ENCODE BYTEDICT,
  product_name          VARCHAR(64) ENCODE BYTEDICT,
  product_type          VARCHAR(32) ENCODE BYTEDICT,
  coins_in              INTEGER,
  rc_in                 INTEGER,
  currency              VARCHAR(32) ENCODE BYTEDICT,
  amount                DECIMAL(14,4) DEFAULT 0,
  usd                   DECIMAL(14,4) DEFAULT 0,
  transaction_id        VARCHAR(64) ENCODE LZO
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, player_key, app, uid, server, player_id);

truncate table processed.fact_revenue_missing_data;
copy processed.fact_revenue_missing_data
from 's3://com.funplus.bitest/mt2/missing_data/fact_revenue.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\,'
IGNOREHEADER 1;

-- Update session data
update processed.fact_session_missing_data
set install_ts = CONVERT_TIMEZONE ( 'UTC-8', 'UTC', install_ts);

update processed.fact_session_missing_data
set date_start = trunc(install_ts),
	install_date = trunc(install_ts),
	ts_start = install_ts,
	ts_end = install_ts,
	session_length = 0,
	level_start = 1,
	level_end = 1,
	vip_level_start = 0,
	vip_level_end = 0,
	app = 'mt2.global.prod';

update processed.fact_session_missing_data
set user_key = MD5(app || uid),
    player_key = MD5(server || uid);

update processed.fact_session_missing_data
set country_code = c.country_code
from processed.dim_country c
where processed.fact_session_missing_data.country_code = c.country;

update processed.fact_session_missing_data
set country_code = '--'
where length(country_code) > 2;

select *
from processed.fact_session_missing_data
limit 100;

-- Update revenue data
update processed.fact_revenue_missing_data
set ts = CONVERT_TIMEZONE ( 'UTC-8', 'UTC', ts),
	install_ts = CONVERT_TIMEZONE ( 'UTC-8', 'UTC', install_ts);

update processed.fact_revenue_missing_data
set date = trunc(ts),
    install_date = trunc(install_ts),
    level = 1,
	vip_level = 1,
	app = 'mt2.global.prod';

update processed.fact_revenue_missing_data
set user_key = MD5(app || uid),
    player_key = MD5(server || uid);

update processed.fact_revenue_missing_data
set country_code = c.country_code
from processed.dim_country c
where processed.fact_revenue_missing_data.country_code = c.country;

update processed.fact_revenue_missing_data
set country_code = '--'
where length(country_code) > 2;

update processed.fact_revenue_missing_data
set amount = usd;

update processed.fact_revenue_missing_data
set usd = amount * case when c.factor is null then 1 else c.factor end
from public.currency c
where processed.fact_revenue_missing_data.currency = c.currency and processed.fact_revenue_missing_data.date = c.dt;

update processed.fact_revenue_missing_data
set rc_in = CAST(regexp_substr(regexp_substr(product_id,'stones[0-9]+'), '[0-9]+') AS INTEGER);

delete
from processed.fact_revenue_missing_data
where ts >= '2015-06-03 03:55:00';

delete
from processed.fact_revenue_missing_data
using processed.fact_revenue r2
where processed.fact_revenue_missing_data.uid = r2.uid and
    processed.fact_revenue_missing_data.server = r2.server and
    processed.fact_revenue_missing_data.ts = r2.ts;

select date, server, extract(hour from ts), count(1)
from processed.fact_revenue_missing_data
group by 1,2,3
order by 1,2,3;

select date, server,  extract(hour from ts), count(1)
from processed.fact_revenue
where server = '1017'
group by 1,2,3
order by 1,2,3;

select *
from processed.fact_revenue_missing_data
limit 100;

-- Insert missing data into fact tables
insert into processed.fact_session
select *
from processed.fact_session_missing_data;

insert into processed.fact_revenue
select *
from processed.fact_revenue_missing_data;

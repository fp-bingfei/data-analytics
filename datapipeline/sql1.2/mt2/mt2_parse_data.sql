-- Dota history data

-- import data from S3 directly
-- drop table if exists processed.raw_data_s3;
-- create table processed.raw_data_s3
-- (
-- 	json_str          VARCHAR(20000) ENCODE LZO,
-- 	load_hour         TIMESTAMP ENCODE DELTA
-- )
--   SORTKEY(load_hour);

drop table if exists processed.raw_data_s3_test;
create table processed.raw_data_s3_test
(
	json_str          VARCHAR(20000) ENCODE LZO
);

truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.mt2/mt2.global.prod/2015/05/21/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

select *
from processed.raw_data_s3_test
where regexp_substr(json_str, '^\\\{.*\\\}$') = '';

delete
from processed.raw_data_s3_test
where regexp_substr(json_str, '^\\\{.*\\\}$') = '';

-- drop table if exists processed.events_raw_test;
-- CREATE TABLE processed.events_raw_test
-- (
-- 	load_hour           TIMESTAMP ENCODE DELTA,
-- 	event               VARCHAR(32) ENCODE BYTEDICT,
-- 	date                DATE ENCODE DELTA,
-- 	ts                  TIMESTAMP ENCODE DELTA,
-- 	ts_str              VARCHAR(16) ENCODE LZO,
-- 	app                 VARCHAR(32) ENCODE BYTEDICT,
-- 	uid                 INTEGER,
-- 	uid_str             VARCHAR(16) ENCODE LZO,
-- 	server              VARCHAR(16) ENCODE BYTEDICT,
-- 	install_ts          TIMESTAMP ENCODE DELTA,
-- 	install_ts_str      VARCHAR(16) ENCODE LZO,
-- 	country_code        VARCHAR(8) ENCODE BYTEDICT,
-- 	level               SMALLINT,
-- 	level_str           VARCHAR(16) ENCODE BYTEDICT,
-- 	viplevel            VARCHAR(32) ENCODE BYTEDICT,
-- 	ip                  VARCHAR(16) ENCODE LZO,
-- 	os                  VARCHAR(32) ENCODE BYTEDICT,
-- 	os_version          VARCHAR(64) ENCODE BYTEDICT,
-- 	app_version         VARCHAR(32) ENCODE BYTEDICT,
-- 	session_id          VARCHAR(32) ENCODE LZO,
-- 	bi_version          VARCHAR(32) ENCODE BYTEDICT,
-- 	idfa                VARCHAR(64) ENCODE LZO,
-- 	gaid                VARCHAR(64) ENCODE LZO,
-- 	android_id          VARCHAR(64) ENCODE LZO,
-- 	mac_address         VARCHAR(64) ENCODE LZO,
-- 	device              VARCHAR(32) ENCODE BYTEDICT,
-- 	lang                VARCHAR(32) ENCODE BYTEDICT,
-- 	json_str            VARCHAR(1024) ENCODE LZO,
-- 	properties          VARCHAR(1024) ENCODE LZO,
-- 	md5                 VARCHAR(32) ENCODE LZO
-- )
--   SORTKEY(load_hour, event, date, ts);

truncate table processed.events_raw_test;

insert into processed.events_raw_test
(
    load_hour
    ,event
    ,ts_str
    ,app
    ,uid_str
    ,server
    ,install_ts_str
    ,country_code
    ,level_str
    ,viplevel
    ,ip
    ,os
    ,os_version
    ,app_version
    ,session_id
    ,bi_version
    ,idfa
    ,gaid
    ,android_id
    ,mac_address
    ,device
    ,lang
    ,json_str
    ,properties
    ,md5
)
select
    '2015-05-21 00:00:00' as load_hour
    ,json_extract_path_text(json_str, 'event') as event
    ,json_extract_path_text(json_str, 'ts') as ts_str
    ,json_extract_path_text(json_str, 'app_id') as app
    ,json_extract_path_text(json_str, 'user_id') as uid_str
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gameserver_id') as server
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') as install_ts_str
    ,'--' as country_code
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'level') as level_str
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'viplevel') as viplevel
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'ip') as ip
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os') as os
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os_version') as os_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'app_version') as app_version
    ,json_extract_path_text(json_str, 'session_id') as session_id
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'idfa') as idfa
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gaid') as gaid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'android_id') as android_id
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'mac_address') as mac_address
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device') as device
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'lang') as lang
    ,json_str as json_str
    ,json_extract_path_text(json_str, 'properties') as properties
    ,md5(json_str) as md5
from processed.raw_data_s3_test;




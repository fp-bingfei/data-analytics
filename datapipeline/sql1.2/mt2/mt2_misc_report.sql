-- TODO: Discrepancy between Adjust and BI
drop table if exists processed.adjust_bi_discrepancy;
create table processed.adjust_bi_discrepancy as
select
    t5.install_date
    ,t5.install_date_str
    ,c.country
    ,t5.install_source
    ,t5.campaign
    ,t5.sub_publisher
    ,t5.creative_id
    ,t5.os
    ,t5.all_adjust_installs
    ,t5.empty_user_id_installs
    ,t5.in_bi_installs
    ,t5.not_in_bi_installs
from
(select
    t1.install_date
    ,cast(t1.install_date as VARCHAR) as install_date_str
    ,t1.country
    ,t1.install_source
    ,t1.campaign
    ,t1.sub_publisher
    ,t1.creative_id
    ,t1.os
    ,t1.all_adjust_installs
    ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
    ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
    ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
from
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,country
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.changaming.mt2', 'com.funplus.mt2') then 'Android'
	    when app_id = '960546280' then 'iOS'
    end as os
    ,count(1) as all_adjust_installs
from unique_adjust
group by 1,2,3,4,5,6,7) t1
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,country
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.changaming.mt2', 'com.funplus.mt2') then 'Android'
	    when app_id = '960546280' then 'iOS'
    end as os
    ,count(1) as empty_user_id_installs
from unique_adjust
where userid = ''
group by 1,2,3,4,5,6,7) t2
    on t1.install_source = t2.install_source and
       t1.install_date = t2.install_date and
       t1.campaign = t2.campaign and
       t1.sub_publisher = t2.sub_publisher and
       t1.creative_id = t2.creative_id and
       t1.country = t2.country and
       t1.os = t2.os
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,a.country
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.changaming.mt2', 'com.funplus.mt2') then 'Android'
	    when app_id = '960546280' then 'iOS'
    end as os
    ,count(1) in_bi_installs
from unique_adjust a join processed.dim_user u on a.userid = u.uid
where a.userid != ''
group by 1,2,3,4,5,6,7) t3
    on t1.install_source = t3.install_source and
       t1.install_date = t3.install_date and
       t1.campaign = t3.campaign and
       t1.sub_publisher = t3.sub_publisher and
       t1.creative_id = t3.creative_id and
       t1.country = t3.country and
       t1.os = t3.os
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,split_part(tracker_name, '::', 4) as creative_id
    ,a.country
    ,trunc(ts) as install_date
    ,case
	    when app_id in ('com.changaming.mt2', 'com.funplus.mt2') then 'Android'
	    when app_id = '960546280' then 'iOS'
    end as os
    ,count(1) not_in_bi_installs
from unique_adjust a left join processed.dim_user u on a.userid = u.uid
where a.userid != '' and u.uid is null
group by 1,2,3,4,5,6,7) t4
    on t1.install_source = t4.install_source and
       t1.install_date = t4.install_date and
       t1.campaign = t4.campaign and
       t1.sub_publisher = t4.sub_publisher and
       t1.creative_id = t4.creative_id and
       t1.country = t4.country and
       t1.os = t4.os
) t5 left join public.country c on t5.country=lower(c.country_code);

-- Fraud with same IP in one day
drop table if exists processed.adjust_ip_fraud;
create table processed.adjust_ip_fraud as
select 
    split_part(a.tracker_name, '::', 1) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,split_part(a.tracker_name, '::', 3) as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when a.app_id in ('com.changaming.mt2', 'com.funplus.mt2') then 'Android'
        when a.app_id = '960546280' then 'iOS'
     end as os
    ,count(1) as fraud_count
    ,count(distinct u.uid) as distinct_fraud_count
from
    unique_adjust a 
    left join 
    processed.dim_user u on a.userid = u.uid 
    left join
    public.country c on a.country=lower(c.country_code)
where 
    (a.ip_address, date(a.ts))
    in 
    (
        select 
            ip_address
            ,date 
        from 
            (
                select 
                    ip_address
                    ,date(ts) as date
                    ,count(1) 
                from 
                    unique_adjust 
                where
                    tracker_name<>'Organic'
                    and date(ts)>=DATEADD(DAY, -90, current_date)
                    and userid != ''
                group by 1,2
                having count(1)>=10
            )
    )
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -90, current_date)
    and a.userid != ''
    and u.uid is not null
group by 1,2,3,4,5,6,7,8,9;

-- TODO: For EAS report
create temp table payment_info as
select app, uid, '' as snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from processed.fact_revenue
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    u.app
    ,u.uid
    ,u.os
    ,u.server
    ,'' as snsid
    ,'' as user_name
    ,'' as email
    ,'' as additional_email
    ,u.install_source
    ,u.install_ts
    ,u.language
    ,u.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,u.total_rc_in as rc
    ,0 as coins
    ,u.last_login_ts as last_login_ts
from processed.dim_user u
    left join payment_info p on u.app = p.app and u.uid = p.uid;

-- TODO: figure out fraud install source or sub_publisher
create temp table adjust_duplicated_users as
select userid, count(1) as install_count
from unique_adjust
where userid != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel;
create table processed.fraud_channel as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,split_part(tracker_name, '::', 3) as sub_publisher
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d on a.userid = d.userid;


-- TODO: re-targeting campaign
-- re-targeting campaign KPI for 0812 payer
drop table if exists processed.retarget_date_0812_payer;
create table processed.retarget_date_0812_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0812) r on d.uid = r.uid
where date >= '2015-08-12'
group by 1;

drop table if exists processed.agg_kpi_retarget_0812_payer;
create table processed.agg_kpi_retarget_0812_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0812_payer rd on d.uid = rd.uid
where date >= '2015-08-12'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0821 payer
drop table if exists processed.retarget_date_0821_payer;
create table processed.retarget_date_0821_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0821) r on d.uid = r.uid
where date >= '2015-08-21'
group by 1;

drop table if exists processed.agg_kpi_retarget_0821_payer;
create table processed.agg_kpi_retarget_0821_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0821_payer rd on d.uid = rd.uid
where date >= '2015-08-21'
group by 1,2,3,4,5,6,7,8,9,10;


-- re-targeting campaign KPI for 0827 payer
drop table if exists processed.retarget_date_0827_payer;
create table processed.retarget_date_0827_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0827) r on d.uid = r.uid
where date >= '2015-08-27'
group by 1;

drop table if exists processed.agg_kpi_retarget_0827_payer;
create table processed.agg_kpi_retarget_0827_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0827_payer rd on d.uid = rd.uid
where date >= '2015-08-27'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0902 payer
drop table if exists processed.retarget_date_0902_payer;
create table processed.retarget_date_0902_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0902) r on d.uid = r.uid
where date >= '2015-09-02'
group by 1;

drop table if exists processed.agg_kpi_retarget_0902_payer;
create table processed.agg_kpi_retarget_0902_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0902_payer rd on d.uid = rd.uid
where date >= '2015-09-02'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0911 payer
drop table if exists processed.retarget_date_0911_payer;
create table processed.retarget_date_0911_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0911) r on d.uid = r.uid
where date >= '2015-09-11'
group by 1;

drop table if exists processed.agg_kpi_retarget_0911_payer;
create table processed.agg_kpi_retarget_0911_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0911_payer rd on d.uid = rd.uid
where date >= '2015-09-11'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0917 payer
drop table if exists processed.retarget_date_0917_payer;
create table processed.retarget_date_0917_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0917) r on d.uid = r.uid
where date >= '2015-09-17'
group by 1;

drop table if exists processed.agg_kpi_retarget_0917_payer;
create table processed.agg_kpi_retarget_0917_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0917_payer rd on d.uid = rd.uid
where date >= '2015-09-17'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0924 payer
drop table if exists processed.retarget_date_0924_payer;
create table processed.retarget_date_0924_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0924) r on d.uid = r.uid
where date >= '2015-09-24'
group by 1;

drop table if exists processed.agg_kpi_retarget_0924_payer;
create table processed.agg_kpi_retarget_0924_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0924_payer rd on d.uid = rd.uid
where date >= '2015-09-24'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1008 payer
drop table if exists processed.retarget_date_1008_payer;
create table processed.retarget_date_1008_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1008) r on d.uid = r.uid
where date >= '2015-10-08'
group by 1;

drop table if exists processed.agg_kpi_retarget_1008_payer;
create table processed.agg_kpi_retarget_1008_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1008_payer rd on d.uid = rd.uid
where date >= '2015-10-08'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1023 payer
drop table if exists processed.retarget_date_1023_payer;
create table processed.retarget_date_1023_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1023) r on d.uid = r.uid
where date >= '2015-10-23'
group by 1;

drop table if exists processed.agg_kpi_retarget_1023_payer;
create table processed.agg_kpi_retarget_1023_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1023_payer rd on d.uid = rd.uid
where date >= '2015-10-23'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1030 payer
drop table if exists processed.retarget_date_1030_payer;
create table processed.retarget_date_1030_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1030) r on d.uid = r.uid
where date >= '2015-10-30'
group by 1;

drop table if exists processed.agg_kpi_retarget_1030_payer;
create table processed.agg_kpi_retarget_1030_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1030_payer rd on d.uid = rd.uid
where date >= '2015-10-30'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1106 payer
drop table if exists processed.retarget_date_1106_payer;
create table processed.retarget_date_1106_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1106) r on d.uid = r.uid
where date >= '2015-11-06'
group by 1;

drop table if exists processed.agg_kpi_retarget_1106_payer;
create table processed.agg_kpi_retarget_1106_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1106_payer rd on d.uid = rd.uid
where date >= '2015-11-06'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1113 payer
drop table if exists processed.retarget_date_1113_payer;
create table processed.retarget_date_1113_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1113) r on d.uid = r.uid
where date >= '2015-11-13'
group by 1;

drop table if exists processed.agg_kpi_retarget_1113_payer;
create table processed.agg_kpi_retarget_1113_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1113_payer rd on d.uid = rd.uid
where date >= '2015-11-13'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1120 payer
drop table if exists processed.retarget_date_1120_payer;
create table processed.retarget_date_1120_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1120) r on d.uid = r.uid
where date >= '2015-11-20'
group by 1;

drop table if exists processed.agg_kpi_retarget_1120_payer;
create table processed.agg_kpi_retarget_1120_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1120_payer rd on d.uid = rd.uid
where date >= '2015-11-20'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1127 payer
drop table if exists processed.retarget_date_1127_payer;
create table processed.retarget_date_1127_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1127) r on d.uid = r.uid
where date >= '2015-11-27'
group by 1;

drop table if exists processed.agg_kpi_retarget_1127_payer;
create table processed.agg_kpi_retarget_1127_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1127_payer rd on d.uid = rd.uid
where date >= '2015-11-27'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1204 payer
drop table if exists processed.retarget_date_1204_payer;
create table processed.retarget_date_1204_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1204) r on d.uid = r.uid
where date >= '2015-12-04'
group by 1;

drop table if exists processed.agg_kpi_retarget_1204_payer;
create table processed.agg_kpi_retarget_1204_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1204_payer rd on d.uid = rd.uid
where date >= '2015-12-04'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1211 payer
drop table if exists processed.retarget_date_1211_payer;
create table processed.retarget_date_1211_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1211) r on d.uid = r.uid
where date >= '2015-12-11'
group by 1;

drop table if exists processed.agg_kpi_retarget_1211_payer;
create table processed.agg_kpi_retarget_1211_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1211_payer rd on d.uid = rd.uid
where date >= '2015-12-11'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1218 payer
drop table if exists processed.retarget_date_1218_payer;
create table processed.retarget_date_1218_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1218) r on d.uid = r.uid
where date >= '2015-12-18'
group by 1;

drop table if exists processed.agg_kpi_retarget_1218_payer;
create table processed.agg_kpi_retarget_1218_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1218_payer rd on d.uid = rd.uid
where date >= '2015-12-18'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1225 payer
drop table if exists processed.retarget_date_1225_payer;
create table processed.retarget_date_1225_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1225) r on d.uid = r.uid
where date >= '2015-12-25'
group by 1;

drop table if exists processed.agg_kpi_retarget_1225_payer;
create table processed.agg_kpi_retarget_1225_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1225_payer rd on d.uid = rd.uid
where date >= '2015-12-25'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 1231 payer
drop table if exists processed.retarget_date_1231_payer;
create table processed.retarget_date_1231_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_1231) r on d.uid = r.uid
where date >= '2015-12-31'
group by 1;

drop table if exists processed.agg_kpi_retarget_1231_payer;
create table processed.agg_kpi_retarget_1231_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1231_payer rd on d.uid = rd.uid
where date >= '2015-12-31'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0107 payer
drop table if exists processed.retarget_date_0107_payer;
create table processed.retarget_date_0107_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0107) r on d.uid = r.uid
where date >= '2016-01-07'
group by 1;

drop table if exists processed.agg_kpi_retarget_0107_payer;
create table processed.agg_kpi_retarget_0107_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0107_payer rd on d.uid = rd.uid
where date >= '2016-01-07'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0115 payer
drop table if exists processed.retarget_date_0115_payer;
create table processed.retarget_date_0115_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0115) r on d.uid = r.uid
where date >= '2016-01-15'
group by 1;

drop table if exists processed.agg_kpi_retarget_0115_payer;
create table processed.agg_kpi_retarget_0115_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0115_payer rd on d.uid = rd.uid
where date >= '2016-01-15'
group by 1,2,3,4,5,6,7,8,9,10;

-- re-targeting campaign KPI for 0122 payer
drop table if exists processed.retarget_date_0122_payer;
create table processed.retarget_date_0122_payer as
select  d.uid
      ,min(d.date) as retarget_date
from processed.fact_dau_snapshot d
    join (select distinct uid from processed.re_target_campaign_d15_inactive_payer_0122) r on d.uid = r.uid
where date >= '2016-01-22'
group by 1;

drop table if exists processed.agg_kpi_retarget_0122_payer;
create table processed.agg_kpi_retarget_0122_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,u.is_payer
      ,d.ab_test
      ,u.vip_level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0122_payer rd on d.uid = rd.uid
where date >= '2016-01-22'
group by 1,2,3,4,5,6,7,8,9,10;
-- drop table if exists marketing.agg_kpi_retarget_nonpayer;
-- create table processed.agg_kpi_retarget_nonpayer as
-- select '2015-08-12' as retarget_date, *
-- from processed.agg_kpi_retarget_0812_nonpayer;

drop table if exists processed.agg_kpi_retarget_payer;
create table processed.agg_kpi_retarget_payer as
select '2016-01-22' as retarget_date, *
from processed.agg_kpi_retarget_0122_payer
union
select '2016-01-15' as retarget_date, *
from processed.agg_kpi_retarget_0115_payer
union
select '2016-01-07' as retarget_date, *
from processed.agg_kpi_retarget_0107_payer
union
select '2015-12-31' as retarget_date, *
from processed.agg_kpi_retarget_1231_payer
union
select '2015-12-25' as retarget_date, *
from processed.agg_kpi_retarget_1225_payer
union
select '2015-12-18' as retarget_date, *
from processed.agg_kpi_retarget_1218_payer
union
select '2015-12-11' as retarget_date, *
from processed.agg_kpi_retarget_1211_payer
union
select '2015-12-04' as retarget_date, *
from processed.agg_kpi_retarget_1204_payer
union
select '2015-11-27' as retarget_date, *
from processed.agg_kpi_retarget_1127_payer
union
select '2015-11-20' as retarget_date, *
from processed.agg_kpi_retarget_1120_payer
union
select '2015-11-13' as retarget_date, *
from processed.agg_kpi_retarget_1113_payer
union
select '2015-11-06' as retarget_date, *
from processed.agg_kpi_retarget_1106_payer
union
select '2015-10-30' as retarget_date, *
from processed.agg_kpi_retarget_1030_payer
union
select '2015-10-23' as retarget_date, *
from processed.agg_kpi_retarget_1023_payer
union
select '2015-10-08' as retarget_date, *
from processed.agg_kpi_retarget_1008_payer
union
select '2015-09-24' as retarget_date, *
from processed.agg_kpi_retarget_0924_payer
union
select '2015-09-17' as retarget_date, *
from processed.agg_kpi_retarget_0917_payer
union
select '2015-09-11' as retarget_date, *
from processed.agg_kpi_retarget_0911_payer
union
select '2015-09-02' as retarget_date, *
from processed.agg_kpi_retarget_0902_payer
union
select '2015-08-27' as retarget_date, *
from processed.agg_kpi_retarget_0827_payer
union
select '2015-08-21' as retarget_date, *
from processed.agg_kpi_retarget_0821_payer
union
select '2015-08-12' as retarget_date, *
from processed.agg_kpi_retarget_0812_payer;
-- TODO: agg_stones_transaction
delete from  processed.agg_stones_transaction
where  date >= (
                     select raw_data_start_date
                     from   mt2.processed.tmp_start_date
               );

insert into processed.agg_stones_transaction
(
    date
    ,transaction_type
    ,user_key
    ,app
    ,uid
    ,app_version
    ,server
    ,os
    ,country
    ,install_source
    ,is_payer
    ,level
    ,vip_level
    ,reason_id
    ,reason
    ,reason_detail
    ,sub_reason

    ,stones_in
    ,stones_out
)
select
    m.date
    ,case
        when m.in_or_out = 'in' then 'stones_in'
        when m.in_or_out = 'out' then 'stones_out'
        else 'Unknown'
    end as transaction_type
    ,m.user_key
    ,m.app
    ,m.uid
    ,m.app_version
    ,m.server
    ,u.os
    ,u.country
    ,u.install_source
    ,u.is_payer
    ,m.level
    ,m.vip_level
    ,coalesce(m.reason, '') as reason_id
    ,coalesce(r.name, '') || '[' || m.reason || ']' as reason
    ,coalesce(r.detail, '') || '[' || m.reason || ']' as reason_detail
    ,m.sub_reason
    ,sum(case when m.in_or_out = 'in' then m.i_money else 0 end) as stones_in
    ,sum(case when m.in_or_out = 'out' then m.i_money else 0 end) as stones_out
from processed.fact_money_transaction m
    join processed.dim_user u on m.user_key = u.user_key
    left join processed.ref_transaction_reason_map r on m.reason = r.id
where date >= (
                    select raw_data_start_date
                    from   mt2.processed.tmp_start_date
              )
      and i_money_type = 'stones'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;

-- TODO: agg_coins_transaction
delete from  processed.agg_coins_transaction
where  date >= (
                     select raw_data_start_date
                     from   mt2.processed.tmp_start_date
               );

insert into processed.agg_coins_transaction
(
    date
    ,transaction_type
    ,user_key
    ,app
    ,uid
    ,app_version
    ,server
    ,os
    ,country
    ,install_source
    ,is_payer
    ,level
    ,vip_level
    ,reason_id
    ,reason
    ,reason_detail
    ,sub_reason

    ,coins_in
    ,coins_out
)
select
    m.date
    ,case
        when m.in_or_out = 'in' then 'coins_in'
        when m.in_or_out = 'out' then 'coins_out'
        else 'Unknown'
    end as transaction_type
    ,m.user_key
    ,m.app
    ,m.uid
    ,m.app_version
    ,m.server
    ,u.os
    ,u.country
    ,u.install_source
    ,u.is_payer
    ,m.level
    ,m.vip_level
    ,coalesce(m.reason, '') as reason_id
    ,coalesce(r.name, '') || '[' || m.reason || ']' as reason
    ,coalesce(r.detail, '') || '[' || m.reason || ']' as reason_detail
    ,m.sub_reason
    ,sum(case when m.in_or_out = 'in' then m.i_money else 0 end) as coins_in
    ,sum(case when m.in_or_out = 'out' then m.i_money else 0 end) as coins_out
from processed.fact_money_transaction m
    join processed.dim_user u on m.user_key = u.user_key
    left join processed.ref_transaction_reason_map r on m.reason = r.id
where date >= (
                    select raw_data_start_date
                    from   mt2.processed.tmp_start_date
              )
      and i_money_type = 'coins'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17;


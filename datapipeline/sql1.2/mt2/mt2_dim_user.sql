--------------------------------------------------------------------------------------------------------------------------------------------
--The MT2 fact dau snapshot
--Version 1.2
--Author Robin
/**
Description:
This script is generate dim_user tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_user
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table temp_dim_user_latest
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) DEFAULT '' ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level                 SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_rc_in               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);


insert into temp_dim_user_latest
(
          user_key 
         ,uid 
         ,snsid
         ,server
         ,app 
         ,language 
         ,birth_date 
         ,gender 
         ,app_version
         ,level
         ,vip_level
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,is_payer
)
select    user_key 
         ,uid 
         ,snsid
         ,server
         ,app 
         ,language 
         ,null as birth_date 
         ,null as gender 
         ,null as app_version
         ,level_end
         ,vip_level_end
         ,os 
         ,os_version 
         ,t.country_code
         ,coalesce(c.country,'Unknown') as country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
from  (
         select *
                ,row_number() over (partition by user_key order by date desc) as row
         from processed.fact_dau_snapshot
         where date >= (
                        select start_date
                        from mt2.processed.tmp_start_date
                     )
      )t
left join processed.dim_country c on c.country_code=t.country_code
where t.row = 1;

-- create temp table to get the min install_ts
create temp table temp_min_install_ts as
select s.user_key, min(s.install_ts) as install_ts
from processed.fact_dau_snapshot s
where date >= (
                    select start_date
                    from mt2.processed.tmp_start_date
              )
group by s.user_key;

update temp_dim_user_latest
set install_ts = t.install_ts,
    install_date = trunc(t.install_ts)
from temp_min_install_ts t
where temp_dim_user_latest.user_key = t.user_key;

-- create temp table to get the revenue for each user till now + conversion + rc_in
create temp table temp_user_last_payment as
select u.user_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
       ,sum(rc_in) as rc_in
from temp_dim_user_latest u
join processed.fact_revenue r on r.user_key=u.user_key
group by 1;

update temp_dim_user_latest
set total_revenue_usd = t.revenue,
    total_rc_in = t.rc_in,
    conversion_ts = t.conversion_ts
from temp_user_last_payment t
where temp_dim_user_latest.user_key = t.user_key;

-- update install source
update temp_dim_user_latest
set install_source = f.install_source,
    campaign = f.campaign,
    sub_publisher =  f.sub_publisher,
    creative_id = f.creative_id
from processed.fact_user_install_source f
where temp_dim_user_latest.user_key = f.user_key;

update temp_dim_user_latest
set install_source = 'Virtual Phone'
where device = 'unknown VM5 Virtual Phone';

-- create temp table temp_user_last_login
create temp table temp_user_last_login as
select user_key, max(ts_start) as last_login_ts
from processed.fact_session
group by 1;


update temp_dim_user_latest
set last_login_ts = t.last_login_ts
from temp_user_last_login t
where temp_dim_user_latest.user_key = t.user_key;

-- delete old user status in dim_user
delete from processed.dim_user
where user_key in
(
 select user_key from temp_dim_user_latest
);

-- insert the new status of users
insert into processed.dim_user
(
  user_key
  ,app
  ,uid
  ,snsid
  ,server
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,vip_level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,total_rc_in
  ,last_login_ts
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,server
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,vip_level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,total_rc_in
  ,last_login_ts
from temp_dim_user_latest;



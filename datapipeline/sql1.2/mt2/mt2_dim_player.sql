--------------------------------------------------------------------------------------------------------------------------------------------
--MT2 dim player
--Version 1.2
--Author Robin
/**
Description:
This script is generate dim_player tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_player
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table temp_dim_player_latest
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) DEFAULT '' ENCODE LZO,
  player_key                VARCHAR(32) ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  player_id                 INTEGER,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level                 SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_rc_in               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(player_key)
SORTKEY(player_key, app, server, player_id);


insert into temp_dim_player_latest
(
          user_key
         ,app
         ,uid 
         ,snsid
         ,player_key
         ,server
         ,player_id
         ,language
         ,birth_date 
         ,gender 
         ,app_version
         ,level
         ,vip_level
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,is_payer
)
select    user_key 
         ,app
         ,uid
         ,snsid
         ,player_key
         ,server
         ,player_id
         ,language 
         ,null as birth_date 
         ,null as gender 
         ,null as app_version
         ,level_end
         ,vip_level_end
         ,os 
         ,os_version 
         ,country_code
         ,country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
from  (
         select *
                ,row_number() over (partition by player_key order by date desc) as row
         from processed.fact_player_dau_snapshot
         where date >= (
                        select start_date
                        from mt2.processed.tmp_start_date
                     )
      )t
where t.row = 1;

-- create temp table to get the min install_ts
create temp table temp_player_min_install_ts as
select s.player_key, min(s.install_ts) as install_ts
from processed.fact_player_dau_snapshot s
where date >= (
                    select start_date
                    from mt2.processed.tmp_start_date
              )
group by s.player_key;

update temp_dim_player_latest
set install_ts = t.install_ts,
    install_date = trunc(t.install_ts)
from temp_player_min_install_ts t
where temp_dim_player_latest.player_key = t.player_key;

-- create temp table to get the revenue for each player till now + conversion + rc_in
create temp table temp_player_last_payment as
select u.player_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
       ,sum(rc_in) as rc_in
from temp_dim_player_latest u
join processed.fact_revenue r on r.player_key=u.player_key
where r.date < CURRENT_DATE
group by 1;

update temp_dim_player_latest
set total_revenue_usd = t.revenue,
    total_rc_in = t.rc_in,
    conversion_ts = t.conversion_ts
from temp_player_last_payment t
where temp_dim_player_latest.player_key = t.player_key;

-- update install source
update temp_dim_player_latest
set install_source = f.install_source,
    campaign = f.campaign,
    sub_publisher =  f.sub_publisher,
    creative_id = f.creative_id
from processed.fact_user_install_source f
where temp_dim_player_latest.user_key = f.user_key;

update temp_dim_player_latest
set install_source = 'Virtual Phone'
where device = 'unknown VM5 Virtual Phone';

-- create temp table temp_player_last_login
create temp table temp_player_last_login as
select player_key, max(ts_start) as last_login_ts
from processed.fact_session
group by 1;


update temp_dim_player_latest
set last_login_ts = t.last_login_ts
from temp_player_last_login t
where temp_dim_player_latest.player_key = t.player_key;

-- delete old player status in dim_player
delete from processed.dim_player
where player_key in
(
 select player_key from temp_dim_player_latest
);

-- insert the new status of players
insert into processed.dim_player
(
  user_key
  ,app
  ,uid
  ,snsid
  ,player_key
  ,server
  ,player_id
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,vip_level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,total_rc_in
  ,last_login_ts
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,player_key
  ,server
  ,player_id
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,vip_level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,total_rc_in
  ,last_login_ts
from temp_dim_player_latest;

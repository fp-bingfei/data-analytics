----------------------------------------------------------------------------------------------------------------------------------------------
-- TODO: remove invalid data
----------------------------------------------------------------------------------------------------------------------------------------------
insert into processed.raw_data_s3_invalid
select *
from processed.raw_data_s3
where regexp_substr(json_str, '^\\\{.*\\\}$') = '';

delete
from processed.raw_data_s3
where regexp_substr(json_str, '^\\\{.*\\\}$') = '';

insert into processed.raw_data_s3_invalid
select *
from processed.raw_data_s3
where trunc(load_hour) < '2015-04-01' and
(trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) >= '2015-04-01' or
case
    when json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') = '' then dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')
    else dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00')
end >= '2015-04-01');

delete
from processed.raw_data_s3
where trunc(load_hour) < '2015-04-01' and
(trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) >= '2015-04-01' or
case
    when json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') = '' then dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')
    else dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00')
end >= '2015-04-01');


----------------------------------------------------------------------------------------------------------------------------------------------
-- TODO: import data
----------------------------------------------------------------------------------------------------------------------------------------------

-- 2015-05-22
truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.mt2/mt2.global.prod/2015/05/22/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

delete from processed.raw_data_s3 where load_hour = '2015-05-22 00:00:00';

insert into processed.raw_data_s3
select json_str, '2015-05-22 00:00:00' as load_hour
from processed.raw_data_s3_test;


-- TODO: remove duplicated rows
drop table processed.events_raw_duplicated_raw;
CREATE TABLE processed.events_raw_duplicated_raw
(
	load_hour           TIMESTAMP ENCODE DELTA,
	event               VARCHAR(32) ENCODE BYTEDICT,
	date                DATE NOT NULL ENCODE DELTA,
	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
	app_id              VARCHAR(32) NOT NULL ENCODE BYTEDICT,
	user_id             INTEGER NOT NULL,
	gameserver_id       VARCHAR(16) ENCODE BYTEDICT,
	install_ts          TIMESTAMP ENCODE DELTA,
	country_code        VARCHAR(8) ENCODE BYTEDICT,
	level               SMALLINT,
	ip                  VARCHAR(16) ENCODE LZO,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	os_version          VARCHAR(64) ENCODE BYTEDICT,
	app_version         VARCHAR(32) ENCODE BYTEDICT,
	session_id          VARCHAR(32) ENCODE LZO,
	bi_version          VARCHAR(32) ENCODE BYTEDICT,
	idfa                VARCHAR(64) ENCODE LZO,
	mac_address         VARCHAR(64) ENCODE LZO,
	device              VARCHAR(32) ENCODE BYTEDICT,
	lang                VARCHAR(32) ENCODE BYTEDICT,
	json_str            VARCHAR(1024) ENCODE LZO,
	properties          VARCHAR(1024) ENCODE LZO,
	md5                 VARCHAR(32) ENCODE LZO
)
  DISTKEY(md5)
  SORTKEY(md5);

insert into processed.events_raw_duplicated_raw
select * from processed.events_raw;


drop table if exists processed.duplicated_md5;
create table processed.duplicated_md5 as
select md5, count(1)
from processed.events_raw_duplicated_raw
group by 1
having count(1) > 1;


create table processed.events_raw_duplicated as
select r.*
from processed.events_raw_duplicated_raw r
	join processed.duplicated_md5 m on r.md5 = m.md5
where ts < '2015-03-31'
union
select r.*
from processed.events_raw_duplicated_raw r
	join processed.duplicated_md5 m on r.md5 = m.md5
where ts >= '2015-03-31';

create table processed.events_raw_duplicated_all as
select t.*, row_number() over (partition by md5 order by load_hour) as rank
from
(select r.*
from processed.events_raw_duplicated_raw r
	join processed.duplicated_md5 m on r.md5 = m.md5) t;

select load_hour, event, count(1)
from processed.events_raw_duplicated
group by 1,2;

delete
from processed.events_raw_duplicated_raw
using processed.duplicated_md5 m
where processed.events_raw_duplicated_raw.md5 = m.md5;

insert into processed.events_raw_duplicated_raw
select *
from processed.events_raw_duplicated;

truncate table processed.events_raw;

insert into processed.events_raw
select *
from processed.events_raw_duplicated_raw;

truncate table processed.raw_data_s3;

insert into processed.raw_data_s3 (json_str, load_hour)
select json_str, load_hour
from processed.events_raw;

create table processed.raw_data_s3_backup_0404 as
select *
from processed.raw_data_s3;


-- check installs
drop table if exists processed.invest_install_list;
CREATE TABLE processed.invest_install_list
(
	uid                 INTEGER,
	player_id           INTEGER,
	idfa                VARCHAR(64) ENCODE LZO,
	ts                  TIMESTAMP ENCODE DELTA,
	days                INTEGER
)
  SORTKEY(uid);

copy processed.invest_install_list
from 's3://com.funplus.bitest/mt2/invest/我叫MT2安裝明細.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
CSV
IGNOREHEADER 1;

-- TODO: 2) 4月8日当日活跃玩家所剩符石数量平均值（当日登陆过且等级大于10的玩家）
-- fact_money_transaction_day_last
drop table if exists processed.fact_money_transaction_day_last;
create table processed.fact_money_transaction_day_last as
select t.*
from
(select *, row_number() over (partition by date, user_key order by ts desc) as row_number
from processed.fact_money_transaction) t
where t.row_number = 1;

select
	count(1)
	,count(distinct user_key) as users
	,sum(case
	        when i_money_type = 'coins' then stones
	        when i_money_type = 'stones' then after_money
	     end) as total_stones
	,sum(case
	        when i_money_type = 'coins' then stones
	        when i_money_type = 'stones' then after_money
	     end) / count(distinct user_key) as avg_stones
from processed.fact_money_transaction_day_last
where date = '2015-04-08' and level >= 10;

count	users	total_stones	avg_stones
30591	30591	50421787	1648

TODO: 3) 玩家二次付费的人数
drop table if exists processed.payment_count;
create table processed.payment_count as
select user_key, count(1) as purchase_count
from processed.fact_revenue
where date >= '2015-03-26'
group by user_key
having count(1) >= 2;

select count(distinct user_key) as users
from processed.payment_count;
-- 5283

TODO: 4) 玩家二次付费的平均周期
create temp table payment_twice as
select r.*
from processed.fact_revenue r
    join processed.payment_count c on r.user_key = c.user_key
where r.date >= '2015-03-26';

create temp table payment_first_2_temp as
select *
from
(select *, row_number() over (partition by user_key order by ts) as row_number
from payment_twice) t
where t.row_number in (1,2);

drop table if exists processed.payment_first_2;
create table processed.payment_first_2 as
select p1.user_key, p1.ts as pay1_ts, p2.ts as pay2_ts, datediff(minutes, p1.ts, p2.ts) as pay_period
from payment_first_2_temp p1
    join payment_first_2_temp p2 on p1.row_number = 1 and p2.row_number = 2 and p1.user_key = p2.user_key;

select
    count(1)
    ,count(distinct user_key) as users
    ,sum(pay_period) as total_pay_period
    ,sum(pay_period) / count(distinct user_key) as avg_pay_period
from processed.payment_first_2;

-- create new database account
select * from pg_user;

create schema mt2_pm;

grant usage on schema mt2_pm to guoguang;
grant create on schema mt2_pm to guoguang;
grant all on all tables in schema mt2_pm to guoguang;

grant usage on schema processed to guoguang;
grant select on all tables in schema processed to guoguang;

grant usage on schema public to guoguang;
grant select on all tables in schema public to guoguang;

revoke all on all tables in schema processed from guoguang;
revoke all on all tables in schema public from guoguang;

-- TODO: lookalike_campaign
create table lookalike_campaign_idfa_0506 as
select s.idfa
from
(select user_key, count(1) as play_days, max(level_end) as max_level
from processed.fact_dau_snapshot
where install_date + 3 <= date
group by 1
having count(1) > 1) t
join processed.fact_session s on t.user_key = s.user_key
where t.max_level >= 10 and s.idfa != ''
union
select s.idfa
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.idfa != '' and u.is_payer = 1;

unload ('select * from lookalike_campaign_idfa_0506;')
to 's3://com.funplus.bitest/mt2/lookalike_campaign/lookalike_campaign_idfa_0506_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

create table lookalike_campaign_gaid_0506 as
select s.gaid
from
(select user_key, count(1) as play_days, max(level_end) as max_level
from processed.fact_dau_snapshot
where install_date + 3 <= date
group by 1
having count(1) > 1) t
join processed.fact_session s on t.user_key = s.user_key
where t.max_level >= 10 and s.gaid != ''
union
select s.gaid
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.gaid != '' and u.is_payer = 1;

unload ('select * from lookalike_campaign_gaid_0506;')
to 's3://com.funplus.bitest/mt2/lookalike_campaign/lookalike_campaign_gaid_0506_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re-targeting campaign
create table marketing.retarget_d3_inactive_payer_ios_idfa as
select distinct idfa
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where u.last_login_ts < '2015-05-18' and u.is_payer = 1 and idfa != '';

create table marketing.retarget_d3_inactive_payer_android_gaid as
select distinct gaid
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where u.last_login_ts < '2015-05-18' and u.is_payer = 1 and s.gaid != '';

create table marketing.retarget_d3_inactive_payer_android_android_id as
select distinct android_id
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where u.last_login_ts < '2015-05-18' and u.is_payer = 1 and s.android_id != '';


-- TODO: remove invalid data
insert into processed.raw_data_s3_invalid
select *
from processed.raw_data_s3
where load_hour = '2015-05-20 00:00:00' and regexp_substr(json_str, '.*\,  \\\}\\\}$') != '';

delete
from processed.raw_data_s3
where load_hour = '2015-05-20 00:00:00' and regexp_substr(json_str, '.*\,  \\\}\\\}$') != '';

insert into processed.raw_data_s3_invalid
select *
from processed.raw_data_s3
where load_hour = '2015-05-20 00:00:00' and json_extract_path_text(json_str, 'user_id') = '';

delete
from processed.raw_data_s3
where load_hour = '2015-05-20 00:00:00' and json_extract_path_text(json_str, 'user_id') = '';


-- TODO: MT2_Retargeting Users (lvl 12以上的玩家)
create table marketing.retargeting_level_12_nonpayer_idfa as
select distinct idfa
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) >= '2015-03-27' and
    trunc(u.last_login_ts) <= '2015-05-21' and
    u.is_payer = 0 and
    s.level_end >= 12 and
    idfa != '';

create table marketing.retargeting_level_12_nonpayer_gaid as
select distinct gaid
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) >= '2015-03-27' and
    trunc(u.last_login_ts) <= '2015-05-21' and
    u.is_payer = 0 and
    s.level_end >= 12 and
    gaid != '';

create table marketing.retargeting_level_12_nonpayer_android_id as
select distinct android_id
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) >= '2015-03-27' and
    trunc(u.last_login_ts) <= '2015-05-21' and
    u.is_payer = 0 and
    s.level_end >= 12 and
    android_id != '';

unload ('select * from marketing.retargeting_level_12_nonpayer_idfa;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_12_nonpayer_idfa_0527_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_12_nonpayer_gaid;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_12_nonpayer_gaid_0527_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_12_nonpayer_android_id;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_12_nonpayer_android_id_0527_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

select * from pg_user;
create user "mt2_pm" with password '';

create schema mt2_pm;

grant usage on schema mt2_pm to "mt2_pm";
grant create on schema mt2_pm to "mt2_pm";
grant all on all tables in schema mt2_pm to "mt2_pm";

grant usage on schema processed to "mt2_pm";
grant select on all tables in schema processed to "mt2_pm";

grant usage on schema public to "mt2_pm";
grant select on all tables in schema public to "mt2_pm";

revoke all on all tables in schema processed from mt2_pm;
revoke all on all tables in schema public from mt2_pm;

-- TODO: Import missing data
drop table processed.fact_session_missing_data;
CREATE TABLE processed.fact_session_missing_data
(
  date_start            TIMESTAMP ENCODE DELTA,
  user_key              VARCHAR(32) ENCODE LZO,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  uid                   INTEGER,
  snsid                 VARCHAR(32) DEFAULT '' ENCODE LZO,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          TIMESTAMP ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts_start              TIMESTAMP ENCODE DELTA,
  ts_end                TIMESTAMP ENCODE DELTA,
  level_start           SMALLINT,
  level_end             SMALLINT,
  vip_level_start       SMALLINT,
  vip_level_end         SMALLINT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(128) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(16) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(64) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  rc_bal_start          INTEGER,
  rc_bal_end            INTEGER,
  coin_bal_start        INTEGER,
  coin_bal_end          INTEGER,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  session_length        INTEGER
)
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, player_key, app, uid, server, player_id);

truncate table processed.fact_session_missing_data;
copy processed.fact_session_missing_data
from 's3://com.funplus.bitest/mt2/missing_data/fact_session.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\,'
IGNOREHEADER 1;

drop table if exists processed.fact_revenue_missing_data;
CREATE TABLE processed.fact_revenue_missing_data
(
  date                  TIMESTAMP ENCODE DELTA,
  user_key              VARCHAR(32) ENCODE LZO,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  uid                   INTEGER,
  snsid                 VARCHAR(32) DEFAULT '' ENCODE LZO,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          TIMESTAMP ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts                    TIMESTAMP ENCODE DELTA,
  level                 SMALLINT,
  vip_level             SMALLINT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(128) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(16) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(64) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,

  payment_processor     VARCHAR(32) ENCODE BYTEDICT,
  product_id            VARCHAR(64) ENCODE BYTEDICT,
  product_name          VARCHAR(64) ENCODE BYTEDICT,
  product_type          VARCHAR(32) ENCODE BYTEDICT,
  coins_in              INTEGER,
  rc_in                 INTEGER,
  currency              VARCHAR(32) ENCODE BYTEDICT,
  amount                DECIMAL(14,4) DEFAULT 0,
  usd                   DECIMAL(14,4) DEFAULT 0,
  transaction_id        VARCHAR(64) ENCODE LZO
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, player_key, app, uid, server, player_id);

truncate table processed.fact_revenue_missing_data;
copy processed.fact_revenue_missing_data
from 's3://com.funplus.bitest/mt2/missing_data/fact_revenue.csv'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\,'
IGNOREHEADER 1;

-- Update session data
update processed.fact_session_missing_data
set install_ts = CONVERT_TIMEZONE ( 'UTC-8', 'UTC', install_ts);

update processed.fact_session_missing_data
set date_start = trunc(install_ts),
	install_date = trunc(install_ts),
	ts_start = install_ts,
	ts_end = install_ts,
	session_length = 0,
	level_start = 1,
	level_end = 1,
	vip_level_start = 0,
	vip_level_end = 0,
	app = 'mt2.global.prod';

update processed.fact_session_missing_data
set user_key = MD5(app || uid),
    player_key = MD5(server || uid);

update processed.fact_session_missing_data
set country_code = c.country_code
from processed.dim_country c
where processed.fact_session_missing_data.country_code = c.country;

update processed.fact_session_missing_data
set country_code = '--'
where length(country_code) > 2;

select *
from processed.fact_session_missing_data
limit 100;

-- Update revenue data
update processed.fact_revenue_missing_data
set ts = CONVERT_TIMEZONE ( 'UTC-8', 'UTC', ts),
	install_ts = CONVERT_TIMEZONE ( 'UTC-8', 'UTC', install_ts);

update processed.fact_revenue_missing_data
set date = trunc(ts),
    install_date = trunc(install_ts),
    level = 1,
	vip_level = 1,
	app = 'mt2.global.prod';

update processed.fact_revenue_missing_data
set user_key = MD5(app || uid),
    player_key = MD5(server || uid);

update processed.fact_revenue_missing_data
set country_code = c.country_code
from processed.dim_country c
where processed.fact_revenue_missing_data.country_code = c.country;

update processed.fact_revenue_missing_data
set country_code = '--'
where length(country_code) > 2;

update processed.fact_revenue_missing_data
set amount = usd;

update processed.fact_revenue_missing_data
set usd = amount * case when c.factor is null then 1 else c.factor end
from public.currency c
where processed.fact_revenue_missing_data.currency = c.currency and processed.fact_revenue_missing_data.date = c.dt;

delete
from processed.fact_revenue_missing_data
where ts >= '2015-06-03 03:55:00';

delete
from processed.fact_revenue_missing_data
using processed.fact_revenue r2
where processed.fact_revenue_missing_data.uid = r2.uid and
    processed.fact_revenue_missing_data.server = r2.server and
    processed.fact_revenue_missing_data.ts = r2.ts;

select date, server, extract(hour from ts), count(1)
from processed.fact_revenue_missing_data
group by 1,2,3
order by 1,2,3;

select date, server,  extract(hour from ts), count(1)
from processed.fact_revenue
where server = '1017'
group by 1,2,3
order by 1,2,3;

select *
from processed.fact_revenue_missing_data
limit 100;

-- Insert missing data into fact tables
insert into processed.fact_session
select *
from processed.fact_session_missing_data;

insert into processed.fact_revenue
select *
from processed.fact_revenue_missing_data;

-- TODO: check adjust_duplicated
drop table if exists processed.adjust_duplicated;
create table processed.adjust_duplicated as
select userid, app_id,
	split_part(tracker_name, '::', 1) as install_source,
	split_part(tracker_name, '::', 3) as sub_publisher,
	count(1)
from adjust
where userid != '' and split_part(tracker_name, '::', 1) = 'Mobvista+(Incent)'
group by 1,2,3,4
having count(1) >= 20;

select a.app_id,
	split_part(a.tracker_name, '::', 1) as install_source,
	split_part(a.tracker_name, '::', 3) as sub_publisher,
	count(distinct a.userid) as unique_user_count,
	count(1) as adjust_count
from adjust a
	join processed.adjust_duplicated d on a.userid = d.userid
where split_part(a.tracker_name, '::', 1) = 'Mobvista+(Incent)'
group by 1,2,3
having count(1) > 20;

select a.*, split_part(a.tracker_name, '::', 3) as sub_publisher
from adjust a
	join (select distinct userid from processed.adjust_duplicated) d on a.userid = d.userid
where split_part(a.tracker_name, '::', 1) = 'Mobvista+(Incent)'
order by a.userid, a.ts;

-- TODO: MT2_Retargeting Users (lvl 15以上的玩家)
-- Taiwan Payer
create table marketing.retargeting_level_15_payer_idfa as
select distinct idfa
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.is_payer = 1 and
    u.country = 'Taiwan' and
    idfa != '';

create table marketing.retargeting_level_15_payer_gaid as
select distinct gaid
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.is_payer = 1 and
    u.country = 'Taiwan' and
    gaid != '';

create table marketing.retargeting_level_15_payer_android_id as
select distinct android_id
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.is_payer = 1 and
    u.country = 'Taiwan' and
    android_id != '';

unload ('select * from marketing.retargeting_level_15_payer_idfa;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_payer_idfa_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_15_payer_gaid;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_payer_gaid_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_15_payer_android_id;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_payer_android_id_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- Taiwan Nonpayer
create table marketing.retargeting_level_15_nonpayer_idfa as
select distinct idfa
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.is_payer = 0 and
    u.country = 'Taiwan' and
    idfa != '';

create table marketing.retargeting_level_15_nonpayer_gaid as
select distinct gaid
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.is_payer = 0 and
    u.country = 'Taiwan' and
    gaid != '';

create table marketing.retargeting_level_15_nonpayer_android_id as
select distinct android_id
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.is_payer = 0 and
    u.country = 'Taiwan' and
    android_id != '';

unload ('select * from marketing.retargeting_level_15_nonpayer_idfa;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_nonpayer_idfa_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_15_nonpayer_gaid;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_nonpayer_gaid_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_15_nonpayer_android_id;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_nonpayer_android_id_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


-- Not in Taiwan
create table marketing.retargeting_level_15_idfa_not_taiwan as
select distinct idfa
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.country != 'Taiwan' and
    idfa != '';

create table marketing.retargeting_level_15_gaid_not_taiwan as
select distinct gaid
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.country != 'Taiwan' and
    gaid != '';

create table marketing.retargeting_level_15_android_id_not_taiwan as
select distinct android_id
from processed.fact_session s
    join processed.dim_user u on s.user_key = u.user_key
where trunc(u.last_login_ts) <= '2015-06-10' and
    s.level_end >= 15 and
    u.country != 'Taiwan' and
    android_id != '';

unload ('select * from marketing.retargeting_level_15_idfa_not_taiwan;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_idfa_not_taiwan_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_15_gaid_not_taiwan;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_gaid_not_taiwan_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select * from marketing.retargeting_level_15_android_id_not_taiwan;')
to 's3://com.funplus.bitest/mt2/retargeting/retargeting_level_15_android_id_not_taiwan_0619_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: check fraud user acquisition channel
create temp table adjust_duplicated_users as
select userid, app_id,
	split_part(tracker_name, '::', 1) as install_source,
	split_part(tracker_name, '::', 3) as sub_publisher,
	count(1)
from unique_adjust
where userid != ''
group by 1,2,3,4
having count(1) >= 20;

select a.app_id,
	split_part(a.tracker_name, '::', 1) as install_source,
	split_part(a.tracker_name, '::', 3) as sub_publisher,
	count(distinct a.userid) as unique_user_count,
	count(1) as adjust_count
from unique_adjust a
	join adjust_duplicated_users d on a.userid = d.userid
group by 1,2,3
having count(1) > 20;

select a.*, split_part(a.tracker_name, '::', 3) as sub_publisher
from unique_adjust a
	join (select distinct userid from adjust_duplicated_users) d on a.userid = d.userid
where split_part(a.tracker_name, '::', 1) = 'Mobvista+(Incent)'
order by a.userid, a.ts;


grant usage on schema mt2_pm to "siyuan.zhang";
grant create on schema mt2_pm to "siyuan.zhang";
grant all on all tables in schema mt2_pm to "siyuan.zhang";

grant usage on schema processed to "siyuan.zhang";
grant select on all tables in schema processed to "siyuan.zhang";

grant usage on schema public to "siyuan.zhang";
grant select on all tables in schema public to "siyuan.zhang";


-- TODO: lookalike_campaign
-- iOS
drop table if exists lookalike_campaign_idfa_0731;
create table lookalike_campaign_idfa_0731 as
select s.idfa
from
(select user_key, count(1) as play_days, max(level_end) as max_level
from processed.fact_dau_snapshot
where install_date + 3 <= date
group by 1
having count(1) > 1) t
join processed.fact_session s on t.user_key = s.user_key
where t.max_level >= 10 and s.idfa != ''
union
select s.idfa
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.idfa != '' and u.is_payer = 1;

unload ('select * from lookalike_campaign_idfa_0731;')
to 's3://com.funplus.bitest/mt2/lookalike_campaign/lookalike_campaign_idfa_0731_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- Android
drop table if exists lookalike_campaign_gaid_0731;
create table lookalike_campaign_gaid_0731 as
select s.gaid
from
(select user_key, count(1) as play_days, max(level_end) as max_level
from processed.fact_dau_snapshot
where install_date + 3 <= date
group by 1
having count(1) > 1) t
join processed.fact_session s on t.user_key = s.user_key
where t.max_level >= 10 and s.gaid != ''
union
select s.gaid
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.gaid != '' and u.is_payer = 1;

unload ('select * from lookalike_campaign_gaid_0731;')
to 's3://com.funplus.bitest/mt2/lookalike_campaign/lookalike_campaign_gaid_0731_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--multiple mappings idfa*uid
drop table if exists processed.uid_idfa_cross_validation;
create table processed.uid_idfa_cross_validation as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
  join processed.fact_session s on u.user_key = s.user_key
where u.level >= 5 and (idfa != '' or gaid != '');

select count(1) from processed.uid_idfa_cross_validation
select count(1) from (select distinct idfa from processed.uid_idfa_cross_validation)
select count(1) from (select distinct uid from processed.uid_idfa_cross_validation)
select count(1) from (select idfa from processed.uid_idfa_cross_validation group by idfa having count(uid) > 5 );
select count(1) from (select uid from processed.uid_idfa_cross_validation group by uid having count(idfa) > 5 );

-- consecutive login for 7 days 
create temp table mt2_login_for_7_days as 
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid from 
(select distinct uid, user_key from 
(select * from 
(select date
, uid
, user_key
, coalesce(datediff(day, date, lag(date, 1) over (partition by uid order by date desc)),10) as diff1
, coalesce(datediff(day, date, lag(date, 2) over (partition by uid order by date desc)),10) as diff2
, coalesce(datediff(day, date, lag(date, 3) over (partition by uid order by date desc)),10) as diff3
, coalesce(datediff(day, date, lag(date, 4) over (partition by uid order by date desc)),10) as diff4
, coalesce(datediff(day, date, lag(date, 5) over (partition by uid order by date desc)),10) as diff5
, coalesce(datediff(day, date, lag(date, 6) over (partition by uid order by date desc)),10) as diff6
, coalesce(datediff(day, date, lag(date, 7) over (partition by uid order by date desc)),10) as diff7
 from processed.fact_dau_snapshot
) t1 
where diff1 = 1 and diff2 = 2 and diff3 = 3 and diff4 = 4 and diff5 = 5 and diff6 = 6 and diff7 = 7 
) t2
) t3 join processed.fact_session s on t3.user_key = s.user_key;

unload ('select * from mt2_login_for_7_days;')
to 's3://com.funplus.bitest/mt2/retargeting/mt2_login_for_7_days_1028_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- Play 5+ times in the last 30 days
create temp table mt2_5_login_1028 as 
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid from
(select user_key, uid from processed.fact_dau_snapshot where date > dateadd(day, -30, current_date) group by 1,2 having count(1) > 4)t 
join processed.fact_session s on t.user_key = s.user_key;

unload ('select * from mt2_5_login_1028;')
to 's3://com.funplus.bitest/mt2/retargeting/mt2_5_login_1028_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--is payer = 1 || level > 45

create temp table mt2_payer_list as 
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid from processed.dim_user u
join processed.fact_session s on s.user_key = u.user_key where u.ispayer = 1 or u.level > 45


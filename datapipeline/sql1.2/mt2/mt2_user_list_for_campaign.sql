-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0812;
create table processed.re_target_campaign_d15_inactive_payer_0812 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0812
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0812.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0812;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0812_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: lookalike_campaign
-- 注册第二天登录的用户
drop table if exists processed.lookalike_campaign_idfa_0812;
create table processed.lookalike_campaign_idfa_0812 as
select distinct s.uid, upper(s.idfa) as idfa
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
	join processed.fact_session s on d.user_key = s.user_key
where d.date = d.install_date + 1 and idfa != '';

unload ('select * from processed.lookalike_campaign_idfa_0812;')
to 's3://com.funplus.bitest/mt2/lookalike_campaign/lookalike_campaign_idfa_0812_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0821;
create table processed.re_target_campaign_d15_inactive_payer_0821 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= '2015-08-06' and date_start <= '2015-08-21';

delete
from processed.re_target_campaign_d15_inactive_payer_0821
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0821.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0821;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0821_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0827;
create table processed.re_target_campaign_d15_inactive_payer_0827 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0827
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0827.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0827;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0827_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0902;
create table processed.re_target_campaign_d15_inactive_payer_0902 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0902
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0902.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0902;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0902_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0911;
create table processed.re_target_campaign_d15_inactive_payer_0911 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0911
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0911.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0911;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0911_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0917;
create table processed.re_target_campaign_d15_inactive_payer_0917 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0917
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0917.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0917;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0917_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0924;
create table processed.re_target_campaign_d15_inactive_payer_0924 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0924
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0924.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0924;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0924_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1008;
create table processed.re_target_campaign_d15_inactive_payer_1008 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1008
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1008.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1008;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1008_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1023;
create table processed.re_target_campaign_d15_inactive_payer_1023 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1023
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1023.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1023;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1023_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1030;
create table processed.re_target_campaign_d15_inactive_payer_1030 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1030
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1030.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1030;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1030_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1106;
create table processed.re_target_campaign_d15_inactive_payer_1106 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1106
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1106.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1106;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1106_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1113;
create table processed.re_target_campaign_d15_inactive_payer_1113 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1113
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1113.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1113;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1113_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1120;
create table processed.re_target_campaign_d15_inactive_payer_1120 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1120
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1120.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1120;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1120_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1127;
create table processed.re_target_campaign_d15_inactive_payer_1127 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1127
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1127.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1127;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1127_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1204;
create table processed.re_target_campaign_d15_inactive_payer_1204 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1204
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1204.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1204;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1204_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1211;
create table processed.re_target_campaign_d15_inactive_payer_1211 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1211
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1211.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1211;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1211_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1218;
create table processed.re_target_campaign_d15_inactive_payer_1218 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1218
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1218.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1218;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1218_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1225;
create table processed.re_target_campaign_d15_inactive_payer_1225 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1225
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1225.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1225;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1225_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_1231;
create table processed.re_target_campaign_d15_inactive_payer_1231 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_1231
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_1231.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_1231;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_1231_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0107;
create table processed.re_target_campaign_d15_inactive_payer_0107 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0107
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0107.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0107;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0107_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0115;
create table processed.re_target_campaign_d15_inactive_payer_0115 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0115
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0115.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0115;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0115_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- TODO: re_targeting list
-- 15天未登录的payer
drop table if exists processed.re_target_campaign_d15_inactive_payer_0122;
create table processed.re_target_campaign_d15_inactive_payer_0122 as
select distinct s.uid, upper(s.idfa) as idfa, upper(s.gaid) as gaid
from processed.dim_user u
	join processed.fact_session s on u.user_key = s.user_key
where u.is_payer = 1 and (idfa != '' or gaid != '');

create temp table users_login_in_15days as
select distinct uid
from processed.fact_session
where date_start >= CURRENT_DATE - 15;

delete
from processed.re_target_campaign_d15_inactive_payer_0122
using users_login_in_15days u
where processed.re_target_campaign_d15_inactive_payer_0122.uid = u.uid;

unload ('select * from processed.re_target_campaign_d15_inactive_payer_0122;')
to 's3://com.funplus.bitest/mt2/retargeting/re_target_campaign_d15_inactive_payer_0122_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
--------------------------------------------------------------------------------------------------------------------------------------------
--MT2 tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate marketing tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

truncate table processed.tab_marketing_kpi;

create temp table tmp_new_install_users as
select user_key, app, uid, install_date, install_source, campaign, sub_publisher, creative_id, country, os, level, is_payer
from processed.dim_user;

create temp table tmp_all_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
group by user_key, app, uid;

create temp table tmp_d1_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 1
group by user_key, app, uid;

create temp table tmp_d3_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 3
group by user_key, app, uid;

create temp table tmp_d7_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 7
group by user_key, app, uid;

create temp table tmp_d30_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 30
group by user_key, app, uid;

create temp table tmp_d60_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 60
group by user_key, app, uid;

create temp table tmp_d90_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 90
group by user_key, app, uid;

create temp table tmp_d120_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 120
group by user_key, app, uid;

create temp table tmp_d150_revenue as
select user_key, app, uid, max(is_payer) as is_payer, sum(revenue_usd) as revenue
from processed.fact_dau_snapshot
where date - install_date <= 150
group by user_key, app, uid;

create temp table tmp_d1_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 1
group by user_key, app, uid;

create temp table tmp_d3_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 3
group by user_key, app, uid;

create temp table tmp_d7_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 7
group by user_key, app, uid;

create temp table tmp_d30_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 30
group by user_key, app, uid;

create temp table tmp_d60_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 60
group by user_key, app, uid;

create temp table tmp_d90_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 90
group by user_key, app, uid;

create temp table tmp_d120_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 120
group by user_key, app, uid;

create temp table tmp_d150_retained as
select user_key, app, uid, 1 as retained
from processed.fact_dau_snapshot
where date - install_date = 150
group by user_key, app, uid;

create temp table tmp_session_cnt as
select user_key, app, uid, sum(session_cnt) as session_cnt
from processed.fact_dau_snapshot
group by user_key, app, uid;

insert into processed.tab_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_source
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os
    ,level

    ,new_installs
    ,d1_new_installs
    ,d3_new_installs
    ,d7_new_installs
    ,d30_new_installs
    ,d60_new_installs
    ,d90_new_installs
    ,d120_new_installs
    ,d150_new_installs

    ,revenue
    ,d1_revenue
    ,d3_revenue
    ,d7_revenue
    ,d30_revenue
    ,d60_revenue
    ,d90_revenue
    ,d120_revenue
    ,d150_revenue

    ,payers
    ,d1_payers
    ,d3_payers
    ,d7_payers
    ,d30_payers
    ,d60_payers
    ,d90_payers
    ,d120_payers
    ,d150_payers

    ,d1_retained
    ,d3_retained
    ,d7_retained
    ,d30_retained
    ,d60_retained
    ,d90_retained
    ,d120_retained
    ,d150_retained

    ,level_u2
    ,level_a2u5
    ,level_a5u10
    ,level_a10
    ,session_cnt
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,u.install_source
    ,u.campaign
    ,u.sub_publisher
    ,u.creative_id
    ,case when u.country in ('Taiwan', 'Hong Kong', 'Malaysia', 'Singapore', 'Macao') then u.country else 'Other' end as country
    ,u.os
    ,u.level

    ,count(1) as new_installs
    ,count(1) as d1_new_installs
    ,count(1) as d3_new_installs
    ,count(1) as d7_new_installs
    ,count(1) as d30_new_installs
    ,count(1) as d60_new_installs
    ,count(1) as d90_new_installs
    ,count(1) as d120_new_installs
    ,count(1) as d150_new_installs

    ,sum(case when r.revenue is null then 0 else r.revenue end) as revenue
    ,sum(case when d1_r.revenue is null then 0 else d1_r.revenue end) as d1_revenue
    ,sum(case when d3_r.revenue is null then 0 else d3_r.revenue end) as d3_revenue
    ,sum(case when d7_r.revenue is null then 0 else d7_r.revenue end) as d7_revenue
    ,sum(case when d30_r.revenue is null then 0 else d30_r.revenue end) as d30_revenue
    ,sum(case when d60_r.revenue is null then 0 else d60_r.revenue end) as d60_revenue
    ,sum(case when d90_r.revenue is null then 0 else d90_r.revenue end) as d90_revenue
    ,sum(case when d120_r.revenue is null then 0 else d120_r.revenue end) as d120_revenue
    ,sum(case when d150_r.revenue is null then 0 else d150_r.revenue end) as d150_revenue

    ,sum(case when r.is_payer is null then 0 else r.is_payer end) as payers
    ,sum(case when d1_r.is_payer is null then 0 else d1_r.is_payer end) as d1_payers
    ,sum(case when d3_r.is_payer is null then 0 else d3_r.is_payer end) as d3_payers
    ,sum(case when d7_r.is_payer is null then 0 else d7_r.is_payer end) as d7_payers
    ,sum(case when d30_r.is_payer is null then 0 else d30_r.is_payer end) as d30_payers
    ,sum(case when d60_r.is_payer is null then 0 else d60_r.is_payer end) as d60_payers
    ,sum(case when d90_r.is_payer is null then 0 else d90_r.is_payer end) as d90_payers
    ,sum(case when d120_r.is_payer is null then 0 else d120_r.is_payer end) as d120_payers
    ,sum(case when d150_r.is_payer is null then 0 else d150_r.is_payer end) as d150_payers

    ,sum(case when d1_retained.retained is null then 0 else d1_retained.retained end) as d1_retained
    ,sum(case when d3_retained.retained is null then 0 else d3_retained.retained end) as d3_retained
    ,sum(case when d7_retained.retained is null then 0 else d7_retained.retained end) as d7_retained
    ,sum(case when d30_retained.retained is null then 0 else d30_retained.retained end) as d30_retained
    ,sum(case when d60_retained.retained is null then 0 else d60_retained.retained end) as d60_retained
    ,sum(case when d90_retained.retained is null then 0 else d90_retained.retained end) as d90_retained
    ,sum(case when d120_retained.retained is null then 0 else d120_retained.retained end) as d120_retained
    ,sum(case when d150_retained.retained is null then 0 else d150_retained.retained end) as d150_retained

    ,sum(case when u.level < 2 then 1 else 0 end) as level_u2
    ,sum(case when u.level >= 2 and u.level < 5 then 1 else 0 end) as level_a2u5
    ,sum(case when u.level >= 5 and u.level < 10 then 1 else 0 end) as level_a5u10
    ,sum(case when u.level >= 10 then 1 else 0 end) as level_a10
    ,sum(case when sc.session_cnt is null then 0 else sc.session_cnt end) as session_cnt
from tmp_new_install_users u
    left join tmp_all_revenue r on u.user_key = r.user_key
    left join tmp_d1_revenue d1_r on u.user_key = d1_r.user_key
    left join tmp_d3_revenue d3_r on u.user_key = d3_r.user_key
    left join tmp_d7_revenue d7_r on u.user_key = d7_r.user_key
    left join tmp_d30_revenue d30_r on u.user_key = d30_r.user_key
    left join tmp_d60_revenue d60_r on u.user_key = d60_r.user_key
    left join tmp_d90_revenue d90_r on u.user_key = d90_r.user_key
    left join tmp_d120_revenue d120_r on u.user_key = d120_r.user_key
    left join tmp_d150_revenue d150_r on u.user_key = d150_r.user_key

    left join tmp_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_d3_retained d3_retained on u.user_key = d3_retained.user_key
    left join tmp_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_d30_retained d30_retained on u.user_key = d30_retained.user_key
    left join tmp_d60_retained d60_retained on u.user_key = d60_retained.user_key
    left join tmp_d90_retained d90_retained on u.user_key = d90_retained.user_key
    left join tmp_d120_retained d120_retained on u.user_key = d120_retained.user_key
    left join tmp_d150_retained d150_retained on u.user_key = d150_retained.user_key

    left join tmp_session_cnt sc on u.user_key = sc.user_key
group by 1,2,3,4,5,6,7,8,9,10;

delete from processed.tab_marketing_kpi where install_date < '2015-03-01';

update processed.tab_marketing_kpi
set d1_new_installs = 0,
    d1_retained = 0,
    d1_revenue = 0,
    d1_payers = 0
where install_date >= CURRENT_DATE - 1;

update processed.tab_marketing_kpi
set d3_new_installs = 0,
    d3_retained = 0,
    d3_revenue = 0,
    d3_payers = 0
where install_date >= CURRENT_DATE - 3;

update processed.tab_marketing_kpi
set d7_new_installs = 0,
    d7_retained = 0,
    d7_revenue = 0,
    d7_payers = 0
where install_date >= CURRENT_DATE - 7;

update processed.tab_marketing_kpi
set d30_new_installs = 0,
    d30_retained = 0,
    d30_revenue = 0,
    d30_payers = 0
where install_date >= CURRENT_DATE - 30;

update processed.tab_marketing_kpi
set d60_new_installs = 0,
    d60_retained = 0,
    d60_revenue = 0,
    d60_payers = 0
where install_date >= CURRENT_DATE - 60;

update processed.tab_marketing_kpi
set d90_new_installs = 0,
    d90_retained = 0,
    d90_revenue = 0,
    d90_payers = 0
where install_date >= CURRENT_DATE - 90;

update processed.tab_marketing_kpi
set d120_new_installs = 0,
    d120_retained = 0,
    d120_revenue = 0,
    d120_payers = 0
where install_date >= CURRENT_DATE - 120;

update processed.tab_marketing_kpi
set d150_new_installs = 0,
    d150_retained = 0,
    d150_revenue = 0,
    d150_payers = 0
where install_date >= CURRENT_DATE - 150;

-- install_source_map
create temp table install_source_map as
select distinct 'Marketing' as source, install_source, install_source as install_source_lower
from processed.tab_marketing_kpi
union
select distinct 'Cost' as source, install_source, install_source as install_source_lower
from raw_data.cost;

update install_source_map
set install_source_lower = replace(install_source_lower, '\+', '');

update install_source_map
set install_source_lower = replace(install_source_lower, '\-', '');

update install_source_map
set install_source_lower = replace(install_source_lower, '\(', '');

update install_source_map
set install_source_lower = replace(install_source_lower, '\)', '');

update install_source_map
set install_source_lower = replace(install_source_lower, ' ', '');

update install_source_map
set install_source_lower = lower(install_source_lower);

-- installs
create temp table installs as
select m.app, m.install_date, m.os, s.install_source_lower, sum(new_installs) as new_installs
from processed.tab_marketing_kpi m
    join install_source_map s on s.source = 'Marketing' and m.install_source = s.install_source
group by 1,2,3,4;

-- cost
create temp table cost as
select c.app_id as app, c.date as install_date, c.os, s.install_source_lower, sum(coalesce(amount, 0)) as cost
from raw_data.cost c
    join install_source_map s on s.source = 'Cost' and c.install_source = s.install_source
group by 1,2,3,4;

-- install_cost
create temp table install_cost as
select i.app, i.install_date, i.os, i.install_source_lower, m.install_source, i.new_installs, coalesce(c.cost, 0) as cost, ROUND(CAST(coalesce(c.cost, 0) AS DECIMAL(14,4)) / i.new_installs, 4) as cpi
from installs i
    join install_source_map m on m.source = 'Marketing' and i.install_source_lower = m.install_source_lower
    left join cost c on i.app = c.app and i.install_date = c.install_date and i.os = c.os and i.install_source_lower = c.install_source_lower;

update processed.tab_marketing_kpi
set cost = ic.cpi * processed.tab_marketing_kpi.new_installs
from install_cost ic
where processed.tab_marketing_kpi.app = ic.app and
    processed.tab_marketing_kpi.install_date = ic.install_date and
    processed.tab_marketing_kpi.os = ic.os and
    processed.tab_marketing_kpi.install_source = ic.install_source;



--------------------------------------------------------------------------------------------------------------------------------------------
--Poker session and payment
--Version 1.2
--Author yanyu
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading 
delete from  processed.fact_session
where  date_start >= (
                     select start_date 
                     from   processed.tmp_start_date
                );
                    
insert into processed.fact_session
(
        date_start
       ,user_key 
       ,uid 
       ,snsid
       ,install_ts
       ,install_date
	     ,install_source
       ,app 
       ,app_version 
       ,session_id 
       ,ts_start 
       ,ts_end 
       ,level_start 
       ,level_end 
       ,os 
       ,os_version 
       ,country_code 
       ,ip 
       ,language 
       ,device   
       ,browser 
       ,browser_version 
       ,rc_bal_start 
       ,rc_bal_end 
       ,coin_bal_start 
       ,coin_bal_end 
       ,ab_test 
       ,ab_variant 
       ,session_length 

)
select  trunc(ts) as date_start
       ,MD5(app||uid) as user_key 
       ,uid 
       ,snsid
       ,install_ts
       ,trunc(install_ts) AS install_date
	     ,install_source 
       ,app 
       ,null as app_version 
       ,null as session_id 
       ,ts as ts_start 
       ,ts as ts_end 
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_start 
       ,null as level_end 
       ,case when os = 'ios' then 'iOS'
             when os = 'android' then 'Android'
             when os='' and substring(json_extract_path_text(properties,'device'), 1, 2) = 'iP' then 'iOS'
             else 'Unknown'
         end as os
       ,os_version 
       ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
       ,ip 
       ,json_extract_path_text(properties,'lang') AS language 
       ,json_extract_path_text(properties,'device') AS device   
       ,browser 
       ,browser_version 
       ,null as rc_bal_start 
       ,null as rc_bal_end 
       ,null as coin_bal_start 
       ,null as coin_bal_end 
       ,null as ab_test
       ,null as ab_variant
       ,null as session_length 
from   public.events
where  event in ('session_start', 'newuser') 
and    trunc(ts) >= (
                     select start_date 
                     from   processed.tmp_start_date
                    );

----------------------------------------------------------------------------------------------------------------------------------------------
--processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

-- Insert data < 2014-09-01


-- insert into processed.fact_revenue
-- (
--          date
--         ,user_key
--         ,uid
--         ,snsid
--         ,app
--         ,ts
--         ,level
--         ,os
--         ,country_code
--         ,payment_processor
--         ,product_id
--         ,product_name
--         ,product_type
--         ,coins_in
--         ,rc_in
--         ,currency
--         ,amount
--         ,usd
--         ,transaction_id
--         ,properties
-- )
-- select  trunc(ts) as date
--         ,MD5(e.app||u.uid)
--         ,cast (u.uid as integer) as uid
--         ,e.uid as snsid
--         ,e.app
--         ,e.ts
--         ,CAST(
--                CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0'
--                     ELSE json_extract_path_text(properties,'level')
--                END AS integer
--              ) AS level
--         ,case when lower(right(os, 3)) = 'ios' then 'iOS'
--              when lower(right(os, 7)) = 'android' then 'Android'
--              when os='' and substring(json_extract_path_text(properties,'device'), 1, 2) = 'iP' then 'iOS'
--              else 'Unknown'
--          end as os
--         ,case when u.country_code='' or u.country_code is null or u.country_code=' ' then '--' else u.country_code end as country_code
--         ,'' AS payment_processor
--         ,json_extract_path_text(properties,'product_id') AS product_id
--         ,'' AS product_name
--         ,'' AS product_type
--         ,0 AS coins_in
--         ,0 AS rc_in
--         ,'' AS currency
--         ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS amount
--         ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS usd
--         ,null AS transaction_id
--         ,properties
-- from    public.events e
-- join    public.app_user u on u.snsid=e.uid
-- where   trunc(ts)<'2014-09-01' and name = 'payment';

--delete the historical data in case repeat loading 
delete from  processed.fact_revenue
where  date >= (
                     select start_date 
                     from   processed.tmp_start_date
                );

insert into processed.fact_revenue
(
         date              
        ,user_key          
        ,uid               
        ,snsid             
        ,app               
        ,ts  
        ,install_ts              
        ,level
        ,os
        ,country_code
        ,payment_processor 
        ,product_id        
        ,product_name      
        ,product_type      
        ,coins_in          
        ,rc_in             
        ,currency          
        ,amount
        ,usd
        ,transaction_id           
        ,properties       
)
select  trunc(ts) as date              
        ,MD5(app||uid)          
        ,uid               
        ,snsid             
        ,app               
        ,ts  
        ,install_ts              
        ,CAST(
               CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' 
                    ELSE json_extract_path_text(properties,'level') 
               END AS integer
             ) AS level
        ,case when lower(right(os, 3)) = 'ios' then 'iOS'
             when lower(right(os, 7)) = 'android' then 'Android'
             when os='' and substring(json_extract_path_text(properties,'device'), 1, 2) = 'iP' then 'iOS'
             else 'Unknown'
         end as os
        ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
        ,json_extract_path_text(properties,'payment_processor') AS payment_processor 
        ,json_extract_path_text(properties,'product_id') AS product_id        
        ,json_extract_path_text(properties,'product_name') AS product_name
        ,json_extract_path_text(properties,'product_type') AS product_type     
        ,CAST(
              case when json_extract_path_text(properties,'coins_in') = '' then '0'
                   else json_extract_path_text(properties,'coins_in')
              end
              AS INTEGER) AS coins_in
        ,CAST(
              case when json_extract_path_text(properties,'rc_in') = '' then '0'
                   else json_extract_path_text(properties,'rc_in')
              end
              AS INTEGER) AS rc_in
        ,json_extract_path_text(properties,'currency') AS currency
        ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS amount
        ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::REAL AS usd
        ,CAST(json_extract_path_text(properties,'transaction_id') AS VARCHAR) AS transaction_id
        ,properties        
from    public.events
where   event = 'payment' 
and     trunc(ts) >= (
                        select start_date 
                        from processed.tmp_start_date
                     );  
----------------------------------------------------------------------------------------------------------------------------------------------
--processed.fact_level_up
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_level_up
where  date_start >= (select start_date from processed.tmp_start_date);

insert into processed.fact_level_up
(
          user_key
          ,uid
          ,snsid
          ,app
          ,level
          ,ts_start
          ,date_start
          ,ts_end
          ,date_end
)
select  MD5(app||uid) as user_key
        ,uid
        ,snsid
        ,app
        ,cast(json_extract_path_text (properties,'level') AS integer) AS level
        ,ts
        ,trunc(ts) as date
        ,lead(ts) over (partition by MD5(app||uid) order by ts asc) as ts_end
        ,trunc(lead(ts) over (partition by user_key order by ts asc)) as date_end
from public.events
where trunc(ts) >= (select start_date from processed.tmp_start_date) and
      event='level_up' 
      and json_extract_path_text(properties,'from_level') ~ '^[0-9]+'
union
select  user_key
        ,uid
        ,snsid
        ,app
        ,level_start as level
        ,min(ts_start) as ts
        ,min(date_start) as date
        ,null
        ,null
from processed.fact_session
where date_start >= (select start_date from processed.tmp_start_date) and level_start=1
group by 1,2,3,4,5;

-- Update end level for rows inserted previously and level 1 fake level up

create temp table tmp_next_level as
select n.user_key,n.level,n.ts_start,n.date_start
from processed.fact_level_up l
join processed.fact_level_up n on n.level=l.level+1 and l.user_key=n.user_key
where l.ts_end is null and n.date_start >= (select start_date from processed.tmp_start_date);

update processed.fact_level_up
set ts_end=tmp_next_level.ts_start, date_end=tmp_next_level.date_start
from tmp_next_level
where fact_level_up.user_key=tmp_next_level.user_key and tmp_next_level.level=fact_level_up.level+1;
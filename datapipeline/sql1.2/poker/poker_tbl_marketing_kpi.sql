------------------------------------------------------------------------------------------------------------------------------------
--Poker marketing kpi
--version 1.2
/**
Discription:

**/
------------------------------------------------------------------------------------------------------------------------------------
  
---create new user table

create temp table tmp_new_user_info
as
select  user_key
       ,app
       ,install_date
       ,install_source
       ,subpublisher_source
       ,campaign_source
       ,country
       ,os
       ,is_payer
       ,conversion_ts
       ,total_revenue_usd
from   processed.dim_user;

create temp table tmp_d3_payers
as
select   user_key
        ,max(is_payer) as is_payer
from   processed.fact_dau_snapshot
where  datediff('day',install_date,date)<=3
group by 1;

create temp table tmp_d1_retention
as
select user_key
       ,1 as retained
from   processed.fact_dau_snapshot
where datediff('day',install_date,date)=1
;

create temp table tmp_d7_retention
as
select user_key
       ,1 as retained
from   processed.fact_dau_snapshot
where datediff('day',install_date,date)=7
;


create temp table tmp_d15_retention
as
select user_key
       ,1 as retained
from   processed.fact_dau_snapshot
where datediff('day',install_date,date)=15
;

create temp table tmp_d30_retention
as
select user_key
       ,1 as retained
from   processed.fact_dau_snapshot
where datediff('day',install_date,date)=30
;



delete from processed.tab_marketing_kpi
;




insert into processed.tab_marketing_kpi
(
     app               
    ,install_date      
    ,install_source    
    ,sub_publisher     
    ,campaign          
    ,country           
    ,os                
    ,new_installs      
    ,revenue           
    ,payers        
    ,d3_payers         
    ,d1_retained       
    ,d7_retained       
    ,d15_retained      
    ,d30_retained   
)
select      t1.app
           ,t1.install_date
           ,t1.install_source
           ,t1.subpublisher_source
           ,t1.campaign_source
           ,t1.country
           ,t1.os
           ,count(t1.user_key) as new_installs
           ,sum(t1.total_revenue_usd) as revenue
           ,sum(t1.is_payer) as payers
           ,sum(case when t2.is_payer is not null then t2.is_payer else 0 end) as d3_payers
           ,sum(case when t3.retained is not null then t3.retained else 0 end) as d1_retained
           ,sum(case when t4.retained is not null then t4.retained else 0 end) as d7_retained
           ,sum(case when t5.retained is not null then t5.retained else 0 end) as d15_retained
           ,sum(case when t6.retained is not null then t6.retained else 0 end) as d30_retained
from       tmp_new_user_info t1
left join  tmp_d3_payers t2
on t1.user_key = t2.user_key
left join  tmp_d1_retention t3
on t1.user_key=t3.user_key
left join  tmp_d7_retention t4
on t1.user_key=t4.user_key
left join  tmp_d15_retention t5
on t1.user_key=t5.user_key
left join  tmp_d30_retention t6
on t1.user_key=t6.user_key
group by 1,2,3,4,5,6,7
;

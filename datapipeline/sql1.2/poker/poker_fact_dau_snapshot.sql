--------------------------------------------------------------------------------------------------------------------------------------------
--Poker fact dau snapshot
--Version 1.2
--Author yanyu
/**
Discription:
This script is generate the dau snapshot tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.fact_dau_snapshot
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading 
delete from processed.fact_dau_snapshot where date >= (
                        select start_date 
                        from processed.tmp_start_date
                     ); 
                      
--------------create temp table to process latest 7 days data
CREATE temp TABLE tmp_dau_snapshot
(
  date DATE NOT NULL ENCODE DELTA,
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  install_ts TIMESTAMP ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  level_start integer,
  level_end integer,
  device   VARCHAR(64) ENCODE BYTEDICT,
  device_alias  VARCHAR(32) DEFAULT NULL,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(32) ENCODE BYTEDICT,
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT Default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT,
  language VARCHAR(50) ENCODE BYTEDICT,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT,
  ab_variant        VARCHAR(7) ENCODE BYTEDICT,
  is_new_user       SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  coins_in          INTEGER DEFAULT 0,
  rc_in             INTEGER DEFAULT 0,
  is_payer       SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  is_converted_today SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  revenue_usd        decimal(14,4) DEFAULT 0,
  purchase_cnt    integer default 0,
  session_cnt       INTEGER DEFAULT 1,
  playtime_sec integer
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);
truncate tmp_dau_snapshot;

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get daily users from raw_login and raw_payment
------------------------------------------------------------------------------------------------------------------------
insert into tmp_dau_snapshot (date, user_key, app, uid, snsid)
select distinct date_start, user_key, app, uid, ''
from processed.fact_session
where date_start >= (select start_date from processed.tmp_start_date)
union
select distinct date, user_key, app, uid, ''
from processed.fact_revenue
where date >= (select start_date from processed.tmp_start_date)
union
select distinct install_date, user_key, app, uid, ''
from processed.dim_user
where install_date >= (select start_date from processed.tmp_start_date);
;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get user's last login everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_daily_login as
select distinct
          date_start
         ,user_key 
         ,uid 
         ,snsid 
         ,first_value(install_ts ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as install_ts
         ,first_value(install_date ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as install_date
         ,first_value(app ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as app
         ,first_value(app_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as app_version
         ,first_value(level_start ignore nulls) OVER (partition by date_start, user_key order by ts_start asc
                                                  rows between unbounded preceding and unbounded following) as level_start
         ,first_value(level_end ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as level_end
         ,first_value(os ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as os
         ,first_value(os_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as os_version
         ,first_value(s.country_code ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as country_code
         ,first_value(c.country ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as country
         ,first_value(language ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as language
         ,first_value(device ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as device
         ,first_value(browser ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as browser
         ,first_value(browser_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as browser_version
         ,first_value(ab_test ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as ab_test
         ,first_value(ab_variant ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                 rows between unbounded preceding and unbounded following) as ab_variant
from processed.fact_session s
left join processed.dim_country c on c.country_code=s.country_code
where date_start >= (select start_date from processed.tmp_start_date);

------------------------------------------------------------------------------------------------------------------------
--Step 3. Get user's revenue
------------------------------------------------------------------------------------------------------------------------
create temp table temp_user_payment as
select user_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
from processed.fact_revenue
group by 1;

------------------------------------------------------------------------------------------------------------------------
--Step 4. Get user's session count everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_session_cnt as
select date_start, user_key, count(1) as session_cnt
from processed.fact_session
where date_start >= (select start_date from processed.tmp_start_date)
group by 1,2;

------------------------------------------------------------------------------------------------------------------------
--Step 5. Get user's payment everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_daily_payment as
select date
       ,user_key
       ,max(level) as level
       ,sum(coins_in) as coins_in
       ,sum(rc_in) as rc_in
       ,sum(usd) as revenue_usd
       ,count(1) as purchase_cnt
from processed.fact_revenue
where date >= (select start_date from processed.tmp_start_date)
group by 1,2;

------------------------------------------------------------------------------------------------------------------------
--Step 6. Get user's max level
------------------------------------------------------------------------------------------------------------------------

create temp table tmp_user_level as
select  d.date
       ,d.user_key
       ,max(l.level) as level_end
from tmp_dau_snapshot d
join processed.fact_level_up l on d.user_key=l.user_key
where d.date >= (select start_date from processed.tmp_start_date) and ((d.date between l.date_start and l.date_end) or (l.date_end is null and d.date>=l.date_start))
group by 1,2;

------------------------------------------------------------------------------------------------------------------------
--Step 7. Update user's info using last login info in each day
------------------------------------------------------------------------------------------------------------------------

update tmp_dau_snapshot
set install_date = t.install_date,
    install_ts = t.install_ts,
    level_end = t.level_end,
    level_start = t.level_start,
    device = t.device,
    country_code = t.country_code,
    country=t.country,
    language = t.language,
    browser = t.browser,
    browser_version = t.browser_version,
    os = t.os,
    os_version = t.os_version,
    ab_test=t.ab_test,
    ab_variant=t.ab_variant
from tmp_user_daily_login t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date_start;

------------------------------------------------------------------------------------------------------------------------
--Step 8. Update level_end
------------------------------------------------------------------------------------------------------------------------

update tmp_dau_snapshot
set level_end=l.level_end
from tmp_user_level l
where tmp_dau_snapshot.user_key = l.user_key and
      tmp_dau_snapshot.date = l.date;

update tmp_dau_snapshot
set level_end=level_start
where level_end is null;

------------------------------------------------------------------------------------------------------------------------
--Step 9. Update user is a payer or not and conversion
------------------------------------------------------------------------------------------------------------------------

update tmp_dau_snapshot
set is_payer = 1, is_converted_today=case when tmp_dau_snapshot.date=trunc(t.conversion_ts) then 1 else 0 end
from temp_user_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date >= trunc(t.conversion_ts);

------------------------------------------------------------------------------------------------------------------------
--Step 10. Update session count
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set session_cnt = t.session_cnt
from tmp_user_session_cnt t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date_start;

------------------------------------------------------------------------------------------------------------------------
--Step 11. Update payment metrics
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set revenue_usd = t.revenue_usd,
    purchase_cnt=t.purchase_cnt
from tmp_user_daily_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 12. Update is new install or not
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set is_new_user = 1
where date = install_date;

------------------------------------------------------------------------------------------------------------------------
--Step 13. Update missing country_code, os, level and install date for user coming from fact_revenue
------------------------------------------------------------------------------------------------------------------------

update tmp_dau_snapshot
set os = r.os
from processed.fact_revenue r
where tmp_dau_snapshot.os is null and tmp_dau_snapshot.user_key=r.user_key and tmp_dau_snapshot.date=r.date;

update tmp_dau_snapshot
set install_ts = r.install_ts,install_date = trunc(r.install_ts)
from processed.fact_revenue r
where tmp_dau_snapshot.install_ts is null and tmp_dau_snapshot.user_key=r.user_key and tmp_dau_snapshot.date=r.date;

update tmp_dau_snapshot
set country_code = r.country_code,country=c.country
from processed.fact_revenue r
left join processed.dim_country c on r.country_code=c.country_code
where tmp_dau_snapshot.country_code='--' and tmp_dau_snapshot.user_key=r.user_key and tmp_dau_snapshot.date=r.date;

update tmp_dau_snapshot
set level_start=r.level, level_end=r.level
from processed.fact_revenue r
where tmp_dau_snapshot.level_start is null and tmp_dau_snapshot.user_key=r.user_key and tmp_dau_snapshot.date=r.date;

create temp table tmp_user_prev_install as
select user_key,min(install_ts) as install_ts
from tmp_dau_snapshot
where user_key in (select distinct user_key from tmp_dau_snapshot where install_ts is null )
group by 1;

update tmp_dau_snapshot
set install_ts=p.install_ts,install_date=trunc(p.install_ts)
from tmp_user_prev_install p
where tmp_dau_snapshot.install_ts is null and p.user_key=tmp_dau_snapshot.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 14. Remove the abnormal date after fix.
-- Reason:
-- If there are still some missing values for key dimensions, we can delete them if count is very small.
-- This makes tableau report very clean
-- We should be very careful to do this. We should check the data before deleting them.
------------------------------------------------------------------------------------------------------------------------
delete from tmp_dau_snapshot where install_date is null;

------------------------------------------------------------------------------------------------------------------------
--Step 15. Insert updated data into Big table
------------------------------------------------------------------------------------------------------------------------

insert into processed.fact_dau_snapshot
select *
from tmp_dau_snapshot;
      
      

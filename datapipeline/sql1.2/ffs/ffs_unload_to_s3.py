#!/usr/bin/env python

import os, sys
import re
from datetime import datetime
from datetime import timedelta

start_date_str = '2014-08-29'

events_table = 'public.events_raw'

events_list = [
    'FishingRecord',
    'login',
    'DailyBonus',
    'RC Balloon',
    'OnlinePackage',
    'UseOP',
    'WaterWell',
    'Fishingbook',
    'Giftbox',
    'FacebookUserInfo',
    'SellProduct',
    'BatchProductionUpgrade',
    'OrderReward',
    'VisitNeighbors',
    'BatchProduction',
    'UseGasoline',
    'luckypackage',
    'Kitchen',
    'garden_level',
    'Collect_Machine_Trade',
    'SeafoodhouseProcessReward',
    'Achievement',
    'FortuneWheel',
    'barnView',
    'item_transaction',
    'rcspend',
    'Mystery_Store_Trade',
    'Animal',
    'ConnectFacebook',
    'Calendar',
    'welcomebackReward',
    'WareHouse',
    'coins_transaction',
    'LevelReward',
    'UseFarmAids',
    'ItemDrop',
    'newuser',
    'BeautyshopProcessReward',
    'SeafoodhouseProcessRecord',
    'FishingPurchase',
    'Session_end',
    'rc_transaction',
    'ItemPurchased',
    'ItemUpgrade',
    'CollectableDecoration',
    'QuestComplete',
    'Getfreerc',
    'FishingStart',
    'MysteriousBox',
    'BeautyshopProcessRecord',
    'payment']

s3_prefix = 's3://com.funplusgame.bidata/events_raw/ffs/'
aws_access_key_id = 'AKIAJRKDNC52OAINDWKA'
aws_secret_access_key = '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'
delimiter = '^'

unload_template = "unload ('select * from table_name') to 's3://com.funplus.bitest/rs/events_raw/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' DELIMITER '^';"
copy_template = "copy table_name from 's3://com.funplus.bitest/tlw/move_database/table_directory/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' DELIMITER '^';"

def gen_unload_cmd_list(unload_cmd_file):
    unload_cmd_fp = open(unload_cmd_file, 'w')
    start_date = datetime.strptime(start_date_str, '%Y-%m-%d').date()
    current_date = datetime.utcnow().date()
    process_date = start_date
    while process_date < current_date:
        process_date_str1 = process_date.strftime('%Y-%m-%d')
        process_date_str2 = process_date.strftime('%Y/%m/%d')
        for event in events_list:
            unload_cmd = "unload ('select * from " + events_table + " where trunc(ts) = \\'" + process_date_str1 + "\\' and event = " + "\\'" + event + "\\';')" \
                    + " to '" + s3_prefix + event + "/" + process_date_str2 + "/'" \
                    + " CREDENTIALS" + " 'aws_access_key_id=" + aws_access_key_id \
                    + ";aws_secret_access_key=" + aws_secret_access_key + "' " \
                    + "DELIMITER" + " " + "'" + delimiter + "'" + ";" + "\n"
            # print process_date_str
            print unload_cmd
            unload_cmd_fp.write(unload_cmd)
        # del_cmd = "delete from public.events_raw where trunc(ts) = '" + process_date_str1 + "';\n"
        # print del_cmd
        # unload_cmd_fp.write(del_cmd)
        process_date = process_date + timedelta(days=1)
    unload_cmd_fp.close()


if __name__ == '__main__':
    unload_cmd = sys.argv[1]
    # copy_cmd = sys.argv[2]
    gen_unload_cmd_list(unload_cmd)
    # gen_copy_cmd_list(copy_cmd)

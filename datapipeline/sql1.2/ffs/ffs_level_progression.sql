drop table if exists temp_conversion_level;
create temp table temp_conversion_level as
with this_date as (select max(date(install_ts)) as date from processed.dim_user)
select user_key, min(level) as conversion_level
from processed.fact_revenue r
join this_date t on 1=1
where r.date<t.date
group by 1;

drop table if exists processed.tab_leveling_speed;
CREATE TABLE processed.tab_leveling_speed
(
  install_date              DATE ENCODE DELTA,
  level                     integer,
  current_level             integer,
  is_level_end              smallint default 0,
  app                       VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  is_payer                  SMALLINT ENCODE BYTEDICT,
  conversion_level_range    SMALLINT ENCODE BYTEDICT,
  active_day                integer,
  days_to_reach             integer default null,
  user_cnt                  INTEGER DEFAULT 0
)
DISTKEY(install_date)
SORTKEY(install_date,level,current_level,is_payer,conversion_level_range, app, os, country, install_source);

insert into processed.tab_leveling_speed
with
    this_date as (select max(date(install_ts)) as date from processed.dim_user),
    active_days as
        (
            select user_key,date,level_end,row_number() over (partition by user_key order by date asc) as active_day
            from processed.fact_dau_snapshot
        )
select
      u.install_date,
      lu.level,
      u.level as current_level,
      case when lu.level=level_end then 1 else 0 end as is_level_end,
      u.app,
      u.os,
      u.country,
      u.install_source,
      u.is_payer,
      ceiling((c.conversion_level / 5::real))*5 as conversion_level_range,
      active_day,
      days_since_install_cnt,
      count(distinct lu.user_key)
from processed.fact_level_up lu
join processed.dim_user u on lu.user_key=u.user_key
join active_days ad on ad.user_key=u.user_key and ad.date=lu.date_start
left join temp_conversion_level c on c.user_key=u.user_key
join this_date t on 1=1
where install_date>='2014-09-01' and install_date<t.date and lu.level<=u.level
group by 1,2,3,4,5,6,7,8,9,10,11,12;


CREATE TABLE if not exists processed.tab_level_dropoff

(
  install_date              DATE ENCODE DELTA,
  level                     integer,
  current_level             integer,
  app                       VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  is_payer                  SMALLINT ENCODE BYTEDICT,
  is_churned_3days          smallint default 0,
  is_churned_7days          smallint default 0,
  is_churned_14days         smallint default 0,
  is_churned_21days         smallint default 0,
  is_churned_30days         smallint default 0,
  user_cnt                  INTEGER DEFAULT 0
)
DISTKEY(install_date)
SORTKEY(install_date,level,current_level, app, os, country, install_source);

truncate processed.tab_level_dropoff;

insert into processed.tab_level_dropoff
with this_date as (select max(install_date) as date from processed.dim_user)
select
  u.install_date,
  l.level,
  u.level as current_level,
  u.app,
  u.os,
  u.country,
  u.install_source,
  u.is_payer,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days,
  count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join this_date t on 1=1
where install_date>='2014-09-01' and install_date<t.date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

drop table if exists processed.tab_level_active_days;
create table processed.tab_level_active_days as
with this_date as (select max(install_date) as date from processed.dim_user)
select
  u.install_date,
  du.level_end,
  u.app,
  u.os,
  u.country,
  u.install_source,
  u.is_payer,
  count(du.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join (
    select user_key,level_end, row_number() over (partition by user_key order by date asc) as active_day
    from processed.fact_dau_snapshot
) du on du.user_key=u.user_key
join this_date t on 1=1
where install_date>='2014-09-01' and install_date<t.date
group by 1,2,3,4,5,6,7;



--ffs.processed.raw_crossbreed
--raw data for crossbreed events
--delete data within the last 21 days and reinsert
delete from ffs.processed.raw_crossbreed
where  ts >= (select start_date from processed.tmp_start_date);

INSERT INTO ffs.processed.raw_crossbreed
SELECT ts,
       app,
       snsid,
       extract(epoch FROM ts) AS ts_epoch,
       nullif(json_extract_path_text (properties,'garden_unlock_ts'),'')::int unlock_epoch,
       (TIMESTAMP 'epoch' + nullif(json_extract_path_text (properties,'garden_unlock_ts'),'')::int * INTERVAL '1 Second ') unlock_ts,
       json_extract_path_text (properties,'in_flower_1') in_flower_1,
       nullif(json_extract_path_text (properties,'in_flower_1_id'),'')::int in_flower_1_id,
       json_extract_path_text (properties,'in_flower_2') in_flower_2,
       nullif(json_extract_path_text (properties,'in_flower_2_id'),'')::int in_flower_2_id,
       json_extract_path_text (properties,'item_name') item_name,
       nullif(json_extract_path_text (properties,'item_id'),'')::int item_id,
       nullif(json_extract_path_text (properties,'item_level'),'')::int item_level,
       nullif(json_extract_path_text (properties,'neighbor_plant'),'')::int neighbor,
       nullif(json_extract_path_text (properties,'garden_level'),'')::int garden_level
FROM ffs.public.events_raw
WHERE event='item_transaction'
  AND ts>=
    (SELECT start_date
     FROM processed.tmp_start_date)
  AND json_extract_path_text (properties,'in_flower_1')!=''
  AND json_extract_path_text (properties,'garden_unlock_ts')!='';

--processed.fact_garden_level_dist
--garden level distribution by date
--delete data within the last 21 days and reinsert
delete from  processed.fact_garden_level_dist
where  date >= (select start_date from processed.tmp_start_date);

INSERT INTO processed.fact_garden_level_dist
SELECT date, 
	   garden_level,
       count(1) AS COUNT
FROM
  (SELECT user_key, date, max(garden_level) garden_level
   FROM
     (SELECT json_extract_path_text (properties,'garden_level')::int AS garden_level,
             trunc(ts) AS date,
             md5(app||uid) AS user_key
      FROM ffs.public.events_raw
      WHERE event='garden_level'
        AND ts>=
          (SELECT start_date
           FROM processed.tmp_start_date)
        AND json_extract_path_text (properties,'garden_level')!='')
   GROUP BY user_key, date)
GROUP BY date, garden_level
ORDER BY date, garden_level;

--ffs.processed.fact_crossbreed_count
--delete data within the last 21 days and reinsert
--count the number of level 1/2/3 flowers by date and garden level
delete from ffs.processed.fact_crossbreed_count
where  date >= (select start_date from processed.tmp_start_date);

INSERT INTO ffs.processed.fact_crossbreed_count
SELECT trunc(ts) AS date,
       garden_level,
       item_level,
       neighbor,
       count(1) AS COUNT
FROM ffs.processed.raw_crossbreed
WHERE ts>=
    (SELECT start_date
     FROM processed.tmp_start_date)
GROUP BY 1,
         garden_level,
         item_level,
         neighbor;

--ffs.processed.fact_first_flower
--summary of how many days it takes for players to get their first 2/3/neighbor flower
DROP TABLE IF EXISTS ffs.processed.fact_first_flower;

CREATE TABLE ffs.processed.fact_first_flower AS
SELECT item_level,
       elapsed,
       count(1) AS COUNT
FROM
  (SELECT app,
          snsid,
          item_level,
          min(elapsed) elapsed
   FROM
     (SELECT app,
             snsid,
             (ts_epoch - unlock_epoch)/(3600*24) elapsed,
             item_level,
             neighbor
      FROM ffs.processed.raw_crossbreed
      WHERE unlock_ts>='2015-05-10'
        AND item_level>1)
   GROUP BY app,
            snsid,
            item_level)
GROUP BY item_level,
         elapsed
UNION
SELECT 99 item_level,
       elapsed,
       COUNT(1) AS COUNT
FROM
  (SELECT app,
          snsid,
          min(elapsed) elapsed
   FROM
     (SELECT app,
             snsid,
             (ts_epoch - unlock_epoch)/(3600*24) elapsed,
             item_level,
             neighbor
      FROM ffs.processed.raw_crossbreed
      WHERE unlock_ts>='2015-05-10'
        AND neighbor=1)
   GROUP BY app,
            snsid)
GROUP BY elapsed;


--aggregate rc_in/rc_out report
delete from processed.agg_rc_transaction                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );  
              
insert into processed.agg_rc_transaction
select trunc(ts) as date
, app
, level
, action
, location
, flow
, subtype
, field1 
, sum(rc) as rc
, sum(quantity) as qty
, count(1) as count
, count(distinct user_key) as users
from
(select ts, user_key, app, level, nvl(action1, action2) as action, location,
  nvl(nvl(rc_in, rc_out),0) as rc,
    case when rc_in>0 then 1 when rc_out>0 then -1 else 0 end flow,
--subtype indicates item/achievement/quest/gallery - supplements action to know what field1 and field2 refer to
case
  when item!='' then 'item'
  when achievement_id!='' then 'achievement'
  when quest_id!='' then 'quest'
  when gallery_id!='' then 'gallery'
  else NULL end subtype,
--field1 indicates item_name/quest_id 
case
  when item!='' then json_extract_path_text(item, 'item_name')
  when achievement_id!='' then achievement_id
  when quest_id!='' then quest_id
  when gallery_id!='' then gallery_id
  else NULL end field1,
--field2 provides additional identifiers (e.g. task_id) if necessary
case
  when achievement_id!='' then nullif(json_extract_path_text(properties, 'achievement_star_id'),'')::int
  when quest_id!='' then nullif(json_extract_path_text(properties, 'task_id'),'')::int
  else NULL end field2,
--single out quantity of items if possible
case
  when item!='' then nullif(json_extract_path_text(item, 'quantity'),'')::int
  else NULL end quantity
from
(select ts, properties, md5(app||uid) as user_key, app,
          nullif(json_extract_path_text (properties,'real_action'),'') as action1,
                    nullif(json_extract_path_text (properties,'action'),'') as action2,
          json_extract_array_element_text(json_extract_path_text (properties,'item_change'),0) as item,
          json_extract_path_text (properties,'achievement_id') as achievement_id,
          json_extract_path_text (properties,'quest_id') as quest_id,
          json_extract_path_text (properties,'gallery_id') as gallery_id,
          json_extract_path_text (properties,'location') as location,
      json_extract_path_text (properties,'level')::int as level,
                    nullif(json_extract_path_text (properties,'rc_out'),'')::bigint as rc_out,
                    nullif(json_extract_path_text (properties,'rc_in'),'')::bigint as rc_in
from public.events_raw
             where event='rc_transaction'
             and ts>=(select start_date from processed.tmp_start_date)
       and snsid not in (select snsid from processed.cheaters)
       and snsid not in (select snsid from processed.cheaters_new)
       )
       )
       group by 1,2,3,4,5,6,7,8;              

--resource inventory distribution
--resource inventory distribution
delete from processed.ffs_resource_dist                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );


create temp table g as
select a.*, u.conversion_ts from
(select trunc(ts) as date, md5(app||uid) as user_key, 
nullif(json_extract_path_text(properties, 'level'), '')::int as level,
properties
FROM ffs.public.events_raw
where event='login' and trunc(ts)>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
and app='ffs.global.prod') a
join
(select user_key, conversion_ts from processed.dim_user
where user_key not in (select user_key from processed.cheaters_new)) u
on a.user_key = u.user_key;


insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'fertilizer'), '25'), '')::bigint, 0) as qty, 'fertilizer_25' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'fertilizer'), '100'), '')::bigint, 0) as qty, 'fertilizer_100' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'kettle'), '25'), '')::bigint, 0) as qty, 'kettle_25' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'kettle'), '100'), '')::bigint, 0) as qty, 'kettle_100' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1001'), '')::bigint, 0) as qty, 'ticket_1001' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1002'), '')::bigint, 0) as qty, 'ticket_1002' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(properties, 'neighbors_num'), '')::bigint, 0) as qty, 'neighbors_num' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(properties, 'power'), '')::bigint, 0) as qty, 'power' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(properties, 'op'), '')::bigint, 0) as qty, 'op' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(properties, 'gas'), '')::bigint, 0) as qty, 'gas' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(properties, 'rc_left'), '')::bigint, 0) as qty, 'rc_left' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;

insert into processed.ffs_resource_dist
select date, (power(2,bin)-1)*50 as bin, resource, level, is_payer, sum(qty) total_qty, count(1) from
(select date, resource, qty, level, floor(log(qty/50+1)/log(2)) as bin,
    case
        when conversion_ts<date then 1
        else 0 end is_payer 
    from
(select *, row_number() over (partition by date, user_key order by qty desc) as rnum from 
(select date, user_key, conversion_ts, level, nullif(nullif(json_extract_path_text(properties, 'coins_left'), '')::bigint, 0) as qty, 'coins_left' as resource
from g))
where rnum =1 and qty>0)
group by 1,2,3,4,5;
--first pay and spend activities
truncate table processed.global_first_pay_spend;
insert into processed.global_first_pay_spend
select a.*, u.install_ts, u.country, u.os, u.total_revenue_usd from
(select user_key, ts, snsid, level, product_id, product_name, product_type, rc_in, amount as first_pay_usd, diff as diff_mins, rc_out, rc_bal,
case
  when item!='' then 'item'
  when achievement_id!='' then 'achievement'
  when quest_id!='' then 'quest'
  when gallery_id!='' then 'gallery'
  else nvl(action1, action2) end subtype,
--field1 indicates item_name/quest_id 
case
  when item!='' then json_extract_path_text(item, 'item_name')
  when achievement_id!='' then achievement_id
  when quest_id!='' then quest_id
  when gallery_id!='' then gallery_id
  else NULL end field1,
--field2 provides additional identifiers (e.g. task_id) if necessary
case
  when achievement_id!='' then nullif(json_extract_path_text(properties, 'achievement_star_id'),'')::int
  when quest_id!='' then nullif(json_extract_path_text(properties, 'task_id'),'')::int
  when item!='' then nullif(json_extract_path_text(item, 'item_id'),'')::int
  else NULL end field2,
--single out quantity of items if possible
case
  when item!='' then nullif(json_extract_path_text(item, 'quantity'),'')::int
  else NULL end quantity, properties
from
(select *, nullif(json_extract_path_text (properties,'real_action'),'') as action1,
                    nullif(json_extract_path_text (properties,'action'),'') as action2,
          json_extract_array_element_text(json_extract_path_text (properties,'item_change'),0) as item,
          json_extract_path_text (properties,'achievement_id') as achievement_id,
          json_extract_path_text (properties,'quest_id') as quest_id,
          json_extract_path_text (properties,'gallery_id') as gallery_id,
          json_extract_path_text (properties,'location') as location,
                    nullif(json_extract_path_text (properties,'rc_out'),'')::int as rc_out,
          nullif(json_extract_path_text (properties,'rc_bal'),'')::int as rc_bal from
(select a.*, datediff('minutes', a.ts, b.ts) diff, b.properties,
row_number() over (partition by a.user_key order by b.ts) as rnum
from 
(select user_key, ts, snsid, level, product_id, product_name, product_type, rc_in, amount from
(SELECT *, row_number() over (partition by user_key order by ts) as rnum FROM ffs.processed.fact_revenue 
where amount>0 and app='ffs.global.prod')
where rnum=1 and ts>='2015-06-01') a
left join
(SELECT *, md5(app||uid) as user_key 
FROM ffs.public.events_raw where event='rc_transaction'
and app='ffs.global.prod'
and nullif(json_extract_path_text (properties,'rc_out'),'')::int>0) b
on a.user_key = b.user_key
and a.ts<=b.ts) c
where rnum=1
)
) a
left join
processed.dim_user u
on a.user_key = u.user_key;

--retargeting ffs 
drop table if exists processed.retarget_date_0608_level_a10;
create table processed.retarget_date_0608_level_a10 as
select  d.user_key
      ,min(d.date) as retarget_date
      ,max(r.level) as level
      ,max(r.is_payer) as is_payer
from processed.fact_dau_snapshot d
    join (select distinct user_key, level, is_payer from processed.re_target_campaign_d7_inactive_level_a10_0608) r on d.user_key = r.user_key
where date >= '2016-06-08'
group by 1;

drop table if exists processed.agg_kpi_retarget_0608_level_a10;
create table processed.agg_kpi_retarget_0608_level_a10 as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,rd.is_payer
      ,d.ab_test
      ,rd.level as level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0608_level_a10 rd on d.user_key = rd.user_key
where date >= '2016-06-08'
group by 1,2,3,4,5,6,7,8,9,10;

drop table if exists processed.retarget_date_0909_payer;
create table processed.retarget_date_0909_payer as
select  d.user_key
      ,min(d.date) as retarget_date
      ,max(r.level) as level
      ,max(r.is_payer) as is_payer
from processed.fact_dau_snapshot d
    join (select distinct user_key, level, is_payer from processed.re_target_campaign_d7_inactive_payer_0909) r on d.user_key = r.user_key
where date >= '2016-09-09'
group by 1;

drop table if exists processed.agg_kpi_retarget_0909_payer;
create table processed.agg_kpi_retarget_0909_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,rd.is_payer
      ,d.ab_test
      ,rd.level as level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0909_payer rd on d.user_key = rd.user_key
where date >= '2016-09-09'
group by 1,2,3,4,5,6,7,8,9,10;

drop table if exists processed.retarget_date_0927_level_a14;
create table processed.retarget_date_0927_level_a14 as
select  d.user_key
      ,min(d.date) as retarget_date
      ,max(r.level) as level
      ,max(r.is_payer) as is_payer
from processed.fact_dau_snapshot d
    join (select distinct user_key, level, is_payer from processed.re_target_campaign_d5_inactive_level_a14_0927) r on d.user_key = r.user_key
where date >= '2016-09-27'
group by 1;

drop table if exists processed.agg_kpi_retarget_0927_level_a14;
create table processed.agg_kpi_retarget_0927_level_a14 as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,rd.is_payer
      ,d.ab_test
      ,rd.level as level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_0927_level_a14 rd on d.user_key = rd.user_key
where date >= '2016-09-27'
group by 1,2,3,4,5,6,7,8,9,10;
--1118
drop table if exists processed.retarget_date_1118_payer;
create table processed.retarget_date_1118_payer as
select  d.user_key
      ,min(d.date) as retarget_date
      ,max(r.level) as level
      ,max(r.is_payer) as is_payer
from processed.fact_dau_snapshot d
    join (select distinct user_key, level, is_payer from processed.re_target_campaign_d7_inactive_payer_1118) r on d.user_key = r.user_key
where date >= '2016-11-18'
group by 1;

drop table if exists processed.agg_kpi_retarget_1118_payer;
create table processed.agg_kpi_retarget_1118_payer as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,rd.is_payer
      ,d.ab_test
      ,rd.level as level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_date_1118_payer rd on d.user_key = rd.user_key
where date >= '2016-11-18'
group by 1,2,3,4,5,6,7,8,9,10;


---

drop table if exists processed.retarget_version_3x_20161121_a;
create table processed.retarget_version_3x_20161121_a as
select  d.user_key
      ,min(d.date) as retarget_date
      ,max(r.level) as level
      ,max(r.is_payer) as is_payer
from processed.fact_dau_snapshot d
    join (select distinct user_key, level, is_payer from processed.re_target_campaign_version_3x_20161121_a) r on d.user_key = r.user_key
where date >= '2016-11-21'
group by 1;

drop table if exists processed.agg_kpi_retarget_version_3x_20161121_a;
create table processed.agg_kpi_retarget_version_3x_20161121_a as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,rd.is_payer
      ,d.ab_test
      ,rd.level as level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_version_3x_20161121_a rd on d.user_key = rd.user_key
where date >= '2016-11-21'
group by 1,2,3,4,5,6,7,8,9,10;

drop table if exists processed.retarget_version_3x_20161121_b;
create table processed.retarget_version_3x_20161121_b as
select  d.user_key
      ,min(d.date) as retarget_date
      ,max(r.level) as level
      ,max(r.is_payer) as is_payer
from processed.fact_dau_snapshot d
    join (select distinct user_key, level, is_payer from processed.re_target_campaign_version_3x_20161121_b) r on d.user_key = r.user_key
where date >= '2016-11-21'
group by 1;

drop table if exists processed.agg_kpi_retarget_version_3x_20161121_b;
create table processed.agg_kpi_retarget_version_3x_20161121_b as
select  date
      ,d.app
      ,d.app_version
      ,u.install_source
      ,d.country
      ,d.os
      ,d.language
      ,rd.is_payer
      ,d.ab_test
      ,rd.level as level
      ,sum(case when d.date = rd.retarget_date then 1 else 0 end) as new_installs
      ,count(d.user_key) as dau
      ,sum(d.is_converted_today) as new_payers
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
      ,sum(0) as revenue_iap
      ,sum(0) as revenue_3rd
      ,sum(d.session_cnt) as session_cnt
from processed.fact_dau_snapshot d
    join processed.dim_user u on d.user_key = u.user_key
    join processed.retarget_version_3x_20161121_b rd on d.user_key = rd.user_key
where date >= '2016-11-21'
group by 1,2,3,4,5,6,7,8,9,10;
--retarget
drop table if exists processed.agg_kpi_retarget_payer;
create table processed.agg_kpi_retarget_payer as
select '2016-06-08_a10' as retarget_date, *
from processed.agg_kpi_retarget_0608_level_a10
union all
select '2016-09-09' as retarget_date, *
from processed.agg_kpi_retarget_0909_payer
union all
select '2016-09-27' as retarget_date, *
from processed.agg_kpi_retarget_0927_level_a14
union all
select '2016-11-18' as retarget_date, *
from processed.agg_kpi_retarget_1118_payer
union all
select '2016-11-21_a' as retarget_date, *
from processed.agg_kpi_retarget_version_3x_20161121_a
union all
select '2016-11-21_b' as retarget_date, *
from processed.agg_kpi_retarget_version_3x_20161121_b
;
--------------------------------------------------------------------------------------------------------------------------------------------
--FFS session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_session
where  date_start >= (
                     select start_date
                     from ffs.processed.tmp_start_date
                );

create temp table tmp_fact_session as
select *
from   public.events_raw
where  event in ('login', 'newuser', 'session_start')
and    ts >= (
                     select start_date
                     from ffs.processed.tmp_start_date
                    );

--temp table for session end events, only take last event in case of duplicate session_ids
create temp table tmp_session_end as
select * from
(select *, row_number() over (partition by session_id order by ts desc) as rnum
from
  (select distinct app, uid, json_extract_path_text (properties,'session_id') session_id,
  json_extract_path_text (properties,'duration')::int duration, ts
  from   public.events_raw
  where  event='Session_end'
  and    ts >= (
                       select start_date
                       from ffs.processed.tmp_start_date
                      )
  and json_extract_path_text (properties,'session_id')!=''
  )
)
where rnum=1;

update tmp_fact_session
set properties = regexp_replace(properties, '\"device\"\:\"[^\"]*\"[^\"]*\"\\\}','\"device\"\:\"Unknown\"\\\}')
where regexp_substr(properties,'\"device\"\:\"[^\"]*\"[^\"]*\"\\\}') != '';

update tmp_fact_session
set properties = regexp_replace(properties, '\"device\"\:\"custom\\\\ version\"','\"device\"\:\"custom version\"')
where regexp_substr(properties,'\"device\"\:\"custom\\\\ version\"') != '';

insert into processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,snsid
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,browser
       ,browser_version
       ,rc_bal_start
       ,rc_bal_end
       ,coin_bal_start
       ,coin_bal_end
       ,ab_test
       ,ab_variant
       ,session_length
)
select  trunc(ts) as date_start
       ,MD5(app||uid) as user_key
       ,app
       ,uid
       ,snsid
       ,case
            when install_ts is null then CAST(json_extract_path_text (properties,'addtime') AS timestamp)
            else install_ts
        end as install_ts
       ,trunc(
        case
            when install_ts is null then CAST(json_extract_path_text (properties,'addtime') AS timestamp)
            else install_ts
        end
        ) AS install_date
	     ,install_source
       ,null as app_version
       ,substring(json_extract_path_text (properties,'session_id'),1,64) as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,nvl(nullif(json_extract_path_text (properties,'level'),'')::int, 0) as level_start
       ,CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level_end
       ,os::varchar(30)
       ,os_version
       ,case when country_code='' or country_code is null or country_code=' ' then '--' else country_code end as country_code
       ,ip::varchar(20)
       ,json_extract_path_text(properties,'lang')::varchar(20) AS language
       ,substring(json_extract_path_text(properties,'device'), 1, 32) AS device
       ,browser
       ,browser_version
       ,null as rc_bal_start
       ,null as rc_bal_end
       ,null as coin_bal_start
       ,null as coin_bal_end
       ,null as ab_test
       ,null as ab_variant
       ,null as session_length
from   tmp_fact_session;

--update fact_session ts_end and session_length from tmp_session_end
update processed.fact_session
set ts_end = s.ts, 
session_length = s.duration
from tmp_session_end s 
where processed.fact_session.session_id = s.session_id 
and processed.fact_session.app = s.app 
and processed.fact_session.uid = s.uid;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_revenue
where  date >= (
                     select start_date
                     from   ffs.processed.tmp_start_date
                );

create temp table tmp_fact_revenue as
select *
from    public.events_raw
where   event = 'payment'
and     ts >= (
                        select start_date
                        from ffs.processed.tmp_start_date
                     );

delete from tmp_fact_revenue
  where app = 'ffs.global.prod'
  and snsid in (select distinct user_id
                    from processed.payment_test_users
                    where app_id = 'ffs.global.prod'
                  )
;

update tmp_fact_revenue
set properties = regexp_replace(properties, '\"device\"\:\"[^\"]*\"[^\"]*\"\\\}','\"device\"\:\"Unknown\"\\\}')
where regexp_substr(properties,'\"device\"\:\"[^\"]*\"[^\"]*\"\\\}') != '';

update tmp_fact_revenue
set properties = regexp_replace(properties, '\"device\"\:\"custom\\\\ version\"','\"device\"\:\"custom version\"')
where regexp_substr(properties,'\"device\"\:\"custom\\\\ version\"') != '';

insert into processed.fact_revenue
(
         date
        ,user_key
        ,app
        ,uid
        ,snsid
        ,ts
        ,level
        ,payment_processor
        ,product_id
        ,product_name
        ,product_type
        ,coins_in
        ,rc_in
        ,currency
        ,amount
        ,usd
        ,transaction_id
)
select  trunc(ts) as date
        ,MD5(app||uid)
        ,app
        ,uid
        ,snsid
        ,ts
        ,CAST(
               CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'level')
               END AS integer
             ) AS level
        ,json_extract_path_text(properties,'payment_processor') AS payment_processor
        ,json_extract_path_text(properties,'product_id') AS product_id
        ,json_extract_path_text(properties,'product_name') AS product_name
        ,json_extract_path_text(properties,'product_type') AS product_type
        ,CAST(json_extract_path_text(properties,'coins_in') AS INTEGER) AS coins_in
        ,CAST(json_extract_path_text(properties,'rc_in') AS INTEGER) AS rc_in
        ,json_extract_path_text(properties,'currency') AS currency
        ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::DECIMAL AS amount
        ,CAST(json_extract_path_text(properties,'amount') AS INTEGER)/100::DECIMAL AS usd
        ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    tmp_fact_revenue;

update processed.fact_revenue
set amount = 17.99
where app = 'ffs.cn.prod' and currency = 'APT' and amount = 118;

update processed.fact_revenue
set amount = 99.99
where app = 'ffs.cn.prod' and currency = 'APT' and amount = 648;

update processed.fact_revenue
set currency = 'CNY'
where currency = 'RMB';

update processed.fact_revenue
set currency = 'USD'
where currency = 'APT';

update processed.fact_revenue
set usd = amount * case when c.factor is null then 1 else c.factor end
from public.currency c
where processed.fact_revenue.currency = c.currency and processed.fact_revenue.date = c.dt;

--delete duplicate records if app is China-version and currency is USD
delete from ffs.processed.fact_revenue
where app='ffs.cn.prod' 
and currency='USD'
and transaction_id in
  (SELECT transaction_id
    FROM ffs.processed.fact_revenue
   GROUP BY transaction_id
  HAVING COUNT(1) > 1);

--delete duplicate records that agree on everything except possibly ts
CREATE TEMP TABLE delete_dupe_rows AS
SELECT date
      ,user_key
      ,app
      ,uid
      ,snsid
      ,min(ts) as ts
      ,level
      ,payment_processor
      ,product_id
      ,product_name
      ,product_type
      ,coins_in
      ,rc_in
      ,currency
      ,amount
      ,usd
      ,transaction_id
FROM ffs.processed.fact_revenue
GROUP BY date
      ,user_key
      ,app
      ,uid
      ,snsid
      ,level
      ,payment_processor
      ,product_id
      ,product_name
      ,product_type
      ,coins_in
      ,rc_in
      ,currency
      ,amount
      ,usd
      ,transaction_id
HAVING COUNT(1) > 1;

delete from ffs.processed.fact_revenue
where ffs.processed.fact_revenue.transaction_id 
  in (select transaction_id from delete_dupe_rows);

insert into processed.fact_revenue
select *
from delete_dupe_rows;

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_level_up
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  processed.fact_level_up
where  date_start >= (select start_date from processed.tmp_start_date);

insert into processed.fact_level_up
(
          user_key
          ,uid
          ,snsid
          ,app
          ,level
          ,ts_start
          ,date_start
          ,ts_end
          ,date_end
          ,days_since_install_cnt
)
select  e.*
        ,lead(e.ts) over (partition by e.user_key order by e.level asc) as ts_end
        ,trunc(lead(e.ts) over (partition by e.user_key order by e.level asc)) as date_end
        ,datediff(day, u.install_date,e.date) as days_since_install_cnt
from
(
    select  MD5(app||uid) as user_key
            ,uid
            ,snsid
            ,app
            ,SUBSTRING(REGEXP_SUBSTR(properties, '"upgrade_level":[0-9]+'),17)::integer AS level
            ,min(ts) as ts
            ,min(trunc(ts)) as date
    from public.events_raw
    where event='LevelReward' and trunc(ts) >= (select start_date from processed.tmp_start_date)
group by 1,2,3,4,5
) e
join processed.dim_user u on u.user_key=e.user_key
union
select  user_key
        ,uid
        ,snsid
        ,app
        ,1 as level
        ,min(ts_start) as ts
        ,min(date_start) as date
        ,null
        ,null
        ,0
from processed.fact_session
where date_start >= (select start_date from processed.tmp_start_date) and install_date=date_start
group by 1,2,3,4,5;

-- Update end level for rows inserted previously and level 1 fake level up

drop table if exists tmp_next_level;
create temp table tmp_next_level as
select n.user_key,n.level,n.ts_start,n.date_start
from processed.fact_level_up l
join processed.fact_level_up n on n.level=l.level+1 and l.user_key=n.user_key
where l.ts_end is null and n.date_start >= (select start_date from processed.tmp_start_date);

update processed.fact_level_up
set ts_end=tmp_next_level.ts_start, date_end=tmp_next_level.date_start
from tmp_next_level
where fact_level_up.user_key=tmp_next_level.user_key and tmp_next_level.level=fact_level_up.level+1;

-- TODO: Remove duplicated records from adjust table
drop table if exists unique_adjust;
create table unique_adjust as
select *
from
(select *, row_number() over (partition by adid order by ts) as row_number
from adjust)  t
where row_number = 1;

update unique_adjust
set userid = a.userid
from adjust a
where unique_adjust.userid = '' and a.userid != '' and unique_adjust.adid = a.adid;

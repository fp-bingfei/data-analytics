-- drop table if exists processed.agg_events_raw_visit_neighbors;
-- CREATE TABLE processed.agg_events_raw_visit_neighbors
-- (
--   app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
--   date                  DATE NOT NULL ENCODE DELTA,
--   uid                   INTEGER NOT NULL,
--   snsid                 VARCHAR(64) NOT NULL ENCODE LZO,
--
--
--   neighbor_id           VARCHAR(64) ENCODE LZO,
--   op_type               VARCHAR(64) ENCODE BYTEDICT,
--   level                 VARCHAR(64) ENCODE BYTEDICT,
--   coins_get             VARCHAR(64) ENCODE BYTEDICT,
--   count                 INTEGER
-- )
--   DISTKEY(uid)
--   SORTKEY(date, app, uid, snsid);

-- ts < '2014-10-01'
insert into processed.agg_events_raw_visit_neighbors
(
    app
    ,date
    ,uid
    ,snsid
    ,neighbor_id
    ,op_type
    ,level
    ,coins_get
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'neighbor_id') as neighbor_id
      ,json_extract_path_text (properties,'op_type') as op_type
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_get') AS coins_get
      ,count(1) as count
from events_raw
where ts < '2014-10-01' and event = 'VisitNeighbors'
group by 1,2,3,4,5,6,7,8;

select date, count(1), sum(count)
from processed.agg_events_raw_visit_neighbors
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts < '2014-10-01' and event = 'VisitNeighbors'
group by 1
order by 1;

delete from events_raw
where ts < '2014-10-01' and event = 'VisitNeighbors';

-- ts >= '2014-10-01' and ts < '2014-11-01'
insert into processed.agg_events_raw_visit_neighbors
(
    app
    ,date
    ,uid
    ,snsid
    ,neighbor_id
    ,op_type
    ,level
    ,coins_get
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'neighbor_id') as neighbor_id
      ,json_extract_path_text (properties,'op_type') as op_type
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_get') AS coins_get
      ,count(1) as count
from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'VisitNeighbors'
group by 1,2,3,4,5,6,7,8;

select date, count(1), sum(count)
from processed.agg_events_raw_visit_neighbors
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'VisitNeighbors'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'VisitNeighbors';

-- ts >= '2014-11-01' and ts < '2014-12-01'
insert into processed.agg_events_raw_visit_neighbors
(
    app
    ,date
    ,uid
    ,snsid
    ,neighbor_id
    ,op_type
    ,level
    ,coins_get
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'neighbor_id') as neighbor_id
      ,json_extract_path_text (properties,'op_type') as op_type
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_get') AS coins_get
      ,count(1) as count
from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'VisitNeighbors'
group by 1,2,3,4,5,6,7,8;

select date, count(1), sum(count)
from processed.agg_events_raw_visit_neighbors
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'VisitNeighbors'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'VisitNeighbors';

-- ts >= '2014-12-01' and ts < '2015-01-01'
update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"[^\"]*\"[^\"]*\"\\\,','\"device\"\:\"Unknown\"\\\,')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'VisitNeighbors' and regexp_substr(properties,'\"device\"\:\"[^\"]*\"[^\"]*\"\\\,') != '';

update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"custom\\\\ version\"','\"device\"\:\"custom version\"')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'VisitNeighbors' and regexp_substr(properties,'\"device\"\:\"custom\\\\ version\"') != '';

insert into processed.agg_events_raw_visit_neighbors
(
    app
    ,date
    ,uid
    ,snsid
    ,neighbor_id
    ,op_type
    ,level
    ,coins_get
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'neighbor_id') as neighbor_id
      ,json_extract_path_text (properties,'op_type') as op_type
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_get') AS coins_get
      ,count(1) as count
from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'VisitNeighbors'
group by 1,2,3,4,5,6,7,8;

select date, count(1), sum(count)
from processed.agg_events_raw_visit_neighbors
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'VisitNeighbors'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'VisitNeighbors';


-- drop table if exists processed.agg_events_raw_item_purchased;
-- CREATE TABLE processed.agg_events_raw_item_purchased
-- (
--   app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
--   date                  DATE NOT NULL ENCODE DELTA,
--   uid                   INTEGER NOT NULL,
--   snsid                 VARCHAR(64) NOT NULL ENCODE LZO,
--
--
--   location              VARCHAR(64) ENCODE BYTEDICT,
--   item_id               VARCHAR(64) ENCODE BYTEDICT,
--   level                 VARCHAR(64) ENCODE BYTEDICT,
--   coins_cost            VARCHAR(64) ENCODE BYTEDICT,
--   rc_spend              VARCHAR(64) ENCODE BYTEDICT,
--   count                 INTEGER
-- )
--   DISTKEY(uid)
--   SORTKEY(date, app, uid, snsid);

-- ts < '2014-10-01'
insert into processed.agg_events_raw_item_purchased
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,item_id
    ,level
    ,coins_cost
    ,rc_spend
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'from') as location
      ,json_extract_path_text (properties,'item_ID') as item_id
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_cost') AS coins_cost
      ,json_extract_path_text(properties,'RC_spend') AS rc_spend
      ,count(1) as count
from events_raw
where ts < '2014-10-01' and event = 'ItemPurchased'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_item_purchased
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts < '2014-10-01' and event = 'ItemPurchased'
group by 1
order by 1;

delete from events_raw
where ts < '2014-10-01' and event = 'ItemPurchased';

-- ts >= '2014-10-01' and ts < '2014-11-01'
insert into processed.agg_events_raw_item_purchased
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,item_id
    ,level
    ,coins_cost
    ,rc_spend
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'from') as location
      ,json_extract_path_text (properties,'item_ID') as item_id
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_cost') AS coins_cost
      ,json_extract_path_text(properties,'RC_spend') AS rc_spend
      ,count(1) as count
from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'ItemPurchased'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_item_purchased
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'ItemPurchased'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'ItemPurchased';

-- ts >= '2014-11-01' and ts < '2014-12-01'
insert into processed.agg_events_raw_item_purchased
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,item_id
    ,level
    ,coins_cost
    ,rc_spend
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'from') as location
      ,json_extract_path_text (properties,'item_ID') as item_id
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_cost') AS coins_cost
      ,json_extract_path_text(properties,'RC_spend') AS rc_spend
      ,count(1) as count
from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'ItemPurchased'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_item_purchased
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'ItemPurchased'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'ItemPurchased';

-- ts >= '2014-12-01' and ts < '2015-01-01'
update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"custom\\\\ version\"','\"device\"\:\"custom version\"')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'ItemPurchased' and regexp_substr(properties,'\"device\"\:\"custom\\\\ version\"') != '';

update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"[^\"]*\"[^\"]*\"\\\,','\"device\"\:\"Unknown\"\\\,')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'ItemPurchased' and regexp_substr(properties,'\"device\"\:\"[^\"]*\"[^\"]*\"\\\,') != '';

insert into processed.agg_events_raw_item_purchased
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,item_id
    ,level
    ,coins_cost
    ,rc_spend
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'from') as location
      ,json_extract_path_text (properties,'item_ID') as item_id
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_cost') AS coins_cost
      ,json_extract_path_text(properties,'RC_spend') AS rc_spend
      ,count(1) as count
from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'ItemPurchased'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_item_purchased
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'ItemPurchased'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'ItemPurchased';


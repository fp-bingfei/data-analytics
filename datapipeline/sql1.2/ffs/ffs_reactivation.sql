delete from processed.fact_welcomebackreward
where reactivation_date >=(select start_date from processed.tmp_start_date);

insert into processed.fact_welcomebackreward
select
    distinct
    e.app,
    MD5(e.app||e.uid) as user_key
    ,trunc(e.ts) as reactivation_date
    ,case cast(SUBSTRING(REGEXP_SUBSTR(properties,'"lapsed_day":"[0-9]+'),15) as integer)
        when 0 then '10-29 days'
        when 1 then '30-89 days'
        else '90+ days'
     end as lapsed_day
    ,case cast(SUBSTRING(REGEXP_SUBSTR(properties,'"level_range":"[0-9]+'),16) as integer)
        when 0 then '1-10'
        when 1 then '10-21'
        when 2 then '21-55'
        else '55+'
     end as level_range
    ,cast (SUBSTRING(REGEXP_SUBSTR(properties,'"reward_times":[0-9]+'),16) as integer) as reward_times
from events_raw e
where trunc(ts) >=(select start_date from processed.tmp_start_date) and event='welcomebackReward' ;

create temporary table last_date as
select max(date) as date
from processed.fact_dau_snapshot;

create temp table tmp_reactivation_users_total_revenue as
select d.user_key, sum(d.usd) as total_revenue_usd
from processed.fact_revenue d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
where  d.date >= w.reactivation_date
group by d.user_key;

create temp table tmp_reactivation_users_d3_payers as
select d.user_key, max(is_converted_today) as d3_payer
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
join last_date l on 1=1
where d.date>=reactivation_date and w.reactivation_date <= l.date -3 and datediff(day,w.reactivation_date,d.date)<=3
group by 1;

create temp table tmp_reactivation_users_d7_revenue as
select d.user_key, sum(d.revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
join last_date l on 1=1
where d.date>=reactivation_date and w.reactivation_date <= l.date-7 and datediff(day,w.reactivation_date,d.date)<=7
group by d.user_key;

create temp table tmp_reactivation_users_d30_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
join last_date l on 1=1
where d.date>=reactivation_date and w.reactivation_date <= l.date-30 and datediff(day,w.reactivation_date,d.date)<=30
group by d.user_key;

create temp table tmp_reactivation_users_d90_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
join last_date l on 1=1
where d.date>=reactivation_date and w.reactivation_date <= l.date-90 and datediff(day,w.reactivation_date,d.date)<=90
group by d.user_key;

create temp table tmp_reactivation_users_d1_retained as
select w.user_key, 1 as d1_retained
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
where date - reactivation_date = 1
group by 1;

create temp table tmp_reactivation_users_d7_retained as
select w.user_key, 1 as d7_retained
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
where date - reactivation_date = 7
group by 1;

create temp table tmp_reactivation_users_d15_retained as
select w.user_key, 1 as d15_retained
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
where date - reactivation_date = 15
group by 1;

create temp table tmp_reactivation_users_d30_retained as
select w.user_key, 1 as d30_retained
from processed.fact_dau_snapshot d
join processed.fact_welcomebackreward w on d.user_key=w.user_key
where date - reactivation_date = 30
group by 1;

drop table if exists processed.tab_reactivation_kpi;
create table processed.tab_reactivation_kpi as
select
    u.app,
    w.reactivation_date,
    u.install_source,
    u.country,
    u.os,
    w.lapsed_day,
    w.level_range,
    w.reward_times,
    case when u.conversion_ts>=w.reactivation_date then 1 else 0 end as is_new_payer,
    case when u.conversion_ts<w.reactivation_date then 1 else 0 end as is_existing_payer,
    count(1) as reactivated_user_cnt,
    sum (case when w.reactivation_date <= ld.date-2 then 1 else 0 end) as d1_reactivated_user_cnt,
    sum (case when w.reactivation_date <= ld.date-4 then 1 else 0 end) as d3_reactivated_user_cnt,
    sum (case when w.reactivation_date <= ld.date-8 then 1 else 0 end) as d7_reactivated_user_cnt,
    sum (case when w.reactivation_date <= ld.date-16 then 1 else 0 end) as d15_reactivated_user_cnt,
    sum (case when w.reactivation_date <= ld.date-31 then 1 else 0 end) as d30_reactivated_user_cnt,
    sum (case when w.reactivation_date <= ld.date-91 then 1 else 0 end) as d90_reactivated_user_cnt,
    sum(coalesce(l.total_revenue_usd,0)) as revenue_usd,
    sum(coalesce(d7_revenue.revenue,0)) as d7_revenue_usd,
    sum(coalesce(d30_revenue.revenue,0))  as d30_revenue_usd,
    sum(coalesce(d90_revenue.revenue,0))  as d90_revenue_usd,
    sum(coalesce(d3p.d3_payer,0)) as d3_payer_cnt,
    sum(coalesce(d1_retained_users.d1_retained,0)) as d1_retained_user_cnt,
    sum(coalesce(d7_retained_users.d7_retained,0)) as d7_retained_user_cnt,
    sum(coalesce(d15_retained_users.d15_retained,0)) as d15_retained_user_cnt,
    sum(coalesce(d30_retained_users.d30_retained,0)) as d30_retained_user_cnt,
    sum(case when d7_revenue.revenue>0 then 1 else 0 end) as d7_payer_after_reactivation_cnt,
    sum(case when d30_revenue.revenue>0 then 1 else 0 end) as d30_payer_after_reactivation_cnt,
    sum(case when d90_revenue.revenue>0 then 1 else 0 end) as d90_payer_after_reactivation_cnt
from processed.fact_welcomebackreward w
join processed.dim_user u on  w.user_key=u.user_key
join (select date from last_date) ld on 1=1
    left join tmp_reactivation_users_d3_payers d3p on u.user_key = d3p.user_key
    left join tmp_reactivation_users_d7_revenue d7_revenue on u.user_key = d7_revenue.user_key
    left join tmp_reactivation_users_d30_revenue d30_revenue on u.user_key = d30_revenue.user_key
    left join tmp_reactivation_users_d90_revenue d90_revenue on u.user_key = d90_revenue.user_key
    left join tmp_reactivation_users_d1_retained d1_retained_users on u.user_key = d1_retained_users.user_key
    left join tmp_reactivation_users_d7_retained d7_retained_users on u.user_key = d7_retained_users.user_key
    left join tmp_reactivation_users_d15_retained d15_retained_users on u.user_key = d15_retained_users.user_key
    left join tmp_reactivation_users_d30_retained d30_retained_users on u.user_key = d30_retained_users.user_key
    left join tmp_reactivation_users_total_revenue l on u.user_key=l.user_key
group by 1,2,3,4,5,6,7,8,9,10;

-- create new database account
select * from pg_user;
create user benyang with password '';

create schema ffs_pm;

grant usage on schema ffs_pm to benyang;
grant create on schema ffs_pm to benyang;
grant all on all tables in schema ffs_pm to benyang;

grant usage on schema processed to benyang;
grant select on all tables in schema processed to benyang;

grant usage on schema public to benyang;
grant select on all tables in schema public to benyang;

revoke all on all tables in schema processed from benyang;
revoke all on all tables in schema public from benyang;

-- test account
create table ffs_pm.test as
select *
from processed.events_raw
limit 100;

set session authorization benyang;

select current_user;

select * from processed.agg_kpi limit 100;

create table ffs_pm.test as
select *
from processed.events_raw
limit 100;

select * from processed.events_raw limit 100;

-- TODO: get detailed info of fraud installs
create temp table fraud_installs_uid as
select
    app_id
    ,country
    ,game
    ,install_source
	,sub_publisher
	,userid
	,count(1) as installs
from processed.fraud_channel
where install_source != 'Organic' and install_date >= '2015-07-01' and install_date < '2015-08-01'
group by 1,2,3,4,5,6
having count(1) >= 10;

select distinct a.*
from
(select *
from fraud_installs_uid) t
join unique_adjust a on t.userid = a.userid
where t.install_source = 'MobVista' and t.sub_publisher = '3603' and t.userid = a.userid;

-- TODO: figure out fraud install source or sub_publisher by ip
create temp table adjust_duplicated_users as
select split_part(ip_address, '.', 1) || '.' || split_part(ip_address, '.', 2) || '.' || split_part(ip_address, '.', 3) as ip_segment
    ,count(1) as install_count
from unique_adjust
where ip_address != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel_by_ip;
create table processed.fraud_channel_by_ip as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,split_part(tracker_name, '::', 3) as sub_publisher
	,a.ip_address
	,d.ip_segment
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d
        on split_part(a.ip_address, '.', 1) || '.' || split_part(a.ip_address, '.', 2) || '.' || split_part(a.ip_address, '.', 3) = d.ip_segment;

select
    app_id
    ,country
    ,game
    ,install_source
	,sub_publisher
	,ip_segment
	,count(1) as installs
from processed.fraud_channel_by_ip
where install_source != 'Organic' and install_date >= '2015-07-01' and install_date < '2015-08-01'
group by 1,2,3,4,5,6
having count(1) >= 10;

--adjust bill
drop table if exists adjustdata;
create temp table adjustdata as 
select a.adid as adid
, a.country as country
, a.idfa as idfa
, a.ip_address as ip_address
, a.tracker_name as tracker_name
, a.ts as ts
, a.userid as userid
, d.user_key as user_key
, case when coalesce(d.user_key, '') != '' then 1 else 0 end as isInFPBI
from
(select adid, country, idfa, ip_address, tracker_name, ts, userid from adjust where tracker_name like 'Aarki%' and date(ts) > '2015-09-30' and date(ts) < '2015-11-01') 
a left outer join processed.dim_user d on a.userid = d.snsid;

drop table if exists tmp_result;
create temp table tmp_result as 
select b.* from (select userid, count(1) from adjustdata where isInFPBI = 1 group by 1 having count(1)>= 10) a join adjustdata b on a.userid = b.userid;

unload ('select * from tmp_result;')
to 's3://com.funplusgame.bidata/dev/test/parser/marketing/Aarki_1103_v2_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


drop table if exists adjustdata;
create temp table adjustdata as 
select a.adid as adid
, a.country as country
, a.idfa as idfa
, a.ip_address as ip_address
, a.android_id as android_id
, a.tracker_name as tracker_name
, a.userid as userid
, d.user_key
, case when coalesce(d.user_key, '') != '' then 1 else 0 end as isInFPBI
from
(select adid, country, idfa, ip_address, android_id, tracker_name, userid from adjust where tracker_name like 'Blindferret%' and date(ts) > '2015-09-30' and date(ts) < '2015-11-01') 
a left outer join processed.dim_user d on a.userid = d.snsid;

drop table if exists tmp_result;
create temp table tmp_result as 
select distinct b.* from (select userid from adjustdata where isInFPBI = 0) a join adjustdata b on a.userid = b.userid;

unload ('select * from tmp_result;')
to 's3://com.funplusgame.bidata/dev/test/parser/marketing/Blindferret_class1_1103_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


--retarget 20150914
drop table if exists processed.re_target_campaign_d30_inactive_payer_0914;
create table processed.re_target_campaign_d30_inactive_payer_0914 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where (level >= 5 or is_payer = 1) and last_login_ts <=CURRENT_DATE - 30 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_payer_0914 where is_payer = 1;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_payer_0914_payer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--retarget payer 20151201
drop table if exists processed.re_target_campaign_d30_inactive_payer_1201;
create table processed.re_target_campaign_d30_inactive_payer_1201 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  is_payer = 1 and last_login_ts <=CURRENT_DATE - 30 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_payer_1201;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_payer_1201_payer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--retarget level above 10 20151201
drop table if exists processed.re_target_campaign_d30_inactive_level_a10_1201;
create table processed.re_target_campaign_d30_inactive_level_a10_1201 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level > 10 and last_login_ts <=CURRENT_DATE - 30 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_a10_1201;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_level_a10_1201_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level 5-10 20151202
drop table if exists processed.re_target_campaign_d30_inactive_level_a5_u10_1202;
create table processed.re_target_campaign_d30_inactive_level_a5_u10_1202 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level > 5 and level <= 10 and last_login_ts <=CURRENT_DATE - 30 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_a5_u10_1202;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_level_a5_u10_1202'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level under 5 20151201
drop table if exists processed.re_target_campaign_d30_inactive_level_u5_1201;
create table processed.re_target_campaign_d30_inactive_level_u5_1201 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level <= 5 and last_login_ts <=CURRENT_DATE - 30 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_u5_1201;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_level_u5_1201_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--update historical install sources 
update kpi_processed.dim_user 
set install_source = 'Aarki(incent)'
where lower(install_source) = 'aarki(incent)';
update kpi_processed.dim_user
set install_source = 'Glispa(incent)'
where lower(install_source) = 'glispa(incent)';
update kpi_processed.dim_user
set install_source = 'NativeX(incent)'
where lower(install_source) like 'native%incent)';
update kpi_processed.dim_user
set install_source = 'Sponsorpay(incent)'
where lower(install_source) = 'sponsorpay(incent)';
update kpi_processed.dim_user
set install_source = 'Supersonic(incent)'
where lower(install_source) like 'supersonic%incent)';
select * from kpi_processed.dim_user where lower(install_source) like 'native%incent)' limit 200

update processed.fact_user_install_source 
set install_source = replace(install_source, '+', ' ')
    ,campaign = replace(campaign, '+', ' ')
    ,sub_publisher = replace(sub_publisher, '+', ' ')
    ,creative_id = replace(creative_id, '+', ' ')
    ,install_source_adjust_raw = replace(install_source, '+', ' ')
    ,install_source_adjust = replace(install_source, '+', ' ')
    ,install_source_raw = replace(install_source, '+', ' ');

--retarget payer 20150307
drop table if exists processed.re_target_campaign_d30_inactive_payer_0307;
create table processed.re_target_campaign_d30_inactive_payer_0307 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  is_payer = 1 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_payer_0307;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_payer_0307_payer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--retarget level above 10 20150307
drop table if exists processed.re_target_campaign_d30_inactive_level_a10_0307;
create table processed.re_target_campaign_d30_inactive_level_a10_0307 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level > 10 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_a10_0307;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_level_a10_0307_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level 5-10 20150307
drop table if exists processed.re_target_campaign_d30_inactive_level_a5_u10_0307;
create table processed.re_target_campaign_d30_inactive_level_a5_u10_0307 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level > 5 and level <= 10 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_a5_u10_0307;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_level_a5_u10_0307'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level under 5 20150307
drop table if exists processed.re_target_campaign_d30_inactive_level_u5_0307;
create table processed.re_target_campaign_d30_inactive_level_u5_0307 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level <= 5 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_u5_0307;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_level_u5_0307_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--retarget payer 20160321
drop table if exists processed.re_target_campaign_d30_inactive_payer_0321;
create table processed.re_target_campaign_d30_inactive_payer_0321 as
select distinct u.snsid as snsid
    ,upper(a.idfa) as idfa
    ,upper(a.gaid) as gaid
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.adjust a on a.userid = u.snsid
where  is_payer = 1 and last_login_ts <= CURRENT_DATE - 15 and (gaid != '' or idfa != '');


unload ('select distinct idfa from processed.re_target_campaign_d30_inactive_payer_0321 where idfa != \'\';')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_payer_0321_payer_ios_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

unload ('select distinct gaid from processed.re_target_campaign_d30_inactive_payer_0321 where gaid != \'\';')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d30_inactive_payer_0321_payer_android_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
--retarget payer 20150412
drop table if exists processed.re_target_campaign_d30_inactive_payer_0412;
create table processed.re_target_campaign_d30_inactive_payer_0412 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  is_payer = 1 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_payer_0412;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d15_inactive_payer_0412_payer_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--retarget level above 10 20150412
drop table if exists processed.re_target_campaign_d30_inactive_level_a10_0412;
create table processed.re_target_campaign_d30_inactive_level_a10_0412 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level > 10 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_a10_0412;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d15_inactive_level_a10_0412_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level 5-10 20150412
drop table if exists processed.re_target_campaign_d30_inactive_level_a5_u10_0412;
create table processed.re_target_campaign_d30_inactive_level_a5_u10_0412 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level > 5 and level <= 10 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_a5_u10_0412;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d15_inactive_level_a5_u10_0412'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level under 5 20150412
drop table if exists processed.re_target_campaign_d30_inactive_level_u5_0412;
create table processed.re_target_campaign_d30_inactive_level_u5_0412 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level <= 5 and last_login_ts <= CURRENT_DATE - 15 and f.facebook != '';


unload ('select facebook from processed.re_target_campaign_d30_inactive_level_u5_0412;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d15_inactive_level_u5_0412_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--retarget level above 10 20150412
drop table if exists processed.re_target_campaign_d3_inactive_level_a10_0512_th;
create table processed.re_target_campaign_d3_inactive_level_a10_0512_th as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
    ,case when random() > 0.5 then 'a' else  'b' end as aorb
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where level > 10 and last_login_ts <= CURRENT_DATE - 3 and f.facebook != '' and u.app = 'ffs.th.prod';

drop table if exists processed.re_target_campaign_d3_inactive_level_a10_0512_th_a;
create table processed.re_target_campaign_d3_inactive_level_a10_0512_th_a as
select snsid
,facebook
,install_source
,install_date
,os
,level
,last_login_ts
,total_revenue
,user_key
,is_payer
from processed.re_target_campaign_d3_inactive_level_a10_0512_th
where aorb = 'a';

drop table if exists processed.re_target_campaign_d3_inactive_level_a10_0512_th_b;
create table processed.re_target_campaign_d3_inactive_level_a10_0512_th_b as
select snsid
,facebook
,install_source
,install_date
,os
,level
,last_login_ts
,total_revenue
,user_key
,is_payer
from processed.re_target_campaign_d3_inactive_level_a10_0512_th
where aorb = 'b';

unload ('select facebook from processed.re_target_campaign_d3_inactive_level_a10_0512_th_a;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d3_inactive_level_a10_0512_th_a'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
unload ('select facebook from processed.re_target_campaign_d3_inactive_level_a10_0512_th_b;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d3_inactive_level_a10_0512_th_b'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--Deeplink A/B test with US level above 15 users
drop table if exists processed.re_target_campaign_d3_inactive_level_a10_20160606;
create table processed.re_target_campaign_d3_inactive_level_a10_20160606 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
    ,case when random() > 0.5 then 'a' else  'b' end as aorb
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where u.level >= 15 and country_code = 'US' and last_login_ts <= CURRENT_DATE - 3 and f.facebook != '' and u.app = 'ffs.global.prod';

drop table if exists processed.re_target_campaign_d3_inactive_level_a15_20160606_a;
create table processed.re_target_campaign_d3_inactive_level_a15_20160606_a as
select snsid
,facebook
,install_source
,install_date
,os
,level
,last_login_ts
,total_revenue
,user_key
,is_payer
from processed.re_target_campaign_d3_inactive_level_a10_20160606
where aorb = 'a';

drop table if exists processed.re_target_campaign_d3_inactive_level_a15_20160606_b;
create table processed.re_target_campaign_d3_inactive_level_a15_20160606_b as
select snsid
,facebook
,install_source
,install_date
,os
,level
,last_login_ts
,total_revenue
,user_key
,is_payer
from processed.re_target_campaign_d3_inactive_level_a10_20160606
where aorb = 'b';

unload ('select facebook from processed.re_target_campaign_d3_inactive_level_a15_20160606_a;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d3_inactive_level_a15_20160606_a'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
unload ('select facebook from processed.re_target_campaign_d3_inactive_level_a15_20160606_b;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d3_inactive_level_a15_20160606_b'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

----retarget level above 10 20160608
drop table if exists processed.re_target_campaign_d7_inactive_level_a10_0608;
create table processed.re_target_campaign_d7_inactive_level_a10_0608 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level >= 10 and last_login_ts <= CURRENT_DATE - 7 and f.facebook != '' and app = 'ffs.global.prod';


unload ('select facebook from processed.re_target_campaign_d7_inactive_level_a10_0608;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d7_inactive_level_a10_0608_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
drop table if exists processed.re_target_campaign_d7_inactive_payer_0909;
create table processed.re_target_campaign_d7_inactive_payer_0909 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  is_payer = 1 and last_login_ts <= CURRENT_DATE - 7 and f.facebook != '' and app = 'ffs.global.prod';


unload ('select facebook from processed.re_target_campaign_d7_inactive_payer_0909;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d7_inactive_payer_0909_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists processed.re_target_campaign_d5_inactive_level_a14_0927;
create table processed.re_target_campaign_d5_inactive_level_a14_0927 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  level >= 14 and last_login_ts <= CURRENT_DATE - 5 and f.facebook != '' and app = 'ffs.global.prod';


unload ('select facebook from processed.re_target_campaign_d5_inactive_level_a14_0927;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d5_inactive_level_a14_0927'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists processed.re_target_campaign_d7_inactive_payer_1118;
create table processed.re_target_campaign_d7_inactive_payer_1118 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where  is_payer = 1 and last_login_ts <= CURRENT_DATE - 7 and f.facebook != '' and app = 'ffs.global.prod';


unload ('select facebook from processed.re_target_campaign_d7_inactive_payer_1118;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_d7_inactive_payer_1118_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;


--Deeplink A/B test with US level above 15 users
drop table if exists processed.re_target_campaign_version_3x_20161121;
create table processed.re_target_campaign_version_3x_20161121 as
select distinct u.snsid as snsid
    ,f.facebook as facebook
    ,u.install_source as install_source
    ,u.install_date as install_date
    ,u.os as os
    ,u.level as level
    ,u.last_login_ts as last_login_ts
    ,u.total_revenue_usd as total_revenue
    ,u.user_key as user_key
    ,u.is_payer as is_payer
    ,case when random() > 0.5 then 'a' else  'b' end as aorb
from processed.dim_user u
  join public.facebook_id f on f.snsid = u.snsid
where is_payer = 1 and last_login_ts <= CURRENT_DATE - 59 and f.facebook != '' and u.app = 'ffs.global.prod';

drop table if exists processed.re_target_campaign_version_3x_20161121_a;
create table processed.re_target_campaign_version_3x_20161121_a as
select snsid
,facebook
,install_source
,install_date
,os
,level
,last_login_ts
,total_revenue
,user_key
,is_payer
from processed.re_target_campaign_version_3x_20161121
where aorb = 'a';

drop table if exists processed.re_target_campaign_version_3x_20161121_b;
create table processed.re_target_campaign_version_3x_20161121_b as
select snsid
,facebook
,install_source
,install_date
,os
,level
,last_login_ts
,total_revenue
,user_key
,is_payer
from processed.re_target_campaign_version_3x_20161121
where aorb = 'b';

unload ('select facebook from processed.re_target_campaign_version_3x_20161121_a;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_version_3x_20161121_a'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
unload ('select facebook from processed.re_target_campaign_version_3x_20161121_b;')
to 's3://com.funplus.bitest/ffs/retargeting/re_target_campaign_version_3x_20161121_b'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;
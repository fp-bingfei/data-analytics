-- drop table if exists processed.agg_events_raw_coins_transaction;
-- CREATE TABLE processed.agg_events_raw_coins_transaction
-- (
--   app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
--   date                  DATE NOT NULL ENCODE DELTA,
--   uid                   INTEGER NOT NULL,
--   snsid                 VARCHAR(64) NOT NULL ENCODE LZO,
--
--   location              VARCHAR(64) ENCODE BYTEDICT,
--   action                VARCHAR(64) ENCODE BYTEDICT,
--   action_detail         VARCHAR(64) ENCODE BYTEDICT,
--   level                 VARCHAR(64) ENCODE BYTEDICT,
--   coins_in              VARCHAR(64) ENCODE BYTEDICT,
--   count                 INTEGER
-- )
--   DISTKEY(uid)
--   SORTKEY(date, app, uid, snsid);

-- ts < '2014-10-01'
update events_raw
set properties = regexp_replace(properties, '\"action_detail\"\:.*\"device\"\:','\"device\"\:')
where ts < '2014-10-01' and event = 'coins_transaction';

insert into processed.agg_events_raw_coins_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,action
    ,action_detail
    ,level
    ,coins_in
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'location') as location
      ,json_extract_path_text (properties,'action') as action
      ,null as action_detail
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_in') AS coins_in
      ,count(1) as count
from events_raw
where ts < '2014-10-01' and event = 'coins_transaction'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_coins_transaction
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts < '2014-10-01' and event = 'coins_transaction'
group by 1
order by 1;

delete from events_raw
where ts < '2014-10-01' and event = 'coins_transaction';


-- ts >= '2014-10-01' and ts < '2014-11-01'
update events_raw
set properties = regexp_replace(properties, '\"action_detail\"\:.*\"device\"\:','\"device\"\:')
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'coins_transaction';

insert into processed.agg_events_raw_coins_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,action
    ,action_detail
    ,level
    ,coins_in
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'location') as location
      ,json_extract_path_text (properties,'action') as action
      ,null as action_detail
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_in') AS coins_in
      ,count(1) as count
from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'coins_transaction'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_coins_transaction
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'coins_transaction'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-10-01' and ts < '2014-11-01' and event = 'coins_transaction';


-- ts >= '2014-11-01' and ts < '2014-12-01'
update events_raw
set properties = regexp_replace(properties, '\"action_detail\"\:.*\"device\"\:','\"device\"\:')
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'coins_transaction';

insert into processed.agg_events_raw_coins_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,action
    ,action_detail
    ,level
    ,coins_in
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'location') as location
      ,json_extract_path_text (properties,'action') as action
      ,null as action_detail
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_in') AS coins_in
      ,count(1) as count
from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'coins_transaction'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_coins_transaction
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'coins_transaction'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-11-01' and ts < '2014-12-01' and event = 'coins_transaction';

-- ts >= '2014-12-01' and ts < '2015-01-01'
update events_raw
set properties = regexp_replace(properties, '\"action_detail\"\:.*\"device\"\:','\"device\"\:')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'coins_transaction';

update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"[^\"]*\"[^\"]*\"\\\,','\"device\"\:\"Unknown\"\\\,')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'coins_transaction' and regexp_substr(properties,'\"device\"\:\"[^\"]*\"[^\"]*\"\\\,') != '';

update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"custom\\\\ version\"','\"device\"\:\"custom version\"')
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'coins_transaction' and regexp_substr(properties,'\"device\"\:\"custom\\\\ version\"') != '';


insert into processed.agg_events_raw_coins_transaction
(
    app
    ,date
    ,uid
    ,snsid
    ,location
    ,action
    ,action_detail
    ,level
    ,coins_in
    ,count
)
select app
      ,trunc(ts)
      ,uid
      ,snsid
      ,json_extract_path_text (properties,'location') as location
      ,json_extract_path_text (properties,'action') as action
      ,null as action_detail
      ,json_extract_path_text(properties,'level') AS level
      ,json_extract_path_text(properties,'coins_in') AS coins_in
      ,count(1) as count
from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'coins_transaction'
group by 1,2,3,4,5,6,7,8,9;

select date, count(1), sum(count)
from processed.agg_events_raw_coins_transaction
group by 1
order by 1;

select trunc(ts), count(1)
from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'coins_transaction'
group by 1
order by 1;

delete from events_raw
where ts >= '2014-12-01' and ts < '2015-01-01' and event = 'coins_transaction';


delete from ffs.processed.rc_transaction
where date >=(
                select start_date
                from ffs.processed.tmp_start_date
             );

insert into ffs.processed.rc_transaction
(
    date
    ,transaction_type
    ,user_key
    ,app
    ,app_version
    ,os
    ,country
    ,install_source
    ,browser
    ,level
    ,location
    ,action
    ,action_detail
    ,rc_in
    ,rc_out
)
select
    trunc(e.ts)
    ,null as transaction_type
    ,MD5(e.app||e.uid) as user_key
    ,e.app
    ,null as app_version
    ,u.os
    ,u.country
    ,u.install_source
    ,u.browser
    ,cast(json_extract_path_text(e.properties,'level') as smallint) as level
    ,json_extract_path_text(e.properties,'location') as location
    ,nvl(nullif(json_extract_path_text (properties,'real_action'),''), json_extract_path_text(e.properties,'action')) as action
    ,null as action_detail
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_in')='' then '0' else json_extract_path_text(e.properties,'rc_in') end as BIGINT)) as rc_in
    ,sum(cast(case when json_extract_path_text(e.properties,'rc_out')='' then '0' else json_extract_path_text(e.properties,'rc_out') end as BIGINT)) as rc_out
from events_raw e join processed.dim_user u on MD5(e.app||e.uid) = u.user_key
where e.event = 'rc_transaction'
and     trunc(ts) >= (
                        select start_date
                        from ffs.processed.tmp_start_date
                     )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

update ffs.processed.rc_transaction
set transaction_type =
    case
        when rc_in > 0 and rc_out = 0 then 'rc in'
        when rc_in = 0 and rc_out > 0 then 'rc out'
        when rc_in > 0 and rc_out > 0 then 'rc in & out'
        else 'Unknown'
    end;

delete
from ffs.processed.rc_transaction
where action = 'edituser';

delete
from ffs.processed.rc_transaction
where date = CURRENT_DATE;

vacuum processed.rc_transaction;

----------------------------
--cheating list
----------------------------
/*delete from processed.cheating_list                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                         
              );  
              
              
insert into processed.cheating_list
select t1.app
       ,t1.date
       ,t2.snsid
       ,sum(t1.rc_in)
from processed.rc_transaction t1
left join processed.dim_user t2
on t1.user_key = t2.user_key
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                         
              ) 
group by 1,2,3
having sum(t1.rc_in) >1000
;*/

-- TODO: figure out fraud install source or sub_publisher
create temp table adjust_duplicated_users as
select userid, count(1) as install_count
from unique_adjust
where userid != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel;
create table processed.fraud_channel as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d on a.userid = d.userid;


-- TODO: Discrepancy between Adjust and BI
drop table if exists processed.adjust_bi_discrepancy;
create table processed.adjust_bi_discrepancy as
select
    t5.install_date
    ,t5.install_date_str
    ,t5.app_id
    ,c.country
    ,t5.install_source
    ,t5.campaign
    ,t5.sub_publisher
    ,t5.creative_id
    ,t5.os
    ,t5.all_adjust_installs
    ,t5.empty_user_id_installs
    ,t5.in_bi_installs
    ,t5.not_in_bi_installs
from
    (select
        t1.install_date
        ,cast(t1.install_date as VARCHAR) as install_date_str
        ,t1.app_id
        ,t1.country
        ,t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.os
        ,t1.all_adjust_installs
        ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
        ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
        ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
    from
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.familyfarm' then 'Android'
                when app_id in ('883531333', '539920547') then 'iOS'
                when app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
            end as os
            ,count(1) as all_adjust_installs
        from unique_adjust
        group by 1,2,3,4,5,6,7,8) t1
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.familyfarm' then 'Android'
                when app_id in ('883531333', '539920547') then 'iOS'
                when app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
            end as os
            ,count(1) as empty_user_id_installs
        from unique_adjust
        where userid = '' or userid is null
        group by 1,2,3,4,5,6,7,8) t2
            on coalesce(t1.install_source,'Unknown') = coalesce(t2.install_source,'Unknown') and
               t1.install_date = t2.install_date and
               coalesce(t1.campaign,'Unknown') = coalesce(t2.campaign,'Unknown') and
               coalesce(t1.sub_publisher,'Unknown') = coalesce(t2.sub_publisher,'Unknown') and
               coalesce(t1.creative_id,'Unknown') = coalesce(t2.creative_id,'Unknown') and
               t1.app_id = t2.app_id and
               coalesce(t1.country,'Unknown') = coalesce(t2.country,'Unknown') and
               coalesce(t1.os,'Unknown') = coalesce(t2.os,'Unknown')
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,a.country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.familyfarm' then 'Android'
                when app_id in ('883531333', '539920547') then 'iOS'
                when app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
            end as os
            ,count(1) in_bi_installs
        from unique_adjust a join processed.dim_user u on a.userid = u.snsid and u.app = a.game and lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) = replace(lower(u.install_source), '+' , ' ') and trunc(a.ts) = u.install_date
        where a.userid != '' and a.userid is not null and u.snsid is not null
        group by 1,2,3,4,5,6,7,8) t3
            on coalesce(t1.install_source,'Unknown') = coalesce(t3.install_source,'Unknown') and
               t1.install_date = t3.install_date and
               coalesce(t1.campaign,'Unknown') = coalesce(t3.campaign,'Unknown') and
               coalesce(t1.sub_publisher,'Unknown') = coalesce(t3.sub_publisher,'Unknown') and
               coalesce(t1.creative_id,'Unknown') = coalesce(t3.creative_id,'Unknown') and
               t1.app_id = t3.app_id and
               coalesce(t1.country,'Unknown') = coalesce(t3.country,'Unknown') and
               coalesce(t1.os,'Unknown') = coalesce(t3.os,'Unknown')
        left join
        (select
            initcap(split_part(tracker_name, '::', 1)) as install_source
            ,split_part(tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 3)
                when lower(split_part(replace(tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(tracker_name, '::', 4) as creative_id
            ,game as app_id
            ,a.country
            ,trunc(ts) as install_date
            ,case
                when app_id = 'com.funplus.familyfarm' then 'Android'
                when app_id in ('883531333', '539920547') then 'iOS'
                when app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
            end as os
            ,count(1) not_in_bi_installs
        from unique_adjust a left join processed.dim_user u on a.userid = u.snsid and u.app = a.game
        where a.userid != '' and a.userid is not null and (u.snsid is null or lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) != replace(lower(u.install_source), '+' , ' ') or trunc(a.ts) != u.install_date)
        group by 1,2,3,4,5,6,7,8) t4
            on coalesce(t1.install_source,'Unknown') = coalesce(t4.install_source,'Unknown') and
               t1.install_date = t4.install_date and
               coalesce(t1.campaign,'Unknown') = coalesce(t4.campaign,'Unknown') and
               coalesce(t1.sub_publisher,'Unknown') = coalesce(t4.sub_publisher,'Unknown') and
               coalesce(t1.creative_id,'Unknown') = coalesce(t4.creative_id,'Unknown') and
               t1.app_id = t4.app_id and
               coalesce(t1.country,'Unknown') = coalesce(t4.country,'Unknown') and
               coalesce(t1.os,'Unknown') = coalesce(t4.os,'Unknown')
    ) t5
    left join
    public.country c
    on t5.country=lower(c.country_code);

-- Fraud with same IP in one day
drop table if exists processed.adjust_ip_fraud;
create table processed.adjust_ip_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when a.app_id = 'com.funplus.familyfarm' then 'Android'
        when a.app_id in ('883531333', '539920547') then 'iOS'
        when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
     end as os
    ,count(1) as fraud_count
    ,count(distinct a.userid) as distinct_fraud_count
from
    unique_adjust a
    left join
    public.country c on a.country=lower(c.country_code)
where 
    (a.ip_address, date(a.ts))
    in 
    (
        select 
            ip_address
            ,date 
        from 
            (
                select 
                    ip_address
                    ,date(ts) as date
                    ,count(1) 
                from 
                    unique_adjust 
                where
                    tracker_name<>'Organic'
                    and date(ts)>=DATEADD(DAY, -90, current_date)
                group by 1,2
                having count(1)>=10
            )
    )
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -90, current_date)
group by 1,2,3,4,5,6,7,8,9;

-- Timestamp pattern of generating records
drop table if exists processed.adjust_ts_pattern;
create table processed.adjust_ts_pattern as
select
    *
from
    (
    select
        t.install_source
        ,t.campaign
        ,t.sub_publisher
        ,t.creative_id
        ,t.app_id
        ,t.country
        ,t.install_date
        ,t.os
        ,t.tslen - (LAG(t.tslen) OVER(PARTITION BY t.install_source ORDER BY t.install_source, t.tslen)) as tsdiff 
    from
        (
        select
            initcap(split_part(a.tracker_name, '::', 1)) as install_source
            ,split_part(a.tracker_name, '::', 2) as campaign
            ,case
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
                when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
             end as sub_publisher
            ,split_part(a.tracker_name, '::', 4) as creative_id
            ,a.game as app_id
            ,c.country
            ,trunc(ts) as install_date
            ,case
                when a.app_id = 'com.funplus.familyfarm' then 'Android'
                when a.app_id in ('883531333', '539920547') then 'iOS'
                when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
             end as os
            ,(extract(epoch from a.ts) - extract(epoch from DATEADD(DAY, -90, current_date))) as tslen
        from
            unique_adjust a
            left join
            public.country c on a.country=lower(c.country_code)
        where
            a.tracker_name<>'Organic'
            and date(a.ts)>=DATEADD(DAY, -90, current_date)
            and split_part(a.tracker_name, '::', 1) != ''
        ) t
    order by t.install_source, t.tslen
    )
where
    tsdiff is not null
    and tsdiff <= 200;

-- Fraud with same device model in one day
drop table if exists processed.adjust_device_fraud;
create table processed.adjust_device_fraud as
select 
        t1.install_source
        ,t1.campaign
        ,t1.sub_publisher
        ,t1.creative_id
        ,t1.app_id
        ,t1.country
        ,t1.device_name
        ,t1.install_date
        ,t1.os
        ,t1.device_count as device_count
        ,t2.device_count as device_total
        ,t1.device_count*1.00/t2.device_count as device_percent
from
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,a.device_name
        ,trunc(ts) as install_date
        ,case
            when a.app_id = 'com.funplus.familyfarm' then 'Android'
            when a.app_id in ('883531333', '539920547') then 'iOS'
            when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
         end as os
        ,count(1) as device_count
    from
        unique_adjust a
        left join
        public.country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.device_name != ''
    group by 1,2,3,4,5,6,7,8,9
    having count(1) >= 10
    ) t1
    left join
    (select 
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,split_part(a.tracker_name, '::', 2) as campaign
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,split_part(a.tracker_name, '::', 4) as creative_id
        ,a.game as app_id
        ,c.country
        ,trunc(ts) as install_date
        ,case
            when a.app_id = 'com.funplus.familyfarm' then 'Android'
            when a.app_id in ('883531333', '539920547') then 'iOS'
            when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
         end as os
        ,count(1) as device_count
    from
        unique_adjust a
        left join
        public.country c on a.country=lower(c.country_code)
    where 
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -90, current_date)
        and a.device_name != ''
    group by 1,2,3,4,5,6,7,8
    having count(1) >= 10
    ) t2
    on
        t1.install_source = t2.install_source
        and t1.campaign = t2.campaign
        and t1.sub_publisher = t2.sub_publisher
        and t1.creative_id = t2.creative_id
        and t1.app_id = t2.app_id
        and t1.country = t2.country
        and t1.install_date = t2.install_date
        and t1.os = t2.os
order by device_percent desc;

-- Adjust interval between install and first action 
drop table if exists processed.adjust_first_action;
create table processed.adjust_first_action as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,trunc(a.ts) as install_date
    ,case
        when a.app_id = 'com.funplus.familyfarm' then 'Android'
        when a.app_id in ('883531333', '539920547') then 'iOS'
        when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
     end as os
    ,a.userid
    ,a.tslen
    ,count(1) as session_cnt
    ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    ,max(s.level_end) as level_end
from
    (select
        *
        ,case
            when u.all_max_level = 1 then null
            else (extract(epoch from u.ts_end) - extract(epoch from u.ts_start))
         end as tslen
    from
        unique_adjust adj 
        left join 
        (select
            *
            ,row_number () over (partition by snsid order by ts_start) as rank
            ,max(level_end) over (partition by snsid) as all_max_level
        from 
            processed.fact_session
        where
            date_start>=DATEADD(DAY, -90, current_date)
            and install_date>=DATEADD(DAY, -90, current_date)
        ) u on adj.userid = u.snsid and u.app = adj.game and trunc(adj.ts) = u.install_date
    where adj.tracker_name<>'Organic'
        and split_part(adj.tracker_name, '::', 1) != ''
        and date(adj.ts)>=DATEADD(DAY, -90, current_date)
        and adj.userid != ''
        and adj.userid is not null
        and u.snsid is not null
        and u.rank = 1
    ) a
    left join
    public.country c on a.country=lower(c.country_code)
    left join
    processed.fact_dau_snapshot s on a.userid = s.snsid and a.game = s.app and trunc(a.ts) = s.install_date
        and s.date >= DATEADD(DAY, -90, current_date) and s.install_date >= DATEADD(DAY, -90, current_date)
group by 1,2,3,4,5,6,7,8,9,10
;

-- Adjust country fraud
drop table if exists processed.adjust_country_fraud;
create table processed.adjust_country_fraud as
select 
    initcap(split_part(a.tracker_name, '::', 1)) as install_source
    ,split_part(a.tracker_name, '::', 2) as campaign
    ,case
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
        when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
     end as sub_publisher
    ,split_part(a.tracker_name, '::', 4) as creative_id
    ,a.game as app_id
    ,c.country
    ,a.ip_address
    ,trunc(ts) as install_date
    ,case
        when a.app_id = 'com.funplus.familyfarm' then 'Android'
        when a.app_id in ('883531333', '539920547') then 'iOS'
        when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
     end as os
    ,count(1) as fraud_count
    ,count(distinct a.userid) as distinct_fraud_count
from
    unique_adjust a
    left join
    public.country c on a.country=lower(c.country_code)
where 
    charindex('facebook', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('google', lower(split_part(a.tracker_name, '::', 1))) = 0
    and charindex('youtube', lower(split_part(a.tracker_name, '::', 1))) = 0
    and c.country not in ('United States', 'Russian Federation','Germany','France','United Kingdom','Korea, Republic of','Japan','Canada','Taiwan','Australia')
    and c.country not in ('Netherlands','Norway','Switzerland','Austria','Sweden','Denmark','Ireland','Spain','Italy','Hong Kong')
    and c.country not in ('New Zealand','Singapore')
    and a.tracker_name<>'Organic'
    and date(a.ts)>=DATEADD(DAY, -8, current_date)
    and date(a.ts)<DATEADD(DAY, -1, current_date)
group by 1,2,3,4,5,6,7,8,9
;

-- Adjust fraud monitor
drop table if exists processed.adjust_fraud_monitor;
create table processed.adjust_fraud_monitor as
select
    t1.install_source
    ,t1.sub_publisher
    ,t1.app_id
    ,trunc(DATEADD(DAY, -8, current_date)) as install_date
    ,t1.install_count
    ,t2.d1_retained*1.00/t2.d1_new_installs as d1_retention
    ,t2.d7_retained*1.00/t2.d7_new_installs as d7_retention
    ,t3.level1_percent
    ,t4.discrepancy
    ,t5.country_fraud_count*1.00/t1.install_count as country_discrepancy
from
    (
    select
        initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,a.game as app_id
        ,count(1) as install_count
    from
        unique_adjust a
    where
        a.tracker_name<>'Organic'
        and date(a.ts)>=DATEADD(DAY, -8, current_date)
        and date(a.ts)<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    having count(1) > 50
    ) as t1
    left join
    (
    select
        initcap(install_source) as install_source
        ,case
            when install_source like 'Google Adwords%' then creative_id
            else sub_publisher
         end as sub_publisher
        ,app
        ,sum(d1_retained) as d1_retained
        ,case
            when sum(d1_new_installs) = 0 then null
            when sum(d1_new_installs) <> 0 then sum(d1_new_installs)
         end as d1_new_installs
        ,sum(d7_retained) as d7_retained
        ,case
            when sum(d7_new_installs) = 0 then null
            when sum(d7_new_installs) <> 0 then sum(d7_new_installs)
         end as d7_new_installs
    from
        processed.tab_marketing_kpi
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t2
    on
        t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
        and t1.app_id = t2.app
    left join
    (
    select
        initcap(f1.install_source) as install_source
        ,f1.sub_publisher
        ,f1.app_id
        ,f1.level1_count*1.00/f2.total as level1_percent
    from
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as level1_count
        from
            processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
            and tslen is null
            and level_end = 1
            and active_days >= 2
        group by 1,2,3
        ) as f1
        left join
        (select
            initcap(install_source) as install_source
            ,sub_publisher
            ,app_id
            ,count(1) as total
        from
            processed.adjust_first_action
        where
            install_date>=DATEADD(DAY, -8, current_date)
            and install_date<DATEADD(DAY, -1, current_date)
        group by 1,2,3
        ) as f2
        on
            f1.install_source = f2.install_source
            and f1.sub_publisher = f2.sub_publisher
            and f1.app_id = f2.app_id
    ) as t3
    on
        t1.install_source = t3.install_source
        and t1.sub_publisher = t3.sub_publisher
        and t1.app_id = t3.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,abs((sum(all_adjust_installs) - sum(in_bi_installs))*1.00/sum(all_adjust_installs)) as discrepancy
    from
        processed.adjust_bi_discrepancy
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t4
    on
        t1.install_source = t4.install_source
        and t1.sub_publisher = t4.sub_publisher
        and t1.app_id = t4.app_id
    left join
    (
    select
        initcap(install_source) as install_source
        ,sub_publisher
        ,app_id
        ,sum(fraud_count) as country_fraud_count
    from
        processed.adjust_country_fraud
    where
        install_date>=DATEADD(DAY, -8, current_date)
        and install_date<DATEADD(DAY, -1, current_date)
    group by 1,2,3
    ) as t5
    on
        t1.install_source = t5.install_source
        and t1.sub_publisher = t5.sub_publisher
        and t1.app_id = t5.app_id
where
    ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.2 and lower(t1.install_source) not in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or ((t2.d1_retained*1.00/t2.d1_new_installs) < 0.1 and lower(t1.install_source) in ('supersonic(incent)', 'sponsorpay(incent)', 'jump ramp'))
    or (t2.d1_retained*1.00/t2.d1_new_installs) > 0.6
    or t3.level1_percent > 0.1
    or t4.discrepancy > 0.2
    or (t5.country_fraud_count*1.00/t1.install_count) > 0.1
;

-- Blacklist
drop table if exists processed.blacklist;
create table processed.blacklist as
select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,ip_address
    ,country
    ,null as device_name
    ,sum(fraud_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,cast(null as numeric) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    processed.adjust_ip_fraud
group by 1,2,3,4,5,6
having sum(fraud_count) > 20

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,country
    ,device_name
    ,sum(device_count) as install_count
    ,cast(null as numeric) as d1_retention
    ,cast(null as numeric) as d7_retention
    ,cast(null as numeric) as level1_percent
    ,cast(null as numeric) as discrepancy
    ,sum(device_count)*1.00/sum(device_total) as device_percent
    ,cast(null as numeric) as country_discrepancy
from
    processed.adjust_device_fraud
group by 1,2,3,4,5,6,7
having 
    sum(device_count) > 20
    and sum(device_count)*1.00/sum(device_total) > 0.2

union all

select
    app_id
    ,install_source
    ,sub_publisher
    ,install_date
    ,null as ip_address
    ,null as country
    ,null as device_name
    ,install_count
    ,d1_retention
    ,d7_retention
    ,level1_percent
    ,discrepancy
    ,cast(null as numeric) as device_percent
    ,country_discrepancy
from
    processed.adjust_fraud_monitor
;

-- Training Data
drop table if exists processed.training_data;
create table processed.training_data as
select
    t1.id
    ,t1.userid
    ,t1.app_id
    ,t1.install_source
    ,t1.sub_publisher
    ,t1.install_date
    ,t1.ip_address
    ,t1.country
    ,t1.device_name
    ,t1.os
    ,t1.os_version
    ,t1.level_end
    ,t1.session_cnt
    ,t1.purchase_cnt
    ,t1.active_days
    ,max(case
        when t1.level_end = 1 and t1.active_days >= 5 then 1
        else 0
     end) as flag1
    ,max(case
        when t1.level_end = 1 and t1.ip_address = t2.ip_address then 1
        else 0
     end) as flag2
    ,max(case
        when t1.level_end = 1 and t1.os = 'Android' and t1.device_name = t2.device_name then 1
        else 0
     end) as flag3
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.d1_retention < 0.05 then 1
        else 0
     end) as flag4
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.level1_percent > 0.4 then 1
        else 0
     end) as flag5
    ,max(case
        when t1.level_end = 1 and lower(t1.install_source) not like '%incent%' and t2.discrepancy > 0.4 then 1
        else 0
     end) as flag6
    ,max(case
        when t1.level_end = 1 and t2.country_discrepancy > 0.1 
            and charindex('facebook', lower(t1.install_source)) = 0
            and charindex('google', lower(t1.install_source)) = 0
            and charindex('youtube', lower(t1.install_source)) = 0
            and t1.country not in ('United States', 'Russian Federation','Germany','France','United Kingdom','Korea, Republic of','Japan','Canada','Taiwan','Australia')
            and t1.country not in ('Netherlands','Norway','Switzerland','Austria','Sweden','Denmark','Ireland','Spain','Italy','Hong Kong')
            and t1.country not in ('New Zealand','Singapore') then 1
        else 0
     end) as flag7
from
    (
    select
        a.id
        ,a.userid
        ,a.game as app_id
        ,initcap(split_part(a.tracker_name, '::', 1)) as install_source
        ,case
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) not in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 3)
            when lower(split_part(replace(a.tracker_name, '+' , ' '), '::', 1)) in ('google adwords', 'google adwords mobile', 'google adwords mobile display') then split_part(a.tracker_name, '::', 4)
         end as sub_publisher
        ,trunc(a.ts) as install_date
        ,a.ip_address
        ,c.country
        ,a.device_name
        ,case
            when a.app_id = 'com.funplus.familyfarm' then 'Android'
            when a.app_id in ('883531333', '539920547') then 'iOS'
            when a.app_id in ('2fc36411ad654d9fb43afa7640026896') then 'Amazon'
         end as os
        ,a.os_version
        ,max(s.level_end) as level_end
        ,sum(s.session_cnt) as session_cnt
        ,sum(s.purchase_cnt) as purchase_cnt
        ,sum(case when s.session_cnt>0 then 1 else 0 end) as active_days
    from
        unique_adjust a
        left join
        public.country c on a.country=lower(c.country_code)
        left join
        processed.fact_dau_snapshot s on a.userid = s.snsid and a.game = s.app and trunc(a.ts) = s.install_date and s.date >= DATEADD(DAY, -8, current_date) and s.date < DATEADD(DAY, -1, current_date)
    where 
        a.tracker_name <> 'Organic'
        and date(a.ts) >= DATEADD(DAY, -8, current_date)
        and date(a.ts) < DATEADD(DAY, -1, current_date)
    group by 1,2,3,4,5,6,7,8,9,10,11
    ) t1
    left join
    (
    select
        *
    from
        processed.blacklist b
    where
        b.install_date < DATEADD(DAY, -1, current_date)
    ) t2
    on
        t1.app_id = t2.app_id
        and t1.install_source = t2.install_source
        and t1.sub_publisher = t2.sub_publisher
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
;

--Resource reports
CREATE TABLE if not exists processed.fact_resource_daily
(
    ts TIMESTAMP ENCODE delta,
    app VARCHAR(100) ENCODE lzo,
    snsid VARCHAR(100) ENCODE lzo,
    resource VARCHAR(25) ENCODE lzo distkey,
    qty INTEGER,
    level SMALLINT,
    vip_level SMALLINT
)
interleaved sortkey (
    ts,
    app,
    resource,
    level,
    qty
);

delete from processed.fact_resource_daily
where datediff('day', ts, current_date)<=3;

create temp table g as
select ts, app, snsid, properties from
(SELECT *, row_number() over (partition by md5(trunc(ts)||app||snsid) order by ts desc) as rnum
FROM ffs.public.events_raw where event='login' 
and datediff('day', ts, current_date)<=3
)
where rnum=1;

insert into processed.fact_resource_daily
select * from (
(select ts, app, snsid, 'farm_aides_kettle_25' as resource, nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'kettle'), '25'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'farm_aides_kettle_100' as resource, nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'kettle'), '100'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'farm_aides_fertilizer_25' as resource, nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'fertilizer'), '25'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'farm_aides_fertilizer_100' as resource, nullif(json_extract_path_text(json_extract_path_text(json_extract_path_text(properties, 'farm_aides'), 'fertilizer'), '100'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'ticket_1001' as resource, nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1001'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'ticket_1002' as resource, nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1002'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'ticket_1003' as resource, nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1003'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'ticket_1004' as resource, nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1004'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'ticket_1006' as resource, nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1006'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'ticket_1007' as resource, nullif(json_extract_path_text(json_extract_path_text(properties, 'ticket'), '1007'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'neighbors_num' as resource, nullif(json_extract_path_text(properties, 'neighbors_num'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'op' as resource, nullif(json_extract_path_text(properties, 'op'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
union
(select ts, app, snsid, 'rc_left' as resource, nullif(json_extract_path_text(properties, 'rc_left'), '')::int as qty, 
nullif(json_extract_path_text(properties, 'level'),'')::int as level,
nullif(json_extract_path_text(properties, 'vip_level'),'')::int as vip_level
from g)
) where qty>0;
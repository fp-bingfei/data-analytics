-- this report is used to determine the unsupported os version and device

delete from processed.agg_ingame_os_device
where date >=(
                select start_date
                from ffs.processed.tmp_start_date
             );

insert into processed.agg_ingame_os_device
(
      date
      ,app
      ,app_version
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,os
      ,os_version
      ,is_payer
      ,new_installs
      ,dau
      ,today_payers
      ,revenue
)
select  date
      ,d.app
      ,d.app_version
      ,d.device
      ,d.device_alias
      ,d.browser
      ,d.browser_version
      ,d.os
      ,d.os_version
      ,d.is_payer
      ,sum(d.is_new_user) as new_installs
      ,count(d.user_key) as dau
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as today_payers
      ,sum(d.revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
where date >= (select start_date from ffs.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10;

---- lifetime payment
--create temp table user_snsid as
--select app
--        ,snsid
--        ,min(os) as os
--        ,min(country) as country
--        ,min(install_source) as install_source
--        ,min(install_date) as install_date
--        ,min(browser) as browser
--        ,trunc(min(conversion_ts)) as conversion_date
--        ,trunc(min(last_login_ts)) as last_login_date
--from processed.dim_user u
--where u.is_payer = 1
--group by app, snsid;
--
--create temp table fact_lifetime_payment as
--select date
--        ,app
--        ,snsid
--        ,level
--        ,count(1) as purchase_cnt
--        ,sum(usd) as revenue
--        ,sum(rc_in) as rc_in
--        ,sum(coins_in) as coins_in
--from processed.fact_revenue r
--group by date, app, snsid, level;
--
--
---- agg_lifetime_payment
--delete from processed.agg_lifetime_payment
--where date >=(
--                select start_date
--                from ffs.processed.tmp_start_date
--             );
--
--insert into processed.agg_lifetime_payment
--(
--  date
--  ,app
--  ,snsid
--  ,level
--  ,install_date
--  ,conversion_date
--  ,last_login_date
--  ,os
--  ,country
--  ,install_source
--  ,browser
--  ,purchase_cnt
--  ,revenue
--  ,rc_in
--  ,coins_in
--)
--select p.date
--        ,p.app
--        ,p.snsid
--        ,p.level
--        ,u.install_date
--        ,u.conversion_date
--        ,u.last_login_date
--        ,u.os
--        ,u.country
--        ,u.install_source
--        ,u.browser
--        ,p.purchase_cnt
--        ,p.revenue
--        ,p.rc_in
--        ,p.coins_in
--from fact_lifetime_payment p
--        join user_snsid u on p.app = u.app and p.snsid = u.snsid
--where p.date >= (select start_date from ffs.processed.tmp_start_date);

-- old lifetime payment
--drop table if exists processed.agg_lifetime_payment_old;
--create table processed.agg_lifetime_payment_old as
--SELECT t.app,
--       t.snsid ,
--       t.install_dt,
--       t.dt,
--       t.install_source,
--       t.device,
--       t.country_code,
--       t.os_version,
--       t.os,
--       t.pay_time,
--       t.max_dt,
--       c.country,p2.ts as firstday_pay,
--       t.conversion_date,t.revenue
--FROM
--  (SELECT p.app,
--  p.snsid,
-- p.install_dt,
-- trunc(p.ts) as dt,
-- p.install_source,
-- p.device,
-- p.country_code,
-- p.os_version,
-- p.os,
-- y.pay_time,
-- d.max_dt,
-- p1.conversion_date,
-- a.revenue FROM payment p
--   LEFT JOIN
--      (select app,
--             snsid,
--             max(dt) as max_dt
--      from   dau
--      group by 1,2
--      )d
--   on p.app=d.app
--   and p.snsid=d.snsid
-- left join (select app,snsid,trunc(ts) as conversion_date from payment where pay_times='1' )p1 on p.app=p1.app and p.snsid=p1.snsid
-- left join (select app,snsid,sum(amount) as revenue from payment group by app,snsid)a on p.app=a.app and p.snsid=a.snsid
--  left join (select app,snsid,max(cast (pay_times as int)) as pay_time from payment group by app,snsid)y on p.app=y.app and p.snsid=y.snsid)t
-- left join payment p2 on t.conversion_date=trunc(p2.ts) and t.app=p2.app and t.snsid=p2.snsid
-- LEFT JOIN country c ON t.country_code=c.country_code;


-- -- History from game server dump

--drop table if exists processed.dim_user_history ;
--create table processed.dim_user_history as
--select
--    d.uid as uid,
--    d.snsid as snsid,
--    d.level,
--    cast(addtime as timestamp) as install_ts,
--    date(addtime) as install_date,
--    trunc(TIMESTAMP 'epoch' + logintime::BIGINT*INTERVAL '1 Second')  as last_login_ts,
--    trunc((TIMESTAMP 'epoch' + logintime::BIGINT*INTERVAL '1 Second'))  as last_login_date,
--    system_version as os_version,
--    case
--       when lower(product) like '%android%' then 'Android'
--       when lower(product) like '%amazon%' then 'Android'
--       when lower(product) like '%ios%' then 'iOS'
--       else null
--    end as os,
--    d.language
--from gameserver_dump.tbl_user d
--where addtime is not null and addtime not in ('0000-00-00 00:00:00','')   and d.product not like '%dev%'  and addtime<'2014-09-01';
--

drop table if exists processed.agg_ltv_dist;
create table processed.agg_ltv_dist as
with users as
(
    select
        user_key, snsid, uid,
        min(install_date) as install_date, max(last_login_date) as last_login_date,
        max(level) as level,
        case when max(country) is null or max(country)='' then 'Unknown' else max(country) end as country,
        case when max(app) is null or max(app)='' then 'Unknown' else max(app) end as app,
        case when max(os) is null or max(os)='' then 'Unknown' else max(os) end as os,
        case when max(language) is null or max(language)='' then 'Unknown' else max(language) end as language
    from (
        select md5(uid||snsid) as user_key,snsid,uid,min(install_date) as install_date, max(trunc(last_login_ts))  as last_login_date, max(level) as level, max(country) as country,max(app) as app, max(os) as os, max(language) as language
        from processed.dim_user u
        where is_payer=1
        group by 1,2,3
        union
        select md5(uid||snsid) as user_key,snsid,uid,min(install_date) as install_date, max(trunc(last_login_ts))  as last_login_date, max(level) as level, null as country_code, null as app, max(os) as os, max(language) as language
        from processed.dim_user_history u
        group by 1,2,3
    )
    group by 1,2,3
),
total_revenue as
(
    select user_key,sum(revenue) as revenue, sum(purchase_cnt) as purchase_cnt, max(last_purchase_date) as last_purchase_date, min(conversion_date) as conversion_date
    from (
        select  md5(r.uid||r.snsid) as user_key
                ,sum(usd) as revenue
                ,count(1) as purchase_cnt
                ,max(date) as last_purchase_date
                ,min(date) as conversion_date
        from processed.fact_revenue r
        where date>='2014-09-01'
        group by 1
        union
        select  md5(u.uid||u.snsid) as user_key
                ,sum(amount/100::real) as revenue
                ,count(1) as purchase_cnt
                ,max(trunc(paid_time)) as last_purchase_date
                ,min(trunc(paid_time)) as conversion_date
        from ffs.public.tbl_payments r
        join processed.dim_user_history u on r.uid=u.snsid
        where paid_time<'2014-09-01'
        group by 1
    )
    group by 1
)
select u.user_key,u.snsid,u.uid,u.level,u.country,u.app,u.os,u.language, u.install_date,u.last_login_date,r.last_purchase_date,r.revenue, conversion_date,purchase_cnt,
PERCENT_RANK () OVER (ORDER BY r.revenue) as percentile
from users u
join total_revenue r on u.user_key=r.user_key;

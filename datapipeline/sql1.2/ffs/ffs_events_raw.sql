update events_raw
set properties = regexp_replace(properties, '\"action_detail\"\:.*\"device\"\:','\"device\"\:')
where event = 'rc_transaction';

update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"[^\"]*\"[^\"]*\"\\\,','\"device\"\:\"Unknown\"\\\,')
where event = 'rc_transaction' and regexp_substr(properties,'\"device\"\:\"[^\"]*\"[^\"]*\"\\\,') != '';

update events_raw
set properties = regexp_replace(properties, '\"device\"\:\"custom\\\\ version\"','\"device\"\:\"custom version\"')
where event = 'rc_transaction' and regexp_substr(properties,'\"device\"\:\"custom\\\\ version\"') != '';
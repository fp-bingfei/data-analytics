--------------------------------------------------------------------------------------------------------------------------------------------
--FFS fact dau snapshot
--Version 1.2
--Author Robin
/**
Description:
This script is generate dim_user tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------------------------------------------
--processed.dim_user
----------------------------------------------------------------------------------------------------------------------------------------------
create temp table temp_dim_user_latest
(
  user_key                  VARCHAR(50) NOT NULL ENCODE LZO,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(100) DEFAULT ''ENCODE BYTEDICT,
  sub_publisher             VARCHAR(500) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(100) DEFAULT ''ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT ,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  last_login_ts             TIMESTAMP ENCODE DELTA,
  vip_level                 SMALLINT
)  
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);


insert into temp_dim_user_latest
(
          user_key 
         ,uid 
         ,snsid 
         ,install_ts
         ,install_date
         ,app 
         ,language 
         ,birth_date 
         ,gender 
         ,app_version
         ,level
         ,os
         ,os_version
         ,country_code
         ,country
         ,device
         ,browser
         ,browser_version
         ,is_payer
)
select    user_key 
         ,uid 
         ,snsid 
         ,install_ts
         ,trunc(install_ts)
         ,app 
         ,language 
         ,null as birth_date 
         ,null as gender 
         ,null as app_version
         ,level_end
         ,os 
         ,os_version 
         ,t.country_code
         ,coalesce(c.country,'Unknown') as country
         ,device  
         ,browser 
         ,browser_version 
         ,is_payer
from  (
         select *
                ,row_number() over (partition by user_key order by date desc) as row
         from processed.fact_dau_snapshot
         where date >= (
                        select start_date
                        from ffs.processed.tmp_start_date
                     )
      )t
left join processed.dim_country c on c.country_code=t.country_code
where t.row = 1;
        
-- create temp table to get the revenue for each user till now + conversion
create temp table temp_user_last_payment as
select u.user_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
from temp_dim_user_latest u
join processed.fact_revenue r on r.user_key=u.user_key
group by 1;

update temp_dim_user_latest
set total_revenue_usd = t.revenue,
    conversion_ts = t.conversion_ts
from temp_user_last_payment t
where temp_dim_user_latest.user_key = t.user_key;

-- update vip_level
create temp table temp_vip_level as
select user_key, max(vip_level) as vip_level 
from
  (SELECT md5(app||uid) as user_key, ts, 
    json_extract_path_text (properties,'vip_level')::int as vip_level 
  FROM ffs.public.events_raw where event='login'
  and ts >= (
                              select start_date
                              from ffs.processed.tmp_start_date
                           )
  and json_extract_path_text (properties,'vip_level')!='')
group by 1;

update temp_dim_user_latest
set vip_level = t.vip_level
from temp_vip_level t
where temp_dim_user_latest.user_key = t.user_key;

-- update install source
update temp_dim_user_latest
set install_source = f.install_source,
    campaign = f.campaign::varchar(100),
    sub_publisher =  f.sub_publisher::varchar(500),
    creative_id =  f.creative_id::varchar(100)
from processed.fact_user_install_source f
where temp_dim_user_latest.user_key = f.user_key;

-- create temp table temp_user_last_login
create temp table temp_user_last_login as
select user_key, max(ts_start) as last_login_ts
from processed.fact_session
group by 1;


update temp_dim_user_latest
set last_login_ts = t.last_login_ts
from temp_user_last_login t
where temp_dim_user_latest.user_key = t.user_key;

-- delete old user status in dim_user
delete from processed.dim_user
where user_key in
(
 select user_key from temp_dim_user_latest
);

-- insert the new status of users
insert into processed.dim_user
(
  user_key
  ,app
  ,uid
  ,snsid
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,last_login_ts
  ,vip_level
)
select
  user_key
  ,app
  ,uid
  ,snsid
  ,install_ts
  ,install_date
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
  ,language
  ,birth_date
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,last_login_ts
  ,vip_level
from temp_dim_user_latest;

--FFS EAS table
create temp table payment_info as
select uid as snsid, 1 is_payer, 
min(paid_time) as conversion_ts, max(paid_time) as last_payment_ts, count(1) as payment_cnt
from
public.tbl_payments
where app='ffs.global.prod'
group by 1;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select 'ffs.global.prod' app,
    t.uid,
    t.snsid,
    t.username as user_name,
    nullif(t.email, 'NULL') as email,
    u.install_source,
    u.install_ts,
    nvl(t.language, u.language) as language,
    u.gender,
    t.level,
    nvl(p.is_payer, 0) as is_payer,
    p.conversion_ts,
    p.last_payment_ts,
    nvl(p.payment_cnt, 0) as payment_cnt,
    t.reward_points as rc,
    t.coins as coins,
    nvl(u.last_login_ts, t.logintime) as last_login_ts
from public.tbl_user_global t
left join payment_info p on t.snsid=p.snsid
left join processed.dim_user u on t.uid=u.uid and u.app='ffs.global.prod';
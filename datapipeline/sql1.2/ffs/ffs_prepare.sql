-- start date table
-- drop table ffs.processed.tmp_start_date;
-- create table ffs.processed.tmp_start_date as
-- select DATEADD(day, -7, CURRENT_DATE) as start_date;

-- update ffs.processed.tmp_start_date
-- set start_date = '2014-09-01';

update ffs.processed.tmp_start_date
set start_date = DATEADD(day, -3, CURRENT_DATE);

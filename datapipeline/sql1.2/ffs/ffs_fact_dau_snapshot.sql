--------------------------------------------------------------------------------------------------------------------------------------------
--FFS fact dau snapshot
--Version 1.2
--Author Robin
/**
Description:
This script is generate the dau snapshot tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_dau_snapshot
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading 
delete from processed.fact_dau_snapshot
where date >= (
                    select start_date
                    from ffs.processed.tmp_start_date
              );
                      
--------------create temp table to process latest 7 days data
create temp table tmp_dau_snapshot
(
  date                      DATE NOT NULL ENCODE DELTA,
  user_key                  VARCHAR(50) NOT NULL ENCODE LZO,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(1024) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_user               SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  BIGINT DEFAULT 0,
  rc_in                     BIGINT DEFAULT 0,
  coins_out                  BIGINT DEFAULT 0,
  rc_out                     BIGINT DEFAULT 0, 
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER
)
DISTKEY(user_key)
SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get daily users from raw_login and raw_payment
------------------------------------------------------------------------------------------------------------------------
insert into tmp_dau_snapshot (date, user_key, uid, snsid, app)
select distinct date_start, user_key, uid, snsid, app
from processed.fact_session
where date_start >= (select start_date from ffs.processed.tmp_start_date)
union
select distinct date, user_key, uid, snsid, app
from processed.fact_revenue
where date >= (select start_date from ffs.processed.tmp_start_date)
union
select distinct install_date, user_key, uid, snsid, app
from processed.dim_user
where install_date >= (select start_date from ffs.processed.tmp_start_date);

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get user's last login everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_daily_last_login as
select distinct
          date_start
         ,user_key 
         ,uid 
         ,snsid 
         ,first_value(install_ts ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as install_ts
         ,first_value(install_date ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as install_date
         ,first_value(app ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as app
         ,first_value(app_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as app_version
         ,first_value(level_start ignore nulls) OVER (partition by date_start, user_key order by ts_start asc
                                                  rows between unbounded preceding and unbounded following) as level_start
         ,first_value(level_end ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as level_end
         ,first_value(os ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as os
         ,first_value(os_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as os_version
         ,first_value(s.country_code ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as country_code
         ,first_value(language ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as language
         ,first_value(device ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as device
         ,first_value(browser ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as browser
         ,first_value(browser_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as browser_version
         ,first_value(ab_test ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                  rows between unbounded preceding and unbounded following) as ab_test
         ,first_value(ab_variant ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                                 rows between unbounded preceding and unbounded following) as ab_variant
from processed.fact_session s
where date_start >= (select start_date from ffs.processed.tmp_start_date);


------------------------------------------------------------------------------------------------------------------------
--Step 3. Get user's first payment time and revenue
------------------------------------------------------------------------------------------------------------------------
create temp table temp_user_payment as
select user_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
from processed.fact_revenue
group by 1;

------------------------------------------------------------------------------------------------------------------------
--Step 4. Get user's session count everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_session_cnt as
select date_start, user_key, count(1) as session_cnt
from processed.fact_session
where date_start >= (select start_date from ffs.processed.tmp_start_date)
group by 1,2;

------------------------------------------------------------------------------------------------------------------------
--Step 5. Get user's payment + levels everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_daily_payment as
select date
       ,user_key
       ,sum(usd) as revenue_usd
       ,count(1) as purchase_cnt
from processed.fact_revenue
where date >= (select start_date from ffs.processed.tmp_start_date)
group by 1,2;

create temp table tmp_user_rc_coins as
select trunc(ts) as date, md5(app||uid) as user_key,
  min(nullif(json_extract_path_text(properties,'level'),'')::int) as level_start,
  max(nullif(json_extract_path_text(properties,'level'),'')::int) as level_end,
  sum(nullif(json_extract_path_text(properties,'coins_in'),'')::bigint) coins_in, 
  sum(nullif(json_extract_path_text(properties,'coins_out'),'')::bigint) coins_out,
  sum(nullif(json_extract_path_text(properties,'rc_in'),'')::bigint) rc_in, 
  sum(nullif(json_extract_path_text(properties,'rc_out'),'')::bigint) rc_out
from public.events_raw 
where event in ('coins_transaction', 'rc_transaction')
and ts>=(select start_date from ffs.processed.tmp_start_date)
group by 1,2;


------------------------------------------------------------------------------------------------------------------------
--Step 6. Update user's info using last login info in each day
------------------------------------------------------------------------------------------------------------------------

-- get last level played by day
create temp table tmp_user_level as
select  d.date
       ,d.user_key
       ,max(l.level) as level_end
from tmp_dau_snapshot d
join processed.fact_level_up l on d.user_key=l.user_key
where d.date >= (select start_date from processed.tmp_start_date) and ((d.date between l.date_start and l.date_end) or (l.date_end is null and d.date>=l.date_start))
group by 1,2;

update tmp_dau_snapshot
set install_date = t.install_date,
    install_ts = t.install_ts,
    level_start = t.level_start,
    level_end = t.level_end,
    device = t.device,
    country_code = t.country_code,
    language = t.language,
    browser = t.browser,
    browser_version = t.browser_version,
    os = t.os,
    os_version = t.os_version,
    ab_test=t.ab_test,
    ab_variant=t.ab_variant
from tmp_user_daily_last_login t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date_start;

update tmp_dau_snapshot
set level_end=l.level_end
from tmp_user_level l
where tmp_dau_snapshot.user_key = l.user_key and
      tmp_dau_snapshot.date = l.date;

update tmp_dau_snapshot
set level_end=level_start
where level_end is null;


------------------------------------------------------------------------------------------------------------------------
--Step 7. Update rc/coins change, level_start, level_end using events_raw level
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set coins_in = t.coins_in,
    coins_out = t.coins_out,
  rc_in = t.rc_in,
  rc_out = t.rc_out
from
tmp_user_rc_coins t
where tmp_dau_snapshot.date = t.date and 
 tmp_dau_snapshot.user_key = t.user_key;

update tmp_dau_snapshot
set level_end=t.level_end
from tmp_user_rc_coins t
where tmp_dau_snapshot.date = t.date and 
 tmp_dau_snapshot.user_key = t.user_key
 and t.level_end>tmp_dau_snapshot.level_end;

update tmp_dau_snapshot
set level_start=t.level_start
from tmp_user_rc_coins t
where tmp_dau_snapshot.date = t.date and 
 tmp_dau_snapshot.user_key = t.user_key
 and t.level_start<tmp_dau_snapshot.level_start;

------------------------------------------------------------------------------------------------------------------------
--Step 8. Update user is a payer or not and conversion
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set is_payer = 1
from temp_user_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date >= trunc(t.conversion_ts);

update tmp_dau_snapshot
set is_converted_today = 1
from temp_user_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = trunc(t.conversion_ts);

------------------------------------------------------------------------------------------------------------------------
--Step 9. Update session count
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set session_cnt = t.session_cnt
from tmp_user_session_cnt t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date_start;

------------------------------------------------------------------------------------------------------------------------
--Step 10. Update payment metrics
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set revenue_usd = t.revenue_usd,
    purchase_cnt=t.purchase_cnt
from tmp_user_daily_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 11.
--a. Create table to get the latest day's data
--b. Use latest day's data to update the missing user's info
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_last_day as
select t.*
from
(select *, row_number() OVER (partition by user_key order by date desc) as row
from tmp_dau_snapshot ds
where ds.install_date is not null) t
where row = 1;

update tmp_dau_snapshot
set install_ts = t.install_ts,
    install_date = t.install_date,
    device = t.device,
    country_code = t.country_code,
    language = t.language,
    browser = t.browser,
    browser_version = t.browser_version,
    os = t.os,
    os_version = t.os_version
from tmp_user_last_day t
where tmp_dau_snapshot.install_ts is null and tmp_dau_snapshot.user_key = t.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 12. Update is new install or not
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set is_new_user = 1
where date = install_date;

------------------------------------------------------------------------------------------------------------------------
--Step 13. Normalize os
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set os = 'Android'
where app in ('ffs.amazon.prod', 'ffs.tango.prod');

update tmp_dau_snapshot
set os = 'WP'
where app = 'ffs.wp.prod';

update tmp_dau_snapshot
set os = case when lower(right(os, 3)) = 'ios' Then 'iOS'
              when lower(right(os, 7)) = 'android' Then 'Android'
              when lower(right(os, 3)) = '360' Then '360'
              when lower(right(os, 6)) = 'xiaomi' Then 'xiaomi'
              else ''
         end
where app = 'ffs.cn.prod';

update tmp_dau_snapshot
set os = case when lower(right(os, 3)) = 'ios' Then 'iOS'
              when lower(right(os, 7)) = 'android' Then 'Android'
              when lower(right(os, 2)) = 'wp' Then 'WP'
              else ''
         end
where app != 'ffs.cn.prod';

update tmp_dau_snapshot
set os = case when substring(device, 1, 2) = 'iP' Then 'iOS'else 'Android' end
where os = '';

------------------------------------------------------------------------------------------------------------------------
--Step 14. Join with country table to get the country name
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set country = c.country
from processed.dim_country c
where tmp_dau_snapshot.country_code = c.country_code;

------------------------------------------------------------------------------------------------------------------------
--Step 15. Remove the abnormal date after fix.
-- Reason:
-- If there are still some missing values for key dimensions, we can delete them if count is very small.
-- This makes tableau report very clean
-- We should be very careful to do this. We should check the data before deleting them.
------------------------------------------------------------------------------------------------------------------------
delete from tmp_dau_snapshot where install_date is null;
delete from tmp_dau_snapshot where date = CURRENT_DATE;

------------------------------------------------------------------------------------------------------------------------
--Step 16. Insert updated data into Big table
------------------------------------------------------------------------------------------------------------------------
insert into processed.fact_dau_snapshot
(
      date
      ,user_key
      ,app
      ,uid
      ,snsid
      ,app_version
      ,install_ts
      ,install_date
      ,level_start
      ,level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,is_new_user
      ,session_cnt
      ,coins_in
      ,rc_in
      ,coins_out
      ,rc_out
      ,revenue_usd
      ,purchase_cnt
      ,is_payer
      ,is_converted_today
      ,playtime_sec
)
select
      date
      ,user_key
      ,app
      ,uid
      ,snsid
      ,app_version
      ,install_ts
      ,install_date
      ,level_start
      ,level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,is_new_user
      ,session_cnt
      ,coins_in
      ,rc_in
      ,coins_out
      ,rc_out
      ,revenue_usd
      ,purchase_cnt
      ,is_payer
      ,is_converted_today
      ,playtime_sec
from tmp_dau_snapshot;

--------------------------------------------------------------------------------------------------------------------------
--update session length and session count using events_raw in fact_dau_snapshot (assume 10 min inactivity ends a session)
--------------------------------------------------------------------------------------------------------------------------
create temp table g as
(select trunc(session_start) as date, user_key, sum(datediff('second', session_start, ts_end)) as total_session_length, count(1) as sessions
from
(select user_key, session_start, max(ts) as ts_end
from
(select *, last_value(start_ts ignore nulls) over 
(partition by user_key order by ts rows between unbounded preceding and current row) as session_start from
(select *, case
  when datediff('second', ts_prev, ts)<600 then NULL
  else ts end as start_ts from
(select *, lag(ts,1) over (partition by user_key order by ts) as ts_prev from 
(select distinct md5(app||uid) as user_key, ts from public.events_raw where
ts >= (
                    select start_date
                    from ffs.processed.tmp_start_date
              )
)
)
)
)
group by 1,2)
group by 1,2);

update processed.fact_dau_snapshot 
set playtime_sec = g.total_session_length
--, session_cnt=g.sessions
from g
where processed.fact_dau_snapshot.date = g.date
and processed.fact_dau_snapshot.user_key = g.user_key;

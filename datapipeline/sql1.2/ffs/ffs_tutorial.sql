-- tutorial report
delete from processed.agg_tutorial
where install_date >=(
                select start_date
                from ffs.processed.tmp_start_date
             );

drop table if exists tmp_tutorial_step;
create temp table tmp_tutorial_step as
select
        MD5(app||uid) as user_key
        ,app
        ,uid
        ,snsid
		,s.tutorial_step
		,s.tutorial_step_desc
		,min(SUBSTRING(REGEXP_SUBSTR(properties, '"level":[0-9]+'),9)::integer) AS level
        ,min(ts) as ts
from events_raw e
join processed.dim_tutorial_step s on SUBSTRING(REGEXP_SUBSTR(properties, '"quest_ID":[0-9]+'),12) =s.quest_id and SUBSTRING(REGEXP_SUBSTR(properties, '"task_id":[0-9]+'),11)=s.task_id
where
    trunc(install_ts) >=(select start_date from ffs.processed.tmp_start_date) and
    event = 'QuestComplete' and
    SUBSTRING(REGEXP_SUBSTR(properties, '"quest_ID":[0-9]+'),12) in('90001','90002','90003','90004','90005','99000','99001','99002')
group by 1,2,3,4,5,6;

update processed.dim_app_version
set date_end=(select max(date) from processed.fact_dau_snapshot)+1
from (select app,os,max(app_version) as app_version from processed.dim_app_version group by 1,2) t
where t.app=dim_app_version.app and t.os=dim_app_version.os and t.app_version=dim_app_version.app_version;

insert into processed.agg_tutorial
select
     u.install_date
    ,u.app
    ,a.app_version
    ,u.os
    ,u.os_version
    ,u.country
    ,u.device
    ,u.install_source
    ,u.browser
    ,u.browser_version
    ,u.language
    ,t.level
    ,t.tutorial_step as step
    ,t.tutorial_step_desc as step_desc
    ,datediff('day',u.install_date,t.ts) as days_since_install_date_cnt
    ,count(distinct u.user_key) as user_cnt
from  tmp_tutorial_step t
join processed.dim_user u on t.user_key = u.user_key
left join processed.dim_app_version a
    on  a.app=u.app and
        ((lower(u.os)='ios' and u.os=a.os) or (lower(a.os)!='ios')) and
        (install_date>=date_start and install_date<date_end)
where
    u.install_date>='2014-09-01' and
    u.install_date >=(select start_date from ffs.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
union
select
     u.install_date
    ,u.app
    ,a.app_version
    ,u.os
    ,u.os_version
    ,u.country
    ,u.device
    ,u.install_source
    ,u.browser
    ,u.browser_version
    ,u.language
    ,1
    ,0 as step
    ,'Install' as step_desc
    ,0
    ,count(distinct u.user_key)
from  processed.dim_user u
left join processed.dim_app_version a
    on  a.app=u.app and
        ((lower(u.os)='ios' and u.os=a.os) or (lower(a.os)!='ios')) and
        (install_date>=date_start and install_date<date_end)
where
    u.install_date>='2014-09-01' and
    u.install_date >=(select start_date from ffs.processed.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15;

delete from processed.tutorial_adhoc
where install_date >=(
                select start_date
                from ffs.processed.tmp_start_date
             );

insert into processed.tutorial_adhoc
select
    trunc(e.install_ts) as install_date,
    s.tutorial_step,
    s.tutorial_step_desc,
    SUBSTRING(REGEXP_SUBSTR(properties, '"level":[0-9]+'),9)::integer AS level,
    datediff('day',e.install_ts,e.ts) as days,
    count(distinct md5(app||uid)) as users
from events_raw e
join processed.dim_tutorial_step s on SUBSTRING(REGEXP_SUBSTR(properties, '"quest_ID":[0-9]+'),12) =s.quest_id and SUBSTRING(REGEXP_SUBSTR(properties, '"task_id":[0-9]+'),11)=s.task_id
where
    trunc(install_ts)>='2014-09-01' and
    trunc(install_ts)  >=(select start_date from ffs.processed.tmp_start_date) and
    event = 'QuestComplete' and
   SUBSTRING(REGEXP_SUBSTR(properties, '"quest_ID":[0-9]+'),12) in('90001','90002','90003','90004','90005','99000','99001','99002')
group by 1,2,3,4,5;
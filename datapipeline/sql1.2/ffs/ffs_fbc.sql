--fbc related tables
delete from processed.agg_fbc_apply                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );

insert into processed.agg_fbc_apply
select date, app, event, country, is_payer, level, count(1), count(distinct user_key) as user_count from
(select u.user_key, event, r.app, country, is_payer, 
nullif(json_extract_path_text(properties,'level'),'')::int as level, trunc(ts) as date from 
public.events_raw r, processed.dim_user u
where r.app = u.app and r.uid = u.uid
and event = 'fbc_apply' and r.ts>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
)
group by 1,2,3,4,5,6;

delete from processed.agg_fbc_submit                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );

insert into processed.agg_fbc_submit 
select date, app, event, country, is_payer, level, sum(charm_point) as charm_point, count(1), count(distinct user_key) as user_count from 
(select u.user_key, event, r.app, country, is_payer, 
nullif(json_extract_path_text(properties,'level'),'')::int as level, 
trunc(ts) as date, 
nullif(json_extract_path_text(properties,'charm_point'),'')::int as charm_point 
from public.events_raw r, processed.dim_user u
where r.app = u.app and r.uid = u.uid
and event = 'fbc_submit' and r.ts>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
)
group by 1,2,3,4,5,6;

delete from processed.agg_fbc_vote_action                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );

insert into processed.agg_fbc_vote_action
select date, app, event, country, is_payer, level, location, action, count(1), count(distinct user_key) as user_count from 
(select u.user_key, event, r.app, country, is_payer, nullif(json_extract_path_text(properties,'level'),'')::int as level, 
trunc(ts) as date, 
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action
from public.events_raw r, processed.dim_user u
where r.app = u.app and r.uid = u.uid
and event = 'fbc_vote_action' and r.ts>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
)
group by 1,2,3,4,5,6,7,8;


drop table if exists processed.fbc_start_dates;
create table processed.fbc_start_dates as
select date, app, c, row_number() over (partition by app order by date) as cnum from
(select a.*, nvl(b.c2, 1) as c2, c*1.0/nvl(b.c2, 1) as r from
(SELECT date, app, sum(count) as c FROM processed.agg_fbc_apply
where date>='2015-08-10'
group by 1,2) a
left join
(SELECT dateadd('day', 1, date) as date, app, sum(count) as c2 FROM processed.agg_fbc_apply
where date>='2015-08-10'
group by 1,2
order by 2,1) b
on a.date = b.date
and a.app=b.app
order by app, date)
where r>50;

delete from processed.agg_fbc_vote_collect                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );

insert into processed.agg_fbc_vote_collect
select date, app, event, country, is_payer, level,
location, sum(ticket) as ticket, count(1), count(distinct user_key) as user_count from
(select u.user_key, event, r.app, country, is_payer, nullif(json_extract_path_text(properties,'level'),'')::int as level, 
trunc(ts) as date, 
json_extract_path_text(properties,'location') as location,
nullif(json_extract_path_text(properties,'ticket'),'')::int as ticket
from public.events_raw r, processed.dim_user u
where r.app = u.app and r.uid = u.uid
and event = 'fbc_vote_collect' and r.ts>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
)
group by 1,2,3,4,5,6,7;

drop table if exists processed.agg_cum_fbc_apply;
create table processed.agg_cum_fbc_apply as
with c as
(select a.*, max(b.cnum) as cnum from
(select distinct date, app 
from processed.agg_fbc_apply) a
join 
processed.fbc_start_dates b
on a.date>=b.date
and a.app = b.app
group by 1,2
order by 2,1),
d as
(select a.*, c.cnum from 
processed.agg_fbc_apply a
join c
on a.date=c.date and a.app=c.app),
e as
(select cumdate as date, app, event, country, is_payer, level, sum(count) as cumcount from
(select d.*, c.date as cumdate from
d 
join c
on c.date>=d.date
and c.app=d.app
and c.cnum=d.cnum)
group by 1,2,3,4,5,6
)
select * from e;

delete from processed.agg_fbc_vote_action_no_location                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );

insert into processed.agg_fbc_vote_action_no_location
select date, app, event, country, is_payer, level, action, count(1), count(distinct user_key) as user_count from 
(select u.user_key, event, r.app, country, is_payer, nullif(json_extract_path_text(properties,'level'),'')::int as level, 
trunc(ts) as date, 
--json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action
from public.events_raw r, processed.dim_user u
where r.app = u.app and r.uid = u.uid
and event = 'fbc_vote_action' and r.ts>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
)
group by 1,2,3,4,5,6,7;

delete from processed.agg_fbc_vote_collect_no_location                                                      
where date >= (                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              );

insert into processed.agg_fbc_vote_collect_no_location
select date, app, event, country, is_payer, level, sum(ticket) as ticket, count(1), count(distinct user_key) as user_count from
(select u.user_key, event, r.app, country, is_payer, nullif(json_extract_path_text(properties,'level'),'')::int as level, 
trunc(ts) as date, 
--json_extract_path_text(properties,'location') as location,
nullif(json_extract_path_text(properties,'ticket'),'')::int as ticket
from public.events_raw r, processed.dim_user u
where r.app = u.app and r.uid = u.uid
and event = 'fbc_vote_collect' and r.ts>=(                                                                               
                    select start_date                                                         
                    from processed.tmp_start_date                                          
              )
)
group by 1,2,3,4,5,6;

--agg_coins_transaction
delete from processed.agg_coins_transaction
where datediff('day', date, current_date)<=2;

insert into processed.agg_coins_transaction
select trunc(ts) as date, app, action,
case action
  when 'add_plant' then json_extract_path_text(detail, 'plant_id')
  when 'friend_fertilize' then json_extract_path_text(detail, 'plant_id')
  when 'useLuckypackage' then json_extract_path_text(detail, 'luckypackage_id')
  when 'water_plants' then ''
  when 'plow_soils' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(detail, 'soils'), 0), 'id')
  when 'sell_partial_storage' then json_extract_path_text(json_extract_array_element_text(json_extract_path_text(detail, 'sell_list'), 0), 'id')
  else json_extract_path_text(detail, 'id')
  end as item_id, level,
  sum(coins_in) as coins_in,
  sum(coins_out) as coins_out,
  count(1)
  from
(select *, json_extract_path_text(properties,'action') as action,
nullif(json_extract_path_text(properties,'level'),'')::int as level, 
nullif(json_extract_path_text(properties,'coins_in'),'')::bigint coins_in, 
nullif(json_extract_path_text(properties,'coins_out'),'')::bigint coins_out,
replace(replace(json_extract_path_text(properties,'action_detail'), '\\"', '"'), '\\\\','\\') as detail
from public.events_raw where event='coins_transaction'
and datediff('day', ts, current_date)<=2)
group by 1,2,3,4,5;

--agg_kitchen
/*delete from processed.agg_kitchen
where datediff('day', date, current_date)<=2;

insert into processed.agg_kitchen
select *, md5(app||uid) as user_key from
(select trunc(ts) as date, app, uid, snsid, utensil, item_id, level, count(1), min(power_left) as power_left from 
(select app, ts, uid, snsid, 
json_extract_path_text(properties,'utensil')::int as utensil,
json_extract_path_text(properties,'item_ID') as item_id,
json_extract_path_text(properties,'level')::int as level,
json_extract_path_text(properties,'power_left')::int as power_left
from public.events_raw
where event='Kitchen' and datediff('day', ts, current_date)<=2)
group by 1,2,3,4,5,6,7);*/
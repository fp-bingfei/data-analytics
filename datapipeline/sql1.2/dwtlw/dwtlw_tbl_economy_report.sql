-- TODO: resource out
delete from  processed.agg_resource_out
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into processed.agg_resource_out
(
    date
    ,app
    ,server
    ,install_date
    ,install_source
    ,device
    ,country
    ,os
    ,language
    ,is_payer
    ,level
    ,vip_level
    ,event
    ,action

    ,player_food
    ,player_water
    ,player_metal
    ,player_fuel
    ,player_money
    ,player_gold
    ,spent_food
    ,spent_water
    ,spent_metal
    ,spent_fuel
    ,spent_money
    ,spent_gold
    ,users
)
select
    r.date
    ,r.app
    ,r.server
    ,u.install_date
    ,u.install_source
    ,u.device
    ,u.country
    ,u.os
    ,u.language
    ,u.is_payer
    ,r.level
    ,r.vip_level
    ,r.event
    ,r.action

    ,sum(player_food) as player_food
    ,sum(player_water) as player_water
    ,sum(player_metal) as player_metal
    ,sum(player_fuel) as player_fuel
    ,sum(player_money) as player_money
    ,sum(player_gold) as player_gold
    ,sum(spent_food) as spent_food
    ,sum(spent_water) as spent_water
    ,sum(spent_metal) as spent_metal
    ,sum(spent_fuel) as spent_fuel
    ,sum(spent_money) as spent_money
    ,sum(spent_gold) as spent_gold
    ,count(distinct r.user_key) as users
from processed.fact_resource_out r
    join processed.dim_user u on r.user_key = u.user_key
where r.spent_gold > 0 and
      r.date >= (
                    select raw_data_start_date
                    from   dwtlw.processed.tmp_start_date
                )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

-- TODO: resource in
delete from  processed.agg_resource_in
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into processed.agg_resource_in
(
    date
    ,app
    ,server
    ,install_date
    ,install_source
    ,device
    ,country
    ,os
    ,language
    ,is_payer
    ,level
    ,vip_level
    ,event
    ,action

    ,player_food
    ,player_water
    ,player_metal
    ,player_fuel
    ,player_money
    ,player_gold
    ,receive_food
    ,receive_water
    ,receive_metal
    ,receive_fuel
    ,receive_money
    ,receive_gold
    ,users
)
select
    r.date
    ,r.app
    ,r.server
    ,u.install_date
    ,u.install_source
    ,u.device
    ,u.country
    ,u.os
    ,u.language
    ,u.is_payer
    ,r.level
    ,r.vip_level
    ,r.event
    ,r.action

    ,sum(player_food) as player_food
    ,sum(player_water) as player_water
    ,sum(player_metal) as player_metal
    ,sum(player_fuel) as player_fuel
    ,sum(player_money) as player_money
    ,sum(player_gold) as player_gold
    ,sum(receive_food) as receive_food
    ,sum(receive_water) as receive_water
    ,sum(receive_metal) as receive_metal
    ,sum(receive_fuel) as receive_fuel
    ,sum(receive_money) as receive_money
    ,sum(receive_gold) as receive_gold
    ,count(distinct r.user_key) as users
from processed.fact_resource_in r
    join processed.dim_user u on r.user_key = u.user_key
where r.receive_gold > 0 and
      r.date >= (
                    select raw_data_start_date
                    from   dwtlw.processed.tmp_start_date
                )
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

-- TODO: headquarter level distribution
drop table if exists headquarter_day_last_row;
create table headquarter_day_last_row as
select date, ts, user_key, app, uid, server, level, action, building_level, building_type
from
(select date, ts, user_key, app, uid, server, level, action, building_level, building_type,
    row_number() over (partition by date, uid, server order by ts desc) as row_number
from processed.fact_city
where building_type = '1') t
where t.row_number = 1;

drop table if exists headquarter_day_level_cube;
create table headquarter_day_level_cube as
select d.date as record_date, h.*
from
(select distinct date from processed.fact_dau_snapshot) d
    left join headquarter_day_last_row h on d.date >= h.date;

drop table if exists headquarter_day_level;
create table headquarter_day_level as
select *
from
(select *, row_number() over (partition by record_date, uid, server order by date desc) as row_number
from headquarter_day_level_cube) t
where t.row_number = 1;

drop table if exists processed.agg_headquarter_day_level;
create table processed.agg_headquarter_day_level as
select
    l.record_date as date
    ,l.app
    ,l.server
    ,l.building_level
    ,u.install_date
    ,u.os
    ,u.country
    ,u.install_source
    ,u.is_payer
    ,u.vip_level
    ,count(distinct l.uid) as users
from headquarter_day_level l
    join processed.dim_user u on l.uid = u.uid
group by 1,2,3,4,5,6,7,8,9,10;

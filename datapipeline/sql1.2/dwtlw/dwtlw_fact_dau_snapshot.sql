--------------------------------------------------------------------------------------------------------------------------------------------
--The DEAD WALK THE LAST WAR fact dau snapshot
--Version 1.2
--Author Robin
/**
Description:
This script is generate the dau snapshot tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------
-- processed.fact_dau_snapshot
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading 
delete from processed.fact_dau_snapshot
where date >= (
                    select start_date
                    from dwtlw.processed.tmp_start_date
              );
                      
--------------create temp table to process latest 7 days data
create temp table tmp_dau_snapshot
(
  date                      DATE NOT NULL ENCODE DELTA,
  user_key                  VARCHAR(50) NOT NULL ENCODE LZO,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  vip_level_end             VARCHAR(16) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(1024) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_user               SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER,
  create_league_cnt         INTEGER DEFAULT 0,
  join_league_cnt           INTEGER DEFAULT 0,
  current_league            VARCHAR(32) ENCODE BYTEDICT,
  dailybonusclaimed_cnt     INTEGER DEFAULT 0
)
DISTKEY(user_key)
SORTKEY(date, user_key, app, uid, install_date, country_code);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get daily users from raw_login and raw_payment
------------------------------------------------------------------------------------------------------------------------
insert into tmp_dau_snapshot (date, user_key, uid, app)
select distinct date_start, user_key, uid, app
from processed.fact_session
where date_start >= (select start_date from dwtlw.processed.tmp_start_date)
union
select distinct date, user_key, uid, app
from processed.fact_revenue
where date >= (select start_date from dwtlw.processed.tmp_start_date);

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get user's last login everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_daily_last_login as
select distinct
     date_start
    ,user_key
    ,uid
    ,first_value(server ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as server
    ,min(install_ts ignore nulls) OVER (partition by user_key) as install_ts
    ,min(install_date ignore nulls) OVER (partition by user_key) as install_date
    ,first_value(app ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as app
    ,first_value(app_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as app_version
    ,first_value(level_start ignore nulls) OVER (partition by date_start, user_key order by ts_start asc
                                             rows between unbounded preceding and unbounded following) as level_start
    ,first_value(level_end ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as level_end
    ,first_value(vip_level_start ignore nulls) OVER (partition by date_start, user_key order by ts_start asc
                                             rows between unbounded preceding and unbounded following) as vip_level_start
    ,first_value(vip_level_end ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as vip_level_end
    ,first_value(os ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as os
    ,first_value(os_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as os_version
    ,first_value(s.country_code ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as country_code
    ,first_value(language ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as language
    ,first_value(device ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as device
    ,first_value(browser ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as browser
    ,first_value(browser_version ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as browser_version
    ,first_value(ab_test ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                             rows between unbounded preceding and unbounded following) as ab_test
    ,first_value(ab_variant ignore nulls) OVER (partition by date_start, user_key order by ts_start desc
                                            rows between unbounded preceding and unbounded following) as ab_variant
from processed.fact_session s
where date_start >= (select start_date from dwtlw.processed.tmp_start_date);


------------------------------------------------------------------------------------------------------------------------
--Step 3. Get user's first payment time and revenue
------------------------------------------------------------------------------------------------------------------------
create temp table temp_user_payment as
select user_key
       ,min(ts) as conversion_ts
       ,sum(usd) as revenue
from processed.fact_revenue
group by 1;

------------------------------------------------------------------------------------------------------------------------
--Step 4. Get user's session count everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_session_cnt as
select date_start, user_key, count(1) as session_cnt
from processed.fact_session
where date_start >= (select start_date from dwtlw.processed.tmp_start_date)
group by 1,2;

------------------------------------------------------------------------------------------------------------------------
--Step 5. Get user's payment everyday
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_daily_payment as
select date
       ,user_key
       ,max(server) as server
       ,min(level) as level_start
       ,max(level) as level_end
       ,min(vip_level) as vip_level_start
       ,max(vip_level) as vip_level_end
       ,sum(coins_in) as coins_in
       ,sum(rc_in) as rc_in
       ,sum(usd) as revenue_usd
       ,count(1) as purchase_cnt
from processed.fact_revenue
where date >= (select start_date from dwtlw.processed.tmp_start_date)
group by 1,2;

drop table if exists tmp_rc_in;
create temp table tmp_rc_in as
select date, user_key, sum(receive_gold) as rc_in
from processed.fact_resource_in
group by 1,2;

update tmp_user_daily_payment
set rc_in = r.rc_in
from tmp_rc_in r
where tmp_user_daily_payment.date = r.date and
      tmp_user_daily_payment.user_key = r.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 6. Update user's info using last login info in each day
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set server = t.server,
    install_date = t.install_date,
    install_ts = t.install_ts,
    level_start = t.level_start,
    level_end = t.level_end,
    vip_level_start = t.vip_level_start,
    vip_level_end = t.vip_level_end,
    device = t.device,
    country_code = t.country_code,
    language = t.language,
    browser = t.browser,
    browser_version = t.browser_version,
    os = t.os,
    os_version = t.os_version,
    ab_test=t.ab_test,
    ab_variant=t.ab_variant
from tmp_user_daily_last_login t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date_start;

------------------------------------------------------------------------------------------------------------------------
--Step 7. Update level_start, level_end, vip_level_start and vip_level_end using payment data
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set level_start = t.level_start
from tmp_user_daily_payment t
where tmp_dau_snapshot.level_start is null and
      tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

update tmp_dau_snapshot
set level_end = t.level_end
from tmp_user_daily_payment t
where (tmp_dau_snapshot.level_end is null or tmp_dau_snapshot.level_end < t.level_end) and
      tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

update tmp_dau_snapshot
set vip_level_start = t.vip_level_start
from tmp_user_daily_payment t
where tmp_dau_snapshot.vip_level_start is null and
      tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

update tmp_dau_snapshot
set vip_level_end = t.vip_level_end
from tmp_user_daily_payment t
where (tmp_dau_snapshot.vip_level_end is null or tmp_dau_snapshot.vip_level_end < t.vip_level_end) and
      tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

update tmp_dau_snapshot
set server = t.server
from tmp_user_daily_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 8. Update user is a payer or not and conversion
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set is_payer = 1
from temp_user_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date >= trunc(t.conversion_ts);

update tmp_dau_snapshot
set is_converted_today = 1
from temp_user_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = trunc(t.conversion_ts);

------------------------------------------------------------------------------------------------------------------------
--Step 9. Update session count
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set session_cnt = t.session_cnt
from tmp_user_session_cnt t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date_start;

------------------------------------------------------------------------------------------------------------------------
--Step 10. Update payment metrics
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set revenue_usd = t.revenue_usd,
    purchase_cnt=t.purchase_cnt
from tmp_user_daily_payment t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 11. Update create_league_cnt
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_create_league as
select date, user_key, count(distinct alliance_id) as create_league_cnt
from processed.fact_league
where date >= (select start_date from dwtlw.processed.tmp_start_date) and
      action = 'create'
group by 1,2;

update tmp_dau_snapshot
set create_league_cnt = t.create_league_cnt
from tmp_create_league t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 12. Update join_league_cnt
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_join_league as
select date, user_key, count(distinct alliance_id) as join_league_cnt
from processed.fact_league
where date >= (select start_date from dwtlw.processed.tmp_start_date) and
      action = 'join'
group by 1,2;

update tmp_dau_snapshot
set join_league_cnt = t.join_league_cnt
from tmp_join_league t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 13. Update current_league
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_join_leave_league_cube as
select d.date, d.user_key, l.ts, l.action, l.alliance_id
from tmp_dau_snapshot d
    join processed.fact_league l on d.user_key = l.user_key and d.date >= l.date
where d.date >= (select start_date from dwtlw.processed.tmp_start_date) and l.action in ('join', 'leave');

create temp table tmp_join_leave_league_date_status as
select date, user_key, ts, action, alliance_id
from
(select date, user_key, ts, action, alliance_id, row_number() over (partition by date, user_key order by ts desc) as row_number
from tmp_join_leave_league_cube) t
where t.row_number = 1;

update tmp_dau_snapshot
set current_league = t.alliance_id
from tmp_join_leave_league_date_status t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date and
      t.action = 'join';

------------------------------------------------------------------------------------------------------------------------
--Step 14. Update dailybonusclaimed_cnt
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_dailybonusclaimed_cnt as
select date, user_key, count(1) as dailybonusclaimed_cnt
from processed.fact_gifts
where date >= (select start_date from dwtlw.processed.tmp_start_date) and
      action = 'dailybonusclaimed'
group by 1,2;

update tmp_dau_snapshot
set dailybonusclaimed_cnt = t.dailybonusclaimed_cnt
from tmp_dailybonusclaimed_cnt t
where tmp_dau_snapshot.user_key = t.user_key and
      tmp_dau_snapshot.date = t.date;

------------------------------------------------------------------------------------------------------------------------
--Step 15.
--a. Create table to get the latest day's data
--b. Use latest day's data to update the missing user's info
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_last_day as
select t.*
from
(select *, row_number() OVER (partition by user_key order by date desc) as row
from tmp_dau_snapshot ds
where ds.install_date is not null) t
where row = 1;

update tmp_dau_snapshot
set install_ts = t.install_ts,
    install_date = t.install_date,
    device = t.device,
    country_code = t.country_code,
    language = t.language,
    browser = t.browser,
    browser_version = t.browser_version,
    os = t.os,
    os_version = t.os_version
from tmp_user_last_day t
where tmp_dau_snapshot.install_ts is null and tmp_dau_snapshot.user_key = t.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 16. Normalize os
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set os = case when lower(os) = 'ios' Then 'iOS'
              when lower(os) = 'android' Then 'Android'
              else ''
         end;

update tmp_dau_snapshot
set os = case when substring(device, 1, 2) = 'iP' Then 'iOS' else 'Android' end
where os = '';

------------------------------------------------------------------------------------------------------------------------
--Step 17. Join with country table to get the country name
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set country = c.country
from processed.dim_country c
where tmp_dau_snapshot.country_code = c.country_code;

update tmp_dau_snapshot
set country = 'Unknown'
where country is null;

------------------------------------------------------------------------------------------------------------------------
--Step 18. Remove the abnormal date after fix.
-- Reason:
-- If there are still some missing values for key dimensions, we can delete them if count is very small.
-- This makes tableau report very clean
-- We should be very careful to do this. We should check the data before deleting them.
------------------------------------------------------------------------------------------------------------------------
delete from tmp_dau_snapshot where install_ts is null;
delete from tmp_dau_snapshot where date = CURRENT_DATE;

------------------------------------------------------------------------------------------------------------------------
--Step 19. Update install_ts and install_date in fact_dau_snapshot table using dim_user table.
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set install_ts = u.install_ts,
    install_date = u.install_date
from processed.dim_user u
where tmp_dau_snapshot.user_key = u.user_key and tmp_dau_snapshot.install_ts > u.install_ts;

------------------------------------------------------------------------------------------------------------------------
--Step 20. Update is new install or not
------------------------------------------------------------------------------------------------------------------------
update tmp_dau_snapshot
set is_new_user = 1
where date = install_date;

------------------------------------------------------------------------------------------------------------------------
--Step 21. Update level_start and level_end
------------------------------------------------------------------------------------------------------------------------
-- update tmp_dau_snapshot
-- set level_end = max_level
-- from processed.daily_level l
-- where tmp_dau_snapshot.date = l.date and
--     tmp_dau_snapshot.uid = l.uid and
--     tmp_dau_snapshot.server = l.server and
--     tmp_dau_snapshot.level_end < l.max_level;
--
-- update tmp_dau_snapshot
-- set level_end = min_level
-- from processed.daily_level l
-- where tmp_dau_snapshot.date + 1 = l.date and
--     tmp_dau_snapshot.uid = l.uid and
--     tmp_dau_snapshot.server = l.server and
--     tmp_dau_snapshot.level_end < l.min_level;

------------------------------------------------------------------------------------------------------------------------
--Step 22. Insert updated data into Big table
------------------------------------------------------------------------------------------------------------------------
insert into processed.fact_dau_snapshot
(
      date
      ,user_key
      ,app
      ,uid
      ,server
      ,app_version
      ,install_ts
      ,install_date
      ,level_start
      ,level_end
      ,vip_level_start
      ,vip_level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,is_new_user
      ,session_cnt
      ,coins_in
      ,rc_in
      ,revenue_usd
      ,purchase_cnt
      ,is_payer
      ,is_converted_today
      ,playtime_sec
      ,create_league_cnt
      ,join_league_cnt
      ,current_league
      ,dailybonusclaimed_cnt
)
select
      date
      ,user_key
      ,app
      ,uid
      ,server
      ,app_version
      ,install_ts
      ,install_date
      ,level_start
      ,level_end
      ,vip_level_start
      ,vip_level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,is_new_user
      ,session_cnt
      ,coins_in
      ,rc_in
      ,revenue_usd
      ,purchase_cnt
      ,is_payer
      ,is_converted_today
      ,playtime_sec
      ,create_league_cnt
      ,join_league_cnt
      ,current_league
      ,dailybonusclaimed_cnt
from tmp_dau_snapshot;

------------------------------------------------------------------------------------------------------------------------
--Step 23. Insert user's record back to fact_dau_snapshot on install date if we miss their data on install date
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_dau_snapshot_missing_installs as
select *
from
(select *, row_number() over (partition by user_key order by date) as rank
from processed.fact_dau_snapshot) t
where t.rank = 1 and t.date > t.install_date;

insert into processed.fact_dau_snapshot
(
      date
      ,user_key
      ,app
      ,uid
      ,server
      ,app_version
      ,install_ts
      ,install_date
      ,level_start
      ,level_end
      ,vip_level_start
      ,vip_level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,is_new_user
      ,session_cnt
      ,is_payer
      ,is_converted_today
)
select
      install_date as date
      ,user_key
      ,app
      ,uid
      ,server
      ,app_version
      ,install_ts
      ,install_date
      ,1 as level_start
      ,level_start as level_end
      ,'1' as vip_level_start
      ,vip_level_start as vip_level_end
      ,device
      ,device_alias
      ,browser
      ,browser_version
      ,country_code
      ,country
      ,os
      ,os_version
      ,language
      ,ab_test
      ,ab_variant
      ,1 as is_new_user
      ,1 as session_cnt
      ,is_payer
      ,0 as is_converted_today
from tmp_dau_snapshot_missing_installs;


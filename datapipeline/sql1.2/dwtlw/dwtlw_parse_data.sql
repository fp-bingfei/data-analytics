-- Dota history data

-- import data from S3 directly
-- drop table if exists processed.raw_data_s3;
-- create table processed.raw_data_s3
-- (
-- 	json_str          VARCHAR(20000) ENCODE LZO,
-- 	load_hour         TIMESTAMP ENCODE DELTA
-- );

drop table if exists processed.raw_data_s3_test;
create table processed.raw_data_s3_test
(
	json_str          VARCHAR(20000) ENCODE LZO
);

truncate table processed.raw_data_s3_test;

copy processed.raw_data_s3_test
from 's3://com.funplus.eshttp/dwtlw.global.prod/2015/05/24/'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
IGNOREBLANKLINES
GZIP
MAXERROR 1000;

drop table if exists processed.raw_data_s3_test_invalid;
create table processed.raw_data_s3_test_invalid
(
	json_str          VARCHAR(20000) ENCODE LZO
);

insert into processed.raw_data_s3_test_invalid
select *
from processed.raw_data_s3_test
where regexp_substr(json_str, '^\\\{.*\\\}$') = '';

delete
from processed.raw_data_s3_test
where regexp_substr(json_str, '^\\\{.*\\\}$') = '';

insert into processed.raw_data_s3_test_invalid
select *
from processed.raw_data_s3_test
where json_str like '%1411541374%';

delete
from processed.raw_data_s3_test
where json_str like '%1411541374%';

-- parsing data from json string
drop table if exists processed.events_raw_test;
CREATE TABLE processed.events_raw_test
(
	load_hour           TIMESTAMP ENCODE DELTA,
	event               VARCHAR(32) ENCODE BYTEDICT,
	date                DATE NOT NULL ENCODE DELTA,
	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
	app_id              VARCHAR(32) NOT NULL ENCODE BYTEDICT,
	user_id             INTEGER NOT NULL,
	gameserver_id       VARCHAR(16) ENCODE BYTEDICT,
	install_ts          TIMESTAMP ENCODE DELTA,
	country_code        VARCHAR(8) ENCODE BYTEDICT,
	level               SMALLINT,
	ip                  VARCHAR(16) ENCODE LZO,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	os_version          VARCHAR(64) ENCODE BYTEDICT,
	app_version         VARCHAR(32) ENCODE BYTEDICT,
	session_id          VARCHAR(32) ENCODE LZO,
	bi_version          VARCHAR(32) ENCODE BYTEDICT,
	idfa                VARCHAR(64) ENCODE LZO,
	mac_address         VARCHAR(32) ENCODE LZO,
	device              VARCHAR(64) ENCODE BYTEDICT,
	lang                VARCHAR(32) ENCODE BYTEDICT,
	json_str            VARCHAR(2048) ENCODE LZO,
	properties          VARCHAR(2048) ENCODE LZO,
	collections         VARCHAR(2048) ENCODE LZO,
	md5                 VARCHAR(32) ENCODE LZO
)
  DISTKEY(user_id)
  SORTKEY(load_hour, event, date, ts);

truncate table processed.events_raw_test;

insert into processed.events_raw_test
select
    null as load_hour
    ,json_extract_path_text(json_str, 'event') as event
    ,trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) as date
    ,dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str, 'app_id') as app_id
    ,CAST(json_extract_path_text(json_str, 'user_id') AS INTEGER) as user_id
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gameserver_id') as gameserver_id
    ,dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,null as country_code
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'level') AS INTEGER) as level
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'ip') as ip
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os') as os
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os_version') as os_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'app_version') as app_version
    ,json_extract_path_text(json_str, 'session_id') as session_id
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'idfa') as idfa
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'mac_address') as mac_address
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device') as device
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'lang') as lang
    ,json_str as json_str
    ,json_extract_path_text(json_str, 'properties') as properties
    ,json_extract_path_text(json_str, 'collections') as collections
    ,md5(json_str) as md5
from processed.raw_data_s3_test;
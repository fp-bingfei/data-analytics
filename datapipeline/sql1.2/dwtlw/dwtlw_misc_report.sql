-- TODO: Discrepancy between Adjust and BI
drop table if exists processed.adjust_bi_discrepancy;
create table processed.adjust_bi_discrepancy as
select
    t1.install_date
    ,cast(t1.install_date as VARCHAR) as install_date_str
    ,t1.install_source
    ,t1.campaign
    ,t1.sub_publisher
    ,t1.os
    ,t1.all_adjust_installs
    ,coalesce(t2.empty_user_id_installs, 0) as empty_user_id_installs
    ,coalesce(t3.in_bi_installs, 0) as in_bi_installs
    ,coalesce(t4.not_in_bi_installs, 0) as not_in_bi_installs
from
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id = 'com.bigkraken.thelastwar' then 'Android'
	    when app_id = '974915593' then 'iOS'
    end as os
    ,count(1) as all_adjust_installs
from unique_adjust
group by 1,2,3,4,5) t1
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id = 'com.bigkraken.thelastwar' then 'Android'
	    when app_id = '974915593' then 'iOS'
    end as os
    ,count(1) as empty_user_id_installs
from unique_adjust
where userid = ''
group by 1,2,3,4,5) t2
    on t1.install_source = t2.install_source and
       t1.install_date = t2.install_date and
       t1.campaign = t2.campaign and
       t1.sub_publisher = t2.sub_publisher and
       t1.os = t2.os
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id = 'com.bigkraken.thelastwar' then 'Android'
	    when app_id = '974915593' then 'iOS'
    end as os
    ,count(1) in_bi_installs
from unique_adjust a join processed.dim_user u on a.userid = u.uid
where a.userid != ''
group by 1,2,3,4,5) t3
    on t1.install_source = t3.install_source and
       t1.install_date = t3.install_date and
       t1.campaign = t3.campaign and
       t1.sub_publisher = t3.sub_publisher and
       t1.os = t3.os
left join
(select
    split_part(tracker_name, '::', 1) as install_source
    ,split_part(tracker_name, '::', 2) as campaign
    ,split_part(tracker_name, '::', 3) as sub_publisher
    ,trunc(ts) as install_date
    ,case
	    when app_id = 'com.bigkraken.thelastwar' then 'Android'
	    when app_id = '974915593' then 'iOS'
    end as os
    ,count(1) not_in_bi_installs
from unique_adjust a left join processed.dim_user u on a.userid = u.uid
where a.userid != '' and u.uid is null
group by 1,2,3,4,5) t4
    on t1.install_source = t4.install_source and
       t1.install_date = t4.install_date and
       t1.campaign = t4.campaign and
       t1.sub_publisher = t4.sub_publisher and
       t1.os = t4.os;

-- TODO: For EAS report
create temp table payment_info as
select app, uid, '' as snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from processed.fact_revenue
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    u.app
    ,u.uid
    ,u.os
    ,u.server
    ,'' as snsid
    ,'' as user_name
    ,'' as email
    ,'' as additional_email
    ,u.install_source
    ,u.install_ts
    ,u.language
    ,u.gender
    ,u.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,u.total_rc_in as rc
    ,0 as coins
    ,u.last_login_ts as last_login_ts
from processed.dim_user u
    left join payment_info p on u.app = p.app and u.uid = p.uid;

--processed.agg_pvp
delete from processed.agg_pvp where
    date>= (
                        select start_date
                        from dwtlw.processed.tmp_start_date
                     );

create temp table temp_pvp as
select ts as date, app, uid, server, level, 
nullif(json_extract_path_text(properties,'action'),'') as action,
nullif(json_extract_path_text(properties,'city_type'),'')::int as city_type,
nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 0), 'resource_name'),'') as spent_resource1_name
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 0), 'resource_amount'),'') as spent_resource1_amount
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 1), 'resource_name'),'') as spent_resource2_name
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 1), 'resource_amount'),'') as spent_resource2_amount
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 2), 'resource_name'),'') as spent_resource3_name
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 2), 'resource_amount'),'') as spent_resource3_amount
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 3), 'resource_name'),'') as spent_resource4_name
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 3), 'resource_amount'),'') as spent_resource4_amount
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 4), 'resource_name'),'') as spent_resource5_name
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 4), 'resource_amount'),'') as spent_resource5_amount
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 5), 'resource_name'),'') as spent_resource6_name
    ,nullif(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 5), 'resource_amount'),'') as spent_resource6_amount
from processed.events_raw a
where a.event = 'pvp'
and date>= (
                        select start_date
                        from dwtlw.processed.tmp_start_date
                     );

insert into processed.agg_pvp
select a.*
    ,u.install_date
    ,u.install_source
    ,u.device
    ,u.country
    ,u.os
    ,u.language
    ,u.is_payer
from
(select date, app, uid, server, level, action, city_type,
    case
        when spent_resource1_name = 'Food' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Food' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Food' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Food' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Food' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Food' then cast(spent_resource6_amount as bigint)
        else 0
    end as spent_food
    ,case
        when spent_resource1_name = 'Water' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Water' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Water' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Water' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Water' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Water' then cast(spent_resource6_amount as bigint)
        else 0
    end as spent_water
    ,case
        when spent_resource1_name = 'Metal' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Metal' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Metal' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Metal' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Metal' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Metal' then cast(spent_resource6_amount as bigint)
        else 0
    end as spent_metal
    ,case
        when spent_resource1_name = 'Fuel' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Fuel' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Fuel' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Fuel' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Fuel' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Fuel' then cast(spent_resource6_amount as bigint)
        else 0
    end as spent_fuel
    ,case
        when spent_resource1_name = 'Money' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Money' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Money' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Money' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Money' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Money' then cast(spent_resource6_amount as bigint)
        else 0
    end as spent_money
    ,case
        when spent_resource1_name = 'Gold' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Gold' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Gold' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Gold' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Gold' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Gold' then cast(spent_resource6_amount as bigint)
        else 0
    end as spent_gold
    from temp_pvp) a
    join
    processed.dim_user u on
    a.app = u.app and
    a.uid = u.uid and
    a.server = u.server;

--processed.agg_server_stats
--join dau_snapshot with dim_user to get install source
create temp table joined as
(select a.*
    ,u.install_source
from
dwtlw.processed.fact_dau_snapshot a
join processed.dim_user u
on a.uid = u.uid
and a.server = u.server
and a.app = u.app);

--distinct dates
create temp table dates as 
(select distinct date as join_date 
from joined);

--preliminary retention table
create temp table g as
(select install_date, server::int,
os, country, install_source,
datediff('days', install_date, date) as diff,
count(distinct uid) as u
FROM joined
group by 1,2,3,4,5,6);

--get d1 and d7 retention numbers
create temp table h as
(select a.*, b.u as u_1, c.u as u_7 from
(select install_date, server, os, country, install_source,
u from g where diff=0) a
left join
(select install_date, server, os, country, install_source,
u from g where diff=1) b
on a.install_date = b.install_date
and a.server = b.server
and a.os = b.os
and a.country = b.country
and a.install_source = b.install_source
left join
(select install_date, server, os, country, install_source,
u from g where diff=7) c
on a.install_date = c.install_date
and a.server = c.server
and a.os = c.os
and a.country = c.country
and a.install_source = c.install_source);

--dau and 3-day dau
create temp table dau_table as
(select a.*, b.dau_3 from
(SELECT date, server::int, os, country, install_source,
count(1) as dau, 
sum(is_new_user) as installs
FROM joined
group by 1,2,3,4,5) a
left join
(select join_date, server::int, os, country, install_source,
count(distinct uid) as dau_3 from
joined a
join
dates b
on (a.date+2>=b.join_date
and a.date<=b.join_date)
group by 1,2,3,4,5) b
on a.date = b.join_date
and a.server = b.server
and a.os = b.os
and a.country = b.country
and a.install_source = b.install_source);

--rev table: 1 day, 3 day, cumulative revenue (and payers)
create temp table rev_table as
(select a.*, b.rev_3, b.payer_3, c.rev_cum, c.payer_cum from
(select date, server::int, os, country, install_source,
sum(revenue_usd) as rev, 
count(nullif(revenue_usd, 0)) as payer
FROM joined
group by 1,2,3,4,5) a
join
(select join_date, server, os, country, install_source,
sum(revenue_usd) as rev_3,
count(distinct uid) as payer_3 from
(select date, server::int, os, country, install_source,
uid, revenue_usd
FROM joined
where revenue_usd>0) a
join
dates b
on (a.date+2>=b.join_date
and a.date<=b.join_date)
group by 1,2,3,4,5) b
on a.date = b.join_date
and a.server = b.server
and a.os = b.os
and a.country = b.country
and a.install_source = b.install_source
join
(select join_date, server, os, country, install_source,
sum(revenue_usd) as rev_cum,
count(distinct uid) as payer_cum from
(select date, server::int, os, country, install_source,
uid, revenue_usd
FROM joined
where revenue_usd>0) a
join
dates b
on a.date<=b.join_date
group by 1,2,3,4,5) c
on a.date = c.join_date
and a.server = c.server
and a.os = c.os
and a.country = c.country
and a.install_source = c.install_source);

--retention table
create temp table retention_table as
(select join_date, server, os, country, install_source,
sum(u) as u, 
sum(u_1) as u_1, 
sum(u_7) as u_7 from
(select * from
h
join
dates m
on h.install_date<=m.join_date)
group by 1,2,3,4,5);

--7-day install
create temp table install_7 as
(select join_date, server, os, country, install_source,
sum(installs) as install_7 from 
dau_table a
join
dates b
on a.date+7<=b.join_date
group by 1,2,3,4,5);

--7-day ltv
create temp table ltv_7 as
(select join_date, server, os, country, install_source,
sum(revenue_usd) ltv7_rev, count(distinct uid) ltv7_payers
from
(SELECT * FROM joined 
where revenue_usd>0 
and install_date+7<=date) a
join 
dates b
on a.date<=b.join_date
group by 1,2,3,4,5);


--putting everything together
drop table if exists processed.agg_server_stats;
create table processed.agg_server_stats as 
select x.*, y.u, y.u_1, y.u_7,
z.rev, z.rev_3, z.rev_cum, z.payer, z.payer_3, z.payer_cum,
w.ltv7_rev, w.ltv7_payers, q.install_7 from
dau_table x
left join
retention_table y
on x.date = y.join_date
and x.server = y.server
and x.os = y.os
and x.install_source = y.install_source
and x.country = y.country
left join
rev_table z
on x.date = z.date
and x.server = z.server
and x.os = z.os
and x.install_source = z.install_source
and x.country = z.country
left join
ltv_7 w
on x.date = w.join_date
and x.server = w.server
and x.os = w.os
and x.install_source = w.install_source
and x.country = w.country
left join
install_7 q
on x.date = q.join_date
and x.server = q.server
and x.os = q.os
and x.install_source = q.install_source
and x.country = q.country;

-- TODO: figure out fraud install source or sub_publisher
create temp table adjust_duplicated_users as
select userid, count(1) as install_count
from unique_adjust
where userid != ''
group by 1
having count(1) >= 10;

drop table if exists processed.fraud_channel;
create table processed.fraud_channel as
select
    trunc(a.ts) as install_date
    ,app_id
    ,country
    ,game
    ,split_part(tracker_name, '::', 1) as install_source
	,split_part(tracker_name, '::', 3) as sub_publisher
	,a.userid
from unique_adjust a
    join adjust_duplicated_users d on a.userid = d.userid;

-- TODO: Conversion rate
drop table if exists play_days;
create temp table play_days as
select 0 as day
union all
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day
union all
select 120 as day;

create temp table agg_player_days_cube as
SELECT
    d.day as play_days
    ,u.install_date
    ,trunc(u.conversion_ts) as conversion_date
    ,u.server
    ,u.os
    ,u.country
    ,u.app
    ,u.install_source
    ,case when DATEDIFF('day', u.install_date, u.conversion_ts) <= d.day then u.is_payer else 0 end as is_payer
    ,u.vip_level
    ,u.player_key
FROM        processed.dim_player u
     join   play_days d
       on   DATEDIFF('day', u.install_date, CURRENT_DATE - 1) >= d.day;

-- insert into agg_conversion_ltv table
drop table if exists processed.agg_conversion_ltv;
create table processed.agg_conversion_ltv as
SELECT
    c.play_days
    ,c.install_date
    ,c.server
    ,c.os
    ,c.country
    ,c.app
    ,c.install_source
    ,c.is_payer
    ,c.vip_level
    ,count(distinct c.player_key) as players
    ,sum(COALESCE(du.revenue_usd, 0)) as revenue
FROM        agg_player_days_cube c
left join   processed.fact_player_dau_snapshot du
       on   c.player_key = du.player_key and DATEDIFF('day', c.install_date, du.date) <= c.play_days
group by 1,2,3,4,5,6,7,8,9;

delete from processed.agg_conversion_ltv where install_date < '2015-03-30';

delete from processed.agg_conversion_ltv
where DATEDIFF('day', install_date, CURRENT_DATE - 1) < play_days;

--processed.item_change
--track item changes of players (1 record per item in/out)
delete from processed.item_change where
    ts>= (
                        select start_date
                        from dwtlw.processed.tmp_start_date
                     );

drop table if exists g;
create temp table g as
select * from
(select * from
(select json_extract_path_text(collections, 'items_spent') items, 
'out' as flow,
ts, uid, server, md5
from processed.events_raw 
where event = 'item'
and json_extract_path_text(collections, 'items_spent')!='')
union all
(select json_extract_path_text(collections, 'items_received') items, 
'in' as flow,
ts, uid, server, md5
from processed.events_raw 
where event = 'item'
and json_extract_path_text(collections, 'items_received')!=''))
where ts>=(
                        select start_date
                        from dwtlw.processed.tmp_start_date
                     );

insert into processed.item_change
select json_extract_path_text(item, 'item_id') as item_id, nullif(json_extract_path_text(item, 'item_amount'),'')::int as amount,
 flow, ts, uid, server, md5 from
(select json_extract_array_element_text(g.items, seq.i) as item, g.*
from g, seq_0_to_100 as seq
where seq.i<json_array_length(g.items));

--processed.dim_payers
truncate table processed.dim_payers;
insert into processed.dim_payers
select a.*, p.last_ts, b.alliance_id 
,g1.goldclone, g2.leadership, g3.purchase, g4.build 
from
(SELECT app, uid, server, total_revenue_usd, last_login_ts, level, country, language, os
FROM dwtlw.processed.dim_player 
where total_revenue_usd>0
) a
left join
(select app, uid, server, max(ts) as last_ts 
from processed.fact_revenue where usd>0
group by 1,2,3) p
on a.app = p.app
and a.uid = p.uid
and a.server = p.server
left join
(SELECT distinct app, uid, server, first_value(alliance_id) over 
(partition by app, uid, server order by ts desc rows between unbounded preceding and unbounded following) as alliance_id 
FROM dwtlw.processed.fact_league where action='join') b
on a.app = b.app
and a.uid = b.uid
and a.server = b.server
left join
(SELECT app, uid, server, sum(spent_gold) as goldclone 
FROM dwtlw.processed.fact_resource_out
where action='goldclone'
group by 1,2,3) g1
on a.app = g1.app
and a.uid = g1.uid
and a.server = g1.server
left join
(SELECT app, uid, server, sum(spent_gold) as leadership 
FROM dwtlw.processed.fact_resource_out
where action='leadershipup'
group by 1,2,3) g2
on a.app = g2.app
and a.uid = g2.uid
and a.server = g2.server
left join
(SELECT app, uid, server, sum(spent_gold) as purchase 
FROM dwtlw.processed.fact_resource_out
where action='purchase'
group by 1,2,3) g3
on a.app = g3.app
and a.uid = g3.uid
and a.server = g3.server
left join
(SELECT app, uid, server, sum(spent_gold) as build 
FROM dwtlw.processed.fact_resource_out
where action='build'
group by 1,2,3) g4
on a.app = g4.app
and a.uid = g4.uid
and a.server = g4.server;

--hero levelup tables for cs
delete from processed.hero_levelup where
    ts>= (
                        select start_date
                        from dwtlw.processed.tmp_start_date
                     );

insert into processed.hero_levelup
select uid, server, hero_id, hero_level, min(ts) as ts from
    (SELECT uid, ts, server, 
    nullif(json_extract_path_text(properties,'hero_id'),'')::int as hero_id,
    nullif(json_extract_path_text(properties,'hero_level'),'')::int as hero_level 
    FROM processed.events_raw 
    where event='hero' and json_extract_path_text(properties,'action')='levelup'
    and ts>= (
                            select start_date
                            from dwtlw.processed.tmp_start_date
                         )
    )
group by 1,2,3,4;

--hero clone/select table for cs
delete from processed.hero_cloneselect where
    ts>= (
                        select start_date
                        from dwtlw.processed.tmp_start_date
                     );

insert into processed.hero_cloneselect
select uid, server, ts,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'hero_id') as hero_id,
json_extract_path_text(collections,'hero_id_list') as hero_id_list
FROM processed.events_raw 
where event='hero' and 
    (json_extract_path_text(properties,'action') like '%clone%'
    or json_extract_path_text(properties,'action')='select')
    and ts>= (
                            select start_date
                            from dwtlw.processed.tmp_start_date
                         );

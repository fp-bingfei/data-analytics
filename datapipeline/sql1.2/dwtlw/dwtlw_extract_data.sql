-- get all different event
select distinct event
from tlw.processed.events_raw_test;

-- get some example
select *
from tlw.processed.events_raw_test
where event = 'pve'
limit 100;

'{"bi_version":"1.2","app_id":"dwtlw.global.sandbox","ts":1427273901,"event":"pve","user_id":"37619342","session_id":"40_1409","properties":{"app_version":"10105","gameserver_id":"40","os":"ios","os_version":"8.1.2","idfa":"0F8255C9-B592-4E0D-9253-0FD20BC51ADB","mac_address":"","device":"iPhone6,2","ip":"117.136.54.23","lang":"zh_CN","level":14,"install_ts":1426948992,"action":"win","chapter_id":1,"encounter_id":17,"hero_id":3,"hero_leadership":20,"hero_level":17},"collections":{"player_resources":[{"resource_id":1,"resource_name":"Food","resource_amount":712901},{"resource_id":2,"resource_name":"Water","resource_amount":1641934},{"resource_id":3,"resource_name":"Metal","resource_amount":749451},{"resource_id":4,"resource_name":"Fuel","resource_amount":371669},{"resource_id":5,"resource_name":"Money","resource_amount":902708},{"resource_id":6,"resource_name":"Gold","resource_amount":1784}],"troops":[{"ArmyType":7,"ArmyCount":9},{"ArmyType":7,"ArmyCount":9},{"ArmyType":7,"ArmyCount":9},{"ArmyType":7,"ArmyCount":9},{"ArmyType":7,"ArmyCount":9},{"ArmyType":7,"ArmyCount":9}],"troops_lost":[{"ArmyType":7,"ArmyCount":0},{"ArmyType":7,"ArmyCount":0},{"ArmyType":7,"ArmyCount":0},{"ArmyType":7,"ArmyCount":0},{"ArmyType":7,"ArmyCount":0},{"ArmyType":7,"ArmyCount":0}]}}'

{
    "bi_version": "1.2",
    "app_id": "dwtlw.global.sandbox",
    "ts": 1427273931,
    "event": "pve",
    "user_id": "37619342",
    "session_id": "40_1409",
    "properties": {
        "app_version": "10105",
        "gameserver_id": "40",
        "os": "ios",
        "os_version": "8.1.2",
        "idfa": "0F8255C9-B592-4E0D-9253-0FD20BC51ADB",
        "mac_address": "",
        "device": "iPhone6,2",
        "ip": "117.136.54.23",
        "lang": "zh_CN",
        "level": 14,
        "install_ts": 1426948992,
        "action": "win",
        "chapter_id": 1,
        "encounter_id": 18,
        "hero_id": 3,
        "hero_leadership": 20,
        "hero_level": 17
    },
    "collections": {
        "player_resources": [
            {
                "resource_id": 1,
                "resource_name": "Food",
                "resource_amount": 712901
            },
            {
                "resource_id": 2,
                "resource_name": "Water",
                "resource_amount": 1641934
            },
            {
                "resource_id": 3,
                "resource_name": "Metal",
                "resource_amount": 749451
            },
            {
                "resource_id": 4,
                "resource_name": "Fuel",
                "resource_amount": 371669
            },
            {
                "resource_id": 5,
                "resource_name": "Money",
                "resource_amount": 902708
            },
            {
                "resource_id": 6,
                "resource_name": "Gold",
                "resource_amount": 1784
            }
        ],
        "troops": [
            {
                "ArmyType": 7,
                "ArmyCount": 9
            },
            {
                "ArmyType": 7,
                "ArmyCount": 9
            },
            {
                "ArmyType": 7,
                "ArmyCount": 9
            },
            {
                "ArmyType": 7,
                "ArmyCount": 9
            },
            {
                "ArmyType": 7,
                "ArmyCount": 9
            },
            {
                "ArmyType": 7,
                "ArmyCount": 9
            }
        ],
        "troops_lost": [
            {
                "ArmyType": 7,
                "ArmyCount": 0
            },
            {
                "ArmyType": 7,
                "ArmyCount": 0
            },
            {
                "ArmyType": 7,
                "ArmyCount": 0
            },
            {
                "ArmyType": 7,
                "ArmyCount": 0
            },
            {
                "ArmyType": 7,
                "ArmyCount": 0
            },
            {
                "ArmyType": 7,
                "ArmyCount": 0
            }
        ]
    }
}

-- extract data from json string
drop table if exists tlw_pm.unload_pve;
create table tlw_pm.unload_pve as
select
    date, ts, app_id, user_id, gameserver_id, install_ts, level, os, os_version, device, lang,
    json_extract_path_text(properties, 'action') as action
    ,json_extract_path_text(properties, 'chapter_id') as chapter_id
    ,json_extract_path_text(properties, 'encounter_id') as encounter_id
    ,json_extract_path_text(properties, 'hero_id') as hero_id
    ,json_extract_path_text(properties, 'hero_leadership') as hero_leadership
    ,json_extract_path_text(properties, 'hero_level') as hero_level
--     get troops
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 0), 'ArmyType') as troops1_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 0), 'ArmyCount') as troops1_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 1), 'ArmyType') as troops2_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 1), 'ArmyCount') as troops2_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 2), 'ArmyType') as troops3_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 2), 'ArmyCount') as troops3_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 3), 'ArmyType') as troops4_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 3), 'ArmyCount') as troops4_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 4), 'ArmyType') as troops5_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 4), 'ArmyCount') as troops5_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 5), 'ArmyType') as troops6_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops'), 5), 'ArmyCount') as troops6_army_count
--     get troops_lost
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 0), 'ArmyType') as troops_lost1_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 0), 'ArmyCount') as troops_lost1_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 1), 'ArmyType') as troops_lost2_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 1), 'ArmyCount') as troops_lost2_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 2), 'ArmyType') as troops_lost3_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 2), 'ArmyCount') as troops_lost3_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 3), 'ArmyType') as troops_lost4_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 3), 'ArmyCount') as troops_lost4_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 4), 'ArmyType') as troops_lost5_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 4), 'ArmyCount') as troops_lost5_army_count
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 5), 'ArmyType') as troops_lost6_army_type
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'troops_lost'), 5), 'ArmyCount') as troops_lost6_army_count
--     get resource
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 0), 'resource_name') as resource1_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 0), 'resource_amount') as resource1_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 1), 'resource_name') as resource2_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 1), 'resource_amount') as resource2_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 2), 'resource_name') as resource3_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 2), 'resource_amount') as resource3_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 3), 'resource_name') as resource4_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 3), 'resource_amount') as resource4_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 4), 'resource_name') as resource5_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 4), 'resource_amount') as resource5_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 5), 'resource_name') as resource6_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 5), 'resource_amount') as resource6_amount
from tlw.processed.events_raw_test
where event = 'pve';

-- dump data to S3
unload ('select * from tlw_pm.unload_pve;')
to 's3://com.funplus.bitest/tlw/unload_data/pve_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

unload ('select * from tlw_pm.unload_city;')
to 's3://com.funplus.bitest/tlw/unload_data/city_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

unload ('select * from tlw_pm.unload_league;')
to 's3://com.funplus.bitest/tlw/unload_data/league_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

unload ('select * from tlw_pm.unload_payment;')
to 's3://com.funplus.bitest/tlw/unload_data/payment_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

unload ('select * from tlw_pm.unload_quest;')
to 's3://com.funplus.bitest/tlw/unload_data/quest_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;

unload ('select * from tlw_pm.unload_quest;')
to 's3://com.funplus.bitest/tlw/unload_data/quest_'
CREDENTIALS 'aws_access_key_id=aws_access_key_id;aws_secret_access_key=aws_secret_access_key'
PARALLEL OFF
GZIP
ALLOWOVERWRITE;


-------------------------------------------------
--The DEAD WALK THE LAST WAR user install source
--Version 1.2
--Author Robin
/**
Description:
This script get user install source
**/
-------------------------------------------------
CREATE TEMP TABLE tmp_fact_user_install_source
(
  user_key                      VARCHAR(32) NOT NULL ENCODE LZO,
  app                           VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                           INTEGER NOT NULL,
  install_date                  DATE ENCODE DELTA,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, install_date, install_source_adjust, install_source, campaign, sub_publisher);

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get the users whose install source should be updated
------------------------------------------------------------------------------------------------------------------------
insert into tmp_fact_user_install_source (user_key, app, uid, install_date)
select md5(app || uid), app, uid, min(install_date) as install_date
from dwtlw.processed.fact_session
where date_start >= (select start_date from dwtlw.processed.tmp_start_date)
group by 1,2,3;

-- delete users whose install source have been updated
delete
from tmp_fact_user_install_source
using dwtlw.processed.fact_user_install_source t2
where tmp_fact_user_install_source.user_key = t2.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get real install date
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_date = u.install_date
from processed.dim_user u
where tmp_fact_user_install_source.user_key = u.user_key;

------------------------------------------------------------------------------------------------------------------------
--Step 3. Get first no 'Organic' install source from adjust
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_user_install_source_adjust as
select game as app, userid as uid,
    first_value(tracker_name ignore nulls) over (partition by game, userid order by ts rows between unbounded preceding and unbounded following) as install_source
from dwtlw.public.adjust
where lower(tracker_name) != 'organic'
order by game, userid;

update tmp_fact_user_install_source
set install_source_adjust_raw = t.install_source,
    install_source_adjust = t.install_source
from tmp_user_install_source_adjust t
where tmp_fact_user_install_source.app = t.app and
      tmp_fact_user_install_source.uid = t.uid;

------------------------------------------------------------------------------------------------------------------------
--Step 4. Get install_source from raw install_source string
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_adjust = split_part(install_source_adjust_raw, '::', 1);

------------------------------------------------------------------------------------------------------------------------
--Step 5. Update install_source, the priority is install_source_adjust > install_source_kochava > install_source_mat;
------------------------------------------------------------------------------------------------------------------------
update tmp_fact_user_install_source
set install_source_raw = install_source_adjust_raw,
    install_source = install_source_adjust
where install_source = 'Organic' and install_source_adjust != '';

update tmp_fact_user_install_source
set install_source = 'Aarki(incent)'
where install_source = 'Aarki' and install_date <= '2015-06-25';

update tmp_fact_user_install_source
set install_source = 'Everyads'
where install_source = 'Everyads(nonincent)';

update tmp_fact_user_install_source
set install_source = 'Fyber(incent)'
where install_source = 'Fyber' and install_date <= '2015-06-25';

update tmp_fact_user_install_source
set install_source = 'iconpeak'
where install_source = 'iconpeak(nonincent)';

update tmp_fact_user_install_source
set install_source = 'NativeX(incent)'
where install_source = 'NativeX' and install_date <= '2015-06-25';

update tmp_fact_user_install_source
set install_source = 'NativeX'
where install_source = 'NativeX+(nonincent)';

update tmp_fact_user_install_source
set install_source = 'Tapjoy(incent)'
where install_source = 'Tapjoy';

update tmp_fact_user_install_source
set install_source = 'SupersonicAds(incent)'
where lower(install_source) = 'supersonicads' and install_date <= '2015-06-25';

update tmp_fact_user_install_source
set install_source = 'SupersonicAds'
where install_source = 'supersonicads(nonincent)';

------------------------------------------------------------------------------------------------------------------------
--Step 6. Normalize install_source,
-- Such as mapping both ‘Adperio’ and ‘AdPerio’ to ‘AdPerio’, mapping both 'App Turbo’ and 'App+Turbo’ to 'App Turbo’
------------------------------------------------------------------------------------------------------------------------
create temp table tmp_ref_install_source_map as
select distinct install_source as install_source_raw, lower(install_source) as install_source_lower, install_source, install_source as bi_install_source
from tmp_fact_user_install_source;

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\+', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\-', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\(', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, '\)', '');

update tmp_ref_install_source_map
set install_source_lower = replace(install_source_lower, ' ', '');

insert into tmp_ref_install_source_map
select *
from dwtlw.processed.ref_install_source_map;

create temp table tmp_install_source_map as
select install_source_lower, install_source_raw
from
(select *, row_number () over (partition by install_source_lower order by install_source_raw) as rank
from tmp_ref_install_source_map) t
where t.rank = 1;

update tmp_ref_install_source_map
set install_source = t.install_source_raw,
    bi_install_source = t.install_source_raw
from tmp_install_source_map t
where tmp_ref_install_source_map.install_source_lower = t.install_source_lower;

-- Split install_source into incent and non-incent group. The mapping is from Wendy.
-- update tmp_ref_install_source_map
-- set bi_install_source = r.bi_install_source
-- from processed.ref_adjust_mat_bi_install_source_map r
-- where tmp_ref_install_source_map.install_source_lower = r.mat_install_source_lower;
--
-- update tmp_ref_install_source_map
-- set bi_install_source = r.bi_install_source
-- from processed.ref_adjust_mat_bi_install_source_map r
-- where tmp_ref_install_source_map.install_source_lower = r.adjust_install_source_lower;

truncate table dwtlw.processed.ref_install_source_map;
insert into dwtlw.processed.ref_install_source_map
select distinct install_source_raw, install_source_lower, install_source, bi_install_source
from tmp_ref_install_source_map;

update tmp_fact_user_install_source
set install_source = t.bi_install_source
from dwtlw.processed.ref_install_source_map t
where tmp_fact_user_install_source.install_source = t.install_source_raw;

------------------------------------------------------------------------------------------------------------------------
--Step 7. Get campaign, sub_publisher and creative_id
------------------------------------------------------------------------------------------------------------------------
-- update campaign
update tmp_fact_user_install_source
set campaign = split_part(install_source_raw, '::', 2);

-- update sub_publisher
update tmp_fact_user_install_source
set sub_publisher = split_part(install_source_raw, '::', 3);

-- update creative_id
update tmp_fact_user_install_source
set creative_id = split_part(install_source_raw, '::', 4);

------------------------------------------------------------------------------------------------------------------------
--Step 8. Delete records whose install_source is still 'Organic';
------------------------------------------------------------------------------------------------------------------------
delete
from tmp_fact_user_install_source
where install_source = 'Organic';

------------------------------------------------------------------------------------------------------------------------
--Step 9. Insert into fact_user_install_source
------------------------------------------------------------------------------------------------------------------------
insert into dwtlw.processed.fact_user_install_source
(
  user_key
  ,app
  ,uid
  ,install_date
  ,install_source_adjust_raw
  ,install_source_adjust
  ,install_source_raw
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
)
select
  user_key
  ,app
  ,uid
  ,install_date
  ,install_source_adjust_raw
  ,install_source_adjust
  ,install_source_raw
  ,install_source
  ,campaign
  ,sub_publisher
  ,creative_id
from tmp_fact_user_install_source;


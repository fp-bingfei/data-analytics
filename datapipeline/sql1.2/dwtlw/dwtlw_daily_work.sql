copy processed.geoip_country from 's3://com.funplus.bitest/tlw/move_database/processed/geoip_country/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' DELIMITER '^';
copy processed.ip_integer_range_block from 's3://com.funplus.bitest/tlw/move_database/processed/ip_integer_range_block/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' DELIMITER '^';

-- create new database account
select * from pg_user;
create user dwtlw_pm with password 'chQHS*nzF!56Yxql';

create schema dwtlw_pm;

grant usage on schema dwtlw_pm to dwtlw_pm;
grant create on schema dwtlw_pm to dwtlw_pm;
grant all on all tables in schema dwtlw_pm to dwtlw_pm;

grant usage on schema processed to dwtlw_pm;
grant select on all tables in schema processed to dwtlw_pm;

grant usage on schema public to dwtlw_pm;
grant select on all tables in schema public to dwtlw_pm;

alter user dwtlw_pm PASSWORD '*h@dakU2';

revoke all on all tables in schema processed from dwtlw_pm;
revoke all on all tables in schema public from dwtlw_pm;
revoke all on all tables in schema tlw_pm from dwtlw_pm;

revoke all on schema processed from dwtlw_pm;
revoke all on schema public from dwtlw_pm;
revoke all on schema dwtlw_pm from dwtlw_pm;

revoke all on database dwtlw from dwtlw_pm;

alter table dwtlw_pm.unload_city_apr27 OWNER to biadmin;

grant usage on schema processed to "siyuan.zhang";
grant select on all tables in schema processed to "siyuan.zhang";

grant usage on schema public to "siyuan.zhang";
grant select on all tables in schema public to "siyuan.zhang";

-- test account
create table dwtlw_pm.test as
select *
from processed.events_raw
limit 100;

select * from processed.agg_kpi limit 100;

set session authorization dwtlw_pm;

select current_user;

create table dwtlw_pm.test as
select *
from processed.events_raw
limit 100;

select * from processed.events_raw limit 100;

alter table processed.events_raw rename column app_id to app;
alter table processed.events_raw rename column user_id to uid;
alter table processed.events_raw rename column gameserver_id to server;

-- Adjust data
CREATE TABLE processed.raw_data_adjust
(
    adid                VARCHAR(256),
    userid              VARCHAR(256),
    game                VARCHAR(256),
    tracker             VARCHAR(256),
    tracker_name        VARCHAR(256),
    app_id              VARCHAR(256),
    ip_address          VARCHAR(256),
    idfa                VARCHAR(256),
    android_id          VARCHAR(256),
    idfa_md5            VARCHAR(256),
    country             VARCHAR(256),
	timestamp           VARCHAR(256),
	mac_md5             VARCHAR(256),
	gps_adid            VARCHAR(256),
	device_name         VARCHAR(256),
	os_name             VARCHAR(256),
	os_version          VARCHAR(256),
	click_time          VARCHAR(256)
)
DISTKEY(userid)
SORTKEY(userid);

insert into processed.raw_data_adjust
select
	json_extract_path_text(json_str,'adid') AS adid,
	json_extract_path_text(json_str,'userid') AS userid,
	json_extract_path_text(json_str,'game') AS game,
	json_extract_path_text(json_str,'tracker') AS tracker,
	json_extract_path_text(json_str,'tracker_name') AS tracker_name,
	json_extract_path_text(json_str,'app_id') AS app_id,
	json_extract_path_text(json_str,'ip_address') AS ip_address,
	json_extract_path_text(json_str,'idfa') AS idfa,
	json_extract_path_text(json_str,'android_id') AS android_id,
	json_extract_path_text(json_str,'idfa_md5') AS idfa_md5,
	json_extract_path_text(json_str,'country') AS country,
	json_extract_path_text(json_str,'timestamp') AS timestamp,
	json_extract_path_text(json_str,'mac_md5') AS mac_md5,
	json_extract_path_text(json_str,'gps_adid') AS gps_adid,
	json_extract_path_text(json_str,'device_name') AS device_name,
	json_extract_path_text(json_str,'os_name') AS os_name,
	json_extract_path_text(json_str,'os_version') AS os_version,
	json_extract_path_text(json_str,'click_time') AS click_time
from processed.raw_data_s3_test;


-- TODO: lookalike_campaign
-- US,GB,CA,AU,DE,FR,NZ,ID,SG,TW,HK,JP,RU,Norway,Korea.
drop table if exists lookalike_campaign_idfa_0702;
create table lookalike_campaign_idfa_0702 as
select s.idfa
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.idfa != '' and s.country_code = 'DE' and u.is_payer = 1;

unload ('select * from lookalike_campaign_idfa_0702;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_idfa_DE_0702_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists lookalike_campaign_gaid_0702;
create table lookalike_campaign_gaid_0702 as
select s.gaid
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.gaid != '' and s.country_code = 'DE' and u.is_payer = 1;

unload ('select * from lookalike_campaign_gaid_0702;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_gaid_DE_0702_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists lookalike_campaign_android_id_0702;
create table lookalike_campaign_android_id_0702 as
select s.android_id
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.android_id != '' and s.country_code = 'DE' and u.is_payer = 1;

unload ('select * from lookalike_campaign_android_id_0702;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_android_id_DE_0702_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- US,GB,CA,AU,DE,FR,NZ,ID,SG,TW,HK,JP,RU,Norway,Korea.
drop table if exists lookalike_campaign_idfa_0706;
create table lookalike_campaign_idfa_0706 as
select distinct s.idfa
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.idfa != '' and s.country_code = 'US' and s.level_start >= 10;

unload ('select * from lookalike_campaign_idfa_0706;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_idfa_US_0706_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists lookalike_campaign_gaid_0706;
create table lookalike_campaign_gaid_0706 as
select distinct s.gaid
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.gaid != '' and s.country_code = 'US' and s.level_start >= 10;

unload ('select * from lookalike_campaign_gaid_0706;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_gaid_US_0706_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

drop table if exists lookalike_campaign_android_id_0706;
create table lookalike_campaign_android_id_0706 as
select distinct s.android_id
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.android_id != '' and s.country_code = 'US' and s.level_start >= 10;

unload ('select * from lookalike_campaign_android_id_0706;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_android_id_US_0706_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

-- All countries
drop table if exists lookalike_campaign_idfa_0708;
create table lookalike_campaign_idfa_0708 as
select distinct s.idfa
from processed.fact_session s
	join processed.dim_user u on s.user_key = u.user_key
where s.idfa != '' and s.level_start >= 5;

unload ('select * from lookalike_campaign_idfa_0708;')
to 's3://com.funplus.bitest/dwtlw/lookalike_campaign/lookalike_campaign_idfa_0708_'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
delimiter '\t'
PARALLEL OFF
ALLOWOVERWRITE
GZIP;

--------------------------------------------------------------------------------------------------------------------------------------------
--The DEAD WALK THE LAST WAR session and payment
--Version 1.2
--Author Robin
/**
Description:
This script is generate basic user info from session_start and payment event.
These data is used to create KPI, Retention, LTV reports.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

-- TODO: raw data
----------------------------------------------------------------------------------------------------------------------------------------------
-- extract data from json
----------------------------------------------------------------------------------------------------------------------------------------------
delete from  dwtlw.processed.events_raw
where  load_hour >= (
                     select raw_data_start_date
                     from dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.events_raw
(
    load_hour
    ,event
    ,date
    ,ts
    ,app
    ,uid
    ,server
    ,install_ts
    ,country_code
    ,level
    ,vip_level
    ,ip
    ,os
    ,os_version
    ,app_version
    ,session_id
    ,bi_version
    ,idfa
    ,gaid
    ,android_id
    ,mac_address
    ,device
    ,lang
    ,json_str
    ,properties
    ,collections
    ,md5
)
select
    load_hour
    ,json_extract_path_text(json_str, 'event') as event
    ,trunc(dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00')) as date
    ,dateadd(second, CAST(json_extract_path_text(json_str, 'ts') AS INTEGER), '1970-01-01 00:00:00') as ts
    ,json_extract_path_text(json_str, 'app_id') as app
    ,CAST(json_extract_path_text(json_str, 'user_id') AS INTEGER) as uid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gameserver_id') as server
    ,dateadd(second, CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'install_ts') AS INTEGER), '1970-01-01 00:00:00') as install_ts
    ,'--' AS country_code
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'level') AS INTEGER) as level
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'vip_level') as vip_level
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'ip') as ip
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os') as os
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'os_version') AS VARCHAR(64)) as os_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'app_version') as app_version
    ,json_extract_path_text(json_str, 'session_id') as session_id
    ,json_extract_path_text(json_str, 'bi_version') as bi_version
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'idfa') as idfa
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'gaid') as gaid
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'android_id') as android_id
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'mac_address') as mac_address
    ,CAST(json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'device') AS VARCHAR(64)) as device
    ,json_extract_path_text( json_extract_path_text(json_str, 'properties'), 'lang') as lang
    ,json_str as json_str
    ,json_extract_path_text(json_str, 'properties') as properties
    ,json_extract_path_text(json_str, 'collections') as collections
    ,md5(json_str) as md5
from dwtlw.processed.raw_data_s3
where  load_hour >= (
                     select raw_data_start_date
                     from dwtlw.processed.tmp_start_date
                );

-- remove test data
delete from dwtlw.processed.events_raw where server = '400';

-- ip_country_map
create temp table ip_address as
select distinct
    ip,
    cast(split_part(ip,'.',1) AS BIGINT) * 16777216
    + cast(split_part(ip,'.',2) AS BIGINT) * 65536
    + cast(split_part(ip,'.',3) AS BIGINT) * 256
    + cast(split_part(ip,'.',4) AS BIGINT) as ip_int
from dwtlw.processed.events_raw
where load_hour >= (
                     select raw_data_start_date
                     from dwtlw.processed.tmp_start_date
                    )
and   ip != ''
and   ip != 'unknown';

create temp table ip_country_map as
select
     i.ip
    ,i.ip_int
    ,c.country_iso_code as country_code
from ip_address i
    join dwtlw.processed.ip_integer_range_block r on i.ip_int >= network_start_integer and i.ip_int <= network_last_integer
    join dwtlw.processed.geoip_country c on r.geoname_id = c.geoname_id
where c.country_iso_code != '';

update dwtlw.processed.events_raw
set country_code = i.country_code
from ip_country_map i
where dwtlw.processed.events_raw.ip = i.ip
and   load_hour >= (
                     select raw_data_start_date
                     from dwtlw.processed.tmp_start_date
                   );

-- TODO: check the data
-- drop table if exists dwtlw.processed.tab_spiderman_capture;
-- create table dwtlw.processed.tab_spiderman_capture (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number_old" BIGINT NOT NULL DEFAULT 0,
--   "row_number_new" BIGINT NOT NULL DEFAULT 0,
--   "row_number_diff" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);
--
-- drop table if exists dwtlw.processed.tab_spiderman_capture_history;
-- create table dwtlw.processed.tab_spiderman_capture_history (
--   "record_date" date NOT NULL,
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL DEFAULT 0
-- )
--   DISTKEY(event)
--   SORTKEY(record_date, event, date, hour);

-- truncate table dwtlw.processed.tab_spiderman_capture;
-- drop table dwtlw.processed.tab_spiderman_capture_old;
-- alter table dwtlw.processed.tab_spiderman_capture_new rename to tab_spiderman_capture_old;

-- create table dwtlw.processed.tab_spiderman_capture_new (
--   "event" varchar(32) NOT NULL,
--   "date" date NOT NULL,
--   "hour" SMALLINT NOT NULL,
--   "row_number" BIGINT NOT NULL
-- )
--   DISTKEY(event)
--   SORTKEY(event, date, hour);

-- insert into dwtlw.processed.tab_spiderman_capture_new
-- select event, trunc(ts) as date, extract(hour from ts) as hour, count(1)
-- from dwtlw.processed.events_raw
-- group by event, trunc(ts), extract(hour from ts);

-- insert into dwtlw.processed.tab_spiderman_capture
-- select n.event, n.date, n.hour, case when o.row_number is null then 0 else o.row_number end as row_number_old,
--     n.row_number as row_number_new
-- from dwtlw.processed.tab_spiderman_capture_new n left join dwtlw.processed.tab_spiderman_capture_old o
--     on n.event = o.event and n.date = o.date and n.hour = o.hour
-- union
-- select o.event, o.date, o.hour, o.row_number as row_number_old,
--     case when n.row_number is null then 0 else n.row_number end as row_number_new
-- from dwtlw.processed.tab_spiderman_capture_old o left join dwtlw.processed.tab_spiderman_capture_new n
--     on n.event = o.event and n.date = o.date and n.hour = o.hour;

-- update dwtlw.processed.tab_spiderman_capture
-- set row_number_diff = row_number_new - row_number_old;

-- delete from dwtlw.processed.tab_spiderman_capture_history where record_date = CURRENT_DATE;
-- insert into dwtlw.processed.tab_spiderman_capture_history
-- (
--     record_date
--     ,event
--     ,date
--     ,hour
--     ,row_number
-- )
-- select
--     CURRENT_DATE as record_date
--     ,event
--     ,date
--     ,hour
--     ,row_number
-- from dwtlw.processed.tab_spiderman_capture_new;

-- TODO: fact tables
----------------------------------------------------------------------------------------------------------------------------------------------
-- dwtlw.processed.fact_session
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from  dwtlw.processed.fact_session
where  date_start >= (
                     select raw_data_start_date
                     from dwtlw.processed.tmp_start_date
                );

create temp table tmp_fact_session as
select *
from   dwtlw.processed.events_raw
where  event in ('newuser', 'session_start')
and    date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
               );

insert into dwtlw.processed.fact_session
(
        date_start
       ,user_key
       ,app
       ,uid
       ,player_key
       ,server
       ,player_id
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts_start
       ,ts_end
       ,level_start
       ,level_end
       ,vip_level_start
       ,vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,browser
       ,browser_version
       ,rc_bal_start
       ,rc_bal_end
       ,coin_bal_start
       ,coin_bal_end
       ,ab_test
       ,ab_variant
       ,session_length
)
select  date as date_start
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,MD5(server || uid) AS player_key
       ,server
       ,null AS player_id
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,null as install_source
       ,app_version as app_version
       ,session_id as session_id
       ,ts as ts_start
       ,ts as ts_end
       ,level as level_start
       ,level as level_end
       ,vip_level as vip_level_start
       ,vip_level as vip_level_end
       ,os
       ,os_version
       ,country_code
       ,ip
       ,lang AS language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,null as browser
       ,null as browser_version
       ,null as rc_bal_start
       ,null as rc_bal_end
       ,null as coin_bal_start
       ,null as coin_bal_end
       ,null as ab_test
       ,null as ab_variant
       ,null as session_length
from   tmp_fact_session;


----------------------------------------------------------------------------------------------------------------------------------------------
-- dwtlw.processed.fact_revenue
----------------------------------------------------------------------------------------------------------------------------------------------

--delete the historical data in case repeat loading
delete from dwtlw.processed.fact_revenue
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

create temp table tmp_fact_revenue as
select *
from    dwtlw.processed.events_raw
where   event = 'payment'
and     date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
                );


insert into dwtlw.processed.fact_revenue
(
       date
       ,user_key
       ,app
       ,uid
       ,player_key
       ,server
       ,player_id
       ,install_ts
       ,install_date
	   ,install_source
       ,app_version
       ,session_id
       ,ts
       ,level
       ,vip_level
       ,os
       ,os_version
       ,country_code
       ,ip
       ,language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,browser
       ,browser_version
       ,ab_test
       ,ab_variant

       ,payment_processor
       ,product_id
       ,product_name
       ,product_type
       ,coins_in
       ,rc_in
       ,currency
       ,amount
       ,usd
       ,transaction_id
)
select  date
       ,MD5(app || uid) as user_key
       ,app
       ,uid
       ,MD5(server || uid) AS player_key
       ,server
       ,null AS player_id
       ,install_ts
       ,trunc(install_ts) AS install_date
	   ,null as install_source
       ,app_version as app_version
       ,session_id as session_id
       ,ts
       ,level
       ,vip_level
       ,os
       ,os_version
       ,country_code
       ,ip
       ,lang AS language
       ,device
       ,idfa
       ,gaid
       ,android_id
       ,mac_address
       ,null as browser
       ,null as browser_version
       ,null as ab_test
       ,null as ab_variant

       ,json_extract_path_text(properties,'payment_processor') AS payment_processor
       ,json_extract_path_text(properties,'iap_product_id') AS product_id
       ,json_extract_path_text(properties,'iap_product_name') AS product_name
       ,json_extract_path_text(properties,'product_type') AS product_type
       ,CAST(
               CASE WHEN json_extract_path_text (properties,'coins_in') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'coins_in')
               END AS integer
             ) AS coins_in
       ,CAST(
               CASE WHEN json_extract_path_text (properties,'rc_in') = '' THEN '0'
                    ELSE json_extract_path_text(properties,'rc_in')
               END AS integer
             ) AS rc_in
       ,json_extract_path_text(properties,'currency') AS currency
       ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4))/100 AS amount
       ,CAST(json_extract_path_text(properties,'amount') AS DECIMAL(14,4))/100 AS usd
       ,json_extract_path_text(properties,'transaction_id') AS transaction_id
from    tmp_fact_revenue;

create temp table tmp_duplicated_transaction_id as
select transaction_id, count(1)
from dwtlw.processed.fact_revenue
group by transaction_id
having count(1) > 1;

create temp table tmp_duplicated_transaction_id_rows as
select t.*, row_number() over (partition by transaction_id order by ts) rank
from
(select r.*
from dwtlw.processed.fact_revenue r join tmp_duplicated_transaction_id d on r.transaction_id = d.transaction_id) t;

create temp table tmp_duplicated_transaction_id_first_row as
select *
from tmp_duplicated_transaction_id_rows
where rank = 1;

alter table tmp_duplicated_transaction_id_first_row drop column rank;

delete
from dwtlw.processed.fact_revenue
using tmp_duplicated_transaction_id d
where dwtlw.processed.fact_revenue.transaction_id = d.transaction_id;

insert into dwtlw.processed.fact_revenue
select *
from tmp_duplicated_transaction_id_first_row;

update dwtlw.processed.fact_revenue
set usd = amount * case when c.factor is null then 1 else c.factor end
from public.currency c
where dwtlw.processed.fact_revenue.currency = c.currency and dwtlw.processed.fact_revenue.date = c.dt;

-- TODO: daily_level
delete from  dwtlw.processed.daily_level
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
               );

create temp table player_level as
select trunc(ts) as date
        ,MD5(app || uid) as user_key
        ,app
        ,uid
        ,MD5(server || uid) AS player_key
        ,server AS server
        ,null as player_id
        ,level
        ,vip_level
from dwtlw.processed.events_raw
where date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

insert into dwtlw.processed.daily_level
(
    date
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,min_level
    ,max_level
    ,min_vip_level
    ,max_vip_level
)
select
    date
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,min(level) as min_level
    ,max(level) as max_level
    ,min(vip_level) as min_vip_level
    ,max(vip_level) as max_vip_level
from player_level
group by 1,2,3,4,5,6;

-- TODO: finish now
create temp table finish_now as
select
    date
    ,ts
    ,MD5(app || uid) as user_key
    ,app
    ,uid
    ,MD5(server || uid) AS player_key
    ,server
    ,null AS player_id
    ,level
    ,vip_level
    ,os
    ,json_extract_path_text(properties, 'action') as action
    ,json_extract_path_text(properties, 'building_level') as building_level
    ,json_extract_path_text(properties, 'building_type') as building_type
    ,properties
from dwtlw.processed.events_raw
where event = 'finishnow' and
      date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

delete from  dwtlw.processed.fact_finish_now
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.fact_finish_now
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,building_level
    ,building_type
    ,properties
)
select
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,case when building_level = '' then null else cast(building_level as smallint) end as building_level
    ,building_type
    ,properties
from finish_now;

-- TODO: city
create temp table city as
select
    date
    ,ts
    ,MD5(app || uid) as user_key
    ,app
    ,uid
    ,MD5(server || uid) AS player_key
    ,server
    ,null AS player_id
    ,level
    ,vip_level
    ,os
    ,json_extract_path_text(properties, 'action') as action
    ,json_extract_path_text(properties, 'building_level') as building_level
    ,json_extract_path_text(properties, 'building_type') as building_type
    ,properties
from dwtlw.processed.events_raw
where event = 'city' and
      date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

delete from  dwtlw.processed.fact_city
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.fact_city
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,building_level
    ,building_type
    ,properties
)
select
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,case when building_level = '' then null else cast(building_level as smallint) end as building_level
    ,building_type
    ,properties
from city;

-- TODO: league
create temp table league as
select
    date
    ,ts
    ,MD5(app || uid) as user_key
    ,app
    ,uid
    ,MD5(server || uid) AS player_key
    ,server
    ,null AS player_id
    ,level
    ,vip_level
    ,os
    ,json_extract_path_text(properties, 'action') as action
    ,json_extract_path_text(properties, 'alliance_id') as alliance_id
    ,json_extract_path_text(properties, 'target_user_id') as target_uid
    ,json_extract_path_text(properties, 'target_user_level') as target_user_level
    ,json_extract_path_text(properties, 'target_Rank') as target_rank
    ,json_extract_path_text(properties, 'gift_level') as gift_level
    ,properties
from dwtlw.processed.events_raw
where event = 'league' and
      date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

delete from  dwtlw.processed.fact_league
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.fact_league
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,alliance_id
    ,target_uid
    ,target_user_level
    ,target_rank
    ,gift_level
    ,properties
)
select
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,case when alliance_id = '' then null else cast(alliance_id as integer) end as alliance_id
    ,case when target_uid = '' then null else cast(target_uid as integer) end as target_uid
    ,case when target_user_level = '' then null else cast(target_user_level as smallint) end as target_user_level
    ,case when target_rank = '' then null else cast(target_rank as smallint) end as target_rank
    ,case when gift_level = '' then null else cast(gift_level as smallint) end as gift_level
    ,properties
from league;

-- TODO: gifts
create temp table gifts as
select
    date
    ,ts
    ,MD5(app || uid) as user_key
    ,app
    ,uid
    ,MD5(server || uid) AS player_key
    ,server
    ,null AS player_id
    ,level
    ,vip_level
    ,os
    ,json_extract_path_text(properties, 'action') as action
    ,properties
from dwtlw.processed.events_raw
where event = 'gifts' and
      date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

delete from  dwtlw.processed.fact_gifts
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.fact_gifts
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,properties
)
select
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,action
    ,properties
from gifts;

-- TODO: fact_resource_out
create temp table resource_out as
select
    date
    ,ts
    ,MD5(app || uid) as user_key
    ,app
    ,uid
    ,MD5(server || uid) AS player_key
    ,server
    ,null AS player_id
    ,level
    ,vip_level
    ,os
    ,event
    ,json_extract_path_text(properties, 'action') as action
--  get user's resource
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 0), 'resource_name') as player_resource1_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 0), 'resource_amount') as player_resource1_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 1), 'resource_name') as player_resource2_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 1), 'resource_amount') as player_resource2_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 2), 'resource_name') as player_resource3_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 2), 'resource_amount') as player_resource3_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 3), 'resource_name') as player_resource4_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 3), 'resource_amount') as player_resource4_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 4), 'resource_name') as player_resource5_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 4), 'resource_amount') as player_resource5_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 5), 'resource_name') as player_resource6_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 5), 'resource_amount') as player_resource6_amount
--  get user spend resource
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 0), 'resource_name') as spent_resource1_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 0), 'resource_amount') as spent_resource1_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 1), 'resource_name') as spent_resource2_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 1), 'resource_amount') as spent_resource2_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 2), 'resource_name') as spent_resource3_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 2), 'resource_amount') as spent_resource3_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 3), 'resource_name') as spent_resource4_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 3), 'resource_amount') as spent_resource4_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 4), 'resource_name') as spent_resource5_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 4), 'resource_amount') as spent_resource5_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 5), 'resource_name') as spent_resource6_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_spent'), 5), 'resource_amount') as spent_resource6_amount
    ,properties
from dwtlw.processed.events_raw
where json_extract_path_text(collections, 'resources_spent') != '' and
      date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

delete from  dwtlw.processed.fact_resource_out
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.fact_resource_out
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,event
    ,action
    ,player_food
    ,player_water
    ,player_metal
    ,player_fuel
    ,player_money
    ,player_gold
    ,spent_food
    ,spent_water
    ,spent_metal
    ,spent_fuel
    ,spent_money
    ,spent_gold
    ,properties
)
select
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,event
    ,action
--  get user's resource
    ,case
        when player_resource1_name = 'Food' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Food' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Food' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Food' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Food' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Food' then cast(player_resource6_amount as bigint)
    end as player_food
    ,case
        when player_resource1_name = 'Water' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Water' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Water' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Water' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Water' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Water' then cast(player_resource6_amount as bigint)
    end as player_water
    ,case
        when player_resource1_name = 'Metal' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Metal' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Metal' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Metal' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Metal' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Metal' then cast(player_resource6_amount as bigint)
    end as player_metal
    ,case
        when player_resource1_name = 'Fuel' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Fuel' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Fuel' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Fuel' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Fuel' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Fuel' then cast(player_resource6_amount as bigint)
    end as player_fuel
    ,case
        when player_resource1_name = 'Money' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Money' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Money' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Money' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Money' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Money' then cast(player_resource6_amount as bigint)
    end as player_money
    ,case
        when player_resource1_name = 'Gold' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Gold' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Gold' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Gold' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Gold' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Gold' then cast(player_resource6_amount as bigint)
    end as player_gold
--  get user spend resource
    ,case
        when spent_resource1_name = 'Food' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Food' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Food' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Food' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Food' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Food' then cast(spent_resource6_amount as bigint)
    end as spent_food
    ,case
        when spent_resource1_name = 'Water' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Water' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Water' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Water' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Water' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Water' then cast(spent_resource6_amount as bigint)
    end as spent_water
    ,case
        when spent_resource1_name = 'Metal' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Metal' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Metal' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Metal' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Metal' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Metal' then cast(spent_resource6_amount as bigint)
    end as spent_metal
    ,case
        when spent_resource1_name = 'Fuel' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Fuel' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Fuel' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Fuel' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Fuel' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Fuel' then cast(spent_resource6_amount as bigint)
    end as spent_fuel
    ,case
        when spent_resource1_name = 'Money' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Money' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Money' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Money' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Money' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Money' then cast(spent_resource6_amount as bigint)
    end as spent_money
    ,case
        when spent_resource1_name = 'Gold' then cast(spent_resource1_amount as bigint)
        when spent_resource2_name = 'Gold' then cast(spent_resource2_amount as bigint)
        when spent_resource3_name = 'Gold' then cast(spent_resource3_amount as bigint)
        when spent_resource4_name = 'Gold' then cast(spent_resource4_amount as bigint)
        when spent_resource5_name = 'Gold' then cast(spent_resource5_amount as bigint)
        when spent_resource6_name = 'Gold' then cast(spent_resource6_amount as bigint)
    end as spent_gold
    ,properties
from resource_out;

-- TODO: fact_resource_in
create temp table resource_in as
select
    date
    ,ts
    ,MD5(app || uid) as user_key
    ,app
    ,uid
    ,MD5(server || uid) AS player_key
    ,server
    ,null AS player_id
    ,level
    ,vip_level
    ,os
    ,event
    ,json_extract_path_text(properties, 'action') as action
--  get user's resource
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 0), 'resource_name') as player_resource1_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 0), 'resource_amount') as player_resource1_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 1), 'resource_name') as player_resource2_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 1), 'resource_amount') as player_resource2_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 2), 'resource_name') as player_resource3_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 2), 'resource_amount') as player_resource3_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 3), 'resource_name') as player_resource4_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 3), 'resource_amount') as player_resource4_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 4), 'resource_name') as player_resource5_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 4), 'resource_amount') as player_resource5_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 5), 'resource_name') as player_resource6_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'player_resources'), 5), 'resource_amount') as player_resource6_amount
--  get user receive resource
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 0), 'resource_name') as receive_resource1_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 0), 'resource_amount') as receive_resource1_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 1), 'resource_name') as receive_resource2_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 1), 'resource_amount') as receive_resource2_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 2), 'resource_name') as receive_resource3_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 2), 'resource_amount') as receive_resource3_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 3), 'resource_name') as receive_resource4_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 3), 'resource_amount') as receive_resource4_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 4), 'resource_name') as receive_resource5_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 4), 'resource_amount') as receive_resource5_amount
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 5), 'resource_name') as receive_resource6_name
    ,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(collections, 'resources_received'), 5), 'resource_amount') as receive_resource6_amount
    ,properties
from dwtlw.processed.events_raw
where json_extract_path_text(collections, 'resources_received') != '' and
      date >= (
                    select raw_data_start_date
                    from dwtlw.processed.tmp_start_date
              );

delete from  dwtlw.processed.fact_resource_in
where  date >= (
                     select raw_data_start_date
                     from   dwtlw.processed.tmp_start_date
                );

insert into dwtlw.processed.fact_resource_in
(
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,event
    ,action
    ,player_food
    ,player_water
    ,player_metal
    ,player_fuel
    ,player_money
    ,player_gold
    ,receive_food
    ,receive_water
    ,receive_metal
    ,receive_fuel
    ,receive_money
    ,receive_gold
    ,properties
)
select
    date
    ,ts
    ,user_key
    ,app
    ,uid
    ,player_key
    ,server
    ,player_id
    ,level
    ,vip_level
    ,os
    ,event
    ,action
--  get user's resource
    ,case
        when player_resource1_name = 'Food' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Food' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Food' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Food' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Food' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Food' then cast(player_resource6_amount as bigint)
    end as player_food
    ,case
        when player_resource1_name = 'Water' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Water' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Water' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Water' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Water' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Water' then cast(player_resource6_amount as bigint)
    end as player_water
    ,case
        when player_resource1_name = 'Metal' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Metal' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Metal' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Metal' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Metal' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Metal' then cast(player_resource6_amount as bigint)
    end as player_metal
    ,case
        when player_resource1_name = 'Fuel' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Fuel' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Fuel' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Fuel' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Fuel' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Fuel' then cast(player_resource6_amount as bigint)
    end as player_fuel
    ,case
        when player_resource1_name = 'Money' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Money' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Money' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Money' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Money' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Money' then cast(player_resource6_amount as bigint)
    end as player_money
    ,case
        when player_resource1_name = 'Gold' then cast(player_resource1_amount as bigint)
        when player_resource2_name = 'Gold' then cast(player_resource2_amount as bigint)
        when player_resource3_name = 'Gold' then cast(player_resource3_amount as bigint)
        when player_resource4_name = 'Gold' then cast(player_resource4_amount as bigint)
        when player_resource5_name = 'Gold' then cast(player_resource5_amount as bigint)
        when player_resource6_name = 'Gold' then cast(player_resource6_amount as bigint)
    end as player_gold
--  get user receive resource
    ,case
        when receive_resource1_name = 'Food' then cast(receive_resource1_amount as bigint)
        when receive_resource2_name = 'Food' then cast(receive_resource2_amount as bigint)
        when receive_resource3_name = 'Food' then cast(receive_resource3_amount as bigint)
        when receive_resource4_name = 'Food' then cast(receive_resource4_amount as bigint)
        when receive_resource5_name = 'Food' then cast(receive_resource5_amount as bigint)
        when receive_resource6_name = 'Food' then cast(receive_resource6_amount as bigint)
    end as receive_food
    ,case
        when receive_resource1_name = 'Water' then cast(receive_resource1_amount as bigint)
        when receive_resource2_name = 'Water' then cast(receive_resource2_amount as bigint)
        when receive_resource3_name = 'Water' then cast(receive_resource3_amount as bigint)
        when receive_resource4_name = 'Water' then cast(receive_resource4_amount as bigint)
        when receive_resource5_name = 'Water' then cast(receive_resource5_amount as bigint)
        when receive_resource6_name = 'Water' then cast(receive_resource6_amount as bigint)
    end as receive_water
    ,case
        when receive_resource1_name = 'Metal' then cast(receive_resource1_amount as bigint)
        when receive_resource2_name = 'Metal' then cast(receive_resource2_amount as bigint)
        when receive_resource3_name = 'Metal' then cast(receive_resource3_amount as bigint)
        when receive_resource4_name = 'Metal' then cast(receive_resource4_amount as bigint)
        when receive_resource5_name = 'Metal' then cast(receive_resource5_amount as bigint)
        when receive_resource6_name = 'Metal' then cast(receive_resource6_amount as bigint)
    end as receive_metal
    ,case
        when receive_resource1_name = 'Fuel' then cast(receive_resource1_amount as bigint)
        when receive_resource2_name = 'Fuel' then cast(receive_resource2_amount as bigint)
        when receive_resource3_name = 'Fuel' then cast(receive_resource3_amount as bigint)
        when receive_resource4_name = 'Fuel' then cast(receive_resource4_amount as bigint)
        when receive_resource5_name = 'Fuel' then cast(receive_resource5_amount as bigint)
        when receive_resource6_name = 'Fuel' then cast(receive_resource6_amount as bigint)
    end as receive_fuel
    ,case
        when receive_resource1_name = 'Money' then cast(receive_resource1_amount as bigint)
        when receive_resource2_name = 'Money' then cast(receive_resource2_amount as bigint)
        when receive_resource3_name = 'Money' then cast(receive_resource3_amount as bigint)
        when receive_resource4_name = 'Money' then cast(receive_resource4_amount as bigint)
        when receive_resource5_name = 'Money' then cast(receive_resource5_amount as bigint)
        when receive_resource6_name = 'Money' then cast(receive_resource6_amount as bigint)
    end as receive_money
    ,case
        when receive_resource1_name = 'Gold' then cast(receive_resource1_amount as bigint)
        when receive_resource2_name = 'Gold' then cast(receive_resource2_amount as bigint)
        when receive_resource3_name = 'Gold' then cast(receive_resource3_amount as bigint)
        when receive_resource4_name = 'Gold' then cast(receive_resource4_amount as bigint)
        when receive_resource5_name = 'Gold' then cast(receive_resource5_amount as bigint)
        when receive_resource6_name = 'Gold' then cast(receive_resource6_amount as bigint)
    end as receive_gold
    ,properties
from resource_in;

-- -- TODO: Remove duplicated records from adjust table
-- drop table if exists unique_adjust;
-- create table unique_adjust as
-- select *
-- from
-- (select *, row_number() over (partition by adid order by ts) as row_number
-- from adjust)  t
-- where row_number = 1;

-- update unique_adjust
-- set userid = a.userid
-- from adjust a
-- where unique_adjust.userid = '' and a.userid != '' and unique_adjust.adid = a.adid;

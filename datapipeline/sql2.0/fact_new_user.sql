------------------------------------------------
--Data 2.0 fact_new_user.sql
------------------------------------------------
CREATE TABLE kpi_processed.fact_new_user
(
	id VARCHAR(128) NOT NULL ENCODE bytedict,
	app_id VARCHAR(64) NOT NULL ENCODE bytedict,
	app_version VARCHAR(32) ENCODE bytedict,
	user_key VARCHAR(128) NOT NULL ENCODE bytedict DISTKEY,
	user_id VARCHAR(128) NOT NULL ENCODE bytedict,
	date_start DATE NOT NULL ENCODE delta,
	date_end DATE ENCODE delta,
	ts_start TIMESTAMP NOT NULL ENCODE delta,
	ts_end TIMESTAMP ENCODE delta,
	install_ts TIMESTAMP NOT NULL ENCODE delta,
	install_date DATE NOT NULL ENCODE delta,
	session_id VARCHAR(128) ENCODE bytedict,
	facebook_id VARCHAR(128) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	os VARCHAR(64) ENCODE bytedict,
	os_version VARCHAR(64) ENCODE bytedict,
	browser VARCHAR(64) ENCODE bytedict,
	browser_version VARCHAR(128) ENCODE bytedict,
	device VARCHAR(128) ENCODE bytedict,
	country VARCHAR(64) ENCODE bytedict,
	email VARCHAR(256) ENCODE bytedict,
	first_name VARCHAR(64) ENCODE bytedict,
	last_name VARCHAR(64) ENCODE bytedict,
	level_start INTEGER,
	level_end INTEGER,
	vip_level INTEGER,
	gender VARCHAR(1) ENCODE bytedict,
	birthday DATE ENCODE delta,
	ip VARCHAR(32) NOT NULL ENCODE bytedict,
	language VARCHAR(16) ENCODE bytedict,
	playtime_sec BIGINT
)
SORTKEY
(
	date_start,
	user_key,
	app_id,
	user_id
);


delete from kpi_processed.fact_new_user
where date_start >= 
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 );


insert into  kpi_processed.fact_new_user
select     md5(u.app_id||u.user_id||u.session_id) as id
          ,u.app_id 
          ,u.app_version
          ,md5(u.app_id||u.user_id) as user_key
          ,u.user_id
          ,trunc(u.ts) as date_start
          ,null as date_end
          ,u.ts as ts_start
          ,null as ts_end
          ,u.install_ts     
          ,trunc(u.install_ts) as install_date     
          ,u.session_id     
          ,u.facebook_id    
          ,u.install_source 
          ,u.os             
          ,u.os_version     
          ,u.browser        
          ,u.browser_version
          ,u.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,u.email          
          ,u.first_name     
          ,u.last_name      
          ,u.level as level_start
          ,u.level as level_end
          ,u.vip_level
          ,u.gender         
          ,u.birthday       
          ,u.ip             
          ,u.gameserver_id  
          ,u.lang as language
from      kpi_events.raw_new_user u
left join kpi_processed.dim_country c
on        s.country_code = c.country_code
where     trunc(s.ts) >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 );

--update dim user using agg_level data
update waf.dim_user 
set level=b.to_level
from 
	(select user_id, max(to_level) as to_level 
	from waf.agg_level_daily group by 1) b
where 
waf.dim_user.user_id = b.user_id
and waf.dim_user.level<b.to_level;

--fact_session_length table
truncate table waf.fact_session_length;
insert into waf.fact_session_length
select user_id, country_code, session_start, datediff('second', session_start, ts_end) as session_length, 
level_start, level_end
from
(
select user_id, country_code, session_start, max(ts_pretty) as ts_end, 
min(level) as level_start, max(level) as level_end
from
(
select *, last_value(start_ts ignore nulls) over 
(partition by user_id order by ts_pretty rows between unbounded preceding and current row) as session_start from
(select user_id, country_code, ts_pretty, ts_prev, level,
case
	when datediff('second', ts_prev, ts_pretty)<3600 then NULL
	else ts_pretty end as start_ts from
(SELECT *, lag(ts_pretty,1) over (partition by user_id order by ts_pretty) as ts_prev
from
(select user_id, country_code, ts_pretty, max(level) as level
FROM custom.waf.catchall
group by 1,2,3) 
))
)
group by 1,2,3
)
order by 1,3;

--payment_funnel
create table if not exists waf.payment_funnel (
	ts BIGINT,
    ts_pretty TIMESTAMP,
	app_id VARCHAR(32) ENCODE lzo,
    user_id VARCHAR(64) ENCODE lzo,
	level INTEGER,
	os VARCHAR(16) ENCODE lzo,
	product_id VARCHAR(64) ENCODE lzo,
    price decimal(6,2),
	buy_ts TIMESTAMP
	);

delete from waf.payment_funnel where datediff('day', ts_pretty, sysdate)<3;

create temp table g as
(select * from
(select b.ts as buy_ts, a.ts,
row_number() over (partition by a.user_id, b.ts order by ts_pretty desc) as rnum 
from 
(select * from waf.fact_revenue where ts>='2015-12-24') b
left join
(SELECT * FROM custom.waf.catchall 
where event in ('click_event')) a
on a.user_id=b.user_id
and a.ts_pretty<=b.ts
and datediff('hour', a.ts_pretty, b.ts)<6
and json_extract_path_text(d_c1, 'value')=b.iap_product_id)
where rnum=1);

insert into waf.payment_funnel
select c.*, g.buy_ts
from
(SELECT ts, ts_pretty, app_id, user_id, level, os, json_extract_path_text(d_c1, 'value') as product_id, 
nullif(regexp_substr(json_extract_path_text(d_c2, 'value'), '[0-9]*\\.?[0-9]+'),'')::decimal(6,2) as price 
FROM custom.waf.catchall 
where event in ('click_event')
and datediff('day', ts_pretty, sysdate)<3) c
left join 
g
on c.ts = g.ts;


--------------------------------------------------------------------------------------------------------------------------------------------
--FF tableau report tables
--Version 1.2
--Author Robin
/**
Description:
This script is generate marketing tables.
**/
---------------------------------------------------------------------------------------------------------------------------------------------

truncate table kpi_processed.agg_marketing_kpi;

create temp table tmp_new_install_users as
select user_key
    ,app_id as app
    ,user_id as uid
    ,install_date
    ,install_source
    ,'' as install_source_group
    ,install_campaign as campaign
    ,install_subpublisher as sub_publisher
    ,'' as creative_id
    ,install_country as country
    ,install_os as os
    ,is_payer
from kpi_processed.dim_user;

create temp table tmp_all_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
group by d.user_key, d.app_id;

create temp table tmp_d1_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
    join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 1
group by d.user_key, d.app_id;

create temp table tmp_d7_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 7
group by d.user_key, d.app_id;

create temp table tmp_d30_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 30
group by d.user_key, d.app_id;

create temp table tmp_d60_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 60
group by d.user_key, d.app_id;

create temp table tmp_d90_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 90
group by d.user_key, d.app_id;

create temp table tmp_d120_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 120
group by d.user_key, d.app_id;

create temp table tmp_d1_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 1
group by d.user_key, d.app_id;

create temp table tmp_d7_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 7
group by d.user_key, d.app_id;

create temp table tmp_d30_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 30
group by d.user_key, d.app_id;

create temp table tmp_d60_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 60
group by d.user_key, d.app_id;

create temp table tmp_d90_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 90
group by d.user_key, d.app_id;

create temp table tmp_d120_retained as
select d.user_key, d.app_id as app, 1 as retained
from kpi_processed.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 120
group by d.user_key, d.app_id;

insert into kpi_processed.agg_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_source
    ,install_source_group
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os

    ,new_installs
    ,d1_new_installs
    ,d7_new_installs
    ,d30_new_installs
    ,d60_new_installs
    ,d90_new_installs
    ,d120_new_installs

    ,revenue
    ,d1_revenue
    ,d7_revenue
    ,d30_revenue
    ,d60_revenue
    ,d90_revenue
    ,d120_revenue

    ,payers
    ,d1_payers
    ,d7_payers
    ,d30_payers
    ,d60_payers
    ,d90_payers
    ,d120_payers

    ,d1_retained
    ,d7_retained
    ,d30_retained
    ,d60_retained
    ,d90_retained
    ,d120_retained
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,u.install_source
    ,u.install_source_group
    ,u.campaign
    ,u.sub_publisher
    ,u.creative_id
    ,u.country
    ,u.os

    ,count(1) as new_installs
    ,count(1) as d1_new_installs
    ,count(1) as d7_new_installs
    ,count(1) as d30_new_installs
    ,count(1) as d60_new_installs
    ,count(1) as d90_new_installs
    ,count(1) as d120_new_installs

    ,sum(case when r.revenue is null then 0 else r.revenue end) as revenue
    ,sum(case when d1_r.revenue is null then 0 else d1_r.revenue end) as d1_revenue
    ,sum(case when d7_r.revenue is null then 0 else d7_r.revenue end) as d7_revenue
    ,sum(case when d30_r.revenue is null then 0 else d30_r.revenue end) as d30_revenue
    ,sum(case when d60_r.revenue is null then 0 else d60_r.revenue end) as d60_revenue
    ,sum(case when d90_r.revenue is null then 0 else d90_r.revenue end) as d90_revenue
    ,sum(case when d120_r.revenue is null then 0 else d120_r.revenue end) as d120_revenue

    ,sum(case when r.is_payer is null then 0 else r.is_payer end) as payers
    ,sum(case when d1_r.is_payer is null then 0 else d1_r.is_payer end) as d1_payers
    ,sum(case when d7_r.is_payer is null then 0 else d7_r.is_payer end) as d7_payers
    ,sum(case when d30_r.is_payer is null then 0 else d30_r.is_payer end) as d30_payers
    ,sum(case when d60_r.is_payer is null then 0 else d60_r.is_payer end) as d60_payers
    ,sum(case when d90_r.is_payer is null then 0 else d90_r.is_payer end) as d90_payers
    ,sum(case when d120_r.is_payer is null then 0 else d120_r.is_payer end) as d120_payers

    ,sum(case when d1_retained.retained is null then 0 else d1_retained.retained end) as d1_retained
    ,sum(case when d7_retained.retained is null then 0 else d7_retained.retained end) as d7_retained
    ,sum(case when d30_retained.retained is null then 0 else d30_retained.retained end) as d30_retained
    ,sum(case when d60_retained.retained is null then 0 else d60_retained.retained end) as d60_retained
    ,sum(case when d90_retained.retained is null then 0 else d90_retained.retained end) as d90_retained
    ,sum(case when d120_retained.retained is null then 0 else d120_retained.retained end) as d120_retained
from tmp_new_install_users u
    left join tmp_all_revenue r on u.user_key = r.user_key
    left join tmp_d1_revenue d1_r on u.user_key = d1_r.user_key
    left join tmp_d7_revenue d7_r on u.user_key = d7_r.user_key
    left join tmp_d30_revenue d30_r on u.user_key = d30_r.user_key
    left join tmp_d60_revenue d60_r on u.user_key = d60_r.user_key
    left join tmp_d90_revenue d90_r on u.user_key = d90_r.user_key
    left join tmp_d120_revenue d120_r on u.user_key = d120_r.user_key

    left join tmp_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_d30_retained d30_retained on u.user_key = d30_retained.user_key
    left join tmp_d60_retained d60_retained on u.user_key = d60_retained.user_key
    left join tmp_d90_retained d90_retained on u.user_key = d90_retained.user_key
    left join tmp_d120_retained d120_retained on u.user_key = d120_retained.user_key
group by 1,2,3,4,5,6,7,8,9,10;

delete from kpi_processed.agg_marketing_kpi where install_date < '2014-07-16';

update kpi_processed.agg_marketing_kpi
set d1_new_installs = 0,
    d1_retained = 0,
    d1_revenue = 0,
    d1_payers = 0
where install_date >= CURRENT_DATE - 1;

update kpi_processed.agg_marketing_kpi
set d7_new_installs = 0,
    d7_retained = 0,
    d7_revenue = 0,
    d7_payers = 0
where install_date >= CURRENT_DATE - 7;

update kpi_processed.agg_marketing_kpi
set d30_new_installs = 0,
    d30_retained = 0,
    d30_revenue = 0,
    d30_payers = 0
where install_date >= CURRENT_DATE - 30;

update kpi_processed.agg_marketing_kpi
set d60_new_installs = 0,
    d60_retained = 0,
    d60_revenue = 0,
    d60_payers = 0
where install_date >= CURRENT_DATE - 60;

update kpi_processed.agg_marketing_kpi
set d90_new_installs = 0,
    d90_retained = 0,
    d90_revenue = 0,
    d90_payers = 0
where install_date >= CURRENT_DATE - 90;

update kpi_processed.agg_marketing_kpi
set d120_new_installs = 0,
    d120_retained = 0,
    d120_revenue = 0,
    d120_payers = 0
where install_date >= CURRENT_DATE - 120;

CREATE TABLE raw_events.session_end
(
data_version VARCHAR(10) ENCODE bytedict,
app_id VARCHAR(64) ENCODE bytedict,
ts VARCHAR(20) ENCODE bytedict,
ts_pretty TIMESTAMP ENCODE delta,
user_id VARCHAR(128) ENCODE bytedict DISTKEY,
session_id VARCHAR(128) ENCODE bytedict,
event VARCHAR(128) ENCODE bytedict,
properties VARCHAR(20000) ENCODE bytedict,
collections VARCHAR(20000) ENCODE bytedict
)
SORTKEY
(
app_id,
ts_pretty
);

grant usage on schema raw_events to lambda_load_user;
grant select on table raw_events.events to lambda_load_user;
grant insert on table raw_events.events to lambda_load_user;

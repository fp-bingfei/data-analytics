update ha.dim_user set app_id='ff2.th.prod' where app_id ='ha.th.prod';
update ha.dim_user set app_id='ff2.us.prod' where app_id ='ha.us.prod';

delete from finance_audit where date = current_date-1;
insert into finance_audit
select 
app,
date(ts) as date,
is_payer,
sum(nvl(nullif(json_extract_path_text(properties,'rc_get'), '')::int, 0)) as rc_get,
sum(nvl(nullif(json_extract_path_text(properties,'rc_spend'), '')::int, 0)) as rc_spend from events_raw h
join ha.dim_user u on h.app=u.app_id and h.uid=u.user_id where event='transaction' and date(ts) = current_date-1
group by 1,2,3;
-------------------------------------------------
--Data 2.0 dim_user.sql
-------------------------------------------------

---- create user_payment table
CREATE temp TABLE tmp_user_payment AS
SELECT DISTINCT
         user_key
        ,date
        ,min(r.ts) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)                      AS conversion_ts
        ,sum(revenue_usd) over (partition by r.user_key, date ORDER BY ts ASC rows between unbounded preceding and unbounded following)         AS revenue_usd
        ,sum(revenue_usd) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)               AS total_revenue_usd
        ,count(transaction_id) over (partition by r.user_key, date ORDER BY ts ASC rows between unbounded preceding and unbounded following )   AS purchase_cnt
        ,count(transaction_id) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)          AS total_purchase_cnt
FROM kpi_processed.fact_revenue r
;
 

DROP TABLE IF EXISTS tmp_dim_user;
CREATE TEMP TABLE tmp_dim_user AS
WITH
    last_info AS
    (
        SELECT
            l.*
            ,row_number() over (partition by user_key order by date desc) as row
            ,c.country
        FROM kpi_processed.tmp_user_daily_login l
        LEFT JOIN kpi_processed.dim_country c
            ON  c.country_code=l.country_code
    )
    ,install_info AS
    (
        SELECT
             user_key
            ,language
            ,locale
            ,install_source
            ,l.country_code
            ,ci.country
            ,os
            ,device
            ,browser
            ,gender
        FROM kpi_processed.tmp_user_daily_login l
        LEFT JOIN kpi_processed.dim_country ci
            ON  ci.country_code=l.country_code
        WHERE l.install_date=l.date
    )
    ,payment_info AS
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
            ,max(total_revenue_usd) as total_revenue_usd
            ,max(total_purchase_cnt) as total_purchase_cnt
        FROM kpi_processed.tmp_user_payment
        GROUP BY 1
    )
    ,last_level AS
    (
        SELECT
            user_key
            ,max(levelup_ts) AS levelup_ts
            ,max(level_end) AS level_end
        FROM kpi_processed.tmp_user_level tl
        GROUP by 1
    )
SELECT DISTINCT
  tu.user_key AS id
  ,tu.user_key
  ,tu.app_id
  ,tu.app_user_id
  ,tu.snsid
  ,COALESCE(du.facebook_id, tu.facebook_id) AS facebook_id
  ,tu.install_ts
  ,tu.install_date
  ,coalesce(ti.install_source, 'Organic') AS install_source
  ,null AS install_subpublisher
  ,null AS install_campaign
  ,coalesce(ti.language,du.install_language) AS install_language
  ,coalesce(ti.locale, du.install_locale) AS install_locale
  ,coalesce(ti.country_code, du.install_country_code) AS install_country_code
  ,coalesce(ti.country, du.install_country) AS install_country
  ,coalesce(lower(ti.os), lower(du.install_os)) AS install_os
  ,coalesce(ti.device, du.install_device) AS install_device
  ,coalesce(lower(ti.browser), lower(du.install_browser)) AS install_browser
  ,coalesce(ti.gender, du.install_gender) AS install_gender
  ,tu.language
  ,tu.locale
  ,tu.birthday
  ,tu.gender
  ,tu.country_code
  ,tu.country
  ,tu.os
  ,tu.os_version
  ,tu.device
  ,NULL AS device_alias
  ,tu.browser
  ,tu.browser_version
  ,tu.app_version
  ,COALESCE(tu.level_end,tl.level_end, tu.level_start,du.level) AS level
  ,COALESCE(tl.levelup_ts,du.levelup_ts) AS levelup_ts
  ,CASE WHEN COALESCE(tp.total_revenue_usd, 0) > 0 THEN 1 ELSE 0 END AS is_payer
  ,tp.conversion_ts
  ,COALESCE(tp.total_revenue_usd,0) AS total_revenue_usd
  ,COALESCE(tp.total_purchase_cnt,0) AS payment_cnt
  ,tu.last_login_ts
  ,COALESCE (tu.email, du.email) AS email
  ,COALESCE (tu.last_ip, du.last_ip) AS last_ip
FROM last_info tu
LEFT JOIN install_info ti
    ON ti.user_key=tu.user_key
LEFT JOIN kpi_processed.dim_user du
    ON tu.user_key = du.user_key
LEFT JOIN payment_info tp
    ON tp.user_key=tu.user_key
LEFT JOIN last_level tl
    ON tu.user_key = tl.user_key
WHERE tu.row=1
;

DELETE FROM kpi_processed.dim_user
USING kpi_processed.tmp_user_daily_login d
WHERE kpi_processed.dim_user.user_key=d.user_key;

INSERT INTO kpi_processed.dim_user
(
  id
  ,user_key
  ,app_id
  ,app_user_id
  ,snsid
  ,facebook_id
  ,install_ts
  ,install_date
  ,install_source
  ,install_subpublisher
  ,install_campaign
  ,install_language
  ,install_locale
  ,install_country_code
  ,install_country
  ,install_os
  ,install_device
  ,install_browser
  ,install_gender
  ,language
  ,locale
  ,birthday
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,device_alias
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,levelup_ts
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,payment_cnt
  ,last_login_ts
  ,email
  ,last_ip
)
SELECT
  id
  ,user_key
  ,app_id
  ,app_user_id
  ,snsid
  ,facebook_id
  ,install_ts
  ,install_date
  ,install_source
  ,install_subpublisher
  ,install_campaign
  ,install_language
  ,install_locale
  ,install_country_code
  ,install_country
  ,install_os
  ,install_device
  ,install_browser
  ,install_gender
  ,language
  ,locale
  ,birthday
  ,gender
  ,country_code
  ,country
  ,os
  ,os_version
  ,device
  ,device_alias
  ,browser
  ,browser_version
  ,app_version
  ,level
  ,levelup_ts
  ,is_payer
  ,conversion_ts
  ,total_revenue_usd
  ,payment_cnt
  ,last_login_ts
  ,email
  ,last_ip
FROM tmp_dim_user
;

--delete from kpi_processed.dim_user where install_date is null;

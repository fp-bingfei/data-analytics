delete from ffs.agg_kpi_device
where date >=(select start_date from kpi_processed.init_start_date)
and app_id like 'ffs%'
;

insert into ffs.agg_kpi_device
select d.date
      ,d.app_id
      ,d.app_version
      ,case when u.app_id like 'farm%' then u.install_source_group
       else u.install_source end as install_source_group
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,d.is_new_user
      ,d.is_payer
      ,d.device
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
      ,d.scene
      ,sum(coalesce(d.revenue_iap,0)) as revenue_iap
      ,sum(coalesce(d.revenue_ads,0)) as revenue_ads
from kpi_processed.fact_dau_snapshot d
join kpi_processed.dim_user u on d.user_key=u.user_key
where date >=(select start_date from kpi_processed.init_start_date)
and
d.app_id like 'ffs%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,22;
create table if not exists waf.agg_tutorial_daily (
	    date DATE,
    app_id VARCHAR(30) ENCODE lzo,
    event VARCHAR(16) ENCODE lzo,
    user_id VARCHAR(64) ENCODE lzo,
    install_ts TIMESTAMP,
    os VARCHAR(14) ENCODE lzo,
    app_version VARCHAR(10) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    action VARCHAR(8) ENCODE lzo,
    tutorial_step INTEGER,
    level INTEGER
	);

delete from waf.agg_tutorial_daily where datediff('day', date, sysdate)<=7;

insert into waf.agg_tutorial_daily
	(date, app_id, event, user_id, install_ts, os, app_version, country_code, action, tutorial_step, level)
	select date_trunc('day', ts_pretty) as date, 
	app_id,
event,
user_id,
install_ts,
os,
app_version,
country_code,
json_extract_path_text(d_c1, 'value') as action,
nullif(json_extract_path_text(m1, 'value'), '')::int as tutorial_step,
max(nullif(level::varchar, '')::int) as level 
	from 
	(select * from waf.catchall where event = 'tutorial' and datediff('day', ts_pretty, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;

create table if not exists waf.agg_level_daily (
	    date DATE,
    app_id VARCHAR(30) ENCODE lzo,
    event VARCHAR(16) ENCODE lzo,
    user_id VARCHAR(64) ENCODE lzo,
    install_ts TIMESTAMP,
    os VARCHAR(14) ENCODE lzo,
    app_version VARCHAR(10) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    level INTEGER,
    to_level INTEGER
	);

delete from waf.agg_level_daily where datediff('day', date, sysdate)<=7;

insert into waf.agg_level_daily
	(date, app_id, event, user_id, install_ts, os, app_version, country_code, level, to_level)
	select date_trunc('day', ts_pretty) as date, 
	app_id,
event,
user_id,
install_ts,
os,
app_version,
country_code,
min(nullif(level::varchar, '')::int) as level,
max(nullif(json_extract_path_text(m2, 'value')::varchar, '')::int) as to_level 
	from 
	(select * from waf.catchall where event = 'level' and datediff('day', ts_pretty, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5, 6, 7, 8;

create table if not exists waf.agg_economy_daily (
        date DATE,
    app_id VARCHAR(30) ENCODE lzo,
    os VARCHAR(14) ENCODE lzo,
    level INTEGER,
    app_version VARCHAR(10) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    currency VARCHAR(14) ENCODE lzo,
    status VARCHAR(14) ENCODE lzo,
    action VARCHAR(32) ENCODE lzo,
    amount INTEGER,
    user_count INTEGER
    );

delete from waf.agg_economy_daily where datediff('day', date, sysdate)<=7;

insert into waf.agg_economy_daily
    (date, app_id, os, level, app_version, country_code, currency, status, action, amount, user_count)
    select date_trunc('day', ts_pretty) as date, 
    app_id,
os,
level,
app_version,
country_code,
json_extract_path_text(d_c1, 'value') as currency,
json_extract_path_text(d_c2, 'value') as status,
json_extract_path_text(d_c3, 'value') as action,
sum(nullif(json_extract_path_text(m1, 'value')::varchar, '')::int) as amount,
count(distinct user_id) as user_count 
    from 
    (select * from waf.catchall where event = 'economic' and datediff('day', ts_pretty, sysdate)<=7) 
     
    group by 1, 2, 3, 4, 5, 6, 7, 8, 9;

vacuum waf.catchall;
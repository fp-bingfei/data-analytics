------------------------------------------------
--Data 2.0 fact_session.sql
------------------------------------------------

delete from kpi_processed.fact_session
where date_start >= 
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 );

insert into kpi_processed.fact_session
select     md5(s.app_id||s.user_id||s.session_id) as id
          ,s.app_id 
          ,s.app_version
          ,md5(s.app_id||s.user_id) as user_key
          ,s.user_id
          ,trunc(s.ts) as date_start
          ,trunc(s2.ts) as date_end
          ,s.ts as ts_start
          ,s2.ts as ts_end
          ,s.install_ts     
          ,trunc(s.install_ts) as install_date     
          ,s.session_id     
          ,s.facebook_id    
          ,s.install_source 
          ,s.os             
          ,s.os_version     
          ,s.browser        
          ,s.browser_version
          ,s.device         
          ,coalesce(c.country,'Unknown')  as country      
          ,s.email          
          ,s.first_name     
          ,s.last_name      
          ,s.level as level_start
          ,e.level as level_end
          ,s.vip_level
          ,s.gender         
          ,s.birthday       
          ,s.ip             
          ,s.gameserver_id  
          ,s.lang as language
          ,e.session_length as playtime_sec
from      kpi_events.raw_session_start s
left join kpi_events.raw_session_end e
on        s.session_id = e.session_id
and       s.app_id = e.app_id
and       s.user_id = e.user_id   
and       trunc(e.ts) >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 )
left join kpi_processed.dim_country c
on        s.country_code = c.country_code
where     trunc(s.ts) >=
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 );

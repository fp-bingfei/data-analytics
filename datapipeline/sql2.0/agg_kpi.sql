----------------------------------------------
--Data 2.0 agg_kpi.sql
----------------------------------------------

delete from kpi_processed.agg_kpi
where date >=(select start_date from kpi_processed.init_start_date);

insert into kpi_processed.agg_kpi
(
	 date                       
	,app_id                     
	,app_version                
	,install_year               
	,install_month              
	,install_week               
	,install_source_group       
	,install_source             
	,level_end                  
	,browser                    
	,browser_version            
	,device_alias               
	,country                    
	,os                         
	,os_version                 
	,language                   
	,is_new_user                
	,is_payer                   
	,new_user_cnt               
	,dau_cnt                    
	,newpayer_cnt               
	,payer_today_cnt            
	,payment_cnt                
	,revenue_usd
	,session_cnt
          ,playtime_sec
)
select
      MD5(d.date||COALESCE(d.app_id,'')||COALESCE(d.app_version,'')||EXTRACT(year FROM u.install_date)||
      EXTRACT(month FROM u.install_date)||EXTRACT(week FROM u.install_date) ||COALESCE(u.install_source,'')||
      COALESCE(level_end,0)||COALESCE(d.browser,'')||COALESCE(d.browser_version,'')||COALESCE(d.device,'')||
      COALESCE(d.country,'')||COALESCE(d.os,'')||COALESCE(d.os_version,'')||COALESCE(d.language,'')||
      COALESCE(d.is_new_user,0)||COALESCE(d.is_payer,0)) as id
      ,d.date
      ,d.app_id
      ,d.app_version
      ,EXTRACT(year FROM u.install_date) as install_year
      ,EXTRACT(month FROM u.install_date) as install_month
      ,EXTRACT(week FROM u.install_date) as install_week
      ,case when u.install_source like 'FF_%' or u.install_source like 'ff_%' then 'FF Promotion'
            when u.install_source like 'RS_%' or u.install_source like 'rs_%' then 'RS Promotion'
            when u.install_source like 'feed%' then 'Feed'
            when u.install_source like 'HA_%' or u.install_source like 'HE_%' then 'Marketing'
            when lower(u.install_source) like 'notif%' then 'Notif'
          else u.install_source end as install_source_group
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.browser_version
      ,d.device
      ,d.country
      ,d.os
      ,d.os_version
      ,d.language
      ,d.is_new_user
      ,d.is_payer
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
from processed_new.fact_dau_snapshot d
join processed_new.dim_user u on d.user_key=u.user_key
where date >=(select start_date from processed_new.tmp_start_date)
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21;

delete from kpi_processed.agg_kpi_history where record_date = CURRENT_DATE;
insert into kpi_processed.agg_kpi_history
(
      record_date
      ,date
      ,app
      ,country
      ,os
      ,new_installs
      ,dau
      ,new_payers
      ,today_payers
      ,revenue
      ,session_cnt
)
select CURRENT_DATE as record_date
      ,date
      ,app_id
      ,country
      ,os
      ,sum(new_user_cnt) as new_installs
      ,sum(dau_cnt) as dau
      ,sum(newpayer_cnt) as new_payers
      ,sum(payer_today_cnt) as today_payers
      ,sum(revenue_usd) as revenue
      ,sum(session_cnt) as session_cnt
from kpi_processed.agg_kpi d
group by 2,3,4,5;
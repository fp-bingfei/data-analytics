------------------------------------------------
--Data 2.0 fact_revenue.sql
------------------------------------------------
delete from kpi_processed.fact_revenue
where date_start >= 
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 );


insert into kpi_processed.fact_revenue
select      md5(app_id||user_key||transaction_id) as id               
           ,p.app_id           
           ,p.app_version      
           ,md5(p.app_id||p.user_id) as user_key
           ,p.user_id
           ,trunc(p.ts) as date
           ,p.ts               
           ,p.session_id       
           ,p.level            
           ,p.vip_level        
           ,p.os               
           ,p.os_version       
           ,p.device           
           ,p.browser          
           ,p.browser_version  
           ,p.country          
           ,p.ip               
           ,p.lang as language         
           ,p.payment_processor
           ,p.iap_product_id   
           ,p.iap_product_name 
           ,p.iap_product_type 
           ,p.currency         
           ,p.amount*1.0000/100 as revenue_amount   
           ,p.amount*1.0000*c.factor/100 as revenue_usd      
           ,p.transaction_id   
           ,p.first_purchase  
from       kpi_events.raw_payment p
left join  currency c
on         p.currency = c.currency
and        trunc(p.ts) = c.date
where      trunc(p.ts) >= 
                 (
                    select start_date 
                    from   kpi_processed.init_start_date
                 );

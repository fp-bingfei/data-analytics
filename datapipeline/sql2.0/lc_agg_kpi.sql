create temp table lc_payments as
select distinct json_extract_path_text(properties,'fpid') as fpid,user_id,app_id from raw_events.events where app_id like 'lc_patch%' and event='payment' and trunc(ts_pretty)>=(select start_date from kpi_processed.init_start_date);

update kpi_processed.dim_user set facebook_id=fpid from lc_payments t where kpi_processed.dim_user.app_id=t.app_id and kpi_processed.dim_user.user_id=t.user_id and kpi_processed.dim_user.app_id  like 'lc_patch%';

delete from littlechef.fact_revenue where date>=(select start_date from kpi_processed.init_start_date);
insert into littlechef.fact_revenue
select r.*, u.facebook_id as funplus_id from kpi_processed.fact_revenue r
join kpi_processed.dim_user u on r.app_id=u.app_id and r.user_id=u.user_id where r.app_id like 'lc_patch%' and
date>=(select start_date from kpi_processed.init_start_date) and 
date<=current_date;


update littlechef.init_start_date                                                          
set start_date =  DATEADD(day, -21, CURRENT_DATE);

delete from littlechef.fact_session where date_start  >=  (
                    select start_date 
                    from   littlechef.init_start_date
                  );
                  
insert into littlechef.fact_session
select * from kpi_processed.fact_session where app_id like 'lc_patch%' and date_start  >=  (
                    select start_date 
                    from   littlechef.init_start_date
                  );


delete from littlechef.fact_new_user where date_start  >=  (
                    select start_date 
                    from   littlechef.init_start_date
                  );
                  
insert into littlechef.fact_new_user
select * from kpi_processed.fact_new_user where app_id like 'lc_patch%' and date_start  >=  (
                    select start_date 
                    from   littlechef.init_start_date
                  );
                  
create temp table facebook_update as
select * from (select app_id,user_id,facebook_id,ROW_NUMBER() OVER (PARTITION BY app_id,user_id ORDER BY ts_start desc) as rank from littlechef.fact_session)t
where rank=1;

update kpi_processed.dim_user set facebook_id=t.facebook_id from facebook_update t where kpi_processed.dim_user.app_id like 'lc_patch%' and kpi_processed.dim_user.app_id=t.app_id and
 kpi_processed.dim_user.user_id=t.user_id;

update littlechef.fact_session set facebook_id=t.facebook_id from facebook_update t where littlechef.fact_session.app_id like 'lc_patch%' and littlechef.fact_session.app_id=t.app_id and
 littlechef.fact_session.user_id=t.user_id and (littlechef.fact_session.facebook_id is null or littlechef.fact_session.facebook_id='');


update littlechef.fact_new_user set facebook_id=t.facebook_id from facebook_update t where littlechef.fact_new_user.app_id like 'lc_patch%' and littlechef.fact_new_user.app_id=t.app_id and
 littlechef.fact_new_user.user_id=t.user_id and (littlechef.fact_new_user.facebook_id is null or littlechef.fact_new_user.facebook_id='');


drop table if exists bi_installs;
create temp table bi_installs as
select 
distinct facebook_id,user_id,app_id,install_ts,last_login_date,date_start
 from (select s.app_id,s.user_id,u.last_login_date,u.facebook_id,s.install_ts,date_start,ROW_NUMBER() OVER (PARTITION BY s.app_id,s.facebook_id ORDER BY s.date_start ASC) as rank from littlechef.fact_session s
left join kpi_processed.dim_user u on s.user_key=u.user_key
where s.app_id like 'lc_patch%' and (s.facebook_id is not null and s.facebook_id<>'')) t
where rank=1
;



update littlechef.fact_session set user_id=t.user_id
,install_ts=t.install_ts
,install_date=trunc(t.install_ts)
from bi_installs t where littlechef.fact_session.app_id=t.app_id and littlechef.fact_session.facebook_id=t.facebook_id 

;

update littlechef.fact_session set user_key=md5(app_id||user_id);



update littlechef.fact_revenue set user_id=u.user_id from bi_installs u where littlechef.fact_revenue.funplus_id=u.facebook_id and littlechef.fact_revenue.app_id=u.app_id
and littlechef.fact_revenue.user_id<>u.user_id 
and littlechef.fact_revenue.funplus_id is not null and littlechef.fact_revenue.funplus_id<>'' 
;

update littlechef.fact_revenue set user_key=md5(app_id||user_id);

update littlechef.fact_new_user set user_id=u.user_id,
install_ts=u.install_ts,
install_date=date(u.install_ts) from bi_installs u where littlechef.fact_new_user.facebook_id=u.facebook_id and littlechef.fact_new_user.app_id=u.app_id
and littlechef.fact_new_user.user_id<>u.user_id and
 littlechef.fact_new_user.facebook_id is not null and 
littlechef.fact_new_user.facebook_id<>'' ;


update littlechef.fact_new_user set user_key=md5(app_id||user_id);


------------------------------------------------
--Data 2.0 tmp_user_daily_login.sql
------------------------------------------------

delete from  littlechef.tmp_user_daily_login
where  date  >=  (
                    select start_date 
                    from   littlechef.init_start_date
                  )
and app_id like 'lc_patch%'
;

insert into littlechef.tmp_user_daily_login
SELECT DISTINCT
            s.date_start AS date
           ,s.app_id
           ,s.user_key
           ,s.user_id
           ,last_value(s.ts_start ignore nulls)
               OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_login_ts
           ,last_value(s.facebook_id ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
           ,last_value(s.birthday ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
            ,last_value(s.first_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
            ,last_value(s.last_name ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
           ,substring(last_value(s.email ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_ts ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following)  
               WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts
                     then u.install_ts  
               ELSE
                   min(s.install_ts ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_ts
           ,CASE
               WHEN u.install_ts is null 
                    THEN
                        min(s.install_date ignore nulls)
                        OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                        ROWS BETWEEN unbounded preceding AND unbounded following)
               WHEN s.app_id like 'royal%' and scene='2' then
               min(s.install_date ignore nulls)
                       OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                       ROWS BETWEEN unbounded preceding AND unbounded following) 
                WHEN scene='Main' and
                       min(s.install_ts ignore nulls)
                     OVER (PARTITION BY s.user_key,s.scene ORDER BY s.ts_start ASC
                     ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date
                     then u.install_date    
               ELSE
                   min(s.install_date ignore nulls)
                   OVER (PARTITION BY s.user_key ORDER BY s.ts_start ASC
                   ROWS BETWEEN unbounded preceding AND unbounded following)
            END AS install_date
           ,last_value(s.app_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS app_version
           ,first_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS level_start
           ,nvl(last_value(s.level_end ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following),
                last_value(s.level_start ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following))
           AS level_end
           ,last_value(s.os ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os
           ,last_value(s.os_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS os_version
           ,last_value(s.country ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS country
           ,last_value(s.ip ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ip
           ,last_value(s.install_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS install_source
           ,last_value(s.language ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS language
           ,last_value(s.gender ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
           ,last_value(s.device ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)::varchar(64)
           AS device
           ,last_value(s.browser ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser
           ,last_value(s.browser_version ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS browser_version
           ,sum(1)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene)
            AS session_cnt
           ,sum(playtime_sec)
               OVER (PARTITION BY s.date_start, s.user_key,s.scene )
            AS playtime_sec
            ,s.scene
            ,last_value(s.fb_source ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS fb_source
            ,null as install_source_group
            ,last_value(s.last_ref ignore nulls)
               OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS last_ref
           --,last_value(s.gameserver_id ignore nulls)
             --  OVER (PARTITION BY s.date_start, s.user_key ORDER BY s.ts_start ASC
               -- ROWS BETWEEN unbounded preceding AND unbounded following)
           --AS gameserver_id
FROM      littlechef.fact_session s
LEFT JOIN littlechef.dim_user u
ON        u.user_key=s.user_key
WHERE     s.date_start >= 
                  (
                    select start_date 
                    from   littlechef.init_start_date
                  )
and s.app_id like 'lc_patch%'        
        ;
        


-- Attempt to catch missing users from fact_new_user

INSERT INTO littlechef.tmp_user_daily_login
SELECT DISTINCT
     n.date_start
    ,n.app_id
    ,n.user_key
    ,n.user_id
    ,last_value(n.ts_start ignore nulls)
        OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,last_value(n.facebook_id ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS facebook_id
    ,last_value(n.birthday ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS birthday
     ,last_value(n.first_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS first_name
     ,last_value(n.last_name ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following)
            AS last_name
    ,substring(last_value(n.email ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
               ROWS BETWEEN unbounded preceding AND unbounded following),1,256)
            AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_ts ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN n.app_id like 'royal%' and n.scene='2' then 
            min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    n.scene='Main' and min(n.install_ts ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(n.install_ts ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(n.install_date ignore nulls)
             OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        when n.app_id like 'royal%' and n.scene='2' then
            min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN   n.scene='Main' and  min(n.install_date ignore nulls)
                OVER (PARTITION BY n.user_key,n.scene ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(n.install_date ignore nulls)
            OVER (PARTITION BY n.user_key ORDER BY n.ts_start ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(n.app_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(n.level_start ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_start
    ,last_value(n.level_end ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(n.os ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(n.os_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(n.country ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(n.ip ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(n.install_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(n.language ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,last_value(n.gender ignore nulls)
               OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
                ROWS BETWEEN unbounded preceding AND unbounded following)
           AS gender
    ,last_value(n.device ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(n.browser ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(n.browser_version ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,n.scene
    ,last_value(n.fb_source ignore nulls)
        OVER (PARTITION BY n.date_start, n.user_key ORDER BY n.ts_start ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM littlechef.fact_new_user n
LEFT JOIN littlechef.tmp_user_daily_login s
    ON  s.user_key=n.user_key
    AND s.date=n.date_start
LEFT JOIN littlechef.dim_user u
    ON  u.user_key=s.user_key
WHERE n.date_start >= 
           (
             select start_date 
             from   littlechef.init_start_date
            )
           and n.app_id like 'lc_patch%'
AND s.user_key is null
;



                  
-- Attempt to catch missing users from fact_revenue

INSERT INTO littlechef.tmp_user_daily_login
WITH revenue_session_cnt AS 
(
    SELECT  user_key
           ,date
           ,scene
           ,count(distinct session_id) as session_cnt
    FROM   littlechef.fact_revenue
    WHERE  session_id is not null
    and app_id like 'lc_patch%'
    GROUP  BY 1,2,3
)
SELECT DISTINCT
     p.date
    ,p.app_id
    ,p.user_key
    ,p.user_id
    ,last_value(p.ts ignore nulls)
        OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_login_ts
    ,cast(null as varchar) AS facebook_id
    ,cast(null as date) AS birthday
    ,null AS  first_name
    ,null AS last_name
    ,cast(null as varchar) AS email
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_ts ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_ts ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_ts THEN
            u.install_ts
        ELSE
            min(p.install_ts ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_ts
    ,CASE
        WHEN    u.install_ts is null THEN
            min(p.install_date ignore nulls)
             OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
        WHEN    min(p.install_date ignore nulls)
                OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
                ROWS BETWEEN unbounded preceding AND unbounded following) > u.install_date THEN
            u.install_date
        ELSE
            min(p.install_date ignore nulls)
            OVER (PARTITION BY p.user_key ORDER BY p.ts ASC
            ROWS BETWEEN unbounded preceding AND unbounded following)
    END AS install_date
    ,last_value(p.app_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS app_version
    ,first_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level
    ,last_value(p.level ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS level_end
    ,last_value(p.os ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
    ,last_value(p.os_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os_version
    ,last_value(p.country ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
    ,last_value(p.ip ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS last_ip
    ,last_value(p.install_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
    ,last_value(p.language ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
    ,null AS gender
    ,last_value(p.device ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
        ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
    ,last_value(p.browser ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
     ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
    ,last_value(p.browser_version ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser_version
    ,COALESCE(c.session_cnt,1) AS session_cnt
    ,0 AS playtime_sec
    ,p.scene
    ,last_value(p.fb_source ignore nulls)
        OVER (PARTITION BY p.date, p.user_key ORDER BY p.ts ASC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS fb_source
    ,null as install_source_group
    ,null as last_ref
FROM littlechef.fact_revenue p
LEFT JOIN revenue_session_cnt c
    ON  p.user_key=c.user_key
    AND p.date=c.date 
    and p.scene=c.scene
LEFT JOIN littlechef.tmp_user_daily_login s
    ON  s.user_key=p.user_key
    AND s.date=p.date
LEFT JOIN littlechef.dim_user u
    ON  u.user_key=s.user_key
WHERE p.date >= 
           (
             select start_date 
             from   littlechef.init_start_date
            )
    and p.app_id like 'lc_patch%'
AND s.user_key is null
;



--update the user info where level_end < level_start
update littlechef.tmp_user_daily_login
set level_start = level_end
where level_end < level_start;


--update the install_ts info
create temp table tmp_user_last_day as
select t.*
from
    (
      select *
             ,row_number() OVER (partition by user_key order by date_start desc) as row
      from littlechef.fact_session ds
      where date_start>=current_date-60
          and ds.install_date is not null 
      and ds.app_id like 'lc_patch%'
     ) t
where row = 1;

update littlechef.tmp_user_daily_login
set    install_date = t.install_date,
       install_ts = t.install_ts,
       device = t.device,
       country = t.country,
       language = t.language,
       browser = t.browser,
       browser_version = t.browser_version,
       os = t.os,
       os_version = t.os_version
from   tmp_user_last_day t
where  littlechef.tmp_user_daily_login.install_date is null 
and    littlechef.tmp_user_daily_login.user_key = t.user_key
and littlechef.tmp_user_daily_login.app_id like 'lc_patch%'
;

--if we still have missing install_ts, remove
delete from littlechef.tmp_user_daily_login where install_ts is NULL
and app_id like 'lc_patch%'
  ;


------------------------------------------------
--Data 2.0 tmp_user_payment.sql
------------------------------------------------

delete from littlechef.tmp_user_payment
where date >=
          (
                    select start_date 
                    from   littlechef.init_start_date
          )
          and app_id like 'lc_patch%'
         ;

/*
--KF: This old sql is wrong! The conversion_ts and cumulative payment is totally off because it only looks at payments since start_date. Conversion_ts in dim_user and new payer counts in fact_dau_snapshot turns out okay  because it does a min(conversion_ts) based on this table, but the total revenue will be lower than actual

insert into kpi_processed.tmp_user_payment
select  DISTINCT
        app_id
        ,user_key
        ,date
        ,min(r.ts) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)                      AS conversion_ts
        ,sum(revenue_usd) over (partition by r.user_key, date,scene ORDER BY ts ASC rows between unbounded preceding and unbounded following)         AS revenue_usd
        ,sum(revenue_usd) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)               AS total_revenue_usd
        ,count(transaction_id) over (partition by r.user_key, date,scene ORDER BY ts ASC rows between unbounded preceding and unbounded following )   AS purchase_cnt
        ,count(transaction_id) over (partition by r.user_key ORDER BY ts ASC rows between unbounded preceding and unbounded following)          AS total_purchase_cnt
        ,scene
FROM kpi_processed.fact_revenue r
where date >=
          (
                    select start_date 
                    from   kpi_processed.init_start_date
          )
          --and app_id like '_game_%'
;*/

insert into littlechef.tmp_user_payment
select app_id, a.user_key, a.date, conversion_ts, revenue_usd, total_revenue_usd, purchase_cnt, total_purchase_cnt, a.scene
from
(select app_id, user_key, scene, date, 
sum(nvl(charged_amount*buyer_forex_rate, revenue_usd)) as revenue_usd,
count(1) as purchase_cnt
from littlechef.fact_revenue
where date >=
          (
                    select start_date 
                    from   littlechef.init_start_date
          )
group by 1,2,3,4) a
left join
(select user_key, trunc(ts) as date, 
max(total_revenue_usd) as total_revenue_usd,
max(total_purchase_cnt) as total_purchase_cnt
from
--cumulative revenue until this date
(select user_key, ts, sum(nvl(charged_amount*buyer_forex_rate, revenue_usd)) over (partition by user_key order by ts rows unbounded preceding) as total_revenue_usd, count(1) over (partition by user_key order by ts rows unbounded preceding) as total_purchase_cnt
from littlechef.fact_revenue)
group by 1,2) b
on a.user_key = b.user_key
and a.date = b.date
left join
(select user_key, min(ts) as conversion_ts
from littlechef.fact_revenue
group by 1) c
on a.user_key = c.user_key;

------------------------------------------------
--Data 2.0 fact_dau_snapshot.sql
------------------------------------------------
----- insert data to fact_dau_snapshot
DELETE FROM littlechef.fact_dau_snapshot
WHERE date >= 
           (
             select start_date 
             from littlechef.init_start_date
           )
           and app_id like 'lc_patch%'
           ;

insert into littlechef.fact_dau_snapshot
(
           id                  
           ,user_key            
           ,date                
           ,app_id              
           ,app_version         
           ,level_start         
           ,level_end           
           ,os                  
           ,os_version          
           ,device              
           ,browser             
           ,browser_version     
           ,country             
           ,language            
           ,is_new_user         
           ,is_payer            
           ,is_converted_today  
           ,revenue_usd         
           ,payment_cnt         
           ,session_cnt         
           ,playtime_sec  
           ,scene
           --,gameserver_id
)
select     md5(s.app_id||s.date||s.user_id) as id
          ,s.user_key
          ,s.date
          ,s.app_id
          ,s.app_version
          ,s.level_start
          ,s.level_end
          ,s.os
          ,s.os_version
          ,s.device
          ,s.browser
          ,s.browser_version
          ,s.country
          ,s.language
          ,CASE                                               
                  WHEN s.date=s.install_date THEN           
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_new_user                            
              ,CASE                                         
                  WHEN s.date >= trunc(p.conversion_ts) THEN
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_payer                               
              ,CASE                                         
                  WHEN s.date = trunc(p.conversion_ts) THEN 
                      1                                     
                  ELSE                                      
                      0                                     
              END AS is_converted_today                     
          ,COALESCE(pd.revenue_usd,0) AS revenue_usd    
          ,COALESCE(pd.purchase_cnt,0) AS payment_cnt   
          ,s.session_cnt                                
          ,case
              when s.playtime_sec>1000000 then 1000000
              when s.playtime_sec<0 then 0
              else s.playtime_sec
           end as playtime_sec
           ,s.scene
           --,s.gameserver_id
FROM      littlechef.tmp_user_daily_login s
LEFT JOIN littlechef.tmp_user_payment pd
ON        s.user_key=pd.user_key
AND       s.date=pd.date and s.scene=pd.scene
LEFT JOIN
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
        FROM littlechef.tmp_user_payment
        GROUP BY 1
    ) AS p
ON        p.user_key=s.user_key
WHERE     s.date >= 
                 (
                   select start_date 
                   from littlechef.init_start_date
                 )
                 and s.app_id like 'lc_patch%'
;

-- Attempt to backfill missing row on install date

insert into littlechef.fact_dau_snapshot
(
    id
    ,user_key
    ,date
    ,app_id
    ,app_version
    ,level_start
    ,level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,is_new_user
    ,is_payer
    ,is_converted_today
    ,revenue_usd
    ,payment_cnt
    ,session_cnt
    ,playtime_sec
    ,scene  
)
WITH missing_install_date AS
(
    SELECT    DISTINCT
              l.user_key
             ,l.install_date
    FROM      littlechef.tmp_user_daily_login l
    LEFT JOIN littlechef.fact_dau_snapshot i
    ON        l.user_key = i.user_key
    AND       l.install_date = i.date
    WHERE     l.install_date < (select start_date from littlechef.init_start_date)
    and     l.app_id like 'lc_patch%'
    AND       i.user_key is null
    UNION
    SELECT     DISTINCT
               l.user_key
              ,l.install_date
    FROM      littlechef.tmp_user_daily_login l
    LEFT JOIN littlechef.tmp_user_daily_login i
        ON    l.user_key = i.user_key
        AND   l.install_date = i.date
    WHERE     l.install_date >= (select start_date from littlechef.init_start_date)
    and     l.app_id like 'lc_patch%'
      AND     i.user_key is null
)
SELECT
     MD5(user_key || install_date) AS id
    ,user_key
    ,install_date as date
    ,app_id
    ,app_version
    ,1 AS level_start
    ,level_start as level_end
    ,os
    ,os_version
    ,device
    ,browser
    ,browser_version
    ,country
    ,language
    ,1 AS is_new_user
    ,0 AS is_payer
    ,0 AS is_converted_today
    ,0 AS revenue_usd
    ,0 AS purchase_cnt
    ,1 AS session_cnt
    ,0 AS playtime_sec
    ,scene
FROM
    (
    SELECT
        d.*
        ,m.install_date
        ,row_number() over (partition by d.user_key order by d.date asc) as row
    FROM littlechef.fact_dau_snapshot d
    JOIN missing_install_date m
        ON  d.user_key = m.user_key
    WHERE d.date > m.install_date
    and d.app_id like 'lc_patch%'
    )  t
WHERE t.row=1
;



delete from littlechef.fact_dau_snapshot where date >= current_date;
-------------------------------------------------
--Data 2.0 dim_user.sql
-------------------------------------------------

DROP TABLE IF EXISTS tmp_dim_user;
CREATE TEMP TABLE tmp_dim_user AS
WITH
    last_info AS
    (
        SELECT
            *
            ,row_number() over (partition by user_key order by date desc) as row
        FROM littlechef.tmp_user_daily_login
        where date >= 
                 (
                   select start_date 
                   from littlechef.init_start_date
                 )
        and app_id like 'lc_patch%' 
    )
    ,install_info AS
    (
        SELECT
             distinct user_key
            ,last_value(nullif(language,'') ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS language
            ,last_value(install_source ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source
            ,last_value(install_source_group ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS install_source_group
            ,last_value(country ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS country
            ,last_value(os ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS os
            ,last_value(device ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS device
            ,last_value(browser ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS browser
            ,last_value(gender ignore nulls)
        OVER (PARTITION BY user_key ORDER BY install_ts DESC
         ROWS BETWEEN unbounded preceding AND unbounded following)
    AS gender
        FROM littlechef.tmp_user_daily_login 
        where install_date <= date
        and install_date > date - 15
        and app_id like 'lc_patch%' 
    )
    ,payment_info AS
    (
        SELECT
            user_key
            ,min(conversion_ts) AS conversion_ts
            ,max(total_revenue_usd) as total_revenue_usd
            ,max(total_purchase_cnt) as total_purchase_cnt
        FROM kpi_processed.tmp_user_payment
        where app_id like 'lc_patch%'
        GROUP BY 1
    )

SELECT DISTINCT
  tu.user_key AS id
  ,tu.user_key
  ,tu.app_id
  ,tu.app_version
  ,tu.user_id
  ,COALESCE(du.facebook_id, tu.facebook_id) AS facebook_id
  ,case when tu.app_id like 'royal%' and tu.scene='2' then nvl(du.install_ts, tu.install_ts) else tu.install_ts end as install_ts
  ,case when tu.app_id like 'royal%' and tu.scene='2' then nvl(du.install_date, tu.install_date) else tu.install_date end as install_date
  ,ti.install_source_group
  ,coalesce(nullif(split_part(ti.install_source, '::', 1),''), 'Organic') AS install_source
  ,nullif(split_part(ti.install_source, '::', 3),'') AS install_subpublisher
  ,nullif(split_part(ti.install_source, '::', 2),'') AS install_campaign
  ,coalesce(ti.language,du.install_language) AS install_language
  ,coalesce(ti.country, du.install_country) AS install_country
  ,coalesce(ti.os, du.install_os) AS install_os
  ,coalesce(ti.device, du.install_device) AS install_device
  ,coalesce(ti.browser, du.install_browser) AS install_browser
  ,coalesce(ti.gender, du.install_gender) AS install_gender
  ,tu.language
  ,tu.birthday
  ,tu.first_name
  ,tu.last_name
  ,tu.gender
  ,tu.country
  ,COALESCE (tu.email, du.email) AS email
  ,tu.os
  ,tu.os_version
  ,tu.device
  ,tu.browser
  ,tu.browser_version
  ,COALESCE (tu.last_ip, du.last_ip) AS last_ip
  ,COALESCE(tu.level_end, tu.level_start,du.level) AS level
  ,CASE WHEN COALESCE(tp.total_revenue_usd, 0) > 0 THEN 1 ELSE 0 END AS is_payer
  ,tp.conversion_ts
  ,COALESCE(tp.total_revenue_usd,0) AS revenue_usd
  ,COALESCE(tp.total_purchase_cnt,0) AS payment_cnt
  ,date(tu.last_login_ts) as last_login_date
  ,nullif(split_part(ti.install_source, '::', 4),'') AS install_creative_id
  ,tu.last_ref
FROM last_info tu
LEFT JOIN install_info ti
    ON ti.user_key=tu.user_key
LEFT JOIN littlechef.dim_user du
    ON tu.user_key = du.user_key
LEFT JOIN payment_info tp
    ON tp.user_key=tu.user_key
WHERE tu.row=1
;

DELETE FROM littlechef.dim_user
where user_key in (
  select user_key from littlechef.tmp_user_daily_login 
  where date >= 
                 (
                   select start_date 
                   from littlechef.init_start_date
                 )
    )
and app_id like 'lc_patch%'
;

INSERT INTO littlechef.dim_user
(
   id                  
  ,user_key            
  ,app_id              
  ,app_version         
  ,user_id             
  ,facebook_id         
  ,install_ts          
  ,install_date        
  ,install_source      
  ,install_source_group
  ,install_subpublisher
  ,install_campaign    
  ,install_language    
  ,install_country     
  ,install_os          
  ,install_device      
  ,install_browser     
  ,install_gender      
  ,language            
  ,birthday            
  ,first_name          
  ,last_name           
  ,gender              
  ,country             
  ,email               
  ,os                  
  ,os_version          
  ,device              
  ,browser             
  ,browser_version     
  ,last_ip             
  ,level               
  ,is_payer            
  ,conversion_ts       
  ,revenue_usd         
  ,payment_cnt         
  ,last_login_date
  ,install_creative_id
  ,last_ref
)
SELECT
   id                  
  ,user_key            
  ,app_id              
  ,app_version         
  ,user_id             
  ,facebook_id         
  ,install_ts          
  ,install_date        
  ,case when install_source='appia' then 'Appia'
when install_source like 'app_page%' then 'app_page'
when install_source like 'book%ks' then 'bookmark'
when install_source like 'canvas_bookma%' then 'canvas_bookmark'
when install_source like 'easonboard2%' then 'easonboard2'
when install_source like 'easwelcome%' then 'easwelcome'
when install_source like 'search%' then 'search'
when install_source like 'shortcut%' then 'shortcut'
when install_source like 'sidebar_bo%' then 'sidebar_bookmark'
when install_source like 'timeline/' then 'timeline'
when install_source like 'ticker/' then 'ticker' 
else install_source 
end as install_source       
  ,install_source_group
  ,install_subpublisher
  ,install_campaign    
  ,install_language    
  ,install_country     
  ,install_os          
  ,install_device      
  ,install_browser     
  ,install_gender      
  ,language            
  ,birthday            
  ,first_name          
  ,last_name           
  ,gender              
  ,country             
  ,email               
  ,os                  
  ,os_version          
  ,device              
  ,browser             
  ,browser_version     
  ,last_ip             
  ,level               
  ,is_payer            
  ,conversion_ts       
  ,revenue_usd         
  ,payment_cnt
  ,last_login_date
  ,install_creative_id
  ,last_ref
FROM tmp_dim_user
;

update littlechef.dim_user
set install_source = f.install_source,
    install_campaign = f.install_campaign,
    install_subpublisher =  f.install_subpublisher,
    install_creative_id = f.install_creative_id
from 
	(select * from kpi_processed.fact_install_source 
		where user_key in (select user_key from littlechef.tmp_user_daily_login)
	) f
where littlechef.dim_user.user_key = f.user_key
	---and lower(littlechef.dim_user.install_source) in ('organic','','mobile')
	;

delete from littlechef.dim_user where app_id like 'lc_patch%' and user_key not in (select user_key from littlechef.tmp_user_daily_login);



delete from littlechef.agg_kpi
where date >=(select start_date from littlechef.init_start_date)
and app_id like 'lc_patch%'
;

insert into littlechef.agg_kpi
(
	 date                       
	,app_id                     
	,app_version                             
	,install_source_group       
	,install_source             
	,level_end                  
	,browser                                             
	,country                    
	,os                                          
	,language                   
	,is_new_user                
	,is_payer
    ---,gameserver_id 
	,new_user_cnt               
	,dau_cnt                    
	,newpayer_cnt               
	,payer_today_cnt            
	,payment_cnt                
	,revenue_usd
	,session_cnt
          ,playtime_sec
          ,scene
          ,revenue_iap
          ,revenue_ads
)
select d.date
      ,d.app_id
      ,d.app_version
      ,case when u.app_id like 'farm%' then u.install_source_group
       else u.install_source end as install_source_group
      ,u.install_source
      ,d.level_end      
      ,d.browser
      ,d.country
      ,d.os
      ,d.language
      ,d.is_new_user
      ,d.is_payer
      --,d.gameserver_id
      ,sum(d.is_new_user) as new_user_cnt
      ,count(d.user_key) as dau_cnt
      ,sum(d.is_converted_today) as new_payer_cnt
      ,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
      ,sum(coalesce(d.payment_cnt,0)) as payment_cnt
      ,sum(coalesce(d.revenue_usd,0)) as revenue_usd
      ,sum(coalesce(d.session_cnt,0)) as session_cnt
      ,sum(coalesce(d.playtime_sec,0)) as playtime_sec
      ,d.scene
      ,sum(coalesce(d.revenue_iap,0)) as revenue_iap
      ,sum(coalesce(d.revenue_ads,0)) as revenue_ads
from littlechef.fact_dau_snapshot d
join littlechef.dim_user u on d.user_key=u.user_key
where date >=(select start_date from littlechef.init_start_date)
and d.app_id like 'lc_patch%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,21;


drop table if exists lc_player_day_cube;
CREATE TEMP TABLE lc_player_day_cube AS
WITH last_date AS (SELECT max(date) AS date FROM littlechef.fact_dau_snapshot)
SELECT
    p.day AS player_day,
    d.app_id,
    d.app_version,
    d.install_date,
    d.install_source,
    d.install_subpublisher,
    d.install_campaign,
    null install_creative_id,
    d.install_device as device_alias,
    d.install_os as os,
    d.install_browser as browser,
    d.install_country as country,
    d.install_language as language,
    d.is_payer,
    count(distinct d.user_key) new_user_cnt
FROM littlechef.dim_user d, player_day p, last_date l, littlechef.fact_dau_snapshot dau
where
    d.install_date> DATEADD(day, -363, current_date) and
    DATEDIFF('day', d.install_date, l.date) >= p.day and
    dau.user_key= d.user_key and dau.date=d.install_date
    and dau.scene='Main'
and d.app_id like 'lc_patch%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14;

drop table if exists lc_baseline;

CREATE TEMP TABLE lc_baseline AS
WITH last_date AS (SELECT max(date) AS date FROM littlechef.fact_dau_snapshot)
SELECT
    p.day as player_day,
    u.app_id,
    u.app_version,
    u.install_date,
    u.install_source,
    u.install_subpublisher,
    u.install_campaign,
    u.install_device as device_alias,
    u.install_os AS os,
    u.install_browser AS browser,
    u.install_country AS country,
    u.install_language AS language,
    u.is_payer,
    SUM(case when DATEDIFF('day', u.install_date, d.date) = p.day then 1 else 0 end ) AS retained_user_cnt,
    SUM(d.revenue_usd) AS cumulative_revenue_usd,
    sum(d.is_converted_today) as new_payer_cnt
 FROM littlechef.fact_dau_snapshot d
 JOIN littlechef.dim_user u ON d.user_key=u.user_key
 JOIN player_day p ON DATEDIFF('day', u.install_date, d.date)<=p.day
 join last_date l on 1=1
 WHERE
    u.install_date> DATEADD(day, -363, current_date) AND
    DATEDIFF('day', u.install_date, l.date) >= p.day
    and d.scene='Main'
    and d.app_id like 'lc_patch%'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-- TODO: insert instead of create table

delete from littlechef.agg_retention_ltv
where app_id like 'lc_patch%'
;

insert into littlechef.agg_retention_ltv 
SELECT
    pc.player_day,
    pc.app_id,
    pc.app_version,
    pc.install_date,
    pc.install_source,
    pc.install_subpublisher,
    pc.install_campaign,
    null install_creative_id,
    pc.device_alias,
    pc.os,
    pc.browser,
    pc.country,
    pc.language,
    pc.is_payer,
    pc.new_user_cnt,
    COALESCE(b.retained_user_cnt,0) retained_user_cnt,
    COALESCE(b.cumulative_revenue_usd,0) AS cumulative_revenue_usd,
    COALESCE(b.new_payer_cnt,0) as new_payer_cnt
FROM lc_player_day_cube pc
LEFT JOIN lc_baseline b ON
    pc.player_day = b.player_day AND
    COALESCE(pc.app_id,'') = COALESCE(b.app_id,'') AND
    COALESCE(pc.app_version,'') =COALESCE(b.app_version,'') AND
    pc.install_date = b.install_date AND
    COALESCE(pc.install_source,'') = COALESCE(b.install_source,'') AND
    COALESCE(pc.install_subpublisher,'') = COALESCE(b.install_subpublisher,'') AND
    COALESCE(pc.install_campaign,'') = COALESCE(b.install_campaign,'')  AND
    COALESCE(pc.device_alias,'') = COALESCE(b.device_alias,'') AND
    COALESCE(pc.os,'') = COALESCE(b.os,'') AND
    COALESCE(pc.browser,'') = COALESCE(b.browser,'') AND
    COALESCE(pc.country,'') = COALESCE(b.country,'') AND
    COALESCE(pc.language,'') = COALESCE(b.language,'') AND
    COALESCE(pc.is_payer,0) = COALESCE(b.is_payer,0);


delete from kpi_processed.agg_kpi where app_id like 'lc_patch%' and date>=(
                    select start_date 
                    from   littlechef.init_start_date
                  );


insert into kpi_processed.agg_kpi
select date,
app_id,
app_version,
install_source_group,
install_source,
level_end,
browser,
country,
os,
language,
is_new_user,
is_payer,
new_user_cnt,
dau_cnt,
newpayer_cnt,
payer_today_cnt,
payment_cnt,
revenue_usd,
session_cnt,
playtime_sec,
scene,
revenue_iap,
revenue_ads from littlechef.agg_kpi where app_id like 'lc_patch%' and date>=(
                    select start_date 
                    from   littlechef.init_start_date
                  );


delete from kpi_processed.fact_dau_snapshot where app_id like 'lc_patch%' and date>=(
                    select start_date 
                    from   littlechef.init_start_date
                  );

insert into kpi_processed.fact_dau_snapshot
select id,
user_key,
date,
app_id,
app_version,
level_start,
level_end,
os,
os_version,
device,
browser,
browser_version,
country,
language,
is_new_user,
is_payer,
is_converted_today,
revenue_usd,
payment_cnt,
session_cnt,
playtime_sec,
scene,
revenue_iap,
revenue_ads from  littlechef.fact_dau_snapshot where app_id like 'lc_patch%' and date>=(
                    select start_date 
                    from   littlechef.init_start_date
                  )
;





delete from kpi_processed.agg_retention_ltv where app_id like 'lc_patch%';

insert into kpi_processed.agg_retention_ltv
select player_day,
app_id,
app_version,
install_date,
install_source,
install_subpublisher,
install_campaign,
install_creative_id,
device_alias,
os,
browser,
country,
language,
is_payer,
new_user_cnt,
retained_user_cnt,
cumulative_revenue_usd,
new_payer_cnt from littlechef.agg_retention_ltv
;



delete from littlechef.agg_marketing_kpi
where app like 'lc_patch%'
;

create temp table tmp_new_install_users as
select user_key
    ,app_id as app
    ,user_id as uid
    ,install_date
    ,install_source
    ,case when (install_source like 'FF_%' and app_id like 'farm%') THEN 'Facebook Install Source'
          when (install_source like 'HA_%' and app_id like 'ha%') THEN 'Facebook Install Source'
          when (install_source like 'RS_%' and app_id like 'royal%') THEN 'Facebook Install Source'
          when (install_source like 'Poker_%' and app_id like 'poker%') THEN 'Facebook Install Source'
      else 'Other Install Source' end
     as install_source_group
    ,install_campaign as campaign
    ,install_subpublisher as sub_publisher
    ,install_creative_id as creative_id
    ,install_country as country
    ,install_os as os
    ,level
    ,is_payer
from littlechef.dim_user
where install_date <= current_date 
and app_id like 'lc_patch%'
;

create temp table tmp_all_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
where app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d1_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
    join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 1
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d3_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 3
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d7_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 7
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d15_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 15
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d30_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 30
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d60_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 60
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d90_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 90
and app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d120_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 120
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d150_revenue as
select d.user_key, d.app_id as app, max(d.is_payer) as is_payer, sum(revenue_usd) as revenue
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date <= 150
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d1_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 1
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d3_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 3
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d7_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 7
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d15_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 15
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d30_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 30
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d60_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 60
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d90_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 90
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d120_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 120
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_d150_retained as
select d.user_key, d.app_id as app, 1 as retained
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
where d.date - u.install_date = 150
and d.app_id like 'lc_patch%'
group by d.user_key, d.app_id;

create temp table tmp_session_cnt as
select d.user_key, d.app_id as app, sum(coalesce(d.session_cnt,0)) as session_cnt
from littlechef.fact_dau_snapshot d
     join tmp_new_install_users u on d.user_key = u.user_key
group by d.user_key, d.app_id;

insert into littlechef.agg_marketing_kpi
(
    app
    ,install_date
    ,install_date_str
    ,install_month
    ,install_source
    ,install_source_group
    ,campaign
    ,sub_publisher
    ,creative_id
    ,country
    ,os
    ,level

    ,new_installs
    ,d1_new_installs
    ,d3_new_installs
    ,d7_new_installs
    ,d15_new_installs
    ,d30_new_installs
    ,d60_new_installs
    ,d90_new_installs
    ,d120_new_installs
    ,d150_new_installs

    ,revenue
    ,d1_revenue
    ,d3_revenue
    ,d7_revenue
    ,d15_revenue
    ,d30_revenue
    ,d60_revenue
    ,d90_revenue
    ,d120_revenue
    ,d150_revenue

    ,payers
    ,d1_payers
    ,d3_payers
    ,d7_payers
    ,d15_payers
    ,d30_payers
    ,d60_payers
    ,d90_payers
    ,d120_payers
    ,d150_payers

    ,d1_retained
    ,d3_retained
    ,d7_retained
    ,d15_retained
    ,d30_retained
    ,d60_retained
    ,d90_retained
    ,d120_retained
    ,d150_retained

    ,level_u2
    ,level_a2u5
    ,level_a5u10
    ,level_a10
    ,session_cnt
)
select
    u.app
    ,u.install_date
    ,cast(u.install_date as VARCHAR) as install_date_str
    ,substring(u.install_date, 1, 7) as install_month
    ,u.install_source::VARCHAR(128)
    ,u.install_source_group::VARCHAR(128)
    ,u.campaign::VARCHAR(128)
    ,u.sub_publisher
    ,u.creative_id
    ,u.country
    ,u.os
    ,u.level

    ,count(1) as new_installs
    ,count(1) as d1_new_installs
    ,count(1) as d3_new_installs
    ,count(1) as d7_new_installs
    ,count(1) as d15_new_installs
    ,count(1) as d30_new_installs
    ,count(1) as d60_new_installs
    ,count(1) as d90_new_installs
    ,count(1) as d120_new_installs
    ,count(1) as d150_new_installs

    ,sum(case when r.revenue is null then 0 else r.revenue end) as revenue
    ,sum(case when d1_r.revenue is null then 0 else d1_r.revenue end) as d1_revenue
    ,sum(case when d3_r.revenue is null then 0 else d3_r.revenue end) as d3_revenue
    ,sum(case when d7_r.revenue is null then 0 else d7_r.revenue end) as d7_revenue
    ,sum(case when d15_r.revenue is null then 0 else d15_r.revenue end) as d15_revenue
    ,sum(case when d30_r.revenue is null then 0 else d30_r.revenue end) as d30_revenue
    ,sum(case when d60_r.revenue is null then 0 else d60_r.revenue end) as d60_revenue
    ,sum(case when d90_r.revenue is null then 0 else d90_r.revenue end) as d90_revenue
    ,sum(case when d120_r.revenue is null then 0 else d120_r.revenue end) as d120_revenue
    ,sum(case when d150_r.revenue is null then 0 else d150_r.revenue end) as d150_revenue

    ,sum(case when r.is_payer is null then 0 else r.is_payer end) as payers
    ,sum(case when d1_r.is_payer is null then 0 else d1_r.is_payer end) as d1_payers
    ,sum(case when d3_r.is_payer is null then 0 else d3_r.is_payer end) as d3_payers
    ,sum(case when d7_r.is_payer is null then 0 else d7_r.is_payer end) as d7_payers
    ,sum(case when d15_r.is_payer is null then 0 else d15_r.is_payer end) as d15_payers
    ,sum(case when d30_r.is_payer is null then 0 else d30_r.is_payer end) as d30_payers
    ,sum(case when d60_r.is_payer is null then 0 else d60_r.is_payer end) as d60_payers
    ,sum(case when d90_r.is_payer is null then 0 else d90_r.is_payer end) as d90_payers
    ,sum(case when d120_r.is_payer is null then 0 else d120_r.is_payer end) as d120_payers
    ,sum(case when d150_r.is_payer is null then 0 else d150_r.is_payer end) as d150_payers

    ,sum(case when d1_retained.retained is null then 0 else d1_retained.retained end) as d1_retained
    ,sum(case when d3_retained.retained is null then 0 else d3_retained.retained end) as d3_retained
    ,sum(case when d7_retained.retained is null then 0 else d7_retained.retained end) as d7_retained
    ,sum(case when d15_retained.retained is null then 0 else d15_retained.retained end) as d15_retained
    ,sum(case when d30_retained.retained is null then 0 else d30_retained.retained end) as d30_retained
    ,sum(case when d60_retained.retained is null then 0 else d60_retained.retained end) as d60_retained
    ,sum(case when d90_retained.retained is null then 0 else d90_retained.retained end) as d90_retained
    ,sum(case when d120_retained.retained is null then 0 else d120_retained.retained end) as d120_retained
    ,sum(case when d150_retained.retained is null then 0 else d150_retained.retained end) as d150_retained

    ,sum(case when u.level < 2 then 1 else 0 end) as level_u2
    ,sum(case when u.level >= 2 and u.level < 5 then 1 else 0 end) as level_a2u5
    ,sum(case when u.level >= 5 and u.level < 10 then 1 else 0 end) as level_a5u10
    ,sum(case when u.level >= 10 then 1 else 0 end) as level_a10
    ,sum(case when sc.session_cnt is null then 0 else sc.session_cnt end) as session_cnt
from tmp_new_install_users u
    left join tmp_all_revenue r on u.user_key = r.user_key
    left join tmp_d1_revenue d1_r on u.user_key = d1_r.user_key
    left join tmp_d3_revenue d3_r on u.user_key = d3_r.user_key
    left join tmp_d7_revenue d7_r on u.user_key = d7_r.user_key
    left join tmp_d15_revenue d15_r on u.user_key = d15_r.user_key
    left join tmp_d30_revenue d30_r on u.user_key = d30_r.user_key
    left join tmp_d60_revenue d60_r on u.user_key = d60_r.user_key
    left join tmp_d90_revenue d90_r on u.user_key = d90_r.user_key
    left join tmp_d120_revenue d120_r on u.user_key = d120_r.user_key
    left join tmp_d150_revenue d150_r on u.user_key = d150_r.user_key

    left join tmp_d1_retained d1_retained on u.user_key = d1_retained.user_key
    left join tmp_d3_retained d3_retained on u.user_key = d3_retained.user_key
    left join tmp_d7_retained d7_retained on u.user_key = d7_retained.user_key
    left join tmp_d15_retained d15_retained on u.user_key = d15_retained.user_key
    left join tmp_d30_retained d30_retained on u.user_key = d30_retained.user_key
    left join tmp_d60_retained d60_retained on u.user_key = d60_retained.user_key
    left join tmp_d90_retained d90_retained on u.user_key = d90_retained.user_key
    left join tmp_d120_retained d120_retained on u.user_key = d120_retained.user_key
    left join tmp_d150_retained d150_retained on u.user_key = d150_retained.user_key

    left join tmp_session_cnt sc on u.user_key = sc.user_key
group by 1,2,3,4,5,6,7,8,9,10,11,12;

delete from littlechef.agg_marketing_kpi where install_date < '2014-09-01'
and app like 'lc_patch%'
    ;

update littlechef.agg_marketing_kpi
set d1_new_installs = 0,
    d1_retained = 0,
    d1_revenue = 0,
    d1_payers = 0
where install_date >= CURRENT_DATE - 1
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d3_new_installs = 0,
    d3_retained = 0,
    d3_revenue = 0,
    d3_payers = 0
where install_date >= CURRENT_DATE - 3
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d7_new_installs = 0,
    d7_retained = 0,
    d7_revenue = 0,
    d7_payers = 0
where install_date >= CURRENT_DATE - 7
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d15_new_installs = 0,
    d15_retained = 0,
    d15_revenue = 0,
    d15_payers = 0
where install_date >= CURRENT_DATE - 15
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d30_new_installs = 0,
    d30_retained = 0,
    d30_revenue = 0,
    d30_payers = 0
where install_date >= CURRENT_DATE - 30
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d60_new_installs = 0,
    d60_retained = 0,
    d60_revenue = 0,
    d60_payers = 0
where install_date >= CURRENT_DATE - 60
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d90_new_installs = 0,
    d90_retained = 0,
    d90_revenue = 0,
    d90_payers = 0
where install_date >= CURRENT_DATE - 90
and app like 'lc_patch%'
;

update littlechef.agg_marketing_kpi
set d120_new_installs = 0,
    d120_retained = 0,
    d120_revenue = 0,
    d120_payers = 0
where install_date >= CURRENT_DATE - 120
and app like 'lc_patch%'
;


update littlechef.agg_marketing_kpi
set d150_new_installs = 0,
    d150_retained = 0,
    d150_revenue = 0,
    d150_payers = 0
where install_date >= CURRENT_DATE - 150
and app like 'lc_patch%'
;


update littlechef.agg_marketing_kpi set install_source = 'Facebook Installs' where install_source = 'Off-Facebook Installs' ;
update littlechef.agg_marketing_kpi set install_source = 'AppLift' where install_source = 'Applift';
update littlechef.agg_marketing_kpi set install_source = 'Cheetah Mobile' where install_source = 'Cheetah';



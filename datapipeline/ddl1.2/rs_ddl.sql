------------------------------------------------------------------------------------------------------------------------------------
--RS ddl
--version 1.2
/**
Description:

**/
------------------------------------------------------------------------------------------------------------------------------------
drop table if exists public.events;
CREATE TABLE public.events
(
	id              VARCHAR(64) ENCODE LZO,
	app             VARCHAR(64) ENCODE BYTEDICT,
	ts              TIMESTAMP ENCODE DELTA,
	uid             INTEGER,
	snsid           VARCHAR(64) ENCODE LZO,
	install_ts      TIMESTAMP ENCODE DELTA,
	install_source  VARCHAR(1024) ENCODE BYTEDICT,
	country_code    VARCHAR(16) ENCODE BYTEDICT,
	ip              VARCHAR(16) ENCODE LZO,
	browser         VARCHAR(32) ENCODE BYTEDICT,
	browser_version VARCHAR(32) ENCODE BYTEDICT,
	os              VARCHAR(32) ENCODE BYTEDICT,
	os_version      VARCHAR(64) ENCODE BYTEDICT,
	event           VARCHAR(32) ENCODE BYTEDICT,
	properties      VARCHAR(4096) ENCODE LZO
)
  DISTKEY(snsid)
  SORTKEY(event, ts);

drop table processed.fact_session;
CREATE TABLE processed.fact_session
(
  date_start            DATE NOT NULL ENCODE DELTA,
  user_key              VARCHAR(50) NOT NULL ENCODE LZO,
  app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                   INTEGER NOT NULL,
  snsid                 VARCHAR(64) NOT NULL ENCODE LZO,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(1024) ENCODE BYTEDICT,
  app_version           VARCHAR(20) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts_start              TIMESTAMP ENCODE DELTA,
  ts_end                TIMESTAMP ENCODE DELTA,
  level_start           SMALLINT,
  level_end             SMALLINT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  os_version            VARCHAR(100) ENCODE BYTEDICT,
  country_code          VARCHAR(20) ENCODE BYTEDICT,
  ip                    VARCHAR(20) ENCODE LZO,
  language              VARCHAR(20) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  rc_bal_start          INTEGER,
  rc_bal_end            INTEGER,
  coin_bal_start        INTEGER,
  coin_bal_end          INTEGER,
  ab_test               VARCHAR(1024) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  session_length        INTEGER DEFAULT 0
)
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, app, uid, snsid, session_id);

drop table processed.fact_revenue;
CREATE TABLE processed.fact_revenue
(
  date              DATE NOT NULL ENCODE DELTA,
  user_key          VARCHAR(50) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  ts                TIMESTAMP ENCODE DELTA,
  level             SMALLINT,
  payment_processor VARCHAR(32) ENCODE BYTEDICT,
  product_id        VARCHAR(64) ENCODE BYTEDICT,
  product_name      VARCHAR(64) ENCODE BYTEDICT,
  product_type      VARCHAR(32) ENCODE BYTEDICT,
  coins_in          INTEGER,
  rc_in             INTEGER,
  currency          VARCHAR(32) ENCODE BYTEDICT,
  amount            DECIMAL(14,4) DEFAULT 0,
  usd               DECIMAL(14,4) DEFAULT 0,
  usd_iap           DECIMAL(14,4) DEFAULT 0,
  usd_ads           DECIMAL(14,4) DEFAULT 0,
  transaction_id    VARCHAR
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, app, uid, snsid);

drop table processed.fact_dau_snapshot;
CREATE TABLE processed.fact_dau_snapshot
(
  date                      DATE ENCODE DELTA,
  user_key                  VARCHAR(50) ENCODE LZO,
  app                       VARCHAR(64) ENCODE BYTEDICT,
  uid                       INTEGER,
  snsid                     VARCHAR(64) ENCODE LZO,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_user               SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  revenue_usd_iap           DECIMAL(14,4) DEFAULT 0,
  revenue_usd_ads           DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER DEFAULT 0
)
DISTKEY(user_key)
SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);

drop table processed.dim_user;
CREATE TABLE processed.dim_user
(
  user_key                  VARCHAR(50) NOT NULL ENCODE LZO,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  user_name                 VARCHAR(256) ENCODE LZO,
  email                     VARCHAR(256) ENCODE LZO,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(16) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4) DEFAULT 0,
  payment_cnt               INTEGER DEFAULT 0,
  rc_wallet                 BIGINT DEFAULT 0,
  coin_wallet               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA,
  is_tester                 SMALLINT DEFAULT 0,
  is_whale                  SMALLINT DEFAULT 0
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);

drop table processed.agg_kpi;
CREATE TABLE processed.agg_kpi
(
  date                  DATE NOT NULL ENCODE DELTA,
  app                   VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version           VARCHAR(20) ENCODE BYTEDICT,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  install_source_group  VARCHAR(128) ENCODE BYTEDICT,
  campaign              VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher         VARCHAR(512) ENCODE BYTEDICT,
  creative_id           VARCHAR(512) ENCODE BYTEDICT,
  level_start           SMALLINT,
  level_end             SMALLINT,
  level_range           VARCHAR(32) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  device_alias          VARCHAR(32) ENCODE BYTEDICT,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  country_code          VARCHAR(16) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  os_version            VARCHAR(100) ENCODE BYTEDICT,
  language              VARCHAR(20) ENCODE BYTEDICT,
  is_new_user           SMALLINT,
  is_payer              SMALLINT,
  is_converted_today    SMALLINT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  is_whale              SMALLINT,
  new_installs          INTEGER DEFAULT 0,
  dau                   INTEGER DEFAULT 0,
  new_payers            INTEGER DEFAULT 0,
  today_payers          INTEGER DEFAULT 0,
  revenue               DECIMAL(14,4) DEFAULT 0,
  revenue_iap           DECIMAL(14,4) DEFAULT 0,
  revenue_ads           DECIMAL(14,4) DEFAULT 0,
  session_cnt           INTEGER DEFAULT 0,
  playtime_sec          BIGINT DEFAULT 0
)
  SORTKEY(date, app, os, browser, install_source_group, level_range, install_source, country, is_payer, is_whale);

drop table processed.agg_kpi_history;
CREATE TABLE processed.agg_kpi_history
(
  record_date           DATE NOT NULL ENCODE DELTA,
  date                  DATE NOT NULL ENCODE DELTA,
  app                   VARCHAR(64) ENCODE BYTEDICT,
  new_installs          INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(record_date, date, app);

drop table processed.agg_retention;
CREATE TABLE processed.agg_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(64) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  install_source_group  VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,

  new_users             BIGINT,
  retained              BIGINT
)
  SORTKEY(retention_days, install_date, os, browser, country, app, install_source_group, install_source, is_payer);

drop table processed.agg_ltv;
CREATE TABLE processed.agg_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(64) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  install_source_group  VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,

  users                 BIGINT,
  revenue               DECIMAL(16,4)
)
  SORTKEY(ltv_days, install_date, os, browser, country, app, install_source_group, install_source, is_payer);

drop table if exists processed.tab_marketing_kpi;
CREATE TABLE processed.tab_marketing_kpi
(
    app                     VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    install_date            DATE ENCODE DELTA,
    install_date_str        VARCHAR(10) ENCODE BYTEDICT,
    install_source          VARCHAR(128) DEFAULT NULL ENCODE BYTEDICT,
    install_source_group    VARCHAR(128) DEFAULT NULL ENCODE BYTEDICT,
    campaign                VARCHAR(128) DEFAULT NULL ENCODE LZO,
    sub_publisher           VARCHAR(512) DEFAULT NULL ENCODE LZO,
    creative_id             VARCHAR(512) DEFAULT NULL ENCODE LZO,
    country                 VARCHAR(64) ENCODE BYTEDICT,
    os                      VARCHAR(32) ENCODE BYTEDICT,

    new_installs            INTEGER DEFAULT 0,
    d1_new_installs         INTEGER DEFAULT 0,
    d7_new_installs         INTEGER DEFAULT 0,
    d30_new_installs        INTEGER DEFAULT 0,
    d60_new_installs        INTEGER DEFAULT 0,
    d90_new_installs        INTEGER DEFAULT 0,
    d120_new_installs       INTEGER DEFAULT 0,

    revenue                 DECIMAL(14,4) DEFAULT 0,
    d1_revenue              DECIMAL(14,4) DEFAULT 0,
    d7_revenue              DECIMAL(14,4) DEFAULT 0,
    d30_revenue             DECIMAL(14,4) DEFAULT 0,
    d60_revenue             DECIMAL(14,4) DEFAULT 0,
	d90_revenue             DECIMAL(14,4) DEFAULT 0,
	d120_revenue            DECIMAL(14,4) DEFAULT 0,

    payers                  INTEGER DEFAULT 0,
    d1_payers               INTEGER DEFAULT 0,
    d7_payers               INTEGER DEFAULT 0,
    d30_payers              INTEGER DEFAULT 0,
    d60_payers              INTEGER DEFAULT 0,
    d90_payers              INTEGER DEFAULT 0,
    d120_payers             INTEGER DEFAULT 0,

    d1_retained             INTEGER DEFAULT 0,
    d7_retained             INTEGER DEFAULT 0,
    d30_retained            INTEGER DEFAULT 0,
    d60_retained            INTEGER DEFAULT 0,
    d90_retained            INTEGER DEFAULT 0,
    d120_retained           INTEGER DEFAULT 0,

    cost                    DECIMAL(14,4) DEFAULT 0
)
  SORTKEY(install_date, os, install_source, install_source_group, country, campaign, sub_publisher, creative_id, app);

-- user‘s install source
drop table processed.fact_user_install_source;
CREATE TABLE processed.fact_user_install_source
(
  user_key                      VARCHAR(64) NOT NULL ENCODE LZO,
  app                           VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid                           INTEGER NOT NULL,
  snsid                         VARCHAR(64) NOT NULL ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_event_raw      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event          VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(uid)
  SORTKEY(user_key, app, uid, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign, sub_publisher);


-- install source map
drop table processed.ref_install_source_map;
CREATE TABLE processed.ref_install_source_map
(
	install_source_raw      VARCHAR(1024),
	install_source_lower    VARCHAR(1024),
	install_source          VARCHAR(1024)
)
  SORTKEY(install_source_raw, install_source_lower, install_source);


CREATE TABLE processed.dim_country
(
	country_code VARCHAR(2),
	country VARCHAR(50)
)
SORTKEY(country_code,country);


insert into rs.processed.dim_country
SELECT * FROM rs.public.country;

insert into rs.processed.dim_country
select '--', 'Unknown';

-- Level up event
drop table processed.fact_level_up;
create table rs.processed.fact_level_up (
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level             SMALLINT,
  date_start        DATE ENCODE DELTA,
  date_end          DATE ENCODE DELTA,
  ts_start          TIMESTAMP ENCODE DELTA,
  ts_end            TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, level, date_start, date_end, ts_start, ts_end);

-- rc_transaction
drop table rs.processed.rc_transaction;
CREATE TABLE rs.processed.rc_transaction
(
  date                      DATE NOT NULL ENCODE DELTA,
  transaction_type          VARCHAR(8) ENCODE BYTEDICT,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  level                     SMALLINT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_payer                  SMALLINT,

  location                  VARCHAR(64) ENCODE BYTEDICT,
  action                    VARCHAR(64) ENCODE BYTEDICT,
  action_detail             VARCHAR(256) ENCODE BYTEDICT,

  rc_in                     BIGINT DEFAULT 0,
  rc_out                    BIGINT DEFAULT 0,
  user_cnt                  BIGINT DEFAULT 0
)
SORTKEY(date, transaction_type, app, level, action, action_detail, install_source_group, location);

-- item_transaction
drop table rs.processed.item_transaction;
CREATE TABLE rs.processed.item_transaction
(
  date                      DATE NOT NULL ENCODE DELTA,
  transaction_type          VARCHAR(8) ENCODE BYTEDICT,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  level_range               VARCHAR(32) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_payer                  SMALLINT,

  location                  VARCHAR(64) ENCODE BYTEDICT,
  action                    VARCHAR(64) ENCODE BYTEDICT,
  action_detail             VARCHAR(256) ENCODE BYTEDICT,
  item_type                 VARCHAR(64) ENCODE BYTEDICT,
  item_class                VARCHAR(64) ENCODE BYTEDICT,
  item_name                 VARCHAR(64) ENCODE BYTEDICT,
  item_id                   VARCHAR(64) ENCODE BYTEDICT,

  item_in                   BIGINT DEFAULT 0,
  item_out                  BIGINT DEFAULT 0,
  user_cnt                  BIGINT DEFAULT 0
)
SORTKEY(date, transaction_type, app, install_source_group, level_range, location, action, item_type, item_class, ab_test);

-- agg_iap
drop table processed.agg_iap;
CREATE TABLE processed.agg_iap
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  date_str                  VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) NOT NULL ENCODE BYTEDICT,
  level                     SMALLINT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, os, country,
    install_source_group, install_source);

-- agg_tutorial
drop table processed.agg_tutorial;
CREATE TABLE processed.agg_tutorial
(
  install_date              DATE ENCODE DELTA,
  app                       VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  tutorial_type             VARCHAR(16) ENCODE BYTEDICT,
  step                      VARCHAR(16) ENCODE BYTEDICT,
  step_number               SMALLINT,
  tutorial_installs         INTEGER DEFAULT 0
)
SORTKEY(install_date, app, os, country, install_source_group, install_source, browser, browser_version, tutorial_type, step, step_number);

-- ref_install_source_group
drop table if exists processed.ref_install_source_group;
CREATE TABLE processed.ref_install_source_group
(
	install_source          VARCHAR(1024),
	new_installs            integer,
	dau                     integer,
	install_source_group    VARCHAR(1024)
)
  SORTKEY(install_source, install_source_group);

drop table rs.processed.cheating_list;
CREATE TABLE rs.processed.cheating_list
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  rc_in                     BIGINT DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0
)
SORTKEY(date, app);

-- lapsed_users
drop table if exists rs.processed.lapsed_users;
CREATE TABLE rs.processed.lapsed_users
(
  app                       VARCHAR(64) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  install_source_group      VARCHAR(128) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  level                     SMALLINT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_payer                  SMALLINT,
  last_login_date           DATE ENCODE DELTA,
  lapsed_days               SMALLINT,

  user_cnt                  BIGINT DEFAULT 0
)
SORTKEY(install_date, lapsed_days, app, level, is_payer, install_source_group);

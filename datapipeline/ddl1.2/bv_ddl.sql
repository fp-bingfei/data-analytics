------------------------------------------------------------------------------------------------------------------------------------
--BV ddl 
--version 1.2
/**
Discription:

**/
------------------------------------------------------------------------------------------------------------------------------------
  
CREATE TABLE processed.fact_session
(
  date_start DATE NOT NULL ENCODE DELTA,
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  install_ts timestamp ENCODE DELTA,
  install_date      DATE ENCODE DELTA,
  install_source    VARCHAR(1024) ENCODE BYTEDICT,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  session_id varchar(50) ENCODE LZO,
  ts_start timestamp ENCODE DELTA,
  ts_end timestamp ENCODE DELTA,
  level_start integer,
  level_end integer,
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  country_code VARCHAR(20) ENCODE BYTEDICT default '--',
  ip VARCHAR(20) ENCODE LZO default 'Unknown',
  language VARCHAR(20) ENCODE BYTEDICT default 'Unknown',
  device   VARCHAR(64) ENCODE BYTEDICT default 'Unknown',
  browser VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  browser_version VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  rc_bal_start integer ENCODE BYTEDICT,
  rc_bal_end integer ENCODE BYTEDICT,
  coin_bal_start integer ENCODE BYTEDICT,
  coin_bal_end integer ENCODE BYTEDICT,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  session_length_sec integer
)
  DISTKEY(user_key)
  SORTKEY(user_key,date_start,uid,snsid,app);

CREATE TABLE processed.fact_revenue
(
  date              DATE NOT NULL ENCODE DELTA,
  user_key          varchar(50) NOT NULL ENCODE LZO,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  ts                TIMESTAMP NOT NULL ENCODE DELTA,
  level             SMALLINT ENCODE BYTEDICT,
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  country_code VARCHAR(20) ENCODE BYTEDICT default '--',
  payment_processor VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  product_id        VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  product_name      VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  product_type      VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  coins_in          INTEGER,
  rc_in             INTEGER,
  currency          VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  amount        decimal(14,4) DEFAULT 0,
  usd        decimal(14,4) DEFAULT 0,
  transaction_id    VARCHAR,
  properties        VARCHAR(20000) ENCODE LZO
)
  DISTKEY(user_key)
  SORTKEY(date, ts, user_key, uid, snsid,os,country_code);

 create table bv.processed.fact_level_up (
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level             smallint,
  date_start        DATE ENCODE DELTA,
  date_end          DATE ENCODE DELTA,
  ts_start          timestamp ENCODE DELTA,
  ts_end            timestamp ENCODE DELTA
)
distkey(user_key)
sortkey(user_key,level,date_start,date_end,ts_start,ts_end);

-- user‘s install source
CREATE TABLE bv.processed.fact_user_install_source
(
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_event_raw      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event          VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign_source                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  subpublisher_source                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(user_key)
  SORTKEY(app, user_key,uid, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign_source, subpublisher_source);


-- install source map
CREATE TABLE processed.ref_install_source_map
(
	install_source_raw      VARCHAR(1024),
	install_source_lower    VARCHAR(1024),
	install_source          VARCHAR(1024)
)
  DISTKEY(install_source_raw)
  SORTKEY(install_source_raw, install_source_lower, install_source);

CREATE TABLE processed.fact_dau_snapshot
(
  date DATE NOT NULL ENCODE DELTA,
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  install_ts TIMESTAMP ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  level_start integer,
  level_end integer,
  device   VARCHAR(64) ENCODE BYTEDICT default 'Unknown',
  device_alias  VARCHAR(32) DEFAULT 'Unknown',
  browser VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  browser_version VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT Default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  language VARCHAR(20) ENCODE BYTEDICT default 'Unknown',
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  is_new_user       SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  coins_in          INTEGER DEFAULT 0,
  rc_in             INTEGER DEFAULT 0,
  is_payer       SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  is_converted_today SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  revenue_usd        decimal(14,4) DEFAULT 0,
  purchase_cnt    integer default 0,
  session_cnt       INTEGER DEFAULT 1,
  playtime_sec integer default null
)
DISTKEY(user_key)
  SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);  

CREATE TABLE processed.dim_user
(
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  install_ts timestamp ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  subpublisher_source varchar(500) DEFAULT 'Unkown' ENCODE BYTEDICT,
  campaign_source varchar(100) DEFAULT 'Unkown' ENCODE BYTEDICT,
  language varchar(20) default 'Unknown' ENCODE BYTEDICT,
  birth_date date ENCODE DELTA,
  gender varchar(10) ENCODE BYTEDICT DEFAULT 'Unknown',
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  device  VARCHAR(64) ENCODE BYTEDICT DEFAULT 'Unknown',
  browser VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  browser_version VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  app_version varchar(20) ENCODE LZO,
  level integer,
  is_payer integer,
  conversion_ts timestamp ENCODE DELTA,
  total_revenue_usd decimal(12,4) ENCODE BYTEDICT default 0,
  login_ts timestamp ENCODE DELTA
)
  DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);

CREATE TABLE processed.dim_user_install
(
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  install_ts timestamp ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  subpublisher_source varchar(500) DEFAULT 'Unkown' ENCODE BYTEDICT,
  campaign_source varchar(100) DEFAULT 'Unkown' ENCODE BYTEDICT,
  language varchar(20) default 'Unknown' ENCODE BYTEDICT,
  birth_date date ENCODE DELTA,
  gender varchar(10) ENCODE BYTEDICT default 'Unkown',
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  device  VARCHAR(64) ENCODE BYTEDICT default 'Unkown',
  browser VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  browser_version VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  app_version varchar(20) ENCODE LZO,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None'
)
DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);


CREATE TABLE processed.agg_kpi
(
  date DATE NOT NULL ENCODE DELTA,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) ENCODE BYTEDICT,
  subpublisher_source varchar(500) ENCODE BYTEDICT,
  campaign_source varchar(100) ENCODE BYTEDICT,
  level_end integer,
  device   VARCHAR(64) ENCODE BYTEDICT default 'Unknown',
  device_alias  VARCHAR(32) DEFAULT NULL,
  browser VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  browser_version VARCHAR(32) ENCODE BYTEDICT default 'Unknown',
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  language VARCHAR(20) ENCODE BYTEDICT default 'Unknown',
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  is_new_user SMALLINT ENCODE BYTEDICT,
  is_payer SMALLINT ENCODE BYTEDICT,
  is_converted_today SMALLINT ENCODE BYTEDICT,
  metric_name varchar(30)  ENCODE BYTEDICT,
  metric_value decimal(14,4)  ENCODE BYTEDICT,
  metric_freq integer  ENCODE BYTEDICT
)
  SORTKEY(date, install_date, country_code, os);


-- CREATE TABLE processed.dim_country
-- (
-- 	country_code VARCHAR(2),
-- 	country VARCHAR(50)
-- )
-- DISTKEY(country_code)
-- SORTKEY(country_code,country);
--
--
-- insert into bv.processed.dim_country
-- SELECT * FROM bv.public.country;
--
-- insert into bv.processed.dim_country
-- select '--', 'Unknown';

-- Level up event


create table bv.processed.fact_ledger (
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0,
  detail varchar(180) default null
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);

create table processed.agg_daily_ledger (
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  detail varchar(180) default null,
  in_amount bigint default 0,
  out_amount bigint default 0,
  purchaser_cnt integer default null,
  user_cnt integer default null
)DISTKEY(date)
  SORTKEY(date,level_bin,transaction_type,in_type,in_name,out_type,out_name,detail);

create table bv.processed.fact_item_transaction (
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);

create table bv.processed.agg_item_transaction (
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount bigint default 0,
  out_amount bigint default 0,
  purchaser_cnt integer default null,
  user_cnt integer default null
)DISTKEY(date)
  SORTKEY(date,level_bin,transaction_type,in_type,in_name,out_type,out_name);



CREATE TABLE processed.tab_marketing_kpi
(
    app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
    install_date      DATE ENCODE DELTA,
    install_source    VARCHAR(128) DEFAULT 'Unknown' ENCODE BYTEDICT,
    campaign_source   VARCHAR(128) DEFAULT 'Unknown' ENCODE LZO,
    subpublisher_source     VARCHAR(512) DEFAULT 'Unknown' ENCODE LZO,
    creative_id_source       VARCHAR(512) DEFAULT 'Unknown' ENCODE LZO,
    country           VARCHAR(64) ENCODE BYTEDICT DEFAULT 'Unknown',
    os                VARCHAR(32) ENCODE BYTEDICT DEFAULT 'Unknown',
    new_user_cnt      INTEGER DEFAULT 0,
    d1_new_user_cnt      INTEGER DEFAULT 0,
    d3_new_user_cnt      INTEGER DEFAULT 0,
    d7_new_user_cnt      INTEGER DEFAULT 0,
    d15_new_user_cnt      INTEGER DEFAULT 0,
    d30_new_user_cnt      INTEGER DEFAULT 0,
    d90_new_user_cnt      INTEGER DEFAULT 0,
    revenue_usd           DECIMAL(14,4) DEFAULT 0,
    d7_revenue_usd        DECIMAL(14,4) DEFAULT 0,
    d30_revenue_usd       DECIMAL(14,4) DEFAULT 0,
    d90_revenue_usd       DECIMAL(14,4) DEFAULT 0,
    payer_cnt            INTEGER DEFAULT 0,
    d3_payer_cnt         INTEGER DEFAULT 0,
    d1_retained_user_cnt       INTEGER DEFAULT 0,
    d7_retained_user_cnt       INTEGER DEFAULT 0,
    d15_retained_user_cnt      INTEGER DEFAULT 0,
    d30_retained_user_cnt      INTEGER DEFAULT 0
)
  DISTKEY(install_date)
  SORTKEY(install_date, os, install_source, country, campaign_source, subpublisher_source, creative_id_source, app);


  -- agg_tutorial
CREATE TABLE processed.agg_tutorial
(
  install_date              DATE ENCODE DELTA,
  app                       VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT default 'Unknown',
  country                   VARCHAR(64) ENCODE BYTEDICT,
  device   VARCHAR(64) ENCODE BYTEDICT ,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  language VARCHAR(20) ENCODE BYTEDICT default 'Unknown',
  tutorial_type             VARCHAR(16) ENCODE BYTEDICT,
  step                     smallint ENCODE BYTEDICT,
  user_cnt         INTEGER DEFAULT 0
)
DISTKEY(install_date)
SORTKEY(install_date, app, os, country, install_source, browser, browser_version);

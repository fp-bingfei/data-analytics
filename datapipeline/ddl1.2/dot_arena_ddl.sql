------------------------------------------------------------------------------------------------------------------------------------
--Dot_Arena ddl
--version 1.2
/**
Description:

**/
------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE public.events
(
	load_hour           TIMESTAMP ENCODE DELTA,
	id                  VARCHAR(64) NOT NULL ENCODE LZO,
	app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
	uid                 INTEGER NOT NULL,
	snsid               VARCHAR(32) NOT NULL ENCODE LZO,
	install_ts          TIMESTAMP ENCODE DELTA,
	install_source      VARCHAR(256) ENCODE BYTEDICT,
	country_code        VARCHAR(8) ENCODE BYTEDICT,
	ip                  VARCHAR(16) ENCODE LZO,
	browser             VARCHAR(32) ENCODE BYTEDICT,
	browser_version     VARCHAR(32) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	os_version          VARCHAR(64) ENCODE BYTEDICT,
	event               VARCHAR(64) ENCODE BYTEDICT,
	properties          VARCHAR(1024) ENCODE LZO
)
  DISTKEY(uid)
  SORTKEY(load_hour, event, ts);

drop table processed.fact_session;
CREATE TABLE processed.fact_session
(
  date_start            DATE NOT NULL ENCODE DELTA,
  user_key              VARCHAR(32) NOT NULL ENCODE LZO,
  app                   VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                   INTEGER NOT NULL,
  snsid                 VARCHAR(32) NOT NULL ENCODE LZO,
  player_key            VARCHAR(32) NOT NULL ENCODE LZO,
  server                VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  player_id             INTEGER NOT NULL,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts_start              TIMESTAMP ENCODE DELTA,
  ts_end                TIMESTAMP ENCODE DELTA,
  level_start           SMALLINT,
  level_end             SMALLINT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(8) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(16) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(64) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  rc_bal_start          INTEGER,
  rc_bal_end            INTEGER,
  coin_bal_start        INTEGER,
  coin_bal_end          INTEGER,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  session_length        INTEGER
)
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, player_key, app, uid, server, player_id);

drop table processed.fact_revenue;
CREATE TABLE processed.fact_revenue
(
  date              DATE NOT NULL ENCODE DELTA,
  user_key          VARCHAR(32) NOT NULL ENCODE LZO,
  app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(32) NOT NULL ENCODE LZO,
  player_key        VARCHAR(32) NOT NULL ENCODE LZO,
  server            VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  player_id         INTEGER NOT NULL,
  ts                TIMESTAMP ENCODE DELTA,
  level             SMALLINT,
  payment_processor VARCHAR(32) ENCODE BYTEDICT,
  product_id        VARCHAR(64) ENCODE BYTEDICT,
  product_name      VARCHAR(64) ENCODE BYTEDICT,
  product_type      VARCHAR(32) ENCODE BYTEDICT,
  coins_in          INTEGER,
  rc_in             INTEGER,
  vip_exp_get       INTEGER,
  currency          VARCHAR(32) ENCODE BYTEDICT,
  amount            DECIMAL(14,4) DEFAULT 0,
  usd               DECIMAL(14,4) DEFAULT 0,
  usd_iap           DECIMAL(14,4) DEFAULT 0,
  usd_3rd           DECIMAL(14,4) DEFAULT 0,
  transaction_id    VARCHAR(64) ENCODE LZO
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, player_key, app, uid, server, player_id);

drop table processed.fact_dau_snapshot;
CREATE TABLE processed.fact_dau_snapshot
(
  date                      DATE NOT NULL ENCODE DELTA,
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level                 SMALLINT DEFAULT 0,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_user               SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  vip_exp_get               INTEGER DEFAULT 0,
  total_vip_exp_get         INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  revenue_usd_iap           DECIMAL(14,4) DEFAULT 0,
  revenue_usd_3rd           DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER
)
DISTKEY(user_key)
SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);

drop table processed.fact_player_dau_snapshot;
CREATE TABLE processed.fact_player_dau_snapshot
(
  date                      DATE NOT NULL ENCODE DELTA,
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  player_key                VARCHAR(32) NOT NULL ENCODE LZO,
  server                    VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  player_id                 INTEGER NOT NULL,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level                 SMALLINT DEFAULT 0,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_player             SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  vip_exp_get               INTEGER DEFAULT 0,
  total_vip_exp_get         INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  revenue_usd_iap           DECIMAL(14,4) DEFAULT 0,
  revenue_usd_3rd           DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER
)
DISTKEY(player_key)
SORTKEY(date, player_key, app, server, player_id, install_date, country_code);

drop table processed.dim_user;
CREATE TABLE processed.dim_user
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  server                    VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4) DEFAULT 0,
  total_revenue_usd_iap     DECIMAL(14,4) DEFAULT 0,
  total_revenue_usd_3rd     DECIMAL(14,4) DEFAULT 0,
  total_rc_in               INTEGER DEFAULT 0,
  total_vip_exp_get         INTEGER DEFAULT 0,
  vip_level                 VARCHAR(16) ENCODE BYTEDICT,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);

drop table processed.dim_player;
CREATE TABLE processed.dim_player
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  snsid                     VARCHAR(64) NOT NULL ENCODE LZO,
  player_key                VARCHAR(32) NOT NULL ENCODE LZO,
  server                    VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  player_id                 INTEGER NOT NULL,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT ,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4) DEFAULT 0,
  total_revenue_usd_iap     DECIMAL(14,4) DEFAULT 0,
  total_revenue_usd_3rd     DECIMAL(14,4) DEFAULT 0,
  total_rc_in               INTEGER DEFAULT 0,
  total_vip_exp_get         INTEGER DEFAULT 0,
  vip_level                 VARCHAR(16) ENCODE BYTEDICT,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(player_key)
SORTKEY(player_key, app, server, player_id);

drop table processed.agg_kpi;
CREATE TABLE processed.agg_kpi
(
  date                  DATE NOT NULL ENCODE DELTA,
  app                   VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  app_version           VARCHAR(20) ENCODE BYTEDICT,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher         VARCHAR(128) ENCODE BYTEDICT,
  campaign              VARCHAR(512) ENCODE BYTEDICT,
  creative_id           VARCHAR(512) ENCODE BYTEDICT,
  level_start           SMALLINT,
  level_end             SMALLINT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  device_alias          VARCHAR(32) ENCODE BYTEDICT,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  country_code          VARCHAR(16) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  os_version            VARCHAR(100) ENCODE BYTEDICT,
  language              VARCHAR(20) ENCODE BYTEDICT,
  is_new_user           SMALLINT,
  is_payer              SMALLINT,
  is_converted_today    SMALLINT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,
  new_installs          INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  revenue_iap           DECIMAL(14,4),
  revenue_3rd           DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(date, app, os, install_date, install_source, country, is_payer, vip_level);

drop table processed.agg_kpi_history;
CREATE TABLE processed.agg_kpi_history
(
  record_date           DATE NOT NULL ENCODE DELTA,
  date                  DATE NOT NULL ENCODE DELTA,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  new_installs          INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(record_date, date, os, country);

drop table processed.agg_player_kpi;
CREATE TABLE processed.agg_player_kpi
(
  date                  DATE NOT NULL ENCODE DELTA,
  server                VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  app                   VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  app_version           VARCHAR(20) ENCODE BYTEDICT,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher         VARCHAR(128) ENCODE BYTEDICT,
  campaign              VARCHAR(512) ENCODE BYTEDICT,
  creative_id           VARCHAR(512) ENCODE BYTEDICT,
  level_start           SMALLINT,
  level_end             SMALLINT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  device_alias          VARCHAR(32) ENCODE BYTEDICT,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  country_code          VARCHAR(16) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  os_version            VARCHAR(100) ENCODE BYTEDICT,
  language              VARCHAR(20) ENCODE BYTEDICT,
  is_new_player         SMALLINT,
  is_payer              SMALLINT,
  is_converted_today    SMALLINT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,
  new_players           INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  revenue_iap           DECIMAL(14,4),
  revenue_3rd           DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(date, server, app, os, install_date, install_source, country, is_payer, vip_level);

drop table processed.agg_retention;
CREATE TABLE processed.agg_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  new_users             BIGINT,
  retained              BIGINT
)
  SORTKEY(retention_days, install_date, os, country, app, install_source, is_payer, vip_level);

drop table processed.agg_player_retention;
CREATE TABLE processed.agg_player_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  server                VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  new_players           BIGINT,
  retained              BIGINT
)
  SORTKEY(retention_days, install_date, server, os, country, app, install_source, is_payer, vip_level);

drop table processed.agg_ltv;
CREATE TABLE processed.agg_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  users                 BIGINT,
  revenue               DECIMAL(16,4)
)
  SORTKEY(ltv_days, install_date, os, country, app, install_source, is_payer, vip_level);

drop table processed.agg_player_ltv;
CREATE TABLE processed.agg_player_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  server                VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  players               BIGINT,
  revenue               DECIMAL(16,4)
)
  SORTKEY(ltv_days, install_date, server, os, country, app, install_source, is_payer, vip_level);

drop table if exists processed.tab_marketing_kpi;
CREATE TABLE processed.tab_marketing_kpi
(
    app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    install_date      DATE ENCODE DELTA,
    install_date_str  VARCHAR(10) ENCODE BYTEDICT,
    install_source    VARCHAR(128) DEFAULT NULL ENCODE BYTEDICT,
    campaign          VARCHAR(128) DEFAULT NULL ENCODE LZO,
    sub_publisher     VARCHAR(512) DEFAULT NULL ENCODE LZO,
    creative_id       VARCHAR(512) DEFAULT NULL ENCODE LZO,
    country           VARCHAR(64) ENCODE BYTEDICT,
    os                VARCHAR(32) ENCODE BYTEDICT,

    new_installs      INTEGER DEFAULT 0,
    d1_new_installs   INTEGER DEFAULT 0,
    d7_new_installs   INTEGER DEFAULT 0,
    d30_new_installs  INTEGER DEFAULT 0,
    d60_new_installs  INTEGER DEFAULT 0,
    d90_new_installs  INTEGER DEFAULT 0,
    d120_new_installs INTEGER DEFAULT 0,

    revenue           DECIMAL(14,4) DEFAULT 0,
    d1_revenue        DECIMAL(14,4) DEFAULT 0,
    d7_revenue        DECIMAL(14,4) DEFAULT 0,
    d30_revenue       DECIMAL(14,4) DEFAULT 0,
    d60_revenue       DECIMAL(14,4) DEFAULT 0,
	d90_revenue       DECIMAL(14,4) DEFAULT 0,
	d120_revenue      DECIMAL(14,4) DEFAULT 0,

    payers            INTEGER DEFAULT 0,
    d1_payers         INTEGER DEFAULT 0,
    d7_payers         INTEGER DEFAULT 0,
    d30_payers        INTEGER DEFAULT 0,
    d60_payers        INTEGER DEFAULT 0,
    d90_payers        INTEGER DEFAULT 0,
    d120_payers       INTEGER DEFAULT 0,

    d1_retained       INTEGER DEFAULT 0,
    d7_retained       INTEGER DEFAULT 0,
    d30_retained      INTEGER DEFAULT 0,
    d60_retained      INTEGER DEFAULT 0,
    d90_retained      INTEGER DEFAULT 0,
    d120_retained     INTEGER DEFAULT 0,

    cost              DECIMAL(14,4) DEFAULT 0
)
  SORTKEY(install_date, os, install_source, country, campaign, sub_publisher, creative_id, app);

-- user‘s install source
drop table processed.fact_user_install_source;
CREATE TABLE processed.fact_user_install_source
(
  user_key                      VARCHAR(32) NOT NULL ENCODE LZO,
  app                           VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                           INTEGER NOT NULL,
  install_date                  DATE ENCODE DELTA,
  install_source_kochava_raw    VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_kochava        VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_mat_raw        VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_mat            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(user_key)
  SORTKEY(user_key, app, uid, install_date, install_source_kochava, install_source_mat, install_source_adjust, install_source, campaign, sub_publisher);


-- install source map
drop table processed.ref_install_source_map;
CREATE TABLE processed.ref_install_source_map
(
	install_source_raw      VARCHAR(1024),
	install_source_lower    VARCHAR(1024),
	install_source          VARCHAR(1024),
	bi_install_source       VARCHAR(1024)
)
  DISTKEY(install_source_raw)
  SORTKEY(install_source_raw, install_source_lower, install_source, bi_install_source);


CREATE TABLE processed.dim_country
(
	country_code VARCHAR(2),
	country VARCHAR(50)
)
DISTKEY(country_code)
SORTKEY(country_code, country);

insert into daota.processed.dim_country
SELECT * FROM daota.public.country;

insert into daota.processed.dim_country
select '--', 'Unknown';

-- Level up event
drop table daota.processed.fact_level_up;
create table daota.processed.fact_level_up (
  user_key          VARCHAR(32) NOT NULL ENCODE LZO,
  app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level             SMALLINT,
  date_start        DATE ENCODE DELTA,
  date_end          DATE ENCODE DELTA,
  ts_start          TIMESTAMP ENCODE DELTA,
  ts_end            TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, level, date_start, date_end, ts_start, ts_end);

-- agg_iap
drop table processed.agg_iap;
CREATE TABLE processed.agg_iap
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  date_str                  VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) NOT NULL ENCODE BYTEDICT,
  level                     SMALLINT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
DISTKEY(date)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, os, country, install_source);

-- agg_player_iap
drop table processed.agg_player_iap;
CREATE TABLE processed.agg_player_iap
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  date_str                  VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) NOT NULL ENCODE BYTEDICT,
  level                     SMALLINT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  server                    VARCHAR(8) NOT NULL ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
DISTKEY(date)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, server, os, country, install_source);

drop table if exists processed.dim_third_part_payment_product;
CREATE TABLE processed.dim_third_part_payment_product
(
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  product_name              VARCHAR(64) ENCODE BYTEDICT,
  product_type              VARCHAR(32) ENCODE BYTEDICT,
  payment_processor         VARCHAR(32) ENCODE BYTEDICT,
  type                      VARCHAR(64) ENCODE BYTEDICT,
  currency                  VARCHAR(32) ENCODE BYTEDICT,
  amount                    DECIMAL(14,4) DEFAULT 0,
  usd                       DECIMAL(14,4) DEFAULT 0,
  rc_in                     INTEGER
)
  DISTKEY(product_id)
  SORTKEY(product_id);

insert into processed.dim_third_part_payment_product
(
    product_id
    ,product_name
    ,product_type
    ,payment_processor
    ,type
    ,currency
    ,amount
    ,usd
    ,rc_in
)
select
    id AS product_id
    ,display_name AS product_name
    ,null AS product_type
    ,'app_third' AS payment_processor
    ,type
    ,'USD' AS currency
    ,CAST(cost AS DECIMAL(14,4)) AS amount
    ,CAST(cost AS DECIMAL(14,4)) AS usd
    ,CAST(final_diamond AS INTEGER) as rc_in
from processed.third_part_payment_price;

-- daily level
drop table daota.processed.daily_level;
create table daota.processed.daily_level (
  date              DATE NOT NULL ENCODE DELTA,
  user_key          VARCHAR(32) NOT NULL ENCODE LZO,
  app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  min_level         SMALLINT,
  max_level         SMALLINT
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);

-- eas_user_info
drop table if exists processed.eas_user_info;
CREATE TABLE processed.eas_user_info
(
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  snsid                     VARCHAR(64) ENCODE LZO,
  user_name                 VARCHAR(64) ENCODE LZO,
  email                     VARCHAR(64) ENCODE LZO,
  additional_email          VARCHAR(64) ENCODE LZO,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  level                     SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  last_payment_ts           TIMESTAMP ENCODE DELTA,
  payment_cnt               BIGINT DEFAULT 0,
  rc                        BIGINT DEFAULT 0,
  coins                     BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(uid)
SORTKEY(uid, app);

















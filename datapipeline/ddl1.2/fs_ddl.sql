CREATE TABLE processed.agg_kpi
(
	id VARCHAR(128) NOT NULL ENCODE bytedict,
	date DATE ENCODE delta,
	app_id VARCHAR(64) ENCODE bytedict,
	app_version VARCHAR(20) ENCODE bytedict,
	install_year VARCHAR(10) ENCODE bytedict,
	install_month VARCHAR(10) ENCODE bytedict,
	install_week VARCHAR(30) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	level_end INTEGER,
	browser VARCHAR(32) ENCODE bytedict,
	browser_version VARCHAR(64) ENCODE bytedict,
	device_alias VARCHAR(64) ENCODE bytedict,
	country VARCHAR(100) ENCODE bytedict,
	os VARCHAR(32) ENCODE bytedict,
	os_version VARCHAR(100) ENCODE bytedict,
	language VARCHAR(8) ENCODE bytedict,
	is_new_user INTEGER,
	is_payer INTEGER,
	new_user_cnt INTEGER,
	dau_cnt INTEGER,
	newpayer_cnt INTEGER,
	payer_today_cnt INTEGER,
	payment_cnt INTEGER,
	revenue_usd NUMERIC(14, 4),
	session_cnt INTEGER,
	session_length_sec INTEGER
)
DISTKEY(date)
SORTKEY(date,app_id,country,os);

ALTER TABLE processed.agg_kpi
ADD CONSTRAINT agg_kpi_pk
PRIMARY KEY (id);

CREATE TABLE processed.agg_iap
(
	date DATE ENCODE delta,
	app_id VARCHAR(64) ENCODE bytedict,
	app_version VARCHAR(20) ENCODE bytedict,
	level INTEGER,
	install_date DATE ENCODE delta,
	install_source VARCHAR(1024) ENCODE bytedict,
	country VARCHAR(100),
	os VARCHAR(32) ENCODE bytedict,
	browser VARCHAR(32) ENCODE bytedict,
	browser_version VARCHAR(64) ENCODE bytedict,
	language VARCHAR(8) ENCODE bytedict,
	is_conversion_purchase smallint,
	product_id VARCHAR(64) ENCODE bytedict,
	product_type VARCHAR(64) ENCODE bytedict,
	revenue_usd NUMERIC(38, 4) ENCODE bytedict,
	purchase_cnt integer ,
	purchase_user_cnt integer
)
DISTKEY(date)
SORTKEY(date,app_id,country,os);



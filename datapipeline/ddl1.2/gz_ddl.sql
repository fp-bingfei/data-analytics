------------------------------------------------------------------------------------------------------------------------------------
--GalaxyStorm ddl
--version 1.2
/**
Description:

**/
------------------------------------------------------------------------------------------------------------------------------------
drop table if exists processed.raw_data_s3;
create table processed.raw_data_s3
(
	json_str          VARCHAR(20000) ENCODE LZO,
	load_hour         TIMESTAMP ENCODE DELTA
);

drop table if exists processed.raw_data_s3_ar;
create table processed.raw_data_s3_ar
(
	json_str          VARCHAR(20000) ENCODE LZO,
	load_hour         TIMESTAMP ENCODE DELTA
);

drop table if exists processed.events_raw;
CREATE TABLE processed.events_raw
(
	load_hour           TIMESTAMP ENCODE DELTA,
	id                  VARCHAR(64) NOT NULL ENCODE LZO,
	app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
	uid                 VARCHAR(32) NOT NULL ENCODE LZO,
	snsid               VARCHAR(64) NOT NULL ENCODE LZO,
	install_ts          TIMESTAMP ENCODE DELTA,
	install_source      VARCHAR(256) ENCODE BYTEDICT,
	country_code        VARCHAR(8) ENCODE BYTEDICT,
	ip                  VARCHAR(64) ENCODE LZO,
	level               VARCHAR(16) ENCODE BYTEDICT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	language            VARCHAR(16) ENCODE BYTEDICT,
	device              VARCHAR(64) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	os_version          VARCHAR(64) ENCODE BYTEDICT,
	event               VARCHAR(64) ENCODE BYTEDICT,
	properties          VARCHAR(4096) ENCODE LZO
)
  DISTKEY(uid)
  SORTKEY(load_hour, event, ts);

drop table processed.fact_session;
CREATE TABLE processed.fact_session
(
  date_start            DATE ENCODE DELTA,
  user_key              VARCHAR(32) ENCODE LZO,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  uid                   VARCHAR(32) ENCODE LZO,
  snsid                 VARCHAR(64) ENCODE LZO,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts_start              TIMESTAMP ENCODE DELTA,
  ts_end                TIMESTAMP ENCODE DELTA,
  level_start           SMALLINT,
  level_end             SMALLINT,
  vip_level_start       SMALLINT,
  vip_level_end         SMALLINT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(8) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(16) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  rc_bal_start          INTEGER,
  rc_bal_end            INTEGER,
  coin_bal_start        INTEGER,
  coin_bal_end          INTEGER,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  session_length        INTEGER
)
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, player_key, app, uid, server, player_id);

drop table processed.fact_revenue;
CREATE TABLE processed.fact_revenue
(
  date              DATE ENCODE DELTA,
  user_key          VARCHAR(32) ENCODE LZO,
  app               VARCHAR(32) ENCODE BYTEDICT,
  uid               VARCHAR(32) ENCODE LZO,
  snsid             VARCHAR(64) ENCODE LZO,
  player_key        VARCHAR(32) ENCODE LZO,
  server            VARCHAR(8) ENCODE BYTEDICT,
  player_id         INTEGER,
  ts                TIMESTAMP ENCODE DELTA,
  level             SMALLINT,
  vip_level         SMALLINT,
  payment_processor VARCHAR(32) ENCODE BYTEDICT,
  product_id        VARCHAR(64) ENCODE BYTEDICT,
  product_name      VARCHAR(64) ENCODE BYTEDICT,
  product_type      VARCHAR(32) ENCODE BYTEDICT,
  coins_in          INTEGER,
  rc_in             INTEGER,
  currency          VARCHAR(32) ENCODE BYTEDICT,
  amount            DECIMAL(14,4) DEFAULT 0,
  usd               DECIMAL(14,4) DEFAULT 0,
  usd_iap           DECIMAL(14,4) DEFAULT 0,
  usd_3rd           DECIMAL(14,4) DEFAULT 0,
  transaction_id    VARCHAR(64) ENCODE LZO
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, player_key, app, uid, server, player_id);

drop table processed.fact_dau_snapshot;
CREATE TABLE processed.fact_dau_snapshot
(
  date                      DATE ENCODE DELTA,
  user_key                  VARCHAR(32) ENCODE LZO,
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  snsid                     VARCHAR(64) ENCODE LZO,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           SMALLINT,
  vip_level_end             SMALLINT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_user               SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  revenue_usd_iap           DECIMAL(14,4) DEFAULT 0,
  revenue_usd_3rd           DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER
)
DISTKEY(user_key)
SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);

drop table processed.fact_player_dau_snapshot;
CREATE TABLE processed.fact_player_dau_snapshot
(
  date                      DATE ENCODE DELTA,
  user_key                  VARCHAR(32) ENCODE LZO,
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  snsid                     VARCHAR(64) ENCODE LZO,
  player_key                VARCHAR(32) ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  player_id                 INTEGER,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           SMALLINT,
  vip_level_end             SMALLINT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_player             SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  revenue_usd_iap           DECIMAL(14,4) DEFAULT 0,
  revenue_usd_3rd           DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER
)
DISTKEY(player_key)
SORTKEY(date, player_key, app, server, player_id, install_date, country_code);

drop table processed.dim_user;
CREATE TABLE processed.dim_user
(
  user_key                  VARCHAR(32) ENCODE LZO,
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  snsid                     VARCHAR(64) ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level                 SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_revenue_usd_iap     DECIMAL(14,4),
  total_revenue_usd_3rd     DECIMAL(14,4),
  total_rc_in               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);

drop table processed.dim_player;
CREATE TABLE processed.dim_player
(
  user_key                  VARCHAR(32) ENCODE LZO,
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  snsid                     VARCHAR(64) ENCODE LZO,
  player_key                VARCHAR(32) ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  player_id                 INTEGER,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level                 SMALLINT ,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_revenue_usd_iap     DECIMAL(14,4),
  total_revenue_usd_3rd     DECIMAL(14,4),
  total_rc_in               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(player_key)
SORTKEY(player_key, app, server, player_id);

drop table processed.agg_kpi;
CREATE TABLE processed.agg_kpi
(
  date                  DATE ENCODE DELTA,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  app_version           VARCHAR(20) ENCODE BYTEDICT,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher         VARCHAR(128) ENCODE BYTEDICT,
  campaign              VARCHAR(512) ENCODE BYTEDICT,
  creative_id           VARCHAR(512) ENCODE BYTEDICT,
  level_start           SMALLINT,
  level_end             SMALLINT,
  vip_level_start       SMALLINT,
  vip_level_end         SMALLINT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  device_alias          VARCHAR(32) ENCODE BYTEDICT,
  country_code          VARCHAR(16) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  os_version            VARCHAR(100) ENCODE BYTEDICT,
  language              VARCHAR(20) ENCODE BYTEDICT,
  is_new_user           SMALLINT,
  is_payer              SMALLINT,
  is_converted_today    SMALLINT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  new_installs          INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  revenue_iap           DECIMAL(14,4),
  revenue_3rd           DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(date, app, os, install_date, install_source, country, is_payer);

drop table processed.agg_kpi_history;
CREATE TABLE processed.agg_kpi_history
(
  record_date           DATE ENCODE DELTA,
  date                  DATE ENCODE DELTA,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  new_installs          INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(record_date, date, os, country);

drop table processed.agg_player_kpi;
CREATE TABLE processed.agg_player_kpi
(
  date                  DATE ENCODE DELTA,
  server                VARCHAR(8) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  app_version           VARCHAR(20) ENCODE BYTEDICT,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher         VARCHAR(128) ENCODE BYTEDICT,
  campaign              VARCHAR(512) ENCODE BYTEDICT,
  creative_id           VARCHAR(512) ENCODE BYTEDICT,
  level_start           SMALLINT,
  level_end             SMALLINT,
  vip_level_start       SMALLINT,
  vip_level_end         SMALLINT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  device_alias          VARCHAR(32) ENCODE BYTEDICT,
  country_code          VARCHAR(16) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  os_version            VARCHAR(100) ENCODE BYTEDICT,
  language              VARCHAR(20) ENCODE BYTEDICT,
  is_new_player         SMALLINT,
  is_payer              SMALLINT,
  is_converted_today    SMALLINT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  new_players           INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  revenue_iap           DECIMAL(14,4),
  revenue_3rd           DECIMAL(14,4),
  session_cnt           INTEGER
)
  SORTKEY(date, server, app, os, install_date, install_source, country, is_payer);

drop table processed.agg_retention;
CREATE TABLE processed.agg_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,

  new_users             BIGINT,
  retained              BIGINT
)
  SORTKEY(retention_days, install_date, os, country, app, install_source, is_payer);

drop table processed.agg_player_retention;
CREATE TABLE processed.agg_player_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  server                VARCHAR(8) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,

  new_players           BIGINT,
  retained              BIGINT
)
  SORTKEY(retention_days, install_date, server, os, country, app, install_source, is_payer);

drop table processed.agg_ltv;
CREATE TABLE processed.agg_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,

  users                 BIGINT,
  revenue               DECIMAL(16,4)
)
  SORTKEY(ltv_days, install_date, os, country, app, install_source, is_payer);

drop table processed.agg_player_ltv;
CREATE TABLE processed.agg_player_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  server                VARCHAR(8) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,

  players               BIGINT,
  revenue               DECIMAL(16,4)
)
  SORTKEY(ltv_days, install_date, server, os, country, app, install_source, is_payer);

drop table if exists processed.tab_marketing_kpi;
CREATE TABLE processed.tab_marketing_kpi
(
    app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    install_date      DATE ENCODE DELTA,
    install_date_str  VARCHAR(10) ENCODE BYTEDICT,
    install_source    VARCHAR(128) DEFAULT NULL ENCODE BYTEDICT,
    campaign          VARCHAR(128) DEFAULT NULL ENCODE LZO,
    sub_publisher     VARCHAR(512) DEFAULT NULL ENCODE LZO,
    creative_id       VARCHAR(512) DEFAULT NULL ENCODE LZO,
    country           VARCHAR(64) ENCODE BYTEDICT,
    os                VARCHAR(32) ENCODE BYTEDICT,

    new_installs      INTEGER DEFAULT 0,
    d1_new_installs   INTEGER DEFAULT 0,
    d7_new_installs   INTEGER DEFAULT 0,
    d30_new_installs  INTEGER DEFAULT 0,
    d60_new_installs  INTEGER DEFAULT 0,
    d90_new_installs  INTEGER DEFAULT 0,
    d120_new_installs INTEGER DEFAULT 0,

    revenue           DECIMAL(14,4) DEFAULT 0,
    d1_revenue        DECIMAL(14,4) DEFAULT 0,
    d7_revenue        DECIMAL(14,4) DEFAULT 0,
    d30_revenue       DECIMAL(14,4) DEFAULT 0,
    d60_revenue       DECIMAL(14,4) DEFAULT 0,
	d90_revenue       DECIMAL(14,4) DEFAULT 0,
	d120_revenue      DECIMAL(14,4) DEFAULT 0,

    payers            INTEGER DEFAULT 0,
    d1_payers         INTEGER DEFAULT 0,
    d7_payers         INTEGER DEFAULT 0,
    d30_payers        INTEGER DEFAULT 0,
    d60_payers        INTEGER DEFAULT 0,
    d90_payers        INTEGER DEFAULT 0,
    d120_payers       INTEGER DEFAULT 0,

    d1_retained       INTEGER DEFAULT 0,
    d7_retained       INTEGER DEFAULT 0,
    d30_retained      INTEGER DEFAULT 0,
    d60_retained      INTEGER DEFAULT 0,
    d90_retained      INTEGER DEFAULT 0,
    d120_retained     INTEGER DEFAULT 0,

    cost              DECIMAL(14,4) DEFAULT 0
)
  SORTKEY(install_date, os, install_source, country, campaign, sub_publisher, creative_id, app);

-- user‘s install source
drop table processed.fact_user_install_source;
CREATE TABLE processed.fact_user_install_source
(
  user_key                      VARCHAR(64) ENCODE LZO,
  app                           VARCHAR(64) ENCODE BYTEDICT,
  uid                           VARCHAR(32) ENCODE LZO,
  snsid                         VARCHAR(64) ENCODE LZO,
  install_date                  DATE ENCODE DELTA,
  install_source_event_raw      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event          VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(uid)
  SORTKEY(user_key, app, uid, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign, sub_publisher, creative_id);


-- install source map
drop table processed.ref_install_source_map;
CREATE TABLE processed.ref_install_source_map
(
	install_source_raw      VARCHAR(1024),
	install_source_lower    VARCHAR(1024),
	install_source          VARCHAR(1024)
)
  DISTKEY(install_source_raw)
  SORTKEY(install_source_raw, install_source_lower, install_source);


CREATE TABLE processed.dim_country
(
	country_code VARCHAR(2),
	country VARCHAR(50)
)
DISTKEY(country_code)
SORTKEY(country_code, country);

insert into gz.processed.dim_country
SELECT * FROM gz.public.country;

insert into gz.processed.dim_country
select '--', 'Unknown';

-- Level up event
drop table gz.processed.fact_level_up;
create table gz.processed.fact_level_up (
  user_key          VARCHAR(32) ENCODE LZO,
  app               VARCHAR(32) ENCODE BYTEDICT,
  uid               VARCHAR(32) ENCODE LZO,
  snsid             VARCHAR(64) ENCODE LZO,
  level             SMALLINT,
  date_start        DATE ENCODE DELTA,
  date_end          DATE ENCODE DELTA,
  ts_start          TIMESTAMP ENCODE DELTA,
  ts_end            TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, level, date_start, date_end, ts_start, ts_end);

-- agg_iap
drop table processed.agg_iap;
CREATE TABLE processed.agg_iap
(
  date                      DATE ENCODE DELTA,
  app                       VARCHAR(32) ENCODE BYTEDICT,
  date_str                  VARCHAR(10) ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) ENCODE BYTEDICT,
  level                     SMALLINT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  payment_processor         VARCHAR(32) ENCODE BYTEDICT,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
DISTKEY(date)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, os, country, install_source);

-- agg_player_iap
drop table processed.agg_player_iap;
CREATE TABLE processed.agg_player_iap
(
  date                      DATE ENCODE DELTA,
  app                       VARCHAR(32) ENCODE BYTEDICT,
  date_str                  VARCHAR(10) ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) ENCODE BYTEDICT,
  level                     SMALLINT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  payment_processor         VARCHAR(32) ENCODE BYTEDICT,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
DISTKEY(date)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, server, os, country, install_source);


-- daily level
drop table gz.processed.daily_level;
create table gz.processed.daily_level (
  date              DATE ENCODE DELTA,
  user_key          VARCHAR(32) ENCODE LZO,
  app               VARCHAR(32) ENCODE BYTEDICT,
  uid               VARCHAR(32) ENCODE LZO,
  snsid             VARCHAR(64) ENCODE LZO,
  min_level         SMALLINT,
  max_level         SMALLINT
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, snsid);

-- TODO: in game table
-- rc_transaction
CREATE TABLE processed.rc_transaction
(
   date             date,
   ts               timestamp,
   app              varchar(64),
   uid              varchar(100),
   snsid            varchar(64),
   user_key         varchar(50),
   install_date     date,
   country_code     varchar(50),
   level            integer,
   viplevel         integer,
   os               varchar(10),
   rc_in            integer,
   rc_out           integer,
   action           varchar(200),
   rc_bal           integer,
   is_gift          integer,
   location         varchar(200),
   install_source   varchar(300),
   device           varchar(100),
   lang             varchar(100),
   gifted_by        varchar(20),
   gifted_to        varchar(20)
);

-- coins_transaction
CREATE TABLE processed.coins_transaction
(
   date             date,
   ts               timestamp,
   app              varchar(64),
   uid              varchar(100),
   snsid            varchar(64),
   user_key         varchar(50),
   install_date     date,
   country_code     varchar(50),
   level            integer,
   viplevel         integer,
   os               varchar(10),
   coins_in         integer,
   coins_out        integer,
   action           varchar(200),
   coins_bal        integer,
   is_gift          integer,
   location         varchar(200),
   install_source   varchar(300),
   device           varchar(100),
   lang             varchar(100),
   gifted_by        varchar(20),
   gifted_to        varchar(20)
);

CREATE TABLE processed.agg_rc_stats
(
   date             date,
   app              varchar(64),
   install_date     date,
   country          varchar(64),
   viplevel         integer,
   os               varchar(30),
   level            integer,
   action           varchar(200),
   rc_in            integer,
   rc_out           integer,
   invite_num       integer
);



create table processed.agg_coins_stats
(
  date              date,
  app               varchar(64),
  install_date      date,
  country           varchar(64),
  viplevel          integer,
  os                varchar(30),
  level             integer,
  action            varchar(200),
  coins_in          integer,
  coins_out         integer,
  invite_num        integer
);


drop table if exists processed.item_transaction;
CREATE TABLE processed.item_transaction
(
   date            date,
   ts              TIMESTAMP ENCODE DELTA,
   app             varchar(64),
   uid             varchar(100),
   snsid           varchar(64),
   user_key        varchar(50),
   install_date    date,
   country_code    varchar(50),
   level           integer,
   viplevel        integer,
   os              varchar(10),
   rc_out          integer,
   coins_out       integer,
   action          varchar(300),
   location        varchar(200),
   install_source  varchar(300),
   device          varchar(100),
   lang            varchar(100),
   item_in         integer,
   item_out        integer,
   item_id         varchar(20),
   item_name       varchar(200),
   item_type       varchar(50),
   item_class      varchar(100),
   gifed_by        varchar(50),
   gifted_to       varchar(50)
);

drop table if exists processed.agg_item_stats;
CREATE TABLE processed.agg_item_stats
(
	date            DATE,
	app             VARCHAR(64),
	install_date    DATE,
	os              VARCHAR(30),
	level           INTEGER,
	viplevel        INTEGER,
	country         VARCHAR(64),
	resource        VARCHAR(20),
	item_name       VARCHAR(200),
	item_class      VARCHAR(10),
	action          VARCHAR(300),
	location        VARCHAR(200),
	item_in         INTEGER,
	item_out        INTEGER,
	rc_out          INTEGER,
	coins_out       INTEGER
);

create table processed.pve_progress
(
   date           date,
   ts             timestamp,
   app            varchar(64),
   uid            varchar(100),
   snsid          varchar(64),
   user_key       varchar(50),
   install_date   date,
   country_code   varchar(50),
   level          integer,
   viplevel       integer,
   os             varchar(10),
   device         varchar(100),
   lang           varchar(100),
   pveid          varchar(200),
   pvelocation    varchar(200)
);


create table processed.agg_tbl_pve
(
   date           date,
   app            varchar(64),
   level          integer,
   viplevel       integer,
   os             varchar(10),
   country        varchar(64),
   install_date   date,
   pveid          varchar(200),
   pvelocation    varchar(200),
   user_num       integer
);

create table processed.equip_intensify_transaction
(
   date             date,
   ts               TIMESTAMP ENCODE DELTA,
   app              varchar(64),
   uid              varchar(100),
   snsid            varchar(64),
   user_key         varchar(50),
   install_date     date,
   country_code     varchar(50),
   level            integer,
   viplevel         integer,
   os               varchar(10),
   device           varchar(100),
   lang             varchar(100),
   equipid          varchar(50),
   equipname        varchar(200),
   equiplevle       integer,
   equipexp         integer,
   afterequipexp    integer,
   afterequiplevel  integer,
   sequence         varchar(200)

);



create table processed.agg_tbl_equip_intensify_times
(
    date            date,
    app             varchar(64) ,
    os              varchar(30),
    install_date    date,
    country         varchar(64),
    level           integer,
    viplevel        integer,
    intensify_times integer
)


create table processed.agg_tbl_equip_intensify_num
(
    date            date,
    app             varchar(64) ,
    os              varchar(30),
    install_date    date,
    country         varchar(64),
    level           integer,
    viplevel        integer,
    equipid         varchar(50),
    equipname       varchar(200),
    intensify_num   integer
);




create table processed.equip_advance_transaction
(
    date            date,
    ts              TIMESTAMP ENCODE DELTA,
    app             varchar(64),
    uid             varchar(100),
    snsid           varchar(64),
    user_key        varchar(50),
    install_date    date,
    country_code    varchar(50),
    level           integer,
    viplevel        integer,
    os              varchar(10),
    device          varchar(100),
    lang            varchar(100),
    equipid         varchar(50),
    equipname       varchar(200),
    star            integer,
    afterstar       integer,
    sequence        varchar(200)
);


create table processed.agg_tbl_equip_advance_times
(
    date            date,
    app             varchar(64) ,
    os              varchar(30),
    install_date    date,
    country         varchar(64),
    level           integer,
    viplevel        integer,
    advance_times   integer
)


create table processed.agg_tbl_equip_advance_num
(
    date            date,
    app             varchar(64) ,
    os              varchar(30),
    install_date    date,
    country         varchar(64),
    level           integer,
    viplevel        integer,
    equipid         varchar(50),
    equipname       varchar(200),
    advance_num     integer
);


create table processed.sns_transaction
(
    date            date,
    ts              TIMESTAMP ENCODE DELTA,
    app             varchar(64),
    uid             varchar(100),
    snsid           varchar(64),
    user_key        varchar(50),
    install_date    date,
    country_code    varchar(50),
    level           varchar(200),
    viplevel        varchar(10),
    os              varchar(10),
    device          varchar(100),
    lang            varchar(100),
    snstype         varchar(200),
    count           integer,
    recnum          integer,
    actoropenid     integer
);



create table processed.agg_tbl_sns_user_num
(
    date            date
    app             varchar(64)
    os              varchar(10)
    install_date    date
    country         varchar(50)
    level           varchar(200)
    viplevel        varchar(10)
    snstype         varchar(200)
    sns_user_num    integer
);



create table processed.roundflow_transaction
(
   date             date,
   ts               TIMESTAMP ENCODE DELTA,
   app              varchar(64),
   uid              varchar(100),
   snsid            varchar(64),
   user_key         varchar(50),
   install_date     date,
   country_code     varchar(50),
   level            varchar(200),
   viplevel         varchar(10),
   os               varchar(10),
   device           varchar(100),
   lang             varchar(100),
   pveid            varchar(200),
   pvelocation      varchar(200),
   result           integer,
   roundtime        integer,
   roundscore       integer,
   rank             integer,
   gold             integer,
   battletype       varchar(200),
   battleid         varchar(200)
);



create table processed.agg_tbl_roundflow
(
   date             date
   app              varchar(64)
   install_date     date
   country          varchar(50)
   level            varchar(200)
   viplevel         varchar(10)
   os               varchar(10)
   device           varchar(100)
   pveid            varchar(200)
   pvelocation      varchar(200)
   result           integer
   battletype       varchar(200)
   user_num         integer
   battle_num       integer
   roundtime        integer
);


create table processed.agg_tbl_roundflow_endless
(
   date             date
   app              varchar(64)
   install_date     date
   country          varchar(50)
   level            varchar(200)
   viplevel         varchar(10)
   os               varchar(10)
   device           varchar(100)
   pveid            varchar(200)
   pvelocation      varchar(200)
   result           integer
   battletype       varchar(200)
   battle_num       integer
   gold             integer
   rank_max         integer
);


create table processed.energyflow_transaction
(
   date             date,
   ts               TIMESTAMP ENCODE DELTA,
   app              varchar(64),
   uid              varchar(100),
   snsid            varchar(64),
   user_key         varchar(50),
   install_date     date,
   country_code     varchar(50),
   level            varchar(200),
   viplevel         varchar(10),
   os               varchar(10),
   device           varchar(100),
   lang             varchar(100),
   reason           varchar(200),
   pveid            varchar(200),
   pvelocation      varchar(200),
   energy_in        integer,
   energy_out       integer,
   afterenergy      integer

);


create table processed.agg_tbl_energyflow
(
   date             date
   app              varchar(64)
   install_date     date
   country          varchar(50)
   level            varchar(200)
   viplevel         varchar(10)
   os               varchar(10)
   device           varchar(100)
   pveid            varchar(200)
   pvelocation      varchar(200)
   reason           varchar(50)
   user_num         integer
   energy_in        integer
   energy_out       integer
);


-- fact_custom_event
drop table if exists processed.fact_custom_event;
CREATE TABLE processed.fact_custom_event
(
  date                  DATE ENCODE DELTA,
  user_key              VARCHAR(32) ENCODE LZO,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  uid                   VARCHAR(32) ENCODE LZO,
  snsid                 VARCHAR(64) ENCODE LZO,
  ts                    TIMESTAMP ENCODE DELTA,
  level                 SMALLINT,
  vip_level             SMALLINT,

  type                  VARCHAR(32) ENCODE BYTEDICT,
  eventdesc             VARCHAR(256) ENCODE BYTEDICT,
  eventid               VARCHAR(32) ENCODE BYTEDICT,
  process               VARCHAR(32) ENCODE BYTEDICT,
  maintype              VARCHAR(64) ENCODE BYTEDICT,
  device                VARCHAR(128) ENCODE BYTEDICT,
  isdone                VARCHAR(32) ENCODE BYTEDICT,
  rank                  SMALLINT
)
  DISTKEY(user_key)
  SORTKEY(date, user_key, app, uid, level, type);

-- eas_user_info
drop table if exists processed.eas_user_info;
CREATE TABLE processed.eas_user_info
(
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  snsid                     VARCHAR(64) ENCODE LZO,
  user_name                 VARCHAR(64) ENCODE LZO,
  email                     VARCHAR(64) ENCODE LZO,
  additional_email          VARCHAR(64) ENCODE LZO,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  level                     SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  last_payment_ts           TIMESTAMP ENCODE DELTA,
  payment_cnt               BIGINT DEFAULT 0,
  rc                        BIGINT DEFAULT 0,
  coins                     BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(uid)
SORTKEY(uid, app);
















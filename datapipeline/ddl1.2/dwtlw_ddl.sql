------------------------------------------------------------------------------------------------------------------------------------
--The DEAD WALK THE LAST WAR ddl
--version 1.2
/**
Description:

**/
------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE processed.raw_data_s3
(
	json_str        VARCHAR(20000) ENCODE LZO,
	load_hour       TIMESTAMP
)
SORTKEY(load_hour);

drop table if exists processed.events_raw;
CREATE TABLE processed.events_raw
(
	load_hour           TIMESTAMP ENCODE DELTA,
	event               VARCHAR(32) ENCODE BYTEDICT,
	date                DATE NOT NULL ENCODE DELTA,
	ts                  TIMESTAMP NOT NULL ENCODE DELTA,
	app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
	uid                 INTEGER NOT NULL,
	server              VARCHAR(16) ENCODE BYTEDICT,
	install_ts          TIMESTAMP ENCODE DELTA,
	country_code        VARCHAR(8) ENCODE BYTEDICT,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	ip                  VARCHAR(16) ENCODE LZO,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	os_version          VARCHAR(64) ENCODE BYTEDICT,
	app_version         VARCHAR(32) ENCODE BYTEDICT,
	session_id          VARCHAR(32) ENCODE LZO,
	bi_version          VARCHAR(32) ENCODE BYTEDICT,
	idfa                VARCHAR(64) ENCODE LZO,
	gaid                VARCHAR(64) ENCODE LZO,
	android_id          VARCHAR(64) ENCODE LZO,
	mac_address         VARCHAR(32) ENCODE LZO,
	device              VARCHAR(64) ENCODE BYTEDICT,
	lang                VARCHAR(32) ENCODE BYTEDICT,
	json_str            VARCHAR(2048) ENCODE LZO,
	properties          VARCHAR(2048) ENCODE LZO,
	collections         VARCHAR(2048) ENCODE LZO,
	md5                 VARCHAR(32) ENCODE LZO
)
DISTKEY(uid)
SORTKEY(load_hour, event, date, ts);

drop table if exists processed.fact_session;
CREATE TABLE processed.fact_session
(
  date_start            DATE NOT NULL ENCODE DELTA,
  user_key              VARCHAR(32) NOT NULL ENCODE LZO,
  app                   VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                   INTEGER NOT NULL,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts_start              TIMESTAMP ENCODE DELTA,
  ts_end                TIMESTAMP ENCODE DELTA,
  level_start           SMALLINT,
  level_end             SMALLINT,
  vip_level_start       VARCHAR(16) ENCODE BYTEDICT,
  vip_level_end         VARCHAR(16) ENCODE BYTEDICT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(8) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(8) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(32) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  rc_bal_start          INTEGER,
  rc_bal_end            INTEGER,
  coin_bal_start        INTEGER,
  coin_bal_end          INTEGER,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,
  session_length        INTEGER
)
DISTKEY(user_key)
SORTKEY(date_start, user_key, player_key, app, uid, server, player_id);

drop table if exists processed.fact_revenue;
CREATE TABLE processed.fact_revenue
(
  date                  DATE NOT NULL ENCODE DELTA,
  user_key              VARCHAR(32) NOT NULL ENCODE LZO,
  app                   VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                   INTEGER NOT NULL,
  player_key            VARCHAR(32) ENCODE LZO,
  server                VARCHAR(8) ENCODE BYTEDICT,
  player_id             INTEGER,
  install_ts            TIMESTAMP ENCODE DELTA,
  install_date          DATE ENCODE DELTA,
  install_source        VARCHAR(256) ENCODE BYTEDICT,
  app_version           VARCHAR(32) ENCODE BYTEDICT,
  session_id            VARCHAR(32) ENCODE LZO,
  ts                    TIMESTAMP ENCODE DELTA,
  level                 SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,
  os                    VARCHAR(32) ENCODE BYTEDICT,
  os_version            VARCHAR(64) ENCODE BYTEDICT,
  country_code          VARCHAR(8) ENCODE BYTEDICT,
  ip                    VARCHAR(16) ENCODE LZO,
  language              VARCHAR(8) ENCODE BYTEDICT,
  device                VARCHAR(64) ENCODE BYTEDICT,
  idfa                  VARCHAR(64) ENCODE LZO,
  gaid                  VARCHAR(64) ENCODE LZO,
  android_id            VARCHAR(64) ENCODE LZO,
  mac_address           VARCHAR(32) ENCODE LZO,
  browser               VARCHAR(32) ENCODE BYTEDICT,
  browser_version       VARCHAR(32) ENCODE BYTEDICT,
  ab_test               VARCHAR(64) ENCODE BYTEDICT,
  ab_variant            VARCHAR(8) ENCODE BYTEDICT,

  payment_processor     VARCHAR(32) ENCODE BYTEDICT,
  product_id            VARCHAR(64) ENCODE BYTEDICT,
  product_name          VARCHAR(64) ENCODE BYTEDICT,
  product_type          VARCHAR(32) ENCODE BYTEDICT,
  coins_in              INTEGER,
  rc_in                 INTEGER,
  currency              VARCHAR(32) ENCODE BYTEDICT,
  amount                DECIMAL(14,4) DEFAULT 0,
  usd                   DECIMAL(14,4) DEFAULT 0,
  transaction_id        VARCHAR(64) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, user_key, player_key, app, uid, server, player_id);

drop table if exists processed.fact_dau_snapshot;
CREATE TABLE processed.fact_dau_snapshot
(
  date                      DATE NOT NULL ENCODE DELTA,
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  vip_level_end             VARCHAR(16) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_user               SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER,
  create_league_cnt         SMALLINT DEFAULT 0,
  join_league_cnt           SMALLINT DEFAULT 0,
  current_league            VARCHAR(32) ENCODE BYTEDICT,
  dailybonusclaimed_cnt     INTEGER DEFAULT 0
)
DISTKEY(user_key)
SORTKEY(date, user_key, app, uid, install_date, country_code);

drop table if exists processed.fact_player_dau_snapshot;
CREATE TABLE processed.fact_player_dau_snapshot
(
  date                      DATE NOT NULL ENCODE DELTA,
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  player_key                VARCHAR(32) ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  player_id                 INTEGER,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  vip_level_end             VARCHAR(16) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32),
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  is_new_player             SMALLINT DEFAULT 0,
  session_cnt               INTEGER DEFAULT 1,
  coins_in                  INTEGER DEFAULT 0,
  rc_in                     INTEGER DEFAULT 0,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              INTEGER DEFAULT 0,
  is_payer                  SMALLINT DEFAULT 0,
  is_converted_today        SMALLINT DEFAULT 0,
  playtime_sec              INTEGER,
  create_league_cnt         SMALLINT DEFAULT 0,
  join_league_cnt           SMALLINT DEFAULT 0,
  current_league            VARCHAR(32) ENCODE BYTEDICT,
  dailybonusclaimed_cnt     INTEGER DEFAULT 0
)
DISTKEY(player_key)
SORTKEY(date, player_key, app, server, player_id, install_date, country_code);

drop table if exists processed.dim_user;
CREATE TABLE processed.dim_user
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level                 VARCHAR(16) ENCODE BYTEDICT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_rc_in               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid);

drop table if exists processed.dim_player;
CREATE TABLE processed.dim_player
(
  user_key                  VARCHAR(32) NOT NULL ENCODE LZO,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                       INTEGER NOT NULL,
  player_key                VARCHAR(32) ENCODE LZO,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  player_id                 INTEGER,
  install_ts                TIMESTAMP ENCODE DELTA,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                  VARCHAR(128) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher             VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  creative_id               VARCHAR(512) DEFAULT '' ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  birth_date                DATE ENCODE DELTA,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  country_code              VARCHAR(20) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30)  ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level                 VARCHAR(16) ENCODE BYTEDICT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  total_revenue_usd         DECIMAL(14,4),
  total_rc_in               BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(player_key)
SORTKEY(player_key, app, server, player_id);

drop table if exists processed.agg_kpi;
CREATE TABLE processed.agg_kpi
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher             VARCHAR(128) ENCODE BYTEDICT,
  campaign                  VARCHAR(512) ENCODE BYTEDICT,
  creative_id               VARCHAR(512) ENCODE BYTEDICT,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  vip_level_end             VARCHAR(16) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  is_new_user               SMALLINT,
  is_payer                  SMALLINT,
  is_converted_today        SMALLINT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  new_installs              INTEGER,
  dau                       INTEGER,
  new_payers                INTEGER,
  today_payers              INTEGER,
  revenue                   DECIMAL(14,4),
  session_cnt               INTEGER,
  new_league_cnt            INTEGER,
  join_league_cnt           INTEGER,
  in_league_users           INTEGER,
  dailybonusclaimed_users   INTEGER
)
SORTKEY(date, app, os, install_date, install_source, country, is_payer, vip_level);

drop table if exists processed.agg_kpi_history;
CREATE TABLE processed.agg_kpi_history
(
  record_date           DATE NOT NULL ENCODE DELTA,
  date                  DATE NOT NULL ENCODE DELTA,
  country               VARCHAR(64) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  new_installs          INTEGER,
  dau                   INTEGER,
  new_payers            INTEGER,
  today_payers          INTEGER,
  revenue               DECIMAL(14,4),
  session_cnt           INTEGER
)
SORTKEY(record_date, date, os, country);

drop table if exists processed.agg_player_kpi;
CREATE TABLE processed.agg_player_kpi
(
  date                      DATE NOT NULL ENCODE DELTA,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  app_version               VARCHAR(20) ENCODE BYTEDICT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  sub_publisher             VARCHAR(128) ENCODE BYTEDICT,
  campaign                  VARCHAR(512) ENCODE BYTEDICT,
  creative_id               VARCHAR(512) ENCODE BYTEDICT,
  level_start               SMALLINT,
  level_end                 SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  vip_level_end             VARCHAR(16) ENCODE BYTEDICT,
  device                    VARCHAR(64) ENCODE BYTEDICT,
  device_alias              VARCHAR(32) ENCODE BYTEDICT,
  browser                   VARCHAR(32) ENCODE BYTEDICT,
  browser_version           VARCHAR(32) ENCODE BYTEDICT,
  country_code              VARCHAR(16) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  os_version                VARCHAR(100) ENCODE BYTEDICT,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  is_new_player             SMALLINT,
  is_payer                  SMALLINT,
  is_converted_today        SMALLINT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  new_players               INTEGER,
  dau                       INTEGER,
  new_payers                INTEGER,
  today_payers              INTEGER,
  revenue                   DECIMAL(14,4),
  session_cnt               INTEGER,
  new_league_cnt            INTEGER,
  join_league_cnt           INTEGER,
  in_league_users           INTEGER,
  dailybonusclaimed_users   INTEGER
)
SORTKEY(date, server, app, os, install_date, install_source, country, is_payer, vip_level_end);

drop table if exists processed.agg_retention;
CREATE TABLE processed.agg_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  new_users             BIGINT,
  retained              BIGINT
)
SORTKEY(retention_days, install_date, os, country, app, install_source, is_payer, vip_level);

drop table if exists processed.agg_player_retention;
CREATE TABLE processed.agg_player_retention
(
  retention_days        INTEGER,
  install_date          DATE ENCODE DELTA,
  server                VARCHAR(8) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  new_players           BIGINT,
  retained              BIGINT
)
SORTKEY(retention_days, install_date, server, os, country, app, install_source, is_payer, vip_level);

drop table if exists processed.agg_ltv;
CREATE TABLE processed.agg_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  users                 BIGINT,
  revenue               DECIMAL(16,4)
)
SORTKEY(ltv_days, install_date, os, country, app, install_source, is_payer, vip_level);

drop table if exists processed.agg_player_ltv;
CREATE TABLE processed.agg_player_ltv
(
  ltv_days              INTEGER,
  install_date          DATE ENCODE DELTA,
  server                VARCHAR(8) ENCODE BYTEDICT,
  os                    VARCHAR(30) ENCODE BYTEDICT,
  country               VARCHAR(64) ENCODE BYTEDICT,
  app                   VARCHAR(32) ENCODE BYTEDICT,
  install_source        VARCHAR(128) ENCODE BYTEDICT,
  is_payer              SMALLINT,
  vip_level             VARCHAR(16) ENCODE BYTEDICT,

  players               BIGINT,
  revenue               DECIMAL(16,4)
)
SORTKEY(ltv_days, install_date, server, os, country, app, install_source, is_payer, vip_level);

drop table if exists processed.tab_marketing_kpi;
CREATE TABLE processed.tab_marketing_kpi
(
    app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    install_date      DATE ENCODE DELTA,
    install_date_str  VARCHAR(10) ENCODE BYTEDICT,
    install_source    VARCHAR(128) DEFAULT NULL ENCODE BYTEDICT,
    campaign          VARCHAR(128) DEFAULT NULL ENCODE LZO,
    sub_publisher     VARCHAR(512) DEFAULT NULL ENCODE LZO,
    creative_id       VARCHAR(512) DEFAULT NULL ENCODE LZO,
    country           VARCHAR(64) ENCODE BYTEDICT,
    os                VARCHAR(32) ENCODE BYTEDICT,

    new_installs      INTEGER DEFAULT 0,
    d1_new_installs   INTEGER DEFAULT 0,
    d7_new_installs   INTEGER DEFAULT 0,
    d30_new_installs  INTEGER DEFAULT 0,
    d60_new_installs  INTEGER DEFAULT 0,
    d90_new_installs  INTEGER DEFAULT 0,
    d120_new_installs INTEGER DEFAULT 0,

    revenue           DECIMAL(14,4) DEFAULT 0,
    d1_revenue        DECIMAL(14,4) DEFAULT 0,
    d7_revenue        DECIMAL(14,4) DEFAULT 0,
    d30_revenue       DECIMAL(14,4) DEFAULT 0,
    d60_revenue       DECIMAL(14,4) DEFAULT 0,
	d90_revenue       DECIMAL(14,4) DEFAULT 0,
	d120_revenue      DECIMAL(14,4) DEFAULT 0,

    payers            INTEGER DEFAULT 0,
    d1_payers         INTEGER DEFAULT 0,
    d7_payers         INTEGER DEFAULT 0,
    d30_payers        INTEGER DEFAULT 0,
    d60_payers        INTEGER DEFAULT 0,
    d90_payers        INTEGER DEFAULT 0,
    d120_payers       INTEGER DEFAULT 0,

    d1_retained       INTEGER DEFAULT 0,
    d7_retained       INTEGER DEFAULT 0,
    d30_retained      INTEGER DEFAULT 0,
    d60_retained      INTEGER DEFAULT 0,
    d90_retained      INTEGER DEFAULT 0,
    d120_retained     INTEGER DEFAULT 0,

    cost              DECIMAL(14,4) DEFAULT 0
)
  SORTKEY(install_date, os, install_source, country, campaign, sub_publisher, creative_id, app);

-- user‘s install source
drop table if exists processed.fact_user_install_source;
CREATE TABLE processed.fact_user_install_source
(
  user_key                      VARCHAR(32) NOT NULL ENCODE LZO,
  app                           VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid                           INTEGER NOT NULL,
  install_date                  DATE ENCODE DELTA,
  install_source_adjust_raw     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust         VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw            VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source                VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign                      VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher                 VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  creative_id                   VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, install_date, install_source_adjust, install_source, campaign, sub_publisher);


-- install source map
drop table if exists processed.ref_install_source_map;
CREATE TABLE processed.ref_install_source_map
(
	install_source_raw      VARCHAR(1024),
	install_source_lower    VARCHAR(1024),
	install_source          VARCHAR(1024),
	bi_install_source       VARCHAR(1024)
)
SORTKEY(install_source_raw, install_source_lower, install_source, bi_install_source);

-- dim_country
drop table if exists processed.dim_country;
CREATE TABLE processed.dim_country
(
	country_code VARCHAR(2),
	country VARCHAR(50)
)
SORTKEY(country_code, country);

insert into processed.dim_country
select '--', 'Unknown';

-- Level up event
drop table if exists processed.fact_level_up;
create table processed.fact_level_up (
  user_key          VARCHAR(32) NOT NULL ENCODE LZO,
  app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  level             SMALLINT,
  vip_level_start   VARCHAR(16) ENCODE BYTEDICT,
  date_start        DATE ENCODE DELTA,
  date_end          DATE ENCODE DELTA,
  ts_start          TIMESTAMP ENCODE DELTA,
  ts_end            TIMESTAMP ENCODE DELTA
)
DISTKEY(user_key)
SORTKEY(user_key, level, date_start, date_end, ts_start, ts_end);

-- agg_iap
drop table if exists processed.agg_iap;
CREATE TABLE processed.agg_iap
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  date_str                  VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) NOT NULL ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, os, country, install_source);

-- agg_player_iap
drop table if exists processed.agg_player_iap;
CREATE TABLE processed.agg_player_iap
(
  date                      DATE NOT NULL ENCODE DELTA,
  app                       VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  date_str                  VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_week_str             VARCHAR(10) NOT NULL ENCODE BYTEDICT,
  date_month_str            VARCHAR(7) NOT NULL ENCODE BYTEDICT,
  level                     SMALLINT,
  vip_level_start           VARCHAR(16) ENCODE BYTEDICT,
  install_date              DATE ENCODE DELTA,
  install_source            VARCHAR(128) ENCODE BYTEDICT,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  os                        VARCHAR(30) ENCODE BYTEDICT,
  country                   VARCHAR(64) ENCODE BYTEDICT,
  ab_test                   VARCHAR(64) ENCODE BYTEDICT,
  ab_variant                VARCHAR(8) ENCODE BYTEDICT,
  conversion_purchase       SMALLINT DEFAULT 0,
  product_type              VARCHAR(64) ENCODE BYTEDICT,
  product_id                VARCHAR(64) ENCODE BYTEDICT,
  revenue_usd               DECIMAL(14,4) DEFAULT 0,
  purchase_cnt              BIGINT DEFAULT 0,
  purchase_user_cnt         BIGINT DEFAULT 0
)
SORTKEY(date, app, level, conversion_purchase, product_type, product_id, date_str, date_week_str, date_month_str, server, os, country, install_source);

-- daily level
drop table if exists processed.daily_level;
create table processed.daily_level (
  date              DATE NOT NULL ENCODE DELTA,
  user_key          VARCHAR(32) NOT NULL ENCODE LZO,
  app               VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  player_key        VARCHAR(32) ENCODE LZO,
  server            VARCHAR(8) ENCODE BYTEDICT,
  player_id         INTEGER,
  min_level         SMALLINT,
  max_level         SMALLINT,
  min_vip_level     VARCHAR(16) ENCODE BYTEDICT,
  max_vip_level     VARCHAR(16) ENCODE BYTEDICT
)
DISTKEY(user_key)
SORTKEY(user_key, app, uid, server);

-- ip_country_map
drop table if exists processed.geoip_country;
create table processed.geoip_country
(
	geoname_id          INTEGER,
	locale_code         VARCHAR(32) ENCODE BYTEDICT,
	continent_code      VARCHAR(32) ENCODE BYTEDICT,
	continent_name      VARCHAR(32) ENCODE BYTEDICT,
	country_iso_code    VARCHAR(32) ENCODE BYTEDICT,
	country_name        VARCHAR(64) ENCODE BYTEDICT
)
SORTKEY(geoname_id, country_iso_code, country_name);

drop table if exists processed.ip_integer_range_block;
create table processed.ip_integer_range_block
(
	network                         VARCHAR(32) ENCODE LZO,
	network_start_ip                VARCHAR(32) ENCODE LZO,
	network_last_ip                 VARCHAR(32) ENCODE LZO,
	network_start_integer           BIGINT,
	network_last_integer            BIGINT,
	geoname_id                      INTEGER,
	registered_country_geoname_id   INTEGER,
	represented_country_geoname_id  INTEGER,
	is_anonymous_proxy              INTEGER,
	is_satellite_provider           INTEGER
)
SORTKEY(network_start_integer, network_last_integer);

drop table if exists processed.ref_ip_country_map;
create table processed.ref_ip_country_map (
  ip                VARCHAR(16) ENCODE LZO,
  ip_int            BIGINT,
  country_code      VARCHAR(8) ENCODE BYTEDICT
)
SORTKEY(ip, ip_int, country_code);

-- currency table
drop table if exists public.currency;
create table public.currency
(
	id          VARCHAR(32) NOT NULL,
	dt          DATE NOT NULL,
	currency    VARCHAR(16) NOT NULL,
	factor      DECIMAL(14,4) NOT NULL
)
SORTKEY(dt, currency);

-- fact_finish_now
drop table if exists processed.fact_finish_now;
CREATE TABLE processed.fact_finish_now
(
    date                DATE NOT NULL ENCODE DELTA,
    ts                  TIMESTAMP ENCODE DELTA,
    user_key            VARCHAR(32) NOT NULL ENCODE LZO,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    uid                 INTEGER NOT NULL,
    player_key          VARCHAR(32) ENCODE LZO,
    server              VARCHAR(8) ENCODE BYTEDICT,
    player_id           VARCHAR(32) ENCODE LZO,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,
	building_level      SMALLINT,
	building_type       VARCHAR(64),
	properties          VARCHAR(1024) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, action);

-- fact_city
drop table if exists processed.fact_city;
CREATE TABLE processed.fact_city
(
    date                DATE NOT NULL ENCODE DELTA,
    ts                  TIMESTAMP ENCODE DELTA,
    user_key            VARCHAR(32) NOT NULL ENCODE LZO,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    uid                 INTEGER NOT NULL,
    player_key          VARCHAR(32) ENCODE LZO,
    server              VARCHAR(8) ENCODE BYTEDICT,
    player_id           VARCHAR(32) ENCODE LZO,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,
	building_level      SMALLINT,
	building_type       VARCHAR(64),
	properties          VARCHAR(1024) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, action);

-- fact_league
drop table if exists processed.fact_league;
CREATE TABLE processed.fact_league
(
    date                DATE NOT NULL ENCODE DELTA,
    ts                  TIMESTAMP ENCODE DELTA,
    user_key            VARCHAR(32) NOT NULL ENCODE LZO,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    uid                 INTEGER NOT NULL,
    player_key          VARCHAR(32) ENCODE LZO,
    server              VARCHAR(8) ENCODE BYTEDICT,
    player_id           VARCHAR(32) ENCODE LZO,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,
	alliance_id         INTEGER,
	target_uid          INTEGER,
	target_user_level   SMALLINT,
	target_rank         SMALLINT,
	gift_level          SMALLINT,
	properties          VARCHAR(1024) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, action);

-- fact_gifts
drop table if exists processed.fact_gifts;
CREATE TABLE processed.fact_gifts
(
    date                DATE NOT NULL ENCODE DELTA,
    ts                  TIMESTAMP ENCODE DELTA,
    user_key            VARCHAR(32) NOT NULL ENCODE LZO,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    uid                 INTEGER NOT NULL,
    player_key          VARCHAR(32) ENCODE LZO,
    server              VARCHAR(8) ENCODE BYTEDICT,
    player_id           VARCHAR(32) ENCODE LZO,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,
	properties          VARCHAR(1024) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, action);

-- fact_resource_out
drop table if exists processed.fact_resource_out;
CREATE TABLE processed.fact_resource_out
(
    date                DATE NOT NULL ENCODE DELTA,
    ts                  TIMESTAMP ENCODE DELTA,
    user_key            VARCHAR(32) NOT NULL ENCODE LZO,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    uid                 INTEGER NOT NULL,
    player_key          VARCHAR(32) ENCODE LZO,
    server              VARCHAR(8) ENCODE BYTEDICT,
    player_id           VARCHAR(32) ENCODE LZO,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	event               VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,

	player_food         BIGINT,
	player_water        BIGINT,
	player_metal        BIGINT,
	player_fuel         BIGINT,
	player_money        BIGINT,
	player_gold         BIGINT,
	spent_food          BIGINT,
	spent_water         BIGINT,
	spent_metal         BIGINT,
	spent_fuel          BIGINT,
	spent_money         BIGINT,
	spent_gold          BIGINT,
	properties          VARCHAR(1024) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, event, action);

-- fact_resource_in
drop table if exists processed.fact_resource_in;
CREATE TABLE processed.fact_resource_in
(
    date                DATE NOT NULL ENCODE DELTA,
    ts                  TIMESTAMP ENCODE DELTA,
    user_key            VARCHAR(32) NOT NULL ENCODE LZO,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    uid                 INTEGER NOT NULL,
    player_key          VARCHAR(32) ENCODE LZO,
    server              VARCHAR(8) ENCODE BYTEDICT,
    player_id           VARCHAR(32) ENCODE LZO,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	os                  VARCHAR(32) ENCODE BYTEDICT,
	event               VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,

	player_food         BIGINT,
	player_water        BIGINT,
	player_metal        BIGINT,
	player_fuel         BIGINT,
	player_money        BIGINT,
	player_gold         BIGINT,
	receive_food        BIGINT,
	receive_water       BIGINT,
	receive_metal       BIGINT,
	receive_fuel        BIGINT,
	receive_money       BIGINT,
	receive_gold        BIGINT,
	properties          VARCHAR(1024) ENCODE LZO
)
DISTKEY(user_key)
SORTKEY(date, event, action);

-- agg_resource_out
drop table if exists processed.agg_resource_out;
CREATE TABLE processed.agg_resource_out
(
    date                DATE NOT NULL ENCODE DELTA,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    server              VARCHAR(8) ENCODE BYTEDICT,
    install_date        DATE ENCODE DELTA,
    install_source      VARCHAR(128) ENCODE BYTEDICT,
    device              VARCHAR(64) ENCODE BYTEDICT,
    device_alias        VARCHAR(32) ENCODE BYTEDICT,
    country             VARCHAR(64) ENCODE BYTEDICT,
    os                  VARCHAR(32) ENCODE BYTEDICT,
    language            VARCHAR(20) ENCODE BYTEDICT,
    is_payer            SMALLINT,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	event               VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,

	player_food         BIGINT,
	player_water        BIGINT,
	player_metal        BIGINT,
	player_fuel         BIGINT,
	player_money        BIGINT,
	player_gold         BIGINT,
	spent_food          BIGINT,
	spent_water         BIGINT,
	spent_metal         BIGINT,
	spent_fuel          BIGINT,
	spent_money         BIGINT,
	spent_gold          BIGINT,
	users               INTEGER
)
SORTKEY(date, event, action, level, os, country, install_source, is_payer);

-- agg_resource_in
drop table if exists processed.agg_resource_in;
CREATE TABLE processed.agg_resource_in
(
    date                DATE NOT NULL ENCODE DELTA,
    app                 VARCHAR(32) NOT NULL ENCODE BYTEDICT,
    server              VARCHAR(8) ENCODE BYTEDICT,
    install_date        DATE ENCODE DELTA,
    install_source      VARCHAR(128) ENCODE BYTEDICT,
    device              VARCHAR(64) ENCODE BYTEDICT,
    device_alias        VARCHAR(32) ENCODE BYTEDICT,
    country             VARCHAR(64) ENCODE BYTEDICT,
    os                  VARCHAR(32) ENCODE BYTEDICT,
    language            VARCHAR(20) ENCODE BYTEDICT,
    is_payer            SMALLINT,
	level               SMALLINT,
	vip_level           VARCHAR(16) ENCODE BYTEDICT,
	event               VARCHAR(32) ENCODE BYTEDICT,
	action              VARCHAR(64) ENCODE BYTEDICT,

	player_food         BIGINT,
	player_water        BIGINT,
	player_metal        BIGINT,
	player_fuel         BIGINT,
	player_money        BIGINT,
	player_gold         BIGINT,
	receive_food        BIGINT,
	receive_water       BIGINT,
	receive_metal       BIGINT,
	receive_fuel        BIGINT,
	receive_money       BIGINT,
	receive_gold        BIGINT,
	users               INTEGER
)
SORTKEY(date, event, action, level, os, country, install_source, is_payer);

-- eas_user_info
drop table if exists processed.eas_user_info;
CREATE TABLE processed.eas_user_info
(
  app                       VARCHAR(32) ENCODE BYTEDICT,
  uid                       VARCHAR(32) ENCODE LZO,
  os                        VARCHAR(32) ENCODE BYTEDICT,
  server                    VARCHAR(8) ENCODE BYTEDICT,
  snsid                     VARCHAR(64) ENCODE LZO,
  user_name                 VARCHAR(64) ENCODE LZO,
  email                     VARCHAR(64) ENCODE LZO,
  additional_email          VARCHAR(64) ENCODE LZO,
  install_source            VARCHAR(128) DEFAULT 'Organic' ENCODE BYTEDICT,
  install_ts                TIMESTAMP ENCODE DELTA,
  language                  VARCHAR(20) ENCODE BYTEDICT,
  gender                    VARCHAR(10) ENCODE BYTEDICT,
  level                     SMALLINT,
  is_payer                  SMALLINT,
  conversion_ts             TIMESTAMP ENCODE DELTA,
  last_payment_ts           TIMESTAMP ENCODE DELTA,
  payment_cnt               BIGINT DEFAULT 0,
  rc                        BIGINT DEFAULT 0,
  coins                     BIGINT DEFAULT 0,
  last_login_ts             TIMESTAMP ENCODE DELTA
)
DISTKEY(uid)
SORTKEY(uid, app);


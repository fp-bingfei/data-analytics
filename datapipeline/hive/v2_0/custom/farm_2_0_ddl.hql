----------------------------------
-- Farm 2.0
-- Measure events
----------------------------------
USE farm_2_0;

-- Measure
DROP TABLE IF EXISTS agg_measure;

CREATE EXTERNAL TABLE IF NOT EXISTS agg_measure (
    event_name string,
    app_id string,
    user_key string,
    app_user_id string,
    facebook_id string,
    date string,
    d_browser string,
    d_browser_version string,
    d_country_code string,
    d_install_source string,
    d_install_date string,
    d_ip string,
    d_lang string,
    d_level string,
    d_os string,
    d_os_version string,
    d_fb_source string,
    d_c1 string,
    d_c2 string,
    d_c3 string,
    d_c4 string,
    d_c5 string,
    m1 string,
    m2 string,
    m3 string,
    m4 string,
    m5 string,
    event_cnt bigint
)
PARTITIONED BY (
    event string,
    app string,
    dt string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_2_0/processed/agg_measure'
TBLPROPERTIES('serialization.null.format'='');


-- Funnel
DROP TABLE IF EXISTS mid_funnel;
CREATE EXTERNAL TABLE IF NOT EXISTS mid_funnel (
    uid string,
    funnel_id string,
    step string,
    action string,
    start_date timestamp,
    end_date timestamp,
    skip_date timestamp,
    length_sec bigint,
    browser string,
    browser_version string,
    country_code string,
    install_source string,
    install_date string,
    lang string,
    os string,
    os_version string,
    rc_in bigint,
    rc_out bigint,
    rc_bal bigint,
    coins_in bigint,
    coins_out bigint,
    coins_bal bigint
)
PARTITIONED BY (
    event string,
    app string,
    dt string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_2_0/processed/mid_funnel'
TBLPROPERTIES('serialization.null.format'='');

DROP TABLE IF EXISTS agg_funnel;

CREATE EXTERNAL TABLE IF NOT EXISTS agg_funnel (
    d_cohort_date string,
    d_app_id string,
    d_browser string,
    d_browser_version string,
    d_country_code string,
    d_install_source string,
    d_install_date string,
    d_lang string,
    d_os string,
    d_os_version string,
    d_c1 string,
    d_c2 string,
    d_c3 string,
    d_c4 string,
    d_c5 string,
    m1 string,
    m2 string,
    m3 string,
    m4 string,
    m5 string,
    time_spent_to_done string,
    user_cnt bigint
)
PARTITIONED BY (
    event string,
    app string,
    dt string
)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_2_0/processed/agg_funnel'
TBLPROPERTIES('serialization.null.format'='');

exit;

----------------------------------
-- Farm 2.0
-- Measure events
----------------------------------

-- Measure
DROP TABLE IF EXISTS custom.agg_measure CASCADE;
CREATE TABLE custom.agg_measure
(
    event_name         varchar(100),
    app_id             varchar(100),
    user_key           varchar(100),
    app_user_id        varchar(100),
    facebook_id        varchar(100),
    date               date,
    d_browser          varchar(100),
    d_browser_version  varchar(100),
    d_country_code     varchar(100),
    d_install_source   varchar(100),
    d_install_date     date,
    d_ip               varchar(100),
    d_lang             varchar(100),
    d_level            varchar(100),
    d_os               varchar(100),
    d_os_version       varchar(100),
    d_fb_source        varchar(100),
    d_c1               varchar(100),
    d_c2               varchar(100),
    d_c3               varchar(100),
    d_c4               varchar(100),
    d_c5               varchar(100),
    m1                 varchar(100),
    m2                 varchar(100),
    m3                 varchar(100),
    m4                 varchar(100),
    m5                 varchar(100),
    event_cnt          bigint
);

-- Funnel
DROP TABLE IF EXISTS custom.agg_funnel CASCADE;
CREATE TABLE custom.agg_funnel
(
    d_cohort_date      date,
    d_app_id           varchar(100),
    d_browser          varchar(100),
    d_browser_version  varchar(100),
    d_country_code     varchar(100),
    d_install_source   varchar(100),
    d_install_date     date,
    d_lang             varchar(100),
    d_os               varchar(100),
    d_os_version       varchar(100),
    d_c1               varchar(100),
    d_c2               varchar(100),
    d_c3               varchar(100),
    d_c4               varchar(100),
    d_c5               varchar(100),
    m1                 varchar(100),
    m2                 varchar(100),
    m3                 varchar(100),
    m4                 varchar(100),
    m5                 varchar(100),
    time_spent_to_done varchar(100),
    user_cnt           bigint
);

COMMIT;

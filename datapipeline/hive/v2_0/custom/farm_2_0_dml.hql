----------------------------------
-- Farm 2.0
-- Measure events
----------------------------------
USE farm_2_0;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET mapred.map.tasks.speculative.execution=false;
SET mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.stats.autogather=false;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


-- DAU
INSERT OVERWRITE TABLE agg_measure PARTITION (event, app, dt)
SELECT
    event as event_name
    ,app as app_id
    ,MD5(concat(app,uid)) as user_key
    ,uid as app_user_id
    ,snsid as facebook_id
    ,to_date(ts_pretty) as date
    ,browser as d_browser
    ,browser_version as d_browser_version
    ,country_code as d_country_code
    ,install_source as d_install_source
    ,to_date(install_ts_pretty) as d_install_date
    ,ip as d_ip
    ,lang as d_lang
    ,level as d_level
    ,os as d_os
    ,os_version as d_os_version
    ,fb_source as d_fb_source
    ,NULL as d_c1
    ,NULL as d_c2
    ,NULL as d_c3
    ,NULL as d_c4
    ,NULL as d_c5
    ,NULL as m1
    ,NULL as m2
    ,NULL as m3
    ,NULL as m4
    ,NULL as m5
    ,count(1) as event_cnt
    ,event
    ,app
    ,dt
FROM
    farm_1_1.raw_dau_daily
WHERE
    dt = '${hiveconf:rpt_date}'
GROUP BY
    MD5(concat(app,uid))
    ,uid
    ,snsid
    ,to_date(ts_pretty)
    ,browser
    ,browser_version
    ,country_code
    ,install_source
    ,to_date(install_ts_pretty)
    ,ip
    ,lang
    ,level
    ,os
    ,os_version
    ,fb_source
    ,event
    ,app
    ,dt
;


-- VIP
INSERT OVERWRITE TABLE agg_measure PARTITION (event, app, dt)
SELECT
    event as event_name
    ,app as app_id
    ,MD5(concat(app,uid)) as user_key
    ,uid as app_user_id
    ,snsid as facebook_id
    ,to_date(ts_pretty) as date
    ,browser as d_browser
    ,browser_version as d_browser_version
    ,country_code as d_country_code
    ,install_source as d_install_source
    ,to_date(install_ts_pretty) as d_install_date
    ,ip as d_ip
    ,lang as d_lang
    ,level as d_level
    ,os as d_os
    ,os_version as d_os_version
    ,fb_source as d_fb_source
    ,concat('{\"key\":\"vip_level\",\"value\":\"',properties['vip_level'],'\"}') as d_c1
    ,NULL as d_c2
    ,NULL as d_c3
    ,NULL as d_c4
    ,NULL as d_c5
    ,concat('{\"key\":\"vip_points_bal\",\"value\":\"',max(properties['vip_points_bal']),'\"}') as m1
    ,concat('{\"key\":\"vip_points_in\",\"value\":\"',sum(properties['vip_points_in']),'\"}') as m2
    ,NULL as m3
    ,NULL as m4
    ,NULL as m5
    ,count(1) as event_cnt
    ,event
    ,app
    ,dt
FROM
    farm_1_1.raw_vip_daily
WHERE
    dt = '${hiveconf:rpt_date}'
GROUP BY
    MD5(concat(app,uid))
    ,uid
    ,snsid
    ,to_date(ts_pretty)
    ,browser
    ,browser_version
    ,country_code
    ,install_source
    ,to_date(install_ts_pretty)
    ,ip
    ,lang
    ,level
    ,os
    ,os_version
    ,fb_source
    ,properties['vip_level']
    ,event
    ,app
    ,dt
;


-- Transaction
INSERT OVERWRITE TABLE agg_measure PARTITION (event, app, dt)
SELECT
    event as event_name
    ,app as app_id
    ,MD5(concat(app,uid)) as user_key
    ,uid as app_user_id
    ,snsid as facebook_id
    ,to_date(ts_pretty) as date
    ,browser as d_browser
    ,browser_version as d_browser_version
    ,country_code as d_country_code
    ,install_source as d_install_source
    ,to_date(install_ts_pretty) as d_install_date
    ,ip as d_ip
    ,lang as d_lang
    ,level as d_level
    ,os as d_os
    ,os_version as d_os_version
    ,NULL as d_fb_source
    ,concat('{\"key\":\"action\",\"value\":\"',action,'\"}') as d_c1
    ,concat('{\"key\":\"action_detail\",\"value\":\"',regexp_replace(action_detail, '\"', '\\\\"'),'\"}') as d_c2
    ,concat('{\"key\":\"location\",\"value\":\"',location,'\"}') as d_c3
    ,NULL as d_c4
    ,NULL as d_c5
    ,concat('{\"key\":\"rc_bal\",\"value\":\"',max(rc_bal),'\"}') as m1
    ,concat('{\"key\":\"rc_in\",\"value\":\"',sum(rc_in),'\"}') as m2
    ,concat('{\"key\":\"rc_out\",\"value\":\"',sum(rc_out),'\"}') as m3
    ,NULL as m4
    ,NULL as m5
    ,count(1) as event_cnt
    ,event
    ,app
    ,dt
FROM
    farm_1_1.raw_rc_transaction_daily
WHERE
    dt = '${hiveconf:rpt_date}'
GROUP BY
    MD5(concat(app,uid))
    ,uid
    ,snsid
    ,to_date(ts_pretty)
    ,action
    ,action_detail
    ,browser
    ,browser_version
    ,country_code
    ,install_source
    ,to_date(install_ts_pretty)
    ,ip
    ,lang
    ,level
    ,location
    ,os
    ,os_version
    ,event
    ,app
    ,dt
;




exit;

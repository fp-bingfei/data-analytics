use rs_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
--SET hive.mapred.reduce.tasks.speculative.execution=false;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;

SET hive.mapred.supports.subdirectories=TRUE;
SET mapred.input.dir.recursive=TRUE;


alter table transaction add if not exists partition (app='royal.spil.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.spil.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.ae.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.ae.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.de.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.de.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.fr.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.fr.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.nl.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.nl.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.plinga.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.plinga.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.th.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.th.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table transaction add if not exists partition (app='royal.us.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) location 's3://com.funplusgame.bidata/logserver/finance/royal.us.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';





insert overwrite table transaction_2ndscene PARTITION (app='royal.all.prod', year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT
 app_id,
 data_version,
 event,
 user_id,
 session_id,
 ts,
 properties
FROM
  transaction
WHERE
app in ('royal.de.prod','royal.ae.prod','royal.fr.prod','royal.nl.prod','royal.spil.prod','royal.plinga.prod','royal.us.prod','royal.th.prod') AND
year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month} and day=${hiveconf:rpt_day} and
properties['scene']='2';

-------------------------------
-- Upshot 2.0
-- Database DDL
-------------------------------

CREATE DATABASE IF NOT EXISTS upshot_2_0 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/upshot_2_0.db';

exit;

----------------------------------
-- Upshot 2.0
-- Hourly and Daily DDL with map
----------------------------------
USE upshot_2_0;

DROP TABLE IF EXISTS raw_invalid_events;
DROP TABLE IF EXISTS raw_events;
DROP TABLE IF EXISTS raw_events_daily;

-- Invalid Events
CREATE EXTERNAL TABLE IF NOT EXISTS raw_invalid_events (
    event string,
    error string,
    errorDetails array<string>,
    invalidJson string
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.upshot/kinesis/upshotStream/events/event=invalid'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Raw events hourly
CREATE EXTERNAL TABLE IF NOT EXISTS raw_events (
    data_version string,
    app_id string,
    ts string,
    ts_pretty string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections map<
        string, array<
            map<
                string, string
            >
        >
    >
)
PARTITIONED BY (
  event string,
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.upshot/kinesis/upshotStream/events/'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Raw events daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_events_daily (
    data_version string,
    app_id string,
    ts string,
    ts_pretty timestamp,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections map<
        string, array<
            map<
                string, string
            >
        >
    >
)
PARTITIONED BY (
  event string,
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.upshot/kinesis/upshotStream/events_daily/'
TBLPROPERTIES (
  'serialization.null.format'=''
);

exit;

----------------------------------
-- Upshot 2.0
-- Daily DML
----------------------------------
USE upshot_2_0; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.stats.autogather=false;

-- Update Hive metastore for partitions
MSCK REPAIR TABLE raw_events;

-- Insert events to daily table
INSERT OVERWRITE TABLE raw_events_daily PARTITION (event, app, dt)
SELECT
  data_version, 
  app_id, 
  ts, 
  ts_pretty, 
  user_id, 
  session_id, 
  properties, 
  collections, 
  event, 
  app, 
  to_date(concat(cast(year as string),'-',cast(month as string),'-',cast(day as string))) dt
FROM
  raw_events
WHERE
  to_date(concat(cast(year as string),'-',cast(month as string),'-',cast(day as string))) > '${hiveconf:rpt_date_start}' 
  AND
  to_date(concat(cast(year as string),'-',cast(month as string),'-',cast(day as string))) <= '${hiveconf:rpt_date}' 
;


exit;

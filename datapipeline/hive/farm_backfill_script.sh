#!/bin/sh

hive -d rpt_dt='2014-08-20' rpt_par=20140820 -f farm_app_user_final.hql

hive -d rpt_dt='2014-08-20' rpt_par=20140820 -f farm_dau_final.hql

hive -d rpt_dt='2014-08-20' rpt_par=20140820 -f farm_payment_final.hql

hive -d rpt_dt='2014-08-20' rpt_par=20140820 -f farm_kpi_raw_final.hql

hive -d rpt_dt='2014-08-20' rpt_par=20140820 -f farm_kpi_final.hql


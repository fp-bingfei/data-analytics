-- clean the DB;


--SET hive.exec.max.created.files=100;
SET hive.hadoop.supports.splittable.combineinputformat=true;
--SET mapreduce.input.fileinputformat.split.maxsize=134217728;
SET mapred.reduce.tasks=50;


app_user
app_user_daily_data
currency
events_1day
kpi_daily_data
dau_daily_data
kpi_raw_daily_data
payment_daily_data

ALTER TABLE app_user_daily_data DROP IF EXISTS PARTITION(dt='20140905');
ALTER TABLE dau_daily_data DROP IF EXISTS PARTITION(dt='20140905');
ALTER TABLE payment_daily_data DROP IF EXISTS PARTITION(dt='20140905');
ALTER TABLE kpi_raw_daily_data DROP IF EXISTS PARTITION(dt='20140905');
ALTER TABLE kpi_daily_data DROP IF EXISTS PARTITION(dt='20140905');


drop table user_country;
drop table app_user_temp1;
drop table app_user_temp2;
drop table app_user_temp3;
drop table app_user_temp4;
drop table dau_temp;

drop table kpi_data_temp;
drop table kpi_raw;
drop table kpi_raw_dau;
drop table kpi_raw_login;
drop table kpi_raw_newpayers;
drop table kpi_raw_newusers;
drop table kpi_raw_payerdau;
drop table kpi_raw_revenue;
drop table kpi_raw_temp;
drop table payer_user_info;
drop table payment_temp;

-- From app_user script;
drop table user_country;
drop table app_user_temp1;
drop table app_user_temp2;
drop table app_user_temp3;
drop table app_user_temp4;
drop table payer_user_info;

-- For dau script;
drop table dau_temp;
drop table payment_temp;

--For KPI RAW;
drop table kpi_raw_login;
drop table kpi_raw_dau;
drop table kpi_raw_newusers;
drop table kpi_raw_newpayers;
drop table kpi_raw_payerdau;
drop table kpi_raw_revenue;
drop table kpi_raw_temp;

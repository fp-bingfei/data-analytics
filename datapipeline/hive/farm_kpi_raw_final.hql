
SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=50;

--Change DB;
USE farm;

--login count;
create table kpi_raw_login as
SELECT 
 e.app as app,
 to_date(e.ts) as dt,
 to_date(e.install_ts) as install_dt,
 e.install_source,
 e.os,
 e.browser,
 a.country_code,
 count(1) as cnt,
 'session_start' as kpi_metric
FROM events_1day e left outer join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
 WHERE e.event='session_start'
  AND to_date(e.ts) = "${rpt_dt}"
GROUP BY 
 e.app,
 to_date(e.ts),
 to_date(e.install_ts),
 e.install_source,
 e.os,
 e.browser,
 a.country_code;

--dau count;
create table kpi_raw_dau as
SELECT 
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 count(DISTINCT user_id) AS cnt,
 'dau' as kpi_metric
FROM dau_daily_data
where dt = "${rpt_dt}" 
GROUP BY 
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code;
 
-- new user count;
 create table kpi_raw_newusers as
 SELECT 
  app,
  to_date(install_ts) as dt,
  to_date(install_ts) as install_dt,
  install_source,
  os,
  browser,
  country_code,
  count(1) AS cnt,
  'newuser' as kpi_metric
 FROM app_user_daily_data u
 WHERE to_date(u.install_ts) = "${rpt_dt}"
GROUP BY 
  app,
  to_date(install_ts),
  install_source,
  os,
  browser,
  country_code;

--payer_dau cnt
create table kpi_raw_payerdau as
SELECT 
 d.app,
 d.dt,
 d.install_dt,
 d.install_source,
 d.os,
 d.browser,
 d.country_code,
 count(d.user_id) AS cnt,
 'payer_dau' as kpi_metric
FROM dau_daily_data d 
JOIN app_user a ON d.user_id = a.user_id 
and a.is_payer='1'
WHERE d.dt= "${rpt_dt}"
GROUP BY
d.app, d.dt, d.install_dt, d.install_source, d.os, d.country_code, d.browser;

-- new payer cnt;
create table kpi_raw_newpayers as
SELECT app,
       to_date(conversion_ts) as dt,
       to_date(install_ts) as install_dt,
       install_source,
       os,
       browser,
       country_code,
       count(user_id) AS cnt,
       'newpayers' as kpi_metric
FROM app_user
WHERE to_date(conversion_ts) = "${rpt_dt}"
and is_payer = '1'
group by 
app,
to_date(conversion_ts),
to_date(install_ts),
install_source,
os,
browser,
country_code;

--revenue;
create table kpi_raw_revenue as
SELECT 
 app,
 to_date(ts) as dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 sum(amount) as cnt,
 'payment' as kpi_metric
from
 payment_daily_data
where  to_date(ts) = "${rpt_dt}"
GROUP BY
 app,
 to_date(ts),
 install_dt,
 install_source,
 os,
 browser,
 country_code;

create table kpi_raw_temp as
select
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 cnt,
 kpi_metric
from
(
 select * from kpi_raw_login
  UNION ALL
 select * from kpi_raw_dau
  UNION ALL
 select * from kpi_raw_newusers
  UNION ALL
 select * from kpi_raw_payerdau
  UNION ALL
 select * from kpi_raw_newpayers
  UNION ALL
 select * from kpi_raw_revenue 
) kpi_tables;

ALTER TABLE kpi_raw_daily_data add partition (date="${rpt_par}") location 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/kpi_raw_daily/${rpt_par}/';

INSERT OVERWRITE TABLE kpi_raw_daily_data partition (date="${rpt_par}") select * from kpi_raw_temp;


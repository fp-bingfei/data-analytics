----------------------------------
-- RS payments
-- maintain it for revenue issue
-- or for reference
----------------------------------
CREATE DATABASE IF NOT EXISTS rs;
USE rs;

CREATE EXTERNAL TABLE IF NOT EXISTS rs_ae_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/ae/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_de_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/de/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_fr_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/fr/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_nl_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/nl/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_th_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/th/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_us_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/us/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_plinga_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount string,
    gameamount string,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/plinga/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS rs_spil_payment (
    id struct<
        oid: string
    >,
    snsid string,
    uid int,
    level int,
    userInfo struct<
        snsid: string,
        lang: string,
        gender: int,
        ip: string
    >,
    amount int,
    gameamount int,
    tid string,
    payTime string,
    currency string,
    type string,
    item string,
    logTime int,
    trackRef string,
    status int,
    test int,
    request_id string,
    mobile int,
    starterPack int,
    starterPackType string
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.id" = "_id",
    "mapping.oid" = "$oid"
)
STORED AS TEXTFILE
LOCATION 's3://com.funplusgame.bidata/etl/rs/spil/mongodb/payment/20150204'
TBLPROPERTIES (
  'serialization.null.format'=''
);

use ha_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.mapred.reduce.tasks.speculative.execution=false;
-- SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;

alter table raw_sendgrid_daily add if not exists partition (app_id='ff2.th.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/ff2.th.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app_id='ff2.us.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/ff2.us.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app_id='ha.th.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/ha.th.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app_id='ha.us.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/ha.us.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';


SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_raw_sendgrid_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');


INSERT OVERWRITE TABLE copy_raw_sendgrid_daily PARTITION (app='${hiveconf:rpt_all_app}', dt='${hiveconf:rpt_date}')
SELECT   snsid,
  email,
  category,
  uid,
  app as app_id,
  from_unixtime(cast(time as BIGINT)),
  ip,
  event,
  day,
  campaign
FROM 
raw_sendgrid_daily WHERE dt > '${hiveconf:rpt_date_d3}' AND dt <= '${hiveconf:rpt_date}';

exit;



-----------------------------
-- Query to generate dim_user
-----------------------------

use ha_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
-- SET hive.mapred.map.tasks.speculative.execution=false;

set rpt_date_days_back=${hiveconf:rpt_date_d7};

INSERT OVERWRITE TABLE dim_user PARTITION(app, dt) 
select
  tu.user_key AS id,
  tu.user_key,
  tu.app_id,
  tu.app_user_id,
  tu.snsid,
  tu.facebook_id,
  COALESCE(du.install_ts,tu.install_ts) as install_ts,
  COALESCE(du.install_date, tu.install_date) as install_date,
  COALESCE(rad.tracker_name[0], du.install_source) AS install_source,
  COALESCE(rad.tracker_name[1], du.install_subpublisher) AS install_subpublisher,
  COALESCE(rad.tracker_name[2], du.install_campaign) AS install_campaign,
  case when tu.date_start = tu.install_date then tu.language else du.install_language end AS install_language,
  case when tu.date_start = tu.install_date then tu.locale else du.install_locale end AS install_locale,
  case when tu.date_start = tu.install_date then tu.country_code else du.install_country_code end AS install_country_code,
  case when tu.date_start = tu.install_date then tu.country else du.install_country end AS install_country,
  case when tu.date_start = tu.install_date then tu.os else du.install_os end AS install_os,
  case when tu.date_start = tu.install_date then tu.device else du.install_device end AS install_device,
  case when tu.date_start = tu.install_date then NULL else du.install_device_alias end AS install_device_alias,
  case when tu.date_start = tu.install_date then tu.browser else du.install_browser end AS install_browser,
  case when tu.date_start = tu.install_date then tu.gender else du.install_gender end AS install_gender,
  case when tu.date_start = tu.install_date then datediff(tu.install_date, tu.birthday) else du.install_age end AS install_age,
  tu.language,
  tu.locale,
  tu.birthday,
  tu.gender,
  tu.country_code,
  tu.country,
  tu.os,
  tu.os_version,
  tu.device,
  NULL AS device_alias,
  tu.browser,
  tu.browser_version,
  tu.app_version,
  COALESCE(tl.level_end, tu.level_end, tu.level_start) AS level,
  COALESCE(tl.levelup_ts,du.levelup_ts) AS levelup_ts,
  tu.ab_experiment,
  tu.ab_variant,
  CASE WHEN COALESCE(du.is_payer,0) = 1 THEN 1 WHEN COALESCE(tp.purchase_cnt, 0) > 0 THEN 1 ELSE 0 END AS is_payer,
  COALESCE (du.conversion_ts, tp.conversion_ts) as conversion_ts,
  COALESCE(tp.revenue_usd,0) AS total_revenue_usd,
  COALESCE(tp.purchase_cnt,0) AS payment_cnt,
  tu.last_login_ts,
  du.email,
  COALESCE (tu.last_ip, du.last_ip) AS last_ip
  , tu.app
  , '${hiveconf:rpt_date}' AS dt
FROM 
(
SELECT * FROM tmp_user_daily_login WHERE dt ='${hiveconf:rpt_date}'
) tu 
LEFT OUTER JOIN dim_user du 
ON (du.dt = '${hiveconf:rpt_date_yesterday}' AND tu.app = du.app and tu.user_key = du.user_key)
LEFT OUTER JOIN 
(
SELECT app, user_key, min(conversion_ts) AS conversion_ts, SUM(revenue_usd) AS revenue_usd, SUM(purchase_cnt) AS purchase_cnt
FROM tmp_user_payment GROUP BY app, user_key
) tp ON (tu.app = tp.app and tu.user_key = tp.user_key )
LEFT OUTER JOIN 
(
SELECT app, user_key, max(level_end) AS level_end, max(levelup_ts) AS levelup_ts 
FROM tmp_user_level WHERE dt ='${hiveconf:rpt_date}'
GROUP BY app, user_key
) tl 
ON (tu.app = tl.app and tu.user_key = tl.user_key )
LEFT OUTER JOIN
(
SELECT * FROM (
SELECT app, userid, split(tracker_name,'::') as tracker_name
, row_number() OVER (PARTITION BY app, userid ORDER BY timestamp DESC) rnum 
FROM raw_adjust_daily WHERE dt > '${hiveconf:rpt_date_days_back}' and dt<='${hiveconf:rpt_date}') A where rnum=1
) rad
ON (tu.app = rad.app AND tu.snsid = rad.userid)
UNION ALL
SELECT du.id, du.user_key, du.app_id, du.app_user_id, du.snsid, du.facebook_id, du.install_ts
, du.install_date
, COALESCE(rad.tracker_name[0], du.install_source) AS install_source
, COALESCE(rad.tracker_name[1], du.install_subpublisher) AS install_subpublisher
, COALESCE(rad.tracker_name[2], du.install_campaign) AS install_campaign
, du.install_language,du.install_locale
, du.install_country_code, du.install_country,du.install_os,du.install_device,du.install_device_alias,du.install_browser
, du.install_gender, du.install_age, du.language, du.locale,du.birthday,du.gender,du.country_code,du.country,du.os
, du.os_version,du.device,du.device_alias,du.browser,du.browser_version,du.app_version,du.level,du.levelup_ts
,du.ab_experiment,du.ab_variant,du.is_payer,du.conversion_ts,du.total_revenue_usd,du.payment_cnt,du.last_login_ts
, du.email, du.last_ip, du.app, '${hiveconf:rpt_date}' AS dt
  FROM (SELECT * FROM dim_user WHERE dt='${hiveconf:rpt_date_yesterday}') du 
  LEFT OUTER JOIN tmp_user_daily_login tu ON
  (tu.app = du.app and tu.user_key = du.user_key AND tu.dt ='${hiveconf:rpt_date}')
  LEFT OUTER JOIN
(
SELECT * FROM (
SELECT app, userid, split(tracker_name,'::') as tracker_name
, row_number() OVER (PARTITION BY app, userid ORDER BY timestamp DESC) rnum 
FROM raw_adjust_daily WHERE dt > '${hiveconf:rpt_date_days_back}' and dt<='${hiveconf:rpt_date}') A where rnum=1
) rad
ON (du.app = rad.app AND du.snsid = rad.userid)
  WHERE tu.app_user_id IS NULL;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
  
ALTER TABLE copy_dim_user DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_dim_user PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT id, user_key, app_id, app_user_id, snsid, facebook_id, install_ts , install_date, install_source
, install_subpublisher, install_campaign, install_language, install_locale, install_country_code, install_country
, install_os, install_device, install_device_alias, install_browser, install_gender, install_age, language, locale
, birthday, gender, country_code, country, os, os_version, device, device_alias, browser, browser_version, app_version
, level, levelup_ts, ab_experiment, ab_variant, is_payer, conversion_ts, total_revenue_usd, payment_cnt, last_login_ts
, email, last_ip
FROM dim_user WHERE dt='${hiveconf:rpt_date}';

exit;

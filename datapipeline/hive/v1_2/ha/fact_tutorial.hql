use ha_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;

INSERT OVERWRITE TABLE fact_tutorial PARTITION (app,dt)
select 
distinct MD5(concat(app_id,user_id,session_id,ts_pretty,properties['tutorial_step'])) as id,
app_id,
properties['app_version'] as app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
properties['os'] as os,
properties['os_version']  as os_version,
properties['device'] as device,
properties['browser'] as browser,
properties['browser_version'] as browser_version,
properties['country_code'] as country_code,
properties['ip'] as ip,
properties['lang'] as language,
properties['locale'] as locale,
properties['level'] as level,
properties['tutorial_step'] as tutorial_step,
properties['tutorial_step_desc'] as tutorial_step_desc,
app,
to_date(ts_pretty) as dt
from (SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, properties, app, dt
FROM raw_events_daily
WHERE  dt ='${hiveconf:rpt_date}' and event='tutorial' and app=app_id)s;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_tutorial DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_tutorial partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id,app_id,app_version,user_key,app_user_id,date,ts,session_id,os,os_version,device,browser,browser_version,country_code,ip,language,locale,
level,tutorial_step,tutorial_step_desc
from fact_tutorial where dt ='${hiveconf:rpt_date}';

exit;






use ha_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;

INSERT OVERWRITE TABLE fact_pumpkin PARTITION (app,dt)
SELECT
distinct MD5(concat(s.app_id, s.event, s.user_id, s.session_id, s.ts_pretty)) as id,
s.app_id as app_id,
s.properties['app_version'] app_version,
MD5(concat(s.app_id,s.user_id)) as user_key,
s.user_id as app_user_id,
s.ts_pretty,
to_date(s.ts_pretty) as date,
s.session_id,
s.properties['install_ts_pretty'] as install_ts,
to_date(s.properties['install_ts_pretty']) as install_date,
s.properties['os'] as os,
s.properties['os_version'] as os_version,
s.properties['browser'] as browser,
s.properties['browser_version'] as browser_version,
s.properties['country_code'] as country_code,
s.properties['level'] as level,
s.properties['lang'] as language,
s.properties['score'] as score,
s.app as app,
s.dt
FROM
(SELECT bi_version, app_id, ts,event, ts_pretty,user_id, session_id, properties, app, dt
FROM raw_events_daily
--lateral view outer explode(collections.ab_tests) coll1 AS ab
WHERE  dt = '${hiveconf:rpt_date}'  and event='pumpkinsmashing'
) s;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_pumpkin DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_pumpkin partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') select
id,app_id,app_version,user_key,app_user_id,ts_pretty,date,session_id,install_ts,install_date,os,os_version,browser,browser_version,country_code,level, language, score
from fact_pumpkin where dt='${hiveconf:rpt_date}';




exit;
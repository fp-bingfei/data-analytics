
use ha_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;

INSERT OVERWRITE TABLE fact_items_received PARTITION (app,dt)
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='level_up' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) l
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='payment' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) p
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, transaction_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['transaction_type'] as transaction_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='transaction' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) t
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, timer_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['timer_type'] as timer_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='timer' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) m
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, mission_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['mission_type'] as mission_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='mission' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) n
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='achievement' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) a
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='gift_received' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) g
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, action_type 
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['action_type'] as action_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='item_actioned' and app = 'ha.us.prod' and dt ='${hiveconf:rpt_date}'
) i
;

INSERT OVERWRITE TABLE fact_items_received PARTITION (app,dt)
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='level_up' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) l
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='payment' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) p
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, transaction_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['transaction_type'] as transaction_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='transaction' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) t
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, timer_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['timer_type'] as timer_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='timer' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) m
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, mission_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['mission_type'] as mission_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='mission' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) n
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='achievement' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) a
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='gift_received' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) g
UNION ALL
SELECT
distinct
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, action_type 
, item_id
, item_name
, item_type
, item_class
, item_amount
, item_level
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['action_type'] as action_type, properties['level'] as level
, pr['item_id'] as item_id, pr['item_name'] as item_name, pr['item_type'] as item_type,pr['item_class'] as item_class, pr['item_amount'] as item_amount, pr['item_level'] as item_level
FROM
raw_events_daily lateral view explode(collections['items_received']) coll AS pr
WHERE event='item_actioned' and app = 'ha.th.prod' and dt ='${hiveconf:rpt_date}'
) i
;
-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


ALTER TABLE copy_fact_items_received DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_items_received partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id
, app_id
, user_key
, level
, event
, date
, ts
, action_type
, item_received_id
, item_received_name
, item_received_type
, item_received_class
, item_received_amount
, item_received_level
from fact_items_received where dt ='${hiveconf:rpt_date}';


exit;
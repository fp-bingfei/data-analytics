use ha_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;


insert overwrite table agg_load_step partition (app,dt)
select app_id
      ,to_date(ts_pretty)
      ,properties['load_step']
      ,properties['country_code']
      ,properties['load_step_desc']
      ,properties['app_version']
      ,properties['level']
      ,properties['browser_version']
      ,properties['lang']
      ,properties['os']
      ,properties['browser']
      ,properties['os_version']
      ,count(distinct md5(concat(app_id,user_id))) as user_cnt
      ,app
      ,dt
from    raw_events_daily
where   event = 'load_step' 
and dt ='${hiveconf:rpt_date}'
group by app_id
      ,to_date(ts_pretty)
      ,properties['load_step']
      ,properties['country_code']
      ,properties['load_step_desc']
      ,properties['app_version']
      ,properties['level']
      ,properties['browser_version']
      ,properties['lang']
      ,properties['os']
      ,properties['browser']
      ,properties['os_version']
      ,app
      ,dt;
      
      
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
      
ALTER TABLE copy_agg_load_step DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_agg_load_step partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select app_id
      ,date
      ,load_step
      ,country_code
      ,load_step_desc
      ,app_version
      ,level
      ,browser_version
      ,language
      ,os
      ,browser
      ,os_version
      ,user_cnt
from agg_load_step
where  dt ='${hiveconf:rpt_date}';
;


USE ha_1_2; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
-- SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=10000;
SET hive.exec.max.dynamic.partitions.pernode=5000;

-- Update Hive metastore for partitions
-- invalid
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- achievement
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='achievement',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- special_offer
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='special_offer',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- facebook_register
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='facebook_register',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- gift_received
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='gift_received',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- item_actioned
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='item_actioned',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- level_up
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='level_up',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- load_step
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='load_step',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- message_sent
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='message_sent',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- mission
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='mission',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- new_user
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- payment
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='payment',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- personal_info
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='personal_info',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- session_end
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- session_start
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- timer
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='timer',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- transaction
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='transaction',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- tutorial
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='tutorial',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- market_stand
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='market_stand',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- notification
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='notification',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- pumpkinsmashing
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='pumpkinsmashing',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- bigdinner
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.us.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='bigdinner',app='ha.th.prod',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

-- Insert events to daily table
INSERT OVERWRITE TABLE raw_events_daily PARTITION (event, app, dt)
SELECT
  bi_version, 
  app_id, 
  case when cast(ts as bigint) > 2147483648 then cast(ts as bigint)/1000 
         else cast(ts as int) end, 
  ts_pretty, 
  user_id, 
  session_id, 
  properties, 
  collections, 
  event, 
  app, 
  '${hiveconf:rpt_date}'
FROM
  raw_events
WHERE
  (
    (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
    OR
    (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
  )
  AND
  to_date(ts_pretty) = '${hiveconf:rpt_date}'
;


exit;

USE ha_1_2; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;


insert overwrite table agg_market_stand partition(app,dt)
select t1.app_id
      ,to_date(t1.ts_pretty) date
      ,coalesce(t2.country,'Unknown') country
      ,t1.properties['app_version'] app_version
      ,t1.properties['lang'] lanuage
      ,t1.properties['os'] os
      ,t1.properties['os_version'] os_version
      ,t1.properties['browser'] browser
      ,t1.properties['browser_version'] browser_version
      ,t1.properties['item_name'] item_name
      ,t1.properties['item_id'] item_id
      ,t1.properties['action'] action
      ,count(distinct md5(concat(app_id,user_id))) user_cmt
      ,count(1) action_cnt
      ,app
      ,dt
from  raw_events_daily t1
left join dim_country t2
on t1.properties['country_code'] = t2.country_code
where t1.event='market_stand'
and t1.dt = '${hiveconf:rpt_date}'
group by t1.app_id
      ,to_date(t1.ts_pretty)
      ,coalesce(t2.country,'Unknown')
      ,t1.properties['app_version']
      ,t1.properties['lang']
      ,t1.properties['os']
      ,t1.properties['os_version']
      ,t1.properties['browser']
      ,t1.properties['browser_version']
      ,t1.properties['item_name']
      ,t1.properties['item_id']
      ,t1.properties['action']
      ,app
      ,dt;
      

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


ALTER TABLE copy_agg_market_stand DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_agg_market_stand partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select  app_id 
       ,date 
       ,country 
       ,app_version
       ,language 
       ,os 
       ,os_version 
       ,browser 
       ,browser_version 
       ,item_name 
       ,item_id 
       ,action 
       ,user_cnt 
       ,action_cnt
from  agg_market_stand
where dt='${hiveconf:rpt_date}';

exit;

use ha_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;
INSERT OVERWRITE TABLE fact_new_user PARTITION (app,dt)
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
properties['install_ts_pretty'] as install_ts,
to_date(properties['install_ts_pretty']) as install_date,
session_id,
properties['facebook_id'] facebook_id,
properties['install_source'] install_source,
properties['os'] os,
properties['os_version'] os_version,
properties['browser'] browser,
properties['browser_version'] browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['frist_name'] first_name,
properties['gender'] gender,
from_unixtime(unix_timestamp(properties['birthday'], 'MM/dd/yyyy'), 'yyyy-MM-dd') as birthday,
properties['email'] email,
properties['ip'] ip,
properties['lang'] language,
properties['locale']  locale,
null  as ab_experiment,
null  as ab_variant,
properties['idfa'] as idfa,
properties['idfv'] as idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
app,
dt
FROM raw_events_daily
--lateral view outer explode(collections.ab_tests) coll1 AS ab
WHERE  dt = '${hiveconf:rpt_date}' and event='new_user';



-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_new_user DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_new_user partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') select 
id,app_id,app_version,user_key,app_user_id,install_ts,install_date,session_id,facebook_id,install_source,os,os_version,browser,browser_version,device,country_code,level,first_name,gender,birthday,email,ip,language,locale,ab_experiment,ab_variant, idfa, idfv, gaid, mac_address, android_id
from fact_new_user where dt='${hiveconf:rpt_date}';

exit;



----------------------------------
-- Happy Acres 1.2
-- Hourly and Daily DDL with map
----------------------------------
USE ha_1_2;

DROP TABLE IF EXISTS raw_invalid_events;
DROP TABLE IF EXISTS raw_events;
DROP TABLE IF EXISTS raw_events_daily;

-- Invalid Events
CREATE EXTERNAL TABLE IF NOT EXISTS raw_invalid_events (
    event string,
    error string,
    errorDetails array<string>,
    invalidJson string
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/events/event=invalid'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Raw events hourly
CREATE EXTERNAL TABLE IF NOT EXISTS raw_events (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections map<
        string, array<
            map<
                string, string
            >
        >
    >
)
PARTITIONED BY (
  event string,
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/events/'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Raw events daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_events_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections map<
        string, array<
            map<
                string, string
            >
        >
    >
)
PARTITIONED BY (
  event string,
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/events_daily/'
TBLPROPERTIES (
  'serialization.null.format'=''
);

exit;

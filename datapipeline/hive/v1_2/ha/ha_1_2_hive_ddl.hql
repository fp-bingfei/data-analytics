----------------------------------------------------------------------------------------------
-- CREATE DATABASE ha_1_2 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/ha_1_2.db';
----------------------------------------------------------------------------------------------

use ha_1_2;


CREATE EXTERNAL TABLE IF NOT EXISTS fact_session(
    `id` string, 
    `app_id` string, 
    `app_version` string, 
    `user_key` string, 
    `app_user_id` string,
    `date_start` string, 
    `date_end` string, 
    `ts_start` timestamp, 
    `ts_end` timestamp, 
    `install_ts` timestamp, 
    `install_date` string, 
    `session_id` string, 
    `facebook_id` string, 
    `install_source` string, 
    `os` string, 
    `os_version` string, 
    `browser` string, 
    `browser_version` string, 
    `device` string, 
    `country_code` string, 
    `level_start` int, 
    `level_end` int,
    `first_name` string, 
    `gender` string, 
    `birthday` string, 
    `email` string, 
    `ip` string, 
    `language` string, 
    `locale` string, 
    `rc_wallet_start` int, 
    `rc_wallet_end` int, 
    `coin_wallet_start` bigint, 
    `coin_wallet_end` bigint, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `session_length_sec` int, 
    `idfa` string, 
    `idfv` string, 
    `gaid` string, 
    `mac_address` string, 
    `android_id` string
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_session'
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS fact_revenue
(
    `id` string, 
    `app_id` string, 
    `app_version` string, 
    `user_key` string, 
    `app_user_id` string, 
    `date` string, 
    `ts` timestamp, 
    `install_ts` timestamp, 
    `install_date` string, 
    `session_id` string, 
    `level` int, 
    `os` string, 
    `os_version` string, 
    `device` string, 
    `browser` string, 
    `browser_version` string, 
    `country_code` string, 
    `install_source` string, 
    `ip` string, 
    `language` string, 
    `locale` string, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `coin_wallet` bigint, 
    `rc_wallet` int, 
    `payment_processor` string, 
    `product_id` string, 
    `product_name` string, 
    `product_type` string, 
    `coins_in` bigint, 
    `rc_in` int, 
    `currency` string, 
    `revenue_currency` double, 
    `revenue_usd` double, 
    `transaction_id` string, 
    `idfa` string, 
    `idfv` string, 
    `gaid` string, 
    `mac_address` string,
    `android_id`  string	  
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_revenue'
TBLPROPERTIES('serialization.null.format'='');

CREATE  TABLE `tmp_user_daily_login`(
  `date_start` string,
  `user_key` string,
  `app_id` string,
  `app_user_id` string,
  `snsid` string,
  `facebook_id` string,
  `install_ts` timestamp,
  `install_date` string,
  `birthday` string,
  `app_version` string,
  `level_start` int,
  `level_end` int,
  `os` string,
  `os_version` string,
  `country_code` string,
  `country` string,
  `last_ip` string,
  `install_source` string,
  `language` string,
  `locale` string,
  `gender` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `ab_experiment` string,
  `ab_variant` string,
  `session_cnt` int,
  `session_length_sec` double,
  `last_login_ts` timestamp)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='');



-- ufact_levelp
CREATE EXTERNAL TABLE IF NOT EXISTS fact_levelup 
(
id  STRING,
user_key  STRING,
app_id  STRING,
app_version STRING,
app_user_id  STRING,
session_id STRING,
previous_level  SMALLINT,
previous_levelup_date  STRING,
previous_levelup_ts  TIMESTAMP,
current_level  SMALLINT,
levelup_date STRING,
levelup_ts TIMESTAMP,
browser STRING,
browser_version STRING,
os STRING,
os_version STRING,
device STRING,
country_code STRING,
ip STRING,
language STRING,
locale STRING,
ab_experiment STRING,
ab_variant STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_levelup'
TBLPROPERTIES('serialization.null.format'='');

-- tmp_user_level
CREATE  TABLE tmp_user_level(
  date string,
  user_key string,
  app_id string,
  levelup_ts timestamp,
  level_start int,
  level_end int)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='');

-- tmp_ledger_daily
CREATE  TABLE tmp_ledger_daily(
  date_start STRING,
  app_name string,
  user_key string,
  uid string,
  coins_bal double,
  coins_in bigint,
  coins_out bigint,
  rc_bal bigint,
  rc_in bigint,
  rc_out bigint  
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='');


-- dim_user
CREATE EXTERNAL TABLE IF NOT EXISTS dim_user
(
  id  STRING,
  user_key STRING,
  app_id STRING,
  app_user_id  STRING,
  snsid STRING,  
  facebook_id STRING,
  install_ts  TIMESTAMP,
  install_date  STRING,
  install_source  STRING,
  install_subpublisher  STRING,
  install_campaign  STRING,
  install_language  STRING,
  install_locale  STRING,
  install_country_code  STRING,
  install_country  STRING,
  install_os  STRING,
  install_device  STRING,
  install_device_alias  STRING,
  install_browser  STRING,
  install_gender  STRING,
  install_age  STRING,
  language  STRING,
  locale STRING,
  birthday  STRING,
  gender STRING,
  country_code  STRING,
  country  STRING,
  os STRING,
  os_version STRING,
  device  STRING,
  device_alias  STRING,
  browser STRING,
  browser_version STRING,
  app_version STRING,
  level INT,
  levelup_ts TIMESTAMP,
  ab_experiment STRING,
  ab_variant STRING,
  is_payer INT,
  conversion_ts TIMESTAMP,
  total_revenue_usd DECIMAL(12,4),
  payment_cnt INT,
  last_login_ts TIMESTAMP,
  email STRING,
  last_ip STRING
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/dim_user'
TBLPROPERTIES('serialization.null.format'='');

-- raw_adjust_daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_adjust_daily
(
  adid  STRING,
  userid STRING,
  game STRING,
  tracker  STRING,
  tracker_name STRING,
  app_id  STRING,
  ip_address  STRING,
  idfa  STRING,
  android_id STRING,
  mac_sha1  STRING,
  idfa_md5  STRING,
  country  STRING,
  timestamp  INT,
  mac_md5  STRING,
  gps_adid  STRING,
  device_name  STRING,
  os_name  STRING,
  os_version  STRING
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
LOCATION 's3://com.funplusgame.bidata/adjust'
TBLPROPERTIES('serialization.null.format'='');

-- fact_tutorial
CREATE EXTERNAL TABLE IF NOT EXISTS fact_tutorial
(
`id`          string,
`app_id`          string,
`app_version` string,
`user_key`   string,
`app_user_id` string,
`date`          string,
`ts`         timestamp,
`session_id`  string,
`os`          string,
`os_version`      string,
`device`    string,
`browser`   string,
`browser_version` string,
`country_code`  string,
`ip`          string,
`language`  string,
`locale`          string,
`level`   int,
`tutorial_step`   int,
`tutorial_step_desc`    string
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_tutorial'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE `fact_dau_snapshot`(
  `id` string,
  `user_key` string,
  `date` string,
  `app_id` string,
  `app_version` string,
  `level_start` int,
  `level_end` int,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `country` string,
  `language` string,
  `ab_experiment` string,
  `ab_variant` string,
  `is_new_user` smallint,
  `rc_in` int,
  `coins_in` bigint,
  `rc_out` int,
  `coins_out` bigint,
  `rc_wallet` int,
  `coin_wallet` bigint,
  `is_payer` smallint,
  `is_converted_today` smallint,
  `revenue_usd` double,
  `payment_cnt` int,
  `session_cnt` int,
  `playtime_sec` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_dau_snapshot'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE fact_mission
(
`id`  string,
`mission_id`  string, 
`app_id`  string, 
`app_version` string, 
`user_key`  string, 
`app_user_id` string, 
`date`  string, 
`ts`  timestamp,    
`session_id`  string, 
`os`  string, 
`os_version`  string, 
`device`  string, 
`browser` string, 
`browser_version` string, 
`country_code`  string, 
`ip`  string, 
`language`  string, 
`mission_start_ts`  timestamp,  
`mission_status`  int,
`level` int 
)PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_mission'
TBLPROPERTIES('serialization.null.format'='')
;

create external table fact_mission_objective
(
`id`  string,
`app_id`  string,
`date`  string,
`objective_id`  string,
`objective_name`  string,
`objective_type`  string,
`objective_amount`  int,
`objective_amount_remaining`  int
)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_mission_objective'
TBLPROPERTIES('serialization.null.format'='')
;

create external table fact_mission_parameter
(
`id`  string,
`app_id`  string,
`date`  string,
`parameter_name`  string,
`parameter_value` int
)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_mission_parameter'
TBLPROPERTIES('serialization.null.format'='')
;

create external table fact_mission_statistic
(
`id`  string,
`app_id`  string,
`date`  string,
`statistic_name`  string,
`statistic_value` int
)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_mission_statistic'
TBLPROPERTIES('serialization.null.format'='')
;

-- fact_player_resources
create external table fact_player_resources
(
id string,
app_id string,
event string,
date string,
ts timestamp,
resource_id string,
resource_name string,
resource_type string,
resource_amount int
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_player_resources'
TBLPROPERTIES('serialization.null.format'='');




----dim_user_device
CREATE EXTERNAL TABLE dim_user_device(
  app_id string,
  app_user_id string,  
  user_key string,  
  idfa string,
  idfv string,
  gaid string,
  mac_address string,
  android_id string,
  first_seen_ts timestamp
)  
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/dim_user_device'
TBLPROPERTIES('serialization.null.format'='');

--- fact_items_spent
CREATE EXTERNAL TABLE `fact_items_spent`(
  `id` string,
  `app_id` string,
  `user_key` string,
  `level` int,
  `event` string,
  `date` string,
  `ts` timestamp,
  `action_type` string,
  `item_spent_id` string,
  `item_spent_name` string,
  `item_spent_type` string,
  `item_spent_class` string,
  `item_spent_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_items_spent'
TBLPROPERTIES('serialization.null.format'='');


------ fact_items_received
CREATE EXTERNAL TABLE `fact_items_received`(
  `id` string,
  `app_id` string,
  `user_key` string,
  `level` int,
  `event` string,
  `date` string,
  `ts` timestamp,
  `action_type` string,
  `item_received_id` string,
  `item_received_name` string,
  `item_received_type` string,
  `item_received_class` string,
  `item_received_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_items_received'
TBLPROPERTIES('serialization.null.format'='');



---- fact_items_target
CREATE EXTERNAL TABLE `fact_items_target`(
  `id` string,
  `app_id` string,
  `user_key` string,
  `level` int,
  `event` string,
  `date` string,
  `ts` timestamp,
  `action_type` string,
  `item_target_id` string,
  `item_target_name` string,
  `item_target_type` string,
  `item_target_class` string,
  `item_target_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_items_target'
TBLPROPERTIES('serialization.null.format'='');



--- fact_resources_spent
CREATE EXTERNAL TABLE `fact_resources_spent`(
  `id` string,
  `app_id` string,
  `user_key` string,
  `level` int,
  `event` string,
  `date` string,
  `ts` timestamp,
  `action_type` string,
  `resource_spent_id` string,
  `resource_spent_name` string,
  `resource_spent_type` string,
  `resource_spent_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_resources_spent'
TBLPROPERTIES('serialization.null.format'='');



----- fact_resources_received
CREATE EXTERNAL TABLE `fact_resources_received`(
  `id` string,
  `app_id` string,
  `user_key` string,
  `level` int,
  `event` string,
  `date` string,
  `ts` timestamp,
  `action_type` string,
  `resource_received_id` string,
  `resource_received_name` string,
  `resource_received_type` string,
  `resource_received_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_resources_received'
TBLPROPERTIES('serialization.null.format'='');


-- raw_sendgrid_daily
CREATE EXTERNAL TABLE raw_sendgrid_daily (
  snsid string,
  email string,
  category string,
  uid string,
  app string,
  time string,
  ip string,
  event string,
  day string,
  campaign string)
PARTITIONED BY (
  app_id string,
  dt string)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
LOCATION 's3://com.funplus.bithirdparty/sendgrid'
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS fact_new_user(
    `id` string, 
    `app_id` string, 
    `app_version` string, 
    `user_key` string, 
    `app_user_id` string, 
    `install_ts` timestamp, 
    `install_date` string, 
    `session_id` string, 
    `facebook_id` string, 
    `install_source` string, 
    `os` string, 
    `os_version` string, 
    `browser` string, 
    `browser_version` string, 
    `device` string, 
    `country_code` string, 
    `level` int, 
    `first_name` string, 
    `gender` string, 
    `birthday` string, 
    `email` string, 
    `ip` string, 
    `language` string, 
    `locale` string, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `idfa` string, 
    `idfv` string, 
    `gaid` string, 
    `mac_address` string, 
    `android_id` string
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_new_user'
TBLPROPERTIES('serialization.null.format'='');

  
  -- dim_country
CREATE EXTERNAL TABLE IF NOT EXISTS dim_country (
  country_code string,
  country string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
  's3://com.funplus.datawarehouse/common/dim_country'
TBLPROPERTIES (
  'serialization.null.format'='');

-- currency
CREATE EXTERNAL TABLE currency (
  id string,
  dt date,
  currency string,
  factor float)
PARTITIONED BY (
  date string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
  's3://com.funplusgame.bidata/currency'
TBLPROPERTIES (
  'serialization.null.format'='');


create external table dim_user_level
(
  user_key STRING,
  app_id STRING,
  app_user_id  STRING,
  levelup_ts timestamp,  
  level int
 )
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/dim_user_level'
TBLPROPERTIES('serialization.null.format'='');


create table tmp_level_up
(
  user_key STRING,
  app_id STRING,
  app_user_id  STRING,
  levelup_ts timestamp,  
  level int
 )
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='');


create external table IF NOT EXISTS agg_load_step
(
  app_id STRING,
  date STRING,
  load_step STRING,
  country_code STRING,
  load_step_desc STRING,
  app_version STRING,
  level bigint,
  browser_version STRING,
  language STRING,
  os STRING,
  browser STRING,
  os_version STRING,
  user_cnt bigint
 ) 
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/agg_load_step'
TBLPROPERTIES('serialization.null.format'='');


create table IF NOT EXISTS copy_agg_load_step
(
  app_id STRING,
  date STRING,
  load_step STRING,
  country_code STRING,
  load_step_desc STRING,
  app_version STRING,
  level bigint,
  browser_version STRING,
  language STRING,
  os STRING,
  browser STRING,
  os_version STRING,
  user_cnt bigint
 ) 
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/copy/agg_load_step'
TBLPROPERTIES('serialization.null.format'='');


create external table IF NOT EXISTS agg_market_stand
(
  app_id STRING,
  date STRING,
  country STRING,
  app_version STRING,
  language STRING,
  os STRING,
  os_version STRING,
  browser STRING,
  browser_version STRING,
  item_name STRING,
  item_id STRING,
  action STRING,
  user_cnt bigint,
  action_cnt bigint
 ) 
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/agg_market_stand'
TBLPROPERTIES('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS fact_bigdinner(
    `id` string,
    `app_id` string,
    `app_version` string,
    `user_key` string,
    `app_user_id` string,
    `ts_pretty` timestamp,
    `date` string,
    `session_id` string,
    `install_ts` timestamp,
    `install_date` string,
    `os` string,
    `os_version` string,
    `browser` string,
    `browser_version` string,
    `country_code` string,
    `level` int,
    `language` string,
    `fuid` string,
    `eatnum` int
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/ha_1_2/processed/fact_bigdinner'
TBLPROPERTIES('serialization.null.format'='');



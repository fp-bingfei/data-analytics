----------------------------------
-- Query to generate fact_levelup
----------------------------------
use ha_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;

INSERT OVERWRITE TABLE fact_levelup PARTITION (app, dt)
select 
MD5(concat(l.app_id, l.event, l.user_id, l.session_id,  l.ts)) as id
, md5(concat(l.app, l.user_id)) AS user_key, l.app_id
, l.properties['app_version']
, l.user_id as app_user_id
, l.session_id
, l.properties['from_level'] AS previous_level
, to_date(COALESCE(lag(l.ts_pretty) over (partition by l.app, l.user_id order by l.properties['level']), prev.levelup_ts,  nu.ts_pretty)) AS previous_levelup_date
, COALESCE(lag(l.ts_pretty) over (partition by l.app, l.user_id order by l.properties['level']), prev.levelup_ts, nu.ts_pretty) AS previous_levelup_ts
, l.properties['level'] AS level
, to_date(l.ts_pretty) AS levelup_date 
, l.ts_pretty AS levelup_ts
, l.properties['browser']
, l.properties['browser_version']
, l.properties['os']
, l.properties['os_version']
, l.properties['device']
, l.properties['country_code']
, l.properties['ip']
, l.properties['lang'] AS language
, null as locale
, null as ab_experiment
, null as ab_variant
, l.app
, l.dt
from 
(
SELECT * FROM (
SELECT app, dt, event,app_id, user_id, session_id, ts, ts_pretty, properties
, ROW_NUMBER() OVER (PARTITION BY app, user_id, properties['level'] ORDER BY ts_pretty ASC) AS rnum
FROM raw_events_daily
WHERE   dt ='${hiveconf:rpt_date}' and event='level_up' and app=app_id
) X WHERE rnum = 1
)  l
left outer join 
(select app_id,app, app_user_id, levelup_ts from dim_user_level where dt = '${hiveconf:rpt_date_yesterday}' 
and app = app_id)
prev ON (l.user_id = prev.app_user_id and l.app_id = prev.app_id)
left outer join 
(SELECT * FROM
(SELECT app, user_id, ts_pretty, ROW_NUMBER() OVER (PARTITION BY app, user_id ORDER BY ts_pretty ASC) AS rnum 
FROM raw_events_daily 
WHERE dt ='${hiveconf:rpt_date}' and event='new_user' and app=app_id 
) Y WHERE rnum = 1)
nu ON (l.app = nu.app AND l.user_id = nu.user_id)
WHERE l.app = l.app_id;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_levelup DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_levelup PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') 
select
id, user_key, app_id, app_version, app_user_id, session_id, previous_level, previous_levelup_date, previous_levelup_ts, current_level, levelup_date, levelup_ts, browser, browser_version, 
os, os_version,  device, country_code, ip, language, locale, ab_experiment, ab_variant 
from fact_levelup where dt ='${hiveconf:rpt_date}';

exit;

-------------------------------
-- Last War 1.2
-- Hourly DDL with map
-------------------------------
USE lastwar_1_2;

DROP TABLE IF EXISTS raw_achievement;
DROP TABLE IF EXISTS raw_facebook_register;
DROP TABLE IF EXISTS raw_gift_received;
DROP TABLE IF EXISTS raw_invalid_events;
DROP TABLE IF EXISTS raw_item_actioned;
DROP TABLE IF EXISTS raw_level_up;
DROP TABLE IF EXISTS raw_load_step;
DROP TABLE IF EXISTS raw_mission;
DROP TABLE IF EXISTS raw_new_user;
DROP TABLE IF EXISTS raw_payment;
DROP TABLE IF EXISTS raw_session_end;
DROP TABLE IF EXISTS raw_session_start;
DROP TABLE IF EXISTS raw_timer;
DROP TABLE IF EXISTS raw_transaction;
DROP TABLE IF EXISTS raw_tutorial;

-- Invalid Events
CREATE EXTERNAL TABLE IF NOT EXISTS raw_invalid_events (
    event string,
    error string,
    errorDetails array<string>,
    invalidJson string
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/invalid'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event session_start hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      facebook_id: string,
--      gender: string,
--      birthday: string,
--      install_ts: int,
--      install_source: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_start (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/session_start'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event session_end hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      facebook_id: string,
--      gender: string,
--      birthday: string,
--      install_ts: int,
--      install_source: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_end (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/session_end'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event new_user hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      facebook_id: string,
--      gender: string,
--      birthday: string,
--      install_ts: int,
--      install_source: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_new_user (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/new_user'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event payment hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      amount: int,
--      currency: string,
--      iap_product_id: string,
--      iap_product_name: string,
--      iap_product_type: string,
--      transaction_id: string,
--      payment_processor: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_payment (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/payment'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event level_up hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      from_level: int
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_level_up (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/level_up'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event achievement hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      achievement_id: string,
--      achievement_name: string,
--      achievement_type: string,
--      achievement_difficulty: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_achievement (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/achievement'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event facebook_register hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      facebook_id: string,
--      gender: string,
--      birthday: string,
--      install_ts: int,
--      install_source: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_facebook_register (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/facebook_register'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event gift_received hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      source_user_id: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_gift_received (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/gift_received'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event item_actioned hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      action_type: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_item_actioned (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_source: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_target: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/item_actioned'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event load_step hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      load_step: int,
--      load_step_desc: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_load_step (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/load_step'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event mission hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      mission_id: string,
--      mission_name: string,
--      mission_type: string,
--      mission_difficulty: string,
--      mission_status: int,
--      mission_start_ts: int,
--      mission_total_sec: int
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_mission (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        mission_statistics: array<
          struct<
            name: string,
            value: string
          >
        >,
        mission_parameters: array<
          struct<
            name: string,
            value: string
          >
        >,
        mission_objectives: array<
          struct<
            objective_id: string,
            objective_name: string,
            objective_type: string,
            objective_amount: int,
            objective_amount_remaining: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/mission'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event timer hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      timer_type: string,
--      timer_total_sec: int,
--      timer_start_ts: int,
--      timer_status: int
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_timer (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_source: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_target: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/timer'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event transaction hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      transaction_type: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_transaction (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/transaction'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event tutorial hourly
--  properties struct<
--      app_version: string,
--      os: string,
--      os_version: string,
--      browser: string,
--      browser_version: string,
--      idfa: string,
--      idfv: string,
--      gaid: string,
--      mac_address: string,
--      android_id: string,
--      device: string,
--      ip: string,
--      lang: string,
--      level: int,
--      tutorial_step: int,
--      tutorial_step_desc: string
--  >
CREATE EXTERNAL TABLE IF NOT EXISTS raw_tutorial (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/lastwar_1_2/events/tutorial'
TBLPROPERTIES (
  'serialization.null.format'=''
);

exit;

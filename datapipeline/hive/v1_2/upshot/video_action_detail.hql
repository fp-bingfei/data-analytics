USE upshot_1_2; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.stats.autogather=false;



insert overwrite table agg_video_action_detail partition(app,dt)
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_text' as action
      ,'font' as action_detail
      ,get_json_object(properties['video_text'],'$.font')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_text'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty) 
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_text' 
        ,'font' 
        ,get_json_object(properties['video_text'],'$.font')
        ,app
        ,dt
        
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_text' as action
      ,'background' as action_detail
      ,get_json_object(properties['video_text'],'$.background')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_text'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty) 
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_text' 
        ,'background' 
        ,get_json_object(properties['video_text'],'$.background')  
        ,app 
        ,dt  
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_text' as action
      ,'size' as action_detail
      ,get_json_object(properties['video_text'],'$.size')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_text'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty)
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_text' 
        ,'size' 
        ,get_json_object(properties['video_text'],'$.size')
        ,app
        ,dt
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_filter' as action
      ,'filter' as action_detail
      ,get_json_object(properties['video_filter'],'$.name')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_filter'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty) 
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_filter' 
        ,'filter' 
        ,get_json_object(properties['video_filter'],'$.name')
        ,app
        ,dt
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_music' as action
      ,'duration' as action_detail
      ,get_json_object(properties['video_music'],'$.duration')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_music'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty)  
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_music' 
        ,'duration' 
        ,get_json_object(properties['video_music'],'$.duration')
        ,app
        ,dt
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_music' as action
      ,'name' as action_detail
      ,get_json_object(properties['video_music'],'$.name')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_music'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty)  
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_music' 
        ,'name' 
        ,get_json_object(properties['video_music'],'$.name')
        ,app
        ,dt
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_share' as action
      ,'platform' as action_detail
      ,get_json_object(properties['video_share'],'$.platform')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_share'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id 
        ,to_date(ts_pretty)  
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_share' 
        ,'platform' 
        ,get_json_object(properties['video_share'],'$.platform')
        ,app
        ,dt
union all
select app_id 
      ,to_date(ts_pretty) as date 
      ,properties['country_code'] 
      ,properties['device_resolution'] 
      ,properties['network'] 
      ,properties['location'] 
      ,properties['carrier'] 
      ,properties['device_storage'] 
      ,properties['os'] 
      ,properties['device'] 
      ,'video_share' as action
      ,'duration' as action_detail
      ,get_json_object(properties['video_share'],'$.duration')
      ,count(1) as attribute_cnt
      ,app
      ,dt
from raw_events_daily 
where event = 'video_action' and properties['action'] = 'video_share'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by  app_id 
        ,to_date(ts_pretty)  
        ,properties['country_code'] 
        ,properties['device_resolution'] 
        ,properties['network'] 
        ,properties['location'] 
        ,properties['carrier'] 
        ,properties['device_storage'] 
        ,properties['os'] 
        ,properties['device'] 
        ,'video_share' 
        ,'duration' 
        ,get_json_object(properties['video_share'],'$.duration')
        ,app
        ,dt
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_video_action_detail DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_agg_video_action_detail partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')  
select   app_id 
        ,date 
        ,country_code 
        ,device_resolution 
        ,network 
        ,location 
        ,carrier 
        ,device_storage 
        ,os 
        ,device 
        ,action 
        ,action_detail 
        ,attribute 
        ,attribute_cnt 
from agg_video_action_detail 
where dt > '${hiveconf:rpt_date_start}' 
and  dt<='${hiveconf:rpt_date}'
;

exit;

--------------------------------------
-- Query to generate agg video action
--------------------------------------
use upshot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;


INSERT OVERWRITE TABLE agg_video_action PARTITION (app,dt)
select  app_id
       ,to_date(ts_pretty)
       ,properties['country_code']
       ,properties['os']
       ,properties['action']
       ,count(distinct user_id)
       ,count(1)
       ,app
       ,dt
from   raw_events_daily
where event = 'video_action'
and   dt > '${hiveconf:rpt_date_start}' 
and   dt<='${hiveconf:rpt_date}' 
group by app_id
       ,to_date(ts_pretty)
       ,properties['country_code']
       ,properties['os']
       ,properties['action']
       ,app
       ,dt
;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_video_action DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_agg_video_action partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')  
select   app_id 
        ,date 
        ,country_code 
        ,os
        ,action 
        ,user_cnt 
        ,action_cnt
from agg_video_action 
where dt > '${hiveconf:rpt_date_start}' 
and  dt<='${hiveconf:rpt_date}'
;

exit;


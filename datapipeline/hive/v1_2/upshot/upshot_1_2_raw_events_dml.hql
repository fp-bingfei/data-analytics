
USE upshot_1_2; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.stats.autogather=false;

-- Update Hive metastore for partitions
-- invalid
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


-- new_user
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='new_user',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


-- session_end
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_end',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


-- session_start
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='session_start',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


-- video_action
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='video_action',app='upshot.global.prod',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


-- Insert events to daily table
INSERT OVERWRITE TABLE raw_events_daily PARTITION (event, app, dt)
SELECT
  data_version, 
  app_id, 
  ts, 
  ts_pretty, 
  user_id, 
  session_id, 
  properties, 
  collections, 
  event, 
  app, 
  to_date(concat(cast(year as string),'-',cast(month as string),'-',cast(day as string))) dt
FROM
  raw_events
WHERE
  to_date(concat(cast(year as string),'-',cast(month as string),'-',cast(day as string))) > '${hiveconf:rpt_date_start}' 
  AND
  to_date(concat(cast(year as string),'-',cast(month as string),'-',cast(day as string))) <= '${hiveconf:rpt_date}' 
;


exit;

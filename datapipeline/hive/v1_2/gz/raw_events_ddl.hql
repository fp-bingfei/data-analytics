---------------------------------------------------------------------------------------------------
-- CREATE DATABASE gz_1_2 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/gz_1_2.db';
---------------------------------------------------------------------------------------------------
use gz_1_2;

create external table raw_events
( app_id string
, ts int
, action string
, action_detail string
, coins_bal string
, coins_in string
, coins_out string
, device string
, event string
, gifted_by string
, gifted_to string
, install_src string
, install_ts string
, is_gift int
, level int
, location string
, os string
, os_version string
, snsid string
, uid string
, vipLevel int
)
PARTITIONED BY (
app STRING,
dt STRING,
hour int
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.app_id"="@key"
)
LOCATION 's3://com.funplus.galaxystorm/events/'
TBLPROPERTIES('serialization.null.format'='');
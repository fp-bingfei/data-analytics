use mt2_1_2;

ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=00) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/00'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=01) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/01'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=02) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/02'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=03) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/03'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=04) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/04'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=05) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/05'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=06) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/06'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=07) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/07'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=08) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/08'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=09) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/09'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=10) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/10'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=11) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/11'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=12) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/12'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=13) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/13'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=14) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/14'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=15) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/15'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=16) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/16'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=17) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/17'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=18) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/18'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=19) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/19'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=20) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/20'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=21) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/21'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=22) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/22'; 
ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}', hour=23) 
LOCATION 's3://com.funplus.galaxystorm/events/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}/23'; 


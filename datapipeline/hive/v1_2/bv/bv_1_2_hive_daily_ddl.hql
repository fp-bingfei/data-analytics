-------------------------------
-- Barn Voyage 1.2
-- Daily DDL with struct
-------------------------------
USE bv_1_2;

DROP TABLE IF EXISTS raw_achievement_daily;
DROP TABLE IF EXISTS raw_facebook_register_daily;
DROP TABLE IF EXISTS raw_gift_received_daily;
DROP TABLE IF EXISTS raw_item_actioned_daily;
DROP TABLE IF EXISTS raw_level_up_daily;
DROP TABLE IF EXISTS raw_load_step_daily;
DROP TABLE IF EXISTS raw_mission_daily;
DROP TABLE IF EXISTS raw_new_user_daily;
DROP TABLE IF EXISTS raw_payment_daily;
DROP TABLE IF EXISTS raw_session_end_daily;
DROP TABLE IF EXISTS raw_session_start_daily;
DROP TABLE IF EXISTS raw_timer_daily;
DROP TABLE IF EXISTS raw_transaction_daily;
DROP TABLE IF EXISTS raw_tutorial_daily;
DROP TABLE IF EXISTS raw_message_sent_daily;

-- Event session_start daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_start_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    install_ts int,
    install_ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/session_start_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event session_end daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_end_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    install_ts int,
    install_ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/session_end_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event new_user daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_new_user_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    install_ts int,
    install_ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/new_user_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event payment daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_payment_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/payment_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event level_up daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_level_up_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/level_up_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event achievement daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_achievement_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/achievement_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event facebook_register daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_facebook_register_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    install_ts int,
    install_ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/facebook_register_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event gift_received daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_gift_received_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/gift_received_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event item_actioned daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_item_actioned_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_source: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_target: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/item_actioned_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event load_step daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_load_step_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/load_step_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event mission daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_mission_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        mission_statistics: array<
          struct<
            name: string,
            value: string
          >
        >,
        mission_parameters: array<
          struct<
            name: string,
            value: string
          >
        >,
        mission_objectives: array<
          struct<
            objective_id: string,
            objective_name: string,
            objective_type: string,
            objective_amount: int,
            objective_amount_remaining: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/mission_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event timer daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_timer_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_source: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_target: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/timer_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event transaction daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_transaction_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >,
        resources_received: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        resources_spent: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        items_received: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >,
        items_spent: array<
          struct<
            item_id: string,
            item_name: string,
            item_class: string,
            item_type: string,
            item_amount: int
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/transaction_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event tutorial daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_tutorial_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/tutorial_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Event message_sent daily
CREATE EXTERNAL TABLE IF NOT EXISTS raw_message_sent_daily (
    bi_version string,
    app_id string,
    ts int,
    ts_pretty timestamp,
    event string,
    user_id string,
    snsid string,
    session_id string,
    properties map<
        string, string
    >,
    collections struct<
        player_resources: array<
          struct<
            resource_id: string,
            resource_name: string,
            resource_type: string,
            resource_amount: int
          >
        >,
        ab_tests: array<
          struct<
            ab_experiment: string,
            ab_variant: string
          >
        >
    >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/bv_1_2/events_daily/message_sent_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

exit;

use bv_1_2;

select t1.hour
      ,t1.session_start_cnt
      ,t2.payment_cnt
      ,t3.new_user_cnt
from      
   (select hour
         ,count(*) as session_start_cnt
   from raw_session_start
   where app='${hiveconf:rpt_app}' and year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month}  and day=${hiveconf:rpt_day}
   group by hour
   )t1
left join
   (
    select hour
          ,count(*) as payment_cnt
    from raw_payment
    where app='${hiveconf:rpt_app}' and year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month}  and day=${hiveconf:rpt_day}
    group by hour
   )t2
on t1.hour=t2.hour
left join
   (
    select hour
          ,count(*) as new_user_cnt
    from raw_new_user
    where app='${hiveconf:rpt_app}' and year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month}  and day=${hiveconf:rpt_day}
    group by hour
   )t3
on t1.hour=t3.hour   
union all
select '24' as hour
      ,t1.session_start_cnt
      ,t2.payment_cnt
      ,t3.new_user_cnt
from      
   (select '24' as hour
         ,count(*) as session_start_cnt
   from raw_session_start_daily
   where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
   )t1
left join
   (
    select '24' as hour
          ,count(*) as payment_cnt
    from raw_payment_daily
    where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
   )t2
on t1.hour=t2.hour
left join
   (
    select '24' as hour
          ,count(*) as new_user_cnt
    from raw_new_user_daily
    where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
   )t3
on t1.hour=t3.hour  
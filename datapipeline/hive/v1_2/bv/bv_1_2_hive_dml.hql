-------------------------------
-- Barn Voyage 1.2
-- Daily DML with struct
-------------------------------
USE bv_1_2;

-- Add to_json function
--CREATE TEMPORARY FUNCTION to_json AS 'brickhouse.udf.json.ToJsonUDF' USING JAR 's3://com.funplusgame.bidata/dev/serde/brickhouse-0.7.1-SNAPSHOT.jar';

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
-- SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

-- Update Hive metastore for partitions
MSCK REPAIR TABLE raw_invalid_events;
MSCK REPAIR TABLE raw_session_start;
MSCK REPAIR TABLE raw_session_end;
MSCK REPAIR TABLE raw_new_user;
MSCK REPAIR TABLE raw_payment;
MSCK REPAIR TABLE raw_level_up;
MSCK REPAIR TABLE raw_achievement;
MSCK REPAIR TABLE raw_facebook_register;
MSCK REPAIR TABLE raw_gift_received;
MSCK REPAIR TABLE raw_item_actioned;
MSCK REPAIR TABLE raw_load_step;
MSCK REPAIR TABLE raw_mission;
MSCK REPAIR TABLE raw_timer;
MSCK REPAIR TABLE raw_transaction;
MSCK REPAIR TABLE raw_tutorial;
MSCK REPAIR TABLE raw_message_sent;

-- Insert data to daily table
INSERT OVERWRITE TABLE raw_session_start_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  properties['install_ts'], 
  properties['install_ts_pretty'], 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_session_start
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_session_end_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  properties['install_ts'], 
  properties['install_ts_pretty'], 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_session_end
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_new_user_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  properties['install_ts'], 
  properties['install_ts_pretty'], 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_new_user
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_payment_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_payment
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_level_up_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_level_up
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_achievement_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_achievement
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_facebook_register_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  properties['install_ts'], 
  properties['install_ts_pretty'], 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_facebook_register
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_gift_received_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_gift_received
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_item_actioned_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_item_actioned
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_load_step_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_load_step
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_mission_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_mission
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_timer_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_timer
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_transaction_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_transaction
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_tutorial_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_tutorial
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

INSERT OVERWRITE TABLE raw_message_sent_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  snsid, 
  session_id, 
  properties, 
  collections
FROM raw_message_sent
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day};

exit;

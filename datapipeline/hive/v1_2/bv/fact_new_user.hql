use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

-- SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE fact_new_user PARTITION (app,dt)
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
install_ts_pretty as install_ts,
to_date(install_ts_pretty) as install_date,
session_id,
properties['facebook_id'] facebook_id,
properties['install_source'] install_source,
properties['os'] os,
properties['os_version'] os_version,
properties['browser'] browser,
properties['browser_version'] browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['gender'] gender,
properties['birthday'] birthday,
properties['email'] email,
properties['ip'] ip,
properties['lang'] language,
properties['locale']  locale,
ab.ab_experiment,
ab.ab_variant,
properties['idfa'] as idfa,
properties['idfv'] as idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
app,
dt
FROM raw_new_user_daily
lateral view outer explode(collections.ab_tests) coll1 AS ab
WHERE  dt = '${hiveconf:rpt_date}';



-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_fact_new_user partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') select 
id,app_id,app_version,user_key,app_user_id,install_ts,install_date,session_id,facebook_id,install_source,os,os_version,browser,browser_version,device,country_code,level,gender,birthday,email,ip,language,locale,ab_experiment,ab_variant, idfa, idfv, gaid, mac_address, android_id
from fact_new_user where dt='${hiveconf:rpt_date}';

exit;





use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

-- SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;





INSERT OVERWRITE TABLE fact_session PARTITION (app,dt)
SELECT
MD5(concat(s.app_id, s.event, s.user_id, s.session_id, s.ts)) as id,
s.app_id as app_id,
s.properties['app_version'] app_version,
MD5(concat(s.app_id,s.user_id)) as user_key,
s.user_id as app_user_id,
s.snsid,
to_date(s.ts_pretty) as date_start,
to_date(r.ts_pretty) as date_end,
s.ts_pretty as ts_start,
r.ts_pretty as ts_end,
COALESCE(s.install_ts_pretty,r.install_ts_pretty) as install_ts,
COALESCE(to_date(s.install_ts_pretty),to_date(r.install_ts_pretty)) as install_date,
s.session_id,
COALESCE(s.properties['facebook_id'],r.properties['facebook_id']) facebook_id,
COALESCE(s.properties['install_source'],r.properties['install_source']) install_source,
COALESCE(s.properties['os'],r.properties['os']) os,
COALESCE(s.properties['os_version'],r.properties['os_version']) os_version,
null as browser,
null as browser_version,
COALESCE(s.properties['device'],r.properties['device']) device,
COALESCE(s.properties['country_code'],r.properties['country_code']) country_code,
s.properties['level'] as level_start,
r.properties['level'] as level_end,
s.properties['gender'] gender,
s.properties['birthday'] birthday,
s.properties['email'] email,
COALESCE(s.properties['ip'],r.properties['ip']) ip,
COALESCE(s.properties['lang'],r.properties['lang']) language,
null as locale,
null as rc_wallet_start,
null as rc_wallet_end,
null as coin_wallet_start,
null as coin_wallet_end,
COALESCE(s.ab_experiment,r.ab_experiment)  as ab_experiment,
COALESCE(s.ab_variant,r.ab_variant)  as ab_variant,
(r.ts-s.ts) session_length_sec,
COALESCE(s.properties['idfa'],r.properties['idfa']) as idfa,
COALESCE(s.properties['idfv'],r.properties['idfv']) as idfv,
COALESCE(s.properties['gaid'],r.properties['gaid']) gaid,
COALESCE(s.properties['mac_address'],r.properties['mac_address']) mac_address,
COALESCE(s.properties['android_id'],r.properties['android_id']) android_id,
s.app as app,
s.dt
FROM
(SELECT bi_version, app_id, ts,event, ts_pretty, install_ts, install_ts_pretty, user_id, snsid, session_id, properties, app, dt,ab.ab_experiment,
ab.ab_variant
FROM raw_session_start_daily
lateral view outer explode(collections.ab_tests) coll1 AS ab
WHERE  dt = '${hiveconf:rpt_date}' and to_date(ts_pretty)='${hiveconf:rpt_date}'

) s
LEFT OUTER JOIN
(SELECT bi_version, app_id, ts, ts_pretty,event, install_ts, install_ts_pretty, user_id, snsid, session_id, properties, app, dt, ab.ab_experiment,ab.ab_variant
FROM raw_session_end_daily
lateral view outer explode(collections.ab_tests) coll1 AS ab
WHERE dt = '${hiveconf:rpt_date}' and to_date(ts_pretty)='${hiveconf:rpt_date}'
) r
ON s.user_id=r.user_id and s.app_id=r.app_id and s.app=r.app and s.session_id=r.session_id and s.snsid=r.snsid;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_session DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_session partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') select 
id,app_id,app_version,user_key,app_user_id,snsid,date_start,date_end,ts_start,ts_end,install_ts,install_date,session_id,facebook_id,install_source,os,os_version,browser,browser_version,device,country_code,level_start,level_end,gender,birthday,email,ip,language,locale,rc_wallet_start,rc_wallet_end,coin_wallet_start,coin_wallet_end,ab_experiment,ab_variant,session_length_sec, idfa, idfv, gaid, mac_address, android_id
from fact_session where dt='${hiveconf:rpt_date}';




exit;



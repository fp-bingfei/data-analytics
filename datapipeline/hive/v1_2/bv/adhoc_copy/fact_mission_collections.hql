use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;



-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_mission_objective DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_objective partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, objective_id, objective_name, objective_type, objective_amount, objective_amount_remaining 
from fact_mission_objective where app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';


ALTER TABLE copy_fact_mission_statistic DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_statistic partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, statistic_name, statistic_value from fact_mission_statistic 
where app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';


ALTER TABLE copy_fact_mission_parameter DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_parameter partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, parameter_name, parameter_value from fact_mission_parameter 
where app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';




exit;

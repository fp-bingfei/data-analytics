use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_dim_user_device DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_dim_user_device partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
app_id, app_user_id, user_key, idfa, idfv, gaid, mac_address, android_id, first_seen_ts 
from dim_user_device where app='bv.global.prod' and dt ='${hiveconf:rpt_date}' AND app_user_id IS NOT NULL and user_key IS NOT NULL;

exit;
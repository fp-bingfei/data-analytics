use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


ALTER TABLE copy_fact_items_spent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_items_spent partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id
, app_id
, user_key
, level
, event
, date
, ts
, action_type
, item_spent_id
, item_spent_name
, item_spent_type
, item_spent_class
, item_spent_amount
from fact_items_spent where dt ='${hiveconf:rpt_date}';

exit;
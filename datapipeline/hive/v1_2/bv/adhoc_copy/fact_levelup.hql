----------------------------------
-- Query to generate fact_levelup
----------------------------------
use bv_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_levelup DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_levelup PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') 
select
id, user_key, app_id, app_version, app_user_id, session_id, previous_level, previous_levelup_date, previous_levelup_ts, current_level, levelup_date, levelup_ts, browser, browser_version, 
os, os_version,  device, country_code, ip, language, locale, ab_experiment, ab_variant 
from fact_levelup where app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';

exit;

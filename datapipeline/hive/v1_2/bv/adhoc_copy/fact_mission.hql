use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_mission DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id,mission_id,app_id,app_version,user_key,app_user_id,date,ts,session_id,os,os_version,device,browser,browser_version,country_code,ip,language,
mission_start_ts,mission_status,level
from fact_mission where  app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';

exit;





exit;
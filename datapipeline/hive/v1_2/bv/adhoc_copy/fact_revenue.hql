
----------------------------------
-- Query to generate fact_revenue
----------------------------------
use bv_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 

ALTER TABLE currency ADD IF NOT EXISTS PARTITION (date='${hiveconf:rpt_date}') LOCATION "s3://com.funplusgame.bidata/currency/${hiveconf:rpt_date_nohyphen}/";


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 

ALTER TABLE copy_fact_revenue DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_revenue partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select id, app_id, app_version, user_key, app_user_id, date, ts,install_ts, install_date, session_id, level
, os, os_version, device, browser, browser_version, country_code, install_source, ip, language, locale
, ab_experiment, ab_variant, coin_wallet, rc_wallet, payment_processor, product_id, product_name, product_type, coins_in, rc_in, currency, revenue_currency
, revenue_usd, transaction_id, idfa, idfv, gaid, mac_address, android_id
from fact_revenue where  app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';

exit;



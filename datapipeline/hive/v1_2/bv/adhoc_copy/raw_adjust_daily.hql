use bv_1_2;

SET rpt_app=bv.global.prod;

ALTER TABLE raw_adjust_daily ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}') 
LOCATION 's3://com.funplusgame.bidata/adjust/${hiveconf:rpt_app}/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}'; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


set rpt_date_days_back=${hiveconf:rpt_date_d7};

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_raw_adjust_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_raw_adjust_daily partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
  adid,
  userid,
  game,
  tracker,
  tracker_name,
  app_id,
  ip_address,
  idfa,
  android_id,
  mac_sha1,
  idfa_md5,
  country,
  from_unixtime(cast(timestamp as BIGINT)),
  mac_md5,
  gps_adid,
  device_name,
  os_name,
  os_version
FROM raw_adjust_daily
WHERE dt > '${hiveconf:rpt_date_days_back}' and dt<='${hiveconf:rpt_date}'
AND to_date(from_unixtime(cast(timestamp as BIGINT))) > '${hiveconf:rpt_date_days_back}' 
AND to_date(from_unixtime(cast(timestamp as BIGINT))) <='${hiveconf:rpt_date}';


EXIT;


use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_session DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_session partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') select 
id,app_id,app_version,user_key,app_user_id,snsid,date_start,date_end,ts_start,ts_end,install_ts,install_date,session_id,facebook_id,install_source,os,os_version,browser,browser_version,device,country_code,level_start,level_end,gender,birthday,email,ip,language,locale,rc_wallet_start,rc_wallet_end,coin_wallet_start,coin_wallet_end,ab_experiment,ab_variant,session_length_sec, idfa, idfv, gaid, mac_address, android_id
from fact_session where app='bv.global.prod' and dt >= '2015-03-14' and dt <='${hiveconf:rpt_date}';




exit;



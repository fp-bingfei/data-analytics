use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

-- SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;




INSERT OVERWRITE TABLE fact_mission_objective PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
objective_id,
objective_name,
objective_type,
objective_amount,
objective_amount_remaining,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.objective_id,
pr.objective_name,pr.objective_type, pr.objective_amount, pr.objective_amount_remaining
FROM raw_mission_daily
lateral view explode(collections.mission_objectives) coll1 AS pr
WHERE  dt ='${hiveconf:rpt_date}' 
)t
;


INSERT OVERWRITE TABLE fact_mission_statistic PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
name as statistic_name,
value as statistic_value,
app,
to_date(ts_pretty) from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.name,pr.value
FROM raw_mission_daily
lateral view explode(collections.mission_statistics) coll1 AS pr
WHERE dt ='${hiveconf:rpt_date}'
)t
;

INSERT OVERWRITE TABLE fact_mission_parameter PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
name as parameter_name,
value as parameter_value,
app,
to_date(ts_pretty) from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.name,pr.value
FROM raw_mission_daily
lateral view explode(collections.mission_parameters) coll1 AS pr
WHERE  dt ='${hiveconf:rpt_date}'
)t
;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_mission_objective DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_objective partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, objective_id, objective_name, objective_type, objective_amount, objective_amount_remaining from fact_mission_objective where dt ='${hiveconf:rpt_date}'
;


ALTER TABLE copy_fact_mission_statistic DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_statistic partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, statistic_name, statistic_value from fact_mission_statistic where dt ='${hiveconf:rpt_date}'
;


ALTER TABLE copy_fact_mission_parameter DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_parameter partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, parameter_name, parameter_value from fact_mission_parameter where dt ='${hiveconf:rpt_date}'
;




exit;

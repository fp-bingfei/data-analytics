use bv_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

-- SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


INSERT OVERWRITE TABLE fact_items_spent PARTITION (app,dt)
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, transaction_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty,properties['transaction_type'] as transaction_type, properties['level'] as level
, pr.item_id, pr.item_name, pr.item_type,pr.item_class, pr.item_amount
FROM
raw_transaction_daily lateral view explode(collections.items_spent) coll AS pr
WHERE dt ='${hiveconf:rpt_date}'
) t
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, timer_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['timer_type'] as timer_type, properties['level'] as level
, pr.item_id, pr.item_name, pr.item_type,pr.item_class, pr.item_amount
FROM
raw_timer_daily lateral view explode(collections.items_spent) coll AS pr
WHERE dt ='${hiveconf:rpt_date}'
) m
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, mission_type as action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['mission_type'] as mission_type, properties['level'] as level
, pr.item_id, pr.item_name, pr.item_type,pr.item_class, pr.item_amount
FROM
raw_mission_daily lateral view explode(collections.items_spent) coll AS pr
WHERE dt ='${hiveconf:rpt_date}'
) n
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, action_type
, item_id
, item_name
, item_type
, item_class
, item_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['action_type'] as action_type, properties['level'] as level
, pr.item_id, pr.item_name, pr.item_type,pr.item_class, pr.item_amount
FROM
raw_item_actioned_daily lateral view explode(collections.items_spent) coll AS pr
WHERE dt ='${hiveconf:rpt_date}'
) i
;



-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


ALTER TABLE copy_fact_items_spent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_items_spent partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id
, app_id
, user_key
, level
, event
, date
, ts
, action_type
, item_spent_id
, item_spent_name
, item_spent_type
, item_spent_class
, item_spent_amount
from fact_items_spent where dt ='${hiveconf:rpt_date}';

exit;

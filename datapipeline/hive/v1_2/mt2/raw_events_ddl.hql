---------------------------------------------------------------------------------------------------
-- CREATE DATABASE mt2_1_2 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/mt2_1_2.db';
---------------------------------------------------------------------------------------------------
use mt2_1_2;

create external table raw_events
( bi_version string
, app_id string
, ts int
, event string
, user_id string
, session_id string
, properties map<string, string>
)
PARTITIONED BY (
app STRING,
dt STRING,
hour int
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES ('errors.ignore'='true')
LOCATION 's3://com.funplus.mt2/'
TBLPROPERTIES('serialization.null.format'='');

---- copy_raw_sendgrid_daily

CREATE  TABLE `copy_raw_sendgrid_daily`(
  `snsid` string,
  `email` string,
  `category` string,
  `uid` string,
  `app_id` string,
  `time` string,
  `ip` string,
  `event` string,
  `day` string,
  `campaign` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.funplus.datawarehouse/ffs_1_2/copy/raw_sendgrid_daily'
TBLPROPERTIES (
  'serialization.null.format'='');

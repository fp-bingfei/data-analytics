--- raw_sendgrid_daily

CREATE EXTERNAL TABLE `raw_sendgrid_daily`(
  `snsid` string COMMENT 'from deserializer',
  `email` string COMMENT 'from deserializer',
  `category` string COMMENT 'from deserializer',
  `uid` string COMMENT 'from deserializer',
  `app` string COMMENT 'from deserializer',
  `time` string COMMENT 'from deserializer',
  `ip` string COMMENT 'from deserializer',
  `event` string COMMENT 'from deserializer',
  `day` string COMMENT 'from deserializer',
  `campaign` string COMMENT 'from deserializer')
PARTITIONED BY (
  `app_id` string,
  `dt` string)
ROW FORMAT SERDE
  'org.openx.data.jsonserde.JsonSerDe'
STORED AS INPUTFORMAT
  'org.apache.hadoop.mapred.TextInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://com.funplus.bithirdparty/sendgrid'
TBLPROPERTIES (
  'serialization.null.format'='')

----------------------------------------------------------------------------------------------
-- CREATE DATABASE rs_1_2 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/rs_1_2.db';
----------------------------------------------------------------------------------------------

use rs_1_2;


-- copy_raw_sendgrid_daily
  CREATE TABLE copy_raw_sendgrid_daily (
  snsid string,
  email string,
  category string,
  uid string,
  app_id string,
  time string,
  ip string,
  event string,
  day string,
  campaign string)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/rs_1_2/copy/raw_sendgrid_daily'
TBLPROPERTIES('serialization.null.format'='');
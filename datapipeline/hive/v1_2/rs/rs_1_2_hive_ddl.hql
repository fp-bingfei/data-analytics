----------------------------------------------------------------------------------------------
-- CREATE DATABASE rs_1_2 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/rs_1_2.db';
----------------------------------------------------------------------------------------------

use rs_1_2;


-- raw_sendgrid_daily
CREATE EXTERNAL TABLE raw_sendgrid_daily (
  snsid string,
  email string,
  category string,
  uid string,
  app string,
  time string,
  ip string,
  event string,
  day string,
  campaign string)
PARTITIONED BY (
  app_id string,
  dt string)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
LOCATION 's3://com.funplus.bithirdparty/sendgrid'
TBLPROPERTIES('serialization.null.format'='');

  
---------------------------------------------------------------------------------------------------
-- CREATE DATABASE dwtlw_1_2 LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/dwtlw_1_2.db';
---------------------------------------------------------------------------------------------------
use dwtlw_1_2;

create external table raw_events
( bi_version string
, app_id string
, ts int
, event string
, user_id string
, session_id string
, properties map<string, string>
)
PARTITIONED BY (
app STRING,
dt STRING,
hour int
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
LOCATION 's3://com.funplus.eshttp/'
TBLPROPERTIES('serialization.null.format'='');

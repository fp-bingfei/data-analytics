USE fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_challenge_raw DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_challenge_raw PARTITION (app='${hiveconf:rpt_all_app}', dt='${hiveconf:rpt_date}')
SELECT
    MD5(concat(app_id, event, user_id, session_id, ts)) as id,
    properties['mission_id'] as mission_id,
    properties['mission_name'] as mission_name,
    properties['mission_type'] as mission_type,
    app_id  ,
    properties['app_version'] as app_version,
    MD5(concat(app_id,user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    from_unixtime(cast (properties['mission_start_ts'] as bigint)) as mission_start_ts,
    COALESCE(properties['mission_status'], null) as mission_status,
    COALESCE(properties['mission_total_sec'], null) as mission_total_sec,
    properties['level'] as level,
    COALESCE(star, null) as star,
    ab_tests as ab_test,
    items_spent as items_spent
FROM
(
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        to_json(collections.ab_tests) as ab_tests,
        to_json(collections.items_spent) as items_spent,
        pr.value as star
    FROM 
        raw_mission_daily lateral view explode(collections.mission_statistics) coll1 AS pr 
    WHERE
        properties['mission_type'] = 'golden_challenge' and 
        pr.name = 'star' and 
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
    
    UNION ALL 
    
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        to_json(collections.ab_tests) as ab_tests,
        to_json(collections.items_spent) as items_spent,
        null as star
    FROM 
        raw_mission_daily 
    WHERE
        properties['mission_type'] = 'golden_challenge' and 
        collections.mission_statistics is null and 
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
    
    UNION ALL 
    
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        to_json(collections.ab_tests) as ab_tests,
        to_json(collections.items_spent) as items_spent,
        null as star
    FROM 
        raw_mission_daily 
    WHERE
        properties['mission_type'] = '24_challenge' and 
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
) t
;


exit;

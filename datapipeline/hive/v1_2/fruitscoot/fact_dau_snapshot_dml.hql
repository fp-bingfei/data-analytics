use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;

SET hive.exec.max.dynamic.partitions=5000;
SET hive.exec.max.dynamic.partitions.pernode=5000;


INSERT OVERWRITE TABLE fact_dau_snapshot PARTITION (app,dt)
select id,user_key,date_start,app_id,app_version,level_start,level_end,os,os_version,device,browser,browser_version,country_code,country,language,ab_experiment,
ab_variant,is_new_user,resource_amount,received_resource_amount,is_payer,is_converted_today,revenue_usd,purchase_cnt,session_cnt,playtime_sec,is_facebook_connected, is_facebook_connected_today, app,dt
from
(
SELECT
MD5(concat(s.user_key,s.date_start)) id,
s.user_key ,
s.date_start,
s.app_id,
s.app_version,
COALESCE(l.level_start, s.level_start) level_start,
COALESCE(l.level_end,s.level_end,s.level_start) level_end,
s.os,
s.os_version,
s.device,
s.browser,
s.browser_version,
s.country_code,
s.country,
s.language,
s.ab_experiment,
s.ab_variant,
if(s.date_start=coalesce(d.install_date,s.install_date), 1 ,0) is_new_user ,
0 as resource_amount ,
0 as received_resource_amount,
coalesce(d.is_payer,if (s.date_start >= to_date(p.conversion_ts),1,0)) is_payer ,
if (s.date_start = to_date(p.conversion_ts),1,0) is_converted_today,
p.revenue_usd ,
p.purchase_cnt ,
s.session_cnt ,
s.session_length_sec as playtime_sec,
if (s.date_start >= to_date(b.facebook_connect_ts),1,0) is_facebook_connected ,
if (s.date_start = to_date(b.facebook_connect_ts),1,0) is_facebook_connected_today,
s.app,
s.dt
from (select * from tmp_user_daily_login where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and app_user_id is not null)s
LEFT OUTER JOIN (select * from dim_user where dt = '${hiveconf:rpt_date_yesterday}') d on s.app = d.app and s.user_key = d.user_key
LEFT OUTER JOIN (select * from tmp_user_payment where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}') p
on s.user_key=p.user_key and s.dt=p.dt and s.app=p.app
LEFT OUTER JOIN (select * from tmp_user_level where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}')l
on s.user_key=l.user_key and s.dt=l.dt and s.app=l.app
left outer join (select distinct app,app_id,user_key,app_user_id,facebook_connect_ts from tmp_user_facebook_connect where dt='${hiveconf:rpt_date}') b
on s.user_key=b.user_key and s.app=b.app and s.app_id=b.app_id
UNION ALL
SELECT MD5(concat(u.user_key,u.install_date)) id,
u.user_key ,
install_date date_start,
u.app_id,
app_version,
null as level_start,
null as level_end,
os,
os_version,
device,
browser,
browser_version,
country_code,
country,
language,
ab_experiment,
ab_variant,
1 as is_new_user ,
0 as resource_amount ,
0 as received_resource_amount,
u.is_payer,
if (u.install_date = to_date(u.conversion_ts),1,0) is_converted_today,
0 as revenue_usd ,
0 as purchase_cnt ,
0 as session_cnt ,
0 as playtime_sec,
if (u.install_date >= to_date(b.facebook_connect_ts),1,0) is_facebook_connected ,
if (u.install_date = to_date(b.facebook_connect_ts),1,0) is_facebook_connected_today,
u.app,
u.install_date as dt from (select * from dim_user where dt ='${hiveconf:rpt_date_yesterday}' and install_date > '${hiveconf:rpt_date_start}' and install_date <='${hiveconf:rpt_date}' and app_user_id is not null) u
left outer join (select distinct app,app_id,user_key,app_user_id,facebook_connect_ts from tmp_user_facebook_connect where dt='${hiveconf:rpt_date}') b
on u.user_key=b.user_key and u.app=b.app and u.app_id=b.app_id
where  not exists (select 1 from tmp_user_daily_login s WHERE dt > '${hiveconf:rpt_date_start}' and dt <= '${hiveconf:rpt_date}' AND u.app_id=s.app_id and u.install_date=s.dt and u.app_user_id=s.app_user_id and u.app=s.app and s.app_user_id is not null) 
)t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_dau_snapshot DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_dau_snapshot partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}') 
select id,user_key,date,app_id,app_version,level_start,level_end,os,os_version,device,browser,browser_version,country_code,country,language,ab_experiment,
ab_variant,is_new_user,resource_amount,received_resource_amount,is_payer,is_converted_today,revenue_usd,payment_cnt,session_cnt,playtime_sec,is_facebook_connected,is_facebook_connected_today
from fact_dau_snapshot where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;

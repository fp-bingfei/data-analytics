-------------------------------
-- Fruitscoot 1.2
-- Daily DML with struct
-------------------------------
USE fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

-- Update Hive metastore for partitions
MSCK REPAIR TABLE raw_invalid_events;
MSCK REPAIR TABLE raw_session_start;
MSCK REPAIR TABLE raw_session_end;
MSCK REPAIR TABLE raw_new_user;
MSCK REPAIR TABLE raw_payment;
MSCK REPAIR TABLE raw_level_up;
MSCK REPAIR TABLE raw_achievement;
MSCK REPAIR TABLE raw_facebook_register;
MSCK REPAIR TABLE raw_gift_received;
MSCK REPAIR TABLE raw_item_actioned;
MSCK REPAIR TABLE raw_load_step;
MSCK REPAIR TABLE raw_mission;
MSCK REPAIR TABLE raw_timer;
MSCK REPAIR TABLE raw_transaction;
MSCK REPAIR TABLE raw_tutorial;

exit;

use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

alter table tmp_user_daily_login drop partition (app='fruitscoot.global.prod');
alter table tmp_user_daily_login drop partition (app='fruitscoot.global.dev');

INSERT OVERWRITE TABLE tmp_user_daily_login PARTITION (app,dt)
SELECT date, user_key,app_id,app_user_id, facebook_id, install_ts, install_date,birthday, app_version, level_start, level_end, os, os_version,
t.country_code,c.country,last_ip,install_source, language,locale, gender,device, browser, browser_version, ab_experiment, ab_variant,count(1) session_cnt, sum(session_length_sec)  playtime_sec, max(ts_start) last_login_ts, app, dt
FROM
(
SELECT
date_start date,
s.app_id,
s.user_key,
s.app_user_id,
ts_start,
first_value(s.facebook_id,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as facebook_id,
first_value(s.birthday,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as birthday,
COALESCE(d.install_ts,first_value(s.install_ts,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following)) as install_ts,
COALESCE(d.install_date,first_value(s.install_date,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following)) as install_date,
first_value(s.app_version,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as app_version,
first_value(s.level_start,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start asc
     rows between unbounded preceding AND unbounded following) as level_start,
first_value(s.level_end,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as level_end,
first_value(s.os,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as os,
first_value(s.os_version,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as os_version,
first_value(s.country_code,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as country_code,
first_value(s.ip,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as last_ip,
first_value(s.install_source,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as install_source,
first_value(s.language,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as language,
first_value(s.locale,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as locale,
first_value(s.gender,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as gender,
first_value(s.device,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as device,
first_value(s.browser,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
 rows between unbounded preceding AND unbounded following) as browser,
first_value(s.browser_version,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as browser_version,
first_value(s.ab_experiment,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as ab_experiment,
first_value(s.ab_variant,true) OVER (PARTITION BY s.date_start, s.user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as ab_variant,
session_length_sec,
s.app,
s.dt
FROM (select * from fact_session where  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and app=app_id and app_user_id is not null)s
LEFT OUTER JOIN (select * from dim_user where dt = '${hiveconf:rpt_date_yesterday}' and app_user_id is not null) d on s.app = d.app and s.user_key = d.user_key
UNION ALL
SELECT
to_date(u.ts_pretty) as date,
u.app_id as app_id,
MD5(concat(u.app_id,u.user_id)) as user_key,
u.user_id as app_user_id,
u.ts_pretty ts_start,
u.properties['facebook_id'] facebook_id,
u.properties['birthday'] birthday,
u.install_ts_pretty install_ts,
to_date(u.install_ts_pretty) install_date,
u.properties['app_version'] app_version,
COALESCE(u.properties['level'], 0) as level_start,
COALESCE(u.properties['level'], 0) as level_end,
u.properties['os'] os,
u.properties['os_version'] os_version,
u.properties['country_code'] country_code,
u.properties['ip'] last_ip,
u.properties['install_source'] install_source,
u.properties['lang'] language,
u.properties['locale'] locale,
u.properties['gender'] gender,
u.properties['device'] device,
u.properties['browser'] browser,
u.properties['browser_version'] browser_version,
u.properties['ab_experiment'] ab_experiment,
u.properties['ab_variant'] ab_variant,
0 as session_length_sec,
u.app,
to_date(u.ts_pretty) as dt
FROM 
(
SELECT * FROM (    
select *, ROW_NUMBER() OVER (PARTITION BY app, user_id ORDER BY ts_pretty ASC) AS rnum 
FROM raw_new_user_daily 
WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app and user_id is not null
) X WHERE rnum = 1
)u
where  not exists (select 1 from fact_session s WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' 
AND u.app_id=s.app_id and to_date(u.ts_pretty)=s.dt and u.user_id=s.app_user_id and u.app=s.app and s.app_user_id is not null)
) t
left outer join dim_country c on t.country_code=c.country_code
WHERE t.app = t.app_id and t.app_user_id is not null
group by date,app_id, user_key,install_ts, app_user_id,facebook_id,install_date,app_version, level_start, level_end, os, os_version,
t.country_code, c.country,last_ip,install_source,language,locale,birthday, gender,device, browser, browser_version, ab_experiment, ab_variant,app,dt;


exit;


use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



INSERT OVERWRITE TABLE fact_mission PARTITION (app,dt)
select
MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
properties['mission_id'] as mission_id,
app_id  ,
properties['app_version'] as app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
properties['os'] as os,
properties['os_version'] as os_version,
properties['device'] as device,
properties['browser'] as browser,
properties['browser_version'] as browser_version,
properties['country_code'] as country_code,
properties['ip'] as ip,
properties['lang'] as language  ,
from_unixtime(cast (properties['mission_start_ts'] as bigint)) as mission_start_ts,
properties['mission_status'] as mission_status,
COALESCE(properties['mission_type'], null) as mission_type,
COALESCE(properties['helped_facebook_id'], null) as helped_facebook_id,
properties['level'] as level,
app,
to_date(ts_pretty) from (select app_id,user_id,app,properties,ts, ts_pretty,event,session_id,dt from raw_mission_daily 
WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and  to_date(ts_pretty) > '${hiveconf:rpt_date_start}' 
and to_date(ts_pretty)<='${hiveconf:rpt_date}' and app=app_id)
t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_mission DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id,mission_id,app_id,app_version,user_key,app_user_id,date,ts,session_id,os,os_version,device,browser,browser_version,country_code,ip,language,
mission_start_ts,mission_status,mission_type, helped_facebook_id, level
from fact_mission where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;

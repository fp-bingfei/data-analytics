use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 

INSERT OVERWRITE TABLE fact_player_resources PARTITION (app,dt)
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, event
, to_date(ts_pretty)
, ts_pretty
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, to_date(ts_pretty) as dt
FROM
(SELECT app, app_id, user_id, session_id, ts, event, ts_pretty
, pr.resource_id, pr.resource_name, pr.resource_type, pr.resource_amount
FROM 
raw_session_start_daily lateral view explode(collections.player_resources) coll AS pr
WHERE  
dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' 
and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
) s
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, event
, to_date(ts_pretty)
, ts_pretty
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, to_date(ts_pretty) as dt
FROM
(SELECT app, app_id, user_id, session_id, ts, event, ts_pretty
, pr.resource_id, pr.resource_name, pr.resource_type, pr.resource_amount
FROM 
raw_level_up_daily lateral view explode(collections.player_resources) coll AS pr
WHERE  
dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' 
and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
) l
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, event
, to_date(ts_pretty)
, ts_pretty
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, to_date(ts_pretty) as dt
FROM
(SELECT app, app_id, user_id, session_id, ts, event, ts_pretty
, pr.resource_id, pr.resource_name, pr.resource_type, pr.resource_amount
FROM 
raw_payment_daily lateral view explode(collections.player_resources) coll AS pr
WHERE  
dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' 
and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
) p
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, event
, to_date(ts_pretty)
, ts_pretty
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, to_date(ts_pretty) as dt
FROM
(SELECT app, app_id, user_id, session_id, ts, event, ts_pretty
, pr.resource_id, pr.resource_name, pr.resource_type, pr.resource_amount
FROM 
raw_transaction_daily lateral view explode(collections.player_resources) coll AS pr
WHERE  
dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' 
and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
) t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_player_resources DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_player_resources partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id
, app_id
, event
, date
, ts_pretty
, resource_id
, resource_name
, resource_type
, resource_amount
from fact_player_resources where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';


msck repair table raw_facebook_reward;

ALTER TABLE copy_facebook_reward_raw DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_facebook_reward_raw PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    MD5(concat(app_id, event, user_id, session_id, ts)) as id,
    MD5(concat(app_id,user_id)) as user_key,
    app_id,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['app_version'] as app_version,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['fb_reward'] as fb_reward,
    properties['reward_acorns'] as mission_status,
    properties['level'] as level
FROM
(
    SELECT
        app_id,
        user_id,
        properties,
        ts,
        ts_pretty,
        event,
        session_id
    FROM
        raw_facebook_reward 
where  to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}'
    ) t
;


msck repair table raw_daily_reward;

ALTER TABLE copy_daily_reward_raw DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_daily_reward_raw PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    MD5(concat(app_id, event, user_id, session_id, ts)) as id,
    app_id  ,
    properties['app_version'] as app_version,
    MD5(concat(app_id,user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    resource_id,
    resource_name,
    resource_type,
    resource_amount
FROM
(
    SELECT
        app_id,
        user_id,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        pr.resource_id,
        pr.resource_name,
        pr.resource_type,
        pr.resource_amount
    FROM
        raw_daily_reward lateral view explode(collections.player_resources) coll1 AS pr
where
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}'
   )t where resource_id='acorns';

exit;

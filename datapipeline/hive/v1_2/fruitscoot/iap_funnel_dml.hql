USE fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

--SET rpt_app=fruitscoot.global.dev;


ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE iap_funnel ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


INSERT OVERWRITE TABLE iap_funnel_daily PARTITION (app, dt)
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty,
  event, 
  user_id, 
  session_id,
  properties, 
  collections,
  app,
  '${hiveconf:rpt_date}'    
FROM iap_funnel
WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} 
AND day=${hiveconf:rpt_day} AND app=app_id and COALESCE(user_id, '') <> '';


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_iap_funnel DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_iap_funnel partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
  bi_version, 
  app_id, 
  ts_pretty,
  event, 
  user_id, 
  session_id,
  properties, 
  collections
from 
  iap_funnel_daily
WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';


exit;

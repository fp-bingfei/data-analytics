use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 


INSERT OVERWRITE TABLE dim_user_device PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT fs.app_id
, fs.app_user_id
, fs.user_key
, COALESCE(dud.idfa, fs.idfa) AS idfa
, COALESCE(dud.idfv, fs.idfv) AS idfv
, COALESCE(dud.gaid, fs.gaid) AS gaid
, COALESCE(dud.mac_address, fs.mac_address) AS mac_address
, COALESCE(dud.android_id, fs.android_id) AS android_id
, COALESCE(dud.first_seen_ts, fs.first_seen_ts) AS first_seen_ts
FROM 
(
SELECT distinct t.app, t.app_id,t.app_user_id,t.user_key,t.idfa,t.idfv,t.gaid,t.mac_address,t.android_id,t.first_seen_ts from
(
SELECT
  app_id
, app_user_id
, user_key
, first_value(idfa,true) OVER (PARTITION BY user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as idfa
, first_value(idfv,true) OVER (PARTITION BY user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as idfv
, first_value(gaid,true) OVER (PARTITION BY user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as gaid
, first_value(mac_address,true) OVER (PARTITION BY user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as mac_address
, first_value(android_id,true) OVER (PARTITION BY user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as android_id
, first_value(install_ts,true) OVER (PARTITION BY user_key order by ts_start asc
    rows between unbounded preceding AND unbounded following) as first_seen_ts
, app
, dt
FROM fact_session where app = '${hiveconf:rpt_app}' AND dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
)t ) fs
LEFT OUTER JOIN 
(SELECT * FROM dim_user_device 
WHERE app = '${hiveconf:rpt_app}' AND dt = '${hiveconf:rpt_date_yesterday}'
) dud 
ON (dud.app = fs.app AND dud.app_user_id = fs.app_user_id)
UNION ALL
SELECT 
dud.app_id
, dud.app_user_id
, dud.user_key
, dud.idfa
, dud.idfv
, dud.gaid
, dud.mac_address
, dud.android_id
, dud.first_seen_ts
FROM 
(SELECT * FROM dim_user_device 
WHERE app = '${hiveconf:rpt_app}' AND dt = '${hiveconf:rpt_date_yesterday}'
) dud 
LEFT OUTER JOIN 
(SELECT distinct user_key,app,app_user_id FROM fact_session WHERE app = '${hiveconf:rpt_app}' AND dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}') 
fs ON (dud.app = fs.app AND dud.app_user_id = fs.app_user_id and dud.user_key = fs.user_key )
WHERE fs.user_key IS NULL;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_dim_user_device DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_dim_user_device partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
app_id, app_user_id, user_key, idfa, idfv, gaid, mac_address, android_id, first_seen_ts 
from dim_user_device where dt ='${hiveconf:rpt_date}' AND app_user_id IS NOT NULL and user_key IS NOT NULL;

exit;

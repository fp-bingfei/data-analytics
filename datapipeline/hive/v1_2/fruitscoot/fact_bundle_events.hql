 use fruitscoot_1_2;


SET hive.exec.compress.intermediate=true;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.exec.parallel=true;
SET hive.stats.autogather=false;
SET hive.merge.mapfiles=true;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.dynamic.partition.mode=nonstrict;


INSERT OVERWRITE TABLE fact_bundle_events PARTITION (app,dt)
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_load_sa_ok_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_load_sa_start_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_load_sa_fail_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_load_sa_manifest_ok_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_load_sa_manifest_start_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_download_ok_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_download_start_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_download_error_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_download_fail_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_download_manifest_ok_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
coalesce(bundle_name,event) as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_bundle_download_manifest_start_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id, ts)) as id,
app_id as app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
event,
properties['os'] os,
properties['os_version'] os_version,
null as browser,
null as browser_version,
properties['device'] device,
properties['country_code'] country_code,
properties['level'] as level,
properties['ip'] ip,
properties['lang'] language,
null as ab_experiment,
null as ab_variant,
properties['idfa'] idfa,
properties['idfv'] idfv,
properties['gaid'] gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id,
properties['tutorial_step'],
event as tutorial_step_desc,
app as app,
to_date(ts_pretty)
FROM raw_map_loaded_begin_story_daily
WHERE  dt > '${hiveconf:rpt_date_start}' AND dt<='${hiveconf:rpt_date}' AND to_date(ts_pretty) > '${hiveconf:rpt_date_start}'
AND to_date(ts_pretty)<='${hiveconf:rpt_date}' and app_id=app
;




-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_bundle_events DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_bundle_events partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id,app_id,app_version,user_key,app_user_id,date,ts,session_id,event,os,os_version,browser,browser_version,device,country_code,level,ip,language,ab_experiment,ab_variant,idfa,idfv,gaid,mac_address,android_id,tutorial_step,
tutorial_step_desc from fact_bundle_events where dt >= '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
AND app_user_id IS NOT NULL AND app_user_id <> '';


insert overwrite table fact_bundle_events_s3copy
select 
id,app_id,app_version,user_key,app_user_id,date,ts,session_id,event,os,os_version,browser,browser_version,device,country_code,level,ip,language,ab_experiment,ab_variant,idfa,idfv,gaid,mac_address,android_id,tutorial_step,tutorial_step_desc from fact_bundle_events where dt >= '2015-07-10' and dt<='${hiveconf:rpt_date}' AND app_user_id IS NOT NULL AND app_user_id <> '';


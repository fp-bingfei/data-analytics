use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;




INSERT OVERWRITE TABLE fact_mission_objective PARTITION (app,dt)
select MD5(concat(app_id,user_id,session_id,ts_pretty,event)) as id,
objective_id,
objective_name,
objective_type,
objective_amount,
objective_amount_remaining,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.objective_id,
pr.objective_name,pr.objective_type, pr.objective_amount, pr.objective_amount_remaining
FROM raw_mission_daily
lateral view explode(collections.mission_objectives) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_fact_mission_objective partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, objective_id, objective_name, objective_type, objective_amount, objective_amount_remaining from copy_fact_mission_objective where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;

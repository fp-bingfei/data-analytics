use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



INSERT OVERWRITE TABLE fact_bundle_download_ok PARTITION (app,dt)
SELECT
    MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
    app_id,
    properties['app_version'] as app_version,
    MD5(concat(app_id, user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['browser'] as browser,
    properties['browser_version'] as browser_version,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    properties['tutorial_step'] as tutorial_step,
    properties['tutorial_step_desc'] as tutorial_step_desc,
    properties['idfa'] as idfa,
    properties['idfv'] as idfv,
    properties['mac_address'] as mac_address,
    properties['android_id'] as android_id,
    properties['gaid'] as gaid,
    app,
    to_date(ts_pretty)
FROM
(
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        to_json(collections.ab_tests) as ab_tests
    FROM 
        raw_message_sent_daily 
        lateral view explode(collections.player_resources) coll1 AS pr
    WHERE
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
)t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_bundle_download_ok DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_bundle_download_ok PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    id,
    app_id,
    app_version,
    user_key,
    app_user_id,
    date,
    ts,
    session_id,
    os,
    os_version,
    device,
    browser,
    browser_version,
    country_code,
    ip,
    language,
    level,
    tutorial_step,
    tutorial_step_desc,
    idfa,
    idfv,
    mac_address,
    android_id,
    gaid
from fact_bundle_download_ok where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;

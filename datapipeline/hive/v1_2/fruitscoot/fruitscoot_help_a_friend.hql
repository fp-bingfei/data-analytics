------------------
--fact_mission_resources_spent
-------------------

--fruitscoot_1_2_hive_ddl.hql
CREATE EXTERNAL TABLE fact_mission_resources_spent
(
`id`  string,
`app_id`  string,
`date`  string,
`user_key` string,
`resource_id` string,
`resource_name` string,
`resource_type` string,
`resource_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/processed/fact_mission_resources_spent'
TBLPROPERTIES('serialization.null.format'='')
;


--fruitscoot_1_2_hive_copy_ddl.hql
CREATE TABLE `copy_fact_mission_resources_spent`
(
`id`  string,
`app_id`  string,
`date`  string,
`user_key` string,
`resource_id` string,
`resource_name` string,
`resource_type` string,
`resource_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_resources_spent'
TBLPROPERTIES('serialization.null.format'='')
;

--fact_mission_collections_dml.hql
INSERT OVERWRITE TABLE fact_mission_resources_spent PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
MD5(concat(app_id,user_id)) as user_key,
resource_id,
resource_name,
resource_type,
resource_amount,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.resource_id,
pr.resource_name, pr.resource_type, pr.resource_amount
FROM raw_mission_daily
lateral view explode(collections.resources_spent) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

ALTER TABLE copy_fact_mission_resources_spent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_resources_spent partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select
id, app_id, date, user_key, resource_id, resource_name, resource_type, resource_amount from fact_mission_resources_spent where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;


--add copy2redshift in pipeline


-------------------
--fact_mission
-------------------
--fruitscoot_1_2_hive_ddl.hql
CREATE EXTERNAL TABLE IF NOT EXISTS fact_mission
(
`id`	string,
`mission_id`	string,	
`app_id`	string,	
`app_version`	string,	
`user_key`	string,	
`app_user_id`	string,	
`date`	string,	
`ts`	timestamp,		
`session_id`	string,	
`os`	string,	
`os_version`	string,	
`device`	string,	
`browser`	string,	
`browser_version`	string,	
`country_code`	string,	
`ip`	string,	
`language`	string,	
`mission_start_ts`	timestamp,	
`mission_status`	int,
`mission_type` string,
`helped_facebook_id` string,
`level`	int	
)PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/processed/fact_mission'
TBLPROPERTIES('serialization.null.format'='')
;

--fruitscoot_1_2_hive_copy_ddl.hql
CREATE TABLE `copy_fact_mission`(
  `id` string,
  `mission_id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date` string,
  `ts` timestamp,
  `session_id` string,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `ip` string,
  `language` string,
  `mission_start_ts` timestamp,
  `mission_status` int,
  `mission_type` string,
  `helped_facebook_id` string,
  `level` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission'
TBLPROPERTIES('serialization.null.format'='');

--fact_mission_dml.hql
use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



INSERT OVERWRITE TABLE fact_mission PARTITION (app,dt)
select
MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
properties['mission_id'] as mission_id,
app_id  ,
properties['app_version'] as app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
session_id,
properties['os'] as os,
properties['os_version'] as os_version,
properties['device'] as device,
properties['browser'] as browser,
properties['browser_version'] as browser_version,
properties['country_code'] as country_code,
properties['ip'] as ip,
properties['lang'] as language  ,
from_unixtime(cast (properties['mission_start_ts'] as bigint)) as mission_start_ts,
properties['mission_status'] as mission_status,
COALESCE(properties['mission_type'], null) as mission_type,
COALESCE(properties['helped_facebook_id'], null) as helped_facebook_id,
properties['level'] as level,
app,
to_date(ts_pretty) from (select app_id,user_id,app,properties,ts, ts_pretty,event,session_id,dt from raw_mission_daily 
WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and  to_date(ts_pretty) > '${hiveconf:rpt_date_start}' 
and to_date(ts_pretty)<='${hiveconf:rpt_date}' and app=app_id)
t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_mission DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id,mission_id,app_id,app_version,user_key,app_user_id,date,ts,session_id,os,os_version,device,browser,browser_version,country_code,ip,language,
mission_start_ts,mission_status,mission_type, helped_facebook_id, level
from fact_mission where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;

----------------------
--accept_help
----------------------
--fruitscoot_1_2_hive_hourly_ddl.hql
DROP TABLE IF EXISTS raw_accept_help;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_accept_help (
  bi_version string,
  app_id string,
  ts int,
  ts_pretty string,
  event string,
  user_id string,
  session_id string,
  properties map<
    string, string
  >,
  collections struct<
    player_resources: array<
      struct<
        resource_id: string,
        resource_name: string,
        resource_type: string,
        resource_amount: int
      >
    >,
    ab_tests: array<
      struct<
        ab_experiment: string,
        ab_variant: string
      >
    >,
    resources_spent: array<
      struct<
        resource_id: string,
        resource_name: string,
        resource_type: string,
        resource_amount: int
      >
    >
  >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/events/accept_help'
TBLPROPERTIES (
  'serialization.null.format'=''
);


--fruitscoot_1_2_hive_daily_ddl.hql
DROP TABLE IF EXISTS raw_accept_help_daily;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_accept_help_daily (
  bi_version string,
  app_id string,
  ts int,
  ts_pretty timestamp,
  event string,
  user_id string,
  session_id string,
  properties map<
    string, string
  >,
  collections struct<
    player_resources: array<
      struct<
        resource_id: string,
        resource_name: string,
        resource_type: string,
        resource_amount: int
      >
    >,
    ab_tests: array<
      struct<
        ab_experiment: string,
        ab_variant: string
      >
    >,
    resources_spent: array<
      struct<
        resource_id: string,
        resource_name: string,
        resource_type: string,
        resource_amount: int
      >
    >
  >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/events_daily/accept_help_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

--raw_accept_help_daily_dml.hql

USE fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

SET rpt_app=fruitscoot.global.prod;
SET rpt_dev_app=fruitscoot.global.dev;

-- for fruitscoot.global.prod
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- for fruitscoot.global.dev
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_accept_help ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


INSERT OVERWRITE TABLE raw_accept_help_daily PARTITION (app, dt)
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  session_id, 
  properties, 
  collections,
  app,
  '${hiveconf:rpt_date}'
FROM raw_accept_help
WHERE app in ('${hiveconf:rpt_app}', '${hiveconf:rpt_dev_app}') AND year=${hiveconf:rpt_year} 
AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} AND COALESCE(user_id, '') <> '';

exit;

--fruitscoot_1_2_hive_ddl.hql

----fact_accept_help
CREATE EXTERNAL TABLE IF NOT EXISTS fact_accept_help
(
  `id`  string,
  `app_id`  string, 
  `app_version` string, 
  `user_key`  string, 
  `app_user_id` string, 
  `date`  string, 
  `ts`  timestamp,    
  `session_id`  string, 
  `os`  string, 
  `os_version`  string, 
  `device`  string, 
  `country_code`  string, 
  `ip`  string, 
  `language`  string, 
  `level` int,
  `mission_id` string,
  `helper_facebook_id` string,
  `resource_id` string,
  `resource_name` string,
  `resource_type` string,
  `resource_amount` int,
  `ab_test` string
)PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/processed/fact_accept_help'
TBLPROPERTIES('serialization.null.format'='')
;

--fruitscoot_1_2_hive_copy_ddl.hql
---copy_fact_accept_help
CREATE TABLE `copy_fact_accept_help`(
  `id`  string,
  `app_id`  string, 
  `app_version` string, 
  `user_key`  string, 
  `app_user_id` string, 
  `date`  string, 
  `ts`  timestamp,    
  `session_id`  string, 
  `os`  string, 
  `os_version`  string, 
  `device`  string, 
  `country_code`  string, 
  `ip`  string, 
  `language`  string, 
  `level` int,
  `mission_id` string,
  `helper_facebook_id` string,
  `resource_id` string,
  `resource_name` string,
  `resource_type` string,
  `resource_amount` int,
  `ab_test` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_accept_help'
TBLPROPERTIES('serialization.null.format'='');

--fact_accept_help_dml.hql
use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



INSERT OVERWRITE TABLE fact_accept_help PARTITION (app,dt)
SELECT
    MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
    app_id,
    properties['app_version'] as app_version,
    MD5(concat(app_id, user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    properties['mission_id'] as mission_id,
    properties['helper_facebook_id'] as helper_facebook_id,
    resource_id,
    resource_name,
    resource_type,
    resource_amount,
    ab_tests as ab_test,
    app,
    to_date(ts_pretty)
FROM
(
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        rs.resource_id as resource_id,
        rs.resource_name as resource_name,
        rs.resource_type as resource_type,
        rs.resource_amount as resource_amount,
        to_json(collections.ab_tests) as ab_tests
    FROM 
        raw_accept_help_daily 
        lateral view explode(collections.resources_spent) coll1 AS rs
    WHERE
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
)t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_accept_help DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_accept_help PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    id,
    app_id,
    app_version,
    user_key,
    app_user_id,
    date,
    ts,
    session_id,
    os,
    os_version,
    device,
    country_code,
    ip,
    language,
    level,
    mission_id,
    helper_facebook_id,
    resource_id,
    resource_name,
    resource_type,
    resource_amount,
    ab_test
from fact_accept_help where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;





----------------------
--message_sent
----------------------
--fruitscoot_1_2_hive_hourly_ddl.hql
DROP TABLE IF EXISTS raw_message_sent;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_message_sent (
  bi_version string,
  app_id string,
  ts int,
  ts_pretty string,
  event string,
  user_id string,
  session_id string,
  properties map<
    string, string
  >,
  collections struct<
    player_resources: array<
      struct<
        resource_id: string,
        resource_name: string,
        resource_type: string,
        resource_amount: int
      >
    >,
    ab_tests: array<
      struct<
        ab_experiment: string,
        ab_variant: string
      >
    >
  >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int,
  hour int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/events/message_sent'
TBLPROPERTIES (
  'serialization.null.format'=''
);


--fruitscoot_1_2_hive_daily_ddl.hql
DROP TABLE IF EXISTS raw_message_sent_daily;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_message_sent_daily (
  bi_version string,
  app_id string,
  ts int,
  ts_pretty timestamp,
  event string,
  user_id string,
  session_id string,
  properties map<
    string, string
  >,
  collections struct<
    player_resources: array<
      struct<
        resource_id: string,
        resource_name: string,
        resource_type: string,
        resource_amount: int
      >
    >,
    ab_tests: array<
      struct<
        ab_experiment: string,
        ab_variant: string
      >
    >
  >
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/events_daily/message_sent_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

--raw_message_sent_daily_dml.hql

USE fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

SET rpt_app=fruitscoot.global.prod;
SET rpt_dev_app=fruitscoot.global.dev;

-- for fruitscoot.global.prod
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- for fruitscoot.global.dev
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_message_sent ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_dev_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);


INSERT OVERWRITE TABLE raw_message_sent_daily PARTITION (app, dt)
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  event, 
  user_id, 
  session_id, 
  properties, 
  collections,
  app,
  '${hiveconf:rpt_date}'
FROM raw_message_sent
WHERE app in ('${hiveconf:rpt_app}', '${hiveconf:rpt_dev_app}') AND year=${hiveconf:rpt_year} 
AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} AND COALESCE(user_id, '') <> '';

exit;

--fruitscoot_1_2_hive.ddl.hql

----fact_message_sent
CREATE EXTERNAL TABLE IF NOT EXISTS fact_message_sent
(
  `id`  string,
  `app_id`  string, 
  `app_version` string, 
  `user_key`  string, 
  `app_user_id` string, 
  `date`  string, 
  `ts`  timestamp,    
  `session_id`  string, 
  `os`  string, 
  `os_version`  string, 
  `device`  string, 
  `country_code`  string, 
  `ip`  string, 
  `language`  string, 
  `level` int,
  `message_type` string,
  `recipient_user_id` string,
  `resource_id` string,
  `resource_name` string,
  `resource_type` string,
  `resource_amount` int,
  `ab_test` string
)PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/processed/fact_message_sent'
TBLPROPERTIES('serialization.null.format'='')
;

--fruitscoot_1_2_hive_copy_ddl.hql
---copy_fact_message_sent
CREATE TABLE `copy_fact_message_sent`(
  `id`  string,
  `app_id`  string, 
  `app_version` string, 
  `user_key`  string, 
  `app_user_id` string, 
  `date`  string, 
  `ts`  timestamp,    
  `session_id`  string, 
  `os`  string, 
  `os_version`  string, 
  `device`  string, 
  `country_code`  string, 
  `ip`  string, 
  `language`  string, 
  `level` int,
  `message_type` string,
  `recipient_user_id` string,
  `resource_id` string,
  `resource_name` string,
  `resource_type` string,
  `resource_amount` int,
  `ab_test` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_message_sent'
TBLPROPERTIES('serialization.null.format'='');


--fact_message_sent_dml.hql
use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



INSERT OVERWRITE TABLE fact_message_sent PARTITION (app,dt)
SELECT
    MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
    app_id,
    properties['app_version'] as app_version,
    MD5(concat(app_id, user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    properties['message_type'] as message_type,
    properties['recipient_user_id'] as recipient_user_id,
    resource_id,
    resource_name,
    resource_type,
    resource_amount,
    ab_tests as ab_test,
    app,
    to_date(ts_pretty)
FROM
(
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        pr.resource_id as resource_id,
        pr.resource_name as resource_name,
        pr.resource_type as resource_type,
        pr.resource_amount as resource_amount,
        to_json(collections.ab_tests) as ab_tests
    FROM 
        raw_message_sent_daily 
        lateral view explode(collections.player_resources) coll1 AS pr
    WHERE
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
)t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_message_sent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_message_sent PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    id,
    app_id,
    app_version,
    user_key,
    app_user_id,
    date,
    ts,
    session_id,
    os,
    os_version,
    device,
    country_code,
    ip,
    language,
    level,
    message_type,
    recipient_user_id,
    resource_id,
    resource_name,
    resource_type,
    resource_amount,
    ab_test
from fact_message_sent where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;


































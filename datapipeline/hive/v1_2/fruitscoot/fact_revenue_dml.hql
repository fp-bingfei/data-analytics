----------------------------------
-- Query to generate fact_revenue
----------------------------------
use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 

ALTER TABLE currency ADD IF NOT EXISTS PARTITION (date='${hiveconf:rpt_date}') LOCATION "s3://com.funplusgame.bidata/currency/${hiveconf:rpt_date_nohyphen}/";

INSERT OVERWRITE TABLE fact_revenue PARTITION (app,dt)
select
 id,
 app_id as app_id,
 app_version,
 user_key,
 app_user_id,
 date,
 ts,
 install_ts,
 install_date,
 session_id,
 level,
 os, 
 os_version,
 device,
 browser,
 browser_version,
 country_code,
 install_source,
 ip,
 language,
 locale,
 ab_experiment,
 ab_variant,
 payment_processor,
 product_id,
 product_name,
 product_type,
 currency,
 revenue_currency, 
 revenue_usd,
 transaction_id,
 idfa,
 idfv,
 gaid,
 mac_address,
 android_id,
 life_wallet,
 app,
 dt
from (
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
properties['install_ts_pretty'] as install_ts,
to_date(properties['install_ts_pretty']) install_date,
session_id,
COALESCE(properties['level'],0) as level,
properties['os'] os, 
properties['os_version'] os_version,
properties['device'] device,
null as browser,
null as browser_version,
properties['country_code'] country_code,
properties['install_source'] install_source,
properties['ip'] ip,
properties['lang'] language,
properties['locale'] locale,
null as ab_experiment,
null as ab_variant,
properties['payment_processor'] payment_processor,
properties['iap_product_id'] product_id,
properties['iap_product_name'] product_name,
properties['iap_product_type'] product_type,
r.properties['currency'] currency,
properties['amount']/100 revenue_currency, 
properties['amount']*c.factor/100 revenue_usd,
properties['transaction_id'] transaction_id,
properties['idfa'] as idfa,
properties['idfv'] as idfv,
properties['gaid'] as gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id, 
resource_amount as life_wallet, 
row_number() over (partition by app_id,user_id,properties['transaction_id'] order by ts_pretty) rank, 
r.app as app,
to_date(r.ts_pretty) as dt
from 
(
select app_id, dt, user_id, ts, event, ts_pretty, session_id, properties, app
,  pr.resource_amount
from raw_payment_daily 
lateral view explode(collections.player_resources) coll1 AS pr 
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' 
and to_date(ts_pretty)<='${hiveconf:rpt_date}' and app=app_id
) r
left outer join 
(SELECT * FROM currency WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}') c on r.properties['currency'] =c.currency and r.dt=c.dt
)t where rank=1 ;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 

ALTER TABLE copy_fact_revenue DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_revenue partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select id, app_id, app_version, user_key, app_user_id, date, ts,install_ts, install_date, session_id, level
, os, os_version, device, browser, browser_version, country_code, install_source, ip, language, locale
, ab_experiment, ab_variant, payment_processor, product_id, product_name, product_type, currency, revenue_currency
, revenue_usd, transaction_id, idfa, idfv, gaid, mac_address, android_id
, life_wallet
from fact_revenue where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

ALTER TABLE copy_payment_resources_received DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_payment_resources_received PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    MD5(concat(app_id, event, user_id, session_id, ts)) as id,
    app_id  ,
    properties['app_version'] as app_version,
    MD5(concat(app_id,user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    resource_id,
    resource_name,
    resource_type,
    resource_amount
FROM
(
    SELECT
        app_id,
        user_id,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        pr.resource_id,
        pr.resource_name,
        pr.resource_type,
        pr.resource_amount
    FROM
        raw_payment_daily lateral view explode(collections.resources_received) coll1 AS pr
   where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
   )t where resource_id='acorns';

exit;





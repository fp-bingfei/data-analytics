use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;




INSERT OVERWRITE TABLE fact_mission_objective PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
objective_id,
objective_name,
objective_type,
objective_amount,
objective_amount_remaining,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.objective_id,
pr.objective_name,pr.objective_type, pr.objective_amount, pr.objective_amount_remaining
FROM raw_mission_daily
lateral view explode(collections.mission_objectives) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

INSERT OVERWRITE TABLE fact_mission_abtest PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
MD5(concat(app_id,user_id)) as user_key,
ab_experiment,
ab_variant,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.ab_experiment,
pr.ab_variant
FROM raw_mission_daily
lateral view explode(collections.ab_tests) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

INSERT OVERWRITE TABLE fact_mission_items_spent PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
MD5(concat(app_id,user_id)) as user_key,
item_id,
item_name,
item_amount,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.item_id,
pr.item_name, pr.item_amount
FROM raw_mission_daily
lateral view explode(collections.items_spent) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

INSERT OVERWRITE TABLE fact_mission_statistic PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
name as statistic_name,
value as statistic_value,
app,
to_date(ts_pretty) from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.name,pr.value
FROM raw_mission_daily
lateral view explode(collections.mission_statistics) coll1 AS pr
WHERE dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

INSERT OVERWRITE TABLE fact_mission_parameter PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
name as parameter_name,
value as parameter_value,
app,
to_date(ts_pretty) from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.name,pr.value
FROM raw_mission_daily
lateral view explode(collections.mission_parameters) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

INSERT OVERWRITE TABLE fact_mission_resources_spent PARTITION (app,dt)
SELECT MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
to_date(ts_pretty) as date,
MD5(concat(app_id,user_id)) as user_key,
resource_id,
resource_name,
resource_type,
resource_amount,
app,
to_date(ts_pretty)
from
(
SELECT bi_version, app_id, ts, ts_pretty, user_id, session_id, event, properties, app, dt, pr.resource_id,
pr.resource_name, pr.resource_type, pr.resource_amount
FROM raw_mission_daily
lateral view explode(collections.resources_spent) coll1 AS pr
WHERE  dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
)t
;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_mission_objective DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_objective partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, objective_id, objective_name, objective_type, objective_amount, objective_amount_remaining from fact_mission_objective where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;

ALTER TABLE copy_fact_mission_abtest DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_abtest partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select
id, app_id, date, user_key, ab_experiment, ab_variant from fact_mission_abtest where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;

ALTER TABLE copy_fact_mission_items_spent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_items_spent partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select
id, app_id, date, user_key, item_id, item_name, item_amount from fact_mission_items_spent where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;

ALTER TABLE copy_fact_mission_statistic DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_statistic partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, statistic_name, statistic_value from fact_mission_statistic where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;


ALTER TABLE copy_fact_mission_parameter DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_parameter partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id, app_id, date, parameter_name, parameter_value from fact_mission_parameter where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;

ALTER TABLE copy_fact_mission_resources_spent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_mission_resources_spent partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select
id, app_id, date, user_key, resource_id, resource_name, resource_type, resource_amount from fact_mission_resources_spent where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}'
;

ALTER TABLE raw_mission_daily_split DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');


INSERT OVERWRITE TABLE raw_mission_daily_split PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
  MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
  bi_version,
  app_id,
  to_date(ts_pretty) as date,
  event,
  user_id,
  collections.mission_objectives[0].objective_name as objective_name1,
  collections.mission_objectives[1].objective_name as objective_name2,
  collections.mission_objectives[2].objective_name as objective_name3,
  collections.mission_objectives[3].objective_name as objective_name4,
  collections.mission_objectives[4].objective_name as objective_name5,
  collections.mission_objectives[5].objective_name as objective_name6,
  collections.mission_objectives[6].objective_name as objective_name7,
  collections.mission_objectives[0].objective_amount_remaining as objective_remain1,
  collections.mission_objectives[1].objective_amount_remaining as objective_remain2,
  collections.mission_objectives[2].objective_amount_remaining as objective_remain3,
  collections.mission_objectives[3].objective_amount_remaining as objective_remain4,
  collections.mission_objectives[4].objective_amount_remaining as objective_remain5,
  collections.mission_objectives[5].objective_amount_remaining as objective_remain6,
  collections.mission_objectives[6].objective_amount_remaining as objective_remain7
 from raw_mission_daily where app='fruitscoot.global.prod' and
 dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}';


ALTER TABLE copy_mission_resources_received DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_mission_resources_received PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    MD5(concat(app_id, event, user_id, session_id, ts)) as id,
    app_id  ,
    properties['app_version'] as app_version,
    MD5(concat(app_id,user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    resource_id,
    resource_name,
    resource_type,
    resource_amount
FROM
(
    SELECT
        app_id,
        user_id,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        pr.resource_id,
        pr.resource_name,
        pr.resource_type,
        pr.resource_amount
    FROM
        raw_mission_daily lateral view explode(collections.resources_received) coll1 AS pr
   where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}' and to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and to_date(ts_pretty)<='${hiveconf:rpt_date}'
   )t where resource_id='acorns';

exit;





exit;

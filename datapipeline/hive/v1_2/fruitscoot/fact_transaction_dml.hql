use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
set hive.merge.mapfiles=true;
set hive.merge.mapredfiles=true;
SET hive.stats.autogather=false;

INSERT OVERWRITE TABLE fact_transaction PARTITION (app,dt)
SELECT
    MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
    properties['transaction_type'] as transaction_type,
    app_id,
    properties['app_version'] as app_version,
    MD5(concat(app_id,user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    ab_tests as ab_test,
    item_id,
    item_name,
    item_type,
    quantity,
    acorn_in,
    acorn_out,
    properties['last_played_mission'] as last_played_mission,
    app,
    to_date(ts_pretty)
FROM
(
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        to_json(collections.ab_tests) as ab_tests,
        ir.item_id as item_id,
        ir.item_name as item_name,
        ir.item_type as item_type,
        ir.item_amount as quantity,
        CASE WHEN collections.resources_received[0].resource_id = 'acorns' THEN COALESCE(collections.resources_received[0].resource_amount,0) ELSE 0 END as acorn_in,
        CASE WHEN collections.resource_spent[0].resource_id = 'acorns' THEN COALESCE(collections.resource_spent[0].resource_amount,0) ELSE 0 END as acorn_out
    FROM 
        raw_transaction_daily 
        lateral view explode(collections.items_received) coll1 AS ir
    WHERE
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
)t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_transaction DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_transaction PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    id,
    transaction_type,
    app_id,
    app_version,
    user_key,
    app_user_id,
    date,
    ts,
    session_id,
    os,
    os_version,
    device,
    country_code,
    ip,
    language,
    level,
    ab_test,
    item_id,
    item_name,
    item_type,
    quantity,
    acorn_in,
    acorn_out,
    last_played_mission
from fact_transaction where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';


INSERT OVERWRITE TABLE copy_transaction_resources_spent PARTITION(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    MD5(concat(app_id, event, user_id, session_id, ts)) as id,
    app_id  ,
    properties['app_version'] as app_version,
    MD5(concat(app_id,user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    resource_id,
    resource_name,
    resource_type,
    resource_amount
FROM
(

    SELECT
        app_id,
        user_id,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        pr.resource_id,
        pr.resource_name,
        pr.resource_type,
        pr.resource_amount
FROM
        raw_transaction_daily lateral view explode(collections.resource_spent) coll1 AS pr
     WHERE
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}'
   )t where resource_id='acorns' ;



exit;



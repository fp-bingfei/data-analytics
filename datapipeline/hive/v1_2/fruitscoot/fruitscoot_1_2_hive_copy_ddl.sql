use fruitscoot_1_2;

-- copy_fact_levelup
CREATE TABLE IF NOT EXISTS copy_fact_levelup 
(
id  STRING COMMENT 'id is md5(concat(app_name, app_user_id,current_level,levelup_ts)',
user_key  STRING,
app_id  STRING COMMENT 'app_id is the same as app which is being used as partition key',
app_user_id  STRING,
session_id STRING,
previous_level  SMALLINT,
previous_levelup_date  STRING,
previous_levelup_ts  TIMESTAMP,
current_level  SMALLINT,
levelup_date STRING,
levelup_ts TIMESTAMP,
browser STRING,
browser_version STRING,
os STRING,
os_version STRING,
device STRING,
country_code STRING,
ip STRING,
language STRING,
life_wallet DOUBLE,
locale STRING,
ab_experiment STRING,
ab_variant STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_levelup'
TBLPROPERTIES('serialization.null.format'='');

-- copy_dim_user
CREATE TABLE IF NOT EXISTS copy_dim_user
(
  id  STRING,
  user_key STRING,
  app_id STRING,
  app_user_id  STRING,
  facebook_id STRING,
  install_ts  TIMESTAMP,
  install_date  STRING,
  install_source  STRING,
  install_subpublisher  STRING,
  install_campaign  STRING,
  install_language  STRING,
  install_locale  STRING,
  install_country_code  STRING,
  install_country  STRING,
  install_os  STRING,
  install_device  STRING,
  install_device_alias  STRING,
  install_browser  STRING,
  install_gender  STRING,
  install_age  STRING,
  language  STRING,
  locale STRING,
  birthday  STRING,
  gender STRING,
  country_code  STRING,
  country  STRING,
  os STRING,
  os_version STRING,
  device  STRING,
  device_alias  STRING,
  browser STRING,
  browser_version STRING,
  app_version STRING,
  level INT,
  levelup_ts TIMESTAMP,
  ab_experiment STRING,
  ab_variant STRING,
  is_payer INT,
  conversion_ts TIMESTAMP,
  total_revenue_usd DECIMAL(12,4),
  payment_cnt INT,
  last_login_ts TIMESTAMP,
  email STRING,
  last_ip STRING
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/dim_user'
TBLPROPERTIES('serialization.null.format'='');



CREATE TABLE IF NOT EXISTS `copy_fact_session`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date_start` string,
  `date_end` string,
  `ts_start` timestamp,
  `ts_end` timestamp,
  `install_ts` timestamp,
  `install_date` string,
  `session_id` string,
  `facebook_id` string,
  `install_source` string,
  `os` string,
  `os_version` string,
  `browser` string,
  `browser_version` string,
  `device` string,
  `country_code` string,
  `level_start` int,
  `level_end` int,
  `gender` string,
  `birthday` string,
  `email` string,
  `ip` string,
  `language` string,
  `locale` string,
  `life_wallet` int,
  `ab_experiment` string,
  `ab_variant` string,
  `session_length_sec` int,
  `idfa` string,
  `idfv` string,
  `gaid` string,
  `mac_address` string,
  `android_id` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_session'
TBLPROPERTIES('serialization.null.format'='');



CREATE  TABLE `copy_fact_revenue`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date` string,
  `ts` timestamp,
  `install_ts` timestamp,
  `install_date` string,
  `session_id` string,
  `level` int,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `install_source` string,
  `ip` string,
  `language` string,
  `locale` string,
  `ab_experiment` string,
  `ab_variant` string,
  `payment_processor` string,
  `product_id` string,
  `product_name` string,
  `product_type` string,
  `currency` string,
  `revenue_currency` double,
  `revenue_usd` double,
  `transaction_id` string,
  `idfa` string,
  `idfv` string,
  `gaid` string,
  `mac_address` string,
  `android_id` string,
  `life_wallet` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_revenue'
TBLPROPERTIES('serialization.null.format'='')

;


CREATE TABLE `copy_fact_mission_resources_spent`
(
`id`  string,
`app_id`  string,
`date`  string,
`user_key` string,
`resource_id` string,
`resource_name` string,
`resource_type` string,
`resource_amount` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_resources_spent'
TBLPROPERTIES('serialization.null.format'='')
;



CREATE TABLE IF NOT EXISTS `copy_fact_dau_snapshot`(
  `id` string,
  `user_key` string,
  `date` string,
  `app_id` string,
  `app_version` string,
  `level_start` int,
  `level_end` int,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `country` string,
  `language` string,
  `ab_experiment` string,
  `ab_variant` string,
  `is_new_user` smallint,
  `resource_amount` int,
  `received_resource_amount` bigint,
  `is_payer` smallint,
  `is_converted_today` smallint,
  `revenue_usd` double,
  `payment_cnt` int,
  `session_cnt` int,
  `playtime_sec` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_dau_snapshot'
TBLPROPERTIES('serialization.null.format'='');





CREATE  TABLE IF NOT EXISTS `copy_fact_tutorial`(
  `id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date` string,
  `ts` timestamp,
  `session_id` string,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `ip` string,
  `language` string,
  `locale` string,
  `life_wallet` int,
`level`	  int,
  `tutorial_step` int,
  `tutorial_step_desc` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_tutorial'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE `copy_fact_mission`(
  `id` string,
  `mission_id` string,
  `app_id` string,
  `app_version` string,
  `user_key` string,
  `app_user_id` string,
  `date` string,
  `ts` timestamp,
  `session_id` string,
  `os` string,
  `os_version` string,
  `device` string,
  `browser` string,
  `browser_version` string,
  `country_code` string,
  `ip` string,
  `language` string,
  `mission_start_ts` timestamp,
  `mission_status` int,
  `mission_type` string,
  `helped_facebook_id` string,
  `level` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE `copy_fact_mission_objective`
(
  `id` string,
  `app_id` string,
  `date` string,
  `objective_id` string,
  `objective_name` string,
  `objective_type` string,
  `objective_amount` int,
  `objective_amount_remaining` int)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_objective'
TBLPROPERTIES('serialization.null.format'='')
;

CREATE TABLE `copy_fact_mission_abtest`
(
  `id` string,
  `app_id` string,
  `date` string,
  `user_key` string,
  `ab_experiment` string,
  `ab_variant` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_abtest'
TBLPROPERTIES('serialization.null.format'='')
;

CREATE TABLE `copy_fact_mission_items_spent`
(
`id`  string,
`app_id`  string,
`date`  string,
`user_key` string,
`item_id` string,
`item_name` string,
`item_amount` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_items_spent'
TBLPROPERTIES('serialization.null.format'='')
;

create table copy_fact_mission_parameter
(
`id`	string,
`app_id`	string,
`date`	string,
`parameter_name`	string,
`parameter_value`	int
)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_parameter'
TBLPROPERTIES('serialization.null.format'='')
;

create  table copy_fact_mission_statistic
(
`id`	string,
`app_id`	string,
`date`	string,
`statistic_name`	string,
`statistic_value`	int
)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_mission_statistic'
TBLPROPERTIES('serialization.null.format'='')
;


CREATE TABLE IF NOT EXISTS copy_raw_adjust_daily
(
  adid  STRING,
  userid STRING,
  game STRING,
  tracker  STRING,
  tracker_name STRING,
  app_id  STRING,
  ip_address  STRING,
  idfa  STRING,
  android_id STRING,
  mac_sha1  STRING,
  idfa_md5  STRING,
  country  STRING,
  timestamp  timestamp,
  mac_md5  STRING,
  gps_adid  STRING,
  device_name  STRING,
  os_name  STRING,
  os_version  STRING
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/raw_adjust_daily'
TBLPROPERTIES('serialization.null.format'='');


create table copy_fact_player_resources
(
id string,
app_id string,
event string,
date string,
ts_pretty timestamp,
resource_id string,
resource_name string,
resource_type string,
resource_amount int
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_player_resources'
TBLPROPERTIES('serialization.null.format'='');


----copy_dim_user_device
CREATE TABLE copy_dim_user_device(
  app_id string,
  app_user_id string,
  user_key string,  
  idfa string,
  idfv string,
  gaid string,
  mac_address string,
  android_id string,
  first_seen_ts timestamp
)  
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/dim_user_device'
TBLPROPERTIES('serialization.null.format'='');

----copy_challenge_raw
CREATE TABLE copy_challenge_raw(
  id string,
  mission_id string,
  mission_name string,
  mission_type string,
  app_id string,
  app_version string,
  user_key string,
  app_user_id string,
  `date` string,
  ts timestamp,
  session_id string,
  os string,
  os_version string,
  device string,
  country_code string,
  ip string,
  language string,
  mission_start_ts timestamp,
  mission_status int,
  mission_total_sec int,
  level int,
  star int,
  ab_test string,
  items_spent string)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/copy_challenge_raw'
TBLPROPERTIES('serialization.null.format'='');

---copy_fact_transaction
CREATE  TABLE `copy_fact_transaction`(
  `id`  string,
  `transaction_type`  string, 
  `app_id`  string, 
  `app_version` string, 
  `user_key`  string, 
  `app_user_id` string, 
  `date`  string, 
  `ts`  timestamp,    
  `session_id`  string, 
  `os`  string, 
  `os_version`  string, 
  `device`  string, 
  `country_code`  string, 
  `ip`  string, 
  `language`  string, 
  `level` int,
  `ab_test` string,
  `item_id` int, 
  `item_name` string,
  `item_type` string,
  `quantity` int,
  `acorn_in` int,
  `acorn_out` int,
  `last_played_mission` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_transaction'
TBLPROPERTIES('serialization.null.format'='');

---copy_fact_accept_help
CREATE TABLE `copy_fact_accept_help`(
  `id`  string,
  `app_id`  string,
  `app_version` string,
  `user_key`  string,
  `app_user_id` string,
  `date`  string,
  `ts`  timestamp,
  `session_id`  string,
  `os`  string,
  `os_version`  string,
  `device`  string,
  `country_code`  string,
  `ip`  string,
  `language`  string,
  `level` int,
  `mission_id` string,
  `helper_facebook_id` string,
  `resource_id` string,
  `resource_name` string,
  `resource_type` string,
  `resource_amount` int,
  `ab_test` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_accept_help'
TBLPROPERTIES('serialization.null.format'='');

---copy_fact_message_sent
CREATE TABLE `copy_fact_message_sent`(
  `id`  string,
  `app_id`  string,
  `app_version` string,
  `user_key`  string,
  `app_user_id` string,
  `date`  string,
  `ts`  timestamp,
  `session_id`  string,
  `os`  string,
  `os_version`  string,
  `device`  string,
  `country_code`  string,
  `ip`  string,
  `language`  string,
  `level` int,
  `message_type` string,
  `recipient_user_id` string,
  `resource_id` string,
  `resource_name` string,
  `resource_type` string,
  `resource_amount` int,
  `ab_test` string)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/fact_message_sent'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_iap_funnel (
    bi_version string,
    app_id string,
    ts_pretty string,
    event string,
    user_id string,
    session_id string,
    properties string,
    collections string
)
PARTITIONED BY (
  `app` string,
  `dt` string)
ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/fruitscoot_1_2/copy/iap_funnel'
TBLPROPERTIES('serialization.null.format'='');

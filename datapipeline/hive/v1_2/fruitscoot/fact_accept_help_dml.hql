use fruitscoot_1_2;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;



INSERT OVERWRITE TABLE fact_accept_help PARTITION (app,dt)
SELECT
    MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
    app_id,
    properties['app_version'] as app_version,
    MD5(concat(app_id, user_id)) as user_key,
    user_id as app_user_id,
    to_date(ts_pretty) as date,
    ts_pretty as ts,
    session_id,
    properties['os'] as os,
    properties['os_version'] as os_version,
    properties['device'] as device,
    properties['country_code'] as country_code,
    properties['ip'] as ip,
    properties['lang'] as language,
    properties['level'] as level,
    properties['mission_id'] as mission_id,
    properties['helper_facebook_id'] as helper_facebook_id,
    resource_id,
    resource_name,
    resource_type,
    resource_amount,
    ab_tests as ab_test,
    app,
    to_date(ts_pretty)
FROM
(
    SELECT
        app_id,
        user_id,
        app,
        properties,
        ts,
        ts_pretty,
        event,
        session_id,
        dt,
        rs.resource_id as resource_id,
        rs.resource_name as resource_name,
        rs.resource_type as resource_type,
        rs.resource_amount as resource_amount,
        to_json(collections.ab_tests) as ab_tests
    FROM 
        raw_accept_help_daily 
        lateral view explode(collections.resources_spent) coll1 AS rs
    WHERE
        dt > '${hiveconf:rpt_date_start}' and 
        dt <= '${hiveconf:rpt_date}' and 
        to_date(ts_pretty) > '${hiveconf:rpt_date_start}' and 
        to_date(ts_pretty) <= '${hiveconf:rpt_date}' and 
        app = app_id 
)t
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_accept_help DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

INSERT OVERWRITE TABLE copy_fact_accept_help PARTITION (app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
SELECT
    id,
    app_id,
    app_version,
    user_key,
    app_user_id,
    date,
    ts,
    session_id,
    os,
    os_version,
    device,
    country_code,
    ip,
    language,
    level,
    mission_id,
    helper_facebook_id,
    resource_id,
    resource_name,
    resource_type,
    resource_amount,
    ab_test
from fact_accept_help where dt > '${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

exit;

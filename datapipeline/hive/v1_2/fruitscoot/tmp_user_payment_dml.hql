
use fruitscoot_1_2;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

--SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


SET hive.exec.max.dynamic.partitions=15000;
SET hive.exec.max.dynamic.partitions.pernode=5000;

alter table tmp_user_payment drop partition (app='fruitscoot.global.prod');
alter table tmp_user_payment drop partition (app='fruitscoot.global.dev');

INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app, dt)
select distinct t.user_key,t.app_id,t.date,t.conversion_ts,t.revenue,t.purchase_cnt,t.app,t.dt 
from
(
select 
user_key,
app_id,
date,
min(ts) over (partition by user_key,app_id) conversion_ts,
sum(revenue_usd) over (partition by user_key,app_id,date) revenue,
count(ts) over (partition by user_key,app_id,date) purchase_cnt,
app,
dt
from 
fact_revenue
WHERE app = app_id
)t ;

alter table tmp_user_facebook_connect drop partition (app='fruitscoot.global.prod');
alter table tmp_user_facebook_connect drop partition (app='fruitscoot.global.dev');

INSERT OVERWRITE TABLE tmp_user_facebook_connect PARTITION (app, dt)
select distinct t.user_key,t.user_id,t.app_id,t.date,t.facebook_connect_ts,t.app,t.dt
from
(
select
MD5(concat(app_id,user_id)) as user_key,
user_id,
app_id,
to_date(ts_pretty) as date,
min(ts_pretty) over (partition by user_id,app_id) facebook_connect_ts,
app,
'${hiveconf:rpt_date}' as dt
from
raw_facebook_register_daily
WHERE  app='${hiveconf:rpt_app}' and app = app_id and to_date(ts_pretty)>='2015-03-01'
)t ;




exit;



SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=30;
SET hive.merge.mapredfiles=true;
SET rpt_dt='${wr_rpt_dt}';
SET rpt_par=${wr_rpt_par};
SET rpt_pre_par=${wr_rpt_pre_par};
SET rpt_yr=${wr_rpt_yr};
SET rpt_mnth=${wr_rpt_mnth};
SET rpt_day=${wr_rpt_day};

-- Change DB;
USE farm;
ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

-- for WATER_EXP_IN and WATER_EXP_GREENERY;
drop table if exists waterexpchange_temp;
create table waterexpchange_temp as
SELECT
 md5(concat(e.app,e.uid,e.snsid,e.ts)) as user_id,
 e.uid,
 e.snsid,
 e.app,
 e.ts,
 to_date(e.install_ts) as install_dt,
 a.install_source,
 a.os,
 a.os_version,
 a.browser,
 e.browser_version,
 a.country_code,
 get_json_object(properties,'$.action') as action,
 get_json_object(properties,'$.expansion_size_from') as exp_size_from,
 cast(case when get_json_object(properties,'$.greenery_in')='' then '0' else get_json_object(properties,'$.greenery_in') end as int) as greenery_in,
 cast(case when get_json_object(properties,'$.greenery_out')='' then '0' else get_json_object(properties,'$.greenery_out') end as int) as greenery_out,
 cast(case when get_json_object(properties,'$.greenery_bal')='' then '0' else get_json_object(properties,'$.greenery_bal') end as int) as greenery_bal,
 cast(case when get_json_object(properties,'$.level')='' then '0' else get_json_object(properties,'$.level') end as int) as level,
 cast(case when get_json_object(properties,'$.water_exp_in')='' then '0' else get_json_object(properties,'$.water_exp_in') end as int) as water_exp_in,
 cast(case when get_json_object(properties,'$.water_level')='' then '0' else get_json_object(properties,'$.water_level') end as int) as water_level,
 cast(case when get_json_object(properties,'$.water_exp')='' then '0' else get_json_object(properties,'$.water_exp') end as int) as water_exp_bal
FROM events_1day e left outer join farm.app_user a on (e.app=a.app and e.snsid=a.snsid and e.uid=a.uid)
where e.event = 'WaterExpChange'
and to_date(e.ts)=${hiveconf:rpt_dt};

drop table if exists farm.waterexpchange_daily_data;
create external table farm.waterexpchange_daily_data (
 user_id STRING,
 uid STRING,
 snsid STRING,
 app STRING,
 ts STRING,
 install_dt STRING,
 install_source STRING,
 os STRING,
 os_version STRING,
 browser STRING,
 browser_version STRING,
 country_code STRING,
 action STRING,
 exp_size_from STRING,
 greenery_in INT,
 greenery_out INT,
 greenery_bal INT,
 level INT,
 water_exp_in INT,
 water_level INT,
 water_exp_bal INT
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/wec_daily/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE waterexpchange_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/wec_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE waterexpchange_daily_data partition (date=${hiveconf:rpt_par}) select * from waterexpchange_temp;

-- Summary tables for WaterRanch Report;
drop table if exists farm.rpt_water_exp_in_temp;
create table farm.rpt_water_exp_in_temp as
 select
  app,
  to_date(ts) as dt,
  install_dt,
  install_source,
  os,
  os_version,
  browser,
  browser_version,
  country_code,
  action,
  exp_size_from,
  level,
  water_level,
  md5(concat(
   COALESCE(app,''),
   COALESCE(to_date(ts),''),
   COALESCE(install_dt,''),
   COALESCE(install_source,''),
   COALESCE(os,''),
   COALESCE(os_version,''),
   COALESCE(browser,''),
   COALESCE(browser_version,''),
   COALESCE(country_code,''),
   COALESCE(action,''), 
   COALESCE(exp_size_from,''),
   COALESCE(level,''),
   COALESCE(water_level,'') )) as id,
  count(distinct uid) as uid_cnt,
  sum(water_exp_in) as tot_exp_in
 from
  farm.waterexpchange_daily_data
 group by
  app,
  to_date(ts),
  install_dt,
  install_source,
  os,
  os_version,
  browser,
  browser_version,
  country_code,
  action,
  exp_size_from,
  level,
  water_level
;

drop table if exists farm.rpt_water_exp_in;
create external table farm.rpt_water_exp_in (
app             string,
dt              string,
install_dt      string,
install_source  string,
os              string,
os_version      string,
browser         string,
browser_version string,
country_code    string,
action          string,
exp_size_from   string,
level           int   ,
water_level     int   ,
id_col string,
uid_cnt int,
tot_exp_in      bigint
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/rpt_waterexp_in/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE rpt_water_exp_in add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/rpt_waterexp_in/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE rpt_water_exp_in partition (date=${hiveconf:rpt_par}) select * from rpt_water_exp_in_temp;

drop table if exists farm.rpt_water_greenery_temp;
create table farm.rpt_water_greenery_temp as
 select
  app,
  to_date(ts) as dt,
  install_dt,
  install_source,
  os,
  os_version,
  browser,
  browser_version,
  country_code,
  action,
  exp_size_from,
  level,
  water_level,
  md5(concat(
     COALESCE(app,''),
     COALESCE(to_date(ts),''),
     COALESCE(install_dt,''),
     COALESCE(install_source,''),
     COALESCE(os,''),
     COALESCE(os_version,''),
     COALESCE(browser,''),
     COALESCE(browser_version,''),
     COALESCE(country_code,''),
     COALESCE(action,''), 
     COALESCE(exp_size_from,''),
     COALESCE(level,''),
     COALESCE(water_level,'') )) as id,
 sum(greenery_in) as tot_greenery_in,
 sum(greenery_out) as tot_greenery_out
 from
  farm.waterexpchange_daily_data
 group by
  app,
  to_date(ts),
  install_dt,
  install_source,
  os,
  os_version,
  browser,
  browser_version,
  country_code,
  action,
  exp_size_from,
  level,
  water_level
;

drop table if exists farm.rpt_water_exp_greenery;
create external table farm.rpt_water_exp_greenery (
app             string,
dt              string,
install_dt      string,
install_source  string,
os              string,
os_version      string,
browser         string,
browser_version string,
country_code    string,
action          string,
exp_size_from   string,
level           int   ,
water_level     int   ,
id_col string,
tot_greenery_in      bigint,
tot_greenery_out      bigint
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/rpt_waterexp_greenery/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE rpt_water_exp_greenery add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/rpt_waterexp_greenery/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE rpt_water_exp_greenery partition (date=${hiveconf:rpt_par}) select * from rpt_water_greenery_temp;

-- For WaterRanch 3rd SCENE DATA;
drop table if exists wr_3rdscene_exp_temp;
create table wr_3rdscene_exp_temp as
SELECT
 md5(concat(e.app,e.uid,e.snsid,e.ts)) as user_id,
 e.uid,
 e.snsid,
 e.app,
 e.ts,
 to_date(e.install_ts) as install_dt,
 a.install_source,
 a.os,
 a.os_version,
 a.browser,
 e.browser_version,
 a.country_code,
 get_json_object(properties,'$.action') as action,
 get_json_object(properties,'$.expansion_size_from') as exp_size_from,
 cast(case when get_json_object(properties,'$.greenery_bal')='' then '0' else get_json_object(properties,'$.greenery_bal') end as int) as greenery_bal,
 cast(case when get_json_object(properties,'$.coins_bal')='' then '0' else get_json_object(properties,'$.coins_bal') end as int) as coins_bal,
 cast(case when get_json_object(properties,'$.rc_bal')='' then '0' else get_json_object(properties,'$.rc_bal') end as int) as rc_bal,
 cast(case when get_json_object(properties,'$.level')='' then '0' else get_json_object(properties,'$.level') end as int) as level,
 cast(case when get_json_object(properties,'$.waterLevel')='' then '0' else get_json_object(properties,'$.waterLevel') end as int) as waterLevel,
 cast(case when get_json_object(properties,'$.expansion_size_to')='' then '0' else get_json_object(properties,'$.expansion_size_to') end as int) as expansion_size_to,
 cast(case when get_json_object(properties,'$.expansion_top_size_to')='' then '0' else get_json_object(properties,'$.expansion_top_size_to') end as int) as expansion_top_size_to
FROM events_1day e left outer join farm.app_user a on (e.app=a.app and e.snsid=a.snsid and e.uid=a.uid)
where e.event = 'ThirdSceneExpand'
and to_date(e.ts)=${hiveconf:rpt_dt};

drop table if exists farm.wr_3rdscene_daily_data;
create external table farm.wr_3rdscene_daily_data (
 user_id STRING,
 uid STRING,
 snsid STRING,
 app STRING,
 ts STRING,
 install_dt STRING,
 install_source STRING,
 os STRING,
 os_version STRING,
 browser STRING,
 browser_version STRING,
 country_code STRING,
 action STRING,
 exp_size_from STRING,
 greenery_bal BIGINT,
 coins_bal BIGINT,
 rc_bal BIGINT,
 level INT,
 waterLevel INT,
 expansion_size_to INT,
 expansion_top_size_to INT
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/wr_3rdscene_daily/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE wr_3rdscene_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/wr_3rdscene_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE wr_3rdscene_daily_data partition (date=${hiveconf:rpt_par}) select * from wr_3rdscene_exp_temp;


--summary 3rd scene;
drop table if exists farm.rpt_wr_3rdscene_exp_temp;
create table farm.rpt_wr_3rdscene_exp_temp as
 select
  app,
  to_date(ts) as dt,
  install_dt,
  install_source,
  os,
  os_version,
  browser,
  browser_version,
  country_code,
  action,
  exp_size_from,
  level,
  waterLevel,
  expansion_size_to,
  expansion_top_size_to,
    md5(concat(
     COALESCE(app,''),
     COALESCE(to_date(ts),''),
     COALESCE(install_dt,''),
     COALESCE(install_source,''),
     COALESCE(os,''),
     COALESCE(os_version,''),
     COALESCE(browser,''),
     COALESCE(browser_version,''),
     COALESCE(country_code,''),
     COALESCE(action,''), 
     COALESCE(exp_size_from,''),
     COALESCE(level,''),
     COALESCE(waterLevel,''),
     COALESCE(expansion_size_to,''),
     COALESCE(expansion_top_size_to,'') )) as id,
  count(distinct (case when expansion_size_to IS NOT NULL then snsid end)) as expansion_size_to_cnt,
  count(distinct (case when expansion_top_size_to IS NOT NULL then snsid end)) as expansion_top_size_to_cnt
 from
  farm.wr_3rdscene_daily_data
 group by
  app,
  to_date(ts),
  install_dt,
  install_source,
  os,
  os_version,
  browser,
  browser_version,
  country_code,
  action,
  exp_size_from,
  level,
  waterLevel,
  expansion_size_to,
  expansion_top_size_to
;

drop table if exists farm.rpt_3rdscene_exp;
create external table farm.rpt_3rdscene_exp (
app             string,
dt              string,
install_dt      string,
install_source  string,
os              string,
os_version      string,
browser         string,
browser_version string,
country_code    string,
action          string,
exp_size_from   string,
level           int   ,
waterLevel     int   ,
expansion_size_to int,
expansion_top_size_to int,
id_col string,
expansion_size_to_cnt bigint,
expansion_top_size_to_cnt bigint
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/rpt_3rdscene_exp/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE rpt_3rdscene_exp add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/rpt_3rdscene_exp/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE rpt_3rdscene_exp partition (date=${hiveconf:rpt_par}) select * from farm.rpt_wr_3rdscene_exp_temp;


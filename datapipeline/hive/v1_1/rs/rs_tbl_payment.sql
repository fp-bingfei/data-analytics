DROP TABLE if exists tbl_payment_staging;

CREATE TABLE tbl_payment_staging
(
   snsid       varchar(100),
   item        varchar(1000),
   currency    varchar(100),
   payTime     varchar(20),
   tid         varchar(100),
   trackRef    varchar(1000),
   type    varchar(100),
   amount      varchar(100),
   gameamount  varchar(100),
   logTime   varchar(20),
   status      varchar(100)
);

truncate table tbl_payment;

--AE
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/rs/ae/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'ae',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  amount,
  gameamount,
  (TIMESTAMP 'epoch' + logTime::BIGINT *INTERVAL '1 Second'),
  status
  from tbl_payment_staging 
  where snsid is not null
;

--DE
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/rs/de/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'de',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  amount,
  gameamount,
  (TIMESTAMP 'epoch' + logTime::BIGINT *INTERVAL '1 Second'),
  status
  from tbl_payment_staging 
  where snsid is not null
;

--FR
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/rs/fr/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'fr',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  amount,
  gameamount,
  (TIMESTAMP 'epoch' + logTime::BIGINT *INTERVAL '1 Second'),
  status
  from tbl_payment_staging 
  where snsid is not null
;

--NL
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/rs/nl/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'nl',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  amount,
  gameamount,
  (TIMESTAMP 'epoch' + logTime::BIGINT *INTERVAL '1 Second'),
  status
  from tbl_payment_staging 
  where snsid is not null
;

--TH
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/rs/th/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'th',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  amount,
  gameamount,
  (TIMESTAMP 'epoch' + logTime::BIGINT *INTERVAL '1 Second'),
  status
  from tbl_payment_staging 
  where snsid is not null
;

--US
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/rs/us/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'us',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  amount,
  gameamount,
  (TIMESTAMP 'epoch' + logTime::BIGINT *INTERVAL '1 Second'),
  status
  from tbl_payment_staging 
  where snsid is not null
;

commit;

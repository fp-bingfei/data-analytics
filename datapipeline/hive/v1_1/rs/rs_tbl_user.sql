DROP TABLE if exists tbl_user_staging;

CREATE TABLE tbl_user_staging (
uid integer,
snsid varchar(100) ENCODE lzo,
install_source varchar(100) ENCODE lzo,
install_ts varchar(100) ENCODE LZO,
lang varchar(10) ENCODE lzo,
rc varchar(30) ENCODE lzo,
coins varchar(30) ENCODE lzo,
email varchar(100) ENCODE lzo,
gender varchar(20) ENCODE lzo,
name varchar(100) ENCODE lzo
);

truncate table tbl_user;

--AE
truncate table tbl_user_staging;

copy tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/rs/ae/mongodb/user/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_user
select 
  'royal.ae.prod',
  uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null
  from tbl_user_staging 
  where snsid is not null
;

--DE
truncate table tbl_user_staging;

copy tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/rs/de/mongodb/user/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_user
select 
  'royal.de.prod',
  uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null
  from tbl_user_staging 
  where snsid is not null
;

--FR
truncate table tbl_user_staging;

copy tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/rs/fr/mongodb/user/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_user
select 
  'royal.fr.prod',
  uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null
  from tbl_user_staging 
  where snsid is not null
;

--NL
truncate table tbl_user_staging;

copy tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/rs/nl/mongodb/user/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_user
select 
  'royal.nl.prod',
  uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null
  from tbl_user_staging 
  where snsid is not null
;

--TH
truncate table tbl_user_staging;

copy tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/rs/th/mongodb/user/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_user
select 
  'royal.th.prod',
  uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null
  from tbl_user_staging 
  where snsid is not null
;

--US
truncate table tbl_user_staging;

copy tbl_user_staging
FROM 's3://com.funplusgame.bidata/etl/rs/us/mongodb/user/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/rs_tbl_user_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_user
select 
  'royal.us.prod',
  uid,
  snsid,
  install_source,
  (TIMESTAMP 'epoch' + install_ts::BIGINT *INTERVAL '1 Second'),
  null,
  null,
  null,
  null,
  null,
  lang,
  rc,
  coins,
  email,
  gender,
  name,
  null
  from tbl_user_staging 
  where snsid is not null
;

commit;

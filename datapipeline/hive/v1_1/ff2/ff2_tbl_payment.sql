DROP TABLE if exists tbl_payment_staging;

CREATE TABLE tbl_payment_staging
(
   snsid       varchar(100),
   item        varchar(1000),
   currency    varchar(100),
   payTime     varchar(100),
   tid         varchar(100),
   trackRef    varchar(1000),
   type    varchar(100),
   amount      varchar(100),
   gameamount  varchar(100),
   logTime   varchar(100),
   status      varchar(100)
);

truncate table tbl_payment;

--TH
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/ff2pc/th/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/ff2_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'th',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  case
      when amount like '%numberLong%' then json_extract_path_text(amount, '$numberLong')
      else amount
  end as amount,
  case
      when gameamount like '%numberLong%' then json_extract_path_text(gameamount, '$numberLong')
      else gameamount
  end as gameamount,
  (TIMESTAMP 'epoch' + (case
      when logTime like '%numberLong%' then json_extract_path_text(logTime, '$numberLong')
      else logTime
  end)::BIGINT *INTERVAL '1 Second'),
  case
      when status like '%numberLong%' then json_extract_path_text(status, '$numberLong')
      else status
  end as status
from tbl_payment_staging 
where snsid is not null
;

--US
truncate table tbl_payment_staging;

copy tbl_payment_staging
FROM 's3://com.funplusgame.bidata/etl/ff2pc/global/mongodb/payment/${RPT_DATE_PLUS1_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
json 's3://com.funplusgame.bidata/dev/jsonpaths/ff2_tbl_payment_staging.json' 
gzip MAXERROR 5000 TRUNCATECOLUMNS EMPTYASNULL BLANKSASNULL;

INSERT INTO tbl_payment
select 
  'us',
  snsid,
  item,
  currency,
  (TIMESTAMP 'epoch' + payTime::BIGINT *INTERVAL '1 Second'),
  tid,
  trackRef,
  type,
  case
      when amount like '%numberLong%' then json_extract_path_text(amount, '$numberLong')
      else amount
  end as amount,
  case
      when gameamount like '%numberLong%' then json_extract_path_text(gameamount, '$numberLong')
      else gameamount
  end as gameamount,
  (TIMESTAMP 'epoch' + (case
      when logTime like '%numberLong%' then json_extract_path_text(logTime, '$numberLong')
      else logTime
  end)::BIGINT *INTERVAL '1 Second'),
  case
      when status like '%numberLong%' then json_extract_path_text(status, '$numberLong')
      else status
  end as status
from tbl_payment_staging 
where snsid is not null
;

commit;

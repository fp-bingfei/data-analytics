-----------------------------
-- Query to generate fact_session
-----------------------------

use bv_1_1;

--ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
--create temporary function MD5 as 'com.funplus.analytics.mr.udfs.Md5';

MSCK REPAIR TABLE raw_newuser;
MSCK REPAIR TABLE raw_session_start;

-- Populate fact_session
INSERT OVERWRITE TABLE fact_session PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=${hiveconf:rpt_hour}) 
SELECT
MD5(concat(app,uid,ts)) as id,
to_date(ts_pretty) as date_start,
MD5(concat(app,uid)) as user_key,
uid,
snsid,
install_ts_pretty as install_ts,
to_date(install_ts_pretty) as install_date,
install_source, 
null as app_version, 
null as session_id, 
ts_pretty as ts_start, 
ts_pretty as ts_end, 
COALESCE(level, 0) as level_start, 
null as level_end, 
case when LOWER(SUBSTR(os, 0, 3)) = 'ios' then 'iOS'
when LOWER(SUBSTR(os,0, 7)) = 'android' then 'Android'
when os='' and SUBSTR(device, 0, 2) = 'iP' then 'iOS'
else 'Unknown'
end as os,
os_version,
COALESCE(country_code, '--'),
ip, 
lang AS language, 
device,   
null as browser, 
null as browser_version,
null as rc_bal_start, 
null as rc_bal_end,
null as coin_bal_start,
null as coin_bal_end,
null as ab_test,
null as ab_variant,
null as session_length 
FROM   raw_newuser 
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and hour = ${hiveconf:rpt_hour}
UNION ALL
SELECT
MD5(concat(app,uid,ts)) as id,
to_date(ts_pretty) as date_start,
MD5(concat(app,uid)) as user_key,
uid,
snsid,
install_ts_pretty as install_ts,
to_date(install_ts_pretty) as install_date,
install_source, 
null as app_version, 
null as session_id, 
ts_pretty as ts_start, 
ts_pretty as ts_end, 
COALESCE(level, 0) as level_start,
null as level_end, 
case when LOWER(SUBSTR(os, 0, 3)) = 'ios' then 'iOS'
when LOWER(SUBSTR(os,0, 7)) = 'android' then 'Android'
when os='' and SUBSTR(device, 0, 2) = 'iP' then 'iOS'
else 'Unknown'
end as os,
os_version,
COALESCE(country_code, '--'),
ip, 
lang AS language, 
device,   
null as browser, 
null as browser_version, 
null as rc_bal_start, 
null as rc_bal_end, 
null as coin_bal_start, 
null as coin_bal_end, 
null as ab_test,
null as ab_variant,
null as session_length 
FROM  raw_session_start
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and hour = ${hiveconf:rpt_hour};

exit;

use bv_1_1;

--fact_dau_snapshot

CREATE EXTERNAL TABLE IF NOT EXISTS fact_dau_snapshot
(
  id   STRING,
  date  DATE,
  user_key STRING,
  uid STRING,
  snsid STRING,
  app_name STRING,
  app_version STRING,
  install_ts TIMESTAMP ,
  install_date DATE,
  level_start INT,
  level_end INT,
  device   STRING,
  device_alias STRING,
  browser STRING,
  browser_version STRING,
  country_code      STRING,
  country  STRING,
  os  STRING,
  os_version  STRING,
  language  STRING,
  ab_test  STRING,
  ab_variant STRING,
  is_new_user SMALLINT ,
  coins_in INT,
  rc_in    INT,
  is_payer SMALLINT ,
  is_converted_today SMALLINT ,
  revenue_usd   DECIMAL(14,4),
  purchase_cnt    INT,
  session_cnt INT,
  last_login_ts TIMESTAMP,
  playtime_sec INT
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_dau_snapshot'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_dau_snapshot
(
  date  DATE,
  user_key STRING,
  uid STRING,
  snsid STRING,
  app_version STRING,
  install_ts TIMESTAMP ,
  install_date DATE,
  level_start INT,
  level_end INT,
  device   STRING,
  device_alias STRING,
  browser STRING,
  browser_version STRING,
  country_code      STRING,
  country  STRING,
  os  STRING,
  os_version  STRING,
  language  STRING,
  ab_test  STRING,
  ab_variant STRING,
  is_new_user SMALLINT ,
  coins_in INT,
  rc_in    INT,
  is_payer SMALLINT ,
  is_converted_today SMALLINT ,
  revenue_usd   DECIMAL(14,4),
  purchase_cnt    INT,
  session_cnt INT,
  last_login_ts TIMESTAMP,
  playtime_sec INT
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT,
step  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');
--LOCATION 's3://com.funplus.datawarehouse/bv_1_1/dim_user';


CREATE TABLE IF NOT EXISTS tmp_dau_user_daily_login
(
date_start  DATE,
user_key  STRING,
uid STRING,
snsid STRING,
install_ts TIMESTAMP,
install_date DATE,
app_version STRING,
level_start INT,
level_end INT,
os STRING,
os_version STRING,
country_code STRING,
country STRING,
language STRING,
device STRING,
browser STRING,
browser_version STRING,
ab_test STRING,
ab_variant STRING
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS temp_dau_user_payment_first
(
user_key STRING,
conversion_ts TIMESTAMP,
usd  DECIMAL(14,4)
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS temp_dau_last_login_ts
(user_key STRING,
last_login_ts TIMESTAMP
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_dau_user_session_cnt
(
date_start  DATE,
user_key STRING,
session_cnt BIGINT
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_dau_user_daily_payment
(
date  DATE,
user_key STRING,
level INT,
coins_in  BIGINT,
rc_in   BIGINT,
usd  DECIMAL(14,4),
purchase_cnt  BIGINT,
os  STRING,
country_code  STRING
)
PARTITIONED BY (
app  STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_dau_user_level
(
date  DATE,
user_key  STRING,
level_end INT
)
PARTITIONED BY (
app  STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

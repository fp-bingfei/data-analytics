-----------------------------
-- Query to generate fact_revenue
-----------------------------

use bv_1_1;

--ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
--create temporary function MD5 as 'com.funplus.analytics.mr.udfs.Md5';

MSCK REPAIR TABLE raw_payment;

-- Populate fact_revenue
INSERT OVERWRITE TABLE fact_revenue PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=${hiveconf:rpt_hour})
SELECT
MD5(concat(app,uid,ts)) as id,
to_date(ts_pretty) as date,
MD5(concat(app,uid)) as user_key,
uid,
snsid,                          
ts_pretty as ts,
COALESCE(level,0) as level,
case when LOWER(SUBSTR(os, 0, 3)) = 'ios' then 'iOS'
when LOWER(SUBSTR(os, 0, 7)) = 'android' then 'Android'
when os='' and SUBSTR(device, 1, 2) = 'iP' then 'iOS'
else 'Unknown'
end as os,
COALESCE(country_code, '--'),
payment_processor, 
product_id,        
product_name,
product_type,     
coins_in,
rc_in,
currency,
amount/100 AS amount,
amount/100 AS usd,
transaction_id
from raw_payment
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and hour = ${hiveconf:rpt_hour};

exit;


use tmp_bv_1_1;

SET hive.exec.compress.intermediate=true;
SET mapred.map.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

--ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
--create temporary function MD5 as 'com.funplus.analytics.mr.udfs.Md5';
	

------------------------------------------------------------------------------------------------------------------------
--Step 1. Get the users whose install source should be updated FROM fact_user_install_source full data
------------------------------------------------------------------------------------------------------------------------

-- INSERT OVERWRITE TABLE tmp_fact_user_install_source partition(app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="1") 
-- SELECT 
-- t1.user_key, t1.uid, t1.snsid, t1.install_date, null, null, null, null, null, 'Organic', null,null
-- FROM
-- (
-- SELECT *
-- FROM 
-- (SELECT user_key, uid, snsid, install_date, row_number() over (partition by user_key, app, uid, snsid, install_date) as rank FROM bv_1_1.fact_session WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate})f WHERE f.rank=1)t;


-- filter users whose install source have been updated FROM the past fact_user_install_source

INSERT OVERWRITE TABLE tmp_fact_user_install_source partition (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="1")SELECT 
t1.user_key, t1.uid, t1.snsid, t1.install_date, null, null, null, null, null, 'Organic', null,null
FROM  
(SELECT user_key, uid, snsid, install_date, row_number() over (partition by user_key, app, uid, snsid, install_date) as rank FROM bv_1_1.fact_session WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate})f WHERE f.rank=1)t1 
LEFT OUTER JOIN 
(SELECT * FROM bv_1_1.fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year_yesterday} AND month=${hiveconf:rpt_month_yesterday} AND day =${hiveconf:rpt_day_yesterday})t2
on t1.app = t2.app and t1.uid = t2.uid and t1.snsid = t2.snsid and t1.install_date = t2.install_date
where t2.app is null;

------------------------------------------------------------------------------------------------------------------------
--Step 2. Get first no 'Organic' install source FROM adjust full data
------------------------------------------------------------------------------------------------------------------------	
			  
-- Full Data of Adjust

-- INSERT OVERWRITE TABLE tmp_user_install_source PARTITION(app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day})
-- SELECT userid as snsid,
-- first_value(tracker_name, true) over (partition by userid order by ts rows between unbounded preceding and unbounded following) as install_source
-- FROM (select * from bv_1_1.adjust where app="${hiveconf:rpt_app}")t
-- where lower(tracker_name) != 'organic';

--Replaced update with Left Outer Join
INSERT OVERWRITE TABLE tmp_fact_user_install_source partition (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="2") 
SELECT 
user_key,uid,t1.snsid,install_date,install_source_event_raw,install_source_event,
t2.install_source as install_source_adjust_raw,t2.install_source as install_source_adjust, 
install_source_raw, t1.install_source,campaign,sub_publisher
FROM (SELECT * FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="1")t1 
LEFT OUTER JOIN 
(SELECT userid AS snsid,
first_value(tracker_name, true) over (partition by userid order by ts rows between unbounded preceding and unbounded following) as install_source
FROM bv_1_1.adjust WHERE app="${hiveconf:rpt_app}" AND lower(tracker_name) != 'organic')t2
ON  t1.snsid = t2.snsid;

------------------------------------------------------------------------------------------------------------------------
--Step 3. Get first no 'Organic' install source FROM event data.
--For history data, we can only get install source FROM event data
------------------------------------------------------------------------------------------------------------------------


-- INSERT OVERWRITE TABLE tmp_user_event_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day})
-- SELECT 
-- uid, snsid, first_value(install_source,true) over (partition by app, uid, snsid order by ts_start rows between unbounded preceding and unbounded following) as install_source
-- FROM
-- (SELECT * FROM bv_1_1.fact_session WHERE  app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate} AND lower(install_source) != 'organic')t;


	  
INSERT OVERWRITE TABLE tmp_fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="3") 
SELECT 
user_key,t1.uid,t1.snsid,install_date,t2.install_source as install_source_event_raw,t2.install_source as install_source_event,
install_source_adjust_raw, install_source_adjust, 
install_source_raw, t1.install_source, campaign, sub_publisher
FROM (SELECT * FROM tmp_fact_user_install_source  WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="2")t1 
LEFT OUTER JOIN 
(
SELECT	  
uid, snsid, first_value(install_source,true) over (partition by app, uid, snsid order by ts_start rows between unbounded preceding and unbounded following) as install_source
FROM bv_1_1.fact_session WHERE  app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate} AND lower(install_source) != 'organic'
) t2
ON t1.uid = t2.uid and t1.snsid = t2.snsid;
	  
	  
------------------------------------------------------------------------------------------------------------------------
--Step 4. Normalize install_source_event and install_source_adjust FROM adjust and event data
------------------------------------------------------------------------------------------------------------------------

INSERT OVERWRITE TABLE tmp_fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="4") 
SELECT user_key, uid, snsid, install_date, install_source_event_raw, 
CASE WHEN install_source_custom = 'organic'
THEN 'Organic'
ELSE install_source_custom
END
as install_source_event,
install_source_adjust_raw, 
split(install_source_adjust_raw, '::')[0] as install_source_adjust, 
install_source_raw,install_source,campaign,sub_publisher
FROM (SELECT t1.*, lower(regexp_replace(split(install_source_event_raw, '::')[0], '\"', '')) install_source_custom FROM tmp_fact_user_install_source t1 WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} and step="3")t2;

------------------------------------------------------------------------------------------------------------------------
--Step 5. Update install_source, install_source_adjust have higher priority
------------------------------------------------------------------------------------------------------------------------	  	   	  

INSERT OVERWRITE TABLE tmp_fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="5") 
SELECT user_key, uid, snsid, install_date, install_source_event_raw, 
install_source_event,install_source_adjust_raw, install_source_adjust, 
CASE 
WHEN install_source = 'Organic' AND install_source_adjust IS NOT NULL
THEN install_source_adjust_raw
WHEN install_source = 'Organic' AND install_source_event IS NOT NULL
THEN install_source_event_raw
ELSE
install_source_raw
END
as install_source_raw,
CASE 
WHEN install_source = 'Organic' AND install_source_adjust IS NOT NULL
THEN install_source_adjust
WHEN install_source = 'Organic' AND install_source_event IS NOT NULL
THEN install_source_event
ELSE
install_source
END
as install_source,
campaign, sub_publisher
FROM (SELECT * FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="4")t;

------------------------------------------------------------------------------------------------------------------------
--Step 6. Normalize install_source,
-- Such as mapping both ‘Adperio’ and ‘AdPerio’ to ‘AdPerio’, mapping both 'App Turbo’ and 'App+Turbo’ to 'App Turbo’
------------------------------------------------------------------------------------------------------------------------

INSERT OVERWRITE TABLE tmp_ref_install_source_map PARTITION (app="${hiveconf:rpt_app}", year=${hiveconf:rpt_year}, month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="1") 
SELECT install_source as install_source_raw, lower(install_source) as install_source_lower, install_source
FROM (SELECT distinct install_source FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="5")t;		

INSERT OVERWRITE TABLE tmp_ref_install_source_map PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="2")
SELECT install_source_raw,
regexp_replace(install_source_lower, "\\+|-| ", ""),
install_source FROM
(SELECT * FROM tmp_ref_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="1")t;

INSERT OVERWRITE TABLE tmp_ref_install_source_map PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="3")
SELECT DISTINCT install_source_raw, install_source_lower, install_source FROM
(SELECT install_source_raw,install_source_lower,install_source FROM tmp_ref_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month} and day=${hiveconf:rpt_day} and step="2"
UNION ALL 
SELECT install_source_raw,install_source_lower,install_source FROM bv_1_1.ref_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year_yesterday} AND month=${hiveconf:rpt_month_yesterday} AND day =${hiveconf:rpt_day_yesterday})t;

		
INSERT OVERWRITE TABLE tmp_install_source_map PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day})
SELECT install_source_lower, install_source FROM
(
SELECT install_source_lower, install_source, row_number () over (partition by install_source_lower order by install_source) as rank
FROM tmp_ref_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month} and day=${hiveconf:rpt_day} and step="3")t
where t.rank = 1;

INSERT OVERWRITE TABLE tmp_ref_install_source_map PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="4")
SELECT t1.install_source_lower, t1.install_source_lower, COALESCE(t2.install_source, t1.install_source) install_source
FROM (SELECT * FROM tmp_ref_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="3")t1 
LEFT OUTER JOIN
(SELECT * FROM tmp_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day})t2
on t1.install_source_lower = t2.install_source_lower;
	
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;	
	
INSERT OVERWRITE TABLE bv_1_1.ref_install_source_map PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day})
SELECT distinct install_source_raw, install_source_lower, install_source FROM tmp_ref_install_source_map 
where app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} and month = ${hiveconf:rpt_month} and day = ${hiveconf:rpt_day} and step="4";

SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec;

INSERT OVERWRITE TABLE tmp_fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="6") 
SELECT 
user_key,uid,snsid,install_date,install_source_event_raw,install_source_event,install_source_adjust_raw,
install_source_adjust,t1.install_source_raw, COALESCE(t2.install_source, t1.install_source) install_source, campaign, sub_publisher 
FROM 
(SELECT * FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="5")t1 
LEFT OUTER JOIN 
(SELECT * FROM bv_1_1.ref_install_source_map WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year_yesterday} AND month=${hiveconf:rpt_month_yesterday} AND day =${hiveconf:rpt_day_yesterday})t2
on t1.install_source = t2.install_source_raw;

-- update campaign and update sub_publisher	
INSERT OVERWRITE TABLE tmp_fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="7") 
SELECT user_key, uid, snsid, install_date, install_source_event_raw, 
install_source_event, install_source_adjust_raw, install_source_adjust, install_source_raw, install_source,
split(install_source_raw, '::')[1] as campaign,
CASE WHEN install_source not in ('Google AdWords', 'Google+AdWords+Mobile')
THEN split(install_source_raw, '::')[2]
WHEN install_source in ('Google AdWords', 'Google+AdWords+Mobile')
THEN split(install_source_raw, '::')[3]
ELSE
sub_publisher
END
as sub_publisher
FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="6";	


INSERT OVERWRITE TABLE tmp_fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day}, step="last") 
SELECT distinct user_key, uid, snsid, install_date, install_source_event_raw, 
install_source_event, install_source_adjust_raw, install_source_adjust, install_source_raw, install_source,
campaign, sub_publisher
FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="7" 
AND install_source not in ('Organic');

SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


INSERT OVERWRITE TABLE bv_1_1.fact_user_install_source PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day})
SELECT 
user_key id,user_key,app app_name,uid,snsid,install_date,install_source_event_raw,install_source_event,install_source_adjust_raw,
install_source_adjust,install_source_raw,install_source,campaign,sub_publisher
FROM tmp_fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="last" 
UNION ALL
SELECT 
id,user_key,app_name,uid,snsid,install_date,install_source_event_raw,install_source_event,install_source_adjust_raw,
install_source_adjust,install_source_raw,install_source,campaign,sub_publisher 
FROM bv_1_1.fact_user_install_source WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year_yesterday} AND month=${hiveconf:rpt_month_yesterday} AND day =${hiveconf:rpt_day_yesterday};

-------------------------
-- Drop Partitions 
-------------------------
ALTER TABLE tmp_fact_user_install_source DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_ref_install_source_map DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_install_source_map DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});

exit; 

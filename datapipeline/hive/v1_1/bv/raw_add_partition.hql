
use bv_1_1;

msck repair table raw_custom;
msck repair table raw_hat_count;
msck repair table raw_invalid;
msck repair table raw_item_transaction;
msck repair table raw_level_up;
msck repair table raw_newbie_package_popup;
msck repair table raw_newuser;
msck repair table raw_npc_order;
msck repair table raw_payment;
msck repair table raw_promote;
msck repair table raw_quest_complete;
msck repair table raw_rc_transaction;
msck repair table raw_reject_neighbor;
msck repair table raw_request_neighbor;
msck repair table raw_sell_most_wanted;
msck repair table raw_session_start;
msck repair table raw_set_most_wanted;
msck repair table raw_tutorial;
msck repair table raw_water_pumpkin;

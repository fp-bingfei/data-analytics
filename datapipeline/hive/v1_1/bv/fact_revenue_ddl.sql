use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS fact_revenue
(
  id  STRING,	
  date  DATE,
  user_key  STRING,
  uid  STRING,
  snsid  STRING,
  ts  TIMESTAMP,
  level  SMALLINT,
  os  STRING,
  country_code STRING,
  payment_processor STRING,
  product_id  STRING,
  product_name  STRING,
  product_type  STRING,
  coins_in  INT,
  rc_in  INT,
  currency  STRING,
  amount  DECIMAL(14,4),
  usd  DECIMAL(14,4),
  transaction_id  STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT,
hour INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_revenue'
TBLPROPERTIES('serialization.null.format'='');

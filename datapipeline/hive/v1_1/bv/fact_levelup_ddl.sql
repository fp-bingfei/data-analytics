use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS fact_level_up 
(
id  STRING,
user_key  STRING,
app_name  STRING,
uid  STRING,
snsid  STRING,
level  SMALLINT,
date_start DATE,
date_end DATE,
ts_start TIMESTAMP,
ts_end TIMESTAMP
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_level_up'
TBLPROPERTIES('serialization.null.format'='');


--------------------------------------------------------------
Fact Temp Tables
--------------------------------------------------------------


CREATE TABLE IF NOT EXISTS tmp_fact_level_up
(
user_key  STRING,
uid  STRING,
snsid  STRING,
level  SMALLINT,
date_start DATE,
date_end DATE,
ts_start TIMESTAMP,
ts_end TIMESTAMP
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_next_level
(
user_key   STRING,
level   SMALLINT,
ts_start  TIMESTAMP,
date_start  DATE
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

-- Copy Table to Redshift
CREATE TABLE IF NOT EXISTS copy_level_up 
(
id  STRING,
user_key  STRING,
app_name  STRING,
uid  STRING,
snsid  STRING,
level  SMALLINT,
date_start DATE,
date_end DATE,
ts_start TIMESTAMP,
ts_end TIMESTAMP
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

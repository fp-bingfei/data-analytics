use bv_1_1;


-- ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
-- create temporary function MD5 as 'com.funplus.analytics.mr.udfs.Md5';

INSERT OVERWRITE TABLE agg_kpi PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT  
MD5(concat(d.date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today,  'DAU')) as id,
d.date,
d.app,
d.app_version,
d.install_date,
u.install_source,
u.subpublisher_source,
u.campaign_source,
d.level_end,
d.device,
d.device_alias,
d.browser,
d.browser_version,
d.country_code,
d.country,
d.os,
d.os_version,
d.language,
d.ab_test,
d.ab_variant,
d.is_new_user,
d.is_payer,
d.is_converted_today,  
'DAU' as metric_name,
count(d.user_key) as metric_value,
0 as metric_freq
FROM (SELECT * FROM fact_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} )d
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on d.user_key = u.user_key
GROUP BY date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,
d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today
UNION ALL
SELECT  
MD5(concat(d.date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today,  'Revenue')) as id,
date,
d.app,
d.app_version,
d.install_date,
u.install_source,
u.subpublisher_source,
u.campaign_source,
d.level_end,
d.device,
d.device_alias,
d.browser,
d.browser_version,
d.country_code,
d.country,
d.os,
d.os_version,
d.language,
d.ab_test,
d.ab_variant,
d.is_new_user,
d.is_payer,
d.is_converted_today,  
'Revenue' as metric_name,
sum(revenue_usd) as metric_value,
0 as metric_freq
FROM (SELECT * FROM fact_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) d
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on d.user_key = u.user_key
GROUP BY date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,
d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today
UNION ALL
SELECT  
MD5(concat(d.date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today,  'Session')) as id,
date,
d.app,
d.app_version,
d.install_date,
u.install_source,
u.subpublisher_source,
u.campaign_source,
d.level_end,
d.device,
d.device_alias,
d.browser,
d.browser_version,
d.country_code,
d.country,
d.os,
d.os_version,
d.language,
d.ab_test,
d.ab_variant,
d.is_new_user,
d.is_payer,
d.is_converted_today,
'Session' as metric_name,
0 as metric_value,
sum(session_cnt) as metric_freq
FROM (SELECT * FROM fact_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) d
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on d.user_key = u.user_key
GROUP BY date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,
d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today
UNION ALL
SELECT  
MD5(concat(d.date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today,  'New Installs')) as id,
date,
d.app,
d.app_version,
d.install_date,
u.install_source,
u.subpublisher_source,
u.campaign_source,
d.level_end,
d.device,
d.device_alias,
d.browser,
d.browser_version,
d.country_code,
d.country,
d.os,
d.os_version,
d.language,
d.ab_test,
d.ab_variant,
d.is_new_user,
d.is_payer,
d.is_converted_today, 
'New Installs' as metric_name,
sum(is_new_user) as metric_value,
0 as metric_freq
FROM (SELECT * FROM fact_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) d
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on d.user_key = u.user_key
group by date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,
d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today
UNION ALL
SELECT  
MD5(concat(d.date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today,  'New Payers')) as id,
date,
d.app,
d.app_version,
d.install_date,
u.install_source,
u.subpublisher_source,
u.campaign_source,
d.level_end,
d.device,
d.device_alias,
d.browser,
d.browser_version,
d.country_code,
d.country,
d.os,
d.os_version,
d.language,
d.ab_test,
d.ab_variant,
d.is_new_user,
d.is_payer,
d.is_converted_today, 
'New Payers' as metric_name,
sum(is_converted_today) as metric_value,
0 as metric_freq
FROM (SELECT * FROM fact_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) d
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on d.user_key = u.user_key
group by date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,
d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today
UNION ALL
SELECT 
MD5(concat(d.date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today,  'Payers')) as id,
date,
d.app,
d.app_version,
d.install_date,
u.install_source,
u.subpublisher_source,
u.campaign_source,
d.level_end,
d.device,
d.device_alias,
d.browser,
d.browser_version,
d.country_code,
d.country,
d.os,
d.os_version,
d.language,
d.ab_test,
d.ab_variant,
d.is_new_user,
d.is_payer,
d.is_converted_today,  
'Payers' as metric_name,
sum(case when revenue_usd>0 then 1 else 0 end) as metric_value,
0 as metric_freq
FROM (SELECT * FROM fact_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) d
LEFT OUTER JOIN (SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on d.user_key = u.user_key
group by date,d.app,d.app_version,d.install_date,u.install_source,u.subpublisher_source,u.campaign_source,d.level_end,d.device,d.device_alias,d.browser,d.browser_version,d.country_code,d.country,
d.os,d.os_version,d.language,d.ab_test,d.ab_variant,d.is_new_user,d.is_payer,d.is_converted_today;

exit;
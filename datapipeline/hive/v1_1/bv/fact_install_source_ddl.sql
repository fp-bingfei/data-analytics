use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS fact_user_install_source
(
  id   STRING,
  user_key          STRING,
  app_name          STRING,
  uid               INT,
  snsid             STRING,
  install_date      DATE,
  install_source_event_raw  STRING,
  install_source_event  STRING,
  install_source_adjust_raw STRING,
  install_source_adjust STRING,
  install_source_raw    STRING,
  install_source    STRING,
  campaign          STRING,
  sub_publisher     STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_user_install_source'
TBLPROPERTIES('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS ref_install_source_map(
install_source_raw STRING,
install_source_lower STRING,
install_source STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/ref_install_source_map'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_fact_user_install_source
(
  user_key          STRING,
  uid               INT,
  snsid             STRING,
  install_date      DATE,
  install_source_event_raw  STRING,
  install_source_event  STRING,
  install_source_adjust_raw STRING,
  install_source_adjust STRING,
  install_source_raw    STRING,
  install_source    STRING,
  campaign          STRING,
  sub_publisher     STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT,
STEP STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_user_install_source(
snsid  STRING,
install_source STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_user_event_install_source(
uid STRING,
snsid STRING,
install_source STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS tmp_ref_install_source_map(
install_source_raw STRING,	
install_source_lower STRING,
install_source STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT,
STEP STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_install_source_map
(
install_source_lower STRING,
install_source STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');

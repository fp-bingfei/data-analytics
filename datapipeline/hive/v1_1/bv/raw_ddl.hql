
**********************
DDL
**********************

use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_level_up (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
from_level STRING,   
rewards ARRAY<STRUCT<item_in:STRING, item_name:STRING, item_id:STRING>>   
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/level_up';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_newuser (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/newuser';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_start (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/session_start';



CREATE EXTERNAL TABLE IF NOT EXISTS raw_water_pumpkin (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
water_used  INT,
rc_in  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/water_pumpkin';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_tutorial (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
step  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/tutorial';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_set_most_wanted (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
num   STRING,
price  STRING,
item  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/set_most_wanted';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_sell_most_wanted (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
num   STRING,
price  STRING,
item  STRING,
friend_snsid  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/sell_most_wanted';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_request_neighbor (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
friend  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/request_neighbor';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_reject_neighbor (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
friend  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/reject_neighbor';



CREATE EXTERNAL TABLE IF NOT EXISTS raw_rc_transaction (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
rc_out  INT,
rc_bal  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/rc_transaction';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_quest_complete (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
quest_id  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/quest_complete';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_promote (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
rc  INT,
items ARRAY<STRUCT<store_level:INT, price:STRING, count:STRING, itemid:STRING>>
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/promote';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_payment (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
promo_id  STRING,
store_id  STRING,
product_id  STRING,
product_name  STRING,
product_type  STRING,
currency  STRING,
amount  INT,
device_type  STRING,
is_promo  INT,
payment_processor  STRING,
transaction_id  STRING,
coins_in  INT,
rc_in  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/payment';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_npc_order (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
npc   STRING,
xp  STRING,
time  STRING,
coins  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/npc_order';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_newbie_package_popup (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
pack_id   STRING,
store_id  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/newbie_package_popup';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_item_transaction (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
item_id   STRING,
item_name   STRING,
item_type   STRING,
item_class  STRING,
item_in  STRING,
item_out  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/item_transaction';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_invalid (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
error  STRING,
errorDetails   ARRAY<STRING>
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/invalid';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_hat_count (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
hat_count  INT,
hat_array   ARRAY<INT>
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/hat_count';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_custom (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
remain_time  BIGINT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/custom';


**********************
DDL
**********************

use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_level_up (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
from_level STRING,   
rewards ARRAY<STRUCT<item_in:STRING, item_name:STRING, item_id:STRING>>   
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/level_up';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_newuser (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/newuser';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_start (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/session_start';



CREATE EXTERNAL TABLE IF NOT EXISTS raw_water_pumpkin (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
water_used  INT,
rc_in  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/water_pumpkin';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_tutorial (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
step  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/tutorial';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_set_most_wanted (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
num   STRING,
price  STRING,
item  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/set_most_wanted';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_sell_most_wanted (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
num   STRING,
price  STRING,
item  STRING,
friend_snsid  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/sell_most_wanted';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_request_neighbor (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
friend  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/request_neighbor';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_reject_neighbor (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
friend  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/reject_neighbor';



CREATE EXTERNAL TABLE IF NOT EXISTS raw_rc_transaction (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  BIGINT,
rc_in  BIGINT,
rc_out  BIGINT,
rc_bal  BIGINT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/rc_transaction';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_quest_complete (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
quest_id  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/quest_complete';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_promote (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
rc  INT,
items ARRAY<STRUCT<store_level:INT, price:STRING, count:STRING, itemid:STRING>>
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/promote';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_payment (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
promo_id  STRING,
store_id  STRING,
product_id  STRING,
product_name  STRING,
product_type  STRING,
currency  STRING,
amount  INT,
device_type  STRING,
is_promo  INT,
payment_processor  STRING,
transaction_id  STRING,
coins_in  INT,
rc_in  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/payment';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_npc_order (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
npc   STRING,
xp  STRING,
time  STRING,
coins  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/npc_order';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_newbie_package_popup (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
pack_id   STRING,
store_id  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/newbie_package_popup';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_item_transaction (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  BIGINT,
item_id   STRING,
item_name   STRING,
item_type   STRING,
item_class  STRING,
item_in  STRING,
item_out  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/item_transaction';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_invalid (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
error  STRING,
errorDetails   ARRAY<STRING>
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/invalid';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_hat_count (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
hat_count  INT,
hat_array   ARRAY<INT>
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/hat_count';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_custom (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
remain_time  BIGINT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/custom';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_merchant_success (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
remain_time  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/merchant_success';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_merchant_fail (
uid STRING, 
key STRING,
event  STRING,
snsid  STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
value  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/merchant_fail';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_merchant_exit (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
value  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/merchant_dismiss';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_merchant_dismiss (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
remain_time  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/merchant_dismiss';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_marketstand_buy (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
sold  INT,
num  STRING,
price  STRING,
tax INT,
item  STRING,
friend  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/marketstand_buy';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_market_sell (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
time  STRING,
num  STRING,
price  STRING,
item  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/market_sell';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_market_buy_npc (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
num	STRING,
price  STRING,
item  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/market_buy_npc';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_market_buy (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
num STRING,
price  STRING,
tax  INT,
item  STRING,
friend  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/market_buy';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_county_fair (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
county_fair_points	INT,
point_value	INT,
speed_fertilizer  INT,
item_name  STRING,
item_id  STRING,
double_fertilizer  INT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/county_fair';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_coins_transaction (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  BIGINT,
coins_in  BIGINT,
coins_out  BIGINT,
coins_bal  BIGINT
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/coins_transaction';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_board_order (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
location STRING,
lang   STRING,
action   STRING,
action_detail  STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
order  STRUCT<reward:STRUCT<coins:STRING, experience:STRING>, tasks:ARRAY<STRUCT<amount:STRING, item:STRING>>, stat:STRING> 
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/board_order';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_FacebookUserInfo (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
platform_id STRING,
access_token  STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/FacebookUserInfo';


CREATE EXTERNAL TABLE IF NOT EXISTS raw_accept_neighbor (
uid STRING, 
key STRING,
event  STRING,
snsid   STRING,
ts STRING,
ts_pretty TIMESTAMP,
install_ts  STRING,
install_ts_pretty TIMESTAMP,
install_source  STRING,
device  STRING,
os STRING,
os_version   STRING,
ip   STRING,
country_code  STRING,
lang   STRING,
abid ARRAY<STRUCT<test:STRING, variant:INT>>,
level  STRING,
friend STRING
)  
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)  
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key" 
)   
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/events/accept_neighbor';

--------------------------------------------

-------------------------------------------
use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS fact_session
(
  id STRING,	
  date_start DATE,
  user_key STRING,
  uid STRING,
  snsid STRING,
  install_ts TIMESTAMP,
  install_date DATE,
  install_source STRING,
  app_version STRING,
  session_id STRING,
  ts_start TIMESTAMP,
  ts_end TIMESTAMP,
  level_start INT,
  level_end INT,
  os STRING,
  os_version STRING,
  country_code STRING,
  ip STRING,
  language STRING,
  device   STRING,
  browser STRING,
  browser_version STRING,
  rc_bal_start INT,
  rc_bal_end INT,
  coin_bal_start BIGINT,
  coin_bal_end BIGINT,
  ab_test STRING,
  ab_variant STRING,
  session_length INT
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT,
hour INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_session'
TBLPROPERTIES('serialization.null.format'='');

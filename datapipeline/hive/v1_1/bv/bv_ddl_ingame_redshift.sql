create table fact_ledger (
  id  VARCHAR(128) PRIMARY KEY ENCODE LZO,
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);



create table agg_daily_ledger (
  id VARCHAR(128) PRIMARY KEY ENCODE LZO,
  app VARCHAR(64) ENCODE LZO,
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_amount integer default 0,
  purchaser_cnt integer default null,
  user_cnt integer default null
)DISTKEY(date)
  SORTKEY(date,level_bin,transaction_type,in_type,in_name,out_type,out_name);

create table fact_item (
  id  VARCHAR(128) PRIMARY KEY ENCODE LZO,
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0,
  item_class varchar(64) encode lzo
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);



create table agg_item (
  id VARCHAR(128) PRIMARY KEY ENCODE LZO,
  date DATE ENCODE DELTA,
  level_bin SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_amount integer default 0,
  purchaser_cnt integer default null,
  user_cnt integer default null,
  item_class VARCHAR(64) ENCODE LZO
)DISTKEY(date)
  SORTKEY(date,level_bin,transaction_type,in_type,in_name,out_type,out_name);

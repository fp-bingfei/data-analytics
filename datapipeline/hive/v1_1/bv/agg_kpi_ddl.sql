use bv_1_1;

CREATE TABLE IF NOT EXISTS agg_kpi
(
  id STRING,
  date DATE,
  app_name STRING,
  app_version STRING,
  install_date DATE ,
  install_source STRING,
  subpublisher_source STRING,
  campaign_source STRING,
  level_end INT,
  device   STRING,
  device_alias  STRING,
  browser STRING,
  browser_version STRING,
  country_code      STRING,
  country           STRING,
  os STRING,
  os_version STRING,
  language STRING,
  ab_test      STRING,
  ab_variant    STRING,
  is_new_user SMALLINT,
  is_payer SMALLINT ,
  is_converted_today SMALLINT ,
  metric_name STRING,
  metric_value DECIMAL(14,4) ,
  metric_freq INT
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/agg_kpi'
TBLPROPERTIES('serialization.null.format'='');
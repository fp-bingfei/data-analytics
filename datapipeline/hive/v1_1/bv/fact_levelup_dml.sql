-----------------------------
-- Query to generate fact_levelup
-----------------------------
use bv_1_1;

--ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
--create temporary function MD5 as 'com.funplus.analytics.mr.udfs.Md5';
	
MSCK REPAIR TABLE raw_level_up;

-- Levelup DML
-- Populate fact_level_up
INSERT OVERWRITE TABLE tmp_fact_level_up PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT DISTINCT user_key,uid,snsid,level,date_start,date_end,ts_start,ts_end from
(
SELECT 
MD5(concat(app,uid)) as user_key,
uid,
snsid,
coalesce(level, from_level+1) AS level,
to_date(ts_pretty) as date_start,
to_date(lead(ts_pretty) over (partition by MD5(concat(app,uid)) order by ts_pretty asc)) as date_end,
ts_pretty as ts_start,
lead(ts_pretty) over (partition by MD5(concat(app,uid)) order by ts_pretty asc) as ts_end
FROM 
(SELECT * FROM raw_level_up WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate})tbl
UNION ALL
SELECT
user_key,
uid,
snsid,
level_start as level,
min(date_start) as date_start,
null as date_end,
min(ts_start) as ts_start,
null as ts_end
FROM
(SELECT * FROM fact_session WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate} and level_start=1)tbl
GROUP BY user_key,uid,snsid,level_start
)temp;

INSERT OVERWRITE TABLE tmp_next_level PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT n.user_key,n.level,n.ts_start,n.date_start
FROM (SELECT * FROM tmp_fact_level_up WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate}) l
JOIN (SELECT * FROM tmp_fact_level_up WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate}) n 
on n.level=l.level+1 and l.user_key=n.user_key
WHERE l.ts_end IS null;

INSERT OVERWRITE TABLE fact_level_up PARTITION (app="${hiveconf:rpt_app}",year,month,day)
SELECT 
MD5(concat(t1.app,t1.uid,t1.ts_start)) as id,
t1.user_key,
t1.app app_name,
t1.uid,
t1.snsid,
t1.level,
t1.date_start as date_start, 
COALESCE(t2.date_start,t1.date_end) as date_end, 
t1.ts_start as ts_start, 
COALESCE(t2.ts_start,t1.ts_end) as ts_end,
year(t1.date_start) year,
month(t1.date_start) month,
day(t1.date_start) day
from ( SELECT * FROM tmp_fact_level_up WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate}) t1 
LEFT OUTER JOIN 
( SELECT * FROM tmp_next_level  WHERE  app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate}) t2
ON t1.user_key=t2.user_key and t1.level+1=t2.level;

--To copy the 7 days data in S3 to Redshift using Redshift Copy Activity
INSERT OVERWRITE TABLE copy_level_up PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) SELECT id,user_key,app,uid,snsid,level,date_start,date_end,ts_start,ts_end FROM fact_level_up WHERE app="${hiveconf:rpt_app}" AND ${hiveconf:rpt_days_back_date_predicate};

------------------------------------------
---- Drop Partitions
------------------------------------------

ALTER TABLE tmp_fact_level_up DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_next_level DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});

exit;

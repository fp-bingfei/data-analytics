CREATE TABLE IF NOT EXISTS tmp_dau_snapshot
(
  date  DATE,
  user_key STRING,
  uid INT,
  snsid STRING,
  app_version STRING,
  install_ts TIMESTAMP ,
  install_date DATE,
  level_start INT,
  level_end INT,
  device   STRING,
  device_alias STRING,
  browser STRING,
  browser_version STRING,
  country_code      STRING,
  country  STRING,
  os  STRING,
  os_version  STRING,
  language  STRING,
  ab_test  STRING,
  ab_variant STRING,
  is_new_userSMALLINT ,
  coins_in INT,
  rc_in    INT,
  is_payerSMALLINT ,
  is_converted_today SMALLINT ,
  revenue_usd   DECIMAL(14,4),
  purchase_cnt    INT,
  session_cntINT,
  playtime_sec INT
)
PARTITION BY (
app  STRING
);

-- Do we need to drop partition?  
--truncate tmp_dau_snapshot;


--Step 1. Get daily users from raw_login and raw_payment-----

INSERT OVERWRITE TABLE tmp_dau_snapshot PARTITION (app)
SELECT u.date_start, u.user_key, u.uid, u.snsid, null, null, null, null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null, u.app
FROM
(
SELECT t.date_start, t.user_key, t.app, t.uid, t.snsid FROM
(SELECT date_start, user_key, app, uid, snsid 
FROM fact_session
WHERE app=@app AND year=@year AND month=@month AND day=@day  
UNION
SELECT date date_start, user_key, app, uid, snsid
FROM fact_revenue
WHERE app=@app AND year=@year AND month=@month AND day=@day) t GROUP BY t.date_start, t.user_key, t.app, t.uid, t.snsid ) u; 


CREATE TABLE IF NOT EXISTS tmp_user_daily_login
(
date_start  DATE,
user_key  STRING,
uid INT,
snsid STRING,
install_ts TIMESTAMP,
install_date DATE,
app_version STRING,
level_start INT,
level_end INT,
os STRING,
os_version STRING,
country_code STRING,
country STRING,
language STRING,
device STRING,
browser STRING,
browser_version STRING,
ab_test STRING,
ab_variant STRING
)
PARTITION BY (
app  STRING
);

CREATE TABLE IF NOT EXISTS dim_country
(
country STRING,
country_code STRING
);


INSERT OVERWRITE TABLE tmp_user_daily_login PARTITION (app)
SELECT date_start, user_key, uid, snsid,install_ts, install_date, app_version, level_start, level_end, os, os_version,
country_code, language, device, browser, browser_version, ab_test, ab_variant, app
FROM
(
SELECT 
date_start,
user_key,
uid,
snsid,
first_value(install_ts,true) OVER (partition by date_start, user_key order by ts_start desc
    rows between unbounded preceding and unbounded following) as install_ts,
first_value(install_date,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as install_date,  
first_value(app_version,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as app_version,
first_value(level_start,true) OVER (partition by date_start, user_key order by ts_start asc
     rows between unbounded preceding and unbounded following) as level_start,
first_value(level_end,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as level_end,
first_value(os,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as os,
first_value(os_version,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as os_version,
first_value(s.country_code,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as country_code,
first_value(c.country,true) OVER (partition by date_start, user_key order by ts_start desc
rows between unbounded preceding and unbounded following) as country,
first_value(language,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as language,
first_value(device,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as device,
first_value(browser,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as browser,
first_value(browser_version,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as browser_version,
first_value(ab_test,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as ab_test,
first_value(ab_variant,true) OVER (partition by date_start, user_key order by ts_start desc
    rows between unbounded preceding and unbounded following) as ab_variant,
first_value(app,true) OVER (partition by date_start, user_key order by ts_start desc
     rows between unbounded preceding and unbounded following) as app
FROM ( SELECT * FROM fact_session WHERE app='bv.global.prod' AND year=2014 AND month=11 AND day=3)s
LEFT OUTER JOIN dim_country c ON c.country_code=s.country_code
) t
group by date_start, user_key, uid, snsid,install_ts, install_date, app_version, level_start, level_end, os, os_version,
country_code, country,language, device, browser, browser_version, ab_test, ab_variant, app;



CREATE TABLE IF NOT EXISTS temp_user_payment
(
user_key STRING,
conversion_ts TIMESTAMP,
usd  DECIMAL(14,4)
)
PARTITION BY (
app  STRING
);

-- Need review --
INSERT OVERWRITE TABLE temp_user_payment PARTITION (app)
select fact_revenue.user_key,
min(ts)  conversion_ts,
app
from fact_revenue where app=@app AND year=@year AND month=@month AND day=@day
and fact_revenue.user_key not in (select user_key from dim_user where is_payer=1)
group by fact_revenue.user_key, app; 


CREATE TABLE IF NOT EXISTS is_payer_temp
(
user_key STRING,
is_payer INT,
app STRING
)
PARTITION BY (
app  STRING
);

INSERT OVERWRITE TABLE is_payer_temp
SELECT user_key,
is_payer,
app from dim_user where is_payer=1
UNION ALL
SELECT user_key
if(conversion_ts is not null, 1,0) is_payer,
app
from temp_user_payment

------------------------------------------------------------------------------------------------------------------------
--Step 4. Get users session count everyday
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS tmp_user_session_cnt
(
date_start  DATE,
user_key STRING,
session_cnt BIGINT
)
PARTITION BY (
app  STRING
);


INSERT OVERWRITE TABLE tmp_user_session_cnt PARTITION (app)
SELECT date_start, user_key, count(1) as session_cnt, app
FROM fact_session
WHERE app=@app AND year=@year AND month=@month AND day=@day
GROUP BY date_start, user_key, app;

CREATE TABLE IF NOT EXISTS tmp_user_daily_payment
(
date  DATE,
user_key STRING,
level INT,
coins_in  BIGINT,
rc_in   BIGINT,
usd  DECIMAL(14,4),
purchase_cnt  BIGINT
)
PARTITION BY (
app  STRING
);

INSERT OVERWRITE TABLE tmp_user_daily_payment PARTITION (app)
select date,
user_key,
max(level) as level,
sum(coins_in) as coins_in,
sum(rc_in) as rc_in,
sum(usd) as revenue_usd,
count(1) as purchase_cnt,
app
from fact_revenue
WHERE app=@app AND year=@year AND month=@month AND day=@day
group by date, user_key, app;




CREATE TABLE IF NOT EXISTS tmp_user_level
(
date  DATE,
user_key  STRING,
level_end INT
)
PARTITION BY (
app  STRING
);


INSERT OVERWRITE TABLE tmp_user_level PARTITION (app)
SELECT  
d.date,
d.user_key,
max(l.level) as level_end,
d.app
FROM tmp_dau_snapshot d
JOIN 
(SELECT * FROM fact_level_up WHERE app=@app AND year=@year AND month=@month AND day=@day) l 
ON d.user_key=l.user_key
WHERE ((d.date BETWEEN l.date_start AND l.date_end) OR (l.date_end IS NULL AND d.date>=l.date_start))
GROUP BY d.date,d.user_key,d.app






INSERT OVERWRITE TABLE tmp_dau_snapshot PARTITION (app)
select t.date,
t.user_key,
t.uid,
t.snsid,
null app_version,
COALESCE(d.install_ts, t.install_ts) install_ts,
COALESCE(d.install_date, t.install_date) install_date,
COALESCE(d.level_start, t.level_start) level_start,
COALESCE(l.level_end, d.level_end, t.level_end, t.level_start) level_end,
COALESCE(d.device, t.device) device,
null device_alias,
COALESCE(d.browser, t.browser) browser,
COALESCE(d.browser_version, t.browser_version) browser_version,
COALESCE(d.country_code, r.country_code, t.country_code) country_code,
COALESCE(d.country, r.country, t.country) country,
COALESCE(d.os, r.os, t.os) os,
COALESCE(d.os_version, t.os_version) os_version,
COALESCE(d.language, t.language) language,
COALESCE(d.ab_test, t.ab_test) ab_test,
COALESCE(d.ab_variant, t.ab_variant) ab_variant,
if(d.date=d.install_date, 1 ,0) is_new_user,
COALESCE(p.coins_in, t.coins_in) coins_in,
COALESCE(p.rc_in, t.rc_in) rc_in,
if(u.user_key is not null, 1,0) is_payer,
if(m.user_key is not null, 1,0) is_converted_today,
COALESCE(p.revenue_usd, t.revenue_usd) revenue_usd,
COALESCE(p.purchase_cnt, t.purchase_cnt) purchase_cnt,
COALESCE(s.session_cnt, t.session_cnt) session_cnt,
null playtime_sec,
t.app
from temp_dau_snapshot t
left outer join tmp_user_daily_login d
on t.user_key=d.user_key and t.date=d.date_start
left outer join tmp_user_level l
on t.user_key=l.user_key and t.date=l.date
left outer join tmp_user_session_cnt s
on t.user_key=s.user_key and t.date=s.date_start
left outer join tmp_user_daily_payment p
on t.user_key=p.user_key and t.date=p.date
left outer join is_payer_temp u
on t.user_key=u.user_key 
left outer join tmp_user_payment m
on t.user_key=u.user_key and t.date=u.conversion_ts
left outer join fact_revenue r
on t.user_key=r.user_key and t.date=r.date;




INSERT OVERWRITE TABLE fact_dau_snapshot PARTITION (app)
SELECT date,
user_key,
uid,
snsid,
app,
app_version,
install_ts,
install_date,
level_start,
level_end,
device,
device_alias,
browser,
browser_version,
country_code,
country,
os,
os_version,
language,
ab_test,
ab_variant,
is_new_user,
coins_in,
rc_in,
is_payer,
is_converted_today,
revenue_usd,
purchase_cnt,
session_cnt,
playtime_sec 
from tmp_dau_snapshot



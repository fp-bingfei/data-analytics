CREATE EXTERNAL TABLE IF NOT EXISTS fact_ledger
(
id  STRING,
date DATE,
ts TIMESTAMP,
user_key STRING,
app_name STRING,
uid STRING,
snsid STRING,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
in_amount BIGINT,
out_name STRING,
out_type STRING,
out_amount BIGINT
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_ledger'
TBLPROPERTIES('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS dim_item 
(
item_id STRING,
item_name STRING,
item_type STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/dim_item'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS dim_product 
(
product_id STRING,
name STRING,
type STRING,
qty INT,
usd_price DECIMAL(14,4)
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/dim_product'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS agg_daily_ledger
(
id  STRING,
date DATE,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
out_name STRING,
out_type STRING,
in_amount INT,
out_amount INT,
purchaser_cnt INT,
user_cnt INT
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/agg_daily_ledger'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS fact_item
(
id  STRING,
date DATE,
ts TIMESTAMP,
user_key STRING,
app_name STRING,
uid STRING,
snsid STRING,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
in_amount BIGINT,
out_name STRING,
out_type STRING,
out_amount BIGINT,
item_class  STRING
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/fact_item'
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS agg_item
(
id  STRING,
date DATE,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
out_name STRING,
out_type STRING,
in_amount BIGINT,
out_amount BIGINT,
purchaser_cnt INT,
user_cnt INT,
item_class  STRING
)
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/agg_item'
TBLPROPERTIES('serialization.null.format'='');


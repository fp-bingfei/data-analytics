--------------------------------------------
--
-------------------------------------------

use bv_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS dim_country
(
country STRING,
country_code STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/common/dim_country';



CREATE EXTERNAL TABLE IF NOT EXISTS dim_user
(
  id  STRING,
  user_key STRING,
  uid  STRING,
  snsid  STRING,
  app_name  STRING,
  install_ts  TIMESTAMP,
  install_date  DATE,
  install_source  STRING,
  subpublisher_source  STRING,
  campaign_source STRING,
  language  STRING,
  birth_date  DATE,
  gender STRING,
  country_code  STRING,
  country  STRING,
  os STRING,
  os_version STRING,
  device  STRING,
  browser STRING,
  browser_version STRING,
  app_version STRING,
  level INT,
  is_payer INT,
  conversion_ts TIMESTAMP,
  total_revenue_usd DECIMAL(12,4),
  login_ts TIMESTAMP
)  
PARTITIONED BY (
app   STRING,
year  INT,
month   INT,
day   INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/bv_1_1/dim_user'
TBLPROPERTIES('serialization.null.format'='');

--------------------------------------------
-- Temp Tables
--------------------------------------------

  
CREATE TABLE IF NOT EXISTS temp_dim_user_latest
(
  user_key STRING,
  uid  STRING,
  snsid  STRING,
  install_ts  TIMESTAMP,
  install_date  DATE,
  install_source  STRING,
  subpublisher_source  STRING,
  campaign_source STRING,
  language  STRING,
  birth_date  DATE,
  gender STRING,
  country_code  STRING,
  country  STRING,
  os STRING,
  os_version STRING,
  device  STRING,
  browser STRING,
  browser_version STRING,
  app_version STRING,
  level INT,
  is_payer INT,
  conversion_ts TIMESTAMP,
  total_revenue_usd DECIMAL(12,4),
  login_ts TIMESTAMP
)  
PARTITIONED BY (
app   STRING,
year INT,
month INT,
day INT,
step  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS  temp_dim_user_payment(
user_key  STRING,
conversion_ts  TIMESTAMP,
revenue DECIMAL(12,4)
)  
PARTITIONED BY (
app   STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
TBLPROPERTIES('serialization.null.format'='');




use bv_1_1;

INSERT OVERWRITE TABLE tmp_dau_snapshot PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="1") 
SELECT u.date_start, u.user_key, u.uid, u.snsid, null, null, null, null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null
FROM
(
SELECT t.date_start, t.user_key, t.app, t.uid, t.snsid FROM
(SELECT date_start, user_key, app, uid, snsid 
FROM fact_session
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} 
UNION ALL
SELECT date date_start, user_key, app, uid, snsid
FROM fact_revenue
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) t GROUP BY t.date_start, t.user_key, t.app, t.uid, t.snsid ) u; 

INSERT OVERWRITE TABLE tmp_dau_user_daily_login PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT date_start, user_key, uid, snsid,install_ts, install_date, app_version, level_start, level_end, os, os_version,
country_code,country, language, device, browser, browser_version, ab_test, ab_variant
FROM
(
SELECT 
date_start,
user_key,
uid,
snsid,
first_value(install_ts,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as install_ts,
first_value(install_date,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as install_date,  
first_value(app_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as app_version,
first_value(level_start,true) OVER (PARTITION BY date_start, user_key order by ts_start asc
     rows between unbounded preceding AND unbounded following) as level_start,
first_value(level_end,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as level_end,
first_value(os,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as os,
first_value(os_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as os_version,
first_value(s.country_code,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as country_code,
first_value(c.country,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as country,
first_value(language,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as language,
first_value(device,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as device,
first_value(browser,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as browser,
first_value(browser_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as browser_version,
first_value(ab_test,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as ab_test,
first_value(ab_variant,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as ab_variant
FROM ( SELECT * FROM fact_session WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} )s
LEFT OUTER JOIN dim_country c ON c.country_code=s.country_code
) t
group by date_start, user_key, uid, snsid,install_ts, install_date, app_version, level_start, level_end, os, os_version,
country_code, country,language, device, browser, browser_version, ab_test, ab_variant;


INSERT OVERWRITE table temp_dau_user_payment_first PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
select user_key,
min(ts)  conversion_ts,
sum(usd) usd
from fact_revenue
group by user_key;


INSERT OVERWRITE TABLE temp_dau_last_login_ts PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT user_key,
max(ts_end)  last_login_ts
from fact_session where app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}
GROUP BY user_key; 


INSERT OVERWRITE TABLE tmp_dau_user_session_cnt PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT date_start, user_key, count(1) as session_cnt
FROM fact_session
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}
GROUP BY date_start, user_key;


INSERT OVERWRITE TABLE tmp_dau_user_daily_payment PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
select date,
user_key,
max(level) as level,
sum(coins_in) as coins_in,
sum(rc_in) as rc_in,
sum(usd) as revenue_usd,
count(1) as purchase_cnt,
os,
country_code
from fact_revenue
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}
group by date,user_key, os, country_code;


INSERT OVERWRITE TABLE tmp_dau_user_level PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT  
d.date,
d.user_key,
max(l.level) as level_end
FROM (SELECT * FROM tmp_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} AND step="1" ) d
JOIN 
(SELECT * FROM fact_level_up WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) l 
ON d.user_key=l.user_key
WHERE ((d.date BETWEEN l.date_start AND l.date_end) OR (l.date_end IS NULL AND d.date>=l.date_start))
GROUP BY d.date,d.user_key;



INSERT OVERWRITE TABLE tmp_dau_snapshot PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="2")
select t.date,
t.user_key,
t.uid,
t.snsid,
null app_version,
COALESCE(d.install_ts, t.install_ts) install_ts,
COALESCE(d.install_date, t.install_date) install_date,
COALESCE(d.level_start, t.level_start) level_start,
COALESCE(l.level_end, d.level_end, d.level_start, t.level_start, t.level_end) level_end,
COALESCE(d.device, t.device) device,
null device_alias,
COALESCE(d.browser, t.browser) browser,
COALESCE(d.browser_version, t.browser_version) browser_version,
COALESCE(d.country_code, p.country_code, t.country_code) country_code,
COALESCE(d.country, t.country) country,
COALESCE(d.os, p.os, t.os) os,
COALESCE(d.os_version, t.os_version) os_version,
COALESCE(d.language, t.language) language,
COALESCE(d.ab_test, t.ab_test) ab_test,
COALESCE(d.ab_variant, t.ab_variant) ab_variant,
if(d.date_start=d.install_date, 1 ,0) is_new_user,
COALESCE(p.coins_in, t.coins_in) coins_in,
COALESCE(p.rc_in, t.rc_in) rc_in,
if(u.user_key is not null, 1,0) is_payer,
if(m.user_key is not null, 1,0) is_converted_today,
COALESCE(p.usd, t.revenue_usd) revenue_usd,
COALESCE(p.purchase_cnt, t.purchase_cnt) purchase_cnt,
COALESCE(s.session_cnt, t.session_cnt) session_cnt,
COALESCE(n.last_login_ts, t.last_login_ts) last_login_ts,
null playtime_sec
from (SELECT * FROM tmp_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} AND step="1") t
LEFT OUTER JOIN 
(SELECT * FROM tmp_dau_user_daily_login WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) d
on t.user_key=d.user_key
LEFT OUTER JOIN 
(SELECT * FROM tmp_dau_user_level  WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})l
on t.user_key=l.user_key
LEFT OUTER JOIN 
(SELECT * FROM tmp_dau_user_session_cnt WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) s
on t.user_key=s.user_key
LEFT OUTER JOIN 
(SELECT * FROM tmp_dau_user_daily_payment WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) p
on t.user_key=p.user_key
LEFT OUTER JOIN 
(SELECT * FROM temp_dau_user_payment_first WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) u
on t.user_key=u.user_key 
LEFT OUTER JOIN 
(SELECT * FROM temp_dau_user_payment_first WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) m
on t.user_key=m.user_key and t.date=to_date(m.conversion_ts)
LEFT OUTER JOIN 
(SELECT * FROM temp_dau_last_login_ts WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}) n
on t.user_key=n.user_key ;

-- If user from fact_revenue get missing fields from dim_user

INSERT OVERWRITE TABLE tmp_dau_snapshot PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="last")
select
t.date,
t.user_key,
t.uid,
t.snsid,
t.app_version,
CASE
WHEN t.install_ts is null
THEN d.install_ts
ELSE t.install_ts
END,
CASE
WHEN t.install_ts is null
THEN d.install_date
ELSE t.install_date
END,
CASE
WHEN t.install_ts is null
THEN d.level
ELSE t.level_start
END,
CASE
WHEN t.install_ts is null
THEN d.level
ELSE t.level_end
END,
CASE
WHEN t.install_ts is null
THEN d.device
ELSE t.device
END,
t.device_alias,
t.browser,
t.browser_version,
CASE
WHEN t.install_ts is null
THEN d.country_code
ELSE t.country_code
END,
t.country,
CASE
WHEN t.install_ts is null
THEN d.os
ELSE t.os
END,
CASE
WHEN t.install_ts is null
THEN d.os_version
ELSE t.os_version
END,
CASE
WHEN t.install_ts is null
THEN d.language
ELSE t.language
END,
t.ab_test,
t.ab_variant,
t.is_new_user,
t.coins_in,
t.rc_in,
t.is_payer,
t.is_converted_today,
t.revenue_usd,
t.purchase_cnt,
t.session_cnt,
t.last_login_ts,
t.playtime_sec
from (SELECT * FROM tmp_dau_snapshot WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day} AND step="2") t
LEFT OUTER JOIN
(SELECT * FROM dim_user WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year_yesterday} AND month=${hiveconf:rpt_month_yesterday} AND day=${hiveconf:rpt_day_yesterday}) d
on t.snsid=d.snsid;


INSERT OVERWRITE TABLE fact_dau_snapshot PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
select 
user_key id,
date,
user_key,
uid,
snsid,
app,
app_version,
install_ts,
install_date,
level_start,
level_end,
device,
device_alias,
browser,
browser_version,
country_code,
country,
os,
os_version,
language,
ab_test,
ab_variant,
is_new_user,
coins_in,
rc_in,
is_payer,
is_converted_today,
revenue_usd,
purchase_cnt,
session_cnt,
last_login_ts,
playtime_sec from tmp_dau_snapshot where app="${hiveconf:rpt_app}" and year=${hiveconf:rpt_year} and month=${hiveconf:rpt_month} and day=${hiveconf:rpt_day} and step="last" ;



--DROP TEMP PARTITIONS

ALTER TABLE tmp_dau_snapshot DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_dau_user_daily_login DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_dau_user_level DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_dau_user_session_cnt DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE tmp_dau_user_daily_payment DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE temp_dau_user_payment_first DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE temp_dau_last_login_ts DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
--ALTER TABLE tmp_dau_user_payment DROP PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});

exit; 

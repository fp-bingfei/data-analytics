use bv_1_1;

-- Add Partitions --
MSCK REPAIR TABLE raw_rc_transaction;
MSCK REPAIR TABLE raw_item_transaction;
MSCK REPAIR TABLE raw_coins_transaction;



INSERT OVERWRITE TABLE fact_ledger PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT
  MD5(CONCAT(t.app,t.uid,t.ts_pretty)) AS id,
  TO_DATE(t.ts_pretty) date,
  t.ts_pretty ts,
  MD5(CONCAT(t.app,t.uid)) AS user_key,
  t.app app_name,
  t.uid,
  t.snsid,
  t.level,
  t.action,
  CASE WHEN t.rc_in>0 THEN 'Rc' ELSE COALESCE(item_name,'Unknown') END  in_name,
  CASE WHEN t.rc_in>0 THEN 'Resource' ELSE COALESCE(i.item_type,'Unknown') END  in_type,
  CASE WHEN t.rc_in>0 THEN rc_in ELSE 1 END  in_amount,
  CASE WHEN t.rc_out>0 THEN 'Rc' ELSE COALESCE(item_name,'Unknown') END  out_name,
  CASE WHEN t.rc_out>0 THEN 'Resource' ELSE COALESCE(i.item_type,'Unknown') END  out_type,
  CASE WHEN t.rc_out>0 THEN rc_out ELSE 1 END  out_amount
FROM (SELECT * FROM raw_rc_transaction WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day}) t 
LEFT OUTER JOIN dim_item i ON i.item_id=action_detail
UNION ALL
SELECT
  MD5(CONCAT(t.app,t.uid,t.ts_pretty)) AS id,
  TO_DATE(t.ts_pretty) date,
  t.ts_pretty ts,
  MD5(CONCAT(t.app,t.uid)) AS user_key,
  t.app app_name,
  t.uid,
  t.snsid,
  t.level,
  t.action,
  CASE WHEN t.coins_in>0 THEN 'Coins' ELSE COALESCE(item_name,'Unknown') END  in_name,
  CASE WHEN t.coins_in>0 THEN 'Resource' ELSE COALESCE(i.item_type,'Unknown') END  in_type,
  CASE WHEN t.coins_in>0 THEN t.coins_in ELSE 1 END  in_amount,
  CASE WHEN t.coins_out>0 THEN 'Coins' ELSE COALESCE(item_name,'Unknown') END  out_name,
  CASE WHEN t.coins_out>0 THEN 'Resource' ELSE COALESCE(i.item_type,'Unknown') END  out_type,
  CASE WHEN t.coins_out>0 THEN t.coins_out ELSE 1 END  out_amount
FROM (SELECT * FROM raw_coins_transaction WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day}) t 
LEFT OUTER JOIN dim_item i ON i.item_id=action_detail;


INSERT INTO TABLE fact_ledger PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT
  id,
  date,
  ts,
  MD5(CONCAT(app,uid)) AS user_key,
  app,
  uid,
  snsid,
  level,
  'IAP',
  p.type  in_name,
  'Resource' as in_type,
  COALESCE(qty,0) as in_amount,
  r.product_id as out_name,
  'IAP' as out_type,
  1  as out_amount
from (SELECT * FROM fact_revenue WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day})r
LEFT OUTER JOIN dim_product p ON r.product_id =p.product_id WHERE lower(p.type) in ('rc','coins') AND p.qty>0 ;



INSERT OVERWRITE TABLE agg_daily_ledger PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT
MD5(concat(l.date,l.level,l.transaction_type, l.in_name, l.in_type, l.out_name, l.out_type)),
l.date,
l.level,
l.transaction_type,
l.in_name,
l.in_type,
l.out_name,
l.out_type,
SUM(COALESCE(l.in_amount,0))  in_amount,
SUM(COALESCE(l.out_amount,0))  out_amount,
COUNT(distinct l.user_key)   purchaser_cnt,
null user_cnt
FROM fact_ledger l WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day}
GROUP BY l.date,l.level,l.transaction_type,l.in_name,l.in_type,l.out_name,
l.out_type;



INSERT OVERWRITE TABLE fact_item PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT
  MD5(CONCAT(t.app,t.uid,t.ts_pretty)) AS id,	
  TO_DATE(t.ts_pretty) date,
  t.ts_pretty ts,
  MD5(CONCAT(t.app,t.uid)) AS user_key,
  app app_name,
  t.uid,
  t.snsid,
  t.level,
  t.action,
  CASE WHEN COALESCE(t.item_in,0)>0 THEN 'Items' ELSE COALESCE(t.item_name,'Unknown') END  in_name,
  CASE WHEN COALESCE(t.item_in,0)>0 THEN 'Item' ELSE COALESCE(t.item_type,'Unknown')  END in_type,
  CASE WHEN COALESCE(t.item_in,0)>0 THEN t.item_in ELSE 1 END  in_amount,
  CASE WHEN COALESCE(t.item_out,0)>0 THEN 'Items' ELSE COALESCE(t.item_name,'Unknown') END  out_name,
  CASE WHEN COALESCE(t.item_out,0)>0 THEN 'Item' ELSE COALESCE(t.item_type,'Unknown') END  out_type,
  CASE WHEN COALESCE(t.item_out,0)>0 THEN t.item_out ELSE 1 END  out_amount,
  item_class
FROM raw_item_transaction t WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day}
;



INSERT OVERWRITE TABLE agg_item PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT
MD5(concat(a.level, a.transaction_type, a.in_name, a.in_type, a.out_name, a.out_type, a.item_class)),
date,
a.level,
a.transaction_type,
a.in_name,
a.in_type,
a.out_name,
a.out_type,
SUM(COALESCE(in_amount,0)) in_amount,
SUM(COALESCE(out_amount,0)) out_amount,
COUNT(DISTINCT a.user_key) purchase_cnt,
null user_cnt,
item_class
FROM
 fact_item a 
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day}
GROUP BY date, a.level, a.transaction_type, a.in_name, a.in_type, a.out_name, a.out_type, a.item_class;


exit;

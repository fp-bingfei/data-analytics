CREATE TABLE fact_session
(
  id VARCHAR(128) NOT NULL PRIMARY KEY ENCODE LZO,
  date_start DATE NOT NULL ENCODE DELTA,
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  install_ts timestamp ENCODE DELTA,
  install_date      DATE ENCODE DELTA,
  install_source    VARCHAR(1024) ENCODE BYTEDICT,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  session_id varchar(50) ENCODE LZO,
  ts_start timestamp ENCODE DELTA,
  ts_end timestamp ENCODE DELTA,
  level_start integer,
  level_end integer,
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT,
  country_code VARCHAR(20) ENCODE BYTEDICT default '--',
  ip VARCHAR(20) ENCODE LZO,
  language VARCHAR(20) ENCODE BYTEDICT,
  device   VARCHAR(64) ENCODE BYTEDICT,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(32) ENCODE BYTEDICT,
  rc_bal_start integer ENCODE BYTEDICT,
  rc_bal_end integer ENCODE BYTEDICT,
  coin_bal_start integer ENCODE BYTEDICT,
  coin_bal_end integer ENCODE BYTEDICT,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  session_length integer
)
  DISTKEY(user_key)
  SORTKEY(user_key,date_start,uid,snsid,app);




CREATE TABLE fact_revenue
(
  id                VARCHAR(128) NOT NULL PRIMARY KEY ENCODE LZO, 
  date              DATE NOT NULL ENCODE DELTA,
  user_key          varchar(50) NOT NULL ENCODE LZO,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  ts                TIMESTAMP NOT NULL ENCODE DELTA,
  level             SMALLINT ENCODE BYTEDICT,
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  country_code VARCHAR(20) ENCODE BYTEDICT default '--',
  payment_processor VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  product_id        VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  product_name      VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  product_type      VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  coins_in          INTEGER,
  rc_in             INTEGER,
  currency          VARCHAR(32) NOT NULL ENCODE BYTEDICT,
  amount        decimal(14,4) DEFAULT 0,
  usd        decimal(14,4) DEFAULT 0,
  transaction_id    VARCHAR
)
  DISTKEY(user_key)
  SORTKEY(date, ts, user_key, uid, snsid,os,country_code);

CREATE TABLE fact_dau_snapshot
(
  id VARCHAR(128)  NOT NULL PRIMARY KEY ENCODE LZO,
  date DATE NOT NULL ENCODE DELTA,
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  install_ts TIMESTAMP ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  level_start integer,
  level_end integer,
  device   VARCHAR(64) ENCODE BYTEDICT,
  device_alias  VARCHAR(32) DEFAULT NULL,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(32) ENCODE BYTEDICT,
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT Default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT,
  language VARCHAR(20) ENCODE BYTEDICT,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  is_new_user       SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  coins_in          INTEGER DEFAULT 0,
  rc_in             INTEGER DEFAULT 0,
  is_payer       SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  is_converted_today SMALLINT DEFAULT 0 ENCODE BYTEDICT,
  revenue_usd        decimal(14,4) DEFAULT 0,
  purchase_cnt    integer default 0,
  session_cnt       INTEGER DEFAULT 1,
  last_login_ts    TIMESTAMP ,
  playtime_sec integer
)
DISTKEY(user_key)
  SORTKEY(date, user_key, app, uid, snsid, install_date, country_code);  


CREATE TABLE fact_user_install_source
(
  id                VARCHAR(128) NOT NULL PRIMARY KEY ENCODE LZO,
  user_key          VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  install_date      DATE ENCODE DELTA,
  install_source_event_raw  VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_event  VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust_raw VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_adjust VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source_raw    VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  install_source    VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  campaign          VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT,
  sub_publisher     VARCHAR(1024) DEFAULT '' ENCODE BYTEDICT
)
  DISTKEY(uid)
  SORTKEY(app, uid, snsid, install_date, install_source_event, install_source_adjust, install_source, campaign, sub_publisher);



CREATE TABLE dim_user
(
  id VARCHAR(128) NOT NULL PRIMARY KEY ENCODE LZO,
  user_key varchar(50) NOT NULL ENCODE LZO,
  uid INTEGER NOT NULL,
  snsid VARCHAR(64) NOT NULL ENCODE LZO,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  install_ts timestamp ENCODE DELTA,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) DEFAULT 'Organic' ENCODE BYTEDICT,
  subpublisher_source varchar(500) DEFAULT '' ENCODE BYTEDICT,
  campaign_source varchar(100) DEFAULT '' ENCODE BYTEDICT,
  language varchar(20) default 'Unknown' ENCODE BYTEDICT,
  birth_date date ENCODE DELTA,
  gender varchar(10) ENCODE BYTEDICT,
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT,
  device  VARCHAR(64) ENCODE BYTEDICT,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(32) ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  level integer,
  is_payer integer,
  conversion_ts timestamp ENCODE DELTA,
  total_revenue_usd decimal(12,4) ENCODE BYTEDICT,
  login_ts timestamp ENCODE DELTA
)
DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);




CREATE TABLE agg_kpi
(
  date DATE NOT NULL ENCODE DELTA,
  app VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE LZO,
  install_date DATE ENCODE DELTA,
  install_source VARCHAR(1024) ENCODE BYTEDICT,
  subpublisher_source varchar(500) ENCODE BYTEDICT,
  campaign_source varchar(100) ENCODE BYTEDICT,
  level_end integer,
  device   VARCHAR(64) ENCODE BYTEDICT,
  device_alias  VARCHAR(32) DEFAULT NULL,
  browser VARCHAR(32) ENCODE BYTEDICT,
  browser_version VARCHAR(32) ENCODE BYTEDICT,
  country_code      VARCHAR(16) ENCODE BYTEDICT DEFAULT '--',
  country           VARCHAR(64) ENCODE BYTEDICT Default 'Unknown',
  os varchar(30)  ENCODE BYTEDICT default 'Unknown',
  os_version varchar(100) ENCODE BYTEDICT,
  language VARCHAR(20) ENCODE BYTEDICT,
  ab_test           VARCHAR(1024) ENCODE BYTEDICT default 'None',
  ab_variant        VARCHAR(7) ENCODE BYTEDICT default 'None',
  is_new_user SMALLINT ENCODE BYTEDICT,
  is_payer SMALLINT ENCODE BYTEDICT,
  is_converted_today SMALLINT ENCODE BYTEDICT,
  metric_name varchar(30)  ENCODE BYTEDICT,
  metric_value decimal(14,4)  ENCODE BYTEDICT,
  metric_freq integer  ENCODE BYTEDICT
)DISTKEY(date)
  SORTKEY(date, install_date, country_code, os);
  



-----------------------------
-- Query to generate dim_user
-----------------------------

use bv_1_1;


--ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
--create temporary function MD5 as 'com.funplus.analytics.mr.udfs.Md5';

INSERT OVERWRITE TABLE temp_dim_user_latest PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="1") 
SELECT
user_key,
uid,
snsid,
install_ts,
to_date(install_ts) install_date,
'Organic' install_source,
'' subpublisher_source,
'' campaign_source,
COALESCE(language, 'Unknown') language,
null birth_date,
null gender,
COALESCE(t.country_code, '--') country_code,
COALESCE(t.country, 'Unknown') country,
COALESCE(os, 'Unknown') os,
os_version,
device,
browser,
browser_version,
null app_version,
level_end,
is_payer,
null conversion_ts,
null total_revenue_usd,
last_login_ts login_ts
FROM (
SELECT *, row_number() OVER (PARTITION BY user_key ORDER BY date DESC) AS row
FROM fact_dau_snapshot
WHERE 
app="${hiveconf:rpt_app}" 
AND 
${hiveconf:rpt_days_back_date_predicate}
)t
WHERE t.row = 1;

INSERT OVERWRITE TABLE temp_dim_user_payment PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day})
SELECT user_key,
min(ts) as conversion_ts,
sum(usd) as revenue
FROM fact_revenue
WHERE fact_revenue.user_key IN 
(SELECT distinct user_key FROM temp_dim_user_latest 
WHERE 
app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="1")
group by user_key;


INSERT OVERWRITE TABLE temp_dim_user_latest PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},step="last")
SELECT 
d.user_key,
d.uid,
d.snsid,
d.install_ts,
d.install_date,
COALESCE(f.install_source, d.install_source) install_source,
COALESCE(f.sub_publisher, d.subpublisher_source) subpublisher_source,
COALESCE(f.campaign, d.campaign_source) campaign_source,
d.language,
d.birth_date,
d.gender,
d.country_code,
d.country,
d.os,
d.os_version,
d.device,
d.browser,
d.browser_version,
d.app_version,
d.level,
d.is_payer,
COALESCE(t.conversion_ts, d.conversion_ts) conversion_ts,
COALESCE(revenue, total_revenue_usd) total_revenue_usd,
d.login_ts
FROM 
(SELECT * FROM temp_dim_user_latest WHERE app="${hiveconf:rpt_app}" 
AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="1") d 
LEFT OUTER JOIN 
(SELECT * FROM temp_dim_user_payment WHERE app="${hiveconf:rpt_app}" 
AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} ) t
ON d.user_key = t.user_key
LEFT OUTER JOIN
(SELECT * FROM fact_user_install_source 
WHERE 
app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} 
)f
ON d.user_key = f.user_key;

INSERT OVERWRITE TABLE dim_user PARTITION (app="${hiveconf:rpt_app}", year=${hiveconf:rpt_year}, month=${hiveconf:rpt_month}, day=${hiveconf:rpt_day})
SELECT 
user_key id,
user_key,
uid,
snsid,
app,
install_ts,
install_date,
install_source,
subpublisher_source,
campaign_source,
language,
birth_date,
gender,
country_code,
country,
os,
os_version,
device,
browser,
browser_version,
app_version,
level,
is_payer,
conversion_ts,
total_revenue_usd,
login_ts
FROM temp_dim_user_latest
WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day} and step="last"
UNION ALL
SELECT
id,
d.user_key,
uid,
snsid,
app_name app,
install_ts,
install_date,
install_source,
subpublisher_source,
campaign_source,
language,
birth_date,
gender,
country_code,
country,
os,
os_version,
device,
browser,
browser_version,
app_version,
level,
is_payer,
conversion_ts,
total_revenue_usd,
login_ts
FROM dim_user d
WHERE
app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year_yesterday} AND month=${hiveconf:rpt_month_yesterday} AND day=${hiveconf:rpt_day_yesterday} AND
d.user_key NOT IN
(
 SELECT user_key FROM temp_dim_user_latest WHERE app="${hiveconf:rpt_app}" AND year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day =${hiveconf:rpt_day}  and step="last"
)
;


-------------------------
-- Drop Partitions 
-------------------------


ALTER TABLE temp_dim_user_latest DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});
ALTER TABLE temp_dim_user_payment DROP IF EXISTS PARTITION (app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});

exit;

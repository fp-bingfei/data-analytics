use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.mapred.reduce.tasks.speculative.execution=false;
--SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


alter table raw_sendgrid_daily add if not exists partition (app='farm.br.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.br.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.de.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.de.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.fr.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.fr.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.it.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.it.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.nl.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.nl.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.pl.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.pl.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.th.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.th.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.tw.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.tw.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.us.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.us.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';


SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_raw_sendgrid_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');


INSERT OVERWRITE TABLE copy_raw_sendgrid_daily PARTITION (app='${hiveconf:rpt_all_app}', dt='${hiveconf:rpt_date}')
SELECT   snsid,
  email,
  category,
  uid,
  app as app_id,
  from_unixtime(cast(time as BIGINT)),
  ip,
  event,
  day,
  campaign
FROM 
raw_sendgrid_daily 
WHERE 
app in (
'farm.br.prod',
'farm.de.prod',
'farm.fr.prod',
'farm.it.prod',
'farm.nl.prod',
'farm.pl.prod',
'farm.th.prod',
'farm.tw.prod',
'farm.us.prod'
)
AND dt > '${hiveconf:rpt_date_d3}' AND dt <= '${hiveconf:rpt_date}';

exit;



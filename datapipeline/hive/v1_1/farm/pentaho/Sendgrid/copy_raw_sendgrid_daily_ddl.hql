use farm_1_1;

-- copy_raw_sendgrid_daily
  CREATE TABLE copy_raw_sendgrid_daily (
  snsid string,
  email string,
  category string,
  uid string,
  app_id string,
  time string,
  ip string,
  event string,
  day string,
  campaign string)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_sendgrid_daily'
TBLPROPERTIES('serialization.null.format'='');
USE farm_1_1;

DROP TABLE IF EXISTS raw_expand_status_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_expand_status_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    1st_ranch STRING,
    1st_yard STRING,
    2nd STRING,
    2nd_ranch STRING,
    2nd_yard STRING,
    3rd STRING,
    3rd_water STRING,
    3rd_ranch STRING,
    neighbor_num BIGINT
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/expand_status'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_expand_status_daily;

CREATE EXTERNAL TABLE raw_expand_status_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    1st_ranch string,
    1st_yard string,
    2nd string,
    2nd_ranch string,
    2nd_yard string,
    3rd string,
    3rd_water string,
    3rd_ranch string,
    neighbor_num bigint
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_expand_status_daily'
TBLPROPERTIES ('serialization.null.format'='');

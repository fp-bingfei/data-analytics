USE farm_1_1;

-- DIYMaterial
DROP TABLE IF EXISTS raw_diymaterial_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_diymaterial_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    material_id   STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/DIYMaterial'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_diymaterial_daily;

CREATE EXTERNAL TABLE raw_diymaterial_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    material_id string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_diymaterial_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- DIYVoucher
DROP TABLE IF EXISTS raw_diyvoucher_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_diyvoucher_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    voucher_id   STRING,
    voucher_qty   STRING,
    is_buy   STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/DIYVoucher'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_diyvoucher_daily;

CREATE EXTERNAL TABLE raw_diyvoucher_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    voucher_id string,
    voucher_qty string,
    is_buy string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_diyvoucher_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- DIYReward
DROP TABLE IF EXISTS raw_diyreward_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_diyreward_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    reward_id STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/DIYReward'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_diyreward_daily;

CREATE EXTERNAL TABLE raw_diyreward_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string,
    reward_id string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_diyreward_daily'
TBLPROPERTIES ('serialization.null.format'='');

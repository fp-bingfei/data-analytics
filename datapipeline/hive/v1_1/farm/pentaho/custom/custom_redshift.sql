--Create temp table to get latest additional email address

delete from processed.personal_info where date = '${RPT_DATE}' and app_id='${RPT_APP}';
copy processed.personal_info from 's3://com.funplus.datawarehouse/farm_1_1/copy/personal_info_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;


create temp table personal_email_latest
as
select distinct   app_id 
          ,last_value(browser   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser
          ,last_value(browser_version   ignore nulls)
              OVER (PARTITION BY md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as browser_version
          ,last_value(country_code   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as country_code
          ,event 
          ,last_value(install_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_source
          ,last_value(install_date   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as install_date
          ,last_value(ip   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as ip
          ,last_value(lang   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as lang
          ,last_value(level   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as level
          ,last_value(os   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os
          ,last_value(os_version   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as os_version
          ,uid
          ,snsid
          ,last_value(fb_source   ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following) as fb_source
          ,last_value(additional_email  ignore nulls)
              OVER (PARTITION BY  md5(app_id||uid) ORDER BY ts ASC
              ROWS BETWEEN unbounded preceding AND unbounded following)  as additional_email
from processed.personal_info
;

-- TODO: For EAS report
create temp table payment_info as
select app, uid, snsid, min(ts) as conversion_ts, max(ts) as last_payment_ts, count(1) as payment_cnt
from
(select p.app_id as app, u.app_user_id as uid, p.uid as snsid, paid_time as ts
from tbl_payment p
    join processed.dim_user u on p.app_id = u.app_id and p.uid = u.facebook_id
union
select r.app_id as app, r.app_user_id as uid, u.facebook_id as snsid, ts
from processed.fact_revenue r
    join processed.dim_user u on r.app_id = u.app_id and r.app_user_id = u.app_user_id
) t
group by 1,2,3;

truncate table processed.eas_user_info;
insert into processed.eas_user_info
select
    t.app
    ,t.uid
    ,t.snsid
    ,t.name as user_name
    ,case when t.email = 'NULL' or t.email is NULL then '' else t.email end as email
    ,coalesce(pe.additional_email,'') as additional_email
    ,t.track_ref as install_source
    ,t.addtime as install_ts
    ,u.language as language
    ,u.gender
    ,t.level
    ,case when p.conversion_ts is not null then 1 else 0 end as is_payer
    ,p.conversion_ts
    ,p.last_payment_ts
    ,case when p.payment_cnt is not null then p.payment_cnt else 0 end as payment_cnt
    ,CAST(t.reward_points AS BIGINT) as rc
    ,CAST(t.coins AS BIGINT) as coins
    ,t.addtime as last_login_ts
from tbl_user t
    left join payment_info p on t.app = p.app and t.uid = p.uid and t.snsid = p.snsid
    left join processed.dim_user u on t.app = u.app_id and t.uid = u.app_user_id and t.snsid = u.facebook_id
    left join personal_email_latest pe on t.app=pe.app_id and t.uid=pe.uid and t.snsid=pe.snsid;

	commit;
	
	--------------------------
	--- Market Order Events --
	--------------------------
	
	-- Delete --
	
	delete from raw_data.raw_marketcratefinish where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketcrateneedhelp where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketexchange where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketorderfinish where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketorderbegin where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	delete from raw_data.raw_marketordernew where trunc(ts_pretty) = '${RPT_DATE}' and key='${RPT_APP}';
	
	delete from raw_data.raw_marketorderfinish_temp where key='${RPT_APP}';
	delete from raw_data.raw_marketorderbegin_temp where key='${RPT_APP}';
	delete from raw_data.raw_marketordernew_temp where key='${RPT_APP}';
		
	-- Load --
	
	copy raw_data.raw_marketcratefinish from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketcratefinish_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketcrateneedhelp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketcrateneedhelp_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketexchange from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketexchange_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketorderfinish_temp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketorderfinish_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketorderbegin_temp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketorderbegin_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	copy raw_data.raw_marketordernew_temp from 's3://com.funplus.datawarehouse/farm_1_1/copy/raw_marketordernew_daily/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	
	
	-- Denormalize Crates --
	
	INSERT INTO raw_data.raw_marketorderfinish
	SELECT uid, key, ts_pretty, country_code, snsid, level, order_points_get, item_quantity, time_left,
	 new_cash1_get, new_cash2_get, new_cash3_get, crates,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',1) crate_1_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',2) crate_1_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',3) crate_1_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',1) crate_2_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',2) crate_2_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',3) crate_2_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',1) crate_3_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',2) crate_3_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',3) crate_3_number
	FROM raw_data.raw_marketorderfinish_temp;
	
	INSERT INTO raw_data.raw_marketorderbegin
	SELECT uid, key, ts_pretty, country_code, snsid, level, order_points, time_left,
	 new_cash1, new_cash2, new_cash3, crates, rc_cost, rc_bal,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',1) crate_1_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',2) crate_1_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',3) crate_1_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',1) crate_2_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',2) crate_2_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',3) crate_2_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',1) crate_3_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',2) crate_3_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',3) crate_3_number
	FROM raw_data.raw_marketorderbegin_temp;
	
	
	INSERT INTO raw_data.raw_marketordernew
	SELECT uid, key, ts_pretty, country_code, snsid, level, order_points, time_left,
	 new_cash1, new_cash2, new_cash3, crates, rc_cost, rc_bal,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',1) crate_1_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',2) crate_1_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',1),':',3) crate_1_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',1) crate_2_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',2) crate_2_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',2),':',3) crate_2_number,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',1) crate_3_item_id, 
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',2) crate_3_item_amount,
	split_part(split_part(regexp_replace(crates,'"|\\{|\\}',''),',',3),':',3) crate_3_number
	FROM raw_data.raw_marketordernew_temp;
	
	
	commit;
	
	--------------------------
	--- AGG Handprint  --
	--------------------------
	--Delete--
	delete from processed.agg_handprint where app_id='${RPT_APP}' and date = '${RPT_DATE}';
	
	--Load--
	copy processed.agg_handprint from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_handprint/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
	
	commit;
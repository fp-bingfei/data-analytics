USE farm_1_1;

-- submarine
DROP TABLE IF EXISTS raw_submarine_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_submarine_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    action  STRING,
    coins   STRING,
    duration    STRING,
    greenery_level  STRING,
    input   STRING,
    output  STRING,
    npc STRING,
    rc  STRING,
    returnTs   STRING,
    sailTs STRING,
    sailors array<string>,
    speedup STRING,
    sub STRING,
    water_level STRING,
    timestamp   STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/submarine'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_submarine_daily;

CREATE EXTERNAL TABLE raw_submarine_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    action string, 
    coins string, 
    duration string, 
    greenery_level string, 
    input string, 
    output string, 
    npc string, 
    rc string, 
    returnTs string, 
    sailTs string, 
    sailors array<string>,
    speedup string, 
    sub string, 
    water_level string, 
    timestamp string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_submarine_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- WaterLevelUp
DROP TABLE IF EXISTS raw_waterlevelup_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_waterlevelup_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    expansion_size_from BIGINT,
    greenery_bal    BIGINT,
    greenery_in BIGINT,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    new_water_level BIGINT,
    old_water_level BIGINT,
    os   STRING,
    os_version   STRING,
    rc_bal  BIGINT,
    rc_get  STRING,
    snsid   STRING,
    uid   STRING,
    water_exp STRING,
    water_level  STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/WaterLevelUp'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_waterlevelup_daily;

CREATE EXTERNAL TABLE raw_waterlevelup_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    expansion_size_from bigint, 
    greenery_bal bigint, 
    greenery_in bigint, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    new_water_level bigint, 
    old_water_level bigint, 
    os string, 
    os_version string, 
    rc_bal bigint, 
    rc_get string, 
    snsid string, 
    uid string, 
    water_exp string, 
    water_level string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_waterlevelup_daily'
TBLPROPERTIES ('serialization.null.format'='');


-- ThirdSceneExpand
DROP TABLE IF EXISTS raw_thirdsceneexpand_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_thirdsceneexpand_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    coins_bal   DOUBLE,
    country_code   STRING,
    event   STRING,
    expansion_size_from   BIGINT,
    expansion_size_to   BIGINT,
    expansion_top_size_from   BIGINT,
    greenery_bal   BIGINT,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    rc_bal   BIGINT,
    snsid   STRING,
    uid   STRING,
    waterLevel   STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/ThirdSceneExpand'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_thirdsceneexpand_daily;

CREATE EXTERNAL TABLE raw_thirdsceneexpand_daily (
    key string,
    ts string,
    ts_pretty timestamp, 
    browser string,
    browser_version string,
    coins_bal double,
    country_code string,
    event string,
    expansion_size_from bigint, 
    expansion_size_to bigint, 
    expansion_top_size_from bigint, 
    greenery_bal bigint, 
    install_source string,
    install_ts string,
    install_ts_pretty timestamp, 
    ip string,
    lang string,
    level bigint, 
    os string,
    os_version string,
    rc_bal bigint, 
    snsid string,
    uid string,
    waterLevel string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_thirdsceneexpand_daily'
TBLPROPERTIES ('serialization.null.format'='');


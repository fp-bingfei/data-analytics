USE farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size=134217728;
SET hive.exec.max.dynamic.partitions=2000;
SET hive.exec.max.dynamic.partitions.pernode=2000;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;

set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};

-- 4th_scene_login
MSCK REPAIR TABLE raw_4th_scene_login_seq;

INSERT OVERWRITE TABLE raw_4th_scene_login_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    first_login,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_login_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- 4th_scene_currency
MSCK REPAIR TABLE raw_4th_scene_currency_seq;

INSERT OVERWRITE TABLE raw_4th_scene_currency_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    action_type,
    chef_points_in,
    chef_points_out,
    chef_points_bal,
    silver_coins_in,
    silver_coins_out,
    silver_coins_bal,
    reputation_in,
    reputation_out,
    reputation_bal,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_currency_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- 4th_scene_action
MSCK REPAIR TABLE raw_4th_scene_action_seq;

INSERT OVERWRITE TABLE raw_4th_scene_action_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    put_on_4th_scene,
    cost_type,
    cost_num,
    decoration_id,
    reputation,
    building,
    item_id,
    item_num,
    finish_with_rc,
    production_time,
    from_num,
    to_num,
    material,
    upgrade_material,
    sell_time,
    dish_id,
    recipe,
    sell_silver_coins,
    sell_chef_points,
    product_id,
    material_id,
    needs,
    request_to_friend,
    request_to_neighbor,
    parent_id,
    use_num,
    direction,
    detail,
    limit_from,
    limit_to,
    type,
    currency,
    currency_num,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_action_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- 4th_scene_level_up
MSCK REPAIR TABLE raw_4th_scene_level_up_seq;

INSERT OVERWRITE TABLE raw_4th_scene_level_up_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    previous_level,
    current_level,
    reward_info,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_4th_scene_level_up_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

exit;


USE farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size=134217728;
SET hive.exec.max.dynamic.partitions=2000;
SET hive.exec.max.dynamic.partitions.pernode=2000;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;

set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};

MSCK REPAIR TABLE raw_guildinfo_seq;
MSCK REPAIR TABLE raw_guildmembership_seq;
MSCK REPAIR TABLE raw_guildshop_seq;
MSCK REPAIR TABLE raw_guildmapreward_seq;
MSCK REPAIR TABLE raw_guilddonation_seq;
MSCK REPAIR TABLE raw_guilddonationreward_seq;
MSCK REPAIR TABLE raw_guilddonationunlock_seq;
MSCK REPAIR TABLE raw_guildlevelup_seq;
MSCK REPAIR TABLE raw_guildvouchers_seq;

-- GuildInfo
INSERT OVERWRITE TABLE raw_guildinfo_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    is_new,
    guildid,
    name,
    cover,
    type,
    required_level,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildinfo_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildMembership
INSERT OVERWRITE TABLE raw_guildmembership_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    memberid,
    status,
    is_self,
    member_count,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildmembership_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildShop
INSERT OVERWRITE TABLE raw_guildshop_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    itemid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildshop_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildMapReward
INSERT OVERWRITE TABLE raw_guildmapreward_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    rewardid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildmapreward_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildDonation
INSERT OVERWRITE TABLE raw_guilddonation_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    topicid,
    itemid,
    item_num,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guilddonation_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildDonationReward
INSERT OVERWRITE TABLE raw_guilddonationreward_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    topicid,
    rewardid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guilddonationreward_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildDonationUnlock
INSERT OVERWRITE TABLE raw_guilddonationunlock_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    topicid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guilddonationunlock_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildLevelUp
INSERT OVERWRITE TABLE raw_guildlevelup_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    guild_level,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildlevelup_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

-- GuildVouchers
INSERT OVERWRITE TABLE raw_guildvouchers_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    guildid,
    voucher_type,
    voucher_in,
    voucher_out,
    action,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_guildvouchers_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

exit;


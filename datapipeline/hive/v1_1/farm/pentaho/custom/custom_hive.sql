
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.exec.compress.intermediate=true;
SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;



set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};


--------------------------------------------------
--- Generate daily handprint events
--------------------------------------------------
ALTER TABLE raw_handprint_seq DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});


ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

-- Add today hour=00 partition
ALTER TABLE raw_handprint_seq DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1});
ALTER TABLE raw_handprint_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);

INSERT OVERWRITE TABLE raw_handprint_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    lang,
    level,
    os,
    os_version,
    snsid,
    uid,
    fb_source,
    action,
    handprint_in,
    handprint_qty,
    handprint_level,
    reward_type
FROM 
    raw_handprint_seq
WHERE 
    app='${hiveconf:rpt_app}' 
    AND 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    ) 
    AND 
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

--------------------------------------------------
--- Insert agg_handprint data
--------------------------------------------------

INSERT OVERWRITE TABLE agg_handprint PARTITION (app, dt)
select h.key
      ,to_date(h.ts_pretty) as date
      ,h.snsid
      ,h.uid 
      ,h.browser
      ,coalesce(c.country,'Unknown') as country
      ,h.install_source
      ,to_date(h.install_ts_pretty) as install_date
      ,h.action
      ,h.handprint_level
      ,sum(h.handprint_in) as handprint_in
      ,h.app
      ,h.dt
from raw_handprint_daily h
left join dim_country c
on h.country_code = c.country_code
where h.app = '${hiveconf:rpt_app}' and h.dt = '${hiveconf:rpt_date}'
group by    h.key
      ,to_date(h.ts_pretty) as date
      ,h.snsid
      ,h.uid 
      ,h.browser
      ,coalesce(c.country,'Unknown')
      ,h.install_source
      ,to_date(h.install_ts_pretty)
      ,h.action
      ,h.handprint_level
      ,h.app
      ,h.dt
;


--------------------------------------------------
--- copy to redshift - raw_personal_info_daily
--------------------------------------------------


SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_personal_info_daily DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_personal_info_daily PARTITION(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
SELECT 
  MD5(concat(app,uid,ts)) as id,
  key as app_id,
  ts_pretty as ts,
  to_date(ts_pretty) as date,
  browser,
  browser_version,
  country_code,
  event,
  install_source,
  install_ts_pretty as install_ts,
  to_date(install_ts_pretty) as install_date,
  ip,
  lang,
  level,
  os,
  os_version,
  snsid,
  uid,
  fb_source,
  additional_email
FROM 
  raw_personal_info_daily
WHERE 
  app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';


--------------------------------------------------
--- copy to redshift - agg_handprint
--------------------------------------------------

ALTER TABLE copy_agg_handprint DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


insert overwrite table copy_agg_handprint partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select  key as app_id 
       ,date
       ,snsid
       ,uid
       ,browser 
       ,country 
       ,install_source 
       ,install_date 
       ,action 
       ,handprint_level 
       ,handprint_in 
from agg_handprint where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';


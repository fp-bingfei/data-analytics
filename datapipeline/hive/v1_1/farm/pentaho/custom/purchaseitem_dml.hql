USE farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size=134217728;
SET hive.exec.max.dynamic.partitions=2000;
SET hive.exec.max.dynamic.partitions.pernode=2000;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.stats.autogather=false;

set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};

MSCK REPAIR TABLE raw_purchaseitem_seq;

INSERT OVERWRITE TABLE raw_purchaseitem_daily PARTITION (app, dt)
SELECT 
    key,
    ts,
    ts_pretty,
    browser,
    browser_version,
    coins_cost,
    country_code,
    event,
    install_source,
    install_ts,
    install_ts_pretty,
    ip,
    item_id,
    item_name,
    item_quantity,
    item_type,
    lang,
    level,
    os,
    os_version,
    rc_bal,
    rc_cost,
    snsid,
    src,
    uid,
    app,
    '${hiveconf:rpt_date}' dt
FROM 
    raw_purchaseitem_seq
WHERE 
    (
        (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
        OR
        (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
    )
    AND
    to_date(ts_pretty) = '${hiveconf:rpt_date}'
;

exit;


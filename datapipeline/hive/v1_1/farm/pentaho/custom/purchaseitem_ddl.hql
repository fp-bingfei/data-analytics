USE farm_1_1;

DROP TABLE IF EXISTS raw_purchaseitem_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_purchaseitem_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    coins_cost   DOUBLE,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    item_id   STRING,
    item_name   STRING,
    item_quantity   BIGINT,
    item_type   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    rc_bal   BIGINT,
    rc_cost   BIGINT,
    snsid   STRING,
    src   STRING,
    uid   STRING
)
PARTITIONED BY(
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/PurchaseItem'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_purchaseitem_daily;

CREATE EXTERNAL TABLE raw_purchaseitem_daily (
    key   string,
    ts   string,
    ts_pretty   timestamp,
    browser   string,
    browser_version   string,
    coins_cost   double,
    country_code   string,
    event   string,
    install_source   string,
    install_ts   string,
    install_ts_pretty   timestamp,
    ip   string,
    item_id   string,
    item_name   string,
    item_quantity   bigint,
    item_type   string,
    lang   string,
    level   bigint,
    os   string,
    os_version   string,
    rc_bal   bigint,
    rc_cost   bigint,
    snsid   string,
    src   string,
    uid   string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_purchaseitem_daily'
TBLPROPERTIES ('serialization.null.format'='');

USE farm_1_1;

-- FarmEvent
DROP TABLE IF EXISTS raw_farmevent;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_farmevent (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    event_type   STRING,
    storage_orig   BIGINT,
    storage_new   BIGINT,
    item_name   STRING,
    item_id   BIGINT,
    points   BIGINT,
    key_id   BIGINT
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key",
    "mapping.key_id"= "key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/FarmEvent'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_farmevent_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_farmevent_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    event_type   STRING,
    storage_orig   BIGINT,
    storage_new   BIGINT,
    item_name   STRING,
    item_id   BIGINT,
    points   BIGINT,
    key_id   BIGINT
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key",
    "mapping.key_id"= "key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/FarmEvent'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_farmevent_daily;

CREATE EXTERNAL TABLE raw_farmevent_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    event_type string,
    storage_orig bigint,
    storage_new bigint,
    item_name string,
    item_id bigint,
    points bigint,
    key_id bigint
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_farmevent_daily'
TBLPROPERTIES ('serialization.null.format'='');


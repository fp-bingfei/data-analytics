USE farm_1_1;

-- VIP
DROP TABLE IF EXISTS raw_vip_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_vip_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    uid string,
    os string,
    country_code string,
    lang string,
    ip string,
    os_version string,
    browser_version string,
    snsid string,
    fb_source string,
    install_ts_pretty string,
    level bigint,
    install_ts string,
    event string,
    browser string,
    install_source string,
    properties map<string,string>
    
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"= "@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/vip_points_in'
TBLPROPERTIES('serialization.null.format'='');    


DROP TABLE IF EXISTS raw_vip_daily;

CREATE EXTERNAL TABLE raw_vip_daily (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    uid string,
    os string,
    country_code string,
    lang string,
    ip string,
    os_version string,
    browser_version string,
    snsid string,
    fb_source string,
    install_ts_pretty string,
    level bigint,
    install_ts string,
    event string,
    browser string,
    install_source string,
    properties map<string,string>
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_vip_daily'
TBLPROPERTIES ('serialization.null.format'='');


USE farm_1_1;

DROP TABLE IF EXISTS raw_handprint_seq;

CREATE EXTERNAL TABLE IF NOT EXISTS raw_handprint_seq (
    key   STRING,
    ts   STRING,
    ts_pretty   TIMESTAMP,
    browser   STRING,
    browser_version   STRING,
    country_code   STRING,
    event   STRING,
    install_source   STRING,
    install_ts   STRING,
    install_ts_pretty   TIMESTAMP,
    ip   STRING,
    lang   STRING,
    level   BIGINT,
    os   STRING,
    os_version   STRING,
    snsid   STRING,
    uid   STRING,
    fb_source STRING,
    action   STRING,
    handprint_in   BIGINT,
    handprint_qty   BIGINT,
    handprint_level   BIGINT,
    reward_type STRING
)
PARTITIONED BY (
    app STRING,
    year int,
    month INT,
    day INT,
    hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
    "mapping.ts" = "@ts",
    "mapping.ts_pretty" ="@ts_pretty",
    "mapping.key"="@key"
)
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_seq/Handprint'
TBLPROPERTIES('serialization.null.format'='');


DROP TABLE IF EXISTS raw_handprint_daily;

CREATE EXTERNAL TABLE raw_handprint_daily (
    key string, 
    ts string, 
    ts_pretty timestamp, 
    browser string, 
    browser_version string, 
    country_code string, 
    event string, 
    install_source string, 
    install_ts string, 
    install_ts_pretty timestamp, 
    ip string, 
    lang string, 
    level bigint, 
    os string, 
    os_version string, 
    snsid string, 
    uid string, 
    fb_source string, 
    action string, 
    handprint_in bigint, 
    handprint_qty bigint, 
    handprint_level bigint, 
    reward_type string
)
PARTITIONED BY ( 
    app string, 
    dt string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_handprint_daily'
TBLPROPERTIES ('serialization.null.format'='');


CREATE EXTERNAL TABLE `agg_handprint`(
  `key` string, 
  `date` string, 
  `snsid` string, 
  `uid` string, 
  `browser` string, 
  `country` string, 
  `install_source` string, 
  `install_date` string, 
  `action` string, 
  `handprint_level` bigint, 
  `handprint_in` bigint)
PARTITIONED BY ( 
  `app` string, 
  `dt` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_handprint'
TBLPROPERTIES ('serialization.null.format'='');
  

CREATE  TABLE `copy_agg_handprint`(
  `app_id` string, 
  `date` string, 
  `snsid` string, 
  `uid` string, 
  `browser` string, 
  `country` string, 
  `install_source` string, 
  `install_date` string, 
  `action` string, 
  `handprint_level` bigint, 
  `handprint_in` bigint)
PARTITIONED BY ( 
  `app` string, 
  `dt` string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/copy/agg_handprint'
TBLPROPERTIES('serialization.null.format'='');


---------------------
--redshift
---------------------

CREATE TABLE processed.agg_handprint
(
	app_id VARCHAR(64) NOT NULL ENCODE bytedict,
	date DATE ENCODE delta,
	snsid VARCHAR(50) ENCODE lzo,
	uid  VARCHAR(64)  ENCODE lzo DISTKEY,
	browser VARCHAR(32) ENCODE bytedict,
	country VARCHAR(64) ENCODE bytedict,
	install_source VARCHAR(1024) ENCODE bytedict,
	install_date DATE ENCODE delta,
	action VARCHAR(1024) ENCODE bytedict,
	handprint_level BIGINT,
	handprint_in BIGINT
)
SORTKEY
(
	app_id,
	date
);
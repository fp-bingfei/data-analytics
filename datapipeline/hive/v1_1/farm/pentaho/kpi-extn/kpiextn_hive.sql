
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};



-- Populate Fact TradeOrder --

insert OVERWRITE TABLE fact_tradeorders PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select     to_date(ts_pretty) as date
          ,to_date(install_ts_pretty) as install_date
          ,level
          ,action
          ,experience
          ,reward_coins
          ,special_num
          ,special_item
            ,sum(exchange_num_1)  as exchange_num_1
          ,exchange_item_1
          ,sum(exchange_num_2)  as exchange_num_2
          ,exchange_item_2
          ,sum(exchange_num_3)  as exchange_num_3
          ,exchange_item_3
          ,count(1) as times
from      raw_tradeorders_daily
where     app='${hiveconf:rpt_app}'
and       dt='${hiveconf:rpt_date}'
group by     to_date(ts_pretty)
          ,to_date(install_ts_pretty)
          ,level
          ,action
          ,experience
          ,reward_coins
          ,special_num
          ,special_item
          ,exchange_item_1
          ,exchange_item_2
          ,exchange_item_3
;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_tradeorders DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_fact_tradeorders PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT
date
    ,install_date
    ,level
    ,action
    ,experience
    ,reward_coins
    ,special_num
    ,special_item
    ,exchange_num_1
    ,exchange_item_1
    ,exchange_num_2
    ,exchange_item_2
    ,exchange_num_3
    ,exchange_item_3
    ,times
    ,app
FROM fact_tradeorders
where app='${hiveconf:rpt_app}'
and       dt='${hiveconf:rpt_date}';

-- Delete Queries
delete from processed.agg_kpi where date >= '${RPT_DATE_D30}' AND date <= '${RPT_DATE}' and app_id='${RPT_APP}';
delete from processed.agg_iap where date = '${RPT_DATE}' and app_id='${RPT_APP}';
delete from processed.agg_tutorial where install_date > '${RPT_DATE_D60}' and app_id='${RPT_APP}';
delete from processed.agg_quest where install_date > '${RPT_DATE_D60}' and app_id='${RPT_APP}';
delete from processed.fact_tradeorders where date = '${RPT_DATE}' and app_id='${RPT_APP}';

-- Copy Scripts

copy processed.fact_tradeorders from 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_tradeorders/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.agg_kpi from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_kpi/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.agg_iap from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_iap/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.agg_tutorial from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_tutorial/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;
copy processed.agg_quest from 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_quest/app=${RPT_APP}/dt=${RPT_DATE}/' CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  DELIMITER '\t' GZIP EMPTYASNULL maxerror 5;


-- Delete Queries in redshift
delete from processed.agg_tradeorders_action where date = '${RPT_DATE}' and app_id='${RPT_APP}';
delete from processed.agg_tradeorders_item where date = '${RPT_DATE}' and app_id='${RPT_APP}';

-- Insert Queries in redshift

--insert data to agg_tradeorders_action
insert into processed.agg_tradeorders_action
select date
      ,app_id
      ,level
      ,action
      ,sum(times)
from  processed.fact_tradeorders
where date = '${RPT_DATE}' and app_id = '${RPT_APP}' 
group by 1,2,3,4;

--insert data to agg_tradeorders_item
insert into processed.agg_tradeorders_item
select date
      ,app_id
      ,level
      ,action
      ,exchange_item_1 as item
      ,sum(exchange_num_1) as item_num
      ,count(exchange_item_1) as item_times
from  processed.fact_tradeorders
where date = '${RPT_DATE}' and app_id = '${RPT_APP}'      
group by 1,2,3,4,5
union all
select date
      ,app_id
      ,level
      ,action
      ,exchange_item_2 as item
      ,sum(exchange_num_2) as item_num
      ,count(exchange_item_2) as item_times
from  processed.fact_tradeorders 
where exchange_item_2 is not null 
and date = '${RPT_DATE}' and app_id = '${RPT_APP}'    
group by 1,2,3,4,5
union all
select date
      ,app_id
      ,level
      ,action
      ,exchange_item_3 as item
      ,sum(exchange_num_3) as item_num
      ,count(exchange_item_3) as item_times
from  processed.fact_tradeorders 
where exchange_item_3 is not null  
and date = '${RPT_DATE}' and app_id = '${RPT_APP}'   
group by 1,2,3,4,5;

-------------------------------
-- ff_marketing_kpi.sql
-------------------------------
create temporary table last_date as
select max(date) as date
from processed.fact_dau_snapshot;


delete from processed.tab_marketing_kpi
using last_date d
where datediff('day',install_date,d.date) <= 120;


create temp table tmp_install_users_d3_payers as
select d.user_key, max(d.is_payer) as d3_payer
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date -3 and datediff(day,u.install_date,d.date)<=3
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d7_revenue as
select d.user_key, sum(d.revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-7 and datediff(day,u.install_date,d.date)<=7
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d30_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-30 and datediff(day,u.install_date,d.date)<=30
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d90_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-90 and datediff(day,u.install_date,d.date)<=90
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d120_revenue as
select d.user_key,  sum(revenue_usd) as revenue
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where u.install_date <= l.date-120 and datediff(day,u.install_date,d.date)<=120
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d1_retained_users as
select d.user_key,  1 as d1_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on d.user_key = u.user_key
join last_date l
on 1=1
where d.date - u.install_date = 1
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d7_retained_users as
select d.user_key,  1 as d7_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on u.user_key = d.user_key
join last_date l
on 1=1
where d.date - u.install_date = 7
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d15_retained_users as
select d.user_key,  1 as d15_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u
on u.user_key = d.user_key
join last_date l
on 1=1
where d.date - u.install_date = 15
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

create temp table tmp_install_users_d30_retained_users as
select d.user_key,  1 as d30_retained_user_cnt
from processed.fact_dau_snapshot d
join processed.dim_user u 
on d.user_key = u.user_key
join last_date l
on 1=1
where d.date - u.install_date = 30
and datediff('day',u.install_date,l.date) <= 120
group by d.user_key;

insert into processed.tab_marketing_kpi
(app, install_date, install_source, install_campaign, install_subpublisher, install_country, install_os, new_user_cnt,d1_new_user_cnt
  ,d3_new_user_cnt,d7_new_user_cnt,d15_new_user_cnt,d30_new_user_cnt,d90_new_user_cnt,d120_new_user_cnt, revenue_usd, d7_revenue_usd, d30_revenue_usd, d90_revenue_usd, d120_revenue_usd,
payer_cnt, d3_payer_cnt, d1_retained_user_cnt, d7_retained_user_cnt, d15_retained_user_cnt, d30_retained_user_cnt)
select
    u.app_id,
    u.install_date,
    case when u.install_source_display = '' or u.install_source is null then 'Unknown' 
          else u.install_source_display end,
    case when u.install_source like 'FF_%' then u.install_source else '' end  as install_campaign,
    u.install_subpublisher,
    case when u.install_country = '' or u.install_country is null then 'Unknown' else u.install_country end,
    case when u.install_os = '' or u.install_os is null then 'Unknown' else u.install_os end,
    count(1) as new_user_cnt,
    sum (case when u.install_date <= ld.date-1 then 1 else 0 end) as d1_new_user_cnt,
    sum (case when u.install_date <= ld.date-3 then 1 else 0 end) as d3_new_user_cnt,
    sum (case when u.install_date <= ld.date-7 then 1 else 0 end) as d7_new_user_cnt,
    sum (case when u.install_date <= ld.date-15 then 1 else 0 end) as d15_new_user_cnt,
    sum (case when u.install_date <= ld.date-30 then 1 else 0 end) as d30_new_user_cnt,
    sum (case when u.install_date <= ld.date-90 then 1 else 0 end) as d90_new_user_cnt,
    sum (case when u.install_date <= ld.date-120 then 1 else 0 end) as d120_new_user_cnt,
    sum(coalesce(u.total_revenue_usd,0)) as revenue_usd,
    sum(coalesce(d7_revenue.revenue,0)) as d7_revenue_usd,
    sum(coalesce(d30_revenue.revenue,0))  as d30_revenue_usd,
    sum(coalesce(d90_revenue.revenue,0))  as d90_revenue_usd,
    sum(coalesce(d120_revenue.revenue,0))  as d120_revenue_usd,
    sum(u.is_payer) as payer_cnt,
    sum(coalesce(d3p.d3_payer,0)) as d3_payer_cnt,
    sum(coalesce(d1_retained_users.d1_retained_user_cnt,0)) as d1_retained_user_cnt,
    sum(coalesce(d7_retained_users.d7_retained_user_cnt,0)) as d7_retained_user_cnt,
    sum(coalesce(d15_retained_users.d15_retained_user_cnt,0)) as d15_retained_user_cnt,
    sum(coalesce(d30_retained_users.d30_retained_user_cnt,0)) as d30_retained_user_cnt
from processed.dim_user u
join (select date from last_date) ld on 1=1
    left join tmp_install_users_d3_payers d3p on u.user_key = d3p.user_key
    left join tmp_install_users_d7_revenue d7_revenue on u.user_key = d7_revenue.user_key
    left join tmp_install_users_d30_revenue d30_revenue on u.user_key = d30_revenue.user_key
    left join tmp_install_users_d90_revenue d90_revenue on u.user_key = d90_revenue.user_key
    left join tmp_install_users_d120_revenue d120_revenue on u.user_key = d120_revenue.user_key
    left join tmp_install_users_d1_retained_users d1_retained_users on u.user_key = d1_retained_users.user_key
    left join tmp_install_users_d7_retained_users d7_retained_users on u.user_key = d7_retained_users.user_key
    left join tmp_install_users_d15_retained_users d15_retained_users on u.user_key = d15_retained_users.user_key
    left join tmp_install_users_d30_retained_users d30_retained_users on u.user_key = d30_retained_users.user_key
where u.install_date <= (select date from last_date)
and datediff('day',u.install_date,ld.date) <= 120
group by 1,2,3,4,5,6,7;

-------------------------------
-- ff_level_progression.sql
-------------------------------
truncate processed.tab_level_dropoff;
insert into processed.tab_level_dropoff
with this_date as (select max(install_date) as date from processed.dim_user)
select
  u.install_date,
  l.level,
  u.level as current_level,
  u.app_id,
  u.install_os,
  u.install_country,
  u.install_source_display,
  u.is_payer,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=3 then 1 else 0 end as is_churned_3days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=7 then 1 else 0 end as is_churned_7days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=14 then 1 else 0 end as is_churned_14days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=21 then 1 else 0 end as is_churned_21days,
  case when l.level=u.level and datediff(day,u.last_login_ts,t.date)>=30 then 1 else 0 end as is_churned_30days,
  count(distinct u.user_key) as user_cnt
from processed.dim_user u
join (select distinct level from processed.dim_user) l on u.level>=l.level
join this_date t on 1=1
where install_date<t.date
group by 1,2,3,4,5,6,7,8,9,10,11,12,13;

-------------------------------
-- ff_retention_ltv.sql
-------------------------------
create temp table player_day as                                                                                        
     SELECT 1 AS day UNION ALL                                                                                        
     SELECT 2 UNION ALL                                                                                               
     SELECT 3 UNION ALL                                                                                               
     SELECT 4 UNION ALL                                                                                               
     SELECT 5 UNION ALL                                                                                               
     SELECT 6 UNION ALL                                                                                               
     SELECT 7 UNION ALL                                                                                               
     SELECT 14 UNION ALL                                                                                              
     SELECT 15 UNION ALL                                                                                              
     SELECT 21 UNION ALL                                                                                              
     SELECT 28 UNION ALL                                                                                              
     SELECT 30 UNION ALL                                                                                              
     SELECT 45 UNION ALL                                                                                              
     SELECT 60 UNION ALL                                                                                              
     SELECT 90 UNION ALL                                                                                              
     SELECT 120 UNION ALL                                                                                             
     SELECT 150 UNION ALL                                                                                             
     SELECT 180 UNION ALL                                                                                             
     SELECT 210 UNION ALL                                                                                             
     SELECT 240 UNION ALL                                                                                             
     SELECT 270 UNION ALL                                                                                             
     SELECT 300 UNION ALL                                                                                             
     SELECT 330 UNION ALL                                                                                             
     SELECT 360                                                                                                       
; 


--create temp table for new users in each install_date
create temp table tmp_new_users as
select  install_date
       ,u.install_os
       ,u.install_country
       ,u.app_id as app_id
       ,u.install_source_display as install_source 
       ,u.install_subpublisher 
       ,u.install_campaign 
       ,u.install_browser
       ,u.language
       ,u.is_payer
       ,count(u.user_key) as new_user_cnt
from processed.dim_user u
join processed.fact_dau_snapshot d
on u.user_key = d.user_key
and u.install_date=d.date
group by 1,2,3,4,5,6,7,8,9,10;

--create temp table to get the last date in dau table.
-- create temporary table last_date
-- as
-- select max(date) as date
-- from processed.fact_dau_snapshot;


--create cube to calculate the retention
create temp table player_day_cube 
as
select  pd.day as player_day
       ,nu.install_date
       ,nu.install_os
       ,nu.install_country
       ,nu.app_id
       ,nu.install_source
       ,nu.install_subpublisher
       ,nu.install_campaign
       ,nu.install_browser
       ,nu.language
       ,nu.is_payer
       ,nu.new_user_cnt
from   tmp_new_users nu
join   player_day pd
on 1=1
join   last_date d
on 1=1
where  datediff('day',nu.install_date,d.date) >= pd.day;       

--create agg_retention table

drop table processed.agg_retention;
create table processed.agg_retention as
select  cube.player_day
       ,cube.install_date
       ,cube.install_os
       ,cube.install_country
       ,cube.app_id
       ,cube.install_source
       ,cube.install_subpublisher
       ,cube.install_campaign
       ,cube.install_browser
       ,cube.language
       ,cube.is_payer
       ,cube.new_user_cnt
       ,coalesce(r.retained_user_cnt,0) as retained_user_cnt
from   player_day_cube cube
left join
      (
        select   datediff('day',u.install_date,du.date) as player_day
                ,u.install_date
                ,u.install_os
                ,u.install_country
                ,u.app_id
                ,u.install_source
                ,u.install_subpublisher
                ,u.install_campaign
                ,u.install_browser
                ,u.language
                ,u.is_payer
                ,count(distinct du.user_key) as retained_user_cnt
        from    processed.fact_dau_snapshot du
        join    processed.dim_user u
        on      du.user_key = u.user_key
        where   datediff('day',u.install_date,du.date) in  (select day from player_day)
        group by 1,2,3,4,5,6,7,8,9,10,11                        
      )r
on  cube.player_day = r.player_day
and cube.install_date = r.install_date
and coalesce(cube.app_id,'') = coalesce(r.app_id,'')
and coalesce(cube.install_os,'') = coalesce(r.install_os,'') 
and coalesce(cube.install_country,'') = coalesce(r.install_country,'') 
and coalesce(cube.install_source,'') = coalesce(r.install_source,'')
and coalesce(cube.install_subpublisher,'')= coalesce(r.install_subpublisher,'')
and coalesce(cube.install_campaign,'') = coalesce(r.install_campaign,'')
and coalesce(cube.install_browser,'') = coalesce(r.install_browser,'')
and coalesce(cube.language,'') = coalesce(r.language,'')
and cube.is_payer = r.is_payer;

------------------------LTV--------------------------------------------------------------
drop table  processed.agg_ltv;
create table processed.agg_ltv as
select
           cube.player_day
          ,cube.install_date
          ,cube.install_os
          ,cube.install_country
          ,cube.app_id
          ,cube.install_source
          ,cube.install_subpublisher
          ,cube.install_campaign
          ,cube.install_browser
          ,cube.language
          ,cube.is_payer
          ,sum(cube.new_user_cnt) as new_user_cnt
          ,coalesce(r.cumulative_revenue_usd, 0.0000) as revenue_usd
from      player_day_cube cube
left join 
          (
            select    p.day as player_day
                     ,u.install_date
                     ,u.install_os
                     ,u.install_country
                     ,u.app_id
                     ,u.install_source
                     ,u.install_subpublisher
                     ,u.install_campaign
                     ,u.install_browser
                     ,u.language
                     ,u.is_payer
                     ,sum(case when revenue_usd is null then 0 else revenue_usd end) as cumulative_revenue_usd
            from     processed.fact_dau_snapshot du
            join     processed.dim_user u
            on       du.user_key = u.user_key     
            join     player_day p
            on       datediff('day',u.install_date,du.date) <= p.day
            group by 1,2,3,4,5,6,7,8,9,10,11                       
           )r
on  cube.player_day = r.player_day
    and cube.install_date = r.install_date
    and coalesce(cube.install_os,'') = coalesce(r.install_os,'')
    and coalesce(cube.install_country,'') = coalesce(r.install_country,'')
    and coalesce(cube.app_id,'') = coalesce(r.app_id,'')
    and coalesce(cube.install_source,'') = coalesce(r.install_source,'')
    and coalesce(cube.install_subpublisher,'') = coalesce(r.install_subpublisher,'')
    and coalesce(cube.install_campaign,'') = coalesce(r.install_campaign,'')
    and coalesce(cube.language,'') = coalesce(r.language,'')
    and coalesce(cube.install_browser,'') = coalesce(r.install_browser,'')
    and COALESCE(cube.is_payer,0) = COALESCE(r.is_payer,0)
group by 1,2,3,4,5,6,7,8,9,10,11,13
;


------------------------Cheating list--------------------------------------------------------------
delete from processed.cheating_list                                                      
where date >= '${RPT_DATE_D30}';  
              
insert into processed.cheating_list
select t1.app_id
       ,t1.date
       ,coalesce(t2.facebook_id,'Unkown') as snsid
       ,t1.rc_in
from processed.fact_dau_snapshot t1
left join processed.dim_user t2
on t1.user_key = t2.user_key
where t1.rc_in >1000
and t1.date >= '${RPT_DATE_D30}'
;

commit;
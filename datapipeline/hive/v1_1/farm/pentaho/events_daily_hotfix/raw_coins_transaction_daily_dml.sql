
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_year = ${RPT_YEAR};
set rpt_month = ${RPT_MONTH};
set rpt_day = ${RPT_DAY};
set rpt_date_plus1 = ${RPT_DATE_PLUS1};
set rpt_year_plus1 = ${RPT_YEAR_PLUS1};
set rpt_month_plus1 = ${RPT_MONTH_PLUS1};
set rpt_day_plus1 = ${RPT_DAY_PLUS1};

ALTER TABLE raw_coins_transaction_seq DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day});

ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

ALTER TABLE raw_coins_transaction_seq DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1});

ALTER TABLE raw_coins_transaction_seq ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);  

INSERT OVERWRITE TABLE raw_coins_transaction_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
key,
ts,
ts_pretty,
action,
action_detail,
browser,
browser_version,
coins_bal,
coins_in,
coins_out,
country_code,
event,
install_source,
install_ts,
install_ts_pretty,
ip,
lang,
level,
location,
os,
os_version,
snsid,
uid 
FROM raw_coins_transaction_seq 
WHERE 
app='${hiveconf:rpt_app}' AND
(
(year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day})
OR
(year=${hiveconf:rpt_year_plus1} AND month = ${hiveconf:rpt_month_plus1} AND day = ${hiveconf:rpt_day_plus1})
) AND 
to_date(ts_pretty) = '${hiveconf:rpt_date}' 
;


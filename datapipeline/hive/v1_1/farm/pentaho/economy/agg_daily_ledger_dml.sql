
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;


-- fix for npe --

SET hive.auto.convert.join=false;
SET hive.input.format= org.apache.hadoop.hive.ql.io.HiveInputFormat;  

-----------------

set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};

-- Populate Agg Ledger --

INSERT OVERWRITE TABLE agg_daily_ledger_test PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(date,p.app,COALESCE(p.level,0),COALESCE(q.os,''),COALESCE(q.device,''),
COALESCE(q.browser,''),COALESCE(q.country,''),COALESCE(q.language,''),
COALESCE(p.action_type,''),COALESCE(p.action,''),COALESCE(received_name,''),COALESCE(received_type,''),
COALESCE(spent_name,''),COALESCE(spent_type,''))) AS id,
date,
p.app,
p.level,
q.os,
q.device,
q.browser,
q.country,
q.language,
p.action_type,
p.action,
received_id,
received_name,
received_type,
SUM(COALESCE(received_amount,0))  received_amount,
spent_id,
spent_name,
spent_type,
SUM(COALESCE(spent_amount,0))  spent_amount
FROM
(SELECT * FROM fact_ledger_test WHERE app="${hiveconf:rpt_app}" AND dt='${hiveconf:rpt_date}' ) p
LEFT OUTER JOIN
(SELECT * from dim_user WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}')q ON p.user_key = q.user_key AND p.app = q.app
GROUP BY date,p.app,p.dt, p.level, q.os, q.device, q.browser, q.country,q.language,p.action_type, p.action, received_id,received_name,received_type, spent_id, spent_name, spent_type;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_daily_ledger_test DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE copy_agg_daily_ledger_test PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT
id,
date,
app_id,
level,
os,
device,
browser,
country,
language,
action_type,
action,
received_id,
received_name,
received_type,
received_amount,
spent_id,
spent_name,
spent_type,
spent_amount
FROM agg_daily_ledger_test
WHERE app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

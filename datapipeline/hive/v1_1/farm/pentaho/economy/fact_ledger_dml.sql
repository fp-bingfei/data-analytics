
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;



SET hive.auto.convert.join=false;
SET hive.input.format= org.apache.hadoop.hive.ql.io.HiveInputFormat;  

set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};

-- Populate Fact Ledger --

INSERT OVERWRITE TABLE fact_ledger_test PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(concat(t.app,uid,ts)) id,
dt date,
t.ts_pretty ts,
t.app app_id,
t.uid,
MD5(CONCAT(t.app,uid)) user_key,
NULL session_id,
t.level,
COALESCE(d.action_type,'Others') action_type,
t.action action,
NULL received_id,
CASE WHEN t.rc_in>0 THEN 'Rc' ELSE COALESCE(t.action_detail,'Unknown') END  received_name,
CASE WHEN t.rc_in>0 THEN 'Resource' ELSE '' END  received_type,
CASE WHEN t.rc_in>0 THEN rc_in ELSE 0 END  received_amount,
NULL spent_id,
CASE WHEN t.rc_out>0 THEN 'Rc' ELSE COALESCE(t.action_detail,'Unknown') END  spent_name,
CASE WHEN t.rc_out>0 THEN 'Resource' ELSE '' END  spent_type,
CASE WHEN t.rc_out>0 THEN rc_out ELSE 0 END  spent_amount
FROM raw_rc_transaction_daily t
left join dim_action d
on t.action = d.action
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
UNION ALL  
SELECT
MD5(concat(t.app,uid,ts)) id,
dt date,
t.ts_pretty ts,
t.app app_id,
t.uid,
MD5(CONCAT(t.app,uid)) user_key,
NULL session_id,
t.level,
coalesce(d.action_type,'Others') action_type,
t.action action,
NULL received_id,
CASE WHEN t.coins_in>0 THEN 'Coins' ELSE COALESCE(t.action_detail,'Unknown') END  received_name,
CASE WHEN t.coins_in>0 THEN 'Resource' ELSE '' END  received_type,
CASE WHEN t.coins_in>0 THEN t.coins_in ELSE 0 END  received_amount,
NULL spent_id,
CASE WHEN t.coins_out>0 THEN 'Coins' ELSE COALESCE(t.action_detail,'Unknown') END  spent_name,
CASE WHEN t.coins_out>0 THEN 'Resource' ELSE '' END  spent_type,
CASE WHEN t.coins_out>0 THEN t.coins_out ELSE 0 END  spent_amount
FROM raw_coins_transaction_daily t
left join dim_action d
on t.action = d.action
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';
-----------------------------
-- Query to generate dim_user
-----------------------------

use farm_1_1;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;



SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_yesterday = ${RPT_DATE_D1};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};

-- SET rpt_geo=us

ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
	
--------------------------------------------------------------------------------------------------------------
-- 1. Populate up to yesterday's data where date_start = install_date; store it in rpt_date_yesterday partition
--    Yesterday's dim_user is assumed (was) empty 
--------------------------------------------------------------------------------------------------------------
INSERT OVERWRITE TABLE dim_user PARTITION(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date_yesterday}')
select
tu.user_key AS id,
tu.user_key,
tu.app as app_id,
tu.uid as app_user_id,
coalesce(b.facebook_id,tu.facebook_id) AS facebook_id,
tu.install_ts,
  tu.install_date,
  split(tu.install_source, '::')[0] AS install_source,
  split(tu.install_source, '::')[1] AS install_subpublisher,
  split(tu.install_source, '::')[2] AS install_campaign,
  tu.language AS install_language,
  tu.locale AS install_locale,
  tu.country_code AS install_country_code,
  tu.country AS install_country,
  tu.os AS install_os,
  tu.device AS install_device,
  NULL AS install_device_alias,
  tu.browser AS install_browser,
  tu.gender AS install_gender,
  datediff(tu.install_date, tu.birthday) AS install_age,
  tu.language,
  tu.locale,
  tu.birthday,
  tu.gender,
  tu.country_code,
  tu.country,
  tu.os,
  tu.os_version,
  tu.device,
  NULL AS device_alias,
  tu.browser,
  tu.browser_version,
  NULL AS app_version,
  COALESCE(tl.level_end, tu.level_end, tu.level_start) AS level,
  tl.levelup_ts AS levelup_ts,
  tu.ab_experiment,
  tu.ab_variant,
  if (tp.purchase_cnt > 0, 1, 0) AS is_payer,
  tp.conversion_ts,
  0 AS total_revenue_usd,
  0 AS payment_cnt,
  tu.last_login_ts,
  tld.coins_bal as coin_wallet,
  tld.coins_in,
  tld.coins_out,
  tld.rc_bal as rc_wallet,
  tld.rc_in,
  tld.rc_out, 
  u.email AS email,
  tu.last_ip AS last_ip
FROM tmp_user_daily_login tu
LEFT OUTER JOIN (select app, user_key, min(conversion_ts) as conversion_ts, sum(revenue_usd) AS revenue_usd
  , sum(purchase_cnt) AS purchase_cnt from tmp_user_payment where app='${hiveconf:rpt_app}' 
  and dt <= '${hiveconf:rpt_date_yesterday}'
  group by app, user_key) tp on (tu.app = tp.app and tu.user_key = tp.user_key)
LEFT OUTER JOIN tmp_user_level tl ON (tu.app = tl.app and tu.dt = tl.dt and tu.user_key = tl.user_key )
LEFT OUTER JOIN tmp_ledger_daily tld on (tu.app = tld.app and tu.dt = tld.dt and tu.uid = tld.uid)
LEFT OUTER JOIN user_geo_${hiveconf:rpt_geo} u on (tu.uid = u.uid and u.dt = '${hiveconf:rpt_date}')
LEFT OUTER JOIN (SELECT * FROM update_snsid_backfill WHERE app='${hiveconf:rpt_app}') b on (tu.app = b.app_id  and tu.uid = b.app_user_id)
WHERE tu.app='${hiveconf:rpt_app}' AND tu.date_start = tu.install_date and tu.dt <= '${hiveconf:rpt_date_yesterday}';


--------------------------------------------------------------------------------------------------------------------------
-- 2. Merge up to yesterday's data (date_start = install_date) with all today's data and store it into rpt_date partition
--------------------------------------------------------------------------------------------------------------------------
INSERT OVERWRITE TABLE dim_user PARTITION(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select
  tu.user_key AS id,
  tu.user_key,
  tu.app as app_id,
  tu.uid as app_user_id,
  COALESCE(b.facebook_id,tu.facebook_id) AS facebook_id,
  COALESCE(du.install_ts,tu.install_ts) as install_ts,
  COALESCE(du.install_date, tu.install_date) as install_date,
  split(tu.install_source, '::')[0] AS install_source,
  split(tu.install_source, '::')[1] AS install_subpublisher,
  split(tu.install_source, '::')[2] AS install_campaign,
  case when tu.date_start = tu.install_date then tu.language else du.install_language end AS install_language,
  case when tu.date_start = tu.install_date then tu.locale else du.install_locale end AS install_locale,
  case when tu.date_start = tu.install_date then tu.country_code else du.install_country_code end AS install_country_code,
  case when tu.date_start = tu.install_date then tu.country else du.install_country end AS install_country,
  case when tu.date_start = tu.install_date then tu.os else du.install_os end AS install_os,
  case when tu.date_start = tu.install_date then tu.device else du.install_device end AS install_device,
  case when tu.date_start = tu.install_date then NULL else du.install_device_alias end AS install_device_alias,
  case when tu.date_start = tu.install_date then tu.browser else du.install_browser end AS install_browser,
  case when tu.date_start = tu.install_date then tu.gender else du.install_gender end AS install_gender,
  case when tu.date_start = tu.install_date then datediff(tu.install_date, tu.birthday) else du.install_age end AS install_age,
  tu.language,
  tu.locale,
  tu.birthday,
  tu.gender,
  tu.country_code,
  tu.country,
  tu.os,
  tu.os_version,
  tu.device,
  NULL AS device_alias,
  tu.browser,
  tu.browser_version,
  NULL AS app_version,
  COALESCE(tl.level_end, tu.level_end, tu.level_start) AS level,
  COALESCE(tl.levelup_ts,du.levelup_ts) AS levelup_ts,
  tu.ab_experiment,
  tu.ab_variant,
  COALESCE (du.is_payer, if (tp.purchase_cnt > 0, 1, 0))  is_payer,
  COALESCE (du.conversion_ts, tp.conversion_ts) as conversion_ts,
  (COALESCE(tp.revenue_usd, 0) + COALESCE(du.total_revenue_usd, 0)) AS total_revenue_usd,
  (COALESCE(tp.purchase_cnt, 0) + COALESCE(du.payment_cnt, 0)) AS payment_cnt,
  tu.last_login_ts,
  COALESCE(tld.coins_bal, du.coin_wallet) as coin_wallet,
  COALESCE(tld.coins_in, du.coins_in) AS coins_in,
  COALESCE(tld.coins_out, du.coins_out) AS coins_out,
  COALESCE(tld.rc_bal, du.rc_wallet) as rc_wallet,
  COALESCE(tld.rc_in, du.rc_in) AS rc_in,
  COALESCE(tld.rc_out, du.rc_out) AS rc_out,
  COALESCE (u.email, du.email) AS email,
  COALESCE (tu.last_ip, du.last_ip) AS last_ip
FROM (select * from (select *,row_number() over (partition by app, uid order by dt desc) rnum from tmp_user_daily_login where (date_start != install_date and dt < '${hiveconf:rpt_date}')  OR dt ='${hiveconf:rpt_date}') X where rnum=1) tu 
LEFT OUTER JOIN dim_user du on (du.app='${hiveconf:rpt_app}' and du.dt = '${hiveconf:rpt_date_yesterday}' and tu.app = du.app and tu.user_key = du.user_key )
LEFT OUTER JOIN (select app, user_key, min(conversion_ts) as conversion_ts, sum(revenue_usd) AS revenue_usd
  , sum(purchase_cnt) AS purchase_cnt from tmp_user_payment where app='${hiveconf:rpt_app}' and dt <= '${hiveconf:rpt_date}'
  group by app, user_key) tp on (tu.app = tp.app and tu.user_key = tp.user_key) 
LEFT OUTER JOIN tmp_user_level tl ON (tu.app = tl.app and tu.dt = tl.dt and tu.user_key = tl.user_key )
LEFT OUTER JOIN tmp_ledger_daily tld on (tu.app = tld.app and tu.dt = tld.dt and tu.uid = tld.uid)
LEFT OUTER JOIN user_geo_${hiveconf:rpt_geo} u on (tu.uid = u.uid and u.dt = '${hiveconf:rpt_date}')
LEFT OUTER JOIN (SELECT * FROM update_snsid_backfill WHERE app='${hiveconf:rpt_app}') b on (tu.app = b.app_id  and tu.uid = b.app_user_id)
WHERE tu.app='${hiveconf:rpt_app}' AND tu.dt <= '${hiveconf:rpt_date}'
UNION ALL
SELECT du.id,du.user_key,du.app_id,du.app_user_id,du.facebook_id,du.install_ts,du.install_date
, du.install_source,du.install_subpublisher,du.install_campaign,du.install_language,du.install_locale
,du.install_country_code,du.install_country,du.install_os,du.install_device,du.install_device_alias,du.install_browser
,du.install_gender,du.install_age,du.language,du.locale,du.birthday,du.gender,du.country_code,du.country,du.os
,du.os_version,du.device,du.device_alias,du.browser,du.browser_version,du.app_version,du.level,du.levelup_ts
,du.ab_experiment,du.ab_variant,du.is_payer,du.conversion_ts,du.total_revenue_usd,du.payment_cnt,du.last_login_ts
,du.coin_wallet, du.coins_in, du.coins_out, du.rc_wallet, du.rc_in, du.rc_out, du.email, du.last_ip
  FROM dim_user du 
LEFT OUTER JOIN (select distinct app, uid, user_key from tmp_user_daily_login where (date_start != install_date and dt < '${hiveconf:rpt_date}')  OR dt ='${hiveconf:rpt_date}') tu ON
  (tu.app = du.app AND tu.user_key = du.user_key )
  WHERE du.app='${hiveconf:rpt_app}' AND du.dt='${hiveconf:rpt_date_yesterday}' AND tu.uid IS NULL;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
  
ALTER TABLE copy_dim_user DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');

INSERT OVERWRITE TABLE copy_dim_user PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT id, user_key, app_id, app_user_id, facebook_id, install_ts , install_date, install_source
, install_subpublisher, install_campaign, install_language, install_locale, install_country_code, install_country
, install_os, install_device, install_device_alias, install_browser, install_gender, install_age, language, locale
, birthday, gender, country_code, country, os, os_version, device, device_alias, browser, browser_version, app_version
, level, levelup_ts, ab_experiment, ab_variant, is_payer, conversion_ts, total_revenue_usd, payment_cnt, last_login_ts
, coin_wallet DOUBLE, coins_in, coins_out, rc_wallet DOUBLE, rc_in, rc_out, email, last_ip
FROM dim_user WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';

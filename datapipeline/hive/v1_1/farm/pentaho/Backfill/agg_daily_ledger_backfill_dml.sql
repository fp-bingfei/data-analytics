
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;

set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;

SET hive.stats.autogather=false;


-- fix for npe --

SET hive.auto.convert.join-false;
SET hive.input.format= org.apache.hadoop.hive.ql.io.HiveInputFormat;  

-----------------

ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;

set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};

-- Populate Agg Ledger --

INSERT OVERWRITE TABLE agg_daily_ledger PARTITION (app , dt)
SELECT
MD5(CONCAT(date,p.app,COALESCE(p.level,0),COALESCE(q.os,''),COALESCE(q.device,''),
COALESCE(q.browser,''),COALESCE(q.country,''),COALESCE(q.language,''),
COALESCE(transaction_type,''),COALESCE(received_name,''),COALESCE(received_type,''),
COALESCE(spent_name,''),COALESCE(spent_type,''))) AS id,
date,
p.app,
p.level,
q.os,
q.device,
q.browser,
q.country,
q.language,
transaction_type,
received_id,
received_name,
received_type,
SUM(COALESCE(received_amount,0))  received_amount,
spent_id,
spent_name,
spent_type,
SUM(COALESCE(spent_amount,0))  spent_amount,
COUNT(distinct p.user_key)   purchaser_cnt,
0 user_cnt,
p.app,
p.dt
FROM 
(SELECT * FROM fact_ledger WHERE app="${hiveconf:rpt_app}" AND dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}')p
LEFT OUTER JOIN
(SELECT * from dim_user WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}')q ON p.user_key = q.user_key
GROUP BY date,p.app,p.level, q.os, q.device, q.browser, q.country,q.language,transaction_type, received_id,received_name,received_type, spent_id, spent_name, spent_type,p.dt;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_agg_daily_ledger partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select id,date,app_id,level,os,device,browser,country,language,transaction_type,received_id,received_name,
received_type,received_amount,spent_id,spent_name,spent_type,spent_amount,purchaser_cnt,user_cnt
from agg_daily_ledger where app='${hiveconf:rpt_app}' and dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

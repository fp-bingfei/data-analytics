
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;


SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;

set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;

SET hive.stats.autogather=false;

ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;

set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};


INSERT OVERWRITE TABLE fact_dau_snapshot PARTITION (app,dt)
SELECT
MD5(concat(s.user_key,s.date_start)) id,
s.user_key ,
s.date_start,
s.app_id,
s.app_version,
COALESCE(l.level_start, s.level_start) level_start,
COALESCE(l.level_end,s.level_end,s.level_start) level_end,
s.os,
s.os_version,
s.device,
s.browser,
s.browser_version,
s.country_code,
s.country,
s.language,
s.ab_experiment,
s.ab_variant,
if(s.date_start=s.install_date, 1 ,0) is_new_user ,
rc_in ,
coins_in ,
rc_out ,
coins_out ,
rc_bal rc_wallet,
coins_bal coin_wallet,
if (s.date_start >= to_date(p.conversion_ts),1,0) is_payer ,
if (s.date_start = to_date(p.conversion_ts),1,0) is_converted_today,
p.revenue_usd ,
p.purchase_cnt ,
s.session_cnt ,
null,
s.app,
s.dt
from (select * from tmp_user_daily_login where app='${hiveconf:rpt_app}' and dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}')s
LEFT OUTER JOIN (select * from tmp_user_payment where app='${hiveconf:rpt_app}'and dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}') p
on s.user_key=p.user_key and s.dt=p.dt and s.app=p.app
LEFT OUTER JOIN (select * from tmp_user_level where app='${hiveconf:rpt_app}' and dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}')l
on s.user_key=l.user_key and s.dt=l.dt and s.app=l.app
LEFT OUTER JOIN (select * from tmp_ledger_daily where app='${hiveconf:rpt_app}' and dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}')d
on s.user_key=d.user_key and s.dt=d.dt and s.app=d.app;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_fact_dau_snapshot partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') select 
id,user_key,date,app_id,app_version,level_start,level_end,os,os_version,device,browser,browser_version,country_code,country,language,ab_experiment,ab_variant,is_new_user,rc_in,coins_in,rc_out,coins_out,rc_wallet,coin_wallet,is_payer,is_converted_today,revenue_usd,payment_cnt,session_cnt,playtime_sec 
from fact_dau_snapshot where app='${hiveconf:rpt_app}' and dt>='${hiveconf:rpt_date_start}' and dt<='${hiveconf:rpt_date}';

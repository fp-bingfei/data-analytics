
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;

set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;

SET hive.stats.autogather=false;

ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;


set rpt_year_start = ${RPT_YEAR_START};
set rpt_month_start = ${RPT_MONTH_START};
set rpt_day_start = ${RPT_DAY_START};
set rpt_year_end = ${RPT_YEAR_END};
set rpt_month_end = ${RPT_MONTH_END};
set rpt_day_end = ${RPT_DAY_END};
set rpt_app = ${RPT_APP};
  
INSERT OVERWRITE TABLE raw_tutorial_daily PARTITION (app, dt)
SELECT
 key,
 ts,
 ts_pretty,
 browser,
 browser_version,
 country_code,
 event,
 install_source,
 install_ts,
 install_ts_pretty,
 ip,
 lang,
 level,
 os,
 os_version,
 snsid,
 step,
 uid,
  app,
  to_date(ts_pretty)
FROM 
  raw_tutorial 
  WHERE 
  app = '${hiveconf:rpt_app}' AND
  ( year >=${hiveconf:rpt_year_start} AND 
  month >= ${hiveconf:rpt_month_start} AND 
  day >= ${hiveconf:rpt_day_start} ) 
  AND 
  ( year <=${hiveconf:rpt_year_end} AND 
  month <= ${hiveconf:rpt_month_end} AND 
  day <= ${hiveconf:rpt_day_end} ) ;

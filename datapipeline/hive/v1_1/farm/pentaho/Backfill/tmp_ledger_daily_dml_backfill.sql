----------------------------------
-- Query to generate tmp_ledger_daily
----------------------------------
use farm_1_1;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;



SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};


ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;


INSERT OVERWRITE TABLE tmp_ledger_daily PARTITION (app='${hiveconf:rpt_app}',dt)
select dt AS date_start, app AS app_name, md5(concat(app, uid)) AS user_key, uid
, max(coins_bal) as coins_bal, max(coins_in) as coins_in, max(coins_out) as coins_out
, max(rc_bal) AS rc_bal, max(rc_in) as rc_in, max(rc_out) as rc_out, dt FROM
(
select app, dt, uid, 0 as coins_bal,  sum(coins_in) AS coins_in, sum(coins_out) AS coins_out, 0 as rc_bal, 0 as rc_in, 0 as rc_out
from raw_coins_transaction_daily 
where app='${hiveconf:rpt_app}' AND dt >= '${hiveconf:rpt_date_start}' AND dt <='${hiveconf:rpt_date}' group by app, dt, uid
UNION ALL
select app, dt, uid, coins_bal, 0 as coins_in, 0 as coins_out,0 as rc_bal, 0 as rc_in, 0 as rc_out  from (
select app, dt, uid, coins_bal, row_number() over (partition by app,dt, uid order by ts_pretty desc) AS rownum 
from raw_coins_transaction_daily 
where app='${hiveconf:rpt_app}' AND dt >= '${hiveconf:rpt_date_start}' AND dt <='${hiveconf:rpt_date}') cb where  rownum = 1
UNION ALL
select app, dt, uid, 0 as coins_bal, 0 as coins_in, 0 as coins_out ,  rc_bal, 0 as rc_in, 0 as rc_out from (
select app, dt, uid, rc_bal, row_number() over (partition by app,dt, uid order by ts_pretty desc) AS rownum 
from raw_rc_transaction_daily 
where app='${hiveconf:rpt_app}' AND dt >= '${hiveconf:rpt_date_start}' AND dt <='${hiveconf:rpt_date}') cb where  rownum = 1
UNION ALL
select app, dt, uid, 0 as coins_bal, 0 as coins_in, 0 as coins_out , 0 as rc_bal, sum(rc_in) AS rc_in, sum(rc_out) AS rc_out
from raw_rc_transaction_daily 
where app='${hiveconf:rpt_app}' AND dt >= '${hiveconf:rpt_date_start}' AND dt <='${hiveconf:rpt_date}' group by app, dt, uid
) tmp
group by dt, app, md5(concat(app, uid)), uid
distribute by date_start;


use farm_1_1;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;



SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};
set rpt_date_d60 = ${RPT_DATE_D60};


ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;


INSERT OVERWRITE TABLE agg_iap PARTITION (app, dt)
SELECT   
r.dt,
u.app_id,
u.app_version,
r.level,
u.install_date,
u.install_source,
d.country,
d.os,
d.ab_experiment,
d.ab_variant,
CASE WHEN u.conversion_ts=r.ts THEN 1 ELSE 0 END conversion_purchase,
product_id,
product_type,
SUM(r.revenue_usd) revenue_usd,
COUNT(1) purchase_cnt,
COUNT(distinct u.user_key) purchase_user_cnt,
u.app_id,
r.dt
FROM 
(SELECT * FROM fact_revenue WHERE app='${hiveconf:rpt_app}' AND (dt >='${hiveconf:rpt_date_start}' OR dt >='${hiveconf:rpt_date_d60}') AND dt <='${hiveconf:rpt_date}') r
JOIN 
(SELECT * FROM fact_dau_snapshot WHERE app='${hiveconf:rpt_app}' AND (dt >='${hiveconf:rpt_date_start}' OR dt >='${hiveconf:rpt_date_d60}') AND dt <='${hiveconf:rpt_date}') d 
ON r.user_key=d.user_key AND r.app=d.app AND r.dt = d.dt
JOIN
(SELECT * FROM dim_user WHERE app='${hiveconf:rpt_app}'  AND dt='${hiveconf:rpt_date}' ) u 
ON u.user_key=r.user_key AND u.app=r.app
GROUP BY 
r.dt, 
u.app_id, 
u.app_version,
r.level, 
u.install_date,
u.install_source,
d.country,
d.os,
d.ab_experiment,
d.ab_variant,
CASE WHEN u.conversion_ts=r.ts THEN 1 ELSE 0 END,
product_id, 
product_type;


-- Parquet to Text File - To enable copy to Redshift --


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_agg_iap partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
SELECT
date, 
app_id, 
app_version, 
level , 
install_date, 
install_source, 
country, 
os, 
ab_experiment, 
ab_variant, 
conversion_purchase, 
product_id, 
product_type, 
revenue_usd, 
purchase_cnt, 
purchase_user_cnt
from agg_iap where app='${hiveconf:rpt_app}' and (dt >='${hiveconf:rpt_date_start}' OR dt >='${hiveconf:rpt_date_d60}') and dt <='${hiveconf:rpt_date}' ;
 	 
	
	

use farm_1_1;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};


ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;


INSERT OVERWRITE TABLE tmp_user_level PARTITION (app , dt )
select levelup_date,
user_key,
app_id,
max(levelup_ts) levelup_ts,
min(previous_level) level_start,
max(current_level) level_end,
app,
dt
from fact_levelup WHERE app='${hiveconf:rpt_app}' AND dt >= '${hiveconf:rpt_date_start}' AND dt <= '${hiveconf:rpt_date}'
group by levelup_date,app_id,user_key,app,dt
;

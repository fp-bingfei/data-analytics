
use farm_1_1;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=10000;
SET hive.stats.fetch.partition.stats=false;
SET hive.stats.autogather=false;


set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};
set rpt_date_start=${RPT_DATE_START};


ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;

INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app,dt)
select distinct t.user_key,t.app_id,t.date,t.conversion_ts,t.revenue,t.purchase_cnt,t.app,t.dt from
(select 
user_key,
app_id,
date,
min(ts) over (partition by user_key,app_id) conversion_ts,
sum(revenue_usd) over (partition by user_key,app_id,date) revenue,
count(ts) over (partition by user_key,app_id,date) purchase_cnt,
app,
dt
from fact_revenue WHERE app='${hiveconf:rpt_app}' AND  dt <= '${hiveconf:rpt_date}')t 
;

 -------------create a temp table from fact_session to get all the dimensions along with session_cnt and last_login_ts ---------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;




set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};


ALTER TABLE tmp_user_snsid DROP IF EXISTS PARTITION (app='${RPT_APP}');
ALTER TABLE tmp_user_daily_login DROP IF EXISTS PARTITION (app='${RPT_APP}');

INSERT OVERWRITE TABLE tmp_user_snsid PARTITION (app='${RPT_APP}' , dt='${RPT_DATE}')
select distinct  
app_id,
app_user_id,
facebook_id
from fact_session 
where app='${RPT_APP}' and dt='${RPT_DATE}';


INSERT OVERWRITE TABLE tmp_user_daily_login PARTITION (app='${RPT_APP}',dt='${RPT_DATE}')
SELECT date, user_key,device_key,app_id,app_user_id,install_ts, install_date,birthday, app_version, level_start, level_end, os, os_version,
t.country_code,c.country,last_ip,install_source, language,locale, gender,device, browser, browser_version, ab_experiment, ab_variant,count(1) session_cnt, max(ts_start) last_login_ts,fb_source
FROM
(
SELECT
date_start date,
app_id,
user_key,
null as device_key,
app_user_id,
ts_start,
birthday,
first_value(install_ts,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as install_ts,
first_value(install_date,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as install_date,
first_value(app_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as app_version,
first_value(level_start,true) OVER (PARTITION BY date_start, user_key order by ts_start asc
rows between unbounded preceding AND unbounded following) as level_start,
first_value(level_end,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as level_end,
first_value(os,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as os,
first_value(os_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as os_version,
first_value(s.country_code,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as country_code,
first_value(s.ip,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as last_ip,
first_value(s.install_source,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as install_source,
first_value(s.fb_source,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as fb_source,
first_value(language,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as language,
first_value(locale,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as locale,
first_value(gender,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as gender,
first_value(device,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as device,
first_value(browser,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as browser,
first_value(browser_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as browser_version,
first_value(ab_experiment,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as ab_experiment,
first_value(ab_variant,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
rows between unbounded preceding AND unbounded following) as ab_variant
FROM (select * from fact_session where app='${RPT_APP}' and dt='${RPT_DATE}')s
UNION ALL
SELECT
date,
app_id,
user_key,
null as device_key,
app_user_id,
ts ts_start,
null birthday,
first_value(install_ts,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as install_ts,
first_value(install_date,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as install_date,
first_value(app_version,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as app_version,
first_value(level,true) OVER (PARTITION BY date, user_key order by ts asc
rows between unbounded preceding AND unbounded following) as level_start,
first_value(level,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as level_end,
first_value(os,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as os,
first_value(os_version,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as os_version,
first_value(country_code,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as country_code,
first_value(ip,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as last_ip,
first_value(install_source,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as install_source,
first_value(fb_source,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as fb_source,
first_value(language,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as language,
first_value(locale,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as locale,
null gender,
first_value(device,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as device,
first_value(browser,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as browser,
first_value(browser_version,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as browser_version,
first_value(ab_experiment,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as ab_experiment,
first_value(ab_variant,true) OVER (PARTITION BY date, user_key order by ts desc
rows between unbounded preceding AND unbounded following) as ab_variant
FROM (SELECT r.* FROM (select * FROM fact_revenue where app='${RPT_APP}' and dt='${RPT_DATE}') r
 LEFT OUTER JOIN (select user_key, app, dt from fact_session where app='${RPT_APP}' and dt='${RPT_DATE}') s 
 ON r.user_key = s.user_key AND r.app = s.app AND r.dt = s.dt
 WHERE s.user_key IS NULL ) rs
) t
left outer join dim_country c on t.country_code=c.country_code
group by date,app_id, user_key,device_key,install_ts, app_user_id,install_date,app_version, level_start, level_end, os, os_version,
t.country_code, c.country,last_ip,install_source,fb_source,language,locale,birthday, gender,device, browser, browser_version, ab_experiment, ab_variant;



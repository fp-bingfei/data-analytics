
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;




set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};


INSERT OVERWRITE TABLE fact_session PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
SELECT
MD5(concat(app,uid,ts)) as id,
key as app_id,
null as app_version,
MD5(concat(app,uid)) as user_key,
uid as app_user_id,
to_date(ts_pretty) as date_start,
to_date(ts_pretty) as date_end,
ts_pretty as ts_start, 
ts_pretty as ts_end,
install_ts_pretty as install_ts,
to_date(install_ts_pretty) as install_date,
null session_id,
snsid as facebook_id,  
install_source,
os,
os_version,
browser, 
browser_version,
null device, 
country_code,  
COALESCE(level, 0) as level_start, 
COALESCE(level,0) as level_end, 
gender,
null birthday,
null email,
ip, 
lang AS language, 
null as locale,
null as rc_wallet_start, 
null as rc_wallet_end,
null as coin_wallet_start,
null as coin_wallet_end,
null as ab_experiment,
null as ab_variant,
null as session_length_sec,
null as idfa,
null as idfv,
null as gaid,
null as mac_address,
null as android_id,
fb_source
FROM   raw_newuser_daily 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
UNION ALL
SELECT
MD5(concat(app,uid,ts)) as id,
key as app_id,
null as app_version,
MD5(concat(app,uid)) as user_key,
uid as app_user_id,
to_date(ts_pretty) as date_start,
to_date(ts_pretty) as date_end,
ts_pretty as ts_start, 
ts_pretty as ts_end,
install_ts_pretty as install_ts,
to_date(install_ts_pretty) as install_date,
null session_id,
snsid as facebook_id,
install_source,
os,
os_version,
browser, 
browser_version,
null device, 
country_code,  
COALESCE(level, 0) as level_start, 
COALESCE(level, 0) as level_end, 
gender,
null birthday,
null email,
ip, 
lang AS language, 
null as locale,
null  rc_wallet_start, 
null  rc_wallet_end,
null  coin_wallet_start,
null  coin_wallet_end,
null  ab_experiment,
null  ab_variant,
null  session_length_sec,
null as idfa,
null as idfv,
null as gaid,
null as mac_address,
null as android_id,
fb_source
FROM  raw_session_start_daily
where app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_session DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


insert overwrite table copy_fact_session partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') select 
id,app_id,app_version,user_key,app_user_id,date_start,date_end,ts_start,ts_end,install_ts,install_date,session_id,facebook_id,install_source,os,os_version,browser,browser_version,device,country_code,level_start,level_end,gender,birthday,email,ip,language,locale,rc_wallet_start,rc_wallet_end,coin_wallet_start,coin_wallet_end,ab_experiment,ab_variant,session_length_sec, idfa, idfv, gaid, mac_address, android_id,fb_source
from fact_session where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';


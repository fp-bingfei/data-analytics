
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;




set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};


INSERT OVERWRITE TABLE fact_dau_snapshot PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT
MD5(concat(s.user_key,s.date_start)) id,
s.user_key ,
s.date_start,
s.app_id,
s.app_version,
COALESCE(l.level_start, s.level_start) level_start,
COALESCE(l.level_end,s.level_end,s.level_start) level_end,
s.os,
s.os_version,
s.device,
s.browser,
s.browser_version,
s.country_code,
s.country,
s.language,
s.ab_experiment,
s.ab_variant,
if(s.date_start=s.install_date, 1 ,0) is_new_user ,
d.rc_in ,
d.coins_in ,
d.rc_out ,
d.coins_out ,
d.rc_bal rc_wallet,
d.coins_bal coin_wallet,
coalesce(u.is_payer,if (s.date_start >= to_date(p.conversion_ts),1,0)) is_payer ,
if (s.date_start = to_date(p.conversion_ts),1,0) is_converted_today,
p.revenue_usd ,
p.purchase_cnt ,
s.session_cnt ,
null
from (select * from tmp_user_daily_login where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}')s
LEFT OUTER JOIN (select * from dim_user where app='${hiveconf:rpt_app}' and dt ='${hiveconf:rpt_date_d1}' ) u 
on s.app = u.app and s.user_key = u.user_key
LEFT OUTER JOIN (select * from tmp_user_payment where app='${hiveconf:rpt_app}'and dt='${hiveconf:rpt_date}') p
on s.user_key=p.user_key and s.dt=p.dt and s.app=p.app
LEFT OUTER JOIN (select * from tmp_user_level where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}')l
on s.user_key=l.user_key and s.dt=l.dt and s.app=l.app
LEFT OUTER JOIN (select * from tmp_ledger_daily where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}')d
on s.user_key=d.user_key and s.dt=d.dt and s.app=d.app;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_fact_dau_snapshot DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


insert overwrite table copy_fact_dau_snapshot partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') select 
id,user_key,date,app_id,app_version,level_start,level_end,os,os_version,device,browser,browser_version,country_code,country,language,ab_experiment,ab_variant,is_new_user,rc_in,coins_in,rc_out,coins_out,rc_wallet,coin_wallet,is_payer,is_converted_today,revenue_usd,payment_cnt,session_cnt,playtime_sec 
from fact_dau_snapshot where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

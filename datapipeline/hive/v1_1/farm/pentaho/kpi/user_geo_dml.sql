------- CREATE TABLES IF NOT EXISTS --------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;



set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_date_plus1_nohyphen = ${RPT_DATE_PLUS1_NOHYPHEN};
set rpt_geo=${RPT_GEO};

DROP TABLE IF EXISTS temp_user_geo_${hiveconf:rpt_geo};

CREATE EXTERNAL TABLE IF NOT EXISTS temp_user_geo_${hiveconf:rpt_geo}(
uid STRING,
snsid STRING,
email STRING,
level INT,
experience INT,
coins INT,
reward_points INT,
new_cash1 INT,
new_cash2 INT,
new_cash3 INT,
order_points INT,
time_points INT,
op INT,
gas INT,
lottery_coins INT,
size_x INT,
size_y INT,
top_map_size INT,
max_work_area_size INT,
work_area_size INT,
addtime timestamp,
logintime timestamp,
loginip STRING,
status INT,
continuous_day INT,
point INT,
love_points INT,
feed_status STRING,
track_ref STRING,
extra_info STRING,
fish_op STRING,
name STRING,
picture STRING,
loginnum INT,
note STRING,
fb_source STRING,
pay_times INT,
water_exp INT,
water_level INT,
greenery INT,
sign_points INT,
fb_source_last STRING,
track_ref_last STRING
) PARTITIONED BY (date INT)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION "s3://com.funplusgame.bidata/etl/farm/${hiveconf:rpt_geo}/mysql/tbl_user/"
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS user_geo_${hiveconf:rpt_geo} (
user_id STRING,
uid STRING,
snsid STRING,
email STRING,
user_name STRING,
addtime timestamp,
logintime timestamp,
loginip STRING,
status INT
) PARTITIONED BY (dt STRING)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION "s3://com.funplus.datawarehouse/farm_1_1/others/user/${hiveconf:rpt_geo}/"
TBLPROPERTIES('serialization.null.format'='');


--- Querys ----

ALTER TABLE temp_user_geo_${hiveconf:rpt_geo} ADD IF NOT EXISTS PARTITION (date=${hiveconf:rpt_date_plus1_nohyphen}) LOCATION "s3://com.funplusgame.bidata/etl/farm/${hiveconf:rpt_geo}/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/";

INSERT OVERWRITE TABLE user_geo_${hiveconf:rpt_geo} PARTITION (dt='${hiveconf:rpt_date}')
SELECT
md5(concat(
if(uid is null, "uid", uid),
if(snsid is null, "snsid", snsid),
if(email is null, "email", email)
)) as user_id,
uid,
snsid,
email,
name as user_name,
addtime,
logintime,
loginip,
status
FROM
(select  uid, snsid, email, name, addtime, logintime, loginip, status, row_number() over (partition by uid order by logintime desc) AS rnum 
FROM  temp_user_geo_${hiveconf:rpt_geo} WHERE date=${hiveconf:rpt_date_plus1_nohyphen} AND uid <> 'uid') t 
where rnum = 1;
 

 -- Drop Old partitions in temp table --
 
ALTER TABLE temp_user_geo_${hiveconf:rpt_geo} DROP IF EXISTS PARTITION (date < ${hiveconf:rpt_date_plus1_nohyphen});






-------- payment daily script ------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

SET hive.stats.autogather=false;

SET hive.merge.mapfiles=true;
SET hive.mergejob.maponly=true;




set rpt_app = ${RPT_APP};
set rpt_date = ${RPT_DATE};
set rpt_date_d1 = ${RPT_DATE_D1};
set rpt_date_nohyphen = ${RPT_DATE_NOHYPHEN};
set rpt_geo=${RPT_GEO};

ALTER TABLE tmp_user_payment DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');


INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app='${hiveconf:rpt_app}' , dt='${hiveconf:rpt_date}')
select 
p.user_key,
p.app_id,
p.date,
coalesce(d.conversion_ts,p.conversion_ts) conversion_ts,
revenue_usd,
purchase_cnt,
coalesce(to_date(p.last_payment_ts),d.last_payment_date) last_payment_date
from (select app_id, user_key, date, min(ts) conversion_ts, sum(revenue_usd) revenue_usd, count(ts) purchase_cnt, max(ts) last_payment_ts, app from fact_revenue 
where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
group by app_id, user_key, date,app )p
left outer join (select * from dim_user where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date_d1}')d
on p.user_key=d.user_key and p.app=d.app
;



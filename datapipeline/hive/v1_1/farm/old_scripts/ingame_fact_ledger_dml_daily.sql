use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size= 134217728;
SET mapred.max.split.size = 134217728;
set hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


-- Populate Fact Ledger --

INSERT OVERWRITE TABLE fact_ledger PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(concat(t.app,uid,ts)) id,
dt date,
t.ts_pretty ts,
t.app app_id,
t.uid,
MD5(CONCAT(t.app,uid)) user_key,
NULL session_id,
t.level,
t.action transaction_type,
NULL received_id,
CASE WHEN t.rc_in>0 THEN 'Rc' ELSE '' END  received_name,
CASE WHEN t.rc_in>0 THEN 'Resource' ELSE '' END  received_type,
CASE WHEN t.rc_in>0 THEN rc_in ELSE 0 END  received_amount,
NULL spent_id,
CASE WHEN t.rc_out>0 THEN 'Rc' ELSE '' END  spent_name,
CASE WHEN t.rc_out>0 THEN 'Resource' ELSE '' END  spent_type,
CASE WHEN t.rc_out>0 THEN rc_out ELSE 0 END  spent_amount
FROM raw_rc_transaction_daily t WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
UNION ALL
SELECT
MD5(concat(t.app,uid,ts)) id,
dt date,
t.ts_pretty ts,
t.app app_id,
t.uid,
MD5(CONCAT(t.app,uid)) user_key,
NULL session_id,
t.level,
t.action transaction_type,
NULL received_id,
CASE WHEN t.coins_in>0 THEN 'Coins' ELSE '' END  received_name,
CASE WHEN t.coins_in>0 THEN 'Resource' ELSE '' END  received_type,
CASE WHEN t.coins_in>0 THEN t.coins_in ELSE 0 END  received_amount,
NULL spent_id,
CASE WHEN t.coins_out>0 THEN 'Coins' ELSE '' END  spent_name,
CASE WHEN t.coins_out>0 THEN 'Resource' ELSE '' END  spent_type,
CASE WHEN t.coins_out>0 THEN t.coins_out ELSE 0 END  spent_amount
FROM raw_coins_transaction_daily t WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';

exit;
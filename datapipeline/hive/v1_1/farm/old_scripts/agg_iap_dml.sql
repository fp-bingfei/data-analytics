
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;


INSERT OVERWRITE TABLE agg_iap PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT   
r.date,
u.app_id,
u.app_version,
r.level,
u.install_date,
u.install_source,
d.country,
d.os,
d.ab_experiment,
d.ab_variant,
CASE WHEN u.conversion_ts=r.ts THEN 1 ELSE 0 END conversion_purchase,
product_id,
product_type,
SUM(r.revenue_usd) revenue_usd,
COUNT(1) purchase_cnt,
COUNT(distinct u.user_key) purchase_user_cnt
FROM 
(SELECT * FROM fact_revenue WHERE app='${hiveconf:rpt_app}' AND dt ='${hiveconf:rpt_date}') r
JOIN 
(SELECT * FROM fact_dau_snapshot WHERE app='${hiveconf:rpt_app}' AND dt ='${hiveconf:rpt_date}') d 
ON r.user_key=d.user_key AND r.app=d.app 
JOIN
(SELECT * FROM dim_user WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}' ) u 
ON u.user_key=r.user_key AND u.app=r.app
GROUP BY 
r.date, 
u.app_id, 
u.app_version,
r.level, 
u.install_date,
u.install_source,
d.country,
d.os,
d.ab_experiment,
d.ab_variant,
CASE WHEN u.conversion_ts=r.ts THEN 1 ELSE 0 END,
product_id, 
product_type;

SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_agg_iap PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
date, 
app_id, 
app_version, 
level , 
install_date, 
install_source, 
country, 
os, 
ab_experiment, 
ab_variant, 
conversion_purchase, 
product_id, 
product_type, 
revenue_usd, 
purchase_cnt, 
purchase_user_cnt
FROM
agg_iap
WHERE app='${rpt_app}' AND dt='${hiveconf:rpt_date}' ;


exit;
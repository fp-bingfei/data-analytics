
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size= 134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

  -- Populate Fact Item --

INSERT OVERWRITE TABLE fact_item PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT MD5(concat(app,uid,ts_pretty)) id,dt date,ts_pretty ts,app app_id, uid app_user_id, MD5(CONCAT(app,uid)) AS user_key,NULL session_id,level, action transaction_type,
CASE WHEN item_in>0 THEN item_id ELSE ''  END received_id,
CASE WHEN item_in>0 THEN item_name ELSE '' END received_name,
CASE WHEN item_in>0 THEN item_type ELSE '' END received_type,
CASE WHEN item_in>0 THEN item_in ELSE 0 END  received_amount,
CASE WHEN item_in>0 THEN item_class ELSE '' END received_item_class,
CASE WHEN item_out>0 THEN item_id ELSE ''  END spent_id,
CASE WHEN item_out>0 THEN item_name ELSE '' END  spent_name,
CASE WHEN item_out>0 THEN item_type ELSE '' END  spent_type,
CASE WHEN item_out>0 THEN item_out ELSE 0 END  spent_amount,
CASE WHEN item_out>0 THEN item_class ELSE '' END spent_item_class  FROM
raw_item_transaction_daily WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';

exit;
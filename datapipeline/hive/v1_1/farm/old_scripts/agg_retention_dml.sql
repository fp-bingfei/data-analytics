
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;


INSERT OVERWRITE TABLE agg_retention PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT 
a.player_day,
a.app_id,
a.app_version,
a.install_date,
a.install_source,
a.install_subpublisher,
a.install_campaign,
a.install_creative_id, 
a.device_alias,
a.os,
a.browser,
a.country,
a.language,
a.ab_experiment, 
a.ab_variant,
a.is_payer,
a.new_user_cnt,
a.retained_user_cnt
FROM
(SELECT * FROM  agg_retention
WHERE app='${hiveconf:rpt_app}' AND dt = '${hiveconf:rpt_date_yesterday}' AND install_date  <= DATE_SUB('${hiveconf:rpt_date}',180) ) a 
UNION ALL
SELECT 
t.player_day,
t.app_id,
t.app_version,
t.install_date,
t.install_source,
t.install_subpublisher,
t.install_campaign,
t.install_creative_id, 
t.device_alias,
t.os,
t.browser,
t.country,
t.language,
t.ab_experiment, 
t.ab_variant,
t.is_payer,
t.new_user_cnt,
t.retained_user_cnt
FROM
tmp_agg_retention_ltv t
WHERE t.app='${hiveconf:rpt_app}' AND t.dt = '${hiveconf:rpt_date}'
;



SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


INSERT OVERWRITE TABLE copy_agg_retention PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT 
t.player_day,
t.app_id,
t.app_version,
t.install_date,
t.install_source,
t.install_subpublisher,
t.install_campaign,
t.install_creative_id, 
t.device_alias,
t.os,
t.browser,
t.country,
t.language,
t.ab_experiment, 
t.ab_variant,
t.is_payer,
t.new_user_cnt,
t.retained_user_cnt
FROM
agg_retention t
WHERE t.app='${hiveconf:rpt_app}' AND t.dt = '${hiveconf:rpt_date}'
;

exit;


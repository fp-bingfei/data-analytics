use farm_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS dim_user
(
  id  STRING,
  user_key STRING,
  app_id STRING,
  app_user_id  STRING,
  facebook_id STRING,
  install_ts  TIMESTAMP,
  install_date  STRING,
  install_source  STRING,
  install_subpublisher  STRING,
  install_campaign  STRING,
  install_language  STRING,
  install_locale  STRING,
  install_country_code  STRING,
  install_country  STRING,
  install_os  STRING,
  install_device  STRING,
  install_device_alias  STRING,
  install_browser  STRING,
  install_gender  STRING,
  install_age  STRING,
  language  STRING,
  locale STRING,
  birthday  STRING,
  gender STRING,
  country_code  STRING,
  country  STRING,
  os STRING,
  os_version STRING,
  device  STRING,
  device_alias  STRING,
  browser STRING,
  browser_version STRING,
  app_version STRING,
  level INT,
  levelup_ts TIMESTAMP,
  ab_experiment STRING,
  ab_variant STRING,
  is_payer INT,
  conversion_ts TIMESTAMP,
  total_revenue_usd DECIMAL(12,4),
  payment_cnt INT,
  last_login_ts TIMESTAMP,
  coin_wallet DOUBLE,
  coins_in BIGINT,
  coins_out BIGINT,
  rc_wallet DOUBLE,
  rc_in BIGINT,
  rc_out BIGINT,
  email STRING,
  last_ip STRING
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/dim_user'
TBLPROPERTIES('serialization.null.format'='');





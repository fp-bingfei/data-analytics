
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size= 67108864;
SET mapred.max.split.size = 134217728;
set hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;

-- Populate Fact Item --

INSERT OVERWRITE TABLE fact_ledger PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT	
  MD5(CONCAT(t.app,t.ts,t.uid,COALESCE(t.action,'Unknown'),COALESCE(t.event,'Unknown'),COALESCE(t.install_source,'Unknown'),
   t.install_ts,COALESCE(t.level,0),COALESCE(t.rc_bal,0),COALESCE(t.rc_in,0),COALESCE(t.rc_out,0),t.snsid)) AS id,
  dt date,
  t.ts_pretty ts,
  MD5(CONCAT(t.app,t.uid)) AS user_key, 
  t.app,
  t.uid,
  t.snsid,
  t.level,
  t.action,
  CASE WHEN t.rc_in>0 THEN 'Rc' ELSE 'Unknown' END  in_name,
  CASE WHEN t.rc_in>0 THEN 'Resource' ELSE 'Unknown' END  in_type,
  CASE WHEN t.rc_in>0 THEN rc_in ELSE 1 END  in_amount,
  CASE WHEN t.rc_out>0 THEN 'Rc' ELSE 'Unknown' END  out_name,
  CASE WHEN t.rc_out>0 THEN 'Resource' ELSE 'Unknown' END  out_type,
  CASE WHEN t.rc_out>0 THEN rc_out ELSE 1 END  out_amount
FROM raw_rc_transaction_daily t WHERE app="${hiveconf:rpt_app}" AND dt='${hiveconf:rpt_date}'
UNION ALL
SELECT
  MD5(CONCAT(t.app,t.ts,t.uid,COALESCE(t.action,'Unknown'),COALESCE(t.event,'Unknown'),COALESCE(t.install_source,'Unknown'), t.install_ts,COALESCE(t.level,0),COALESCE(t.coins_bal,0),
  COALESCE(t.coins_in,0),COALESCE(t.coins_out,0),t.snsid)) AS id,
  dt date,
  t.ts_pretty ts,
  MD5(CONCAT(t.app,t.uid)) AS user_key,
  t.app,
  t.uid,
  t.snsid,
  t.level,
  t.action,
  CASE WHEN t.coins_in>0 THEN 'Coins' ELSE 'Unknown' END  in_name,
  CASE WHEN t.coins_in>0 THEN 'Resource' ELSE 'Unknown' END  in_type,
  CASE WHEN t.coins_in>0 THEN t.coins_in ELSE 1 END  in_amount,
  CASE WHEN t.coins_out>0 THEN 'Coins' ELSE 'Unknown' END  out_name,
  CASE WHEN t.coins_out>0 THEN 'Resource' ELSE 'Unknown' END  out_type,
  CASE WHEN t.coins_out>0 THEN t.coins_out ELSE 1 END  out_amount
FROM raw_coins_transaction_daily t WHERE app="${hiveconf:rpt_app}" AND dt='${hiveconf:rpt_date}';


-- Populate Agg Item --

INSERT OVERWRITE TABLE agg_daily_ledger PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(l.date,app,l.level,l.transaction_type,l.in_name,l.in_type,l.out_name,l.out_type)) AS id,
l.date,
l.app,
l.level,
l.transaction_type,
l.in_name,
l.in_type,
l.out_name,
l.out_type,
SUM(COALESCE(l.in_amount,0))  in_amount,
SUM(COALESCE(l.out_amount,0))  out_amount,
COUNT(distinct l.user_key)   purchaser_cnt,
0 user_cnt
FROM fact_ledger l WHERE app="${hiveconf:rpt_app}" AND dt='${hiveconf:rpt_date}'
GROUP BY l.date,l.app, l.level,l.transaction_type,l.in_name,l.in_type,l.out_name, l.out_type;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert into table copy_agg_daily_ledger partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select id,date,app_name,level,transaction_type,in_name, in_type, out_name, out_type, in_amount, out_amount, purchaser_cnt,user_cnt 
from agg_daily_ledger where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

exit;
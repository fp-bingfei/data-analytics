-------- payment daily script ------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app='${hiveconf:rpt_app}' , dt='${hiveconf:rpt_date}')
select 
p.user_key,
p.app_id,
p.date,
coalesce(d.conversion_ts,p.conversion_ts) conversion_ts,
revenue_usd,
purchase_cnt
from (select app_id, user_key, date, min(ts) conversion_ts, sum(revenue_usd) revenue_usd, count(ts) purchase_cnt,app from fact_revenue 
where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
group by app_id, user_key, date,app )p
left outer join (select * from dim_user where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date_yesterday}')d
on p.user_key=d.user_key and p.app=d.app
;



exit;

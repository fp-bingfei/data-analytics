-------------------  fact_revenue -------------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.supports.subdirectories=true;
set hive.exec.parallel=false;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.exec.max.dynamic.partitions.pernode=100000;

ALTER TABLE currency ADD IF NOT EXISTS PARTITION (date='${hiveconf:rpt_date}') LOCATION "s3://com.funplusgame.bidata/currency/${hiveconf:rpt_date_nohyphen}/";

INSERT OVERWRITE TABLE fact_revenue PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select
 t.id,
 t.app_id,
 t.app_version,
 t.user_key,
 t.app_user_id,
 t.date,
 t.ts,
 t.install_ts,
 t.install_date,
 t.session_id,
 t.level,
 t.os, 
 t.os_version,
 t.device,
 t.browser,
 t.browser_version,
 t.country_code,
 t.install_source,
 t.ip,
 t.language,
 t.locale,
 t.ab_experiment,
 t.ab_variant,
 t.coin_wallet,
 t.rc_wallet,
 t.payment_processor,
 t.product_id,
 t.product_name,
 t.product_type,
 t.coins_in ,
 t.rc_in,
 t.currency,
 t.revenue_amount, 
 t.revenue_usd,
 t.transaction_id,
 t.idfa,
 t.idfv,
 t.gaid,
 t.mac_address,
 t.android_id 
from (
SELECT
MD5(concat(app,uid,ts)) as id,
key as app_id,
null app_version,
MD5(concat(app,uid)) as user_key,
uid as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
install_ts_pretty as install_ts,
to_date(install_ts_pretty) install_date,
null session_id,
COALESCE(level,0) as level,
os, 
os_version,
null device,
browser,
browser_version,
country_code,
install_source,
ip,
lang language,
null locale,
null as ab_experiment,
null as ab_variant,
coins_bal as coin_wallet,
rc_bal as rc_wallet,
payment_processor,
product_id,
product_name,
product_type,
coins_in,
rc_in,
r.currency,
amount revenue_amount, 
amount*c.factor/100 revenue_usd,
transaction_id,
null as idfa,
null as idfv,
null as gaid,
null as mac_address,
null as android_id,
row_number() over (partition by app,uid,transaction_id order by ts) rank
from (select * from raw_payment_daily WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}') r
left outer join 
(SELECT * FROM currency WHERE dt = '${hiveconf:rpt_date}') c on r.currency=c.currency and r.dt=c.dt
)t where t.rank=1 ;



-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_fact_revenue partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') select 
	id,app_id,app_version,user_key,app_user_id,date,ts,install_ts,install_date,session_id,level,os,os_version,device,browser,browser_version,country_code,install_source,ip,language,locale,ab_experiment,ab_variant,coin_wallet,rc_wallet,payment_processor,product_id,product_name,product_type,coins_in,rc_in,currency,revenue_amount,revenue_usd,transaction_id, idfa, idfv, gaid, mac_address, android_id
from fact_revenue where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

exit;

USE farm_1_1;

-- raw_item_transaction_daily form raw_item_transaction

CREATE EXTERNAL TABLE raw_item_transaction_daily(
  key STRING, 
  ts STRING, 
  ts_pretty TIMESTAMP, 
  action STRING, 
  browser STRING, 
  browser_version STRING, 
  country_code STRING, 
  event STRING, 
  gifted_to STRING, 
  install_source STRING, 
  install_ts STRING, 
  install_ts_pretty TIMESTAMP, 
  ip STRING, 
  item_class STRING, 
  item_id STRING, 
  item_name STRING, 
  item_in BIGINT, 
  item_out BIGINT, 
  item_type STRING, 
  lang STRING, 
  level STRING, 
  location STRING, 
  os STRING, 
  os_version STRING, 
  snsid STRING, 
  uid STRING, 
  x_from ARRAY<STRING>
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_item_transaction_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
  );
  
-- raw_rc_transaction_daily form raw_rc_transaction hourly partitioned table
  
 CREATE EXTERNAL TABLE IF NOT EXISTS raw_rc_transaction_daily (
  key   STRING,
  ts   STRING,
  ts_pretty   TIMESTAMP,
  action   STRING,
  action_detail   STRING,
  browser   STRING,
  browser_version   STRING,
  country_code   STRING,
  event   STRING,
  install_source   STRING,
  install_ts   STRING,
  install_ts_pretty   TIMESTAMP,
  ip   STRING,
  lang   STRING,
  level   BIGINT,
  location   STRING,
  os   STRING,
  os_version   STRING,
  rc_bal   BIGINT,
  rc_in   BIGINT,
  rc_out   BIGINT,
  snsid   STRING,
  uid   STRING
  )
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_rc_transaction_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);
	
-- raw_coins_transaction_daily form raw_coins_transaction hourly partitioned table

CREATE EXTERNAL TABLE IF NOT EXISTS raw_coins_transaction_daily (
	key   STRING,
	ts   STRING,
	ts_pretty   TIMESTAMP,
	action   STRING,
	action_detail   STRING,
	browser   STRING,
	browser_version   STRING,
	coins_bal   DOUBLE,
	coins_in   BIGINT,
	coins_out   BIGINT,
	country_code   STRING,
	event   STRING,
	install_source   STRING,
	install_ts   STRING,
	install_ts_pretty   TIMESTAMP,
	ip   STRING,
	lang   STRING,
	level   BIGINT,
	location   STRING,
	os   STRING,
	os_version   STRING,
	snsid   STRING,
	uid   STRING
	)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_coins_transaction_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
  );

-- user dim to update for fact_item and fact_ledger
CREATE TABLE tmp_ingame_user_dim(
uid STRING,
device_key STRING,
os STRING,
os_version STRING,
device STRING,
browser STRING,
browser_version STRING,
country_code STRING,
ip STRING,
language STRING,
locale STRING,
ab_experiment STRING,
ab_variant STRING
)PARTITIONED BY (
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_ingame_user_dim'
TBLPROPERTIES('serialization.null.format'='');
    
-- fact_item from raw_item_transaction_daily table
 
CREATE EXTERNAL TABLE IF NOT EXISTS fact_item
(
id  STRING,
date STRING,
ts TIMESTAMP,
app_id  STRING,
app_user_id STRING,
user_key STRING,
session_id STRING,
level INT,
transaction_type STRING,
received_id STRING,
received_name STRING,
received_type STRING,
received_amount BIGINT,
received_item_class  STRING,
spent_id STRING,
spent_name STRING,
spent_type STRING,
spent_amount BIGINT,
spent_item_class  STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_item'
TBLPROPERTIES('serialization.null.format'='');


-- fact_ledger from raw_rc_transactions_daily and raw_coins_transactions_daily table

CREATE EXTERNAL TABLE IF NOT EXISTS fact_ledger
(
id  STRING,
date STRING,
ts TIMESTAMP,
app_id  STRING,
app_user_id STRING,
user_key STRING,
session_id STRING,
level INT,
transaction_type STRING,
received_id STRING,
received_name STRING,
received_type STRING,
received_amount INT,
spent_id STRING,
spent_name STRING,
spent_type STRING,
spent_amount INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_ledger'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS agg_item
(
id  STRING,
date STRING,
app_id  STRING,
level INT,
os STRING,
device STRING,
browser STRING,
country STRING,
language STRING,
transaction_type STRING,
received_id STRING,
received_name STRING,
received_type STRING,
received_amount BIGINT,
received_item_class  STRING,
spent_id STRING,
spent_name STRING,
spent_type STRING,
spent_amount BIGINT,
spent_item_class STRING,
purchaser_cnt INT,
user_cnt  INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_item'
TBLPROPERTIES('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS agg_daily_ledger
(
id  STRING,
date STRING,
app_id  STRING,
level INT,
os STRING,
device STRING,
browser STRING,
country STRING,
language STRING,
transaction_type STRING,
received_id STRING,
received_name STRING,
received_type STRING,
received_amount BIGINT,
spent_id STRING,
spent_name STRING,
spent_type STRING,
spent_amount BIGINT,
purchaser_cnt INT,
user_cnt  INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_daily_ledger'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS copy_agg_item
(
id  STRING,
date STRING,
app_id  STRING,
level INT,
os STRING,
device STRING,
browser STRING,
country STRING,
language STRING,
transaction_type STRING,
received_id STRING,
received_name STRING,
received_type STRING,
received_amount BIGINT,
received_item_class STRING,
spent_id STRING,
spent_name STRING,
spent_type STRING,
spent_amount BIGINT,
spent_item_class STRING,
purchaser_cnt INT,
user_cnt  INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_item'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS copy_agg_daily_ledger
(
id  STRING,
date STRING,
app_id  STRING,
level INT,
os STRING,
device STRING,
browser STRING,
country STRING,
language STRING,
transaction_type STRING,
received_id STRING,
received_name STRING,
received_type STRING,
received_amount BIGINT,
spent_id STRING,
spent_name STRING,
spent_type STRING,
spent_amount BIGINT,
purchaser_cnt INT,
user_cnt  INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_daily_ledger'
TBLPROPERTIES('serialization.null.format'='');

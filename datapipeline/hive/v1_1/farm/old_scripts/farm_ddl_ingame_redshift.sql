
create table fact_ledger (
  id  VARCHAR(128) not null ENCODE LZO,
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);

ALTER TABLE fact_ledger
   ADD CONSTRAINT fact_ledger_pkey
   PRIMARY KEY (id);

CREATE TABLE agg_daily_ledger
(
   id                varchar(128)    NOT NULL,
   date              date,
   app               varchar(64),
   level         smallint        NOT NULL,
   transaction_type  varchar(64)     NOT NULL,
   in_name           varchar(256)    DEFAULT 'Unknown'::character varying,
   in_type           varchar(256)    DEFAULT 'Unknown'::character varying,
   out_name          varchar(256)    DEFAULT 'Unknown'::character varying,
   out_type          varchar(256)    DEFAULT 'Unknown'::character varying,
   in_amount         integer         DEFAULT 0,
   out_amount        integer         DEFAULT 0,
   purchaser_cnt     integer,
   user_cnt          integer
)DISTKEY(date)
  SORTKEY(date,level,transaction_type,in_type,in_name,out_type,out_name);
  ;

ALTER TABLE agg_daily_ledger
   ADD CONSTRAINT agg_daily_ledger_pkey
   PRIMARY KEY (id);


create table fact_item (
  id  VARCHAR(128) not null ENCODE LZO,
  date DATE ENCODE DELTA,
  ts timestamp not null,
  user_key  VARCHAR(64) NOT NULL ENCODE LZO,
  app               VARCHAR(64) NOT NULL ENCODE BYTEDICT,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(64) NOT NULL ENCODE LZO,
  install_date      date encode DELTA,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_amount integer default 0,
  item_class varchar(64) ENCODE LZO,
  item_id VARCHAR(64) ENCODE LZO
)DISTKEY(user_key)
  SORTKEY(user_key, app, uid, snsid);
  
ALTER TABLE fact_item
   ADD CONSTRAINT fact_item_pkey
   PRIMARY KEY (id);



create table agg_item (
  id VARCHAR(128) NOT NULL ENCODE LZO,
  date DATE ENCODE DELTA,
  app varchar(64) encode LZO,
  install_date DATE ENCODE DELTA,
  level SMALLINT not null,
  transaction_type varchar(64) not null ENCODE BYTEDICT,
  in_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_name VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  out_type VARCHAR(64) DEFAULT 'Unknown' ENCODE BYTEDICT,
  in_amount integer default 0,
  out_amount integer default 0,
  purchaser_cnt integer default null,
  user_cnt integer default null,
  item_class VARCHAR(64) ENCODE LZO,
  item_id VARCHAR(64) ENCODE LZO
)DISTKEY(date)
  SORTKEY(date,level,transaction_type,in_type,in_name,out_type,out_name,item_class,item_id);


ALTER TABLE agg_item
   ADD CONSTRAINT agg_item_pkey
   PRIMARY KEY (id);
  

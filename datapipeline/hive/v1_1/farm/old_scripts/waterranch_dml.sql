----------------------------------
-- Query to generate agg_tutorial
----------------------------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;

-- Add greenery_in and greenery_out as String and remove snsid

INSERT OVERWRITE TABLE waterexpchange_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(w.app, w.uid)) user_key,
w.uid app_user_id,
w.app app_id,
w.ts_pretty,
COALESCE(d.install_date, to_date(w.install_ts)) install_dt,
d.install_source,
d.os,
d.os_version,
d.browser,
w.browser_version,
d.country_code,
w.action,
w.expansion_size_from,
COALESCE(w.greenery_in,'0') greenery_in,
COALESCE(w.greenery_out,'0') greenery_out,
COALESCE(w.greenery_bal,'0') greenery_bal,
COALESCE(w.level,'0') level,
COALESCE(w.water_exp_in,'0') water_exp,
COALESCE(w.water_level,'0') water_level,
COALESCE(w.water_exp,'0') water_exp
FROM
(SELECT *
FROM
raw_waterexpchange_daily
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';
) w
LEFT OUTER JOIN
(SELECT *
FROM
dim_user 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';
) d
ON
MD5(CONCAT(w.app, w.uid)) = d.app_user_id;


INSERT OVERWRITE TABLE rpt_water_exp_in PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(
 COALESCE(app_id,''),
 COALESCE(dt,''),
 COALESCE(install_dt,''),
 COALESCE(install_source,''),
 COALESCE(os,''),
 COALESCE(os_version,''),
 COALESCE(browser,''),
 COALESCE(browser_version,''),
 COALESCE(country_code,''),
 COALESCE(action,''), 
 COALESCE(expansion_size_from,''),
 COALESCE(level,''),
 COALESCE(water_level,'') )) AS id,
app_id,
dt,
install_dt,
install_source,
os,
os_version,
browser,
browser_version,
country_code,
action,
expansion_size_from,
level,
water_level,
COUNT(DISTINCT uid) AS uid_cnt,
SUM(water_exp_in) AS tot_exp_in
raw_waterexpchange_daily
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
GROUP BY
app_id,
dt,
install_dt,
install_source,
os,
os_version,
browser,
browser_version,
country_code,
action,
expansion_size_from,
level,
water_level
;


INSERT OVERWRITE TABLE rpt_water_exp_greenery PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(
 COALESCE(app_id,''),
 COALESCE(dt,''),
 COALESCE(install_dt,''),
 COALESCE(install_source,''),
 COALESCE(os,''),
 COALESCE(os_version,''),
 COALESCE(browser,''),
 COALESCE(browser_version,''),
 COALESCE(country_code,''),
 COALESCE(action,''), 
 COALESCE(expansion_size_from,''),
 COALESCE(level,''),
 COALESCE(water_level,'') )) AS id,
app_id,
dt,
install_dt,
install_source,
os,
os_version,
browser,
browser_version,
country_code,
action,
expansion_size_from,
level,
water_level,
SUM(greenery_in) AS tot_greenery_in,
SUM(greenery_out) AS tot_greenery_out
raw_waterexpchange_daily
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
GROUP BY
app_id,
dt,
install_dt,
install_source,
os,
os_version,
browser,
browser_version,
country_code,
action,
expansion_size_from,
level,
water_level
;

INSERT OVERWRITE TABLE thirdsceneexpand_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(w.app, w.uid)) user_key,
w.uid app_user_id,
w.app app_id,
w.ts_pretty,
COALESCE(d.install_date, to_date(w.install_ts)) install_dt,
d.install_source,
d.os,
d.os_version,
d.browser,
w.browser_version,
d.country_code,
w.action,
w.expansion_size_from,
COALESCE(w.greenery_bal,'0') greenery_bal,
COALESCE(w.coins_bal,'0') coins_bal,
COALESCE(w.rc_bal,'0') rc_bal,
COALESCE(w.level,'0') level,
COALESCE(w.waterLevel,'0') waterLevel,
COALESCE(w.expansion_size_to,'0') expansion_size_to,
COALESCE(w.expansion_top_size_to,'0') expansion_top_size_to
FROM
(SELECT *
FROM
raw_thirdsceneexpand_daily
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';
) w
LEFT OUTER JOIN
(SELECT *
FROM
dim_user 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';
) d
ON
MD5(CONCAT(w.app, w.uid)) = d.app_user_id;


INSERT OVERWRITE TABLE rpt_thirdsceneexpand_exp PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(
 COALESCE(app_id,''),
 COALESCE(dt,''),
 COALESCE(install_dt,''),
 COALESCE(install_source,''),
 COALESCE(os,''),
 COALESCE(os_version,''),
 COALESCE(browser,''),
 COALESCE(browser_version,''),
 COALESCE(country_code,''),
 COALESCE(action,''), 
 COALESCE(expansion_size_from,''),
 COALESCE(level,''),
 COALESCE(water_level,''),
 COALESCE(expansion_size_to,''),
 COALESCE(expansion_top_size_to,'') )) AS id,
app_id,
dt,
install_dt,
install_source,
os,
os_version,
browser,
browser_version,
country_code,
action,
expansion_size_from,
level,
water_level,
expansion_size_to,
expansion_top_size_to,
COUNT(distinct expansion_size_to) AS expansion_size_to_cnt,
COUNT(distinct expansion_top_size_to) AS expansion_top_size_to_cnt
FROM
thirdsceneexpand_daily
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
GROUP BY
app_id,
dt,
install_dt,
install_source,
os,
os_version,
browser,
browser_version,
country_code,
action,
expansion_size_from,
level,
water_level,
expansion_size_to,
expansion_top_size_to;

exit;
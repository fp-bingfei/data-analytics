
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;


INSERT OVERWRITE TABLE tmp_player_day_cube PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT 
p.day,
d.app_id,
d.app_version,
d.install_date,
d.install_source,
d.install_subpublisher,
d.install_campaign,
null install_creative_id, 
d.install_device_alias,
d.install_os,
d.install_browser,
d.install_country,
d.install_language,
d.ab_experiment, 
d.ab_variant,
d.is_payer,
count(1) new_user_cnt
FROM
dim_user d 
JOIN
player_day p 
ON 1=1
WHERE d.app='${hiveconf:rpt_app}' AND d.dt='${hiveconf:rpt_date}' AND d.install_date > DATE_SUB('${hiveconf:rpt_date}',180)
GROUP BY
p.day,
d.app_id,
d.app_version,
d.install_date,
d.install_source,
d.install_subpublisher,
d.install_campaign,
d.install_device_alias,
d.install_os,
d.install_browser,
d.install_country,
d.install_language,
d.ab_experiment, 
d.ab_variant,
d.is_payer;


INSERT OVERWRITE TABLE tmp_agg_retention_ltv PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
pc.player_day,
pc.app_id,
pc.app_version,
pc.install_date,
pc.install_source,
pc.install_subpublisher,
pc.install_campaign,
null install_creative_id, 
pc.device_alias,
pc.os,
pc.browser,
pc.country,
pc.language,
pc.ab_experiment, 
pc.ab_variant,
pc.is_payer,
pc.new_user_cnt,
COALESCE(r.retained_user_cnt,0) retained_user_cnt,
COALESCE(r.revenue,0) revenue,
SUM(COALESCE(r.revenue,0)) OVER (PARTITION BY 
COALESCE(pc.app_id,''), COALESCE(pc.app_version,''), COALESCE(pc.install_date,''),
COALESCE(pc.install_source,''), COALESCE(pc.install_subpublisher,''), COALESCE(pc.install_campaign,''),
COALESCE(pc.device_alias,''), COALESCE(pc.os,''), COALESCE(pc.browser,''),
COALESCE(pc.country,''), COALESCE(pc.language,''), COALESCE(pc.ab_experiment,''),
COALESCE(pc.ab_variant,''), COALESCE(pc.is_payer,'')
ORDER BY pc.player_day ROWS UNBOUNDED PRECEDING ) AS cumulative_revenue
FROM
(SELECT * FROM tmp_player_day_cube  WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}') pc
LEFT OUTER JOIN
(
SELECT 
p.day,
d.app_id,
d.app_version,
d.install_date,
d.install_source,
d.install_subpublisher,
d.install_campaign,   
d.install_device_alias device_alias,
d.install_os os,
d.install_browser browser,
d.install_country country,
d.install_language language,
d.ab_experiment, 
d.ab_variant,
d.is_payer,
count(1) as retained_user_cnt,
SUM(revenue) revenue
FROM
(SELECT * FROM dim_user WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}' AND install_date > DATE_SUB('${hiveconf:rpt_date}',180)) d
JOIN 
(SELECT user_key, dt, SUM(COALESCE(revenue_usd,0)) revenue  
FROM 
fact_dau_snapshot 
WHERE app='${hiveconf:rpt_app}' AND dt > DATE_SUB('${hiveconf:rpt_date}',180) AND dt <= '${hiveconf:rpt_date}'
GROUP BY user_key, dt) f
ON d.user_key = f.user_key
JOIN
player_day p
ON DATEDIFF(f.dt,d.install_date) = p.day
GROUP BY
p.day,
d.app_id,
d.app_version,
d.install_date,
d.install_source,
d.install_subpublisher,
d.install_campaign,
d.install_device_alias,
d.install_os,
d.install_browser,
d.install_country,
d.install_language,
d.ab_experiment, 
d.ab_variant,
d.is_payer
) r
ON
COALESCE(pc.player_day,'') = COALESCE(r.day,'') AND
COALESCE(pc.app_id,'') = COALESCE(r.app_id,'') AND
COALESCE(pc.app_version,'') =COALESCE(r.app_version,'') AND
COALESCE(pc.install_date,'') = COALESCE(r.install_date,'') AND
COALESCE(pc.install_source,'') = COALESCE(r.install_source,'') AND
COALESCE(pc.install_subpublisher,'') = COALESCE(r.install_subpublisher,'') AND
COALESCE(pc.install_campaign,'') = COALESCE(r.install_campaign,'')  AND
COALESCE(pc.device_alias,'') = COALESCE(r.device_alias,'') AND
COALESCE(pc.os,'') = COALESCE(r.os,'') AND
COALESCE(pc.browser,'') = COALESCE(r.browser,'') AND
COALESCE(pc.country,'') = COALESCE(r.country,'') AND
COALESCE(pc.language,'') = COALESCE(r.language,'') AND
COALESCE(pc.ab_experiment,'') = COALESCE(r.ab_experiment,'') AND 
COALESCE(pc.ab_variant,'') = COALESCE(r.ab_variant,'') AND
COALESCE(pc.is_payer,'') = COALESCE(r.is_payer,'');

exit;


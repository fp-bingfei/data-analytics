
use farm_1_1;


INSERT OVERWRITE TABLE dim_install_source 
partition(app="${hiveconf:rpt_app}",year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day}) 
select id, user_key, app_name, uid, install_date, install_source as install_source_event_raw
, case when install_source_array[0] is null then 'Organic' else install_source_array[0] end as install_source_event
, null as install_source_3rdparty_raw, null as install_source_3rdparty, install_source as install_source_raw
, case when install_source_array[0] is null then 'Organic' else install_source_array[0] end as install_source
, install_source_array[1] as campaign, install_source_array[2] as sub_publisher from 
(
select distinct concat(u.app_name,u.uid,u.install_ts) as id, u.user_key, u.app_name, u.uid, u.install_date
, u.install_source, split(u.install_source, '::') as install_source_array from tmp_user_daily_login u
left outer join dim_install_source i on (u.user_key = i.user_key and u.install_date = i.install_date)
where 
u.app="${hiveconf:rpt_app}" AND u.year=${hiveconf:rpt_year} AND u.month=${hiveconf:rpt_month} 
AND u.day=${hiveconf:rpt_day} AND u.date_start = u.install_date  AND i.user_key is NULL 
) t;

exit;
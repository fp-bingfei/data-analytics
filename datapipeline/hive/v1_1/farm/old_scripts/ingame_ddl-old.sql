USE farm_1_1;

CREATE EXTERNAL TABLE raw_item_transaction_daily(
  key STRING, 
  ts STRING, 
  ts_pretty TIMESTAMP, 
  action STRING, 
  browser STRING, 
  browser_version STRING, 
  country_code STRING, 
  event STRING, 
  gifted_to STRING, 
  install_source STRING, 
  install_ts STRING, 
  install_ts_pretty TIMESTAMP, 
  ip STRING, 
  item_class STRING, 
  item_id STRING, 
  item_name STRING, 
  item_in BIGINT, 
  item_out BIGINT, 
  item_type STRING, 
  lang STRING, 
  level STRING, 
  location STRING, 
  os STRING, 
  os_version STRING, 
  snsid STRING, 
  uid STRING, 
  x_from ARRAY<STRING>
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_item_transaction_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
  );
  
  
 CREATE EXTERNAL TABLE IF NOT EXISTS raw_rc_transaction_daily (
  key   STRING,
  ts   STRING,
  ts_pretty   TIMESTAMP,
  action   STRING,
  action_detail   STRING,
  browser   STRING,
  browser_version   STRING,
  country_code   STRING,
  event   STRING,
  install_source   STRING,
  install_ts   STRING,
  install_ts_pretty   TIMESTAMP,
  ip   STRING,
  lang   STRING,
  level   BIGINT,
  location   STRING,
  os   STRING,
  os_version   STRING,
  rc_bal   BIGINT,
  rc_in   BIGINT,
  rc_out   BIGINT,
  snsid   STRING,
  uid   STRING
  )
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_rc_transaction_daily'
TBLPROPERTIES (
'serialization.null.format'=''
);
	

CREATE EXTERNAL TABLE IF NOT EXISTS raw_coins_transaction_daily (
	key   STRING,
	ts   STRING,
	ts_pretty   TIMESTAMP,
	action   STRING,
	action_detail   STRING,
	browser   STRING,
	browser_version   STRING,
	coins_bal   DOUBLE,
	coins_in   BIGINT,
	coins_out   BIGINT,
	country_code   STRING,
	event   STRING,
	install_source   STRING,
	install_ts   STRING,
	install_ts_pretty   TIMESTAMP,
	ip   STRING,
	lang   STRING,
	level   BIGINT,
	location   STRING,
	os   STRING,
	os_version   STRING,
	snsid   STRING,
	uid   STRING
	)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_coins_transaction_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
  );
  
  

CREATE EXTERNAL TABLE IF NOT EXISTS fact_ledger
(
id  STRING,
date STRING,
ts TIMESTAMP,
user_key STRING,
app_name  STRING,
uid STRING,
snsid STRING,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
in_amount BIGINT,
out_name STRING,
out_type STRING,
out_amount BIGINT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_ledger'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS agg_daily_ledger
(
id  STRING,
date STRING,
app_name  STRING,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
out_name STRING,
out_type STRING,
in_amount INT,
out_amount INT,
purchaser_cnt INT,
user_cnt  INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_daily_ledger'
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS fact_item
(
id  STRING,
date STRING,
ts TIMESTAMP,
user_key STRING,
app_name  STRING,
uid STRING,
snsid STRING,
install_date STRING,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
in_amount BIGINT,
out_name STRING,
out_type STRING,
out_amount BIGINT,
item_class  STRING,
item_id STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_item'
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS agg_item
(
id  STRING,
date STRING,
app_name  STRING,
install_date STRING,
level INT,
transaction_type STRING,
in_name STRING,
in_type STRING,
out_name STRING,
out_type STRING,
in_amount BIGINT,
out_amount BIGINT,
purchaser_cnt INT,
user_cnt  INT,
item_class  STRING,
item_id STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_item'
TBLPROPERTIES('serialization.null.format'='');


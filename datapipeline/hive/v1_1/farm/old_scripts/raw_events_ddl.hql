use farm_1_1;



CREATE EXTERNAL TABLE IF NOT EXISTS raw_Achievement (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
achievement_id   STRING,
achievement_name   STRING,
achievement_step   BIGINT,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/Achievement';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_Calendar (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
open_date   STRING,
os   STRING,
os_version   STRING,
sign_day   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/Calendar';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_Construction (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
coins_cost   DOUBLE,
country_code   STRING,
event   STRING,
finish_now   BIGINT,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item1_id   STRING,
item1_name   STRING,
item1_quantity   STRING,
item_id   STRING,
item_name   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/Construction';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_ContinuousLogin (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
coins_get   BIGINT,
country_code   STRING,
days_login   BIGINT,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
last_login_time   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/ContinuousLogin';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_DAU (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/DAU';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_DAU (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/DAU';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_ExchangeTimePoint (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
gas   BIGINT,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
time_point   BIGINT,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/ExchangeTimePoint';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_FarmClub (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
farm_club_id   STRING,
farm_club_point   BIGINT,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item1_id   STRING,
item1_name   STRING,
item1_quantity   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/FarmClub';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_Fertilize (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
fertilizer_id   STRING,
fertilizer_left   BIGINT,
fertilizer_name   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/Fertilize';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_Fishing (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
items   ARRAY<STRUCT<id:STRING,qty:BIGINT,name:STRING>>,
lang   STRING,
level   BIGINT,
material_id   STRING,
material_name   STRING,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/Fishing';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_LevelUpReward (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item1_id   STRING,
item1_name   STRING,
item1_quantity   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/LevelUpReward';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_MarketCrateFinish (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
assist   BOOLEAN,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_quantity   BIGINT,
lang   STRING,
level   BIGINT,
order_points_get   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/MarketCrateFinish';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_MarketCrateNeedHelp (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_quantity   BIGINT,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/MarketCrateNeedHelp';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_MarketExchange (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_quantity   BIGINT,
lang   STRING,
level   BIGINT,
new_cash1_cost   BIGINT,
new_cash1_left   BIGINT,
new_cash2_cost   BIGINT,
new_cash2_left   STRING,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/MarketExchange';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_MarketOrderBegin (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
crates   MAP<STRING,BIGINT>,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
new_cash1   BIGINT,
order_points   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
time_left   BIGINT,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/MarketOrderBegin';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_MarketOrderFinish (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
crates   MAP<STRING,BIGINT>,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
new_cash1_get   BIGINT,
order_points_get   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
time_left   BIGINT,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/MarketOrderFinish';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_NotificationReward (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
coins_get   BIGINT,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/NotificationReward';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_PurchaseItem (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
coins_cost   DOUBLE,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_name   STRING,
item_quantity   BIGINT,
item_type   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
src   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/PurchaseItem';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_ShopBuy (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_name   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
shop_type   STRING,
snsid   STRING,
target_item_id   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/ShopBuy';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_ShopProduct (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_name   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/ShopProduct';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_ShopSpeedUp (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_name   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/ShopSpeedUp';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_SmallGame (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
game   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/SmallGame';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_SpecialDelivery (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
items   ARRAY<STRUCT<id:STRING,qty:BIGINT,name:STRING>>,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/SpecialDelivery';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_SpeedUp (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_id   STRING,
item_name   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/SpeedUp';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_SpendTimePoint (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
machine_id   STRING,
machine_name   STRING,
os   STRING,
os_version   STRING,
product_quantity   BIGINT,
snsid   STRING,
time_point   BIGINT,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/SpendTimePoint';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_StoryPostpone (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_cost   BIGINT,
snsid   STRING,
story_id   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/StoryPostpone';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_ThirdSceneExpand (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
country_code   STRING,
event   STRING,
expansion_size_from   BIGINT,
expansion_size_to   BIGINT,
expansion_top_size_from   BIGINT,
greenery_bal   BIGINT,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
snsid   STRING,
uid   STRING,
waterLevel   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/ThirdSceneExpand';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_WaterExpChange (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
action   STRING,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
expansion_size_from   BIGINT,
greenery_bal   BIGINT,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING,
water_exp   STRING,
water_exp_in   STRING,
water_level   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/WaterExpChange';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_WaterLevelUp (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
expansion_size_from   BIGINT,
greenery_bal   BIGINT,
greenery_in   BIGINT,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
new_water_level   BIGINT,
old_water_level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_get   STRING,
snsid   STRING,
uid   STRING,
water_exp   STRING,
water_level   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/WaterLevelUp';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_coins_transaction (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
action   STRING,
action_detail   STRING,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
coins_in   BIGINT,
coins_out   BIGINT,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
location   STRING,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/coins_transaction';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_item_transaction (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
action   STRING,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
gifted_to   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
item_class   STRING,
item_id   STRING,
item_name   STRING,
item_in   BIGINT,
item_out   BIGINT,
item_type   STRING,
lang   STRING,
level   BIGINT,
location   STRING,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING,
x_from   ARRAY<STRING>
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/item_transaction';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_levelup (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
coins_get   BIGINT,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_get   STRING,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/levelup';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_newuser (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
gender   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING,
device  STRING,
fb_source STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/newuser';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_payment (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
amount   DOUBLE,
browser   STRING,
browser_version   STRING,
coins_bal   DOUBLE,
coins_in   BIGINT,
country_code   STRING,
currency   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
is_gift   BIGINT,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
payment_processor   STRING,
product_id   STRING,
product_name   STRING,
product_type   STRING,
raw   STRUCT<amount:DOUBLE,cash_type:STRING,currency:STRING,from_uid:STRING,gameamount:STRING, gift:STRING,gift_id:STRING,level:BIGINT,paid_time:STRING,status:BIGINT,type:STRING,uid:STRING>,
rc_bal   BIGINT,
rc_in   BIGINT,
snsid   STRING,
transaction_id   STRING,
uid   STRING,
fb_source STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/payment';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_quest (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
action   STRING,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
quest_id   STRING,
quest_type   STRING,
snsid   STRING,
task_id   BIGINT,
uid   STRING,
rc_in BIGINT,
rc_out BIGINT,
rc_bal BIGINT,
coins_in BIGINT,
coins_out BIGINT,
coins_bal DOUBLE
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/quest';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_rc_transaction (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
action   STRING,
action_detail   STRING,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
location   STRING,
os   STRING,
os_version   STRING,
rc_bal   BIGINT,
rc_in   BIGINT,
rc_out   BIGINT,
snsid   STRING,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/rc_transaction';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_end (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
duration   BIGINT,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING,
fb_source STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/session_end';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_session_start (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
gender   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING,
device  STRING,
fb_source STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/session_start';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_tutorial (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
step   BIGINT,
uid   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/tutorial';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_invalid (
key   STRING,
ts   STRING,
action   STRING,
browser   STRING,
browser_version   STRING,
coins_in   BIGINT,
error   STRING,
errorDetails   ARRAY<STRING>,
event   STRING,
ip   STRING,
lang   STRING,
location   STRING,
os   STRING,
os_version   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/invalid';

CREATE EXTERNAL TABLE IF NOT EXISTS raw_personal_info (
key   STRING,
ts   STRING,
ts_pretty   TIMESTAMP,
browser   STRING,
browser_version   STRING,
country_code   STRING,
event   STRING,
install_source   STRING,
install_ts   STRING,
install_ts_pretty   TIMESTAMP,
ip   STRING,
lang   STRING,
level   BIGINT,
os   STRING,
os_version   STRING,
snsid   STRING,
uid   STRING,
fb_source STRING,
additional_email   STRING
)
PARTITIONED BY(
app STRING,
year int,
month INT,
day INT,
hour INT
)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
WITH SERDEPROPERTIES (
"mapping.ts" = "@ts",
"mapping.ts_pretty" ="@ts_pretty",
"mapping.key"="@key"
)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events/personal_info';

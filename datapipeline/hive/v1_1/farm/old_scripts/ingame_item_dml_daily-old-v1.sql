
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;


  -- Populate Fact Item --

INSERT OVERWRITE TABLE fact_item PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT MD5(CONCAT (id, ROW_NUMBER() OVER (PARTITION BY id))) as id, date, ts, user_key, app, uid, snsid, install_date, level, action, in_name, in_type, in_amount
, out_name, out_type, out_amount, item_class, item_id
FROM (
SELECT
MD5(CONCAT(t.app,t.ts,t.uid,t.action,t.level,COALESCE(t.gifted_to,'Unknown'),COALESCE(t.item_class,'Unknown'),COALESCE(t.item_type,'Unknown'),COALESCE(t.item_name,'Unknown'),COALESCE(t.item_id,'Unknown'),COALESCE(t.item_in,0),COALESCE(t.item_out,0),t.snsid)) AS id,
dt date,
 t.ts_pretty ts,
 MD5(CONCAT(t.app,t.uid)) AS user_key,
  t.app,
  t.uid,
  t.snsid,
  TO_DATE(t.install_ts_pretty) install_date,
  t.level,
  t.action,
  CASE WHEN COALESCE(t.item_in,0)>0 AND t.item_name != '' THEN COALESCE(t.item_name,'Unknown') ELSE 'Unknown' END  in_name,
  CASE WHEN COALESCE(t.item_in,0)>0 AND t.item_type != '' THEN COALESCE(t.item_type,'Unknown') ELSE 'Unknown' END in_type,
  CASE WHEN COALESCE(t.item_in,0)>0 THEN t.item_in ELSE 0 END  in_amount,
  CASE WHEN COALESCE(t.item_out,0)>0 AND t.item_name != '' THEN COALESCE(t.item_name,'Unknown') ELSE 'Unknown' END  out_name,
  CASE WHEN COALESCE(t.item_out,0)>0 AND t.item_type != '' THEN COALESCE(t.item_type,'Unknown') ELSE 'Unknown' END  out_type,
  CASE WHEN COALESCE(t.item_out,0)>0 THEN t.item_out ELSE 0 END  out_amount,
  item_class,
  item_id
FROM raw_item_transaction_daily t 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
) x
;

-- Populate Agg Item --

INSERT OVERWRITE TABLE agg_item PARTITION PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(a.date,a.app,a.level,a.transaction_type,a.in_name,a.in_type,a.out_name,a.out_type,a.item_class,a.item_id)) AS id,
a.date,
a.app,
null install_date,
a.level,
a.transaction_type,
a.in_name,
a.in_type,
a.out_name,
a.out_type,
SUM(COALESCE(in_amount,0)) in_amount,
SUM(COALESCE(out_amount,0)) out_amount,
COUNT(DISTINCT a.user_key) purchase_cnt,
0 user_cnt,
a.item_class,
a.item_id
FROM
 fact_item a 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
GROUP BY a.date, a.app, a.level, a.transaction_type, a.in_name, a.in_type, a.out_name, a.out_type, a.item_class, a.item_id,a.dt;


exit;

USE farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;


ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_item_transaction ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);

INSERT OVERWRITE TABLE raw_item_transaction_daily PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
  key, 
  ts, 
  ts_pretty, 
  action, 
  browser, 
  browser_version, 
  country_code, 
  event, 
  gifted_to, 
  install_source, 
  install_ts, 
  install_ts_pretty, 
  ip, 
  item_class, 
  item_id, 
  item_name, 
  item_in, 
  item_out, 
  item_type, 
  lang, 
  level, 
  location, 
  os, 
  os_version, 
  snsid, 
  uid, 
  x_from
  FROM raw_item_transaction WHERE app='${hiveconf:rpt_app}' AND year=${hiveconf:rpt_year} AND month = ${hiveconf:rpt_month} AND day = ${hiveconf:rpt_day} ;

exit;

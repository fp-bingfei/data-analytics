CREATE TABLE IF NOT EXISTS copy_fact_session(
id string,
app_id string,
app_version string,
user_key string,
uid string,
date_start string,
date_end string,
ts_start timestamp,
ts_end timestamp,
install_ts timestamp,
install_date string,
session_id string,
device_key string,
facebook_id string,
install_source string,
os string,
os_version string,
browser string,
browser_version string,
device string,
country_code string,
level_start int,
level_end int,
gender string,
birthday string,
email string,
ip string,
language string,
locale string,
rc_wallet_start int,
rc_wallet_end int,
coin_wallet_start bigint,
coin_wallet_end bigint,
ab_experiment string,
ab_variant string,
session_length_sec int)
PARTITIONED BY (
app string,
dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_session'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS `copy_agg_retention`(
`player_day` int, 
`app_id` string, 
`app_version` string, 
`install_date` string, 
`install_source` string, 
`install_subpublisher` string, 
`install_campaign` string, 
`install_creative_id` string, 
`device_alias` string, 
`os` string, 
`browser` string, 
`country` string, 
`language` string, 
`ab_experiment` string, 
`ab_variant` string, 
`is_payer` int, 
`new_user_cnt` int, 
`retained_user_cnt` int)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/agg_retention'
TBLPROPERTIES (
'serialization.null.format'='');


CREATE TABLE IF NOT EXISTS `copy_agg_ltv`(
`player_day` int, 
`app_id` string, 
`app_version` string, 
`install_date` string, 
`install_source` string, 
`install_subpublisher` string, 
`install_campaign` string, 
`install_creative_id` string, 
`device_alias` string, 
`os` string, 
`browser` string, 
`country` string, 
`language` string, 
`ab_experiment` string, 
`ab_variant` string, 
`is_payer` int, 
`new_user_cnt` int, 
`revenue` double)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/agg_ltv'
TBLPROPERTIES (
'serialization.null.format'='');


CREATE TABLE IF NOT EXISTS `copy_agg_iap`(
`date`  string, 
`app_id`  string, 
`app_version`  string, 
`level`   int, 
`install_date`  string, 
`install_source`  string, 
`country`  string, 
`os`  string, 
`ab_experiment`  string, 
`ab_variant`  string, 
`conversion_purchase`  int, 
`product_id`  string, 
`product_type`  string, 
`revenue_usd`  double, 
`purchase_cnt`  int, 
`purchase_user_cnt`  int)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/agg_iap'
TBLPROPERTIES (
'serialization.null.format'=''
);

CREATE TABLE IF NOT EXISTS copy_fact_revenue
(
id STRING,
app_id STRING,
app_version STRING,
user_key STRING,
uid STRING,
date STRING,
ts TIMESTAMP,
install_ts TIMESTAMP,
install_date STRING,
device_key STRING,
session_id STRING,
level int,
os STRING,
os_version STRING,
device STRING,
browser STRING,
browser_version STRING,
country_code STRING,
install_source STRING,
ip STRING,
language STRING,
locale STRING,
ab_experiment STRING,
ab_variant STRING,
coin_wallet BIGINT,
rc_wallet INT,
payment_processor STRING, 
product_id STRING,
product_name STRING,
product_type STRING, 
coins_in BIGINT ,
rc_in INT,
currency STRING,
revenue_amount DOUBLE,
revenue_usd DOUBLE,
transaction_id STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_revenue'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_fact_levelup 
(
idSTRING COMMENT 'id is md5(concat(app_name, uid,current_level,levelup_ts)',
user_keySTRING,
app_idSTRING COMMENT 'app_id is the same as app which is being used as partition key',
uidSTRING,
session_id STRING,
previous_levelSMALLINT,
previous_levelup_dateSTRING,
previous_levelup_tsTIMESTAMP,
current_levelSMALLINT,
levelup_date STRING,
levelup_ts TIMESTAMP,
browser STRING,
browser_version STRING,
os STRING,
os_version STRING,
device_key STRING,
device STRING,
coin_wallet BIGINT,
coins_in BIGINT,
country_code STRING,
ip STRING,
language STRING,
rc_wallet BIGINT,
rc_in BIGINT,
locale STRING,
ab_experiment STRING,
ab_variant STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_levelup'
TBLPROPERTIES('serialization.null.format'='');



CREATE TABLE IF NOT EXISTS copy_fact_dau_snapshot
(
id STRING,
user_key STRING,
date STRING,
device_key STRING,
app_id STRING,
app_version STRING,
level_start int,
level_end int,
os STRING,
os_version STRING,
device STRING,
browser STRING,
browser_version STRING,
country_code STRING,
country STRING,
language STRING,
gender STRING,
ab_experiment STRING,
ab_variant STRING,
is_new_user SMALLINT,
rc_in INT,
coins_in BIGINT,
rc_out INT,
coins_out BIGINT,
rc_wallet INT,
coin_wallet BIGINT,
is_payer SMALLINT,
is_converted_today SMALLINT,
revenue_usd DOUBLE,
payment_cnt INT,
session_cnt INT,
playtime_sec INT
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_dau_snapshot'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS copy_dim_user
(
idSTRING,
user_key STRING,
app_id STRING,
uidSTRING,
device_key STRING,
facebook_id STRING,
install_tsTIMESTAMP,
install_dateSTRING,
install_sourceSTRING,
install_subpublisherSTRING,
install_campaignSTRING,
install_languageSTRING,
install_localeSTRING,
install_country_codeSTRING,
install_countrySTRING,
install_osSTRING,
install_deviceSTRING,
install_device_aliasSTRING,
install_browserSTRING,
install_genderSTRING,
install_ageSTRING,
languageSTRING,
locale STRING,
birthdaySTRING,
gender STRING,
country_codeSTRING,
countrySTRING,
os STRING,
os_version STRING,
deviceSTRING,
device_aliasSTRING,
browser STRING,
browser_version STRING,
app_version STRING,
level INT,
levelup_ts TIMESTAMP,
ab_experiment STRING,
ab_variant STRING,
is_payer INT,
conversion_ts TIMESTAMP,
total_revenue_usd DECIMAL(12,4),
payment_cnt INT,
last_login_ts TIMESTAMP,
coin_wallet DOUBLE,
coins_in BIGINT,
coins_out BIGINT,
rc_wallet DOUBLE,
rc_in BIGINT,
rc_out BIGINT,
email STRING,
last_ip STRING
)
PARTITIONED BY (
app STRING,
dtSTRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/dim_user'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_agg_kpi(
id string, 
date string, 
app_id string, 
app_version string, 
install_year string,
install_month string, 
install_week string, 
install_source string, 
level_end int, 
browser string, 
browser_version string, 
device_alias string, 
country string, 
os string, 
os_version string, 
language string, 
is_new_user int, 
is_payer int, 
new_user_cnt int, 
dau_cnt int, 
newpayer_cnt int, 
payer_today_cnt int, 
payment_cnt int, 
revenue_usd double, 
session_cnt int, 
session_length_sec int)
PARTITIONED BY ( 
app string, 
dt string)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION
's3://com.funplus.datawarehouse/farm_1_1/copy/agg_kpi'
TBLPROPERTIES ('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS copy_agg_quest
(
<<<<<<< HEAD
install_dateSTRING,
app_id STRING,
os STRING,
os_version STRING,
countrySTRING,
install_sourceSTRING,
browser STRING,
browser_version STRING,
languageSTRING,
quest_id STRING,
quest_type STRING,
task_id STRING,
action STRING,
quest_cnt BIGINT,
rc_out BIGINT 
)
=======
  install_date  STRING,
  app_id STRING,
  os STRING,
  os_version STRING,
  country  STRING,  
  install_source  STRING,
  browser STRING,
  browser_version STRING,
  language  STRING,
  quest_id STRING,
  quest_type STRING,
  task_id STRING,
  action STRING,
  user_cnt BIGINT,
  rc_out BIGINT     
)  
>>>>>>> 9d0867735b0ba3544eddb2deff8303df7a5208e3
PARTITIONED BY (
app STRING,
dtSTRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_quest'
TBLPROPERTIES('serialization.null.format'='');

CREATE EXTERNAL TABLE IF NOT EXISTS copy_agg_tutorial
(
<<<<<<< HEAD
install_dateSTRING,
app_id STRING,
os STRING,
os_version STRING,
countrySTRING,
install_sourceSTRING,
browser STRING,
browser_version STRING,
languageSTRING,
step INT,
tutorial_cnt BIGINT
)
=======
  install_date  STRING,
  app_id STRING,
  os STRING,
  os_version STRING,
  country  STRING,  
  install_source  STRING,
  browser STRING,
  browser_version STRING,
  language  STRING,
  step INT,
  user_cnt BIGINT
)  
>>>>>>> 9d0867735b0ba3544eddb2deff8303df7a5208e3
PARTITIONED BY (
app STRING,
dtSTRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_tutorial'
TBLPROPERTIES('serialization.null.format'='');
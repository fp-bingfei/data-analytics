----------------------------------
-- Query to generate fact_levelup
----------------------------------
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE fact_levelup PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select md5(concat(l.key, l.uid,l.level,l.ts)) AS id, md5(concat(l.key, l.uid)) AS user_key, l.key as app_id, l.uid as app_user_id
, null as session_id, COALESCE(lag(l.level) over (partition by l.key, l.uid order by l.level),l.level - 1) AS previous_level
, to_date(COALESCE(lag(l.ts_pretty) over (partition by l.key, l.uid order by l.level), prev.levelup_ts, n.install_ts_pretty)) AS previous_levelup_date
, COALESCE(lag(l.ts_pretty) over (partition by l.key, l.uid order by l.level), prev.levelup_ts, n.install_ts_pretty) AS previous_levelup_ts
, l.level, l.dt AS levelup_date, l.ts_pretty AS levelup_ts, l.browser, l.browser_version, l.os, l.os_version
, null as device, l.coins_bal as coins_wallet, l.coins_get as coins_in
, l.country_code, l.ip, l.lang AS language, l.rc_bal as rc_wallet, l.rc_get as rc_in
, null as locale, null as ab_experiment, null as ab_variant
from 
(
	select * from (
	select *, row_number() over (partition by app, uid, level order by ts_pretty asc) rnum 
	from raw_levelup_daily where app='${hiveconf:rpt_app}' and dt ='${hiveconf:rpt_date}'
	) X where rnum = 1
)  l
left outer join fact_levelup prev on (l.app = prev.app  and l.uid = prev.app_user_id and l.level = (prev.current_level + 1) 
	and prev.dt <= '${hiveconf:rpt_date}')
left outer join raw_newuser_daily n on (l.app = n.app  and l.uid = n.uid and n.dt <= '${hiveconf:rpt_date}');

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

INSERT OVERWRITE TABLE copy_fact_levelup PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
select
id, user_key, app_id, app_user_id, session_id, previous_level, previous_levelup_date, previous_levelup_ts, current_level, levelup_date, levelup_ts, browser, browser_version, 
os, os_version,  device, coin_wallet, coins_in, country_code, ip, language, rc_wallet, rc_in, locale, ab_experiment, ab_variant 
from fact_levelup where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

exit;

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions.pernode=100000;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.stats.autogather=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat;

INSERT OVERWRITE TABLE agg_sendgrid_retention PARTITION(app, install_dt) 
SELECT install_date, app, retention_days, language
  ,os
  ,browser
  ,country
  ,install_source
  ,is_payer
  , email_flag
  , count(*)
  , app AS appname
  , install_date AS install_dt
    FROM (
  select
   du.install_date   
   ,du.app
   ,datediff(fds.date, du.install_date) AS retention_days
   ,du.app_user_id
  ,du.language 
  ,du.os
  ,du.browser
  ,du.country
  ,du.install_source
  ,du.is_payer
  , case when fs.app_user_id is NOT null then true else false end AS email_flag
FROM
(
  SELECT install_date
   ,app
   ,user_key
   ,app_user_id   
  ,language
  ,os
  ,browser
  ,country
  ,install_source
  ,is_payer FROM
dim_user x  WHERE 
x.app in (select distinct app_id from fact_sendgrid where campaign_dt > '${hiveconf:rpt_date_days_back}' AND campaign_dt <= '${hiveconf:rpt_date}')
AND dt = '${hiveconf:rpt_date}'
AND install_date > '${hiveconf:rpt_date_days_back}' AND install_date <= '${hiveconf:rpt_date}')
du
INNER JOIN 
(select app, user_key, date from fact_dau_snapshot y
  WHERE y.app in (select distinct app_id from fact_sendgrid where campaign_dt > '${hiveconf:rpt_date_days_back}' AND campaign_dt <= '${hiveconf:rpt_date}')
AND dt > '${hiveconf:rpt_date_days_back}' AND  dt <= '${hiveconf:rpt_date}')
fds ON (du.app = fds.app AND du.user_key = fds.user_key)
LEFT OUTER JOIN 
(SELECT distinct app, app_user_id FROM fact_sendgrid WHERE campaign_dt > '${hiveconf:rpt_date_days_back}' AND campaign_dt <= '${hiveconf:rpt_date}'
AND delivered_ts is not null and bounce_ts is null )
fs
ON (du.app = fs.app and du.app_user_id = fs.app_user_id)
) ASR GROUP BY install_date, app, retention_days, language
  ,os
  ,browser
  ,country
  ,install_source
  ,is_payer
  , email_flag
  DISTRIBUTE BY app, install_date;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
  
ALTER TABLE copy_agg_sendgrid_retention DROP IF EXISTS PARTITION (app='farm.all.prod');

INSERT OVERWRITE TABLE copy_agg_sendgrid_retention PARTITION (app='farm.all.prod',dt='${hiveconf:rpt_date}')
SELECT install_date
, app_id
, retention_days
, language
, os
, browser
, country
, install_source
, is_payer
, email_flag
, user_cnt
FROM agg_sendgrid_retention WHERE install_dt > '${hiveconf:rpt_date_days_back}' AND  install_dt <= '${hiveconf:rpt_date}';


exit;


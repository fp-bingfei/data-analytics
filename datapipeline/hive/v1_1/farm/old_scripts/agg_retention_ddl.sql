use farm_1_1;


CREATE EXTERNAL TABLE IF NOT EXISTS player_day 
(day INT)
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/others/player_day'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS tmp_player_day_cube 
(   
player_day INT,
app_id   STRING,
app_version   STRING,
install_date STRING,
install_source STRING,
install_subpublisher STRING,
install_campaign STRING,
install_creative_id STRING,
device_alias STRING,
os STRING,
browser STRING,
country STRING,
language STRING,
ab_experiment STRING,
ab_variant STRING,
is_payer INT,
new_user_cnt INT
)
PARTITIONED BY ( 
  app STRING, 
  dt  STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='');



CREATE TABLE IF NOT EXISTS tmp_agg_retention_ltv 
(   
player_day INT,
app_id   STRING,
app_version   STRING,
install_date STRING,
install_source STRING,
install_subpublisher STRING,
install_campaign STRING,
install_creative_id STRING,
device_alias STRING,
os STRING,
browser STRING,
country STRING,
language STRING,
ab_experiment STRING,
ab_variant STRING,
is_payer INT,
new_user_cnt INT,
retained_user_cnt  INT,
revenue   DOUBLE,
cumulative_revenue   DOUBLE
)
PARTITIONED BY ( 
  app STRING,
  dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS agg_retention 
(   
player_day INT,
app_id   STRING,
app_version   STRING,
install_date STRING,
install_source STRING,
install_subpublisher STRING,
install_campaign STRING,
install_creative_id STRING,
device_alias STRING,
os STRING,
browser STRING,
country STRING,
language STRING,
ab_experiment STRING,
ab_variant STRING,
is_payer INT,
new_user_cnt INT,
retained_user_cnt  INT
)
PARTITIONED BY ( 
  app STRING, 
  dt  STRING )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_retention'
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS agg_ltv
(   
player_day INT,
app_id   STRING,
app_version   STRING,
install_date STRING,
install_source STRING,
install_subpublisher STRING,
install_campaign STRING,
install_creative_id STRING,
device_alias STRING,
os STRING,
browser STRING,
country STRING,
language STRING,
ab_experiment STRING,
ab_variant STRING,
is_payer INT,
new_user_cnt INT,
revenue  DOUBLE
)
PARTITIONED BY ( 
  app STRING, 
  dt  STRING )
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_ltv'
TBLPROPERTIES('serialization.null.format'='');


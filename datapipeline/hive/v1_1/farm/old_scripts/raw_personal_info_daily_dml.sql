
USE farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;



ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=00);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=01);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=02);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=03);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=04);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=05);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=06);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=07);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=08);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=09);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=10);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=11);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=12);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=13);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=14);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=15);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=16);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=17);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=18);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=19);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=20);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=21);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=22);
ALTER TABLE raw_personal_info ADD IF NOT EXISTS PARTITION (app='${hiveconf:rpt_app}',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=23);



INSERT OVERWRITE TABLE raw_personal_info_daily PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT 
  key,
  ts,
  ts_pretty,
  browser,
  browser_version,
  country_code,
  event,
  install_source,
  install_ts,
  install_ts_pretty,
  ip,
  lang,
  level,
  os,
  os_version,
  snsid,
  uid,
  fb_source,
  additional_email
FROM 
  raw_personal_info
WHERE 
  app='${hiveconf:rpt_app}' AND 
  year=${hiveconf:rpt_year} AND 
  month = ${hiveconf:rpt_month} AND 
  day = ${hiveconf:rpt_day};


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

INSERT OVERWRITE TABLE copy_personal_info_daily PARTITION(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') 
SELECT 
  MD5(concat(app,uid,ts)) as id,
  key as app_id,
  ts_pretty as ts,
  to_date(ts_pretty) as date,
  browser,
  browser_version,
  country_code,
  event,
  install_source,
  install_ts_pretty as install_ts,
  to_date(install_ts_pretty) as install_date,
  ip,
  lang,
  level,
  os,
  os_version,
  snsid,
  uid,
  fb_source,
  additional_email
FROM 
  raw_personal_info_daily
WHERE 
  app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

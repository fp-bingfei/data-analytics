use farm_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS agg_quest
(
  install_date  STRING,
  app_id STRING,
  os STRING,
  os_version STRING,
  country  STRING,  
  install_source  STRING,
  browser STRING,
  browser_version STRING,
  language  STRING,
  quest_id STRING,
  quest_type STRING,
  task_id STRING,
  action STRING,
  user_cnt BIGINT,
  rc_out BIGINT     
)  
PARTITIONED BY (
app   STRING,
install_dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_quest'
TBLPROPERTIES('serialization.null.format'='');





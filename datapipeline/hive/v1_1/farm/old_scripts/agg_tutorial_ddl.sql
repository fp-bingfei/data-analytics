use farm_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS agg_tutorial
(
  install_date  STRING,
  app_id STRING,
  os STRING,
  os_version STRING,
  country  STRING,  
  install_source  STRING,
  browser STRING,
  browser_version STRING,
  language  STRING,
  step INT,
  user_cnt BIGINT
)  
PARTITIONED BY (
app   STRING,
install_dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/agg_tutorial'
TBLPROPERTIES('serialization.null.format'='');





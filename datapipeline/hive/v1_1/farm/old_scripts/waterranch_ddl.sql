use farm_1_1;

--waterexpchange_daily_data
create external table waterexpchange_daily (
 user_key STRING,
 app_user_id STRING,
 app_id STRING,
 ts STRING,
 install_dt STRING,
 install_source STRING,
 os STRING,
 os_version STRING,
 browser STRING,
 browser_version STRING,
 country_code STRING,
 action STRING,
 exp_size_from STRING,
 greenery_in INT,
 greenery_out INT,
 greenery_bal INT,
 level INT,
 water_exp_in INT,
 water_level INT,
 water_exp_bal INT
) PARTITIONED BY (app   STRING, dt  STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/waterexpchange_daily';


create external table rpt_water_exp_in (
id string,
app_id          string,
dt              string,
install_dt      string,
install_source  string,
os              string,
os_version      string,
browser         string,
browser_version string,
country_code    string,
action          string,
exp_size_from   string,
level           int   ,
water_level     int   ,
id_col string,
app_user_id_cnt int, -- Better name ?
tot_exp_in      bigint
) PARTITIONED BY (app   STRING, dt  STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/rpt_water_exp_in';

create external table rpt_water_exp_greenery (
id string,
app_id          string,
dt              string,
install_dt      string,
install_source  string,
os              string,
os_version      string,
browser         string,
browser_version string,
country_code    string,
action          string,
exp_size_from   string,
level           int   ,
water_level     int   ,
id_col string,
tot_greenery_in      bigint,
tot_greenery_out      bigint
) PARTITIONED BY (app   STRING, dt  STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/rpt_water_exp_greenery';

-- wr_3rdscene_daily_data
create external table thirdsceneexpand_daily (
 user_key STRING,
 app_user_id STRING,
 app_id STRING,
 ts STRING,
 install_dt STRING,
 install_source STRING,
 os STRING,
 os_version STRING,
 browser STRING,
 browser_version STRING,
 country_code STRING,
 action STRING,
 exp_size_from STRING,
 greenery_bal BIGINT,
 coins_bal BIGINT,
 rc_bal BIGINT,
 level INT,
 waterLevel INT,
 expansion_size_to INT,
 expansion_top_size_to INT
) PARTITIONED BY (app   STRING, dt  STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/thirdsceneexpand_daily';

-- rpt_3rdscene_exp
create external table rpt_thirdsceneexpand_exp (
id string,
app_id          string,
dt              string,
install_dt      string,
install_source  string,
os              string,
os_version      string,
browser         string,
browser_version string,
country_code    string,
action          string,
exp_size_from   string,
level           int,
waterLevel     int,
expansion_size_to int,
expansion_top_size_to int,
id_col string,
expansion_size_to_cnt bigint,
expansion_top_size_to_cnt bigint
) PARTITIONED BY (app   STRING, dt  STRING)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/rpt_thirdsceneexpand_exp';
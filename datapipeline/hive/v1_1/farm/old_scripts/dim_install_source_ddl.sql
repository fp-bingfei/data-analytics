use farm_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS dim_install_source
(
  id   STRING,
  user_key          STRING,
  app_name          STRING,
  uid               INT,
  install_date      STRING,
  install_source_event_raw  STRING,
  install_source_event  STRING,
  install_source_3rdparty_raw STRING,
  install_source_3rdparty STRING,
  install_source_raw    STRING,
  install_source    STRING,
  campaign          STRING,
  sub_publisher     STRING
)
PARTITIONED BY(
app STRING,
year INT,
month INT,
day INT
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/dim_install_source'
TBLPROPERTIES('serialization.null.format'='');


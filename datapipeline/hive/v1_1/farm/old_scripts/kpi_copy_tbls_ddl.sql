CREATE TABLE IF NOT EXISTS copy_fact_session(
    `id` string, 
    `app_id` string, 
    `app_version` string, 
    `user_key` string, 
    `app_user_id` string, 
    `date_start` string, 
    `date_end` string, 
    `ts_start` timestamp, 
    `ts_end` timestamp, 
    `install_ts` timestamp, 
    `install_date` string, 
    `session_id` string, 
    `facebook_id` string, 
    `install_source` string, 
    `os` string, 
    `os_version` string, 
    `browser` string, 
    `browser_version` string, 
    `device` string, 
    `country_code` string, 
    `level_start` int, 
    `level_end` int, 
    `gender` string, 
    `birthday` string, 
    `email` string, 
    `ip` string, 
    `language` string, 
    `locale` string, 
    `rc_wallet_start` int, 
    `rc_wallet_end` int, 
    `coin_wallet_start` bigint, 
    `coin_wallet_end` bigint, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `session_length_sec` int, 
    `idfa` string, 
    `idfv` string, 
    `gaid` string, 
    `mac_address` string, 
    `android_id` string,
    `fb_source`  string
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_session'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE `copy_agg_retention`(
  `player_day` int, 
  `app_id` string, 
  `app_version` string, 
  `install_date` string, 
  `install_source` string, 
  `install_subpublisher` string, 
  `install_campaign` string, 
  `install_creative_id` string, 
  `device_alias` string, 
  `os` string, 
  `browser` string, 
  `country` string, 
  `language` string, 
  `ab_experiment` string, 
  `ab_variant` string, 
  `is_payer` int, 
  `new_user_cnt` int, 
  `retained_user_cnt` int)
  PARTITIONED BY(
  app STRING,
  dt STRING
  )
  ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
  STORED AS TEXTFILE
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/copy/agg_retention'
TBLPROPERTIES (
  'serialization.null.format'='');
  

CREATE TABLE IF NOT EXISTS copy_fact_revenue
(
    `id` string, 
    `app_id` string, 
    `app_version` string, 
    `user_key` string, 
    `app_user_id` string, 
    `date` string, 
    `ts` timestamp, 
    `install_ts` timestamp, 
    `install_date` string, 
    `session_id` string, 
    `level` int, 
    `os` string, 
    `os_version` string, 
    `device` string, 
    `browser` string, 
    `browser_version` string, 
    `country_code` string, 
    `install_source` string, 
    `ip` string, 
    `language` string, 
    `locale` string, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `coin_wallet` bigint, 
    `rc_wallet` int, 
    `payment_processor` string, 
    `product_id` string, 
    `product_name` string, 
    `product_type` string, 
    `coins_in` bigint, 
    `rc_in` int, 
    `currency` string, 
    `revenue_amount` double, 
    `revenue_usd` double, 
    `transaction_id` string, 
    `idfa` string, 
    `idfv` string, 
    `gaid` string, 
    `mac_address` string, 
    `android_id` string,
    `fb_source`  string
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_revenue'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_fact_levelup 
(
    `id` string COMMENT 'id is md5(concat(app_name, app_user_id,current_level,levelup_ts)', 
     `user_key` string, 
     `app_id` string COMMENT 'app_id is the same as app which is being used as partition key', 
     `app_user_id` string, 
     `session_id` string, 
     `previous_level` smallint, 
     `previous_levelup_date` string, 
     `previous_levelup_ts` timestamp, 
     `current_level` smallint, 
     `levelup_date` string, 
     `levelup_ts` timestamp, 
     `browser` string, 
     `browser_version` string, 
     `os` string, 
     `os_version` string, 
     `device` string, 
     `coin_wallet` bigint, 
     `coins_in` bigint, 
     `country_code` string, 
     `ip` string, 
     `language` string, 
     `rc_wallet` bigint, 
     `rc_in` bigint, 
     `locale` string, 
     `ab_experiment` string, 
     `ab_variant` string
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_levelup'
TBLPROPERTIES('serialization.null.format'='');



CREATE TABLE IF NOT EXISTS copy_fact_dau_snapshot
(
    `id` string, 
    `user_key` string, 
    `date` string, 
    `app_id` string, 
    `app_version` string, 
    `level_start` int, 
    `level_end` int, 
    `os` string, 
    `os_version` string, 
    `device` string, 
    `browser` string, 
    `browser_version` string, 
    `country_code` string, 
    `country` string, 
    `language` string, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `is_new_user` smallint, 
    `rc_in` int, 
    `coins_in` bigint, 
    `rc_out` int, 
    `coins_out` bigint, 
    `rc_wallet` int, 
    `coin_wallet` bigint, 
    `is_payer` smallint, 
    `is_converted_today` smallint, 
    `revenue_usd` double, 
    `payment_cnt` int, 
    `session_cnt` int, 
    `playtime_sec` int
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/fact_dau_snapshot'
TBLPROPERTIES('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS copy_dim_user
(
    `id` string, 
    `user_key` string, 
    `app_id` string, 
    `app_user_id` string, 
    `facebook_id` string, 
    `install_ts` timestamp, 
    `install_date` string, 
    `install_source` string, 
    `install_subpublisher` string, 
    `install_campaign` string, 
    `install_language` string, 
    `install_locale` string, 
    `install_country_code` string, 
    `install_country` string, 
    `install_os` string, 
    `install_device` string, 
    `install_device_alias` string, 
    `install_browser` string, 
    `install_gender` string, 
    `install_age` string, 
    `language` string, 
    `locale` string, 
    `birthday` string, 
    `gender` string, 
    `country_code` string, 
    `country` string, 
    `os` string, 
    `os_version` string, 
    `device` string, 
    `device_alias` string, 
    `browser` string, 
    `browser_version` string, 
    `app_version` string, 
    `level` int, 
    `levelup_ts` timestamp, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `is_payer` int, 
    `conversion_ts` timestamp, 
    `total_revenue_usd` decimal(12,4), 
    `payment_cnt` int, 
    `last_login_ts` timestamp, 
    `coin_wallet` double, 
    `coins_in` bigint, 
    `coins_out` bigint, 
    `rc_wallet` double, 
    `rc_in` bigint, 
    `rc_out` bigint, 
    `email` string, 
    `last_ip` string
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/dim_user'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_agg_kpi(
    `id` string, 
    `date` string, 
    `app_id` string, 
    `app_version` string, 
    `install_year` string, 
    `install_month` string, 
    `install_week` string, 
    `install_source` string, 
    `level_end` int, 
    `browser` string, 
    `browser_version` string, 
    `device_alias` string, 
    `country` string, 
    `os` string, 
    `os_version` string, 
    `language` string, 
    `is_new_user` int, 
    `is_payer` int, 
    `new_user_cnt` int, 
    `dau_cnt` int, 
    `newpayer_cnt` int, 
    `payer_today_cnt` int, 
    `payment_cnt` int, 
    `revenue_usd` double, 
    `session_cnt` int, 
    `session_length_sec` int
)
PARTITIONED BY ( 
  app string, 
  dt string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/copy/agg_kpi'
TBLPROPERTIES ('serialization.null.format'='');


CREATE TABLE IF NOT EXISTS copy_agg_quest
(
  install_date  STRING,
  app_id STRING,
  os STRING,
  os_version STRING,
  country  STRING,  
  install_source  STRING,
  browser STRING,
  browser_version STRING,
  language  STRING,
  quest_id STRING,
  quest_type STRING,
  task_id STRING,
  action STRING,
  user_cnt BIGINT,
  rc_out BIGINT     
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_quest'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_agg_tutorial
(
  install_date  STRING,
  app_id STRING,
  os STRING,
  os_version STRING,
  country  STRING,  
  install_source  STRING,
  browser STRING,
  browser_version STRING,
  language  STRING,
  step INT,
  user_cnt BIGINT
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/agg_tutorial'
TBLPROPERTIES('serialization.null.format'='');

CREATE TABLE IF NOT EXISTS copy_personal_info_daily (
    `id` string, 
    `app_id` string, 
    `ts` timestamp, 
    `date` string, 
    `browser` string, 
    `browser_version` string, 
    `country_code` string, 
    `event` string, 
    `install_source` string, 
    `install_ts` timestamp, 
    `install_date` string, 
    `ip` string, 
    `lang` string, 
    `level` int, 
    `os` string, 
    `os_version` string, 
    `snsid` string, 
    `uid` int, 
    `fb_source` string, 
    `additional_email` string
)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/copy/personal_info_daily'
TBLPROPERTIES('serialization.null.format'='');

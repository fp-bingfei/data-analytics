use farm_1_1;

CREATE  TABLE tmp_ledger_daily(
  date_start STRING,
  app_name string,
  user_key string,
  uid string,
  coins_bal double,
  coins_in bigint,
  coins_out bigint,
  rc_bal bigint,
  rc_in bigint,
  rc_out bigint  
)  
PARTITIONED BY (
app   STRING,
dt  STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/tmp_ledger_daily'
TBLPROPERTIES('serialization.null.format'='');
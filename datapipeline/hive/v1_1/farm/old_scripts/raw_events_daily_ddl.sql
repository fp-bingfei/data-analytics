USE farm_1_1;

CREATE EXTERNAL TABLE `raw_session_start_daily`(
  `key` string, 
  `ts` string, 
  `ts_pretty` timestamp, 
  `browser` string, 
  `browser_version` string, 
  `country_code` string, 
  `event` string, 
  `gender` string, 
  `install_source` string, 
  `install_ts` string, 
  `install_ts_pretty` timestamp, 
  `ip` string, 
  `lang` string, 
  `level` bigint, 
  `os` string, 
  `os_version` string, 
  `snsid` string, 
  `uid` string, 
  `device` string,
  `fb_source` string)
PARTITIONED BY ( 
  `app` string, 
  `dt` string)
  ROW FORMAT DELIMITED
  FIELDS TERMINATED BY '\t'
  STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_session_start_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);


CREATE EXTERNAL TABLE `raw_payment_daily`(
  `key` string, 
  `ts` string, 
  `ts_pretty` timestamp, 
  `amount` double, 
  `browser` string, 
  `browser_version` string, 
  `coins_bal` double, 
  `coins_in` bigint, 
  `country_code` string, 
  `currency` string, 
  `event` string, 
  `install_source` string, 
  `install_ts` string, 
  `install_ts_pretty` timestamp, 
  `ip` string, 
  `is_gift` bigint, 
  `lang` string, 
  `level` bigint, 
  `os` string, 
  `os_version` string, 
  `payment_processor` string, 
  `product_id` string, 
  `product_name` string, 
  `product_type` string, 
  `raw` struct<amount:double,cash_type:string,currency:string,from_uid:string,gameamount:string,gift:string,gift_id:string,level:bigint,paid_time:string,status:bigint,type:string,uid:string>, 
  `rc_bal` bigint, 
  `rc_in` bigint, 
  `snsid` string, 
  `transaction_id` string, 
  `uid` string,
  `fb_source` string)
  PARTITIONED BY ( 
    `app` string, 
    `dt` string)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_payment_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);


CREATE EXTERNAL TABLE `raw_newuser_daily`(
  `key` string, 
  `ts` string, 
  `ts_pretty` timestamp, 
  `browser` string, 
  `browser_version` string, 
  `country_code` string, 
  `event` string, 
  `gender` string, 
  `install_source` string, 
  `install_ts` string, 
  `install_ts_pretty` timestamp, 
  `ip` string, 
  `lang` string, 
  `level` bigint, 
  `os` string, 
  `os_version` string, 
  `snsid` string, 
  `uid` string, 
  `device` string,
  `fb_source` string)
  PARTITIONED BY ( 
    `app` string, 
    `dt` string)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_newuser_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE `raw_levelup_daily`(
  `key` string, 
  `ts` string, 
  `ts_pretty` timestamp, 
  `browser` string, 
  `browser_version` string, 
  `coins_bal` string, 
  `coins_get` bigint, 
  `country_code` string, 
  `event` string, 
  `install_source` string, 
  `install_ts` string, 
  `install_ts_pretty` timestamp, 
  `ip` string, 
  `lang` string, 
  `level` bigint, 
  `os` string, 
  `os_version` string, 
  `rc_bal` bigint, 
  `rc_get` string, 
  `snsid` string, 
  `uid` string)
  PARTITIONED BY ( 
    `app` string, 
    `dt` string)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_levelup_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);


CREATE EXTERNAL TABLE `raw_tutorial_daily`(                                                                                                                                                                         `key` string,                                                                                                                                                                   `ts` string,                                                                                                                                                                    `ts_pretty` timestamp,                                                                                                                                                          `browser` string,                                                                                                                            
  `browser_version` string, 
  `country_code` string, 
  `event` string, 
  `install_source` string, 
  `install_ts` string, 
  `install_ts_pretty` timestamp, 
  `ip` string, 
  `lang` string, 
  `level` bigint, 
  `os` string, 
  `os_version` string, 
  `snsid` string, 
  `step` bigint, 
  `uid` string)
  PARTITIONED BY ( 
    `app` string, 
    `dt` string)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_tutorial_daily'
TBLPROPERTIES (
    'serialization.null.format'=''
  );


  CREATE EXTERNAL TABLE `raw_quest_daily`(
    `key` string, 
    `ts` string, 
    `ts_pretty` timestamp, 
    `action` string, 
    `browser` string, 
    `browser_version` string, 
    `country_code` string, 
    `event` string, 
    `install_source` string, 
    `install_ts` string, 
    `install_ts_pretty` timestamp, 
    `ip` string, 
    `lang` string, 
    `level` bigint, 
    `os` string, 
    `os_version` string, 
    `quest_id` string, 
    `quest_type` string, 
    `snsid` string, 
    `task_id` string, 
    `uid` string, 
    `rc_in` string, 
    `rc_out` string, 
    `rc_bal` string, 
    `coins_in` double, 
    `coins_out` double, 
    `coins_bal` double)
    PARTITIONED BY ( 
      `app` string, 
      `dt` string)
      ROW FORMAT DELIMITED
      FIELDS TERMINATED BY '\t'
      STORED AS PARQUET
  LOCATION
    's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_quest_daily'
  TBLPROPERTIES (
      'serialization.null.format'=''
    );

CREATE EXTERNAL TABLE `raw_personal_info_daily`(
  `key` string, 
  `ts` string, 
  `ts_pretty` timestamp, 
  `browser` string, 
  `browser_version` string, 
  `country_code` string, 
  `event` string, 
  `install_source` string, 
  `install_ts` string, 
  `install_ts_pretty` timestamp, 
  `ip` string, 
  `lang` string, 
  `level` bigint, 
  `os` string, 
  `os_version` string, 
  `snsid` string, 
  `uid` string, 
  `fb_source` string, 
  `additional_email` string)
  PARTITIONED BY ( 
    `app` string, 
    `dt` string)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS PARQUET
LOCATION
  's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_personal_info_daily'
TBLPROPERTIES (
  'serialization.null.format'=''
);

exit;
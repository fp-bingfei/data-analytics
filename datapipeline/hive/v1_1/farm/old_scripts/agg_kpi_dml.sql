use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.exec.max.dynamic.partitions.pernode=200;

set hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;


INSERT OVERWRITE TABLE agg_kpi PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
select MD5(concat(COALESCE(d.date,''),COALESCE(d.app_id,''),COALESCE(d.app_version,''),COALESCE(year(u.install_date),''),COALESCE(month(u.install_date),''),COALESCE(weekofyear(u.install_date),''),COALESCE(u.install_source,''),COALESCE(d.level_end,0),COALESCE(d.browser,''),COALESCE(d.browser_version,''),COALESCE(u.device_alias,''),COALESCE(d.country,''),COALESCE(d.os,''),COALESCE(d.os_version,''),COALESCE(d.language,''),COALESCE(d.is_new_user,''),COALESCE(d.is_payer,'0')))
,d.date
,d.app_id
,d.app_version
,year(u.install_date)  install_year
,month(u.install_date) install_month
,weekofyear(u.install_date) install_week
,u.install_source
,d.level_end
,d.browser
,d.browser_version
,u.device_alias
,d.country
,d.os
,d.os_version
,d.language
,d.is_new_user
,d.is_payer
,sum(d.is_new_user) as new_user_cnt
,count(d.user_key) as dau_cnt
,sum(d.is_converted_today) as newpayer_cnt
,sum(case when d.revenue_usd > 0 then 1 else 0 end) as payer_today_cnt
,sum(d.payment_cnt) as payment_cnt
,sum(d.revenue_usd) as revenue_usd
,sum(d.session_cnt) as session_cnt
,0           
from (select * from fact_dau_snapshot where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}') d
Left outer join (select * from dim_user where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}') u
on d.user_key = u.user_key and d.app=u.app
group by d.date,d.app_id,d.app_version,u.install_source,year(u.install_date),month(u.install_date),weekofyear(u.install_date),d.os,d.os_version,d.browser,d.browser_version,u.device_alias,d.country,d.country_code,d.language,d.is_new_user,d.is_payer,d.level_end;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_agg_kpi partition(app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}') SELECT
id, date, app_id, app_version, install_year, install_month, install_week, install_source, level_end, browser, browser_version, device_alias, country, os, os_version, language, is_new_user, is_payer, new_user_cnt, dau_cnt, newpayer_cnt, payer_today_cnt, payment_cnt, revenue_usd, session_cnt, session_length_sec 
from agg_kpi where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';
 	 
exit;



-----------------------------------
-- Dump tbl_payment to Redshift
-----------------------------------

DROP TABLE if exists tbl_payment;
CREATE TABLE tbl_payment
(
    app_id varchar(20)
   ,id  varchar(200)
   ,uid varchar(200)
   ,from_uid varchar(200)   
   ,type varchar(200)   
   ,currency varchar(200)   
   ,amount  varchar(200)
   ,gameamount  varchar(200)
   ,status  varchar(200)
   ,paid_time   timestamp
   ,cash_type varchar(200)
   ,level varchar(200)
   ,gift varchar(200)
   ,gift_id varchar(200)
);

DROP TABLE if exists tbl_payment_staging;
CREATE TABLE tbl_payment_staging
(
    id  varchar(200)
   ,uid varchar(200)
   ,from_uid varchar(200)   
   ,type varchar(200)   
   ,currency varchar(200)   
   ,amount  varchar(200)
   ,gameamount  varchar(200)
   ,status  varchar(200)
   ,paid_time   varchar(200)
   ,cash_type varchar(200)
   ,level varchar(200)
   ,gift varchar(200)
   ,gift_id varchar(200)
   ,column1 varchar(200)
   ,column2 varchar(200)
   ,column3 varchar(200)
   ,column4 varchar(200)
   ,column5 varchar(200)
);

--us
copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/us/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.us.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;
   

--br
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/br/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.br.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


--de
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/de/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.de.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


--fr
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/fr/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.fr.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;

--it
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/it/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.it.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


--nl
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/nl/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.nl.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


--pl
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/pl/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.pl.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


--th
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/th/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.th.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


--tw
truncate table tbl_payment_staging;

copy tbl_payment_staging 
from 's3://com.funplusgame.bidata/etl/farm/tw/mysql/tbl_payments/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_payment
select 'farm.tw.prod'
       ,id  
       ,uid 
       ,from_uid 
       ,type 
       ,currency 
       ,amount  
       ,gameamount  
       ,status  
       ,(TIMESTAMP 'epoch' + paid_time::BIGINT *INTERVAL '1 Second')
       ,cash_type 
       ,level 
       ,gift 
       ,gift_id 
from    tbl_payment_staging;


commit;

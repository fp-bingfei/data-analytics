use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions.pernode=100000;
SET hive.exec.max.dynamic.partitions=100000;
SET hive.stats.autogather=false;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat;

alter table raw_sendgrid_daily add if not exists partition (app='farm.br.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.br.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.de.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.de.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.fr.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.fr.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.it.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.it.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.nl.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.nl.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.pl.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.pl.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.th.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.th.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.tw.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.tw.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';
alter table raw_sendgrid_daily add if not exists partition (app='farm.us.prod', dt='${hiveconf:rpt_date}') location 's3://com.funplus.bithirdparty/sendgrid/farm.us.prod/${hiveconf:rpt_year}/${hiveconf:rpt_month}/${hiveconf:rpt_day}';

set rpt_date_days_back=${hiveconf:rpt_date_d3};


INSERT OVERWRITE TABLE fact_sendgrid PARTITION(app, campaign_dt) 
SELECT sg.app_id
, sg.campaign
, sg.campaign_date
, sg.delivered_per_campaign_date
, sg.category
, du.app_user_id
, sg.snsid
, sg.email
, du.language
, du.os
, du.browser
, du.country
, sg.delivered_ts
, sg.bounce_ts
, sg.open_ts
, sg.open_daydiff
, sg.click_ts
, sg.click_daydiff
, sum(case when dau.date < to_date(sg.delivered_ts) then dau.session_cnt else 0 end) precamp_session_cnt
, sum(case when dau.date < to_date(sg.delivered_ts) then dau.payment_cnt else 0 end) precamp_purchase_cnt
, sum(case when dau.date < to_date(sg.delivered_ts) then dau.revenue_usd else 0 end) precamp_revenue_usd
, max(case when dau.date < to_date(sg.delivered_ts) then dau.level_end else 1 end) AS precamp_level_end
, max(dau.level_end) AS level_end
, sum(dau.session_cnt) total_session_cnt, du.payment_cnt AS total_purchase_cnt, du.total_revenue_usd
, sg.app_id as app
, sg.campaign_date as campaign_dt
FROM
(select app_id, campaign
, campaign_date
, count(*) over (partition by campaign, campaign_date) delivered_per_campaign_date
, snsid
, email
, category
, uid
, delivered_ts
, bounce_ts
, open_ts
, case when open_ts is not null then datediff(open_ts, delivered_ts) else null  end open_daydiff
, click_ts
, case when click_ts is not null then datediff(click_ts,  delivered_ts) else null  end click_daydiff
FROM 
( select app_id, campaign
, snsid
, email
, category
, uid
, min(case when event = 'delivered' then to_date(from_unixtime(bigint(time))) else null end) AS campaign_date
, min(case when event = 'delivered' then from_unixtime(bigint(time)) else null end) AS delivered_ts
, min(case when event = 'bounce' then from_unixtime(bigint(time)) else null end) AS bounce_ts
, min(case when event = 'open' then from_unixtime(bigint(time)) else null end) AS open_ts
, min(case when event = 'click' then from_unixtime(bigint(time)) else null end) AS click_ts
from raw_sendgrid_daily
where 
app in ('farm.br.prod', 'farm.de.prod', 'farm.fr.prod', 'farm.it.prod', 'farm.nl.prod', 'farm.pl.prod'
, 'farm.th.prod', 'farm.tw.prod', 'farm.us.prod') AND day = '${hiveconf:rpt_date}'
group by app_id, campaign
, snsid
, email
, category
, uid
) rsd WHERE campaign_date IS NOT NULL) sg
INNER JOIN
(select app, user_key, app_user_id, language, os, browser, country, payment_cnt, total_revenue_usd FROM dim_user where dt = '${hiveconf:rpt_date}') 
du 
ON (sg.app_id = du.app and sg.uid = du.app_user_id)
INNER JOIN
(select app, date, user_key, session_cnt, payment_cnt, revenue_usd, level_end 
from fact_dau_snapshot fds where 
app in ('farm.br.prod', 'farm.de.prod', 'farm.fr.prod', 'farm.it.prod', 'farm.nl.prod', 'farm.pl.prod'
, 'farm.th.prod', 'farm.tw.prod', 'farm.us.prod') AND dt <= '${hiveconf:rpt_date}'
)
dau
ON (du.app = dau.app and du.user_key = dau.user_key)
GROUP BY
sg.app_id
, sg.campaign
, sg.campaign_date
, sg.delivered_per_campaign_date
, sg.category
, du.app_user_id
, sg.snsid
, sg.email
, du.language
, du.os
, du.browser
, du.country
, sg.delivered_ts
, sg.bounce_ts
, sg.open_ts
, sg.open_daydiff
, sg.click_ts
, sg.click_daydiff
, du.payment_cnt
, du.total_revenue_usd;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
  
ALTER TABLE copy_fact_sendgrid DROP IF EXISTS PARTITION (app='farm.all.prod');

INSERT OVERWRITE TABLE copy_fact_sendgrid PARTITION (app='farm.all.prod',dt='${hiveconf:rpt_date}')
SELECT app_id
, campaign_name
, campaign_date
, delivered_per_campaign_date
, category
, app_user_id
, snsid
, email
, language
, os
, browser
, country
, delivered_ts
, bounce_ts
, open_ts
, open_daydiff
, click_ts
, click_daydiff
, precamp_session_cnt
, precamp_purchase_cnt
, precamp_revenue_usd
, precamp_level_end
, level_end
, total_session_cnt
, total_purchase_cnt
, total_revenue_usd
FROM 
fact_sendgrid WHERE 
app in ('farm.br.prod', 'farm.de.prod', 'farm.fr.prod', 'farm.it.prod', 'farm.nl.prod', 'farm.pl.prod'
, 'farm.th.prod', 'farm.tw.prod', 'farm.us.prod') AND campaign_dt = '${hiveconf:rpt_date}';

exit;



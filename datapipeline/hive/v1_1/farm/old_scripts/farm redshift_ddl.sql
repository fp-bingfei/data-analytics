


CREATE table processed.fact_session(
  id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
  app_id varchar(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE BYTEDICT,
  user_key varchar(64) NOT NULL ENCODE LZO,
  app_user_id varchar(64) NOT NULL ENCODE LZO,
  date_start date NOT NULL ENCODE DELTA,
  date_end date ENCODE DELTA,
  ts_start timestamp ENCODE DELTA,
  ts_end timestamp ENCODE DELTA,
  install_ts timestamp ENCODE DELTA,
  install_date date ENCODE DELTA,
  session_id varchar(50) ENCODE LZO,
  device_key varchar(64) ENCODE LZO,
  facebook_id varchar(50) ENCODE LZO,
  install_source varchar(1024) ENCODE BYTEDICT,
  os varchar(30) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  browser varchar(32) ENCODE BYTEDICT,
  browser_version varchar(64) ENCODE BYTEDICT,
  device varchar(64) ENCODE BYTEDICT,
  country_code varchar(16) ENCODE BYTEDICT,
  level_start int,
  level_end int,
  gender varchar(16) ENCODE BYTEDICT,
  birthday date,
  email varchar(256) ENCODE BYTEDICT,
  ip varchar(64) ENCODE BYTEDICT,
  language varchar(8) ENCODE BYTEDICT,
  locale varchar(8) ENCODE BYTEDICT,
  rc_wallet_start int,
  rc_wallet_end int,
  coin_wallet_start bigint,
  coin_wallet_end bigint,
  ab_experiment varchar(1024) ENCODE BYTEDICT,
  ab_variant varchar(1024) ENCODE BYTEDICT,
  session_length_sec int)
  DISTKEY(user_key)
  SORTKEY(date_start, user_key, app_id, app_user_id);
  





CREATE TABLE processed.fact_revenue(
  id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
  app_id varchar(64) NOT NULL ENCODE BYTEDICT,
  app_version varchar(20) ENCODE BYTEDICT,
  user_key varchar(64) NOT NULL ENCODE LZO,
  app_user_id varchar(64) ENCODE LZO,
  date date NOT NULL ENCODE DELTA, 
  ts timestamp ENCODE DELTA,
  install_ts timestamp ENCODE DELTA,
  install_date date ENCODE DELTA,
  device_key varchar(64) ENCODE LZO,
  session_id varchar(50) ENCODE LZO,
  level int,
  os varchar(30) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device varchar(64) ENCODE BYTEDICT,
  browser varchar(32) ENCODE BYTEDICT,
  browser_version varchar(64) ENCODE BYTEDICT,
  country_code varchar(16) ENCODE BYTEDICT,
  install_source varchar(1024) ENCODE BYTEDICT,
  ip varchar(32) ENCODE BYTEDICT,
  language varchar(8) ENCODE BYTEDICT,
  locale varchar(8) ENCODE BYTEDICT,
  ab_experiment varchar(1024) ENCODE BYTEDICT,
  ab_variant varchar(1024) ENCODE BYTEDICT,
  coin_wallet bigint,
  rc_wallet int,
  payment_processor varchar(64) ENCODE BYTEDICT,
  product_id varchar(64) ENCODE BYTEDICT,
  product_name varchar(64) ENCODE BYTEDICT,
  product_type varchar(64) ENCODE BYTEDICT,
  coins_in bigint,
  rc_in int,
  currency varchar(20) ENCODE BYTEDICT,
  revenue_amount numeric(14,4),
  revenue_usd numeric(14,4),
  transaction_id varchar(256) ENCODE BYTEDICT
  )DISTKEY(user_key)
  SORTKEY(date, user_key, app_id, app_user_id)
;
 
CREATE TABLE processed.fact_levelup(
  id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
  user_key varchar(64) NOT NULL ENCODE LZO,
  app_id varchar(64) NOT NULL ENCODE BYTEDICT,
  app_user_id varchar(64) ENCODE LZO,
  session_id varchar(50) ENCODE LZO,
  previous_level smallint,
  previous_levelup_date date ENCODE DELTA,
  previous_levelup_ts timestamp ENCODE DELTA,
  current_level smallint,
  levelup_date date ENCODE DELTA,
  levelup_ts timestamp ENCODE DELTA,
  browser varchar(32) ENCODE BYTEDICT,
  browser_version varchar(64) ENCODE BYTEDICT,
  os varchar(32) ENCODE BYTEDICT,
  os_version varchar(100) ENCODE BYTEDICT,
  device_key varchar(64) ENCODE BYTEDICT,
  device varchar(64) ENCODE BYTEDICT,
  coin_wallet bigint,
  coins_in bigint,
  country_code varchar(16) ENCODE BYTEDICT,
  ip varchar(32) ENCODE BYTEDICT,
  language varchar(8) ENCODE BYTEDICT,
  rc_wallet bigint,
  rc_in bigint,
  locale varchar(8) ENCODE BYTEDICT,
  ab_experiment varchar(1024) ENCODE BYTEDICT,
  ab_variant varchar(1024) ENCODE BYTEDICT
  )
  distkey(user_key)
sortkey(user_key,current_level,levelup_date);




CREATE TABLE processed.fact_dau_snapshot (
   id varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   user_key varchar(64) NOT NULL ENCODE LZO,
   date date NOT NULL ENCODE DELTA,
   device_key varchar(64) ENCODE BYTEDICT,
   app_id varchar(64) ENCODE BYTEDICT,
   app_version varchar(20) ENCODE BYTEDICT,
   level_start  int,
   level_end  int,
   os varchar(32) ENCODE BYTEDICT,
   os_version varchar(100) ENCODE BYTEDICT,
   device varchar(64) ENCODE BYTEDICT,
   browser varchar(32) ENCODE BYTEDICT,
   browser_version varchar(64) ENCODE BYTEDICT,
   country_code varchar(16) ENCODE BYTEDICT,
   country varchar(100) ENCODE BYTEDICT,
   language varchar(8) ENCODE BYTEDICT,
   gender varchar(16) ENCODE BYTEDICT,
   ab_experiment varchar(1024) ENCODE BYTEDICT,
   ab_variant varchar(1024) ENCODE BYTEDICT,
   is_new_user  smallint,
   rc_in  int,
   coins_in  bigint,
   rc_out  int,
   coins_out  bigint,
   rc_wallet  int,
   coin_wallet  bigint,
   is_payer  smallint,
   is_converted_today  smallint,
   revenue_usd  numeric(14,4),
   payment_cnt  int,
   session_cnt  int,
   playtime_sec  int)
   DISTKEY(user_key)
   SORTKEY(date, user_key, app_id, country_code);



CREATE  TABLE  processed.dim_user (
   id   varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   user_key   varchar(64) NOT NULL ENCODE LZO,
   app_id   varchar(64) NOT NULL ENCODE BYTEDICT,
   app_user_id   varchar(64) ENCODE LZO,
   device_key   varchar(64) ENCODE BYTEDICT,
   facebook_id   varchar(50) ENCODE BYTEDICT,
   install_ts  timestamp ENCODE DELTA,
   install_date   date ENCODE DELTA,
   install_source   varchar(1024) ENCODE BYTEDICT,
   install_subpublisher   varchar(500) ENCODE BYTEDICT,
   install_campaign   varchar(128) ENCODE BYTEDICT,
   install_language   varchar(8) ENCODE BYTEDICT,
   install_locale   varchar(8) ENCODE BYTEDICT,
   install_country_code   varchar(16) ENCODE BYTEDICT,
   install_country   varchar(100) ENCODE BYTEDICT,
   install_os   varchar(32) ENCODE BYTEDICT,
   install_device   varchar(64) ENCODE BYTEDICT,
   install_device_alias   varchar(64) ENCODE BYTEDICT,
   install_browser   varchar(32) ENCODE BYTEDICT,
   install_gender   varchar(16) ENCODE BYTEDICT,
   install_age   varchar(20) ENCODE BYTEDICT,
   language   varchar(8) ENCODE BYTEDICT,
   locale   varchar(8) ENCODE BYTEDICT,
   birthday   date ENCODE DELTA,
   gender   varchar(16) ENCODE BYTEDICT,
   country_code   varchar(16) ENCODE BYTEDICT,
   country   varchar(100) ENCODE BYTEDICT,
   os   varchar(32) ENCODE BYTEDICT,
   os_version   varchar(100) ENCODE BYTEDICT,
   device   varchar(64) ENCODE BYTEDICT,
   device_alias   varchar(64) ENCODE BYTEDICT,
   browser   varchar(32) ENCODE BYTEDICT,
   browser_version   varchar(64) ENCODE BYTEDICT,
   app_version   varchar(20) ENCODE BYTEDICT,
   level  int,
   levelup_ts  timestamp ENCODE DELTA,
   ab_experiment   varchar(1024) ENCODE BYTEDICT,
   ab_variant   varchar(1024) ENCODE BYTEDICT,
   is_payer  int,
   conversion_ts  timestamp ENCODE DELTA,
   total_revenue_usd  numeric(14,4),
   payment_cnt  int,
   last_login_ts  timestamp,
   coin_wallet  numeric(14,2),
   coins_in  bigint,
   coins_out  bigint,
   rc_wallet  numeric(14,2),
   rc_in  bigint,
   rc_out  bigint,
   email   varchar(500) ENCODE BYTEDICT,
   last_ip   varchar(64) ENCODE BYTEDICT
)
   DISTKEY(user_key)
  SORTKEY(user_key, app_id, app_user_id);


CREATE TABLE  processed.agg_kpi (
   id   varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   date   date NOT NULL ENCODE DELTA,
   app_id   varchar(64) NOT NULL ENCODE BYTEDICT,
   app_version   varchar(20) ENCODE BYTEDICT,
   install_month   varchar(30) ENCODE BYTEDICT,
   install_week   varchar(30) ENCODE BYTEDICT,
   install_source   varchar(1024) ENCODE BYTEDICT,
   level_end  int,
   browser   varchar(32) ENCODE BYTEDICT,
   browser_version   varchar(64) ENCODE BYTEDICT,
   device_alias   varchar(64) ENCODE BYTEDICT,
   country   varchar(100) ENCODE BYTEDICT,
   os   varchar(32) ENCODE BYTEDICT,
   os_version   varchar(100) ENCODE BYTEDICT,
   language   varchar(8) ENCODE BYTEDICT,
   is_new_user  int,
   is_payer  int,
   new_user_cnt  int,
   dau_cnt  int,
   newpayer_cnt  int,
   payer_today_cnt  int,
   payment_cnt  int,
   revenue_usd  numeric(14,4),
   session_cnt  int,
   session_length_sec  int)
DISTKEY(date)
SORTKEY(date, app_id, country, os);



CREATE TABLE  processed.agg_item (
   id   varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   date   date NOT NULL ENCODE DELTA,
   app_id   varchar(64) NOT NULL ENCODE BYTEDICT,
   level  int,
   os   varchar(64) ENCODE BYTEDICT,
   device   varchar(64) ENCODE BYTEDICT,
   browser   varchar(64) ENCODE BYTEDICT,
   country   varchar(100) ENCODE BYTEDICT,
   language   varchar(8) ENCODE BYTEDICT,
   transaction_type   varchar(128) ENCODE BYTEDICT,
   received_id   varchar(64) ENCODE BYTEDICT,
   received_name   varchar(64) ENCODE BYTEDICT,
   received_type   varchar(64) ENCODE BYTEDICT,
   received_amount  bigint,
   spent_id   varchar(64) ENCODE BYTEDICT,
   spent_name   varchar(64) ENCODE BYTEDICT,
   spent_type   varchar(64) ENCODE BYTEDICT,
   spent_amount  bigint,
   purchaser_cnt  int,
   user_cnt  int,
   item_class   varchar(16) ENCODE BYTEDICT)
DISTKEY(date)
SORTKEY(date, app_id, country, os);
;


CREATE TABLE  processed.agg_daily_ledger (
   id   varchar(128) PRIMARY KEY NOT NULL ENCODE BYTEDICT,
   date   date not null ENCODE DELTA,
   app_id   varchar(64) NOT NULL ENCODE BYTEDICT,
   level  int,
   os   varchar(64) ENCODE BYTEDICT,
   device   varchar(64) ENCODE BYTEDICT,
   browser   varchar(64) ENCODE BYTEDICT,
   country   varchar(100) ENCODE BYTEDICT,
   language   varchar(8) ENCODE BYTEDICT,
   transaction_type   varchar(128) ENCODE BYTEDICT,
   received_id   varchar(64) ENCODE BYTEDICT,
   received_name   varchar(64) ENCODE BYTEDICT,
   received_type   varchar(64) ENCODE BYTEDICT,
   received_amount  bigint,
   spent_id   varchar(64) ENCODE BYTEDICT,
   spent_name   varchar(64) ENCODE BYTEDICT,
   spent_type   varchar(64) ENCODE BYTEDICT,
   spent_amount  bigint,
   purchaser_cnt  int,
   user_cnt  int)
DISTKEY(date)
SORTKEY(date, app_id, country, os);
;


create table processed.cheating_list
(
       app VARCHAR(64) ENCODE BYTEDICT,
       date  DATE ENCODE DELTA,
       snsid VARCHAR(64) NOT NULL ENCODE LZO,
       rc bigint
);


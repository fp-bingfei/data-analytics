use farm_1_1;

CREATE EXTERNAL TABLE IF NOT EXISTS fact_levelup 
(
id  STRING COMMENT 'id is md5(concat(app_name, app_user_id,current_level,levelup_ts)',
user_key  STRING,
app_id  STRING COMMENT 'app_id is the same as app which is being used as partition key',
app_user_id  STRING,
session_id STRING,
previous_level  SMALLINT,
previous_levelup_date  STRING,
previous_levelup_ts  TIMESTAMP,
current_level  SMALLINT,
levelup_date STRING,
levelup_ts TIMESTAMP,
browser STRING,
browser_version STRING,
os STRING,
os_version STRING,
device_key STRING,
device STRING,
coin_wallet BIGINT,
coins_in BIGINT,
country_code STRING,
ip STRING,
language STRING,
rc_wallet BIGINT,
rc_in BIGINT,
locale STRING,
ab_experiment STRING,
ab_variant STRING
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_levelup'
TBLPROPERTIES('serialization.null.format'='');

-------------create a temp table from fact_session to get all the dimensions along with session_cnt and last_login_ts ---------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE tmp_user_daily_login PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT date, user_key,device_key,app_id,uid,install_ts, install_date,birthday, app_version, level_start, level_end, os, os_version,
t.country_code,c.country,last_ip,install_source, language,locale, gender,device, browser, browser_version, ab_experiment, ab_variant,count(1) session_cnt, max(ts_start) last_login_ts
FROM
(
SELECT
date_start date,
app_id,
user_key,
device_key,
uid,
ts_start,
birthday,
first_value(install_ts,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as install_ts,
first_value(install_date,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as install_date,
first_value(app_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as app_version,
first_value(level_start,true) OVER (PARTITION BY date_start, user_key order by ts_start asc
     rows between unbounded preceding AND unbounded following) as level_start,
first_value(level_end,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as level_end,
first_value(os,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as os,
first_value(os_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as os_version,
first_value(s.country_code,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as country_code,
first_value(s.ip,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as last_ip,
first_value(s.install_source,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as install_source,
first_value(language,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as language,
  first_value(locale,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as locale,
first_value(gender,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as gender,
first_value(device,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as device,
first_value(browser,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
 rows between unbounded preceding AND unbounded following) as browser,
first_value(browser_version,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as browser_version,
first_value(ab_experiment,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
     rows between unbounded preceding AND unbounded following) as ab_experiment,
first_value(ab_variant,true) OVER (PARTITION BY date_start, user_key order by ts_start desc
    rows between unbounded preceding AND unbounded following) as ab_variant
FROM (select * from fact_session where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}')s
UNION ALL
SELECT
date,
app_id,
r.user_key,
device_key,
uid,
ts ts_start,
null birthday,
first_value(install_ts,true) OVER (PARTITION BY date, r.user_key order by ts desc
    rows between unbounded preceding AND unbounded following) as install_ts,
first_value(install_date,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as install_date,
first_value(app_version,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as app_version,
first_value(level,true) OVER (PARTITION BY date, r.user_key order by ts asc
     rows between unbounded preceding AND unbounded following) as level_start,
first_value(level,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as level_end,
first_value(os,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as os,
first_value(os_version,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as os_version,
first_value(r.country_code,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as country_code,
first_value(r.ip,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as last_ip,
first_value(install_source,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as install_source,
first_value(language,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as language,
first_value(locale,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as locale,
null gender,
first_value(device,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as device,
first_value(browser,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as browser,
first_value(browser_version,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as browser_version,
first_value(ab_experiment,true) OVER (PARTITION BY date, r.user_key order by ts desc
     rows between unbounded preceding AND unbounded following) as ab_experiment,
first_value(ab_variant,true) OVER (PARTITION BY date, r.user_key order by ts desc
    rows between unbounded preceding AND unbounded following) as ab_variant
FROM (select * FROM fact_revenue where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}') r
where r.user_key not in (select user_key from fact_session where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}')
) t
left outer join dim_country c on t.country_code=c.country_code
group by date,app_id, user_key,device_key,install_ts, uid,install_date,app_version, level_start, level_end, os, os_version,
t.country_code, c.country,last_ip,install_source,language,locale,birthday, gender,device, browser, browser_version, ab_experiment, ab_variant;



exit;



---------create a temp table from fact_revenue which gives purchase_cnt, revenue_usd and conversion_ts and scans complete fact_revenue (first run)-------------------

SET hive.exec.dynamic.partition.mode=nonstrict;

SET mapred.max.split.size = 134217728;

SET hive.mapred.reduce.tasks.speculative.execution=false;


INSERT OVERWRITE TABLE tmp_user_payment_backfill PARTITION (app,dt)
select distinct t.user_key,t.app_id,t.date,t.conversion_ts,t.revenue,t.purchase_cnt,t.app,t.dt from
(select 
user_key,
app_id,
date,
min(ts) over (partition by user_key,app_id) conversion_ts,
sum(revenue_usd) over (partition by user_key,app_id,date) revenue,
count(ts) over (partition by user_key,app_id,date) purchase_cnt,
app,
dt
from fact_revenue)t 
;


-------- payment daily script ------

-------- payment daily script ------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app='${hiveconf:rpt_app}' , dt='${hiveconf:rpt_date}')
select 
p.user_key,
p.app_id,
p.date,
coalesce(d.conversion_ts,p.conversion_ts) conversion_ts,
revenue_usd,
purchase_cnt
from (select app_id, user_key, date, min(ts) conversion_ts, sum(revenue_usd) revenue_usd, count(ts) purchase_cnt,app from fact_revenue 
where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
group by app_id, user_key, date,app )p
left outer join (select * from dim_user where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date_yesterday}')d
on p.user_key=d.user_key and p.app=d.app
;



exit;

---------create a temp table from fact_levelup which gives levelup_ts, level_start and level_end from the fact_levelup table -------------


SET hive.exec.dynamic.partition.mode=nonstrict;

SET mapred.max.split.size = 134217728;

SET hive.mapred.reduce.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE tmp_user_level PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
select levelup_date,
user_key,
app_id,
max(levelup_ts) levelup_ts,
min(previous_level) level_start,
max(current_level) level_end
from fact_levelup where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}'
group by levelup_date,app_id,user_key
;
exit;
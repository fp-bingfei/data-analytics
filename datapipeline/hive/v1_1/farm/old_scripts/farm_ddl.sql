CREATE EXTERNAL TABLE `fact_session`(
  `id` string, 
  `app_id` string, 
  `app_version` string, 
  `user_key` string, 
  `app_user_id` string, 
  `date_start` string, 
  `date_end` string, 
  `ts_start` timestamp, 
  `ts_end` timestamp, 
  `install_ts` timestamp, 
  `install_date` string, 
  `session_id` string, 
  `facebook_id` string, 
  `install_source` string, 
  `os` string, 
  `os_version` string, 
  `browser` string, 
  `browser_version` string, 
  `device` string, 
  `country_code` string, 
  `level_start` int, 
  `level_end` int, 
  `gender` string, 
  `birthday` string, 
  `email` string, 
  `ip` string, 
  `language` string, 
  `locale` string, 
  `rc_wallet_start` int, 
  `rc_wallet_end` int, 
  `coin_wallet_start` bigint, 
  `coin_wallet_end` bigint, 
  `ab_experiment` string, 
  `ab_variant` string, 
  `session_length_sec` int, 
  `idfa` string, 
  `idfv` string, 
  `gaid` string, 
  `mac_address` string,
  `android_id`  string,
   `fb_source`  string)
PARTITIONED BY ( 
  `app` string, 
  `dt` string)
ROW FORMAT DELIMITED 
  FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_session'
TBLPROPERTIES('serialization.null.format'='');




CREATE EXTERNAL TABLE IF NOT EXISTS fact_revenue
(
    `id` string, 
    `app_id` string, 
    `app_version` string, 
    `user_key` string, 
    `app_user_id` string, 
    `date` string, 
    `ts` timestamp, 
    `install_ts` timestamp, 
    `install_date` string, 
    `session_id` string, 
    `level` int, 
    `os` string, 
    `os_version` string, 
    `device` string, 
    `browser` string, 
    `browser_version` string, 
    `country_code` string, 
    `install_source` string, 
    `ip` string, 
    `language` string, 
    `locale` string, 
    `ab_experiment` string, 
    `ab_variant` string, 
    `coin_wallet` bigint, 
    `rc_wallet` int, 
    `payment_processor` string, 
    `product_id` string, 
    `product_name` string, 
    `product_type` string, 
    `coins_in` bigint, 
    `rc_in` int, 
    `currency` string, 
    `revenue_amount` double, 
    `revenue_usd` double, 
    `transaction_id` string, 
    `idfa` string, 
    `idfv` string, 
    `gaid` string, 
    `mac_address` string,
    `android_id`  string,
    `fb_source`  string	  
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_revenue'
TBLPROPERTIES('serialization.null.format'='');



CREATE EXTERNAL TABLE IF NOT EXISTS fact_dau_snapshot
(
	`id` string, 
	  `user_key` string, 
	  `date` string, 
	  `app_id` string, 
	  `app_version` string, 
	  `level_start` int, 
	  `level_end` int, 
	  `os` string, 
	  `os_version` string, 
	  `device` string, 
	  `browser` string, 
	  `browser_version` string, 
	  `country_code` string, 
	  `country` string, 
	  `language` string, 
	  `ab_experiment` string, 
	  `ab_variant` string, 
	  `is_new_user` smallint, 
	  `rc_in` int, 
	  `coins_in` bigint, 
	  `rc_out` int, 
	  `coins_out` bigint, 
	  `rc_wallet` int, 
	  `coin_wallet` bigint, 
	  `is_payer` smallint, 
	  `is_converted_today` smallint, 
	  `revenue_usd` double, 
	  `payment_cnt` int, 
	  `session_cnt` int, 
	  `playtime_sec` int
)
PARTITIONED BY(
app STRING,
dt STRING
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/farm_1_1/processed/fact_dau_snapshot'
TBLPROPERTIES('serialization.null.format'='');

CREATE  TABLE tmp_user_daily_login(
  date_start string,
  user_key string,
  device_key string,
  app_id string,
  uid string,
  install_ts timestamp,
  install_date string,
  birthday string,
  app_version string,
  level_start int,
  level_end int,
  os string,
  os_version string,
  country_code string,
  country string,
  last_ip string,
  install_source string,
  language string,
  locale string,
  gender string,
  device string,
  browser string,
  browser_version string,
  ab_experiment string,
  ab_variant string,
  session_cnt int,
  last_login_ts timestamp,
  fb_source string)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_daily_login'
TBLPROPERTIES('serialization.null.format'='');




CREATE  TABLE tmp_user_level(
  date string,
  user_key string,
  app_id string,
  levelup_ts timestamp,
  level_start int,
  level_end int)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_level'
TBLPROPERTIES('serialization.null.format'='');



CREATE  TABLE tmp_user_payment(
  user_key string,
  app_id string,
  date string,
  conversion_ts timestamp,
  revenue_usd double,
  purchase_cnt int)
PARTITIONED BY (
  app string,
  dt string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS PARQUET
LOCATION 's3://com.funplus.datawarehouse/hive/warehouse/farm_1_1.db/tmp_user_payment'
TBLPROPERTIES('serialization.null.format'='');

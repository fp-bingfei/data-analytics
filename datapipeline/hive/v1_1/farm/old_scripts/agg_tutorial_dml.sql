----------------------------------
-- Query to generate agg_tutorial
----------------------------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions.pernode=100000;
SET hive.exec.max.dynamic.partitions=100000;

INSERT OVERWRITE TABLE agg_tutorial PARTITION(app='${hiveconf:rpt_app}',install_dt) 
select du.install_date, du.app as app_id, du.os, du.os_version, du.country, du.install_source, du.browser
, du.browser_version, du.language
, rt.step
, count(*) AS user_cnt
, du.install_date as install_dt
FROM 
(
    SELECT DISTINCT app, uid, step FROM raw_tutorial_daily WHERE app = '${hiveconf:rpt_app}' 
    AND to_date(install_ts_pretty) > '${hiveconf:rpt_date_days_back}'  AND dt > '${hiveconf:rpt_date_days_back}' AND dt <= '${hiveconf:rpt_date}'
) rt
INNER JOIN
(
    select app_user_id, install_date, app, os, os_version, country, install_source
    , browser, browser_version, language
    FROM dim_user WHERE app = '${hiveconf:rpt_app}' AND dt = '${hiveconf:rpt_date}' 
    AND install_date <= '${hiveconf:rpt_date}' AND install_date > '${hiveconf:rpt_date_days_back}'
) du 
ON (du.app = rt.app AND du.app_user_id = rt.uid)
GROUP BY install_date, du.app, du.os, du.os_version, du.country, du.install_source, du.browser
, du.browser_version, du.language
, rt.step
UNION ALL
SELECT
install_date,
app_id,
os,
os_version,
country,  
install_source,
browser,
browser_version,
language,
0 AS step,
count(*) AS user_cnt
, install_date as install_dt
FROM dim_user
WHERE 
app = '${hiveconf:rpt_app}' AND dt = '${hiveconf:rpt_date}' 
AND install_date <= '${hiveconf:rpt_date}' AND install_date > '${hiveconf:rpt_date_days_back}'
GROUP BY install_date, app_id, os, os_version, country, install_source
, browser, browser_version, language
distribute by install_date sort by install_date, app_id, os, os_version, country, install_source
, browser, browser_version, language;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

ALTER TABLE copy_agg_tutorial DROP IF EXISTS PARTITION (app='${hiveconf:rpt_app}');
  
INSERT OVERWRITE TABLE copy_agg_tutorial PARTITION (app='${hiveconf:rpt_app}',dt='${hiveconf:rpt_date}')
SELECT 
install_date,
app_id,
os,
os_version,
country,  
install_source,
browser,
browser_version,
language,
step,
user_cnt
FROM agg_tutorial
WHERE 
app='${hiveconf:rpt_app}' 
AND install_dt <= '${hiveconf:rpt_date}'
AND install_dt > '${hiveconf:rpt_date_days_back}';

exit;
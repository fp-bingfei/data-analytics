-----------------------------
-- Dump tbl_user to Redshift
-----------------------------

-- create tables
DROP TABLE if exists tbl_user;
CREATE TABLE tbl_user
(
    app varchar(50)
    ,uid varchar(200)
    ,snsid varchar(200)
    ,name varchar(200)
    ,email varchar(200)
    ,track_ref varchar(200)
    ,addtime varchar(200)
    ,level varchar(200)
    ,reward_points varchar(200)
    ,coins varchar(200)
    ,logintime varchar(200)
);

DROP TABLE if exists tbl_user_staging;
CREATE TABLE tbl_user_staging
(
     uid varchar(200)
    ,snsid varchar(200)
    ,name varchar(200)
    ,email varchar(200)
    ,track_ref varchar(200)
    ,addtime varchar(200)
    ,level varchar(200)
    ,reward_points varchar(200)
    ,coins varchar(200)
    ,logintime varchar(200)
);


--us
copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/us/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.us.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;
   

--br
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/br/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.br.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;


--de
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/de/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.de.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;


--fr
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/fr/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.fr.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;


--it
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/it/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.it.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;


--nl
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/nl/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.nl.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;

--pl
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/pl/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.pl.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;


--th
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/th/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.th.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;

--tw
truncate table tbl_user_staging;

copy tbl_user_staging 
from 's3://com.funplusgame.bidata/etl/farm/tw/mysql/tbl_user/${RPT_DATE_NOHYPHEN}/' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'  
DELIMITER '\t' IGNOREHEADER 1 FILLRECORD GZIP;


INSERT INTO tbl_user
select 'farm.tw.prod'
,uid 
,snsid 
,name 
,email 
,track_ref 
,addtime 
,level 
,reward_points 
,coins 
,logintime 
from    tbl_user_staging;


commit;

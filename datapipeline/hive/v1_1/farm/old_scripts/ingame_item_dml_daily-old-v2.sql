
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size= 67108864;
SET mapred.max.split.size = 134217728;
set hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;

  -- Populate Fact Item --

INSERT OVERWRITE TABLE fact_item PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(t.app,t.ts,t.uid)) AS id,
dt date,
 t.ts_pretty ts,
 MD5(CONCAT(t.app,t.uid)) AS user_key,
  t.app,
  t.uid,
  t.snsid,
  TO_DATE(t.install_ts_pretty) install_date,
  t.level,
  t.action,
  CASE WHEN t.item_in>0 THEN COALESCE(t.item_name,'Unknown') ELSE 'Unknown' END  in_name,
  CASE WHEN t.item_in>0 THEN COALESCE(t.item_type,'Unknown') ELSE 'Unknown' END in_type,
  CASE WHEN t.item_in>0 THEN t.item_in ELSE 0 END  in_amount,
  CASE WHEN t.item_out>0 THEN COALESCE(t.item_name,'Unknown') ELSE 'Unknown' END  out_name,
  CASE WHEN t.item_out>0 THEN COALESCE(t.item_type,'Unknown') ELSE 'Unknown' END  out_type,
  CASE WHEN t.item_out>0 THEN t.item_out ELSE 0 END  out_amount,
  item_class,
  item_id
FROM raw_item_transaction_daily t 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}';

-- Populate Agg Item --

INSERT OVERWRITE TABLE agg_item PARTITION (app = '${hiveconf:rpt_app}', dt = '${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(a.date,a.app,a.level,a.transaction_type,a.in_name,a.in_type,a.out_name,a.out_type,a.item_class,a.item_id)) AS id,
a.date,
a.app,
null install_date,
a.level,
a.transaction_type,
a.in_name,
a.in_type,
a.out_name,
a.out_type,
SUM(COALESCE(in_amount,0)) in_amount,
SUM(COALESCE(out_amount,0)) out_amount,
COUNT(DISTINCT a.user_key) purchase_cnt,
0 user_cnt,
a.item_class,
a.item_id
FROM
 fact_item a 
WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}'
GROUP BY a.date, a.app, a.level, a.transaction_type, a.in_name, a.in_type, a.out_name, a.out_type, a.item_class, a.item_id,a.dt;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert into table copy_agg_item partition(app='farm.us.prod',dt='${hiveconf:rpt_date}') 
select id,date,app_name, install_date, level, transaction_type, in_name, in_type, out_name, out_type, in_amount, out_amount, 
purchaser_cnt,user_cnt,item_class,item_id 
from agg_item where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';

exit;
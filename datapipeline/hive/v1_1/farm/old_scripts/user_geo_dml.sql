------- CREATE TABLES IF NOT EXISTS --------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

CREATE EXTERNAL TABLE IF NOT EXISTS temp_user_geo_${hiveconf:rpt_geo}(
uid STRING,
snsid STRING,
email STRING,
level INT,
experience INT,
coins INT,
reward_points INT,
new_cash1 INT,
new_cash2 INT,
order_points INT,
time_points INT,
op INT,
gas INT,
lottery_coins INT,
size_x INT,
size_y INT,
top_map_size INT,
max_work_area_size INT,
work_area_size INT,
addtime timestamp,
logintime timestamp,
loginip STRING,
status INT,
continuous_day INT,
point INT,
feed_status STRING,
extra_info STRING,
track_ref STRING,
fish_op STRING,
name STRING,
picture STRING,
loginnum INT,
note STRING,
fb_source STRING,
pay_times INT
) PARTITIONED BY (date INT)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS TEXTFILE
LOCATION "s3://com.funplusgame.bidata/etl/farm/${hiveconf:rpt_geo}/mysql/tbl_user/"
TBLPROPERTIES('serialization.null.format'='');


CREATE EXTERNAL TABLE IF NOT EXISTS user_geo_${hiveconf:rpt_geo} (
user_id STRING,
uid STRING,
snsid STRING,
email STRING,
addtime timestamp,
logintime timestamp,
loginip STRING,
status INT
) PARTITIONED BY (dt STRING)
ROW FORMAT DELIMITED 
FIELDS TERMINATED BY '\t' 
STORED AS PARQUET
LOCATION "s3://com.funplus.datawarehouse/farm_1_1/others/user/${hiveconf:rpt_geo}/"
TBLPROPERTIES('serialization.null.format'='');


--- Querys ----

ALTER TABLE temp_user_geo_${hiveconf:rpt_geo} ADD IF NOT EXISTS PARTITION (date=${hiveconf:rpt_date_plus1_nohyphen}) LOCATION "s3://com.funplusgame.bidata/etl/farm/${hiveconf:rpt_geo}/mysql/tbl_user/${hiveconf:rpt_date_plus1_nohyphen}/";

INSERT OVERWRITE TABLE user_geo_${hiveconf:rpt_geo} PARTITION (dt='${hiveconf:rpt_date}')
SELECT
   md5(concat(
   if(uid is null, "uid", uid),
   if(snsid is null, "snsid", snsid),
   if(email is null, "email", email)
   )) as user_id,
 uid,
 snsid,
 email,
 addtime,
 logintime,
 loginip,
 status
FROM
 (select  uid, snsid, email, addtime, logintime, loginip, status, row_number() over (partition by uid order by logintime desc) AS rnum 
 	FROM  temp_user_geo_${hiveconf:rpt_geo} WHERE date=${hiveconf:rpt_date_plus1_nohyphen} AND uid <> 'uid') 
 t where rnum = 1;
 

 -- Drop Old partitions in temp table --
 
ALTER TABLE temp_user_geo_${hiveconf:rpt_geo} DROP IF EXISTS PARTITION (date < ${hiveconf:rpt_date_plus1_nohyphen});


exit;





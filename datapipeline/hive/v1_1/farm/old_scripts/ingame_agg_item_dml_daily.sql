
use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size= 134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

-- Populate Agg Item --

INSERT OVERWRITE TABLE agg_item PARTITION (app='${hiveconf:rpt_app}', dt='${hiveconf:rpt_date}')
SELECT
MD5(CONCAT(date,p.app,COALESCE(p.level,0),COALESCE(q.os,''),COALESCE(q.device,''),COALESCE(q.browser,''),
COALESCE(q.country,''),COALESCE(q.language,''),
COALESCE(transaction_type,''),COALESCE(received_name,''),COALESCE(received_type,''),
COALESCE(spent_name,''),COALESCE(spent_type,''),COALESCE(received_item_class,''),COALESCE(spent_item_class,''))) AS id,
date,
p.app,
p.level,
q.os,
q.device,
q.browser,
q.country,
q.language, 
transaction_type,
received_id,
received_name,
received_type,
SUM(COALESCE(received_amount,0)) received_amount,
received_item_class,
spent_id,
spent_name,
spent_type,
SUM(COALESCE(spent_amount,0)) spent_amount,
spent_item_class,
COUNT(DISTINCT p.user_key) purchaser_cnt,
0 user_cnt
FROM
(SELECT * from fact_item WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}')p
LEFT OUTER JOIN 
(SELECT * from dim_user WHERE app='${hiveconf:rpt_app}' AND dt='${hiveconf:rpt_date}')q ON p.user_key = q.user_key
GROUP BY date, p.app, p.level, q.os, q.device, q.browser, q.country, q.language, transaction_type, received_id, received_name, received_type, received_item_class, spent_id, spent_name, spent_type, spent_item_class;


-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;

insert overwrite table copy_agg_item partition(app='farm.us.prod',dt='${hiveconf:rpt_date}') 
select id,date,app_id,level,os,device,browser,country,language,transaction_type,received_id,
received_name,received_type,received_amount,received_item_class, spent_id,spent_name,spent_type,spent_amount,spent_item_class,purchaser_cnt,user_cnt 
from agg_item where app='${hiveconf:rpt_app}' and dt='${hiveconf:rpt_date}';
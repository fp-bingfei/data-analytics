
---------create a temp table from fact_levelup which gives levelup_ts, level_start and level_end from the fact_levelup table -------------

use farm_1_1;

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

INSERT OVERWRITE TABLE tmp_user_level PARTITION (app='${hiveconf:rpt_app}' , dt ='${hiveconf:rpt_date}')
select levelup_date,
user_key,
app_id,
max(levelup_ts) levelup_ts,
min(previous_level) level_start,
max(current_level) level_end
from fact_levelup where app='${hiveconf:rpt_app}' and dt ='${hiveconf:rpt_date}'
group by levelup_date,app_id,user_key
;



exit;

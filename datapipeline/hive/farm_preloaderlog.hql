-------------------------------
-- Farm preloaderLog
-- 
-- app=farm.preloaderLog
-------------------------------
CREATE DATABASE IF NOT EXISTS farm;
USE farm;

CREATE EXTERNAL TABLE IF NOT EXISTS action_start (
    isNew boolean,
    loginStartDate string,
    uid string,
    lang string,
    loginSession string,
    ver int,
    type string,
    action string,
    clientInfo struct<
        flashvars: map<
            string, string
        >,
        href: string,
        sys: string,
        browser: string
    >
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm/events/preloaderLog'
TBLPROPERTIES (
  'serialization.null.format'=''
);

CREATE EXTERNAL TABLE IF NOT EXISTS action_end (
    ver int,
    loginStartDate string,
    loginEndDate string,
    uid string,
    lang string,
    loginSession string,
    type string,
    action string
)
PARTITIONED BY (
  app string,
  year int,
  month int,
  day int)
ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
STORED AS SEQUENCEFILE
LOCATION 's3://com.funplus.datawarehouse/farm/events/preloaderLog'
TBLPROPERTIES (
  'serialization.null.format'=''
);

-- Update Hive metastore for partitions
MSCK REPAIR TABLE action_start;
MSCK REPAIR TABLE action_end;

-- Examples
-- select * from action_start where action='start' limit 10;
-- select * from action_end where action='end' limit 10;

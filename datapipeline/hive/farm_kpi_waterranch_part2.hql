
SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=50;
SET hive.merge.mapredfiles=true;
SET rpt_dt='${wr_rpt_dt}';
SET rpt_par=${wr_rpt_par};
SET rpt_pre_par=${wr_rpt_pre_par};
SET rpt_yr=${wr_rpt_yr};
SET rpt_mnth=${wr_rpt_mnth};
SET rpt_day=${wr_rpt_day};

SET;
--create DB;
CREATE DATABASE IF NOT EXISTS farm;

-- Change DB;
USE farm;
ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

--Add events data of rpt day;
drop table if exists farm.events_1day;
create external table events_1day (
id STRING, 
app STRING, 
ts TIMESTAMP, 
uid STRING, 
snsid STRING, 
install_ts TIMESTAMP, 
install_source STRING, 
country_code STRING, 
ip STRING, 
browser STRING, 
browser_version STRING, 
os STRING, 
os_version STRING, 
event STRING, 
properties STRING
) partitioned by (year int, month int, day int, hour int)
row format delimited fields terminated by '\t' location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/'
TBLPROPERTIES ('serialization.null.format'='');        
        

alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=00) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/00/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=01) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/01/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=02) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/02/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=03) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/03/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=04) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/04/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=05) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/05/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=06) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/06/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=07) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/07/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=08) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/08/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=09) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/09/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=10) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/10/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=11) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/11/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=12) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/12/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=13) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/13/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=14) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/14/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=15) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/15/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=16) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/16/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=17) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/17/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=18) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/18/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=19) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/19/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=20) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/20/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=21) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/21/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=22) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/22/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=23) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/23/';

-- Add app_user data;
drop table if exists farm.app_user;
create external table farm.app_user (
 user_id string,
 app string,
 uid string,
 snsid string,
 install_ts string,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 is_payer string,
 conversion_ts string,
 last_login_ts string
) PARTITIONED BY (date int) 
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE app_user add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user/${hiveconf:rpt_par}/";

drop table if exists farm.app_user_daily_data;
create external table farm.app_user_daily_data (
 app string,
 uid string,
 snsid string,
 install_ts timestamp,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 conversion_ts timestamp,
 last_login_ts timestamp
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user_daily/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE app_user_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user_daily/${hiveconf:rpt_par}/";

drop table if exists dau_temp;
create table dau_temp as 
select * from 
(
select
 md5(concat(app,uid,snsid,dt)) as user_id,
 app,
 dt,
 uid,
 snsid,
 install_source,
 install_dt,
 os,
 os_version,
 country_code,
 level,
 browser,
 browser_version from (select 
 app,
 dt,
 uid,
 snsid,
 install_source,
 install_dt,
 os,
 os_version,
 country_code,
 level,
 browser,
 browser_version,
 row_number() over(partition by app,uid,snsid order by dt) as rank
from
 (
 SELECT 
e.app as app,
to_date(e.ts) as dt,
e.uid as uid,
e.snsid as snsid,
a.install_source, 
to_date(e.install_ts) AS install_dt,
a.os as os,
a.os_version as os_version,
a.country_code as country_code,
max(get_json_object(properties,'$.level')) AS LEVEL,
a.browser as browser,
e.browser_version as browser_version
FROM 
 events_1day e left outer join farm.app_user_daily_data a 
on (e.app=a.app and e.snsid=a.snsid and e.uid=a.uid)
WHERE to_date(e.ts)=${hiveconf:rpt_dt}
GROUP BY 
 e.app,to_date(e.ts),e.uid,e.snsid,a.install_source,to_date(e.install_ts),a.os,a.os_version,a.country_code,a.browser,e.browser_version
) t ) t2 where t2.rank = 1
) dau_data;

drop table if exists farm.dau_daily_data;
create external table farm.dau_daily_data (
 user_id string,
 app string,
 dt string,
 uid string,
 snsid string,
 install_source string,
 install_dt string,
 os string,
 os_version string,
 country_code string,
 level string,
 browser string,
 browser_version string
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/dau_daily/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE dau_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/dau_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE dau_daily_data partition(date=${hiveconf:rpt_par}) select * from dau_temp;

-- PAYMENT for Data;

--For currency;
drop table if exists farm.currency;
create external table farm.currency (
id STRING, 
dt STRING,
currency STRING,
factor FLOAT
) partitioned by (date int)
row format delimited fields terminated by '\t' location 's3://com.funplusgame.bidata/currency/'
TBLPROPERTIES ('serialization.null.format'='');        


alter table currency add partition(date=${hiveconf:rpt_par}) location "s3://com.funplusgame.bidata/currency/${hiveconf:rpt_par}/";

drop table if exists payment_temp;
create table payment_temp as
SELECT 
 md5(concat(e.app,e.uid,e.snsid,e.ts)) as user_id,
 e.uid,
 e.snsid,
 e.app,
 e.ts,
 to_date(e.install_ts) as install_dt,
 a.install_source,
 a.os,
 a.os_version,
 a.browser,
 e.browser_version,
 a.country_code,
 get_json_object(properties,'$.product_id') as product_id,
 get_json_object(properties,'$.product_name') as product_name,
 cast(case when get_json_object(properties,'$.coins_bal')='' then '0' else get_json_object(properties,'$.coins_bal') end as int) as coins_bal,
 get_json_object(properties,'$.lang') as lang,
 get_json_object(properties,'$.currency') as currency,
 cast(case when get_json_object(properties,'$.level')='' then '0' else get_json_object(properties,'$.level') end as int) as level,
 get_json_object(properties,'$.payment_processor') as payment_processor,
 (get_json_object(properties,'$.amount')/100.0) * c.factor as amount,
 get_json_object(properties,'$.is_gift') as is_gift,
 cast(case when get_json_object(properties,'$.coins_in')='' then '0' else get_json_object(properties,'$.coins_in') end as int) as coins_in,
 get_json_object(properties,'$.product_type') as product_type,
 cast(case when get_json_object(properties,'$.rc_in')='' then '0' else get_json_object(properties,'$.rc_in') end as int) as rc_in,
 get_json_object(properties,'$.transaction_id') as transaction_id,
 cast(case when get_json_object(properties,'$.rc_bal')='' then '0' else get_json_object(properties,'$.rc_bal') end as int) as rc_bal
FROM events_1day e left outer join farm.app_user a on (e.app=a.app and e.snsid=a.snsid and e.uid=a.uid) 
left outer join farm.currency c on (get_json_object(e.properties,'$.currency')) = c.currency and to_date(e.ts)=c.dt
where e.event = 'payment'
and to_date(e.ts)=${hiveconf:rpt_dt};

drop table if exists farm.payment_daily_data;
create external table farm.payment_daily_data (
user_id		  string,
uid               string,
snsid             string,
app               string,   
ts                timestamp,
install_dt        string,   
install_source    string,   
os                string,   
os_version        string,   
browser           string,   
browser_version   string,   
country_code      string,   
product_id        string,   
product_name      string,   
coins_bal         int,      
lang              string,   
currency          string,   
level             int,      
payment_processor string,   
amount            double,   
is_gift           string,   
coins_in          int   ,   
product_type      string,   
rc_in             int   ,   
transaction_id    string,   
rc_bal            int
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/payment_daily/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE payment_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/payment_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE payment_daily_data partition(date=${hiveconf:rpt_par}) select * from payment_temp;

-- FOR KPI RAW data;

--login count;
drop table if exists kpi_raw_login;
create table kpi_raw_login as
SELECT
 e.app as app,
 to_date(e.ts) as dt,
 to_date(e.install_ts) as install_dt,
 e.install_source,
 e.os,
 e.browser,
 a.country_code,
 count(*) as cnt,
 'session_start' as kpi_metric
FROM events_1day e left outer join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
 WHERE e.event='session_start'
  AND to_date(e.ts)=${hiveconf:rpt_dt}
GROUP BY
 e.app,
 to_date(e.ts),
 to_date(e.install_ts),
 e.install_source,
 e.os,
 e.browser,
 a.country_code;

--dau count;
drop table if exists kpi_raw_dau;
create table kpi_raw_dau as
SELECT
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 count(DISTINCT user_id) AS cnt,
 'dau' as kpi_metric
FROM dau_daily_data
where dt=${hiveconf:rpt_dt}
GROUP BY
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code;

 --payer_dau cnt
drop table if exists kpi_raw_payerdau;
create table kpi_raw_payerdau as
 SELECT
  d.app,
  d.dt,
  d.install_dt,
  d.install_source,
  d.os,
  d.browser,
  d.country_code,
  count(d.user_id) AS cnt,
  'payer_dau' as kpi_metric
 FROM dau_daily_data d
 JOIN app_user a ON d.user_id = a.user_id
 and a.is_payer='1'
 WHERE d.dt=${hiveconf:rpt_dt}
 GROUP BY
 d.app, d.dt, d.install_dt, d.install_source, d.os, d.country_code, d.browser;
  
drop table if exists kpi_raw_newusers0;
create table kpi_raw_newusers0 as
SELECT
 u.app,
 to_date(u.install_ts) as dt,
 u.install_source,
 u.os,
 u.browser,
 u.country_code,
 count(u.app) AS cnt
FROM app_user_daily_data u
WHERE to_date(u.install_ts)=${hiveconf:rpt_dt}
GROUP BY
 u.app,
 to_date(u.install_ts),
 u.install_source,
 u.os,
 u.browser,
 u.country_code
 ;
 
 drop table if exists kpi_raw_newusers;
 create table kpi_raw_newusers as
  SELECT
   app,
   dt,
   dt as install_dt,
   install_source,
   os,
   browser,
   country_code,
   cnt,
   'new_user' as kpi_metric
  FROM kpi_raw_newusers0
  ;

-- new payer cnt;
 drop table if exists kpi_raw_newpayers;
 create table kpi_raw_newpayers as
 SELECT app,
        to_date(conversion_ts) as dt,
        to_date(install_ts) as install_dt,
        install_source,
        os,
        browser,
        country_code,
        count(user_id) AS cnt,
        'newpayers' as kpi_metric
 FROM app_user
 WHERE to_date(conversion_ts)=${hiveconf:rpt_dt}
 and is_payer = '1'
 group by
 app,
 to_date(conversion_ts),
 to_date(install_ts),
 install_source,
 os,
 browser,
country_code;

--revenue;
drop table if exists kpi_raw_revenue;
create table kpi_raw_revenue as
SELECT
 app,
 to_date(ts) as dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 sum(amount) as cnt,
 'payment' as kpi_metric
from
 payment_daily_data
where  to_date(ts)=${hiveconf:rpt_dt}
GROUP BY
 app,
 to_date(ts),
 install_dt,
 install_source,
 os,
 browser,
 country_code;

drop table if exists kpi_raw_temp;
create table kpi_raw_temp as
select
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 cnt,
 kpi_metric
from
(
 select * from kpi_raw_login
  UNION ALL
 select * from kpi_raw_dau
  UNION ALL
 select * from kpi_raw_newusers
  UNION ALL
 select * from kpi_raw_payerdau
  UNION ALL
 select * from kpi_raw_newpayers
  UNION ALL
 select * from kpi_raw_revenue
) kpi_tables;

drop table if exists farm.kpi_raw_daily_data;
create external table farm.kpi_raw_daily_data (
 app string,
 dt string,
 install_dt string,
 install_source string,
 os string,
 browser string,
 country_code string,
 cnt double,
 kpi_metric string
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/kpi_raw_daily/';

ALTER TABLE kpi_raw_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/kpi_raw_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE kpi_raw_daily_data partition (date=${hiveconf:rpt_par}) select * from kpi_raw_temp;

-- For KPI metrics;
drop table if exists kpi_data_temp;
create table kpi_data_temp as
SELECT
 md5(concat(
  if(app is null, "app", app),
  if(dt is null, "dt", dt),
  if(install_dt is null, "install_dt", install_dt),
  if(install_source is null, "install_source", install_source),
  if(os is null, "os", os),
  if(browser is null, "browser", browser),
  if(country_code is null, "country_code", country_code)
  )) as user_id,
 app,
 dt,
 install_dt,
 REGEXP_REPLACE(if(install_source is null, 'NULL', install_source), '[^-a-zA-Z0-9_.: ]+', '') as install_source,
 REGEXP_REPLACE(if(os is null, 'NULL', os), '[^-a-zA-Z0-9_.: ]+', '') as os,
 REGEXP_REPLACE(if(browser is null, 'NULL', browser), '[^-a-zA-Z0-9_.: ]+', '') as browser,
 country_code,
 sum((case
  when kpi_metric = 'payment' then cnt
  else 0.0
 end)) as amount,
 sum((case
   when kpi_metric = 'session_start' then cnt
   else 0.0
 end)) as login_cnt,
sum((case
    when kpi_metric = 'dau' then cnt
    else 0.0
 end)) as dau_cnt,
 sum((case
    when kpi_metric = 'new_user' then cnt
    else 0.0
 end)) as newuser_cnt,
 sum((case
    when kpi_metric = 'newpayers' then cnt
    else 0.0
 end)) as newpayers_cnt,
 sum((case
    when kpi_metric = 'payer_dau' then cnt
    else 0.0
 end)) as payer_dau_cnt
FROM kpi_raw_temp
where dt=${hiveconf:rpt_dt}
GROUP BY
app,
dt,
install_dt,
install_source,
os,
browser,
country_code;

drop table if exists farm.kpi_daily_data;
create external table farm.kpi_daily_data (
 user_id string,
 app string,
 dt string,
 install_dt string,
 install_source string,
 os string,
 browser string,
 country_code string,
 amount double,
 login_cnt double,
 dau_cnt double,
 newuser_cnt double,
 newpayers_cnt double,
 payer_dau_cnt double
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/kpi_daily/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE kpi_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/kpi_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE kpi_daily_data partition (date=${hiveconf:rpt_par}) select * from kpi_data_temp;

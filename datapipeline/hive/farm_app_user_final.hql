
--SET hive.exec.max.created.files=100;
SET hive.hadoop.supports.splittable.combineinputformat=true;
--SET mapreduce.input.fileinputformat.split.maxsize=134217728;
SET mapred.reduce.tasks=50;

-- Change DB;
USE farm;

--Adding MD5 UDF; 
add jar /home/hadoop/cchandra/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

-- create user country;
create table farm.user_country
as
select 
 app
 ,uid
 ,snsid
 ,install_ts
 ,country_code
from(
SELECT
  app
  ,uid
  ,snsid
  ,install_ts
  ,country_code
  ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events_1day 
where event <> 'payment'
and   to_date(ts)="${rpt_dt}" 
) t
where rank = 1;

--create app user temp table;
create table farm.app_user_temp1 as
select 
       t3.app,
       t3.uid,
       t3.snsid,
       t3.install_ts,
       t3.install_source,
       t3.os,
       t3.os_version,
       t3.country_code,
       t3.level,
       t3.language,
       t3.browser,
       t3.browser_version,
       t3.ts as latest_login_ts
  from (
select 
 t1.app,
 t1.uid,
 t1.snsid,
 t1.install_source,
 t1.install_ts,
 t1.os,
 t1.os_version,
 t1.level,
 t1.language,
 t1.browser,
 t1.browser_version,
 t1.ts,
 row_number() over(partition by t1.app, t1.uid, t1.snsid order by t1.install_ts desc) as rank,
 t2.country_code
from
(select
 app,
 uid,
 snsid,
 install_source,
 install_ts,
 os,
 os_version,
 max(get_json_object(properties,'$.level')) as level,
 get_json_object(properties,'$.language') as language,
 browser,
 browser_version,
 ts
from events_1day
 where to_date(ts)="${rpt_dt}"
 group by 
app,
uid,
snsid,
install_source,
install_ts,
os,
os_version,
get_json_object(properties,'$.language'),
browser,
browser_version, ts) t1 left outer join user_country t2
on (t1.app = t2.app and t1.snsid = t2.snsid and t1.uid = t2.uid)) t3 where rank = 1;

-- get conversion_ts for todays data;
create table farm.app_user_temp2 as
select a.*, b.conversion_ts
 from farm.app_user_temp1 a left outer join farm.app_user b
  on (a.uid=b.uid and a.snsid=b.snsid and a.app=b.app);

--get payer info details;
create table farm.payer_user_info
as
select app,
 uid,
 snsid,
 min(ts) as conversion_ts
FROM events_1day 
where event = 'payment'
and to_date(ts)="${rpt_dt}"
group by app,uid,snsid;

--update conversion_ts of payment users;
-- This is new data for the day;
create table farm.app_user_temp3 as
select
 a.app,
 a.uid,
 a.snsid,
 a.install_ts,
 a.install_source,
 a.os,
 a.os_version,
 a.country_code,
 a.level,
 a.language,
 a.browser,
 a.browser_version,
 COALESCE(cast(b.conversion_ts as string), a.conversion_ts) as conversion_ts,
 a.latest_login_ts
from farm.app_user_temp2 a left outer join farm.payer_user_info b 
   on (a.uid=b.uid and a.snsid=b.snsid and a.app=b.app);

-- for delete statment;
create table farm.app_user_temp4 as
 select a.* from farm.app_user a left outer join farm.app_user_temp3 b
  on (a.uid=b.uid and a.snsid=b.snsid and a.app=b.app) 
   where (b.uid is null or b.snsid is null or b.app is null);

-- delete previous app_user;
drop table farm.app_user;

--update app_user table;
create table farm.app_user as
 select 
 user_id,
 app,
 uid,
 snsid,
 if(install_ts is null, "", cast(install_ts as string)) as install_ts,
 REGEXP_REPLACE(if(install_source is null, 'NULL', install_source), '[^-a-zA-Z0-9_.: ]+', '') as install_source,
 REGEXP_REPLACE(if(os is null, 'NULL', os), '[^-a-zA-Z0-9_.: ]+', '') as os,
 REGEXP_REPLACE(if(os_version is null, 'NULL', os_version), '[^-a-zA-Z0-9_.: ]+', '') as os_version,
 if(country_code is null, 'NULL', country_code) as country_code,
 if(level is null, '0', level) as level,
 REGEXP_REPLACE(if(language is null, 'NULL', language), '[^-a-zA-Z0-9_.: ]+', '') as language,
 REGEXP_REPLACE(if(browser is null, 'NULL', browser), '[^-a-zA-Z0-9_.: ]+', '') as browser,
 REGEXP_REPLACE(if(browser_version is null, 'NULL', browser_version), '[^-a-zA-Z0-9_.: ]+', '') as browser_version,
 if(conversion_ts is null, '0', '1') as is_payer,
 if(conversion_ts is null, "", cast(conversion_ts as string)) as conversion_ts,
 if(last_login_ts is null, "", cast(last_login_ts as string)) as last_login_ts
 from (
 select 
 user_id,
 app,
 uid,
 snsid,
 install_ts,
 install_source,
 os,
 os_version,
 country_code,
 level,
 language,
 browser,
 browser_version,
 conversion_ts,
 last_login_ts 
 from 
 app_user_temp4 
 UNION ALL
 select 
 md5(concat(app,uid,snsid)) as user_id,
 app,
 uid,
 snsid,
 install_ts,
 install_source,
 os,
 os_version,
 country_code,
 level,
 language,
 browser,
 browser_version,
 conversion_ts,
 latest_login_ts as last_login_ts
 from app_user_temp3
 ) app_user_union;

ALTER TABLE app_user_daily_data add partition (date="${rpt_par}") location 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/app_user_daily/${rpt_par}/';

INSERT OVERWRITE TABLE app_user_daily_data partition (date="${rpt_par}") select * from app_user_temp3;



SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=50;

--Change DB;
USE farm;

--Adding jar file;
add jar /home/hadoop/cchandra/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

create table kpi_data_temp as
SELECT 
 md5(concat(
  if(app is null, "app", app), 
  if(dt is null, "dt", dt), 
  if(install_dt is null, "install_dt", install_dt), 
  if(install_source is null, "install_source", install_source), 
  if(os is null, "os", os), 
  if(browser is null, "browser", browser), 
  if(country_code is null, "country_code", country_code) 
  )) as user_id,
 app,
 dt,
 install_dt,
 install_source,
 os,
 browser,
 country_code,
 sum((case
  when kpi_metric = 'payment' then cnt
  else 0.0
 end)) as amount,
 sum((case
   when kpi_metric = 'session_start' then cnt
   else 0.0
 end)) as login_cnt,
sum((case
    when kpi_metric = 'dau' then cnt
    else 0.0
 end)) as dau_cnt,
 sum((case
    when kpi_metric = 'newuser' then cnt
    else 0.0
 end)) as newuser_cnt,
 sum((case
    when kpi_metric = 'newpayers' then cnt
    else 0.0
 end)) as newpayers_cnt,
 sum((case
    when kpi_metric = 'payer_dau' then cnt
    else 0.0
 end)) as payer_dau_cnt 
FROM kpi_raw_temp
where dt = "${rpt_dt}"
GROUP BY 
app,
dt,
install_dt,
install_source,
os,
browser,
country_code;

ALTER TABLE kpi_daily_data add partition (date="${rpt_par}") location 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/kpi_daily/${rpt_par}/';

INSERT OVERWRITE TABLE kpi_daily_data partition (date="${rpt_par}") select * from kpi_data_temp;


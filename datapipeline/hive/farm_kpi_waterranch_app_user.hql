
SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=50;
SET hive.merge.mapredfiles=true;
SET rpt_dt='${wr_rpt_dt}';
SET rpt_par=${wr_rpt_par};
SET rpt_pre_par=${wr_rpt_pre_par};
SET rpt_yr=${wr_rpt_yr};
SET rpt_mnth=${wr_rpt_mnth};
SET rpt_day=${wr_rpt_day};

SET;
--create DB;
CREATE DATABASE IF NOT EXISTS farm;

-- Change DB;
USE farm;
ADD JAR s3://com.funplusgame.emr/results/hive_automation/scripts/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

-- Add app_user previous day table;
drop table if exists farm.app_user;
create external table farm.app_user (
 user_id string,
 app string,
 uid string,
 snsid string,
 install_ts string,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 is_payer string,
 conversion_ts string,
 last_login_ts string
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user/'
TBLPROPERTIES ('serialization.null.format'='');

ALTER TABLE app_user add partition (date=${hiveconf:rpt_pre_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user/${hiveconf:rpt_pre_par}/";


--Add events data of rpt day;
drop table if exists farm.events_1day;
create external table events_1day (
id STRING, 
app STRING, 
ts TIMESTAMP, 
uid STRING, 
snsid STRING, 
install_ts TIMESTAMP, 
install_source STRING, 
country_code STRING, 
ip STRING, 
browser STRING, 
browser_version STRING, 
os STRING, 
os_version STRING, 
event STRING, 
properties STRING
) partitioned by (year int, month int, day int, hour int)
row format delimited fields terminated by '\t' location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/'
TBLPROPERTIES ('serialization.null.format'='');        

alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=00) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/00/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=01) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/01/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=02) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/02/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=03) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/03/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=04) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/04/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=05) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/05/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=06) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/06/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=07) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/07/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=08) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/08/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=09) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/09/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=10) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/10/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=11) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/11/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=12) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/12/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=13) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/13/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=14) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/14/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=15) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/15/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=16) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/16/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=17) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/17/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=18) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/18/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=19) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/19/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=20) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/20/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=21) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/21/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=22) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/22/';
alter table events_1day add partition (year=${hiveconf:rpt_yr}, month=${hiveconf:rpt_mnth}, day=${hiveconf:rpt_day}, hour=23) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/${hiveconf:rpt_yr}/${hiveconf:rpt_mnth}/${hiveconf:rpt_day}/23/';

drop table if exists events_app_user;
create table events_app_user as
select * from events_1day
 where (event='session_end' or event='session_start' or event='DAU' or event='levelup' or event='newuser');

-- create user country;
drop table if exists farm.user_country;
create table farm.user_country
as
select 
 app
 ,uid
 ,snsid
 ,install_ts
 ,country_code
from(
SELECT
  app
  ,uid
  ,snsid
  ,install_ts
  ,country_code
  ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events_app_user 
where event <> 'payment'
and   to_date(ts)=${hiveconf:rpt_dt} 
) t
where rank = 1;

--create app user temp table;
drop table if exists farm.app_user_temp1;
create table farm.app_user_temp1 as
select 
       t3.app,
       t3.uid,
       t3.snsid,
       t3.install_ts,
       t3.install_source,
       t3.os,
       t3.os_version,
       t3.country_code,
       t3.level,
       t3.language,
       t3.browser,
       t3.browser_version,
       t3.ts as latest_login_ts
  from (
select 
 t1.app,
 t1.uid,
 t1.snsid,
 t1.install_source,
 t1.install_ts,
 t1.os,
 t1.os_version,
 t1.level,
 t1.language,
 t1.browser,
 t1.browser_version,
 t1.ts,
 row_number() over(partition by t1.app, t1.uid, t1.snsid order by t1.install_ts desc) as rank,
 t2.country_code
from
(select
 app,
 uid,
 snsid,
 install_source,
 install_ts,
 os,
 os_version,
 max(get_json_object(properties,'$.level')) as level,
 get_json_object(properties,'$.lang') as language,
 browser,
 browser_version,
 ts
from events_app_user
 where to_date(ts)=${hiveconf:rpt_dt}
 group by 
app,
uid,
snsid,
install_source,
install_ts,
os,
os_version,
get_json_object(properties,'$.lang'),
browser,
browser_version, ts) t1 left outer join user_country t2
on (t1.app = t2.app and t1.snsid = t2.snsid and t1.uid = t2.uid)) t3 where rank = 1;

-- get conversion_ts for todays data;
drop table if exists farm.app_user_temp2;
create table farm.app_user_temp2 as
select a.*, b.conversion_ts
 from farm.app_user_temp1 a left outer join farm.app_user b
  on (a.uid=b.uid and a.snsid=b.snsid and a.app=b.app);

--get payer info details;
drop table if exists farm.payer_user_info;
create table farm.payer_user_info
as
select app,
 uid,
 snsid,
 min(ts) as conversion_ts
FROM events_1day 
where event = 'payment'
and to_date(ts)=${hiveconf:rpt_dt}
group by app,uid,snsid;

--update conversion_ts of payment users;
-- This is new data for the day;
drop table if exists farm.app_user_temp3;
create table farm.app_user_temp3 as
select
 a.app,
 a.uid,
 a.snsid,
 a.install_ts,
 a.install_source,
 a.os,
 a.os_version,
 a.country_code,
 a.level,
 a.language,
 a.browser,
 a.browser_version,
 COALESCE(a.conversion_ts,cast(b.conversion_ts as string)) as conversion_ts,
 a.latest_login_ts
from farm.app_user_temp2 a left outer join farm.payer_user_info b 
   on (a.uid=b.uid and a.snsid=b.snsid and a.app=b.app);

drop table if exists farm.app_user_daily_data;
create external table farm.app_user_daily_data (
 app string,
 uid string,
 snsid string,
 install_ts timestamp,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 conversion_ts timestamp,
 last_login_ts timestamp
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user_daily/'
TBLPROPERTIES ('serialization.null.format'='');        

ALTER TABLE app_user_daily_data add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user_daily/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE app_user_daily_data partition (date=${hiveconf:rpt_par}) select * from app_user_temp3;

-- for delete statment;
drop table if exists farm.app_user_temp4;
create table farm.app_user_temp4 as
 select a.* from farm.app_user a left outer join farm.app_user_temp3 b
  on (a.uid=b.uid and a.snsid=b.snsid and a.app=b.app) 
   where (b.uid is null or b.snsid is null or b.app is null);

drop table if exists farm.app_user_final;
create table farm.app_user_final as
 select 
 user_id,
 app,
 uid,
 snsid,
 if(install_ts is null, "", cast(install_ts as string)) as install_ts,
 REGEXP_REPLACE(if(install_source is null, 'NULL', install_source), '[^-a-zA-Z0-9_.: ]+', '') as install_source,
 REGEXP_REPLACE(if(os is null, 'NULL', os), '[^-a-zA-Z0-9_.: ]+', '') as os,
 REGEXP_REPLACE(if(os_version is null, 'NULL', os_version), '[^-a-zA-Z0-9_.: ]+', '') as os_version,
 if(country_code is null, 'NULL', country_code) as country_code,
 if(level is null, '0', level) as level,
 REGEXP_REPLACE(if(language is null, 'NULL', language), '[^-a-zA-Z0-9_.: ]+', '') as language,
 REGEXP_REPLACE(if(browser is null, 'NULL', browser), '[^-a-zA-Z0-9_.: ]+', '') as browser,
 REGEXP_REPLACE(if(browser_version is null, 'NULL', browser_version), '[^-a-zA-Z0-9_.: ]+', '') as browser_version,
 if(conversion_ts is null, '0', '1') as is_payer,
 if(conversion_ts is null, "", cast(conversion_ts as string)) as conversion_ts,
 if(last_login_ts is null, "", cast(last_login_ts as string)) as last_login_ts
 from (
 select 
 user_id,
 app,
 uid,
 snsid,
 install_ts,
 install_source,
 os,
 os_version,
 country_code,
 level,
 language,
 browser,
 browser_version,
 conversion_ts,
 last_login_ts 
 from 
 app_user_temp4 
 UNION ALL
 select 
 md5(concat(app,uid,snsid)) as user_id,
 app,
 uid,
 snsid,
 install_ts,
 install_source,
 os,
 os_version,
 country_code,
 level,
 language,
 browser,
 browser_version,
 conversion_ts,
 latest_login_ts as last_login_ts
 from app_user_temp3
 ) app_user_union;

-- delete previous app_user;
drop table if exists farm.app_user;
create external table farm.app_user (
 user_id string,
 app string,
 uid string,
 snsid string,
 install_ts string,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 is_payer string,
 conversion_ts string,
 last_login_ts string
) PARTITIONED BY (date int) 
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user/'
TBLPROPERTIES ('serialization.null.format'='');        


ALTER TABLE app_user add partition (date=${hiveconf:rpt_par}) location "s3://com.funplusgame.emr/results/hive_automation/hive_warehouse/app_user/${hiveconf:rpt_par}/";
INSERT OVERWRITE TABLE app_user partition (date=${hiveconf:rpt_par}) select * from app_user_final;



===== Create tables script =======

--Step 00: add partition on events daily data
create external table events_1day (
id STRING, 
app STRING, 
ts TIMESTAMP, 
uid STRING, 
snsid STRING, 
install_ts TIMESTAMP, 
install_source STRING, 
country_code STRING, 
ip STRING, 
browser STRING, 
browser_version STRING, 
os STRING, 
os_version STRING, 
event STRING, 
properties STRING
) partitioned by (year int, month int, day int, hour int)
row format delimited fields terminated by '\t' location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/';        

alter table events_1day add partition (year=2014, month=09, day=05, hour=00) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/00/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=01) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/01/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=02) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/02/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=03) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/03/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=04) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/04/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=05) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/05/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=06) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/06/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=07) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/07/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=08) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/08/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=09) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/09/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=10) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/10/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=11) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/11/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=12) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/12/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=13) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/13/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=14) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/14/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=15) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/15/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=16) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/16/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=17) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/17/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=18) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/18/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=19) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/19/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=20) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/20/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=21) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/21/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=22) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/22/';
alter table events_1day add partition (year=2014, month=09, day=05, hour=23) location 's3://com.funplusgame.bidata/etl/results/farm/events/staging/hourly/2014/09/05/23/';


--Step 01: APP_USER;
create external table farm.app_user (
 user_id string,
 app string,
 uid string,
 snsid string,
 install_ts string,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 is_payer string,
 conversion_ts string,
 last_login_ts string
) ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/app_user/';


create external table farm.app_user_daily_data (
 app string,
 uid string,
 snsid string,
 install_ts timestamp,
 install_source string,
 os string,
 os_version string,
 country_code string,
 level string,
 language string,
 browser string,
 browser_version string,
 conversion_ts timestamp,
 last_login_ts timestamp
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/app_user_daily/';


-- for DAU;
create external table farm.dau_daily_data (
 user_id string,
 app string,
 dt string,
 uid string,
 snsid string,
 install_source string,
 install_dt string,
 os string,
 os_version string,
 country_code string,
 level string,
 browser string,
 browser_version string
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/dau_daily/';


--For currency;
create external table farm.currency (
id STRING, 
dt STRING,
currency STRING,
factor FLOAT
) partitioned by (date int)
row format delimited fields terminated by '\t' location 's3://com.funplusgame.bidata/currency/';



--For payment;
create external table farm.payment_daily_data (
user_id		  string,
uid               string,
snsid             string,
app               string,   
ts                timestamp,
install_dt        string,   
install_source    string,   
os                string,   
os_version        string,   
browser           string,   
browser_version   string,   
country_code      string,   
product_id        string,   
product_name      string,   
coins_bal         int,      
lang              string,   
currency          string,   
level             int,      
payment_processor string,   
amount            double,   
is_gift           string,   
coins_in          int   ,   
product_type      string,   
rc_in             int   ,   
transaction_id    string,   
rc_bal            int
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/payment_daily/';



--For KPI RAW;
create external table farm.kpi_raw_daily_data (
 app string,
 dt string,
 install_dt string,
 install_source string,
 os string,
 browser string,
 country_code string,
 cnt double,
 kpi_metric string
) PARTITIONED BY (date int)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/kpi_raw_daily/';


--For KPI;
create external table farm.kpi_daily_data (
 user_id string,
 app string,
 dt string,
 install_dt string,
 install_source string,
 os string,
 browser string,
 country_code string,
 amount double,
 login_cnt double,
 dau_cnt double,
 newuser_cnt double,
 newpayers_cnt double,
 payer_dau_cnt double
) PARTITIONED BY (date string)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
LOCATION 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/kpi_daily/';


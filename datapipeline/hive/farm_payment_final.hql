
SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=50;

--Change DB;
USE farm;

--Adding jar file;
add jar /home/hadoop/cchandra/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

--Add currency;
alter table currency add partition(date="${rpt_par}") location 's3://com.funplusgame.bidata/currency/${rpt_par}/';

create table payment_temp as
SELECT 
 md5(concat(e.app,e.uid,e.snsid,e.ts)) as user_id,
 e.uid,
 e.snsid,
 e.app,
 e.ts,
 to_date(e.install_ts) as install_dt,
 a.install_source,
 a.os,
 a.os_version,
 a.browser,
 e.browser_version,
 a.country_code,
 get_json_object(properties,'$.product_id') as product_id,
 get_json_object(properties,'$.product_name') as product_name,
 cast(case when get_json_object(properties,'$.coins_bal')='' then '0' else get_json_object(properties,'$.coins_bal') end as int) as coins_bal,
 get_json_object(properties,'$.lang') as lang,
 get_json_object(properties,'$.currency') as currency,
 cast(case when get_json_object(properties,'$.level')='' then '0' else get_json_object(properties,'$.level') end as int) as level,
 get_json_object(properties,'$.payment_processor') as payment_processor,
 (get_json_object(properties,'$.amount')/100.0) * c.factor as amount,
 get_json_object(properties,'$.is_gift') as is_gift,
 cast(case when get_json_object(properties,'$.coins_in')='' then '0' else get_json_object(properties,'$.coins_in') end as int) as coins_in,
 get_json_object(properties,'$.product_type') as product_type,
 cast(case when get_json_object(properties,'$.rc_in')='' then '0' else get_json_object(properties,'$.rc_in') end as int) as rc_in,
 get_json_object(properties,'$.transaction_id') as transaction_id,
 cast(case when get_json_object(properties,'$.rc_bal')='' then '0' else get_json_object(properties,'$.rc_bal') end as int) as rc_bal
FROM events_1day e left outer join farm.app_user a on (e.app=a.app and e.snsid=a.snsid and e.uid=a.uid) 
left outer join farm.currency c on (get_json_object(e.properties,'$.currency')) = c.currency and to_date(e.ts)=c.dt
where e.event = 'payment'
and to_date(e.ts)= "${rpt_dt}";

ALTER TABLE payment_daily_data add partition (date="${rpt_par}") location 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/payment_daily/${rpt_par}/';

INSERT OVERWRITE TABLE payment_daily_data partition (date="${rpt_par}") select * from payment_temp;




SET hive.hadoop.supports.splittable.combineinputformat=true;
SET mapred.reduce.tasks=50;

--Change DB;
USE farm;

--adding md5 function jar file;
add jar /home/hadoop/cchandra/hive_udfs.jar;
create temporary function md5 as 'com.funplus.analytics.mr.udfs.Md5';

-- create dau temp;
create table dau_temp as 
select * from 
(
select  
 md5(concat(app,uid,snsid)) as user_id,
 app,
 dt,
 uid,
 snsid,
 install_source,
 install_dt,
 os,
 os_version,
 country_code,
 level,
 browser,
 browser_version from (select 
 app,
 dt,
 uid,
 snsid,
 install_source,
 install_dt,
 os,
 os_version,
 country_code,
 level,
 browser,
 browser_version,
 row_number() over(partition by app,uid,snsid order by dt) as rank
from
 (
 SELECT 
e.app as app,
to_date(e.ts) as dt,
e.uid as uid,
e.snsid as snsid,
a.install_source, 
to_date(e.install_ts) AS install_dt,
a.os as os,
a.os_version as os_version,
a.country_code as country_code,
max(get_json_object(properties,'$.level')) AS LEVEL,
a.browser as browser,
e.browser_version as browser_version
FROM 
 events_1day e left outer join farm.app_user a 
on (e.app=a.app and e.snsid=a.snsid and e.uid=a.uid)
WHERE to_date(e.ts)="${rpt_dt}"
GROUP BY 
 e.app,to_date(e.ts),e.uid,e.snsid,a.install_source,to_date(e.install_ts),a.os,a.os_version,a.country_code,a.browser,e.browser_version
) t ) t2 where t2.rank = 1
) dau_data;

ALTER TABLE dau_daily_data add partition (date="${rpt_par}") location 's3://com.funplusgame.emr/results/farm_backfill/hive_warehouse/dau_daily/${rpt_par}/';

INSERT OVERWRITE TABLE dau_daily_data partition(date="${rpt_par}") select * from dau_temp;



-- Water Ranch;
drop table rpt_waterranch_exp;commit;
create table rpt_waterranch_exp (
 app varchar(100) encode lzo,
 dt varchar(100) encode lzo,
 install_dt date DISTKEY encode delta32k,
 install_source varchar(100) encode lzo,
 os varchar(100) encode lzo,
 os_version varchar(100) encode lzo,
 browser varchar(100) encode lzo,
 browser_version varchar(100) encode lzo,
 country_code varchar(100) encode bytedict,
 action varchar(100) encode lzo,
 exp_size_from varchar(100) encode lzo,
 level int encode bytedict,
 water_level int encode bytedict,
 id varchar(100) encode lzo,
 uid_cnt bigint encode raw,
 tot_water_in bigint encode raw,
 primary key(id)
) sortkey(app,country_code,dt,os,browser);
commit;

----------------------------;

drop table rpt_waterranch_greenery;commit;
create table rpt_waterranch_greenery (
 app varchar(100) encode lzo,
 dt varchar(100) encode lzo,
 install_dt date DISTKEY encode delta32k,
 install_source varchar(100) encode lzo,
 os varchar(100) encode lzo,
 os_version varchar(100) encode lzo,
 browser varchar(100) encode lzo,
 browser_version varchar(100) encode lzo,
 country_code varchar(100) encode bytedict,
 action varchar(100) encode lzo,
 exp_size_from varchar(100) encode lzo,
 level int encode bytedict,
 water_level int encode bytedict,
 id varchar(100) encode lzo,
 tot_greenery_in bigint encode raw,
 tot_greenery_out bigint encode raw,
 primary key(id)
)sortkey(app,country_code,dt,os,browser);commit;

-----------------------------------;
drop table waterranch_3rdscene;commit;
create table waterranch_3rdscene (
user_id         varchar(100) encode lzo,
uid             varchar(100) encode lzo,
snsid           varchar(100) encode lzo distkey,
app             varchar(100) encode lzo,
ts              timestamp encode lzo,
install_dt      varchar(100) encode lzo,
install_source  varchar(100) encode lzo,
os              varchar(100) encode lzo,
os_version      varchar(100) encode lzo,
browser         varchar(100) encode lzo,
browser_version varchar(100) encode raw,
country_code    varchar(100) encode bytedict,
action          varchar(100) encode lzo,
exp_size_from   varchar(100) encode raw,
greenery_bal    bigint encode raw,
coins_bal    bigint   encode raw,
rc_bal    bigint   encode raw,
level           int   encode raw,
waterLevel     bigint  encode raw, 
expansion_size_to  int encode raw,
expansion_top_size_to  int encode raw,
primary key(user_id)
);commit;

drop table rpt_waterranch_3rdscene;commit;
create table rpt_waterranch_3rdscene (
 app varchar(100) encode lzo,
 dt varchar(100) encode lzo,
 install_dt date DISTKEY encode delta32k,
 install_source varchar(100) encode lzo,
 os varchar(100) encode lzo,
 os_version varchar(100) encode lzo,
 browser varchar(100) encode lzo,
 browser_version varchar(100) encode lzo,
 country_code varchar(100) encode bytedict,
 action varchar(100) encode lzo,
 exp_size_from varchar(100) encode lzo,
 level int encode raw,
 waterLevel int encode raw,
 expansion_size_to int encode raw,
 expansion_top_size_to int encode raw,
 id varchar(100) encode lzo,
 expansion_size_to_cnt bigint encode raw,
 expansion_top_size_to_cnt bigint encode raw,
  primary key(id)
);commit;



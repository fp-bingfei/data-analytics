
drop table app_user;commit;
create table app_user (
 user_id varchar(100) encode lzo,
 app varchar(25) encode lzo,
 uid varchar(100) encode lzo,
 snsid varchar(100) encode lzo DISTKEY,
 install_ts timestamp encode raw,
 install_source varchar(1000) encode lzo,
 os varchar(1000) encode lzo,
 os_version varchar(1000) encode lzo,
 country_code varchar(10) encode lzo,
 level int encode raw,
 language varchar(100) encode lzo,
 browser varchar(1000) encode lzo,
 browser_version varchar(1000) encode lzo,
 is_payer boolean encode raw,
 conversion_ts datetime encode raw,
 last_login_ts datetime encode raw,
 primary key(user_id)
);commit;

-- DAU;
drop table dau;commit;
create table dau (
 user_id varchar(100) encode lzo,
 app varchar(100) encode lzo,
 dt varchar(100) encode lzo,
 uid varchar(100) encode lzo,
 snsid varchar(100) encode lzo DISTKEY,
 install_source varchar(100) encode lzo,
 install_dt date encode delta32k,
 os varchar(100) encode lzo,
 os_version varchar(100) encode lzo,
 country_code varchar(100) encode bytedict,
 level int encode bytedict,
 browser varchar(100) encode lzo,
 browser_version varchar(100) encode lzo,
 primary key(user_id)
);commit;

-- Payment;
drop table payment;commit;
create table payment (
user_id varchar(100) encode lzo,
uid varchar(100) encode lzo,
snsid varchar(100) encode lzo DISTKEY, 
app varchar(100) encode lzo,
ts varchar(100) encode lzo,
install_dt varchar(100) encode lzo,     
install_source varchar(100) encode lzo,
os varchar(100) encode lzo,              
os_version varchar(100) encode lzo, 
browser varchar(100) encode lzo,
browser_version varchar(100) encode lzo,
country_code varchar(100) encode bytedict,
product_id varchar(100) encode raw,
product_name varchar(1000) encode lzo,
coins_bal integer encode raw,
lang varchar(100) encode lzo,
currency varchar(100) encode lzo,
level integer encode raw,
payment_processor varchar(100) encode lzo,
amount float encode raw,
is_gift varchar(100) encode raw,     
coins_in integer encode raw,
product_type varchar(100) encode lzo,    
rc_in integer encode raw,
transaction_id varchar(100) encode raw,   
rc_bal integer encode lzo,
primary key(user_id)
);commit;

drop table kpi;commit;
create table kpi (
user_id varchar(100) encode lzo,
app varchar(100) encode lzo,
dt varchar(100) encode lzo DISTKEY,
install_dt varchar(100) encode lzo,
install_source varchar(100) encode lzo,
os varchar(100) encode lzo,
browser varchar(100) encode lzo,
country_code varchar(100) encode lzo,
amount float encode raw,
login_cnt float encode raw,
dau_cnt float encode raw,
newuser_cnt float encode raw,
newpayers_cnt float encode raw,
payer_dau_cnt float encode raw,
primary key(user_id)
);commit;


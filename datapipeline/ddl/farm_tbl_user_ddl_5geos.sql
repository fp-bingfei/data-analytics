
-- Farm Tbl User;

-- DE locale;

drop table farm_tbl_user_de;commit;
create table farm_tbl_user_de (
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_de from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/de/20141016/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

-- -----------------------------;
--FR;
drop table farm_tbl_user_fr;commit;
create table farm_tbl_user_fr (
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 feed_status varchar(1000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_fr from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/fr/20141016/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;


drop table farm_tbl_user_nl;commit;
create table farm_tbl_user_nl (
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 feed_status varchar(1000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_nl from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/nl/20141016/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;


drop table farm_tbl_user_pl;commit;
create table farm_tbl_user_pl (
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 feed_status varchar(1000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_pl from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/pl/20141016/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;


-- -----------------------------;
--TH;

drop table farm_tbl_user_th;commit;
create table farm_tbl_user_th(
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 extra_info varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_th from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/th/20141015/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;


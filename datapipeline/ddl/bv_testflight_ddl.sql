CREATE TABLE events_raw 
(
  id                VARCHAR(50) NOT NULL ENCODE LZO,
  app               VARCHAR(50) NOT NULL ENCODE LZO,
  ts                TIMESTAMP NOT NULL ENCODE DELTA SORTKEY,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(100) NOT NULL DISTKEY ENCODE LZO,
  install_ts        TIMESTAMP ENCODE BYTEDICT,
  install_source    VARCHAR(500) ENCODE LZO,
  country_code      VARCHAR(30) ENCODE BYTEDICT,
  ip                VARCHAR(30) ENCODE LZO,
  browser           VARCHAR(30) ENCODE BYTEDICT,
  browser_version   VARCHAR(30) ENCODE BYTEDICT,
  os                VARCHAR(30) ENCODE BYTEDICT,
  os_version        VARCHAR(30) ENCODE LZO,
  event             VARCHAR(50) NOT NULL ENCODE LZO,
  properties        VARCHAR(20000) ENCODE LZO,
  PRIMARY KEY (id)
);

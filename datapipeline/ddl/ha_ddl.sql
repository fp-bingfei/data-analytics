CREATE TABLE events_raw 
(
  id                VARCHAR(50) NOT NULL ENCODE LZO,
  app               VARCHAR(50) NOT NULL ENCODE LZO,
  ts                TIMESTAMP NOT NULL ENCODE DELTA SORTKEY,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(100) NOT NULL DISTKEY ENCODE LZO,
  install_ts        TIMESTAMP ENCODE BYTEDICT,
  install_source    VARCHAR(500) ENCODE LZO,
  country_code      VARCHAR(30) ENCODE BYTEDICT,
  ip                VARCHAR(30) ENCODE LZO,
  browser           VARCHAR(30) ENCODE BYTEDICT,
  browser_version   VARCHAR(200) ENCODE BYTEDICT,
  os                VARCHAR(30) ENCODE BYTEDICT,
  os_version        VARCHAR(200) ENCODE LZO,
  event             VARCHAR(50) NOT NULL ENCODE LZO,
  properties        VARCHAR(20000) ENCODE LZO,
  PRIMARY KEY (id)
);

CREATE TABLE tbl_user 
(
  user_id           VARCHAR(50) NOT NULL ENCODE LZO,
  app               VARCHAR(50) NOT NULL ENCODE LZO,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(100) NOT NULL DISTKEY ENCODE LZO,
  install_source    VARCHAR(500) ENCODE LZO,
  install_ts        TIMESTAMP ENCODE BYTEDICT,
  os                VARCHAR(30) ENCODE BYTEDICT,
  os_version        VARCHAR(200) ENCODE LZO,
  country_code      VARCHAR(30) ENCODE BYTEDICT,
  level             INTEGER,
  device            VARCHAR(100) ENCODE LZO,
  lang VARCHAR(100) ENCODE BYTEDICT,
  browser           VARCHAR(100) ENCODE LZO,
  browser_version   VARCHAR(100) ENCODE LZO,
  rc                INTEGER,
  coins             INTEGER,
  email             VARCHAR(100) ENCODE LZO,
  gender            VARCHAR(20) ENCODE LZO,
  name              VARCHAR(5000) ENCODE LZO,
  age_num           VARCHAR(10) ENCODE LZO,
  PRIMARY KEY (user_id)
);
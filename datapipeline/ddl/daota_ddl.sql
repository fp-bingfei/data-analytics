CREATE TABLE app_user
(
   user_id          varchar(50) encode lzo,
   app              varchar(50) encode lzo,
   uid              integer encode lzo,
   snsid            varchar(50) encode lzo distkey,
   install_ts       timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   level            integer encode bytedict,
   language         varchar(100) encode lzo,
   device           varchar(100) encode lzo,
   is_payer         boolean         DEFAULT false encode raw,
   conversion_ts    timestamp encode raw,
   latest_login_ts  timestamp encode lzo
)
;

CREATE TABLE dau
(
   user_id          varchar(50) encode lzo,
   app              varchar(30) encode lzo,
   dt               date encode lzo,
   uid              varchar(100) encode lzo,
   snsid            varchar(100) encode lzo distkey,
   install_source   varchar(100) encode lzo,
   install_dt       date encode delta sortkey,
   os               varchar(50) encode lzo,
   os_version       varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   level            integer encode bytedict,
   device          varchar(100) encode lzo
)
;


CREATE TABLE payment
(
   user_id          varchar(50) encode lzo,
   uid                integer    encode lzo,
   snsid              varchar(50) encode lzo distkey,
   app                varchar(50) encode lzo,
   ts                 timestamp encode lzo sortkey,
   install_dt         date encode delta,
   install_source     varchar(50) encode lzo,
   os                 varchar(50) encode lzo,
   os_version         varchar(50) encode lzo,
   device             varchar(100) encode lzo,
   country_code       varchar(10) encode bytedict,
   product_id         varchar(100) encode raw,
   product_name       varchar(100) encode raw,
   lang               varchar(10) encode lzo,
   currency           varchar(10) encode raw,
   level              integer encode delta,
   payment_processor  varchar(50) encode raw,
   amount             numeric(18) encode raw,
   is_gift            varchar(10) encode raw,
   coins_in           integer encode raw,
   product_type       varchar(50) encode raw,
   rc_in              integer encode raw,
   transaction_id     varchar(50) encode lzo,
   promo_id           varchar(50) encode raw,
   is_promo           varchar(10) encode raw
)
;

CREATE TABLE kpi_raw
(
   app             varchar(50),
   dt              date,
   install_dt      date,
   install_source  varchar(100),
   os              varchar(30),
   device          varchar(100),
   country_code    varchar(10),
   amount          numeric(18,4),
   login_cnt       integer,
   dau_cnt         integer,
   newuser_cnt     integer,
   newpayer_cnt    integer,
   payer_dau_cnt   integer,
   event           varchar(100)
);


CREATE TABLE kpi
(
   app             varchar(50) encode lzo,
   dt              date encode lzo,
   install_dt      date encode delta32k,
   install_source  varchar(100) encode lzo,
   os              varchar(30) encode bytedict,
   device         varchar(100) encode bytedict,
   country_code    varchar(10) encode bytedict,
   amount          float8 encode raw,
   login_cnt       integer encode raw,
   dau_cnt         integer encode lzo,
   newuser_cnt     integer encode raw,
   newpayer_cnt    integer encode raw,
   payer_dau_cnt   integer encode raw
)
sortkey(app,country_code,dt,os,device);

---------------------------------choose distkey and sort-------------------------

CREATE TABLE item_transaction
(
   snsid            varchar(120) encode lzo distkey,
   uid              varchar(100) encode lzo,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   device          varchar(100) encode lzo,
   install_dt       date encode delta32k,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(50) encode lzo,
   item_in          float8 encode bytedict,
   item_out         float8 encode bytedict,
   item_bal         float8 encode runlength,
   level            varchar(30) encode bytedict,
   action           varchar(120) encode lzo,
   location         varchar(120) encode lzo,
   action_detail    varchar(100) encode lzo,
   item_name        varchar(150) encode lzo,
   item_class       varchar(50) encode lzo,
   item_type        varchar(100) encode lzo,
   item_id          varchar(120) encode lzo
)
;


CREATE TABLE coin_transaction
(
   snsid            varchar(100) encode lzo distkey,
   uid              varchar(100) encode lzo,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   device          varchar(50) encode lzo,
   install_dt       date encode delta32k,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(50) encode lzo,
   coins_in         bigint encode lzo,
   coins_out        bigint encode bytedict,
   coins_bal        bigint encode lzo,
   level            varchar(20) encode bytedict,
   action           varchar(100) encode lzo,
   location         varchar(100) encode lzo,
   action_detail    varchar(150) encode lzo
)
;



CREATE TABLE rpt_item
(
   app              varchar(50) encode lzo,
   dt               date encode lzo,
   install_dt       date encode delta32k,
   install_source   varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   device          varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo, 
   item_in          bigint encode bytedict,
   item_out         bigint encode bytedict,
   item_bal         bigint encode runlength,
   level            varchar(50) encode lzo,
   location         varchar(150) encode lzo,
   action           varchar(150) encode lzo,
   action_detail    varchar(150) encode lzo,
   item_name        varchar(150) encode lzo,
   item_class       varchar(50) encode lzo,
   item_type        varchar(150) encode lzo,
   item_id          varchar(100) encode lzo
)
sortkey(app,dt,country_code,os,device);

CREATE TABLE rc_transaction
(
   snsid            varchar(100) encode lzo distkey,
   uid              varchar(100) encode lzo,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   device          varchar(100) encode lzo,
   install_dt       date encode delta32k,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(50) encode lzo,
   rc_in            integer encode delta,
   rc_out           integer encode delta,
   rc_bal           integer encode bytedict,
   level            varchar(20) encode bytedict,
   action           varchar(120) encode lzo,
   location         varchar(120) encode lzo,
   action_detail    varchar(120) encode lzo
)
;


CREATE TABLE rpt_rc
(
   app              varchar(50) encode lzo,
   dt               date encode raw,
   install_dt       date encode raw,
   install_source   varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   device          varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo,
   rc_in            bigint encode delta,
   rc_out           bigint encode delta,
   rc_bal           bigint encode bytedict,
   level            varchar(50) encode lzo,
   location         varchar(100) encode lzo,
   action           varchar(120) encode lzo,
   action_detail    varchar(120) encode lzo
)
sortkey(app,dt,country_code,os,device);


CREATE TABLE quest_completion
(
   uid              varchar(100) encode lzo,
   snsid            varchar(100) encode lzo distkey,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   device          varchar(100) encode lzo,
   install_dt       date encode delta32k,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(100) encode lzo,
   coins_in         float8 encode bytedict,
   coins_out        float8 encode runlength,
   coins_bal        float8 encode bytedict,
   rc_in            integer encode lzo,
   rc_out           integer encode lzo,
   rc_bal           integer encode lzo,
   quest_id         varchar(100) encode lzo,
   lang             varchar(100) encode lzo,
   level            varchar(50) encode lzo,
   action           varchar(120) encode lzo,
   task_id          varchar(50) encode lzo,
   quest_type       varchar(50) encode lzo
)
;



CREATE TABLE rpt_coin
(
   app              varchar(50) encode lzo,
   dt               date encode lzo,
   install_dt       date encode delta32k,
   install_source   varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   device          varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo,
   coins_in         bigint encode delta32k,
   coins_out        bigint encode lzo,
   coins_bal        bigint encode lzo,
   level            varchar(50) encode lzo,
   location         varchar(100) encode text255,
   action           varchar(120) encode lzo,
   action_detail    varchar(120) encode lzo
)
sortkey(app,dt,country_code,os,device);



CREATE TABLE rpt_tutorial
(
   user_id         varchar(100) encode lzo,
   snsid           varchar(100) encode lzo,
   uid             varchar(100) encode lzo,
   max_dt          date encode lzo,
   step            varchar(50) encode lzo,
   app             varchar(50) encode lzo,
   t_snsid         varchar(100) encode lzo,
   install_source  varchar(100) encode lzo,
   os              varchar(100) encode lzo,
   install_dt      date encode delta,
   country_code    varchar(20) encode bytedict,
   device         varchar(100) encode lzo,
   country         varchar(100) encode lzo
)
sortkey(app,max_dt,country_code,os,device);

CREATE TABLE kochava
(
   id                 varchar(100)    NOT NULL ENCODE LZO,
   app               VARCHAR(50) NOT NULL ENCODE LZO,
   campaign_id        varchar(50) ENCODE LZO,
   campaign_name      varchar(50) ENCODE LZO,
   click_date         timestamp ENCODE DELTA,
   country_code       varchar(10) ENCODE BYTEDICT,
   creative           varchar(200) ENCODE LZO,
   install_date       timestamp ENCODE DELTA SORTKEY,
   network_name       varchar(50) ENCODE LZO,
   site_id            varchar(500) ENCODE LZO,
   android_id         varchar(100) ENCODE LZO,
   android_md5        varchar(100) ENCODE LZO,
   android_sha1       varchar(100) ENCODE LZO,
   imei               varchar(100) ENCODE LZO,
   imei_md5           varchar(100) ENCODE LZO,
   imei_sha1          varchar(100) ENCODE LZO,
   udid               varchar(100) ENCODE LZO,
   udid_md5           varchar(100) ENCODE LZO,
   udid_sha1          varchar(100) ENCODE LZO,
   idfa               varchar(100) ENCODE LZO,
   idfv               varchar(100) ENCODE LZO,
   kochava_device_id  varchar(100) ENCODE LZO,
   odin               varchar(100) ENCODE LZO,
   mac                varchar(100) ENCODE LZO,
   fpid               varchar(50) ENCODE LZO DISTKEY,
   PRIMARY KEY (id)
   
);



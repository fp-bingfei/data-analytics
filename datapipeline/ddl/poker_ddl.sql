CREATE TABLE adjust 
(
  id             VARCHAR(50) ENCODE LZO,
  adid           VARCHAR(50) ENCODE LZO,
  android_id     VARCHAR(50) ENCODE LZO,
  app_id         VARCHAR(50) ENCODE LZO,
  country        VARCHAR(10) ENCODE LZO,
  game           VARCHAR(50) ENCODE LZO,
  idfa           VARCHAR(50) ENCODE LZO,
  idfa_md5       VARCHAR(50) ENCODE LZO,
  ip_address     VARCHAR(30) ENCODE LZO,
  mac_sha1       VARCHAR(50) ENCODE LZO,
  ts             TIMESTAMP ENCODE DELTA SORTKEY,
  tracker        VARCHAR(50) ENCODE LZO,
  tracker_name   VARCHAR(500) ENCODE LZO,
  userid         VARCHAR(50) ENCODE LZO DISTKEY,
  PRIMARY KEY (id)
);
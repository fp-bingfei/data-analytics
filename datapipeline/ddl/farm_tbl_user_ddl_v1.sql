
-- Farm Tbl User;

-- IT locale;

drop table farm_tbl_user_it;commit;
create table farm_tbl_user_it (
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 love_points varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_it from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/it/20141014/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

select * from farm_tbl_user_it limit 100;

select * from stl_load_errors where starttime > '1024-10-20 15:00:00';

desc farm_tbl_user_it;

truncate farm_tbl_user_it;commit;

-- -----------------------------;
--TW;
drop table farm_tbl_user_tw;commit;
create table farm_tbl_user_tw (
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 extra_info varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_it from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/tw/20141015/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

-- -----------------------------;
--TR;

drop table farm_tbl_user_tr;commit;
create table farm_tbl_user_tr(
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 love_points varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_it from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/tw/20141015/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

-- -----------------------------;
--AE;

drop table farm_tbl_user_ae;commit;
create table farm_tbl_user_ae(
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 love_points varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_ae from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/ae/20141015/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

-- -----------------------------;
--BR;

drop table farm_tbl_user_br;commit;
create table farm_tbl_user_br(
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 love_points varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_br from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/br/20141015/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

-- -----------------------------;
--US;

drop table farm_tbl_user_us;commit;
create table farm_tbl_user_us(
 user_id varchar(1000) encode lzo,
 uid varchar(1000) encode lzo DISTKEY,
 snsid varchar(1000) encode lzo,
 email varchar(1000) encode lzo,
 level varchar(1000) encode lzo,
 experience varchar(1000) encode lzo,
 coins bigint encode raw,
 reward_points varchar(1000) encode lzo,
 new_cash1 varchar(1000) encode lzo,
 new_cash2 varchar(1000) encode lzo,
 order_points varchar(1000) encode lzo,
 time_points varchar(1000) encode lzo,
 op varchar(1000) encode lzo,
 gas varchar(1000) encode lzo,
 lottery_coins varchar(1000) encode lzo,
 size_x varchar(1000) encode lzo,
 size_y varchar(1000) encode lzo,
 top_map_size varchar(1000) encode lzo,
 max_work_area_size varchar(1000) encode lzo,
 work_area_size varchar(1000) encode lzo,
 addtime varchar(1000) encode lzo,
 logintime varchar(1000) encode lzo,
 loginip varchar(1000) encode lzo,
 status varchar(1000) encode lzo,
 continuous_day varchar(1000) encode lzo,
 point_val varchar(1000) encode lzo,
 love_points varchar(1000) encode lzo,
 feed_status varchar(1000) encode lzo,
 track_ref varchar(5000) encode lzo,
 extra_info varchar(1000) encode lzo,
 fish_op varchar(1000) encode lzo,
 name varchar(1000) encode lzo,
 picture varchar(1000) encode lzo,
 loginnum varchar(1000) encode lzo,
 note varchar(1000) encode lzo,
 fb_source varchar(1000) encode lzo,
 pay_times integer encode raw,
 primary key(user_id)
);commit;

copy farm_tbl_user_us from 's3://com.funplusgame.emr/results/hive_automation/hive_warehouse/farm_tbl_user/us/20141015/' credentials 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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'
DELIMITER '\t' ACCEPTINVCHARS as ' ';commit;

select trunc(cast(addtime as datetime)) as dt, count(*) as cnt from  farm_tbl_user_br group by 1;


truncate farm_tbl_user_it;commit;
truncate farm_tbl_user_tr;commit;
truncate farm_tbl_user_tw;commit;
truncate farm_tbl_user_ae;commit;
truncate farm_tbl_user_br;commit;
truncate farm_tbl_user_us;commit;

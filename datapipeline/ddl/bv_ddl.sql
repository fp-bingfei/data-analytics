CREATE TABLE events_raw 
(
  id                VARCHAR(50) NOT NULL ENCODE LZO,
  app               VARCHAR(50) NOT NULL ENCODE LZO,
  ts                TIMESTAMP NOT NULL ENCODE DELTA SORTKEY,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(100) NOT NULL DISTKEY ENCODE LZO,
  install_ts        TIMESTAMP ENCODE BYTEDICT,
  install_source    VARCHAR(500) ENCODE LZO,
  country_code      VARCHAR(30) ENCODE BYTEDICT,
  ip                VARCHAR(30) ENCODE LZO,
  browser           VARCHAR(30) ENCODE BYTEDICT,
  browser_version   VARCHAR(30) ENCODE BYTEDICT,
  os                VARCHAR(30) ENCODE BYTEDICT,
  os_version        VARCHAR(30) ENCODE LZO,
  event             VARCHAR(50) NOT NULL ENCODE LZO,
  properties        VARCHAR(20000) ENCODE LZO,
  PRIMARY KEY (id)
);

CREATE TABLE experiments 
(
  id              VARCHAR(100) NOT NULL ENCODE LZO,
  app             VARCHAR(50) NOT NULL ENCODE LZO,
  exid            VARCHAR(512) DISTKEY NOT NULL ENCODE LZO,
  name            VARCHAR(512) NOT NULL ENCODE LZO,
  ex_type         VARCHAR(512) NOT NULL ENCODE LZO,
  status          VARCHAR(512) NOT NULL ENCODE LZO,
  start_ts_orig   float8,
  stop_ts_orig    float8,
  start_ts        TIMESTAMP SORTKEY,
  stop_ts         TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE TABLE variants 
(
  id              VARCHAR(100) NOT NULL ENCODE LZO,
  app             VARCHAR(50) NOT NULL ENCODE LZO,
  exid            VARCHAR(512) DISTKEY SORTKEY NOT NULL ENCODE LZO,
  vid             VARCHAR(512) NOT NULL ENCODE LZO,
  name            VARCHAR(512) NOT NULL ENCODE LZO,
  PRIMARY KEY (id)
);

CREATE TABLE experiment_assignments
(
   id                   VARCHAR(100) NOT NULL ENCODE LZO,
   app                  VARCHAR(50) NOT NULL ENCODE LZO,
   snsid                  varchar(512) NOT NULL ENCODE LZO,
   exid                 varchar(512) DISTKEY NOT NULL ENCODE LZO,
   vid                  varchar(512) NOT NULL ENCODE LZO,
   first_assignment_ts_orig  float8,
   first_assignment_ts  TIMESTAMP SORTKEY,
   PRIMARY KEY (id)
);

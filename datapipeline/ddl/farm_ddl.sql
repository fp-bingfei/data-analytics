CREATE TABLE app_user
(
   user_id          varchar(50) encode lzo,
   app              varchar(50) encode lzo,
   uid              integer encode lzo,
   snsid            varchar(50) encode lzo distkey,
   install_ts       timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   level            integer encode bytedict,
   language         varchar(100) encode lzo,
   browser          varchar(100) encode lzo,
  browser_version   varchar(100) encode lzo,
   is_payer         boolean         DEFAULT false encode raw,
   conversion_ts    timestamp encode raw,
   latest_login_ts  timestamp encode lzo
)
;

CREATE TABLE dau
(
   user_id          varchar(50) encode lzo,
   app              varchar(30) encode lzo,
   dt               date encode lzo,
   uid              varchar(100) encode lzo,
   snsid            varchar(100) encode lzo distkey,
   install_source   varchar(100) encode lzo,
   install_dt       date encode delta sortkey,
   os               varchar(50) encode lzo,
   os_version       varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   level            integer encode bytedict,
   browser          varchar(50) encode lzo,
   browser_version  varchar(50) encode lzo
)
;


CREATE TABLE payment
(
   user_id          varchar(50) encode lzo,
   uid                integer    encode lzo,
   snsid              varchar(50) encode lzo distkey,
   app                varchar(50) encode lzo,
   ts                 timestamp encode lzo sortkey,
   install_dt         date encode delta,
   install_source     varchar(50) encode lzo,
   os                 varchar(50) encode lzo,
   os_version         varchar(50) encode lzo,
   browser            varchar(50) encode lzo,
   browser_version    varchar(50) encode raw,
   country_code       varchar(10) encode bytedict,
   product_id         varchar(100) encode raw,
   product_name       varchar(100) encode raw,
   coins_bal          integer encode raw,
   lang               varchar(10) encode lzo,
   currency           varchar(10) encode raw,
   level              integer encode delta,
   payment_processor  varchar(50) encode raw,
   amount             numeric(18) encode raw,
   is_gift            varchar(10) encode raw,
   coins_in           integer encode raw,
   product_type       varchar(50) encode raw,
   rc_in              integer encode raw,
   transaction_id     varchar(50) encode lzo,
   rc_bal             integer encode raw
)
;

CREATE TABLE kpi
(
   app             varchar(50) encode lzo,
   dt              date encode lzo,
   install_dt      date encode delta32k,
   install_source  varchar(100) encode lzo,
   os              varchar(30) encode bytedict,
   browser         varchar(30) encode bytedict,
   country_code    varchar(10) encode bytedict,
   amount          float8 encode raw,
   login_cnt       integer encode raw,
   dau_cnt         integer encode lzo,
   newuser_cnt     integer encode raw,
   newpayer_cnt    integer encode raw,
   payer_dau_cnt   integer encode raw
)
sortkey(app,country_code,dt,os,browser);

---------------------------------choose distkey and sort-------------------------

CREATE TABLE item_transaction
(
   snsid            varchar(120) encode lzo distkey,
   uid              varchar(100) encode lzo,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   browser          varchar(50) encode lzo,
   install_dt       date encode delta32k,
   browser_version  varchar(50) encode lzo,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(50) encode lzo,
   item_in          float8 encode bytedict,
   item_out         float8 encode bytedict,
   item_bal         float8 encode runlength,
   level            varchar(30) encode bytedict,
   action           varchar(120) encode lzo,
   location         varchar(120) encode lzo,
   action_detail    varchar(100) encode lzo,
   item_name        varchar(150) encode lzo,
   item_class       varchar(50) encode lzo,
   item_type        varchar(100) encode lzo,
   item_id          varchar(120) encode lzo
)
;


CREATE TABLE coin_transaction
(
   snsid            varchar(100) encode lzo distkey,
   uid              varchar(100) encode lzo,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   browser          varchar(50) encode lzo,
   install_dt       date encode delta32k,
   browser_version  varchar(50) encode lzo,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(50) encode lzo,
   coins_in         bigint encode lzo,
   coins_out        bigint encode bytedict,
   coins_bal        bigint encode lzo,
   level            varchar(20) encode bytedict,
   action           varchar(100) encode lzo,
   location         varchar(100) encode lzo,
   action_detail    varchar(150) encode lzo
)
;



CREATE TABLE rpt_item
(
   app              varchar(50) encode lzo,
   dt               date encode lzo,
   install_dt       date encode delta32k,
   install_source   varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   browser          varchar(150) encode lzo,
   browser_version  varchar(150) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo, 
   item_in          bigint encode bytedict,
   item_out         bigint encode bytedict,
   item_bal         bigint encode runlength,
   level            varchar(50) encode lzo,
   location         varchar(150) encode lzo,
   action           varchar(150) encode lzo,
   action_detail    varchar(150) encode lzo,
   item_name        varchar(150) encode lzo,
   item_class       varchar(50) encode lzo,
   item_type        varchar(150) encode lzo,
   item_id          varchar(100) encode lzo
)
sortkey(app,dt,country_code,os,browser);

CREATE TABLE rc_transaction
(
   snsid            varchar(100) encode lzo distkey,
   uid              varchar(100) encode lzo,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   browser          varchar(50) encode lzo,
   install_dt       date encode delta32k,
   browser_version  varchar(50) encode lzo,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(50) encode lzo,
   rc_in            integer encode delta,
   rc_out           integer encode delta,
   rc_bal           integer encode bytedict,
   level            varchar(20) encode bytedict,
   action           varchar(120) encode lzo,
   location         varchar(120) encode lzo,
   action_detail    varchar(120) encode lzo
)
;


CREATE TABLE rpt_rc
(
   app              varchar(50) encode lzo,
   dt               date encode raw,
   install_dt       date encode raw,
   install_source   varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   browser          varchar(100) encode lzo,
   browser_version  varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo,
   rc_in            bigint encode delta,
   rc_out           bigint encode delta,
   rc_bal           bigint encode bytedict,
   level            varchar(50) encode lzo,
   location         varchar(100) encode lzo,
   action           varchar(120) encode lzo,
   action_detail    varchar(120) encode lzo
)
sortkey(app,dt,country_code,os,browser);


CREATE TABLE quest_completion
(
   uid              varchar(100) encode lzo,
   snsid            varchar(100) encode lzo distkey,
   app              varchar(50) encode lzo,
   ts               timestamp encode lzo sortkey,
   install_source   varchar(100) encode lzo,
   os               varchar(50) encode lzo,
   browser          varchar(100) encode lzo,
   install_dt       date encode delta32k,
   browser_version  varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   os_version       varchar(100) encode lzo,
   coins_in         float8 encode bytedict,
   coins_out        float8 encode runlength,
   coins_bal        float8 encode bytedict,
   rc_in            integer encode lzo,
   rc_out           integer encode lzo,
   rc_bal           integer encode lzo,
   quest_id         varchar(100) encode lzo,
   lang             varchar(100) encode lzo,
   level            varchar(50) encode lzo,
   action           varchar(120) encode lzo,
   task_id          varchar(50) encode lzo,
   quest_type       varchar(50) encode lzo
)
;



CREATE TABLE rpt_coin
(
   app              varchar(50) encode lzo,
   dt               date encode lzo,
   install_dt       date encode delta32k,
   install_source   varchar(100) encode lzo,
   country_code     varchar(10) encode bytedict,
   browser          varchar(100) encode lzo,
   browser_version  varchar(100) encode lzo,
   os               varchar(100) encode lzo,
   os_version       varchar(100) encode lzo,
   coins_in         bigint encode delta32k,
   coins_out        bigint encode lzo,
   coins_bal        bigint encode lzo,
   level            varchar(50) encode lzo,
   location         varchar(100) encode text255,
   action           varchar(120) encode lzo,
   action_detail    varchar(120) encode lzo
)
sortkey(app,dt,country_code,os,browser);



CREATE TABLE rpt_tutorial
(
   user_id         varchar(100) encode lzo,
   snsid           varchar(100) encode lzo,
   uid             varchar(100) encode lzo,
   max_dt          date encode lzo,
   step            varchar(50) encode lzo,
   app             varchar(50) encode lzo,
   t_snsid         varchar(100) encode lzo,
   install_source  varchar(100) encode lzo,
   os              varchar(100) encode lzo,
   install_dt      date encode delta,
   country_code    varchar(20) encode bytedict,
   browser         varchar(100) encode lzo,
   country         varchar(100) encode lzo
)
sortkey(app,max_dt,country_code,os,browser);


CREATE TABLE events_raw 
(
  id                VARCHAR(50) NOT NULL ENCODE LZO,
  app               VARCHAR(100) NOT NULL ENCODE LZO,
  ts                TIMESTAMP NOT NULL ENCODE DELTA SORTKEY,
  uid               INTEGER NOT NULL,
  snsid             VARCHAR(100) NOT NULL DISTKEY ENCODE LZO,
  install_ts        TIMESTAMP ENCODE BYTEDICT,
  install_source    VARCHAR(500) ENCODE LZO,
  country_code      VARCHAR(30) ENCODE BYTEDICT,
  ip                VARCHAR(30) ENCODE LZO,
  browser           VARCHAR(30) ENCODE BYTEDICT,
  browser_version   VARCHAR(200) ENCODE BYTEDICT,
  os                VARCHAR(100) ENCODE BYTEDICT,
  os_version        VARCHAR(200) ENCODE LZO,
  event             VARCHAR(50) NOT NULL ENCODE LZO,
  properties        VARCHAR(20000) ENCODE LZO,
  PRIMARY KEY (id)
);

CREATE TABLE adjust 
(
  id             VARCHAR(50) ENCODE LZO,
  adid           VARCHAR(50) ENCODE LZO,
  android_id     VARCHAR(50) ENCODE LZO,
  app_id         VARCHAR(50) ENCODE LZO,
  country        VARCHAR(10) ENCODE LZO,
  game           VARCHAR(50) ENCODE LZO,
  idfa           VARCHAR(50) ENCODE LZO,
  idfa_md5       VARCHAR(50) ENCODE LZO,
  ip_address     VARCHAR(30) ENCODE LZO,
  mac_sha1       VARCHAR(50) ENCODE LZO,
  ts             TIMESTAMP ENCODE DELTA SORTKEY,
  tracker        VARCHAR(50) ENCODE LZO,
  tracker_name   VARCHAR(500) ENCODE LZO,
  userid         VARCHAR(50) ENCODE LZO DISTKEY,
  PRIMARY KEY (id)
);

CREATE TABLE currency 
(
  id         VARCHAR(30) NOT NULL,
  dt         DATE NOT NULL SORTKEY,
  currency   VARCHAR(10) NOT NULL DISTKEY,
  factor     NUMERIC(15,10) NOT NULL,
  PRIMARY KEY (id)
);



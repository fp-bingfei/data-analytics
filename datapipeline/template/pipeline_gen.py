import json
import argparse
import os
import string
import shutil

class pipelineGen:
    def __init__(self, args):
        self.args = args


    def run(self):
        config = self.setParams()

        for filename in os.listdir(self.args['tmplpath']):
            if os.path.isfile(self.args['tmplpath'] + filename):
                output_name = self.filenameGen(filename, config)
                output_content = self.commandsGen(filename, config)
                self.writeFile(output_name, output_content)


    def filenameGen(self, filename, config):
        tmpl_name = string.Template(filename)
        result = self.args['tmplpath'] + 'result/' + tmpl_name.safe_substitute(app_all = config['app_all'])

        return result


    def commandsGen(self, filename, config):
        fullname = self.args['tmplpath'] + filename
        fp = open(fullname, 'r')
        tmpl_content = string.Template(fp.read())
        fp.close()
        result = tmpl_content.safe_substitute(id = config['id'], shortname = config['shortname'], app = config['apps'][0], \
                    schema = config['schema'], app_all = config['app_all'], workerGroup = config['workerGroup'], \
                    database_databaseName = config['database_databaseName'], database_username = config['database_username'],\
                    database_password = config['database_password'], database_clusterId = config['database_clusterId'],\
                    EMRcluster_name = config['EMRcluster_name'], EMRcluster_terminateAfter = config['EMRcluster_terminateAfter'],\
                    EMRcluster_coreInstanceCount = config['EMRcluster_coreInstanceCount'], pipelineName = config['pipelineName'])

        return result


    def writeFile(self, filename, content):
        fp = open(filename, 'w')
        fp.write(content)
        fp.close()


    def setParams(self):
        with open(self.args['file'], 'r') as f:
            json_object = json.load(f)
        f.close()
        if os.path.exists(self.args['tmplpath'] + 'result/'):
            shutil.rmtree(self.args['tmplpath'] + 'result/')
        os.makedirs(self.args['tmplpath'] + 'result/')

        return json_object
 


def parse_args():
    parser = argparse.ArgumentParser(description='Events count verification')
    parser.add_argument('-t', '--tmplpath', help='path to template folder')
    parser.add_argument('-f', '--file', help='path to configuration files')
    args = vars(parser.parse_args())

    return args


def main():
    args = parse_args()
    pg = pipelineGen(args)
    pg.run()


if __name__ == '__main__':
    main()
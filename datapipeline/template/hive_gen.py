import json
import argparse

class hiveGen:
    def __init__(self, args):
        self.args = args


    def run(self):
        self.setParams()
        header = self.headerGen()
        commands = self.commandsGen()
        tail = self.tailGen()
        content = header + commands + tail
        self.writeFile(content)


    def headerGen(self):
        result = u'''
USE %s; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.map.tasks.speculative.execution=false;
SET hive.mapred.reduce.tasks.speculative.execution=false;

-- Update Hive metastore for partitions
-- invalid
''' % self.id
        for app in self.apps:
            for i in range(24):
                result += (u"ALTER TABLE raw_invalid_events ADD IF NOT EXISTS PARTITION (app='%s',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=%.2d);\n" % (app, i))

        return result


    def commandsGen(self):
        result = u''
        for event in self.events:
            result += (u'-- %s\n' % event)
            for app in self.apps:
                for i in range(24):
                    result += (u"ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='%s',app='%s',year=${hiveconf:rpt_year},month=${hiveconf:rpt_month},day=${hiveconf:rpt_day},hour=%.2d);\n" % (event, app, i))
                result += u'\n' 
            result += u'-- Add today hour=00 partition\n'
            for app in self.apps:
                result +=(u"ALTER TABLE raw_events ADD IF NOT EXISTS PARTITION (event='%s',app='%s',year=${hiveconf:rpt_year_plus1},month=${hiveconf:rpt_month_plus1},day=${hiveconf:rpt_day_plus1},hour=00);\n" % (event, app))
            result += u'\n'

        return result


    def tailGen(self):
    	result = u'''
-- Insert events to daily table
INSERT OVERWRITE TABLE raw_events_daily PARTITION (event, app, dt)
SELECT
  bi_version, 
  app_id, 
  ts, 
  ts_pretty, 
  user_id, 
  session_id, 
  properties, 
  collections, 
  event, 
  app, 
  '${hiveconf:rpt_date}'
FROM
  raw_events
WHERE
  (
    (year=${hiveconf:rpt_year} AND month=${hiveconf:rpt_month} AND day=${hiveconf:rpt_day})
    OR
    (year=${hiveconf:rpt_year_plus1} AND month=${hiveconf:rpt_month_plus1} AND day=${hiveconf:rpt_day_plus1})
  )
  AND
  to_date(ts_pretty) = '${hiveconf:rpt_date}'
;


exit;
'''
        return result


    def writeFile(self, content):
        fp = open(('%s_raw_events_dml.hql' % self.id), 'w')
        fp.write(content)
        fp.close()


    def setParams(self):
        with open(self.args['file'], 'r') as f:
            json_object = json.load(f)
        self.id = json_object[u'id']
        self.apps = json_object[u'apps']
        self.events = json_object[u'events']
        f.close()


def parse_args():
    parser = argparse.ArgumentParser(description='Events count verification')
    parser.add_argument('-f', '--file', help='path to configuation file')
    args = vars(parser.parse_args())

    return args


def main():
    args = parse_args()
    hg = hiveGen(args)
    hg.run()


if __name__ == '__main__':
    main()

import json
import argparse
import os
import string
import shutil

class tableGen:
    def __init__(self, args):
        self.args = args


    def run(self):
        self.setParams()

        for filename in os.listdir(self.args['tmplpath']):
            if os.path.isfile(self.args['tmplpath'] + filename):
                output_name = self.filenameGen(filename)
                output_content = self.commandsGen(filename)
                self.writeFile(output_name, output_content)


    def filenameGen(self, filename):
        tmpl_name = string.Template(filename)
        result = self.args['tmplpath'] + 'result/' + tmpl_name.safe_substitute(id = self.id)

        return result


    def commandsGen(self, filename):
        fullname = self.args['tmplpath'] + filename
        fp = open(fullname, 'r')
        tmpl_content = string.Template(fp.read())
        fp.close()
        result = tmpl_content.safe_substitute(id = self.id)

        return result


    def writeFile(self, filename, content):
        fp = open(filename, 'w')
        fp.write(content)
        fp.close()


    def setParams(self):
        with open(self.args['file'], 'r') as f:
            json_object = json.load(f)
        self.id = json_object[u'id']
        self.apps = json_object[u'apps']
        f.close()
        if os.path.exists(self.args['tmplpath'] + 'result/'):
            shutil.rmtree(self.args['tmplpath'] + 'result/')
        os.makedirs(self.args['tmplpath'] + 'result/')
 


def parse_args():
    parser = argparse.ArgumentParser(description='Events count verification')
    parser.add_argument('-t', '--tmplpath', help='path to template folder')
    parser.add_argument('-f', '--file', help='path to configuration files')
    args = vars(parser.parse_args())

    return args


def main():
    args = parse_args()
    tg = tableGen(args)
    tg.run()


if __name__ == '__main__':
    main()

use ${id};
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;

SET hive.mapred.map.tasks.speculative.execution=false;
set hive.mapred.reduce.tasks.speculative.execution=false;


SET hive.exec.max.dynamic.partitions=15000;
SET hive.exec.max.dynamic.partitions.pernode=5000;


INSERT OVERWRITE TABLE tmp_user_payment PARTITION (app, dt)
select distinct t.user_key,t.app_id,t.date,t.conversion_ts,t.revenue,t.purchase_cnt,t.app,t.dt
from
(
select
user_key,
app_id,
date,
min(ts) over (partition by user_key,app_id) conversion_ts,
sum(revenue_usd) over (partition by user_key,app_id,date) revenue,
count(ts) over (partition by user_key,app_id,date) purchase_cnt,
app,
dt
from
fact_revenue
)t ;




exit;




----------------------------------
-- Query to generate fact_revenue
----------------------------------
use ${id};

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;
ALTER TABLE currency ADD IF NOT EXISTS PARTITION (date='${hiveconf:rpt_date}') LOCATION "s3://com.funplusgame.bidata/currency/${hiveconf:rpt_date_nohyphen}/";

INSERT OVERWRITE TABLE fact_revenue PARTITION (app,dt)
select
 id,
 app_id,
 app_version,
 user_key,
 app_user_id,
 date,
 ts,
 install_ts,
 install_date,
 session_id,
 level,
 os, 
 os_version,
 device,
 browser,
 browser_version,
 country_code,
 install_source,
 ip,
 language,
 locale,
 ab_experiment,
 ab_variant,
 coin_wallet,
 rc_wallet,
 payment_processor,
 product_id,
 product_name,
 product_type,
 coins_in,
 rc_in,
 currency,
 revenue_currency, 
 revenue_usd,
 transaction_id,
 idfa,
 idfv,
 gaid,
 mac_address,
 android_id,
 app,
 dt
from (
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id,
app_id,
properties['app_version'] app_version,
MD5(concat(app_id,user_id)) as user_key,
user_id as app_user_id,
to_date(ts_pretty) as date,
ts_pretty as ts,
properties['install_ts_pretty'] as install_ts,
to_date(properties['install_ts_pretty']) install_date,
session_id,
properties['level'] as level,
properties['os'] os, 
properties['os_version'] os_version,
properties['device'] device,
properties['browser'] as browser,
properties['browser_version'] as browser_version,
properties['country_code'] country_code,
properties['install_source'] install_source,
properties['ip'] ip,
properties['lang'] language,
properties['locale'] locale,
null as ab_experiment,
null as ab_variant,
null as coin_wallet,
null as rc_wallet,
properties['payment_processor'] payment_processor,
properties['iap_product_id'] product_id,
properties['iap_product_name'] product_name,
properties['iap_product_type'] product_type,
null as coins_in,
null as rc_in,
r.properties['currency'] currency,
properties['amount']/100 revenue_currency, 
properties['amount']*c.factor/100 revenue_usd,
properties['transaction_id'] transaction_id,
properties['idfa'] as idfa,
properties['idfv'] as idfv,
properties['gaid'] as gaid,
properties['mac_address'] mac_address,
properties['android_id'] android_id, 
row_number() over (partition by app_id,user_id,properties['transaction_id'] order by ts_pretty) rank, 
r.app as app,
r.dt
from 
(
select app_id, dt, user_id, ts, event, ts_pretty, session_id, properties, app
from raw_events_daily 
--lateral view outer explode(collections.ab_tests) coll1 AS ab 
WHERE   dt='${hiveconf:rpt_date}' and event='payment'
) r
left outer join 
(SELECT * FROM currency WHERE  dt='${hiveconf:rpt_date}') c on r.properties['currency'] =c.currency and r.dt=c.dt
)t where rank=1 ;





-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;
SET hive.input.format=org.apache.hadoop.hive.ql.io.HiveInputFormat; 

ALTER TABLE copy_fact_revenue DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_revenue partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select id, app_id, app_version, user_key, app_user_id, date, ts,install_ts, install_date, session_id, level
, os, os_version, device, browser, browser_version, country_code, install_source, ip, language, locale
, ab_experiment, ab_variant, coin_wallet, rc_wallet, payment_processor, product_id, product_name, product_type, coins_in, rc_in, currency, revenue_currency
, revenue_usd, transaction_id, idfa, idfv, gaid, mac_address, android_id
from fact_revenue where  dt='${hiveconf:rpt_date}';

exit;



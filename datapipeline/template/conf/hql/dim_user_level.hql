
use ${id};

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;


INSERT OVERWRITE TABLE tmp_level_up PARTITION (app,dt)
select user_key,app_id,app_user_id,max(levelup_ts),max(level),app,dt from
(select
md5(concat(app, user_id)) AS user_key,
app_id,
user_id as app_user_id,
min(ts_pretty) levelup_ts,
properties['level'] as level,
app,
dt
from raw_events_daily where event='level_up' and dt='${hiveconf:rpt_date}'
group by app_id,user_id,properties['level'],md5(concat(app, user_id)),app,dt
)t
group by user_key,app_id,app_user_id,app,dt;

INSERT OVERWRITE TABLE dim_user_level PARTITION (app,dt)
select user_key,app_id,app_user_id,levelup_ts,level,app,'${hiveconf:rpt_date}' as dt from
(
select t.user_key,t.app_id,t.app_user_id,COALESCE(t.levelup_ts,d.levelup_ts) as levelup_ts,coalesce(t.level,d.level) as level,t.app from
(select * from tmp_level_up where dt='${hiveconf:rpt_date}')t
left outer join (select * from dim_user_level where dt='${hiveconf:rpt_date_yesterday}')d on t.user_key=d.user_key and t.app=d.app
UNION ALL
select d.user_key, d.app_id, d.app_user_id, d.levelup_ts, d.level,d.app from (select * from dim_user_level  WHERE dt='${hiveconf:rpt_date_yesterday}')d
LEFT OUTER JOIN tmp_level_up t ON
  (t.app = d.app and t.user_key = d.user_key AND t.dt ='${hiveconf:rpt_date}')
where t.user_key IS NULL
)s
;

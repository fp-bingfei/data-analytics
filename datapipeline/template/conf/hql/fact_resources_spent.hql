use ${id};
SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET hive.exec.compress.intermediate=true;

SET hive.exec.max.dynamic.partitions=50000;
SET hive.exec.max.dynamic.partitions.pernode=5000;
SET hive.metastore.batch.retrieve.max=10000;
SET hive.metastore.batch.retrieve.table.partition.max=10000;

SET hive.exec.parallel=true;
SET mapreduce.map.speculative=false;
SET mapreduce.reduce.speculative=false;

set hive.vectorized.execution.enabled = true;
set hive.vectorized.execution.reduce.enabled = true;


SET hive.stats.autogather=false;
SET hive.stats.collect.rawdatasize=false;
SET hive.stats.fetch.partition.stats=false;



INSERT OVERWRITE TABLE fact_resources_spent PARTITION (app,dt)
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, transaction_type as action_type
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts,dt, event, ts_pretty, properties['transaction_type'] as transaction_type, properties['level'] as level
, pr['resource_id'] as resource_id, pr['resource_name'] as resource_name, pr['resource_type'] as resource_type, pr['resource_amount'] as resource_amount
FROM
raw_events_daily lateral view outer explode(collections['resources_spent']) coll AS pr
WHERE dt ='${hiveconf:rpt_date}' and event='transaction'
) t
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, timer_type as action_type
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['timer_type'] as timer_type, properties['level'] as level
, pr['resource_id'] as resource_id, pr['resource_name'] as resource_name, pr['resource_type'] as resource_type, pr['resource_amount'] as resource_amount
FROM
raw_events_daily lateral view outer explode(collections['resources_spent']) coll AS pr
WHERE dt ='${hiveconf:rpt_date}' and event='timer'
) m
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, mission_type as action_type
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['mission_type'] as mission_type, properties['level'] as level
, pr['resource_id'] as resource_id, pr['resource_name'] as resource_name, pr['resource_type'] as resource_type, pr['resource_amount'] as resource_amount
FROM
raw_events_daily lateral view outer explode(collections['resources_spent']) coll AS pr
WHERE dt ='${hiveconf:rpt_date}' and event='mission'
) n
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, null as action_type
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['level'] as level
, pr['resource_id'] as resource_id, pr['resource_name'] as resource_name, pr['resource_type'] as resource_type, pr['resource_amount'] as resource_amount
FROM
raw_events_daily lateral view outer explode(collections['resources_spent']) coll AS pr
WHERE dt ='${hiveconf:rpt_date}' and event='gift_received'
) g
UNION ALL
SELECT
MD5(concat(app_id, event, user_id, session_id,  ts)) as id
, app_id
, MD5(concat(app_id,user_id)) as user_key
, level
, event
, to_date(ts_pretty)
, ts_pretty
, action_type
, resource_id
, resource_name
, resource_type
, resource_amount
, app
, dt
FROM
(SELECT app, app_id, user_id, session_id, ts, dt, event, ts_pretty, properties['action_type'] as action_type, properties['level'] as level
, pr['resource_id'] as resource_id, pr['resource_name'] as resource_name, pr['resource_type'] as resource_type, pr['resource_amount'] as resource_amount
FROM
raw_events_daily lateral view outer explode(collections['resources_spent']) coll AS pr
WHERE dt ='${hiveconf:rpt_date}' and event='item_actioned'
) i
;

-- Parquet to Text File - To enable copy to Redshift --
SET hive.exec.compress.output=true;
SET mapred.output.compression.codec=org.apache.hadoop.io.compress.GzipCodec;


ALTER TABLE copy_fact_resources_spent DROP IF EXISTS PARTITION (app='${hiveconf:rpt_all_app}');

insert overwrite table copy_fact_resources_spent partition(app='${hiveconf:rpt_all_app}',dt='${hiveconf:rpt_date}')
select 
id
, app_id
, user_key
, level
, event
, date
, ts
, action_type
, resource_spent_id
, resource_spent_name
, resource_spent_type
, resource_spent_amount
from fact_resources_spent where dt ='${hiveconf:rpt_date}';

exit;

SET mapred.map.tasks 100;

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/json-simple-1.1.1.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/elephant-bird-pig-4.4.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/elephant-bird-core-4.4.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/google-collections-1.0.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/elephant-bird-hadoop-compat-4.4.jar';

X1 = LOAD 's3://com.funplusgame.bidata/fluentd/farm.fr.prod/2014/07/{24,25}/{[0-2]*}/archi*' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad');

X2 = FILTER X1 BY ($0#'event' == 'item_transaction' AND $0#'item_class' == 'c' AND $0#'item_in' > 1 AND $0#'item_name' matches '.*Ferti.*');

X3 = FOREACH X2 GENERATE $0#'snsid' as snsid, $0#'@ts' as ts, $0#'item_id' as item_id, $0#'item_name' as item_name, $0#'item_in' as item_in, $0#'item_out' as item_out, $0#'item_class' as item_class, $0#'action' as action, $0#'rc_out' as rc_out, $0#'coins_out' as coins_out, $0#'rc_bal' as rc_bal, $0#'coins_bal' as coins_bal, $0#'event' as event_name;

X4 = ORDER X3 BY snsid, ts, item_id;

STORE X4 INTO 's3://com.funplusgame.emr/results/items/May2014/itemsout_c1.gz';



SET mapred.map.tasks 100;

--SET default_parallel 20;

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/json-simple-1.1.1.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/elephant-bird-pig-4.4.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/elephant-bird-core-4.4.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/google-collections-1.0.jar';

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/elephant-bird-hadoop-compat-4.4.jar';

--REGISTER 's3://com.funplusgame.emr/results/scripts/jars/myudfs.jar';

X4 = LOAD 's3://com.funplusgame.emr/results/rc_finance/nl_rc_2014P1.gz/part*' USING com.twitter.elephantbird.pig.load.JsonLoader('-nestedLoad');

X5 = FOREACH X4 GENERATE $0#'geo' as locale, $0#'ts' as ts, $0#'snsid' as snsid, $0#'uid', $0#'addtime', (($0#'rc_get' IS NULL) ? 0 : $0#'rc_get'), 
        (($0#'rc_cost' IS NULL) ? 0 : $0#'rc_cost'), (($0#'rc_left' IS NULL) ? 0 : $0#'rc_left'), (($0#'amount' IS NULL) ? 0 : $0#'amount'), $0#'event_name';

X6 = ORDER X5 BY $2, $1;

STORE X6 INTO 's3://com.funplusgame.emr/results/rc_finance/nl_rc_data_2014.gz';



SET mapred.map.tasks 100;

--SET default_parallel 20;

REGISTER 's3://com.funplusgame.emr/results/scripts/jars/myudfs.jar';

X1 = LOAD 's3://com.funplusgame.bidata/farm/de/prod/2014*' USING PigStorage('|') AS (eRec:chararray);

X2 = FILTER X1 BY (eRec matches '.*rc_transaction.*') OR (eRec matches'.*Payment.*');

X3 = FOREACH X2 GENERATE myudfs.CreateJsonString($0);

STORE X3 INTO 's3://com.funplusgame.emr/results/rc_finance/de_rc_2014P1.gz';


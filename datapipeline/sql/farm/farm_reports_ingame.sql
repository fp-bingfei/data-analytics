------------------------------------------------transaction---------------------------------------------------------------------------------
DELETE FROM rc_transaction WHERE trunc(ts) = ?::DATE;
insert into rc_transaction
(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,rc_in,rc_out,rc_bal,level,
location,action,action_detail)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end 
as integer) as "rc_in" ,
cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') 
end as integer) as 
"rc_out" ,
cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') 
end as integer) as 
"rc_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail from events where event='rc_transaction' and trunc(ts) = ?::DATE;


DELETE FROM rpt_rc WHERE dt = ?::DATE;
insert into rpt_rc
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(rc_in) ,sum(rc_out) ,sum(rc_bal) ,
 level, location,action, action_detail from rc_transaction 
 WHERE trunc(ts) = ?::DATE
group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version, level, 
location,action, 
action_detail;



delete from coin_transaction where trunc(ts) = ?::DATE;
insert into coin_transaction
(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,coins_in,coins_out,
coins_bal,level,location,action,action_detail)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text
(properties,'coins_in') end as bigint) as 
"coins_in" ,
cast(case when json_extract_path_text(properties,'coins_out')='' then '0' else json_extract_path_text
(properties,'coins_out') end as bigint) as 
"coins_out" ,
cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text
(properties,'coins_bal') end as bigint) as 
"coins_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail from events where event='coins_transaction' AND trunc(ts) = ?::DATE;



delete from rpt_coin where dt = ?::DATE;
insert into rpt_coin
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(coins_in) ,sum(coins_out) ,sum(coins_bal) ,
 level,location,action, action_detail from coin_transaction where trunc(ts) = ?::DATE
group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version, level, 
location,action, action_detail;





delete from quest_completion where trunc(ts) = ?::DATE;
insert into quest_completion
(uid,snsid,app,ts,install_source,os,browser,install_dt,browser_version,country_code,os_version,coins_in,coins_out,
coins_bal,rc_in,rc_out,rc_bal,quest_id,lang,level,action,task_id,quest_type)
select e.uid,e.snsid,e.app,e.ts,e.install_source,e.os,e.browser,trunc(e.install_ts), e.browser_version 
,e.country_code,e.os_version,
cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text
(properties,'coins_in') end as float) as 
"coins_in" ,
cast(case when json_extract_path_text(properties,'coins_out')='' then '0' else json_extract_path_text
(properties,'coins_out') end as float) as 
"coins_out" ,
cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text
(properties,'coins_bal') end as float) as 
"coins_bal",
cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end 
as integer) as 
"rc_in",
cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') 
end as integer) as 
"rc_out",
cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') 
end as integer) as 
"rc_bal",
json_extract_path_text(properties, 'quest_id') as "quest_id",
json_extract_path_text(properties, 'lang') as "lang",
json_extract_path_text(properties, 'level') as "level",
json_extract_path_text(properties, 'action') as "action",
json_extract_path_text(properties, 'task_id') as "task_id",
json_extract_path_text(properties, 'quest_type') as "quest_type"
from events e where e.event='quest'
AND trunc(ts) = ?::DATE;




delete from rpt_tutorial where max_dt = ?::DATE;

create temp table temp_rpt_tutorial
as 
SELECT t.user_id,
       t.snsid ,
       t.uid,
       t.max_dt ,
       t.step,
       t.app ,
       t.t_snsid,
       t.install_source,
       t.os,
       t.install_dt,
       t.country_code,
       t.browser,
       c.country
FROM 
  (SELECT a.snsid,
          a.user_id,
          a.uid,
          e.snsid AS t_snsid,
          trunc(e.max_ts) AS max_dt,
          a.country_code,
          trunc(a.install_ts) AS install_dt,
          a.os,
          a.install_source,
          a.app,
          a.browser,
          e.step
   FROM app_user a
   LEFT JOIN 
      (select app,
             uid,
             snsid,
             max(ts) as max_ts,
             json_extract_path_text(properties,'step') as step
      from   events
      where  event = 'tutorial'
      AND trunc(ts) = ?::DATE 
      group by 1,2,3,5
      )e
   on a.app=e.app
   and a.uid=e.uid
   and a.snsid=e.snsid
  )t 
LEFT JOIN country c ON t.country_code=c.country_code;

delete from rpt_tutorial where (user_id,step) in (select user_id, step from temp_rpt_tutorial);


insert into rpt_tutorial
select user_id,snsid,uid,max_dt,step,app,t_snsid,install_source,os,install_dt,country_code,browser,country from 
temp_rpt_tutorial;

delete from item_transaction where trunc(ts) = ?::DATE;
insert into item_transaction
(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,item_in,item_out,item_bal,
level,location,action,action_detail,item_name,item_class,item_type,item_id)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') 
end as float) as 
"item_in" ,
cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text
(properties,'item_out') end as float) as 
"item_out" ,
cast(case when json_extract_path_text(properties,'item_bal')='' then '0' else json_extract_path_text
(properties,'item_bal') end as float) as 
"item_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail,
json_extract_path_text(properties,'item_name') as item_name,
json_extract_path_text(properties,'item_class') as item_class,
json_extract_path_text(properties,'item_type') as item_type,
json_extract_path_text(properties,'item_id') as item_id
 from events where event='item_transaction' 
 and ts >=?::DATE
 and ts< dateadd('hour',12,?::DATE);


insert into item_transaction
(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,item_in,item_out,item_bal,
level,location,action,action_detail,item_name,item_class,item_type,item_id)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') 
end as float) as 
"item_in" ,
cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text
(properties,'item_out') end as float) as 
"item_out" ,
cast(case when json_extract_path_text(properties,'item_bal')='' then '0' else json_extract_path_text
(properties,'item_bal') end as float) as 
"item_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail,
json_extract_path_text(properties,'item_name') as item_name,
json_extract_path_text(properties,'item_class') as item_class,
json_extract_path_text(properties,'item_type') as item_type,
json_extract_path_text(properties,'item_id') as item_id
 from events where event='item_transaction' 
 and ts>=  dateadd('hour',12,?::DATE)
 and ts<  dateadd('hour',24,?::DATE);





delete from rpt_item where dt = ?::DATE;
insert into rpt_item
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(item_in) ,sum(item_out) ,sum(item_bal) ,level, location,action,action_detail,item_name,item_class,item_type,item_id
 from item_transaction 
 where trunc(ts) = ?::DATE
 group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
 level, location,action,action_detail,item_name,item_class,item_type,item_id;
 


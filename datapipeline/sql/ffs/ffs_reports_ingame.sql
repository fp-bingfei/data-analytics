delete from coin_transaction where trunc(ts)=?::DATE;

insert into coin_transaction
(user_id,app,ts,uid,snsid,install_dt,install_source,country_code,device,os,os_version,coins_in,coins_out,
coins_bal,rc_bal,level,action,location,game_version)
select md5(a.app||e.uid||e.snsid),
a.app,
e.ts,
e.uid,
e.snsid,
trunc(a.install_ts) as install_dt,
substring(a.install_source, 1, 100),
a.country_code,
a.device,
a.os,
a.os_version,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_in')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_in') 
end as BIGINT) as coins_in,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_out')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_out')
end as BIGINT) as coins_out,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left') 
end as BIGINT) as coins_bal ,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left') 
end as BIGINT) as rc_bal,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'level') as level,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'action') as action,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'location') as location,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'GameVersion') as game_version from events_raw e 
left join app_user a on  e.snsid=a.snsid and e.uid=a.uid where e.event='coins_transaction'and trunc(e.ts)=?::DATE;

delete from rpt_coin where dt=?::DATE;

insert into rpt_coin(app,dt,install_dt,os,os_version,device,country_code,coins_in,coins_out,coins_bal,install_source,level,location,action)
select app,
trunc(ts) as dt,
install_dt,
os,
os_version,
device,
country_code,
sum(coins_in),
sum(coins_out),
sum(coins_bal),
install_source,
level,
location,
action from coin_transaction
where trunc(ts)=?::DATE
group by app,trunc(ts),install_dt,
os,os_version,device,country_code,install_source,level,location,action;



delete from rc_transaction where trunc(ts)=?::DATE;

insert into rc_transaction
(user_id,app,ts,uid,snsid,install_dt,install_source,country_code,device,os,os_version,rc_in,rc_out,
rc_bal,coins_bal,level,action,location,game_version)
select md5(a.app||e.uid||e.snsid),
a.app,
e.ts,
e.uid,
e.snsid,
trunc(a.install_ts) as install_dt,
substring(a.install_source, 1, 100),
a.country_code,
a.device,
a.os,
a.os_version,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_in')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_in') 
end as integer) as rc_in,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_out')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_out') end as integer) as rc_out,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left') 
end as integer) as rc_bal ,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left') 
end as integer) as coins_bal,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'level') as level,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'action') as action,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'location') as location,
json_extract_path_text(regexp_replace(properties,'\\\\.',''), 'GameVersion') as game_version from events_raw e 
left join app_user a on  e.snsid=a.snsid and e.uid=a.uid where e.event='rc_transaction'and trunc(e.ts)=?::DATE;

delete from rpt_rc where dt=?::DATE;

insert into rpt_rc(app,dt,install_dt,os,os_version,device,country_code,rc_in,rc_out,rc_bal,install_source,level,location,action)
select app,
trunc(ts) as dt,
install_dt,
os,
os_version,
device,
country_code,
sum(rc_in),
sum(rc_out),
sum(rc_bal),
install_source,
level,
location,
action from rc_transaction
where trunc(ts)=?::DATE
group by app,trunc(ts),install_dt,
os,os_version,device,country_code,install_source,level,location,action;

delete from quest_completion where trunc(ts)=?::DATE;

insert into quest_completion(user_id,app,uid,snsid,install_source,install_dt,ts,os,os_version,device,quest_id,task_id,quest_state,quest_type,coins_bal,rc_bal,rc_out,game_version)
select md5(a.app||e.uid||e.snsid) as user_id,
a.app,
e.uid,
e.snsid,
substring(a.install_source, 1, 100),
trunc(a.install_ts),
e.ts,
a.os,
a.os_version,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') as device,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'quest_ID')  as quest_id,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'task_id')  as task_id,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'quest_state') as quest_state,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'quest_type') as quest_type,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left') end as float) as coins_bal ,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left') end as float) as rc_bal,
cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'RC_spend')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'RC_spend') end as integer) as rc_out,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'GameVersion') as game_version from events_raw e
left join app_user a on e.uid=a.uid and e.snsid=a.snsid where event='QuestComplete' and trunc(e.ts)=?::DATE;



delete from item_transaction where trunc(ts)=?::DATE;


INSERT INTO item_transaction(user_id,app,uid,snsid,install_dt,ts,install_source,country_code,os,os_version,device,item_in,item_out,rc_bal,coins_bal,coins_out,rc_out,level,op_type,item_id,location,game_version)
SELECT md5(e.app||e.uid||e.snsid) as user_id,
       a.app,
       e.uid,e.snsid,
       trunc(a.install_ts),
       e.ts,
       substring(a.install_source, 1, 100),
       a.country_code,
       a.os,
       a.os_version,
       json_extract_path_text(properties,'device') as device,
       CASE WHEN event='ItemPurchased' then 1 else 0 end AS item_in,
       case when event='ItemDrop' then 1 else 0 end AS item_out,

       CAST(CASE WHEN json_extract_path_text (properties,'rc_left') = '' THEN '0' ELSE json_extract_path_text (properties,'rc_left') END AS int) AS rc_bal,
       CAST(CASE WHEN json_extract_path_text (properties,'coins_left') = '' THEN '0' ELSE json_extract_path_text (properties,'coins_left') END AS int) AS coins_bal,
       CAST(CASE WHEN json_extract_path_text (properties,'coins_cost') = '' THEN '0' ELSE json_extract_path_text (properties,'coins_cost') END AS int) AS coins_out,
       CAST(CASE WHEN json_extract_path_text (properties,'RC_spend') = '' THEN '0' ELSE json_extract_path_text (properties,'RC_spend') END AS int) AS rc_out,
       json_extract_path_text(properties,'level') AS level,
       json_extract_path_text(properties,'op_type') AS op_type,
       json_extract_path_text(properties,'item_ID') AS item_id,
       json_extract_path_text(properties,'from') AS location,
       json_extract_path_text(properties,'GameVersion') AS game_version
FROM events_raw e
  LEFT JOIN app_user a
         ON e.snsid = a.snsid and e.uid=a.uid
WHERE event= 'ItemDrop' or event='ItemPurchased'
AND   trunc(e.ts)=?::DATE;

delete from rpt_item where dt=?::DATE;

INSERT INTO rpt_item(app,dt,install_dt,install_source,country_code,os,device,item_in,item_out,coins_out,rc_out,level,location,item_id)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       device,
       SUM(item_in),
       SUM(item_out),
       sum(coins_out),
       sum(rc_out),
       level,
       location,
       item_id
FROM item_transaction
WHERE trunc(ts)=?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         device,
         level,
         location,
         item_id;




CREATE TEMP TABLE USER_COUNTRY
as
select app
       ,uid
       ,snsid
       ,install_ts
       ,country_code

from(
SELECT           uid
                ,snsid
                ,app
                ,install_ts
                ,country_code
                ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events_raw
where event <> 'payment'
and   install_ts is not null and
trunc(ts) =?::DATE
)t
where rank = 1;

--Get payer,conversion_ts,and last_login_ts info

CREATE TEMP TABLE payer_user_info
as
select uid,
       snsid,
       app,
       install_ts,
       ts as conversion_ts
FROM events_raw 
where event = 'payment' and json_extract_path_text(regexp_replace(properties,'\\\\.',''),'pay_times')='1' 
and install_ts is not null
and   trunc(ts)=?::DATE;



--Get other user info for app users
truncate table app_user_temp;

insert into app_user_temp 
(app,uid,snsid,install_ts,install_source,os,os_version,country_code,level,language,device,latest_login_ts)
select t.app,
       t.uid,
       t.snsid,
       t.install_ts,
       t.install_source,
       t.os,
       t.os_version,
       t.country_code,
       t.level,
       t.language,
       t.device,
       t.latest_login_ts
from  
(select app,
       uid,
       snsid,
       install_source,
       install_ts,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       latest_login_ts,
      row_number() over(partition by app,uid,snsid,install_ts order by latest_login_ts desc ) as rank

from
       (select  t1.uid,
                t1.app,
              t1.snsid,  --sortkey
               CASE WHEN t1.install_source ='"Facebook Organic"' THEN 'Facebook Organic' 
                   when t1.install_source ='"Google Adwords"' THEN 'Google Adwords' 
                   when t1.install_source ='"App Turbo"' THEN 'App Turbo'
                   when t1.install_source like 'Facebook%' THEN 'Facebook' 
                   when t1.install_source  like 'Off-Facebook%' THEN 'Off-Facebook Installs'
                   when t1.install_source='organic' THEN 'Organic' 
                   ELSE t1.install_source end as install_source,
              t1.install_ts,  --diskey
              CASE WHEN t1.os ='ffs.global.android' THEN 'android' 
                   when t1.os ='ffs.global.iOS' THEN 'iOS'
                   when t1.os ='ffs.global.amazon' THEN 'amazon'
                   when t1.os ='ffs.global.tango' THEN 'tango'
                   when t1.os ='ffs.thai.iOS' THEN 'iOS'
                   when t1.os ='ffs.thai.android' THEN 'android'
                   when t1.os ='ffs.thai.wp' THEN 'windows phone'
                   ELSE 'Unknown' end as os, 
              t1.os_version,
              max(CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') = '' THEN '0' ELSE 
json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') END AS integer)) AS level,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'language') as language,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') as device ,
              t1.ts as latest_login_ts,
              t2.country_code
from  events_raw t1 left join USER_COUNTRY t2
on t1.snsid = t2.snsid
and t1.uid = t2.uid
where trunc(ts) =?::DATE and t1.install_ts is not null 
group by 1,2,3,4,5,6,7,9,10,11,12
)
)t where t.rank=1;


update app_user_temp set conversion_ts=app_user.conversion_ts from app_user where app_user_temp.uid=app_user.uid and 
app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;

update app_user_temp set conversion_ts=payer_user_info.conversion_ts from payer_user_info where 
app_user_temp.uid=payer_user_info.uid and app_user_temp.snsid=payer_user_info.snsid 
and app_user_temp.conversion_ts is null;

update app_user_temp set install_source = app_user.install_source from app_user where app_user_temp.uid=app_user.uid and
app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;

--delete the old data of app user
delete from app_user where (app,uid,snsid) in (select app,uid,snsid from app_user_temp);

insert into app_user
(
       user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       conversion_ts,
       latest_login_ts
)
select 
t.user_id,t.app,t.uid,t.snsid,t.install_ts,t.install_source,t.os,t.os_version,t.country_code,t.level,t.language,t.device,t
.conversion_ts,t.latest_login_ts from
(
select md5(app||uid||snsid) as user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       conversion_ts,
       latest_login_ts
from app_user_temp)t ;

--update the payer flag
update app_user set is_payer = true where conversion_ts is not null;


------update install source------------------------------
Update app_user set install_source = 'Organic' where install_source = '';

update app_user set install_source = d.tracker_name
from adjust d
where app_user.snsid = d.userid
and app_user.install_source = 'Organic'
and app_user.app=d.game
and trunc(d.ts) = ?::DATE and d.tracker_name <> 'Organic';
 
update ffs.public.app_user
set os = u.os
from ffs.processed.dim_user u
where ffs.public.app_user.app = u.app and ffs.public.app_user.uid = u.uid;

update ffs.public.app_user
set os = 'Android'
where os = 'android';

delete from dau where dt =?::DATE;

INSERT INTO dau(user_id,uid,snsid,level,dt,app,install_source,country_code,os,os_version,install_dt,device)
select t.user_id,t.uid,t.snsid,t.level,t.dt,t.app,t.install_source,t.country_code,t.os,t.os_version,t.install_dt,t.device 
from
(
SELECT md5(a.app||e.uid||e.snsid) as user_id,e.uid,e.snsid,
       max(CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') = '' THEN '0' ELSE 
json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') END AS integer)) AS level,
       trunc(e.ts) AS dt,
       a.app,
       a.install_source,
       a.country_code,
       a.os,a.os_version,
       trunc(a.install_ts) AS install_dt,
       a.device,
       row_number() over(partition by a.app,e.uid,e.snsid order by dt) as rank
FROM events_raw e
  INNER JOIN app_user a
          ON e.snsid = a.snsid and e.uid=a.uid
WHERE trunc(e.ts) =?::DATE and e.install_ts is not null
GROUP BY e.uid,e.snsid,
         dt,
         install_dt,
         a.device,
         a.country_code,
         a.install_source,
         a.os,a.os_version,
         a.app)t where t.rank=1;

update ffs.public.dau
set os = u.os
from ffs.processed.dim_user u
where ffs.public.dau.app = u.app and ffs.public.dau.uid = u.uid;

update ffs.public.dau
set os = 'Android'
where os = 'android';

-------------------------------------------------
------Payment--------------------------------------------------------------------------------------        

delete from payment where trunc(ts)=?::DATE;

INSERT INTO payment
(user_id,uid,snsid,app,ts,install_dt,install_source,os,os_version,country_code,product_id,product_name,product_type,lang,currency,cash_type,device,level,payment_processor,amount,coins_in,
rc_in,transaction_id,reward_package,rc_bal,coins_bal,pay_times,game_version,is_gift)
SELECT md5(e.app||e.uid||e.snsid),
       e.uid,
       e.snsid,
       e.app,
       e.ts,
       e.install_dt,
       e.install_source,
       e.os,
       e.os_version,
       e.country_code,
       product_id,
       product_name,
       product_type,
       lang,
       currency,
       cash_type,
       e.device,
       e.level,
       e.payment_processor,
       e.amount,
       e.coins_in,
       e.rc_in,
       e.transaction_id,
       e.reward_package,
       e.rc_bal,
       e.coins_bal,
       e.pay_times,
       e.game_version,
       e.is_gift

FROM
     (
       select r.uid,
              r.snsid,
              a.app,
              r.ts,a.install_source,a.os,a.os_version,a.country_code,
              trunc(a.install_ts) as install_dt,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_id') as product_id,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_name') as product_name,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'lang') as lang,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'currency') as currency,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'cash_type') as cash_type,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') as device,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'game_version') as game_version,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'is_gift') as is_gift,                          
 
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') end as integer) as level,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'payment_processor') as payment_processor,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'amount')::float/100::float * case when c.factor is null then 1 else c.factor end AS amount,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_in')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_in') end as integer) as coins_in,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_type') as product_type,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_in')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_in') end as integer) as rc_in,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_left') end as integer) as rc_bal,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_left') end as bigint) as coins_bal,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'pay_times')='' then '0' else 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'pay_times') end as integer) as pay_times,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'transaction_id') as transaction_id,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'reward_package') as reward_package,
              row_number() OVER (partition by a.app,r.uid,r.snsid,a.install_ts,r.ts order by transaction_id ) as rank

       from events_raw r
       left join app_user a on  r.snsid=a.snsid and r.uid=a.uid  
       left join currency c on json_extract_path_text(properties,'currency') = c.currency and trunc(r.ts)=c.dt              
       where trunc(r.ts) =?::DATE and event = 'payment'
       )e where e.rank=1

;


---------------------------------------------------kpi 
----------raw--------------------------------------------------------------------------------------
truncate table kpi_raw;


----login cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,login_cnt,event)
SELECT a.app,
       trunc(e.ts),
       trunc(a.install_ts),
       a.install_source,
       a.os,
       a.country_code,
       count(1) AS login_cnt,
       'login'
FROM events_raw e left join app_user a on e.snsid=a.snsid and e.uid=a.uid
WHERE e.event='login'
  AND trunc(e.ts) =?::DATE
GROUP BY 1,2,3,4,5,6;

--dau cnt

INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,dau_cnt,event)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       country_code,
       count(distinct user_id) AS dau_cnt,
       'dau'
FROM dau
where dt =?::DATE
GROUP BY 1,2,3,4,5,6;

--newuser_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,newuser_cnt,event)
SELECT app,
       trunc(install_ts),
       trunc(install_ts),
       install_source,
       os,

       country_code,
       count(distinct user_id) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) =?::DATE
GROUP BY 1,2,3,4,5,6;


--payer_dau_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,payer_dau_cnt,event)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       count(d.USER_ID) AS payer_dau_cnt,
       'payer_dau'
FROM dau d
INNER JOIN app_user a ON d.user_id = a.user_id
and a.is_payer is true
WHERE d.dt=?::DATE
GROUP BY 1,2,3,4,5,6;

--newpayer_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,newpayer_cnt,event)
SELECT app,
       trunc(conversion_ts),
       trunc(install_ts),
       install_source,
       os,
       country_code,
       count(distinct user_id) AS newpayer_cnt,
       'newpayers'
FROM app_user
WHERE trunc(conversion_ts) =?::DATE
and is_payer is true
group by 1,2,3,4,5,6
;


--revenue
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,amount,event)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       os,
       country_code,
       sum(amount) as amount,
       'payment'
from   payment
where  trunc(ts) =?::DATE
GROUP by 1,2,3,4,5,6;

---------------------------------------------------
--------------------kpi----------------------------------------------------------------------------------------

delete from kpi_daily where dt =?::DATE;

INSERT INTO kpi_daily
(app,dt,install_date,install_source,os,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_raw
where dt =?::DATE
GROUP BY 1,2,3,4,5,6;

update ffs.public.kpi_daily
set install_source_truncated = split_part(install_source, '::', 1);

update ffs.public.kpi_daily
set install_source_truncated = replace(install_source_truncated, '\"', '');

update ffs.public.kpi_daily
set install_source_truncated = t.install_source
from ffs.processed.ref_install_source_map t
where ffs.public.kpi_daily.install_source_truncated = t.install_source_raw;

update ffs.public.kpi_daily
set os = 'Android'
where os = 'android';

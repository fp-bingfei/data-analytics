UPDATE app_user
   SET install_source = u.track_ref
FROM tbl_user_global u
WHERE app_user.snsid = u.snsid
AND   app_user.uid = u.uid
AND   app_user.app = 'ffs-global';

DELETE
FROM events_agg
WHERE dt = ?::DATE;

INSERT INTO events_agg
SELECT nvl(md5(e.name||trunc (e.ts) ||e.app||e.properties||u.os||u.install_source||trunc (u.install_ts) ||u.country_code),'error_id'),
       e.name,
       trunc(e.ts) AS dt,
       e.app,
       e.properties,
       u.os,
       u.install_source,
       trunc(u.install_ts) AS install_date,
       u.country_code,
       COUNT(1)
FROM events e
  LEFT JOIN app_user u
         ON e.app = u.app
        AND e.uid = u.snsid
WHERE trunc(e.ts) = ?::DATE
GROUP BY e.name,
         dt,
         e.app,
         e.properties,
         u.os,
         u.install_source,
         install_date,
         u.country_code;

DELETE
FROM dau
WHERE dt = ?::DATE;

--dau
INSERT INTO dau
(
  app,
  dt,
  os,
  level,
  country_code,
  snsid,
  os_version,
  install_source,
  install_dt,
  device
)
SELECT e.app,
       trunc(e.ts),
       a.os,
       max(CAST(json_extract_path_text (properties,'level') AS integer)) AS level,
       a.country_code,
       e.uid,
       a.os_version,
       a.install_source,
       trunc(a.install_ts),
       a.device
FROM events e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.uid = a.snsid
WHERE trunc(e.ts) = ?::DATE
GROUP BY e.uid,
         a.install_source,
         a.country_code,
         a.os_version,
         a.device,
         e.app,
         trunc(a.install_ts),
         trunc(e.ts),
         a.os;

DELETE
FROM payment
WHERE trunc(ts) = ?::DATE;

--payment
INSERT INTO payment
(
  app,
  install_ts,
  ts,
  snsid,
  os,
  level,
  country_code,
  os_version,
  install_source,
  device,
  amount,
  pay_type,
  item_name,
  currency,
  cash_type,
  pay_times,
  product_id,
  reward_package,
  transaction_id
)
SELECT e.app,
       a.install_ts,
       e.ts,
       e.uid AS snsid,
       a.os,
       CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text (properties,'level') END AS integer) AS level,
       a.country_code,
       a.os_version,
       a.install_source,
       a.device,
       json_extract_path_text(properties,'amount')::float/ 100 AS amount,
       json_extract_path_text(properties,'pay_type') AS pay_type,
       json_extract_path_text(properties,'item_name') AS item_name,
       json_extract_path_text(properties,'currency') AS currency,
       json_extract_path_text(properties,'cash_type') AS cash_type,
       json_extract_path_text(properties,'pay_times') AS pay_times,
       json_extract_path_text(properties,'product_id') AS product_id,
       json_extract_path_text(properties,'reward_package') AS reward_package,
       json_extract_path_text(properties,'transaction_id') AS transaction_id
FROM events e
  JOIN app_user a
    ON e.app = a.app
   AND e.uid = a.snsid
WHERE name = 'payment'
AND   trunc(e.ts) = ?::DATE;

DELETE
FROM user_payment_stats
WHERE trunc(ts) = ?::DATE;

INSERT INTO user_payment_stats
(
  snsid,
  ts,
  install_dt,
  country_code,
  os,
  app,
  install_source,
  device,
  os_version
)
SELECT snsid,
       ts,
       trunc(install_ts),
       country_code,
       os,
       app,
       install_source,
       device,
       os_version
FROM payment
WHERE pay_times = '1'
AND   trunc(ts) = ?::DATE;

TRUNCATE TABLE kpi_raw;

--login_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  login_cnt,
  event
)
SELECT u.app,
       trunc(e.ts),
       trunc(u.install_ts),
       u.install_source,
       u.os,
       u.country_code,
       COUNT(1) AS login_cnt,
       'login'
FROM events e
  JOIN app_user u
    ON e.app = u.app
   AND e.uid = u.snsid
WHERE e.name = 'login'
AND   trunc(e.ts) = ?::DATE
GROUP BY u.app,
         trunc(e.ts),
         trunc(u.install_ts),
         u.install_source,
         u.os,
         u.country_code;

--dau_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  dau_cnt,
  event
)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       COUNT(snsid) AS dau_cnt,
       'dau'
FROM dau d
WHERE d.dt = ?::DATE
GROUP BY d.app,
         d.dt,
         d.install_dt,
         d.install_source,
         d.os,
         d.country_code;

--newuser_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  newuser_cnt,
  event
)
SELECT u.app,
       trunc(u.install_ts),
       trunc(u.install_ts),
       u.install_source,
       u.os,
       u.country_code,
       COUNT(1) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY u.app,
         trunc(u.install_ts),
         trunc(u.install_ts),
         u.install_source,
         u.os,
         u.country_code;

--revenue
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  amount,
  event
)
SELECT p.app,
       trunc(p.ts),
       trunc(p.install_ts),
       p.install_source,
       p.os,
       p.country_code,
       SUM(amount) AS amount,
       'payment'
FROM payment p
WHERE trunc(p.ts) = ?::DATE
GROUP BY p.app,
         trunc(p.ts),
         trunc(p.install_ts),
         p.install_source,
         p.os,
         p.country_code;

--newpayer
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  newpayer_cnt,
  event
)
SELECT p.app,
       trunc(p.ts),
       trunc(p.install_ts),
       p.install_source,
       p.os,
       p.country_code,
       COUNT(snsid) AS newpayer_cnt,
       'newpayers'
FROM payment p
WHERE p.pay_times = '1'
AND   trunc(p.ts) = ?::DATE
GROUP BY p.app,
         trunc(p.ts),
         trunc(p.install_ts),
         p.install_source,
         p.os,
         p.country_code;

--payer_dau_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  payer_dau_cnt,
  event
)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       COUNT(d.snsid) AS payer_dau_cnt,
       'payer_dau'
FROM dau d
  INNER JOIN user_payment_stats t
          ON d.snsid = t.snsid
         AND d.os = t.os
         AND d.install_source = t.install_source
         AND d.app = t.app
         AND d.install_dt = t.install_dt
         AND d.country_code = t.country_code
         AND d.device = t.device
         AND d.os_version = t.os_version
WHERE d.dt = ?::DATE
GROUP BY d.os,
         d.country_code,
         d.app,
         d.install_source,
         d.dt,
         d.install_dt;

DELETE
FROM kpi_raw
WHERE app != 'ffs-global';

DELETE
FROM kpi_daily
WHERE dt = ?::DATE;

INSERT INTO kpi_daily
(
  app,
  dt,
  install_date,
  install_source,
  os,
  country_code,
  amount,
  login_cnt,
  dau_cnt,
  newuser_cnt,
  newpayer_cnt,
  payer_dau_cnt
)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       country_code,
       SUM(amount),
       SUM(login_cnt),
       SUM(dau_cnt),
       SUM(newuser_cnt),
       SUM(newpayer_cnt),
       SUM(payer_dau_cnt)
FROM kpi_raw
GROUP BY app,
         dt,
         install_dt,
         install_source,
         os,
         country_code;

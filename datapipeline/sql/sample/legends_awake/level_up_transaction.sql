with
/*level up info*/
l as (
select
a.*
from
(select
user_id,
ts_pretty,
json_extract_path_text(d_c1,'value') as action,
json_extract_path_text(m1,'value') as from_level,
json_extract_path_text(m2,'value') as to_level
from legendsawake.catchall
where event = 'player_levelup' and ts_pretty >= current_date -10
) a
where a.from_level < a.to_level),
/*currency transaction info*/
t as 
(
select
c.user_id,
event as currency_type,
c.ts_pretty,
c.level::int,
json_extract_path_text(d_c1,'value') as action,
sum(json_extract_path_text(m1,'value')::bigint) as currency_received,
sum(json_extract_path_text(m2,'value')::bigint) as currency_spent
from legendsawake.catchall c
where event in ('coins_transaction','rc_transaction','tc_transaction','PVP_Currency_Flow') and ts_pretty >= current_date -10
group by 1,2,3,4,5
order by 1,2,3,4,5),
/*item transaction info*/
i as
(
select
user_id,
level::int,
ts_pretty,
json_extract_path_text(d_c1,'value') as item_id,
m.item_name ,
sum(json_extract_path_text(m2,'value')::bigint) as item_received,
sum(json_extract_path_text(m1,'value')::bigint) as item_spent
from legendsawake.catchall c
left join legendsawake.item_mapping m
on json_extract_path_text(d_c1,'value')=m.item_id
where event='item_transaction' and ts_pretty >= current_date -10
group by 1,2,3,4,5)

/*aggregate all*/
select
t_agg.*,
i_agg.item_name,
i_agg.item_received,
i_agg.item_spent
from
(/*aggregate currency transactions of level up*/
select
trunc(l.ts_pretty) as date,
l.from_level,
l.to_level,
l.action,
t.currency_type,
sum(t.currency_received) as currency_received,
sum(t.currency_spent) as currency_spent
from
t
join
l
on t.user_id = l.user_id
and t.ts_pretty <= l.ts_pretty
and t.level = l.from_level
and t.level < l.to_level
group by 1,2,3,4,5) t_agg
left join
(/*aggregate item transactions of level up*/
select
trunc(l.ts_pretty) as date,
l.from_level,
l.to_level,
l.action,
i.item_name,
sum(i.item_received) as item_received,
sum(i.item_spent) as item_spent
from i
join l
on i.user_id = l.user_id
and i.ts_pretty <= l.ts_pretty
and i.level = l.from_level
and i.level < l.to_level
group by 1,2,3,4,5) i_agg
on t_agg.date = i_agg.date
and t_agg.from_level = i_agg.from_level
and t_agg.to_level = i_agg.to_level
and t_agg.action = i_agg.action
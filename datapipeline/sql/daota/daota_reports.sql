-----------update the os value in events table

update events set os = 'ios' where os = 'iPhone OS';
update events set os = 'android' where os = 'Android';

CREATE TEMP TABLE USER_COUNTRY
as
select app
       ,uid
       ,snsid
       ,install_ts
       ,country_code

from(
SELECT           app
                ,uid
                ,snsid
                ,install_ts
                ,country_code
                ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events
where event <> 'payment'
and   trunc(ts) =?::DATE
)t
where rank = 1;

--Get payer,conversion_ts,and last_login_ts info

CREATE TEMP TABLE payer_user_info
as
select app,
       uid,
       snsid,
       min(ts) as conversion_ts
FROM events 
where event = 'payment'
and   trunc(ts)=?::DATE
group by 1,2,3;




--Get other user info for app users
truncate table app_user_temp;

insert into app_user_temp (app,uid,snsid,install_ts,install_source,os,os_version,country_code,level,language,device,latest_login_ts)
select t1.app,
       t1.uid,
       t1.snsid,
       t1.install_ts,
       t1.install_source,
       t1.os,
       t1.os_version,
       t2.country_code,
       t1.level,
       t1.language,
       t1.device,
       t1.ts as latest_login_ts
from   
(select app,
       uid,
       snsid,
       install_source,
       install_ts,
       os,
       os_version,
       level,
       language,
       device,
       ts,
       row_number() over(partition by app,uid,snsid order by install_ts desc) as rank
from
       (select app,
              uid,
              snsid,  --sortkey
              install_source,
              install_ts,  --diskey
              os,
              os_version,
              max(cast (json_extract_path_text(properties,'level') as int)) as level,
              json_extract_path_text(properties,'language') as language,
              json_extract_path_text(properties,'device') as device,
              ts
       from  events
       where trunc(ts) =?::DATE
       and os <>''
       group by 1,2,3,4,5,6,7,9,10,11
       )
)t1
left join USER_COUNTRY t2
on t1.app = t2.app
and t1.snsid = t2.snsid
and t1.uid = t2.uid
where t1.rank=1;

update app_user_temp set conversion_ts=app_user.conversion_ts from app_user where app_user_temp.uid=app_user.uid and app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;



update app_user_temp set conversion_ts=payer_user_info.conversion_ts from payer_user_info where app_user_temp.uid=payer_user_info.uid and app_user_temp.snsid=payer_user_info.snsid and app_user_temp.app=payer_user_info.app
and app_user_temp.conversion_ts is null;

update app_user_temp set install_source = app_user.install_source from app_user where app_user_temp.uid=app_user.uid and
app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;


--delete the old data of app user
delete from app_user where (app,uid,snsid) in (select app,uid,snsid from app_user_temp);

insert into app_user
(
       user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       conversion_ts,
       latest_login_ts
)
select md5(app||uid||snsid),
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       conversion_ts,
       latest_login_ts
from app_user_temp
;

--update the payer flag
update app_user set is_payer = true where conversion_ts is not null;

----update install_ts per user (not per server)----
create temp table install_ts_update as
select app,uid,min(install_ts) as install_ts from app_user
group by app,uid;


update app_user set install_ts=t.install_ts from install_ts_update t
where app_user.app=t.app and app_user.uid=t.uid;

----update conversion_ts---
create temp table conv_ts_update as
select app,uid,min(conversion_ts) as conversion_ts from app_user
group by app,uid;


update app_user set conversion_ts=t.conversion_ts from conv_ts_update t
where app_user.app=t.app and app_user.uid=t.uid;


  
--update install_source
update app_user set install_source = mat.publisher_name,os_version = mat.os_version,language = mat.language,device = mat.device_model
from mat where mat.site_name = 'DT_ZH' and app_user.uid = mat.user_id and app_user.os = 'ios';

update app_user set install_source = mat.publisher_name,os_version = mat.os_version,language = mat.language,device = mat.device_model
from mat where mat.site_name = 'DT_ZH_ANDROID' and app_user.uid = mat.user_id and os = 'android';
  
update app_user set install_source = kochava.campaign_name 
from kochava where kochava.app = app_user.app and app_user.uid = kochava.fpid and app_user.install_source in ('organic','Origin'); 

delete from dau where dt =?::DATE;

INSERT INTO dau
select md5(app||uid||snsid),
       app,
       dt,
       uid,
       snsid,
       install_source,
       install_dt,
       os,
       os_version,
       country_code,
       level,
       device
from
   (select app,
           dt,
           uid,
           snsid,
           install_source,
           install_dt,
           os,
           os_version,
           country_code,
           level,
           device,
           row_number() over(partition by app,uid,snsid order by dt) as rank
     from
             (SELECT e.app,
                    trunc(e.ts) as dt,
                    e.uid,
                    e.snsid,
                    a.install_source, 
                    trunc(a.install_ts) AS install_dt,
                    a.os,
                    a.os_version,
                    a.country_code,
                    max(cast(json_extract_path_text(properties,'level') AS integer)) AS LEVEL,
                    a.device
             FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
             WHERE trunc(e.ts)= ?::DATE
             GROUP BY 1,2,3,4,5,6,7,8,9,11
             )
     )t
where t.rank=1;     

-------------------------------------------------Payment--------------------------------------------------------------------------------------         

delete from payment where trunc(ts)=?::DATE;
INSERT INTO payment

SELECT md5(e.app||e.uid||e.snsid),
       e.uid,
       e.snsid,
       e.app,
       e.ts,
       trunc(a.install_ts) as install_dt,
       a.install_source,
       a.os,
       a.os_version,
       a.device,
       a.country_code,
       product_id,
       product_name,
       lang,
       e.currency,
       e.level,
       payment_processor,
       amount*c.factor as amount,
       is_gift,
       coins_in,
       product_type,
       rc_in,
       transaction_id,
       promo_id,
       is_promo
FROM 
     (
       select uid,
              snsid,
              app,
              ts,
              json_extract_path_text(properties,'product_id') as product_id,
              json_extract_path_text(properties,'product_name') as product_name,
              json_extract_path_text(properties,'lang') as lang,
              json_extract_path_text(properties,'currency') as currency,
              cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as integer) as level,
              json_extract_path_text(properties,'payment_processor') as payment_processor,
              json_extract_path_text(properties,'amount')::float as amount,
              json_extract_path_text(properties,'is_gift') as is_gift,
              cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as integer) as coins_in,
              json_extract_path_text(properties,'product_type') as product_type,
              cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as integer) as rc_in,
              json_extract_path_text(properties,'transaction_id') as transaction_id,
              json_extract_path_text(properties,'promo_id') as promo_id,
              json_extract_path_text(properties,'is_promo') as is_promo,
              row_number() over(partition by app,json_extract_path_text(properties,'transaction_id') order by ts desc) as rank
       from events
       where trunc(events.ts) =?::DATE
       and event = 'payment'
       and json_extract_path_text(properties,'product_id') like 'com.funplus%'
                                                             
     )e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
left join currency c on trunc(e.ts)=c.dt
and  c.currency='CNY'
where e.rank = 1
;

---------------------------------------------------kpi raw--------------------------------------------------------------------------------------
CREATE temp TABLE kpi_temp
(
   app             varchar(50),
   dt              date,
   install_dt      date,
   install_source  varchar(100),
   os              varchar(30),
   device          varchar(100),
   country_code    varchar(10),
   amount          numeric(18,4),
   login_cnt       integer,
   dau_cnt         integer,
   newuser_cnt     integer,
   newpayer_cnt    integer,
   payer_dau_cnt   integer,
   event           varchar(100)
);



----login cnt
INSERT INTO kpi_temp(app,dt,install_dt,install_source,os,device,country_code,login_cnt,event)
SELECT e.app,
       trunc(e.ts),
       trunc(a.install_ts),
       e.install_source,
       e.os,
       json_extract_path_text(properties,'device') as device,
       a.country_code,
       count(1) AS login_cnt,
       'session_start'
FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
WHERE e.event='session_start'
 AND trunc(e.ts) =?::DATE
GROUP BY 1,2,3,4,5,6,7;

--dau cnt

INSERT INTO kpi_temp(app,dt,install_dt,install_source,os,device,country_code,dau_cnt,event)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       device,
       country_code,
       count(DISTINCT uid) AS dau_cnt,
       'dau'
FROM dau
where dt =?::DATE
GROUP BY 1,2,3,4,5,6,7;

--newuser_cnt
INSERT INTO kpi_temp(app,dt,install_dt,install_source,os,device,country_code,newuser_cnt,event)
SELECT app,
       trunc(install_ts),
       trunc(install_ts),
       install_source,
       os,
       device,
       country_code,
       count(distinct uid) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY 1,2,3,4,5,6,7;


--payer_dau_cnt
INSERT INTO kpi_temp(app,dt,install_dt,install_source,os,country_code,payer_dau_cnt,device,event)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       count(d.uid) AS payer_dau_cnt,
       d.device,
       'payer_dau'
FROM dau d 
INNER JOIN app_user a ON d.user_id = a.user_id 
and a.is_payer is true
WHERE d.dt=?::DATE
GROUP BY 1,2,3,4,5,6,8;

--newpayer_cnt
INSERT INTO kpi_temp(app,dt,install_dt,install_source,os,device,country_code,newpayer_cnt,event)
SELECT app,
       trunc(conversion_ts),
       trunc(install_ts),
       install_source,
       os,
       device,
       country_code,
       count(uid) AS newpayer_cnt,
       'newpayers'
FROM app_user
WHERE trunc(conversion_ts) =?::DATE
and  is_payer is true
group by 1,2,3,4,5,6,7
;


--revenue
INSERT INTO kpi_temp(app,dt,install_dt,install_source,os,device,country_code,amount,event)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       os,
       device,
       country_code,
       sum(amount) as amount,
       'payment'
from   payment
where  trunc(ts) = ?::DATE 
GROUP by 1,2,3,4,5,6,7;


---------------------------------------------------kpi----------------------------------------------------------------------------------------
delete from kpi where dt =?::DATE;


INSERT INTO kpi(app,dt,install_dt,install_source,os,device,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       device,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_temp
where dt=?::DATE
GROUP BY 1,2,3,4,5,6,7;


--drop temp table

drop table kpi_temp;
-------------------------------------------------Dota Overall report----------------------------------------------------------------------
delete from tbl_overall_report_kpi where dt =?::DATE;
insert into tbl_overall_report_kpi
(
 dt 
 ,app 
 ,country_code 
 ,os 
 ,device 
 ,install_source 
 ,new_account_num 
 ,new_user_num 
 ,dau_num 
 ,payer_num 
 ,revenue 
 ,new_payer_num 
 ,new_revenue 
)

SELECT  ?::DATE
       ,a.app
       ,a.country_code
       ,a.os
       ,a.device
       ,a.install_source 
       ,count(distinct case when trunc(a.install_ts) = ?::DATE then a.uid else null end)  
       ,count(distinct case when trunc(a.install_ts) = ?::DATE then a.snsid else null end) 
       ,count(distinct d.snsid)
       ,count(distinct p.snsid) 
       ,case when sum(p.revenue) is not null then sum(p.revenue) else 0.00 end
       ,count(case when a.is_payer is true and trunc(a.conversion_ts) = ?::DATE  then 1 else null end) 
       ,sum(case when a.is_payer is true and trunc(a.conversion_ts) = ?::DATE then p.revenue else 0 end) 
from app_user a
left join dau d
on a.uid=d.uid
and a.app=d.app
and a.snsid=d.snsid
and d.dt = ?::DATE
left join(
           select app,
                  uid,
                  snsid,
                  sum(amount) as revenue
           from   payment
           where  trunc(ts) = ?::DATE       
           group by 1,2,3
          )p
on a.app=p.app and a.uid=p.uid and a.snsid=p.snsid
where trunc(a.install_ts)<=?::DATE
group by 1,2,3,4,5,6;
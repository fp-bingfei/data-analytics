----Transactions----------------

delete from rc_transaction where trunc(ts)=?::DATE;

INSERT INTO rc_transaction(user_id,app,uid,snsid,ts,install_source,os,install_dt,country_code,rc_in,rc_out,rc_bal,level,location,action,action_detail,device,lang)
SELECT md5(e.app||e.uid||e.snsid),e.app,e.uid,e.snsid,
       e.ts,
       a.install_source,
       a.os,
       trunc(a.install_ts),
       a.country_code,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') END AS float) AS rc_in,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') END AS float) AS rc_out,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_bal') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_bal') END AS float) AS rc_bal,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') AS level,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'location') AS location,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action') AS action,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail') AS action_detail,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') AS device,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'lang') AS lang    
FROM events_raw e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.snsid = a.snsid and e.uid=a.uid 
WHERE event = 'rc_transaction' and e.install_ts is not null
AND   trunc(ts) = ?::DATE
AND length(json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail'))<=2000;

delete from rpt_rc where dt=?::DATE;

INSERT INTO rpt_rc(app,dt,install_dt,install_source,country_code,os,device,rc_in,rc_out,rc_bal,level,location,action)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       device,
       SUM(rc_in),
       SUM(rc_out),
       SUM(rc_bal),
       level,
       location,
       action
FROM rc_transaction
WHERE trunc(ts) =?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         device,
         level,
         location,
         action;

delete from coin_transaction where trunc(ts)=?::DATE;

INSERT INTO coin_transaction(user_id,app,uid,snsid,ts,install_source,os,install_dt,country_code,coins_in,coins_out,coins_bal,level,location,action,action_detail,device,lang)
SELECT  md5(e.app||e.uid||e.snsid),e.app,e.uid,e.snsid,
       e.ts,
       a.install_source,
       a.os,
       trunc(a.install_ts),
       a.country_code,

       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') END AS float) AS coins_in,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') END AS float) AS coins_out,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_bal') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_bal') END AS float) AS coins_bal,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') AS level,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'location') AS location,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action') AS action,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail') AS action_detail,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') AS device,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'lang') AS lang
FROM events_raw e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.snsid = a.snsid and e.uid=a.uid
WHERE event = 'coins_transaction' and e.install_ts is not null
AND   trunc(ts) = ?::DATE
and length(json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail'))<=2000;


delete from rpt_coin where dt=?::DATE;

INSERT INTO rpt_coin(app,dt,install_dt,install_source,country_code,os,device,coins_in,coins_out,coins_bal,level,location,action)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       device,
       SUM(coins_in),
       SUM(coins_out),
       SUM(coins_bal),
       level,
       location,
       action
FROM coin_transaction
WHERE trunc(ts) = ?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         device,
         level,
         location,
         action;


delete from item_transaction where trunc(ts)=?::DATE;

INSERT INTO item_transaction(user_id,app,uid,snsid,install_dt,ts,install_source,country_code,os,os_version,device,item_in,item_out,item_bi,level,location,action,action_detail,item_name,item_class,item_type,item_id)
SELECT md5(e.app||e.uid||e.snsid) as user_id,e.app,
       e.uid,e.snsid,
       trunc(a.install_ts),e.ts,
       a.install_source,
       a.country_code,
       a.os,
       a.os_version,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') AS device,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') END AS float) AS item_in,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') END AS float) AS item_out,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_BI') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_BI') END AS float) AS item_BI,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') AS level,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'location') AS location,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action') AS action,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail') AS action_detail,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_name') AS item_name,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_class') AS item_class,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_type') AS item_type,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_id') AS item_id
FROM events_raw e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.snsid = a.snsid and e.uid=a.uid and e.install_ts is not null
WHERE event = 'item_transaction'
AND   trunc(ts) = ?::DATE
and length(json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail'))<=200;

delete from rpt_item where dt=?::DATE;

INSERT INTO rpt_item(app,dt,install_dt,install_source,country_code,os,device,item_in,item_out,item_bi,level,location,action,item_name,
item_class,item_type,item_id)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       device,
       SUM(item_in),
       SUM(item_out),
       sum(item_bi),
       level,
       location,
       action,
       item_name,
       item_class,
       item_type,
       item_id
FROM item_transaction
WHERE trunc(ts) = ?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         device,
         level,
         location,
         action,
         item_name,
         item_class,
         item_type,
         item_id;


create temp table temp_rpt_tutorial
as 
SELECT t.user_id,
       t.snsid ,
       t.uid,
       t.max_dt ,
       t.step,
       t.app ,
       t.t_snsid,
       t.install_source,
       t.os,
       t.install_dt,
       t.country_code,
       t.device,
       c.country
FROM 
  (SELECT a.snsid,
          a.user_id,
          a.uid,
          e.snsid AS t_snsid,
          trunc(e.max_ts) AS max_dt,
          a.country_code,
          trunc(a.install_ts) AS install_dt,
          a.os,
          a.install_source,
          a.app,
          a.device,
          e.step
   FROM app_user a
   LEFT JOIN 
      (select app,
             uid,
             snsid,
             max(ts) as max_ts,
             json_extract_path_text(properties,'step') as step
      from   events_raw
      where  event = 'tutorial' and install_ts is not null and trunc(ts)=?::DATE
      group by 1,2,3,5
      )e
   on a.app=e.app
   and a.uid=e.uid
   and a.snsid=e.snsid
  )t 
LEFT JOIN country c ON t.country_code=c.country_code;

delete from rpt_tutorial where (user_id,step) in (select user_id, step from temp_rpt_tutorial);

insert into rpt_tutorial(user_id,snsid,uid,max_ts,step,app,t_snsid,install_source,os,install_ts,country_code,country)
select user_id,snsid,uid,max_dt,step,app,t_snsid,install_source,os,install_dt,country_code,country from temp_rpt_tutorial;

delete from rpt_tutorial_device where (user_id,step) in (select user_id, step from temp_rpt_tutorial);

insert into rpt_tutorial_device(user_id,snsid,uid,max_ts,step,app,t_snsid,install_source,os,install_ts,device,country_code,country)
select user_id,snsid,uid,max_dt,step,app,t_snsid,install_source,os,install_dt,device,country_code,country from temp_rpt_tutorial;

delete from hat_count where trunc(ts)=?::DATE;
insert into hat_count(uid,ts,app,hat_id,hat_count,install_source,os,device,country_code,install_dt)
select t.snsid,t.ts,t.app,t.hat_id,t.hat_count,substring(a.install_source,0,100),a.os,a.device,
a.country_code,trunc(a.install_ts) from
(
SELECT snsid,ts,app,JSON_EXTRACT_ARRAY_ELEMENT_TEXT(json_extract_path_text(properties,'hat_array'), seq.i) as hat_id,
cast(case when json_extract_path_text(properties,'hat_count')='' then '0' else json_extract_path_text(properties,'hat_count') end as integer)/JSON_ARRAY_LENGTH(json_extract_path_text(properties,'hat_array')) as hat_count  
    FROM events_raw, seq_0_to_10 AS seq
    WHERE seq.i < JSON_ARRAY_LENGTH(json_extract_path_text(properties,'hat_array')) and json_extract_path_text(properties,'hat_count')>0 and event='hat_count' and install_ts is not null and trunc(ts)=?::DATE)t
    left join app_user a on t.app=a.app and t.snsid=a.snsid;


delete from quest_completion where trunc(ts)=?::DATE;

insert into quest_completion(uid,snsid,app,country,install_source,country_code,device,os,install_dt,ts,level,action,method,quest_id)
select e.uid,e.snsid,e.app,c.country,a.install_source,a.country_code,a.device,a.os,trunc(a.install_ts) as install_dt,e.ts,
CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text (properties,'level') END AS integer) AS level,
json_extract_path_text(properties, 'action') as action,
json_extract_path_text(properties, 'method') as method,
json_extract_path_text(properties, 'quest_id') as quest_id
from events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid left join country c on a.country_code=c.country_code where e.event='quest_complete' and trunc(e.ts)=?::DATE;



create temp table coins_bal as
select t.app,
t.snsid,
t.uid,
trunc(t.ts) as dt,
t.install_dt,
t.level,
t.coins_bal from
(
select app,uid,
snsid,
install_dt,
level,
coins_bal,
ts ,
row_number() OVER (partition by app, uid, snsid order by ts desc) as row 
from coin_transaction 
where trunc(ts)=?::DATE

)t where t.row=1;






create temp table rc_bal as
select t.app,
t.snsid,
t.uid,
trunc(t.ts) as dt,
t.install_dt,
t.level,
t.rc_bal from
(
select app,uid,
snsid,
install_dt,
level,
rc_bal,
ts,
row_number() OVER (partition by app, uid, snsid order by ts desc) as row 
from rc_transaction
where trunc(ts)=?::DATE
)t where t.row=1;

DELETE FROM coins_rc_wallet where dt=?::DATE;




insert into coins_rc_wallet (user_id,app,dt,uid,snsid,install_source,install_dt,os,os_version,country_code,level,device)
select user_id,app,dt,uid,snsid,install_source,install_dt,os,os_version,country_code,level,device  from dau
where dt=?::DATE;








update coins_rc_wallet  set coins_bal=b.coins_bal from coins_bal b 
where coins_rc_wallet.app=b.app
and coins_rc_wallet.snsid=b.snsid
and coins_rc_wallet.uid=b.uid
and coins_rc_wallet.dt=b.dt
and coins_rc_wallet.install_dt=b.install_dt;



update coins_rc_wallet  set rc_bal=b.rc_bal from rc_bal b 
where coins_rc_wallet.app=b.app
and coins_rc_wallet.snsid=b.snsid
and coins_rc_wallet.uid=b.uid
and coins_rc_wallet.dt=b.dt
and coins_rc_wallet.install_dt=b.install_dt;





 
create temp table total_payer as 
select count(distinct snsid) as total_payers, app,install_source,os,country_code,trunc(ts) as dt,install_dt from payment
where trunc(ts)=?::DATE
group by app,install_source,os,country_code,trunc(ts),install_dt;

create temp table coin_rc_bal as 
select sum(coins_bal) as coins_bal,sum(rc_bal) as rc_bal, app,install_source,os,country_code,dt,install_dt from coins_rc_wallet
where dt=?::DATE
group by app,install_source,os,country_code,dt,install_dt;

delete from kpi_product where dt=?::DATE;

insert into kpi_product(app, dt, install_dt, install_source, os, country_code, amount, login_cnt, dau_cnt, payer_dau_cnt, newuser_cnt, newpayer_cnt)
select app,
dt,
install_dt,
install_source,
os,
country_code,
amount,
login_cnt,
dau_cnt,
payer_dau_cnt,
newuser_cnt,
newpayer_cnt from kpi
where dt=?::DATE;

update kpi_product set totalpayer_cnt=p.total_payers from total_payer p
where kpi_product.app=p.app and kpi_product.install_dt=p.install_dt and kpi_product.install_source=p.install_source and
kpi_product.dt=p.dt and kpi_product.os=p.os and kpi_product.country_code=p.country_code;


update kpi_product set coins_bal=c.coins_bal from coin_rc_bal c
where kpi_product.app=c.app and kpi_product.install_dt=c.install_dt and kpi_product.install_source=c.install_source and
kpi_product.dt=c.dt and kpi_product.os=c.os and kpi_product.country_code=c.country_code;

update kpi_product set rc_bal=c.rc_bal from coin_rc_bal c
where kpi_product.app=c.app and kpi_product.install_dt=c.install_dt and kpi_product.install_source=c.install_source and
kpi_product.dt=c.dt and kpi_product.os=c.os and kpi_product.country_code=c.country_code;

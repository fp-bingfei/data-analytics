update app_user
set install_source = a.install_source
from adeven a
where app_user.snsid = a.snsid and app_user.uid = a.uid and a.event = 'install';

update app_user
set install_source = a.install_source
from adeven a
where app_user.snsid = a.snsid and app_user.uid = a.uid and a.event = 'first_open';


DELETE FROM EVENTS WHERE ID IN
(
  SELECT ID FROM EVENTS WHERE TRUNC(ts) >= ?::DATE AND properties LIKE '^%'
);

DELETE
FROM dau
WHERE dt = ?::DATE;

INSERT INTO dau
SELECT e.uid,
       max(CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') END AS integer)) AS level,
       trunc(e.ts) AS dt,
       e.app,
       a.install_source,
       a.country_code,
       a.os,
       trunc(a.install_ts) AS install_dt,
       a.device
FROM events e
  INNER JOIN app_user a
          ON e.app = a.app
         AND e.uid = a.snsid
WHERE trunc(e.ts) = ?::DATE
GROUP BY e.uid,
         dt,
         install_dt,
         a.device,
         a.country_code,
         a.install_source,
         a.os,
         e.app;

DELETE
FROM payment
WHERE trunc(ts) = ?::DATE;

insert into payment
select e.ts,e.app,e.uid,a.install_source,a.os,a.country_code,a.install_ts,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') as level, a.device, 
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'cash_type') as cash_type,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'currency') as currency,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'amount')::float/100 as amount,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'paid_time') as paid_time,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_id') as product_id,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_name') as product_name,
json_extract_path_text(regexp_replace(properties,'\\\\.',''),'type') as product_type 
from events e
join app_user a on e.app = a.app and e.uid = a.snsid where e.name = 'payment' 
and trunc(e.ts)=?::DATE;



--------------------------------------------------------USER PAYMENT TABLE------------------------------------------
DELETE FROM user_payment_stats WHERE TRUNC(ts)=?::DATE;

CREATE TEMP TABLE TEMP_USER_PAYMENT AS
(
  SELECT uid,
       min(ts) AS ts,
       trunc(install_ts) as install_dt,
       country_code,
       os,
       app,
       install_source
FROM payment
WHERE trunc(ts) = ?::DATE
GROUP BY uid,
         country_code,
         trunc(install_ts),
         os,
         install_source,
         app
);


INSERT INTO user_payment_stats
(
  uid,
  ts,
  install_dt,
  country_code,
  os,
  app,
  install_source
)
SELECT  uid,
        ts,
        install_dt,
        country_code,
        os,
        app,
        install_source
FROM TEMP_USER_PAYMENT
WHERE (app,uid) not in (select app,uid from user_payment_stats);

drop table TEMP_USER_PAYMENT;

truncate table kpi_raw;

--login cnt 
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  login_cnt,
  device,
  event
)
SELECT u.app,
       trunc(e.ts),
       trunc(u.install_ts),
       u.install_source,
       u.os,
       u.country_code,
       COUNT(1) AS login_cnt,
       u.device,
       'login'
FROM events e
  JOIN app_user u
    ON e.app = u.app
   AND e.uid = u.snsid
WHERE e.name = 'login'
AND   trunc(e.ts) = ?::DATE
GROUP BY u.app,
         trunc(e.ts),
         trunc(u.install_ts),
         u.install_source,
         u.os,
         u.country_code,
         u.device;

--dau cnt 
insert into kpi_raw(app,dt,install_dt,install_source,os,country_code,dau_cnt,device,event)

select d.app,d.dt,d.install_dt,d.install_source,d.os,d.country_code,count( d.uid) as dau_cnt,d.device,'dau' 
from dau d 

where d.dt = ?::DATE

group by d.app,d.dt,d.install_dt,d.install_source,d.os,d.country_code,d.device;


--newuser_cnt 
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  newuser_cnt,
  device,
  event
)
SELECT u.app,
       trunc(u.install_ts),
       trunc(u.install_ts),
       u.install_source,
       u.os,
       u.country_code,
       COUNT(1) AS newuser_cnt,
       u.device,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY u.app,
         trunc(u.install_ts),
         trunc(u.install_ts),
         u.install_source,
         u.os,
         u.country_code,
         u.device;

--revenue 
insert into kpi_raw(app,dt,install_dt,install_source,os,country_code,amount,device,event)
select p.app,trunc(p.ts),trunc(p.install_ts),p.install_source,p.os,p.country_code,sum(amount),p.device_type,'payment' 
from payment p
where trunc(p.ts) = ?::DATE
group by p.app,trunc(p.ts),trunc(p.install_ts),p.install_source,p.os,p.country_code,p.device_type;

--payer_dau_cnt 
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  payer_dau_cnt,
  device,
  event
)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       COUNT(d.uid) AS payer_dau_cnt,
       d.device,
       'payer_dau'
FROM dau d
  INNER JOIN user_payment_stats t
          ON d.uid = t.uid
         AND d.os = t.os
         AND d.install_source = t.install_source
         AND d.app = t.app
         AND d.install_dt = t.install_dt
         AND d.country_code = t.country_code
WHERE d.dt = ?::DATE
GROUP BY d.os,
         d.country_code,
         d.app,
         d.install_source,
         d.dt,
         d.install_dt,
         d.device;

--newpayer_cnt 
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,newpayer_cnt,device,event)
SELECT p.app,
       trunc(p.ts) AS dt,
       p.install_dt,
       p.install_source,
       p.os,
       p.country_code,
       count(p.uid) AS newpayer_cnt,
       a.device,
       'newpayers'
FROM user_payment_stats p
LEFT JOIN app_user a ON p.uid=a.snsid
AND p.app=a.app
AND p.install_dt=trunc(a.install_ts)
AND p.country_code=a.country_code
AND p.os =a.os
AND p.install_source=a.install_source
WHERE trunc(p.ts)=?::DATE
GROUP BY p.os,
         p.country_code,
         p.app,
         p.install_source,
         dt,
         p.install_dt,
         a.device;

DELETE
FROM kpi
WHERE dt = ?::DATE;


INSERT INTO kpi
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  amount,
  login_cnt,
  dau_cnt,
  newuser_cnt,
  newpayer_cnt,
  payer_dau_cnt
)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       country_code,
       SUM(amount),
       SUM(login_cnt),
       SUM(dau_cnt),
       SUM(newuser_cnt),
       SUM(newpayer_cnt),
       SUM(payer_dau_cnt)
FROM kpi_raw
GROUP BY app,
         dt,
         install_dt,
         install_source,
         os,
         country_code;

DELETE
FROM kpi_device
WHERE dt = ?::DATE;

INSERT INTO kpi_device
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  device,
  amount,
  login_cnt,
  dau_cnt,
  newuser_cnt,
  newpayer_cnt,
  payer_dau_cnt
)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       country_code,
       device,
       SUM(amount),
       SUM(login_cnt),
       SUM(dau_cnt),
       SUM(newuser_cnt),
       SUM(newpayer_cnt),
       SUM(payer_dau_cnt)
FROM kpi_raw
where dt = ?::DATE
GROUP BY app,
         dt,
         install_dt,
         install_source,
         os,
         country_code,
         device;

TRUNCATE TABLE rpt_tutorial;

INSERT INTO rpt_tutorial
SELECT t.snsid,
       max_ts,
       t.step,
       t.app,
       t.install_source,
       t.os,
       t.install_ts,
       t.country_code,
       c.country
FROM (SELECT a.snsid,
             MAX(trunc(ts)) AS max_ts,
             a.country_code,
             trunc(a.install_ts) AS install_ts,
             a.os,
             a.install_source,
             a.app,
             json_extract_path_text(regexp_replace(properties,'\\\\.',''),'step') AS step
      FROM events e
        LEFT JOIN app_user a
               ON e.app = a.app
              AND e.uid = a.snsid
      GROUP BY a.snsid,
               a.country_code,
               install_ts,
               a.os,
               a.install_source,
               a.app,
               json_extract_path_text(regexp_replace(properties,'\\\\.',''),'step')) t
  LEFT JOIN country c ON t.country_code = c.country_code;

TRUNCATE TABLE rpt_tutorial_device;
INSERT INTO rpt_tutorial_device
SELECT t.snsid,
       max_ts,
       t.step,
       t.app,
       t.install_source,
       t.os,
       t.device,
       t.install_ts,
       t.country_code,
       c.country
FROM (SELECT a.snsid,
             MAX(trunc(ts)) AS max_ts,
             a.country_code,
             trunc(a.install_ts) AS install_ts,
             a.os,
             a.device,
             a.install_source,
             a.app,
             json_extract_path_text(regexp_replace(properties,'\\\\.',''),'step') AS step
      FROM events e
        LEFT JOIN app_user a
               ON e.app = a.app
              AND e.uid = a.snsid
      GROUP BY a.snsid,
               a.country_code,
               install_ts,
               a.os,
               a.install_source,
               a.app,
               a.device,
               json_extract_path_text(regexp_replace(properties,'\\\\.',''),'step')) t
  LEFT JOIN country c ON t.country_code = c.country_code;

DELETE
FROM item_transaction
WHERE trunc(ts) = ?::DATE;

DELETE
FROM coin_transaction
WHERE trunc(ts) = ?::DATE;

DELETE
FROM rc_transaction
WHERE trunc(ts) = ?::DATE;

DELETE
FROM rpt_item
WHERE dt = ?::DATE;

DELETE
FROM rpt_coin
WHERE dt = ?::DATE;

DELETE
FROM rpt_rc
WHERE dt = ?::DATE;

INSERT INTO item_transaction
SELECT e.app,
       e.ts,
       e.uid,
       trunc(a.install_ts),
       a.install_source,
       a.country_code,
       a.os,
       a.os_version,
       a.device,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_in') END AS integer) AS "item_in",
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'item_out') END AS integer) AS "item_out",
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_bi') AS item_bi,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') AS level,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'location') AS location,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action') AS action,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action_detail') AS action_detail,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_name') AS item_name,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_class') AS item_class,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_type') AS item_type,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'item_id') AS item_id
FROM events e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.uid = a.snsid
WHERE name = 'item_transaction'
AND   trunc(ts) = ?::DATE;

INSERT INTO rpt_item
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       os_version,
       device,
       SUM(item_in),
       SUM(item_out),
       item_bi,
       level,
       location,
       action,
       action_detail,
       item_name,
       item_class,
       item_type,
       item_id
FROM item_transaction
WHERE trunc(ts) = ?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         os_version,
         device,
         level,
         location,
         action,
         action_detail,
         item_bi,
         item_name,
         item_class,
         item_type,
         item_id;

INSERT INTO coin_transaction
SELECT e.uid,
       e.app,
       e.ts,
       a.install_source,
       a.os,
       trunc(a.install_ts),
       a.country_code,
       a.device,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_in') END AS integer) AS "coins_in",
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_out') END AS integer) AS "coins_out",
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_bal') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'coins_bal') END AS integer) AS "coins_bal",
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') AS level,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'location') AS location,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action') AS action
FROM events e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.uid = a.snsid
WHERE name = 'coins_transaction'
AND   trunc(ts) = ?::DATE;

INSERT INTO rpt_coin
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       device,
       SUM(coins_in),
       SUM(coins_out),
       SUM(coins_bal),
       level,
       location,
       action
FROM coin_transaction
WHERE trunc(ts) = ?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         device,
         level,
         location,
         action;

INSERT INTO rc_transaction
SELECT e.uid,
       e.app,
       e.ts,
       a.install_source,
       a.os,
       trunc(a.install_ts),
       a.country_code,
       a.device,
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_in') END AS integer) AS "rc_in",
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') END AS integer) AS "rc_out",
       CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_bal') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_bal') END AS integer) AS "rc_bal",
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') AS level,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'location') AS location,
       json_extract_path_text(regexp_replace(properties,'\\\\.',''),'action') AS action
FROM events e
  LEFT JOIN app_user a
         ON e.app = a.app
        AND e.uid = a.snsid
WHERE name = 'rc_transaction'
AND   trunc(ts) = ?::DATE;

INSERT INTO rpt_rc
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       country_code,
       os,
       device,
       SUM(rc_in),
       SUM(rc_out),
       SUM(rc_bal),
       level,
       location,
       action
FROM rc_transaction
WHERE trunc(ts) = ?::DATE
GROUP BY app,
         trunc(ts),
         install_dt,
         install_source,
         country_code,
         os,
         device,
         level,
         location,
         action;

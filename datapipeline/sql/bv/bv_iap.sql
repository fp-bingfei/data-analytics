drop table bv.public.tab_payment;
create table bv.public.tab_payment as
with payer as (select distinct snsid from bv.public.payment)
SELECT  i.snsid,i.ts, i.level,install_dt, i.install_source, coalesce(c.country,'Unknown') as country, i.device_type,
		    case
		      when p.name is null then i.product_name
		      when p.qty=1 then p.name
		      when lower(p.name) like '%special%' then p.name || ' : ' || p.qty || ' ' || p.type
		      else p.qty || ' ' || p.type
		    end as product_name,
		    case
		      when  p.name is null then i.product_type
		      when lower(p.name) like '%special%' then 'Special package'
		      else p.type
		    end as product_type,
		    amount,
		    row_number() over (partition by i.snsid order by i.ts asc) as iap_row,
		    count(*) over (partition by i.snsid rows between unbounded preceding and unbounded following) as totpurchase_cnt
FROM bv.public.payment i
left join bv.public.dim_product p on i.product_id=p.product_id
left join bv.public.country c on c.country_code=i.country_code
left join public.app_user u on u.snsid=i.snsid;
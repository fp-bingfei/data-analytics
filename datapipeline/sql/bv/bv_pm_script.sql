delete from feed_object where trunc(ts)=?::DATE;

INSERT INTO feed_object
SELECT
 e.app,
 e.ts,
 e.snsid,
 trunc(a.install_ts),
 a.install_source,
 a.country_code,
 a.os,
 a.os_version,
 a.device,
 json_extract_path_text(properties,'item') as item_id,
 i.item_name,
 json_extract_path_text(properties,'level') as level
FROM events_raw e
 LEFT JOIN app_user a
 ON e.app = a.app
 AND e.snsid = a.snsid and e.uid=a.uid
 LEFT JOIN item i
 ON json_extract_path_text(properties,'item') = i.item_id
WHERE event = 'feed_object' AND trunc(ts)=?::DATE;

delete from crop_actions where trunc(ts)=?::DATE;

INSERT INTO crop_actions
SELECT
     e.snsid,
     e.app,
     e.ts,
     trunc(a.install_ts),
     a.install_source,
     a.country_code,
     a.device,
     a.os,
     a.os_version,
     json_extract_path_text(properties,'item') as item_id,
     i.item_name,
     e.event,
     json_extract_path_text(properties,'action') as method,
     json_extract_path_text(properties,'level') as level
FROM events_raw e
     LEFT JOIN app_user a
     ON e.app = a.app
  AND e.snsid = a.snsid and e.uid=a.uid
     LEFT JOIN item i
     ON json_extract_path_text(properties,'item') = i.item_id
WHERE event = 'water_crop' AND trunc(ts)=?::DATE;

delete from purchase_well_time where trunc(ts)=?::DATE;

insert into purchase_well_time
select
e.snsid, 
e.app, 
e.ts, 
trunc(a.install_ts) AS install_dt, 
a.install_source, 
a.country_code, 
a.device, 
a.os, 
a.os_version, 
CAST(case when json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') = '' then '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_out') end as integer) as rc_out, 
CAST(case when json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_bal') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'rc_bal') end as integer) as rc_bal, 
json_extract_path_text(properties,'level') AS level 
FROM events_raw e 
LEFT JOIN app_user a 
ON e.app = a.app 
AND e.snsid = a.snsid and e.uid=a.uid
WHERE event = 'rc_transaction' AND json_extract_path_text(properties,'action') = 'purchase_well_time' 
AND trunc(ts)=?::DATE;


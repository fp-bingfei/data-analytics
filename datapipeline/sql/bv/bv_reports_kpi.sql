
CREATE TEMP TABLE USER_COUNTRY
as
select app
       ,uid
       ,snsid
       ,install_ts
       ,country_code

from(
SELECT           app
                ,uid
                ,snsid
                ,install_ts
                ,country_code
                ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events_raw
where event <> 'payment'
and   install_ts is not null and
trunc(ts) =?::DATE
)t
where rank = 1;

--Get payer,conversion_ts,and last_login_ts info

CREATE TEMP TABLE payer_user_info
as
select app,
       uid,
       snsid,
       install_ts,
       min(ts) as conversion_ts
FROM events_raw 
where event = 'payment' and install_ts is not null
and   trunc(ts)=?::DATE
group by 1,2,3,4;






--Get other user info for app users
truncate table app_user_temp;

insert into app_user_temp (app,uid,snsid,install_ts,install_source,os,os_version,country_code,level,language,device,latest_login_ts)
select t.app,
       t.uid,
       t.snsid,
       t.install_ts,
       t.install_source,
       t.os,
       t.os_version,
       t.country_code,
       t.level,
       t.language,
       t.device,
       t.latest_login_ts
from  
(select app,
       uid,
       snsid,
       install_source,
       install_ts,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       latest_login_ts,
      row_number() over(partition by app,uid,snsid,install_ts order by latest_login_ts desc ) as rank

from
       (select t1.app,
              t1.uid,
              t1.snsid,  --sortkey
              t1.install_source,
              t2.install_ts,  --diskey
              t1.os,
              t1.os_version,
            max(CAST(
              CASE
                WHEN event='level_up' and json_extract_path_text (regexp_replace(properties,'\\\\.',''),'to') != '' THEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'to')
                WHEN  json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') != '' THEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level')
                ELSE '0'
              END
              AS integer)) AS level,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'language') as language,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') as device ,
              t1.ts as latest_login_ts,t2.country_code

       from  events_raw t1 left join USER_COUNTRY t2
on t1.app = t2.app
and t1.snsid = t2.snsid
and t1.uid = t2.uid
where trunc(ts) =?::DATE and t2.install_ts is not null and t1.install_ts is not null
group by 1,2,3,4,5,6,7,9,10,11,12
)
)t where t.rank=1;


update app_user_temp set conversion_ts=app_user.conversion_ts from app_user where app_user_temp.uid=app_user.uid and app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;


update app_user_temp set conversion_ts=payer_user_info.conversion_ts from payer_user_info where app_user_temp.uid=payer_user_info.uid and app_user_temp.snsid=payer_user_info.snsid and app_user_temp.app=payer_user_info.app
and app_user_temp.conversion_ts is null;


update app_user_temp set install_source = app_user.install_source from app_user where app_user_temp.uid=app_user.uid and
app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;


--delete the old data of app user
delete from app_user where (app,uid,snsid) in (select app,uid,snsid from app_user_temp);

insert into app_user
(
       user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       conversion_ts,
       latest_login_ts
)
select t.user_id,t.app,t.uid,t.snsid,t.install_ts,t.install_source,t.os,t.os_version,t.country_code,t.level,t.language,t.device,t.conversion_ts,t.latest_login_ts from
(
select md5(app||uid||snsid) as user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       device,
       conversion_ts,
       latest_login_ts
from app_user_temp)t ;

--update the payer flag
update app_user set is_payer = true where conversion_ts is not null;


--update install source
Update app_user set install_source = 'Organic' where install_source = '';

update app_user set install_source = d.tracker_name
from adjust d
where app_user.snsid = d.userid
and app_user.install_source = 'Organic'
and app_user.app=d.game
and trunc(d.ts) = ?::DATE and d.tracker_name <> 'Organic';


delete from dau where dt =?::DATE;



INSERT INTO dau(user_id,uid,snsid,level,dt,app,install_source,country_code,os,install_dt,device)
SELECT md5(e.app||e.uid||e.snsid),e.uid,e.snsid,
       max(CAST(CASE WHEN json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') = '' THEN '0' ELSE json_extract_path_text (regexp_replace(properties,'\\\\.',''),'level') END AS integer)) AS level,
       trunc(e.ts) AS dt,
       e.app,
       a.install_source,
       a.country_code,
       a.os,
       trunc(a.install_ts) AS install_dt,
       a.device
FROM events_raw e
  INNER JOIN app_user a
          ON e.app = a.app
         AND e.snsid = a.snsid and e.uid=a.uid
WHERE trunc(e.ts) = ?::DATE and e.install_ts is not null
GROUP BY e.uid,e.snsid,
         dt,
         install_dt,
         a.device,
         a.country_code,
         a.install_source,
         a.os,
         e.app;


    

-------------------------------------------------Payment--------------------------------------------------------------------------------------        

delete from payment where trunc(ts)=?::DATE;

INSERT INTO payment(user_id,uid,snsid,app,ts,install_dt,install_source,os,os_version,country_code,product_id,product_name,product_type,lang,currency,device_type,device,level,payment_processor,amount,coins_in,
rc_in,transaction_id,store_id)
SELECT md5(e.app||e.uid||e.snsid),
       e.uid,
       e.snsid,
       e.app,
       e.ts,
       e.install_dt,
       e.install_source,
       e.os,
       e.os_version,
       e.country_code,
       product_id,
       product_name,
       product_type,
       lang,
       currency,
       device_type,
       e.device,
       e.level,
       payment_processor,
       amount as amount,
       coins_in,
       rc_in,
       transaction_id,
       store_id

FROM
     (
       select r.uid,
              r.snsid,
              r.app,
              r.ts,a.install_source,a.os,a.os_version,a.country_code,
              trunc(a.install_ts) as install_dt,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_id') as product_id,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_name') as product_name,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'lang') as lang,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'currency') as currency,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device_type') as device_type,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'device') as device,
             
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'level') end as integer) as level,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'payment_processor') as payment_processor,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'amount')::float/100::float  AS amount,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_in')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'coins_in') end as integer) as coins_in,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'product_type') as product_type,
              cast(case when json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_in')='' then '0' else json_extract_path_text(regexp_replace(properties,'\\\\.',''),'rc_in') end as integer) as rc_in,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'transaction_id') as transaction_id,
              json_extract_path_text(regexp_replace(properties,'\\\\.',''),'store_id') as store_id,
                              row_number() OVER (partition by r.app,r.uid,r.snsid,r.install_ts,r.ts order by transaction_id ) as rank

       from events_raw r
       left join app_user a on r.app=a.app and r.snsid=a.snsid and r.uid=a.uid  where trunc(r.ts) =?::DATE
       and event = 'payment'
       )e where e.rank=1

;


---------------------------------------------------kpi raw--------------------------------------------------------------------------------------
truncate table kpi_raw;


----login cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,device,country_code,login_cnt,event)
SELECT e.app,
       trunc(e.ts),
       trunc(e.install_ts),
       a.install_source,
       a.os,
       json_extract_path_text(properties,'device') as device,
       a.country_code,
       count(1) AS login_cnt,
       'login'
FROM events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
WHERE e.event='login'
  AND trunc(e.ts) =?::DATE
GROUP BY 1,2,3,4,5,6,7;

--dau cnt

INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,device,country_code,dau_cnt,event)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       device,
       country_code,
       count(user_id) AS dau_cnt,
       'dau'
FROM dau
where dt =?::DATE
GROUP BY 1,2,3,4,5,6,7;

--newuser_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,device,country_code,newuser_cnt,event)
SELECT app,
       trunc(install_ts),
       trunc(install_ts),
       install_source,
       os,
       device,
       country_code,
       count(distinct user_id) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) =?::DATE
GROUP BY 1,2,3,4,5,6,7;


--payer_dau_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,payer_dau_cnt,device,event)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       count(d.USER_ID) AS payer_dau_cnt,
       d.device,
       'payer_dau'
FROM dau d
INNER JOIN app_user a ON d.user_id = a.user_id
and a.is_payer is true
WHERE d.dt=?::DATE
GROUP BY 1,2,3,4,5,6,8;

--newpayer_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,device,country_code,newpayer_cnt,event)
SELECT app,
       trunc(conversion_ts),
       trunc(install_ts),
       install_source,
       os,
       device,
       country_code,
       count(distinct user_id) AS newpayer_cnt,
       'newpayers'
FROM app_user
WHERE trunc(conversion_ts) =?::DATE
and is_payer is true
group by 1,2,3,4,5,6,7
;


--revenue
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,device,country_code,amount,event)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       os,
       device,
       country_code,
       sum(amount) as amount,
       'payment'
from   payment
where  trunc(ts) =?::DATE
GROUP by 1,2,3,4,5,6,7;
 
---------------------------------------------------kpi----------------------------------------------------------------------------------------
delete from kpi where dt =?::DATE;

INSERT INTO kpi(app,dt,install_dt,install_source,os,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_raw
where dt =?::DATE
GROUP BY 1,2,3,4,5,6;



delete from kpi_device where dt =?::DATE;

INSERT INTO kpi_device(app,dt,install_dt,install_source,os,device,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       device,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_raw
where dt =?::DATE
GROUP BY 1,2,3,4,5,6,7;




-- Retention
 drop table public.staging_player_day_cube;
 drop table public.staging_new_user;
 drop table public.staging_retention_base;
 drop table bv.public.tab_retention;

drop table player_day ;
create temporary table player_day as
select 1 as day
union
select 2 as day
union
select 3 as day
union
select 4 as day
union
select 5 as day
union
select 6 as day
union
select 7 as day
union
select 14 as day
union
select 21 as day
union
select 28 as day
union
select 30 as day
union
select 45 as day
union
select 60 as day
union
select 90 as day
union
select 120 as day
union
select 150 as day
union
select 180 as day
union
select 210 as day
union
select 240 as day
union
select 270 as day
union
select 300 as day
union
select 360 as day;

create table bv.public.staging_new_user as
select
  date(install_ts) as install_dt, os,
   coalesce(c.country,'Unknown') as country,install_source,
   device,
  is_payer,
  count(u.snsid) as newuser_cnt
from bv.public.app_user u
left join bv.public.country c on c.country_code=u.country_code
where u.install_ts is not null and date(install_ts)>='2014-07-17'
group by install_dt, os,country,install_source,is_payer, device;

drop table last_date;
create temporary table last_date as
select max(dt) as date from public.dau;

create table bv.public.staging_player_day_cube as
select su.*, rd.day as player_day
from bv.public.staging_new_user su
join player_day rd on 1 = 1
join last_date d on 1=1
where DATEDIFF('day', install_dt, d.date) >= player_day;

create table public.staging_retention_base as
SELECT
  DATEDIFF('day', install_dt, d.dt) AS player_day,install_dt,d.dt,
  u.os,  coalesce(c.country,'Unknown') as country, u.install_source,
  u.device,
  is_payer,
  count(distinct d.snsid) as retaineduser_cnt
FROM bv.public.dau d
join bv.public.app_user u on d.snsid=u.snsid
join last_date l on 1=1
left join bv.public.country c on c.country_code=u.country_code
where u.install_ts is not null and date(install_ts)>='2014-07-17' and DATEDIFF('day', install_dt, d.dt) in (select day from player_day)
group by install_dt, d.dt, u.os, country, u.install_source, is_payer, u.device;

create table bv.public.tab_retention as
select  c.*,coalesce(retaineduser_cnt,0) as retaineduser_cnt
from  bv.public.staging_player_day_cube c
left join bv.public.staging_retention_base rb
on  c.player_day = rb.player_day and c.install_dt = rb.install_dt and
    c.os = rb.os and c.country = rb.country and
    c.install_source = rb.install_source and
    c.is_payer = rb.is_payer  and c.device = rb.device;


-- LTV

drop table bv.public.staging_ltv_base;
create table bv.public.staging_ltv_base as
SELECT
  d.day as player_day,
  date(u.install_ts) as install_dt, u.os,
  coalesce(c.country,'Unknown') as country,u.install_source,
  u.device,
  True as is_payer,
  sum(amount) as revenue
FROM bv.public.payment p
join bv.public.app_user u on p.snsid=u.snsid
left join bv.public.country c on c.country_code=u.country_code
join player_day d on DATEDIFF('day', date(u.install_ts), date(p.ts)) <= d.day
where u.install_ts is not null and date(install_ts)>='2014-07-17'
group by player_day,date(u.install_ts), u.os,country,u.install_source,is_payer, u.device;

drop table bv.public.tab_ltv;
create table bv.public.tab_ltv as
select  c.*,
        coalesce(rb.revenue,0) as revenue
from  bv.public.staging_player_day_cube c
left join bv.public.staging_ltv_base rb
on  c.player_day = rb.player_day and c.install_dt = rb.install_dt and
    c.os = rb.os and c.country = rb.country and
    c.install_source = rb.install_source and
    c.is_payer = rb.is_payer  and c.device = rb.device;

-- Repeat Rate

drop table tab_repeat_rate;
create table tab_repeat_rate as
with dau_player_day as
(SELECT
   d.snsid,
   dt,
    install_dt,
   DATEDIFF('day', install_dt, d.dt) AS player_day,
   u.install_source,
   d.country_code,
   is_payer,
   CASE WHEN datediff('day', (lag(d.dt)
   OVER (PARTITION BY d.snsid
     ORDER BY dt)), dt) = 1 THEN 1
   ELSE 0 END                        AS is_repeat
 FROM bv.public.dau d
   JOIN bv.public.app_user u
     ON d.snsid = u.snsid
)
select dt,install_dt,player_day,install_source,coalesce(c.country,'Unknown') as country, is_payer,is_repeat, count(snsid) as user_cnt
from dau_player_day d
left join bv.public.country c on c.country_code=d.country_code
group by dt,install_dt,player_day,install_source,coalesce(c.country,'Unknown'), is_payer,is_repeat;



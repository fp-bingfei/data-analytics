---------------------------
---------- Users ----------
---------------------------

---- Manually correct incorrect data sent to us.
--DROP TABLE IF EXISTS facebook_fix;
--CREATE TABLE facebook_fix(
--    user_id varchar(32) NOT NULL,
--    facebook_id varchar(20) NOT NULL
--);
--
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('','10152510593037716');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('491','304588163054855');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('490','735997553123546');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('200490','251073928435039');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('491','304588163054855');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('100483','1475317202712161');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('465','321869731311287');
--INSERT INTO facebook_fix(user_id,facebook_id) VALUES ('972','330823913737225');

delete from events where trunc((TIMESTAMP 'epoch' + json_extract_path_text (DATA,'ts')::BIGINT*INTERVAL '1 Second'))<'2014-12-10';

delete from events where data like '%}:%';

TRUNCATE facebook_user;

INSERT INTO facebook_user
select *
from facebook_fix
union
select distinct
    json_extract_path_text (DATA,'user_id') as user_id,
    json_extract_path_text (DATA,'properties','facebook_id')
from events
where
    json_extract_path_text (DATA,'properties','facebook_id') !='' and
    json_extract_path_text (DATA,'user_id') !='' and
    json_extract_path_text (DATA,'properties','facebook_id') ~ '^[0-9]+' and
    json_extract_path_text (DATA,'user_id')  not in (select user_id from facebook_fix);

---------------------------
-------- Statistics -------
---------------------------

TRUNCATE TABLE mission_statistics;

INSERT INTO mission_statistics
SELECT *
FROM (SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),0),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),0),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 1
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),1),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),1),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 2
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),2),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),2),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 3
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),3),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),3),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 4
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),4),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),4),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 5
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),5),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),5),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 6
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),6),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),6),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 7
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),7),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),7),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 8
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),8),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),8),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 9
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),9),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_statistics'),9),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_statistics')) >= 10
);

---------------------------
-------- Parameters -------
---------------------------

TRUNCATE TABLE mission_parameters;

INSERT INTO mission_parameters
SELECT *
FROM (SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),0),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),0),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 1
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),1),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),1),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 2
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),2),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),2),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 3
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),3),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),3),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 4
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),4),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),4),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 5
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),5),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),5),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 6
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),6),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),6),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 7
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),7),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),7),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 8
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),8),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),8),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 9
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),9),'name')) AS name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_parameters'),9),'value')) AS value
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_parameters')) >= 10
);

---------------------------
-------- Objectives -------
---------------------------

TRUNCATE TABLE mission_objectives;

INSERT INTO mission_objectives
SELECT *
FROM (SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),0),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),0),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),0),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),0),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),0),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 1
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),1),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),1),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),1),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),1),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),1),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 2
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),2),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),2),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),2),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),2),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),2),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 3
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),3),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),3),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),3),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),3),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),3),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 4
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),4),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),4),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),4),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),4),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),4),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 5
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),5),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),5),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),5),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),5),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),5),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 6
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),6),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),6),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),6),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),6),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),6),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 7
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),7),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),7),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),7),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),7),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),7),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 8
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),8),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),8),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),8),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),8),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),8),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 9
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),9),'objective_amount')) AS objective_amount,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),9),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),9),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),9),'objective_type')) AS objective_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives'),9),'objective_amount_remaining')) AS objective_amount_remaining
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives')) >= 10
);

--delete invalid data (bug has been fixed)
delete from mission_objectives where not(objective_amount ~ '^[0-9]+');

TRUNCATE TABLE mission_objectives_status;

INSERT INTO mission_objectives_status
SELECT *
FROM (SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),0),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),0),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),0),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),0),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 1
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),1),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),1),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),1),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),1),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 2
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),2),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),2),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),2),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),2),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 3
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),3),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),3),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),3),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),3),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 4
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),4),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),4),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),4),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),4),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 5
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),5),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),5),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),5),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),5),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 6
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),6),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),6),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),6),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),6),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 7
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),7),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),7),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),7),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),7),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 8
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),8),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),8),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),8),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),8),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 9
      UNION ALL
      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),9),'objective_amount_remaining')) AS objective_amount_remaining,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),9),'objective_id')) AS objective_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),9),'objective_name')) AS objective_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','mission_objectives_status'),9),'objective_type')) AS objective_type
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','mission_objectives_status')) >= 10
);

TRUNCATE TABLE FACT_MISSION_OBJECTIVE;

INSERT INTO FACT_MISSION_OBJECTIVE
SELECT md5(json_extract_path_text (DATA,'user_id')::varchar||json_extract_path_text (DATA,'ts')::VARCHAR) AS mission_key,
        mo.objective_id AS objective_id,
       mo.objective_name AS objective_name,
       mo.objective_type AS objective_type,
       CASE WHEN mo.objective_amount ~ '^\\d+$' THEN mo.objective_amount::integer ELSE null END AS objective_amount,
       case
            when mo.objective_amount_remaining ~ '^[0-9]+' then mo.objective_amount_remaining::INTEGER
            when json_extract_path_text (DATA,'properties','mission_status')='started' THEN NULL
            ELSE mos.objective_amount_remaining::INTEGER
       END AS objective_amount_remaining
FROM events
  LEFT JOIN mission_objectives mo ON md5 (DATA) = mo.event_id
  LEFT JOIN mission_objectives_status mos
        ON md5 (DATA) = mos.event_id
        AND mo.objective_id = mos.objective_id
WHERE json_extract_path_text (DATA,'event') = 'mission';

---------------------------
-------- Order Status -----
---------------------------

DROP TABLE IF EXISTS TEMP_MISSION_ORDER_STATUS;

CREATE TEMPORARY TABLE TEMP_MISSION_ORDER_STATUS (
mission_key VARCHAR(50),
order_status VARCHAR(1000)
);


INSERT INTO TEMP_MISSION_ORDER_STATUS
SELECT mission_key,
       CASE
         WHEN order_status_10 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9|| ', ' ||order_status_10
         WHEN order_status_9 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8|| ', ' ||order_status_9
         WHEN order_status_8 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7|| ', ' ||order_status_8
         WHEN order_status_7 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6|| ', ' ||order_status_7
         WHEN order_status_6 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5|| ', ' ||order_status_6
         WHEN order_status_5 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4|| ', ' ||order_status_5
         WHEN order_status_4 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3|| ', ' ||order_status_4
         WHEN order_status_3 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2|| ', ' ||order_status_3
         WHEN order_status_2 IS NOT NULL THEN order_status_1|| ', ' ||order_status_2
         WHEN order_status_1 IS NOT NULL THEN order_status_1
       END AS order_status
FROM (SELECT DISTINCT *
      FROM (SELECT mission_key,
                   nth_value(order_status,1) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_1,
                   nth_value(order_status,2) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_2,
                   nth_value(order_status,3) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_3,
                   nth_value(order_status,4) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_4,
                   nth_value(order_status,5) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_5,
                   nth_value(order_status,6) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_6,
                   nth_value(order_status,7) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_7,
                   nth_value(order_status,8) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_8,
                   nth_value(order_status,9) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_9,
                   nth_value(order_status,10) IGNORE NULLS OVER (PARTITION BY mission_key ORDER BY objective_name ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED following) AS order_status_10
            FROM (SELECT mission_key,
                         objective_name,
                         objective_name|| ':' ||objective_amount::INTEGER- objective_amount_remaining::INTEGER|| '/' ||objective_amount AS order_status
                  FROM (SELECT DISTINCT * FROM fact_mission_objective))
            WHERE order_status != ''));

---------------------------
-------- Fact_mission -----
---------------------------

DROP TABLE IF EXISTS TEMP_FACT_MISSION;

CREATE TEMPORARY TABLE TEMP_FACT_MISSION AS
SELECT DISTINCT * FROM 
(
SELECT 
                   md5(json_extract_path_text (DATA,'user_id')::varchar||json_extract_path_text (DATA,'ts')::VARCHAR) AS mission_key,
                   json_extract_path_text(DATA,'properties','mission_id') AS mission_id,
                   trunc((TIMESTAMP 'epoch' + json_extract_path_text (DATA,'ts')::BIGINT*INTERVAL '1 Second')) AS dt,
                   (TIMESTAMP 'epoch' + json_extract_path_text(DATA,'ts')::BIGINT *INTERVAL '1 Second') AS ts,
                   CASE WHEN json_extract_path_text(DATA,'properties','mission_start_ts') != ''
									 THEN (TIMESTAMP 'epoch' + json_extract_path_text(DATA,'properties','mission_start_ts')::BIGINT *INTERVAL '1 Second') ELSE null END
									 AS mission_start_ts,
                   json_extract_path_text(DATA,'session_id') AS session_id,
                   json_extract_path_text(DATA,'user_id') AS user_id,
                   case when json_extract_path_text(DATA,'properties','device_id')!='' then json_extract_path_text(DATA,'properties','device_id') else '' end as device_id,
                   case when json_extract_path_text(DATA,'properties','idfa')!='' then json_extract_path_text(DATA,'properties','idfa') else ''  end as idfa,
                   case when json_extract_path_text(DATA,'properties','android_id')!='' then json_extract_path_text(DATA,'properties','android_id') else '' end as android_id,
                   case when json_extract_path_text(DATA,'properties','gaid')!='' then json_extract_path_text(DATA,'properties','gaid') else '' end as gaid,
                   case when json_extract_path_text(DATA,'properties','mac_address')!='' then json_extract_path_text(DATA,'properties','mac_address') else '' end as mac_address,
                   coalesce(f.facebook_id,'') AS facebook_id,
                   json_extract_path_text(DATA,'app_id') AS app_id,
                   json_extract_path_text(DATA,'properties','app_version') AS app_version,
                   json_extract_path_text(DATA,'properties','os') AS os,
                   json_extract_path_text(DATA,'properties','os_version') AS os_version,
                   json_extract_path_text(DATA,'properties','ip') AS ip,
                   json_extract_path_text(DATA,'properties','lang') AS lang,
                   json_extract_path_text(DATA,'properties','device') AS device,
                   json_extract_path_text(DATA,'properties','mission_status') AS mission_status,
                   mp.initial_moves AS initial_moves,
                   mp.points_2_stars AS points_2_stars,
                   mp.points_3_stars AS points_3_stars,
                   mp.frozen_fruits_rate AS frozen_fruits_rate,
                   mp.butterfly_number AS butterfly_number,
                   ms.moves_remaining AS moves_remaining,
                   ms.moves_used AS moves_used,
                   ms.moves_buy AS moves_buy,
                   ms.points_final AS points_final,
                   ms.points_initial AS points_initial,
                   ms.no_possible_moves AS no_possible_moves
            FROM events
              LEFT JOIN facebook_user f on f.user_id=json_extract_path_text(DATA,'user_id')
              LEFT JOIN (SELECT event_id,
                                SUM(intial_moves) AS initial_moves,
                                SUM(points_2_stars) AS points_2_stars,
                                SUM(points_3_stars) AS points_3_stars,
                                SUM(frozen_fruits_rate) AS frozen_fruits_rate,
                                SUM(butterfly_number) AS butterfly_number
                         FROM (SELECT event_id,
                                      CASE mission_parameters.name
                                        WHEN 'initial_moves' THEN mission_parameters.value::INTEGER
                                        ELSE null
                                      END AS intial_moves,
                                      CASE mission_parameters.name
                                        WHEN 'points_2_stars' THEN mission_parameters.value::INTEGER
                                        ELSE null
                                      END AS points_2_stars,
                                      CASE mission_parameters.name
                                        WHEN 'points_3_stars' THEN mission_parameters.value::INTEGER
                                        ELSE null
                                      END AS points_3_stars,
                                      CASE mission_parameters.name
                                        WHEN 'frozen_fruits_rate' THEN mission_parameters.value::INTEGER
                                        ELSE null
                                      END AS frozen_fruits_rate,
                                      CASE mission_parameters.name
                                        WHEN 'butterfly_number' THEN mission_parameters.value::INTEGER
                                        ELSE null
                                      END AS butterfly_number
                               FROM mission_parameters)
                         GROUP BY event_id) mp ON md5 (DATA) = mp.event_id
              LEFT JOIN (SELECT event_id,
                                SUM(moves_remaining) AS moves_remaining,
                                SUM(moves_used) AS moves_used,
                                SUM(moves_buy) AS moves_buy,
                                SUM(points_final) AS points_final,
                                SUM(points_initial) AS points_initial,
                                SUM(no_possible_moves) AS no_possible_moves
                         FROM (SELECT event_id,
                                      CASE mission_statistics.name
                                        WHEN 'moves_remaining' THEN mission_statistics.value::INTEGER
                                        ELSE null
                                      END AS moves_remaining,
                                      CASE mission_statistics.name
                                        WHEN 'moves_used' THEN mission_statistics.value::INTEGER
                                        ELSE null
                                      END AS moves_used,
                                      CASE mission_statistics.name
                                        WHEN 'moves_buy' THEN mission_statistics.value::INTEGER
                                        ELSE null
                                      END AS moves_buy,
                                      CASE mission_statistics.name
                                        WHEN 'points_final' THEN mission_statistics.value::INTEGER
                                        ELSE null
                                      END AS points_final,
                                      CASE mission_statistics.name
                                        WHEN 'points_initial' THEN mission_statistics.value::INTEGER
                                        ELSE null
                                      END AS points_initial,
                                      CASE mission_statistics.name
                                        WHEN 'no_possible_moves' THEN mission_statistics.value::INTEGER
                                        ELSE null
                                      END AS no_possible_moves
                               FROM mission_statistics)
                         GROUP BY event_id) ms ON md5 (DATA) = ms.event_id and mp.event_id = ms.event_id
            WHERE json_extract_path_text (DATA,'event') = 'mission' and json_extract_path_text(DATA,'properties','mission_id')!=''
);

drop TABLE if exists FACT_MISSION;
create table  FACT_MISSION as
SELECT TEMP_FACT_MISSION.*,TEMP_MISSION_ORDER_STATUS.order_status from
TEMP_FACT_MISSION LEFT JOIN TEMP_MISSION_ORDER_STATUS ON TEMP_FACT_MISSION.mission_key = TEMP_MISSION_ORDER_STATUS.mission_key;

---------------------------
-------- Item_spent -------
---------------------------

TRUNCATE TABLE  item_spent;

INSERT INTO item_spent
SELECT *
FROM (SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),0),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),0),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),0),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),0),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 1

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),1),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),1),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),1),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),1),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 2

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),2),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),2),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),2),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),2),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 3

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),3),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),3),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),3),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),3),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 4

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),4),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),4),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),4),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),4),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 5

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),5),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),5),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),5),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),5),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 6

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),6),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),6),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),6),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),6),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 7

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),7),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),7),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),7),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),7),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 8

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),8),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),8),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),8),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),8),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 9

      UNION ALL

      SELECT md5(DATA) AS event_id,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),9),'item_name')) AS item_name,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),9),'item_class')) AS item_class,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),9),'item_type')) AS item_type,
             nvl(json_extract_path_text (json_extract_array_element_text (json_extract_path_text (DATA,'collections','items_spent'),9),'item_amount')) AS item_amount
      FROM events
      WHERE json_extract_path_text (DATA,'event') = 'mission'
      AND   json_array_length (json_extract_path_text (DATA,'collections','items_spent')) >= 10
);      

DROP TABLE IF EXISTS FACT_MISSION_ITEM_SPENT;

CREATE TABLE FACT_MISSION_ITEM_SPENT AS

SELECT md5(json_extract_path_text (DATA,'user_id')::varchar||json_extract_path_text (DATA,'ts')::VARCHAR) AS mission_key,
it.item_name AS item_name,
it.item_class AS item_class,
it.item_type AS item_type,
it.item_amount AS item_amount
FROM events
  LEFT JOIN item_spent it ON md5 (DATA) = it.event_id
  WHERE it.item_name is not null;

GRANT SELECT ON FACT_MISSION to fruitscoot_dev;
GRANT SELECT ON FACT_MISSION_OBJECTIVE to fruitscoot_dev;
GRANT SELECT ON FACT_MISSION_ITEM_SPENT to fruitscoot_dev;


--------------------------------------- App_User Start ----------------------------------------------

---------------------------------------USER COUNTRY FOR USERS ---------------------------------------

--Get the country for each user


CREATE TEMP TABLE USER_COUNTRY
as
select app
       ,uid
       ,snsid
       ,install_ts
       ,country_code

from(
SELECT           app
                ,uid
                ,snsid
                ,install_ts
                ,country_code
                ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events
where event <> 'payment'
and   trunc(ts) = ?::DATE
)t
where rank = 1;

--Get payer,conversion_ts,and last_login_ts info

CREATE TEMP TABLE payer_user_info
as
select app,
       uid,
       snsid,
       min(ts) as conversion_ts
FROM events 
where event = 'payment'
and   trunc(ts)= ?::DATE
group by 1,2,3;


--Get other user info for app users
truncate table app_user_raw;

insert into app_user_raw (app,uid,snsid,install_ts,install_source,os,os_version,country_code,level,language,browser,browser_version,latest_login_ts)
select t1.app,
       t1.uid,
       t1.snsid,
       t1.install_ts,
       t1.install_source,
       t1.os,
       t1.os_version,
       t2.country_code,
       t1.level,
       t1.language,
       t1.browser,
       t1.browser_version,
       t1.ts as latest_login_ts
from   
(select app,
       uid,
       snsid,
       install_source,
       install_ts,
       os,
       os_version,
       level,
       language,
       browser,
       browser_version,
       ts,
       row_number() over(partition by app,uid,snsid order by install_ts desc) as rank
from
       (select app,
              uid,
              snsid,  --sortkey
              install_source,
              install_ts,  --diskey
              os,
              os_version,
              max(cast (json_extract_path_text(properties,'level') as int)) as level,
              json_extract_path_text(properties,'language') as language,
              browser,
              browser_version,
              ts
       from  events
       where trunc(ts) = ?::DATE
       group by 1,2,3,4,5,6,7,9,10,11,12
       )
)t1
left join USER_COUNTRY t2
on t1.app = t2.app
and t1.snsid = t2.snsid
and t1.uid = t2.uid
where t1.rank=1;

update app_user_raw set conversion_ts=app_user.conversion_ts from app_user where app_user_raw.uid=app_user.uid and app_user_raw.snsid=app_user.snsid and app_user_raw.app=app_user.app;



update app_user_raw set conversion_ts=payer_user_info.conversion_ts from payer_user_info where app_user_raw.uid=payer_user_info.uid and app_user_raw.snsid=payer_user_info.snsid and app_user_raw.app=payer_user_info.app
and app_user_raw.conversion_ts is null;

--delete the old data of app user

delete from app_user where (app,uid,snsid) in (select app,uid,snsid from app_user_raw);

insert into app_user
(
       user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       browser,
       browser_version,
       conversion_ts,
       latest_login_ts
)
select md5(app||uid||snsid),
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       browser,
       browser_version,
       conversion_ts,
       latest_login_ts
from app_user_raw
;

--update the payer flag
update app_user set is_payer = true where conversion_ts is not null;

------------------------------------------dau--------------------------------------------------
delete from dau where dt = ?::DATE;

INSERT INTO dau
select md5(app||uid||snsid),
       app,
       dt,
       uid,
       snsid,
       install_source,
       install_dt,
       os,
       os_version,
       country_code,
       level,
       browser,
       browser_version
from
   (select app,
           dt,
           uid,
           snsid,
           install_source,
           install_dt,
           os,
           os_version,
           country_code,
           level,
           browser,
           browser_version,
           row_number() over(partition by app,uid,snsid order by dt) as rank
     from
             (SELECT e.app,
                    trunc(e.ts) as dt,
                    e.uid,
                    e.snsid,
                    a.install_source, 
                    trunc(e.install_ts) AS install_dt,
                    a.os,
                    a.os_version,
                    a.country_code,
                    max(cast(json_extract_path_text(properties,'level') AS integer)) AS LEVEL,
                    a.browser,
                    a.browser_version
             FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
             WHERE trunc(e.ts)=?::DATE
             GROUP BY 1,2,3,4,5,6,7,8,9,11,12
             )
     )t
where t.rank=1;     

-------------------------------------------------Payment--------------------------------------------------------------------------------------         

delete from payment where trunc(ts)=?::DATE;


INSERT INTO payment(user_id,uid,snsid,app,ts,install_dt,install_source,os,os_version,browser,browser_version,country_code,payment_processor,product_id,is_gift,product_name,rc_in,product_type,lang,currency,transaction_id,amount,level,coins_in,rc_bal,coins_bal)
SELECT md5(e.app||e.uid||e.snsid),
       e.uid,
       e.snsid,
       e.app,
       e.ts,
       trunc(a.install_ts) as install_dt,
       a.install_source,
       a.os,
       a.os_version,
       a.browser,
       a.browser_version,
       a.country_code,
       json_extract_path_text(properties,'payment_processor') as payment_processor,
       json_extract_path_text(properties,'product_id') as product_id,
       json_extract_path_text(properties,'is_gift') as is_gift,
       json_extract_path_text(properties,'product_name') as product_name,
       cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as integer) as rc_in,
       json_extract_path_text(properties,'product_type') as product_type,
       json_extract_path_text(properties,'lang') as lang,
       json_extract_path_text(properties,'currency') as currency,
       json_extract_path_text(properties,'transaction_id') as transaction_id,
       json_extract_path_text(properties,'amount')::float/100::float * c.factor AS amount,
       cast(json_extract_path_text(properties,'level') as integer) as level,
       cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as integer) as coins_in,
       cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') end as integer) as rc_bal,
       cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text(properties,'coins_bal') end as integer) as coins_bal
FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
left join currency c on json_extract_path_text(properties,'currency') = c.currency and trunc(e.ts)=c.dt
where e.event = 'payment' and   trunc(e.ts)=?::DATE
;

-----------------------kpi_raw--------------------------------------------------------------------------------------



truncate table kpi_raw;
----login cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,login_cnt,event)
SELECT e.app,
       trunc(e.ts),
       trunc(e.install_ts),
       e.install_source,
       e.os,
       e.browser,
       a.country_code,
       count(1) AS login_cnt,
       'session_start'
FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
WHERE e.event='session_start'
  AND trunc(e.ts) = ?::DATE
GROUP BY 1,2,3,4,5,6,7;

--dau cnt

INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,dau_cnt,event)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       count(DISTINCT user_id) AS dau_cnt,
       'dau'
FROM dau
where dt = ?::DATE
GROUP BY 1,2,3,4,5,6,7;

--newuser_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,newuser_cnt,event)
SELECT app,
       trunc(install_ts),
       trunc(install_ts),
       install_source,
       os,
       browser,
       country_code,
       count(1) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY 1,2,3,4,5,6,7;


--payer_dau_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,payer_dau_cnt,browser,event)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       count(d.USER_ID) AS payer_dau_cnt,
       d.browser,
       'payer_dau'
FROM dau d 
INNER JOIN app_user a ON d.user_id = a.user_id 
and a.is_payer is true
WHERE d.dt=?::DATE
GROUP BY 1,2,3,4,5,6,8;

--newpayer_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,newpayer_cnt,event)
SELECT app,
       trunc(conversion_ts),
       trunc(install_ts),
       install_source,
       os,
       browser,
       country_code,
       count(user_id) AS newpayer_cnt,
       'newpayers'
FROM app_user
where trunc(conversion_ts) = ?::DATE and
 is_payer is true
group by 1,2,3,4,5,6,7
;


--revenue
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,amount,event)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       sum(amount) as amount,
       'payment'
from   payment
where  trunc(ts) = ?::DATE   
GROUP by 1,2,3,4,5,6,7;
  
---------kpi----------------------------------------------------------------------------------------
delete from kpi where dt = ?::DATE;

INSERT INTO kpi(app,dt,install_dt,install_source,os,browser,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_raw
where dt = ?::DATE
GROUP BY 1,2,3,4,5,6,7;

---------------------transaction---------------------------------------------------------------------------------


DELETE FROM rc_transaction where trunc(ts)=?::DATE;


insert into rc_transaction(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,rc_in,rc_out,rc_bal,level,location,action,action_detail)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as float) as "rc_in" ,
cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') end as float) as 
"rc_out" ,
cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') end as float) as 
"rc_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail from events where event='rc_transaction' 
and trunc(ts)=?::DATE
;



DELETE FROM rpt_rc where dt=?::DATE;


insert into rpt_rc(app,dt,install_dt,install_source,country_code,browser,browser_version,os,os_version,rc_in,rc_out,rc_bal,level,location,action,action_detail)
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(rc_in) ,sum(rc_out) ,sum(rc_bal) ,
 level, location,action, action_detail from rc_transaction where trunc(ts)=?::DATE
group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version, level, location,action, 
action_detail;



DELETE FROM coin_transaction where trunc(ts)=?::DATE;


insert into coin_transaction(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,coins_in,coins_out,coins_bal,level,location,action,action_detail)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as bigint) as 
"coins_in" ,
cast(case when json_extract_path_text(properties,'coins_out')='' then '0' else json_extract_path_text(properties,'coins_out') end as bigint) as 
"coins_out" ,
cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text(properties,'coins_bal') end as bigint) as 
"coins_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail from events where event='coins_transaction'  and trunc(ts)=?::DATE
 ;




DELETE FROM rpt_coin where dt=?::DATE;

insert into rpt_coin(app,dt,install_dt,install_source,country_code,browser,browser_version,os,os_version,coins_in,coins_out,coins_bal,level,location,action,action_detail)
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(coins_in) ,sum(coins_out) ,sum(coins_bal) ,
 level,location,action, action_detail from coin_transaction WHERE trunc(ts)=?::DATE
group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version, level,location,action, 
action_detail;


DELETE FROM quest_completion where trunc(ts)=?::DATE;

insert into quest_completion(uid,snsid,app,ts,install_source,os,browser,install_dt,browser_version,country_code,os_version,coins_in,coins_out,coins_bal,rc_in,rc_out,rc_bal,quest_id,lang,level,action)
select e.uid,e.snsid,e.app,e.ts,e.install_source,e.os,e.browser,trunc(e.install_ts), e.browser_version ,e.country_code,e.os_version,
cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as float) as 
"coins_in" ,
cast(case when json_extract_path_text(properties,'coins_out')='' then '0' else json_extract_path_text(properties,'coins_out') end as float) as 
"coins_out" ,
cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text(properties,'coins_bal') end as float) as 
"coins_bal",
cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as integer) as 
"rc_in",
cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') end as integer) as 
"rc_out",
cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') end as integer) as 
"rc_bal",
json_extract_path_text(properties, 'quest_id') as "quest_id",
json_extract_path_text(properties, 'lang') as "lang",
json_extract_path_text(properties, 'level') as "level",
json_extract_path_text(properties, 'action') as "action"
from events e where e.event='quest'
and trunc(ts)=?::DATE;


create temp table temp_rpt_tutorial
as 
SELECT t.user_id,
       t.snsid ,
       t.uid,
       t.max_dt ,
       t.step,
       t.app ,
       t.t_snsid,
       t.install_source,
       t.os,
       t.install_dt,
       t.country_code,
       t.browser,
       t.browser_version,
       t.tutorial_type,
       c.country
FROM 
  (SELECT a.snsid,
          a.user_id,
          a.uid,
          e.snsid AS t_snsid,
          trunc(e.max_ts) AS max_dt,
          a.country_code,
          trunc(a.install_ts) AS install_dt,
          a.os,
          a.install_source,
          a.app,
          a.browser,
          a.browser_version,
          e.step,
          e.tutorial_type
   FROM app_user a
   LEFT JOIN 
      (select app,
             uid,
             snsid,
             max(ts) as max_ts,
             json_extract_path_text(properties,'step') as step,
             json_extract_path_text(properties,'abid') as tutorial_type
      from   events
      where  event = 'tutorial' and trunc(ts)=?::DATE
      group by 1,2,3,5,6
      )e
   on a.app=e.app
   and a.uid=e.uid
   and a.snsid=e.snsid
  )t 
LEFT JOIN country c ON t.country_code=c.country_code;

delete from rpt_tutorial where (user_id,step) in (select user_id, step from temp_rpt_tutorial);

insert into rpt_tutorial(user_id,snsid,uid,max_dt,step,app,t_snsid,install_source,os,install_dt,country_code,tutorial_type,browser,browser_version,country)
select user_id,snsid,uid,max_dt,step,app,t_snsid,install_source,os,install_dt,country_code,tutorial_type,browser,browser_version,country from temp_rpt_tutorial;


DELETE FROM item_transaction where trunc(ts)=?::DATE;
insert into item_transaction(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,item_in,item_out,item_bal,level,location,action,action_detail,item_name,item_class,item_type,item_id)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as float) as 
"item_in" ,
cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as float) as 
"item_out" ,
cast(case when json_extract_path_text(properties,'item_bal')='' then '0' else json_extract_path_text(properties,'item_bal') end as float) as 
"item_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail,
json_extract_path_text(properties,'item_name') as item_name,
json_extract_path_text(properties,'item_class') as item_class,
json_extract_path_text(properties,'item_type') as item_type,
json_extract_path_text(properties,'item_id') as item_id
 from events where event='item_transaction'
 and trunc(ts)=?::DATE;

delete from rpt_item where dt=?::DATE;

insert into rpt_item(app,dt,install_dt,install_source,country_code,browser,browser_version,os,os_version,item_in,item_out,item_bal,level,location,action,action_detail,item_name,item_class,item_type,item_id)
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(item_in) ,sum(item_out) ,sum(item_bal) ,level, location,action,action_detail,item_name,item_class,item_type,item_id
 from item_transaction where trunc(ts)=?::DATE
 group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
 level, location,action,action_detail,item_name,item_class,item_type,item_id;

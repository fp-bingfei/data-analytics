create table rs.public.player_day as
select 1 as day
union all
select 2 as day
union all
select 3 as day
union all
select 4 as day
union all
select 5 as day
union all
select 6 as day
union all
select 7 as day
union all
select 14 as day
union all
select 21 as day
union all
select 28 as day
union all
select 30 as day
union all
select 45 as day
union all
select 60 as day
union all
select 90 as day
union all
select 120 as day
union all
select 150 as day
union all
select 180 as day
union all
select 210 as day
union all
select 240 as day
union all
select 270 as day
union all
select 300 as day
union all
select 360 as day;

-- Retention
 drop table rs.public.tmp_player_day_cube;
 drop table rs.public.tmp_new_user;
 drop table rs.public.tmp_new_user_info;
 drop table rs.public.tmp_retention_base;
 drop table rs.public.tab_retention;

create table rs.public.tmp_new_user as
select
    date(u.install_ts) as install_date, os,app,
    coalesce(country,'Unknown') as country,install_source,
    browser,
    is_payer,
    count(user_id) as newuser_cnt
from  rs.public.app_user u
left join rs.public.country c on c.country_code=u.country_code
where install_ts is not null and trunc(install_ts)>='2014-7-16'
group by install_date, os,country,install_source,is_payer, browser,app;

create table rs.public.tmp_player_day_cube as
with last_date as (select max(dt) as date from rs.public.dau)
select su.*, rd.day as player_day,  d.date as max_date
from rs.public.tmp_new_user su
join rs.public.player_day rd on 1 = 1
join last_date d on 1=1
where DATEDIFF('day', install_date, d.date) >= player_day;

create table rs.public.tmp_retention_base as
with last_date as (select max(dt) as date from rs.public.dau)
SELECT
  DATEDIFF('day', trunc(u.install_ts), d.dt) AS player_day,trunc(u.install_ts) as install_date,
  u.os,  coalesce(c.country,'Unknown') as country, u.install_source,u.app,
  u.browser,
  u.is_payer,
  count(distinct d.user_id) as retaineduser_cnt
FROM rs.public.dau d
join rs.public.app_user u on d.user_id=u.user_id
join last_date l on 1=1
left join rs.public.country c on c.country_code=u.country_code
where u.install_ts is not null and trunc(u.install_ts)>='2014-7-16' and DATEDIFF('day', trunc(u.install_ts), d.dt) in (select day from rs.public.player_day)
group by trunc(u.install_ts), d.dt, u.os, country, u.install_source, u.is_payer, u.browser,u.app;

create table rs.public.tab_retention as
select  c.*,coalesce(retaineduser_cnt,0) as retaineduser_cnt
from  rs.public.tmp_player_day_cube c
left join rs.public.tmp_retention_base rb
on  c.player_day = rb.player_day and c.install_date = rb.install_date and
    c.os = rb.os and c.country = rb.country and
    c.install_source = rb.install_source and
    c.is_payer = rb.is_payer  and c.browser = rb.browser and c.app=rb.app;



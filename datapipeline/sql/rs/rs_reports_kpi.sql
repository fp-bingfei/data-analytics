--------------------------------------- App_User Start ----------------------------------------------

---------------------------------------USER COUNTRY FOR USERS ---------------------------------------

--Get the country for each user


CREATE TEMP TABLE USER_COUNTRY
as
select app
       ,uid
       ,snsid
       ,install_ts
       ,country_code

from(
SELECT           app
                ,uid
                ,snsid
                ,install_ts
                ,country_code
                ,row_number() OVER (partition by app,uid,snsid,install_ts order by ts desc) as rank
from events
where event <> 'payment'
and   trunc(ts) = ?::DATE
)t
where rank = 1;

--Get payer,conversion_ts,and last_login_ts info

CREATE TEMP TABLE payer_user_info
as
select app,
       uid,
       snsid,
       min(ts) as conversion_ts
FROM events 
where event = 'payment'
and   trunc(ts)= ?::DATE
group by 1,2,3;


--Get other user info for app users
truncate table app_user_raw;

insert into app_user_raw (app,uid,snsid,install_ts,install_source,os,os_version,country_code,level,language,browser,browser_version,latest_login_ts)
select t1.app,
       t1.uid,
       t1.snsid,
       t1.install_ts,
       t1.install_source,
       t1.os,
       t1.os_version,
       t2.country_code,
       t1.level,
       t1.language,
       t1.browser,
       t1.browser_version,
       t1.ts as latest_login_ts
from   
(select app,
       uid,
       snsid,
       install_source,
       install_ts,
       os,
       os_version,
       level,
       language,
       browser,
       browser_version,
       ts,
       row_number() over(partition by app,uid,snsid order by install_ts desc) as rank
from
       (select app,
              uid,
              snsid,  --sortkey
              install_source,
              install_ts,  --diskey
              os,
              os_version,
              MAX(CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer)) as level,
              json_extract_path_text(properties,'langs') as language,
              browser,
              browser_version,
              ts
       from  events
       where trunc(ts) = ?::DATE
       group by 1,2,3,4,5,6,7,9,10,11,12
       )
)t1
left join USER_COUNTRY t2
on t1.app = t2.app
and t1.snsid = t2.snsid
and t1.uid = t2.uid
where t1.rank=1;


update app_user_raw set conversion_ts=payer_user_info.conversion_ts from payer_user_info where app_user_raw.uid=payer_user_info.uid and app_user_raw.snsid=payer_user_info.snsid and app_user_raw.app=payer_user_info.app
and app_user_raw.conversion_ts is null;


update app_user_raw set conversion_ts=app_user.conversion_ts from app_user where app_user_raw.uid=app_user.uid and app_user_raw.snsid=app_user.snsid and app_user_raw.app=app_user.app;




--delete the old data of app user

delete from app_user where (app,uid,snsid) in (select app,uid,snsid from app_user_raw);

insert into app_user
(
       user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       browser,
       browser_version,
       conversion_ts,
       latest_login_ts
)
select md5(app||uid||snsid),
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       browser,
       browser_version,
       conversion_ts,
       latest_login_ts
from app_user_raw
;

--update the payer flag
update app_user set is_payer = true where conversion_ts is not null;

------------------------------------------dau--------------------------------------------------
delete from dau where dt = ?::DATE;

INSERT INTO dau
select md5(app||uid||snsid),
       app,
       dt,
       uid,
       snsid,
       install_source,
       install_dt,
       os,
       os_version,
       country_code,
       level,
       browser,
       browser_version,
       ab_data
from
   (select app,
           dt,
           uid,
           snsid,
           install_source,
           install_dt,
           os,
           os_version,
           country_code,
           level,
           browser,
           browser_version,
           ab_data,
           row_number() over(partition by app,uid,snsid order by dt) as rank
     from
             (SELECT e.app,
                    trunc(e.ts) as dt,
                    e.uid,
                    e.snsid,
                    a.install_source, 
                    trunc(e.install_ts) AS install_dt,
                    a.os,
                    a.os_version,
                    a.country_code,
                    MAX(CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer)) as level,
                    json_extract_path_text(properties,'ab_data') as ab_data,
                    a.browser,
                    a.browser_version
             FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
             WHERE trunc(e.ts)=?::DATE
             GROUP BY 1,2,3,4,5,6,7,8,9,11,12,13
             )
     )t
where t.rank=1;     

-------------------------------------------------Payment--------------------------------------------------------------------------------------         

delete from payment where trunc(ts)=?::DATE;


INSERT INTO payment(user_id,uid,snsid,app,ts,install_dt,install_source,os,os_version,browser,browser_version,country_code,payment_processor,product_id,is_gift,product_name,rc_in,product_type,lang,currency,transaction_id,amount,level,coins_in,rc_bal,coins_bal)
SELECT md5(e.app||e.uid||e.snsid),
       e.uid,
       e.snsid,
       e.app,
       e.ts,
       trunc(a.install_ts) as install_dt,
       a.install_source,
       a.os,
       a.os_version,
       a.browser,
       a.browser_version,
       a.country_code,
       json_extract_path_text(properties,'payment_processor') as payment_processor,
       json_extract_path_text(properties,'product_id') as product_id,
       json_extract_path_text(properties,'is_gift') as is_gift,
       json_extract_path_text(properties,'product_name') as product_name,
       cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as integer) as rc_in,
       json_extract_path_text(properties,'product_type') as product_type,
       json_extract_path_text(properties,'lang') as lang,
       json_extract_path_text(properties,'currency') as currency,
       json_extract_path_text(properties,'transaction_id') as transaction_id,
       json_extract_path_text(properties,'amount')::float/100::float * c.factor AS amount,
       CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text(properties,'level') END AS integer) as level,
       cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as integer) as coins_in,
       cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') end as integer) as rc_bal,
       cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text(properties,'coins_bal') end as integer) as coins_bal
FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
left join currency c on json_extract_path_text(properties,'currency') = c.currency and trunc(e.ts)=c.dt
where e.event = 'payment' and   trunc(e.ts)=?::DATE
;

-----------------------kpi_raw--------------------------------------------------------------------------------------



truncate table kpi_raw;
----login cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,login_cnt,event)
SELECT e.app,
       trunc(e.ts),
       trunc(e.install_ts),
       e.install_source,
       e.os,
       e.browser,
       a.country_code,
       count(1) AS login_cnt,
       'session_start'
FROM events e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
WHERE e.event='session_start'
  AND trunc(e.ts) = ?::DATE
GROUP BY 1,2,3,4,5,6,7;

--dau cnt

INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,dau_cnt,event)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       count(DISTINCT user_id) AS dau_cnt,
       'dau'
FROM dau
where dt = ?::DATE
GROUP BY 1,2,3,4,5,6,7;

--newuser_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,newuser_cnt,event)
SELECT app,
       trunc(install_ts),
       trunc(install_ts),
       install_source,
       os,
       browser,
       country_code,
       count(1) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY 1,2,3,4,5,6,7;


--payer_dau_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,payer_dau_cnt,browser,event)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       count(d.USER_ID) AS payer_dau_cnt,
       d.browser,
       'payer_dau'
FROM dau d 
INNER JOIN app_user a ON d.user_id = a.user_id 
and a.is_payer is true
WHERE d.dt=?::DATE
GROUP BY 1,2,3,4,5,6,8;

--newpayer_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,newpayer_cnt,event)
SELECT app,
       trunc(conversion_ts),
       trunc(install_ts),
       install_source,
       os,
       browser,
       country_code,
       count(user_id) AS newpayer_cnt,
       'newpayers'
FROM app_user
where trunc(conversion_ts) = ?::DATE and
 is_payer is true
group by 1,2,3,4,5,6,7
;


--revenue
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,amount,event)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       sum(amount) as amount,
       'payment'
from   payment
where  trunc(ts) = ?::DATE   
GROUP by 1,2,3,4,5,6,7;
  
---------kpi----------------------------------------------------------------------------------------
delete from kpi where dt = ?::DATE;

INSERT INTO kpi(app,dt,install_dt,install_source,os,browser,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_raw
where dt = ?::DATE
GROUP BY 1,2,3,4,5,6,7;


--------ab_data table---
truncate table ab_data;

insert into ab_data
select t.user_id,
t.ab_data
from ( select user_id,
ab_data,
row_number() over(partition by app,uid,snsid order by dt desc) as rank
from dau)t where t.rank=1;

-------session report table-------
delete from rpt_session where trunc(ts_start)=?::DATE;

insert into rpt_session 
select md5(t.app||t.uid||t.snsid) as user_id,
t.session_id,
t.app,
t.uid,
t.snsid,
trunc(t.ts_start) as dt,
t.ts_start,
a.ts_end,
t.install_dt,
t.install_source,
t.country_code,t.os,t.browser,t.level_start from 
(select app,uid,snsid,ts as ts_start,install_source,trunc(install_ts) as install_dt,
country_code,os,browser,json_extract_path_text(properties,'level') as level_start ,json_extract_path_text(properties,'session_id') as session_id
from events where event='session_start' and trunc(ts)=?::DATE and  json_extract_path_text(properties,'session_id')<>'')t
inner join (select app,uid,snsid,max(ts) as ts_end,json_extract_path_text(properties,'session_id') as session_id
from events where event='session_active' and  trunc(ts)=?::DATE and json_extract_path_text(properties,'session_id')<>''
group by 1,2,3,5 )a on t.app=a.app and t.uid=a.uid and t.snsid=a.snsid and t.session_id=a.session_id;














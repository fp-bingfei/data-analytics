create temp table payment_info as
select t.app,t.snsid,(t.pay_times+t.pay_time) as pay_times,(t.pay_amount+t.revenue) as revenue from 
(
select f.app,f.snsid,cast(case when f.pay_times is null then '0' else f.pay_times end as integer) as pay_times,cast(case when p.pay_time is null then '0' else p.pay_time end as integer) as pay_time ,
cast(case when f.pay_amount is null then '0' else f.pay_amount end as float) as  pay_amount,cast(case when p.pay_amount is null then '0' else p.pay_amount end as float) as revenue 
 from app_user_info f left join  (select app,snsid,count(ts) as pay_time,sum(amount) as pay_amount from payment  where trunc(ts)=?::DATE group by app,snsid)p on f.app=p.app and f.snsid=p.snsid)t;

delete from app_user_info where (app,snsid) in (select app,snsid from tbl_user);
insert into app_user_info (id,app,snsid,pay_times,pay_amount,email,gender,age,name,coins_bal,rc_bal)
select md5(app||snsid),
t.app,
t.snsid,
t.pay_times,
t.pay_amount,
t.email,
t.gender,
t.age ,
t.name,
t.coins_bal,t.rc_bal from
(select u.app,u.snsid,u.email,u.gender,u.age_num as age,u.name,cast(case when coins='NULL' then '0' else coins end as integer) as coins_bal,cast(case when rc='NULL' then '0' else rc end as integer) as rc_bal,p.pay_times,p.revenue as pay_amount from tbl_user u
left join (select app,snsid,count(ts) as pay_times ,sum(amount) as revenue from payment where trunc(ts)=?::DATE group by app,snsid )p on u.app=p.app and u.snsid=p.snsid)t;



update app_user_info set pay_times=payment_info.pay_times from payment_info where app_user_info.app=payment_info.app and app_user_info.snsid=payment_info.snsid;

update app_user_info set pay_amount=payment_info.revenue from payment_info where app_user_info.app=payment_info.app and app_user_info.snsid=payment_info.snsid;

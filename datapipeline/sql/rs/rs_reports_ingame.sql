---------------------transaction---------------------------------------------------------------------------------


DELETE FROM rc_transaction where trunc(ts)=?::DATE;


insert into rc_transaction(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,rc_in,rc_out,rc_bal,level,location,action,action_detail)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as float) as "rc_in" ,
cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') end as float) as 
"rc_out" ,
cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') end as float) as 
"rc_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail from events where event='rc_transaction' 
and trunc(ts)=?::DATE
;



DELETE FROM rpt_rc where dt=?::DATE;


insert into rpt_rc(app,dt,install_dt,install_source,country_code,browser,browser_version,os,os_version,rc_in,rc_out,rc_bal,level,location,action,action_detail)
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(rc_in) ,sum(rc_out) ,sum(rc_bal) ,
 level, location,action, action_detail from rc_transaction where trunc(ts)=?::DATE
group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version, level, location,action, 
action_detail;



DELETE FROM coin_transaction where trunc(ts)=?::DATE;


insert into coin_transaction(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,coins_in,coins_out,coins_bal,level,location,action,action_detail)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as bigint) as 
"coins_in" ,
cast(case when json_extract_path_text(properties,'coins_out')='' then '0' else json_extract_path_text(properties,'coins_out') end as bigint) as 
"coins_out" ,
cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text(properties,'coins_bal') end as bigint) as 
"coins_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail from events where event='coins_transaction'  and trunc(ts)=?::DATE
 ;




DELETE FROM rpt_coin where dt=?::DATE;

insert into rpt_coin(app,dt,install_dt,install_source,country_code,browser,browser_version,os,os_version,coins_in,coins_out,coins_bal,level,location,action,action_detail)
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(coins_in) ,sum(coins_out) ,sum(coins_bal) ,
 level,location,action, action_detail from coin_transaction WHERE trunc(ts)=?::DATE
group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version, level,location,action, 
action_detail;


DELETE FROM quest_completion where trunc(ts)=?::DATE;

insert into quest_completion(uid,snsid,app,ts,install_source,os,browser,install_dt,browser_version,country_code,os_version,coins_in,coins_out,coins_bal,rc_in,rc_out,rc_bal,quest_id,lang,level,action)
select e.uid,e.snsid,e.app,e.ts,e.install_source,e.os,e.browser,trunc(e.install_ts), e.browser_version ,e.country_code,e.os_version,
cast(case when json_extract_path_text(properties,'coins_in')='' then '0' else json_extract_path_text(properties,'coins_in') end as float) as 
"coins_in" ,
cast(case when json_extract_path_text(properties,'coins_out')='' then '0' else json_extract_path_text(properties,'coins_out') end as float) as 
"coins_out" ,
cast(case when json_extract_path_text(properties,'coins_bal')='' then '0' else json_extract_path_text(properties,'coins_bal') end as float) as 
"coins_bal",
cast(case when json_extract_path_text(properties,'rc_in')='' then '0' else json_extract_path_text(properties,'rc_in') end as integer) as 
"rc_in",
cast(case when json_extract_path_text(properties,'rc_out')='' then '0' else json_extract_path_text(properties,'rc_out') end as integer) as 
"rc_out",
cast(case when json_extract_path_text(properties,'rc_bal')='' then '0' else json_extract_path_text(properties,'rc_bal') end as integer) as 
"rc_bal",
json_extract_path_text(properties, 'quest_id') as "quest_id",
json_extract_path_text(properties, 'lang') as "lang",
json_extract_path_text(properties, 'level') as "level",
json_extract_path_text(properties, 'action') as "action"
from events e where e.event='quest'
and trunc(ts)=?::DATE;


create temp table temp_rpt_tutorial
as 
SELECT t.user_id,
       t.snsid ,
       t.uid,
       t.start_ts ,
       t.end_ts,
       t.step,
       t.app ,
       t.t_snsid,
       t.install_source,
       t.os,
       t.install_dt,
       t.country_code,
       t.browser,
       t.browser_version,
       t.tutorial_type,
       c.country
FROM 
  (SELECT a.snsid,
          a.user_id,
          a.uid,
          e.snsid AS t_snsid,
          e.start_ts AS start_ts,
          e1.end_ts as end_ts,
          a.country_code,
          trunc(a.install_ts) AS install_dt,
          a.os,
          a.install_source,
          a.app,
          a.browser,
          a.browser_version,
          e.step,
          e.tutorial_type
   FROM app_user a
   LEFT JOIN 
      (select app,
             uid,
             snsid,
             max(ts) as start_ts,
             cast (json_extract_path_text(properties,'step') as int) as step,
             json_extract_path_text(properties,'abid') as tutorial_type
      from   events
      where  event = 'tutorial' and trunc(ts)=?::DATE
      and position('.' in json_extract_path_text(properties,'step'))=0
      group by 1,2,3,5,6
      )e
   on a.app=e.app
   and a.uid=e.uid
   and a.snsid=e.snsid
   left join
      (select app,
              uid,
              snsid,
              max(ts) as end_ts,
              cast (json_extract_path_text(properties,'step') as int) as step,
              json_extract_path_text(properties,'abid') as tutorial_type
              from events
              where event='tutorial' and trunc(ts)=?::DATE
              and position('.' in json_extract_path_text(properties,'step'))=0
              group by 1,2,3,5,6)e1
              on e.app=e1.app 
              and e.uid=e1.uid 
              and e.snsid=e1.snsid 
              and e.step+1=e1.step 
              and e.tutorial_type=e1.tutorial_type and e1.end_ts>=e.start_ts
              
 )t 
LEFT JOIN country c ON t.country_code=c.country_code ;

delete from rpt_tutorial where (user_id,step) in (select user_id, step from temp_rpt_tutorial);


insert into rpt_tutorial(user_id,snsid,uid,step,start_ts,end_ts,app,t_snsid,install_source,os,install_dt,country_code,tutorial_type,browser,
browser_version,country)
select user_id,
snsid,
uid,
step,
start_ts,
end_ts,
app,
t_snsid,
install_source,
os,
install_dt,
country_code,
tutorial_type,
browser,
browser_version,
country from temp_rpt_tutorial;


DELETE FROM item_transaction where trunc(ts)=?::DATE;
insert into item_transaction(app,ts,snsid,uid,install_dt,install_source,country_code,browser,browser_version,os,os_version,item_in,item_out,item_bal,level,location,action,action_detail,item_name,item_class,item_type,item_id)
select app,ts,snsid,uid,trunc(install_ts),install_source,country_code,browser,browser_version,os,os_version,
cast(case when json_extract_path_text(properties,'item_in')='' then '0' else json_extract_path_text(properties,'item_in') end as float) as 
"item_in" ,
cast(case when json_extract_path_text(properties,'item_out')='' then '0' else json_extract_path_text(properties,'item_out') end as float) as 
"item_out" ,
cast(case when json_extract_path_text(properties,'item_bal')='' then '0' else json_extract_path_text(properties,'item_bal') end as float) as 
"item_bal" ,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'location') as location,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'action_detail') as action_detail,
json_extract_path_text(properties,'item_name') as item_name,
json_extract_path_text(properties,'item_class') as item_class,
json_extract_path_text(properties,'item_type') as item_type,
json_extract_path_text(properties,'item_id') as item_id
 from events where event='item_transaction'
 and trunc(ts)=?::DATE;

delete from rpt_item where dt=?::DATE;

insert into rpt_item(app,dt,install_dt,install_source,country_code,browser,browser_version,os,os_version,item_in,item_out,item_bal,level,location,action,action_detail,item_name,item_class,item_type,item_id)
select app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
sum(item_in) ,sum(item_out) ,sum(item_bal) ,level, location,action,action_detail,item_name,item_class,item_type,item_id
 from item_transaction where trunc(ts)=?::DATE
 group by  app,trunc(ts),install_dt,install_source,country_code,browser,browser_version,os,os_version,
 level, location,action,action_detail,item_name,item_class,item_type,item_id;





-------------coins_rc_wallet----------

create temp table coins_bal as
select t.app,
t.snsid,
t.uid,
trunc(t.ts) as dt,
t.install_dt,
t.level,
t.coins_bal from
(
select app,uid,
snsid,
install_dt,
level,
coins_bal,
ts ,
row_number() OVER (partition by app, uid, snsid order by ts desc) as row 
from coin_transaction 
where trunc(ts)=?::DATE

)t where t.row=1;






create temp table rc_bal as
select t.app,
t.snsid,
t.uid,
trunc(t.ts) as dt,
t.install_dt,
t.level,
t.rc_bal from
(
select app,uid,
snsid,
install_dt,
level,
rc_bal,
ts,
row_number() OVER (partition by app, uid, snsid order by ts desc) as row 
from rc_transaction
where trunc(ts)=?::DATE 
)t where t.row=1;


delete from coins_rc_wallet where dt=?::DATE;



insert into coins_rc_wallet (user_id,app,dt,uid,snsid,install_source,install_dt,os,os_version,country_code,level,browser)
select user_id,app,dt,uid,snsid,install_source,install_dt,os,os_version,country_code,level,browser  from dau
where dt=?::DATE
;





update coins_rc_wallet  set coins_bal=b.coins_bal from coins_bal b 
where coins_rc_wallet.app=b.app
and coins_rc_wallet.snsid=b.snsid
and coins_rc_wallet.uid=b.uid
and coins_rc_wallet.dt=b.dt
and coins_rc_wallet.install_dt=b.install_dt
;



update coins_rc_wallet  set rc_bal=b.rc_bal from rc_bal b 
where coins_rc_wallet.app=b.app
and coins_rc_wallet.snsid=b.snsid
and coins_rc_wallet.uid=b.uid
and coins_rc_wallet.dt=b.dt
and coins_rc_wallet.install_dt=b.install_dt
;
create temp table payment_info as
select t.app,
       t.uid,
       t.snsid,
       (t.pay_times+t.pay_time) as pay_times,
       (t.pay_amount+t.revenue) as revenue 
from 
    (
     select f.app,
            f.snsid,
            f.uid,
            cast(case when f.pay_times is null then '0' else f.pay_times end as integer) as pay_times,
            cast(case when p.pay_time is null then '0' else p.pay_time end as integer) as pay_time ,
            cast(case when f.pay_amount is null then '0' else f.pay_amount end as float) as  pay_amount,
            cast(case when p.pay_amount is null then '0' else p.pay_amount end as float) as revenue 
     from    app_user_info f 
     left join (select   app,
                         uid,
                         snsid,
                         count(ts) as pay_time,
                         sum(amount) as pay_amount 
                 from    payment  
                 where   trunc(ts)=?::DATE 
                 group by app,uid,snsid
                  )p 
        on f.app=p.app and f.snsid=p.snsid and f.uid=p.uid
     )t;

delete from app_user_info where (app,uid,snsid) in (select app,uid,snsid from tbl_user);
delete from app_user_info where (app,uid,snsid) in (select app,uid,snsid from tbl_user_th);


insert into app_user_info (id,app,uid,snsid,email,gender,age,name,coins_bal,rc_bal,pay_times,pay_amount)
select md5(app||uid||snsid),
       t.app,
       t.uid,
       t.snsid,
       t.email,
       t.gender,
       t.age ,
       t.name,
       t.coins_bal,
       t.rc_bal,
       t.pay_times,
       t.revenue 
from
    (select u.app,
            u.uid,
            u.snsid,
            u.email,
            u.gender,
            u.age_num as age,
            u.name,
            p.pay_times,
            p.revenue,
            u.coins as coins_bal,
            u.rc as rc_bal 
     from tbl_user u
     left join (select app,uid,
                      snsid,
                      count(ts) as pay_times ,
                      sum(amount) as revenue 
                from payment 
                where trunc(ts)=?::DATE 
                group by app,uid,snsid 
                )p 
     on         u.app=p.app and u.snsid=p.snsid and u.uid=p.uid
    )t
union all                        
select md5(app||uid||snsid),
       t.app,
       t.uid,
       t.snsid,
       t.email,
       t.gender,
       t.age ,
       t.name,
       t.coins_bal,
       t.rc_bal,
       t.pay_times,
       t.revenue 
from
    (select u.app,
            u.uid,
            u.snsid,
            u.email,
            u.gender,
            u.age_num as age,
            u.name,
            p.pay_times,
            p.revenue,
            u.coins as coins_bal,
            u.rc as rc_bal 
     from tbl_user_th u
     left join (select app,uid,
                      snsid,
                      count(ts) as pay_times ,
                      sum(amount) as revenue 
                from payment 
                where trunc(ts)=?::DATE 
                group by app,uid,snsid 
                )p 
     on         u.app=p.app and u.snsid=p.snsid and u.uid=p.uid
    )t            
;





update app_user_info set pay_times=payment_info.pay_times from payment_info where app_user_info.app=payment_info.app and app_user_info.snsid=payment_info.snsid and app_user_info.uid=payment_info.uid;


update app_user_info set pay_amount=payment_info.revenue from payment_info where app_user_info.app=payment_info.app and app_user_info.snsid=payment_info.snsid and app_user_info.uid=payment_info.uid;

TRUNCATE TABLE processed.dim_user_old;

INSERT INTO processed.dim_user_old
SELECT MD5(concat (u.app,u.uid)) AS id,
       MD5(concat (u.app,u.uid)) AS user_key,
       u.app AS app_id,
       u.uid AS app_user_id,
       NULL AS device_key,
       CASE
         WHEN LENGTH(u.snsid) > 50 THEN SUBSTRING(u.snsid,1,50)
         ELSE u.snsid
       END AS facebook_id,
       u.install_ts AS install_ts,
       TRUNC(u.install_ts) AS install_date,
       u.install_source AS install_source,
       NULL AS install_subpublisher,
       NULL AS install_campaign,
       u.language AS install_language,
       NULL AS install_locale,
       u.country_code AS install_country_code,
       NULL AS install_country,
       u.os AS install_os,
       NULL AS install_device,
       NULL AS install_device_alias,
       u.browser AS install_browser,
       info.gender AS install_gender,
       info.age AS install_age,
       u.language AS LANGUAGE,
       NULL AS locale,
       NULL AS birthday,
       info.gender AS gender,
       u.country_code AS country_code,
       NULL AS country,
       u.os AS os,
       u.os_version AS os_version,
       NULL AS device,
       NULL AS device_alias,
       u.browser AS browser,
       u.browser_version AS browser_version,
       NULL AS app_version,
       u.level AS level,
       NULL AS levelup_ts,
       NULL AS ab_experiment,
       NULL AS ab_variant,
       u.is_payer AS is_payer,
       u.conversion_ts AS conversion_ts,
       info.pay_amount AS total_revenue_usd,
       info.pay_times AS payment_cnt,
       u.latest_login_ts AS last_login_ts,
       info.coins_bal AS coin_wallet,
       0 AS coins_in,
       0 AS coins_out,
       info.rc_bal AS rc_wallet,
       0 AS rc_in,
       0 AS rc_out,
       info.email AS email,
       NULL AS last_ip
FROM (SELECT user_id,
             app,
             uid,
             snsid,
             install_ts,
             install_source,
             os,
             os_version,
             country_code,
             level,
             LANGUAGE,
             browser,
             browser_version,
             is_payer,
             conversion_ts,
             latest_login_ts
      FROM (SELECT user_id,
                   app,
                   uid,
                   snsid,
                   install_ts,
                   install_source,
                   os,
                   os_version,
                   country_code,
                   level,
                   LANGUAGE,
                   browser,
                   browser_version,
                   is_payer,
                   conversion_ts,
                   latest_login_ts,
                   ROW_NUMBER() OVER (PARTITION BY app,uid,snsid ORDER BY latest_login_ts DESC) AS rownum
            FROM app_user) tmp
      WHERE rownum = 1) u
  LEFT JOIN app_user_info info
    ON u.app = info.app
   AND u.uid = info.uid
   AND u.snsid = info.snsid;

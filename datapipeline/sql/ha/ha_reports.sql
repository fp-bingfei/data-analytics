DELETE
FROM events_agg
WHERE dt = ?::DATE;

INSERT INTO events_agg
SELECT nvl(md5(e.name||trunc (e.ts) ||e.app||e.properties||u.os||u.browser||u.install_source||trunc (u.install_ts) ||u.country_code),'error_id'),
       e.name,
       trunc(e.ts) AS dt,
       e.app,
       e.properties,
       u.os,
       u.browser,
       u.install_source,
       trunc(u.install_ts) AS install_date,
       u.country_code,
       COUNT(1)
FROM events e
  LEFT JOIN app_user u
         ON e.app = u.app
        AND e.uid = u.snsid
WHERE trunc(e.ts) = ?::DATE
GROUP BY e.name,
         dt,
         e.app,
         e.properties,
         u.os,
         u.browser,
         u.install_source,
         install_date,
         u.country_code;

DELETE
FROM dau
WHERE dt = ?::DATE;

INSERT INTO dau
SELECT e.uid,
       max(CAST(json_extract_path_text (properties,'level') AS integer)) AS level,
       trunc(e.ts) AS dt,
       a.browser,
       e.app,
       a.install_source,
       a.country_code,
       a.os,
       trunc(a.install_ts) AS install_dt
FROM events e
  INNER JOIN app_user a
          ON e.app = a.app
         AND e.uid = a.snsid
WHERE trunc(e.ts) = ?::DATE
GROUP BY e.uid,
         dt,
         install_dt,
         a.country_code,
         a.install_source,
         a.os,
         e.app,
         a.browser;

DELETE
FROM payment
WHERE trunc(ts) = ?::DATE;

INSERT INTO payment
SELECT e1.uid,
       u.app,
       e1.ts,
       u.install_ts,
       u.install_source,
       u.os,
       u.browser,
       e1.first_time,
       e1.currency,
       u.country_code,
       e1.item,
       e1.amount,
       e1.level
FROM (SELECT ts,
             app,
             uid,
             json_extract_path_text(properties,'level') AS level,
             json_extract_path_text(properties,'first_time') AS first_time,
             json_extract_path_text(properties,'item') AS item,
             json_extract_path_text(properties,'currency') AS currency,
             json_extract_path_text(properties,'amount')::INT/ 100::FLOAT *c.factor AS amount
      FROM events e
        JOIN currency c ON json_extract_path_text (properties,'currency') = c.currency
      WHERE name = 'payment'
      AND   trunc(e.ts) = ?::DATE) e1
  JOIN app_user u
    ON e1.app = u.app
   AND e1.uid = u.snsid;

DELETE
FROM user_payment_stats
WHERE trunc(ts) = ?::DATE;

INSERT INTO user_payment_stats
(
  uid,
  ts,
  install_dt,
  country_code,
  os,
  app,
  install_source,
  browser
)
SELECT uid,
       ts,
       trunc(install_ts),
       country_code,
       os,
       app,
       install_source,
       browser
FROM payment
WHERE first_time = '1'
AND   trunc(ts) = ?::DATE;

TRUNCATE TABLE kpi_raw;

--login cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  browser,
  country_code,
  login_cnt,
  event
)
SELECT u.app,
       trunc(e.ts),
       trunc(u.install_ts),
       u.install_source,
       substring(u.os,0,10),
       u.browser,
       u.country_code,
       COUNT(1) AS login_cnt,
       'login'
FROM events e
  JOIN app_user u
    ON e.app = u.app
   AND e.uid = u.snsid
WHERE e.name = 'login'
AND   trunc(e.ts) = ?::DATE
GROUP BY u.app,
         trunc(e.ts),
         trunc(u.install_ts),
         u.install_source,
         u.os,
         u.browser,
         u.country_code;

--dau cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  browser,
  country_code,
  dau_cnt,
  event
)
SELECT u.app,
       trunc(e.ts),
       trunc(u.install_ts),
       u.install_source,
       substring(u.os,0,10),
       u.browser,
       u.country_code,
       COUNT(DISTINCT snsid) AS dau_cnt,
       'dau'
FROM events e
  JOIN app_user u
    ON e.app = u.app
   AND e.uid = u.snsid
   AND trunc (e.ts) = ?::DATE
GROUP BY u.app,
         trunc(e.ts),
         trunc(u.install_ts),
         u.install_source,
         u.os,
         u.browser,
         u.country_code;

--newuser_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  browser,
  country_code,
  newuser_cnt,
  event
)
SELECT u.app,
       trunc(u.install_ts),
       trunc(u.install_ts),
       u.install_source,
       substring(u.os,0,10),
       u.browser,
       u.country_code,
       COUNT(1) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY u.app,
         trunc(u.install_ts),
         trunc(u.install_ts),
         u.install_source,
         substring(u.os,0,10),
         u.browser,
         u.country_code;

--payer_dau_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  country_code,
  payer_dau_cnt,
  browser,
  event
)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       COUNT(d.uid) AS payer_dau_cnt,
       d.browser,
       'payer_dau'
FROM dau d
  INNER JOIN user_payment_stats t
          ON d.uid = t.uid
         AND d.os = t.os
         AND d.browser = t.browser
         AND d.install_source = t.install_source
         AND d.app = t.app
         AND d.install_dt = t.install_dt
         AND d.country_code = t.country_code
WHERE d.dt = ?::DATE
GROUP BY d.os,
         d.country_code,
         d.app,
         d.install_source,
         d.dt,
         d.install_dt,
         d.browser;

--newpayer_cnt
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  browser,
  country_code,
  newpayer_cnt,
  event
)
SELECT p.app,
       trunc(p.ts),
       trunc(p.install_ts),
       p.install_source,
       substring(p.os,0,10),
       p.browser,
       p.country_code,
       COUNT(uid) AS newpayer_cnt,
       'newpayers'
FROM payment p
WHERE p.first_time = '1'
AND   trunc(p.ts) = ?::DATE
GROUP BY p.app,
         trunc(p.ts),
         trunc(p.install_ts),
         p.install_source,
         p.os,
         p.browser,
         p.country_code;

--revenue
INSERT INTO kpi_raw
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  browser,
  country_code,
  amount,
  event
)
SELECT u.app,
       trunc(e1.ts),
       trunc(u.install_ts),
       u.install_source,
       substring(u.os,0,10),
       u.browser,
       u.country_code,
       SUM(e1.amount),
       'payment'
FROM (SELECT ts,
             app,
             uid,
             json_extract_path_text(properties,'amount')::INT/ 100::FLOAT AS original_amount,
             json_extract_path_text(properties,'currency') AS currency,
             json_extract_path_text(properties,'amount')::INT/ 100::FLOAT *c.factor AS amount
      FROM events e
        JOIN currency c ON json_extract_path_text (properties,'currency') = c.currency
      WHERE name = 'payment'
      AND   trunc(e.ts) = ?::DATE) e1
  JOIN app_user u
    ON e1.app = u.app
   AND e1.uid = u.snsid
GROUP BY u.app,
         trunc(e1.ts),
         trunc(u.install_ts),
         u.install_source,
         substring(u.os,0,10),
         u.browser,
         u.country_code;

DELETE
FROM kpi
WHERE dt = ?::DATE;

INSERT INTO kpi
(
  app,
  dt,
  install_dt,
  install_source,
  os,
  browser,
  country_code,
  amount,
  login_cnt,
  dau_cnt,
  newuser_cnt,
  newpayer_cnt,
  payer_dau_cnt
)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       SUM(amount),
       SUM(login_cnt),
       SUM(dau_cnt),
       SUM(newuser_cnt),
       SUM(newpayer_cnt),
       SUM(payer_dau_cnt)
FROM kpi_raw
GROUP BY app,
         dt,
         install_dt,
         install_source,
         os,
         browser,
         country_code;

TRUNCATE TABLE rpt_tutorial;

INSERT INTO rpt_tutorial
SELECT t.snsid,
       max_ts,
       t.step,
       t.app,
       t.browser,
       t.install_source,
       t.os,
       t.install_ts,
       t.country_code,
       c.country
FROM (SELECT a.snsid,
             MAX(trunc(ts)) AS max_ts,
             a.country_code,
             trunc(a.install_ts) AS install_ts,
             a.browser,
             a.os,
             a.install_source,
             a.app,
             json_extract_path_text(properties,'step') AS step
      FROM events e
        LEFT JOIN app_user a
               ON e.app = a.app
              AND e.uid = a.snsid
      GROUP BY a.snsid,
               a.country_code,
               install_ts,
               a.os,
               a.install_source,
               a.app,
               a.browser,
               json_extract_path_text(properties,'step')) t
  LEFT JOIN country c ON t.country_code = c.country_code;

--------------------------------------- App_User Start ----------------------------------------------

---------------------------------------USER COUNTRY FOR USERS ---------------------------------------

--Get the country for each user


CREATE TEMP TABLE USER_COUNTRY
as
select app
       ,uid
       ,snsid
       ,install_ts
       ,country_code

from(
SELECT           app
                ,uid
                ,snsid
                ,install_ts
                ,country_code
                ,row_number() OVER (partition by app,uid,snsid order by ts desc) as rank
from events_raw
where event in ('logintimes','dau','loading','onlinetime') AND snsid<>''
and   trunc(ts) = ?::DATE
)t
where rank = 1;

--Get payer,conversion_ts,and last_login_ts info

CREATE TEMP TABLE payer_user_info
as
select app,
       uid,
       snsid,
       min(ts) as conversion_ts
FROM events_raw 
where event = 'payment'
and   trunc(ts)= ?::DATE
group by 1,2,3;


--Get other user info for app users



truncate table app_user_temp;

insert into app_user_temp (app,uid,snsid,install_ts,install_source,os,os_version,country_code,level,language,browser,browser_version,latest_login_ts)
select t1.app,
       t1.uid,
       t1.snsid,
       t1.install_ts,
       t1.install_source,
       t1.os,
       t1.os_version,
       t2.country_code,
       t1.level,
       t1.language,
       t1.browser,
       t1.browser_version,
       t1.ts as latest_login_ts
from   
(select app,
       uid,
       snsid,
       substring(install_source,0,100) as install_source,
       install_ts,
       os,
       os_version,
       level,
       language,
       browser,
       browser_version,
       ts,
       row_number() over(partition by app,uid,snsid order by install_ts desc) as rank
from
       (select app,
              uid,
              snsid,  --sortkey
              install_source,
              install_ts,  --diskey
              os,
              os_version,
              max(CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text (properties,'level') END AS integer)) as level,
              json_extract_path_text(properties,'g_locale') as language,
              browser,
              substring(browser_version, 1, 50) as browser_version,
              ts
       from  events_raw
       where trunc(ts) =?::DATE
       group by 1,2,3,4,5,6,7,9,10,11,12
       )
)t1
left join USER_COUNTRY t2
on t1.app = t2.app
and t1.snsid = t2.snsid
and t1.uid = t2.uid
where t1.rank=1;



update app_user_temp set conversion_ts=app_user.conversion_ts from app_user where app_user_temp.uid=app_user.uid and app_user_temp.snsid=app_user.snsid and app_user_temp.app=app_user.app;


update app_user_temp set conversion_ts=payer_user_info.conversion_ts from payer_user_info where app_user_temp.uid=payer_user_info.uid and app_user_temp.snsid=payer_user_info.snsid and app_user_temp.app=payer_user_info.app
and app_user_temp.conversion_ts is null;








--delete the old data of app user




delete from app_user where (app,uid,snsid) in (select app,uid,snsid from app_user_temp);

insert into app_user
(
       user_id,
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       browser,
       browser_version,
       conversion_ts,
       latest_login_ts
)
select md5(app||uid||snsid),
       app,
       uid,
       snsid,
       install_ts,
       install_source,
       os,
       os_version,
       country_code,
       level,
       language,
       browser,
       browser_version,
       conversion_ts,
       latest_login_ts
from app_user_temp
;

--update the payer flag
update app_user set is_payer = true where conversion_ts is not null;

--update the language which the game team didn't give us the right value
update app_user set language = 'en'
where app = 'ff2.us.prod'
and language not in
('en', 'it', 'fr', 'de', 'pt', 'nl', 'es', 'pl', 'ar', 'tr');

update app_user set language = 'th'
where app = 'ff2.th.prod'
and language not in
('th','zh');

------------------------------------------dau--------------------------------------------------
delete from dau where dt =?::DATE;

INSERT INTO dau
select md5(app||uid||snsid),
       app,
       dt,
       uid,
       snsid,
       install_source,
       install_dt,
       os,
       os_version,
       country_code,
       level,
       browser,
       browser_version
from
   (select app,
           dt,
           uid,
           snsid,
           install_source,
           install_dt,
           os,
           os_version,
           country_code,
           level,
           browser,
           browser_version,
           row_number() over(partition by app,uid,snsid order by dt) as rank
     from
             (SELECT e.app,
                    trunc(e.ts) as dt,
                    e.uid,
                    e.snsid,
                    a.install_source, 
                    trunc(e.install_ts) AS install_dt,
                    a.os,
                    a.os_version,
                    a.country_code,
                    max(CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text (properties,'level') END AS integer)) AS LEVEL,
                    a.browser,
                    a.browser_version
             FROM events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
             WHERE trunc(e.ts)=?::DATE
             GROUP BY 1,2,3,4,5,6,7,8,9,11,12
             )
     )t
where t.rank=1;     

  
-------------------------------------------------Payment--------------------------------------------------------------------------------------         

delete from payment where trunc(ts)=?::DATE;


INSERT INTO payment
(   
  user_id
  ,uid,snsid
  ,app,ts
  ,install_dt
  ,install_source
  ,os
  ,os_version
  ,browser
  ,browser_version
  ,country_code
  ,abtest
  ,amount
  ,currency
  ,first_time
  ,gameamount
  ,item
  ,level,coins_bal,rc_bal
  ,locale
  ,paid_time
)
SELECT md5(e.app||e.uid||e.snsid),
       e.uid,
       e.snsid,
       e.app,
       e.ts,
       trunc(a.install_ts) as install_dt,
       a.install_source,
       a.os,
       a.os_version,
       a.browser,
       a.browser_version,
       a.country_code,
       json_extract_path_text(properties,'abtest') as abtest,
       json_extract_path_text(properties,'amount')::float/100::float * c.factor AS amount,
       json_extract_path_text(properties,'currency') as currency,
       json_extract_path_text(properties,'first_time') as first_time,
       json_extract_path_text(properties,'gameamount') as gameamount,
       json_extract_path_text(properties,'item') as item,
       CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text (properties,'level') END AS integer) as level,
       CAST(CASE WHEN json_extract_path_text (properties,'coins_left') = '' THEN '0' ELSE json_extract_path_text (properties,'coins_left') END AS integer) AS coins_bal,
       CAST(CASE WHEN json_extract_path_text (properties,'rc_left') = '' THEN '0' ELSE json_extract_path_text (properties,'rc_left') END AS integer) AS rc_bal,

       json_extract_path_text(properties,'locale') as locale,
       json_extract_path_text(properties,'paid_time') as paid_time
FROM events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
left join currency c on json_extract_path_text(properties,'currency') = c.currency and trunc(e.ts)=c.dt
where e.event = 'payment' and   trunc(e.ts)=?::DATE
;

-----------------------kpi_raw--------------------------------------------------------------------------------------



truncate table kpi_raw;
----login cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,login_cnt,event)
SELECT e.app,
       trunc(e.ts),
       trunc(e.install_ts),
       e.install_source,
       e.os,
       e.browser,
       a.country_code,
       count(1) AS login_cnt,
       'session_start'
FROM events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
WHERE e.event='logintimes'
  AND trunc(e.ts) =?::DATE
GROUP BY 1,2,3,4,5,6,7;

--dau cnt

INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,dau_cnt,event)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       count(DISTINCT user_id) AS dau_cnt,
       'dau'
FROM dau
where dt =?::DATE
GROUP BY 1,2,3,4,5,6,7;

--newuser_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,newuser_cnt,event)
SELECT app,
       trunc(install_ts),
       trunc(install_ts),
       install_source,
       os,
       browser,
       country_code,
       count(1) AS newuser_cnt,
       'newuser'
FROM app_user u
WHERE trunc(u.install_ts) = ?::DATE
GROUP BY 1,2,3,4,5,6,7;


--payer_dau_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,country_code,payer_dau_cnt,browser,event)
SELECT d.app,
       d.dt,
       d.install_dt,
       d.install_source,
       d.os,
       d.country_code,
       count(d.USER_ID) AS payer_dau_cnt,
       d.browser,
       'payer_dau'
FROM dau d 
INNER JOIN app_user a ON d.user_id = a.user_id 
and a.is_payer is true
WHERE d.dt=?::DATE
GROUP BY 1,2,3,4,5,6,8;

--newpayer_cnt
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,newpayer_cnt,event)
SELECT app,
       trunc(conversion_ts),
       trunc(install_ts),
       install_source,
       os,
       browser,
       country_code,
       count(user_id) AS newpayer_cnt,
       'newpayers'
FROM app_user
where trunc(conversion_ts) =?::DATE and
 is_payer is true
group by 1,2,3,4,5,6,7
;


--revenue
INSERT INTO kpi_raw(app,dt,install_dt,install_source,os,browser,country_code,amount,event)
SELECT app,
       trunc(ts),
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       sum(amount) as amount,
       'payment'
from   payment
where  trunc(ts) =?::DATE
GROUP by 1,2,3,4,5,6,7;
  
---------kpi----------------------------------------------------------------------------------------
delete from kpi where dt = ?::DATE;

INSERT INTO kpi(app,dt,install_dt,install_source,os,browser,country_code,amount,login_cnt,dau_cnt,newuser_cnt,newpayer_cnt,payer_dau_cnt)
SELECT app,
       dt,
       install_dt,
       install_source,
       os,
       browser,
       country_code,
       sum(amount),
       sum(login_cnt),
       sum(dau_cnt),
       sum(newuser_cnt),
       sum(newpayer_cnt),
       sum(payer_dau_cnt)
FROM kpi_raw
where dt = ?::DATE
GROUP BY 1,2,3,4,5,6,7;





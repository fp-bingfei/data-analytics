------------------------Transactions----------------------
update processed.tmp_start_date set start_date = current_date -3;
delete from events_raw where trunc(ts)=?::DATE and 1=2;
delete from events_raw where trunc(ts)=?::DATE and 1=2;
delete from events_raw where trunc(ts)=?::DATE and 1=2;
--create temp table temp_rpt_tutorial
--as 
--SELECT t.user_id,
--       t.snsid ,
--       t.uid,
--       t.max_dt ,
--       t.step,
--       t.app ,
--       t.t_snsid,
--       t.install_source,
--       t.os,t.os_version,
--       t.install_dt,
--       t.country_code,
--       t.browser,
--       t.browser_version,
--       t.abtest,t.level,
--       c.country
--FROM 
--  (SELECT a.snsid,
--          a.user_id,
--          a.uid,
--          e.snsid AS t_snsid,
--          trunc(e.max_ts) AS max_dt,
--          a.country_code,
--          trunc(a.install_ts) AS install_dt,
--          a.os,a.os_version,
--          a.install_source,
--          a.app,
--          a.browser,
--          a.browser_version,
--          e.step,
--          e.abtest,e.level
--   FROM app_user a
--   LEFT JOIN 
--      (select app,
--             uid,
--             snsid,
--             max(ts) as max_ts,
--             json_extract_path_text(properties,'step') as step,
--             CAST(CASE WHEN json_extract_path_text (properties,'level') = '' THEN '0' ELSE json_extract_path_text (properties,'level') END AS integer) AS level,
--             json_extract_path_text(properties,'abtest') as abtest
--      from   events_raw
--      where  event = 'tutorial' and trunc(ts)=?::DATE
--      group by 1,2,3,5,6,7
--      )e
--   on a.app=e.app
--   and a.uid=e.uid
--   and a.snsid=e.snsid
--  )t 
--LEFT JOIN country c ON t.country_code=c.country_code;
--
--delete from rpt_tutorial where (user_id,step) in (select user_id, step from temp_rpt_tutorial);
--
--insert into rpt_tutorial(user_id,snsid,uid,dt,step,level,abtest,app,t_snsid,install_source,os,os_version,install_dt,country_code,browser,
--browser_version,country)
--select user_id,
--snsid,
--uid,
--max_dt,
--step,
--level,
--abtest,
--app,
--t_snsid,
--install_source,
--os,
--os_version,
--install_dt,
--country_code,
--browser,
--browser_version,
--country from temp_rpt_tutorial;


delete from task_completion where trunc(ts)=?::DATE;

insert into task_completion
(user_id,event,snsid,uid,app,ts,install_source,os,os_version,browser,browser_version,install_dt,country,level,action,
missionid,locale,task_id,rc_spend,coins_spend)
select md5(e.app||e.uid||e.snsid),
e.event,
e.snsid as snsid,
e.uid,
e.app,
e.ts,
a.install_source,
a.os,
a.os_version,
a.browser,
a.browser_version,
trunc(a.install_ts),
c.country, 
cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as integer) as level,
json_extract_path_text(properties, 'action') as action,
json_extract_path_text(properties, 'missionId') as missionid,
json_extract_path_text(properties, 'locale') as locale,
json_extract_path_text(properties, 'task_id') as task_id,
cast(case when json_extract_path_text(properties,'rc_spend')='' then '0' else json_extract_path_text
(properties,'rc_spend') end as integer) as rc_spend,
cast(case when json_extract_path_text(properties,'coins_spend')='' then '0' else json_extract_path_text
(properties,'coins_spend') end as float) as coins_spend
from events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
left join country c on a.country_code=c.country_code where e.event='mission' and trunc(e.ts)=?::DATE;


delete from rc_transaction where trunc(ts)=?::DATE;

insert into rc_transaction
(user_id,app,event,ts,uid,snsid,install_dt,install_source,country_code,browser,browser_version,os,os_version,rc_in,rc_out,
rc_bal,level,action,location,abtest,game_version)
select md5(e.app||e.uid||e.snsid),
e.app,
e.event,
e.ts,
e.uid,
e.snsid,
trunc(a.install_ts) as install_dt,
a.install_source,
a.country_code,
a.browser,
a.browser_version,
a.os,
a.os_version,
cast(case when json_extract_path_text(properties,'rc_get')='' then '0' else json_extract_path_text(properties,'rc_get') 
end as integer) as rc_in,
cast(case when json_extract_path_text(properties,'rc_spend')='' then '0' else json_extract_path_text
(properties,'rc_spend') end as integer) as rc_out,
cast(case when json_extract_path_text(properties,'rc_left')='' then '0' else json_extract_path_text(properties,'rc_left') 
end as integer) as rc_bal ,
json_extract_path_text(properties, 'level') as level,
json_extract_path_text(properties, 'action') as action,
json_extract_path_text(properties, 'location') as location,
json_extract_path_text(properties, 'abtest') as abtest,
json_extract_path_text(properties, 'GameVersion') as game_version from events_raw e 
left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid where e.event='transaction' 
and trunc(e.ts)=?::DATE
and 1=2;

delete from rpt_rc where dt=?::DATE;

insert into rpt_rc
(app,dt,install_dt,os,os_version,browser,browser_version,country_code,rc_in,rc_out,rc_bal,install_source,level,location,
action,action_detail,abtest)
select app,
trunc(ts) as dt,
install_dt,
os,
os_version,
browser,
browser_version,
country_code,
sum(rc_in),
sum(rc_out),
sum(rc_bal),
install_source,
level,
location,
action,
action_detail,
abtest from rc_transaction
where trunc(ts)=?::DATE
and 1=2
group by app,
trunc(ts),
install_dt,
os,
os_version,
browser,
browser_version,
country_code,
install_source,
level,
location,
action,
action_detail,
abtest;

delete from coin_transaction where trunc(ts)=?::DATE;

insert into coin_transaction
(user_id,app,event,ts,uid,snsid,install_dt,install_source,country_code,browser,browser_version,os,os_version,coins_in,
coins_out,coins_bal,level,action,action_detail,location,abtest,game_version)
select md5(e.app||e.uid||e.snsid),
e.app,
e.event,
e.ts,
e.uid,
e.snsid,
trunc(a.install_ts) as install_dt,
a.install_source,
a.country_code,
a.browser,
a.browser_version,
a.os,
a.os_version,
cast(case when json_extract_path_text(properties,'coins_get')='' then '0' else json_extract_path_text
(properties,'coins_get') end as integer) as coins_in,
cast(case when json_extract_path_text(properties,'coins_spend')='' then '0' else json_extract_path_text
(properties,'coins_spend') end as integer) as coins_out,
cast(case when json_extract_path_text(properties,'coins_left')='' then '0' else json_extract_path_text
(properties,'coins_left') end as integer) as coins_bal ,
json_extract_path_text(properties, 'level') as level,
json_extract_path_text(properties, 'action') as action,
json_extract_path_text(properties, 'action_detail') as action_detail,
json_extract_path_text(properties, 'location') as location,
json_extract_path_text(properties, 'abtest') as abtest,
json_extract_path_text(properties, 'GameVersion') as game_version from events_raw e 
left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid where e.event='transaction' and 
trunc(e.ts)=?::DATE and 1=2;

delete from rpt_coin where dt=?::DATE;

insert into rpt_coin
(app,dt,install_dt,os,os_version,browser,browser_version,country_code,coins_in,coins_out,coins_bal,install_source,level,
location,action,action_detail,abtest)
select app,
trunc(ts) as dt,
install_dt,
os,
os_version,
browser,
browser_version,
country_code,
sum(coins_in),
sum(coins_out),
sum(coins_bal),
install_source,
level,
location,
action,
action_detail,
abtest from coin_transaction
where trunc(ts)=?::DATE
and 1=2
group by app,
trunc(ts),
install_dt,
os,
os_version,
browser,
browser_version,
country_code,
install_source,
level,
location,
action,
action_detail,
abtest;

------delete from item_transaction where trunc(ts)=?::DATE;

----insert into item_transaction
---(user_id,app,event,ts,uid,snsid,install_dt,install_source,country_code,browser,browser_version,os,os_version,qty,level,
---item_id,action,abtest,game_version)
----select md5(e.app||e.uid||e.snsid),
---e.app,
---e.event,
---e.ts,
---e.uid,
---e.snsid as snsid,
---trunc(a.install_ts) as install_dt,
---a.install_source,
---a.country_code,
---a.browser,
---a.browser_version,
---a.os,
---a.os_version,
---cast(case when json_extract_path_text(properties,'count')='' then '0' else json_extract_path_text(properties,'count') end as float) as qty ,
---json_extract_path_text(properties, 'level') as level,
---json_extract_path_text(properties, 'item') as item_id,
---json_extract_path_text(properties, 'action') as action,
---json_extract_path_text(properties, 'abtest') as abtest,
---json_extract_path_text(properties, 'GameVersion') as game_version from events_raw e 
---left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid where trunc(e.ts)=?::DATE;

-----delete from rpt_item where dt=?::DATE;

--insert into rpt_item(app,dt,install_dt,os,os_version,browser,browser_version,item_id,
---country_code,qty,install_source,level,event,action,abtest)
---select app,
---trunc(ts) as dt,
---install_dt,
---os,
---os_version,
---browser,
---browser_version,
---item_id,
---country_code,
---sum(qty),
---install_source,
---level,
--event,
---action,
---abtest from item_transaction
---where  trunc(ts)=?::DATE
---group by app,
---trunc(ts),
---install_dt,
---os,
---os_version,
---browser,
---browser_version,
--country_code,
---install_source,
---item_id,
---level,
---event,
---action,
---abtest;



delete from boardorder where dt=?::DATE;

insert into boardorder 
select snsid,
app,
trunc(ts),
cast(case when json_extract_path_text(properties,'coins_get')='' then '0' else json_extract_path_text
(properties,'coins_get') end as integer) as coins_get,
cast(case when json_extract_path_text(properties,'xp_get')='' then '0' else json_extract_path_text(properties,'xp_get') 
end as integer) as xp_get,
cast(case when json_extract_path_text(properties,'rc_get')='' then '0' else json_extract_path_text(properties,'rc_get') 
end as integer) as rc_get,
cast(case when json_extract_path_text(properties,'coins_spend')='' then '0' else json_extract_path_text
(properties,'coins_spend') end as integer) as coins_spend,
cast(case when json_extract_path_text(properties,'xp_spend')='' then '0' else json_extract_path_text
(properties,'xp_spend') end as integer) as xp_spend,
cast(case when json_extract_path_text(properties,'rc_spend')='' then '0' else json_extract_path_text
(properties,'rc_spend') end as integer) as rc_spend,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'abtest') as abtest,
json_extract_path_text(properties,'locale') as locale from events_raw where event='boardOrder' and trunc(ts)=?::DATE;


delete from rpt_unique_actions where dt=?::DATE;

insert into rpt_unique_actions
select a.app,
a.install_source,
a.os,
a.os_version,
a.browser,
a.browser_version,
a.country_code,
trunc(e.ts) as dt,
trunc(a.install_ts) as install_dt,
c.country,
cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as integer) as level ,
count(distinct md5(e.app||e.uid||e.snsid)) as users,
sum(case when json_extract_path_text(properties,'action')='MapModel::feedAnimal'  then 1 else 0 end ) as animals_fed ,
sum(case when json_extract_path_text(properties,'action')='MapModel::plantCrop'  then 1 else 0 end ) as plants_cropped 
from events_raw e 
left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid
left join country c on a.country_code=c.country_code 
where trunc(ts)=?::DATE
group by 1,2,3,4,5,6,7,8,9,10,11;


delete from login_duration where trunc(ts)=?::DATE;

insert into login_duration(uid,snsid,ts,app,install_dt,os,install_source,browser,browser_version,country,level,online_time,locale)
select e.uid,
e.snsid,
e.ts,
e.app,
trunc(a.install_ts) as install_dt ,
a.os,
a.install_source,
a.browser,
a.browser_version,
c.country,
cast(case when json_extract_path_text(properties,'level')='' then '0' else json_extract_path_text(properties,'level') end as integer) as level,
cast(case when json_extract_path_text(properties,'online_time')='' then '0' 
else json_extract_path_text(properties,'online_time') end as integer) as online_time,
json_extract_path_text(properties, 'locale') as locale
from events_raw e left join app_user a on e.app=a.app and e.snsid=a.snsid and e.uid=a.uid 
left join country c on a.country_code=c.country_code
where event='onlinetime' and trunc(e.ts)=?::DATE;




delete from rpt_levelup_wallet where trunc(ts)=?::DATE;

insert into rpt_levelup_wallet
(user_id,app,uid,snsid,install_dt,ts,install_source,country_code,country,is_payer,browser,
browser_version,os,os_version,level,coins_bal,rc_bal)
select md5(e.app||e.uid||e.snsid) as user_id,
e.app,
e.uid,
e.snsid as snsid,
trunc(a.install_ts) as install_dt,
e.ts,
a.install_source,
a.country_code,
c.country,
a.is_payer,
a.browser,
a.browser_version,
a.os,
a.os_version,
json_extract_path_text(properties,'level') as level,
cast(case when json_extract_path_text(properties,'coins_left')='' then '0' else json_extract_path_text
(properties,'coins_left') end as bigint) as coins_bal,
cast(case when json_extract_path_text(properties,'rc_left')='' then '0' else json_extract_path_text
(properties,'rc_left') end as integer) as rc_bal from events_raw e
left join app_user a on a.app=e.app and a.snsid=e.snsid and a.uid=e.uid
left join country c on a.country_code=c.country_code 
where event='levelup' and trunc(e.ts)=?::DATE;


delete from processed.fact_leaderboard
where trunc(ts)=?::DATE;

insert into processed.fact_leaderboard
select   app       
        ,ts              
        ,uid             
        ,snsid           
        ,install_ts      
        ,install_source  
        ,country_code    
        ,ip              
        ,browser         
        ,browser_version 
        ,os              
        ,os_version      
        ,event         
        ,json_extract_path_text(properties,'birthday') as birthday
        ,json_extract_path_text(properties,'test_user') as test_user
        ,json_extract_path_text(properties,'locale') as locale
        ,json_extract_path_text(properties,'sign_email') as sign_email
        ,cast(json_extract_path_text(properties,'rc_left') as int)as rc_left
        ,cast(json_extract_path_text(properties,'level') as int) as level
        ,cast(json_extract_path_text(properties,'coins_left') as int) as coins_left
        ,json_extract_path_text(properties,'gameversion') as gameversion
        ,json_extract_path_text(properties,'action') as action
        ,json_extract_path_text(properties,'gender') as gender
        ,json_extract_path_text(properties,'g_locale') as g_locale
        ,json_extract_path_text(properties,'abtest') as abtest
from    events_raw 
where   event = 'rank'
and trunc(ts)=?::DATE;



delete from processed.tbl_leaderboard
where date = ?::DATE;

insert into processed.tbl_leaderboard
select d.app
      ,trunc(d.ts) as date
      ,trunc(d.install_ts) as install_date
      ,d.install_source
      ,coalesce(c.country,'Unknown') as country
      ,d.browser
      ,d.os
      ,d.event
      ,d.birthday
      ,d.test_user
      ,d.gameversion
      ,substring(d.action from position('::' in d.action)+2 for length(d.action))
      ,d.gender
      ,d.abtest
      ,d.level
      ,count(distinct md5(app||uid)) as open_number
      ,count(case when substring(d.action from position('::' in d.action)+2 for length(d.action)) ='openAction' then md5(app||uid) else null end) as open_times
      ,count(case when substring(d.action from position('::' in d.action)+2 for length(d.action)) ='load_user_dataAction' then md5(app||uid) else null end) as visit_times
from processed.fact_leaderboard d
left join country c
on d.country_code = c.country_code
where trunc(d.ts) = ?::DATE
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
;



create table virals_temp as 
select md5(e.app||e.uid||e.snsid) as user_id,
e.app,
e.uid,
e.snsid,
e.event,
trunc(e.install_ts) as dt,
e.ts,
e.country_code,
e.install_source,
e.browser,
e.os,
a.is_payer,
json_extract_path_text(properties,'abtest') as abtest,
json_extract_path_text(properties,'locale') as locale,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'type') as type,
cast(case when json_extract_path_text(properties,'click')='' then '0' else json_extract_path_text(properties,'click') end as integer) as clicks,
cast(case when json_extract_path_text(properties,'send')='' then '0' else json_extract_path_text(properties,'send') end as integer) as sends
from events_raw e
left join app_user a on e.app=a.app and e.uid=a.uid and e.snsid=a.snsid
where (event='feed' or event='opengragh' or event='request') and trunc(ts)=?::DATE
;


delete from rpt_viral where dt=?::DATE;

insert into  rpt_viral
select d.app,
d.user_id,
d.uid,
d.snsid,
d.dt,
d.install_dt,
d.install_source,
d.os,
d.os_version,
d.country_code,
v.level,
v.is_payer,
v.sends,
v.clicks,
v.type,
v.abtest,
v.locale,
v.event from dau d
left join virals_temp v on d.app=v.app and d.uid=v.uid and d.snsid=v.snsid and trunc(v.ts)=d.dt
where d.dt=?::DATE
;

drop table virals_temp;


--delete from rpt_loading where dt=?::DATE;
--
--insert into rpt_loading
--select md5(t.app||t.uid||t.snsid),
--t.app,
--t.uid,
--t.snsid,
--trunc(a.install_ts) as install_dt,
--trunc(ts) as dt,
--t.ts as ts_start,
--lead(ts) over (partition by MD5(t.app||t.uid||t.snsid||json_extract_path_text(properties,'loadingId')) order by CAST(json_extract_path_text(properties,'step') AS INT) asc) as ts_end,
--a.install_source,
--a.country_code,
--a.browser,
--a.os,
--CAST(json_extract_path_text(properties,'step') AS INT),
--json_extract_path_text(properties,'level') as level,
--json_extract_path_text(properties,'loadingId') as loading_id,
--json_extract_path_text(properties,'abtest') as ab_test from events_raw t
--left join app_user a on a.app=t.app and a.uid=t.uid and a.snsid=t.snsid
--where event='loadingStep' and json_extract_path_text(properties,'loadingId')<>'' and trunc(ts)=?::DATE
--and position('.' in json_extract_path_text(properties,'step'))=0
--;

--update rpt_loading set ts_end=ts_start where ts_end<ts_start;


delete from  rpt_stork where trunc(ts)=?::DATE;


INSERT INTO rpt_stork
select e.app,
cast(e.uid as varchar(100)),
a.country_code,
a.install_source,
a.os,
a.browser,
a.os_version,
a.browser_version,
ts,
json_extract_path_text(properties,'action') as action,
json_extract_path_text(properties,'by') as searchby,
json_extract_path_text(properties,'ident') as ident,
json_extract_path_text(properties,'level') as level,
CAST(CASE WHEN json_extract_path_text (properties,'rc_left') = '' THEN '0' ELSE json_extract_path_text (properties,'rc_left') END AS integer) rc_bal,
CAST(CASE WHEN json_extract_path_text (properties,'coins_left') = '' THEN '0' ELSE json_extract_path_text (properties,'coins_left') END AS integer) coin_bal,
json_extract_path_text(properties,'index') as index,
CAST(CASE WHEN json_extract_path_text (properties,'num') = '' THEN '0' ELSE json_extract_path_text (properties,'num') END AS integer) num,
CAST(CASE WHEN json_extract_path_text (properties,'price') = '' THEN '0' ELSE json_extract_path_text (properties,'price') END AS integer) price
from events_raw e  
left join (select distinct * from app_user) a on a.app=e.app and a.uid=e.uid
where event='bird' and trunc(ts)=?::DATE
;




drop table fact_feed;
create table fact_feed as 
select md5(e.app||e.uid||e.snsid) as user_id,
e.app,
e.uid,
e.snsid,
e.event,
trunc(e.install_ts) as dt,
e.ts,
e.country_code,
e.install_source,
e.browser,
e.os,
a.is_payer,
json_extract_path_text(properties,'abtest') as abtest,
json_extract_path_text(properties,'locale') as locale,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'type') as type,
cast(case when json_extract_path_text(properties,'click')='' then '0' else json_extract_path_text(properties,'click') end as integer) as clicks,
cast(case when json_extract_path_text(properties,'send')='' then '0' else json_extract_path_text(properties,'send') end as integer) as sends
from events_raw e
left join app_user a on e.app=a.app and e.uid=a.uid and e.snsid=a.snsid
where event='feed' 
;


drop table rpt_feed;
create table rpt_feed as
select app
       ,trunc(ts) as dt
       ,country_code
       ,install_source
       ,browser
       ,os
       ,is_payer
       ,abtest
       ,locale
       ,level
       ,type
       ,sum(clicks) as clicks
       ,sum(sends) as sends
       ,count(distinct case when sends >=1 then user_id else null end) as senders
       ,count(distinct case when clicks >=1 then user_id else null end) as clickers
from   fact_feed
group by 1,2,3,4,5,6,7,8,9,10,11;       

--------------------------------
--1.flea market buy
--------------------------------    

delete from processed.flea_market_buy where trunc(ts)>=
(select start_date from processed.tmp_start_date); 

insert into processed.flea_market_buy
select  id               
       ,app              
       ,ts               
       ,uid              
       ,snsid            
       ,install_ts       
       ,install_source   
       ,country_code     
       ,ip               
       ,browser          
       ,browser_version  
       ,os               
       ,os_version       
       ,event               
       ,json_extract_path_text(properties,'birthday')
       ,json_extract_path_text(properties,'locale')::varchar(50)
       ,json_extract_path_text(properties,'sign_email')::varchar(50)
       ,json_extract_path_text(properties,'rc_left')
       ,json_extract_path_text(properties,'level')
       ,json_extract_path_text(properties,'coins_left')
       ,json_extract_path_text(properties,'action')
       ,json_extract_path_text(properties,'gender')
       ,json_extract_path_text(properties,'g_locale')
       ,json_extract_path_text(properties,'abtest')
       ,json_extract_path_text(properties,'ident')
       ,cast( case when json_extract_path_text(json_extract_path_text(properties,'token'),'orangetoken') = '' then '0'
              else json_extract_path_text(json_extract_path_text(properties,'token'),'orangetoken')
          end as int) as orangetoken
       ,cast( case when json_extract_path_text(json_extract_path_text(properties,'token'),'whitetoken') = '' then '0'
              else json_extract_path_text(json_extract_path_text(properties,'token'),'whitetoken')
          end as int) as whitetoken
       ,cast( case when json_extract_path_text(json_extract_path_text(properties,'token'),'greentoken') = '' then '0'
              else json_extract_path_text(json_extract_path_text(properties,'token'),'greentoken')
          end as int) as greentoken
       ,cast( case when json_extract_path_text(json_extract_path_text(properties,'token'),'purpletoken') = '' then '0'
              else json_extract_path_text(json_extract_path_text(properties,'token'),'purpletoken')
          end as int) as purpletoken
       ,cast( case when json_extract_path_text(json_extract_path_text(properties,'token'),'bluetoken') = '' then '0'
              else json_extract_path_text(json_extract_path_text(properties,'token'),'bluetoken')
          end as int) as bluetoken
from   events_raw
where  event='roadsideshop'
and    json_extract_path_text(properties,'action') = 'roadsideshopModel::buy'
and trunc(ts) >= (select start_date from processed.tmp_start_date);




delete from processed.agg_flea_market_buy where date>=
(select start_date from processed.tmp_start_date); 

insert into processed.agg_flea_market_buy
select app
       ,trunc(ts) as date
       ,trunc(install_ts) as install_date
       ,coalesce(t2.country,'Unknown')
       ,cast(case when level = '' then '0' else level end as int)
       ,product
       ,count(uid) as buy_times
from processed.flea_market_buy t1
left join country t2
on t1.country_code =t2.country_code
where trunc(t1.ts)>=
(select start_date from processed.tmp_start_date) 
group by 1,2,3,4,5,6;
    

--------------------------------
--2.flea market ask
--------------------------------    


delete from processed.flea_market_ask where trunc(ts)>=
(select start_date from processed.tmp_start_date); 

insert into processed.flea_market_ask
select  id               
       ,app              
       ,ts               
       ,uid              
       ,snsid            
       ,install_ts       
       ,install_source   
       ,country_code     
       ,ip               
       ,browser          
       ,browser_version  
       ,os               
       ,os_version       
       ,event               
       ,json_extract_path_text(properties,'birthday')
       ,json_extract_path_text(properties,'locale')
       ,json_extract_path_text(properties,'sign_email')::varchar(50)
       ,json_extract_path_text(properties,'rc_left')
       ,json_extract_path_text(properties,'level')
       ,json_extract_path_text(properties,'coins_left')
       ,json_extract_path_text(properties,'action')
       ,json_extract_path_text(properties,'gender')
       ,json_extract_path_text(properties,'g_locale')
       ,json_extract_path_text(properties,'abtest')
       ,cast(case when json_extract_path_text(properties,'count') = '' then '0' else json_extract_path_text(properties,'count') end as integer)
from   events_raw
where  event='roadsideshop'
and    json_extract_path_text(properties,'action') = 'roadsideshopModel::ask'
and trunc(ts)>=(select start_date from processed.tmp_start_date);



delete from processed.agg_flea_market_ask where date>=
(select start_date from processed.tmp_start_date); 


insert into processed.agg_flea_market_ask
select  app
       ,trunc(ts) as date
       ,trunc(install_ts) as install_date
       ,coalesce(t2.country,'Unknown')
       ,cast(case when level = '' then '0' else level end as int)
       ,count
       ,count(uid) as users
from   processed.flea_market_ask t1
left join country t2
on t1.country_code = t2.country_code
where trunc(t1.ts) >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6;


----------------------------------------------
--3.open vs accept one vs acceptall vs expand
----------------------------------------------

delete from processed.flea_market_action_compare where date>=
(select start_date from processed.tmp_start_date); 


insert into processed.flea_market_action_compare
select  app
       ,trunc(ts) as date
       ,trunc(install_ts) as install_date
       ,coalesce(t2.country,'Unknown')
       ,cast(case when json_extract_path_text(properties,'level') = '' then '0' else json_extract_path_text(properties,'level') end as int)
       ,json_extract_path_text(properties,'action')
       ,count(uid) as users
from   events_raw t1
left join country t2
on t1.country_code=t2.country_code
where  event='roadsideshop'
and    (json_extract_path_text(properties,'action') = 'roadsideshopController::openAction'
or    json_extract_path_text(properties,'action') = 'roadsideshopModel::accept'
or    json_extract_path_text(properties,'action') = 'roadsideshopModel::acceptall'
or    json_extract_path_text(properties,'action') = 'roadsideshopModel::expand')
and trunc(t1.ts) >=(select start_date from processed.tmp_start_date)
group by 1,2,3,4,5,6 
; 


-------------------------------------------
--Referrals
-------------------------------------------
delete from processed.referrals
where date >=(select start_date from processed.tmp_start_date);

insert into processed.referrals
select  app              
       ,trunc(ts) as date               
       ,uid              
       ,snsid            
       ,install_ts       
       ,install_source   
       ,country_code     
       ,ip               
       ,browser          
       ,browser_version  
       ,os               
       ,os_version       
       ,event            
       ,json_extract_path_text(properties,'birthday')
       ,json_extract_path_text(properties,'test_user')
       ,json_extract_path_text(properties,'locale')::varchar(40)
       ,json_extract_path_text(properties,'sign_email')::varchar(40)
       ,json_extract_path_text(properties,'rc_left')
       ,json_extract_path_text(properties,'level')
       ,json_extract_path_text(properties,'coins_left')
       ,json_extract_path_text(properties,'GameVersion')
       ,json_extract_path_text(properties,'action')
       ,json_extract_path_text(properties,'gender')
       ,json_extract_path_text(properties,'g_locale')
       ,json_extract_path_text(properties,'abtest')
       ,json_extract_path_text(properties,'rsnsid')
from   events_raw 
where event = 'Referrals'
and trunc(ts) >=(select start_date from processed.tmp_start_date);  


delete from processed.referral_1
where date >=(select start_date from processed.tmp_start_date);

insert into processed.referral_1
select  t1.app
       ,t1.date
       ,coalesce(t2.country,'Unknown') as country
       ,count(distinct case when action = 'inviteClick'  then  uid else null end) as ctr_panel
       ,count(distinct case when action = 'open_recommend'  then  uid else null end) as panel_open
       ,count(distinct case when action = 'inviteRequest'  then  uid else null end) as send_user
       ,count(case when action = 'inviteRequest'  then uid else null end) as invite_sends
       ,count(distinct case when action = 'inviteRequest'  then  rsnsid else null end) as  been_invited_user
       ,count(distinct case when action = 'inviteRequestFromOther'  then  uid else null end) as send_user_from_other
       ,count(case when action = 'inviteRequestFromOther'  then uid else null end) as invite_sends_from_other
       ,count(distinct case when action = 'inviteRequestFromOther'  then  rsnsid else null end) as  been_invited_user_from_other
       ,count(case when action = 'clickRequest'  then uid else null end) as  invite_clicks
       ,count(case when action = 'acceptRequest'  then uid else null end) as  invite_accepted
       ,0 as  invite_accepted_till_now
       ,count(distinct case when action = 'getLevel5Gift'  then  uid else null end) as  rewards_level5_user
       ,count(case when action = 'getLevel5GiftCash'  then uid else null end) as  rewards_level5
       ,count(distinct case when action = 'getLevel20Gift'  then  uid else null end) as  rewards_level20_user
       ,count(case when action = 'getLevel20GiftCash'  then uid else null end) as  rewards_level20
       ,count(distinct case when action = 'getLevel30Gift'  then  uid else null end) as  rewards_level30_user
       ,count(case when action = 'getLevel30GiftCash'  then uid else null end) as  rewards_level30
from   processed.referrals t1
left join country t2
on t1.country_code = t2.country_code
where t1.date >=(select start_date from processed.tmp_start_date)
group by 1,2,3
;



delete from processed.referral_2
where date >=(select start_date from processed.tmp_start_date);

insert into processed.referral_2
select app
       ,date
       ,max(level)
       ,count(distinct uid) as users
from processed.referrals
where action = 'invitedLevel'
and date  >=(select start_date from processed.tmp_start_date)
group by 1,2;
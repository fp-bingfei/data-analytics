import psycopg2
import HTML
import datetime
import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
class InfraCostSummary:

    def __init__(self):
        self.currentMonth = datetime.date.today().strftime('%Y-%m')
        self.earlierMonth = (datetime.date.today() - datetime.timedelta(50)).strftime('%Y-%m')
        self.allgames = [('921287511715', 'zhenxuan.yang@funplus.com')]
        # self._allgames = [('131433655760', 'fruitscoot'),
        #                     ('135620544099', 'poker'),
        #                     ('147744843438', 'waf'),
        #                     ('266508744455', 'devops'),
        #                     ('289997590022', 'ShineZone'),
        #                     ('370884182004', 'battlewarship'),
        #                     ('376371647872', 'Niedan'),
        #                     ('403045566686', 'NLP'),
        #                     ('424384843432', 'farm'),
        #                     ('431036357599', 'royal'),
        #                     ('457994678495', 'royal'),
        #                     ('464507519694', 'galaxy'),
        #                     ('484933957969', 'aladdin'),
        #                     ('541015476938', 'farm'),
        #                     ('583300917620', 'Infra'),
        #                     ('614675119462', 'VideoCommunity'),
        #                     ('634033320744', 'xiyou'),
        #                     ('645037889100', 'LastWar'),
        #                     ('677477847658', 'Littlechef'),
        #                     ('683077249754', 'Platform'),
        #                     ('687657786659', 'Funplus'),
        #                     ('716504217123', 'Slots'),
        #                     ('735714818384', 'ffs'),
        #                     ('753756377410', 'kuaiya'),
        #                     ('838011746804', 'daota'),
        #                     ('873210112193', 'farm'),
        #                     ('889096239409', 'dragonwar'),
        #                     ('918819464745', 'farm'),
        #                     ('921287511715', 'data'),
        #                     ('924801593386', 'ha'),
        #                     ('941628076438', 'ffs'),
        #                     ('972926864295', 'mt2')]

    def run(self):
        conn = self.getConn()
        for gameinfo in self.allgames:
            tmpRes = self.getSQLResult(conn, gameinfo)
            htmltext = self.parseResult(tmpRes)
            self.sendMail(gameinfo, htmltext)
        conn.close()


    def getConn(self):
        db_host = 'kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
        db_port = '5439'
        db_name = 'kpi'
        db_username = 'biadmin'
        db_password = 'Halfquest_2014'
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def getSQLResult(self, conn, gameinfo):
        cursor = conn.cursor()

        command = """SELECT month
                    ,productname
                    ,coalesce(case when operation = 'BI Usage' then 'BI'
                        else reservedinstance end, 'N')
                    ,sum(truecost)
                    FROM db_usage.monthly_billing
                    WHERE linkedaccountid = '%s'
                    AND month >= '%s'
                    AND month < '%s'
                    GROUP BY 1,2,3
                    ORDER BY 2,3,1""" % (gameinfo[0], self.earlierMonth, self.currentMonth)

        try:
            cursor.execute(command)
            result = cursor.fetchall()
            print 'Fetching data for account: ' + gameinfo[0]
            print 'Success!'
        except Exception, e:
            result.append('1')
            print 'Fetching data for account: ' + gameinfo[0]
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def parseResult(self, tmpRes):
        htmltext = '''
                <h3>This is a report of your monthly Infrastructure cost, the number might be a little different with the number from Finance Team.</h3>
                '''

        table_header = ['Product Name', 'Usage Type', 'Cost', 'Previous Cost', 'Percent Diff']
        t = HTML.Table(header_row = table_header)
        total_cost = 0
        total_previous_cost = 0
        for line in tmpRes:
            if line[0] != self.earlierMonth:
                tmp_product = line[1]
                tmp_usage = line[2]
                tmp_cost = round(float(line[3].to_eng_string()), 2)
                total_cost += line[3]
                for otherline in tmpRes:
                    if (tmp_product == otherline[1] and tmp_usage == otherline[2] and otherline[0] == self.earlierMonth):
                        tmp_previous_cost = round(float(otherline[3].to_eng_string()), 2)
                        total_previous_cost += otherline[3]
                        percent_diff = str(round((tmp_cost - tmp_previous_cost) / tmp_previous_cost * 100, 2)) + '%' if tmp_previous_cost != 0 else '--'
                        color = 'lime' if percent_diff[0] == '-' else 'orange'
                        tmp_row = [HTML.TableCell(tmp_product, bgcolor = color),
                                    HTML.TableCell(tmp_usage, bgcolor = color),
                                    HTML.TableCell(tmp_cost, bgcolor = color),
                                    HTML.TableCell(tmp_previous_cost, bgcolor = color),
                                    HTML.TableCell(percent_diff, bgcolor = color)]
                    else:
                        continue
                t.rows.append(tmp_row)
            else:
                continue
        percent_diff = str(round(float(((total_cost - total_previous_cost) / total_previous_cost * 100).to_eng_string()), 2)) + '%' 
        total_cost = round(float(total_cost.to_eng_string()), 2)
        total_previous_cost = round(float(total_previous_cost.to_eng_string()), 2)
        color = 'lime' if percent_diff[0] == '-' else 'orange'
        tmp_row = [HTML.TableCell('Total Cost', bgcolor = color),
                    HTML.TableCell('', bgcolor = color),
                    HTML.TableCell(total_cost, bgcolor = color),
                    HTML.TableCell(total_previous_cost, bgcolor = color),
                    HTML.TableCell(percent_diff, bgcolor = color)]
        t.rows.append(tmp_row)


        # for line in tmpRes:
        #     tmp_row = [line[0], line[1]]
        #     if line[2] == 'Y':
        #         usageType = 'Reserved'
        #     elif line[2] == 'BI':
        #         usageType = 'BI Usage'
        #     else:
        #         usageType = 'On Demand'
        #     tmp_row.append(usageType)
        #     tmp_row.append(round(float(line[3].to_eng_string()), 2))
        #     t.rows.append(tmp_row)

        htmlcode = htmltext + str(t)
        return htmlcode
    def sendMail(self, gameinfo, htmltext):
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmltext,'html')
        msg.attach(html)

        recipients = [gameinfo[1]]
        msg['Subject'] = 'Infrastructure Cost ' + (datetime.date.today() - datetime.timedelta(30)).strftime("%Y-%m")
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ', '.join(recipients)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue

def main():
    ics = InfraCostSummary()
    ics.run()

if __name__ == '__main__':
    main()

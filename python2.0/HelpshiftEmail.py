import csv
import argparse
import boto3
import gzip
import sys

class HelpshiftEmail(object):
    def __init__(self,args):
        self.args = args
        self.s3src = 'com.funplus.helpshift'
        self.s3tgt = 'com.funplusgame.bidata'
        self.s3srckey = 'date=' + self.args["date"] + '/' + 'funplus-issues-daily.csv.gz'
        self.s3tgtkey = 'helpshift_email/' + self.args["date"] + '/' + 'funplus-email.csv'
        self.localsrc = '/tmp/funplus-issues-daily.csv.gz'
        self.localtgt = '/tmp/funplus-email.csv'
        
    def run(self):
        s3 = self.getS3Client()
        self.processS3file(s3, self.s3src, self.s3srckey, self.localsrc, self.localtgt)
        self.upLoadS3file(s3, self.s3tgt, self.s3tgtkey, self.localtgt)
    
    def getS3Client(self):
        s3 = boto3.session.Session(aws_access_key_id='AKIAJRKDNC52OAINDWKA',
                  aws_secret_access_key='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',
                  region_name='us-west-2').resource('s3')
        return s3
    def processS3file(self, s3, s3src, s3srckey, localsrc, localtgt):
        try:
            s3.meta.client.download_file(s3src, s3srckey, localsrc)
        except botocore.exceptions.ClientError as e:
            print 'Get an ClientError:', e.message
            sys.exit(1)        
        with gzip.open(localsrc, 'rb') as infile, open(localtgt, 'wb') as outfile:
            reader = csv.reader(infile)
            next(reader, None)
            writer = csv.writer(outfile)
            for row in reader:
                if row[3] != 'NULL':
                    line = []
                    line.append(self.args["date"])
                    line.append(row[3].replace("'",""))
                    line.append(row[5].replace("'",""))
                    line.append(row[8].replace("'",""))
                    writer.writerow(line)

    def upLoadS3file(self, s3, s3tgt, s3tgtkey, localtgt):
        try:
            s3.meta.client.upload_file(localtgt, s3tgt, s3tgtkey)
        except botocore.exceptions.ClientError as e:
            print 'Get an ClientError:', e.message
            sys.exit(1)
            
        
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--date',help='please provide date',required=True)
    args = vars(parser.parse_args())
    return args
    
def main():
    args = parse_args()
    he = HelpshiftEmail(args)
    he.run()
    
if __name__ == "__main__":
    main()

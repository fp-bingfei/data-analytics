import subprocess
import subprocess32
import datetime
import os
import time
import logging
from multiprocessing import Process, Manager, Value, Lock

logging.getLogger().addHandler(logging.StreamHandler())
logger = logging.getLogger('')
logger.setLevel(logging.INFO)

RETRY_TIMES_EACH = 3
TIMEOUT = 1800
current_running_id = Value('i', -1)
current_running_id_lock = Lock()


def prYellow(prt):
    print("\033[93m {}\033[00m" .format(prt))


def prRed(prt):
    print("\033[91m {}\033[00m" .format(prt))


def retry(tries, delay=1, factor=2):

    import time
    import math
    import functools

    if factor <= 1:
        raise ValueError("backoff must be greater than 1")

    tries = math.floor(tries)
    if tries < 0:
        raise ValueError("tries must be 0 or greater")

    if delay <= 0:
        raise ValueError("delay must be greater than 0")

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            _tries, _delay = tries, delay
            _tries += 1  # ensure we call func at least once
            while _tries > 0:
                try:
                    ret = func(*args, **kwargs)
                    return ret
                except Exception as e:
                    logger.info("Retry for %s with argument %s %s", func.__name__, str(args), str(kwargs))
                    _tries -= 1
                    # retried enough and still fail? raise original exception
                    if _tries == 0:
                        raise e
                    time.sleep(_delay)
                    # wait longer after each failure
                    _delay *= factor

        return wrapper

    return decorator


def generate_command(date, hour):
    command_pieces = ['/usr/lib/spark/bin/spark-submit',
                      '--master',
                      'yarn-client',
                      '--driver-memory',
                      '8G',
                      '--conf',
                      'spark.yarn.am.memory=4G',
                      '--num-executors',
                      '6',
                      '--executor-cores',
                      '5',
                      '--executor-memory',
                      '12G',
                      '--driver-class-path',
                      '"/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*"',
                      '--conf',
                      '"spark.executor.extraClassPath=/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*"',
                      '--jars',
                      '/usr/lib/spark/auxlib/json-schema-validator-2.2.6.jar,/usr/lib/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/usr/lib/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/usr/lib/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/usr/lib/spark/extras/lib/spark-streaming-kinesis-asl_2.10-1.5.0.jar,/usr/lib/spark/auxlib/amazon-kinesis-client-1.2.1.jar,/usr/lib/spark/auxlib/maxmind-db-1.0.0.jar,/usr/lib/spark/auxlib/geoip2-2.1.0.jar',
                      '--class',
                      'com.funplus.batch.CustomEventsParser',
                      '/home/hadoop/zhenxuan/kinesis-app-koa.jar',
                      'DragonwarCustom2.0_PROD',
                      '"%s/%s"' % (date.strftime("%Y-%m-%d"), str(hour).zfill(2)),
                      's3n://com.funplusgame.bidata/logserver/custom/',
                      's3n://com.funplus.datawarehouse/dragonwar_2_0/custom/',
                      's3n://com.funplus.datawarehouse/dragonwar_2_0/custom/scratch/',
                      'koa.global.prod,dragonwar.global.prod',
                      'true']
    return ' '.join(command_pieces)

def yarn_application_kill(application_name):
    # Kill the yarn application with the mini application id
    try:
        from subprocess import Popen, PIPE

        p = subprocess.Popen(['yarn',  'application', '-list'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = p.communicate(b"input data that is passed to subprocess' stdin")
        rc = p.returncode

        matched_lines = [line for line in output.split('\n') if application_name in line]
        application_ids = [line.split()[0] for line in matched_lines]
        if application_ids:
            print application_ids, 'to be killed'
            min_application_id = min(application_ids, key= lambda x: int(x.split('_')[1] + x.split('_')[2]))
            p_kill = subprocess.Popen(['yarn', 'application', '-kill', min_application_id], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            kill_output, err = p_kill.communicate(b"input data that is passed to subprocess' stdin")
            print kill_output
    except Exception:
        pass

def timeout_monitor(timestamp, start, timeout):
    print 'timestamp', timestamp
    print 'start', start
    print 'timeout', timeout

    now = datetime.datetime.now()
    while True:
        time.sleep(1)
        now = datetime.datetime.now()
        with current_running_id_lock:
            if timestamp != current_running_id.value:
                break
            if (now - start).seconds> timeout and timestamp == current_running_id.value:
                # Yarn kill application
                yarn_application_kill(application_name="DragonwarCustom2.0_PROD")
                break



def timeout_command(bash_file_path, timestamp, timeout):
    """call shell-command and either return its output or kill it
    if it doesn't normally exit within timeout seconds and return 1"""
    start = datetime.datetime.now()
    p = Process(target=timeout_monitor, args=(timestamp, start, timeout))
    p.start()
    try:
        output = subprocess32.check_output([bash_file_path], stderr=subprocess32.PIPE, timeout=timeout, shell=True)
    except Exception:
        return 1
    else:
        return 0
    finally:
        with current_running_id_lock:
            current_running_id.value = -1

@retry(RETRY_TIMES_EACH)
def run_task(date, hour, bash_file_path, shebang, timeout):
    timestamp = int(time.time())
    with current_running_id_lock:
        current_running_id.value = timestamp
    # generate the bash file
    command_to_run = generate_command(date, hour)
    with open(bash_file_path, "w") as f:
        f.write('\n'.join([shebang, command_to_run]))
        f.flush()
    os.chmod(bash_file_path, 0o777)

    logger.info('command: %s %s', date.strftime("%Y-%m-%d"), str(hour).zfill(2))
    prYellow('\n\n%s\n\n' % command_to_run)

    # The core to run the bash script:
    status = timeout_command(bash_file_path, timestamp, timeout)

    if status != 0:
        raise Exception("Exception Happened When handling subprocess!")



def parse_arguments():
    import argparse
    parser = argparse.ArgumentParser(description='')
    bjtime = datetime.datetime.utcnow()
    parser.add_argument(
        '-d', '--date', type=lambda x: datetime.datetime.strptime(x, "%Y-%m-%d"),
        default=(bjtime - datetime.timedelta(1)).strftime("%Y-%m-%d"))
    return parser.parse_args()


def main():
    args = parse_arguments()
    date = args.date
    failed_commands = []
    for hour in range(0, 24):
        try:
            run_task(date=date, hour=hour, bash_file_path='./tmp_bash_to_execute.sh', shebang='#!/bin/bash', timeout=TIMEOUT)
        except Exception as e:
            failed_commands.append(generate_command(date, hour))

    if failed_commands:
        num = len(failed_commands)
        logger.info("###" * 50)
        logger.info("Summary:\n\n%s command(s) Failed: \n\n", num)
        prRed('\n\n'.join(failed_commands))
        raise Exception("Python Script Failed!")

if __name__ == '__main__':
    main()

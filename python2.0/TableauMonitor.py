import psycopg2
import yaml
import pprint
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os
import HTML
import datetime
import time
import httplib
import urllib

class TableauMonitor:

    def __init__(self):
        self.results = {}
        self.daysBack = 4
        self.yamls = ['dotarena_2_0.yaml', 'lastwar_2_0.yaml', 'gz_2_0.yaml', 'xy_2_0.yaml','mt2_2_0.yaml', 'kpi-diandian.yaml'] # add kpi-funplus later

    def run(self):
        for filename in self.yamls:
            self.setDBParams(filename)
            conn = self.getConnection()
            result = self.runScripts(conn)
            self.results[self.db_name] = result
            self.recordRevenue(result)

        #for key in self.results.keys():
           # print 'key=%s, value=%s' % (key, self.results[key])
        self.sendMail()

    def runScripts(self, conn):        
        cursor = conn.cursor()
        result = []

        historyDay = datetime.date.today() - datetime.timedelta(self.daysBack)
        historyDay = historyDay.strftime('%Y-%m-%d')
        historyDay = "'" + historyDay + "'"

        command = self.command % historyDay
        try:
            cursor.execute(command)
            result.append(cursor.fetchall())
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Success!'
        except Exception, e:
            result.append('1')
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self, filename):
        filename = '/mnt/funplus/data-analytics/python/conf/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['redshift']['db_host']
        self.db_port = confMap['redshift']['db_port']
        self.db_name = confMap['redshift']['db_name']
        self.db_username = confMap['dbinfo']['username']
        self.db_password = confMap['dbinfo']['password']
        self.command = confMap['redshift']['command']
        #self.aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        #self.aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']

    def recordRevenue(self, result):
        filename = '/tmp/' + self.db_name + '_revenue.csv'
        f = open(filename, 'w')
        for line in result[0]:
            output = line[0].strftime('%Y-%m-%d') + '|' + line[1] + '|' + str(line[4]) + '\n'
            f.write(output)
        f.close()
        s3path = ' s3://com.funplus.datawarehouse/PNL/' + datetime.datetime.now().strftime('%Y-%m-%d') + '/'
        os.system('s3cmd put ' + filename + s3path)

    def getSlaInfo(self):

        htmltext = '''
                <p>The current SLA for All Games using 2.0 is before 11:30(UTC +8), for games using 1.1 is 15:30(UTC +8)</p>
                <a href='http://wiki.ifunplus.cn/display/BI/SLA+for+DATA+2.0'>More Info about our SLA, Click here</a>
                </ br>'''
        self.db_host = 'kpi-test.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
        self.db_name = 'dev'
        conn = self.getConnection()
        cursor = conn.cursor()
        legacy_games = ['dwtlw', 'dotarena', 'xy', 'mt2']

        sla_result = []
        sla_time = datetime.datetime(datetime.date.today().year, datetime.date.today().month,
                                    datetime.date.today().day, 11, 30, 00)
        today_str = "'" + datetime.date.today().strftime('%Y-%m-%d') + "'"
        command = self.command % today_str
        try:
            cursor.execute(command)
            sla_result = cursor.fetchall()
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Success!'
        except Exception, e:
            sla_result = []
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        table_header = ['Game', 'Finish Time']
        t = HTML.Table(header_row = table_header)
        for line in sla_result:
            tmp_row = [line[0]]
            if (line[1] + datetime.timedelta(hours=8) < sla_time or (line[0] in legacy_games and line[1] + datetime.timedelta(hours=4) < sla_time)):
                color = 'lime'
            elif (line[1] + datetime.timedelta(hours=5) < sla_time or (line[0] in legacy_games and line[1] + datetime.timedelta(hours=1) < sla_time)):
                color = 'orange'
            else:
                color = 'red'

            tmp_row.append(HTML.TableCell((line[1] + datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S'), bgcolor = color))
            t.rows.append(tmp_row)

        htmlcode = str(t) + htmltext

        return htmlcode


    def getAdjustInfo(self):

        htmltext = '''
                <p>If the diff of two sources is under 20%, GREEN cell, between 20% and 50%, ORANGE cell, other wise RED cell</p>
                </ br>'''
        self.setDBParams('kpi-diandian.yaml')
        conn = self.getConnection()
        cursor = conn.cursor()

        adjust_result = []
        yesterday_str = "'" + (datetime.date.today() - datetime.timedelta(1)).strftime('%Y-%m-%d') + "'"
        command = '''select a.game, a.cnt as adjust, b.cnt as bi from (
            select trunc(ts), game, count(1) as cnt from adjust where trunc(ts) = %s and userid is not null and userid != '' group by 1,2 ) a
            join
            (select install_date, app_id, count(1) as cnt from kpi_processed.dim_user where install_date = %s group by 1,2) b
            on a.game = b.app_id;''' % (yesterday_str, yesterday_str)

        try:
            cursor.execute(command)
            adjust_result = cursor.fetchall()
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Success!'
        except Exception, e:
            adjust_result = []
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        table_header = ['Game', 'Adjust Installs', 'BI Installs', 'Difference Percentage %']
        t = HTML.Table(header_row = table_header)
        for line in adjust_result:
            tmp_row = [line[0]]
            if ((line[1] < line[2] * 0.8 or line[1] * 0.8 > line[2])):
                color = 'orange'
            elif ((line[1] < line[2] * 0.5 or line[1] * 0.5 > line[2]) and abs(line[1] - line[2]) > 100):
                color = 'red'
            else:
                color = 'lime'

            diff = round(100.0 * (line[2] - line[1]) / line[2], 2)if line[2] != 0 else 'ERROR'
            tmp_row.append(HTML.TableCell(line[1], bgcolor = color))
            tmp_row.append(HTML.TableCell(line[2], bgcolor = color))
            tmp_row.append(HTML.TableCell(diff, bgcolor = color))

            t.rows.append(tmp_row)
        htmlcode = str(t) + htmltext

        return htmlcode

    def getMonitorInfo(self):
        htmltext ='''
        <p>History AVG: this means the average number from "4 days ago" to 2 "days ago"</p>
        <p>Yesterday: this means the number of yesterday</p>
        <p>If the number of yesterday is less than 0.7*AVG, the table cell will be 'ORANGE'; If the number of yesterday is less than 0.3*AVG, the table cell will be 'RED'; the table cell will be 'GREEN' otherwise</p>
        </ br>'''

        table_header = ['APP', 'App_id', 'New installs', 'DAU', 'Revenue']

        t = HTML.Table(header_row = table_header)
        tmp_row = ['History AVG', 'Yesterday'] * 3
        t.rows.append(tmp_row)
        yesterday = datetime.date.today() - datetime.timedelta(1)

        for key in self.results.keys():
            app_ids = []
            if (self.results[key][0] == '1'):
                print "Something is wrong with the Game %s" % key
                item = [key] + [''] * 7
                continue
            else:
                for value in self.results[key][0]:
                    if (value[1] not in app_ids):
                        app_ids.append(value[1])

            for app_id in app_ids:
                tmp_row = [key, app_id]
                tmp_historySUM = [0] * (self.daysBack - 1)
                tmp_yesterday = [0] * (self.daysBack - 1)
                colors = ['lime'] * (self.daysBack - 1)
                for value in self.results[key][0]:
                    if (value[1] == app_id):
                        if (value[0] == yesterday):
                            tmp_yesterday[0] += value[2]
                            tmp_yesterday[1] += value[3]
                            tmp_yesterday[2] += float(value[4]) if value[4] else 0
                        else:
                            tmp_historySUM[0] += value[2]
                            tmp_historySUM[1] += value[3]
                            tmp_historySUM[2] += float(value[4]) if value[4] else 0
                for i in range(self.daysBack - 1):
                    if (tmp_yesterday[i] >= tmp_historySUM[i] / (self.daysBack - 1) * 0.7):
                        pass
                    elif (tmp_yesterday[i]<= tmp_historySUM[i] / (self.daysBack - 1) * 0.3 and (tmp_yesterday[i] + 1000) <= tmp_historySUM[i] / (self.daysBack - 1) ):
                        if (app_id not in ['bv.global.prod', 'ffs.cn.prod']):
                            colors[i] = 'red'
                        else:
                            colors[i] = 'orange'
                    else:
                        colors[i] = 'orange'
                    tmp_row.append(HTML.TableCell(round(tmp_historySUM[i] / (self.daysBack - 1), 4), bgcolor = colors[i]))
                    tmp_row.append(HTML.TableCell(tmp_yesterday[i], bgcolor = colors[i]))

                t.rows.append(tmp_row)

        htmlcode = str(t) + htmltext

        return htmlcode


    def sendMail(self):
        #GET SLA INFO
        sla_table = self.getSlaInfo()
        adjust_table = self.getAdjustInfo()
        monitor_table = self.getMonitorInfo()
        htmlcode = monitor_table + sla_table + adjust_table
        tmp = htmlcode.replace('<TH>','<TH colspan = "2">', 5)
        html_final = tmp.replace('<TH colspan = "2">', '<TH rowspan = "2">', 2)
        msg = MIMEMultipart('alternative')
        html = MIMEText(html_final,'html')
        msg.attach(html)

        #This part is used for devops monitor
        green_cnt = html_final.count('bgcolor="lime"')
        orange_cnt = html_final.count('bgcolor="orange"')
        red_cnt = html_final.count('bgcolor="red"')
        if (red_cnt > 2):
            status = 2
        elif (red_cnt > 0):
            status = 1
        else:
            status = 0
        devopsMessage = "Red Cell Count:" + str(red_cnt) + "\nOrange Cell Count:" + str(orange_cnt) + "\nGreen Cell Count:" + str(green_cnt) + "\n"
        params = urllib.urlencode({"category": "data", "status": status, "service": "dataMonitor", "tags": "Tableau", "checktime": int(time.time()), "message": devopsMessage, "space_uuid": 'fe62a568-7eee-410b-8ca6-ddf1e2027903', "token": '0539B7B5A3BB7EC85BE4A9DEE5D4D395'})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        conn = httplib.HTTPConnection("duty.funplus.io")
        conn.request("POST", "/Event/index/", params, headers)
        response = conn.getresponse()
        print "The http response of sending notification to Devops is:"
        print response.status, response.reason
        conn.close()
        #This part is used for devops monitor

        recipients = ['data@funplus.com', 'zhenxuan.yang@funplus.com']
        msg['Subject'] = 'Data Validation results of ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ', '.join(recipients)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue


def main():
    tm = TableauMonitor()
    tm.run()

if __name__ == '__main__':
    main()
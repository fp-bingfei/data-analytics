from datetime import date, timedelta
import hashlib
import requests
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.sql import text

ffs_conn = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
engine = create_engine(ffs_conn)

def df_from_dates(start_date, end_date=date.today()):
    pub_id = 86535
    api_token = '8d1760c83344d210c35af3aeff09f395'
    
    params_raw = 'aggregation_level=days&end_date={ed}&group_by=applications&start_date={sd}&'
    params = params_raw.format(sd=start_date.strftime('%Y-%m-%d'), ed=end_date.strftime('%Y-%m-%d'))
    key = '/publishers/v1/'+str(pub_id)+'/statistics.json&'+params+api_token
    hashkey = hashlib.sha1(key).hexdigest()

    url = 'https://api.sponsorpay.com/publishers/v1/'+str(pub_id)+'/statistics.json?'+params+'hashkey='+hashkey
    
    r = requests.get(url)

    if r.status_code==200:
        r= r.json()['applications']
        df_list = []

        for o in r:
            df = pd.DataFrame(o)
            df['date'] = pd.date_range(start_date, end_date)
            df_list.append(df)

        ret = pd.concat(df_list)
        return ret[ret['clicks']>0]
    else:
        return pd.DataFrame()

sd = date.today()-timedelta(days=7)
df = df_from_dates(sd)

if len(df)>0:
    conn = engine.connect()
    sql = text("delete from processed.daily_ad_revenue where date>=:sd").bindparams(sd=sd)
    conn.execute(sql)
    df.to_sql('daily_ad_revenue', engine, schema='processed', if_exists='append', index=False)
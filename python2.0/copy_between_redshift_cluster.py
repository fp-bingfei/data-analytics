###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse

import psycopg2
import yaml
import pprint
import argparse


class CopyFactToCustom:
    def __init__(self, args):
        self.args = args

    def run(self):
        self.setDBParams()
        # conn = self.getConnection()
        source_conn = self.getConnection()
        target_conn = self.getTargetConnection()
        if self.args['preSqlForTarget'] != None:
            self.executeTargetPrepareSql(target_conn)
        else:
            self.args['preSqlForTarget'] = ''

        # self.unload(conn)
        # self.copy()
        self.unload(source_conn)
        source_conn.commit()
        self.copy(target_conn)
        target_conn.commit()

    def executeTargetPrepareSql(self, target_conn):
        deleteSql = self.args['preSqlForTarget']
        cursor = target_conn.cursor()
        cursor.execute(deleteSql)
        print ("\nprepare sql for target DB executed: " + deleteSql)

    def getCommandOptions(self):
        commandOptions = self.args['command_options'].split(',')
        commandOptionStr = ''
        for commandOpt in commandOptions:
            cleanStr = commandOpt.strip()
            # - added single quote for string starts with s3://
            commandOptionStr += (' ' + '\'' + cleanStr + '\'') if cleanStr.startswith('s3://') else (' ' + cleanStr)
        return commandOptionStr

    def copy(self, conn):
        query = "COPY %s FROM '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' FILLRECORD %s " \
                "maxerror  %s " % (
                    self.args['targetTable'], self.args['s3path'], self.aws_access_key_id, self.aws_secret_access_key,
                    self.getCommandOptions(), (str(max(int(self.args['maxerror']), 100)) if (
                        self.args['maxerror'] and self.args['maxerror'].isdigit()) else '100'))
        print("\ncopy query:" + query)
        cursor = conn.cursor()
        cursor.execute(query)
        # conn.commit()
        print("\nfinish copy query")

    def unload(self, conn):
        selectSql = "SELECT * FROM %s %s" % (self.args['sourceTable'], self.args['selectFromSource'])
        query = "UNLOAD ('%s') TO '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' %s" % (
            selectSql, self.args['s3path'], self.aws_access_key_id, self.aws_secret_access_key,
            self.getCommandOptions())
        print("\nupload query: " + query)
        cursor = conn.cursor()
        cursor.execute(query)
        # conn.commit()
        print("\nfinish upload query")

    # Get source DB connection
    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
            self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    # Get target DB connection
    def getTargetConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
            self.target_db_host, self.target_db_port, self.target_db_name, self.target_db_username,
            self.target_db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self):
        # Source DB params setting
        f = open(self.args['sourceConf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['redshift']['db_host']
        self.db_port = confMap['redshift']['db_port']
        self.db_name = confMap['redshift']['db_name']
        self.db_username = confMap['dbinfo']['username']
        self.db_password = confMap['dbinfo']['password']
        self.aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        self.aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']

        # Target DB params setting
        f = open(self.args['targetConf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.target_db_host = confMap['redshift']['db_host']
        self.target_db_port = confMap['redshift']['db_port']
        self.target_db_name = confMap['redshift']['db_name']
        self.target_db_username = confMap['dbinfo']['username']
        self.target_db_password = confMap['dbinfo']['password']
        self.target_aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        self.target_aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']


def parse_args():
    parser = argparse.ArgumentParser(description='Copy table between two redshift cluster')
    parser.add_argument('-sc', '--sourceConf', help='Config file in YAML format want to copy out', required=True)
    parser.add_argument('-st', '--sourceTable',
                        help='RedShift DB table name want to copy out. e.g. schema_name.table_name',
                        required=True)
    parser.add_argument('-s3path', '--s3path', help='The s3path of data source', required=True)
    parser.add_argument('-tc', '--targetConf', help='Config file in YAML format want to copy in', required=True)
    parser.add_argument('-tt', '--targetTable',
                        help='RedShift DB table name want to copy in. e.g. schema_name.table_name', required=True)
    parser.add_argument('-select', '--selectFromSource', help='e.g.  where x=\'xxx\' and y=yyyy', required=False)
    parser.add_argument('-pre', '--preSqlForTarget',
                        help='SQL statement executed prior to target table . e.g. delete from table where ...',
                        required=False)
    parser.add_argument('-command-options', '--command-options',
                        help='The COPY command options e.g. json,jsonpath,gzip or delimiter,\'\\t\',gzip',
                        required=True)
    parser.add_argument('-maxerror', '--maxerror', help='The max error can be ignored, default is 100', required=False)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    c2c = CopyFactToCustom(args)
    c2c.run()


if __name__ == '__main__':
    main()

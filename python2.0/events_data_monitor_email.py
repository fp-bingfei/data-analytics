#! /usr/bin/python

import smtplib
import argparse

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime

class EventsDataMonitor:
	# Create message container - the correct MIME type is multipart/alternative.
	msg = MIMEMultipart('alternative')
	# msg['Subject'] = ''
	msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
	msg['To'] = "bidev@funplus.com"


	def __init__(self, args):
		self.args = args
		self.msg['Subject'] = self.args['subject']

	def run(self):

			
		table = '<h3>This report was sent at: ' + datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S") + ' <b>UTC</b></h3><br>'
		table += '<table border=1>'	
		with open(self.args['file'], "r") as f:
			columnNames = '<tr>' 
			firstLine = f.readline().split('\t')
			for col in firstLine:
				columnNames += '<td>'+col.strip()+'</td>'
			columnNames += '</tr>' 
			table += columnNames + "\n"
			previousCols = None
			for line in f:
				cols = line.strip().split('\t')
					
				# ----------------------------------------------------------------------------------------------------------------
				# Data format in source file
				# game_name	app	event_date	event_hour	event_cnt	session_cnt	payment_cnt	newuser_cnt	
				# cols[0] : game_name
				# cols[1] : app
				# ....
				# ----------------------------------------------------------------------------------------------------------------
				row = '<tr>'
				for col in cols:
					row += '<td>'+col.strip()+'</td>'
				row += '</tr>' + "\n"

				if (previousCols is not None):	
					# If same locale / app name
                                        if (cols[1].strip() == previousCols[1].strip()):
                                                # and same event_date
                                                if (cols[2].strip() == previousCols[2].strip()):
							# if hour diff is more than 1 
                                                        if ( (int(previousCols[3].strip()) - int(cols[3].strip())) > 1):
								table += '<tr bgcolor="RED"><td>' + cols[0].strip() + '</td><td>' + cols[1].strip() + '</td><td>' + cols[2].strip() + '</td><td>' + str(int(previousCols[3].strip()) - 1).zfill(2) + '</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>' + "\n"
						# different event_date
						else:
							if (previousCols[3].strip() == 0 and cols[3].strip() != 23):
								table += '<tr bgcolor="RED"><td>' + cols[0].strip() + '</td><td>' + cols[1].strip() + '</td><td>' + cols[2].strip() + '</td><td>23</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>'
							if (previousCols[3].strip() != 0 and cols[3].strip() == 23):
								table += '<tr bgcolor="RED"><td>' + cols[0].strip() + '</td><td>' + cols[1].strip() + '</td><td>' + previousCols[2].strip() + '</td><td>00</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>'
							
					else:
						table += '</table>' + "\n"
						table += '<br><br>' + "\n"
						table += '<table border=1>' + "\n"
						table += columnNames + "\n"
				
				table += row
				previousCols = cols
					
		table += '</table>'	
		print table
		self.email(table)	

	def email(self, body):
		
		# Create the body of the message (a plain-text and an HTML version).
		text = ""
		html = '<html><head><body>'
		html += body
		html += '</body></head></html>'

		# Record the MIME types of both parts - text/plain and text/html.
		part1 = MIMEText(text, 'plain')
		part2 = MIMEText(html, 'html')

		self.msg.attach(part1)
		self.msg.attach(part2)

		for x in range(0,3):
			try:
				s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
				s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
				s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())
				s.quit()
				break
			except smtplib.socket.error:
				print ("==== smtplib.socket.error ===\n")
				print ("==== Re-trying ....  ====\n")
				continue
			except smtplib.SMTPException:
				print ("==== smtplib.SMTPException ====\n")
				print ("==== Re-trying ....  ====\n")
				continue


def parse_args():
        parser = argparse.ArgumentParser(description='Events Hourly Data Monitor')
        parser.add_argument('-s','--subject', help='Email Subject', required=False)
        parser.add_argument('-f','--file', help='Full path of data filename', required=True)
        args = vars(parser.parse_args())
        return args

def main():
        args = parse_args()
        edm = EventsDataMonitor(args)
        edm.run()


if __name__ == '__main__':
        main()

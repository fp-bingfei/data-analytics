import concurrent.futures as futures
import pandas as pd
from sqlalchemy import create_engine
from datetime import datetime, date
from pandas_to_redshift import *
from sqlalchemy import text
import os

df_list = []

size_sql = '''select database, schema, "table" as table_name, size, pct_used, tbl_rows 
    from SVV_TABLE_INFO 
    order by size desc 
    ;'''

clusters = {'bicluster': 'ffs', 'bicluster-funplus': 'fruitscoot', 'custom-diandian': 'custom', 'bicluster-distribution': 'daota'}
conn_str = "redshift+psycopg2://superadmin:Funp1usData@{cluster}.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/{db}"

def task(cluster, db):
    e= create_engine(conn_str.format(cluster=cluster, db=db))
    df = pd.read_sql_query(size_sql, e)
    df['date'] = datetime.datetime.utcnow().date()
    df['cluster'] = cluster
    print cluster, db, len(df)
    return df

for c in clusters:
    print c
    engine = create_engine(conn_str.format(cluster=c, db=clusters[c]))
    sql = '''SELECT distinct datname FROM pg_database;'''
    db_df = pd.read_sql_query(sql, engine)
    dbs = db_df['datname'].values

    print '*** Main thread waiting'
    
    with futures.ThreadPoolExecutor(8) as executor:
        fs = [executor.submit(task, c, db) for db in dbs if not db.startswith('template')]
        for i, f in enumerate(futures.as_completed(fs)):
            df_list.append(f.result())

ret = pd.concat(df_list)

ret['size'] = ret['size'].astype(np.int64)
ret['tbl_rows'] = ret['tbl_rows'].astype(np.int64)
#ret.to_csv('{home}/cluster_usage/{date}.csv'.format(home=os.path.expanduser('~'), date=datetime.datetime.utcnow().date().strftime('%Y%m%d')), index=False)

del_sql = '''delete from db_usage.cluster_usage where date='{date}';'''.format(date=datetime.datetime.utcnow().date().strftime('%Y-%m-%d'))
kpi_engine = create_engine(conn_str.format(cluster='kpi-diandian', db='kpi'))
kpi_engine.execute(text(del_sql).execution_options(autocommit=True))

h = 'kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
df_to_redshift(ret, 'db_usage.cluster_usage', h, 'kpi', append=True)
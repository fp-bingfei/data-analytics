

import MySQLdb
import httplib
import datetime
import time

class AdjustCallbackMonitor:
    def __init__():
    	pass
    
    def run():
    	self.mysqlMonitor()
    	self.domainMonitor()

    def mysqlMonitor(self):
        db = MySQLdb.connect(host='funplus-adjust.cr0ktgcuumkc.us-west-2.rds.amazonaws.com', user='worker', passwd='r0qUqnWMrROyKKDh', db="infras", use_unicode=True, charset="utf8")
        cur = db.cursor()

        sql = '''select case 
            when UNIX_TIMESTAMP(itime) >= UNIX_TIMESTAMP(NOW()) - 600 then 1
            when UNIX_TIMESTAMP(itime) >= UNIX_TIMESTAMP(NOW()) - 1200 and UNIX_TIMESTAMP(itime) < UNIX_TIMESTAMP(NOW()) - 600 then 2
            when UNIX_TIMESTAMP(itime) >= UNIX_TIMESTAMP(NOW()) - 1800 and UNIX_TIMESTAMP(itime) < UNIX_TIMESTAMP(NOW()) - 1200 then 3
            when UNIX_TIMESTAMP(itime) >= UNIX_TIMESTAMP(NOW()) - 2400 and UNIX_TIMESTAMP(itime) < UNIX_TIMESTAMP(NOW()) - 1800 then 4
            end as period,
            count(1)
            from fp_adjust_callback
            where UNIX_TIMESTAMP(itime) > UNIX_TIMESTAMP(NOW()) -2400
            group by period;'''
        cur.execute(sql)
        res = cur.fetchall()
        
        if (res[1][1] < 0.05 * (res[2][1] + res[3][1] + res[4][1]) / 3):
            status = 2
        else if (res[1][1] < 0.3 * (res[2][1] + res[3][1] + res[4][1]) / 3):
            status = 1
        else:
            status = 0

        devopsMessage = "Adjust MySQL server is in error, PLEASE CALL MATT AT 13651843700, OR ZHENXUAN AT 18614077033"
        params = urllib.urlencode({"category": "daily", "status": status, "service": "dataMonitor", "tags": "AdjustCallbackSQLServer", "checktime": int(time.time()), "message": devopsMessage})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        conn = httplib.HTTPConnection("duty.socialgamenet.com")
        conn.request("POST", "/Event/create/", params, headers)
        response = conn.getresponse()
        conn.close()


    def domainMonitor(self):
        conn = httplib.HTTPConnection("thirdparty.funplus.com")
        conn.request("GET", "/status")
        response = conn.getresponse()
        if (int(response.status) == 200):
        	status = 0
        else:
        	status = 2
        conn.close()

		devopsMessage = "Adjust Domain is in error, PLEASE CALL MATT AT 13651843700, OR ZHENXUAN AT 18614077033"
        params = urllib.urlencode({"category": "daily", "status": status, "service": "dataMonitor", "tags": "AdjustCallbackDomain", "checktime": int(time.time()), "message": devopsMessage})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        conn = httplib.HTTPConnection("duty.socialgamenet.com")
        conn.request("POST", "/Event/create/", params, headers)
        response = conn.getresponse()
        conn.close()

def main():
	acm = AdjustCallbackMonitor()
	acm.run()

if __name__ = '__main__':
    main()

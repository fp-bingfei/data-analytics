from flask import Flask
from flask import abort, request
import logging
from logging.handlers import TimedRotatingFileHandler
import MySQLdb
import datetime

app = Flask(__name__)

#default testing parameters, should be overridden in config file
app.config['ADJUST_DB_HOST'] = 'localhost'
app.config['ADJUST_DB_USER'] = 'root'
app.config['ADJUST_DB_PW'] = ''
app.config['ADJUST_LOG_FILE'] = '/tmp/adjust_callback.log'
app.config['DEBUG'] = True

#take env var to overwrite the configuration
app.config.from_envvar('ADJUST_CALLBACK_SETTINGS', silent=True)

#setup logging
logger = logging.getLogger()
fmt = '%(asctime)s %(levelname)s %(pathname)s %(module)s.%(funcName)s Line:%(lineno)d %(message)s'
hdlr = TimedRotatingFileHandler(app.config['ADJUST_LOG_FILE'], 'midnight', 1, 100)
hdlr.setFormatter(logging.Formatter(fmt))
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

@app.route('/adjust_first_time_open/')
def adjust_first_time_open():
    #this entry point requires fpid
    return _adjust(True, request)

@app.route('/adjust_install/')
def adjust_install():
    #this entry point has no fpid, and also no click_time
    return _adjust(False, request)

def _adjust(first_time, request):

    game = request.args.get('game', '')
    tracker = request.args.get('tracker', '')
    tracker_name = request.args.get('tracker_name', '')
    adid = request.args.get('adid', '')
    app_id = request.args.get('app_id', '')
    ip_address = request.args.get('ip_address', '')
    idfa = request.args.get('idfa', '')
    android_id = request.args.get('android_id', '')
    idfa_md5 = request.args.get('idfa_md5', '')
    mac_sha1 = request.args.get('mac_sha1', '')
    mac_md5 = request.args.get('mac_md5', '')
    gps_adid = request.args.get('gps_adid', '')
    device_name = request.args.get('device_name', '')
    os_name = request.args.get('os_name', '')
    os_version = request.args.get('os_version', '')
    country = request.args.get('country', '')
    installed_at=request.args.get('installed_at', None)
    #Default is current utc time
    installed_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    if installed_at is not None:
        installed_time = datetime.datetime.fromtimestamp(long(installed_at)).strftime('%Y-%m-%d %H:%M:%S')

    #only in first time, not install
    click_time = request.args.get('click_time', '')
    click_id = request.args.get('network_click_id', '')

    fpid = None

    if first_time:
        #candidates for fpid, which only exist in first time
        userid = request.args.get('userid', None)
        snsid = request.args.get('snsid', None)
        fpid_param = request.args.get('fpid', None)
        user_id = request.args.get('user_id', None)

        #pick first one that exist, in this order
        for f in [userid, snsid, fpid_param, user_id]:
            if f:
                fpid = f
                break

    if not game or not tracker or not tracker_name or not adid or not app_id or not ip_address or not country or (first_time and not fpid):
        app.logger.warning("Missing required arguments: game=%s tracker=%s tracker_name=%s adid=%s app_id=%s ip_address=%s country=%s first_time=%s fpid=%s request=%s",
                           game, tracker, tracker_name, adid, app_id, ip_address, country, first_time, fpid, request.url)
        abort(404) #matches previous behavior

    try:

        db = MySQLdb.connect(host=app.config['ADJUST_DB_HOST'], user=app.config['ADJUST_DB_USER'], passwd=app.config['ADJUST_DB_PW'], db="infras", use_unicode=True, charset="utf8")
        cur = db.cursor()

        if first_time:

            sql = """insert into fp_adjust_callback (adid,fpid,game,tracker,tracker_name,app_id,ip_address,idfa,android_id,mac_sha1,idfa_md5,country,itime,mac_md5,gps_adid,device_name,os_name,os_version,click_time,click_id)
                     values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                     ON DUPLICATE KEY UPDATE fpid=%s,game=%s,tracker=%s,tracker_name=%s,app_id=%s,ip_address=%s,idfa=%s,android_id=%s,mac_sha1=%s,idfa_md5=%s,country=%s,mac_md5=%s,gps_adid=%s,device_name=%s,os_name=%s,os_version=%s,click_time=%s,click_id=%s"""
            cur.execute(sql, (adid, fpid, game, tracker, tracker_name, app_id, ip_address, idfa, android_id, mac_sha1, idfa_md5, country, installed_time, mac_md5, gps_adid, device_name, os_name, os_version, click_time, click_id,
                                    fpid, game, tracker, tracker_name, app_id, ip_address, idfa, android_id, mac_sha1, idfa_md5, country, mac_md5, gps_adid, device_name, os_name, os_version, click_time, click_id))
        else:

            sql = """insert into fp_adjust_callback (adid,game,tracker,tracker_name,app_id,ip_address,idfa,android_id,mac_sha1,idfa_md5,country,itime,mac_md5,gps_adid,device_name,os_name,os_version,click_id)
                     values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                     ON DUPLICATE KEY UPDATE game=%s,tracker=%s,tracker_name=%s,app_id=%s,ip_address=%s,idfa=%s,android_id=%s,mac_sha1=%s,idfa_md5=%s,country=%s,mac_md5=%s,gps_adid=%s,device_name=%s,os_name=%s,os_version=%s,click_id=%s"""
            cur.execute(sql, (adid, game, tracker, tracker_name, app_id, ip_address, idfa, android_id, mac_sha1, idfa_md5, country, installed_time, mac_md5, gps_adid, device_name, os_name, os_version, click_id,
                                    game, tracker, tracker_name, app_id, ip_address, idfa, android_id, mac_sha1, idfa_md5, country, mac_md5, gps_adid, device_name, os_name, os_version, click_id))

        db.commit()
        db.close()
        return "OK", 200

    except Exception, e:
        app.logger.exception("Error in sql")
        return str(e), 500

@app.route('/status')
def status():
    return "OK", 200

if __name__ == '__main__':
    app.run(debug=app.config['DEBUG'])
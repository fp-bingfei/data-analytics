import os
import argparse
from boto.s3.connection import S3Connection
from pprint import pprint
import csv
import gzip

locales  = {
    "farm" : [  "farm.ae.prod",
                "farm.br.prod",
                "farm.de.prod",
                "farm.fr.prod",
                # "farm.hyves.prod",
                "farm.it.prod",
                # "farm.nk.prod",
                "farm.nl.prod",
                "farm.pl.prod",
                # "farm.plingaplay.prod",
                # "farm.spil.prod",
                "farm.th.prod",
                "farm.tr.prod",
                "farm.tw.prod",
                "farm.us.prod",
                # "farm.vz.prod"
    ]
}

def main():
    args = parse_args()
    
    s3_connection = S3Connection(
        aws_access_key_id="AKIAJIXNS6MDKWNPEW3Q",
        aws_secret_access_key="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"
    )

    output_dir = args.get("output")
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    app = args.get("app")
    start_date = args.get("start_date")
    
    result_list = list()
    bucket = s3_connection.get_bucket("com.funplusgame.bidata")
    for locale in locales.get(app):
        for i in xrange(24):
            hour = "%02d" % i
            path = "fluentd/{locale}/{start_date}/{hour}/".format(locale=locale,start_date=start_date,hour=hour)
            print path
            items = bucket.list(path)
            counter = 0;
            output_record = list()
            
            for item in items:
                counter = counter + 1
            if counter == 0:
                output_record.append("s3://com.funplusgame.bidata/" + path)
            
            if len(output_record) > 0:
                result_list.append(output_record)
                
    pprint(result_list)
    result_list.insert(0,["file_path","number of files"])
    
    filepath = os.path.join(output_dir,app+"_s3_check_"+start_date.replace("/","-")+".gz") 
    with gzip.open(filepath,'w') as f:
        writer = csv.writer(f,delimiter='\t',quoting=csv.QUOTE_MINIMAL,quotechar='^')
        writer.writerows(result_list)
    
    print "Write complete: " + filepath 

        
        
    


def parse_args():
    parser = argparse.ArgumentParser(description='S3 Data Checker')
    parser.add_argument('-app','--app', help='The appfor which the data will be checked.', required=True)
    parser.add_argument('-start_date','--start_date', help='The start date for which the data will be checked. YYYY/MM/DD', required=True)
    parser.add_argument('-output','--output', help='The output directory', required=True)
    args = vars(parser.parse_args())
    return args

if __name__ == "__main__":
    main()
    
    
        

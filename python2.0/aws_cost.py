import datetime
import boto
import psycopg2
import json
import urllib
import yaml
import os
import math

class AwsCost:

    def __init__(self):
        pass

    def run(self):
        self.getSourceData()
        conn = self.getConnection()
        self.runScripts(conn)


    def getSourceData(self):
        s3_conn = boto.connect_s3()
        bucket = s3_conn.get_bucket('com.funplus.bitest')

        pathToCsv = '/mnt/funplus/analytics/awsCost/tmp.csv'
        key = 'robin/aws_cost.csv'
        csv_key = bucket.get_key(key)

        if csv_key:
            csv_key.get_contents_to_filename(pathToCsv)
        else:
            print 'csv file is not available in s3'

        yesterday = datetime.date.today() - datetime.timedelta(2)
        before_yesterday = datetime.date.today() - datetime.timedelta(3)
        yesterday = yesterday.strftime('%Y-%m-%d')
        before_yesterday = before_yesterday.strftime('%Y-%m-%d')

        url = 'http://cost.socialgamenet.com/Report/costByProject?project=bidata&start=%s&end=%s' % (before_yesterday, yesterday)
        response = urllib.urlopen(url)
        json_object = yaml.safe_load(response.read())

        colnames = ['Usage_Date', 's3', 'ebs', 'ec2', 'ec2_instance', 'Amazon Kinesis', 'cloudwatch', 'data_pipeline', 'dynamodb', 'emr', 'rds', 'redshift', 'ses', 'simpledb', 'sns']
        newline = yesterday

        for key in colnames:
            if key != 'Usage_Date':
                newline += '\t' + str(round(float(json_object['data'][yesterday].get(key) if json_object['data'][yesterday].get(key) else 0), 2))

        f = open(pathToCsv, "a")
        f.write(newline + '\n')
        f.close()

        csv_key.set_contents_from_filename('/mnt/funplus/analytics/awsCost/tmp.csv')
        os.remove('/mnt/funplus/analytics/awsCost/tmp.csv')
        s3_conn.close()


    def runScripts(self, conn):
        pathToScript = '/mnt/funplus/analytics/awsCost/aws_cost.sql'
        f = open(pathToScript, "r")
        commands = f.read()
        f.close()

        cursor = conn.cursor()
        result = []

        try:
            cursor.execute(commands)
            conn.commit()
        except Exception, e:
            result.append('1')
            print e

        return result

    def getConnection(self):
        db_host = 'bicluster-distribution.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
        db_port = '5439'
        db_name = 'robin'
        db_username = 'biadmin'
        db_password = 'Halfquest_2014'
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)
        conn = psycopg2.connect(conn_string)
        return conn

def main():
    ac = AwsCost()
    ac.run()

if __name__ == '__main__':
    main()

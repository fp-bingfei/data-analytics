import boto.emr
import urllib2
import json
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import HTML
import datetime
import time

class EMRClusterMonitor:
    def __init__(self):
        self.awsClusterInfos = []
        self.results = []

    def run(self):
        conn = self.getConn()
        jobFlows = self.getJobFlows(conn)
        for jobFlow in jobFlows:
            awsClusterInfo = self.getAWSClusterInfo(jobFlow)
            result = self.verifyNodes(awsClusterInfo)
            self.awsClusterInfos.append(awsClusterInfo)
            self.results.append(result)
        self.sendMail()


    def getConn(self):
        try:
            conn = boto.emr.connect_to_region("us-west-2")
        except Exception, e:
            print e
        else:
            pass
        finally:
            pass
        
        return conn

    def getJobFlows(self, conn):
        try:
            jobFlows  = conn.describe_jobflows(states = ["RUNNING","WAITING"])
        except Exception, e:
            print e
        else:
            pass
        finally:
            pass
        
        return jobFlows


    def getAWSClusterInfo(self, jobFlow):
        clusterName = jobFlow.name
        masterName = jobFlow.masterpublicdnsname
        hadoopVersion = jobFlow.hadoopversion
        instanceCount = -1
        for instancegroup in jobFlow.instancegroups:
            instanceCount += int(instancegroup.instancerunningcount)
        print clusterName, hadoopVersion
        return (clusterName, masterName, instanceCount, hadoopVersion)


    def getYarnInfo(self, url):
        print url
        nodesInfo = urllib2.urlopen(url)
        print 'success'
        jsonNodes = json.loads(nodesInfo.read())
        activeNodesCount = 0
        for node in jsonNodes["nodes"]["node"]:
            if node["state"] == "RUNNING":
                activeNodesCount += 1

        return activeNodesCount

    def verifyNodes(self, awsClusterInfo):
        if awsClusterInfo[3].startswith("2.6"):
            url = "http://" + awsClusterInfo[1] + ":8088/ws/v1/cluster/nodes"
        else:
            url = "http://" + awsClusterInfo[1] + ":9026/ws/v1/cluster/nodes"
        print awsClusterInfo[0]
        activeNodesCount = self.getYarnInfo(url)

        return (activeNodesCount, (activeNodesCount == awsClusterInfo[2]))

    def sendMail(self):
        htmltext = '''
        <p>AWS instances: running instances on AWS console, both core instances and task instances</p>
        <p>Active Nodes: active nodes in the hadoop yarn console</p>
        '''

        table_header = ["Cluster Name", "AWS Instances", "Active Nodes", "Cluster Status"]
        t = HTML.Table(header_row = table_header)
        for i in range(len(self.results)):
            color = "lime" if self.results[i][1] else "red"
            tmp_row = []
            tmp_row.append(HTML.TableCell(self.awsClusterInfos[i][0], bgcolor = color))
            tmp_row.append(HTML.TableCell(self.awsClusterInfos[i][2], bgcolor = color))
            tmp_row.append(HTML.TableCell(self.results[i][0], bgcolor = color))
            tmp_row.append(HTML.TableCell(self.results[i][1], bgcolor = color))
            t.rows.append(tmp_row)
        
        htmlcode = str(t) + htmltext
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmlcode,'html')
        msg.attach(html)
        recipients = ['zhenxuan.yang@funplus.com']
        msg['Subject'] = 'Data Validation results of ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ', '.join(recipients)
        if htmlcode.count('bgcolor="red"') > 0:
            for x in range(0,3):
                try:
                    s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                    s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                    s.sendmail(msg['From'], msg['To'], msg.as_string())
                    s.quit()
                    break
                except smtplib.socket.error:
                    print ("==== smtplib.socket.error ===\n")
                    print ("==== Re-trying ....  ====\n")
                    continue
                except smtplib.SMTPException:
                    print ("==== smtplib.SMTPException ====\n")
                    print ("==== Re-trying ....  ====\n")
                    continue


def main():
    emrcm = EMRClusterMonitor()
    emrcm.run()

if __name__ == '__main__':
    main()

import psycopg2
import yaml
import pprint
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import Encoders
import os
import HTML
import datetime
import time
import httplib
import urllib

class FraudMonitor:

    def __init__(self):
        self.results = {}
        self.yamls = ['dotarena_2_0.yaml', 'xy_2_0.yaml', 'kpi-diandian_2_0.yaml', 'kpi-funplus_2_0.yaml']

    def run(self):
        for filename in self.yamls:
            self.setDBParams(filename)
            conn = self.getConnection()
            result = self.runScripts(conn)
            self.results[self.db_name] = result
            if len(result[0]) > 0:
                # generate csv file
                self.genCSVFile(conn)
                # send mail
                self.sendMail()

    def runScripts(self, conn):        
        cursor = conn.cursor()
        result = []

        command = self.fraud_command
        try:
            cursor.execute(command)
            result.append(cursor.fetchall())
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def genCSVFile(self, conn):
        cursor = conn.cursor()
        result = []

        command = self.fraud_csv_command
        try:
            cursor.execute(command)
            result.append(cursor.fetchall())

            # Generate csv file
            header = ['user_key', 'snsid', 'app_id', 'install_source', 'sub_publisher', 'install_date', 'ip_address', 'country', 'device_name', 'os', 'os_version', 'level_end', 'session_cnt', 'purchase_cnt', 'active_days', 'reason']
            f = open('/tmp/FraudInstalls_' + self.db_name + '.csv', 'w')
            title = ''
            for item in header:
                title = title + item + ','
            print >> f, title
            for record in result[0]:
                tmp_row = ''
                for item in record:
                    tmp_row = tmp_row + str(item) + ','
                print >> f, tmp_row
            f.close()

        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self, filename):
        filename = '/mnt/funplus/data-analytics/game_conf/' + filename
        #filename = '/home/ec2-user/temp/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['redshift']['db_host']
        self.db_port = confMap['redshift']['db_port']
        self.db_name = confMap['redshift']['db_name']
        self.db_username = confMap['dbinfo']['username']
        self.db_password = confMap['dbinfo']['password']
        self.fraud_email_list = confMap['fraudmonitor']['fraud_email_list']
        self.fraud_command = confMap['fraudmonitor']['fraud_command']
        self.fraud_csv_command = confMap['fraudmonitor']['fraud_csv_command']
        #self.aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        #self.aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']

    def sendMail(self):

        footer = """
        <p>
        <br/>
        <b>Fraud Monitor Strategies: </b>
            <ul>
            <li>50 or more installs in one day</li>
            <li>Percent of D1 retention < 20%% (Incent D1 retention < 10%%) or D1 retention > 60%%</li>
            <li>%s</li>
            <li>%s</li>
            </ul>
        </p>
        """ %("Percent of level 1 with more than 2 active days > 10%" if self.db_name == 'daota' or self.db_name == 'xy' else "Percent of level 1 with more than 2 active days > 20%",
            "Percent of discrepancy between Adjust and BI  > 40%" if self.db_name == 'daota' else "Percent of discrepancy between Adjust and BI  > 20%")

        table_header = ['Install Source', 'App ID', 'Install Period', 'Install Count', 'D1 Retention', 'D7 Retention', 'Level 1 Percent', 'Install Discrepancy', 'Country Discrepancy']
        incent_source = ['supersonic(incent)', 'sponsorpay(incent)', 'jump ramp']

        t = HTML.Table(header_row = table_header)
        color = 'AliceBlue'
        for record in self.results[self.db_name][0]:
            tmp_row = []
            # Install Source
            value = '<b>Install Source</b>: ' + record[0] + '<br/><b>Sub Publisher</b>: ' + record[1]
            tmp_row.append(HTML.TableCell(value, bgcolor = color))
            # App ID
            value = record[2]
            tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # Install Date
            value = str(record[3]) + ' ~ ' + str(datetime.date.today() - datetime.timedelta(2))
            tmp_row.append(HTML.TableCell(value, bgcolor = color, width = '100px', align = 'center'))
            # Install Count
            value = record[4]
            tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # D1 Retention
            value = str(round(record[5] * 100, 1)) + '%' if record[5] else '0.0%'
            if (record[5] < 0.05 or record[5] > 0.6):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Tomato', align = 'center'))
            elif ((record[5] < 0.2 and not record[0].lower() in incent_source) or (record[5] < 0.1 and record[0].lower() in incent_source)):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Gold', align = 'center'))
            else:
                tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # D7 Retention
            value = str(round(record[6] * 100, 1)) + '%' if record[6] else '0.0%'
            tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # Level 1 Percent
            value = str(round(record[7] * 100, 1)) + '%' if record[7] else '0.0%'
            if (record[7] > 0.4):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Tomato', align = 'center'))
            elif (record[7] > 0.2 or ((self.db_name == 'daota' or self.db_name == 'xy') and record[7] > 0.1)):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Gold', align = 'center'))
            else:
                tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # Discrepancy
            value = str(round(record[8] * 100, 1)) + '%' if record[8] else '0.0%'
            if (record[8] > 0.4):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Tomato', align = 'center'))
            elif (record[8] > 0.2 and self.db_name != 'daota'):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Gold', align = 'center'))
            else:
                tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # Country Discrepancy
            value = str(round(record[9] * 100, 1)) + '%' if record[9] else '0.0%'
            if (record[9] > 0.4):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Tomato', align = 'center'))
            elif (record[9] > 0.1):
                tmp_row.append(HTML.TableCell(value, bgcolor = 'Gold', align = 'center'))
            else:
                tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))

            # Append the row
            t.rows.append(tmp_row)
            # Change color
            if (color == 'white'):
                color = 'AliceBlue'
            else:
                color = 'white'

        # Generate HTML
        htmlcode = str(t) + footer
        #print htmlcode

        # Send Email
        msg = MIMEMultipart()
        msg['Subject'] = '[' + self.db_name + '] Market Fraud Monitor ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "Anti-Fraud<tableau_admin@funplus.com>"
        msg['To'] = self.fraud_email_list

        # attach html code
        html = MIMEText(htmlcode, 'html')
        msg.attach(html)

        # attach csv file
        csvFile = '/tmp/FraudInstalls_' + self.db_name + '.csv'
        part = MIMEBase('application', 'octet-stream')
        part.set_payload( open(csvFile, 'rb').read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(csvFile)))
        msg.attach(part)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], self.fraud_email_list.split(','), msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue

def main():
    fm = FraudMonitor()
    fm.run()

if __name__ == '__main__':
    main()
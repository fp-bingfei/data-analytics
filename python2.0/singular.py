import urllib
import argparse
import json
import sys
import time
import os
import boto3, botocore
import shutil

class SingularData(object):
    def __init__(self,args):
        self.args = args
        self.url_param_dict = {}
        self.url_param_dict["start_date"] = self.args["start_date"]
        self.url_param_dict["end_date"] = self.args["end_date"]
        self.clean_up()
        self.path = self.create_dir()
        self.api_key = "0135e4d1ab75e12b02f45b8ab553ff9c22e104d54ed65eee6aa5b593eb30132c"
        self.time_breakdown = "day"
        self.file_list = []
        self.dimensions = ["app"
                           ,"source"
                           ,"os"
                           ,"country_field"
                           ,"adn_campaign"
                           ]
        
        self.metrics = ["adn_impressions"
                        ,"custom_clicks"
                        ,"custom_installs"
                        ,"adn_cost"
                        ]
        self.url_param_dict["api_key"] = self.api_key
        self.url_param_dict["dimensions"] = ",".join(self.dimensions)
        self.url_param_dict["metrics"] = ",".join(self.metrics)
        self.url_param_dict["time_breakdown"] = self.time_breakdown
        print self.url_param_dict
    
    def write_file(self):
        file_name = {}
        url = "https://api.singular.net/api/fetch_reporting_data?%s"
        params = urllib.urlencode(self.url_param_dict)
        print params
        resp = urllib.urlopen(url % params)        
        data = json.load(resp)
        for element in data["value"]["results"]:
            line = ""
            if not file_name.get(element["app"],False):
                file_path = self.path + "singular_data_" + element["app"].replace(" ","_").lower() + "_" + self.args["end_date"] + ".tsv"
                file_name[element["app"]] = file_path
            target_file = open(file_name[element["app"]],"a")
            for value in element.values():
                line += unicode(value).encode("utf-8")+ "|" 
            line = line[:-1] + "\n"
            target_file.writelines(line)
            target_file.close()
        return file_name
    
    def create_dir(self):
        path = "/mnt/funplus/scratch/third-party/singular/" + self.args["end_date"] + "/"
        if not os.path.exists(path):
            os.mkdir(path)
            print "creating path" +path
        return path
    def clean_up(self):
        path = "/mnt/funplus/scratch/third-party/singular/" + self.args["end_date"] + "/"
        if os.path.exists(path):
            shutil.rmtree(path)
        print "deleting existing path" +path
            
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--start_date',help='Start date of singular api call',required=True)
    parser.add_argument('-e','--end_date',help='End date of singular api call',required=True)
    args = vars(parser.parse_args()) 
    return args

def upload_to_s3(bucket, source_path, target_path):
    try:
        # Connect to S3
        s3 = boto3.session.Session(aws_access_key_id='AKIAJRKDNC52OAINDWKA',
                  aws_secret_access_key='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',
                  region_name='us-west-2').resource('s3')
        # Uplaod file to S3
        s3.meta.client.upload_file(source_path, bucket, target_path + os.path.basename(source_path))
        return True
    except botocore.exceptions.ClientError as e:
        print 'Get an ClientError:', e.message
        return False

def main():
    t1 = time.time()
    args = parse_args()
    sd = SingularData(args)
    source_path = sd.write_file()
    bucket = 'com.funplusgame.bidata'
    target_path = 'dev/singular/' + args["end_date"] + '/'
    for sp in source_path.values():
        result = upload_to_s3(bucket, sp, target_path)
        print 'result = %s' %(result)
    t2 = time.time()
    print 'done'
    print "total time is %s" %(t2-t1) + "s"

if __name__ == "__main__":
    main()
    

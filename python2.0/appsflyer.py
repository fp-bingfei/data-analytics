#!/usr/bin/env python
# -*- coding:utf-8 -*-

from urllib2 import Request, urlopen, URLError, HTTPError
import ujson as json
from pprint import pprint as pp
import argparse
import time
import os
import gzip
from datetime import datetime, timedelta
import urllib
import yaml

__installs_uri = """
https://hq.appsflyer.com/export/{site_id}/installs_report?api_token={api_key}&from={start_date}&to={end_date} 
"""


def __construct_url(**kwargs):
        
    end_date = kwargs.get('dt')
    dt_obj = datetime.strptime(kwargs.get('dt'), '%Y-%m-%d')
    start_date = (dt_obj - timedelta(days=int(kwargs.get('days')))).strftime("%Y-%m-%d")
    
    return __installs_uri.format(start_date=start_date,
                        end_date=end_date,
                        site_id=kwargs.get('site_id'),
                        api_key = kwargs.get('api_key'),
                        )

def __download_file(**kwargs):
    pp(kwargs)

    url = __construct_url(**kwargs)
    print url
    
    dt_obj = datetime.strptime(kwargs.get('dt'), '%Y-%m-%d')
    filename = '{dt}_{app}_{site_id}.csv'.format(dt=dt_obj.strftime("%Y%m%d"),app=kwargs.get('app_id'),site_id=kwargs.get('site_id'))
    target_file = os.path.join(kwargs.get('output'),filename)
    
    req = urlopen(url)
    CHUNK = 64 * 1024
    with open(target_file,'wb') as f:
        while True:
            chunk = req.read(CHUNK)
            if not chunk: break
            f.write(chunk)
    
    return target_file
    
def __post_process(target_file,**kwargs):
    target_file_gz = target_file + '.gz'
    site_id = kwargs.get('site_id')
    with gzip.open(target_file_gz,'wb') as f_gz:
        with open(target_file,'rb') as f:
            for i,line in enumerate(f):
                if (i==0):
                    line = "site_id," + line
                else:
                    line = site_id + "," + line
                f_gz.write(line)
                
    print 'cleaning up'
    os.remove(target_file)
            
    

def parse_args():
    parser = argparse.ArgumentParser(description='Appsflyer Install Source Downloader')
    parser.add_argument('-c','--config', help='The config file to be used', required=True)
    parser.add_argument('-dt','--dt', help='The date for which the data will be fetched. YYYY-MM-DD', required=True)
    parser.add_argument('-o','--output', help='The output directory', required=True)
    parser.add_argument('-d','--days', help='days back', required=False, default=30)
    args = vars(parser.parse_args())
    return args
    
def main():
    args = parse_args()
    config_path = args.get('config')

    with open(config_path,"r") as f:
        confMap = yaml.safe_load(f)

    print confMap

    params = dict()
    params['app_id'] = confMap.get('id')
    params['dt'] = args.get('dt')
    params['api_key'] = confMap.get('appsflyer').get('api_key')
    params['output'] = args.get('output')
    params['days'] = args.get('days')

    pp(params)
    
    output_dir = args.get('output')
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for site_id in confMap.get('appsflyer').get('site_id'):
        params['site_id'] = site_id
        target_file = __download_file(**params)
        print target_file
        __post_process(target_file,**params)

if __name__ == '__main__':
    main()
    

#! /usr/bin/python

import smtplib
import argparse

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime

class EventsDataMonitor:
	# Create message container - the correct MIME type is multipart/alternative.
	msg = MIMEMultipart('alternative')
	# msg['Subject'] = ''
	msg['From'] = "KPI Alerts<tableau_admin@funplus.com>"
	msg['To'] = "bidev@funplus.com"


	def __init__(self, args):
		self.args = args
		self.msg['Subject'] = self.args['subject']

	def run(self):

		warningThreshold = 25
	
		table = '<h3>This report was sent at: ' + datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S") + ' <b>UTC</b></h3><br>'

		table += '<table border=1>'
		filedir = self.args['filedir']
		files = self.args['files'].split(',')

		header = '<tr><td>GameName</td><td>ReporDate</td><td>ReportPrevDate</td><td>NewInstalls</td><td>PrevNewInstalls</td><td>NewInstall%Diff</td>'
		header += '<td>DAU</td><td>PreDAU</td><td>DAU%Diff</td><td>Revenue</td><td>PrevRevenue</td><td>Revenue%Diff</td></tr>'
		table += header
		for kpifile in files:
			with open(filedir + '/' + kpifile, "r") as f:
				for line in f:
					cols = line.strip().split('\t')
					columnNames = '<tr>' 
					
					newInstallPctDiff = (cols[5].strip() if len(cols) > 11 and cols[5] is not None and len(cols[5].strip()) > 0 else None) #NewInstall%Diff
					dauPctDiff = (cols[8].strip() if len(cols) > 11 and cols[8] is not None and len(cols[8].strip()) > 0 else None) #DAU%Diff
					revenuePctDiff = (cols[11].strip() if len(cols) > 11 and cols[11] is not None and cols[11].strip() > 0 else None) #Revenue%Diff

                                        if (newInstallPctDiff is None or float(newInstallPctDiff) <= (0 - warningThreshold) or dauPctDiff is None or float(dauPctDiff) <= (0 - warningThreshold) or revenuePctDiff is None or  float(revenuePctDiff) <= (0 - warningThreshold)):
                                                columnNames = '<tr bgcolor="RED">'
                                        elif (float(newInstallPctDiff) >= warningThreshold or float(dauPctDiff) >= warningThreshold or  float(revenuePctDiff) >= warningThreshold):
                                                columnNames = '<tr bgcolor="YELLOW">'

					for col in cols:
						columnNames += '<td>'+col.strip()+'</td>'
					columnNames += '</tr>' 
					table += columnNames + "\n"
					
		table += '</table>'	
		table += '<br>*** Percentage Diff <= ' + str(0 - warningThreshold) + '% (or Null/Empty) is highlighted in red color'
		table += '<br>*** Percentage Diff >= ' + str(warningThreshold) + '% is highlighted in yellow color'

		self.email(table)	

	def email(self, body):
		
		# Create the body of the message (a plain-text and an HTML version).
		text = ""
		html = '<html><head><body>'
		html += body
		html += '</body></head></html>'

		# Record the MIME types of both parts - text/plain and text/html.
		part1 = MIMEText(text, 'plain')
		part2 = MIMEText(html, 'html')

		self.msg.attach(part1)
		self.msg.attach(part2)

		for x in range(0,3):
			try:
                                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
				s.sendmail(self.msg['From'], self.msg['To'], self.msg.as_string())
				s.quit()
				break
			except smtplib.socket.error:
				print ("==== smtplib.socket.error ===\n")
				print ("==== Re-trying ....  ====\n")
				continue
			except smtplib.SMTPException:
				print ("==== smtplib.SMTPException ====\n")
				print ("==== Re-trying ....  ====\n")
				continue


def parse_args():
        parser = argparse.ArgumentParser(description='Events Hourly Data Monitor')
        parser.add_argument('-s','--subject', help='Email Subject', required=False)
        parser.add_argument('-f','--files', help='All source filenames', required=True)
        parser.add_argument('-d','--filedir', help='Source files dir', required=True)
        args = vars(parser.parse_args())
        return args

def main():
        args = parse_args()
        edm = EventsDataMonitor(args)
        edm.run()


if __name__ == '__main__':
        main()

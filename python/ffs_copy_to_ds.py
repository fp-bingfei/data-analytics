import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy import text
import pickle
import time
import sys

ffs_conn = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
ds_conn = "redshift+psycopg2://biadmin:Halfquest_2014@ds-test.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"

ffs_engine = create_engine(ffs_conn)
ds_engine = create_engine(ds_conn)

#or create your own table list
table_list = [u'processed.rc_transaction', u'processed.re_target_campaign_d30_inactive_level_a10_1201', u'processed.re_target_campaign_d30_inactive_level_a5_u10_1202', u'processed.re_target_campaign_d30_inactive_level_u5_1201', u'processed.re_target_campaign_d30_inactive_payer_0914', u'processed.re_target_campaign_d30_inactive_payer_1201', u'processed.ref_install_source_map', u'processed.retarget_ab_us_payer_0609', u'processed.retarget_date_0914_payer', u'processed.retarget_date_1201_level_a10', u'processed.retarget_date_1201_level_u5', u'processed.retarget_date_1201_payer', u'processed.retarget_date_1202_level_a5_u10', u'processed.snsid', u'processed.tab_level_active_days', u'processed.tab_level_dropoff', u'processed.tab_leveling_speed', u'processed.tab_marketing_kpi', u'processed.tab_reactivation_kpi', u'processed.tab_spiderman_capture', u'processed.tab_spiderman_capture_new', u'processed.tab_spiderman_capture_old', u'processed.tango_quest', u'processed.test', u'processed.test_action_detail', u'processed.test_payer_list', u'processed.tmp_start_date', u'processed.tutorial_adhoc']

unload_sql = '''unload ('select * from {table};') to '{s3_path}' 
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
DELIMITER '^' GZIP ESCAPE ALLOWOVERWRITE;'''

copy_sql = '''copy {table} from '{s3_path}'
CREDENTIALS 'aws_access_key_id=AKIAJRKDNC52OAINDWKA;aws_secret_access_key=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' 
DELIMITER '^' GZIP ESCAPE;'''

clock = {}
for table in table_list:
	try:
		sql = '''select count(1) from {table};'''.format(table=table)
		count_df = pd.read_sql_query(sql, ffs_engine)
		start_time = time.time()
		s3_path = 's3://tangtest/ds/'+table+'/'
		if len(count_df)>0:
			ffs_engine.execute(text(unload_sql.format(table=table, s3_path=s3_path)))
			unload_time = time.time()
			ds_engine.execute(text(copy_sql.format(table=table, s3_path=s3_path)).execution_options(autocommit=True))
			copy_time = time.time()
			clock[table] = [unload_time-start_time, copy_time-start_time]
			print table, unload_time-start_time, copy_time-start_time
	except:
		print sys.exc_info()
		pass

pickle.dump(clock, open("clock.p", "wb"))

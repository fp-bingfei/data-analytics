import argparse
import yaml
import psycopg2
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import boto
import sys

class ReduceTableSize(object):
    def __init__(self, args):
        self.args = args
        self.start_date = args['start_date']
        self.end_date = args['end_date']
    
    def run(self):
        self.setDBParams()
        conn = self.getConnection()
        s3_conn, bucket = self.getS3Conn()
        self.preDelS3(s3_conn, bucket)
        print ("\n Cleanup finish")
        self.unloadToS3(conn)
        print ("\n unload finish")
        if self.checkS3(s3_conn, bucket):
            self.execSql(conn)
            print ("\n delete finish")
        else:
            print ("Target file not exists, please check the script!")
        conn.close()
    
    def checkS3(self, s3_conn, bucket):
        if self.args['prefix'][-1] != '/':
            self.args['prefix'] += '/'
        ifExist = True
        for table_list in self.tableList:
            key = bucket.get_all_keys(prefix=self.args['prefix'] + table_list[table_list.index('.')+1:] + '/' + self.start_date + '-' + self.end_date + '/', delimiter='/')
            if len(key) == 0 and ifExist == True:
                ifExist = False
        return ifExist
    
    def getS3Conn(self):
        s3_conn = boto.connect_s3(self.aws_access_key_id, self.aws_secret_access_key)
        bucket = s3_conn.get_bucket(self.args['s3bucket'])
        return s3_conn,bucket
    
    def preDelS3(self, s3_conn, bucket):
        if self.args['prefix'][-1] != '/':
            self.args['prefix'] += '/'
        
        for table_list in self.tableList:
            for key in bucket.list(prefix=self.args['prefix'] + table_list[table_list.index('.')+1:] + '/' + self.start_date + '-' + self.end_date + '/', delimiter='/'):
                key.delete()

def unloadToS3(self, conn):
    try:
        for table_list in self.tableList:
            if table_list == 'public.events_raw' or table_list == 'public.events':
                query = """
                    unload ('select * from {table} where date(ts) >= \\'{start_date}\\' and date(ts)<= \\'{end_date}\\';')  to '{s3_prefix}{s3_table}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
                    """.format(start_date=self.start_date, end_date=self.end_date,
                               s3_prefix='s3://' + self.args['s3bucket'] + '/' + self.args['prefix'],
                               s3_date_path=self.start_date + '-' + self.end_date, aws_access_key_id=self.aws_access_key_id,
                               aws_secret_access_key=self.aws_secret_access_key, delimiter='^', command_options='GZIP',
                               table=table_list,
                               s3_table=table_list[table_list.index('.')+1:])
                               elif table_list == 'processed_new.fact_session':
                                   query = """
                                       unload ('select * from {table} where date_start >= \\'{start_date}\\' and date_start<= \\'{end_date}\\';')  to '{s3_prefix}{s3_table}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
                                       """.format(start_date=self.start_date, end_date=self.end_date,
                                                  s3_prefix='s3://' + self.args['s3bucket'] + '/' + self.args['prefix'],
                                                  s3_date_path=self.start_date + '-' + self.end_date, aws_access_key_id=self.aws_access_key_id,
                                                  table=table_list,
                                                  aws_secret_access_key=self.aws_secret_access_key, delimiter='^', command_options='GZIP',
                                                  s3_table=table_list[table_list.index('.')+1:])
                               elif table_list == 'raw_events.events':
                                   query = """
                                       unload ('select * from {table} where date(ts_pretty) >= \\'{start_date}\\' and
                                       date(ts_pretty)<= \\'{end_date}\\';')  to '{s3_prefix}{s3_table}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
                                       """.format(start_date=self.start_date, end_date=self.end_date,
                                                  s3_prefix='s3://' + self.args['s3bucket'] + '/' + self.args['prefix'],
                                                  s3_date_path=self.start_date + '-' + self.end_date, aws_access_key_id=self.aws_access_key_id,
                                                  table=table_list,
                                                  aws_secret_access_key=self.aws_secret_access_key, delimiter='^', command_options='GZIP',
                                                  s3_table=table_list[table_list.index('.')+1:])
                                           else:
                                               query = """
                                                   unload ('select * from {table} where date >= \\'{start_date}\\' and date <= \\'{end_date}\\';')  to '{s3_prefix}{s3_table}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
                                                   """.format(start_date=self.start_date, end_date=self.end_date,
                                                              s3_prefix='s3://' + self.args['s3bucket'] + '/' + self.args['prefix'],
                                                              s3_date_path=self.start_date + '-' + self.end_date, aws_access_key_id=self.aws_access_key_id,
                                                              table=table_list,
                                                              aws_secret_access_key=self.aws_secret_access_key, delimiter='^', command_options='GZIP',
                                                              s3_table=table_list[table_list.index('.')+1:])
                                                       print query
                                                           cursor = conn.cursor()
                                                               cursor.execute(query)
                                                           conn.commit()
    except Exception, e:
        conn.rollback()
            text = "Unload rollback\n %s" % (e)
            print text
            self.sendMail(text)
            sys.exit(1)

def execSql(self, conn):
    try:
        for table_list in self.tableList:
            if table_list == 'public.events_raw' or table_list == 'public.events':
                query = """
                    delete from {table} where date(ts) >= '{start_date}' and date(ts)<= '{end_date}';
                    """.format(start_date=self.start_date, end_date=self.end_date, table=table_list)
                elif table_list == 'processed_new.fact_session':
                    query = """
                        delete from {table} where date_start >= '{start_date}' and date_start<= '{end_date}';
                        """.format(start_date=self.start_date, end_date=self.end_date, table=table_list)
                elif table_list == 'raw_events.events':
                    query = """
                        delete from {table} where date(ts_pretty) >= '{start_date}' and date(ts_pretty)<= '{end_date}';
                        """.format(start_date=self.start_date, end_date=self.end_date, table=table_list)
                else:
                    query = """
                        delete from {table} where date >= '{start_date}' and date<= '{end_date}';
                        """.format(start_date=self.start_date, end_date=self.end_date, table=table_list)
                print query
                cursor = conn.cursor()
                            cursor.execute(query)
                                conn.commit()
                                    except Exception, e:
                                        conn.rollback()
                                            text = "delete rollback\n %s" % (e)
                                                self.sendMail(text)

def getConnection(self):
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
                                                                     self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn
            
                                                                     def setDBParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']
        self.tableList = confMap['tableList']
            
                                                                     def sendMail(self, text):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Redshift Unload Job"
        msg['From'] = "Redshift Monitor<tableau_admin@funplus.com>"
        msg['To'] = "data@funplus.com"
        msg.attach(MIMEText(text, 'plain'))
        for x in range(0, 3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com', 465)
                s.login('AKIAJKBQSFVYRX7GHDYA', 'AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
            continue


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-t', '--table', help='DB table name for specific table', required=False)
    parser.add_argument('-prefix', '--prefix', help='The s3prefix of unloaded data', required=True)
    parser.add_argument('-start_date', '--start_date', help='The unload data start date', required=True)
    parser.add_argument('-end_date', '--end_date', help='The unload data end date', required=True)
    parser.add_argument('-s3bucket', '--s3bucket', help='The unload data s3bucket', required=True)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    rts = ReduceTableSize(args)
    rts.run()


if __name__ == '__main__':
    main()

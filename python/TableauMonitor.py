import psycopg2
import yaml
import pprint
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os
import HTML
import datetime
import time
import httplib
import urllib
from sqlalchemy import create_engine
import pandas as pd
import commands
import json
import xml.etree.ElementTree as ET
import string

class TableauMonitor:

    def __init__(self):
        self.results = {}
        self.daysBack = 4
        self.yamls =  ['daota.yaml', 'royal.yaml', 'farm_1_2.yaml', 'gz.yaml',
            'ha_1_2.yaml','mt2.yaml','ffs_new.yaml','xiyou.yaml', 'kpi-diandian.yaml', 'kpi-funplus-core.yaml']

    def run(self):
        for filename in self.yamls:
            self.setDBParams(filename)
            conn = self.getConnection()
            result = self.runScripts(conn)
            if self.db_name != 'kpi':
                self.results[self.db_name] = result
            else:
                self.results[self.db_host.split('.')[0]] = result
            if(self.db_name not in ['farm', 'ff2pc', 'ffs', 'rs']):
                self.recordRevenue(result)

        #for key in self.results.keys():
           # print 'key=%s, value=%s' % (key, self.results[key])
        self.sendMail()

    def runScripts(self, conn):        
        cursor = conn.cursor()
        result = []

        historyDay = datetime.date.today() - datetime.timedelta(self.daysBack)
        historyDay = historyDay.strftime('%Y-%m-%d')
        historyDay = "'" + historyDay + "'"

        command = self.command % historyDay
        try:
            cursor.execute(command)
            result.append(cursor.fetchall())
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Success!'
        except Exception, e:
            result.append('1')
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self, filename):
        filename = '/mnt/funplus/data-analytics/python/conf/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.command = confMap['tasks']['defaults']['command']
        #self.aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        #self.aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']

    def recordRevenue(self, result):
        filename = '/tmp/' + self.db_host.split('.')[0] + '_' + self.db_name + '_revenue.csv'
        f = open(filename, 'w')
        for line in result[0]:
            output = line[0].strftime('%Y-%m-%d') + '|' + line[1] + '|' + str(line[4]) + '\n'
            f.write(output)
        f.close()
        s3path = ' s3://com.funplus.datawarehouse/PNL/' + datetime.datetime.now().strftime('%Y-%m-%d') + '/'
        os.system('s3cmd put ' + filename + s3path)

    def getSlaInfo(self):

        htmltext = '''
                <p>The current SLA for All Games using 2.0 is before 11:30(UTC +8), for games using 1.1 is 15:30(UTC +8)</p>
                <a href='http://wiki.ifunplus.cn/display/BI/SLA+for+DATA+2.0'>More Info about our SLA, Click here</a>
                </ br>'''
        self.setDBParams('kpi_test.yaml')
        conn = self.getConnection()
        cursor = conn.cursor()
        legacy_games = ['dwtlw', 'dotarena', 'xy', 'mt2', 'ffs', 'valiantforce', 'allgames_kpi_daily', 'allgames_kpi_weekly', 'allgames_kpi_monthly']

        sla_result = []
        sla_time = datetime.datetime(datetime.date.today().year, datetime.date.today().month,
                                    datetime.date.today().day, 10, 00, 00)
        today_str = "'" + datetime.date.today().strftime('%Y-%m-%d') + "'"
        command = self.command % today_str
        try:
            cursor.execute(command)
            sla_result = cursor.fetchall()
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Success!'
        except Exception, e:
            sla_result = []
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        table_header = ['Game', 'Finish Time']
        t = HTML.Table(header_row = table_header)
        for line in sla_result:
            tmp_row = [line[0]]
            if (line[1] + datetime.timedelta(hours=8) < sla_time or (line[0] in legacy_games and line[1] + datetime.timedelta(hours=3) < sla_time)):
                color = 'lime'
            elif (line[0] in legacy_games and line[1] < sla_time):
                color = 'orange'
            else:
                color = 'red'

            tmp_row.append(HTML.TableCell((line[1] + datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S'), bgcolor = color))
            t.rows.append(tmp_row)

        htmlcode = str(t) + htmltext

        return htmlcode


    def getAdjustInfo(self):

        adjust_result = []
        yesterday_str = "'" + (datetime.date.today() - datetime.timedelta(1)).strftime('%Y-%m-%d') + "'"

        htmltext = '''
                <p>If the diff of two sources is under 20%, GREEN cell, between 20% and 50%, ORANGE cell, other wise RED cell</p>
                </ br>'''

        command = '''
SELECT a.app_id,
       a.install_type AS install_type,
       a.all_adjust_installs AS all_adjust_installs,
       b.cnt AS bi,
       a.empty_user_id_installs AS empty_user_id_installs,
       a.not_in_bi_installs AS not_in_bi_installs
FROM (SELECT install_date,
             app_id,
             CASE
               WHEN LOWER(install_source) LIKE '%s' THEN 'Organic'
               ELSE 'Paid'
             END AS install_type,
             SUM(all_adjust_installs) AS all_adjust_installs,
             SUM(empty_user_id_installs) AS empty_user_id_installs,
             SUM(not_in_bi_installs) AS not_in_bi_installs
      FROM adjust_bi_discrepancy
      WHERE install_date = %s
      GROUP BY 1,
               2,
               3) a
  JOIN (SELECT install_date,
               app_id,
               CASE
                 WHEN LOWER(install_source) LIKE '%s' THEN 'Organic'
                 ELSE 'Paid'
               END AS install_type,
               COUNT(1) AS cnt
        FROM kpi_processed.dim_user
        WHERE install_date = %s
        AND   app_version NOT LIKE '%s'
        GROUP BY 1,
                 2,
                 3) b
    ON a.app_id = b.app_id
    AND a.install_type = b.install_type
    ORDER BY 1,2,3;''' % ('%organic%', yesterday_str, '%organic%', yesterday_str, '%canvas%')


        for filename in ['kpi-diandian.yaml', 'kpi-funplus-core.yaml']:
            self.setDBParams(filename)
            conn = self.getConnection()
            cursor = conn.cursor()

            try:
                cursor.execute(command)
                adjust_result += cursor.fetchall()
                print 'In Database: ' + self.db_name
                print 'Excuting command: ' + command
                print 'Success!'
            except Exception, e:
                print 'In Database: ' + self.db_name
                print 'Excuting command: ' + command
                print 'Error: ',
                print e
                conn.rollback()

        table_header = ['Game', 'Install Type', 'Adjust Installs', 'BI Installs', 'Empty User_id Percentage', 'Not in BI Percentage', 'Difference Percentage %']
        t = HTML.Table(header_row = table_header)
        for line in adjust_result:
            tmp_row = [line[0], line[1]]

            if ((line[2] < line[3] * 0.9 or line[2] * 0.9 > line[3]) and abs(line[2] - line[3]) > 100):
                color = '#E60000'
            elif ((line[2] < line[3] * 0.95 or line[2] * 0.95 > line[3]) and abs(line[2] - line[3]) > 50):
                color = 'orange'
            else:
                color = 'lime'

            diff = round(100.0 * (line[2] - line[3]) / line[2], 2) if line[2] != 0 else 'ERROR'
            empty_rate = round(100.0 * line[4] / line[2], 2) if line[2] != 0 else 'ERROR'
            not_in_bi_rate = round(100.0 * line[5] / line[2], 2) if line[2] != 0 else 'ERROR'
            tmp_row.append(HTML.TableCell(line[2]))
            tmp_row.append(HTML.TableCell(line[3]))
            tmp_row.append(HTML.TableCell(empty_rate))
            tmp_row.append(HTML.TableCell(not_in_bi_rate))
            tmp_row.append(HTML.TableCell(diff, bgcolor = color))

            t.rows.append(tmp_row)
        htmlcode = str(t) + htmltext

        return htmlcode

    #calculates revenue discrepancy between BI and payment 2 days ago (only for transactions that exist in both tables)
    def paymentInfo(self):
        #we are reading kpi diandian db credentials
        self.setDBParams('kpi-diandian.yaml')
        engine_config = "redshift+psycopg2://{user}:{pw}@kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/{db}".format(user=self.db_username, pw=self.db_password, db=self.db_name)
        engine = create_engine(engine_config)
        #compare BI/payments n days ago
        n = 3
        query_date = (datetime.date.today() - datetime.timedelta(n)).strftime('%Y-%m-%d')

        #if currency=EGB, we compare it to USD using a 40:1 ratio
        #we also ignore farm iap_product_types that starts with 'mb' (those are mobile payments with 3rd party vendors and there is no way the game can send the right amount to BI)
        sql = """select a.*, round((bi_usd-payment_usd)*100/nvl(nullif(payment_usd,0),1), 1) as diff 
                --,b.bi_total_usd
                --, bi_usd/bi_total_usd as counted
                from
                (SELECT date, app_id, sum(case when lower(currency)='egb' then revenue_amount/40 else revenue_usd end)::int as bi_usd
                , sum(buyer_forex_rate*charged_amount)::int as payment_usd
                FROM kpi.kpi_processed.fact_revenue 
                where date<CURRENT_DATE and date>='{qd}'
                and charged_amount>0 
                --and lower(currency)!='egb'
                and (app_id not like 'farm%%' or iap_product_type not like 'mb%%')
                group by 1,2) a
                """.format(qd=query_date)

        df = pd.read_sql_query(sql, engine)
        df.columns = ['date', 'app_id', 'BI rev', 'Payment rev', '% Diff']
        d1 = df.set_index(['app_id', 'date']).unstack().stack(0).unstack().fillna(0).sort_index(axis=1, level='date', ascending=False)
        tmp_rows = []
        coln = []
        for j, r in enumerate(d1.fillna(0).sort_values(by=d1.columns[2], ascending=False).iterrows()):
            tmp_row = '<tr><th>{0}</th>{1}</tr>'
            #.format(r[0])
            tmp_color = []
            for c in r[1].values.tolist()[0::3]:
                delta = abs(c)
                if (delta>10):
                    color = '#E60000'
                elif (delta>5):
                    color = 'orange'
                else:
                    color = 'lime'
                tmp_color.extend([color,]*3)
            
            tmp_td = ''
            for i, (v, c) in enumerate(zip(r[1].values.tolist(), tmp_color)):
                #picking only the columns matt wants to see
                if i<=2 or i%3==0:
                    if j==0:
                        coln.append(i)
                    tmp_td += "<td style='background-color: {0}'>{1}</td>".format(c, v)
            tmp_rows.append(tmp_row.format(r[0], tmp_td))
            

        colored_table_html = d1.head(0).iloc[:, coln].to_html().replace('<tbody>', '<tbody>\n{0}'.format('\n'.join(tmp_rows)))

        #setting table style...default table css is weird
        x = ET.fromstring(colored_table_html)
        x.set('cellpadding', '4')
        x.set('style', "border:1px solid #000000; border-collapse:collapse")
        
        text = '''
                <br><p>The following table calculates the discrepancy between BI and payment since {qd} ({n} days ago). It only looks at transactions that exist in both tables (so the number will be lower than total revenue)</p><br>'''.format(qd=query_date, n=n)

        return text + ET.tostring(x)

    def sdkTestInfo(self):
        yesterday_redshift = (datetime.date.today() - datetime.timedelta(1)).strftime('%Y-%m-%d')
        yesterday_s3 = (datetime.date.today() - datetime.timedelta(1)).strftime('%Y%m%d')
        events_count_dict = {'session_start': 0, 'session_end': 0, 'payment': 0, 'new_user': 0}
        # events_count_dict = {'session_start': 0, 'session_end': 0}

        # Get sdk events from S3
        events_file_name = "%s-success.log" % yesterday_s3
        directory = "/mnt/funplus/files/sdk_test/"
        status, result = commands.getstatusoutput("s3cmd get --force s3://funplus-public-data/sdk_test/events/%s %s" % (events_file_name, directory))
        if status == 0:
            events_file = open(directory + events_file_name)
            for line in events_file:
                if line != '':
                    event_arr = json.loads(line.replace('\r\n', ''))
                    event_name = event_arr['event']
                    if events_count_dict.has_key(event_name):
                        events_count_dict[event_name] += 1

        # Get BI sdk.global.prod events from redshift
        self.setDBParams('kpi-diandian.yaml')
        conn = self.getConnection()
        cursor = conn.cursor()
        query_sql = """select event, count(1) as cnt from raw_events.events where app_id='sdk.global.prod'
                and date(ts_pretty)='{qd}' group by 1""".format(qd = yesterday_redshift)
        redshift_result = []
        redshift_events = {'session_start': 0, 'session_end': 0, 'payment': 0, 'new_user': 0}
        # redshift_events = {'session_start': 0, 'session_end': 0}
        try:
            cursor.execute(query_sql)
            redshift_result = cursor.fetchall()
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + query_sql
            print 'Success!'
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + query_sql
            print 'Error: ',
            print e
            conn.rollback()
        for line in redshift_result:
            redshift_events[line[0]] = line[1]

        # Generate table html, and compare the events count
        table_header = ['Event Name', 'Sdk Send Count', 'BI Count', 'Difference Percentage%']
        t = HTML.Table(header_row = table_header)
        for k,v in events_count_dict.iteritems():
            difference = 0
            color = 'lime'
            if redshift_events[k] != 0:
                difference = round(100.0 * (v - redshift_events[k]) / redshift_events[k], 2)
            if difference != 0:
                color = 'orange'
            elif redshift_events[k] == 0:
                color = 'orange'
            tmp_row = [str(k)]
            tmp_row.append(HTML.TableCell(str(v), bgcolor = color))
            tmp_row.append(HTML.TableCell(str(redshift_events[k]), bgcolor = color))
            tmp_row.append(HTML.TableCell(str(difference), bgcolor = color))
            t.rows.append(tmp_row)
        text = '''
                <p>BI and SDK test events discrepancy on {qd} (1 day ago): </p>'''.format(qd=yesterday_redshift)

        return text + str(t)
    
#    def sdkAdjustInfo(self):
#        yest_redshift = (datetime.date.today() - datetime.timedelta(1)).strftime('%Y-%m-%d')
#        yest_s3 = (datetime.date.today() - datetime.timedelta(1)).strftime('%Y%m%d')
#        adjust_count_dict = {'adjust': 0}
#        
#        # Get adjust events from S3
#        adjust_file_name = "%s-success.log" % yest_s3
#        directory = "/mnt/funplus/files/sdk_adjust/"
#        status, result = commands.getstatusoutput("s3cmd get --force s3://funplus-public-data/sdk_test/adjust/%s %s" % (adjust_file_name, directory))
#        if status == 0:
#            adjust_file = open(directory + adjust_file_name)
#            for line in adjust_file:
#                adjust_count_dict += 1
#        
#        # Get BI sdk.global.prod adjust events from redshift
#        self.setDBParams('kpi-diandian.yaml')
#        conn = self.getConnection()
#        cursor = conn.cursor()
#        adjust_sql = """select 'adjust' as event, count(1) as cnt from adjust where game='sdk.global.prod'
#            and date(ts)='{qd}' group by 1""".format(qd = yest_redshift)
#        adjust_result = []
#        adjust_events = {'adjust': 0}
#        try:
#            cursor.execute(adjust_sql)
#            adjust_result = cursor.fetchall()
#            print 'In Database: ' + self.db_name
#            print 'Excuting command: ' + adjust_sql
#            print 'Success!'
#                except Exception, e:
#            print 'In Database: ' + self.db_name
#            print 'Excuting command: ' + adjust_sql
#            print 'Error: ',
#            print e
#                    conn.rollback()
#                for line in adjust_result:
#                    adjust_events[line[0]] = line[1]
#                        
#                        # Generate table html, and compare the events count
#                        table_header = ['Event Name', 'Sdk Send Count', 'BI Count', 'Difference Percentage%']
#                            t = HTML.Table(header_row = table_header)
#for k,v in adjust_count_dict.iteritems():
#    difference = 0
#        color = 'lime'
#            if adjust_events[k] != 0:
#                difference = round(100.0 * (v - adjust_events[k]) / adjust_events[k], 2)
#        if difference != 0:
#            color = 'orange'
#            tmp_row = [str(k)]
#            tmp_row.append(str(v))
#            tmp_row.append(str(adjust_events[k]))
#            tmp_row.append(HTML.TableCell(str(difference), bgcolor = color))
#            t.rows.append(tmp_row)
#                text = '''
#                    <p>BI and SDK test adjust events discrepancy on {qd} (1 day ago): </p>'''.format(qd=yest_redshift)
#                        
#                        return text + str(t)

    def sendMail(self):

        htmltext ='''
        <p>History AVG: this means the average number from "4 days ago" to 2 "days ago"</p>
        <p>Yesterday: this means the number of yesterday</p>
        <p>If the number of yesterday is less than 0.7*AVG, the table cell will be 'ORANGE'; If the number of yesterday is less than 0.3*AVG, the table cell will be 'RED'; the table cell will be 'GREEN' otherwise</p>
        </ br>'''

        table_header = ['APP', 'App_id', 'New installs', 'DAU', 'Revenue']

        t = HTML.Table(header_row = table_header)
        tmp_row = ['History AVG', 'Yesterday'] * 3
        t.rows.append(tmp_row)
        yesterday = datetime.date.today() - datetime.timedelta(1)

        for key in self.results.keys():
            app_ids = []
            if (self.results[key][0] == '1'):
                print "Something is wrong with the Game %s" % key
                item = [key] + [''] * 7
                continue
            else:
                for value in self.results[key][0]:
                    if (value[1] not in app_ids):
                        app_ids.append(value[1])

            for app_id in app_ids:
                tmp_row = [key, app_id]
                tmp_historySUM = [0] * (self.daysBack - 1)
                tmp_yesterday = [0] * (self.daysBack - 1)
                colors = ['lime'] * (self.daysBack - 1)
                for value in self.results[key][0]:
                    if (value[1] == app_id):
                        if (value[0] == yesterday):
                            tmp_yesterday[0] += value[2]
                            tmp_yesterday[1] += value[3]
                            tmp_yesterday[2] += float(value[4]) if value[4] else 0
                        else:
                            tmp_historySUM[0] += value[2]
                            tmp_historySUM[1] += value[3]
                            tmp_historySUM[2] += float(value[4]) if value[4] else 0
                for i in range(self.daysBack - 1):
                    if (tmp_yesterday[i] >= tmp_historySUM[i] / (self.daysBack - 1) * 0.7):
                        pass
                    elif (tmp_yesterday[i]<= tmp_historySUM[i] / (self.daysBack - 1) * 0.3 and (tmp_yesterday[i] + 1000) <= tmp_historySUM[i] / (self.daysBack - 1) ):
                        if (app_id not in ['bv.global.prod', 'ffs.cn.prod']):
                            colors[i] = 'red'
                        else:
                            colors[i] = 'orange'
                    else:
                        colors[i] = 'orange'
                    tmp_row.append(HTML.TableCell(round(tmp_historySUM[i] / (self.daysBack - 1), 4), bgcolor = colors[i]))
                    tmp_row.append(HTML.TableCell(tmp_yesterday[i], bgcolor = colors[i]))

                t.rows.append(tmp_row)

        #GET SLA INFO
        sla_table = self.getSlaInfo()
        adjust_table = self.getAdjustInfo()
        payment_discrepancy_table = self.paymentInfo()
        sdk_test_table = self.sdkTestInfo()
        #sdk_adjust_table = self.sdkAdjustInfo()
        htmlcode = str(t) + htmltext + sla_table + adjust_table + payment_discrepancy_table + sdk_test_table# + sdk_adjust_table
        tmp = htmlcode.replace('<TH>','<TH colspan = "2">', 5)
        html_final = tmp.replace('<TH colspan = "2">', '<TH rowspan = "2">', 2)
        msg = MIMEMultipart('alternative')
        html = MIMEText(html_final,'html')
        msg.attach(html)

        #This part is used for devops monitor
        green_cnt = html_final.count('bgcolor="lime"')
        orange_cnt = html_final.count('bgcolor="orange"')
        red_cnt = html_final.count('bgcolor="red"')
        if (red_cnt > 2):
            status = 2
        elif (red_cnt > 0):
            status = 1
        else:
            status = 0
        devopsMessage = "Red Cell Count:" + str(red_cnt) + "\nOrange Cell Count:" + str(orange_cnt) + "\nGreen Cell Count:" + str(green_cnt) + "\n"
        params = urllib.urlencode({"category": "data", "status": status, "service": "dataMonitor", "tags": "Tableau", "checktime": int(time.time()), "message": devopsMessage, "space_uuid": 'fe62a568-7eee-410b-8ca6-ddf1e2027903', "token": '0539B7B5A3BB7EC85BE4A9DEE5D4D395'})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        
        conn = httplib.HTTPConnection("duty.funplus.io")
        conn.request("POST", "/Event/index/", params, headers)
        response = conn.getresponse()
        print "The http response of sending notification to Devops is:"
        print response.status, response.reason
        conn.close()

        # This part is used for devops monitor

        recipients = ['data@funplus.com']
        # recipients = ['ting.jia@funplus.com']
        msg['Subject'] = 'Data Validation results of ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ', '.join(recipients)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue


def main():
    tm = TableauMonitor()
    tm.run()

if __name__ == '__main__':
    main()
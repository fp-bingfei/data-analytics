#! /usr/bin/python

import smtplib
import os
import argparse
import yaml

dn=os.path.dirname(os.path.realpath('/mnt/funplus/'))
tn = os.path.join(dn,"funplus")
sn = os.path.join(tn,"tableau")

from email import Encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.multipart import MIMEBase
import datetime


class KpiEmail:

    def __init__(self, args):
        self.args = args

    def run(self):
        self.sendMail()
        self.recordTime()

    def sendMail(self):
        msg = MIMEMultipart('related')
        msg['From'] = "tableau_admin@funplus.com"
        msg['To'] = self.to
        msg['Subject'] = self.subject
        msgAlternative=MIMEMultipart('alternative')
        msg.attach(msgAlternative)
        msgText = MIMEText('<a href="%s"><img src="cid:%s"><br>Data Team!' % (self.link,os.path.basename(self.fn)), 'html')
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(self.fn, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition','inline', filename="%s" % os.path.basename(self.fn))
        msgAlternative.attach(msgText)
        msg.attach(part)

        s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
        s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
        s.sendmail(msg['From'],self.recipients , msg.as_string())
        s.quit()

    def readParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.subject = confMap['tasks']['defaults']['subject'] + datetime.date.today().strftime("%Y-%m-%d")
        self.to = confMap['tasks']['defaults']['email_to']
        self.recipients = confMap['tasks']['defaults']['recipients']
        self.link = confMap['tasks']['defaults']['link']
        self.fn = os.path.join(sn, confMap['tasks']['defaults']['path'])
        self.id = confMap['id']

    def recordTime(self):
        filename = '/tmp/' + self.id + '.csv'
        f = open(filename, 'w')
        line = self.id + '|' + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '|'
        f.write(line)
        f.close()
        s3path = ' s3://com.funplus.datawarehouse/SLA/' + datetime.datetime.now().strftime('%Y-%m-%d') + '/'
        os.system('s3cmd put ' + filename + s3path)

def parse_args():
    parser = argparse.ArgumentParser(description='KPI Tableau Email')
    parser.add_argument('-c','--conf', help='Config file in YAML format', required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()

    edm = KpiEmail(args)
    edm.readParams()
    edm.run()


if __name__ == '__main__':
    main()



import datetime
import argparse
from subprocess import Popen,PIPE

def parse_args():
    parser = argparse.ArgumentParser(description='LogCleaner')
    parser.add_argument('-p','--path',help='input the hdfs file path. ex:/var/log/spark/apps/, /var/log/hadoop-yarn/apps/hadoop/logs', required=True)
    args = vars(parser.parse_args())
    return args

def main(args):
    logs = "/usr/lib/hadoop/bin/hadoop fs -ls " + args["path"] + " | awk '{print $6,$8}'"
    p = Popen(logs,stdout=PIPE,shell=True)
    result = list()
    for line in p.stdout:
        if len(line.strip()) > 0:
            line_list = line.split()
            today = datetime.date.today()
            if (today - datetime.datetime.strptime(line_list[0], '%Y-%m-%d').date()).days >=7:
                result.append(line_list[1])
    
    for file in result:
        pp = Popen('hadoop fs -rm -r ' + file,shell=True) 

if __name__ == '__main__':
    args = parse_args()
    main(args)

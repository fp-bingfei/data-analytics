import boto
import copy
import argparse

class S3Usage:

    def __init__(self, args):

        self.args = args
        self.apps = ['bv_1_2/', 'farmeu_1_1/','farm_1_1/', 'farm_2_0/', 'ff2_1_1/', 'ffs_1_2/', 'ffs_2_0/', 'fruitscoot_1_2/', 'ha_1_2/', 'rs_1_2/', 'rs_2_0/', 'waf_2_0/']


    def run(self):

        s3_conn = boto.connect_s3()
        bucket = s3_conn.get_bucket('com.funplus.datawarehouse')
        date = self.args['date']
        result = []

        for app in self.apps:
            keyString = app + 'scratch/' + date + '/counters/part-00000'
            key = bucket.get_key(keyString)
            if key:
                eventRaw = key.get_contents_as_string()
                result += self.getSummary(app, eventRaw)
            else:
                print "Warning: " + keyString + " doesn't exist!!"

        self.writeResult(date, result)


    def getSummary(self, app, eventRaw):

        tmpRes = []
        buildingDict = {"app": "", "app_id": "", "event": "", "count": 0}

        for line in eventRaw.split('\n'):
            tmpDict = copy.deepcopy(buildingDict)
            if len(line.split()) < 2:
                continue
            elif len(line.split()) == 2:
                s3Path = line.split()[0]
                count = line.split()[1]
            else:
                s3Path = '_'.join(line.split()[0:-1])
                count = line.split()[-1]
            for tmp in s3Path.split('/'):
                if tmp.startswith('event='):
                    event = tmp.split('=')[1]
                elif tmp.startswith('app='):
                    app_id = tmp.split('=')[1]

            tmpDict["app"] = app
            try:
                tmpDict["app_id"] = app_id
            except Exception, e:
                tmpDict["app_id"] = "N/A"
            try:
                tmpDict["event"] = event
            except Exception, e:
                tmpDict["event"] = "N/A"
            tmpDict["count"] = int(count)
            tmpRes.append(tmpDict)

        return tmpRes


    def writeResult(self, date, result):

        f = open('/tmp/counters-' + date + '.txt', 'w')
        for record in result:
            line = date + '\t' + record["app"][:-1] + '\t' + record["app_id"] + '\t' + record["event"] + '\t' + str(record["count"]) + '\n'
            f.write(line)
        f.close()


def parse_args():

    parser = argparse.ArgumentParser(description='S3Usage statistics')
    parser.add_argument('-d','--date', help='The date want to count')
    args = vars(parser.parse_args())

    return args

def main():

    args = parse_args()
    s3u = S3Usage(args)
    s3u.run()


if __name__ == '__main__':

    main()
###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse
# 4. yum install boto

import boto.sqs
import psycopg2
import yaml
import argparse
import json
import datetime
import commands


class SQSEventsHandler:
    def __init__(self, args):
        self.args = args
        print(datetime.datetime.now().strftime('%Y%m%d_%H%M%S') + " init is done \n ")

    def run(self):

        self.setDBParams()
        db_conn = self.getConnection()

        if self.args['pre_sql'] != None:
            self.execPreSql(db_conn)

        self.monitorSQSQueue(db_conn)

    def monitorSQSQueue(self, db_conn):
        aws_access_key_id = 'AKIAJRKDNC52OAINDWKA'
        aws_secret_access_key = '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'
        region = "us-west-2"
        conn = boto.sqs.connect_to_region(
            region,
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key
        )

        queue = conn.get_queue(self.args['sqs_queue_name'])
        # 10 is time out second while read a message
        message = queue.read(10)
        s3_path_list = []

        # the last message in this queue is None
        while (message is not None):
            m_body = message.get_body()
            try:
                decode_json = json.loads(m_body)
                if ('Records' in decode_json.keys()):
                # the key need to add bucket prefix at the beginning
                    s3path = self.args['bucket'] + decode_json['Records'][0]['s3']['object']['key']
                    s3_path_normal = s3path.replace('%3D', '=')
                    # print("s3path: " + s3_path_normal)
                    one_s3_path_dict = {}
                    one_s3_path_dict['url'] = s3_path_normal
                    one_s3_path_dict['mandatory'] = True
                    s3_path_list.append(one_s3_path_dict)
            except Exception, e:
                print e
            finally:
                queue.delete_message(message)
                message = queue.read(10)

        # do nothing if s3_path_list <= 0
        if (len(s3_path_list) > 0):
            path_list_dict = {
                'entries': s3_path_list
            }
            json_str = json.dumps(path_list_dict)
            s3_file_name = self.putFileToS3(json_str)
            self.copy(db_conn, s3_file_name)
            print("s3_file_name : " + s3_file_name)

        print(datetime.datetime.now().strftime('%Y%m%d_%H%M%S') + " all message handled\n")

    def putFileToS3(self, str):
        now = datetime.datetime.now()
        file_name = self.args['sqs_queue_name'] + '_' + now.strftime('%Y-%m-%d_%H-%M-%S') + '.manifest'
        sys_file_name = '/mnt/funplus/sqs_data/' + file_name
        manifest_file = file(sys_file_name, 'w')
        manifest_file.write(str)
        manifest_file.close()

        s3_file_name = self.args['bucket'] + 'sqs/mainfest_file/' + self.args['table'] + '/' + file_name
        result = commands.getoutput('/usr/local/s3cmd-1.5.0-rc1/s3cmd put ' + sys_file_name + ' ' + s3_file_name)

        return s3_file_name

    def execPreSql(self, conn):
        cursor = conn.cursor()
        cursor.execute(self.args['pre_sql'])
        print ("\npre_sql:" + self.args['pre_sql'])

    def getCommandOptions(self):
        commandOptions = self.args['command_options'].split(',')
        commandOptionStr = ''
        for commandOpt in commandOptions:
            cleanStr = commandOpt.strip()
            # - added single quote for string starts with s3://
            commandOptionStr += (' ' + '\'' + cleanStr + '\'') if cleanStr.startswith('s3://') else (' ' + cleanStr)
        return commandOptionStr

    def copy(self, conn, s3path):
        # copy manifest files
        query = "copy %s from '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' %s TRIMBLANKS maxerror %s manifest" % (
            self.args['table'], s3path, self.aws_access_key_id, self.aws_secret_access_key, self.getCommandOptions(), (
                str(max(int(self.args['maxerror']), 100)) if (
                    self.args['maxerror'] and self.args['maxerror'].isdigit()) else '100'))
        print ("\ncopy query: " + query)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        print ("\ncommit above query")

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
            self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']


def parse_args():
    parser = argparse.ArgumentParser(description='Handle message in SQS queue, copy to redshift')
    parser.add_argument('-b', '--bucket', help='bucket string. e.g. s3://com.funplus.data-funplus/', required=True)
    parser.add_argument('-q', '--sqs_queue_name', help='SQS queue name we want to handle', required=True)
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-s', '--schema', help='DB schema name for specific table', required=False)
    parser.add_argument('-t', '--table', help='DB table name', required=True)
    parser.add_argument('-pre-sql', '--pre-sql',
                        help='SQL statement executed prior to data loading. e.g. delete from table where ...',
                        required=False)
    parser.add_argument('-command-options', '--command-options',
                        help='The COPY command options e.g. json,jsonpath,gzip or delimiter,\'\\t\',gzip',
                        required=True)
    parser.add_argument('-maxerror', '--maxerror', help='The max error can be ignored, default is 100', required=False)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    handler = SQSEventsHandler(args)
    handler.run()


if __name__ == '__main__':
    main()

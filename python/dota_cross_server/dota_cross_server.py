__author__ = 'jhyu'
# -*- coding: utf-8 -*-

import psycopg2 as pg
import pandas as pd
import pandas.io.sql as sql
import MySQLdb
import os
import threading
from time import sleep, ctime
import time


class entry_mysql(object):
# Get Mysql db entry
    host_format = 'dgame-oss-db-{0}.cpcrcjdtsyi8.ap-southeast-1.rds.amazonaws.com'
    host = ''
    port = 3306
    user = "oss_page"
    passwd = "Lilith_501"

class SqlProcess:
    def __init__(self):
        # where you want to read your sql file
        self.sql_path = '/Users/funplus/Desktop/dota/sql/'
        server_list = ['1200' + str(i) for i in range(1, 10)]
        server_list.extend(str(i) for i in range(12010, 12081))
        self.server_list = server_list
        # where you want to export your data
        self.export_path = '/Users/funplus/Desktop/dota/export/'

    def run(self):
    # Create Multiple Threads to Run Multiple Queries
        sql_list = []
        for (dirpath, dirnames, filenames) in os.walk(self.sql_path):
            for filename in filenames:
                if filename == '.DS_Store':
                    pass
                else:
                    sql_list.append(filename)
        threads = []
        for key in sql_list:
            t = threading.Thread(target=self.RunQuery, args=(key,))
            threads.append(t)
        for i in range(len(sql_list)):
            threads[i].start()
        for i in range(len(sql_list)):
            threads[i].join()
        print 'end:%s' %ctime()

    def GetQuery(self, file_name):
    # Get sql files
        fd = open(self.sql_path + file_name, 'r')
        sql_file = fd.read()
        fd.close()
        return sql_file

    def FormatQuery(self, file_name, server_id):
        sql_file = self.GetQuery(file_name)
        # add parameters to format your sql query
        query = sql_file.format(server_id)
        return query

    def GetEntryList(self):
        entry_list = []
        for i in xrange(0, len(self.server_list)):
            entry_item = entry_mysql()
            entry_item.host_id = self.server_list[i]
            entry_item.host = str.format(entry_item.host_format, self.server_list[i])
            entry_list.append(entry_item)
        return entry_list

    def RunQuery(self, file_name):
        entry_list = self.GetEntryList()
        data_merge = None
        print "{0} begins at {1}" .format(file_name.split('.')[0],ctime())
        start = time.clock()
        for i in range(len(entry_list)):
            item = entry_list[i]
            print item.host
            conn = MySQLdb.connect(host=item.host, port=item.port, user=item.user, passwd=item.passwd)
            query = self.FormatQuery(file_name, self.server_list[i])
            data_record = sql.read_sql(query, conn)
            #print self.server_list[i], 'len: ', len(data_record)
            if data_merge is None:
                data_merge = data_record
            else:
                data_merge = pd.concat([data_merge, data_record])
            conn.close()
        end = time.clock()
        print "The time for {0} was {1} min".format(file_name.split('.')[0], (end - start)/60)
        data_merge.to_csv(self.export_path + file_name.split('.')[0]+'.csv',index=False)

def main():
    sp=SqlProcess()
    sp.run()


if __name__ == "__main__":
    main()
--a sample query to get hero data from multiple servers
select 
{0} as server_id,
date(dtEventTime) as date,
iAction,
iHeroType,
count(distinct iUserId) as user_count,
count(1) as counts
from dgame_oss_db_{0}.hero_gain 
where iHeroType = '94' 
and dtEventTime >='2016-09-10'
group by 1,2,3,4
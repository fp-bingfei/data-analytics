import boto3
import argparse
import string
import os
import hashlib
import json
import datetime
from shutil import copyfile

#Testing cmd:
#   python GenerateDataPipeline.py -g TEST -w wg-ec2-pipeline-01 -dcsu s3://tangtest/abc.sql -daggu s3://tangtest/123.sql -st '2016-01-27 01:10:00' -c /Users/Yanyu/funplus/analytics/python/conf/custom_pipeline.json

class GenerateDataPipeline(object):
    def __init__(self, args):
        self.args = args
    def generateDataPipeline(self):
        pipeline_name = self.args["game"] + '-Custom-Pipeline'
        pipeline_uniqueId = hashlib.md5(pipeline_name).hexdigest()
        pipeline_object_json = self.generateJson()
        pipeline_object = json.loads(pipeline_object_json)["pipelineObjects"]
        datapipeline = boto3.session.Session(aws_access_key_id='AKIAJRKDNC52OAINDWKA', 
                                       aws_secret_access_key='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', 
                                       region_name='us-west-2').client('datapipeline')
        
        #use datapipeline api to create datapipeline
        pipelineId_dict = datapipeline.create_pipeline(name = pipeline_name, uniqueId = pipeline_uniqueId)
        response = datapipeline.put_pipeline_definition(pipelineId=pipelineId_dict["pipelineId"], pipelineObjects=pipeline_object)
        if not response["errored"]:
            datapipeline.activate_pipeline(pipelineId=pipelineId_dict["pipelineId"],
                                           startTimestamp=datetime.datetime.strptime(self.args["starttime"], '%Y-%m-%d %H:%M:%S'))
        else:
            print "DataPipeline defination is not valid."
        

    def run(self):
        self.generateDataPipeline()
        
    
    def generateJson(self):
        src = self.args["conf"]
        dst = '/mnt/funplus/files/'
        if not os.path.exists(dst):
            os.makedirs(dst)
        try:
            copyfile(src, dst + self.args["game"] + '.json')
        except Exception:
            print "copy file failed!"
        with open (src,'rb') as f:
            tmpl_content = string.Template(f.read())
            content = tmpl_content.substitute(game = self.args["game"], dailyaggURL = self.args["dailyaggURL"], 
                                                   workergroup = self.args["workergroup"], dailycustomURL = self.args["dailycustomURL"],
                                                   starttime = self.args["starttime"].replace(' ','T'))
        with open(dst + self.args["game"] + '.json', 'wb') as f:
            f.write(content)
        return content
        

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g','--game',help='input game name',required=True)
    parser.add_argument('-w','--workergroup',help='input the workergroup name',required=True)
    parser.add_argument('-dcsu','--dailycustomURL',help='input the daily custom script url',required=True)
    parser.add_argument('-daggu','--dailyaggURL',help='input the daily agg script url',required=True)
    parser.add_argument('-st','--starttime',help='input the start time of schedule, format is yyyy-MM-dd HH:mm:SS',required=True)
    parser.add_argument('-c','--conf',help='Config file in JSON format',required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    gdp = GenerateDataPipeline(args);
    gdp.run()
    

if __name__ == '__main__':
    main()

# required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse

import psycopg2
import yaml
import pprint
import argparse

class CopyToRedshift:

	def __init__(self, args):
		self.args = args	

	def run (self):
		self.setDBParams()
		conn = self.getConnection()
		for k in self.args:
			print k
		if self.args['pre_sql'] != None:
			self.execPreSql(conn)
		self.copy(conn)
		if self.args['post_sql'] != None:
			self.execPostSql(conn)

	def execPreSql(self, conn):
		cursor = conn.cursor()
		cursor.execute(self.args['pre_sql'])
		print ("\npre_sql:" + self.args['pre_sql'])

	def execPostSql(self, conn):
		cursor = conn.cursor()
		postsqls = [x.strip() for x in self.args['post_sql'].split(';') if x!='']
		for p in postsqls:
			cursor.execute(p)
			conn.commit()
		print ("\npost_sql:" + self.args['post_sql'])

	def getCommandOptions(self):
		commandOptions = self.args['command_options'].split(',')
		commandOptionStr = ''
		for commandOpt in commandOptions:
			cleanStr = commandOpt.strip()
			# - added single quote for string starts with s3:// 
			commandOptionStr += (' ' + '\'' + cleanStr + '\'') if cleanStr.startswith('s3://') else (' ' + cleanStr)
		return commandOptionStr		
		
	def copy(self, conn):
		##query = "copy %s from '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' %s maxerror 100" % (self.args['table'],self.args['s3path'],self.aws_access_key_id, self.aws_secret_access_key, self.args['command_options'].replace(',', '\t'))
		query = "copy %s %s from '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' %s maxerror %s" % (self.args['table'], self.args.get('columns',''), self.args['s3path'],self.aws_access_key_id, self.aws_secret_access_key, self.getCommandOptions(), (str(max(int(self.args['maxerror']), 100)) if (self.args['maxerror'] and self.args['maxerror'].isdigit()) else '100'))
		print ("\nquery:" + query)
		cursor = conn.cursor()
		cursor.execute(query)
		conn.commit()
        print ("\ncommit above query")
		

	def getConnection(self):
		conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host,self.db_port,self.db_name,self.db_username,self.db_password)
		conn = psycopg2.connect(conn_string)
		return conn

	def setDBParams(self):
		f = open(self.args['conf'], "r")
		confMap = yaml.safe_load(f)
		f.close()
		self.db_host = confMap['tasks']['defaults']['db_host']
		self.db_port = confMap['tasks']['defaults']['db_port']
		self.db_name = confMap['tasks']['defaults']['db_name']
		self.db_username = confMap['tasks']['defaults']['db_username']
		self.db_password = confMap['tasks']['defaults']['db_password']
		self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
		self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']

def parse_args():
	parser = argparse.ArgumentParser(description='Copy file into Redshift')
	parser.add_argument('-c','--conf', help='Config file in YAML format', required=True)
	parser.add_argument('-s','--schema', help='DB schema name for specific table', required=False)
	parser.add_argument('-t','--table', help='DB table name', required=True)
	parser.add_argument('--columns', help='DB table columns', required=False)
	parser.add_argument('-s3path','--s3path', help='The s3path of data source', required=True)
	parser.add_argument('-pre-sql','--pre-sql', help='SQL statement executed prior to data loading. e.g. delete from table where ...', required=False)
	parser.add_argument('-post-sql','--post-sql', help='SQL statement executed after data loading. e.g. delete from table where ...', required=False)
	parser.add_argument('-command-options','--command-options', help='The COPY command options e.g. json,jsonpath,gzip or delimiter,\'\\t\',gzip', required=True)
	parser.add_argument('-maxerror', '--maxerror', help='The max error can be ignored, default is 100', required=False)
	args = vars(parser.parse_args())
	return args

def main():
	args = parse_args()
	c2r = CopyToRedshift (args)
	c2r.run()
	

if __name__ == '__main__':
	main()



import MySQLdb
import datetime
import urllib2
import requests
import json
import argparse
import os

class YoutubeReport():
    def __init__(self, args):
        self.client_id = '359069064671-seavphgsm79rqeavqukgsnm335rvns71.apps.googleusercontent.com'
        self.client_secret = 'oSy9wONZK72aZbveM1fyJDmD'
        self.youtube_db_host = 'funplus-adjust.cr0ktgcuumkc.us-west-2.rds.amazonaws.com'
        self.youtube_db_user = 'worker'
        self.youtube_db_pw = 'r0qUqnWMrROyKKDh'
        self.youtube_db_name = 'infras'
        self.cur_date = datetime.datetime.strptime(args['date'], '%Y-%m-%d')


    def run(self):
        tokens = self.get_all_tokens()
        for refresh_token in tokens:
            (name, access_token) = self.get_update_access_token(refresh_token)
            job_list = self.list_job(access_token)
            job_id = self.get_job_id(job_list)
            if job_id == -1:
                self.create_job(access_token)
            else:
                report_list = self.list_report(access_token, job_id) # could set date here, days back is 3 by default
                (report_id, downloadUrl) = self.get_report_id(report_list)
                if report_id == -1:
                    print "NO valid report for job_id: " + job_id
                else:
                    self.download_report(access_token, downloadUrl, name)

    def get_all_tokens(self):
        sql_refresh_token = "SELECT refresh_token FROM youtube WHERE refresh_token is not null and is_active = 'YES';"

        db = self.get_db_connection()
        try:
            cur = db.cursor()
            cur.execute(sql_refresh_token)
            result = cur.fetchall()
        except Exception, e:
            print e
            result = []
        
        db.commit()
        db.close()

        return result

    def list_job(self, access_token):
        url_list_jobs = 'https://youtubereporting.googleapis.com/v1/jobs?'
        headers = {'Authorization': 'Bearer ' + access_token}
        try:
            r = requests.get(url_list_jobs, headers = headers)
            r_json = json.loads(r.text)
        except Exception, e:
            print e
            print 'In list job: ' + job_id + ' for token: ' + access_token
            return -1

        print r_json
        return r_json
        #res is something like this
        # {
        #"jobs": [
        #         {
        #             "id": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
        #             "reportTypeId": "channel_basic_a1",
        #             "name": "test",
        #             "createTime": "2016-05-19T15:46:55.000Z"
        #         }
        #      ]
        # }


    def create_job(self, access_token):
        url_create_job = 'https://youtubereporting.googleapis.com/v1/jobs'
        headers = {'Authorization': 'Bearer ' + access_token}

        payload = {}
        payload['reportTypeId'] = 'channel_basic_a1'
        payload['name'] = 'funplus'

        print payload
        #print headers
        #payload['createTime'] = (datetime.date.today() - datetime.timedelta(2)).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        try:
            r = requests.post(url_create_job, headers = headers, data = json.dumps(payload))
            r_json = json.loads(r.text)
        except Exception, e:
            print e
            print 'In create_job: ' + ' for token: ' + access_token
            return -1

        print r_json
        return r_json


    #def get_job_id(self, job_list, name='funplus', reportTypeId='channel_basic_a1'):
    def get_job_id(self, job_list, reportTypeId='channel_basic_a1'):
        job_id = -1

        if job_list.has_key('error'):
            print job_list
            print "Please check the error!"
        elif not job_list.has_key('jobs'):
            return job_id

        for job in job_list['jobs']:
            #if job['name'] == name and job['reportTypeId'] == reportTypeId:
            if job['reportTypeId'] == reportTypeId:
                if job_id == -1:
                    job_id = job['id']
                    createTime = datetime.datetime.strptime(job['createTime'], '%Y-%m-%dT%H:%M:%S.000Z')
                elif createTime < datetime.datetime.strptime(job['createTime'], '%Y-%m-%dT%H:%M:%S.000Z'):
                    job_id = job['id']
                    createTime = datetime.datetime.strptime(job['createTime'], '%Y-%m-%dT%H:%M:%S.000Z')

        return job_id


    def get_job(self, access_token, job_id):
        #original API by youtube
        url_get_job = 'https://youtubereporting.googleapis.com/v1/jobs/%s' % job_id
        headers = {'Authorization': 'Bearer ' + access_token}
        try:
            r = requests.get(url_get_job, headers = headers)
            r_json = json.loads(r.text)
        except Exception, e:
            print e
            print 'In get_job: ' + job_id + ' for token: ' + access_token
            return -1

        return r_json
        #res is something like this
        # {
        #     "id": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
        #     "reportTypeId": "channel_basic_a1",
        #     "name": "test",
        #     "createTime": "2016-05-19T15:46:55.000Z"
        # }


    def delete_job(self, access_token, job_id):
        url_delete_job = 'https://youtubereporting.googleapis.com/v1/jobs/%s' % job_id
        headers = {'Authorization': 'Bearer ' + access_token}
        try:
            r = requests.delete(url_delete_job, headers = headers)
            r_json = json.loads(r.text)
        except Exception, e:
            print e
            print 'In delete_job: ' + job_id + ' for token: ' + access_token
            return -1

        return 1
        #empty if succeed, error if failed
        # {
        #     "id": "307131370",
        #     "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
        #     "startTime": "2016-03-09T08:00:00.000Z",
        #     "endTime": "2016-03-10T08:00:00.000Z",
        #     "createTime": "2016-06-13T05:34:47.449186Z",
        #     "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/307131370?alt=media"
        # }


    def list_report(self, access_token, job_id):
        url_list_reports = 'https://youtubereporting.googleapis.com/v1/jobs/%s/reports?startTimeAtOrAfter=%s' % (job_id,
                                        (self.cur_date - datetime.timedelta(3)).strftime('%Y-%m-%dT%H:%M:%S.000Z'))
        headers = {'Authorization': 'Bearer ' + access_token}
        try:
            r = requests.get(url_list_reports, headers = headers)
            r_json = json.loads(r.text)
        except Exception, e:
            print e
            print 'In list_report: ' + job_id + ' for token: ' + access_token + ' for date: ' + datetime.date.today().strftime('%Y-%m-%dT%H:%M:%S.000Z')
            return -1

        return r_json
        # {
        #  "reports": [
        #   {
        #    "id": "69663612",
        #    "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
        #    "startTime": "2016-06-27T07:00:00.000Z",
        #    "endTime": "2016-06-28T07:00:00.000Z",
        #    "createTime": "2016-06-29T08:41:40.391042Z",
        #    "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/69663612?alt=media"
        #   },
        #   {
        #    "id": "315070369",
        #    "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
        #    "startTime": "2016-06-26T07:00:00.000Z",
        #    "endTime": "2016-06-27T07:00:00.000Z",
        #    "createTime": "2016-06-28T04:59:27.527061Z",
        #    "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/315070369?alt=media"
        #   }
        #  ]
        # }

    def get_report_id(self, report_list):
        report_id = -1
        print report_list
        if not report_list.has_key('reports'):
            return (report_id, None)

        for report in report_list['reports']:
            if report['startTime'].split('T')[0] == self.cur_date.strftime('%Y-%m-%d'):
                #this should be the latest report available, need to add more if-else later
                if report_id == -1:
                    report_id = report['id']
                    createTime = datetime.datetime.strptime(report['createTime'], '%Y-%m-%dT%H:%M:%S.%fZ')
                    downloadUrl = report['downloadUrl']
                elif createTime < datetime.datetime.strptime(report['createTime'], '%Y-%m-%dT%H:%M:%S.%fZ'):
                    report_id = report['id']
                    downloadUrl = report['downloadUrl']
                    createTime = datetime.datetime.strptime(report['createTime'], '%Y-%m-%dT%H:%M:%S.%fZ')

        return (report_id, downloadUrl)

    def get_report(self, access_token, job_id, report_id):
        #original API by youtube
        url_get_report = 'https://youtubereporting.googleapis.com/v1/jobs/%s/reports/%s' % (job_id, report_id)
        headers = {'Authorization': 'Bearer ' + access_token}
        try:
            r = requests.get(url_get_report, headers = headers)
            r_json = json.loads(r.text)
        except Exception, e:
            print e
            print 'In get_report: ' + job_id + ' for token: ' + access_token + ' for report_id: ' + report_id
            return -1

        return r_json
        # {
        #  "id": "315070369",
        #  "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
        #  "startTime": "2016-06-26T07:00:00.000Z",
        #  "endTime": "2016-06-27T07:00:00.000Z",
        #  "createTime": "2016-06-28T04:59:27.527061Z",
        #  "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/315070369?alt=media"
        # }
    def download_report(self, access_token, downloadUrl, name):
        headers = {'Authorization': 'Bearer ' + access_token}
        try:
            r = requests.get(downloadUrl, headers = headers)
            filename = '/tmp/youtube/' + self.cur_date.strftime('%Y-%m-%d/') + name + '.csv'
            dirname = os.path.dirname(filename)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            f = open(filename, 'w')
            f.write(r.text)
            f.close()
        except Exception, e:
            print e
            return -1

        return 1

    def get_update_access_token(self, refresh_token):
        url_validate_token = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
        sql_validate_token = 'SELECT name, access_token FROM youtube WHERE refresh_token = %s'

        db = self.get_db_connection()
        cur = db.cursor()
        cur.execute(sql_validate_token, (refresh_token, ))
        name, access_token = cur.fetchone()
        print "In getting report for user: " + name
        try:
            r = requests.get(url_validate_token % access_token)
            r_json = json.loads(r.text)
            if r_json.has_key('error'):
                payload = {}
                payload['client_id'] = self.client_id
                payload['client_secret'] = self.client_secret
                payload['grant_type'] = 'refresh_token'
                payload['refresh_token'] = refresh_token

                r = requests.post('https://accounts.google.com/o/oauth2/token', data = payload)
                r_json = json.loads(r.text)

                if r_json.has_key('access_token'):
                    access_token = r_json['access_token']
                    sql_update_token = '''UPDATE youtube SET access_token = %s WHERE refresh_token = %s'''
                    cur.execute(sql_update_token, (access_token, refresh_token))

                else:
                    print "Refresh_token not successful: " + refresh_token
        except Exception, e:
            print e

        db.commit()
        db.close()
        return (name, access_token)
        
    def get_db_connection(self):
        db_connection = MySQLdb.connect(host = self.youtube_db_host,
                        user = self.youtube_db_user,
                        passwd = self.youtube_db_pw,
                        db = self.youtube_db_name,
                        use_unicode=True, charset="utf8")

        return db_connection

def parse_args():
    parser = argparse.ArgumentParser(description='Download adjust data to BI')
    parser.add_argument('-d','--date', help='date to process, yyyy-mm-dd', required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    yr = YoutubeReport(args)
    yr.run()

if __name__ == '__main__':
    main()




from fabric.api import *

env.hosts = ['ec2-52-33-211-136.us-west-2.compute.amazonaws.com', 'ec2-54-69-2-199.us-west-2.compute.amazonaws.com']
env.user = 'ec2-user'
env.key_filename = '~/keys/data-keypair.pem'

def gitpull():
    with settings(prompts={"Password for 'https://fp-data-r@bitbucket.org': ": 'Fun-P1us'}):
    	run('cd /mnt/funplus/data-analytics/ && git pull')
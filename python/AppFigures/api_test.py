""" Script that demonstrates how to access the appFigures API
    from python. It relies on requests, because urllib is no fun
    Released into the Public Domain (for what it's worth)
"""
import pprint
import requests
import pandas as pd
import numpy as np
from pandas_to_redshift import *
import concurrent.futures as futures
import itertools
from sqlalchemy import create_engine
from sqlalchemy import text
from datetime import date

# usage: python -m AppFigures.api_test (move up to python/)
# Fill these constants in
USERNAME = 'lentini@qq.com'
PASSWORD = 'halfquest'
APP_KEY = '58acdadbbf4e4633a03364909dba9d92'
BASE_URI = "https://api.appfigures.com/v2/"
conn_str = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
ffs_engine = create_engine(conn_str)

# Helper function for auth and app_key
# first / in uri is optional

def json_value_from_path(dic, path):
  for p in path:
    if p in dic:
      dic = dic[p]
    else:
      return None
  return dic

def make_request(uri, **querystring_params):
  headers = {"X-Client-Key": APP_KEY}
  auth =(USERNAME, PASSWORD)
  return requests.get(BASE_URI + uri.lstrip("/"),
                        auth=auth,
                        params=querystring_params,
                        headers=headers)

# Get the root resource to show we are in business
"""root_response = make_request("/")
assert 200 == root_response.status_code
assert USERNAME == root_response.json()["user"]["email"]"""

def country_rank(product_id, date_tuple, countries):
  rank_response = make_request("/ranks/{pid}/daily/{sd}/{ed}/?countries={country}".format(pid=product_id, sd=date_tuple[0], ed=date_tuple[1], country=countries))
  return rank_response

"""search_response = make_request("/products/search/hay+day?count=50")
assert search_response.status_code==200
print search_response.json()"""


# Get a list of products
"""product_response = make_request("/products/mine")
if product_response.status_code!=200:
  print product_response.text
assert 200 == product_response.status_code
assert 0 < len(product_response.json())
product_dict = {}
for (id, product) in product_response.json().items():
  if product["type"]=="app":
    product_dict[product["id"]] = [product["name"], product["vendor_identifier"]]

countries = make_request("/data/countries")
assert 200 == countries.status_code
game_countries = "MT,SS,TZ,SR,ZA,KH,MV,OM,SD,GE,HN,JM,NC,SX,HT,LV,MN,PS,TR,DJ,EG,ES,JP,ME,RO,TW,NZ,TJ,VI,IN,MM,MW,RE,NF,TC,US,SY,ZW,LT,PF,SA,UA,TF,TH,MD,RW,SI,YT,KR,NR,PW,TG,CC,CL,CW,IO,KP,VG,AR,AU,AW,FJ,FO,KY,LI,GQ,LB,LR,NI,A1,A2,AE,AO,AZ,BO,BT,BY,HR,DZ,EU,FM,MA,MY,RU,VC,GR,LU,LY,PM,LK,PE,QA,TN,BI,BM,BR,GU,GY,PH,SZ,IM,ML,MO,PT,PY,TT,BA,BE,BL,GM,GN,MX,SJ,VN,BQ,CF,CH,IE,JO,PN,NA,SV,UG,GH,KN,KZ,NG,O1,TL,YE,VE,EC,GB,GD,MF,PG,DO,ER,ET,LC,MS,EH,GP,GS,MU,SH,EE,GF,GI,MG,SE,TK,SC,WF,KI,NP,PL,TD,CM,CO,CY,IR,KW,VU,MQ,SO,TV,MP,SM,TM,DE,DK,DM,JE,LS,SK,WS,AD,AF,AM,AP,BG,CD,CG,NO,ST,MC,PR,SB,UM,BD,BH,BN,GT,GW,PA,TO,VA,MH,SG,SN,BJ,BS,BZ,HM,ID,PK,BW,CI,CU,IL,KG,UY,AG,AI,AQ,BV,CA,CK,CV,UZ,GA,HK,IQ,MZ,SL,AL,AS,AT,CX,FI,GL,KE,GG,HU,KM,NE,ZM,AX,BB,BF,FK,FR,MK,NL,CN,CR,CZ,IT,LA,IS,MR,NU,RS".split(',')

invalid_countries = "SD,MV,GE,HT,NC,PS,DJ,ME,MM,RE,VI,NF,PF,SY,TF,RW,YT,NR,TG,CW,IO,AW,FO,LI,GQ,MA,LY,BI,GU,IM,BA,GN,CF,TL,ER,ET,GP,GS,GF,GI,TK,WF,KI,CM,IR,VU,MQ,SO,TV,MP,SM,LS,JE,WS,AD,AF,CD,MC,PR,BD,TO,VA,MH,CU,CI,AQ,BV,CK,GA,IQ,AS,GL,GG,KM,AX,ZM,FK,NU,RS".split(',')

#valid_countries = [c for c in game_countries if c in countries.json() and c not in invalid_countries]"""
valid_countries = "MT,TZ,SR,ZA,KH,OM,HN,JM,LV,MN,TR,EG,ES,JP,RO,TW,NZ,TJ,IN,MW,TC,US,ZW,LT,SA,UA,TH,MD,SI,KR,PW,CL,VG,AR,AU,FJ,KY,LB,LR,NI,AE,AO,AZ,BO,BT,BY,HR,DZ,FM,MY,RU,VC,GR,LU,LK,PE,QA,TN,BM,BR,GY,PH,SZ,ML,MO,PT,PY,TT,BE,GM,MX,VN,CH,IE,JO,NA,SV,UG,GH,KN,KZ,NG,YE,VE,EC,GB,GD,PG,DO,LC,MS,MU,EE,MG,SE,SC,NP,PL,TD,CO,CY,KW,TM,DE,DK,DM,SK,AM,BG,CG,NO,ST,SB,BH,BN,GT,GW,PA,SG,SN,BJ,BS,BZ,ID,PK,BW,IL,KG,UY,AG,AI,CA,CV,UZ,HK,MZ,SL,AL,AT,FI,KE,HU,NE,BB,BF,FR,MK,NL,CN,CR,CZ,IT,LA,IS,MR".split(',')

#39762191619: 'Hay Day'
game_mapping = {307046454: 'Family Farm Seaside'}

games = [';'.join([str(x) for x in game_mapping.keys()])]

def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

valid_country_strings = [';'.join(chunk) for chunk in chunks(valid_countries, 50)]

sql = '''select name, country, max(date) as date from processed.ranking where name in ('{names}') group by 1,2;'''
dates_df = pd.read_sql_query(sql.format(names="','".join(game_mapping.values())), ffs_engine)
begin_date = dates_df['date'].min()
del_sql = '''delete from processed.ranking where date>='{date}' and name in ('{names}');'''.format(date=begin_date.strftime('%Y-%m-%d'), names="','".join(game_mapping.values()))
ffs_engine.execute(text(del_sql).execution_options(autocommit=True))
print begin_date

date_array = [(begin_date.date(), date.today())]
#for prd in pd.period_range('1/1/2015', '2/1/2016', freq='M'):
#  date_array.append((prd.start_time.date(), prd.end_time.date()))
arrays = [games, date_array, valid_country_strings]

rank_rows = []
invalid_countries = []

with futures.ThreadPoolExecutor(8) as executor:
  fs = [executor.submit(country_rank, *q) for q in itertools.product(*arrays)]
  for i, f in enumerate(futures.as_completed(fs)):
    rank_response = f.result()
    if rank_response.status_code==200:
      path_dict = {'country': ['country'], 
                   'device': ['category', 'device'], 
                   'chart_name': ['category', 'name'], 
                   'chart_id': ['category', 'id'],
                   'parent_chart_id': ['category', 'parent_id'],
                   'store': ['category', 'store'],
                   'subtype': ['category', 'subtype'],
                   'ranks': ['positions'],
                   'product_id': ['product_id']}
      dates = pd.to_datetime(rank_response.json()['dates'])
      for detail in rank_response.json()["data"]:
        tmp = {}
        for k, path in path_dict.items():
          tmp[k] = json_value_from_path(detail, path)
        ranks = tmp.pop('ranks')
        product_id = tmp.pop('product_id')
        product_name = game_mapping[product_id]
        for i, rank in enumerate(ranks):
          row = {}
          row['rank'] = rank
          row['date'] = dates[i]
          row['name'] = product_name
          row.update(tmp)
          rank_rows.append(row)
    else:
      msg = rank_response.json().get('message', '')
      if 'The country' in msg:
        invalid_countries.append(msg[12:14])

df = pd.DataFrame(rank_rows)
df = df[df['rank'].notnull()]
df['parent_chart_id'] = df['parent_chart_id'].fillna(-1).astype(np.int64)
df['rank'] = df['rank'].astype(np.int64)

print invalid_countries
  
if len(df)>0:
    h = 'bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    df_to_redshift(df, 'processed.ranking', h, 'ffs', append=True)
else:
    print 'No ranking within this time period!'

"""
if __name__ == '__main__' and __package__ is None:
    import os
    os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Get data for all inapps for a year by month
products = product_response.json().values()
inapps = [p for p in products if p["type"] == u"app"]
inapp_ids = [str(product["id"]) for product in inapps]
inapp_names = [inapp["name"] for inapp in inapps]
route = "/sales/dates+products/2012-01-01/2012-12-31"
inapp_sales_response = make_request(route,
                                    granularity="monthly",
                                    products=";".join(inapp_ids))
print(inapp_sales_response.json())
assert 200 == inapp_sales_response.status_code

# Make a little table of revenue
data = inapp_sales_response.json()
months = sorted(data.keys())
print(",".join(["date\\product_name"] + inapp_names))
for month in months:
  values = data[month]
  downloads = []
  for inapp_id in inapp_ids:
    if inapp_id in values:
      downloads.append(values[inapp_id]["downloads"])
    else:
      downloads.append("0")
  print(",".join([month] + map(str, downloads)))"""
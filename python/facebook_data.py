import urllib2
import datetime
import time
import json
import argparse


class FacebookData:
    def __init__(self, args):
        self.apps = ['1526052704380683', '209256072755630', '928643917165279', '308159869371679', '843964315647667',
            '737039859709497', '418582391520509', '225931867500395', '200942910003390', '365409093472457',
            '317180824986452', '262053390586780', '252340124853215', '231120903637672', '617904311583729',
            '357392490938552', '296247723822639', '480173892052608', '562157243794316', '405163009637002',
            '228207830651511', '454132378016398', '535493929807232', '313720965417486', '654608787886376',
            '187271604773962', '384296748343281', '1503546813190251', '1484657951840471', '221511944655378',
            '418107208278997', '324465824375200', '496231573836614', '217316998627741', '1781521478748822',
            '1470915916565194', '875555822515981', '985661654798211', '1644872099080413', '826324577431836',
            '918957474850980', '554786404548889', '546278088730267', '125058397663538', '149990781832240',
            '584293908258021', '571940576167659', '402436213188733', '990625624346027', '1593321087621585',
            '779071605541974', '1597521733896516', '164205810639904', '999935923410441', '501485990051465',
            '1076968169031385', '1653603358186132']
                    # Total 111 accounts, 3 of them are not valid anymore
                    # ['1781521478748822', '1076968169031385', '1237052822974654', '149107925273276', '512288065503830',
                    #     '1598145473809703', '223009818058664', '217316998627741', '1104502566248095', '1674471196137650',
                    #     '990625624346027', '1515782845396015', '951516744925761', '439377969602891', '1129423147076195',
                    #     '1652056151721986', '763709073772658', '160350187682779', '517987521721702', '1034821549911718',
                    #     '1520896378206065', '748408368626062', '1039114062807288', '308159869371679', '209256072755630',
                    #     '1666441210280373', '1526052704380683', '918957474850980', '1491040221198840', '1470915916565194',
                    #     '1484657951840471', '225931867500395', '1094352833910531', '1614753235460219', '645885125554812',
                    #     '1620574764892713', '1653603358186132', '850120705054246', '947712931959613', '376739152521317',
                    #     '875555822515981', '950837111604702', '1414640515521010', '400281643488408', '1593321087621585',
                    #     '856124924475883', '458419454334309', '1436971906598343', '899722016717182', '671644752964182',
                    #     '484523415032224', '680736332055864', '1644872099080413', '771944589563522', '100289063641243',
                    #     '1574647622792824', '454132378016398', '146065298878544', '554786404548889', '1555851311325114',
                    #     '187271604773962', '252340124853215', '654608787886376', '149990781832240', '562157243794316',
                    #     '617904311583729', '262053390586780', '584293908258021', '402436213188733', '418107208278997',
                    #     '221511944655378', '317180824986452', '496231573836614', '418582391520509', '480173892052608',
                    #     '430534780397752', '535493929807232', '313720965417486', '546278088730267', '125058397663538',
                    #     '571940576167659', '1483781788536698', '789834494430507', '1011228495558445', '324916637703083',
                    #     '489163017888140', '826324577431836', '296247723822639', '384296748343281', '1503546813190251',
                    #     '470303449738850', '228207830651511', '324465824375200', '405163009637002', '785967861448161',
                    #     '231120903637672', '357392490938552', '365409093472457', '1569733913256571', '200942910003390',
                    #     '928643917165279', '743994782300241', '199223433514943', '1619757391619824', '441799879341689',
                    #     '882570758446322', '1428151550807717', '1441000202791435']
                    #     '1105818746149066', '278506362489501', '295116950696964'
        self.httpApi = 'https://graph.facebook.com/v2.6/'
        self.access_token = 'EAADaw5aNczQBAId34ErabDnRbCT7MzwZAtkKsAXKObsq7mPnJgIzBAWsJkPy2sFXemZCOHNXe8aXk7hKliFtKhLImKslN7rJeZCrNrm4uZBTZAPMuQVZBRE5BK6gNVBYZBxElkwMFphyMAgOcBaEpmzYNzAZCkiU4TIZD'
        self.metrics = [('page_impressions', 'day'), ('page_negative_feedback', 'day'), ('page_fan_adds', 'day'),
                            ('page_actions_post_reactions_total', 'day'), ('page_stories_by_story_type', 'day'),
                            ('page_storytellers_by_story_type', 'day'), ('page_fans', 'lifetime')]
        self.daysBack = '5' if args['daysBack'] is None else args['daysBack']

    def run(self):
        stringBuilder = 'date|game|page_fan_adds|page_impressions|page_actions_post_reactions_total|page_storytellers_by_story_type|page_negative_feedback|page_fans\n'
        rowBuilder = '%s|%s|%d|%d|%d|%d|%d|%d\n'
        emotions_stringBuilder = 'date|game|like|love|wow|haha|sorry|anger\n'
        emotions_rowBuilder = '%s|%s|%d|%d|%d|%d|%d|%d\n'

        for app in self.apps:
            tmpRes = []
            for metrics in self.metrics:
                tmpData = self.getMetrics(app, metrics)
                #print tmpData
                if len(tmpData) > 0:
                    tmpRes.append(tmpData[0])
                else:
                    continue
            # print '#######'
            # print tmpRes
            result = self.parseData(tmpRes)
            for key in result.keys():
                row = rowBuilder % (key, app, result[key]['page_fan_adds'], result[key]['page_impressions'],
                                    result[key]['page_actions_post_reactions_total'], result[key]['page_storytellers_by_story_type'],
                                    result[key]['page_negative_feedback'],result[key]['page_fans'])
                stringBuilder += row

            emotions_result = self.parseEmotions(tmpRes)
            for key in emotions_result.keys():
                emotions_row = emotions_rowBuilder % (key, app, emotions_result[key]['like'], emotions_result[key]['love'],
                                    emotions_result[key]['wow'], emotions_result[key]['haha'],
                                    emotions_result[key]['sorry'], emotions_result[key]['anger'])
                emotions_stringBuilder += emotions_row

        f = open('/tmp/facebook_emotions.csv', 'w')
        f.write(emotions_stringBuilder.encode('utf8'))
        f.close()

        f = open('/tmp/facebookData.csv', 'w')
        f.write(stringBuilder.encode('utf8'))
        f.close()

    def getMetrics(self, app, metrics):
        until = int(time.time())
        since = until - 86400 * int(self.daysBack)
        url = self.httpApi + '%s/insights/%s?period=%s&since=%d&until=%d&access_token=%s' % (
            app, metrics[0], metrics[1], since, until, self.access_token)

        try:
            res =  urllib2.urlopen(url)
            jsonRes = json.loads(res.read())
        except Exception, e:
            print e
            print app + metrics[0]

        try:
            return jsonRes["data"]
        except Exception, e:
            print app
            print jsonRes

    def parseData(self, data):
        #result = {date:{{'metric_name': value},{'metric_name': value}, ...}, date:{...}}
        result = {}
        for datum in data:
            metricName = datum['name']
            for v in datum['values']:
                date = v['end_time'].split('T')[0]
                if (v.has_key('value') and isinstance(v['value'], int)):
                    value = v['value']
                elif not v.has_key('value'):
                    value = 0
                else:
                    value = sum(v['value'].values())

                if result.has_key(date):
                    result[date][metricName] = value
                else:
                    result[date] = {metricName: value}

        return result

    def parseEmotions(self, data):
        #result = {date:{{'like': value},{'love': value}, ...}, date:{...}}
        result = {}
        for datum in data:
            if datum['name'] == 'page_actions_post_reactions_total':
                for v in datum['values']:
                    date = v['end_time'].split('T')[0]
                    for key in v['value'].keys():
                        if result.has_key(date):
                            result[date][key] = v['value'][key]
                        else:
                            result[date] = {key: v['value'][key]}
            else:
                pass

        return result


def parse_args():
    parser = argparse.ArgumentParser(description='Download adjust data to BI')
    parser.add_argument('-d','--daysBack', help='how many days data you want until now, default is 5', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    ad = FacebookData(args)
    ad.run()

if __name__ == '__main__':
    main()
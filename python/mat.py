#!/usr/bin/env python
# -*- coding:utf-8 -*-

import urllib2
from urllib2 import Request, urlopen, URLError, HTTPError
import ujson as json
from pprint import pprint as pp
import argparse
import time
import os
import gzip
from datetime import datetime, timedelta
import urllib
import yaml

__fields = ['id', 'site_event_type', 'status', 'created', 'publisher_id', 'publisher.name','advertiser_id', 'site_id', 'site.name', 'site_event_id', 'campaign_id', 'campaign.name', 'campaign_payout_id', 'currency_code', 'currency_rate', 'existing_user', 'ad_network_id', 'agency_id', 'campaign_ad_id', 'advertiser_file_id', 'campaign_url_id', 'publisher_adgroup_id', 'publisher_adgroup_item_id', 'publisher_sub_publisher_id', 'publisher_sub_site_id', 'publisher_sub_campaign_id', 'publisher_sub_adgroup_id', 'publisher_sub_ad_id', 'publisher_sub_keyword_id', 'advertiser_sub_publisher_id', 'advertiser_sub_site_id', 'advertiser_sub_campaign_id', 'advertiser_sub_adgroup_id', 'advertiser_sub_ad_id', 'advertiser_sub_keyword_id', 'country_id', 'country.name','region_id', 'region.name','ios_ifa_md5', 'os_id_md5', 'ios_ifv_md5', 'os_id_sha1', 'os_id_sha256', 'ios_ifa_sha1', 'device_id_sha256', 'ios_ifv_sha1', 'device_id', 'device_id_md5', 'device_id_sha1', 'os_id', 'user_id', 'device_ip', 'mac_address', 'mac_address_md5', 'mac_address_sha1', 'odin', 'mat_id', 'ios_ifv', 'ios_ifa', 'unid', 'google_aid', 'fb_user_id', 'google_user_id', 'twitter_user_id', 'windows_aid', 'windows_aid_md5', 'windows_aid_sha1', '_id', 'tracking_id', 'ip', 'session_ip', 'session_datetime', 'impression_created', 'user_agent', 'status_code', 'advertiser_ref_id', 'publisher_ref_id', 'publisher_sub1', 'publisher_sub2', 'publisher_sub3', 'publisher_sub4', 'publisher_sub5', 'match_type', 'sdk', 'sdk_version', 'device_brand', 'device_model', 'device_carrier', 'os_version', 'os_jailbroke', 'language', 'package_name', 'app_name', 'app_version', 'device_type', 'age', 'gender', 'latitude', 'longitude', 'altitude']

__installs_export_queue_uri = """
https://api.mobileapptracking.com/v2/advertiser/stats/installs/find_export_queue.json?api_key={api_key}&start_date={start_date}&end_date={end_date}&response_timezone=UTC&fields={fields}&filter=(site_id+in+({site_id}))
"""

__file_download_uri = """
https://api.mobileapptracking.com/v1/export/download.json?api_key={api_key}&job_id={job_id}
"""

__all_apps_uri = """
    https://api.mobileapptracking.com/v2/advertiser/sites/find.json?api_key={api_key}
"""

__all_publishers_uri = """
https://api.mobileapptracking.com/v2/advertiser/publishers/find.json?api_key={api_key}&debug=0
"""

def __get_all_apps(**kwargs):
    url = __all_apps_uri.format(api_key=kwargs.get('api_key'))
    print url
    f = urllib2.urlopen(url)
    resp = f.read()
    return json.loads(resp).get('data') if resp else None

def __get_app_id_by_name(app_name):
    app_id = None
    apps = __get_all_apps()
    print apps

    for app in apps:
        if app.get('name') == app_name:
            app_id = app.get('id')
            break
    
    if app_id:
        return app_id
    else:
        raise Exception('app_id lookup failed')
            
def __get_all_publishers():
    f = urllib2.urlopen(__all_publishers_uri)
    resp = f.read()
    return json.loads(resp) if resp else None
    

def __construct_url(**kwargs):
    fields = ','.join(__fields)
        
    end_date = kwargs.get('dt')
    dt_obj = datetime.strptime(kwargs.get('dt'), '%Y-%m-%d')
    start_date = (dt_obj - timedelta(days=int(kwargs.get('days')))).strftime("%Y-%m-%d")
    site_ids = kwargs.get("site_id")
    
    site_id_filter = ",".join("'"+str(v)+"'" for v in site_ids)
    
    return __installs_export_queue_uri.format(start_date=start_date,
                        end_date=end_date,
                        site_id=site_id_filter,
                        fields = ','.join(__fields),
                        api_key = kwargs.get('api_key'),
                        )

def __download_file(url,**kwargs):
    pp(kwargs)
    
    dt_obj = datetime.strptime(kwargs.get('dt'), '%Y-%m-%d')
    filename = '{dt}_{app}.csv'.format(dt=dt_obj.strftime("%Y%m%d"),app=kwargs.get('app_id'))
    target_file = os.path.join(kwargs.get('output'),filename)
    
    # url = kwargs.get('file_url')
    print url

    req = urlopen(url)
    CHUNK = 64 * 1024
    with open(target_file,'wb') as f:
        while True:
            chunk = req.read(CHUNK)
            if not chunk: break
            f.write(chunk)
    
    return target_file
    
def __post_process(target_file,**kwargs):



    target_file_gz = target_file + '.gz'
    with gzip.open(target_file_gz,'wb') as f_gz:
        with open(target_file,'rb') as f:
            for line in f:
                f_gz.write(line)
                
    print 'cleaning up'
    os.remove(target_file)

def parse_args():
    parser = argparse.ArgumentParser(description='MAT/Hasoffer Install Source Downloader')
    parser.add_argument('-c','--config', help='The config file to be used', required=True)
    parser.add_argument('-dt','--dt', help='The date for which the data will be fetched. YYYY-MM-DD', required=True)
    parser.add_argument('-o','--output', help='The output directory', required=True)
    parser.add_argument('-d','--days', help='days back', required=False, default=30)
    args = vars(parser.parse_args())
    return args
    
def main():
    args = parse_args()
    config_path = args.get('config')

    with open(config_path,"r") as f:
        confMap = yaml.safe_load(f)
        
        
    print confMap
    
        
    
    
    # app_id = __get_app_id_by_name(args.get('test'))
    # app_id = 'test'

    params = dict()
    params['app_id'] = confMap.get('id')
    params['dt'] = args.get('dt')
    params['api_key'] = confMap.get('mat').get('api_key')
    params['site_id'] = confMap.get('mat').get('site_id')
    params['output'] = args.get('output')
    params['days'] = args.get('days')

    pp(params)
    
    # pp(__get_all_apps(**params))
#     exit(0)
    output_dir = args.get('output')
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    url = __construct_url(**params)
    
    print url
    
    f = urllib2.urlopen(url)
    resp = f.read()
    data = json.loads(resp)
    job_id = data.get('data')

    url = __file_download_uri.format(job_id=job_id,api_key=params.get('api_key'))
    print url
    file_url = None

    while True:
        f = urllib2.urlopen(url)
        resp = f.read()
        data = json.loads(resp)
        pp(data)
        status = data.get('data').get('status')
        if status == 'complete':
            file_url = data.get('data').get('data').get('url')
            break
        print 'File not ready - sleeping'
        time.sleep(30)
        
    print file_url
    
    # params['file_url'] = file_url

    target_file = __download_file(file_url,**params)
    print target_file
    
    __post_process(target_file,**params)
    
    

if __name__ == '__main__':
    main()
    

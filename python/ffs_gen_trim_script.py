#buckets are by month, but we split files up by day within the bucket for better granularity
import pandas as pd
import argparse
from datetime import datetime, date, timedelta
from sqlalchemy import create_engine

ffs_conn = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
ffs_engine = create_engine(ffs_conn)

def gen_cleanup_script(unload_cmd_file, start_date=None, end_date=date.today()-timedelta(days=5), events=None, exclude=None):
    if events is None:
        events = []
    if exclude is None:
        exclude = []
    events = [e for e in events if e not in exclude]

    if events==[]:
        sql = '''select event, trunc(ts) as date, count(1) from events_raw 
        where ts>='{sd}' and ts<'{ed}' and event not in ('{exclude}') group by 1,2
        order by 1,2;'''
    else:
        assert type(events)==list
        sql = '''select event, trunc(ts) as date, count(1) from events_raw 
        where ts>='{sd}' and ts<'{ed}' 
        and event in ('{events}') group by 1,2
        order by 1,2;'''

    if start_date is None:
        start_date = date(2014,1,1)

    if end_date is None:
        end_date = date.today()-timedelta(days=5)

    if end_date<=start_date:
        print "End date needs to be later than start date! Exiting."
        return
    
    dr = pd.date_range(start_date, end_date)
    
    sql_new = sql.format(sd=dr[0].strftime('%Y-%m-%d'), ed=dr[-1].strftime('%Y-%m-%d'), events='\',\''.join(events), exclude='\',\''.join(exclude))
    print sql_new
    
    events_df = pd.read_sql_query(sql_new, ffs_engine)
    events = events_df['event'].unique()

    if len(events)==0:
        print "No valid events chosen! Exiting."
        return
    
    s3_prefix = 's3://com.funplusgame.bidata/events_raw/ffs/'
    aws_access_key_id = 'AKIAJRKDNC52OAINDWKA'
    aws_secret_access_key = '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'
    delimiter = '^'
    command_options = 'GZIP ESCAPE'
    events_table = 'public.events_raw'
    
    unload_cmd_raw = """unload ('select * from {events_table} where trunc(ts) >= \\'{start_date}\\' and trunc(ts) < \\'{end_date}\\' and event = \\'{event}\\';') to '{s3_prefix}{event}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
                """
    delete_sql_raw = """delete from public.events_raw where ts>='{sd}' and ts<'{ed}' and event in ('{events}');"""
    vacuum_sql = """vacuum {table};""".format(table=events_table)
    
    unload_cmd_fp = open(unload_cmd_file, 'w')
    
    for ix, r in events_df.iterrows():
        e = r['event']
        d = r['date']
        
        unload_cmd = unload_cmd_raw.format(start_date=d.strftime('%Y-%m-%d'),end_date=(d+pd.DateOffset(days=1)).strftime('%Y-%m-%d'),\
                        event=e,s3_prefix=s3_prefix,s3_date_path=d.strftime('%Y/%m/%d'),\
                        aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key,\
                        delimiter=delimiter,command_options=command_options,events_table=events_table)
        unload_cmd_fp.write(unload_cmd.strip() + '\n')
        unload_cmd_fp.write('\n')
        
    delete_sql = delete_sql_raw.format(sd=start_date.strftime('%Y-%m-%d'), ed=end_date.strftime('%Y-%m-%d'), events='\',\''.join(events))
    unload_cmd_fp.write(delete_sql.strip() + '\n')
    unload_cmd_fp.write(vacuum_sql+ '\n')
    unload_cmd_fp.close()

def main():
    parser = argparse.ArgumentParser(description='Clean up FFS raw events')
    parser.add_argument('f', help='Output file')
    parser.add_argument('-sd', help='Start date', required=False)
    parser.add_argument('-ed', help='End date', required=False)
    parser.add_argument('-e','--events', nargs='*', help='List of events to trim', required=False)
    parser.add_argument('-ex','--exclude', nargs='*', help='List of events to exclude from trimming', required=False)
    args = parser.parse_args()

    if type(args.sd) == str:
        args.sd = datetime.strptime(args.sd,'%Y-%m-%d').date()
    if type(args.ed) == str:
        args.ed = datetime.strptime(args.ed,'%Y-%m-%d').date()

    print args.sd, args.ed
    gen_cleanup_script(args.f, args.sd, args.ed, args.events, args.exclude)

if __name__ == '__main__':
    main()
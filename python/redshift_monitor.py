###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse

import psycopg2
from html import HTML
import datetime
import smtplib
import yaml
import argparse
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class RedshiftMonitor(object):
    def __init__(self,args):
        self.args = args

    def run(self):
        self.setDBParams()
        conn = self.getConnection()
        cursor = conn.cursor()
        sql = "select pid,userid,user_name,starttime,datediff(m,starttime,GETDATE())*1.00/60 as elapsed_hours,db_name,query from  stv_recents where status = 'Running' and query not like '%vacuum%' and datediff(h,starttime,GETDATE())>1"
        cursor.execute(sql)
        result = cursor.fetchall()
        if len(result) != 0:
            htmlobj = HTML()
            htmltable = htmlobj.table(border='1')
            htmltableheader = htmltable.tr
            htmltableheader.th('pid')
            htmltableheader.th('userid')
            htmltableheader.th('user_name')
            htmltableheader.th('starttime')
            htmltableheader.th('elapsed_hours')
            htmltableheader.th('db_name')
            htmltableheader.th('query')
            for row in range(len(result)):
                htmltable_row = htmltable.tr
                for col in range(len(result[0])):
                    htmltable_row.td(str(result[row][col]))
            self.sendMail(str(htmltable))
        conn.close()

    def setDBParams(self):
        f = open(self.args['conf'],"r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" %(self.db_host,self.db_port,self.db_name,self.db_username,self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn


    def sendMail(self,htmlcode):
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmlcode,'html')
        msg.attach(html)
        msg['Subject'] = 'Redshift long running query result ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "Redshift Monitor<tableau_admin@funplus.com>"
        msg['To'] = 'data@funplus.com'
        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
            continue

def parse_args():
    parser = argparse.ArgumentParser(description='Redshift Monitor')
    parser.add_argument('-c','--conf',help='Config file in YAML format', required=True)
    parser.add_argument('-s','--schema',help='DB schema name for specific table',required=False)
    parser.add_argument('-t','--table',help='DB table name', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    rm = RedshiftMonitor(args)
    rm.run()


if __name__ == '__main__':
    main()

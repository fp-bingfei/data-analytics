import urllib2
import json
import datetime
import csv
import time
import argparse
from urllib import urlencode
from facepy.utils import get_extended_access_token


class FbFeedScrape:
    def __init__(self,args):
        self.group_id = ['497477766976645','134662876689335','406333626087960']
        self.app_id = '240533579330356'
        self.app_secret = '43938ceb3417428328ec43e1bba0e4f5'
        self.access_token = 'EAADaw5aNczQBAM6tBDZA5VT2Gab0m8QIEIXf7Oy8hdSq8JfeOprqZCrRxpwsemtTNvyTgwZC5leFsxQge5FzZA1QIVJryezJk0OWmZCSFN3TncHrFcje8c5SijhTPl7jmCcgbFmSgXSV9m5I8SppE1QRazxUK5IoZD'
        self.daysBack = '5' if args['daysBack'] is None else args['daysBack']

    def run(self):
        # check access_token is expired or not
        long_lived_access_token, expires_at = self.refresh_token(self.access_token)
        if expires_at <= datetime.datetime.fromtimestamp(int(time.time())):
            long_lived_access_token, expires_at = self.refresh_token(long_lived_access_token)

        for group_id in self.group_id:
            self.scrapeFacebookPageFeedStatus(group_id, long_lived_access_token)

    def refresh_token(self,short_lived_access_token):
        long_lived_access_token, expires_at = get_extended_access_token(short_lived_access_token, self.app_id,self.app_secret)
        return long_lived_access_token,expires_at

    def request_until_succeed(self,url):
        req = urllib2.Request(url)
        success = False
        while success is False:
            try:
                response = urllib2.urlopen(req)
                if response.getcode() == 200:
                    success = True
            except Exception, e:
                print e
                time.sleep(5)

                print "Error for URL %s: %s" % (url, datetime.datetime.now())
                print "Retrying."

        return response.read()

    # Needed to write tricky unicode correctly to csv
    def unicode_normalize(self,text):
        return text.translate({0x2018: 0x27, 0x2019: 0x27, 0x201C: 0x22, 0x201D: 0x22,
                               0xa0: 0x20}).encode('utf-8')

    def getFacebookPageFeedData(self, group_id, access_token, num_statuses):

        # Construct the URL string; see
        # http://stackoverflow.com/a/37239851 for Reactions parameters
        base = "https://graph.facebook.com/v2.7"
        node = "/%s/feed" % group_id
        fields = "/?fields=message,permalink_url,created_time,type,name,id," + \
                 "comments.limit(0).summary(true),shares,reactions." + \
                 "limit(0).summary(true),from"
        since = int(time.time()) - 86400 * int(self.daysBack)
        parameters = "&limit=%s&since=%s&access_token=%s" % (num_statuses, since, access_token)
        url = base + node + fields + parameters
        # retrieve data
        data = json.loads(self.request_until_succeed(url))

        return data

    def getReactionsForStatus(self, status_id, access_token):

        # See http://stackoverflow.com/a/37239851 for Reactions parameters
        # Reactions are only accessable at a single-post endpoint

        base = "https://graph.facebook.com/v2.6"
        node = "/%s" % status_id
        reactions = "/?fields=" \
                    "reactions.type(LIKE).limit(0).summary(total_count).as(like)" \
                    ",reactions.type(LOVE).limit(0).summary(total_count).as(love)" \
                    ",reactions.type(WOW).limit(0).summary(total_count).as(wow)" \
                    ",reactions.type(HAHA).limit(0).summary(total_count).as(haha)" \
                    ",reactions.type(SAD).limit(0).summary(total_count).as(sad)" \
                    ",reactions.type(ANGRY).limit(0).summary(total_count).as(angry)"
        parameters = "&access_token=%s" % access_token
        url = base + node + reactions + parameters

        # retrieve data
        data = json.loads(self.request_until_succeed(url))

        return data

    def processFacebookPageFeedStatus(self,status, access_token):

        status_id = status['id']
        permanent_link = 'None' if 'permalink_url' not in status.keys() else self.unicode_normalize(status['permalink_url'])
        status_message = 'None' if 'message' not in status.keys() else self.unicode_normalize(status['message'])
        link_name = 'None' if 'name' not in status.keys() else self.unicode_normalize(status['name'])
        status_type = status['type']
        #status_link = '' if 'link' not in status.keys() else self.unicode_normalize(status['link'])
        status.setdefault('from')
        if status['from'] is None:
            status_author = 'None'
        else:
            status_author = self.unicode_normalize(status['from']['name'])

        # Time needs special care since a) it's in UTC and
        # b) it's not easy to use in statistical programs.

        status_published = datetime.datetime.strptime(status['created_time'], '%Y-%m-%dT%H:%M:%S+0000')
        # status_published = status_published + datetime.timedelta(hours=-5)  # EST
        # best time format for spreadsheet programs:
        status_published = status_published.strftime('%Y-%m-%d %H:%M:%S')

        # Nested items require chaining dictionary keys.

        num_reactions = 0 if 'reactions' not in status else status['reactions']['summary']['total_count']
        num_comments = 0 if 'comments' not in status else status['comments']['summary']['total_count']
        num_shares = 0 if 'shares' not in status else \
            status['shares']['count']

        # Counts of each reaction separately; good for sentiment
        # Only check for reactions if past date of implementation:
        # http://newsroom.fb.com/news/2016/02/reactions-now-available-globally/

        reactions = self.getReactionsForStatus(status_id, access_token) \
            if status_published > '2016-02-24 00:00:00' else {}

        num_likes = 0 if 'like' not in reactions else \
            reactions['like']['summary']['total_count']

        # Special case: Set number of Likes to Number of reactions for pre-reaction
        # statuses

        num_likes = num_reactions if status_published < '2016-02-24 00:00:00' else \
            num_likes

        def get_num_total_reactions(reaction_type, reactions):
            if reaction_type not in reactions:
                return 0
            else:
                return reactions[reaction_type]['summary']['total_count']

        num_loves = get_num_total_reactions('love', reactions)
        num_wows = get_num_total_reactions('wow', reactions)
        num_hahas = get_num_total_reactions('haha', reactions)
        num_sads = get_num_total_reactions('sad', reactions)
        num_angrys = get_num_total_reactions('angry', reactions)

        # return a tuple of all processed data

        return (status_id, status_message, status_author, link_name, status_type,
                permanent_link, status_published, num_reactions, num_comments,
                num_shares, num_likes, num_loves, num_wows, num_hahas, num_sads,
                num_angrys)

    def scrapeFacebookPageFeedStatus(self,group_id, access_token):
        directory = '/tmp/facebook_sentiment/%s_facebook_status.csv' % group_id
        with open(directory, 'wb') as file:
            w = csv.writer(file)
            w.writerow(["status_id", "status_message", "status_author",
                        "link_name", "status_type", "status_link",
                        "status_published", "num_reactions", "num_comments",
                        "num_shares", "num_likes", "num_loves", "num_wows",
                        "num_hahas", "num_sads", "num_angrys"])

            has_next_page = True
            num_processed = 0  # keep a count on how many we've processed
            scrape_starttime = datetime.datetime.now()

            print "Scraping %s Facebook Page: %s\n" % \
                  (group_id, scrape_starttime)

            statuses = self.getFacebookPageFeedData(group_id, access_token, 100)

            while has_next_page:
                for status in statuses['data']:

                    # Ensure it is a status with the expected metadata
                    if 'reactions' in status:
                        w.writerow(self.processFacebookPageFeedStatus(status, access_token))

                    # output progress occasionally to make sure code is not
                    # stalling
                    num_processed += 1
                    if num_processed % 100 == 0:
                        print "%s Statuses Processed: %s" % (num_processed,
                                                             datetime.datetime.now())

                # if there is no next page, we're done.
                if 'paging' in statuses.keys():
                    statuses = json.loads(self.request_until_succeed(statuses['paging']['next']))
                else:
                    has_next_page = False

            print "\nDone!\n%s Statuses Processed in %s" % \
                  (num_processed, datetime.datetime.now() - scrape_starttime)

def parse_args():
    parser = argparse.ArgumentParser(description='Download adjust data to BI')
    parser.add_argument('-d','--daysBack', help='how many days data you want until now, default is 5', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    fb_scrape = FbFeedScrape(args)
    fb_scrape.run()

if __name__ == '__main__':
    main()

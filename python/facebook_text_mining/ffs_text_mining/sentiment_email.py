import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.dates as mdates
import numpy as np
import datetime
pd.set_option('display.max_colwidth', -1)
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

'''select sentiment tables of which the negative sentiment > 0.1 '''
def split_date(row):
    return row['status_published'].split(' ')[0]
df_raw = pd.read_csv('/tmp/all_group_sentiment.csv')
df_raw['date'] = df_raw.apply(split_date,axis=1)
fb_sort = df_raw[['status_id','date','status_message','status_type','sentiment_neg','num_reactions','status_link']].sort_values(by = ['sentiment_neg'], ascending=False).reset_index()
fb_sort2 = fb_sort[fb_sort['sentiment_neg']>0.1]

'''Generate the chart'''
# generate moving averages
fb_eng_sent = df_raw[['sentiment_compound','date']].groupby('date').mean().reset_index()
fb_eng_ma_7d = fb_eng_sent.rolling(window=7,center=False).mean().fillna(0)
fb_eng_ma_14d = fb_eng_sent.rolling(window=14,center=False).mean().fillna(0)
d = {'sentiment': np.array(fb_eng_sent.sentiment_compound),
     'sentiment_7dma': np.array(fb_eng_ma_7d.sentiment_compound),
     'sentiment_14dma':np.array(fb_eng_ma_14d.sentiment_compound)}
df = pd.DataFrame(data=d, index=fb_eng_sent.set_index('date').index)
sns.set_style('darkgrid')
plt.figure(num=None, figsize=(20, 15), dpi=80)
df.plot()
axes = plt.gca()
axes.set_ylim([-1,1])
plt.xlabel('date')
plt.ylabel('sentiment value')
plt.title('Sentiment Across Time')
fn = '/tmp/analysis.png'
plt.savefig(fn)
plt.legend(loc='best')

'''Email funciton'''
sender = 'tableau_admin@funplus.com'
receiver = "jiehu.yu@funplus.com"
#smtpserver = 'email-smtp.us-west-2.amazonaws.com'
subject = 'FFS Facebook Sentiment Analysis Report {0}'.format((datetime.date.today()-datetime.timedelta(days=1)).strftime("%Y-%m-%d"))

msgRoot = MIMEMultipart('related')
msgRoot['From'] = sender
msgRoot['To'] = receiver
msgRoot['Subject'] = subject

msgText = MIMEText('<b>This is a test.</b><br><img src="cid:image1"><br><htm>'
                   + fb_sort2.to_html(classes='table',escape=False,index=False),
                   'html','utf-8')
msgRoot.attach(msgText)
fp = open('/tmp/analysis.png', 'rb')
msgImage = MIMEImage(fp.read())
fp.close()
msgImage.add_header('Content-ID', '<image1>')
msgRoot.attach(msgImage)

s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
s.sendmail(msgRoot['From'], [x.strip() for x in msgRoot['To'].split(",")], msgRoot.as_string())
s.quit()
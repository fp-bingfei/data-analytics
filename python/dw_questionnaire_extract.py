__author__ = 'jiating'

###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse
# 4. yum install boto

import boto.sqs
import psycopg2
import yaml
import argparse
import json
import datetime
import commands
import csv

# Usage:
# python dw_questionnaire_extract.py
# --conf=/Users/funplus/Documents/workspace/analytics/python/conf/custom-funplus.yaml
# --file=/Users/funplus/Documents/jiating/dragonwar/koa_questions.csv
# --table=dragonwar.player_questionnaire
class QuestionnaireExtract:
    def __init__(self, args):
        self.args = args
        print(datetime.datetime.now().strftime('%Y%m%d_%H%M%S') + " init is done \n ")

    def run(self):
        self.setDBParams()
        conn = self.getConnection()
        with open(self.args['file'], 'rb') as csvFile:
            # reader = csv.reader(csvFile, delimiter='\n')
            # for row in reader:
            #     print '\n'.join(row)
            reader = csv.DictReader(csvFile)
            date_dict = [row for row in reader]

        insert_sql = "insert into %s(date,funplus_id,player_name,question,answer) values" % self.args['table']
        insert_values = ""
        # print date_dict
        for row in date_dict:
            date_str = ''
            funplus_id = ''
            player_name = ''
            result_list = []

            for key, value in row.items():
                # print "dict[%s]=" % key, value
                if value == '':
                    continue
                if key == 'timestamp':
                    date_list = value.split(" ")
                    date_str = date_list[0].replace("/", "-")
                elif key == 'Player Name':
                    player_name = value
                elif key == 'FunPlus ID':
                    funplus_id = value
                else:
                    value_list = value.split(";")
                    for v in value_list:
                        dict = {}
                        dict["key"] = key
                        dict["value"] = v
                        result_list.append(dict)
            if funplus_id.isdigit():
                for d in result_list:
                    insert_values += "('%s', '%s', '%s', '%s', '%s')," % (
                        date_str, funplus_id, str(player_name).replace("'", """\\'"""),
                        str(d['key']).replace("'", """\\'"""),
                        str(d['value']).replace("'", """\\'"""))
        # print insert_values[:-1]
        self.execSql(conn, insert_sql + insert_values[:-1])

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
            self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def execSql(self, conn, sqlQuery):
        cursor = conn.cursor()
        cursor.execute(sqlQuery)
        print ("\nsql executed:" + sqlQuery)
        conn.commit()
        print ("\ncommit executed")

    def setDBParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']


def parse_args():
    parser = argparse.ArgumentParser(description='Handle message in SQS queue, copy to redshift')
    # parser.add_argument('-b', '--bucket', help='bucket string. e.g. s3://com.funplus.data-funplus/', required=True)
    # parser.add_argument('-q', '--sqs_queue_name', help='SQS queue name we want to handle', required=True)
    # /Users/funplus/Documents/workspace/analytics/python/conf/custom-funplus.yaml
    parser.add_argument('-f', '--file', help='data file', required=True)
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-t', '--table', help='table name in redshift', required=True)

    # parser.add_argument('-s', '--schema', help='DB schema name for specific table', required=False)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    handler = QuestionnaireExtract(args)
    handler.run()


if __name__ == '__main__':
    main()

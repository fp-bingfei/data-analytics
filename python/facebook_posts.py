import urllib2
import datetime
import time
import json
import collections

class FacebookPosts:
    def __init__(self):
        self.apps = ['1526052704380683', '209256072755630', '928643917165279', '308159869371679', '843964315647667',
            '737039859709497', '418582391520509', '225931867500395', '200942910003390', '365409093472457',
            '317180824986452', '262053390586780', '252340124853215', '231120903637672', '617904311583729',
            '357392490938552', '296247723822639', '480173892052608', '562157243794316', '405163009637002',
            '228207830651511', '454132378016398', '535493929807232', '313720965417486', '654608787886376',
            '187271604773962', '384296748343281', '1503546813190251', '1484657951840471', '221511944655378',
            '418107208278997', '324465824375200', '496231573836614', '217316998627741', '1781521478748822',
            '1470915916565194', '875555822515981', '985661654798211', '1644872099080413', '826324577431836',
            '918957474850980', '554786404548889', '546278088730267', '125058397663538', '149990781832240',
            '584293908258021', '571940576167659', '402436213188733', '990625624346027', '1593321087621585',
            '779071605541974', '1597521733896516', '164205810639904', '999935923410441', '501485990051465',
            '1076968169031385', '1653603358186132']
                    # Total 111 accounts, 3 of them are not valid anymore
                    # ['1781521478748822', '1076968169031385', '1237052822974654', '149107925273276', '512288065503830',
                    #     '1598145473809703', '223009818058664', '217316998627741', '1104502566248095', '1674471196137650',
                    #     '990625624346027', '1515782845396015', '951516744925761', '439377969602891', '1129423147076195',
                    #     '1652056151721986', '763709073772658', '160350187682779', '517987521721702', '1034821549911718',
                    #     '1520896378206065', '748408368626062', '1039114062807288', '308159869371679', '209256072755630',
                    #     '1666441210280373', '1526052704380683', '918957474850980', '1491040221198840', '1470915916565194',
                    #     '1484657951840471', '225931867500395', '1094352833910531', '1614753235460219', '645885125554812',
                    #     '1620574764892713', '1653603358186132', '850120705054246', '947712931959613', '376739152521317',
                    #     '875555822515981', '950837111604702', '1414640515521010', '400281643488408', '1593321087621585',
                    #     '856124924475883', '458419454334309', '1436971906598343', '899722016717182', '671644752964182',
                    #     '484523415032224', '680736332055864', '1644872099080413', '771944589563522', '100289063641243',
                    #     '1574647622792824', '454132378016398', '146065298878544', '554786404548889', '1555851311325114',
                    #     '187271604773962', '252340124853215', '654608787886376', '149990781832240', '562157243794316',
                    #     '617904311583729', '262053390586780', '584293908258021', '402436213188733', '418107208278997',
                    #     '221511944655378', '317180824986452', '496231573836614', '418582391520509', '480173892052608',
                    #     '430534780397752', '535493929807232', '313720965417486', '546278088730267', '125058397663538',
                    #     '571940576167659', '1483781788536698', '789834494430507', '1011228495558445', '324916637703083',
                    #     '489163017888140', '826324577431836', '296247723822639', '384296748343281', '1503546813190251',
                    #     '470303449738850', '228207830651511', '324465824375200', '405163009637002', '785967861448161',
                    #     '231120903637672', '357392490938552', '365409093472457', '1569733913256571', '200942910003390',
                    #     '928643917165279', '743994782300241', '199223433514943', '1619757391619824', '441799879341689',
                    #     '882570758446322', '1428151550807717', '1441000202791435']
                    #     '1105818746149066', '278506362489501', '295116950696964'
        self.httpApi = 'https://graph.facebook.com/v2.7/'
        self.access_token = 'EAADaw5aNczQBAId34ErabDnRbCT7MzwZAtkKsAXKObsq7mPnJgIzBAWsJkPy2sFXemZCOHNXe8aXk7hKliFtKhLImKslN7rJeZCrNrm4uZBTZAPMuQVZBRE5BK6gNVBYZBxElkwMFphyMAgOcBaEpmzYNzAZCkiU4TIZD'
        self.metrics = [('post_impressions_unique', 'lifetime'), ('post_impressions_paid_unique', 'lifetime'), ('post_impressions_organic_unique', 'lifetime'),
                            ('post_consumptions', 'lifetime'), ('post_consumptions_unique', 'lifetime'), ('post_negative_feedback_unique', 'lifetime'),
                            ('post_story_adds_by_action_type', 'lifetime'), ('post_story_adds_by_action_type_unique', 'lifetime'), ('post_reactions_by_type_total', 'lifetime')]
        self.start_ts = int(time.time()) - 40 * 24 * 3600
        print 'post start time for the today is :'
        print self.start_ts
        self.invalid_post = set()


    def run(self):
        stringBuilder = u'date|game|post_type|post_message|created_time|permalink|post_impressions|post_impressions_organic|post_impressions_paid|post_consumptions|post_consumptions_unique|post_negative_feeback|like|comment|share|like_unique|comment_unique|share_unique|emotion_like|emotion_love|emotion_wow|emotion_haha|emotion_sorry|emotion_anger\n'
        rowBuilder = u'%s|%s|%s|%s|%s|%s|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d\n'

        for app in self.apps:
            posts = self.getPosts(app)
            for post in posts:
                for metric in self.metrics:
                    post[metric[0]] = self.getMetric(post['id'], metric)
                
                rowValues = self.parseData(post)
                stringBuilder += rowBuilder % rowValues

        f = open('/tmp/facebook_posts.csv', 'w')
        f.write(stringBuilder.encode('utf8'))
        f.close()    


    def getPosts(self, app):
        url = self.httpApi + '%s/posts?access_token=%s' % (app, self.access_token)
        result = []

        try:
            res =  urllib2.urlopen(url)
            jsonRes = json.loads(res.read())
            result += jsonRes["data"]
            until = int(urllib2.urlparse.parse_qs(jsonRes["paging"]["next"].split("?")[1])["until"][0])
            while (until > self.start_ts):
                res = urllib2.urlopen(jsonRes["paging"]["next"])
                jsonRes = json.loads(res.read())
                if (len(jsonRes["data"]) == 0 or not jsonRes.has_key("paging")):
                    break
                result += jsonRes["data"]
                until = int(urllib2.urlparse.parse_qs(jsonRes["paging"]["next"].split("?")[1])["until"][0])
                print until

        except Exception, e:
            print e
            print app

        return result


    def getMetric(self, postId, metric):
        url = self.httpApi + '%s/insights/%s/%s?access_token=%s' % (postId, metric[0], metric[1], self.access_token)
        try:
            res =  urllib2.urlopen(url)
            jsonRes = json.loads(res.read())

            return jsonRes["data"][0]
        except Exception, e:
            print e
            print postId + metric[0]
            self.invalid_post.add(postId)
            return -1


    def parseData(self, postJson):
        #tmpRes = {app1: [{post1}, {post2}, ..], app2: [{}, {},...], ...} 

        cur_date = datetime.datetime.today().strftime('%Y-%m-%d')
        postId = postJson['id']
        post_type = 'Unknown'
        post_message = postJson.get('message', '').split('\n')[0]
        created_time = postJson['created_time'].replace('T', ' ').split('+')[0]
        permalink = 'https://www.facebook.com/%s/posts/%s' % tuple(postId.split('_'))

        if postId in self.invalid_post:
            return (cur_date, postId.split('_')[0], post_type, post_message, created_time, permalink, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

        post_impressions = postJson['post_impressions_unique']['values'][0]['value']
        post_impressions_organic = postJson['post_impressions_organic_unique']['values'][0]['value']
        post_impressions_paid = postJson['post_impressions_paid_unique']['values'][0]['value']
        post_consumptions = postJson['post_consumptions']['values'][0]['value']
        post_consumptions_unique = postJson['post_consumptions_unique']['values'][0]['value']
        post_negative_feeback = postJson['post_negative_feedback_unique']['values'][0]['value']
        like = postJson['post_story_adds_by_action_type']['values'][0]['value'].get('like', 0)
        comment = postJson['post_story_adds_by_action_type']['values'][0]['value'].get('comment', 0)
        share = postJson['post_story_adds_by_action_type']['values'][0]['value'].get('share', 0)
        like_unique = postJson['post_story_adds_by_action_type_unique']['values'][0]['value'].get('like', 0)
        comment_unique = postJson['post_story_adds_by_action_type_unique']['values'][0]['value'].get('comment', 0)
        share_unique = postJson['post_story_adds_by_action_type_unique']['values'][0]['value'].get('share', 0)
        emotion_like = postJson['post_reactions_by_type_total']['values'][0]['value']['like']
        emotion_love = postJson['post_reactions_by_type_total']['values'][0]['value']['love']
        emotion_wow = postJson['post_reactions_by_type_total']['values'][0]['value']['wow']
        emotion_haha = postJson['post_reactions_by_type_total']['values'][0]['value']['haha']
        emotion_sorry = postJson['post_reactions_by_type_total']['values'][0]['value']['sorry']
        emotion_anger = postJson['post_reactions_by_type_total']['values'][0]['value']['anger']

        return (cur_date, postId.split('_')[0], post_type, post_message, created_time, permalink, post_impressions, post_impressions_organic, post_impressions_paid, post_consumptions,
            post_consumptions_unique, post_negative_feeback, like, comment, share, like_unique, comment_unique, share_unique, emotion_like, emotion_love, emotion_wow,
            emotion_haha, emotion_sorry, emotion_anger)


def main():
    fp = FacebookPosts()
    fp.run()

if __name__ == '__main__':
    main()
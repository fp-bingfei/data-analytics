import argparse
import os
from string import Template
from datetime import date, timedelta
from pyhocon import ConfigFactory

def main(args):
    header = """
    USE farm_1_1;

    SET hive.exec.dynamic.partition.mode=nonstrict;
    SET parquet.compression=SNAPPY;
    SET parquet.enable.dictionary=true;
    SET parquet.page.size=1048576;
    SET parquet.block.size=134217728;
    SET mapred.max.split.size=134217728;
    SET hive.exec.max.dynamic.partitions=50000;
    SET hive.exec.max.dynamic.partitions.pernode=5000;
    SET mapred.map.tasks.speculative.execution=false;
    SET mapred.reduce.tasks.speculative.execution=false;
    SET hive.stats.autogather=false;
    """

    table = Template("""
    -- Raw events daily
    CREATE EXTERNAL TABLE IF NOT EXISTS raw_${event}_daily (
        data_version string,
        app_id string,
        ts string,
        ts_pretty string,
        event string,
        user_id string,
        session_id string,
        properties map<
            string, string
        >
    )
    PARTITIONED BY (
      app string,
      dt string)
    ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' COLLECTION ITEMS TERMINATED BY ',' MAP KEYS TERMINATED BY ':'
    STORED AS PARQUET
    LOCATION 's3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_${event}_daily/'
    TBLPROPERTIES (
      'serialization.null.format'=''
    );
    """)

    partition = Template("    ALTER TABLE raw_${event}_daily ADD IF NOT EXISTS PARTITION (app='${app}', dt='${dt}');")

    footer = """
    EXIT;
    """

    f = open('raw_parquet_daily.hql', 'w')
    print >> f, header

    conf = ConfigFactory.parse_file(args['config'])
    events = conf.get('events')
    split_startdate = args['startdate'].split('-')
    startdate = date(int(split_startdate[0]), int(split_startdate[1]), int(split_startdate[2]))
    if args['enddate'] != None:
        split_enddate = args['enddate'].split('-')
        enddate = date(int(split_enddate[0]), int(split_enddate[1]), int(split_enddate[2]))
    else:
        enddate = startdate + timedelta(days=1)
    apps = ['farm.br.prod', 'farm.de.prod', 'farm.fr.prod', 'farm.it.prod', 'farm.nl.prod', 'farm.pl.prod', 'farm.th.prod', 'farm.tw.prod', 'farm.us.prod']

    # tables
    for event in events:
        print >> f, table.substitute(event=event)

    # partitions
    for event in events:
        daycount = 1
        curdate = startdate
        while curdate < enddate:
            curyear = curdate.year
            curmonth = curdate.month
            if curmonth < 10:
                curmonth = '0%d' %curmonth
            curday = curdate.day
            if curday < 10:
                curday = '0%d' %curday
            dt = '%s-%s-%s' %(curyear, curmonth, curday)
            for curapp in apps:
                print >> f, partition.substitute(event=event, app=curapp, dt=dt)
            curdate = startdate + timedelta(days=daycount)
            daycount += 1

    print >> f, footer
    f.close()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help = 'input the path of config file', required=True)
    parser.add_argument('-s', '--startdate', help = 'input the start date of processing', required=True)
    parser.add_argument('-e', '--enddate', help = 'input the end date of processing', required=False)
    args = vars(parser.parse_args())
    return args

if __name__ == "__main__":
    args = parse_args()
    main(args)


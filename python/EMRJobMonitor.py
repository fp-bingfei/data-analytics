import subprocess
import argparse
import urllib
import smtplib
import json
import time
import datetime
import sys
from html import HTML
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class EMRJobMonitor(object):
    def __init__(self, args):
        self.url = "http://" + args["dns"] + ":8088/ws/v1/cluster/apps?states=" + args["stats"] 
        self.threshold_hour = float(args["hour"])


        
    def run(self):
        if len(self.getApplicationList()) > 0:
            application = self.getMinID(self.getApplicationList())
            self.killApplication(application["id"], application)
        else:
            print datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + " no hanging jobs"
            
    def genHtml(self,application):
        htmlobj = HTML()
        htmltable = htmlobj.p('Below application has been killed:')
        htmltable.table(border='1')
        htmltableheader = htmltable.tr
        htmltableheader.th('ID')
        htmltableheader.th('Name')
        htmltableheader.th('StartTime')
        htmltableheader.th('ElapsedTime(hour)')
        htmltableheader.th('TrackingUI')
        starttime = datetime.datetime.fromtimestamp(int(application["startedTime"]/1000)).strftime('%Y-%m-%d %H:%M:%S')
        elapsedtime = str(long(application["elapsedTime"])*1.00/(1000*60*60))
        html_row = htmltable.tr
        html_row.td(application["id"])
        html_row.td(application["name"])
        html_row.td(starttime)
        html_row.td(elapsedtime)
        html_row.td(application["trackingUrl"])
        return htmltable
            
    def getMinID(self, appList):
        minIndex = 0
        minApp = appList[0]["id"]
        for index in range(len(appList)):
            if appList[index]["id"] < minApp:
                minApp = appList[index]["id"]
                min_index = index
        return appList[minIndex]
            
    
    def getApplicationList(self):
        applicationList = []
        appJsonStr = urllib.urlopen(self.url).read()
        try:
            appJson = json.loads(appJsonStr)
        except Exception, e:
            print 'Json string error' + appJson
        if appJson["apps"] is not None:
            for appInfo in appJson["apps"]["app"]:
                stime = int(appInfo["startedTime"])
                if 'hourly' in appInfo["name"] and self.getElapsedTime(stime) > 0.75: 
                    applicationList.append(appInfo)
                elif self.getElapsedTime(stime) > self.threshold_hour:
                    applicationList.append(appInfo)
        return applicationList
            
    def killApplication(self, appId, application):
        cmd = "yarn application -kill " + appId
        try:
            subprocess.call(cmd, shell=True)
            self.sendMail(str(self.genHtml(application)))
            print cmd + " Success"
        except Exception, e:
            print cmd + " Failed" 
        
        
    def getElapsedTime(self, stime):
        ctime = int(round(time.time()*1000))
        return (ctime - stime)*1.00/3600000
    
    def sendMail(self,htmlcode):
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmlcode,'html')
        msg.attach(html)
        msg['Subject'] = 'Auto killed Hanging Job List ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "EMR JOB Monitor<tableau_admin@funplus.com>"
        msg['To'] = 'chun.zeng@funplus.com'
        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
            continue    
        

     
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dns', help = 'input the emr public dns address', required=True)
    parser.add_argument('-s', '--stats', help = 'input the application states, specified as a comma-separated list', required=True)
    parser.add_argument('-hour', '--hour', default = 0.5, help = 'input the application threshold of running hour', required=True)
    args = vars(parser.parse_args())
    return args
def main():
    argsMap = parse_args()
    ejm = EMRJobMonitor(argsMap)
    ejm.run()

if __name__ == '__main__':
    main()

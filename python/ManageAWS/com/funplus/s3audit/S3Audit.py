__author__ = 'praveenak'

# Final S3Audit Script
import subprocess
from boto.s3.connection import S3Connection
import logging

AWS_KEY = 'AKIAJRKDNC52OAINDWKA'
AWS_SECRET = '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'

aws_connection = S3Connection(AWS_KEY, AWS_SECRET)
logging.basicConfig(filename="s3_bucket_audit.log", level=logging.INFO)

query = "s3cmd ls | awk '{print $3}'"
bucketsP = subprocess.Popen(query, stdout=subprocess.PIPE, shell=True)

(buckets, err) = bucketsP.communicate()

#print buckets

bucketsList = buckets.split("\n")
#bucketsList = ["s3://com.funplus.bitest","s3://patrick_test000"]

for bucket in bucketsList:
    if (bucket):
        query = "s3cmd ls " + bucket + "| grep DIR | awk '{print $2}'"

        if bucket.startswith('s3://'):
            bucket = bucket[len('s3://'):]

        logging.info("[+] In Bucket: "+ bucket)

        s3bucket = aws_connection.get_bucket(bucket)

        bucketObjectsP = subprocess.Popen(query, stdout=subprocess.PIPE, shell=True)

        (bucketObjects, err) = bucketObjectsP.communicate()


        bucketObjectsList = bucketObjects.split("\n")

        bucketSize = 0
        bucketCnt = 0
        for object in bucketObjectsList:
            if(object):
                if object.startswith('s3://'):
                     object = object[len('s3://'):]
                if object.startswith(bucket):
                    object = object[len(bucket):].lstrip("/")
                logging.info("[+] For Object: "+object)

                objectSize = 0
                objectCnt = 0

                for key in s3bucket.list(prefix=object):
                    objectSize += key.size
                    objectCnt = objectCnt + 1

                objectSize = objectSize*1.0/1024/1024/1024
                #print "%.3f GB" % (objectSize*1.0/1024/1024/1024)
                logging.info("[+] Total Size of Object (in GB):"+object+" in Bucket: "+bucket+" is:"+ str(objectSize))
                logging.info("[+] No. of Objects in Object:"+object+" in Bucket: "+bucket+" is:"+str(objectCnt))

                bucketSize += objectSize
                bucketCnt += objectCnt

        logging.info("[+] Total Size of Bucket (in GB): "+bucket+" is:"+ str(bucketSize))
        logging.info("[+] No. of Objects in Bucket: "+bucket+" is:"+ str(bucketCnt))
        logging.info("[+] Finished processing Bucket: "+bucket)
__author__ = 'praveenak'
import re
import pprint
reportdict = {}

lines = [line[len('INFO:root:[+] '):] for line in open('s3_bucket_audit.log')]

for line in lines:
    if (line.startswith("In Bucket:")):
        bucket = line[len('In Bucket: '):].lstrip().rstrip()
        #print "Bucket Name: ",bucket
        reportdict[bucket] = []

    elif (line.startswith("For Object:")):
        object = line[len('For Object: '):].lstrip().rstrip()
        objectDict = {}
        objectDict['objectName'] = object
        #print "Object Name: ",line[len('For Object: '):].lstrip().rstrip()
    elif (line.startswith("Total Size of Object")):
        matchObj = re.match( r'Total Size of Object .* in Bucket: (.*) is:(.*)', line, re.M|re.I)
        #print "Bucket Name: ", matchObj.group(1)
        objectSize = matchObj.group(2).lstrip().rstrip()
        objectDict['objectSize'] = objectSize
        #print "Object Size : ", objectSize
    elif (line.startswith("No. of Objects in Object")):
        matchObj = re.match( r'No. of Objects in Object.*is:(.*)', line, re.M|re.I)
        objectCnt = matchObj.group(1)
        objectDict['objectCnt'] = objectCnt
        #print "Object cnt : ", objectCnt
        reportdict[bucket].append(objectDict)
    elif (line.startswith("No. of Objects in Bucket")):
        continue
    elif (line.startswith("Total Size of Bucket")):
        continue
    else:
        #print line
        continue

#pprint.pprint(reportdict,width=1)

for bucket in reportdict.keys():
    #print bucket
    for objdict in reportdict[bucket]:
        print bucket,'\t', objdict['objectName'].rstrip('/'),'\t',objdict['objectSize'],'\t',objdict['objectCnt']
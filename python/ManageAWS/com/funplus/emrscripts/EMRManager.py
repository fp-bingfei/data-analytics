__author__ = 'praveenak'

import yaml
import argparse
import MySQLdb as mdb
import time
import logging
import boto3
from boto.emr.connection import EmrConnection
from boto.emr.bootstrap_action import BootstrapAction
from boto.emr.step import InstallHiveStep
from boto.regioninfo import RegionInfo
from boto.emr.step import JarStep
from boto.emr.instance_group import InstanceGroup
import sys

"""
create-cluster -c=/Users/balaji/funplus/code/analytics/python/conf/farm_1_2.yaml -name=emr-farm-scripts
terminate-cluster -c=/Users/praveenak/funplus/analytics/python/conf/emrcluster.yaml -name=emr-farm-scripts

In MySQL:
status in mysql - 0 (Cluster Creating), 1(Cluster Waiting for Steps), -1(Cluster Terminated)
"""

class EMRManager:
    def __init__(self, args, **confMap):
        self.args = args
        self.confMap = confMap
        self.zone_name =  confMap['tasks']['emr']['region_name']
        self.ec2_keyname = confMap['tasks']['emr']['ec2_keyname']
        self.log_uri = confMap['tasks']['emr']['log-uri']
        self.ami_version = confMap['tasks']['emr']['ami-version']
        self.auto_terminate = confMap['tasks']['emr']['auto-terminate']
        self.emr_status_wait = 20
        self.releaseLabel = confMap['tasks']['emr']['releaseLabel']
        self.mysqlconn = None
        self.emrconn = None
        self.curr = None
        self.step_emr4 = confMap['tasks']['emr']['step_emr4']
        self.application = confMap['tasks']['emr']['application']
        self.instance = confMap['tasks']['emr']['instance']
        self.instance_on_demand = confMap['tasks']['emr']['instance_on_demand']

        self.cluster_name = args['cluster_name']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']

        self.instance_groups = []
        self.bootstrap_actions = []
        self.steps = []

    def setDBParams(self, **db_props):
        if (self.confMap['tasks'].has_key('db-props')):
            self.db_host = db_props['db_host']
            self.db_port = db_props['db_port']
            self.db_name = db_props['db_name']
            self.db_username = db_props['db_username']
            self.db_password = db_props['db_password']
            #logging.INFO("[+] Set DB properties: "+self.db_host+str(self.db_port)+ self.db_name+ self.db_username+ self.db_password)

    def setupInstanceGroups(self,**instanceMap):
        for role, igdict in instanceMap.iteritems():
            self.instance_groups.append(InstanceGroup(**igdict))

    def setupBA(self,**BAMap):
        for name, BAdict in BAMap.iteritems():
            if not BAdict.has_key('bootstrap_action_args'):
                 BAdict['bootstrap_action_args'] = ''
            self.bootstrap_actions.append(BootstrapAction(**BAdict))

    def setupSteps(self,**StepsMap):
        hive_install_step = InstallHiveStep();
        self.steps.append(hive_install_step)
        for name, Stepdict in StepsMap.iteritems():
            self.steps.append(JarStep(**Stepdict))

    def getDBConnection(self):
        try:
            #conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host,self.db_port,self.db_name,self.db_username,self.db_password)
            self.mysqlconn = mdb.connect(self.db_host, self.db_username, self.db_password, self.db_name)
            self.cur = self.mysqlconn.cursor()
        except Exception, e:
            logging.error("[-] MySQL DB connection failed with error: "+str(e))
            sys.exit(1)
        return self.mysqlconn

    def getEMRConnection(self):
        try:
            self.emrconn = EmrConnection(self.aws_access_key_id, self.aws_secret_access_key,
                                 region=RegionInfo(name=self.zone_name,
                                 endpoint=self.zone_name + '.elasticmapreduce.amazonaws.com'))

            logging.info("[+] EMR Connection established")
        except Exception, e:
            logging.error("[-] unable to establish EMR Connection with error: "+str(e))
            sys.exit(1)

    def isCreateNewCluster(self, cluster_name):
        try:
            query = "select cluster_status from cluster_info where cluster_name=\""+cluster_name+ "\" and date(record_ts)= date(now()) order by record_ts desc limit 1"
            logging.info("Method: isCreateNewCluster - Executing query:"+query)
            self.cur.execute(query)
            row=self.cur.fetchone()
            if row:
                if (row[0] == 1):
                    logging.info("[+] Cluster with same name is created today and is Running. So no new cluster is required.")
                    return False
            return True
        except Exception, e:
            logging.error("[-] Checking cluster status failed with error: "+str(e))
            sys.exit(1)

    def getClusterDNSName(self, cluster_id):
        master_dns = self.emrconn.describe_jobflow(cluster_id).masterpublicdnsname
        return master_dns

    def terminate_emr_cluster(self):
        try:
            cluster_id = self.get_cluster_id(self.cluster_name)
            logging.info("Cluster_id is:"+cluster_id)

            if cluster_id:
                logging.info("=== Terminating cluster with name "+str(self.cluster_name)+ " and job-flow id "+str(cluster_id))
                self.emrconn.set_termination_protection(cluster_id, False)
                self.emrconn.terminate_jobflow(self.get_cluster_id(self.cluster_name))
                self.insert_to_db(self.cluster_name, cluster_id, '', -1 )
                logging.info("[+] EMR Cluster with jobflow id " + str(cluster_id) +" is Terminated")
                return cluster_id
            else:
                logging.info("[-] No Cluster exists with the cluster-name "+str(self.cluster_name)+ " provided by you to terminate")
        except Exception, e:
            logging.error("[-] unable to terminate EMR Cluster with error "+str(e))
            sys.exit(1)
    
    #this function is in order to get cluster id by using boto3, since emr4.x can not support ami version at all
    def getEMRClient(self):
        try:
            self.session = boto3.session.Session(aws_access_key_id=self.aws_access_key_id, aws_secret_access_key=self.aws_secret_access_key, 
                             region_name=self.zone_name)
            self.emrclient = self.session.client('emr')
            logging.info("[+] EMRclient established")
        except Exception, e:
            logging.error("[-] unable to establish EMR Connection with error: " + str(e))
            sys.exit(1)
        
        
    def launch_emr_cluster(self):
        try:
            isCreateCluster = self.isCreateNewCluster(self.cluster_name)
            logging.info("[+] Is Create New Cluster ?:"+str(isCreateCluster))
            if isCreateCluster:
                #Launching the cluster
                resource = self.emrclient.run_job_flow(Name=self.cluster_name,
                                                       LogUri=self.log_uri,
                                                       ReleaseLabel=self.releaseLabel,
                                                       Instances=self.instance,
                                                       Steps=self.step_emr4,
                                                       VisibleToAllUsers=True,
                                                       JobFlowRole='EMR_EC2_DefaultRole',
                                                       ServiceRole='EMR_DefaultRole',
                                                       Applications=self.application
                                                       )
                cluster_id = resource[u'JobFlowId']
                
                #cluster_id = self.emrconn.run_jobflow(
                             #name=self.cluster_name,
                             #bootstrap_actions=self.bootstrap_actions,
                             #ec2_keyname=self.ec2_keyname,
                             #steps=self.steps,
                             #keep_alive=True,
                             #action_on_failure = 'CANCEL_AND_WAIT',
                             #instance_groups=self.instance_groups,
                             #job_flow_role="EMR_EC2_DefaultRole",
                             #service_role="EMR_DefaultRole",
                             #ami_version=self.ami_version,
                             #visible_to_all_users=True,
                             #enable_debugging=True,
                             #log_uri=self.log_uri)

                logging.info("[+] Starting cluster: "+ cluster_id)
                self.insert_to_db(self.cluster_name, cluster_id, '', 0)

                #Enabling the termination protection
                self.emrconn.set_termination_protection(cluster_id, True)

                #Checking the state of EMR cluster
                wait_time = 0
                state = self.emrconn.describe_jobflow(cluster_id).state
                while state != u'COMPLETED' and state != u'SHUTTING_DOWN' and state != u'FAILED' and state != u'WAITING':
                    #sleeping to recheck for status.
                    time.sleep(int(self.emr_status_wait))
                    wait_time += int(self.emr_status_wait)
                    state = self.emrconn.describe_jobflow(cluster_id).state
                    # waste more than 1 hour
                    if wait_time > 3600:
                        # terminate current cluster
                        self.terminate_emr_cluster()
                        # try to use on_demand instances
                        resource = self.emrclient.run_job_flow(Name=self.cluster_name,
                                                       LogUri=self.log_uri,
                                                       ReleaseLabel=self.releaseLabel,
                                                       Instances=self.instance_on_demand,
                                                       Steps=self.step_emr4,
                                                       VisibleToAllUsers=True,
                                                       JobFlowRole='EMR_EC2_DefaultRole',
                                                       ServiceRole='EMR_DefaultRole',
                                                       Applications=self.application
                                                       )
                        cluster_id = resource[u'JobFlowId']
                        logging.info("[+] Starting cluster: "+ cluster_id)
                        self.insert_to_db(self.cluster_name, cluster_id, '', 0)
                        #Enabling the termination protection
                        self.emrconn.set_termination_protection(cluster_id, True)
                        # reset wait_time
                        wait_time = 0
                        state = self.emrconn.describe_jobflow(cluster_id).state

                if state == u'SHUTTING_DOWN' or state == u'FAILED':
                    logging.error("[-] Launching EMR cluster failed")
                    return "ERROR"

                #Check if the state is WAITING. Then we can launch the next steps
                if state == u'WAITING':
                    #Finding the master node dns of EMR cluster
                    self.master_dns = self.emrconn.describe_jobflow(cluster_id).masterpublicdnsname
                    logging.info("[+] Launched EMR Cluster Successfully with cluster_id:"+ str(cluster_id))
                    logging.info("[+] Master node DNS of EMR " + self.master_dns)
                    self.insert_to_db(self.cluster_name, cluster_id, self.master_dns, 1 )
                    return self.master_dns
            else:
                return self.getClusterDNSName(self.get_cluster_id(self.cluster_name))
        except Exception, e:
            logging.error("[-] Exception while creating cluster: "+str(e))
            return "FAILED"

    def insert_to_db(self, cluster_name, cluster_id, cluster_dns, cluster_status=-1):
        try:
            self.cur = self.mysqlconn.cursor()
            query = "insert into cluster_info(cluster_id, cluster_name, cluster_dns_name, cluster_status) values (\"" + cluster_id + "\",\"" + cluster_name + "\",\"" + cluster_dns + "\"," + str(cluster_status) +")";
            logging.info("===Insert query is:"+query)
            self.cur.execute(query)
            self.mysqlconn.commit()
        except Exception, e:
            logging.error("[-] unable to insert to DB with error: "+str(e))

    def get_cluster_id(self, cluster_name):
        self.cur = self.mysqlconn.cursor()
        query = "select cluster_id from cluster_info where cluster_name=\""+cluster_name+"\" order by record_ts desc limit 1";
        logging.info("===Get ClusterId query is:"+query)
        self.cur.execute(query)
        row=self.cur.fetchone()
        if row:
            if row[0]:
                return row[0]
        else:
            None


def parse_args():
    logging.info("Parsing command-line arguments")
    parser = argparse.ArgumentParser(description='Creates and Shutdowns EMR Cluster from cluster config file')
    parser.add_argument('action', help='Action to perform - create-cluster [OR] terminate-cluster')
    parser.add_argument('-c','--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-name','--cluster-name', help='Name of the emr cluster - e.g. emr-farm', required=True)
    parser.add_argument('-id','--cluster-id', help='Id of the emr cluster - e.g. j-2GC7N0B29F0LO', required=False)
    args = vars(parser.parse_args())
    return args

def loadConfFile(args):
    logging.info("Parsing yaml conf file")
    f = open(args['conf'], "r")
    confMap = yaml.safe_load(f)
    f.close()
    return confMap

def main():
    logging.basicConfig(filename="emr_cluster_management.log", level=logging.INFO)

    argsMap = parse_args()
    confMap = loadConfFile(argsMap)
    c2r = EMRManager(argsMap, **confMap)
    db_props = confMap['tasks']['db-props']
    c2r.setDBParams(**db_props)
    c2r.getDBConnection()
    if(argsMap['action'] == 'create-cluster'):
        c2r.getEMRConnection()
        c2r.getEMRClient()
        ig_props = confMap['tasks']['emr']['instancegroups']
        c2r.setupInstanceGroups(**ig_props)
        ba_props = confMap['tasks']['emr']['bootstrap-actions']
        steps_props = confMap['tasks']['emr']['steps']
        c2r.setupBA(**ba_props)
        c2r.setupSteps(**steps_props)
        print c2r.launch_emr_cluster()

    if(argsMap['action'] == 'terminate-cluster'):
        c2r.getEMRConnection()
        return c2r.terminate_emr_cluster()

if __name__ == '__main__':
    main()
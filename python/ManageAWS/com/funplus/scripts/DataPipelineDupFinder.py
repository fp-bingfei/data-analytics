__author__ = 'praveenak'

from boto.datapipeline import connect_to_region
from boto.datapipeline.layer1 import DataPipelineConnection
from boto.regioninfo import RegionInfo
import time

RegionEndpoint = 'datapipeline.us-west-2.amazonaws.com'
RegionName="us-west-2"
kw_params={}

# Connect to a region
conn = connect_to_region(RegionName, **kw_params)
region = RegionInfo(RegionName,RegionEndpoint)
region.endpoint=region.name

kw_params['region'] = region
kw_params['host'] = "datapipeline.us-west-2.amazonaws.com"

kw_params['aws_access_key_id'] = 'AKIAJRKDNC52OAINDWKA'
kw_params['aws_secret_access_key'] = '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'

# Establish data pipeline service connection to the above connected region
dp_connection = DataPipelineConnection(**kw_params)

# Get list of pipelines
results=[]
has_more_results,marker = True, None

while has_more_results:
    time.sleep(1)
    response = dp_connection.list_pipelines(marker)
    has_more_results=response['hasMoreResults']
    marker= has_more_results and response['marker'] or None
    results.extend(response['pipelineIdList'])

print "Total no. of pipelines:",len(results)

pipelines={}

# prepare dict of pipelineId and pipelineName
for pipeline in results:
    pipelines[pipeline['id']] = pipeline['name']

print pipelines

#piplineobjectsmapping = {}
duppipelines = {}

# Create Activity Id and list of pipelines with same Activity Id mapping
for pipelineId in pipelines.keys():
    time.sleep(0.25)
    pipelineObjects = dp_connection.get_pipeline_definition(pipelineId)['pipelineObjects']
    #piplineobjectsmapping[pipelineId] = []
    #print pipelineObjects
    for pipelineObject in pipelineObjects:
        activityId = pipelineObject['id']
        if activityId.startswith('ActivityId'):
            #piplineobjectsmapping[pipelineId].append(activityId)
            if activityId in duppipelines.keys():
                duppipelines[activityId].append(pipelineId)
            else:
                duppipelines[activityId] = [pipelineId]

# duppipelines.values() is a list of lists
#print duppipelines.values()

deduppipelines_set = set(tuple(x) for x in duppipelines.values())
deduppipelines = [ list(x) for x in deduppipelines_set ]


#dupipelines = list(set(duppipelines.values())) # Remove duplicate values in the dupipelines map

#print duppipelines

#Remove lists with dup pipelines in the list to not consider duplicate piplines because the dups are inactive
for plist in deduppipelines:
    if len(plist) >= 2:
        namelist = [pipelines[id] for id in plist]
        #print namelist
        deduped_namelist = list(set(namelist))
        if len(deduped_namelist) > 1: #print only if the pipeline in the pipeline groups are not same
            print deduped_namelist
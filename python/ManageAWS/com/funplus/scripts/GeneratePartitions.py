###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-argparse

from subprocess import Popen, PIPE
import argparse
import yaml
import datetime

"""

python GeneratePartitons --conf=<path-to-yaml-file> --rpt_date=2015-01-01
OR
python GeneratePartitions --conf=<path-to-yaml-file> -g=farm_1_1 --app=all --tables=default --rpt_date=2015-01-01 --outfile=/tmp/events-hourly-partitions.txt

python GeneratePartitions -c /mnt/funplus/analytics/python/conf/farm.yaml -r 2015-01-01 -a report-apps -o /mnt/scratch/events-hourly-partition/${}-partitions.txt -t events-default -n true
"""
class GeneratePartitions:

    def __init__(self, args, **confMap):
        self.game = args.get('game') or confMap.get('game')
        self.rpt_date = args['rpt_date']
        self.apps = confMap.get(args['app']) or confMap.get('report-apps')
        self.tblList = confMap.get(args['tables'])
        self.outfile = args.get('outfile') or confMap.get('outfile')
        self.includeNextDayPartition = args.get('include_rpt_date_plus_1')


    def run(self):
        if not self.tblList:
            query = "s3cmd ls s3://com.funplus.datawarehouse/%s/events/ | awk -F'/' '{print $6}'" % self.game
            eventsP = Popen(query, stdout=PIPE, shell=True)
            (events, err) = eventsP.communicate()
            eventsList = events.split("\n")
            self.tblList = ["raw_"+event.lower() for event in eventsList if event != '']

            # include partition for next day in case of overlap
        includeOnly0hour = "true"
        rptDates = []
        rptDates.append(self.rpt_date)
        if self.includeNextDayPartition:
            rpt_datetime = datetime.datetime.strptime(self.rpt_date, "%Y-%m-%d")
            rpt_datetime_plus_1 = rpt_datetime + datetime.timedelta(days=1)
            rptDates.append(rpt_datetime_plus_1.strftime("%Y-%m-%d"))

        #Create and write file or overwrite existing file
        with open(self.outfile, 'w+') as outfile:
            outfile.write("use %s;\n" % (self.game))
            for app in self.apps:
                for table in self.tblList:
                    for rptDate in rptDates:
                        (year,month,day) = rptDate.split("-")
                        outfile.write("\n")
                        outfile.write("ALTER TABLE %s DROP IF EXISTS PARTITION (app='%s',year=%s,month=%s,day=%s);\n" % (table,app,year,month,day))
                        outfile.write("\n")
                        for i in range(24):
                            if not (i != 0 and includeOnly0hour and rptDate != self.rpt_date):
                                outfile.write("ALTER TABLE %s ADD IF NOT EXISTS PARTITION (app='%s',year=%s,month=%s,day=%s,hour=%.2d);\n" % (table,app,year,month,day,i))

def parse_args():
    parser = argparse.ArgumentParser(description='Generate Partitions for Events Hourly Table')
    parser.add_argument('-c','--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-g','--game', help='game e.g. --game=farm_1_1', required=False)
    parser.add_argument('-a','--app', help='--app=all or --app=fb-apps or --app=report-apps', required=False)
    parser.add_argument('-r','--rpt_date', help='Report Date e.g. --rpt_date=2015-01-01', required=True)
    parser.add_argument('-t','--tables', help='e.g. --table=default', required=False)
    parser.add_argument('-o','--outfile', help='Output File Path', required=False)
    parser.add_argument('-n','--include_rpt_date_plus_1', help='Include rpt_date + 1', default='false', required=False)
    args = vars(parser.parse_args())
    return args

def load_conf_file(args):
    f = open(args['conf'], "r")
    confMap = yaml.safe_load(f)
    f.close()
    return confMap

def main():
    argsMap = parse_args()
    confMap = load_conf_file(argsMap)
    gp = GeneratePartitions(argsMap,**confMap)
    gp.run()

if __name__ == '__main__':
    main()
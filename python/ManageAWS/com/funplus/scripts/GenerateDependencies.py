__author__ = 'praveenak'

import argparse
import yaml

"""
allreports_${GAME_ID}_${VERSION}    events_hourly_${APP}_${VERSION}_${HOUR}
job=allreports_${GAME_ID}_${VERSION} dependency=events_hourly_${APP}_${VERSION}_${HOUR} g=farm v=1.1
e.g. python GenerateDependencies --conf=/Users/praveenak/PycharmProjects/ManageAWS/com/funplus/s3audit/generate-dependencies.yaml --game=farm --version=1.1 --app=report-apps --outfile=/Users/praveenak/Desktop/events-hourly-dependencies.sql
"""
class GenerateDependencies:

    def __init__(self, args, **confMap):
        self.game = args.get('game') or confMap.get('game')
        self.version='1.1'
        self.app='farm.us.prod'
        job = "allreports_${GAME_ID}_${VERSION}"
        self.job_id = job.replace("${GAME_ID}",self.game).replace("${VERSION}",self.version)
        self.dependency = "events_hourly_${APP}_${VERSION}_${HOUR}"
        self.dependency = self.dependency.replace("${VERSION}",self.version)
        self.apps=confMap.get(args['app']) or confMap.get('report-apps')
        self.outfile = args.get('outfile') or confMap.get('outfile')

    def run(self):
        values=[]
        for app in self.apps:
            for hour in range(24):
                dependency_id = self.dependency.replace("${APP}", app).replace("${HOUR}", str(hour).zfill(2))
                values.append((self.job_id, dependency_id))

        with open(self.outfile, 'w+') as outfile: #Create and write file or overwrite existing file
            for key,value in values:
                outfile.write("insert into common.job_dependency values (\"" + key + "\",\"" + value + "\");\n")

def parse_args():
    parser = argparse.ArgumentParser(description='Generate Partitions for Events Hourly Table')
    parser.add_argument('-c','--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-g','--game', help='game e.g. --game=farm_1_1', required=False)
    parser.add_argument('-a','--app', help='--app=all or --app=fb-apps or --app=report-apps', required=False)
    parser.add_argument('-v','--version', help='--version=1.1 or --version=1.2', required=False)
    parser.add_argument('-o','--outfile', help='Output File Path', required=False)
    args = vars(parser.parse_args())
    return args

def load_conf_file(args):
    f = open(args['conf'], "r")
    confMap = yaml.safe_load(f)
    f.close()
    return confMap

def main():
    argsMap = parse_args()
    confMap = load_conf_file(argsMap)
    gp = GenerateDependencies(argsMap,**confMap)
    gp.run()

if __name__ == '__main__':
    main()
__author__ = 'praveenak'

"""
pip install boto

Find list of list of pipelines with at least 1 activity with same name
Find list of pipelines that needs to be fixed
Consequence of dup activity in different pipelines : If the pipelines are running on same cluster with same activityId and are ShellCommandActivity then we found concurrency issues
"""


from boto.datapipeline import connect_to_region #pip install boto
from boto.datapipeline.layer1 import DataPipelineConnection #pip install boto
from boto.regioninfo import RegionInfo #pip install boto
import time

RegionEndpoint = 'datapipeline.us-west-2.amazonaws.com'
RegionName="us-west-2"
kw_params={}

# Connect to a region
conn = connect_to_region(RegionName, **kw_params)
region = RegionInfo(RegionName,RegionEndpoint)
region.endpoint=region.name

kw_params['region'] = region
kw_params['host'] = "datapipeline.us-west-2.amazonaws.com"

kw_params['aws_access_key_id'] = 'AKIAJRKDNC52OAINDWKA'
kw_params['aws_secret_access_key'] = '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'

# Establish data pipeline service connection to the above connected region
dp_connection = DataPipelineConnection(**kw_params)

# Get list of pipelines
complete_pipeline_results=[]
has_more_results,marker = True, None

partial_response = dp_connection.list_pipelines()


while has_more_results:
    partial_response = dp_connection.list_pipelines(marker)
    has_more_results=partial_response['hasMoreResults']
    marker= has_more_results and partial_response['marker'] or None #Prepare marker from previous response to request more response
    complete_pipeline_results.extend(partial_response['pipelineIdList'])

print complete_pipeline_results

print "Total no. of pipelines:",len(complete_pipeline_results)

pipelines_dict={}

# prepare dict with key as pipelineId and value as pipelineName
for pipeline_dict in complete_pipeline_results:
    pipelines_dict[pipeline_dict['id']] = pipeline_dict['name']

print pipelines_dict

pipelineIds_list = pipelines_dict.keys()

pipelineStatus_dict = {}
start = 0
end = 25
process = True

while process:
    if(end >= len(pipelineIds_list)):
        end = len(pipelineIds_list)
    mydict = dp_connection.describe_pipelines(pipelineIds_list[start:end])
    for key in mydict.keys():
        pipelinelist = mydict[key]
        #pipelinedict has keys [fields, pipelineId, name, tags]
        for pipelinedict in pipelinelist:
            fields_dict = pipelinedict['fields']
            pipelineStatus_dict[pipelinedict['pipelineId']] = next((pipeline_status for pipeline_status in fields_dict if pipeline_status['key'] == '@pipelineState'), {'@pipelineState':'INVALID'})['stringValue']
    if (end  == len(pipelineIds_list)):
        process = False
    start = end
    end = end + 25

# for key, value in pipelineStatus_dict.iteritems():
#     print key, value

duppipelines = {}

# Create Activity Id and list of Active pipelines with same Activity Id mapping
for pipelineId in pipelines_dict.keys():
    if pipelineStatus_dict[pipelineId] == 'SCHEDULED':
        pipelineObjects = dp_connection.get_pipeline_definition(pipelineId)['pipelineObjects']
        for pipelineObject in pipelineObjects:
            activityId = pipelineObject['id']
            if activityId.startswith('ActivityId'):
                if activityId in duppipelines.keys():
                    duppipelines[activityId].append(pipelineId)
                else:
                    duppipelines[activityId] = [pipelineId]
        time.sleep(0.25)

# duppipelines.values() is a list of lists
#print duppipelines.values()

deduppipelines_set = set(tuple(x) for x in duppipelines.values())
deduppipelines = [ list(x) for x in deduppipelines_set ]


#dupipelines = list(set(duppipelines.values())) # Remove duplicate values in the dupipelines map

#print duppipelines

fix_pipelines_list = []


#Remove lists with dup pipelines in the list to not consider duplicate piplines because the dups are inactive

print "Below are list with list of pipelines of atleast one conflicting activity id:"
for plist in deduppipelines:
    if len(plist) >= 2:
        namelist = [pipelines_dict[id] for id in plist]
        #print namelist
        deduped_namelist = list(set(namelist))
        if len(deduped_namelist) > 1: #print only if the pipeline in the pipeline groups are not same
            print deduped_namelist
            fix_pipelines_list.extend(deduped_namelist)



fix_pipelines_list = list(set(fix_pipelines_list))

print "Fix these pipeline:",fix_pipelines_list
print "length of pipelines to be fixed:",len(fix_pipelines_list)

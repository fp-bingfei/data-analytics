import urllib2
import datetime
import time
import json
import argparse


class AdjustData:
    def __init__(self, args):
        self.date = datetime.datetime.strptime(args['date'], '%Y-%m-%d').date()
        self.apps = [('WAF_GLOBAL_ANDROID','tva6rbs9cqdc'),('FFS_GLOBAL_ANDROID','x4e5kgunchqq'),('FFS_GLOBAL_IOS', 'eqgwx9hq8uvx')]
        self.httpApi = 'http://api.adjust.com/kpis/v1/'
        self.user_token = 'hK1UMZUWoAryyGCDoEzi'
        self.cohorts_csv = ''
        self.kpis_csv = ''


    def run(self):
        for app in self.apps:
            for i in [1, 3, 7, 15, 30]:
                startDate = (self.date - datetime.timedelta(i)).strftime('%Y-%m-%d')
                endDate = self.date
                trackers = self.getTrackers(app, startDate, endDate)
                kpis = self.getKPI(app, trackers, startDate, startDate)
                cohorts = self.getCohorts(app, trackers, startDate, endDate)
                self.parseKPI(kpis, startDate)
                self.parseCohorts(cohorts,startDate)
        
        f = open('/tmp/cohorts.csv', 'w')
        f.write(self.cohorts_csv.encode('utf8'))
        f.close()
        f = open('/tmp/kpis.csv', 'w')
        f.write(self.kpis_csv.encode('utf8'))
        f.close()
        #print self.cohorts_csv
        #print self.kpis_csv

    def getTrackers(self, app, startDate, endDate):

        url = self.httpApi + '%s?start_date=%s&end_date=%s&kpis=clicks,installs&user_token=hK1UMZUWoAryyGCDoEzi&grouping=trackers' % (
            app[1], startDate, endDate)
        res =  urllib2.urlopen(url)
        jsonRes = json.loads(res.read())

        print startDate + ' ' + app[0] + ' in getTrackers'

        return jsonRes["result_parameters"]["trackers"]


    def getKPI(self, app, trackers, startDate, endDate):

        res = [] 
        for tracker in trackers:
            url = self.httpApi + '%s/trackers/%s?start_date=%s&end_date=%s&kpis=clicks,installs&user_token=hK1UMZUWoAryyGCDoEzi&grouping=campaigns,adgroups'% (
                app[1], tracker['token'], startDate, endDate)
            tmp = urllib2.urlopen(url)
            jsonTmp = json.loads(tmp.read())
            res.append((app[0], jsonTmp['result_set']))

            print startDate + ' ' + app[0] + ' ' + tracker['name'] + ' in getKPI'

        return res

    def getCohorts(self, app, trackers, startDate, endDate):
        res = [] 
        for tracker in trackers:
            url = self.httpApi + '%s/trackers/%s/cohorts?start_date=%s&end_date=%s&kpis=cohort_size,retained_users,sessions,time_spent&user_token=hK1UMZUWoAryyGCDoEzi&grouping=campaigns,adgroups,periods&period=day' % (
                app[1], tracker['token'], startDate, endDate)
            tmp = urllib2.urlopen(url)
            jsonTmp = json.loads(tmp.read())
            res.append((app[0], jsonTmp['result_set']))

            print startDate + ' ' + app[0] + ' ' + tracker['name'] + ' in getCohorts'

        return res


    def parseKPI(self, kpis, startDate):
        
        #date, game, tracker, campaign, adgroup, clicks, installs
        for (game, kpi) in kpis:
            try:
                builder_1 = startDate + '|' + game + '|' + kpi['name']
                for campaign in kpi["campaigns"]:
                    builder_2 = builder_1 + '|' + campaign['name']
                    for adgroup in campaign['adgroups']:
                        builder_3 = builder_2 + '|' + adgroup['name'] + '|' + str(adgroup["kpi_values"][0]) + '|' + str(adgroup["kpi_values"][1]) + '\n'
                        self.kpis_csv += builder_3
            except Exception, e:
                print startDate + ' ' + 'ERROR In ' + game + ' ' + kpi['name'] + ': ' + e.message
                continue


    def parseCohorts(self, cohorts, startDate):
        #date, game, tracker, campaign, adgroup, (cohort_size, retained_users, sessions, time_spent) * (d0, d1, d3, d7, d15, d30)
        for (game, cohort) in cohorts:
            try:
                builder_1 = startDate + '|' + game + '|' + cohort['name']
                for campaign in cohort['campaigns']:
                    builder_2 = builder_1 + '|' + campaign['name']
                    for adgroup in campaign['adgroups']:
                        builder_3 = (builder_2 + '|' + adgroup['name'] + '|%d' * 24 + '\n') % tuple(adgroup['periods'][0]['kpi_values']
                                + (adgroup['periods'][1]['kpi_values'] if len(adgroup['periods']) > 1 else [0,0,0,0])
                                + (adgroup['periods'][3]['kpi_values'] if len(adgroup['periods']) > 3 else [0,0,0,0])
                                + (adgroup['periods'][7]['kpi_values'] if len(adgroup['periods']) > 7 else [0,0,0,0])
                                + (adgroup['periods'][15]['kpi_values'] if len(adgroup['periods']) > 15 else [0,0,0,0])
                                + (adgroup['periods'][30]['kpi_values'] if len(adgroup['periods']) > 30 else [0,0,0,0]))
                        self.cohorts_csv += builder_3
            except Exception, e:
                print startDate + ' ' + 'ERROR In ' + game + ' ' + cohort['name'] + ': ' + e.message
                continue
                    

    def saveToS3(self):
        pass


def parse_args():
    parser = argparse.ArgumentParser(description='Download adjust data to BI')
    parser.add_argument('-d','--date', help='date to process, yyyy-mm-dd', required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    ad = AdjustData(args)
    ad.run()

if __name__ == '__main__':
    main()

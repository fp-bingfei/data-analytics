
import yaml
from collections import OrderedDict
import itertools
import pandas as pd
from sqlalchemy import create_engine
import json
from datetime import datetime
import numpy as np
import sys
import os

#auxiliary functions
def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load_all(stream, OrderedLoader)

def wrap1(f, a):
    return f+'('+ str(a) + ')'

def wrap(f, a, b):
    return f+'('+ str(a) +', '+ str(b)+ ')'

def json_wrap(a,b):
    return wrap('json_extract_path_text', a, '\''+b+'\'')

def array_wrap(a,b):
    return wrap('json_extract_array_element_text', a, b)

def json_wrap_list(a):
    assert type(a)==list
    if len(a)==0:
        return ''
    if len(a)==1:
        return str(a[0])
    else:
        ret = json_wrap(a[0], a[1])
        for i in range(2, len(a)):
            ret = json_wrap(ret, a[i])
        return ret

def expand_df(df):
    df_list = []
    for c in df.columns:
        k = df[c].map(type).value_counts().order(ascending=False)
        if any([x in k.index for x in (unicode, str)]):
            s = unicode
        elif dict in k.index:
            s = dict
        else:
            s = k.index[0]
        #print c, s
        if s!=list:
            if s!=dict:
                df_list.append(df[c])
            else:
                try:
                    z = pd.DataFrame.from_records(df[c].map(lambda x: {} if pd.isnull(x) else x))
                    z.columns = [c+','+x for x in z.columns]
                    df_list.append(expand_df(z))
                except TypeError:
                    print df[c].fillna({})
    return pd.concat(df_list, axis=1)

def type_len_tuple(df, c):
    type_dic = {
                pd.tslib.Timestamp: 'TIMESTAMP',
                datetime.date: 'DATE',
                }
    
    k = df[c].map(type).value_counts().order(ascending=False)
    if any([x in k.index for x in (unicode, str)]):
        s = unicode
    else:
        s = k.index[0]
    
    #for integers, assign integer type based on maximal value of column
    if s==np.int64:
        max_len = df[c].map(str).map(len).max()
        if max_len>32:
            type_str = 'BIGINT'
            yaml_str = 'bigint'
        else:
            type_str = 'INTEGER'
            yaml_str = 'int'

    #for string types, create varchar with length twice the maximum length in current column for buffer
    elif s==unicode or s==str:
        max_len = df[c].map(unicode).map(len).max()*2
        type_str = 'VARCHAR({a})'.format(a=max_len)
        yaml_str = 'varchar'
    elif s==np.float64:
        max_len = df[c].map(lambda x: len(unicode(x).replace('.',''))).max()+2
        type_str = 'NUMERIC({a},2)'.format(a=max_len)
        yaml_str = 'decimal'
    elif s not in (list, dict):
        max_len = 0
        type_str = type_dic[s]
        yaml_str = type_str.lower()
    else:
        yaml_str = 'list'
        max_len = 0
    
    return [yaml_str, int(max_len)]


#read config file
assert os.path.exists(sys.argv[1])

with open(sys.argv[1], 'r') as f:
    configs = list(ordered_load(f, yaml.SafeLoader))

#the real sql generation begins here
coldstart = False
conn_str = "redshift+psycopg2://{user}:{pw}@{cluster}.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/{db}"
result_sql = ""

if len(configs)>=2:
	engine = create_engine(conn_str.format(user=configs[0]['username'], pw=configs[0]['password'], cluster=configs[0]['cluster'], db=configs[0]['db']))

for e in configs[-1].keys():
	v = configs[-1][e]

	assert 'agg_freq' in v
	a = v['agg_freq']
	dest_ts = a.keys()[0]
	datepart = a[dest_ts][1]

	type_map = {'int': 'INTEGER', 
	            'bigint': 'BIGINT', 
	            'date': 'DATE', 
	            'timestamp': 'TIMESTAMP',
	            'varchar': 'VARCHAR',
	            'decimal': 'DECIMAL'}

	create_sql = '''create table if not exists {table} (
	    {col_def}
	);'''

	col_defs = []
	#datepart only recognizes d/w/m parameters, everything else will be seen as raw timestamp
	if datepart in ('day', 'week', 'month'):
		col_defs.append(dest_ts+' '+'DATE')
	else:
		col_defs.append(dest_ts+' '+'TIMESTAMP')
	    
	z = v.get('d', {}).copy()
	z.update(v.get('m', {}))

	for k in z:
	    b = z[k][1]
	    b[0]= b[0].lower()
	    if b[0]=='varchar':
	        col_defs.append(k+' '+wrap1(type_map[b[0]], b[1])+' ENCODE lzo')
	    elif b[0] in ('int', 'bigint'):
	        col_defs.append(k+' '+type_map[b[0]])
	    elif b[0]=='decimal':
	        col_defs.append(k+' '+wrap1(type_map[b[0]], str(b[1])+', 2'))
	    else:
	        col_defs.append(k+' '+type_map.get(b[0], 'VARCHAR(32)'))
	        
	            
	result_sql+= create_sql.format(table=v['dest_table'], col_def=',\n    '.join(col_defs))
	result_sql+= '\n\n'

	#delete from dest
	dest_delete_sql = '''delete from {dest_table} where {date_cond};'''

	#insert from source
	where_params = ''
	where_list_clean = []
	where_num = 0
	if 'where' in v:
	    for p in v['where']:
	        where_num+=1
	        tmp = v['where'][p]
	        if type(tmp)==str:
	            where_list_clean.append(p + ' = \'' + tmp + '\'')
	        elif type(tmp)==list:
	            where_list_clean.append(p + ' in ('+ repr(tmp).replace('[','').replace(']','')+')')

	where_list = list(where_list_clean)
	if datepart not in ('day', 'week', 'month'):
		datepart_force = 'day'
	else:
		datepart_force = datepart


	if 'overwrite_last' in a: 
		le = 'datediff(\'{datepart}\', {col}, sysdate){sym}{num}'.format(col=a[dest_ts][0], \
	                                                            datepart=datepart_force, num=a['overwrite_last'], sym='<=')
		ge = 'datediff(\'{datepart}\', {col}, sysdate){sym}{num}'.format(col=a[dest_ts][0], \
	                                                            datepart=datepart_force, num=a['overwrite_last'], sym='>')
		if not coldstart: 
			where_list.append(le)

	where_params = ' and '.join(where_list)
	if where_params:
	    where_params = 'where ' + where_params


	if 'overwrite_last' in a:
		date_str = 'datediff(\'{datepart}\', {dest_ts}, sysdate)<={num}'.format(datepart=datepart_force, dest_ts=dest_ts,\
	                                                                            num=a['overwrite_last'])
	else:
	    date_str = '{dest_ts}>=(select min({source_ts}) from {source_table} {where})'
	    if datepart in ('day', 'week', 'month'):
	        source_ts = 'date_trunc(\'{datepart}\', {col})'.format(col=a[dest_ts][0], datepart=datepart)
	    else:
	        source_ts = a[dest_ts][0]
	    date_str = date_str.format(source_ts = source_ts, source_table=v['source_table'], \
	                               dest_ts=dest_ts, where= where_params)
	    
	result_sql+= dest_delete_sql.format(dest_table=v['dest_table'], date_cond=date_str)
	result_sql+= '\n\n'
	            
	#first layer - explode and apply filter
	if 'explode' in v:
	    core = '''(select *, {explode} as {explode_key} from
	    (select *, {parse_explode} as exploded_items from {table} {where}) as g, seq_0_to_100 as seq
	    where seq.i<json_array_length(g.exploded_items)
	    )'''
	    
	    
	    explode_params = v['explode'][v['explode'].keys()[0]]
	    
	    core_filled = core.format(parse_explode=json_wrap_list(explode_params), table=v['source_table'], where= where_params, \
	                      explode=array_wrap('g.exploded_items', 'seq.i'), explode_key=v['explode'].keys()[0])
	    
	else:
	    core = '''(select * from {table} {where})'''
	    core_filled = core.format(table=v['source_table'], where= where_params)
	    
	#second layer - parse json, group bys, aggs

	next_layer ='''insert into {dest_table}
	({insert_cols})
	select {agg_freq} 
	{ds} 
	from 
	{core} 
	{exclude} 
	group by {groupby_num};'''
	    
	exclude = []
	agg_freq = ''

	if datepart in ('day', 'week', 'month'):
	    agg_freq = 'date_trunc(\'{datepart}\', {col}) as {alias},'.format(datepart=datepart, \
	                                                                     col=a[dest_ts][0], alias=dest_ts)
	else:
	    agg_freq = '{col} as {alias},'.format(col=a[dest_ts][0], alias=dest_ts)
	
	dims = []
	measures = []
	insert_cols = [dest_ts]

	if 'd' in v:  
	    for k in v['d']:
	        b = v['d'][k][0]
	        if type(b) in (str, unicode):
	        	phrase = '\''+ b + '\''
	        elif type(b) in (int,):
	        	phrase = str(b)
	        elif json_wrap_list(b)=='':
	            phrase = k
	        else:
	            phrase = json_wrap_list(b)

	        if v['d'][k][1][0].lower() in ('int', 'bigint'):
	        	#if len>2, it means we are doing some json parsing, so need to put nullif and convert to int, otherwise assume data is already an int
	        	if type(b)==list and len(b)>=2 :
	        		phrase = wrap('nullif', phrase+'::varchar', '\'\'')+'::'+v['d'][k][1][0].lower()
	        if phrase!=k:
	        	phrase += ' as '+ k
	        dims.append(phrase)
	        if len(v['d'][k])>=3:
	            e = v['d'][k][2].get('add_sql', '')
	            if len(e)>0:
	                exclude.append(json_wrap_list(b)+ ' '+e)

	        insert_cols.append(k)
	    
	if 'm' in v:
	    for k in v['m']:
	        valid = False
	        b = v['m'][k][0]
	        if json_wrap_list(b)=='':
	            phrase = k
	        else:
	            phrase = json_wrap_list(b)

	        if v['m'][k][1][1] in ('sum', 'max', 'min', 'avg'):
	            valid = True
	            measures.append(wrap1(v['m'][k][1][1], wrap('nullif', phrase+'::varchar', '\'\'')+'::'+v['m'][k][1][0])+ ' as ' + k)
	        elif v['m'][k][1][1]=='count':
	            valid = True
	            measures.append(wrap1('count', phrase)+ ' as ' + k)
	        elif v['m'][k][1][1]=='countd':
	            valid = True
	            measures.append(wrap1('count', 'distinct '+ phrase)+ ' as ' + k)
	        
	        if len(v['m'][k])>=3 and valid:
	            e = v['m'][k][2].get('add_sql', '')
	            if len(e)>0:
	                exclude.append(phrase+ ' '+e)

	        insert_cols.append(k)

	assert len(dims)+len(measures)>0

	#print dims
	#print measures
	#print exclude
	    
	if len(exclude)>0:
	    exclude_clause = 'where '+' and '. join(exclude)
	else:
	    exclude_clause = ''

	result_sql+= next_layer.format(agg_freq= agg_freq, ds=',\n'.join(dims+measures), \
	                        core=core_filled, exclude=exclude_clause, dest_table=v['dest_table'],\
	                        groupby_num=', '.join([str(x) for x in range(1, int('agg_freq' in v)+len(dims)+1)]),
	                        insert_cols=', '.join(insert_cols))
	#result_sql+= '\n\nvacuum {0};'.format(v['dest_table'])
	result_sql+= '\n\n'

	#move to s3/ delete from source
	source_delete_sql = '''delete from {source_table}{where};'''

	if bool(a.get('delete_raw', False)):
	    if 'overwrite_last' in a:
	        where_list = list(where_list_clean)
	        where_list.append(ge)
	    
	    no_cur_period = 'date_trunc(\'{datepart}\', {source_ts})< date_trunc(\'{datepart}\', sysdate)'\
	                        .format(datepart=datepart_force, source_ts=a[dest_ts][0])
	    where_list.append(no_cur_period)
	    where = ' and '.join(where_list)
	    if where:
	        where = ' where '+ where
	        
	    #copy source to s3
	    if 's3' in v:
	    	assert len(configs)>=2
	        assert v['s3'].get('base','').startswith('s3://')
	        min_date_sql = '''select distinct(trunc({source_ts})) as date 
	            from {source_table} {where} order by 1;'''
	        minmax_df = pd.read_sql_query(min_date_sql.format(source_ts=a[dest_ts][0], source_table=v['source_table'], where = where), engine)
	        
	        if len(minmax_df.dropna())>0:
	            #sd = minmax_df['min_date'].iloc[0]
	            #ed = minmax_df['max_date'].iloc[0]

	            tmp = []
	            for keys in v['where']:
	                if type(v['where'][keys]) in (str, unicode):
	                    tmp_list = [v['where'][keys]]
	                elif type(v['where'][keys])==list:
	                    tmp_list = v['where'][keys]

	                if bool(v['s3'].get('explicit', True)):
	                    tmp.append([keys+'='+str(t) for t in tmp_list])
	                else:
	                    tmp.append(tmp_list)

	            unload_cmd_raw = """unload ('select * from {source_table} where trunc({source_ts}) >= \\'{start_date}\\' and trunc({source_ts}) < \\'{end_date}\\' and {where};') to '{s3_prefix}/{where_path}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
	                        """

	            aws_access_key_id = 'AKIAJRKDNC52OAINDWKA'
	            aws_secret_access_key = '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'
	            delimiter = '^'
	            command_options = 'GZIP ESCAPE'

	            for i in itertools.product(*tmp):
	                j = '/'.join([str(x) for x in i])
	                for d in minmax_df['date'].values:
	                    unload_cmd = unload_cmd_raw.format(start_date=d.strftime('%Y-%m-%d'),end_date=(d+pd.DateOffset(days=1)).strftime('%Y-%m-%d'),\
	                        event=e,s3_prefix=v['s3']['base'],s3_date_path=d.strftime('%Y/%m/%d'), source_ts=a[dest_ts][0],\
	                        aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key,\
	                        delimiter=delimiter,command_options=command_options,where=' and '.join(where_list_clean).replace('\'', '\\\''), where_path=j, source_table=v['source_table'])
	                    result_sql+= unload_cmd

	    result_sql+= source_delete_sql.format(source_table=v['source_table'], where = where)
	    result_sql+= '\n\n'

#result_sql+='vacuum {source_table};'.format(source_table=v['source_table'])

if len(sys.argv)>=3:
	with open(sys.argv[2], 'w') as f:
		f.write(result_sql)
else:
	print result_sql
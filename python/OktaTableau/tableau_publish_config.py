import yaml
from collections import OrderedDict
import itertools
import pandas as pd
from sqlalchemy import create_engine
from okta_tableau import *
import json
from datetime import datetime
import numpy as np
import sys
import os

#auxiliary functions
def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load_all(stream, OrderedLoader)

assert os.path.exists(sys.argv[1])
with open(sys.argv[1], 'r') as f:
    configs = list(ordered_load(f, yaml.SafeLoader))[0]

username = sys.argv[2]
password = sys.argv[3]
db_username = sys.argv[4]
db_password = sys.argv[5]

for key in configs:
	config = configs[key]
	if 'upload' in config:
		print config
		#publish_flag=True -> upload to Tableau server, otherwise modify and keep a local copy
		publish_flag = config['upload']
		p = TableauPublish(username, password, site=config.get('site', ''))
		p.query_projects()
		for proj in p.projects:
			if proj.get('name').lower()==config['project'].lower():
				proj_id = proj.get('id')
				if not os.path.exists(config['template']):
					fn = 'TableauTemplates/'+config['template']
				else:
					fn = config['template']
				wb_name = config['name']
				attrib_dict = dict(config.get('attrib', {}))
				sql_replace_dict = dict(config.get('custom_sql_replace', {}))
				p.edit_template_publish(fn, wb_name, proj_id, attrib_dict=attrib_dict, sql_replace_dict=sql_replace_dict, user=db_username, password=db_password, publish=publish_flag)

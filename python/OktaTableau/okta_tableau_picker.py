from flask import Flask
from flask import abort, request, render_template, redirect, make_response
import logging
from logging.handlers import TimedRotatingFileHandler
import MySQLdb
import time
import requests
import json
from saml2 import (
    BINDING_HTTP_POST,
    BINDING_HTTP_REDIRECT,
    entity,
)
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config
import hashlib
import base64

app = Flask(__name__)

#default testing parameters, should be overridden in config file
app.config['OKTA_DB_HOST'] = 'localhost'
app.config['OKTA_DB_USER'] = 'root'
app.config['OKTA_DB_PW'] = ''
app.config['OKTA_LOG_FILE'] = '/tmp/okta_callback.log'
app.config['OKTA_AUTH_TOKEN'] = '0060A1iJxZ3ruG3qXlwygTwCwyqKDSINTwpC7elvql'
app.config['SERVER_HOST'] = 'http://localhost:5000'
app.config['ENCRYPTION_KEY'] = 'Zq4GS8n492S7bC7w' 

app.config['DEBUG'] = True

#take env var to overwrite the configuration
app.config.from_envvar('OKTA_CALLBACK_SETTINGS', silent=True)

#setup logging
logger = logging.getLogger()
fmt = '%(asctime)s %(levelname)s %(pathname)s %(module)s.%(funcName)s Line:%(lineno)d %(message)s'
hdlr = TimedRotatingFileHandler(app.config['OKTA_LOG_FILE'], 'midnight', 1, 100)
hdlr.setFormatter(logging.Formatter(fmt))
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

#tableau database
#CREATE TABLE `user_apps` (
# `uid` varchar(255) NOT NULL,
# `appid` varchar(255) NOT NULL,
# PRIMARY KEY (`uid`, `appid`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8

def check_permission( uid, appid ):
#make sure this user and app is inside authorized list

    db = MySQLdb.connect(host=app.config['OKTA_DB_HOST'],
                         user=app.config['OKTA_DB_USER'],
                         passwd=app.config['OKTA_DB_PW'],
                         db="tableau",
                         use_unicode=True,
                         charset="utf8")

    cur = db.cursor()

    sql = """select uid from user_apps where uid=%s and appid=%s"""
    cur.execute(sql, (uid,appid)),
    results = cur.fetchall()
    db.close()

    return len(results) == 1
      
def get_uid(request):

    signed_uid = request.cookies.get('uid')
    if signed_uid:
        uid, timestamp, signature = base64.b64decode(signed_uid).split() 
        generated_sig = hashlib.sha256(uid+timestamp+app.config['ENCRYPTION_KEY']).hexdigest()
        if signature == generated_sig:
            if float(timestamp) < float(time.time() + 60): #allow a minute
                return uid
            else:
                app.logger.warning("cookie for %s expired", uid)
        else:
            app.logger.warning("bad signature")

    raise Exception('invalid login')
        
@app.route('/account/changer/', methods = ['POST'])
def tableau_acct_changer():

    try:
        uid = get_uid(request) #check if user is logged in
        appid = request.form['appid']

        if check_permission(uid, appid):
            tableau_url = 'https://funplus.okta.com/api/v1/users/'
            user = requests.get(tableau_url+uid, headers={"Authorization": "SSWS "+app.config['OKTA_AUTH_TOKEN']}).json()
            user_id = user['id']
            new_dict = {"profile":{"tableauID": appid}}
            r = requests.post(tableau_url+user_id, data=json.dumps(new_dict), 
                                                   headers={"Authorization": "SSWS "+app.config['OKTA_AUTH_TOKEN'], 
                                                            "Accept": "application/json", 
                                                            "Content-Type": "application/json"})
            if r.status_code==200:
                time.sleep(1)
                return redirect("https://funplus.okta.com/home/tableau/0oa220h18aETJCiLi0x7/2649", code=302)
            else:
                return 'Update for {0} failed! Status code: {1}'.format(user_id, r.status_code), 400

        else:
            return "Invalid uid and/or appid", 400

    except: #not logged in
        app.logger.exception("can't change account")
        return redirect("https://funplus.okta.com/home/tableau/0oa220h18aETJCiLi0x7/2649", code=302)
        

def _get_saml_client():
    acs_url = app.config['SERVER_HOST'] + "/account/acs/"
    rv = requests.get('https://funplus.okta.com/app/exk3zq7b4fHdrCTee0x7/sso/saml/metadata')
    import tempfile
    tmp = tempfile.NamedTemporaryFile()
    f = open(tmp.name, 'w')
    f.write(rv.text)
    f.close()
    saml_settings = {
        'metadata': {
            "local": [tmp.name],
        },
        'service': {
            'sp': {
                'endpoints': {
                    'assertion_consumer_service': [
                        (acs_url, BINDING_HTTP_REDIRECT),
                        (acs_url, BINDING_HTTP_POST)
                    ],
                },
                'allow_unsolicited': True,
                'authn_requests_signed': False,
                'logout_requests_signed': True,
                'want_assertions_signed': True,
                'want_response_signed': False,
            },
        },
    }

    spConfig = Saml2Config()
    spConfig.load(saml_settings)
    spConfig.allow_unknown_attributes = True
    saml_client = Saml2Client(config=spConfig)
    tmp.close()
    return saml_client

@app.route('/account/acs/', methods = ['POST'])
def tableau_okta_acs():
    saml_client = _get_saml_client()
    resp = request.form['SAMLResponse']

    if len(resp) <= 0:
        return "Permission Denied", 401

    authn_response = saml_client.parse_authn_request_response(
        resp, entity.BINDING_HTTP_POST)

    user_identity = authn_response.get_identity()
    if user_identity is None:
        return "Permission Denied", 401

    uid = user_identity["Email"][0]

    try:

        db = MySQLdb.connect(host=app.config['OKTA_DB_HOST'],
                             user=app.config['OKTA_DB_USER'],
                             passwd=app.config['OKTA_DB_PW'],
                             db="tableau",
                             use_unicode=True,
                             charset="utf8")

        cur = db.cursor()

        sql = """select appid from user_apps where uid=%s"""
        cur.execute(sql, (uid,))
        results = cur.fetchall()
        appids = []
        for row in results:
            appids.append(row[0])
        db.close()

        if len(appids) >= 2:
            response = make_response(render_template('okta_tableau_picker.html', appids=appids, uid=uid))
        else:
            response = redirect("https://reports.funplus.com", code=302)

        #setup session for user
        timestamp = str(time.time())
        generated_sig = hashlib.sha256(uid+timestamp+app.config['ENCRYPTION_KEY']).hexdigest()
        signed_uid = base64.b64encode( uid + ' ' + timestamp + ' ' + generated_sig )
        response.set_cookie('uid', signed_uid) 
        return response

    except Exception, e:
        app.logger.exception("Error in sql")
        import traceback
        return traceback.format_exc(), 500

@app.route('/')
@app.route('/account/signin/') #legacy
def signin():
    saml_client = _get_saml_client()
    _, info = saml_client.prepare_for_authenticate()
    for key, value in info['headers']:
        if key == 'Location':
            redirect_url = value
            break
    return redirect(redirect_url, code=302)

if __name__ == '__main__':
    app.run(debug=app.config['DEBUG'])

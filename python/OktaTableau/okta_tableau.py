import pandas as pd
import requests
from sqlalchemy import create_engine
import requests
import json
import math
# Contains methods used to build and parse XML
import xml.etree.ElementTree as ET
import requests  # Contains methods used to make HTTP requests
import copy
import random
import os

# The following packages are used to build a multi-part/mixed request.
# They are contained in the 'requests' library.
from requests.packages.urllib3.fields import RequestField
from requests.packages.urllib3.filepost import encode_multipart_formdata

# usage:
# o = Okta([okta token filename])
# o.update_tableauid('abc@funplus.com', 'xyz')
# o.update_tableauid('abc@funplus.com') <-- maps email of user to tableauID 
# users = o.get_users()

class Okta:
    #token_file contains the okta key
    def __init__(self, token_file):
        self.token = open(token_file, 'r').readline()
        self.url = "https://funplus.okta.com/api/v1/users"
        self.auth_header = {"Authorization": "SSWS "+self.token}
        self.users = []
        
    #get list of all users on okta using pagination, takes a while
    def get_users(self):
        users = []
        cur_url = self.url
        while True:
            pages = requests.head(url=cur_url, headers=self.auth_header)
            users.extend(requests.get(cur_url, headers=self.auth_header).json())
            if 'next' in pages.links:
                cur_url = pages.links['next']['url']
            else:
                break
     
        self.users = users
        return users
    
    #get email addresses mapped to each tableau ID, returns a dict
    def get_tableau_mapping(self):
        tableau_dict = {}
        if len(self.users)==0:
            print 'User list empty! Call get_users to populate list first!'
        else:
            for u in self.users:
                if u.get('profile', {}).get('tableauID', '')!='':
                    tid = u['profile']['tableauID']
                    email = u['profile']['email']
                    if tid in tableau_dict:
                        tableau_dict[tid].append(email)
                    else:
                        tableau_dict[tid] = [email]
        
        return tableau_dict
    
    #change tableau id of an okta user
    def update_tableauid(self, login, tid=''):
        user = requests.get(self.url+'/'+login, headers=self.auth_header).json()
        print user
        user_id = user['id']
        if tid=='':
            tid=login
        new_dict = {"profile":{"tableauID": tid}}
        r = requests.post(self.url+'/'+user_id, data=json.dumps(new_dict), headers={"Authorization": "SSWS "+self.token, "Accept": "application/json", "Content-Type": "application/json"})
        if r.status_code==200:
            print 'Success! Updated {0} Tableau ID to {1}'.format(login, tid)
        else:
            print 'Update for {0} failed! Status code: {1}'.format(login, r.status_code)
            print r.text

# usage:
# t = TableauUsers()
# t.to_csv('out.csv') <-- export table of users/sites to csv
# t.freq('user') / t.freq('site') <-- returns dict of how many sites a user belongs to, or vice versa
# t.matrix_csv('matrix.csv') <-- exports a site/user matrix to csv
class TableauUsers:
    def __init__(self):
        self.conn = "postgresql+psycopg2://readonly:tableau@reports.funplus.com:8060/workgroup"
        self.engine = create_engine(self.conn)
        self.sql = '''select a.name as user, a.friendly_name, c.name as site, c.url_namespace
                        from
                        (SELECT site_id, system_user_id 
                        from public.users) b
                        join
                        (SELECT id, name, friendly_name FROM 
                        workgroup.public.system_users
                        where created_at is not NULL) a
                        on a.id = b.system_user_id
                        join
                        (select id, name, url_namespace
                        from public.sites) c
                        on c.id = b.site_id
                        order by 1;'''
        self.df = pd.read_sql_query(self.sql, self.engine)

    #export list of users/sites to csv
    def to_csv(self, filename):
        self.df.to_csv(filename)
        
    #returns a dict of how many sites a user belongs to, or vice versa
    def freq(self, by='user'):
        if by!='user':
            by='site'     
        return self.df.groupby(by).size().sort_values(ascending=False).to_dict()
    
    #export a site/user matrix to csv
    def matrix_csv(self, filname):
        self.df.groupby(['user', 'site']).size().unstack().fillna(0).to_csv(filename)

#Usage:
#p = TableauPublish('[tableau_username]', '[tableau_pw]', site='ffs') <- site='' is default site
#p.query_projects()  <-- prints and saves list of projects in site in class
#default_project_id = '2b9b6a2a-0d2a-11e4-81d1-f73c3c0a33cc'
#fn = 'waf_test.twb' 

#Publishes a twb to this project:
#p.publish_simple(fn, 'waf_test', default_project_id, doctype='workbook', user='user', password='pass')
#Here user/pass are the db passwords

#1) Edits connection params (in this case update schema to kpi) in workbook before publishing
#p.edit_template_publish(fn, 'ff_test', default_project_id, attrib_dict={'schema': 'kpi'}, user='user', password='pass')
#2) Edits connection params (changes all mentions of waf into farm in custom sql queries) in workbook before publishing
#p.edit_template_publish(fn, 'ff_test', default_project_id, sql_replace_dict={'waf': 'farm'}, user='user', password='pass')

#example connection params in tableau workbook:
#{'username': 'biadmin', 'server-oauth': '', 'single-node': 'no', 'dbname': 'kpi', 'class': 'redshift', 
#'odbc-connect-string-extras': '', 'sslmode': '', 'workgroup-auth-mode': 'prompt', 
#'server': 'kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com', 'port': '5439', 'schema': 'kpi_processed'}
# {'table': '[processed].[agg_kpi]', 'type': 'table', 'name': 'agg_kpi'}

class TableauPublish:
    ET.register_namespace('', "http://tableausoftware.com/api")
    ET.register_namespace('user', 'http://www.tableausoftware.com/xml/user')

    def __init__(self, user, password, site='', server='https://reports.funplus.com'):
        """
            'user'     is the name (not ID) of the user to sign in as.
                       Note that most of the functions in this example require that the user
                       have system administrator permissions.
            'password' is the password for the user.
            'site'     is the ID (as a string) of the site on the server to sign in to. The
                       default is "", which signs in to the default site.

            Returns the authentication token and the site ID.
        """
        self.xmlns = {'t': 'http://tableausoftware.com/api'}
        self.server = server

        def sign_in():
            url = server + "/api/2.0/auth/signin"

            # Builds the request
            xml_payload_for_request = ET.Element('tsRequest')
            credentials_element = ET.SubElement(xml_payload_for_request, 'credentials', name=user, password=password)
            site_element = ET.SubElement(credentials_element, 'site', contentUrl=site)
            xml_payload_for_request = ET.tostring(xml_payload_for_request)

            # Makes the request to Tableau Server
            server_response = requests.post(url, data=xml_payload_for_request)
            if server_response.status_code != 200:
                print(server_response.text)

            # Reads and parses the response
            xml_response = ET.fromstring(server_response.text)

            # Gets the token and site ID
            token = xml_response.find(
                't:credentials', namespaces=self.xmlns).attrib.get('token')
            site_id = xml_response.find('.//t:site', namespaces=self.xmlns).attrib.get('id')
            user_id = xml_response.find('.//t:user', namespaces=self.xmlns).attrib.get('id')
            return token, site_id, user_id

        self.token, self.site_id, self.user_id = sign_in()
        self.projects = []

    def _make_multipart(self, parts):
        """
        helper function - Creates one "chunk" for a multi-part upload.

        'parts' is a dictionary that provides key-value pairs of the format name: (filename, body, content_type).

        For more information, see this post:
            http://stackoverflow.com/questions/26299889/how-to-post-multipart-list-of-json-xml-files-using-python-requests
        """

        mime_multipart_parts = []

        for name, (filename, blob, content_type) in parts.items():
            multipart_part = RequestField(name=name, data=blob, filename=filename)
            multipart_part.make_multipart(content_type=content_type)
            mime_multipart_parts.append(multipart_part)

        post_body, content_type = encode_multipart_formdata(mime_multipart_parts)
        content_type = ''.join(
            ('multipart/mixed',) + content_type.partition(';')[1:])
        return post_body, content_type

    #saves the list of project xmls in self.projects, and prints out their names and ids
    def query_projects(self):
        """
        Gets a list of projects on the site.
        The function paginates over results (if required) using a page size of 100

        Returns a list of <project> elements.
        """
        pageNum, pageSize = 1, 100
        url = self.server + "/api/2.0/sites/{0}/projects".format(self.site_id)
        paged_url = url + "?pageSize={}&pageNumber={}".format(pageSize, pageNum)

        server_response = requests.get(
            paged_url, headers={"x-tableau-auth": self.token})
        if server_response.status_code != 200:
            print(server_response.text)
        xml_response = ET.fromstring(server_response.text)
        total_count_of_projects = int(xml_response.find(
            't:pagination', namespaces=self.xmlns).attrib.get('totalAvailable'))

        if total_count_of_projects > pageSize:
            projects = []
            projects.extend(xml_response.findall('.//t:project', namespaces=self.xmlns))
            number_of_pages = math.ceil(total_count_of_projects / pageSize)
            # Starts from page 2 because page 1 has already been returned
            for page in range(2, number_of_pages + 1):
                paged_url = url + \
                    "?pageSize={}&pageNumber={}".format(pageSize, page)
                server_response = requests.get(
                    paged_url, headers={"x-tableau-auth": TOKEN})
                if server_response.status_code != 200:
                    print(server_response.text)
                projects_from_page = ET.fromstring(server_response.text).findall(
                    './/t:project', namespaces=self.xmlns)
                projects.extend(projects_from_page)
        else:
            projects = xml_response.findall('.//t:project', namespaces=self.xmlns)

        for proj in projects:
            print proj.get('name'), proj.get('id')
        
        self.projects = projects

    def publish_simple(self, fn, name, project_id, doctype='workbook', user='', password=''):
        """
        Publishes a file to Tableau Server in a single request

        'fn'         is the name of the workbook/datasource to publish.
        'name'       is the workbook/datasource name as a string.
        'project_id' is the ID (as a string) of the project to publish to.
        'doctype'    indicates whether you are publishing a twb or tds
        'user/password'    is the username/password for the connection (only 1 set allowed)

        Returns a list of <workbook> elements.
        """
        with open(fn, 'rb') as f:
            workbook_bytes = f.read()
            
        if doctype!='workbook':
            doctype = 'datasource'

        # Builds the publish Request
        xml_payload_for_request = ET.Element('tsRequest')
        workbook_element = ET.SubElement(
            xml_payload_for_request, doctype, name=name, showTabs='True')
        p = ET.SubElement(workbook_element, 'project', id=project_id)
        if user!='' and password!='':
            cred_xml = ET.SubElement(workbook_element, 'connectionCredentials', name=user, password=password, embed='True')
        xml_payload_for_request = ET.tostring(xml_payload_for_request)

        payload, content_type = self._make_multipart(
            {'request_payload': ('', xml_payload_for_request, 'text/xml'),
             'tableau_workbook': (fn, workbook_bytes, 'application/octet-stream')})

        url = self.server + "/api/2.0/sites/{0}/{1}s".format(self.site_id, doctype)
        #url += "?workbookType={}".format(fn.split('.')[1])  # Get extension
        url += "?overwrite=true"

        server_response = requests.post(
            url, data=payload, headers={'x-tableau-auth': self.token, 'content-type': content_type})
        if server_response.status_code != 201:
            print(server_response.text)
            
        xml_response = ET.fromstring(server_response.text)
        return xml_response.find('t:workbook', namespaces=self.xmlns)

    #helper method to modify attributes in datasource-xml
    def update_ds_attrib(self, xml_tree_orig, attrib_dict={}, sql_replace_dict={}):
        assert type(attrib_dict)==dict
        assert type(sql_replace_dict)==dict
        
        xml_tree = copy.deepcopy(xml_tree_orig)
        for c in xml_tree.findall('.//datasource'):
            if 'caption' in c.attrib and 'inline' in c.attrib:
                for d in c.findall('.//relation'):
                    #if datasource is custom query
                    if d.get('type')=='text':
                        if len(sql_replace_dict)>0:
                            for old, new in sql_replace_dict.items():
                                d.text = d.text.replace(old, new)
                    #if using a table directly
                    elif d.get('type')=='table':
                        if 'table' in attrib_dict:
                            table_list = attrib_dict['table'].split('.')[-2:]
                            d.set('table', '.'.join(['['+x+']' for x in table_list]))
                            d.set('name', table_list[-1])
                #update params in connection tag using attrib_dict
                for d in c.findall('.//connection'):
                    for k, v in attrib_dict.items():
                        if k in d.attrib:
                            d.set(k, v)
                            
        return xml_tree

    #if publish=True, publish edited workbook to Tableau server. else keep a copy locally.
    def edit_template_publish(self, fn, wb_name, proj_id, attrib_dict={}, sql_replace_dict={}, user='', password='', publish=True):
        workbook_xml = ET.parse(fn)
        updated_xml = self.update_ds_attrib(workbook_xml, attrib_dict, sql_replace_dict)
        spec_tag = "<?xml version='1.0' encoding='utf-8' ?>"
        payload = spec_tag+ET.tostring(updated_xml.getroot())
        new_fn = 'temp_'+wb_name+'_'+str(random.random())+'.twb'
        with open(new_fn, 'wb') as f:
            f.write(payload)

        if publish:
            ret = self.publish_simple(new_fn, wb_name, proj_id, user=user, password=password)
            os.remove(new_fn)
            return ret

        return None
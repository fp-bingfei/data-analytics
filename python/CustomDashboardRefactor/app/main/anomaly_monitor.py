import pandas as pd
from sqlalchemy import create_engine
from helpers import *
from email import Encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.multipart import MIMEBase
import smtplib
import os
import argparse
import yaml
import datetime
import matplotlib.dates as mdates
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import json

import seaborn as sns
sns.set()

ffs_conn = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster-ffs.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"

ffs_engine = create_engine(ffs_conn)

sql = """select date, app, os, sum(revenue_usd) as rev, count(distinct user_key) as dau,
sum(is_new_user) as new_installs, sum(is_converted_today) as new_payers
from processed.fact_dau_snapshot
where date>='2015-01-01'
and app!='ffs.wp.prod'
group by 1,2,3;"""

df = pd.read_sql(sql, ffs_engine)
print df.head().to_string()

sql = """SELECT install_date as date, app, retention_days, round(sum(retained)*100.0/sum(new_users), 1) as r FROM ffs.processed.agg_retention where install_date>='2016-01-01' and retention_days in (1,3,7) group by 1,2,3;"""

retention_df = pd.read_sql(sql, ffs_engine)

x = []
for app, dfg in df.groupby('app'):
    tmp = df_rolling_mean(dfg.groupby('date').sum(), intervals=[1,7])
    tmp['key'] = tmp['key'].map(lambda x: '/'.join([app,x]))
    x.append(tmp)

for (app, rday), dfg in retention_df.groupby(['app', 'retention_days']):
	tmp = df_rolling_mean(pd.DataFrame(dfg.groupby('date')['r'].sum()), intervals=[1,7])
	tmp['key'] = app+'/D{0} retention'.format(str(rday))
	x.append(tmp)

dfx = pd.concat(x)
dfx.index = range(len(dfx))

days_to_plot = 60
ytd = datetime.date.today() - datetime.timedelta(1)
#ytd = datetime.date(2016,6,9)
dateFmt = mdates.DateFormatter('%m/%d')
dfx_ix = []

anomaly_list = []
for app, dft in df.groupby('app'):
    for arg in ['dau', 'new_installs', 'rev', 'new_payers']:
        ts = dft.groupby('date')[arg].sum()
        ts, avg, lb = anomaly_traces(ts)
        if ytd in ts.index:
            #print ts.ix[ytd], lb.ix[ytd]
            if ts.ix[ytd]<lb.ix[ytd]:
                anomaly_list.append((ts, avg, lb, [app, arg]))
                
fig = plt.figure(figsize=(10, len(anomaly_list)*5))
for i, (ts, avg, lb, args) in enumerate(anomaly_list):
    ax = fig.add_subplot(len(anomaly_list), 1, i+1)
                #plt.figure(figsize=(8,3))
                #ax = fig.add_subplot()
    ts.iloc[-days_to_plot:].plot(ax=ax)
    avg.iloc[-days_to_plot:].plot(ax=ax, label='trend')
    lb.iloc[-days_to_plot:].plot(ax=ax, label='lower')
    ax.scatter(ts[ts<lb].index[-days_to_plot:], ts[ts<lb][-days_to_plot:], c='purple')
    ax.legend()
    ax.set_title('/'.join(args))
    ax.xaxis.set_major_formatter(dateFmt)
    ax.set_ylim(ts.iloc[-days_to_plot:].min()*0.95,ts.iloc[-days_to_plot:].max()*1.05)
    dfx_ix.extend(dfx[(dfx['date'] == ytd) & (dfx['key']=='/'.join(args))].index)
                
if len(anomaly_list)>0:
	fn = "anomaly.png"
	fig.savefig(fn)

	#calculate delta %, and change cell text color to red/green
	x = ET.fromstring(dfx.ix[dfx_ix].set_index('date').to_html())
	print dfx.ix[dfx_ix].set_index('date').to_string()

	x.set('cellpadding', '4')
	x.set('style', "border:1px solid #000000; border-collapse:collapse")

	for z in x.findall('.//tbody/tr'):
	    for i, c in enumerate(z.getchildren()):
	    	if i>=1:
		        if i==1:
		            orig = float(c.text)
		            c.attrib={}
		        #need the +1 for key_col_index for alignment...
		        elif i<3+1:
		            val = float(c.text)
		            delta = orig-val
		            if val==0:
		            	val=0.1
		            pct = delta*100/val
		            space = ' ('
		            if pct>0:
		                space+='+'
		            c.text = '{:.2f}'.format(val)+space+'{:.1f}%'.format(pct)+')'
		            if delta>0:
		                c.set('style', 'color: green')
		            else:
		                c.set('style', 'color: red')
		        elif i==3+1:
		        	c.set('data-key', c.text)

	msg = MIMEMultipart('related')
	msg['From'] = "tableau_admin@funplus.com"
	msg['To'] = "zhenxuan.yang@funplus.com, zhisheng.wang@funplus.com, ffspm@funplus.com"
	msg['Subject'] = "FFS KPI Anomaly Monitor - "+ ytd.strftime('%Y-%m-%d')
	msgAlternative=MIMEMultipart('alternative')
	msg.attach(msgAlternative)
	msgText = MIMEText(ET.tostring(x)+'<br><img src="cid:{fn}">'.format(fn=os.path.basename(fn)), 'html')
	part = MIMEBase('application', 'octet-stream')
	part.set_payload(open(fn, 'rb').read())
	Encoders.encode_base64(part)
	part.add_header('Content-Disposition','inline', filename="%s" % os.path.basename(fn))
	msgAlternative.attach(msgText)
	msg.attach(part)

	s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
	s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
	s.sendmail(msg['From'], [x.strip() for x in msg['To'].split(",")], msg.as_string())
	s.quit()
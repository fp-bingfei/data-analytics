create table if not exists agg.agg_collectabledecoration_daily (
	    date DATE,
    snsid VARCHAR(64) ENCODE lzo,
    app VARCHAR(30) ENCODE lzo,
    os VARCHAR(36) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    reward_num INTEGER,
    level INTEGER
	);

delete from agg.agg_collectabledecoration_daily where datediff('day', date, sysdate)<=7;

insert into agg.agg_collectabledecoration_daily
	(date, snsid, app, os, country_code, reward_num, level)
	select date_trunc('day', ts) as date, 
	snsid,
app,
os,
country_code,
sum(nullif(json_extract_path_text(properties, 'reward_num')::varchar, '')::int) as reward_num,
max(nullif(json_extract_path_text(properties, 'level')::varchar, '')::int) as level 
	from 
	(select * from public.events_raw where event = 'CollectableDecoration' and datediff('day', ts, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5;


create table if not exists agg.agg_visitneighbors_daily (
	    date DATE,
    snsid VARCHAR(64) ENCODE lzo,
    app VARCHAR(30) ENCODE lzo,
    os VARCHAR(36) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    count_visit INTEGER,
    level INTEGER,
    coins_left BIGINT,
    rc_left BIGINT
	);

delete from agg.agg_visitneighbors_daily where datediff('day', date, sysdate)<=7;

insert into agg.agg_visitneighbors_daily
	(date, snsid, app, os, country_code, count_visit, level, coins_left, rc_left)
	select date_trunc('day', ts) as date, 
	snsid,
app,
os,
country_code,
count(1) as count_visit,
max(nullif(json_extract_path_text(properties, 'level')::varchar, '')::int) as level,
avg(nullif(json_extract_path_text(properties, 'coins_left')::varchar, '')::bigint) as coins_left,
avg(nullif(json_extract_path_text(properties, 'rc_left')::varchar, '')::bigint) as rc_left 
	from 
	(select * from public.events_raw where event = 'VisitNeighbors' and datediff('day', ts, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5;


create table if not exists agg.agg_usefarmaids_daily (
	    date DATE,
    app VARCHAR(30) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    item_type VARCHAR(28) ENCODE lzo,
    os VARCHAR(36) ENCODE lzo,
    snsid VARCHAR(64) ENCODE lzo,
    level INTEGER,
    count_farmAids INTEGER
	);

delete from agg.agg_usefarmaids_daily where datediff('day', date, sysdate)<=7;

insert into agg.agg_usefarmaids_daily
	(date, app, country_code, item_type, os, snsid, level, count_farmAids)
	select date_trunc('day', ts) as date, 
	app,
country_code,
json_extract_path_text(properties, 'item_type') as item_type,
os,
snsid,
max(nullif(json_extract_path_text(properties, 'level')::varchar, '')::int) as level,
count(1) as count_farmAids 
	from 
	(select * from public.events_raw where event = 'UseFarmAids' and datediff('day', ts, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5, 6;


create table if not exists agg.agg_usegasoline_daily (
	    date DATE,
    app VARCHAR(30) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    op_type VARCHAR(24) ENCODE lzo,
    os VARCHAR(36) ENCODE lzo,
    snsid VARCHAR(64) ENCODE lzo,
    level INTEGER,
    count_gasoline INTEGER
	);

delete from agg.agg_usegasoline_daily where datediff('day', date, sysdate)<=7;

insert into agg.agg_usegasoline_daily
	(date, app, country_code, op_type, os, snsid, level, count_gasoline)
	select date_trunc('day', ts) as date, 
	app,
country_code,
json_extract_path_text(properties, 'op_type') as op_type,
os,
snsid,
max(nullif(json_extract_path_text(properties, 'level')::varchar, '')::int) as level,
count(1) as count_gasoline 
	from 
	(select * from public.events_raw where event = 'UseGasoline' and datediff('day', ts, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5, 6;


create table if not exists agg.agg_useop_daily (
	    date DATE,
    app VARCHAR(30) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    op_type VARCHAR(24) ENCODE lzo,
    os VARCHAR(36) ENCODE lzo,
    snsid VARCHAR(64) ENCODE lzo,
    level INTEGER,
    amount_op INTEGER
	);

delete from agg.agg_useop_daily where datediff('day', date, sysdate)<=7;

insert into agg.agg_useop_daily
	(date, app, country_code, op_type, os, snsid, level, amount_op)
	select date_trunc('day', ts) as date, 
	app,
country_code,
json_extract_path_text(properties, 'op_type') as op_type,
os,
snsid,
max(nullif(json_extract_path_text(properties, 'level')::varchar, '')::int) as level,
sum(nullif(json_extract_path_text(properties, 'amount')::varchar, '')::int) as amount_op 
	from 
	(select * from public.events_raw where event = 'UseOP' and datediff('day', ts, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5, 6;


create table if not exists agg.agg_claim_daily_story_daily (
	    date DATE,
    action VARCHAR(24) ENCODE lzo,
    app VARCHAR(30) ENCODE lzo,
    country_code VARCHAR(4) ENCODE lzo,
    os VARCHAR(36) ENCODE lzo,
    snsid VARCHAR(64) ENCODE lzo,
    level INTEGER,
    count_story INTEGER
	);

delete from agg.agg_claim_daily_story_daily where datediff('day', date, sysdate)<=7;

insert into agg.agg_claim_daily_story_daily
	(date, action, app, country_code, os, snsid, level, count_story)
	select date_trunc('day', ts) as date, 
	json_extract_path_text(properties, 'action') as action,
app,
country_code,
os,
snsid,
max(nullif(json_extract_path_text(properties, 'level')::varchar, '')::int) as level,
count(1) as count_story 
	from 
	(select * from public.events_raw where event = 'claim_daily_story' and datediff('day', ts, sysdate)<=7) 
	 
	group by 1, 2, 3, 4, 5, 6;


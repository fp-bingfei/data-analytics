from helpers import sql_to_es, ordered_load, chronicle_es_body
import yaml
from elasticsearch import Elasticsearch
import sys 
from collections import OrderedDict
from sqlalchemy import create_engine, text
import sqlalchemy
from os.path import dirname, basename, abspath
import os
from boto import s3
from boto.s3.connection import OrdinaryCallingFormat
from boto.s3.key import Key
import commands

redshift_cfg = {
'ffs':{
    'BI': {'cluster': 'bicluster-ffs.cpaytjecvzyu'},
    'Tools': {'cluster': 'ffs-oplog.c2dd4vsii706', 'user': 'ffs', 'pw': 'flu89uKm0nh'}
    },
'farm': {
    'BI': {'cluster': 'bicluster.cpaytjecvzyu', 'db': 'farm'}
    }
}

def main(test, gameid, **kwargs):    
    #the config file is now on s3
    """access_key = 'AKIAJ7DQBN5LXCCFQKJA'
    secret_key = '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'
    conn = s3.connect_to_region('us-west-2', aws_access_key_id = access_key,
                            aws_secret_access_key = secret_key,
                            calling_format=OrdinaryCallingFormat())

    b = conn.get_bucket('com.funplusgame.bidata') # substitute your bucket name here
    k = Key(b)
    k.key = 'chronicle/sql_to_es_config.yaml'
    """
    fn = '/'.join(abspath(__file__).split('/')[:-2]) +'/configs/sql_to_es_config.yaml'
    status, output = commands.getstatusoutput("aws s3 cp s3://com.funplusgame.bidata/chronicle/sql_to_es_config.yaml {fn}".format(fn=fn))
    #k.get_contents_to_filename(fn)
    with open(fn, 'r') as f:
        configs = list(ordered_load(f, yaml.SafeLoader))[0][gameid]

    if test:
    	es = Elasticsearch()
    else:
    	es = Elasticsearch('52.74.59.31:9200', timeout=60)

    es.indices.create(index='chronicle_'+gameid, body=chronicle_es_body, ignore=400)

    for cfg in configs:
        if 'event' in cfg:
            rs_cfg = redshift_cfg[gameid][cfg['db']]
            db_config = "redshift+psycopg2://{user}:{pw}@{cluster}.us-west-2.redshift.amazonaws.com:5439/{db}".format(cluster=rs_cfg.get('cluster', 'bicluster.cpaytjecvzyu'), user=rs_cfg.get('user', 'biadmin'), pw = rs_cfg.get('pw', 'Halfquest_2014'), db=rs_cfg.get('db', 'ffs'))
            engine = create_engine(db_config)
            try:
                sql_to_es(es, cfg['sql'], engine, event=cfg['event'], index='chronicle_'+gameid)
            except:
                print 'SQL to ES error:'
                print sys.exc_info()

if __name__ == '__main__':
    test_flag = False
    if len(sys.argv)>1:
		if sys.argv[1]=='test':
			test_flag = True
		if sys.argv[-1]!='test':
			fn = sys.argv[-1]

    for gameid in redshift_cfg:
        main(test_flag, gameid)
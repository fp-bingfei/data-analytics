import os
from app import create_app
import sys
from gevent.wsgi import WSGIServer

if __name__ == '__main__':
	if len(sys.argv)>1:
		config = sys.argv[1]
	else:
		config = None

	app = create_app(config)
	app.debug = True
	http_server = WSGIServer(('0.0.0.0', 5000), app)
	http_server.serve_forever()
	#app.run(host='0.0.0.0', threaded=True, debug=True)
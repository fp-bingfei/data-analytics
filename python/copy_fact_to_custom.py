###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse

import psycopg2
import yaml
import pprint
import argparse


class CopyFactToCustom:
    def __init__(self, args):
        self.args = args

    def run(self):
        self.setDBParams()
        conn = self.getConnection()
        if self.args['pre_sql'] != None:
            self.execPreSql(conn)
        self.unload(conn)
        self.emptyCustomData(conn)
        self.copy(conn)

    # empty the custom data this date
    def emptyCustomData(self, conn):
        deleteSql = "delete from %s where date='%s';" % (self.args['customTable'], self.args['date'])
        cursor = conn.cursor()
        cursor.execute(deleteSql)
        print ("\ndelete sql executed: " + deleteSql)

    def getCommandOptions(self):
        commandOptions = self.args['command_options'].split(',')
        commandOptionStr = ''
        for commandOpt in commandOptions:
            cleanStr = commandOpt.strip()
            # - added single quote for string starts with s3://
            commandOptionStr += (' ' + '\'' + cleanStr + '\'') if cleanStr.startswith('s3://') else (' ' + cleanStr)
        return commandOptionStr

    def copy(self, conn):
        query = "COPY %s FROM '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' %s maxerror %s" % (
            self.args['customTable'], self.args['s3path'], self.aws_access_key_id, self.aws_secret_access_key,
            self.getCommandOptions(), (str(max(int(self.args['maxerror']), 100)) if (
                self.args['maxerror'] and self.args['maxerror'].isdigit()) else '100'))
        print("\nquery:" + query)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        print("\ncommit above query")

    def unload(self, conn):
        selectSql = "SELECT * FROM %s WHERE date=\\'%s\\'" % (self.args['table'], self.args['date'])
        query = "UNLOAD ('%s') TO '%s' CREDENTIALS 'aws_access_key_id=%s;aws_secret_access_key=%s' " % (
            selectSql, self.args['s3path'], self.aws_access_key_id, self.aws_secret_access_key)
        print("\nupload query: " + query)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        print("\ncommit upload query")

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
            self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']


def parse_args():
    parser = argparse.ArgumentParser(description='Copy fact table to custom')
    parser.add_argument('-d', '--date', help='Date format YYYY-mm-dd, which day want to copy out', required=True)
    parser.add_argument('-c', '--conf', help='Config file in YAML format want to copy out', required=True)
    parser.add_argument('-t', '--table', help='RedShift DB table name want to copy out. e.g. schema_name.table_name',
                        required=True)
    parser.add_argument('-s3path', '--s3path', help='The s3path of data source', required=True)
    parser.add_argument('-customConf', '--customConf', help='Config file in YAML format want to copy in', required=True)
    parser.add_argument('-customTable', '--customTable',
                        help='RedShift DB table name want to copy in. e.g. schema_name.table_name', required=True)
    parser.add_argument('-pre-sql', '--pre-sql',
                        help='SQL statement executed prior to data loading. e.g. delete from table where ...',
                        required=False)
    parser.add_argument('-command-options', '--command-options',
                        help='The COPY command options e.g. json,jsonpath,gzip or delimiter,\'\\t\',gzip',
                        required=True)
    parser.add_argument('-maxerror', '--maxerror', help='The max error can be ignored, default is 100', required=False)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    c2c = CopyFactToCustom(args)
    c2c.run()


if __name__ == '__main__':
    main()

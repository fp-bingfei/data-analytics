import boto
from boto.sts import STSConnection
from boto.s3.connection import S3Connection
import argparse
import os
import re
# The calls to AWS STS AssumeRole must be signed using the access key ID and secret
# access key of an IAM user or using existing temporary credentials. (You cannot call
# AssumeRole using the access key for an account.) The credentials can be in 
# environment variables or in a configuration file and will be discovered automatically
# by the STSConnection() function. For more information, see the Python SDK 
# documentation: http://boto.readthedocs.org/en/latest/boto_config_tut.html

files_types = {
    "experiments" : "experiments.tsv",
    "variants" : "variants.tsv",
    "data" : "0",
    "all" : ""
    
}

def main():
    args = parse_args()

    sts_connection = STSConnection(
        aws_access_key_id="AKIAJIXNS6MDKWNPEW3Q",
        aws_secret_access_key="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"
    )
    assumedRoleObject = sts_connection.assume_role(
        role_arn="arn:aws:iam::698279275790:role/funplus",
        role_session_name="AssumeRoleSession1",
        external_id="ExportedTestInfo"
    )

    # Use the temporary credentials returned by AssumeRole to call Amazon S3  
    # and list all buckets in the account that owns the role (the trusting account)
    s3_connection = S3Connection(
        aws_access_key_id=assumedRoleObject.credentials.access_key,
        aws_secret_access_key=assumedRoleObject.credentials.secret_key,
        security_token=assumedRoleObject.credentials.session_token
    )

    output_dir = args.get("output")
    
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        
    dt = args.get("dt")
    env = args.get("env")
    if args.get("type") not in files_types:
        print "ERROR: data_type: " + args.get("type")  + " not found in file_types"
        exit(1)
    
    data_type = files_types.get(args.get("type"))
    bucket = s3_connection.get_bucket("funplus-export")

    pattern = env + "/" + dt + "/" + data_type 
    hasData = False

    for item in bucket.list(pattern):
        hasData = True
        print "Downloading: " + item.key
        filename_arr = str(item.key).split("/")
        local_file = os.path.join(output_dir,filename_arr[len(filename_arr)-1])
        item.get_contents_to_filename(local_file)
        print "Dowloaded: " + item.key + " to " + local_file

    if (hasData == False):
        raise Exception ("No data found for: " + pattern)

def parse_args():
    parser = argparse.ArgumentParser(description='MAT/Hasoffer Install Source Downloader')
    parser.add_argument('-app','--app', help='The app(s) for which the data will be fetched. Specify multiple apps separated by commas (no spaces)', required=True)
    parser.add_argument('-dt','--dt', help='The date for which the data will be fetched. YYYY/MM/DD', required=True)
    parser.add_argument('-env','--env', help='The env dir for which the data will be fetched. funplus-barnvoyage-prod', required=True)
    parser.add_argument('-output','--output', help='The output directory', required=True)
    parser.add_argument('-type','--type', help='The file types to download', required=True)
    args = vars(parser.parse_args())
    return args

if __name__ == "__main__":
    main()



import argparse
import os
from string import Template
from datetime import date, timedelta
from pyhocon import ConfigFactory

def main(args):
    header = """#!/bin/bash

export JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF-8'
    """

    condiction = Template("hadoop fs -test -e s3://com.funplus.datawarehouse/farm_1_1/events_seq/${event}/app=farm.us.prod/year=${year}/month=${month}/day=${day}")

    cleanup = Template("    hdfs dfs -rmr s3://com.funplus.datawarehouse/farm_1_1/events_daily/raw_${event}_daily/app=*/dt=${dt}/*")

    parquet = Template("""
    hdfs dfs -rmr s3://com.funplus.datawarehouse/farm_1_1/events_daily/scratch/dt=${dt}

    export SPARK_YARN_MODE=true
    /usr/lib/spark/bin/spark-submit --conf spark.streaming.stopGracefullyOnShutdown=true --driver-class-path "/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --conf "spark.executor.extraClassPath=/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --master yarn-client --driver-memory 4G --conf spark.yarn.am.memory=4G --num-executors 10 --executor-cores 7 --executor-memory 20G --jars /usr/lib/spark/auxlib/json-schema-validator-2.2.6.jar,/usr/lib/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/usr/lib/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/usr/lib/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/usr/lib/spark/extras/lib/spark-streaming-kinesis-asl.jar,/usr/lib/spark/auxlib/amazon-kinesis-client-1.2.1.jar,/usr/lib/spark/auxlib/maxmind-db-1.0.0.jar,/usr/lib/spark/auxlib/geoip2-2.1.0.jar,/usr/lib/spark/auxlib/parquet-avro-1.7.0.jar --class com.funplus.batch.EventsParquetDaily /usr/lib/spark/auxlib/kinesis-app-1.0.jar farm_parquet_daily_part_a ${dt} s3n://com.funplus.datawarehouse/farm_1_1/events_seq/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/scratch/dt=${dt}/part_a/ farm.br.prod,farm.de.prod s3n://com.funplusgame.bidata/dev/spark/farm_parquet.conf

    /usr/lib/spark/bin/spark-submit --conf spark.streaming.stopGracefullyOnShutdown=true --driver-class-path "/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --conf "spark.executor.extraClassPath=/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --master yarn-client --driver-memory 4G --conf spark.yarn.am.memory=4G --num-executors 10 --executor-cores 7 --executor-memory 20G --jars /usr/lib/spark/auxlib/json-schema-validator-2.2.6.jar,/usr/lib/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/usr/lib/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/usr/lib/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/usr/lib/spark/extras/lib/spark-streaming-kinesis-asl.jar,/usr/lib/spark/auxlib/amazon-kinesis-client-1.2.1.jar,/usr/lib/spark/auxlib/maxmind-db-1.0.0.jar,/usr/lib/spark/auxlib/geoip2-2.1.0.jar,/usr/lib/spark/auxlib/parquet-avro-1.7.0.jar --class com.funplus.batch.EventsParquetDaily /usr/lib/spark/auxlib/kinesis-app-1.0.jar farm_parquet_daily_part_b ${dt} s3n://com.funplus.datawarehouse/farm_1_1/events_seq/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/scratch/dt=${dt}/part_b/ farm.fr.prod,farm.it.prod,farm.nl.prod s3n://com.funplusgame.bidata/dev/spark/farm_parquet.conf

    /usr/lib/spark/bin/spark-submit --conf spark.streaming.stopGracefullyOnShutdown=true --driver-class-path "/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --conf "spark.executor.extraClassPath=/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --master yarn-client --driver-memory 4G --conf spark.yarn.am.memory=4G --num-executors 10 --executor-cores 7 --executor-memory 20G --jars /usr/lib/spark/auxlib/json-schema-validator-2.2.6.jar,/usr/lib/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/usr/lib/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/usr/lib/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/usr/lib/spark/extras/lib/spark-streaming-kinesis-asl.jar,/usr/lib/spark/auxlib/amazon-kinesis-client-1.2.1.jar,/usr/lib/spark/auxlib/maxmind-db-1.0.0.jar,/usr/lib/spark/auxlib/geoip2-2.1.0.jar,/usr/lib/spark/auxlib/parquet-avro-1.7.0.jar --class com.funplus.batch.EventsParquetDaily /usr/lib/spark/auxlib/kinesis-app-1.0.jar farm_parquet_daily_part_c ${dt} s3n://com.funplus.datawarehouse/farm_1_1/events_seq/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/scratch/dt=${dt}/part_c/ farm.us.prod s3n://com.funplusgame.bidata/dev/spark/farm_parquet.conf

    /usr/lib/spark/bin/spark-submit --conf spark.streaming.stopGracefullyOnShutdown=true --driver-class-path "/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --conf "spark.executor.extraClassPath=/home/hadoop/customsetup/guava-16.0.1.jar:/etc/hadoop/conf:/etc/hive/conf:/usr/lib/hadoop/*:/usr/lib/hadoop-hdfs/*:/usr/lib/hadoop-mapreduce/*:/usr/lib/hadoop-yarn/*:/usr/lib/hadoop-lzo/lib/*:/usr/share/aws/emr/emrfs/conf:/usr/share/aws/emr/emrfs/lib/*:/usr/share/aws/emr/emrfs/auxlib/*" --master yarn-client --driver-memory 4G --conf spark.yarn.am.memory=4G --num-executors 10 --executor-cores 7 --executor-memory 20G --jars /usr/lib/spark/auxlib/json-schema-validator-2.2.6.jar,/usr/lib/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/usr/lib/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/usr/lib/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/usr/lib/spark/extras/lib/spark-streaming-kinesis-asl.jar,/usr/lib/spark/auxlib/amazon-kinesis-client-1.2.1.jar,/usr/lib/spark/auxlib/maxmind-db-1.0.0.jar,/usr/lib/spark/auxlib/geoip2-2.1.0.jar,/usr/lib/spark/auxlib/parquet-avro-1.7.0.jar --class com.funplus.batch.EventsParquetDaily /usr/lib/spark/auxlib/kinesis-app-1.0.jar farm_parquet_daily_part_d ${dt} s3n://com.funplus.datawarehouse/farm_1_1/events_seq/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/ s3n://com.funplus.datawarehouse/farm_1_1/events_daily/scratch/dt=${dt}/part_d/ farm.pl.prod,farm.th.prod,farm.tw.prod s3n://com.funplusgame.bidata/dev/spark/farm_parquet.conf
    """)

    footer = """
    hive -f ~/parquet_daily/raw_parquet_daily.hql >parquet_daily.log 2>&1
    \nfi"""

    f = open('raw_cleanup_daily.sh', 'w')
    print >> f, header

    conf = ConfigFactory.parse_file(args['config'])
    events = conf.get('events')
    split_startdate = args['startdate'].split('-')
    startdate = date(int(split_startdate[0]), int(split_startdate[1]), int(split_startdate[2]))
    if args['enddate'] != None:
        split_enddate = args['enddate'].split('-')
        enddate = date(int(split_enddate[0]), int(split_enddate[1]), int(split_enddate[2]))
    else:
        enddate = startdate + timedelta(days=1)

    # condictions
    condictions = ''
    for event in events:
        condictions = condictions + condiction.substitute(event=event, year=split_startdate[0], month=split_startdate[1], day=split_startdate[2]) + ' || '
    print >> f, 'if %s; then\n' %condictions[:-4]

    # cleanup
    for event in events:
        daycount = 1
        curdate = startdate
        while curdate < enddate:
            curyear = curdate.year
            curmonth = curdate.month
            if curmonth < 10:
                curmonth = '0%d' %curmonth
            curday = curdate.day
            if curday < 10:
                curday = '0%d' %curday
            dt = '%s-%s-%s' %(curyear, curmonth, curday)
            print >> f, cleanup.substitute(event=event, dt=dt)
            curdate = startdate + timedelta(days=daycount)
            daycount += 1

    # parquet files
    daycount = 1
    curdate = startdate
    while curdate < enddate:
        curyear = curdate.year
        curmonth = curdate.month
        if curmonth < 10:
            curmonth = '0%d' %curmonth
        curday = curdate.day
        if curday < 10:
            curday = '0%d' %curday
        dt = '%s-%s-%s' %(curyear, curmonth, curday)
        print >> f, parquet.substitute(dt=dt)
        curdate = startdate + timedelta(days=daycount)
        daycount += 1

    print >> f, footer
    f.close()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help = 'input the path of config file', required=True)
    parser.add_argument('-s', '--startdate', help = 'input the start date of processing', required=True)
    parser.add_argument('-e', '--enddate', help = 'input the end date of processing', required=False)
    args = vars(parser.parse_args())
    return args

if __name__ == "__main__":
    args = parse_args()
    main(args)


import urllib2
import json
import datetime
import sys
import os
import argparse

class CurrencyFromWeb:
    def __init__(self, args):
        self.date = args['date']#datetime.datetime.strptime(args['date'], '%Y-%m-%d').date()
        self.prefix = '/mnt/funplus/jars/currency_staging/'
        self.suffix = ''
        self.url = 'http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json'

    def run(self):
        response = urllib2.urlopen(self.url)
        data = json.load(response)
        result = self.parse(data)
        self.save_upload(result)


    def parse(self, data):
        stringBuilder = ''
        templateString = '%s\t%s\t%s\t%.7f\n'
        for resource in data['list']['resources']:
            if resource['resource']['fields']['name'].startswith('USD') and float(resource['resource']['fields']['price']) != 0:
                currency = resource['resource']['fields']['name'].split('/')[1].strip() if len(resource['resource']['fields']['name'].split('/')) > 1 else 'USD'
                factor = 1.0 / float(resource['resource']['fields']['price'])
                stringBuilder += templateString % (str(hash(self.date + currency)), self.date, currency, factor)
                if currency == 'CNY':
                    stringBuilder += templateString % (str(hash(self.date + 'RMB')), self.date, 'RMB', factor)

        return stringBuilder.strip()

    def save_upload(self, data):
        output = self.prefix + self.date.replace('-', '') + '_currency'
        f = open(output, 'w')
        f.write(data.encode('utf8'))
        f.close()
        if os.path.exists(output + '.gz'):
            os.remove(output + '.gz')

        os.system('gzip ' + output)
        os.system('s3cmd put ' + output + '.gz s3://com.funplusgame.bidata/currency/' + self.date.replace('-', '') + '/')


def parse_args():
    parser = argparse.ArgumentParser(description='Download adjust data to BI')
    parser.add_argument('-d','--date', help='date to process, yyyy-mm-dd', required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    cfw = CurrencyFromWeb(args)
    cfw.run()

if __name__ == '__main__':
    main()
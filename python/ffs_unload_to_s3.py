#!/usr/bin/env python

import os, sys
import re
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta

months = [
    '2015-05-01',
    '2015-06-01',
    '2015-07-01']


events_table = 'public.events_raw'

events_list = [
'Achievement',
'Animal',
'barnView',
'BatchProduction',
'BatchProductionUpgrade',
'BeautyshopProcessRecord',
'BeautyshopProcessReward',
'Calendar',
'claim_daily_story',
'coins_transaction',
'Collect_Machine_Trade',
'CollectableDecoration',
'ConnectFacebook',
'DailyBonus',
'FacebookUserInfo',
'Fishingbook',
'FishingPurchase',
'FishingRecord',
'FishingStart',
'FortuneWheel',
'garden_book_star',
'garden_level',
'Getfreerc',
'Giftbox',
'item_transaction',
'ItemDrop',
'ItemPurchased',
'ItemUpgrade',
'Kitchen',
'LevelReward',
'login',
'luckypackage',
'MysteriousBox',
'Mystery_Store_Trade',
'NewOrder',
'NewOrderRefresh',
'newuser',
'OnlinePackage',
'OrderReward',
'payment',
'QuestComplete',
'RC Balloon',
'rc_transaction',
'rcspend',
'SeafoodhouseProcessRecord',
'SeafoodhouseProcessReward',
'SellProduct',
'Session_end',
'UseFarmAids',
'UseGasoline',
'UseOP',
'VisitNeighbors',
'WareHouse',
'WaterWell',
'weeklystory',
'welcomebackReward']



s3_prefix = 's3://com.funplusgame.bidata/events_raw/ffs/'
aws_access_key_id = 'AKIAJRKDNC52OAINDWKA'
aws_secret_access_key = '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'
delimiter = '^'
command_options = 'GZIP'

def gen_unload_cmd_list(unload_cmd_file):
    unload_cmd_fp = open(unload_cmd_file, 'w')
    for month in months:

        start_date = datetime.strptime(month,'%Y-%m-%d').date();
        end_date =  start_date + relativedelta(months=1)
        
        s3_date_path = start_date.strftime('%Y/%m')

        unload_cmd_fp.write("--" + str(start_date) + "\n")

        for event in events_list:
            unload_cmd = """
                unload ('select * from {events_table} where trunc(ts) >= \\'{start_date}\\' and trunc(ts) < \\'{end_date}\\' and event = \\'{event}\\';') to '{s3_prefix}{event}/{s3_date_path}/' CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}' DELIMITER '{delimiter}' {command_options};
            """.format(start_date=start_date,end_date=end_date,event=event,s3_prefix=s3_prefix,s3_date_path=s3_date_path,aws_access_key_id=aws_access_key_id,aws_secret_access_key=aws_secret_access_key,delimiter=delimiter,command_options=command_options,events_table=events_table)
            unload_cmd_fp.write(unload_cmd.strip() + '\n')
    unload_cmd_fp.close()


if __name__ == '__main__':
    unload_cmd = sys.argv[1]
    gen_unload_cmd_list(unload_cmd)

import pandas as pd
import argparse
from sqlalchemy import create_engine
from datetime import date
from pandas_to_redshift import *

#rerun is deprecated, just kept for legacy reasons to minimize disruption to current pipeline
def parse_args():
    parser = argparse.ArgumentParser(description='ffs cheater list update')
    parser.add_argument("cutoff", help='daily max in cutoff to identify as cheater', type=int)
    parser.add_argument("dau", help='check cheaters from dau for the past x days', type=int)
    parser.add_argument("--rerun", help="rerun model",action="store_true")
    args = parser.parse_args()
    return args

def update_cheater_list(args):
    ffs_conn = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster-ffs.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
    ffs_engine = create_engine(ffs_conn)

    cutoff=args.cutoff
    dau_threshold = args.dau

    sql = '''select a.*, c.max_in, c.rc_in, d.max_all, d.rc_all, current_date as first_date
			from
			(SELECT a.snsid, u.user_key, u.total_revenue_usd as rev
			FROM 
			(select distinct snsid from ffs.processed.fact_dau_snapshot where datediff('day', date, current_date)<={dau_threshold} and app='ffs.global.prod'
				and snsid not in (select snsid from processed.cheaters_new)
				) a
			left join
			processed.dim_user u
			on a.snsid = u.snsid) a
			left join
			(select user_key, max(rc_in) as max_in, sum(rc_in) as rc_in
			from
			(select date, user_key, sum(rc_in) as rc_in
			FROM ffs.processed.rc_transaction
			    where 
			    action not in ('loadPayment', 'use_gift', 'mypay', 'light_loading', 'finish_task', 'retrieve_data', 'secondFarm_loading', 'store_realMoney_buy', 'package_realMoney_buy', 'rcCow_collect')
			    and app='ffs.global.prod'
				group by 1,2)
				group by 1) c
				on a.user_key = c.user_key
				left join
			(select user_key, max(rc_in) as max_all, sum(rc_in) as rc_all
			from
			(select date, user_key, sum(rc_in) as rc_in
			FROM ffs.processed.rc_transaction
			    where 
			    app='ffs.global.prod'
				group by 1,2)
				group by 1) d
				on a.user_key = d.user_key
				where max_in>={cutoff};'''

    cheater_df = pd.read_sql_query(sql.format(dau_threshold=dau_threshold, cutoff=cutoff), ffs_engine)
    h = 'bicluster-ffs.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    df_to_redshift(cheater_df, 'processed.cheaters_new', h, 'ffs', append=True)

def main():
    args = parse_args()
    update_cheater_list(args)

if __name__ == '__main__':
    main()

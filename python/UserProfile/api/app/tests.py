from django.test import TestCase
import aerospike
from client import AerospikeClient



class AerospikeClientTests(TestCase):
    _client = None

    def setUp(self):
        print("===setUp===")
        self._client = AerospikeClient({"LOCATION": "127.0.0.1:3000", "OPTIONS": {"NAMESPACE": "userprofile", "SET": "users"}})
        print(self._client.aero_namespace)
        print(self._client.aero_set)
        if (not self._client.is_connected()):
            self._client.connect()

    def tearDown(self):
        print("===tearDown===")
        #self._client.close()

    def test_add(self):
        print("===test_add===")
        key = "41213"
        bins = {
            "id": 41213,
            "snsid": 23454656,
            "app_id": "royal.th.prod",
            "facebook_id": "johndoe@example.com",
            "birthday": "1990-03-01",
            "first_name": "Linda",
            "last_name": "Lee",
            "gender": "female",
            "country": "US",
            "email": "Linda@gmail.com",
            "locale": "en_US",
            "payment_class": "dolphin",
            "payment_behav": "biggest_discount",
            "payment_freq": "often",
            "genre": [
                "strategy",
                "simulation"
            ],
            "sociability": "low",
            "playing_habit": "hardcore",
            "conversion_t": "2015-12-28 03:36:12",
            "is_payer": 1,
            "3_days_churn": 0,
            "conversion": 1,
            "active_class": ""
        }
        result = self._client.add(key, bins)
        self.assertEqual(result, True)
    
    def test_get(self):
        print("===test_get===")
        key = "41213"
        result = self._client.get(key)
        print(result)
        self.assertNotEqual(result, None)

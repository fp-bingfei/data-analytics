import threading
import pprint
import sys
try:
    import aerospike
except ImportError:
    raise InvalidCacheBackendError(
        "Aerospike cache backend requires the 'aerospike' library")

global_client = None

lock = threading.Lock()

class AerospikeClient(object):
    _settings = None
    _server = None
    _client = None
    _timeout = 10000
    _ttl = -1

    def __init__(self, params):
        global global_client

        self._settings = params
        self._server = self._settings['LOCATION']
        if ':' in self._server:
            host, port = self._server.rsplit(':', 1)
            try:
                port = int(port)
            except (ValueError, TypeError):
                raise ImproperlyConfigured("port value must be an integer")
        else:
            host, port = '127.0.0.1', 3000

        config = {
            "hosts": [
                  ( host, port )
              ],
              "policies": {
                  #aerospike timeout has no equivalent in django cache
                  #"timeout": self.timeout # milliseconds
            }
        }

        with lock:
            if global_client is None:
                try:
                    global_client = aerospike.client(config)
                except Exception as e:
                    print("error: {0}".format(e))
        self._client = global_client

    @property
    def server(self):
        """
        the server:port combination for aerospike server
        """
        return self._server or "127.0.0.1:3000"

    @property
    def timeout(self):
        """
        The default value changed to 10 s == 10000 ms
        """
        return self._timeout

    @property
    def options(self):
        """
        The configuration options property.
        NAMESPACE - aerospike namespace name
        SET - aerospike set name
        """
        return self._settings['OPTIONS']

    @property
    def meta(self):
        """
        The meta data for the record. For now only setting the ttl value.
        0: default ttl
        -1: never expire
        """
        meta = {
            'ttl': self._ttl
        }
        return meta

    @property
    def policy(self):
        """
        The policy for the record. For now default is to to send the digest.
        """
        policy = {
            'key': aerospike.POLICY_KEY_DIGEST
        } # store the key along with the record
        return policy

    @property
    def aero_namespace(self):
        """
        The configured aerospike namespace to hold the cache.
        """
        return self.options.get('NAMESPACE', "userprofile")

    @property
    def aero_set(self):
        """
        The configured aerospike set to hold the cache.
        """
        return self.options.get('SET', "users")

    def connect(self):
        """
        Connect to the aerospike database
        """
        try:
            self._client.connect()
        except Exception as e:
            print("error: {0}".format(e))

    def is_connected(self):
        """
        Check the database connection
        """
        return self._client.is_connected()

    def close(self):
        """
        Closes the database connection
        """
        self._client.close()

    def make_key(self, key, version=None):
        """
        Constructs the aerospike key from given user key
        """
        ret_key = (self.aero_namespace, self.aero_set, key)
        return ret_key

    def add(self, key, bins, timeout=-1, version=None):
        """
        Returns True if the bins was stored, False otherwise.
        """
        aero_key = self.make_key(key, version=version)

        try:
            ret = self._client.put(aero_key, bins, self.meta, self.policy)

            if ret == 0:
                return True
        except Exception as e:
            print("error: {0}".format(e))
        return False

    def get(self, key, default=None, version=None):
        """
        Fetch a given key from the cache. If the key does not exist, return
        default, which itself defaults to None.
        """
        aero_key = self.make_key(key, version=version)

        try:
            (key, metadata, bins) = self._client.get(aero_key, self.policy)
            if bins is None:
                return default
            return bins
        except Exception as e:
            print("error: {0}".format(e))
        return default

    def set(self, key, bins, timeout=-1, version=None):
        """
        Set a bins in the cache. It is similar to add
        """
        return self.add(key, bins, timeout, version)

    def delete(self, key, version=None):
        """
        Delete a key from the cache, failing silently.
        """
        self._client.remove(self.make_key(key, version=version))

    def clear(self):
        """
        Remove *all* values from the cache at once.
        """
        #remove each record in the bin
        def callback((key, meta, bins)):
            self._client.remove(key)

        scan_obj = self._client.scan(self.aero_namespace, self.aero_set)

        scan_obj.foreach(callback)

    def has_key(self, key, version=None):
        """
        Returns True if the key is in the cache and has not expired.
        """
        meta = None
        try:
            key, meta = self._client.exists(self.make_key(key, version=version))
        except Exception as e:
            print("error: {0}".format(e))

        if meta == None:
            return False

        return True


from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse
from client import AerospikeClient

def getUserById(request):
    # Get parameters
    key = request.GET.get('id','41213')
    id_type = request.GET.get('id_type','uid')
    app_id = request.GET.get('app_id','')

    # Connect to Aerospike
    client = AerospikeClient(settings.AEROSPIKE_CLIENT['default'])
    if (not client.is_connected()):
        client.connect()
    # Get the value of the key
    result = client.get(key)

    return JsonResponse(result)

django==1.9.4
djangorestframework==3.3.3
mock==1.0.1
coverage==3.7.1
flake8==2.5.4
unittest_xml_reporting==2.0.0
httpretty==0.8.14

import urllib2
import datetime
import time
import json
import collections
import requests
import time

class FacebookAdsInsights:
    def __init__(self):
        self.account_ids = ['635934239843858', '107464062773986', '909353222501957', '873756539394959', '873744276062852']

        self.httpApi = 'https://graph.facebook.com/v2.7/'
        self.fields = ['account_id,', 'account_name,', 'action_values,', 'actions,', 'ad_id,', 'ad_name,', 'adset_id,', 'adset_name,', 'app_store_clicks,', 'buying_type,', 'call_to_action_clicks,', 'campaign_id,', 'campaign_name,', 'canvas_avg_view_percent,', 'canvas_avg_view_time,', 'clicks,', 'cost_per_10_sec_video_view,', 'cost_per_action_type,', 'cost_per_estimated_ad_recallers,', 'cost_per_inline_link_click,', 'cost_per_inline_post_engagement,', 'cost_per_total_action,', 'cost_per_unique_action_type,', 'cost_per_unique_click,', 'cost_per_unique_inline_link_click,', 'cpc,', 'cpm,', 'cpp,', 'ctr,', 'date_start,', 'date_stop,', 'deeplink_clicks,', 'estimated_ad_recall_rate,', 'estimated_ad_recallers,', 'frequency,', 'impressions,', 'inline_link_click_ctr,', 'inline_link_clicks,', 'inline_post_engagement,', 'newsfeed_avg_position,', 'newsfeed_clicks,', 'newsfeed_impressions,', 'objective,', 'place_page_name,', 'reach,', 'relevance_score,', 'social_clicks,', 'social_impressions,', 'social_reach,', 'social_spend,', 'spend,', 'total_action_value,', 'total_actions,', 'total_unique_actions,', 'unique_actions,', 'unique_clicks,', 'unique_ctr,', 'unique_impressions,', 'unique_inline_link_click_ctr,', 'unique_inline_link_clicks,', 'unique_link_clicks_ctr,', 'unique_social_clicks,', 'unique_social_impressions,', 'video_10_sec_watched_actions,', 'video_15_sec_watched_actions,', 'video_30_sec_watched_actions,', 'video_avg_pct_watched_actions,', 'video_avg_percent_watched_actions,', 'video_avg_sec_watched_actions,', 'video_avg_time_watched_actions,', 'video_complete_watched_actions,', 'video_p100_watched_actions,', 'video_p25_watched_actions,', 'video_p50_watched_actions,', 'video_p75_watched_actions,', 'video_p95_watched_actions,', 'website_clicks,', 'website_ctr']

        self.params = [{'action_attribution_windows': [["1d_view","28d_click"], '7d_click'],
                        'action_report_time': 'impression', 
                        'breakdowns': [['age','gender'], 'country'],
                        'level': ['ad'], #do we need others, adset, campaign, account
                        'time_increment': 1,
                        #'time_range': {'since': args['start_date'], 'until': args['end_date']}
                        'date_preset': 'last_3_days'
                        }]

        self.access_token = 'EAABt6nzdyE4BAJBskiaceuCzVlxblB0NFZBP4q6FzWPE6NZCLLmI5lPTsKX2tRjQszmJuPnuP1AJoh5fdLZCiZAhOg4TyWisciQKAr2HrhSSPkkUdBNpzL1TwwGJt4iiZCFfiDiZCM8NQLgoi6vnIJmRojTiLWFpEZD'
# recommended fields
# account_id, account_name, ad_id, ad_name, adset_id, adset_name, app_store_clicks, buying_type, campaign_id, campaign_name, clicks, date_start, date_stop, deeplink_clicks?, frequency?, impressions, objective, reach, relevance_score(only for ads), spend, unique_clicks?, total_actions??, unique_impressions, website_clicks
# actions?
# action_values?
# 1d_view/28d_click, 7d_click

# country,
# gender,age
# impression_device, placement


    def reports_gen(self):
        #'''POST to https://graph.facebook.com/v2.7//{graph_ad_account_node_id}/insights to start a async report job, reference https://developers.facebook.com/docs/marketing-api/reference/ad-report-run'''
        url = self.httpApi + 'act_%s/insights'
        payload = {
        # how to read the values in self.params?
            'action_attribution_windows': '7d_click',
            'level': 'ad',
            'time_increment': 1,
            #'time_range': {'since': args['start_date'], 'until': args['end_date']},
            'date_preset': 'last_3_days',
            'breakdowns': 'country',
            'fields': ''.join(self.fields),
            'access_token': self.access_token
        }

        for account_id in self.account_ids:
            r = requests.post(url % account_id, data = payload)
            print r.text

            res = self.poll_status(json.loads(r.text)['report_run_id'])
            if res == -1:
                print 'Generate report failed for account: %s' % account_id

        return 0

    def poll_status(self, report_run_id):
        #'''GET https://graph.facebook.com/v2.7/{report_run_id} to fetch the status of the report, download report when finish'''
        if report_run_id is None:
            print 'OOOPS No valid reports ID'
            return -1

        url = self.httpApi + '%s?access_token=%s' % (report_run_id, self.access_token)
        i = 5
        while i > 0:
            r = requests.get(url)
            if (json.loads(r.text)['async_status'] != "Job Completed"):
                print 'Current status: %d percent is done!' % json.loads(r.text)['async_percent_completion']
            else:
                print "Start downloading~"
                t = self.download_report(report_run_id, json.loads(r.text)['account_id'])
                break

            i -= 1
            print 'The %dth try, sleeping .....' % i
            time.sleep(30)

        return 1


    def download_report(self, report_run_id, account_id):
        #'''download the https://www.facebook.com/ads/ads_insights/export_report/?report_run_id={report_run_id}&name={get_a_cool_name}&format=csv'''

        url = 'https://www.facebook.com/ads/ads_insights/export_report/?report_run_id=%s&name=FB_MKT_%s&format=csv&access_token=%s' % (report_run_id, account_id, self.access_token)

        file_name = '/tmp/FB_MKT/FB_MKT_%s.csv' % account_id
        u = urllib2.urlopen(url)
        f = open(file_name, 'wb')
        meta = u.info()
        file_size = int(meta.getheaders("Content-Length")[0])
        print "Downloading: %s Bytes: %s" % (file_name, file_size)

        file_size_dl = 0
        block_sz = 8192
        while True:
            buffer = u.read(block_sz)
            if not buffer:
                break
            file_size_dl += len(buffer)
            f.write(buffer)
            status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
            status = status + chr(8)*(len(status)+1)
            print status,

        f.close()

        return 1

def main():
    fai = FacebookAdsInsights()
    fai.reports_gen()

if __name__ == '__main__':
    main()

# def parse_args():
#     parser = argparse.ArgumentParser(description='Download adjust data to BI')
#     parser.add_argument('-st','--start_date', help='The start time of the report, YYYY-mm-dd', required=True)
#     parser.add_argument('-et','--end_date', help='The end time of the report, YYYY-mm-dd', required=True)
#     args = vars(parser.parse_args())
#     return args

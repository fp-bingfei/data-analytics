###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse

import psycopg2
import yaml
import pprint
import argparse
import os
import boto
import datetime

from boto.s3.key import Key


class FFSEventsCount:
    def __init__(self, args):
        self.args = args

    def run(self):
        self.setDBParams()
        conn = self.getConnection()
        print self.args
        date = self.args['date']

        tmpFilePath = '/tmp/ffs_' + date + '_hive.table'
        if os.path.isfile(tmpFilePath) == True:
            os.remove(tmpFilePath)
        tmpFile = open(tmpFilePath, 'a')
        eventsArr = ['login', 'payment', 'newuser']

        for hour in range(24):
            thisHour = self.intToHour(hour)
            outputStr = str(hour) + '\t'
            for event in eventsArr:
                selectSql = "select count(*) from public.events_raw where event='" + event + "' and ts >='" + date + " " + thisHour + ":00:00' and ts <= '" + date + " " + thisHour + ":59:59';"
                hourEventCount = self.execSql(conn, selectSql)[0]
                outputStr += (str(hourEventCount) + '\t')
            print outputStr
            outputStr = str.rstrip(outputStr) + '\n'
            tmpFile.write(outputStr)
        dayOutputStr = '24\t'

        for event in eventsArr:
            selectSql = "select count(*) from public.events_raw where event='" + event + "' and ts >='" + date + " 00:00:00' and ts <= '" + date + " 23:59:59';"
            dayEventCount = self.execSql(conn, selectSql)[0]
            dayOutputStr += (str(dayEventCount) + '\t')

        print dayOutputStr
        tmpFile.write(str.rstrip(dayOutputStr))
        tmpFile.close()

        self.putFileToS3(tmpFilePath)

    def intToHour(self, intHour):
        if intHour < 10:
            strHour = '0' + str(intHour)
        else:
            strHour = str(intHour)

        return strHour

    def execSql(self, conn, sql):
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchone();
        return data

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
        self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']

    def putFileToS3(self, localFilePath):
        s3_conn = boto.connect_s3()
        bucket = s3_conn.get_bucket(self.args['s3bucket'])

        s3Key = Key(bucket)
        s3Key.name = "%s/%s" % (self.args['s3path'], 'hive.table')
        s3Key.set_contents_from_filename(localFilePath)
        print "########"
        print s3Key
        s3_conn.close()


def parse_args():
    parser = argparse.ArgumentParser(description='Copy file into Redshift')
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-d', '--date', help='Schedule date, format is YYY-MM-dd', required=True)
    parser.add_argument('-s', '--s3bucket', help='S3 bucket where the files', required=True)
    parser.add_argument('-p', '--s3path', help='S3 path where the file put', required=True)

    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    c2r = FFSEventsCount(args)
    c2r.run()


if __name__ == '__main__':
    main()

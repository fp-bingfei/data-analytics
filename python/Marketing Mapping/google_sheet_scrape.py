# -*- coding: utf-8 -*-
from __future__ import print_function
import httplib2
import os
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
import yaml
import argparse
import pandas as pd
import pandas.io.sql as sql
import psycopg2 as pg
from decimal import *

# Get entry of kpi-diandian cluster

class gs_data:
    def __init__(self,args):
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?version=v4')
        self.service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)
        self.args = args
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        # redshift connections
        self.db_host = confMap['tasks']['redshift']['db_host']
        self.db_port = confMap['tasks']['redshift']['db_port']
        self.db_name = confMap['tasks']['redshift']['db_name']
        self.db_username = confMap['tasks']['redshift']['db_username']
        self.db_password = confMap['tasks']['redshift']['db_password']
        self.sql = confMap['tasks']['redshift']['sql']

        # google sheet information
        self.CLIENT_SECRET_FILE = confMap['tasks']['googlesheet']['CLIENT_SECRET_FILE']
        self.SCOPES = confMap['tasks']['googlesheet']['SCOPES']
        self.APPLICATION_NAME = confMap['tasks']['googlesheet']['APPLICATION_NAME']
        self.spreadsheetId = confMap['tasks']['googlesheet']['spreadsheetId']
        self.sheet_name = confMap['tasks']['googlesheet']['sheet_name']
        self.sheet_range = confMap['tasks']['googlesheet']['sheet_range']
        self.items = confMap['tasks']['googlesheet']['items']
        self.values = confMap['tasks']['googlesheet']['values']

    def run(self):
        self.insert_tosql()
        # self.get_sheetvalue()

    # Get oauth credential json file from google dev tool
    def get_credentials(self):
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'sheets.googleapis.com-python-quickstart.json')
        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
            flow.user_agent = self.APPLICATION_NAME
            credentials = tools.run_flow(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def insert_tosql(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = pg.connect(conn_string)
        cur = conn.cursor()
        cur.execute(self.sql)
        sheet_data = self.get_sheetvalue()
        conn.commit()
        for i in range(len(sheet_data)):
            query = 'INSERT INTO dragonwar.roi_cost values('+ ','.join(['%s' for number in range(len(self.items))]) +')'

            data = tuple([sheet_data[i][j] for j in range(len(self.items))])
            cur.execute(query, data)
            conn.commit()

    def get_sheetvalue(self):
        rangeName= self.sheet_name + '!'+ self.sheet_range
        print(rangeName)
        raw_result = self.service.spreadsheets().values().get(spreadsheetId=self.spreadsheetId, range=rangeName).execute()
        sheet_value = raw_result.get('values',[])
        return sheet_value

def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    gs = gs_data(args)
    gs.run()

if __name__ == '__main__':
    main()
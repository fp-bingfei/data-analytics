# -*- coding: utf-8 -*-
from __future__ import print_function
import httplib2
import os
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
import pandas as pd
import pandas.io.sql as sql
import psycopg2 as pg

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# Get entry of kpi-diandian cluster
class diandian_cluster(object):
    db_host = 'kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    db_port = '5439'
    db_username = 'biadmin'
    db_password = 'Halfquest_2014'
    db_name = 'kpi'
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)

class funplus_cluster(object):
    db_host = 'kpi-funplus.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    db_port = '5439'
    db_username = 'biadmin'
    db_password = 'Halfquest_2014'
    db_name = 'kpi'
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)

class singular:
    def __init__(self,args):
        self.SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
        self.CLIENT_SECRET_FILE = '/Users/funplus/Documents/workspace/singular_project/client_secret.json'
        self.APPLICATION_NAME = 'Google Sheets API Quickstart'
        self.spreadsheetId = '1aLM1z5ZoK_Gudny36mDaMAT28TvVjdX5DUTAeA0B2pA'
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?version=v4')
        self.service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)
        self.input_sql_path = '/Users/funplus/PycharmProjects/install_source/'
        self.cluster = 'funplus' if args['cluster'] is None else args['cluster']

    def run(self):
        sheet_list = self.get_sheetlist()
        file_list = []
        if self.cluster == 'diandian':
            for sheet_name in sheet_list:
                print(sheet_name)
                sql_file = self.generate_sql(self.cluster, sheet_name)
                file_list.append(sql_file)
            self.output('/n'.join(file_list))
        else:
            for sheet_name in sheet_list:
                print(sheet_name)
                sql_file = self.generate_sql(self.cluster, sheet_name)
                file_list.append(sql_file)
            self.output('/n'.join(file_list))

    def output(self,file):
        fd = open(self.input_sql_path+self.cluster+'_agg_marketing.sql','w')
        fd.write(file)
        fd.close

    def redshift(self):
        if self.cluster == 'diandian':
            conn = diandian_cluster.conn_string
        else:
            conn = funplus_cluster.conn_string
        return conn

    # Get oauth credential json file from google dev tool
    def get_credentials(self):
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'sheets.googleapis.com-python-quickstart.json')
        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
            flow.user_agent = self.APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else:  # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def get_sheetlist(self):
        sheet_metadata = self.service.spreadsheets().get(spreadsheetId=self.spreadsheetId).execute()
        sheets = sheet_metadata.get('sheets', '')
        # Create dict for singular mapping sheets
        sheet_list = []
        for sheet in sheets:
            title = sheet.get("properties", {}).get("title", "Sheet1")
            sheet_list.append(title)
        return sheet_list

    def get_sheetvalue(self, sheet_name):
        # install source group mapping
        rangeName= sheet_name + '!A3:B'

        # Get raw value from google sheet
        raw_result = self.service.spreadsheets().values().get(spreadsheetId=self.spreadsheetId, range=rangeName).execute()
        map_raw = raw_result.get('values',[])

        # Create mapping dictionary
        map_dict = {}
        for row in map_raw:
            install_source_group = row[0]
            install_source = row[1]
            map_dict.setdefault(install_source_group, [])
            map_dict[install_source_group].append(install_source)

        # Create case when str in sql format
        sql_str = []
        group_list = [k for k, v in map_dict.items()]
        source_list = [v for k, v in map_dict.items()]
        for i in range(len(map_dict)):
            install_source = "'"+ "','".join(a for a in source_list[i])+"'"
            case_str = "when install_source in ({0}) then {1}".format(install_source, "'"+group_list[i]+"'")
            sql_str.append(case_str)

        sql_str = "case "+ " ".join(sql_str) + " else 'Other Install Source' end as install_source_group"
        print(sql_str)
        return sql_str

    def read_sql(self):
        fd = open(self.input_sql_path + self.cluster+'_input.sql', 'r')
        sql_file = fd.read()
        fd.close()
        return sql_file

    def generate_sql(self,cluster,sheet_name):
        sql_raw = self.read_sql()
        print(self.get_sheetvalue(sheet_name))
        sql_file = sql_raw.format(self.get_sheetvalue(sheet_name))
        return sql_file

def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-c','--cluster', help='which cluster you choose to run: Default is funplus', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    sg = singular(args)
    sg.run()

if __name__ == '__main__':
    main()
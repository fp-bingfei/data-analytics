###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse

import psycopg2
import yaml
import pprint
import argparse
import os
import boto
import datetime

from boto.s3.key import Key


class FFSRecordsCount:
    def __init__(self, args):
        self.args = args

    def run(self):
        self.setDBParams()
        conn = self.getConnection()
        #print self.args
        date = self.args['date']

        selectSql = "select count(*) from processed.tab_marketing_kpi where install_date='" + date + "';"
        recordCount = self.execSql(conn, selectSql)[0]
        #print recordCount
        return recordCount

    def execSql(self, conn, sql):
        cursor = conn.cursor()
        cursor.execute(sql)
        data = cursor.fetchone();
        return data

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
        self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']

def parse_args():
    parser = argparse.ArgumentParser(description='Copy file into Redshift')
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-d', '--date', help='Schedule date, format is YYY-MM-dd', required=True)

    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    c2r = FFSRecordsCount(args)
    recordCount = c2r.run()
    # Check records count
    if recordCount > 0:
        exit(0)
    else:
        exit(1)


if __name__ == '__main__':
    main()


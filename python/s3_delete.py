import argparse
import boto
# Create by Zhenxuan 2016/03/03
# Used to delete versioned keys in a bucket
# If you want to delete s3://com.funplusgame.bidata/samples/*/*/2016/02/*
# python s3_delete.py --s3bucket=com.funplusgame.bidata --prefix=samples --suffix=2016/02/

# If you want to delete s3://com.funplusgame.bidata/samples/*/*/*/2016/02/*
# python s3_delete.py --s3bucket=com.funplusgame.bidata --prefix=samples/ --suffix=2016/02/

# If you want to delete a prefix
# python s3_delete.py --s3bucket=com.funplusgame.bidata --prefix=samples/


class S3Delete:

    def __init__(self, args):
        self.args = args

    def run(self):
        bucket = self.getBucket()
        self.deletePrefix(bucket)

    def getBucket(self):
        conn = boto.connect_s3()
        bucket = conn.get_bucket(self.args['s3bucket'])
        return bucket

    def deletePrefix(self, bucket):        

        if (self.args['suffix'] is not None):
            #print self.args['suffix']
            layer0_prefixs = bucket.list(prefix = self.args['prefix'], delimiter = '/')
            for layer0_prefix in layer0_prefixs:
            	#print  'layer0_prefixs: ' + layer0_prefix.name
                layer1_prefixs = bucket.list(prefix = layer0_prefix.name, delimiter = '/')
                for layer1_prefix in layer1_prefixs:
                    #print 'layer1_prefixs: ' + layer1_prefix.name
                    layer2_prefixs = bucket.list(prefix = layer1_prefix.name, delimiter = '/')
                    for layer2_prefix in layer2_prefixs:
                        key_with_suffix = layer2_prefix.name + self.args['suffix']
                        #print 'The prefix will be deleted is: ' + key_with_suffix
                        keys = bucket.list_versions(prefix = key_with_suffix)
                        self.deleteKeys(keys, bucket)
        else:
            keys = bucket.list_versions(prefix = self.args['prefix'])
            self.deleteKeys(keys, bucket)


    def deleteKeys(self, keys, bucket):
        i = 0
        for key in keys:
            if i % 1000 == 0:
                print 'Deleted %d files!' % i
            bucket.delete_key(key.name, version_id = key.version_id)
            i += 1


def parse_args():
    parser = argparse.ArgumentParser(description='Delete versioned files')
    parser.add_argument('-s3bucket', '--s3bucket', help='The bucket where the key is in', required=True)
    parser.add_argument('-prefix', '--prefix', help='delete all data of certain prefix', required=True)
    parser.add_argument('-suffix', '--suffix', help='delete all data of the prefix and suffix', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    s3d = S3Delete(args)
    s3d.run()

if __name__ == '__main__':
    main()

import argparse
import string
import yaml


class GenPartitionDaily(object):
    def __init__(self, args):
        self.args = args
    
    def run(self):
        header = '''
USE ${hive_db}; 

SET hive.exec.dynamic.partition.mode=nonstrict;
SET parquet.compression=SNAPPY;
SET parquet.enable.dictionary=true;
SET parquet.page.size=1048576;
SET parquet.block.size=134217728;
SET mapred.max.split.size = 134217728;
SET hive.mapred.reduce.tasks.speculative.execution=false;
SET hive.exec.max.dynamic.partitions=4000;
SET hive.exec.max.dynamic.partitions.pernode=400;
'''
        
        partition = "ALTER TABLE raw_events_daily ADD IF NOT EXISTS PARTITION (event='${event}',app='${app}',dt='${dt}');"
        
        footer = '''
EXIT;
'''

        conf_file = open(self.args["conf"], "r")
        confMap = yaml.safe_load(conf_file)
        conf_file.close()
        hive_db = confMap["hive_db"]
        events = confMap["events_list"]
        apps = confMap["apps_list"]
        dt = self.args["date"]

        f = open('raw_events_daily.hql','wb')
        print >> f, string.Template(header).substitute(hive_db=hive_db)

        for event in events:
            for app in apps:
                print >> f, string.Template(partition).substitute(event=event, app=app, dt=dt)

        print >> f, footer
        f.close()

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--date',help = 'input the date for partition', required = True)
    parser.add_argument('-c','--conf',help = 'input the config YAML file path', required = True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    gpd = GenPartitionDaily(args)
    gpd.run()

if __name__ == '__main__':
    main()

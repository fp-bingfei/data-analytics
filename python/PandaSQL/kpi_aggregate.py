__author__ = 'jiating'

from sqlalchemy import text
from pandasql import *
import time

kpi_conn = "redshift+psycopg2://biadmin:Halfquest_2014@kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi"
kpi_engine = create_engine(kpi_conn)

dau_snapshot_table = 'kpi_processed.fact_dau_snapshot_temp'
dim_user_table = 'kpi_processed.dim_user_test'
agg_kpi_table = "kpi_processed.agg_kpi_test"

# prepare date/single game data conditions
rerun_days = 3


# list of datetime conditions. choose your own column.
def time_cond(rerun_days=3, col='date'):
    dc = ['datediff(day, {1}, CURRENT_DATE)<={0}'.format(rerun_days, col), '{0}<CURRENT_DATE'.format(col)]
    return dc


date_cond = time_cond(rerun_days)
# onegame_date_cond = date_cond + ["app_id like 'ffs%%'"]

# list of sqls to be executed
pipeline_sql_text = []

# delete + insert to agg_kpi
delete_agg_kpi_sql = DataTable.delete(agg_kpi_table, time_cond(rerun_days, 'date'))
pipeline_sql_text.append(sqlparse.format(delete_agg_kpi_sql, reindent=True))

agg_kpi_cols = ['date', 'app_id', 'device_id', 'app_version', 'install_source_group', 'install_source', 'level_end',
                'browser', 'country', 'os', 'language', 'is_new_user', 'is_payer', 'new_user_cnt', 'dau_cnt',
                'newpayer_cnt', 'payer_today_cnt', 'payment_cnt', 'revenue_usd', 'session_cnt', 'playtime_sec',
                'scene', 'revenue_iap', 'revenue_ads']

install_source_group = "case when app_id like 'farm%' then install_source_group else install_source end"
skeleton = DataTable(dau_snapshot_table).join(DataTable(dim_user_table),
                                              on=['t1.user_key=t2.user_key'],
                                              select='t1.*, t2.install_source, t2.install_source_group',
                                              where=date_cond,
                                              how='left') \
    .groupby(['date', 'app_id', 'device_id', 'app_version', {install_source_group: "install_source_group"},
              'install_source', 'level_end', 'browser', 'country', 'os', 'language',
              'is_new_user', 'is_payer', 'scene'],
             agg=[{'sum(is_new_user)': 'new_user_cnt'}, {'count(distinct user_key)': 'dau_cnt'},
                  {'sum(is_converted_today)': 'newpayer_cnt'},
                  {'sum(case when revenue_usd > 0 then 1 else 0 end)': 'payer_today_cnt'},
                  {'sum(coalesce(payment_cnt,0))': 'payment_cnt'}, {'sum(coalesce(revenue_usd,0))': 'revenue_usd'},
                  {'sum(coalesce(session_cnt,0))': 'session_cnt'}, {'sum(coalesce(playtime_sec,0))': 'playtime_sec'},
                  {'sum(coalesce(revenue_iap,0))': 'revenue_iap'}, {'sum(coalesce(revenue_ads,0))': 'revenue_ads'}],
             namefirst=False)

pipeline_sql_text.append(
    sqlparse.format(skeleton.select(columns=agg_kpi_cols).insert(agg_kpi_table, columns=agg_kpi_cols), reindent=True))

print "#### \n"
print pipeline_sql_text

# execute generated sql script
begin_ts = time.time()
for i, q in enumerate(pipeline_sql_text):
    with kpi_engine.begin() as conn:
        start_ts = time.time()
        r = conn.execute(text(q).execution_options(autocommit=True))
        end_ts = time.time()
        print "Executed query #{0}:".format(str(i))
        print q
        print "Time elapsed: {0}s".format(str(int(end_ts-start_ts)))
        print "Rows affected: {0}".format(str(r.rowcount))

print "Total time elaspsed: {0}s".format(str(int(time.time()-begin_ts)))

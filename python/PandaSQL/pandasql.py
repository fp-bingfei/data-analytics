from numbers import Number
from sqlalchemy import create_engine
import pandas as pd
import copy
import sqlparse
import unittest

class DataTable:
    @staticmethod
    def wrap(string, l="(", r=")"):
        return l+string+r
    
    @staticmethod
    def jsonx(*args):
        j = 'json_extract_path_text({0}, {1})'
        base = ''
        if len(args)>=2:
            base = j.format(args[0], "'"+args[1]+"'")
            for a in args[2:]:
                base = j.format(base, "'"+a+"'")
        return base
    
    #columns is a list. each element contains either a string or a key-value.
    @staticmethod
    def rename_cols(columns, alias):
        new_cols = []
        old_cols = []
        for i, c in enumerate(columns):
                if isinstance(c, dict):
                    new_cols.append(c.items()[0][0]+" as "+ c.items()[0][1])
                    old_cols.append(c.items()[0][0])
                elif isinstance(c, basestring):
                    new_cols.append(c)
                    old_cols.append(c)
        cols = ', '.join([alias+x for x in new_cols])
        
        return cols, old_cols        
    
    #initialize with string that represents a table
    def __init__(self, tbl):
        self.table = tbl
        self.alias = None
        self.columns = []
        
    def copy(self):
        return copy.deepcopy(self)
    
    def rename(self, new_alias):
        self.alias = new_alias
        return self
        
    def optionalWrap(self):
        if self.table.startswith('select'):
            return self.wrap(self.table)
        return self.table
            
    def select(self, n=None, columns=[]):
        limit_expr = ''
        if n:
            limit_expr = ' limit {n}'.format(n=n)
        
        colstr = '*'
        #cols is of the form ['date', {'app_id': 'app'}, 'level'...]
        alias = ''
        #if self.alias:
        #    alias = self.alias+'.'
        
        cols = []
        if len(columns)>0 and isinstance(columns, list):
            colstr, cols = self.rename_cols(columns, alias)
            if len(self.columns)>0 and colstr!='*':
                extracols = [x for x in cols if x not in self.columns]
                if len(extracols)>0:
                    raise NameError('{0} are not in the table!'.format(','.join(extracols)))
            
        if self.table.startswith('select * from'):
            new_table = self.table.replace('select * from', 'select {0} from'.format(colstr))+limit_expr
        else:
            new_table = "select {cols} from {t}{lt}".format(cols=colstr, t=self.optionalWrap(), lt=limit_expr)
        copy = self.copy()
        copy.table = new_table
        
        if len(cols)>0 and isinstance(columns, list):
            copy.columns = cols
        return copy
    
    #wherelist can have the following form: ["date<='2016-05-01", {'level': 20}, {'action': ['xyz', 'pqr']},
    #                                          {'subtype':'item', 'field1': ['Almond', 'gold']}]
    #dict that contains more than 1 key implies OR. each element in the list represents AND.
    @classmethod
    def gen_where_expr(cls, wherelist):
        where_expr_list = []
        for w in wherelist:
            if isinstance(w, dict):
                tmp = []
                for k, v in w.items():
                    if isinstance(v, list):
                        v_clone = v[:]
                        for i in range(len(v_clone)):
                            if isinstance(v[i], Number):
                                v_clone[i] = str(v_clone[i])
                            else:
                                v_clone[i] = cls.wrap(v_clone[i], "'", "'")
                                
                        tmp.append(k+" in ("+','.join(v_clone)+")")
                    elif isinstance(v, basestring):
                        tmp.append(k+"='"+v+"'")
                    elif isinstance(v, Number):
                        tmp.append(k+"="+str(v))
                        
                tmp_joined = ' OR '.join(tmp)
                if len(tmp)>1:
                    tmp_joined = cls.wrap(tmp_joined)
                where_expr_list.append(tmp_joined)
            elif isinstance(w, basestring):
                where_expr_list.append(w)
                
        return ' AND '.join(where_expr_list)
    
    def where(self, wherelist):
        if not isinstance(wherelist, list):
            wherelist = [wherelist]
        where_expr = self.gen_where_expr(wherelist)
        new_table = "select * from {t} where {where_expr}".format(t=self.optionalWrap(), where_expr=where_expr)
        copy = self.copy()
        copy.table = new_table
        return copy
    
    @classmethod
    def delete(cls, table, wherelist):
        if not isinstance(wherelist, list):
            wherelist = [wherelist]
        where_expr = cls.gen_where_expr(wherelist)
        del_sql = "delete from {t} where {where_expr};".format(t=table, where_expr=where_expr)
        return del_sql
    
    #generate create (temp) table statement from datatable instance dt
    def create(self, table, temp=True):
        tp = 'temp'
        if temp is False:
            tp = ''
        create_sql = '''drop table if exists {table}; \ncreate {temp} table {table} as {dt};'''.format(temp=tp, table=table, dt=self.table)
        return create_sql
    
    #print sql statement
    def sql(self, pprint=True):
        if self.table.startswith('select'):
            sql = self.table+';'
        else:
            #just plain table name
            sql = self.select(10).table+';'
        if pprint:
            print sqlparse.format(sql, reindent=True)
        return sql
        
    #aggdict has format [{'date': ['max', 'date']}, {'coins': ['sum', 'coins_left']}]
    #or you can pass a dict: {'date': ['max', 'date'], 'coins': ['sum', 'coins_left']}
    #but column orders might not be preserved
    #use countd for count(distinct)
    #datediff('day', install_date, CURRENT_DATE) as diff -> 'diff': ['datediff', 'day', 'install_date, 'CURRENT_DATE']
    def aggexpr(self, agg, namefirst=True):
        aggs = ''
        aggcols = []
        if isinstance(agg, list):
            agglist = agg
        elif isinstance(agg, dict):
            agglist = []
            for k, v in agg.items():
                agglist.append({k:v})
        for e in agglist:
            if isinstance(e, dict):
                if namefirst:
                    new_name, fn = e.items()[0]
                else:
                    fn, new_name = e.items()[0]
                if isinstance(fn, basestring):
                    aggs += ", " +fn+" as "+new_name
                elif isinstance(fn, list):
                    if len(fn)==2:
                        if fn[0].lower()=='countd':
                            aggs += ", count(distinct "+fn[1]+") as "+new_name
                        else:
                            aggs += ", "+fn[0]+self.wrap(fn[1])+" as "+new_name
                        aggcols.append(fn[1])
                    elif len(fn)>2:
                        aggs += ", "+fn[0]+self.wrap(', '.join([str(x) for x in fn[1:]]))+" as "+new_name
        return aggs
        
    def groupby(self, columns, agg=[], namefirst=True):
        aggs = ''
        if len(agg)>0:
            aggs = self.aggexpr(agg, namefirst)
        colstr, cols = self.rename_cols(columns, '')
                    
        expr = "select {cols} {aggs} from {t} group by {ncols}".format(cols=colstr, aggs=aggs, t=self.optionalWrap(), ncols=','.join([str(x) for x in range(1, len(columns)+1)]))
        
        copy = self.copy()
        copy.table = expr
        return copy
    
    def addcols(self, agg, namefirst=True):
        aggs = self.aggexpr(agg, namefirst)
        copy = self.copy()
        copy.table  = "select * {aggs} from {t}".format(aggs=aggs, t=self.optionalWrap())
        return copy
    
    #on is a list of strings: ['t1.user_key=t2.user_key', 't1.date<=t2.date']
    #the left and right tables are called t1, t2 by default
    def join(self, table2, on, select='*', how='inner', where=[]):
        a1 = self.alias or 't1'
        a2 = table2.alias or 't2'
        j = 'join'
        if how=='left':
            j = 'left join'
        ondict = ' and '.join([x.replace('t1', a1).replace('t2', a2) for x in on])
        where_expr = ''
        if isinstance(where, list) and len(where)>0:
            where_expr = 'where '+self.gen_where_expr(where)
        copy = self.copy()
        copy.table = "select {select} from {t1} {a1} {j} {t2} {a2} on {ondict} {where_expr}"\
        .format(select=select.replace('t1', a1).replace('t2', a2), j=j, a1=a1, a2=a2, t1=self.optionalWrap(),\
                t2=table2.optionalWrap(), ondict=ondict, where_expr=where_expr.replace('t1', a1).replace('t2', a2))
        copy.alias = None
        return copy
    
    def verify(self, cluster='kpi-diandian', db='kpi'):
        conn = "redshift+psycopg2://biadmin:Halfquest_2014@{cluster}.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/{db}"
        engine = create_engine(conn.format(cluster=cluster, db=db))
        df = pd.read_sql_query(self.select(3).sql(), engine)
        self.sample = df
        self.columns = df.columns.tolist()
        print self.columns
        return self
    
    #table_name is a string, datatable is an instance of DataTable
    def insert(self, table_name, columns=[]):
        cols = ''
        if len(columns)>0:
            cols = self.wrap(','.join(columns))
        insert_sql = 'insert into {t} {cols} {dt};'.format(t=table_name, cols=cols, dt=self.table)
        return insert_sql


def trim(s):
    return "".join(s.split())

class TestPandasql(unittest.TestCase):

    def test_simple(self):
        t = DataTable('kpi_processed.dim_user').sql()
        ret = 'select * from kpi_processed.dim_user limit 10;'
        self.assertEqual(trim(t), trim(ret))
        
    def test_simpleselect(self):
        t = DataTable('kpi_processed.dim_user').select(5).sql()
        ret = 'select * from kpi_processed.dim_user limit 5;'
        self.assertEqual(trim(t), trim(ret))

    def test_where(self):
        t = DataTable('kpi_processed.fact_revenue').where(["date<='2016-05-01", {'level': 20}, {'action': ['xyz', 'pqr']},{'subtype':'item', 'field1': ['Almond', 'gold']}]).select(n=10, columns=['date', {'app_id': 'app'}, 'usd']).sql()
        
        ret = "select date, app_id as app, usd from kpi_processed.fact_revenue where date<='2016-05-01 AND level=20 AND action in ('xyz','pqr') AND (subtype='item' OR field1 in ('Almond','gold')) limit 10;"
        self.assertEqual(trim(t), trim(ret))
        
    def test_groupby(self):
        t = DataTable('kpi_processed.fact_dau_snapshot').addcols([{'diff': ['datediff', 'day', 'date', 'CURRENT_DATE']}]).groupby(['diff', 'date'], [{'c': ['countd', 'user_key']}]).sql()
        
        ret = "select diff, date , count(distinct user_key) as c from (select * , datediff(day, date, CURRENT_DATE) as diff from kpi_processed.fact_dau_snapshot) group by 1,2;"
        
        self.assertEqual(trim(t), trim(ret))
        
    def test_groupby2(self):
        t = DataTable('kpi_processed.fact_dau_snapshot').addcols([{'diff': ['datediff', 'day', 'date', 'CURRENT_DATE']}]).groupby(['diff', 'date'], [{'count(distinct user_key)': 'c'}], namefirst=False).sql()
        
        ret = "select diff, date , count(distinct user_key) as c from (select * , datediff(day, date, CURRENT_DATE) as diff from kpi_processed.fact_dau_snapshot) group by 1,2;"
        
        self.assertEqual(trim(t), trim(ret))
        
    def test_join(self):
        t = DataTable('kpi_processed.fact_dau_snapshot').join(DataTable('kpi_processed.dim_user').rename('u'), on=['t1.user_key=t2.user_key'], select='t1.*, t2.install_date', where=["t1.date>='2016-05-01'"]).addcols([{'diff': ['datediff', 'day', 'install_date', 'CURRENT_DATE']}]).groupby(['install_date', 'diff', 'app_id', 'os'], [{'retained': ['countd', 'user_key']}]).sql()
        
        ret = """select install_date, diff, app_id, os, count(distinct user_key) as retained from (select * , datediff(day, install_date, CURRENT_DATE) as diff from (select t1.*, u.install_date from kpi_processed.fact_dau_snapshot t1 join kpi_processed.dim_user u on t1.user_key=u.user_key where t1.date>='2016-05-01')) group by 1,2,3,4;"""
        
        self.assertEqual(trim(t), trim(ret))

#cols is a list whose elements are either a string (unchanged) or a dict (value is extracted)
def extract_cols(cols):
    ret = []
    for c in cols:
        if isinstance(c, basestring):
            ret.append(c)
        elif isinstance(c, dict):
            ret.append(str(c.values()[0]))
    return ret

if __name__ == '__main__':     
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPandasql)
    unittest.TextTestRunner(verbosity=2).run(suite)
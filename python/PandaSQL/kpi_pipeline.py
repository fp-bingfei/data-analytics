import pandas as pd
from sqlalchemy import create_engine, text
from pandasql import *
import time
import sys  

newkpi_conn = "redshift+psycopg2://biadmin:Halfquest_2014@kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi"
newkpi_engine = create_engine(newkpi_conn)
dau_snapshot_table = 'kpi_processed.fact_dau_snapshot_temp'
dau_snapshot_master = 'kpi_processed.fact_dau_snapshot_master_temp'
dim_user_table = 'kpi_processed.dim_user_test'

#prepare date/single game data conditions
rerun_days=10

#list of datetime conditions. choose your own column.
def time_cond(rerun_days=3, col='ts_pretty'):
    dc = ['datediff(day, {1}, CURRENT_DATE)<={0}'.format(rerun_days, col), '{0}<CURRENT_DATE'.format(col)]
    return dc

date_cond = time_cond(rerun_days)
#onegame_date_cond = date_cond + ["app_id like 'ffs%%'"]
cond = date_cond

#list of sqls to be executed
pipeline_sql_text = []

#commonly used column definitions
scene_string = '''case
  when nvl(nullif(json_extract_path_text(properties,'scene'),''), '1') ='1' then 'Main'
  else json_extract_path_text(properties,'scene') end'''
level = "nullif({0}, '')::int".format(DataTable.jsonx('properties', 'level'))
vip_level = "nullif({0}, '')::int".format(DataTable.jsonx('properties', 'vip_level'))
level_start = 'min({0})'.format(level)
level_end = 'max({0})'.format(level)
user_key = 'md5(app_id||user_id)'
user_key_device = 'md5(app_id||user_id||device_id)'
user_key_date = 'md5(app_id||user_id||trunc(ts_pretty))'
device_id = "nvl(nullif(android_id, ''), idfa)"
date = 'trunc(ts_pretty)'
install_ts = "nullif({0}, '')".format(DataTable.jsonx('properties', 'install_ts_pretty'))+'::timestamp'
install_date = 'trunc({0})'.format(install_ts)
is_new_user = '''case when trunc(install_ts) = date then 1 else 0 end'''
is_payer = '''case when date>=trunc(conversion_ts) then 1 else 0 end'''
is_converted_today = '''case when date=trunc(conversion_ts) then 1 else 0 end'''
groupby_user_cols = [{date: 'date'}, {user_key:'user_key'}, {device_id: 'device_id'}, 'user_id', 'app_id', 'scene', 'gameserver_id']
join_session_on = ['t1.session_id=t2.session_id', 't1.user_id=t2.user_id', 't1.app_id=t2.app_id']
join_user_on = ['t1.{0}=t2.{0}'.format(g) for g in extract_cols(groupby_user_cols)]

#session start/end
session_start = DataTable('kpi_processed.session_start').where(cond)
session_end = DataTable('kpi_processed.session_end').where(cond)

#catch missing new users/payers for each date not in session_start
missing_new_user = DataTable('kpi_processed.new_user').where(cond+ ['{0} not in ({1})'.format(user_key_date, session_start.select(columns=[user_key_date]).table)])

session_start_cols = [u'event_id', u'data_version', u'app_id', u'ts', u'ts_pretty', u'event', u'user_id', u'session_id', u'app_version', u'gameserver_id', u'os', u'os_version', u'browser', u'browser_version', u'idfa', u'idfv', u'gaid', u'android_id', u'mac_address', u'device', u'ip', u'country_code', u'lang', u'level', u'vip_level', u'facebook_id', u'gender', u'first_name', u'last_name', u'birthday', u'email', u'googleplus_id', u'gamecenter_id', u'install_ts', u'install_source', u'scene', u'fb_source']

#payment has different columns from session_start so need to reselect the columns
missing_payers = DataTable('kpi_processed.payment').where(cond+ ['{0} not in ({1})'.format(user_key_date, session_start.select(columns=[user_key_date]).table)]).select(columns=session_start_cols)

pipeline_sql_text.append(sqlparse.format(missing_new_user.insert('kpi_processed.session_start'), reindent=True))
pipeline_sql_text.append(sqlparse.format(missing_payers.insert('kpi_processed.session_start', columns=session_start_cols), reindent=True))

#create base dau table for dau skeleton, using session_start + session_end
#attr_dict holds the aggregation functions for the columns
attr_dict = []
for key in ['app_version', 'os', 'os_version', 'device', 'browser', 'browser_version', 'country_code', 'install_source', 'facebook_id', 'vip_level', 'gender', 'first_name', 'last_name', 'birthday', 'email', 'ip', 'last_ref']:
    tmp = {}
    tmp[key] = 'max({0})'.format(key)
    attr_dict.append(tmp)
    
attr_dict.append({'language': 'max(lang)'}) 
attr_dict.append({'install_ts': 'nvl(min(install_ts), min(ts_pretty))'}) 
attr_dict.append({'last_login_date': 'max(ts_pretty)'}) 
attr_dict.extend([{'level_start': 'min(level)'}, {'level_end': 'max(level)'}])
attr_dict.extend([{'session_cnt': 'count(1)'}, {'playtime_sec':'least(sum(session_length), 24*3600*60)'}])

base_dau = session_start.join(session_end, on=join_session_on,\
            select="t1.*, nvl(round(greatest(session_length, 0)::bigint), 0) as session_length",\
            how='left').groupby(columns=groupby_user_cols, agg=attr_dict).addcols([{'is_new_user': is_new_user}])

pipeline_sql_text.append(sqlparse.format(base_dau.create('base_temp', temp=False), reindent=True))

#payment related parts of dau skeleton
#do a join with kpi_processed.payment to get device_id (fill in from base_temp if needed)
#pp: we join payment info for past n days and all-time
payment_fill_device_id = DataTable('kpi_processed.payment').addcols([{'date': 'trunc(ts_pretty)'}, {'user_key': user_key}]).join(DataTable('base_temp').groupby(columns=['date', 'user_key'], agg={'device_id': 'max(device_id)'}), on=['t1.date=t2.date', 't1.user_key=t2.user_key'], select="t1.*, nvl(nullif({0}, ''), device_id) as device_id".format(device_id))
payment_recent = DataTable('kpi_processed.fact_revenue').where(time_cond(rerun_days, 'ts')).join(payment_fill_device_id, on=['t1.ts=t2.ts_pretty', 't1.user_key=t2.user_key'], select="t1.*, t2.device_id, t2.gameserver_id").groupby(columns=extract_cols(groupby_user_cols), agg={'sum(revenue_usd)':'revenue_usd', 'count(1)': 'payment_cnt'}, namefirst=False)
payment_all = DataTable('kpi_processed.fact_revenue').where(['revenue_amount>0', 'ts<CURRENT_DATE']).join(payment_fill_device_id, on=['t1.ts=t2.ts_pretty', 't1.user_key=t2.user_key'], select="t1.*, t2.device_id, t2.gameserver_id").groupby(columns=extract_cols(groupby_user_cols)[1:], agg={'min(ts)':'conversion_ts', 'sum(revenue_usd)':'total_revenue_usd', 'count(1)': 'total_payment_cnt'}, namefirst=False)

#join_user_on[1:] removes the date from join condition
pp = payment_recent.join(payment_all, on=join_user_on[1:], select='t1.*, t2.conversion_ts, t2.total_revenue_usd, t2.total_payment_cnt').rename('pp')

#left join base dau table with country, pp and then add is_payer + is_converted_today columns
skeleton = DataTable('base_temp').join(DataTable('kpi_processed.dim_country'), on=['t1.country_code=t2.country_code'], select="t1.*, nvl(t2.country, 'Unknown') as country", how='left').join(pp, on=join_user_on, select='t1.*, t2.revenue_usd, t2.payment_cnt, t2.conversion_ts, t2.total_revenue_usd, t2.total_payment_cnt', how='left').addcols({is_payer:'is_payer', is_converted_today: 'is_converted_today'}, namefirst=False)

pipeline_sql_text.append(sqlparse.format(skeleton.create('skeleton', temp=False), reindent=True))

#delete + insert to fact_dau_snapshot_master and fact_dau_snapshot
delete_fact_dau_master_sql = DataTable.delete(dau_snapshot_master, time_cond(rerun_days, 'date'))
pipeline_sql_text.append(sqlparse.format(delete_fact_dau_master_sql, reindent=True))
delete_fact_dau_sql = DataTable.delete(dau_snapshot_table, time_cond(rerun_days, 'date'))
pipeline_sql_text.append(sqlparse.format(delete_fact_dau_sql, reindent=True))

fact_dau_master_cols = ['id', 'date', 'user_key', 'app_id', 'device_id', 'gameserver_id', 'scene', 'level_start', 'level_end', 'app_version', 'os', 'os_version', 'device', 'browser', 'browser_version', 'country', 'language', 'is_new_user', 'playtime_sec', 'is_payer', 'revenue_usd', 'payment_cnt', 'session_cnt', 'is_converted_today']
pipeline_sql_text.append(sqlparse.format(skeleton.addcols({'id': 'md5(user_key||date)'}).select(columns=fact_dau_master_cols).insert(dau_snapshot_master, columns=fact_dau_master_cols), reindent=True))

#group by and insert into original fact_dau_snapshot - 1 row per user_key
fact_dau_cols = [x for x in fact_dau_master_cols if x not in ['device_id', 'gameserver_id']]
fact_dau_sum_cols = ['playtime_sec', 'revenue_usd', 'payment_cnt', 'session_cnt']
fact_dau_min_cols = ['is_new_user', 'is_converted_today']
fact_dau_agglist = []
for c in fact_dau_cols:
    if c not in ['id', 'date', 'user_key', 'app_id']:
        if c in fact_dau_sum_cols:
            fact_dau_agglist.append({c: 'sum({0})'.format(c)})
        elif c in fact_dau_min_cols:
            fact_dau_agglist.append({c: 'min({0})'.format(c)})
        else:
            fact_dau_agglist.append({c: 'max({0})'.format(c)})

pipeline_sql_text.append(sqlparse.format(skeleton.groupby(['date', 'user_key', 'app_id'], agg=fact_dau_agglist).addcols({'id': 'md5(user_key||date)'}).select(columns=fact_dau_cols).insert(dau_snapshot_table, columns=fact_dau_cols), reindent=True))

#do group by on skeleton to prepare join with dim_user (before insert)
groupby_user_cols = [x for x in extract_cols(groupby_user_cols) if x in ['user_key', 'user_id', 'app_id']]

agglist = []
agglist.append({'level': 'max(level_end)'})
agglist.append({'conversion_ts': 'min(conversion_ts)'})
agglist.append({'install_ts': 'min(install_ts)'})
agglist.append({'revenue_usd': 'max(total_revenue_usd)'})
agglist.append({'payment_cnt': 'max(total_payment_cnt)'})
agglist.append({'last_ip': 'max(ip)'})
skeleton_agg_cols = ['last_login_date', 'facebook_id', 'language', 'birthday', 'first_name', 'last_name', 'gender', 'country', 'email', 'os', 'os_version', 'device', 'browser', 'browser_version', 'last_ref', 'is_payer', 'app_version', 'install_source']
for c in skeleton_agg_cols:
    agglist.append({c:'max({0})'.format(c)})

#all_dim_user_skeleton_cols is used to keep track of all the columns in the skeleton, so that we can easily insert into dim_user later
all_dim_user_skeleton_cols = groupby_user_cols[:]

#define join columns with dim_user
joinlist = ['t1.'+x for x in groupby_user_cols if x!='install_ts']
dim_user_compare_columns = ['facebook_id', 'install_language', 'install_country', 'install_os', 'install_device', 'install_browser', 'install_gender', 'language', 'birthday', 'first_name', 'last_name', 'gender', 'country', 'email', 'os', 'os_version', 'device', 'browser', 'browser_version', 'last_ref', 'install_source', 'last_ip']

#handle the join columns that don't need to be compared to dim_user first
for c in [x.keys()[0] for x in agglist]:
    if c not in dim_user_compare_columns:
        #ugly special case for install_ts
        if c=='install_ts':
            joinlist.append('least(t1.install_ts, u.install_ts) as install_ts')
        else:
            joinlist.append('t1.'+c)
        all_dim_user_skeleton_cols.append(c)

#for columns that start with 'install_' (except install_source), we only update dim_user if the corresponding column is empty. for other columns, we overwrite from skeleton         
for c in dim_user_compare_columns:
    if c.startswith('install_'):
        r = c
        if c!='install_source':
            r = c.replace('install_', '')
        joinlist.append("nvl(t2.{0}, t1.{1}) as {0}".format(c, r))
    else:
        joinlist.append("nvl(t1.{0}, t2.{0}) as {0}".format(c))
    all_dim_user_skeleton_cols.append(c)

#expression to parse install_source
def parse_is(i):
    return "nullif(split_part(install_source, '::', {0}),'')".format(str(i))

install_subpublisher = parse_is(3)
install_campaign = parse_is(2)
install_creative_id = parse_is(4)

#install_source related columns to be added
post_add_cols = [{'install_subpublisher': install_subpublisher}, {'install_campaign': install_campaign}, {'install_creative_id': install_creative_id}, {'id': 'user_key'}, {'install_date': 'trunc(install_ts)'}]

all_dim_user_skeleton_cols.extend([x.keys()[0] for x in post_add_cols])

#ok the real logic starts from here...we do a groupby first and then join
dim_user_skeleton = DataTable('skeleton').groupby(groupby_user_cols, agg=agglist).join(DataTable('kpi_processed.dim_user_test').rename('u'), on=['t1.{0}=t2.{0}'.format(g) for g in groupby_user_cols], select=', '.join(joinlist), how='left').addcols(post_add_cols)

#delete from dim_user whose users are in skeleton
delete_dim_user_sql = DataTable.delete(dim_user_table, ['{0} in ({1})'.format(user_key, DataTable('skeleton').select(columns=[user_key]).table)])

pipeline_sql_text.append(sqlparse.format(delete_dim_user_sql, reindent=True))
pipeline_sql_text.append(sqlparse.format(dim_user_skeleton.select(columns=all_dim_user_skeleton_cols).insert(dim_user_table, columns=all_dim_user_skeleton_cols), reindent=True))

dim_user_cols = [u'id', u'user_key', u'app_id', u'app_version', u'user_id', u'facebook_id', u'install_ts', u'install_date', u'install_source', u'install_subpublisher', u'install_campaign', u'install_language', u'install_country', u'install_os', u'install_device', u'install_browser', u'install_gender', u'language', u'birthday', u'first_name', u'last_name', u'gender', u'country', u'email', u'os', u'os_version', u'device', u'browser', u'browser_version', u'last_ip', u'level', u'is_payer', u'conversion_ts', u'revenue_usd', u'payment_cnt', u'last_login_date', u'install_source_group', u'install_creative_id', u'last_ref']

print [x for x in dim_user_cols if x not in all_dim_user_skeleton_cols]

if __name__ == '__main__': 
    #generate script
    if len(sys.argv)>1:
        try:
            default = '/Users/funplus/Downloads/kpi_diandian_gensql.sql'
            if sys.argv[1]=='default':
                fn = default
            else:
                fn = sys.argv[1]
            with open(fn, 'w') as f:
                f.write('\n'.join(pipeline_sql_text))
        except:
            print 'Generated SQL cannot be written to file!'
    else:
        #execute generated sql script
        begin_ts = time.time()
        for i, q in enumerate(pipeline_sql_text):
        	with newkpi_engine.begin() as conn:
        		start_ts = time.time()
        		r = conn.execute(text(q).execution_options(autocommit=True))
        		end_ts = time.time()
        		print "Executed query #{0}:".format(str(i))
        		print q
        		print "Time elapsed: {0}s".format(str(int(end_ts-start_ts)))
        		print "Rows affected: {0}".format(str(r.rowcount))

        print "Total time elaspsed: {0}s".format(str(int(time.time()-begin_ts)))
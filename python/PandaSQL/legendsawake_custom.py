import pandas as pd
from sqlalchemy import create_engine, text
import json
from pandasql import *

customdd_conn = "redshift+psycopg2://biadmin:Halfquest_2014@custom-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/custom"

customdd_engine = create_engine(customdd_conn)

sql = """select * from
        (SELECT *, row_number() over (partition by event order by md5(ts||user_id)) as rnum
        FROM custom.legendsawake.catchall)
        where rnum<=100;"""

events = pd.read_sql_query(sql, customdd_engine)

#we ignore c1, c2
catchall_cols = [u'data_version', u'app_id', u'ts', u'ts_pretty', u'event', u'user_id', u'session_id', u'os', u'install_ts', u'lang', u'level', u'browser', u'app_version', u'ip', u'country_code', u'd_c1', u'd_c2', u'd_c3', u'd_c4', u'd_c5', u'm1', u'm2', u'm3', u'm4', u'm5', u'idfa', u'gaid', u'server', u'load_hour']
p_cols = [u'd_c1', u'd_c2', u'd_c3', u'd_c4', u'd_c5', u'm1', u'm2', u'm3', u'm4', u'm5']

rerun_days=2
dc = 'datediff(day, ts_pretty, CURRENT_DATE)<='+str(rerun_days)
create_table_template = """CREATE TABLE if not exists {table}
(
	data_version VARCHAR(10) ENCODE lzo,
	app_id VARCHAR(64) ENCODE lzo,
	ts BIGINT,
	ts_pretty TIMESTAMP,
	event VARCHAR(64) ENCODE lzo,
	user_id VARCHAR(64) ENCODE lzo,
	session_id VARCHAR(100) ENCODE lzo,
	os VARCHAR(64) ENCODE lzo,
	install_ts TIMESTAMP,
	lang VARCHAR(16) ENCODE lzo,
	level INTEGER,
	browser VARCHAR(64) ENCODE lzo,
	app_version VARCHAR(64) ENCODE lzo,
	ip VARCHAR(64) ENCODE lzo,
	country_code VARCHAR(8) ENCODE lzo,
	idfa VARCHAR(128) ENCODE lzo,
	gaid VARCHAR(128) ENCODE lzo,
	server VARCHAR(64) ENCODE lzo,
	load_hour TIMESTAMP
    {addcols}
);"""

pipeline_sql_text = []
for e in events['event'].unique():
    new_table_name = 'legendsawake.daily_processed_'+e
    key_dict = {}
    for c in p_cols:
        s = events[events['event']==e][c]
        if s.notnull().sum()>0:
            key_dict[c] = s[s.notnull()].map(lambda x: json.loads(x)['key']).value_counts().index[0]
        
    extract_list = []
    new_cols = []
    for k, v in key_dict.items():
        new_cols.append(v)
        extract_list.append("json_extract_path_text({0}, 'value') as {1}".format(k, v))
    
    #create table if not exists
    with customdd_engine.begin() as conn:
        conn.execute(text(create_table_template.format(table=new_table_name, addcols='\n'.join([',\t{0} VARCHAR(128) ENCODE lzo'.format(x) for x in new_cols]))).execution_options(autocommit=True))
    #pipeline_sql_text.append(create_table_template.format(table=new_table_name, addcols=',\n\t'.join(['{0} VARCHAR(128) ENCODE lzo'.format(x) for x in new_cols])))
    count_sql = "select count(1) as c from {0};".format(new_table_name)
    cdf = pd.read_sql_query(count_sql, customdd_engine)
    #if table already exists and not empty, we rerun last n days
    if cdf.iloc[0,0]>0:
        delete_sql = DataTable.delete(new_table_name, [dc])
        #delete last n days
        pipeline_sql_text.append(sqlparse.format(delete_sql, reindent=True))
        where_list = [{'event': e}, dc]
    else:
        where_list = [{'event': e}]
    print e, cdf.iloc[0,0]
    
    select_cols = [x for x in catchall_cols if x not in p_cols]+extract_list
    insert_cols = [x for x in catchall_cols if x not in p_cols]+new_cols
    #insert last n days or everything
    pipeline_sql_text.append(sqlparse.format(DataTable('legendsawake.catchall').where(where_list).select(columns=select_cols).insert(new_table_name, columns=insert_cols), reindent=True))
    
pipeline_sql_text.append('grant usage on schema legendsawake to "la_pm";')
pipeline_sql_text.append('grant select on all tables in schema legendsawake to "la_pm";')

import time
begin_ts = time.time()
for i, q in enumerate(pipeline_sql_text):
    with customdd_engine.begin() as conn:
        start_ts = time.time()
        r = conn.execute(text(q).execution_options(autocommit=True))
        end_ts = time.time()
        print "Executed query #{0}:".format(str(i))
        print q
        print "Time elapsed: {0}s".format(str(int(end_ts-start_ts)))
        print "Rows affected: {0}".format(str(r.rowcount))
print "Total time elaspsed: {0}s".format(str(int(time.time()-begin_ts)))
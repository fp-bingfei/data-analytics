select
user_id as fpid,
email,
device_id,
language,
last_login_date,  
case when revenue_usd <= 0.99 then '<=0.99'
when revenue_usd > 0.99 and revenue_usd <= 5 then '0.99-5'
when revenue_usd > 5 and revenue_usd <= 10 then '5-10'
when revenue_usd > 10 and revenue_usd <= 20 then '10-20'
when revenue_usd > 20 and revenue_usd <= 30 then '20-30'
when revenue_usd > 30 and revenue_usd <= 50 then '30-50'
when revenue_usd > 50 and revenue_usd <= 100 then '50-100'
else '>100'
end as revenue_seg
from kpi_processed.dim_user_by_device 
where last_login_date <= dateadd(day,-7,current_date)
and level < 15
and is_payer = 1
and (email <> '' or device_id <> '')
and app_id = 'koa.global.prod';
import numpy as np
import pandas as pd
from sklearn.cross_validation import StratifiedShuffleSplit
import psycopg2 as pg
import pandas.io.sql as sql
import argparse
from scipy import stats

class entry_redshift(object):
    db_host = 'kpi-funplus.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    db_port = '5439'
    db_username = 'biadmin'
    db_password = 'Halfquest_2014'
    db_name = 'kpi'
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)

class sample:
    def __init__(self,args):
        self.redshift_conn = entry_redshift.conn_string
        self.sql_path = '/home/ec2/temp/sampling/sql/'
        self.export_path = '/home/ec2/temp/sampling/export/'
        self.label = args['label']

    def run(self):
        df_merge = self.merge()
        df_merge.to_csv(self.export_path,index=False)

    def getdata(self):
        fd = open(self.sql_path + 'group2.sql', 'r')
        sqlFile = fd.read()
        fd.close()
        red_conn = pg.connect(self.redshift_conn)
        user_list = sql.read_sql(sqlFile, red_conn)
        return user_list

    def sampling(self, df, n_iter=2,test_size=0.5,random_state=0):
        # create numpy array
        fpid = np.array(df['fpid'])
        label = np.array(df[self.label])
        # create stratified shuffle sampling
        sss = StratifiedShuffleSplit(label, n_iter, test_size=test_size, random_state=random_state)
        for control_index, test_index in sss:
            fpid_control, fpid_test = fpid[control_index], fpid[test_index]
            label_control, label_test = label[control_index], label[test_index]
        d_control = {'fpid':fpid_control,'revenue_seg':label_control, 'group_type':np.repeat('control',len(fpid_control))}
        d_test = {'fpid':fpid_test,'revenue_seg':label_test, 'group_type':np.repeat('test',len(fpid_test))}
        ks = self.ks_test(label_control,label_test)
        print "K-S stats value: ", ks[0]
        print "P-Value of K-S Test: ", ks[1]
        return d_control,d_test

    def merge(self):
        df_raw = self.getdata()
        df_control, df_test = self.sampling(df_raw)
        df = pd.concat([df_control, df_test])
        df_merge = pd.merge(df, df_raw, on=['fpid', self.label], how='outer')
        return df_merge

    def ks_test(self,test,control):
        ks = stats.ks_2samp(test, control)
        return ks

def parse_args():
    parser = argparse.ArgumentParser(description='Which Label to be Stratified')
    parser.add_argument('-l','--label', help='Which Label to be stratified', required=True)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    sp = sample(args)
    sp.run()

if __name__ == '__main__':
    main()


import boto
import copy
import argparse
import os.path
import HTML
import smtplib
import datetime
import math
import shutil
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class EventsCount:

    def __init__(self, args):

        self.args = args
        self.results = {}
        self.keys = ['session_start', 'payment', 'new_user']
        self.hist = 3


    def run(self):

        paths = self.set_params()
        template = self.set_template()
        template_counter = self.set_template(flag = 1)
        self.clean_up()
        self.get_s3_files(paths)
        hive = self.get_hive(paths,template)
        counters = self.get_counters(paths, template_counter)
        hive_hist = self.get_hive_hist(paths, template)
        self.send_mail(hive, counters, hive_hist)
        self.clean_up()


    def get_hive_hist(self, paths, template):

        hive_hist = copy.deepcopy(template)

        for k in range(self.hist):
            tmp_day = self.cur_day - datetime.timedelta(k + 1)
            filename = paths['local_hive_hist'] % (tmp_day.year, tmp_day.month, tmp_day.day)
            if not os.path.isfile(filename):
                continue
            
            f = open(filename, "r")
            lines = f.readlines()

            for i in range(len(lines)):
                value = lines[i].split()
                for j, key in enumerate(self.keys):
                    hive_hist[int(value[0])][key] += int(value[j+1]) if value[j+1] != 'NULL' else 0

        return hive_hist


    def get_counters(self, paths, template):

        counters = copy.deepcopy(template)

        for i in range(24):
            filename = paths['local_counters'] % i
            if os.path.isfile(filename):
                f = open(filename, "r")
                for line in f:
                    if line.split()[0] in self.keys + ['invalid']:
                        counters[i][line.split()[0]] = int(line.split()[1])
                        counters[24][line.split()[0]] += int(line.split()[1])

        return counters


    def get_hive(self, paths, template):

        hive = copy.deepcopy(template)
        if not os.path.isfile(paths['local_hive']):
            return hive

        f = open(paths['local_hive'],"r")
        lines = f.readlines()

        keys = ['session_start', 'payment', 'new_user']

        for i in range(min(len(lines),25)):
            value = lines[i].split()
            for j, key in enumerate(self.keys):
               hive[int(value[0])][key] = int(value[j+1]) if value[j+1] != 'NULL' else 0

        return hive


    def get_s3_files(self, paths):

        s3_conn = boto.connect_s3()
        bucket = s3_conn.get_bucket(self.args['s3bucket'])

        for i in range(24):

            key = paths['remote_counters'] % i
            counter_key = bucket.get_key(key)

            if counter_key:
                filename = paths['local_counters'] % i
                counter_key.get_contents_to_filename(filename)
            else:
                print "counter of hour " + str(i) + "does not exist in given path"

        key = paths['remote_hive']
        hive_key = bucket.get_key(key)

        if hive_key:
            filename = paths['local_hive']
            hive_key.get_contents_to_filename(filename)
        else:
            print "hive.table does not exist in given path"

        for i in range(self.hist):

            tmp_day = self.cur_day - datetime.timedelta(i+1)
            key = paths['remote_hive_hist'] % (tmp_day.year, tmp_day.month, tmp_day.day)
            hive_key = bucket.get_key(key)

            if hive_key:
                filename = paths['local_hive_hist'] % (tmp_day.year, tmp_day.month, tmp_day.day)
                hive_key.get_contents_to_filename(filename)
            else:
                print "hive.table of " + filename.split('_')[0] + "does not exist in given path"

        s3_conn.close()


    def send_mail(self, hive, counters, hive_hist):

        htmp = '''
        <p>To download the INVALID events, visit this link: <a href="https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=com.funplus.public&prefix=%s">INVALID EVENTS</a> </p>
        <p>If can't access the file, login using this link: <a href="https://921287511715.signin.aws.amazon.com/console">Login here</a> </p>
        <ul>
        <li>User Name: public</li>
        <li>Password: funplus64301</li>
        </ul>
        ''' % self.args['path'].replace('scratch', 'invalid')

        htmltext = htmp + '''
        <p>json validator: this means number of events that didn't pass our json validator</p>
        <p>fluentd: events count in map-reduce, total is the sum of 0-23 hours</p>
        <p>hive: events count for data in hive table, 0-23 are from hourly table, total is from daily table</p>
        <p>hive-hist: average hive events count of previous 3(adjustable) three days</p>
        <p>If the number of hive is different(up or down) from hive-hist by more than max(0.3*hive, 3), the table cell will be 'ORANGE'; and if the number of fluentd is different from hive, the table cell will be 'RED'</p>
        '''

        table_header = ['Time']
        for key in self.keys:
            table_header.append(key)
        table_header += ['invalid events']

        t = HTML.Table(header_row = table_header)
        tmp_row = [''] + ['hive', 'fluentd', 'hive_hist'] * 3 + ['json validator']
        t.rows.append(tmp_row)

        for i in range(25):
            if i == 24:
                tmp_row = ['total']
            else:
                tmp_row = ['%.2d' % i]

            for key in self.keys:
                color = 'lime' if abs(hive[i][key] - hive_hist[i][key] / self.hist) <= 0.3 * max(hive[i][key], 10) else 'orange'
                color = color if (hive[i][key] == counters[i][key]) else 'red'
                hive_item = HTML.TableCell(hive[i][key], bgcolor = color)
                counter_item = HTML.TableCell(counters[i][key], bgcolor = color)
                hive_hist_item = HTML.TableCell(math.ceil(hive_hist[i][key] / self.hist), bgcolor = color)
                tmp_row.append(hive_item)
                tmp_row.append(counter_item)
                tmp_row.append(hive_hist_item)

            invalid = HTML.TableCell(counters[i]['invalid'], bgcolor = 'yellow')
            tmp_row.append(invalid)

            t.rows.append(tmp_row)

        htmlcode = str(t) + htmltext
        tmp = htmlcode.replace('<TH>','<TH colspan = "3">', 4)
        html_final = tmp.replace('<TH colspan = "3">', '<TH colspan = "1">', 1)
        msg = MIMEMultipart('alternative')
        html = MIMEText(html_final,'html')
        msg.attach(html)
        recipients = ['bidev@funplus.com','zhenxuan.yang@funplus.com']
        msg['Subject'] = 'Data Validation results of ' + self.args['path'].split('/')[3].split('=')[1] + ' ' + self.cur_day.strftime("%Y-%m-%d")
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ', '.join(recipients)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue


    def clean_up(self):

        tmp_dir = '/mnt/funplus/analytics/dataValidation/tmp/' + self.args['path'].split('/')[3].split('=')[1] + '/'

        for tmpfile in os.listdir(tmp_dir):
            file_path = os.path.join(tmp_dir, tmpfile)

            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception, e:
                print e


    def set_template(self, flag = 0):

        template = {}
        if flag == 0:
            item = dict.fromkeys(self.keys, 0)
        else:
            item = dict.fromkeys(self.keys + ['invalid'], 0)

        for i in range(25):
            template[i] = copy.deepcopy(item)

        return template


    def set_params(self):

        paths = {}
        tmp_dir = '/mnt/funplus/analytics/dataValidation/tmp/' + self.args['path'].split('/')[3].split('=')[1] + '/'

        if not os.path.exists(tmp_dir):
            os.makedirs(tmp_dir)

        paths['remote_counters'] = self.args['path'] + 'hour=%.2d/counters.txt'
        paths['remote_hive'] = self.args['path'] + 'hive.table'
        paths['local_counters'] = tmp_dir + '%.2d_counter.txt'
        paths['local_hive'] = tmp_dir + 'hive.table'
        
        path_comp = self.args['path'].split('/')#get date
        day = path_comp[-2].split('=')[1]
        month = path_comp[-3].split('=')[1]
        year = path_comp[-4].split('=')[1]
        self.cur_day = datetime.date(int(year), int(month), int(day))

        path_comp[-2] = 'day=%.2d'
        path_comp[-3] = 'month=%.2d'
        path_comp[-4] = 'year=%.4d'

        paths['remote_hive_hist'] = '/'.join(path_comp) + 'hive.table'
        paths['local_hive_hist'] = tmp_dir + '%.4d%.2d%.2d_hive.table'

        return paths


def parse_args():

    parser = argparse.ArgumentParser(description='Events count verification')
    parser.add_argument('-s','--s3bucket', help='S3 bucket where the files is')
    parser.add_argument('-p','--path', help='path to .../scratch/app=xxx/year=YYYY/month=MM/day=DD/')
    args = vars(parser.parse_args())

    return args


def main():

    args = parse_args()
    ec = EventsCount(args)
    ec.run()


if __name__ == '__main__':

    main()
# -*- coding: utf-8 -*-
from __future__ import print_function
import httplib2
import os
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
import pandas as pd
import pandas.io.sql as sql
import psycopg2 as pg
import argparse

#try:
#    import argparse
# flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
#except ImportError:

# Get entry of kpi-diandian cluster
class diandian_cluster(object):
    db_host = 'kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    db_port = '5439'
    db_username = 'biadmin'
    db_password = 'Halfquest_2014'
    db_name = 'kpi'
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)

class funplus_cluster(object):
    db_host = 'kpi-funplus.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    db_port = '5439'
    db_username = 'biadmin'
    db_password = 'Halfquest_2014'
    db_name = 'kpi'
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)

class singular:
    def __init__(self,args):
        self.SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
        self.CLIENT_SECRET_FILE = '/Users/funplus/Documents/workspace/singular_project/client_secret.json'
        self.APPLICATION_NAME = 'Google Sheets API Quickstart'
        self.spreadsheetId = '1usPcMrXnTLnt0CR7PwdIG1_KdbxfBspDJtMVhJ4V0xg'
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?version=v4')
        self.service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discoveryUrl)
        self.input_sql_path = '/Users/funplus/PycharmProjects/singular_project/'
        self.cluster = args['cluster']
        self.app = args['app']

    def run(self):
        sheet_name = self.get_sheetlist()
        sql_file = self.generate_sql(self.cluster, sheet_name)
        self.output(sql_file)

    def output(self,file):
        app_name = self.app_map(self.app)
        fd = open(self.input_sql_path+app_name+'_agg_marketing_kpi_singular.sql','w')
        fd.write(file)
        fd.close

    def redshift(self):
        if self.cluster == 'diandian':
            conn = diandian_cluster.conn_string
        else:
            conn = funplus_cluster.conn_string
        return conn

    # Get oauth credential json file from google dev tool
    def get_credentials(self):
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,'sheets.googleapis.com-python-quickstart.json')
        store = oauth2client.file.Storage(credential_path)
        credentials = store.get()
        #flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
            flow.user_agent = self.APPLICATION_NAME
            credentials = tools.run_flow(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def get_sheetlist(self):
        sheet_metadata = self.service.spreadsheets().get(spreadsheetId=self.spreadsheetId).execute()
        sheets = sheet_metadata.get('sheets', '')
        # Create dict for singular mapping sheets
        sheet_name = ''
        for sheet in sheets:
            title = sheet.get("properties", {}).get("title", "Sheet1")
            #sheet_id = sheet.get("properties", {}).get("sheetId", 0)
            if u"对应关系" in title:
                app_name = title.split(u"：")[0]
                if app_name == self.app:
                    sheet_name = title
                else:
                    pass
        return sheet_name

    def get_sheetvalue(self, sheet_name):
        # for singular and bi have different names:

        rangeName_1= sheet_name + '!A3:B'
        result1 = self.service.spreadsheets().values().get(spreadsheetId=self.spreadsheetId, range=rangeName_1).execute()
        values_diff = result1.get('values',[])
        sql_list1 = []
        for row in values_diff:
            # row[0]: bi_source, row[1]: singular_source
            case_str = "when source = '{0}' then '{1}' ".format(row[1],row[0])
            sql_list1.append(case_str)

        rangeName_2 = sheet_name + '!D3:F'
        result2 = self.service.spreadsheets().values().get(spreadsheetId=self.spreadsheetId,range=rangeName_2).execute()
        values_adn = result2.get('values', [])
        sql_list2 = []
        for row in values_adn:

            bi_source = row[0]
            sg_source = row[1]
            rule = row[2]
            if u"不包含" in rule:
                rule = rule.replace(u'”', '"').replace(u'“', '"')
                #print(rule)
                name = rule.split('"')[1].split('"')[0]
                case_str = "when source = '{0}' and position(lower('{1}') in lower(adn_campaign)) = 0 then '{2}'" \
                    .format(sg_source, name, bi_source)
                print(case_str)
                sql_list2.append(case_str)
            else:
                rule = rule.replace(u'”', '"').replace(u'“', '"')
                case_str = "when source = '{0}' and position(lower('{1}') in lower(adn_campaign)) >0 then '{2}'"\
                    .format(sg_source,rule.split('"')[1].split('"')[0],bi_source) if rule <> 'None' else \
                    "when source = '{0}' and position(lower('GDN') in lower(adn_campaign)) = 0 " \
                        "and position(lower('SRC') in lower(adn_campaign)) = 0 " \
                        "and position(lower('UAC') in lower(adn_campaign)) = 0 " \
                        "and position(lower('Admob') in lower(adn_campaign)) = 0 " \
                        "and position(lower('VDO') in lower(adn_campaign)) = 0 "\
                        "and position(lower('-AOS-') in lower(adn_campaign)) = 0 " \
                       "then '{1}'".format(sg_source,bi_source)

                print(case_str)
                sql_list2.append(case_str)

        sql_str = 'case '+ ' '.join(sql_list1)+ ' '.join(sql_list2) + 'else source end as bi_source'
        return sql_str

    def read_sql(self):
        fd = open(self.input_sql_path + self.cluster+'_input.sql', 'r')
        sql_file = fd.read()
        fd.close()
        return sql_file

    # It's better to use yaml file to generate config files
    def app_map(self,app_name):
        app_dict = {}
        if app_name == 'Family Farm Seaside':
            app = 'ffs'
            app_dict.setdefault(app_name, app)
        elif app_name == 'King of Avalon':
            app = 'koa'
            app_dict.setdefault(app_name, app)
        elif app_name == 'Valiant Force':
            app = 'valiantforce'
            app_dict.setdefault(app_name, app)
        else:
            pass
        app_df = pd.DataFrame(app_dict.items(), columns=['app_name', 'app'])
        return app_df['app'][0]

    def generate_sql(self,cluster,sheet_name):
        sql_raw = self.read_sql()
        app = self.app_map(self.app)
        sql_file = sql_raw.format(self.get_sheetvalue(sheet_name), self.app, app)
        #sql_try = sql_file+" limit 10"
        #conn = pg.connect(self.redshift())
        #try:
            #sql.read_sql(sql_try, conn)
        #except:
            #print('SQL exception')
        return sql_file
def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-app', '--app', help='which app you choose to run', required=False)
    parser.add_argument('-cluster','--cluster', help='which cluster you choose to run: Default is funplus', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    sg = singular(args)
    sg.run()

if __name__ == '__main__':
    main()
import urllib2
import datetime
import time
import json
import argparse


class SensorTower():
    def __init__(self, args):
        self.ios_categories = ['6014']
        self.ios_countries = [('US', [u'Previous Editors\u2019 Choices', u'New Games We Love', u'Best New Games']),
                        ('GB', [u'Previous Editors\u2019 Choices', u'New Games We Love', u'Best New Games'])]

        self.android_categories = ['game']
        self.android_countries = [('US', [u'Most Popular Games', u'Most Popular Games - Android Apps on Google Play',
                                        u'New + Updated Games', u'New + Updated Games - Android Apps on Google Play']),
                        ('GB', [u'Browse the Best', u'New + Updated Games', u'Most Popular Games'])]

        self.date = datetime.datetime.strptime(args['date'], '%Y-%m-%d').date()
        self.days_back = int(args['days_back']) if args['days_back'] else 3 
        self.android_app_ids = set()
        self.ios_app_ids = set()

    def run(self):
        ios_result = []
        android_result = []
        while self.days_back > 0:
            cur_date = self.date - datetime.timedelta(self.days_back)
            print cur_date
            for country in self.ios_countries:
                for category in self.ios_categories:
                    tmp_ios_ranking = self.get_feature_ranking('ios', category, country, cur_date)
                    tmp_ios_app_ids = self.parse_feature_ranking('ios', tmp_ios_ranking, country)
                    tmp_ios_rev_dl = self.get_ios_rev_dl(tmp_ios_app_ids)
                    ios_result += tmp_ios_rev_dl

            for country in self.android_countries:
                for category in self.android_categories:
                    tmp_android_ranking = self.get_feature_ranking('android', category, country, cur_date)
                    tmp_android_app_ids = self.parse_feature_ranking('android', tmp_android_ranking, country)
                    tmp_android_rev_dl = self.get_android_rev_dl(tmp_android_app_ids)
                    android_result += tmp_android_rev_dl
            self.days_back -= 1

        self.save_ios_result(ios_result)# add category in the data if necessary
        self.save_android_result(android_result)

        app_result = []
        for app_id in self.android_app_ids:
            app_result.append(self.get_names_by_app_id('android', app_id))
        for app_id in self.ios_app_ids:
            app_result.append(self.get_names_by_app_id('ios', app_id))

        self.save_app_result(app_result)

    def get_feature_ranking(self, os, category, country, date):
        url_base = 'https://api.sensortower.com:443/v1/%s/featured_rankings/by_categories?categories=%s&countries=%s&start_date=%s&end_date=%s&auth_token=UyyTNvt3cymteF3ceDHw'
        url = url_base % (os, category, country[0], date, date)
        try:
            res = urllib2.urlopen(url)
            jsonRes = json.loads(res.read())
        except Exception, e:
            print "In get_feature_ranking" + os + ' ' + category + ' ' + country[0] + ' ' + date
            print e
            return -1

        return jsonRes

    def parse_feature_ranking(self, os, data, country):
        result = []

        url_base = 'https://api.sensortower.com:443/v1/%s/featured_rankings?ids=%s&auth_token=UyyTNvt3cymteF3ceDHw'
        for item in data:
            for label in item['children']:
                if label and label.has_key('label') and label['label'] in country[1]:
                    url = url_base % (os, label['object_id'])
                    try:
                        res = urllib2.urlopen(url)
                        jsonRes = json.loads(res.read())
                        if jsonRes[0]['children'][0].has_key('object_id'):
                            item['children'] += jsonRes[0]['children']
                        else:
                            result += jsonRes
                    except Exception, e:
                        print "In parse_feature_ranking" + ' ' + os + ' ' + country[0]
                        print data
                        print e
                        
        return result

    def get_ios_rev_dl(self, data):
        result = []
        for item in data:
            for obj in item['children']:
                tmp_res = self.get_rev_dl_by_app_id('ios', item['country'], item['date'].split('T')[0], obj['app_id'])
                #(app_name, publisher_name) = self.get_names_by_app_id('ios', obj['app_id'])
                self.ios_app_ids.add(obj['app_id'])
                if tmp_res != -1 and len(tmp_res) > 0:
                    tmp_res[0][u'device_type'] = item['device_type']
                    tmp_res[0][u'label_name'] = item['name']
                    tmp_res[0][u'rank'] = item['children'].index(obj) + 1
                    #tmp_res[0][u'app_name'] = app_name
                    #tmp_res[0][u'publisher_name'] = publisher_name
                    result += tmp_res

        return result

    def get_android_rev_dl(self, data):
        result = []
        for item in data:
            for obj in item['children']:
                tmp_res = self.get_rev_dl_by_app_id('android', item['country'], item['date'], obj['app_id'])
                #(app_name, publisher_name) = self.get_names_by_app_id('android', obj['app_id'])
                self.android_app_ids.add(obj['app_id'])
                if tmp_res != -1 and len(tmp_res) > 0:
                    tmp_res[0][u'label_name'] = item['name']
                    tmp_res[0][u'rank'] = item['children'].index(obj) + 1
                    #tmp_res[0][u'app_name'] = app_name
                    #tmp_res[0][u'publisher_name'] = publisher_name
                    result += tmp_res

        return result

    def get_rev_dl_by_app_id(self, os, country, date, app_id):
        url_base = 'https://api.sensortower.com:443/v1/%s/sales_report_estimates?app_ids=%s&countries=%s&date_granularity=daily&start_date=%s&end_date=%s&auth_token=UyyTNvt3cymteF3ceDHw'
        url = url_base % (os, str(app_id), country, date, date)
        try:
            res = urllib2.urlopen(url)
            jsonRes = json.loads(res.read())
        except Exception, e:
            print "In get_rev_dl_by_app_id" + os + ' ' + country[0] + ' ' + date + ' ' + app_id
            print e
            return -1

        return jsonRes

    def get_names_by_app_id(self, os, app_id):
        url_base = 'https://api.sensortower.com:443/v1/%s/apps?app_ids=%s&auth_token=UyyTNvt3cymteF3ceDHw'
        url = url_base % (os, app_id)
        try:
            res = urllib2.urlopen(url)
            jsonRes = json.loads(res.read())
        except Exception, e:
            print "In get_names_by_app_id" + os + ' ' + app_id
            print e
            return -1

        return (os, app_id, jsonRes['apps'][0]['name'], jsonRes['apps'][0]['publisher_name'], jsonRes['apps'][0]['categories'])


    def save_ios_result(self, data):
        string_builder = 'date|country|device_type|label_name|rank|app_id|iphone_downloads|iphone_revenue|ipad_downloads|ipad_revenue\n'
        base_string = '%s|%s|%s|%s|%d|%s|%d|%d|%d|%d\n'
        for item in data:
            try:
                tmp_string = base_string % (item['d'].split('T')[0], item['cc'], item['device_type'],
                            item['label_name'], item['rank'], str(item['aid']),
                            item.get('iu', 0), item.get('ir', 0), item.get('au', 0), item.get('ar', 0))
                string_builder += tmp_string
            except Exception, e:
                print e
                print item
        f = open('/tmp/ios_feature.csv', 'w')
        f.write(string_builder.encode('utf8'))
        f.close()

    def save_android_result(self, data):
        string_builder = 'date|country|label_name|rank|app_id|android_downloads|android_revenue\n'
        base_string = '%s|%s|%s|%d|%s|%d|%d\n'
        for item in data:
            try:
                tmp_string = base_string % (item['d'].split('T')[0], item['c'], item['label_name'],
                            item['rank'], item['aid'], item.get('u', 0), item.get('r', 0))
                string_builder += tmp_string
            except Exception, e:
                print e
                print item
        f = open('/tmp/android_feature.csv', 'w')
        f.write(string_builder.encode('utf8'))
        f.close()

    def save_app_result(self, data):
        string_builder = 'os|app_id|app_name|publisher_name|categories\n'
        base_string = '%s|%s|%s|%s|%s\n'
        for item in data:
            try:
                tmp_string = base_string % (item[0], item[1], item[2], item[3], ', '.join(str(x) for x in item[4]))
                string_builder += tmp_string
            except Exception, e:
                print e
                print item
        f = open('/tmp/app_info.csv', 'w')
        f.write(string_builder.encode('utf8'))
        f.close()

def parse_args():
    parser = argparse.ArgumentParser(description='Download adjust data to BI')
    parser.add_argument('-d','--date', help='date to process, yyyy-mm-dd', required=True)
    parser.add_argument('-b','--days_back', help='days for backfill, default is 3', required=False)
    args = vars(parser.parse_args())
    return args

def main():
    args = parse_args()
    st = SensorTower(args)
    st.run()

if __name__ == '__main__':
    main()


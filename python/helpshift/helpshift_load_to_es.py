import glob
import pandas as pd
import numpy as np
import cld
import pickle
import datetime
from helpshift_helpers import *
from elasticsearch import Elasticsearch
import string
import re
from nltk.corpus import stopwords
from gensim import corpora, models
import nltk
from nltk.collocations import *
from nltk.probability import FreqDist
import copy
import json
import langid
import HTMLParser
import elasticsearch
import concurrent.futures as futures
from nltk.tokenize import word_tokenize
import sys

last_n_days = 7
last_date = datetime.date.today()-datetime.timedelta(days=last_n_days)
issues_df_list = []
ratings_df_list = []

for f in glob.glob('./csv/issues/*.csv'):
    dy = pd.to_datetime(f.split('/')[-1].replace('.csv',''))
    if dy.date()>=last_date:
        tmp = pd.read_csv(f)
        issues_df_list.append(tmp)
        
for f in glob.glob('./csv/ratings/*.csv'):
    dy = pd.to_datetime(f.split('/')[-1].replace('.csv',''))
    if dy.date()>=last_date:
        tmp = pd.read_csv(f)
        ratings_df_list.append(tmp)
        
issues_df = pd.concat(issues_df_list)
ratings_df = pd.concat(ratings_df_list)

#detect language, extract relevant columns and parse farm ID and player email
issues_df = issues_df[['IssueId', 'AgentName', 'AgentEmail', 'UserEmail', 'AppName', 'IssueStatus', 'IssueDescription', 'IssueCreatedTime', 'IssueUpdatedTime', 'DevicePlatform', 'IssueMetadata', 'IssueLanguage', "IssueTags"]]
issues_df.index = range(len(issues_df))
issues_df['IssueCreatedTime'] = pd.to_datetime(issues_df['IssueCreatedTime'], errors='coerce')
issues_df['IssueUpdatedTime'] = pd.to_datetime(issues_df['IssueUpdatedTime'], errors='coerce')
issues_df = issues_df.groupby('IssueId').apply(lambda x: pd.DataFrame(x.sort_values(by='IssueUpdatedTime').iloc[-1]).T).drop('IssueId', axis=1).reset_index().drop('level_1', axis=1)
issues_df = issues_df.applymap(df_strip)
issues_df['langid'] = issues_df['IssueDescription'].map(lambda x: cld.detect(x.decode('unicode_escape', errors='ignore').encode('utf-8'), pickSummaryLanguage=False, removeWeakMatches=True)[0])
issues_df['IssueUpdatedTime'] = pd.to_datetime(issues_df['IssueUpdatedTime'],errors='coerce')
issues_df['IssueCreatedDate'] = issues_df['IssueCreatedTime'].map(pd.Timestamp.date)
issues_df['farm_id'] = issues_df['IssueMetadata'].str.extract('\'Farm ID\':\'(.{7})\'')
issues_df['UserEmail'] = issues_df['UserEmail'].map(lambda x: str(x).strip())
issues_df['langid'] = issues_df['langid'].str.lower()
#helpshift has some badly structured tags that need to be handled before parsing
issues_df['IssueTags'] = issues_df['IssueTags'].map(lambda x: json.loads(x.replace("'", '"').replace('" "', '","')))
issues_df = pd.merge(issues_df, ratings_df.iloc[:,:3], left_on='IssueId', right_on='IssueId', how='left')

es = Elasticsearch('52.74.59.31:9200', timeout=60)

#we process english tickets separately first, then other languages as a whole
#stop words
stop = stopwords.words('english')
stops = '''wrok think would one please would see also fransis keith game andre kang chen wang farm i'm thank thanks
fan max ffs play help get m hi two three four five six seven eight nine im anything something thing need go quot pls
still u day well ive really hello cant back dont id want like new make know use since able didnt got getting
never find days last way problem wont every let could playing tell fix havent seaside said us try another cannot going
hey say com'''

reinclude = '''cannot wont cant dont new didnt never havent'''
punc_regex = re.compile('[%s]' % re.escape(string.punctuation))
custom_stop = stop + [x for x in stops.split() if x not in reinclude.split()]

tickets_en = issues_df[issues_df['langid']=='english']
if len(tickets_en)>0:
    tickets_en['stem'] = tickets_en['IssueDescription'].map(lambda x: cleaned_stem(re.sub('[0-9]+', '', punc_regex.sub(' ', x.decode('unicode_escape', errors='ignore').encode('ascii', 'ignore').replace('\'', '').lower())), custom_stop))
    tickets_en_nonull_new = tickets_en[tickets_en['stem'].map(len)>0].drop(['IssueMetadata'], axis=1)
    retries = 0
    #we retry up to 3 times
    while retries<3:
        try:
            create_ticket_index(es, 'helpshift-en')
            success, _ = bulk(es, parse_tickets(tickets_en_nonull_new.fillna(''), doctype='ticket'), index='helpshift-en', raise_on_error=True)
            print('Wrote %d English tickets to ES.' % success)
            #trending keywords for english
            sb = ['new year', 'happy new', 'happy year']
            kw_rank = trending_bigrams(tickets_en_nonull_new, 'en', num_days=0, stop_bigrams=sb)
            latest_kw = [list(x[0]) for x in kw_rank[max(kw_rank.keys())]]
            es_create_panels(es, latest_kw)
            break
        except elasticsearch.exceptions.ConnectionError:
            print "Writing to ES failed."
            print sys.exc_info()
            retries+=1

    if retries==2:
        raise ConnectionError('Writing to ES failed after 3 retries!')

else:
	print 'No records found!'

#now we process other languages
h = HTMLParser.HTMLParser()
lang_dict = {'russian': 'ru', 'german': 'de', 'dutch': 'nl', 'french': 'fr', 'spanish': 'es'}
stop_dict = {}
stop_dict['german'] = u'''farm hallo bitte dank vielen spiel sch\xf6ne liebes geehrte herren m\xf6chte problem
            wieso mehr helfen name level einen guten rutsch frohe danke bie'''.split()
stop_dict['french'] = '''bonjour j'ai merci'''.split()
stop_dict['dutch'] = '''hallo spel'''.split()

for lang in lang_dict:
    ignored_words = nltk.corpus.stopwords.words(lang)
    ignored_words.extend(stop_dict.get(lang,[]))
    issues_lang = issues_df[issues_df['langid']==lang].copy()
    #issues_lang['stem'] = issues_lang['IssueDescription'].map(lambda x: [t for t in word_tokenize(h.unescape(re.sub('[0-9]+', '', x.decode('unicode_escape', errors='ignore'))).lower()) if len(t) >= 3 and t not in ignored_words])
    
    #list that holds (iloc, translation) tuple
    ret = []
    with futures.ThreadPoolExecutor(1) as executor:
        fs = [executor.submit(translate_funplus, n, h.unescape(desc.decode('unicode_escape', errors='ignore')), lang_dict[lang]) for (n, desc) in issues_lang['IssueDescription'].iteritems()]
        for i, f in enumerate(futures.as_completed(fs)):
            ret.append(f.result())

    issues_lang['translation'] = pd.DataFrame(ret, columns=['loc', 'desc']).set_index('loc').sort_index()
    lang_index = 'helpshift-'+lang_dict[lang]
    retries = 0
    #we retry up to 3 times
    while retries<3:
        try:
            create_other_lang_index(es, lang_index, lang)
            success, _ = bulk(es, parse_tickets(issues_lang.fillna('')), index=lang_index, raise_on_error=True)
            break
        except elasticsearch.exceptions.ConnectionError:
                print "Writing to ES failed."
                print sys.exc_info()
                retries+=1

    if retries==2:
        raise elasticsearch.exceptions.ConnectionError('Writing to ES failed after 3 retries!')

    print lang_index
    print('Performed %d actions' % success)
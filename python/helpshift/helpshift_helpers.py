from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk, streaming_bulk
from pattern.en import suggest, pluralize, singularize, conjugate, lemma, lexeme, tag, parse, pprint
import json
import HTMLParser
import pandas as pd
import numpy as np
from nltk.corpus import stopwords
from gensim import corpora, models
import nltk
from nltk.collocations import *
from nltk.probability import FreqDist
import copy
import requests
from nltk.util import ngrams

@classmethod
def one_from_each_doc(cls, docs, window_size=2, stop_bigrams=[]):
    wfd = FreqDist()
    bfd = FreqDist()
    
    stop_sets = [set(x.split()) for x in stop_bigrams]
    
    if window_size < 2:
        raise ValueError("Specify window_size at least 2")
        
    for doc in docs:
        current_grams = {}
        for window in ngrams(doc, window_size, pad_right=True):
            w1 = window[0]
            if w1 is not None:
                if w1 not in current_grams:
                    wfd[w1]+=1
                    current_grams[w1] = 1
                for w2 in window[1:]:
                    if w2 is not None and w2!=w1:
                            if set((w1, w2)) not in stop_sets:
                                new_tuple = tuple(set((w1, w2)))
                                if new_tuple not in current_grams:
                                    bfd[new_tuple] += 1
                                    current_grams[new_tuple] = 1
                            
    return cls(wfd, bfd, window_size=window_size)

BigramCollocationFinder.one_from_each_doc = one_from_each_doc

def cleaned_stem(s, stoplist):
    tmp = []
    for o in parse(s, relations=True, lemmata=True).split():
        for x in o:
            if x[-1] not in stoplist:
                if x[1] not in ('.', '$', 'CD'):
                    #tmp.append(suggest(x[-1])[0][0])
                    tmp.append(x[-1])
    return tmp

def df_strip(x):
    if type(x) in [str, unicode]:
        return x.strip("'")
    return x

def translate_funplus(n, q, source, target='en'):
    params = {"appId":9999, 'source': source, 'textType': 'mail', 'target':'en', 'q': q}
    tries = 0
    while tries<10:
        try:
            ret = requests.post("http://translate.funplusgame.com/api/v2/translate", data = params)
        except:
            tries+=1
            print n, source, 'connection reset'
        else:
            break
    
    try:
        if type(ret.json())==dict:
            if 'translation' in ret.json():
                return (n, ret.json().get('translation','').get('targetText', ''))
    except:
        pass
        #print n, q, ret.text.encode('ascii', 'ignore')
    
    return (n, '')

def create_ticket_index(client, index):
    client.indices.create(
        index=index,
        body={
          'settings': {
            "analysis": {
                  "filter": {
                    "english_stop": {
                      "type":       "stop",
                      "stopwords":  "_english_"
                    },
                    "english_possessive_stemmer": {
                      "type":       "stemmer",
                      "language":   "possessive_english"
                    }
                  },
                "char_filter": { 
                    "quotes": {
                      "type": "mapping",
                      "mappings": [ 
                        "'=>",
                      ]
                    }
                  },
                  "analyzer": {
                    "english_ticket": {
                      "tokenizer": "standard",
                      "char_filter": [ "quotes" ],
                      "filter": [
                        "english_possessive_stemmer",
                        "lowercase",
                        "english_stop",
                        "asciifolding",
                        "kstem" 
                      ]
                    }
                  }
                }
          },
          'mappings': {
            'ticket': {
              'properties': {
                'created_ts': {'type': 'date', "format": "yyyy-MM-dd'T'HH:mm:ss"},
                'updated_ts': {'type': 'date', "format": "yyyy-MM-dd'T'HH:mm:ss"},
                'issue_id': {'type': 'integer'},
                'user_email': {'type': 'string', 'index' : 'not_analyzed'},
                'agent_email': {'type': 'string', 'index' : 'not_analyzed'},
                'game': {'type': 'string', 'index' : 'not_analyzed'},
                'status': {'type': 'string', 'index' : 'not_analyzed'},
                'lang': {'type': 'string', 'index': 'not_analyzed'},
                'description': {
                    'type': 'string', 
                    "fields": {
                        "english": { 
                          "type":     "string",
                          "analyzer": "english_ticket"
                        }
                  }
                },
                'os': {'type': 'string'},
                'farm_id': {'type': 'string', 'index': 'not_analyzed'},
                'rating': {'type': 'integer'},
                'feedback': {'type': 'string'}
              }
            }
          }
        },
        ignore=400
    )

def create_other_lang_index(client, index, lang):
    client.indices.create(
        index=index,
        body={
            'mappings': {
            'ticket': {
              'properties': {
                'issue_id': {'type': 'integer'},
                'user_email': {'type': 'string', 'index' : 'not_analyzed'},
                'agent_email': {'type': 'string', 'index' : 'not_analyzed'},
                'game': {'type': 'string', 'index' : 'not_analyzed'},
                'status': {'type': 'string', 'index' : 'not_analyzed'},
                'os': {'type': 'string'},
                'lang': {'type': 'string', 'index': 'not_analyzed'},
                'description': {
                    'type': 'string', 
                    "fields": {
                        "analyzed": { 
                          "type":     "string",
                          "analyzer": lang
                        }
                  }
                },
                "translation": {"type": "string", "analyzer": "english"},
                'farm_id': {'type': 'string', 'index': 'not_analyzed'},
                'rating': {'type': 'integer'},
                'feedback': {'type': 'string'},
                'created_ts': {'type': 'date', "format": "yyyy-MM-dd'T'HH:mm:ss"},
                'updated_ts': {'type': 'date', "format": "yyyy-MM-dd'T'HH:mm:ss"}
              }
            }
          }
        },
        ignore=400
    )

def parse_tickets(df, op_type='index', doctype='ticket'):
    h = HTMLParser.HTMLParser()
    es_mapping_dict = {'IssueId': 'issue_id', 'UserEmail': 'user_email', 'AppName': 'game', 'IssueStatus': 'status', \
                   'IssueDescription': 'description', 'IssueCreatedTime': 'created_ts', 'IssueUpdatedTime': 'updated_ts', \
                   'DevicePlatform': 'os', 'farm_id': 'farm_id', 'Rating': 'rating', 'Feedback': 'feedback', \
                    'AgentEmail': 'agent_email', 'translation': 'translation', 'langid': 'lang', 'IssueTags': 'tags'} 
    s = df[[x for x in es_mapping_dict.keys() if x in df.columns]].rename(columns=es_mapping_dict).to_dict(orient='records')
    for o in s: 
        o['description'] = h.unescape(o['description'].decode('unicode_escape', errors='ignore'))
        if not o['user_email'] or o['user_email']=='NULL':
            o.pop('user_email', '')
        ret = {}
        ret['_type'] = doctype
        ret['_id'] = o['issue_id']
        ret['_op_type'] = op_type
        ret['_source'] = o
        yield ret

def replace_query_title(es, keywords, es_obj, doc_type, suffix, index='helpshift-en', prefix='ticket_en-visualization_', title_prefix=''):
    #es_obj is a complete result from a es query (id, source, etc)
    #this function changes the title of the kibana object (search/visualization) and indexes it in ES
    assert type(keywords)==list
    query_str = " AND ".join(["description.english: "+ x for x in keywords])
    j = json.loads(es_obj['_source']['kibanaSavedObjectMeta']['searchSourceJSON'])
    j['query']['query_string']['query'] = query_str
    j['index'] = index
    es_obj['_source']['kibanaSavedObjectMeta']['searchSourceJSON'] = json.dumps(j)
    es_obj['_source']['title'] = title_prefix + ' '.join(keywords)
    es.index(index=".kibana", doc_type=doc_type, id=str(prefix)+str(suffix), body=es_obj['_source'])
    #return es_obj['_source']

def es_create_panels(es, keywords, prefix='ticket_en-visualization_', n=20):
    #keywords is a list of lists
    #creates as many visualization and search panels in kibana as needed, using the provided keywords
    viz_obj = es.search(index='.kibana', doc_type='visualization', body={"query": {"prefix" : {
        "_id" : prefix
    }}})['hits']['hits'][0]
    
    search_obj = es.search(index='.kibana', doc_type='search', body={"query": {"prefix" : {
        "_id" : prefix
    }}})['hits']['hits'][0]
    
    for i in range(1,n+1):
        if len(keywords)<i:
            k_list = keywords[-1]
        else:
            k_list = keywords[i-1]
    #visualization
        replace_query_title(es, list(k_list), viz_obj, 'visualization', suffix=i)       
    #search
        replace_query_title(es, list(k_list), search_obj, 'search', suffix=i, title_prefix='Ticket-en: ')

def trending_bigrams(df, source, date_col='IssueCreatedDate', text_col='stem', num_days=6, last_n_days=7, stop_bigrams=[]):
    #for each of the last num_days (d), grab new tickets for the last num_days up to d
    kw_rank_dict = {}
    for d in pd.date_range(df[date_col].max()-pd.DateOffset(days=num_days), periods=num_days+1):
        recent_tickets = df[((d - pd.to_datetime(df[date_col]))/np.timedelta64(1,'D')<=last_n_days) & (d>=pd.to_datetime(df[date_col]))]
        #using recent tickets, compute tfidf of each keyword
        finder = BigramCollocationFinder.one_from_each_doc(recent_tickets[text_col].values, window_size=5)
        tfidf_docs = []
        #date_list = []
        for k, v in recent_tickets.groupby(date_col)[text_col]:
            current_1grams = {}
            compressed_tokens = []
            #date_list.append(k)
            for t in v:
                for x in t:
                    if x not in current_1grams:
                        compressed_tokens.append(x)
                        current_1grams[x] = 1
            tfidf_docs.append(compressed_tokens)

        dictionary = corpora.Dictionary(tfidf_docs)
        corpus = [dictionary.doc2bow(text) for text in tfidf_docs]
        tfidf = models.TfidfModel(corpus)
        corpus_tfidf = tfidf[corpus]

        #tfidf of keywords at d
        tfidf_dict = {}
        for o in sorted(corpus_tfidf[-1], key=lambda x: -x[1]):
            tfidf_dict[dictionary[o[0]]] = o[1]

        bigram_measures = nltk.collocations.BigramAssocMeasures()
        ranks = {}

        texts = recent_tickets[pd.to_datetime(recent_tickets[date_col])==d][text_col].values

        #bigram collocation finder with window size 5
        finder = BigramCollocationFinder.one_from_each_doc(texts, window_size=5, stop_bigrams=stop_bigrams)
        finder.apply_freq_filter(3)
        
        #scoring, multiplying likelihood and tfidf for individual keywords in bigram
        #other options are bigram_measures.pmi and bigram_measures.raw_freq
        for f in finder.score_ngrams(bigram_measures.likelihood_ratio):
            #tf-idf for a term that appears in every document is 0, so dict might not have that term
            a = tfidf_dict.get(f[0][0],0.02)
            b = tfidf_dict.get(f[0][1],0.02)
            #print f[0], f[1], a, b
            #ranks[f[0]] = f[1]*max(a,b)
            score = f[1]*max(a,b)*min(a,b,0.08)*np.log(finder.ngram_fd[f[0]])
            ranks[f[0]] = (score, finder.ngram_fd[f[0]])

        kw_rank = []
        print d
        for r in sorted(ranks.items(), key=lambda x: -x[1][0])[:20]:
            q = ' '.join(r[0])
            params = {"appId":9999, 'source':source, 'textType': 'mail', 'target':'en', 'q': q}
            if source.lower() not in ['en', 'english']:
                try:
                    translate = requests.get("http://translate.funplusgame.com/api/v2/translate", params = params).json().get('translation', '').get('targetText', '')
                except:
                    translate = 'translation failed'
            else:
                translate = ''    
            
            kw_rank.append(list(r)+[translate])
            print q, r[1], ' : ', translate

        kw_rank_dict[d] = kw_rank

    return kw_rank_dict
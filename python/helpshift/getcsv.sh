m=0
if [ -n "$1" ] && [ $1 -eq 1 ] 
then 
	grep_arg="issues-daily"
	folder="issues"
else
	grep_arg="csat-daily"
	folder="ratings"
fi

while read -r f
do 
 z=$(echo $f | rev | cut -d'/' -f1 | rev | cut -d'.' -f1 | sed 's/-//g' | bc)
 if [ $z -gt $m ] 
 	then m=$z 
 fi
done< <(find ./csv/$folder -iname "*.csv")
echo $m

fcount=0
while read i
do 
	IFS=" " read b <<< "$(echo $i)"
	re='[0-9]{4}-[0-9]{2}-[0-9]{2}'
	if [[ $b =~ $re ]]; then
    	c=${BASH_REMATCH[0]}
    	a=${c//-}
		if [ $a -gt $m ]
		then 
			$(which s3cmd) get --force $b ./csv/$folder/$c.csv.gz 
			fcount=$((fcount+1))
		fi
	fi
done< <($(which s3cmd) -r ls s3://com.funplus.helpshift/ | grep $grep_arg | awk '{print $4}')

echo $fcount
if [ $fcount -gt 0 ]
then 
    gunzip -fq ./csv/$folder/*.gz
fi
import datetime
import os
import commands

# Clean log files older than 14 days
class LogCleaner:

    def cleanLocalFiles(self, path):
        # Clean local log files
        if (os.path.exists(path)):
            if (os.path.isfile(path)):
                os.system("> %s" % (path))
            else:
                status,output = commands.getstatusoutput("ls -ltr --time-style='+%%Y-%%m-%%d %%H:%%M' %s | awk '{print $6,$8}'" % (path))
                if (status == 0):
                    result = list()
                    for line in output.split('\n'):
                        if len(line.strip()) > 0:
                            line_list = line.split()
                            today = datetime.date.today()
                            if (today - datetime.datetime.strptime(line_list[0], '%Y-%m-%d').date()).days >= 14:
                                result.append(line_list[1])

                    for file in result:
                        os.system("rm -rf %s%s" % (path,file))

    def cleanHdfsFiles(self, path):
        # Clean HDFS log files
        status,output = commands.getstatusoutput("hadoop fs -test -e %s" % (path))
        if (status == 0):
            status,output = commands.getstatusoutput("hadoop fs -ls %s | awk '{print $6,$8}'" % (path))
            if (status == 0):
                result = list()
                for line in output.split('\n'):
                    if len(line.strip()) > 0:
                        line_list = line.split()
                        today = datetime.date.today()
                        if (today - datetime.datetime.strptime(line_list[0], '%Y-%m-%d').date()).days >= 14:
                            result.append(line_list[1])

                for file in result:
                    os.system("hadoop fs -rm -r -f %s" % (file))


def main():
    lc = LogCleaner()
    # Clean local log files
    lc.cleanLocalFiles("/mnt/var/run/hadoop-state-pusher/hadoop-state-pusher-batch-log.txt")
    lc.cleanLocalFiles("/mnt/var/lib/hadoop-state-pusher/jobcache/")
    lc.cleanLocalFiles("/mnt/var/log/hive/tmp/hadoop/")
    # Clean HDFS log files
    lc.cleanHdfsFiles("/var/log/hadoop-yarn/apps/hadoop/logs/")
    lc.cleanHdfsFiles("/var/log/spark/apps/")

if __name__ == '__main__':
    main()

import psycopg2
import yaml
import pprint
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import Encoders
import os
import HTML
import datetime
import textwrap
import time
import httplib
import urllib

class FraudMonitor:

    def __init__(self):
        self.results = {}
        self.yamls = ['koa_fraud.yaml']

    def run(self):
        for filename in self.yamls:
            self.setDBParams(filename)
            conn = self.getConnection()
            result = self.runScripts(conn)
            self.results[self.db_name] = result
            if len(result[0]) > 0:
                # generate csv file
                self.genCSVFile(conn)
                # send mail
                self.sendMail()

    def runScripts(self, conn):
        cursor = conn.cursor()
        result = []

        command = self.fraud_command
        try:
            cursor.execute(command)
            result.append(cursor.fetchall())
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def genCSVFile(self, conn):
        cursor = conn.cursor()
        result = []

        command = self.fraud_csv_command
        try:
            cursor.execute(command)
            result.append(cursor.fetchall())

            # Generate csv file
            header = ['install_source', 'sub_publisher', 'install_date', 'fraud_prob', 'install_count', 'd1_retention', 'd7_retention', 'ip_rep_count',
                      'convert_fraud_count', 'ts_fraud_count', 'device_fraud_count', 'level1_percent', 'install_discrepancy', 'country_fraud_percent']
            f = open('/tmp/FraudInstalls_' + 'KOA' + '.csv', 'w')
            title = ''
            for item in header:
                title = title + item + ','
            print >> f, title
            for record in result[0]:
                tmp_row = ''
                for item in record:
                    tmp_row = tmp_row + str(item) + ','
                print >> f, tmp_row
            f.close()

        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self, filename):
        filename = '/mnt/funplus/data-analytics/python/conf/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.fraud_email_list = confMap['tasks']['defaults']['fraud_email_list']
        self.fraud_command = confMap['tasks']['defaults']['fraud_command']
        self.fraud_csv_command = confMap['tasks']['defaults']['fraud_csv_command']
        #self.aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        #self.aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']

    def sendMail(self):

        header = """
        <p>
        <br/>
        <b>KOA Fraud Monitor Strategies: </b>
            <ul>
            <li>50 or more installs in one day</li>
            <li>Percent of D1 retention < 15% or D1 retention > 45%</li>
            <li>Percent of D7 retention < 5% </li>
            <li>Same IP per sub publisher > 100 </li>
            <li>Conversion duration (click to install duration) < 30s </li>
            <li>Time spend per user < 60s </li>
            <li>One device id has more than 3 user_ids </li>
            <li>Percent of level 1 with more than 2 active days > 10%</li>
            <li>Percent of discrepancy between Adjust and BI  > 20%</li>
            <li>Percent of country discrepancy of campaigns > 10%</li>
            </ul>
        </p>
        """

        table_header = ['Install Source', 'Install Period', 'Fraud Probability','Install Count', 'Fraud Sub Publisher Percent', 'Fraud Reason']
        col_width = ['10%']*5+(['50%']*1)

        t = HTML.Table(header_row = table_header,col_width=col_width)
        color = 'AliceBlue'
        for record in self.results[self.db_name][0]:
            tmp_row = []
            # Install Source
            value = record[0]
            tmp_row.append(HTML.TableCell(value, bgcolor = color))
            # Install Date
            value = str(record[12]) + ' ~ ' + str(datetime.date.today() - datetime.timedelta(2))
            tmp_row.append(HTML.TableCell(value, bgcolor = color, width = '100px', align = 'center'))
            # Fraud Probability
            value = str(round(record[14] * 100, 1)) + '%' if record[14] else '0.0%'
            if (record[14] >= 0.5):
                tmp_row.append(HTML.TableCell(value, bgcolor='Tomato', align='center'))
            elif (record[14] < 0.5 and record[14] >= 0.3):
                tmp_row.append(HTML.TableCell(value, bgcolor='Gold', align='center'))
            else:
                tmp_row.append(HTML.TableCell(value, bgcolor=color, align='center'))
            # Install Count
            value = "{:,}".format(record[2])
            tmp_row.append(HTML.TableCell(value, bgcolor=color, align='center'))
            # Fraud Sub Publisher Count
            value = "{:,}".format(record[1])
            tmp_row.append(HTML.TableCell(value, bgcolor=color, align='center'))
            # Fraud Reason
            def str_output(i,desc):
                a = desc if record[i] >0 else ''
                v = str(round(record[i] * 100, 1)) + '%' if record[i]>0 else ''
                if a <> '' and v <> '':
                    output = a + ': (' + v +')'
                else:
                    output = ''
                return output
            dict = {3:'D1 Retention Fraud',
                    4:'D7 Retention Fraud',
                    5:'Same IP Address Fraud',
                    6:'Conversion Duration Fraud',
                    7:'Time Spend Fraud',
                    8:'Device ID Fraud',
                    9: 'Level1 Fraud',
                    10: 'Install Discrepancy Fraud',
                    11: 'Country Discrepancy Fraud'
                    }
            str_list = []

            for i in range(3,12):
                str_list.append(str_output(i,dict[i]))
            reason_list = list(filter(None,str_list))
            value = ', '.join(reason_list)
            tmp_row.append(HTML.TableCell(value, bgcolor = color, align = 'center'))
            # Append the row
            t.rows.append(tmp_row)
            # Change color
            if (color == 'white'):
                color = 'AliceBlue'
            else:
                color = 'white'

        # Generate HTML
        htmlcode = header+ str(t)
        #print htmlcode

        # Send Email
        msg = MIMEMultipart()
        msg['Subject'] = '[' + 'KOA' + '] Market Fraud Monitor ' + datetime.date.today().strftime("%Y-%m-%d")
        msg['From'] = "Anti-Fraud<tableau_admin@funplus.com>"
        msg['To'] = self.fraud_email_list

        # attach html code
        html = MIMEText(htmlcode, 'html')
        msg.attach(html)

        # attach csv file
        csvFile = '/tmp/FraudInstalls_' + 'KOA' + '.csv'
        part = MIMEBase('application', 'octet-stream')
        part.set_payload( open(csvFile, 'rb').read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(csvFile)))
        msg.attach(part)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], self.fraud_email_list.split(','), msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue

def main():
    fm = FraudMonitor()
    fm.run()

if __name__ == '__main__':
    main()
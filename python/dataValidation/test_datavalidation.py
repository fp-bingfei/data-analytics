###########
# Required modules:
# 1. yum install PyYAML
# 2. yum install python-psycopg2
# 3. yum install python-argparse
# 4. wget http://www.decalage.info/files/HTML.py-0.04.zip


import psycopg2
import yaml
import pprint
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os
import HTML
import datetime

class DataValidation:

    def __init__(self):
        self.results = {}

    def run(self):
        for filename in os.listdir("/mnt/funplus/analytics/dataValidation/conf"):
            if filename.endswith('.yaml'):
                self.setDBParams(filename)
                conn = self.getConnection()

                if filename != 'ha_1_2.yaml':
                    result = self.runScripts(conn)
                else:
                    result = self.runScripts(conn, 1)
                
                self.results[filename.split('.')[0]] = result

        #for key in self.results.keys():
            #print 'key=%s, value=%s' % (key, self.results[key])
        self.sendMail()

    def runScripts(self,conn, flag = 0):

        pathToScript = '/mnt/funplus/analytics/dataValidation/conf/sql_script'
        f = open(pathToScript, "r")
        sqlFile = f.read()
        f.close()
        if flag == 1:
            sqlFile = sqlFile.replace('processed', 'processed_new')
        sqlCommands = sqlFile.split(';')
        
        cursor = conn.cursor()
        result = []

        yesterday = datetime.date.today() - datetime.timedelta(1)
        yesterday = yesterday.strftime('%Y-%m-%d')
        yesterday = "'" + yesterday + "'"

        for command in sqlCommands:
            #print  pathToScript + 'Query: ' + line
            command = command.replace('yesterday', yesterday)
            if command.strip() == '':
                break

            try:
                cursor.execute(command)
                result.append(cursor.fetchall())
            except Exception, e:
                result.append('1')
                conn.rollback()

        return result

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self, filename):
        filename = '/mnt/funplus/analytics/dataValidation/conf/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        #self.aws_access_key_id = confMap['dbinfo']['aws_access_key_id']
        #self.aws_secret_access_key = confMap['dbinfo']['aws_secret_access_key']

    def sendMail(self):
        htmltext = '''<p>SUCCESS: result is empty</p>
        <p>WARNING: result is NON-empty</p>
        <p>FAIL: result is NON-empty and needs to check immediately</p>
        <p>N/A: command is not runnable</p>'''

        t = HTML.Table(header_row=['Game', 'dim_user<br />Is_payer', \
            'fact_revenue<br />Duplicates','fact_revenue<br />Sanity check','fact_session<br />Duplicates','fact_dau_snapshot<br />Duplicates',\
            'fact_session<br />Sanity check','fact_levelup<br />Duplicates','fact_levelup<br/>Sanity check'])

        for key in self.results.keys():
            items = [key]
            i = 0
            for item in self.results[key]:
                color = 'lime' if len(item) == 0 or item == '1' else 'red'
                color = 'yellow' if item == '1' else color
                color = 'orange' if i in [3, 4, 6, 8] and color == 'red' else color
                flag = 'SUCCESS' if len(item) == 0 else 'FAIL'
                flag = 'N/A' if item == '1' else flag
                flag = 'WARNING' if i in [3, 4, 6, 8] and flag == 'FAIL' else flag
                colored_item = HTML.TableCell(flag, bgcolor=color)
                items.append(colored_item)
                i += 1
            t.rows.append(items)

        htmlcode = str(t) + htmltext
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmlcode, 'html')
        msg.attach(html)
        msg['Subject'] = 'Data Validation results of redshift ' + (datetime.date.today() - datetime.timedelta(1)).strftime("%Y-%m-%d")
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        recipients = ['ting.jia@funplus.com']
        msg['To'] = ', '.join(recipients)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], msg['To'], msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue


def main():
    dv = DataValidation()
    dv.run()

if __name__ == '__main__':
    main()

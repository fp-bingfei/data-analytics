import psycopg2
import HTML
import datetime
import smtplib
import locale

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
class InfraCostSummary:

    def __init__(self):
        self.currentMonth = (datetime.date.today() - datetime.timedelta(20)).strftime('%Y-%m')
        self.earlierMonth = (datetime.date.today() - datetime.timedelta(50)).strftime('%Y-%m')

        #self.allgames = [('921287511715', 'data','zhenxuan.yang@funplus.com')]
        self.allgames = [('131433655760', 'fruitscoot', ['fs_pm@funplus.com']),
                            ('135620544099', 'poker', ['dan.hao@funplus.com']),
                            ('147744843438', 'waf', ['waf_pm@funplus.com', 'waf_backend@funplus.com']),
                            ('266508744455', 'devops', ['ops@funplus.com']),
                            #('289997590022', 'ShineZone'),
                            ('370884182004', 'battlewarship', ['rui.li@funplus.com']),
                            ('376371647872', 'Niedan', ['nanqing.liu@funplus.com']),
                            ('403045566686', 'NLP', ['weihua.fan@funplus.com']),
                            ('424384843432', 'farm', ['farm_pm_new@funplus.com', 'farm_php@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('431036357599', 'royal', ['rs_pm@funplus.com', 'junjie.ning@funplus', 'yan.zhang@funplus.com', 'kai.zheng@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('457994678495', 'royal', ['rs_pm@funplus.com', 'junjie.ning@funplus', 'yan.zhang@funplus.com', 'kai.zheng@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('464507519694', 'galaxy', ['galaxyzero@funplus.com']),
                            ('484933957969', 'Smash Island', ['yaran.hou@funplus.com']),
                            ('541015476938', 'farm', ['farm_pm_new@funplus.com', 'farm_php@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('583300917620', 'Infra', ['xiaoxiang.du@funplus.com']),
                            ('614675119462', 'SIP', ['sip@funplus.com']),
                            ('634033320744', 'xiyou', ['hualin.pang@funplus.com', 'nanqing.liu@funplus.com']),
                            ('645037889100', 'LegendsAwake', ['yang.zhang@funplus.com','nanqing.liu@funplus.com']),
                            ('677477847658', 'Littlechef', ['josh.babich@funplus.com']),
                            ('683077249754', 'Platform', ['distribution@funplus.com']),
                            ('687657786659', 'Funplus', ['ops@funplus.com']),
                            #('716504217123', 'Slots'),
                            ('735714818384', 'ffs', ['ffspm@funplus.com', 'zhengyang.zhu@funplus.com', 'ge.yang@funplus.com', 'hui.wang@funplus.com', 'yilei.bao@funplus.com', 'xiaochun.liu@funplus.com', 'huibin.zhu@funplus.com', 'hualiang.zhang@funplus.com']),
                            #('753756377410', 'kuaiya'),
                            ('838011746804', 'daota', ['dotarena@funplus.com', 'nanqing.liu@funplus.com', 'yang.zhang@funplus.com']),
                            ('873210112193', 'farm', ['farm_pm_new@funplus.com', 'farm_php@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('889096239409', 'dragonwar', ['yuning.liu@funplus.com', 'jipeng.sun@funplus.com','li.tian@funplus.com', 'heng.shi@funplus.com']),
                            ('918819464745', 'farm', ['farm_pm_new@funplus.com', 'farm_php@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('921287511715', 'data', ['data@funplus.com']),
                            ('924801593386', 'ha', ['ha_pm@funplus.com', 'yongjun.li@funplus.com', 'pan.wang@funplus.com', 'xiao.shi@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('941628076438', 'ffs', ['ffspm@funplus.com', 'zhengyang.zhu@funplus.com', 'ge.yang@funplus.com', 'hui.wang@funplus.com', 'yilei.bao@funplus.com', 'xiaochun.liu@funplus.com', 'huibin.zhu@funplus.com', 'hualiang.zhang@funplus.com']),
                            ('972926864295', 'mt2', ['linxun.zhang@funplus.com', 'nanqing.liu@funplus.com']),
                            ('758529155342', 'ValiantForce', ['nanqing.liu@funplus.com']),
                            ('670914317338', 'LOE', ['chengguo.huang@funplus.com']),
                            ('647268852127', 'Wartide', ['hualin.pang@funplus.com' 'nanqing.liu@funplus.com']),
                            ('860495783590', 'Maitai Madness', ['edric.chitra@funplus.com']),
                            ('618191603327', 'Smash Island CDN', ['yaran.hou@funplus.com']),
                            ('956231806574', 'Sandigma', ['hualin.pang@funplus.com', 'nanqing.liu@funplus.com']),
                            ('069809866012', 'Inke', ['huang.li@funplus.com', 'chaobing.zhang@funplus.com', 'fang.li@funplus.com', 'nan.jiang@funplus.com']),
                            ('575908059789', 'Vegas', ['wenyang.fang@funplus.com'])]


    def run(self):
        conn = self.getConn()
        for gameinfo in self.allgames:
            tmpRes = self.getSQLResult(conn, gameinfo)
            htmltext = self.parseResult(tmpRes, gameinfo)
            self.sendMail(gameinfo, htmltext)
        conn.close()


    def getConn(self):
        db_host = 'kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
        db_port = '5439'
        db_name = 'kpi'
        db_username = 'biadmin'
        db_password = 'Halfquest_2014'
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def getSQLResult(self, conn, gameinfo):
        cursor = conn.cursor()

        command = """SELECT month
                    ,productname
                    ,coalesce(case when operation = 'BI Usage' then 'BI'
                        else reservedinstance end, 'N')
                    ,sum(truecost)
                    FROM db_usage.monthly_billing
                    WHERE linkedaccountid = '%s'
                    AND month >= '%s'
                    AND month <= '%s'
                    GROUP BY 1,2,3
                    ORDER BY 4 DESC""" % (gameinfo[0], self.earlierMonth, self.currentMonth)

        try:
            cursor.execute(command)
            result = cursor.fetchall()
            print 'Fetching data for account: ' + gameinfo[0]
            print 'Success!'
        except Exception, e:
            result.append('1')
            print 'Fetching data for account: ' + gameinfo[0]
            print 'Error: ',
            print e
            conn.rollback()

        return result

    def parseResult(self, tmpRes, gameinfo):
        htmltext = '''
                <h3>This is a report of your monthly Infrastructure cost, one email per AWS account %s. </h3>
                ''' % gameinfo[0]
        htmlfoot = '''
                <p>Color 'Green', 'Orange', 'Red' means 'Percent Diff <= 0', '0 < Percent Diff <= 5', 'Percent Diff > 5 and Diff > $50'.</p>
                <p>If you receive more than one email, it means you have multiple accounts.</p
                <p>Please send email to data@funplus.com if you have any suggestions on the report, Thanks.</p>
                '''
        usageMap = {'Y': 'Reserved', 'N': 'On-Demand', 'BI': 'BI Cost', '--': '--'}
        locale.setlocale(locale.LC_ALL, 'en_US')
        table_header = ['Product Name', 'Usage Type',
                        self.currentMonth, 'Percent of Total',
                        self.earlierMonth, 'Percent Diff']
        t = HTML.Table(header_row = table_header)

        total_cost = sum([x[3] for x in tmpRes if x[0] != self.earlierMonth])
        total_previous_cost = sum([x[3] for x in tmpRes if x[0] == self.earlierMonth])
        tmpRes.append((self.currentMonth, 'Total Cost', '--', total_cost))
        tmpRes.append((self.earlierMonth, 'Total Cost', '--', total_previous_cost))
        for line in tmpRes:
            tmp_row = []
            if line[0] != self.earlierMonth:
                tmp_product = line[1]
                tmp_usage = usageMap[line[2]]
                tmp_cost = float(line[3].to_eng_string()) if line[3] != 0 else 0
                percent_total = tmp_cost / float(total_cost.to_eng_string()) * 100 if total_cost != 0 else 0
                for otherline in tmpRes:
                    if (tmp_product == otherline[1] and tmp_usage == usageMap[otherline[2]] and otherline[0] == self.earlierMonth):
                        tmp_previous_cost = float(otherline[3].to_eng_string()) if otherline[3] != 0 else 0
                        percent_diff = (tmp_cost - tmp_previous_cost) / tmp_previous_cost * 100 if tmp_previous_cost != 0 else 0
                        if percent_diff > 5 and (tmp_cost - tmp_previous_cost) > 50:
                            color = 'red'
                        elif percent_diff > 0:
                            color = 'orange'
                        else:
                            color = 'lime'
                        tmp_row = [HTML.TableCell(tmp_product),
                                    HTML.TableCell(tmp_usage),
                                    HTML.TableCell('$' + locale.format("%.2f", tmp_cost, grouping=True)),
                                    HTML.TableCell(locale.format("%.2f", percent_total, grouping=True) + '%'),
                                    HTML.TableCell('$' + locale.format("%.2f", tmp_previous_cost, grouping=True)),
                                    HTML.TableCell(locale.format("%.2f", percent_diff, grouping=True) + '%', bgcolor = color)]
                    else:
                        if len(tmp_row) == 0:
                            tmp_row = [HTML.TableCell(tmp_product),
                                    HTML.TableCell(tmp_usage),
                                    HTML.TableCell('$' + locale.format("%.2f", tmp_cost, grouping=True)),
                                    HTML.TableCell(locale.format("%.2f", percent_total, grouping=True) + '%'),
                                    HTML.TableCell('$' + locale.format("%.2f", 0, grouping=True)),
                                    HTML.TableCell(locale.format("%.2f", 0, grouping=True) + '%', bgcolor = 'lime')]
                t.rows.append(tmp_row)
            else:
                continue

        # for line in tmpRes:
        #     tmp_row = [line[0], line[1]]
        #     if line[2] == 'Y':
        #         usageType = 'Reserved'
        #     elif line[2] == 'BI':
        #         usageType = 'BI Usage'
        #     else:
        #         usageType = 'On Demand'
        #     tmp_row.append(usageType)
        #     tmp_row.append(round(float(line[3].to_eng_string()), 2))
        #     t.rows.append(tmp_row)

        htmlcode = htmltext + str(t) + htmlfoot
        return htmlcode


    def sendMail(self, gameinfo, htmltext):
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmltext,'html')
        msg.attach(html)

        recipients = gameinfo[2] + ['data@funplus.com', 'ops@funplus.com']
        msg['Subject'] = 'Infrastructure Cost ' + self.currentMonth + ' ' + gameinfo[1]
        msg['From'] = "Raw Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ','.join(recipients)

        for x in range(0,3):
            try:
                s = smtplib.SMTP_SSL('email-smtp.us-west-2.amazonaws.com',465)
                s.login('AKIAJKBQSFVYRX7GHDYA','AqEm4+h5ZiCFqbYsumD5gFUTPJlJXyflAOcIfrylnKnw')
                s.sendmail(msg['From'], recipients, msg.as_string())
                s.quit()
                break
            except smtplib.socket.error:
                print ("==== smtplib.socket.error ===\n")
                print ("==== Re-trying ....  ====\n")
                continue
            except smtplib.SMTPException:
                print ("==== smtplib.SMTPException ====\n")
                print ("==== Re-trying ....  ====\n")
                continue

def main():
    ics = InfraCostSummary()
    ics.run()

if __name__ == '__main__':
    main()

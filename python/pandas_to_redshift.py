
# coding: utf-8

# In[96]:

from __future__ import division, print_function

try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO

import gzip
import json
import math
import os
import re

try:
    from urlparse import urljoin
except ImportError:
    from urllib.parse import urljoin
import uuid

from boto.s3.connection import S3Connection
from boto.s3.connection import OrdinaryCallingFormat
import pandas as pd
import psycopg2
import datetime
import numpy as np

def chunk_dataframe(df, num_chunks):
    """Chunk DataFrame into `chunks` DataFrames in a list"""
    chunk_size = int(math.floor(len(df) / num_chunks)) or 1
    chunker = list(range(chunk_size, len(df), chunk_size)) or [chunk_size]
    if len(df) == num_chunks:
        chunker.append(len(df))
    last_iter = 0
    df_list = []
    for c in chunker:
        if c == chunker[-1]:
            c = len(df)
        df_list.append(df[last_iter:c])
        last_iter = c
    return df_list

#given an aws endpoint, return the region it is in
def get_aws_region(path):
    aws_regions = ['us-east-1',
                         'us-west-1',
                         'us-west-2',
                         'eu-west-1',
                         'eu-central-1',
                         'ap-southeast-1',
                         'ap-southeast-2',
                         'ap-northeast-1',
                         'cn-north-1',
                         'us-gov-west-1']
        
    for r in aws_regions:
        if r in path:
            return r
        
    return None

class PGShift(object):

    def __init__(self, table):
        self.table = table
        self.manifest_url = None

    def put_to_s3(self, bucket_name='tangtest', keypath='/pgshift/', chunks=1, aws_access_key_id=None,
                  aws_secret_access_key=None, mandatory_manifest=True):
        """
        Will put the result table to S3 as a gzipped CSV with an accompanying
        .manifest file. The aws keys are not required if you have environmental
        params set for boto to pick up:
        http://boto.readthedocs.org/en/latest/s3_tut.html#creating-a-connection
        Each call to this function will generate a unique UUID for that
        particular run.
        Ex: If bucket is 'mybucket', keypath is 'pgshift/temp/',
        and chunks is 2, then will write the following:
        s3://mybucket/pgshift/temp/pgdump_uuid_0.gz
        s3://mybucket/pgshift/temp/pgdump_uuid_1.gz
        s3://mybucket/pgshift/temp/pgtemp.manifest
        Parameters
        ----------
        bucket_name: str
            S3 bucket name
        keypath: str
            Key path for writing file
        chunks: int, default 1
            Number of gzipped chunks to write. Upload speed
            is *much* faster if chunks = multiple-of-slices. Ex: DW1.XL nodes
            have 2 slices per node, so if running 2 nodes you will want
            chunks=4, 8, etc
        aws_access_key_id: str, default None
        aws_secret_access_key: str, default None
        mandatory_manifest: bool, default True
            Should .manifest entries be mandatory?
        """
        if aws_access_key_id and aws_secret_access_key:
            self.conn = S3Connection(aws_access_key_id, aws_secret_access_key)
        else:
            self.conn = S3Connection()

        
        self.bucket = self.conn.get_bucket(bucket_name)        
        endpoint = self.bucket.get_website_endpoint()
        self.region = get_aws_region(endpoint)
        print(endpoint)

        self.manifest = {'entries': []}
        self.generated_keys = []
        table_chunks = chunk_dataframe(self.table, chunks)
        batch_uuid = str(uuid.uuid4())
        for idx, chunk in enumerate(table_chunks):
            zipname = '_'.join(['pgdump', batch_uuid, str(idx)]) + '.gz'
            fp, gzfp = StringIO(), StringIO()
            csvd = chunk.to_csv(fp, index=False, header=False)
            fp.seek(0)
            url = urljoin(keypath, zipname)
            key = self.bucket.new_key(url)
            self.generated_keys.append(url)
            gzipped = gzip.GzipFile(fileobj=gzfp, mode='w')
            gzipped.write(fp.read())
            gzipped.close()
            gzfp.seek(0)
            print('Uploading {0}...'.format(self.bucket.name + url))
            key.set_contents_from_file(gzfp)

            self.manifest['entries'].append({
                'url': ''.join(['s3://', self.bucket.name, url]),
                'mandatory': mandatory_manifest}
                )

        manifest_name = 'pgshift_{0}.manifest'.format(batch_uuid)
        fest_url = urljoin(keypath, manifest_name)
        self.generated_keys.append(fest_url)
        self.manifest_url = ''.join(['s3://', self.bucket.name, fest_url])
        fest_key = self.bucket.new_key(fest_url)
        fest_fp = StringIO(json.dumps(self.manifest, sort_keys=True,
                                      indent=4))
        fest_fp.seek(0)
        print('Uploading manifest file {0}...'.format(
            self.bucket.name + fest_url))
        fest_key.set_contents_from_file(fest_fp)

    def clean_up_s3(self):
        """Clean up S3 keys generated in `put_to_s3`"""
        for key in self.generated_keys:
            print('Deleting {0}...'.format(self.bucket.name + key))
            self.bucket.delete_key(key)

    def copy_to_redshift(self, table_name, aws_access_key_id=None,
                         aws_secret_access_key=None, database=None, user=None,
                         password=None, host=None, port=None, sslmode=None,
                         sortkey=None, distkey=None, append=False):
        """
        COPY data from S3 to Redshift using the data and manifest generated
        with `put_to_s3`, which must be called first in order to
        perform the COPY statement.
        Parameters
        ----------
        table_name: str
            Table name to copy data to
        aws_access_key_id: str
        aws_secret_access_key: str
        database: str, if None os.environ.get('PGDATABASE')
        user: str, if None os.environ.get('PGUSER')
        password: str, if None os.environ.get('PGPASSWORD')
        host: str, if None os.environ.get('PGHOST')
        port: int, if None os.environ.get('PGPORT') or 5439
        sslmode: str
            sslmode param (ex: 'require', 'prefer', etc)
        """
        aws_secret_access_key = (aws_secret_access_key
                                 or os.environ.get('AWS_SECRET_ACCESS_KEY'))
        aws_access_key_id = (aws_access_key_id
                             or os.environ.get('AWS_ACCESS_KEY_ID'))

        database = database or os.environ.get('PGDATABASE')
        user = user or os.environ.get('PGUSER')
        password = password or os.environ.get('PGPASSWORD')
        host = host or os.environ.get('PGHOST')
        redshift_region = get_aws_region(host)
        port = port or os.environ.get('PGPORT') or 5439

        print('Connecting to Redshift...')
        self.conn = psycopg2.connect(database=database, user=user,
                                     password=password, host=host,
                                     port=port, sslmode='require')

        self.cur = self.conn.cursor()
        
        if not append:
            drop_query = """drop table if exists {0}""".format(table_name)
            self.cur.execute(drop_query)
            self.conn.commit()

            print("The following table will be created: ")
            create_query = create_table_sql(self.table, table_name, sortkey, distkey)
            self.cur.execute(create_query)
            self.conn.commit()
        
        raw_query_string = """COPY {0}
                   FROM '{1}'
                   CREDENTIALS 'aws_access_key_id={2};aws_secret_access_key={3}'
                   MANIFEST
                   GZIP
                   CSV"""
        
        if self.region != redshift_region:
            raw_query_string+='\n REGION \''+self.region+'\''
            
        raw_query_string+=';'
        
        query = raw_query_string.format(table_name, self.manifest_url,
                                  aws_access_key_id, aws_secret_access_key)
        
        
        print("COPYing data from {0} into table {1}...".format(self.manifest_url,
                                                             table_name))
        self.cur.execute(query)
        self.conn.commit()
        self.conn.close()


# In[118]:

#helper function to create the appropriate redshift ddl from a pandas dataframe
def create_table_sql(df, table_name, sortkey=None, distkey=None):
    type_dic = {
                pd.tslib.Timestamp: 'TIMESTAMP',
                datetime.date: 'DATE',
                }

    
    base = 'CREATE TABLE '+table_name+'\n('
    
    stack = []
    for c in df.dropna(1, how='all').columns:
        #find the most frequent type in column - should we only allow each column to only have one data type?
        s = df[c].map(type).value_counts().order(ascending=False).index[0]
        
        #for integers, assign integer type based on maximal value of column
        if s==np.int64:
            max_len = df[c].map(lambda x: np.ceil(np.log2(x))).max()
            if max_len>32:
                type_str = 'BIGINT'
            elif max_len<=4:
                type_str = 'SMALLINT'
            else:
                type_str = 'INTEGER'

        #for string types, create varchar with length twice the maximum length in current column for buffer
        elif s==unicode or s==str:
            max_len = df[c].map(len).max()
            type_str = 'VARCHAR({a})'.format(a=max_len*2)
        elif s==np.float64:
            max_len = df[c].map(lambda x: len(str(x).replace('.',''))).max()
            type_str = 'NUMERIC({a},4)'.format(a=max_len+2)
        else:
            max_len = 0
            type_str = type_dic[s]

        stack.append('    '+c+' '+type_str)
    
    base+=',\n'.join(stack)
    base+=')'
    if sortkey:
        base+='\n'
        base+='SORTKEY(\n    '
        base+=',\n    '.join(sortkey)
        base+='\n)'
    if distkey:
        base+='\n'
        base+='DISTKEY(\n    '
        base+=',\n    '.join(distkey)
        base+='\n)'
    base+=';'
    
    print(base)
    return base


# In[109]:

def df_to_redshift(df, table_name, host, db, columns=None, bucket_name=None, keypath=None, append=False):
    """
    EXAMPLE USAGE
    =============
    h = 'bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    df_to_redshift(cheat_df, 'processed.cheaters', h, 'ffs')
    =============
    Parameters
    ----------
    columns: list
        use this parameter if you want to change the column names written to Redshift
    df: Pandas dataframe
    host, db: Redshift host and database names
    """
    if columns:
        df.columns = columns
    
    pgs = PGShift(df)
    aws_access_key_id="AKIAJRKDNC52OAINDWKA"
    aws_secret_access_key="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"
    pgs.put_to_s3(aws_access_key_id=aws_access_key_id,
                  aws_secret_access_key=aws_secret_access_key)
    
    pgs.copy_to_redshift(table_name, aws_access_key_id,
                         aws_secret_access_key, database=db, user='biadmin',
                         password='Halfquest_2014', host=host, port=5439, append=append)
    
    pgs.clean_up_s3()





# coding: utf-8
'''
=======
Usage: python ffs_cheater_script.py 0.3 2 [--rerun]

- The first parameter is the cutoff threshold
- The second parameter is the number of days of DAU (before today) to look for new cheaters
- Use the rerun flag to indicate that the model needs to be rerun using updated cheater data
=======
'''
import pandas as pd
import argparse
from sqlalchemy import create_engine
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
from sklearn.cross_validation import KFold, StratifiedKFold
from sklearn.metrics import confusion_matrix
from datetime import date
from pandas_to_redshift import *

def parse_args():
    parser = argparse.ArgumentParser(description='ffs cheater list update')
    parser.add_argument("cutoff", help='probability cutoff to identify as cheater for logistic regression', type=float)
    parser.add_argument("dau", help='check cheaters from dau for the past x days', type=int)
    parser.add_argument("--rerun", help="rerun model",action="store_true")
    args = parser.parse_args()
    return args

def update_cheater_list(args):
    ffs_conn = "redshift+psycopg2://biadmin:Halfquest_2014@bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
    ffs_engine = create_engine(ffs_conn)

    #parse args from command line, limiting dau threshold to 5
    cutoff=args.cutoff
    #dau_threshold = min(args.dau, 5)
    dau_threshold = args.dau

    sql = '''
    select p.*, u.total_revenue_usd as rev, c.first_date from
    (select user_key, snsid, max(rc_in) max_in, sum(rc_in) sum_in, 
    1 cheater from
    (
    select a.*, b.date, b.rc_in, b.rc_out from
    (SELECT user_key, snsid FROM ffs.processed.cheaters_new) a
    join
    (SELECT date, user_key, sum(rc_in) rc_in,
    sum(rc_out) rc_out
    FROM ffs.processed.rc_transaction
    where action not in ('loadPayment', 'mypay', 'light_loading', 'finish_task', 'retrieve_data', 'secondFarm_loading', 'store_realMoney_buy', 'package_realMoney_buy', 'rcCow_collect')
    group by 1,2) b
    on a.user_key = b.user_key)
    group by 1,2) p
    join
    processed.dim_user u
    on p.user_key = u.user_key
    left join
    processed.cheaters_new c
    on p.user_key = c.user_key;'''

    cheater_df = pd.read_sql_query(sql, ffs_engine)

    if len(cheater_df)>0: 
        print 'Cheater list acquired.'
    else:
        #cheaters_new table is empty, read from original cheaters table
        sql = '''select p.*, u.total_revenue_usd as rev, c.first_date from
                    (select user_key, snsid, max(rc_in) max_in, sum(rc_in) sum_in, 
                    1 cheater from
                    (
                    select a.*, b.date, b.rc_in, b.rc_out from
                    (SELECT m.snsid, n.user_key FROM 
                        ffs.processed.cheaters m
                        join 
                        processed.dim_user n
                        on m.snsid=n.snsid
                        and n.app='ffs.global.prod') a
                    join
                    (SELECT date, user_key, sum(rc_in) rc_in,
                    sum(rc_out) rc_out
                    FROM ffs.processed.rc_transaction
                    where action not in ('loadPayment', 'mypay', 'light_loading', 'finish_task', 'retrieve_data', 'secondFarm_loading', 'store_realMoney_buy', 'package_realMoney_buy', 'rcCow_collect')
                    group by 1,2) b
                    on a.user_key = b.user_key)
                    group by 1,2) p
                    join
                    processed.dim_user u
                    on p.user_key = u.user_key
                    left join
                    processed.cheaters_new c
                    on p.user_key = c.user_key'''

        cheater_df = pd.read_sql_query(sql, ffs_engine)
        print "Got cheater list from original list."

    cheater_df = cheater_df[cheater_df['max_in']>10]

    if args.rerun:
        sql = '''
        select a.*, u.snsid, u.rev from
        (select user_key, max(rc_in) max_in, 
        --max(rc_out) max_out, 
        sum(rc_in) sum_in,
        --sum(rc_out) sum_out, 
        0 cheater from 
        (SELECT date, user_key, sum(rc_in) rc_in,
        sum(rc_out) rc_out
        FROM ffs.processed.rc_transaction
        where 
        action not in ('loadPayment', 'mypay', 'light_loading', 'finish_task', 'retrieve_data', 'secondFarm_loading', 'store_realMoney_buy', 'package_realMoney_buy', 'rcCow_collect')
        and
        user_key not in
        (select u.user_key from
        ffs.processed.cheaters a
        join
        processed.dim_user u 
        on a.snsid = u.snsid
        and u.app='ffs.global.prod')
        group by 1,2)
        group by 1
        order by md5('seed' || user_key)
        ) a
        join
        (select user_key, snsid, total_revenue_usd as rev from
        processed.dim_user 
        where app='ffs.global.prod') u
        on a.user_key = u.user_key
        limit 100000;'''

        non_cheater_df = pd.read_sql_query(sql, ffs_engine)
        
        train_df = pd.concat([cheater_df, non_cheater_df]).fillna(0)
        train_df.index = range(len(train_df))

        print 'Random non cheater list acquired.'

        print train_df['cheater'].value_counts()

        X = train_df[['max_in', 'sum_in']].values
        y = train_df['cheater'].values
        scaler = preprocessing.StandardScaler().fit(X)
        X_scaled = scaler.transform(X)

        clf = LogisticRegression()
        clf.fit(X_scaled, y)
        print clf.coef_

        m = pd.Series(clf.predict_proba(X_scaled)[:,1])
        pred = np.where(m>cutoff, 1, 0)
        print pd.DataFrame(confusion_matrix(y, pred)).to_string()

        #save model and scaler
        joblib.dump(scaler, 'scaler.pkl')
        joblib.dump(clf, 'ffs_cheaters_logit.pkl') 

    else:
        clf = joblib.load('ffs_cheaters_logit.pkl') 
        scaler = joblib.load('scaler.pkl')
        print "Current model parameters are:"
        print clf.coef_

    #take updated cheaters list and append probability of cheating based on model
    cheater_df.index = range(len(cheater_df))
    X = cheater_df[['max_in', 'sum_in']].values
    X_scaled = scaler.transform(X)
    m = pd.Series(clf.predict_proba(X_scaled)[:,1])
    m.name = 'prob'
    cheater_df_prob = pd.concat([cheater_df.drop('cheater', axis=1), m], axis=1)

    print "Getting rc data from DAU over the past {t} days...please wait".format(t=dau_threshold)

    sql = '''select a.*, u.snsid, u.rev from
    (select user_key, max(rc_in) max_in, 
    --max(rc_out) max_out, 
    sum(rc_in) sum_in
    --,sum(rc_out) sum_out 
    from 
    (SELECT date, user_key, sum(rc_in) rc_in,
    sum(rc_out) rc_out
    FROM ffs.processed.rc_transaction
    where 
    action not in ('loadPayment', 'mypay', 'light_loading', 'finish_task', 'retrieve_data', 'secondFarm_loading', 'store_realMoney_buy', 'package_realMoney_buy', 'rcCow_collect')
    and app='ffs.global.prod'
    and user_key in 
    (select user_key from processed.fact_dau_snapshot where date>=CURRENT_DATE-{t})
    group by 1,2)
    group by 1
    order by md5('seed' || user_key)
    ) a
    join
    (select user_key, snsid, total_revenue_usd as rev from
    processed.dim_user 
    where app='ffs.global.prod') u
    on a.user_key = u.user_key'''

    dau_sample = pd.read_sql_query(sql.format(t=dau_threshold), ffs_engine)

    X_t = dau_sample[['max_in', 'sum_in']].fillna(0).values
    X2 = scaler.transform(X_t)
    m = pd.Series(clf.predict_proba(X2)[:,1])
    m.name = 'prob'
    df = pd.concat([dau_sample, m], axis=1)
    df_sorted = df[~df['user_key'].isin(cheater_df['user_key'])].sort('prob', ascending=False)
    new_cheaters = df_sorted[(df_sorted['prob']>cutoff)]
    print "Found {n} new cheaters.".format(n=len(new_cheaters))

    h = 'bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    truncate_sql = 'truncate table processed.cheaters_new'
    ffs_engine.execute(truncate_sql)
    df_to_write = pd.concat([cheater_df_prob, new_cheaters])
    df_to_write = df_to_write[['user_key', 'max_in', 'sum_in', 'snsid', 'rev', 'prob', 'first_date']]
    df_to_write['max_in'] = df_to_write['max_in'].astype(int)
    df_to_write['sum_in'] = df_to_write['sum_in'].astype(int)
    df_to_write['rev'] = df_to_write['rev'].fillna(0.0)
    df_to_write['first_date'] = df_to_write['first_date'].fillna(date.today())

    #df_to_redshift(df_to_write.fillna(0), 'processed.cheaters_new', h, 'ffs')
    df_to_redshift(df_to_write.fillna(0), 'processed.cheaters_new', h, 'ffs', append=True)

def main():
    args = parse_args()
    update_cheater_list(args)

if __name__ == '__main__':
    main()

import pandas as pd
from sqlalchemy import create_engine, text
import sqlalchemy

ffs_conn = "redshift+psycopg2://zhisheng:rDY5GxfM@bicluster-ffs.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs"
ffs_engine = create_engine(ffs_conn)
dest_table = "pm.fact_dau_metrics"
n_days = 3

delete_sql = """delete from {0} where datediff(day, date, CURRENT_DATE)<={1};""".format(dest_table, str(n_days))
insert_sql = """insert into {0}
select * from ({1}) where datediff(day, date, CURRENT_DATE)<={2};"""

queries = []

#add your queries to be executed here
#the allowed columns for each query is [date, snsid, key, value]
sql = """select a.*, 'total_rev' as key, sum(usd) as value from
(select date, snsid from processed.fact_dau_snapshot) a
left join
(select date, snsid, usd from processed.fact_revenue) b
on a.snsid = b.snsid 
and a.date>=b.date
group by 1,2,3;"""

queries.append(sql)

#delete begins
with ffs_engine.begin() as conn:
	conn.execute(text(delete_sql).execution_options(autocommit=True))

print "Done deleting data for last {0} days".format(str(n_days))

#insert begins
for q in queries:
	with ffs_engine.begin() as conn:
		isql = insert_sql.format(dest_table, q.replace(';', ''), str(n_days))
		conn.execute(text(isql).execution_options(autocommit=True))
		print "Completed the following insert:"
		print isql
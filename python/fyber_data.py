import requests
import json
import datetime
import argparse


class FyberData:
    def __init__(self, args):
        self.date = datetime.datetime.strptime(args['date'], '%Y-%m-%d').date()
        self.days_back = int(args['days_back']) if args['days_back'] else 3
        self.hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
                    'Accept-Encoding': 'none',
                    'Accept-Language': 'en-US,en;q=0.8',
                    'Connection': 'keep-alive',
                    'Authorization': 'Basic ODY1MzU6OGQxNzYwYzgzMzQ0ZDIxMGMzNWFmM2FlZmYwOWYzOTU='}

    def run(self):
        url_parser_pairs = [('https://api.fyber.com/publishers/v2/reporting/ad-networks-kpis.json?since=%s&until=%s', FyberData.parse_ad_networks_data),
                            ('https://api.fyber.com/publishers/v2/reporting/publisher-kpis.json?since=%s&until=%s', FyberData.parse_fyber_publisher_data)
                            ]
        for url_pattern, parse_method in url_parser_pairs:
            json_obj = self.fetch_data(url_pattern)
            parse_method(json_obj)

    def fetch_data(self, url_pattern):
        url = url_pattern % ((self.date - datetime.timedelta(self.days_back)).strftime('%Y-%m-%d'),
                             self.date.strftime('%Y-%m-%d'))
        try:
            r = requests.get(url, headers=self.hdr)
            json_res = json.loads(r.text)
        except Exception, e:
            print 'Error in fetching data for %s' % url
            print e
            return -1

        return json_res

    @staticmethod
    def parse_ad_networks_data(json_obj):
        string_builder = 'date|application_id|application_name|ad_format|ad_network|country|fills|impressions|completions|revenue_usd|ecpm_usd\n'
        base_string = '%s|%d|%s|%s|%s|%s|%d|%d|%d|%.2f|%.2f\n'
        for item in json_obj['data']:
            try:
                tmp_string = base_string % (item['date'], item['application_id'], item['application_name'],
                                            item['ad_format'], item['ad_network'], item['country'],
                                            item['fills'] if item['fills'] else 0,
                                            item['impressions'] if item['impressions'] else 0,
                                            item['completions'] if item['completions'] else 0,
                                            item['revenue_usd'] if item['revenue_usd'] else 0.0,
                                            item['ecpm_usd'] if item['ecpm_usd'] else 0.0)
                string_builder += tmp_string
            except Exception, e:
                print e
                print item

        f = open('/tmp/fyber_ad_networks_data.csv', 'w')
        f.write(string_builder.encode('utf8'))
        f.close()

    @staticmethod
    def parse_fyber_publisher_data(json_obj):
        fyber_publisher_data_table_columns_in_order_with_format = [('date', '%s'),
                                                                   ('application_id', '%d'),
                                                                   ('application_name', '%s'),
                                                                   ('ad_format', '%s'),
                                                                   ('country', '%s'),
                                                                   ('requests', '%d'),
                                                                   ('fills', '%d'),
                                                                   ('impressions', '%d'),
                                                                   ('completions', '%d'),
                                                                   ('revenue_usd', '%.2f'),
                                                                   ('ecpm_usd', '%.2f')]

        column_names = [e[0] for e in fyber_publisher_data_table_columns_in_order_with_format]
        csv_content_header = '|'.join(column_names)
        rows = [csv_content_header]
        row_pattern = '|'.join([e[1] for e in fyber_publisher_data_table_columns_in_order_with_format])
        for item in json_obj['data']:
            try:
                rows.append(row_pattern % (item['date'], item['application_id'], item['application_name'],
                                           item['ad_format'], item['country'],
                                           item['requests'] if item['requests'] else 0,
                                           item['fills'] if item['fills'] else 0,
                                           item['impressions'] if item['impressions'] else 0,
                                           item['completions'] if item['completions'] else 0,
                                           item['revenue_usd'] if item['revenue_usd'] else 0.0,
                                           item['ecpm_usd'] if item['ecpm_usd'] else 0.0)
                            )
            except Exception, e:
                print e
                print item
        contents = '\n'.join(rows)
        with open('/tmp/fyber_publisher_data.csv', 'w') as f:
            f.write(contents.encode('utf8'))


def parse_args():
    parser = argparse.ArgumentParser(description='Download Fyber data to BI')
    parser.add_argument('-d', '--date', help='date to process, yyyy-mm-dd', required=True)
    parser.add_argument('-b', '--days_back', help='days for backfill, default is 3', required=False)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    fd = FyberData(args)
    fd.run()


if __name__ == '__main__':
    main()

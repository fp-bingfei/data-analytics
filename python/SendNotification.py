import time
import urllib2
import os

class SendNotification:
    def __init__(self):
        pass

    def run(self):
        self.send()


    def send(self):
        for file in os.listdir("/home/ec2-user/ffs_tmp"):
            if file.startswith("tango"):
                f = open("/home/ec2-user/ffs_tmp/" + file, "r")


        for line in f.readlines():
            url = "http://ffs-tango.funplusgame.com/" + "mobile.php?" + \
                "controller=mobileuser&action=sendSysMsgForBi&snsid="
            if len(line.split(',')) > 1:
                url = url + line.split(',')[0].strip() + "&bi_key=" + line.split(',')[1].strip().replace('test', 'bi_')
            else:
                url = url + line.strip() + "&bi_key=bi_1"
            response = urllib2.urlopen(url).read()
            if (response != '{"error":""}'):
                print line + ': ' + response
            else:
                print line + ': ' + 'Success!'

        print "Successfully send!"
        f.close()

def main():
    sn = SendNotification()
    sn.run()

if __name__ == '__main__':
    main()
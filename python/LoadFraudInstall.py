import psycopg2
import argparse
import yaml
import pprint
import os
from os import listdir
from os.path import isfile, join
import commands


class LoadFraudInstall:

    def __init__(self, args):
        self.args = args
        self.yaml = self.args["yaml"]
        self.app = self.args["app"]
        self.date = self.args["date"]

    def run(self):
        self.setDBParams(self.yaml)
        conn = self.getConnection()
        status = self.checkS3Folder(self.app, self.date)
        # Found the s3 folder
        if (status == 0):
            # Remove processed CSV files
            self.removeProcessedFiles(self.app, self.date)
            # Downlaod CSV files
            self.downloadCSVFiles(self.app, self.date)
            # Process CSV files
            self.processCSVFiles(self.app, self.date)
            # Upload processed CSV files
            self.uploadCSVFiles(self.app, self.date)
            # Copy processed CSV files to Redshift
            self.runScripts(conn, self.app, self.date, self.aws_access_key_id, self.aws_secret_access_key)

    def checkS3Folder(self, app, date):
        status,output = commands.getstatusoutput("aws s3 ls s3://com.funplus.public/fraud_install/%s/%s/" % (app,date))
        return status

    def removeProcessedFiles(self, app, date):
        status,output = commands.getstatusoutput("aws s3 rm s3://com.funplus.public/fraud_install/%s/%s/processed/ --recursive" % (app,date))
        return status

    def downloadCSVFiles(self, app, date):
        status,output = commands.getstatusoutput("mkdir -p /mnt/fraud_install/%s/%s/; aws s3 cp s3://com.funplus.public/fraud_install/%s/%s/ /mnt/fraud_install/%s/%s/ --recursive" % (app,date,app,date,app,date))
        return status

    def checkColumn(self, line1, line2):
        fields1 = line1.split()
        fields2 = line2.split()
        column = -1
        for i in range(0, len(fields1)):
            if (len(fields1[i]) == 32 and len(fields2[i]) == 32 and fields1[i] != fields2[i]):
                column = i
                break
        return column

    def processCSVFiles(self, app, date):
        # mkdir
        status,output = commands.getstatusoutput("mkdir -p /mnt/fraud_install/%s/%s/processed/" % (app,date))
        # Process CSV files
        path = "/mnt/fraud_install/%s/%s/" % (app, date)
        for f in listdir(path):
            if (isfile(f)):
                oldfile = open(join(path, f), "r")
                lines = oldfile.read().decode("utf-16").split('\n')
                if (len(lines) > 2):
                    line1 = lines[1]
                    line2 = lines[2]
                    # Find the column of ID
                    column = self.checkColumn(line1, line2)
                    if (column >= 0):
                        newfile = open(path + "processed/" + f, 'w')
                        # Print header
                        print >> newfile, "id,app,load_hour"
                        skipHeader = 0
                        for line in lines:
                            if (len(line) > 0 and skipHeader != 0):
                                fields = line.split()
                                row = fields[column] + "," + app + "," + date
                                # Print row
                                print >> newfile, row
                            else:
                                skipHeader = 1
                        newfile.close()
                oldfile.close()

    def uploadCSVFiles(self, app, date):
        status,output = commands.getstatusoutput("aws s3 cp /mnt/fraud_install/%s/%s/processed/ s3://com.funplus.public/fraud_install/%s/%s/processed/ --recursive" % (app,date,app,date))
        return status

    def runScripts(self, conn, app, date, aws_access_key_id, aws_secret_access_key):
        try:
            cursor = conn.cursor()
            command = "delete from processed.adjust_fraud_install where app='%s' and load_hour='%s';" % (app,date)
            cursor.execute(command)
            command = "copy processed.adjust_fraud_install from 's3://com.funplus.public/fraud_install/%s/%s/processed/' credentials 'aws_access_key_id=%s;aws_secret_access_key=%s' csv IGNOREHEADER 1;" % (app,date,aws_access_key_id,aws_secret_access_key)
            cursor.execute(command)
            conn.commit()
        except Exception, e:
            print 'In Database: ' + self.db_name
            print 'Excuting command: ' + command
            print 'Error: ',
            print e
            conn.rollback()

    def getConnection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def setDBParams(self, filename):
        filename = '/mnt/funplus/data-analytics/python/conf/' + filename
        f = open(filename, "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.fraud_email_list = confMap['tasks']['defaults']['fraud_email_list']
        self.fraud_command = confMap['tasks']['defaults']['fraud_command']
        self.fraud_csv_command = confMap['tasks']['defaults']['fraud_csv_command']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-y','--yaml',help='yaml file name',required=True)
    parser.add_argument('-a','--app',help='app short name',required=True)
    parser.add_argument('-d','--date',help='loading date',required=True)
    args = vars(parser.parse_args()) 
    return args

def main():
    args = parse_args()
    fi = LoadFraudInstall(args)
    fi.run()

if __name__ == '__main__':
    main()

__author__ = 'jiating'

import argparse
import psycopg2
import yaml
import datetime
import time


class RoyalStorySignInCalculator:
    def __init__(self, args):
        self.args = args

    def run(self):
        print "\nStart Time :  %s" % time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        self.set_db_params()
        conn = self.get_connection()
        # if self.args['pre_sql'] != None:
        #     self.execute_sql(conn, self.args['pre_sql'])
        self.calculate(conn)
        print "\nEnd Time :  %s" % time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    def calculate(self, conn):
        days_ago_30 = (datetime.datetime.strptime(self.args['date'], '%Y-%m-%d') - datetime.timedelta(29)).strftime(
            '%Y-%m-%d')

        user_list_sql = "select distinct user_key from %s where signin_date > '%s' " \
                        "and signin_date <= '%s'" % (self.args['table'], days_ago_30, self.args['date'])

        user_list = self.execute_sql(conn, user_list_sql)

        i = 0
        while i < len(user_list):
            index_num = len(user_list) if (len(user_list) - i) < 5000 else (i + 5000)

            insert_sql = "INSERT INTO custom.calendar_max_days VALUES "
            records = ""
            for j in range(i, index_num):
                user_key = user_list[j][0]
                max_days = self.get_max_sign_in_days(conn, days_ago_30, self.args['date'], user_key)
                records += "('%s', '%s', %s)," % (self.args['date'], user_key, max_days)
            # for user in user_list:
            records = records[:-1]
            insert_sql += records
            self.execute_update_sql(conn, insert_sql)
            i += 5000

    def get_max_sign_in_days(self, conn, start_date, end_date, user_key):
        date_diff_str = "datediff(day, '%s', signin_date)" % start_date
        query_sql = "SELECT %s FROM %s WHERE signin_date > '%s' AND signin_date <= '%s' " \
                    "AND user_key = '%s' ORDER BY %s ASC" % (date_diff_str, self.args['table'], start_date, end_date, user_key, date_diff_str)
        days_list = self.execute_sql(conn, query_sql)

        # Initial a sign in list
        sign_in_list = []
        for day in days_list:
            sign_in_list.append(day[0])

        temp_max_days = 0
        max_days = 0
        for i in range(0, len(sign_in_list)):
            if i == 0 or (sign_in_list[i] - sign_in_list[i - 1] == 1):
                temp_max_days += 1
                continue
            if temp_max_days > max_days:
                max_days = temp_max_days
            temp_max_days = 1
        if temp_max_days > max_days:
            max_days = temp_max_days
        print "user_key : " + user_key + ", max_days: " + str(max_days)
        return temp_max_days

    def execute_sql(self, conn, query_sql):
        try:
            cursor = conn.cursor()
            cursor.execute(query_sql)
            print ("\nsql executed:" + query_sql)
            conn.commit()
            results = cursor.fetchall()
            print ("\ncommit executed")
            return results
        except Exception, e:
            print 'Excuting command: ' + query_sql
            print 'Error: ',
            print e
            conn.rollback()
            return []

    def execute_update_sql(self, conn, query_sql):
        try:
            cursor = conn.cursor()
            cursor.execute(query_sql)
            print ("\nsql executed:" + query_sql)
            conn.commit()
            print ("\ncommit executed")
        except Exception, e:
            print 'Excuting command: ' + query_sql
            print 'Error: ',
            print e
            conn.rollback()

    def get_connection(self):
        conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (
            self.db_host, self.db_port, self.db_name, self.db_username, self.db_password)
        conn = psycopg2.connect(conn_string)
        return conn

    def set_db_params(self):
        f = open(self.args['conf'], "r")
        confMap = yaml.safe_load(f)
        f.close()
        self.db_host = confMap['tasks']['defaults']['db_host']
        self.db_port = confMap['tasks']['defaults']['db_port']
        self.db_name = confMap['tasks']['defaults']['db_name']
        self.db_username = confMap['tasks']['defaults']['db_username']
        self.db_password = confMap['tasks']['defaults']['db_password']
        self.aws_access_key_id = confMap['tasks']['defaults']['aws_access_key_id']
        self.aws_secret_access_key = confMap['tasks']['defaults']['aws_secret_access_key']


def parse_args():
    parser = argparse.ArgumentParser(description='Copy file into Redshift')
    parser.add_argument('-c', '--conf', help='Config file in YAML format', required=True)
    parser.add_argument('-s', '--schema', help='DB schema name for specific table', required=False)
    parser.add_argument('-t', '--table', help='DB table name', required=True)
    parser.add_argument('-d', '--date', help='The calculate date', required=True)
    args = vars(parser.parse_args())
    return args


def main():
    args = parse_args()
    calculator = RoyalStorySignInCalculator(args)
    calculator.run()


if __name__ == '__main__':
    main()

#################################################################################################################
#__author__ = 'BIDev'
# Required Python Modules:
#   1. Install PyYAML
#       (1) Install via pip
#           $ pip install pyyaml
#       (2) Install from source
#           (i) download from http://pyyaml.org/download/pyyaml/PyYAML-3.11.tar.gz
#           (ii) sudo python setup.py install (or python2.6 for older version)
#
#   2. Install boto (pip install boto) or
#       (1) Install via pip
#           $ pip install boto
#       (2) Install from source
#           $ git clone git://github.com/boto/boto.git
#           $ cd boto
#           $ sudo python setup.py install
#           or (sudo /System/Library/Frameworks/Python.framework/Versions/2.6/bin/python2.6 setup.py install)
#################################################################################################################

import getopt,sys
import yaml, pprint
from string import Template
import boto.datapipeline
import subprocess
class DataPipelineBuilder:
    apps = []
    TEMPLATE = 'template'
    templateFilename = None
    SCHEDULE_TYPE = 'schedule_type'
    scheduleType = None
    templateDir = ''
    outputDir = ''
    activation = ''
    credentials = ''


    def __init__(self, argv = None):
        if argv is None:
            argv = sys.argv

        try:
            self.opts, self.args = getopt.getopt(sys.argv[1:], "ht:c:a:T:O:x:i:", ["help", "task=","config=", "app=", "template_dir=", "output_dir=", "activate=", "credentials="])
        except getopt.GetoptError as err:
            print (str(err))
            self.usage()
            sys.exit(2)

        if len(self.opts) == 0:
            self.usage()
            sys.exit(2)

        for opt, arg in self.opts:
            if opt in ("-t", "--task"):
                self.task = arg
            elif opt in ("-c", "--config"):
                self.config = arg
            elif opt in ("-a", "--app"):
                self.app = arg
            elif opt in ("-T", "--template_dir"):
                self.templateDir = arg
            elif opt in ("-O", "--output_dir"):
                self.outputDir = arg
            elif opt in ("-x", "--activate"):
                if arg == 'True':
                    self.activation = '--activate'
            elif opt in ("-i", "--credentials"):
                self.credentials = "--credentials " + arg
            elif opt in ("-h", "--help"):
                self.usage()
                sys.exit(0)
            else:
                assert False, "unhandled option"
                self.usage()
                print ("unhandled option")

        if self.task is None or self.config is None or self.config is None:
            self.usage()
            sys.exit(2)

    def usage(self):
        print "Usage:\n" + (sys.argv[0]) + "--task=sendgrid --config=farm.yaml --activate=True --app=ALL || --app=farm.th.prod"


    def generate(self):
        yamlDataMap = self.parseYaml()

        substituteDict = self.getSubstituteDict(yamlDataMap)

        f_templ = open(self.templateFile, "r")
        template = Template(f_templ.read())
        f_templ.close()

        for app in self.apps:
            substituteDict['app'] =  app
            substituteDict['email_subject'] = self.task + ' ' + substituteDict.get('env') + ' ' + app
            pipelineName = self.task + '-' + app + '-' + self.scheduleType
            templateJsonOutput = pipelineName + '.json'

            output = open (templateJsonOutput, "w")
            output.write(template.substitute(substituteDict))
            output.close()

            self.createPipeline(pipelineName.upper(), templateJsonOutput)


    def createPipeline(self, pipelineName, templateJsonOutput):
        #cmd = "/Users/patrickliu/datapipeline-cli/datapipeline --credentials /Users/patrickliu/credentials.json --create '%s' --put %s --force %s" %(pipelineName, templateJsonOutput, self.activation)
        cmd = "~/datapipeline-cli/datapipeline %s --create '%s' --put %s --force %s" %(self.credentials, pipelineName, templateJsonOutput, self.activation)

        return_code = subprocess.call(cmd, shell=True)
        if return_code != 0:
            print("Pipeline creation (" + cmd + ") failed with return code " + str(return_code))


    def parseYaml(self):
        f = open(self.config, "r")
        dataMap = yaml.safe_load(f)
        f.close()
        return dataMap


    def getSubstituteDict(self, yamlDataMap):
        subDict = dict()

        if self.app == 'ALL':
            ''' in yaml file, apps in array format [farm.br.prod,farm.de.prod] '''
            if yamlDataMap["apps"] != None:
                for key in yamlDataMap["apps"]:
                    self.apps.append(key)
        else:
            self.apps.append(self.app)

        if yamlDataMap["tasks"]["defaults"] != None:
            for key in yamlDataMap["tasks"]["defaults"]:
                if yamlDataMap["tasks"]["defaults"][key] is None or isinstance(yamlDataMap["tasks"]["defaults"][key],dict):
                    continue
                subDict[key] = yamlDataMap["tasks"]["defaults"][key]


        if yamlDataMap["tasks"][self.task] != None:
            for key in yamlDataMap["tasks"][self.task]:
                if yamlDataMap["tasks"][self.task][key] is None or isinstance(yamlDataMap["tasks"][self.task][key],dict):
                    continue
                if key == self.TEMPLATE:
                    self.templateFile = yamlDataMap["tasks"][self.task][key]
                elif key == self.SCHEDULE_TYPE:
                    self.scheduleType = yamlDataMap["tasks"][self.task][key]
                else:
                    subDict[key] = yamlDataMap["tasks"][self.task][key]


        return subDict

def main():
    dpb = DataPipelineBuilder(sys.argv)
    dpb.generate()

if __name__ == "__main__":
    sys.exit(main())




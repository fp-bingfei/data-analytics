import MySQLdb
import datetime
import urllib2
import requests
import json
import logging

from logging.handlers import TimedRotatingFileHandler
from flask import Flask
from flask import abort, request, redirect, make_response

app = Flask(__name__)

#default testing parameters, should be overridden in config file
app.config['CLIENT_ID'] = '359069064671-seavphgsm79rqeavqukgsnm335rvns71.apps.googleusercontent.com'
app.config['CLIENT_SECRET'] = 'oSy9wONZK72aZbveM1fyJDmD'
app.config['SCOPE'] = 'https://www.googleapis.com/auth/yt-analytics.readonly'
app.config['YOUTUBE_DB_HOST'] = 'localhost'
app.config['YOUTUBE_DB_USER'] = 'root'
app.config['YOUTUBE_DB_PW'] = ''
app.config['YOUTUBE_DB_NAME'] = 'test'
app.config['YOUTUBE_LOG_FILE'] = '/tmp/youtube.log'
app.config['DEBUG'] = True
app.config['ACCESS_TOKEN'] = 'http://localhost:5000/access_token'
app.config['REFRESH_TOKEN'] = 'http://localhost:5000/refresh_token'

app.config.from_envvar('YOUTUBE_SERVER_SETTINGS', silent=True)

#global varibales
_url = 'https://accounts.google.com/o/oauth2/auth?client_id=%s&redirect_uri=%s&scope=%s&response_type=code&access_type=offline'

#setup logging
logger = logging.getLogger()
fmt = '%(asctime)s %(levelname)s %(pathname)s %(module)s.%(funcName)s Line:%(lineno)d %(message)s'
hdlr = TimedRotatingFileHandler(app.config['YOUTUBE_LOG_FILE'], 'midnight', 1, 100)
hdlr.setFormatter(logging.Formatter(fmt))
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)
app.logger.warning(app.config)


@app.route('/')
def index():
    #get user name and check if existed
    try:
        name = request.args.get('name', '')
        if name == '':
            return 'Please input your name in the URL: http://youtube.funplus.net/?name={your_name}'
        db = get_db_connection()
        cur = db.cursor()
        sql = '''SELECT * FROM youtube WHERE name = %s and is_active = %s;'''
        if cur.execute(sql, (name, 'YES')):
            app.logger.warning(sql % (name, 'YES'))
            return 'Name already exist, choose another name'
        else:
            insert_sql = '''INSERT INTO youtube (name, is_active, last_update_time) VALUES (%s, %s, %s)
                    ON DUPLICATE KEY UPDATE last_update_time = %s;'''
            app.logger.warning(insert_sql % (name, 'NO', datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 
                                            datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            cur.execute(insert_sql, (name, 'NO', datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        db.commit()
        db.close()
    except Exception, e:
        app.logger.warning(str(e))
        return "Exception happens, please check your log"

    url = _url % (app.config['CLIENT_ID'], app.config['ACCESS_TOKEN'], app.config['SCOPE'])
    resp = make_response(redirect(url, code = 302))
    resp.set_cookie('name', name)

    return resp


@app.route('/access_token')
def get_access_token():
    if request.method == 'GET':
        access_token = request.args.get('code', '')
        if request.cookies.has_key('name'):
            name = request.cookies.get('name')
        else:
            return "No username provided! Please don't clean your cookies~"
    #The token we get the first time with the authorization using GET is called "code"
        try:
            if ((access_token != '') and (access_token.startswith('4/'))):
                db = get_db_connection()
                cur = db.cursor()
        #We use the code to exchange for a refresh_token, and a short lived access_token, and save them to the database
                payload = {}
                payload['client_id'] = app.config['CLIENT_ID']
                payload['client_secret'] = app.config['CLIENT_SECRET']
                payload['code'] = access_token
                payload['redirect_uri'] = app.config['ACCESS_TOKEN']
                payload['grant_type'] = 'authorization_code'
                r = requests.post('https://accounts.google.com/o/oauth2/token', data = payload)
                r_json = json.loads(r.text)
                app.logger.warning(r.text)

                if r_json.has_key("refresh_token"):
                    refresh_token = r_json['refresh_token']
                    access_token = r_json['access_token']
                    sql_update_token = '''UPDATE youtube SET access_token = %s, refresh_token = %s, is_active = %s, last_update_time = %s WHERE name = %s'''
                    cur.execute(sql_update_token, (access_token, refresh_token, 'YES', datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), name))
                else:
                    sql_update_token = '''UPDATE youtube SET access_token = %s, last_update_time = %s WHERE name = %s'''
                    cur.execute(sql_update_token, (access_token, datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), name))
                    app.logger.warning("Didn't get refresh_token for user: " + name)

                db.commit()
                db.close()
                return "Successfully add user: " + name
            else:
                return "Access denied!"
        except Exception, e:
            app.logger.warning(str(e))

    else:
        return "Request Method is not GET"

def get_db_connection():
    db_connection = MySQLdb.connect(host = app.config['YOUTUBE_DB_HOST'],
                        user = app.config['YOUTUBE_DB_USER'],
                        passwd = app.config['YOUTUBE_DB_PW'],
                        db = app.config['YOUTUBE_DB_NAME'],
                        use_unicode=True, charset="utf8")
    
    return db_connection

# @app.route('/refresh_token')
# def get_refresh_token():
#     print request.args
#     return 'OK'

# @app.route('/create_job')
# #https://developers.google.com/youtube/reporting/v1/reference/rest/v1/jobs/create
# def create_job():
#     pass

# @app.route('/list_job')
# #https://developers.google.com/youtube/reporting/v1/reference/rest/v1/jobs/list
# def list_job():
#     access_token = get_update_access_token()
#     url_list_jobs = 'https://youtubereporting.googleapis.com/v1/jobs?'
#     headers = {'Authorization': 'Bearer ' + access_token}
#     r = requests.get(url_list_jobs, headers = headers)
#     r_json = json.loads(r.text)
    #res is something like this
    # {
    #"jobs": [
    #         {
    #             "id": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
    #             "reportTypeId": "channel_basic_a1",
    #             "name": "test",
    #             "createTime": "2016-05-19T15:46:55.000Z"
    #         }
    #      ]
    # }

# @app.route('/get_job')
# #https://developers.google.com/youtube/reporting/v1/reference/rest/v1/jobs/get
# def get_job():
#     job_id = '06b896df-c8d8-4ff5-b1a0-8137d084894f' # as an example
#     access_token = get_update_access_token()
#     url_get_job = 'https://youtubereporting.googleapis.com/v1/jobs/%s' % job_id
#     headers = {'Authorization': 'Bearer ' + access_token}
#     r = requests.get(url_get_job, headers = headers)
#     r_json = json.loads(r.text)
    #res is something like this
    # {
    #     "id": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
    #     "reportTypeId": "channel_basic_a1",
    #     "name": "test",
    #     "createTime": "2016-05-19T15:46:55.000Z"
    # }

# @app.route('/delete_job')
# #https://developers.google.com/youtube/reporting/v1/reference/rest/v1/jobs/delete
# def delete_job():
#     job_id = '06b896df-c8d8-4ff5-b1a0-8137d084894f' # as an example
#     access_token = get_update_access_token()
#     url_delete_job = 'https://youtubereporting.googleapis.com/v1/jobs/%s' % job_id
#     headers = {'Authorization': 'Bearer ' + access_token}
#     r = requests.delete(url_delete_job, headers = headers)
#     r_json = json.loads(r.text)
    #empty if succeed, error if failed
    # {
    #     "id": "307131370",
    #     "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
    #     "startTime": "2016-03-09T08:00:00.000Z",
    #     "endTime": "2016-03-10T08:00:00.000Z",
    #     "createTime": "2016-06-13T05:34:47.449186Z",
    #     "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/307131370?alt=media"
    # }

# @app.route('/get_report')
# #https://developers.google.com/youtube/reporting/v1/reference/rest/v1/jobs.reports/get
# def get_report():
#     job_id = '06b896df-c8d8-4ff5-b1a0-8137d084894f' # as an example
#     report_id = '307131370'
#     access_token = get_update_access_token()
#     url_get_report = 'https://youtubereporting.googleapis.com/v1/jobs/%s/reports/%s' % (job_id, report_id)
#     headers = {'Authorization': 'Bearer ' + access_token}
#     r = requests.delete(url_delete_job, headers = headers)
#     r_json = json.loads(r.text)

    
# @app.route('/list_report')
# #https://developers.google.com/youtube/reporting/v1/reference/rest/v1/jobs.reports/list
# def list_report():
#     job_id = '06b896df-c8d8-4ff5-b1a0-8137d084894f' # as an example
#     access_token = get_update_access_token()
#     url_list_reports = 'https://youtubereporting.googleapis.com/v1/jobs/%s/reports'
#     headers = {'Authorization': 'Bearer ' + access_token}
#     r = requests.get(url_list_reports, headers = headers)
#     r_json = json.loads(r.text)
    # {
    # "reports": [
    #         {
    #             "id": "307131370",
    #             "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
    #             "startTime": "2016-03-09T08:00:00.000Z",
    #             "endTime": "2016-03-10T08:00:00.000Z",
    #             "createTime": "2016-06-13T05:34:47.449186Z",
    #             "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/307131370?alt=media"
    #         },
    #         {
    #            "id": "307092473",
    #            "jobId": "06b896df-c8d8-4ff5-b1a0-8137d084894f",
    #            "startTime": "2016-06-11T07:00:00.000Z",
    #            "endTime": "2016-06-12T07:00:00.000Z",
    #            "createTime": "2016-06-13T03:01:13.940486Z",
    #            "downloadUrl": "https://youtubereporting.googleapis.com/v1/media/CHANNEL/h6332DtamSB9_PSY3VexOw/jobs/06b896df-c8d8-4ff5-b1a0-8137d084894f/reports/307092473?alt=media"
    #         }
    #     ],
    # "nextPageToken": "66201315"
    # }





# def check_name_exists():
#     pass
# def get_update_access_token():
#     #Before calling an API, we need to validate the token, and if it's not valid
#     #We fetch a refresh token
#     url_validate_token = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
#     sql_validate_token = 'SELECT * FROM youtube WHERE name = %s'

#     db = get_db_connection()
#     cur = db.cursor()
#     cur.execute(sql_validate_token, (user_name,))
#     _, access_token, refresh_token = cur.fetchone()

#     r = requests.get(url_validate_token % access_token)
#     r_json = json.loads(r.text)
#     if r_json.has_key('error'):
#         payload = {}
#         payload['client_id'] = app.config['CLIENT_ID']
#         payload['client_secret'] = app.config['CLIENT_SECRET']
#         payload['grant_type'] = 'refresh_token'
#         payload['refresh_token'] = refresh_token

#         r = requests.post('https://accounts.google.com/o/oauth2/token', data = payload)
#         r_json = json.loads(r.text)

#         if r_json.has_key('access_token'):
#             access_token = r_json['access_token']
#             sql_update_token = '''UPDATE youtube SET access_token = %s WHERE name = %s'''
#             cur.execute(sql_update_token, (access_token, user_name))
#             db.commit()
#             db.close()

#             return access_token
#         else:
#             return 'Something is wrong'
#     else:
#         db.commit()
#         db.close()
#         return access_token

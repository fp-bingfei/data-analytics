import time
import urllib2
import os

class SendNotification:
    def __init__(self):
        pass

    def run(self):
        self.send()


    def send(self):
        retrylist = []
        retries = 5
        url_builder = "http://ffs-global.funplusgame.com/" + "mobile.php?" + \
            "controller=mobileuser&action=sendSysMsgForBi&snsid="

        for file in os.listdir("/home/ec2-user/ffs_global_tmp"):
            if file.startswith("global"):
                f = open("/home/ec2-user/ffs_global_tmp/" + file, "r")

        for line in f.readlines():
            if len(line.split(',')) > 1:
                url = url_builder + line.split(',')[0].strip() + "&bi_key=" + line.split(',')[1].strip().replace('test', 'bi_')
            else:
                url = url_builder + line.strip() + "&bi_key=bi_1"

            try:
                response = urllib2.urlopen(url).read()
                if (response != '{"error":""}'):
                    print line + ': ' + response
                    print 'The request is: ' + url
                else:
                    pass
                    #print line + ': ' + 'Success!'
            except Exception, e:
                time.sleep(2)
                print e
                print 'ERROR 1:' + line
                print 'The request is: ' + url
                retrylist.append(line)

        while ((len(retrylist) > 0) and retries > 0):
            if len(line.split(',')) > 1:
                url = url_builder + line.split(',')[0].strip() + "&bi_key=" + line.split(',')[1].strip().replace('test', 'bi_')
            else:
                url = url_builder + line.strip() + "&bi_key=bi_1"

            try:
                response = urllib2.urlopen(url).read()
                if (response != '{"error":""}'):
                    print line + ': ' + response
                else:
                    pass
                retrylist.remove(line)
                    #print line + ': ' + 'Success!'
            except Exception, e:
                time.sleep(2)
                print e
                print 'ERROR 2:' + line
                print 'The request is: ' + url
                retries = retries - 1

        print "Successfully send!"
        f.close()

def main():
    sn = SendNotification()
    sn.run()

if __name__ == '__main__':
    main()
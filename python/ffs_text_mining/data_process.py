from __future__ import division
from sentiment_analysis import SentimentIntensityAnalyzer
import decimal
import pandas as pd
import urllib
import os

# Process sentiment analysis

class SentimentAnalyze:
    def __init__(self):
        self.sid = SentimentIntensityAnalyzer()
        self.path = '/tmp/facebook_sentiment/'
    def run(self):
        data_merge = None
        for filename in os.listdir(self.path):
            if filename == '.DS_Store' or filename == '497477766976645_facebook_statuses.csv':
                pass
            else:
                print filename
                data = self.score_process(filename)
                print data.head()
                if data_merge is None:
                    data_merge = data
                else:
                    data_merge = pd.concat([data_merge, data])
        data_merge.to_csv('/tmp/all_group_sentiment.csv',index=False)

    def score_process(self,filename):
        data = pd.read_csv(self.path+filename)
        data['group_id'] = data.apply(self.group_id,axis=1)
        data['sentiment'] = data.apply(self.sentiment_analysis,axis=1)
        data['sentiment_compound'] = data.apply(lambda row: 0 if row['sentiment'] == 'None' else row['sentiment']['compound'],axis=1)
        data['sentiment_neg'] = data.apply(lambda row: 0 if row['sentiment'] == 'None' else row['sentiment']['neg'],axis=1)
        data['sentiment_neu'] = data.apply(lambda row: 0 if row['sentiment'] == 'None' else row['sentiment']['neu'],axis=1)
        data['sentiment_pos'] = data.apply(lambda row: 0 if row['sentiment'] == 'None' else row['sentiment']['pos'],axis=1)
        del data['sentiment']
        return data
    def sentiment_analysis(self,row):
        if type(row['status_message']) is float:
            sentiment = 'None'
        else:
            sentiment = self.sid.polarity_scores(row['status_message'])
        return sentiment
    def group_id(self,row):
        return row['status_id'].split("_")[0]

def main():
    sa = SentimentAnalyze()
    sa.run()

if __name__ == '__main__':
    main()

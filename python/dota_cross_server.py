__author__ = 'jhyu'
# -*- coding: utf-8 -*-

import psycopg2 as pg
import pandas as pd
import pandas.io.sql as sql
import MySQLdb
import os
import threading
from time import sleep, ctime
import time


class entry_mysql(object):
#Get Mysql db entry
    host_format = 'dgame-oss-db-{0}.cpcrcjdtsyi8.ap-southeast-1.rds.amazonaws.com'
    host = ''
    port = 3306
    user = "oss_page"
    passwd = "Lilith_501"

class entry_redshift(object):
#Get Redshift db entry
    db_host = 'bicluster-distribution.cpaytjecvzyu.us-west-2.redshift.amazonaws.com'
    db_port = '5439'
    db_username = 'biadmin'
    db_password = 'Halfquest_2014'
    db_name = 'daota'
    conn_string = "host=%s port=%s dbname=%s user=%s password=%s" % (db_host, db_port, db_name, db_username, db_password)

class SqlProcess:
    def __init__(self):
        self.redshift_conn = entry_redshift.conn_string
        self.sql_path = '/Users/funplus/Documents/Tableau_files/dota/sql/'
        server_list = ['1200' + str(i) for i in range(1, 10)]
        server_list.extend(str(i) for i in range(12010, 12079))
        self.server_list = server_list
        self.export_path = '/Users/funplus/Documents/Tableau_files/dota/export/'

    def run(self):
    # Create Multiple Threads to Run Multiple Queries
        sql_list = []
        for (dirpath, dirnames, filenames) in os.walk(self.sql_path):
            for filename in filenames:
                if filename == '.DS_Store' or filename == 'churn_payer.sql' or filename == 'real_arena.sql':
                    pass
                else:
                    sql_list.append(filename)
        threads = []
        for key in sql_list:
            t = threading.Thread(target=self.RunQuery, args=(key,))
            threads.append(t)
        for i in range(len(sql_list)):
            threads[i].start()
        for i in range(len(sql_list)):
            threads[i].join()
        print 'end:%s' %ctime()

    def GetUserInfo(self):
    # Get Churn payer user information data
        fd = open(self.sql_path + 'churn_payer.sql', 'r')
        sqlFile = fd.read()
        fd.close()
        red_conn = pg.connect(self.redshift_conn)
        churn_payer = sql.read_sql(sqlFile, red_conn)
        return churn_payer

    def GetUserIdList(self):
    # Get Churn payer user id and export to dictionary
        churn_payer = self.GetUserInfo()
        userdict = {}
        for x in range(len(churn_payer)):
            server_id = churn_payer.iloc[x, 1]
            player_id = churn_payer.iloc[x, 2]
            userdict.setdefault(server_id, [])
            userdict[server_id].append(player_id)
        return userdict

    def GetQuery(self, file_name):
    # Get sql files
        fd = open(self.sql_path + file_name, 'r')
        sql_file = fd.read()
        fd.close()
        return sql_file

    def FormatQuery(self, file_name, server_id, interval=7):
        sql_file = self.GetQuery(file_name)
        userdict = self.GetUserIdList()
        user_id = ','.join(a for a in map(str, userdict[server_id]))
        query = sql_file.format(server_id, user_id, interval)
        return query

    def GetEntryList(self):
        entry_list = []
        for i in xrange(0, len(self.server_list)):
            entry_item = entry_mysql()
            entry_item.host_id = self.server_list[i]
            entry_item.host = str.format(entry_item.host_format, self.server_list[i])
            entry_list.append(entry_item)
        return entry_list

    def RunQuery(self, file_name, interval=7):
        entry_list = self.GetEntryList()
        data_merge = None
        print "{0} begins at {1}" .format(file_name,ctime())
        start = time.clock()
        for i in range(len(entry_list)):
            item = entry_list[i]
            #print item.host
            conn = MySQLdb.connect(host=item.host, port=item.port, user=item.user, passwd=item.passwd)
            query = self.FormatQuery(file_name, self.server_list[i], interval)
            data_record = sql.read_sql(query, conn)
            #print self.server_list[i], 'len: ', len(data_record)
            if data_merge is None:
                data_merge = data_record
            else:
                data_merge = pd.concat([data_merge, data_record])
            conn.close()
        end = time.clock()
        print "The time for prepare_data was {}".format(end - start)
        return data_merge

def main():
    sp=SqlProcess()
    sp.run()


if __name__ == "__main__":
    main()
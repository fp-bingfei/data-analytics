#!/bin/bash

# This script scans all the directories, sub-directories for the given basePath
# f.g.   s3://bucketname/basePath/xxx/yyy/filter (two variables)
# and limits the path needed using filter variable.
# Filtered paths are then deleted
# add by jiating

bucket=$1
basePaths=$2
filter=$3

if [[ $# -ne 3 ]];
then
        echo "ERROR: Script excepts 3 arguments - bucket, basePaths and filter";
        exit 1;
fi

echo "EventsParser cleanup job started for bucket($bucket), basePaths($basePaths) and filter($filter)"

# todo why
basePathStr=$(echo $basePaths | tr "," "\n")

# get s3://bucketname/basePath/xxx
for path in `hadoop fs -ls -h  $bucket$basePathStr | awk -v OFS='\t' '{print $6}'`;
do
        # get s3://bucketname/basePath/xxx/yyy/ 
        for subPath in `hadoop fs -ls -h  $path | awk -v OFS='\t' '{print $6}'`;
        do
                if hadoop fs -test -d $subPath/$filter; then
                        echo "Deleting Path --> "$subPath$filter
                        ### Uncomment when in actual use ###
                        hadoop fs -rmr $subPath$filter
                fi
        done
        
done

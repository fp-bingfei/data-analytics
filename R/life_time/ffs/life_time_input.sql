delete from model.life_time_input;

-- Prepare data
insert into model.life_time_input

select
d1.install_date 
,c1.snsid
,d1.install_source
,d1.country
,d1.os
,trunc(d1.conversion_ts) as conversion_date
,last_login_date
,case when trunc(d1.conversion_ts)-last_login_date>=0 then trunc(d1.conversion_ts) - install_date else last_login_date - install_date end as lt_days
,trunc(d1.conversion_ts) - install_date as days_to_conversion
,active_days_to_conversion
,avg_session_cnt_to_conversion
,active_days_after_conversion
,avg_sessions_cnt_after_conversion
from
(
	select
	a1.snsid
	,max(b1.date) as last_login_date
	,count(distinct b2.date) as active_days_to_conversion
	,sum(b2.session_cnt)/(case when count(b2.date)=0 then 1 else count(b2.date) end) as avg_session_cnt_to_conversion
    ,count(distinct b3.date) as active_days_after_conversion
    ,sum(case when b3.session_cnt is null then 0 else b3.session_cnt end)/ (case when count(b3.date)=0 then 1 else count(b3.date) end) as avg_sessions_cnt_after_conversion
	from
	----random sampling users is not necessary as only 10% of total users are payers
		
				(
				select 
				*
				from processed.dim_user a
				where 
				(
				   trunc(conversion_ts)=DATEADD(DAY,-14,'_YESTERDAY_') --- predict life time for payers converted 14 days ago
				or 
					(install_date<=DATEADD(DAY,-360-14,'_YESTERDAY_') and install_date>DATEADD(DAY,-360-180-14,'_YESTERDAY_') 
						and trunc(conversion_ts)<DATEADD(DAY,-14,'_YESTERDAY_')-- historical data: installed 360 days ago and converted earlier than test date
					)
				)
				and is_payer=1
				) a1
	
	left join ---lt days calculation
		(
			select * from processed.fact_dau_snapshot 
		) b1 on 
		     datediff('day',a1.install_date, b1.date)<=365 ---within 1 year
		     and datediff('day',a1.install_date, b1.date)>=0
		     and a1.user_key = b1.user_key
	left join ---lt days calculation
		(
			select * from processed.fact_dau_snapshot 
		) b2 on 
		     datediff('day',b2.date,trunc(a1.conversion_ts))>=0 ---days before conversion		     
		     and datediff('day',a1.install_date,b2.date)>=0
		     and a1.user_key = b2.user_key
    left join
        
        (
        		select * from processed.fact_dau_snapshot 
        ) b3 on
        	datediff('day',trunc(a1.conversion_ts),b3.date)>0
        	and datediff('day',trunc(a1.conversion_ts),b3.date)<=14 --- 14 days after conversion		     
		    and a1.user_key = b3.user_key
	group by 
	a1.snsid
) c1
left join 
               (
				select 
				*
				from processed.dim_user a
				where 
				(
				   trunc(conversion_ts)=DATEADD(DAY,-14,'_YESTERDAY_') --- predict life time for payers converted 14 days ago
				or 
					(install_date<=DATEADD(DAY,-360-14,'_YESTERDAY_') and install_date>DATEADD(DAY,-360-180-14,'_YESTERDAY_') 
						and trunc(conversion_ts)<DATEADD(DAY,-14,'_YESTERDAY_')
					)
				)
				and is_payer=1
				) d1 on c1.snsid=d1.snsid

where trunc(d1.conversion_ts) - d1.install_date<=365
;
commit;
# Get parameter
argv <- commandArgs(TRUE)
# Check length
if(length(argv) < 1)
  q()
# Get the date of yesterday
yesterday <- as.Date(argv[1])

# Load SparkR library
library(SparkR)

# Init Spark context and SQL context
sc <- sparkR.init()
sqlContext <- sparkRSQL.init(sc)

# Get data from redshift
raw_data<-loadDF(sqlContext, source="jdbc", driver="com.amazon.redshift.jdbc41.Driver", url="jdbc:redshift://bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/ffs?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true", dbtable="model.life_time_input")

###input
input_data<-collect(raw_data)
library(randomForest)

input_data$conversion_date<-as.Date(input_data$conversion_date)
###test date
test_date<-as.Date(yesterday)-14
test<-base::subset(input_data,input_data$conversion_date==test_date) #should be conversion date in production
country<-levels(as.factor(test$country))
ni<-length(country)
train<-base::subset(input_data,input_data$conversion_date<test_date) # past 180 day historical data
train<-base::subset(train,is.na(train$avg_session_cnt_to_conversion)==FALSE&is.na(train$avg_sessions_cnt_after_conversion)==FALSE)
###model
output<-cbind(input_data[1,],predict=0)
for(i in 1:ni){
  
  if(nrow(train)>0)
  {
    train1<-cbind(train,r1=train$country==country[i])
    train1<-base::subset(train1,train1$r1==TRUE,select=-r1)
  }
 
  if(nrow(test)>0)
  {
  test1<-cbind(test,r1=test$country==country[i])
  test1<-base::subset(test1,test1$r1==TRUE,select=-r1)
  
  if(nrow(test1)>0){ #check if we have test data
    out<-cbind(test1,predict=365) #default LT=365
    
    ########################days to conversion >=300
    test30<-base::subset(out,out$days_to_conversion>=300)
    if(nrow(test30)>0)
    {test30$predict<-ifelse(test30$predict>360,test30$predict,360)}
    
    if(nrow(train1)>0){
      ########################days to conversion between 240 and 300
      train31<-base::subset(train1,train1$days_to_conversion>=240&train1$days_to_conversion<300)
      test31<-base::subset(out,out$days_to_conversion>=240&out$days_to_conversion<300)
      if(nrow(test31)>0){
        if(nrow(train31)>=5){
          rf<-randomForest(lt_days~active_days_to_conversion+days_to_conversion+avg_session_cnt_to_conversion+avg_sessions_cnt_after_conversion+active_days_after_conversion,train31)
          test31$predict<-ifelse(predict(rf,test31)>test31$predict,predict(rf,test31),test31$predict)
        }
      }
      ########################days to conversion between 180 and 240
      train32<-base::subset(train1,train1$days_to_conversion>=180&train1$days_to_conversion<240)
      test32<-base::subset(out,out$days_to_conversion>=180&out$days_to_conversion<240)
      if(nrow(test32)>0){
        if(nrow(train32)>=5){
          rf<-randomForest(lt_days~active_days_to_conversion+days_to_conversion+avg_session_cnt_to_conversion+avg_sessions_cnt_after_conversion+active_days_after_conversion,train32)
          test32$predict<-ifelse(predict(rf,test32)>test32$predict,predict(rf,test32),test32$predict)
        }
      }  
      ########################days to conversion btw 120 and 180
      train33<-base::subset(train1,train1$days_to_conversion>=120&train1$days_to_conversion<180)
      
      test33<-base::subset(out,out$days_to_conversion>=120&out$days_to_conversion<180)
      if(nrow(test33)>0){
        if(nrow(train33)>=5){
          rf<-randomForest(lt_days~active_days_to_conversion+days_to_conversion+avg_session_cnt_to_conversion+avg_sessions_cnt_after_conversion+active_days_after_conversion,train33)
          test33$predict<-ifelse(predict(rf,test33)>test33$predict,predict(rf,test33),test33$predict)
        }
      }  
      ########################days to conversion btw 60 and 120
      train34<-base::subset(train1,train1$days_to_conversion>=60&train1$days_to_conversion<120)
      
      test34<-base::subset(out,out$days_to_conversion>=60&out$days_to_conversion<120)
      if(nrow(test34)>0){
        if(nrow(train34)>=5){
          rf<-randomForest(lt_days~active_days_to_conversion+days_to_conversion+avg_session_cnt_to_conversion+avg_sessions_cnt_after_conversion+active_days_after_conversion,train34)
          test34$predict<-ifelse(predict(rf,test34)>test34$predict,predict(rf,test34),test34$predict)
        }
      }  
      ########################days to conversion btw 10 and 60
      train35<-base::subset(train1,train1$days_to_conversion>=10&train1$days_to_conversion<60)
      
      test35<-base::subset(out,out$days_to_conversion>=10&out$days_to_conversion<60)
      if(nrow(test35)>0){
        if(nrow(train35)>=5){
          rf<-randomForest(lt_days~active_days_to_conversion+days_to_conversion+avg_session_cnt_to_conversion+avg_sessions_cnt_after_conversion+active_days_after_conversion,train35)
          test35$predict<-ifelse(predict(rf,test35)>test35$predict,predict(rf,test35),test35$predict)
        }
      }  
      ########################days to conversion <10
      
      test36<-base::subset(out,out$days_to_conversion<10)
      if(nrow(test36)>0)
      { test36$predict<-ifelse(test36$active_days_after_conversion>=11,300
                               ,ifelse(test36$active_days_after_conversion>=7,200,30))
      }
    out<-rbind(test30,test31,test32,test33,test34,test35,test36)
    output<-rbind(output,out)
    }
    
  #  out<-rbind(test30,test31,test32,test33,test34,test35,test36)
  #  output<-rbind(output,out)
  }
  }
}
output<-output[-1,]
output$predict<-round(output$predict,0)

# Save as JSON file
if(nrow(output) > 0)
{
  output<-createDataFrame(sqlContext, output)
  output$conversion_date<-cast(output$conversion_date, "String")
  output$install_date<-cast(output$install_date, "String")
  output$last_login_date<-cast(output$last_login_date, "String")
  saveDF(output, path=paste("s3n://com.funplusgame.bidata/model/r/ffs/life_time/model_output/", as.character(yesterday), "/", sep=""), source="json",  mode="overwrite")

}

# Stop Spark context
sparkR.stop()

DROP TABLE IF EXISTS model.life_time_model_performance_waf 	CASCADE;
CREATE TABLE model.life_time_model_performance_waf
(
  install_date                      date,
  user_key                          varchar(64),
  install_source                    varchar(64),
  country                           varchar(64),
  os                                varchar(64),
  conversion_date                   date,
  last_login_date                   date,
  lt_days                           integer,
  days_to_conversion                integer,
  active_days_to_conversion         integer,
  avg_session_cnt_to_conversion     numeric(14,4),
  active_days_after_conversion      integer,
  avg_sessions_cnt_after_conversion numeric(14,4),
  predict                           integer
 );

 COMMIT;
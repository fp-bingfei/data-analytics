

select
c1.user_key
,to_char(d1.install_date,'yyyy-mm-dd') as install_date
,d1.install_source
,d1.country
,d1.os
,to_char(trunc(d1.conversion_ts),'yyyy-mm-dd') as conversion_date
,to_char(last_login_date,'yyyy-mm-dd') as last_login_date
,case when trunc(d1.conversion_ts)-last_login_date>=0 then trunc(d1.conversion_ts) - install_date else last_login_date - install_date end as lt_days
,trunc(d1.conversion_ts) - install_date as days_to_conversion
,active_days_to_conversion
,level_end_to_conversion
,avg_session_cnt_to_conversion
,avg_revenue_usd_to_conversion
,active_days_after_conversion
,avg_sessions_cnt_after_conversion
from
(
	select
	a1.user_key
	,max(b1.date) as last_login_date
	,count(distinct b2.date) as active_days_to_conversion
	,max(b2.level_end) as level_end_to_conversion
	,sum(b2.session_cnt)/(case when count(b2.date)=0 then 1 else count(b2.date) end) as avg_session_cnt_to_conversion
    ,sum(b2.revenue_usd)/(case when count(b2.date)=0 then 1 else count(b2.date) end) as avg_revenue_usd_to_conversion
    ,count(distinct b3.date) as active_days_after_conversion
    ,sum(case when b3.session_cnt is null then 0 else b3.session_cnt end)/ (case when count(b3.date)=0 then 1 else count(b3.date) end) as avg_sessions_cnt_after_conversion
	from
	----random sampling users is not necessary as only 10% of total users are payers
		
				(
				select 
				*
				from processed.dim_user a
				where 
				install_date>='2014-09-01'
				and install_date<='2014-11-15'
				and country in ('Germany','United States')
				and is_payer=1
				) a1
	
	left join ---lt days calculation
		(
			select * from processed.fact_dau_snapshot 
		) b1 on 
		     datediff('day',a1.install_date, b1.date)<=365 ---within 1 year
		     and datediff('day',a1.install_date, b1.date)>=0
		     and a1.user_key = b1.user_key
	left join ---lt days calculation
		(
			select * from processed.fact_dau_snapshot 
		) b2 on 
		     datediff('day',b2.date,trunc(a1.conversion_ts))>=0 ---days before conversion		     
		     and datediff('day',a1.install_date,b2.date)>=0
		     and a1.user_key = b2.user_key
    left join
        
        (
        		select * from processed.fact_dau_snapshot 
        ) b3 on
        	datediff('day',trunc(a1.conversion_ts),b3.date)>0
        	and datediff('day',trunc(a1.conversion_ts),b3.date)<=14 --- 14 days after conversion		     
		    and a1.user_key = b3.user_key
	group by 
	a1.user_key
) c1
left join 
               (
				select 
				*
				from processed.dim_user a
				where 
				install_date>='2014-09-01'
				and install_date<='2014-11-15'
				and is_payer=1
				) d1 on c1.user_key=d1.user_key

where trunc(d1.conversion_ts) - d1.install_date<=365
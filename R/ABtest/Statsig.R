#!/usr/bin/Rscript --default-packages=methods,utils,stats,Rook

# import required methods and packages
source('/home/worker/rook/sample_size_test_duration.R')
library(RJDBC)

app <- function(env) {

    driver <- JDBC("com.amazon.redshift.jdbc41.Driver", "/home/worker/rook/RedshiftJDBC41-1.1.17.1017.jar", identifier.quote="`")
    url <- "jdbc:redshift://kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi?user=biadmin&password=Halfquest_2014"
    sql_path <- "/home/worker/rook/avg_kpi.sql"

    request <- Request$new(env)
    response <- Response$new()

    app_id <- request$GET()["game_id"]
    goals_list <- request$GET()["goal"]
    improvement <- as.numeric(request$GET()["improvement"])

    output_list <- estimation(driver, url, sql_path, app_id, goals_list, improvement)

    jsonp_response <- sprintf('%s({\"sample_size\":%f, \"duration\":%f})',
                              request$GET()["callback"], output_list$sample_size, output_list$test_duration)

    response$header('"Content-Type": "application/javascript"')
    response$write(jsonp_response)
    response$finish()
}

args <- commandArgs(trailingOnly=TRUE)

if (length(args) < 1) {
       cat(paste("Usage:",
                 substring(grep("^--file=", commandArgs(), value=T), 8),
                 "<port-number>\n"))
       quit(save="no", status=1)
} else if (length(args) > 1)
   cat("Warning: extra arguments ignored\n")

s <- Rhttpd$new()
app <- RhttpdApp$new(name='statsig', app=app)
s$add(app)
s$start(port=args[1], quiet=F)

suspend_console()

select
avg(k.dau) as dau,
avg(k.new_user) as new_user,
avg(k.new_payer) as new_payer,
avg(k.payers) as payers,
avg(k.revenue) as revenue,
avg(k.session_cnt) as session_cnt,
avg(k.playtime_sec) as playtime_sec,
avg(k.arppu) as arppu,
avg(k.arpdau) as arpdau,
sum(r.d1_retention)/count(r.d1_retention) as d1_retention,
sum(r.d3_retention)/count(r.d3_retention) as d3_retention,
sum(r.d7_retention)/count(r.d7_retention) as d7_retention,
avg(k.dau)/STDDEV_SAMP(k.dau) as cv_dau,
avg(k.new_user)/STDDEV_SAMP(k.new_user) as cv_new_user,
avg(k.new_payer)/STDDEV_SAMP(k.new_payer) as cv_new_payer,
avg(k.payers)/STDDEV_SAMP(k.payers) as cv_payers,
avg(k.revenue)/STDDEV_SAMP(k.revenue) as cv_revenue,
avg(k.session_cnt)/STDDEV_SAMP(k.session_cnt) as cv_session_cnt,
avg(k.playtime_sec)/STDDEV_SAMP(k.playtime_sec) as cv_playtime_sec
from
(select 
date,
sum(dau_cnt) as dau,
sum(new_user_cnt) as new_user,
sum(newpayer_cnt) as new_payer,
sum(payer_today_cnt) as payers,
sum(revenue_usd) as revenue,
sum(session_cnt) as session_cnt,
sum(playtime_sec) as playtime_sec,
sum(newpayer_cnt)/sum(dau_cnt)::float(8) as conversion_rate,
sum(revenue_usd)/sum(payer_today_cnt) as arppu,
sum(revenue_usd)/sum(dau_cnt) as arpdau
from kpi_processed.agg_kpi 
where 
app_id like '%s' 
and date >= dateadd(day,-14,sysdate)
group by 1) k
left join
(select
install_date,
max(case when player_day = 1 then retention else null end) as d1_retention,
max(case when player_day = 3 then retention else null end) as d3_retention,
max(case when player_day = 7 then retention else null end) as d7_retention
from
(select 
install_date,
player_day,
sum(retained_user_cnt)/sum(new_user_cnt)::float(8) as retention
from kpi_processed.agg_retention_ltv 
where app_id like '%s' 
and install_date >= dateadd(day,-14,sysdate)
group by 1,2
order by 1,2) a
group by 1) r
on k.date = r.install_date
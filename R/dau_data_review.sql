
drop table model.temp_quying;
CREATE TABLE model.temp_quying
(
   date               date,
   user_key           varchar(64),
   snsid              varchar(64),
   country            varchar(64),
   install_date       date,
   is_payer           integer,
   is_new_user        integer,
   final_level_end    integer,
   conversion_date    date,
   install_source     varchar(64),
   level              varchar(64),
   diff_to_install    integer,
   diff_to_today      integer,
   level_end          integer,
   session_cnt        integer,
   revenue_usd        numeric(14,4)
);

COMMIT;

delete from model.temp_quying;
insert into model.temp_quying

select
c3.date
,c3.user_key
,c3.snsid
,c3.country
,c3.install_date
,c3.is_payer
,c3.is_new_user
,c3.level_end as final_level_end
,c3.conversion_date
,c3.install_source
,c3.level
,datediff('day',c3.install_date,d1.date) as diff_to_install
,datediff('day',c3.date,d1.date) as diff_to_today
,d1.level_end
,d1.session_cnt
,d1.revenue_usd
from
	(
		select 
		     c2.*
			,datediff('day',install_date,date) as days_to_install
			,case when conversion_date is not null then datediff('day',install_date,conversion_date) else 10000 end as days_to_conversion
		from
			(
			select 
				c1.*
				,rank() over(partition by country,install_source,is_payer order by rownum) as rk
			from
				(
				select 
				     a1.date
					,a1.user_key
					,a1.snsid
					,b1.country
					,b1.install_date
					,a1.is_payer
					,a1.is_new_user
					,a1.level_end
					,trunc(b1.conversion_ts) as conversion_date
					,case when b1.install_source='Organic' then 'organic' else 'non-organic' end as install_source
					,case when a1.level_end>6 then 'high_level' else 'low_level' end as level
					,random() as rownum
				from processed.fact_dau_snapshot a1
				left join processed.dim_user b1
				on a1.user_key = b1.user_key
					where 
					a1.date ='2015-10-01'
					and b1.country='Germany'
					and a1.is_new_user=0
					and a1.level_end>6
				) c1
			) c2
			where rk<=5000
	) c3
left join processed.fact_dau_snapshot d1
		 on c3.user_key=d1.user_key
		 and datediff('day',c3.install_date,d1.date)>=0
		 and datediff('day',c3.date,d1.date)<=60 --future 60days
;
--------pulling data 
select 
date
,snsid
,datediff('day',install_date,date) as days_to_install
,final_level_end
,sum(case when diff_to_today>0 and diff_to_today<=30 then 1 else 0 end) as active_days_fwd_30
,max(case when diff_to_today>0 and diff_to_today<=30 then level_end else final_level_end end) as level_end_fwd_30
,sum(case when diff_to_today>30 and diff_to_today<=60 then 1 else 0 end) as active_days_fwd_60
,max(case when diff_to_today>30 and diff_to_today<=60 then level_end else final_level_end end) as level_end_fwd_60
,sum(case when diff_to_today<0 and diff_to_today>=-30 then 1 else 0 end) as active_days_bwd_30
,max(case when diff_to_today<0 and diff_to_today>=-30 then level_end else final_level_end end) as level_end_bwd_30
,sum(case when diff_to_today<-30 and diff_to_today>=-60 then 1 else 0 end) as active_days_bwd_60
,max(case when diff_to_today<-30 and diff_to_today>=-60 then level_end else final_level_end end) as level_end_bwd_60
,sum(case when diff_to_today<0 then 1 else 0 end) as total_active_days_to_today
from model.temp_quying
where
install_source='organic'
and is_payer=0
group by 
date
,snsid
,datediff('day',install_date,date) 
,final_level_end

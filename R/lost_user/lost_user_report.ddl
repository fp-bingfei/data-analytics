DROP TABLE IF EXISTS model.lost_user_report CASCADE;
CREATE TABLE model.lost_user_report
(
   date               date,
   snsid              varchar(64),
   country            varchar(64),
   install_date       date,
   days_to_install    integer,
   is_payer           smallint,
   bd_wk              integer,
   fd_wk              integer,
   level_end          smallint,
   session_cnt        integer,
   probability        numeric(14,4),
   prediction         integer,
   level              varchar(128) encode lzo,
   vip_level          varchar(128) encode lzo,
   ticket_1001        varchar(128) encode lzo,
   ticket_1002        varchar(128) encode lzo,
   ticket_1003        varchar(128) encode lzo,
   ticket_1005        varchar(128) encode lzo,
   kettle_25          varchar(128) encode lzo,
   kettle_100         varchar(128) encode lzo,
   fertilizer_25      varchar(128) encode lzo,
   fertilizer_100     varchar(128) encode lzo,
   neighbors_num      varchar(128) encode lzo,
   power              varchar(128) encode lzo,
   op                 varchar(128) encode lzo,
   gas                varchar(128) encode lzo,
   rc_left            varchar(128) encode lzo,
   coins_left         varchar(128) encode lzo
);

COMMIT;



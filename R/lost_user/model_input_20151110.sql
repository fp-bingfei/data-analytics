select 
		b1.date
		,b1.user_key
		,b1.snsid
		,b1.country
		,b1.install_date
		,b1.date - b1.install_date as days_to_install
		,b1.is_payer
		,b1.bd_wk
		,b1.fd_wk
--		,b2.fd_wk_7
		,b1.level_end
		,b1.session_cnt
	from  
		(select
						date
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
						,sum(case when diff<0 then 1 else 0 end) as bd_wk
						,sum(case when diff>0 then 1 else 0 end) as fd_wk
						,avg(level_end) as level_end
						,avg(session_cnt) as session_cnt
						from
						(
						select 
						a.*
						, datediff('day', a.date, c.date) as diff
						, a.date - a.install_date as days_to_install
						from
							(select 
							*
							from processed.fact_dau_snapshot 
							where 
							date>='2015-09-18'  -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
							and date<='2015-10-01'-- including test_date and training data from test_date -7
							and country in ('Russian Federation','Thailand') 
							) a
						LEFT JOIN
							(select 
								date
							    , user_key 
							from processed.fact_dau_snapshot
							) c
						on datediff('day', a.date, c.date)<=3 ----change it for future n days
						and datediff('day', a.date, c.date)>=-14
						and a.user_key = c.user_key
						) d
						group by 
						date
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
		) b1
--		LEFT JOIN
--		(select
--						date
--						,user_key
--						,sum(case when diff>0 then 1 else 0 end) as fd_wk_7
--						from
--						(
--						select 
--						a.user_key
--						,a.date
--						, datediff('day', a.date, c2.date) as diff
--						from
--							(select 
--							*
--							from processed.fact_dau_snapshot 
--							where 
--							date>='2015-09-18'  -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
--							and date<='2015-09-24'-- test_date -7
--							and country in ('Russian Federation','Thailand')
--							) a
--						LEFT JOIN
--							(select 
--								date
--							    , user_key 
--							from processed.fact_dau_snapshot
--							) c2
--						--on c.date>=a.date 
--						on datediff('day', a.date, c2.date)<=7 ----change it for future n days
--						and datediff('day', a.date, c2.date)>=0
--						and a.user_key = c2.user_key
--		                ) d2
--		                group by 
--		                date
--		                ,user_key
--		) b2 on b1.user_key=b2.user_key and b1.date=b2.date
--	where b1.bd_wk>0 and b1.level_end>3
--
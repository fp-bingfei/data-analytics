delete from model.lost_user_report where date='_YESTERDAY_';

-- Insert records
insert into model.lost_user_report

select 
  a.*
   ,b.level           
   ,b.vip_level
   ,b.ticket_1001
   ,b.ticket_1002
   ,b.ticket_1003
   ,b.ticket_1005
   ,b.kettle_25
   ,b.kettle_100
   ,b.fertilizer_25
   ,b.fertilizer_100
   ,b.neighbors_num
   ,b.power
   ,b.op
   ,b.gas
   ,b.rc_left
   ,b.coins_left   
from

  (select * from model.lost_user_model_output where date='_YESTERDAY_') a
left join
 (select ts,uid,snsid,
json_extract_path_text(properties,'level') as level,
json_extract_path_text(properties,'vip_level') as vip_level,
nullif(json_extract_path_text(properties,'ticket','1001'),'') as ticket_1001,
nullif(json_extract_path_text(properties,'ticket','1002'),'') as ticket_1002,
nullif(json_extract_path_text(properties,'ticket','1003'),'') as ticket_1003,
nullif(json_extract_path_text(properties,'ticket','1005'),'') as ticket_1005,
nullif(json_extract_path_text(properties,'farm_aides','kettle','25'),'') as kettle_25,
nullif(json_extract_path_text(properties,'farm_aides','kettle','100'),'') as kettle_100,
nullif(json_extract_path_text(properties,'farm_aides','fertilizer','25'),'') as fertilizer_25,
nullif(json_extract_path_text(properties,'farm_aides','fertilizer','100'),'') as fertilizer_100,
nullif(json_extract_path_text(properties,'neighbors_num'),'') as neighbors_num,
nullif(json_extract_path_text(properties,'power'),'') as power,
nullif(json_extract_path_text(properties,'op'),'') as op,
nullif(json_extract_path_text(properties,'gas'),'') as gas,
nullif(json_extract_path_text(properties,'rc_left'),'') as rc_left,
nullif(json_extract_path_text(properties,'coins_left'),'') as coins_left
from public.events_raw where event='login' and trunc(ts)='_YESTERDAY_'  and app='ffs.tango.prod') b
on a.date=trunc(b.ts)
and a.snsid=b.snsid
;

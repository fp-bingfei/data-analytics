DROP TABLE IF EXISTS model.lost_user_model_input CASCADE;

CREATE TABLE model.lost_user_model_input
(
   date               date,
   user_key           varchar(64),
   snsid              varchar(64),
   country            varchar(64),
   install_date       date,
   days_to_install    integer,
   is_payer           smallint,
   bd_wk              integer,
   fd_wk              integer,
   level_end          integer,
   session_cnt        integer
);

COMMIT;


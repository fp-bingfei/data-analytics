DROP TABLE IF EXISTS model.aa_test_metrics CASCADE;
CREATE TABLE model.aa_test_metrics 
(
date                 date
,country             varchar(32)
,user_tag            varchar(32)
,snsid               varchar(64)
,app                 varchar(32)
,time_window         varchar(32)
,retain              varchar(32)
,bwd_active_days     integer
)
;
DROP TABLE IF EXISTS model.ffs_message_open CASCADE;
CREATE TABLE model.ffs_message_open 
(
country        varchar(32)
,country_code  varchar(32)
,app           varchar(32)
,sent_date     date
,open_date     date
,snsid         varchar(64)
)
;

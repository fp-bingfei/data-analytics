DELETE FROM model.aa_test_metrics;
INSERT INTO model.aa_test_metrics


with sample as
(
select
date
,country
,user_tag
,snsid
,app
,count(*)
from
(
select a.*
from model.lost_user_model_msg_list a
where ((a.user_tag='test' and a.obs=1) or a.user_tag='control')
union all 
select b.*
from model.lost_user_model_msg_list_2 b
where ((b.user_tag='test' and b.obs=1) or b.user_tag='control')
)
group by 
date
,country
,user_tag
,snsid
,app
)


-- next 14 days
select 
t1.date
,t1.country
,t1.user_tag
,t1.snsid
,t1.app
,t1.time_window
,case when t1.bwd_active_days>=7 then 'retained' else 'lost' end as retain
,t1.bwd_active_days
from
(
select 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app	
	,sum(case when datediff('day',s.date,f.date)<0 then 1 else 0 end) as bwd_active_days
	,'fwd_14d' as time_window
from sample s
left join processed.fact_dau_snapshot f
	on s.snsid = f.snsid
	and s.app = f.app
	and datediff('day',s.date, f.date)>=-14
	and datediff('day',s.date, f.date)<0
where s.date<=dateadd(day,-14,'_YESTERDAY_') -- at least 14 days before yesterday
group by 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
) t1 

union all

select
t2.date
,t2.country
,t2.user_tag
,t2.snsid
,t2.app
,t2.time_window
,case when t2.bwd_active_days>=3 then 'retained' else 'lost' end as retain
,t2.bwd_active_days
from
(
-- next 7 days
select 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
	,sum(case when datediff('day',s.date,f.date)<0 then 1 else 0 end) as bwd_active_days
	,'fwd_7d' as time_window
from sample s
left join processed.fact_dau_snapshot f
	on s.snsid = f.snsid
	and s.app = f.app
	and datediff('day',s.date, f.date)>=-7
	and datediff('day',s.date, f.date)<0
where s.date<=dateadd(day,-7,'_YESTERDAY_') -- at least 7 days before yesterday
group by 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
) t2




;
-- Clean input table
delete from model.lost_user_model_input_test;

-- Prepare data
insert into model.lost_user_model_input_test
select 
		b1.date
		,b1.app
		,b1.user_key
		,b1.snsid
		,b1.country
		,b1.install_date
		,b1.date - b1.install_date as days_to_install
		,b1.is_payer
		,b1.bd_wk
		,b1.fd_wk
--		,b2.fd_wk_7
		,b1.level_end
		,b1.session_cnt
	from  
		(select
						date
						,app_id as app
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
						,sum(case when diff<0 then 1 else 0 end) as bd_wk
						,sum(case when diff>0 then 1 else 0 end) as fd_wk
						,avg(level_end) as level_end
						,avg(session_cnt) as session_cnt
						from
						(
						select 
						a.*
						, datediff('day', a.date, c.date) as diff
						, a.date - a.install_date as days_to_install
						from
							(select 
							a1.*
							,b1.install_date 
							,b1.user_id as snsid
							from kpi_processed.fact_dau_snapshot a1
							left join kpi_processed.dim_user b1
							on a1.user_key = b1.user_key
							where 
							(date>=DATEADD(DAY, -13, '_YESTERDAY_')    -----test_date -13
							and date<=DATEADD(DAY, -7, '_YESTERDAY_')  -- test_date -7
							or
							date='_YESTERDAY_')                        -- test_date
							and a1.country = 'Germany'
							and a1.app_id = 'farm.de.prod'
							and level_end>3 -- excluding new users
							) a
						LEFT JOIN
							(select 
								date
							    , user_key 
							from kpi_processed.fact_dau_snapshot
							where app_id = 'farm.de.prod'
							and country = 'Germany'
							) c
						on datediff('day', a.date, c.date)<=3 ----change it for future n days
						and datediff('day', a.date, c.date)>=-14
						and a.user_key = c.user_key
						) d
						group by 
						date
						,app_id
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
		) b1

--
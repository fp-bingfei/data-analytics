delete from model.lost_user_model_msg_list_2 where date='_YESTERDAY_'; 

insert into model.lost_user_model_msg_list_2

with temp_table_a as 
(
select 
		a.date
		,a.snsid
		,a.country
		,b.app
		,c.user_tag
		,c.obs
from model.lost_user_model_output_2 a
left join processed.dim_user b 
     on a.snsid = b.snsid
left join model.lost_user_model_msg_list_2 c 
     on a.snsid = c.snsid 
     and c.date < '_YESTERDAY_' 
where a.date = '_YESTERDAY_'
      and a.prediction=0
      and b.app='ffs.global.prod'
)

select
date
,app
,country
,snsid
,user_tag
,case when user_tag='test' then obs+1 else obs end as obs
from temp_table_a
where ((user_tag='test' and obs<=7) or user_tag='control') -------already in experiment

union all

select ----those not in experiment
date
,app
,country
,snsid
,case when rk<=cut_off then 'test' else 'control' end as user_tag
,case when rk<=cut_off then 1 else 0 end as obs
from
(
select 
a1.*
,rank() over(partition by date,country,app order by random_num) as rk
from
	(
	select 
		a.*
		,random() as random_num
		,round(snsid_cnt/2) as cut_off
	from temp_table_a a 
	left join 
		(select 
		 date,app,country,count(distinct snsid) as snsid_cnt 
		 from temp_table_a 
		 where (user_tag is null or obs>7)
		 group by date,app,country
		 ) b 
		 on a.date = b.date 
		 and a.country = b.country 
		 and a.app = b.app
		 where (user_tag is null or obs>7)
	) a1
) a2
;
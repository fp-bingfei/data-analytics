DROP TABLE IF EXISTS model.lost_user_model_msg_obs_global CASCADE;
CREATE TABLE model.lost_user_model_msg_obs_global
(
  
   snsid              varchar(64),
   user_tag           varchar(64),
   obs                integer

);
COMMIT;
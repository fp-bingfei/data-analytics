delete from model.lost_user_model_msg_obs_test; 

insert into model.lost_user_model_msg_obs_test



	select
	snsid, user_tag
	,datediff('day', date, '_YESTERDAY_')+1 as obs
	from
	(
	select 
	snsid, user_tag, date, rank() over(partition by snsid,user_tag order by date desc) as rk
	from model.lost_user_model_msg_list_test
	where user_tag = 'test1'
	and obs=1
	and date < '_YESTERDAY_'
	) a1
	where rk=1

	union all

	select
	snsid, user_tag
	,datediff('day', date, '_YESTERDAY_')+1 as obs
	from
	(
	select 
	snsid, user_tag, date, rank() over(partition by snsid,user_tag order by date desc) as rk
	from model.lost_user_model_msg_list_test
	where user_tag = 'test2'
	and obs=1
	and date < '_YESTERDAY_'
	) a1
	where rk=1

    union all

    select
	snsid, user_tag
	,datediff('day', date, '_YESTERDAY_')+1 as obs
	from
	(
	select 
	snsid, user_tag, date, rank() over(partition by snsid,user_tag order by date desc) as rk
	from model.lost_user_model_msg_list_test
	where user_tag = 'test3'
	and obs=1
	and date < '_YESTERDAY_'
	) a1
	where rk=1

	union all 

    select snsid, user_tag, obs
    from model.lost_user_model_msg_list_test
    where user_tag = 'control'
    and date < '_YESTERDAY_'
;
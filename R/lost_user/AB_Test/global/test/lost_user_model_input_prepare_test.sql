-- Clean input table
delete from model.lost_user_model_input_test;

-- Prepare data
insert into model.lost_user_model_input_test
select 
		b1.date
		,b1.app
		,b1.user_key
		,b1.snsid
		,b1.country
		,b1.install_date
		,b1.date - b1.install_date as days_to_install
		,b1.is_payer
		,b1.bd_wk
		,b1.fd_wk
--		,b2.fd_wk_7
		,b1.level_end
		,b1.session_cnt
	from  
		(select
						date
						,app
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
						,sum(case when diff<0 then 1 else 0 end) as bd_wk
						,sum(case when diff>0 then 1 else 0 end) as fd_wk
						,avg(level_end) as level_end
						,avg(session_cnt) as session_cnt
						from
						(
						select 
						a.*
						, datediff('day', a.date, c.date) as diff
						, a.date - a.install_date as days_to_install
						from
						(
							select 
							a.*
							from processed.fact_dau_snapshot a
							join 
								(
									select
										country,avg(users) as avg_users
										from
										(
										select country,date,count(distinct user_key) as users
										from processed.fact_dau_snapshot
										where app='ffs.global.prod'
										and level_end>3
										and date <= '_YESTERDAY_'
										and date >=DATEADD(DAY, -14, '_YESTERDAY_') 
										group by country,date
										)
										group by country
										having avg(users)>=2000
								) b -- only including countries with avg dau >=200
							      on a.country = b.country
							where 
								(a.date>=DATEADD(DAY, -13, '_YESTERDAY_')    -----test_date -13
								and a.date<=DATEADD(DAY, -7, '_YESTERDAY_')  -- test_date -7
								or
								a.date='_YESTERDAY_')                        -- test_date
								--and country in ('Russian Federation','Poland','Egypt','Brazil','Netherlands') 
								and a.app = 'ffs.global.prod'
								and a.level_end>3 -- excluding new users
							) a
						LEFT JOIN
							(select 
								date
							    , user_key 
							from processed.fact_dau_snapshot
							) c
						on datediff('day', a.date, c.date)<=3 ----change it for future n days
						and datediff('day', a.date, c.date)>=-14
						and a.user_key = c.user_key
						) d
						group by 
						date
						,app
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
		) b1

--
DELETE FROM model.lost_user_model_input_test_global;
INSERT INTO model.lost_user_model_input_test_global


with users as 
(
select 
	country, count(distinct user_key)/count(distinct date) 
	from processed.fact_dau_snapshot
	where 
	date>=DATEADD(DAY, -7, '_YESTERDAY_')    -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
	and date<'_YESTERDAY_'
	--and app ='ffs.tango.prod'
	and app = 'ffs.global.prod'
	group by country
	having count(distinct user_key)/count(distinct date) >=10000 -- only include country with avg dau>=2000

)
,rc_out as (
select 
	a.date
	,a.user_key
	,a.snsid
	,sum(case when datediff('day', a.date, b.date)<-7 then rc_out else 0 end) as bw_total_rc_out_14
	,sum(case when datediff('day', a.date, b.date)<=0 and datediff('day', a.date, b.date)>=-7 then rc_out else 0 end) as bw_total_rc_out_7
	,sum(case when datediff('day', a.date, b.date)<-7 then rc_in else 0 end) as bw_total_rc_in_14
	,sum(case when datediff('day', a.date, b.date)<=0 and datediff('day', a.date, b.date)>=-7 then rc_in else 0 end) as bw_total_rc_in_7
from
(
select 
		date
		,user_key
		,snsid
	    ,count(*)
	from processed.fact_dau_snapshot p1
	join users p2
	on p1.country = p2.country
	where 
		((
		date>=DATEADD(DAY, -13, '_YESTERDAY_')    -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
		and date<=DATEADD(DAY, -7, '_YESTERDAY_') 
		) -- test_date -7
		or
		date='_YESTERDAY_' )                      -- test_date
		--and country = 'Germany'
		--and app = 'ffs.tango.prod'
		and app = 'ffs.global.prod'
    group by 
	    date
		,user_key
		,snsid
) a
LEFT JOIN 
	(
	select
		date
		,user_key
		,app
		,country
		,max(level) as level_end
		,sum(case when rc_out is null then 0 else rc_out end) as rc_out
		,sum(case when rc_in is null then 0 else rc_in end) as rc_in
	from processed.rc_transaction
	where 
		--app = 'ffs.tango.prod'
		app = 'ffs.global.prod'
		--and country = 'Germany'
		and date >=DATEADD(DAY, -13-14, '_YESTERDAY_')
		and date <='_YESTERDAY_'
	group by 
		date
		,user_key
		,app
		,country
	) b
on a.user_key = b.user_key
and datediff('day', a.date, b.date)<=0 ----change it for future n days
and datediff('day', a.date, b.date)>=-14
group by 
	a.date
	,a.user_key
	,a.snsid
)
, temp_table as 
(
select 
date, user_key, snsid, country, install_date,is_payer,days_to_install
,bw_days_14, bw_days_7, fd_wk
,case when bw_level_14 is null and bw_level_start_7 is not null then bw_level_start_7 
      when bw_level_14 is null and bw_level_start_7 is null then 1 else bw_level_14 end as bw_level_14
,case when bw_level_7 is null and bw_level_start_7 is not null then bw_level_start_7 
      when bw_level_7 is null and bw_level_start_7 is null then 1 else bw_level_7 end as bw_level_7
,bw_avg_sessions_14, bw_avg_sessions_7
,bw_avg_playtime_14, bw_avg_playtime_7
,bw_total_coins_in_14, bw_total_coins_in_7
,bw_total_coins_out_14, bw_total_coins_out_7
from
(
select
						date
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
						,days_to_install
						-- active days
						,count(distinct case when diff<-7 then date2 else null end) as bw_days_14
						,count(distinct case when diff<=0 and diff>=-7 then date2 else null end) as bw_days_7
						,count(distinct case when diff>0 then date2 else null end) as fd_wk
						-- maximum level
						,max(case when diff<-7 then level_end2 else null end) as bw_level_14
						,max(case when diff<=0 and diff>=-7 then level_end2 else null end) as bw_level_7
						,min(case when diff<=0 then level_start2 else null end) as bw_level_start_7
                        -- average daily sessions
						,case when count(distinct case when diff<-7 then date2 else null end) >0 then 
						      sum(case when diff<-7 then session_cnt2 else 0 end) * 1.00/count(distinct case when diff<-7 then date2 else null end)
						      else 0 end as bw_avg_sessions_14
						,case when count(distinct case when diff<=0 and diff>=-7 then date2 else null end) >0 then 
						      sum(case when diff<=0 and diff>=-7 then session_cnt2 else 0 end) * 1.00/count(distinct case when diff<=0 and diff>=-7 then date2 else null end)
						      else 0 end as bw_avg_sessions_7

                        -- average daily playtime seconds
						,case when count(distinct case when diff<-7 then date2 else null end) >0 then 
						      sum(case when diff<-7 then playtime2 else 0 end) * 1.00/count(distinct case when diff<-7 then date2 else null end)
						      else 0 end as bw_avg_playtime_14
						,case when count(distinct case when diff<=0 and diff>=-7 then date2 else null end) >0 then 
						      sum(case when diff<=0 and diff>=-7 then playtime2 else 0 end) * 1.00/count(distinct case when diff<=0 and diff>=-7 then date2 else null end)
						      else 0 end as bw_avg_playtime_7
						-- total coins in/out

						,sum(case when diff<-7 then coins_in2 else 0 end) as bw_total_coins_in_14
						,sum(case when diff<=0 and diff>=-7 then coins_in2 else 0 end) as bw_total_coins_in_7
						,sum(case when diff<-7 then coins_out2 else 0 end) as bw_total_coins_out_14
						,sum(case when diff<=0 and diff>=-7 then coins_out2 else 0 end) as bw_total_coins_out_7

						from
						(
						select 
						a.*
						, datediff('day', a.date, c.date) as diff
						, a.date - a.install_date as days_to_install
						,c.date as date2
						,c.level_start as level_start2
						,c.level_end as level_end2
						,c.session_cnt as session_cnt2
						,c.playtime as playtime2
						,c.coins_in as coins_in2
						,c.coins_out as coins_out2
						from
							(select 
								date
								,user_key
								,snsid
								,p1.country
								,install_date
								,is_payer
								,min(level_start) as level_start
								,max(level_end) as level_end
								,sum(session_cnt) as session_cnt
								,sum(playtime_sec) as playtime
								,sum(coins_in) as coins_in
								,sum(coins_out) as coins_out
							from processed.fact_dau_snapshot p1
							join users p2
							   on p1.country = p2.country
							where 
								((
								date>=DATEADD(DAY, -13, '_YESTERDAY_')    -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
								and date<=DATEADD(DAY, -7, '_YESTERDAY_') 
								) -- test_date -7
								or
								date='_YESTERDAY_'  
								)                     -- test_date
								--and country in ('Germany') 
								--and app = 'ffs.tango.prod'
								and app = 'ffs.global.prod'
								and level_end>3
                            group by 
                                date
								,user_key
								,snsid
								,p1.country
								,install_date
								,is_payer
							) a
						LEFT JOIN
							(select 
								date
							    , user_key 
							    , min(level_start) as level_start
							    , max(level_end) as level_end
							    , sum(session_cnt) as session_cnt
							    , sum(case when playtime_sec is null then 0 else playtime_sec end) as playtime
							    , sum(case when coins_in is null then 0 else coins_in end) as coins_in
							    , sum(case when coins_out is null then 0 else coins_out end) as coins_out
							from processed.fact_dau_snapshot
							where 
							--country in ('Germany') 
							--app = 'ffs.tango.prod'
							app = 'ffs.global.prod'
							group by 
							date, user_key
							) c
						on datediff('day', a.date, c.date)<=7 ----change it for future n days
						and datediff('day', a.date, c.date)>=-14
						and a.user_key = c.user_key

						) d
						group by 
						date
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
						,days_to_install
)
) 

select 
t1.*
,r1.bw_total_rc_out_7
,r1.bw_total_rc_out_14
,r1.bw_total_rc_in_7
,r1.bw_total_rc_in_14
--,c1.user_key as cheaters 
from temp_table t1
left join rc_out r1
on t1.user_key = r1.user_key
and t1.date = r1.date
--left join processed.cheaters_new c1
-- on t1.user_key = c1.user_key

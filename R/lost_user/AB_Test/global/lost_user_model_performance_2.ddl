DROP TABLE IF EXISTS model.lost_user_model_performance_2 CASCADE;
CREATE TABLE model.lost_user_model_performance_2
(
   date               date,
   app                varchar(64),
   country            varchar(64),
   obs                integer,
   recall             numeric(14,4),
   precision          numeric(14,4),
   cutoff             numeric(14,4)     
);

COMMIT;
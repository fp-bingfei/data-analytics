DELETE FROM model.ab_test_metrics;
INSERT INTO model.ab_test_metrics


with sample as
(
select 
date
,country
,user_tag
,snsid
,app
,status
,count(*)
from
	(
	select 
	c.*
	,case when d.open_date is not null then 'open' else 'not_open' end as status
	from
	(
	select a.*
	from model.lost_user_model_msg_list a
	where ((a.user_tag='test' and a.obs=1) or a.user_tag='control')
	union all 
	select b.*
	from model.lost_user_model_msg_list_2 b
	where ((b.user_tag='test' and b.obs=1) or b.user_tag='control')
	) c
	left join model.ffs_message_open d
	on c.snsid = d.snsid
	)
where date > '2016-01-20'
group by 
date
,country
,user_tag
,snsid
,app
,status
)

-- next 14 days
select 
t1.date
,t1.country
,t1.user_tag
,t1.snsid
,t1.app
,t1.status
,t1.time_window
,case when t1.fwd_active_days>=7 then 'retained' else 'lost' end as retain
,t1.level_end
,t1.fwd_active_days
,t1.bwd_active_days
,case when t1.fwd_active_days>0 then t1.fwd_session_cnt_total*1.00/t1.fwd_active_days else 0 end as avg_fwd_session_cnt
,case when t1.bwd_active_days>0 then t1.bwd_session_cnt_total*1.00/t1.bwd_active_days else 0 end as avg_bwd_session_cnt
,t1.fwd_payers
,t1.bwd_payers
,t1.fwd_revenue
,t1.bwd_revenue
from
(
select 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
	,s.status
	,sum(case when datediff('day',s.date,f.date)>0 then 1 else 0 end) as fwd_active_days
	,sum(case when datediff('day',s.date,f.date)<0 then 1 else 0 end) as bwd_active_days

	,sum(case when datediff('day',s.date,f.date)>0 then session_cnt else 0 end) as fwd_session_cnt_total
	,sum(case when datediff('day',s.date,f.date)<0 then session_cnt else 0 end) as bwd_session_cnt_total
	
	,max(case when datediff('day',s.date,f.date)<=0 then level_end else null end) as level_end

	,max(case when datediff('day',s.date,f.date)>0 then is_payer else null end) as fwd_payers
	,max(case when datediff('day',s.date,f.date)<0 then is_payer else null end) as bwd_payers

	,sum(case when datediff('day',s.date,f.date)>0 then revenue_usd else 0 end) as fwd_revenue
	,sum(case when datediff('day',s.date,f.date)<0 then revenue_usd else 0 end) as bwd_revenue

	,'fwd_14d' as time_window
from sample s
left join processed.fact_dau_snapshot f
	on s.snsid = f.snsid
	and s.app = f.app
	and datediff('day',s.date, f.date)>=-14
	and datediff('day',s.date, f.date)<=14
where s.date<=dateadd(day,-14,'_YESTERDAY_') -- at least 14 days before yesterday
group by 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
	,s.status
) t1 

union all

select
t2.date
,t2.country
,t2.user_tag
,t2.snsid
,t2.app
,t2.status
,t2.time_window
,case when t2.fwd_active_days>=3 then 'retained' else 'lost' end as retain
,t2.level_end
,t2.fwd_active_days
,t2.bwd_active_days
,case when t2.fwd_active_days>0 then t2.fwd_session_cnt_total*1.00/t2.fwd_active_days else 0 end as avg_fwd_session_cnt
,case when t2.bwd_active_days>0 then t2.bwd_session_cnt_total*1.00/t2.bwd_active_days else 0 end as avg_bwd_session_cnt
,t2.fwd_payers
,t2.bwd_payers
,t2.fwd_revenue
,t2.bwd_revenue
from
(
-- next 7 days
select 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
	,s.status
	,sum(case when datediff('day',s.date,f.date)>0 then 1 else 0 end) as fwd_active_days
	,sum(case when datediff('day',s.date,f.date)<0 then 1 else 0 end) as bwd_active_days

	,sum(case when datediff('day',s.date,f.date)>0 then session_cnt else 0 end) as fwd_session_cnt_total
	,sum(case when datediff('day',s.date,f.date)<0 then session_cnt else 0 end) as bwd_session_cnt_total

	,max(case when datediff('day',s.date,f.date)<=0 then level_end else null end) as level_end

	,max(case when datediff('day',s.date,f.date)>0 then is_payer else null end) as fwd_payers
	,max(case when datediff('day',s.date,f.date)<0 then is_payer else null end) as bwd_payers

	,sum(case when datediff('day',s.date,f.date)>0 then revenue_usd else 0 end) as fwd_revenue
	,sum(case when datediff('day',s.date,f.date)<0 then revenue_usd else 0 end) as bwd_revenue

	,'fwd_7d' as time_window
from sample s
left join processed.fact_dau_snapshot f
	on s.snsid = f.snsid
	and s.app = f.app
	and datediff('day',s.date, f.date)>=-7
	and datediff('day',s.date, f.date)<=7
where s.date<=dateadd(day,-7,'_YESTERDAY_') -- at least 14 days before yesterday
group by 
	s.date
	,s.country
	,s.user_tag
	,s.snsid
	,s.app
    ,s.status
) t2



;
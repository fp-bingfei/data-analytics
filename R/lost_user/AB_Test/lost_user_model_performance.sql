-- Delete existing records
delete from model.lost_user_model_performance where date=DATEADD(DAY, -3, '_YESTERDAY_'); --should be yesterday-3

-- Insert records
insert into model.lost_user_model_performance

select
	date,country,obs
	,case when actual_0>0 then cast(correct_0 as float)/cast(actual_0 as float) else 0 end as recall
	,case when predict_0>0 then cast(correct_0 as float)/cast(predict_0 as float) else 0 end as precision
	,0.1 as cutoff
from
(
select
date
,country
,sum(1) as obs
,sum(case when actual=0 and prediction=0 then 1 else 0 end) as correct_0
,sum(case when actual=0 then 1 else 0 end) as actual_0
,sum(case when prediction=0 then 1 else 0 end) as predict_0
from
	(
	select 
	e.*
	,case when e.fd_wk>1 then 1 else 0 end as actual	
	,f.prediction
	from
		(
			select
			date
			,snsid
			,country
			,sum(case when diff>0 then 1 else 0 end) as fd_wk
			from
				(
					select 
					a.*
					, datediff('day', a.date, c.date) as diff
					from
							(select 
							*
							from processed.fact_dau_snapshot 
							where 
							date=DATEADD(DAY, -3, '_YESTERDAY_')  ---in production, it should be yesterday-3, assuming yesterday is 2015-10-04
							and country in ('Russian Federation','Thailand','Germany','Egypt','Turkey','Romania','Brazil','Poland','Saudi Arabia','United States','France') 
							and app = 'ffs.tango.prod'
							) a
						LEFT JOIN
							(select 
							date
							, user_key 
							from processed.fact_dau_snapshot
							) c
					on datediff('day', a.date, c.date)<=3 ----for future 3 days
					and datediff('day', a.date, c.date)>=0
					and a.user_key = c.user_key
				) d
			group by 
			date
			,snsid
			,country
		) e ----actual data	
	left join 
		(
			select 
			date
			,snsid
			,prediction
			from model.lost_user_model_output
			where date=DATEADD(DAY, -3, '_YESTERDAY_') ---- yesterday-3
		) f 
	on e.date=f.date and e.snsid=f.snsid
	) g
group by date,country
) h
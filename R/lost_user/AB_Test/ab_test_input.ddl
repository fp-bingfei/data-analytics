DROP TABLE IF EXISTS model.ab_test_input CASCADE;
CREATE TABLE model.ab_test_input
(
date                         date
,country                     varchar(32)
,app                         varchar(32)
,time_window                 varchar(32)
,obs_test                    integer
,obs_ctr                     integer
,retention_rate_test_ab      numeric(14,4)
,retention_rate_ctr_ab       numeric(14,4)
,retention_rate_test_aa      numeric(14,4)
,retention_rate_ctr_aa       numeric(14,4)
,delta_retention_rate_test   numeric(14,4)
,delta_retention_rate_ctr    numeric(14,4)
,delta_active_days_test      integer
,delta_active_days_ctr       integer
,delta_session_cnt_test      numeric(14,4)
,delta_session_cnt_ctr       numeric(14,4)
)
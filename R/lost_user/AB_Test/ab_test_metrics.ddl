DROP TABLE IF EXISTS model.ab_test_metrics CASCADE;
CREATE TABLE model.ab_test_metrics 
(
date                 date
,country             varchar(32)
,user_tag            varchar(32)
,snsid               varchar(64)
,app                 varchar(32)
,status              varchar(32)
,time_window         varchar(32)
,retain              varchar(32)
,level_end           integer
,fwd_active_days     integer
,bwd_active_days     integer
,avg_fwd_session_cnt numeric(14,4)
,avg_bwd_session_cnt numeric(14,4)
)
;
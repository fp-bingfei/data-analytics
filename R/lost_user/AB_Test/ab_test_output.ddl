DROP TABLE IF EXISTS model.ab_test_output CASCADE;
CREATE TABLE model.ab_test_output
(
country                    varchar(32)
,app                       varchar(32)
,time_window               varchar(32)
,df                        integer
,avg_obs_test              integer
,avg_obs_ctr               integer
,delta_retention_rate      numeric(14,4)
,sd_retention_rate         numeric(14,4)
,t_score_retention_rate    numeric(14,4)
,p_value_retention_rate    numeric(14,4)
,delta_session_cnt         numeric(14,4)
,sd_session_cnt            numeric(14,4)
,t_score_session_cnt       numeric(14,4)
,p_value_session_cnt       numeric(14,4)
,delta_active_days         numeric(14,4)
,sd_active_days            numeric(14,4)
,t_score_active_days       numeric(14,4)
,p_value_active_days       numeric(14,4)
,delta_retention_rate_ctr  numeric(14,4)
,delta_retention_rate_test numeric(14,4)
,delta_session_cnt_ctr     numeric(14,4)
,delta_session_cnt_test    numeric(14,4)
,delta_active_days_ctr     numeric(14,4)
,delta_active_days_test    numeric(14,4)
)
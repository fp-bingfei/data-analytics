with temp_table as 
(
	select 
	date
	,case when country in ('Russian Federation','Brazil','Germany','Egypt','Turkey','Romania') then country else 'others' end as country
	,snsid
	,count(*)
	from model.lost_user_model_output
	where 
	date>='2015-12-14'
	and date<='2015-12-21'
	and prediction=0
	group by 
	date
	,case when country in ('Russian Federation','Brazil','Germany','Egypt','Turkey','Romania') then country else 'others' end
	,snsid

)
, temp_table2 as 
(
	select
	date
	,country
	,snsid
	,case when rk<=cut_off then 'control' else 'test' end as user_tag
	from
	(
		select
		a1.*
		,rank() over(partition by a1.date,a1.country order by random_num) as rk
		,round(obs/2) as cut_off
		from
			(
			select 
			t.*
			,random() as random_num
			from temp_table t
			) a1
		left join 
		(select date, country, count(snsid) as obs from temp_table group by date, country) b1
		on a1.date = b1.date
		and a1.country = b1.country
	) a
)
,temp_table3 as 
(

select
	a1.date
	,a1.country
	,a1.snsid
	,a1.user_tag
	,datediff('day',a1.date,b1.date) as date_diff
	,b1.level_end
	,b1.session_cnt
	,b1.is_payer
from temp_table2 a1
left join processed.fact_dau_snapshot b1
	on a1.snsid = b1.snsid
	and datediff('day',a1.date,b1.date)>=-3
	and datediff('day',a1.date,b1.date)<=3
)
,temp_table4 as
(
	select
	date
	,country
	,user_tag
	,count(snsid) as obs
	,sum(case when churn>0 then 1 else 0 end)*1.0/count(snsid) as retention_rate
	,avg(avg_fwd_session_cnt)*1.0 as avg_fwd_session_cnt
	,avg(avg_bwd_session_cnt)*1.0 as avg_bwd_session_cnt
	,avg(fwd_level_end)*1.0 as avg_fwd_level_end
	,avg(bwd_level_end)*1.0 as avg_bwd_level_end
	,avg(avg_fwd_session_cnt)*1.0 - avg(avg_bwd_session_cnt)*1.0 as diff_avg_session_cnt
	,avg(fwd_level_end)*1.0 - avg(bwd_level_end)*1.0 as diff_avg_level_end
	from
		(
		select
		date
		,country
		,snsid
		,user_tag
		,is_payer
		,case when fwd_active_days>1 then 1 else 0 end as churn
		,case when fwd_active_days>0 then fwd_session_cnt/fwd_active_days else 0 end as avg_fwd_session_cnt
		,fwd_level_end
		,bwd_active_days
		,case when bwd_active_days>0 then bwd_session_cnt/bwd_active_days else 0 end as avg_bwd_session_cnt
		,bwd_level_end
		from
		(
		select
		date
		,country
		,snsid
		,user_tag
		,is_payer
		,sum(case when date_diff>0 then 1 else 0 end) as fwd_active_days
		,sum(case when date_diff>0 then session_cnt else 0 end) as fwd_session_cnt
		,max(case when date_diff>0 then level_end else 0 end) as fwd_level_end
		,sum(case when date_diff<0 then 1 else 0 end) as bwd_active_days
		,sum(case when date_diff<0 then session_cnt else 0 end) as bwd_session_cnt
		,max(case when date_diff<0 then level_end else 0 end) as bwd_level_end
		from temp_table3
		group by 
		date
		,country
		,snsid
		,user_tag
		,is_payer
		) a
	) c
	group by 
	date
	,country
	,user_tag
)

select 
t1.date
,t1.country
,t1.obs as obs_ctr
,t1.retention_rate as retention_rate_ctr
,t1.avg_fwd_session_cnt as avg_fwd_session_cnt_ctr
,t1.avg_bwd_session_cnt as avg_bwd_session_cnt_ctr
,t1.avg_fwd_level_end as avg_fwd_level_end_ctr
,t1.avg_bwd_level_end as avg_bwd_level_end_ctr
,t1.diff_avg_session_cnt as diff_avg_session_cnt_ctr
,t1.diff_avg_level_end as diff_avg_level_end_ctr

,t2.obs as obs_test
,t2.retention_rate as retention_rate_test
,t2.avg_fwd_session_cnt as avg_fwd_session_cnt_test
,t2.avg_bwd_session_cnt as avg_bwd_session_cnt_test
,t2.avg_fwd_level_end as avg_fwd_level_end_test
,t2.avg_bwd_level_end as avg_bwd_level_end_test
,t2.diff_avg_session_cnt as diff_avg_session_cnt_test
,t2.diff_avg_level_end as diff_avg_level_end_test

, t2.retention_rate - t1.retention_rate as delta_retention_rate
, t2.diff_avg_session_cnt - t1.diff_avg_session_cnt as delta_session_cnt
, t2.diff_avg_level_end - t1.diff_avg_level_end as delta_level_end

from (select * from temp_table4 where user_tag='control') t1
left join (select * from temp_table4 where user_tag='test') t2
on t1.date = t2.date
and t1.country = t2.country
DROP TABLE IF EXISTS model.lost_user_model_msg_list_test_tango CASCADE;
CREATE TABLE model.lost_user_model_msg_list_test_tango
(
   date               date,
   app                varchar(64),
   country            varchar(64),
   snsid              varchar(64),
   user_tag           varchar(64),
   obs                integer
);
COMMIT;
DROP TABLE IF EXISTS model.lost_user_model_input_test_tango CASCADE;

CREATE TABLE model.lost_user_model_input_test_tango
(
   date               date,
   user_key           varchar(64),
   snsid              varchar(64),
   country            varchar(64),
   install_date       date,
   is_payer           smallint,
   days_to_install    integer,
   bw_days_14         integer,
   bw_days_7          integer,
   fd_wk              integer,
   bw_level_14        integer,
   bw_level_7         integer,
   bw_avg_sessions_14 bigint,
   bw_avg_sessions_7  bigint,
   bw_avg_playtime_14 bigint,
   bw_avg_playtime_7  bigint,
   bw_total_coins_in_14  bigint, 
   bw_total_coins_in_7   bigint,
   bw_total_coins_out_14 bigint,
   bw_total_coins_out_7  bigint,
   bw_total_rc_out_7  bigint,
   bw_total_rc_out_14 bigint,
   bw_total_rc_in_7   bigint,
   bw_total_rc_in_14  bigint

);

COMMIT;

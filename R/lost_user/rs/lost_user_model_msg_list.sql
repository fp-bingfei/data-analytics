delete from model.lost_user_model_msg_list_test where date='_YESTERDAY_'; 


drop table if exists temp_duplicate_user;
create temp table temp_duplicate_user as 
select snsid, count(1) as user_cnt 
from model.lost_user_model_output_test 
where date='_YESTERDAY_' 
	and prediction=0 group by 1;

insert into model.lost_user_model_msg_list_test

with temp_table_a as 
(
select date,snsid,country,user_tag,obs
from
	(
	select 
			a.date
			,a.snsid
			,a.country
			,c.date as date_pre
			,c.user_tag
			,c.obs
			,rank() over(partition by a.date,a.snsid order by c.date) as rk

	from model.lost_user_model_output_test a
	left join model.lost_user_model_msg_list_test c
	     on a.snsid = c.snsid 
	     and datediff('day',a.date,c.date)<0
	where a.date = '_YESTERDAY_'
	      and a.prediction=0
	      and a.snsid not in (
	      		select snsid from temp_duplicate_user where user_cnt > 1
	      	)
	)
	where rk = 1
	and country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
)

,temp_table_b as
(
select
date
,country
,snsid
,user_tag
,1 as obs
from temp_table_a
where ((user_tag='test1') or (user_tag='test2') or user_tag='control') -------already in experiment and within 7 days since last msg sent


union all

select ----those not in experiment
date
,country
,snsid
,case when rk<=cut_off then 'test1' 
      when rk > cut_off and rk <= cut_off * 2 then 'test2'
      else 'control' end as user_tag
,case when rk<=cut_off then 0
      when rk > cut_off and rk <= cut_off * 2 then 0
      else 1 end as obs
from
(
select 
a1.*
,rank() over(partition by date,country order by random_num) as rk
from
	(
	select 
		a.*
		,random() as random_num
		,round(snsid_cnt/3) as cut_off
	from temp_table_a a 
	left join 
		(select 
		 date,country,count(distinct snsid) as snsid_cnt 
		 from temp_table_a 
		 where (user_tag is null)
		 group by date,country
		 ) b 
		 on a.date = b.date 
		 and a.country = b.country 
		 where (user_tag is null)
	) a1
) a2
)
--- excluding users which might be assigned to both control and test
select
date,country,t1.snsid,user_tag,obs
from
	(select 
	date
	,country
	,snsid
	,user_tag
	,obs
	,count(*)
	from temp_table_b 
	group by 1,2,3,4,5
	) t1
left join 
	(select
		snsid
		,count(distinct user_tag) 
	 from model.lost_user_model_msg_list_test 
	 group by snsid 
	 having count(distinct user_tag)>1
	 ) t2
on t1.snsid = t2.snsid
where t2.snsid is null
;

update model.lost_user_model_msg_list_test
set obs = 1
from model.notification_events n
where model.lost_user_model_msg_list_test.date = '_YESTERDAY_'
	and date(n.ts_pretty) = '_YESTERDAY_'
	and model.lost_user_model_msg_list_test.snsid = json_extract_path_text(n.properties,'facebook_id')
	and json_extract_path_text(json_extract_path_text(n.properties,'d_c1'),'value') = 'click'
;

commit;

DELETE FROM model.lost_user_model_performance_test where date = DATEADD(DAY, -7, '_YESTERDAY_');
INSERT INTO model.lost_user_model_performance_test


select
	date,country,obs
	,case when actual_0>0 then cast(correct_0 as float)/cast(actual_0 as float) else 0 end as recall
	,case when predict_0>0 then cast(correct_0 as float)/cast(predict_0 as float) else 0 end as precision
	,0.4 as cutoff
from
(
select
date
,country
,sum(1) as obs
,sum(case when actual=0 and prediction=0 then 1 else 0 end) as correct_0
,sum(case when actual=0 then 1 else 0 end) as actual_0
,sum(case when prediction=0 then 1 else 0 end) as predict_0
from
	(
	select 
	e.*
	,case when e.fd_wk>3 then 1 else 0 end as actual	
	,f.prediction
	from
		(
			select
			date
			,snsid
			,country
			,sum(case when diff>0 then 1 else 0 end) as fd_wk
			from
				(
					select 
					a.*
					, datediff('day', a.date, c.date) as diff
					from
							(select 
							*
							from processed.fact_dau_snapshot 
							where 
							date=DATEADD(DAY, -7, '_YESTERDAY_')  
							and country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
							) a
						LEFT JOIN
							(select 
							date
							, user_key 
							from processed.fact_dau_snapshot
							where country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
							) c
					on datediff('day', a.date, c.date)<=7 ----for future 7 days
					and datediff('day', a.date, c.date)>0
					and a.user_key = c.user_key
				) d
			group by 
			date
			,snsid
			,country
		) e ----actual data	
	left join 
		(
			select 
			date
			,snsid
			,prediction
			from model.lost_user_model_output_test
			where date=DATEADD(DAY, -7, '_YESTERDAY_') ---- yesterday-7
		) f 
	on e.date=f.date and e.snsid=f.snsid
	) g
group by date,country
) h
where predict_0 >0 
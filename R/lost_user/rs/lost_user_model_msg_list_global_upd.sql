delete from model.lost_user_model_msg_list_test_global where date='2016-06-15'; 

insert into model.lost_user_model_msg_list_test_global

with temp_table_a as 
(
select date,snsid,country,app,user_tag,obs
from
	(
	select 
			a.date
			,a.snsid
			,a.country
			,b.app
			,c.date as date_pre
			,c.user_tag
			,c.obs
			,rank() over(partition by a.date,a.snsid order by c.date) as rk

	from model.lost_user_model_output_test_global a
	left join processed.dim_user b 
	     on a.snsid = b.snsid
	left join model.lost_user_model_msg_list_test_global c
	     on a.snsid = c.snsid 
	     and datediff('day',a.date,c.date)<0
	where a.date = '2016-06-15'
	      and a.prediction=0
	)
	where rk = 1
	and app='ffs.global.prod'
	--and country = 'Germany'
)

,temp_table_b as
(
select
date
,app
,country
,snsid
,user_tag
,1 as obs
from temp_table_a
where ((user_tag='test1') or (user_tag='test2') or (user_tag='test3') or user_tag='control') -------already in experiment and within 7 days since last msg sent


union all

select ----those not in experiment
date
,app
,country
,snsid
,case when rk<=cut_off then 'test1' 
      when rk > cut_off and rk <= cut_off * 2 then 'test2'
      when rk > cut_off * 2 and rk <= cut_off * 3 then 'test3'
      else 'control' end as user_tag
,case when rk<=cut_off then 0
      when rk > cut_off and rk <= cut_off * 2 then 0
      when rk > cut_off * 2 and rk <= cut_off * 3 then 0
      else 1 end as obs
from
(
select 
a1.*
,rank() over(partition by date,country,app order by random_num) as rk
from
	(
	select 
		a.*
		,random() as random_num
		,round(snsid_cnt/4) as cut_off
	from temp_table_a a 
	left join 
		(select 
		 date,app,country,count(distinct snsid) as snsid_cnt 
		 from temp_table_a 
		 where (user_tag is null)
		 group by date,app,country
		 ) b 
		 on a.date = b.date 
		 and a.country = b.country 
		 and a.app = b.app
		 where (user_tag is null)
	) a1
) a2
)
--- excluding users which might be assigned to both control and test
select
date,app,country,t1.snsid,user_tag,obs
from
	(select 
	date
	,app
	,country
	,snsid
	,user_tag
	,obs
	,count(*)
	from temp_table_b 
	group by 1,2,3,4,5,6
	) t1
left join 
	(select 
		snsid
		,count(distinct user_tag) 
	 from model.lost_user_model_msg_list_test_global 
	 group by snsid 
	 having count(distinct user_tag)>1
	 ) t2
on t1.snsid = t2.snsid
where t2.snsid is null
;
commit;

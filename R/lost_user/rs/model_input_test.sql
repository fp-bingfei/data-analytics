DELETE FROM model.lost_user_model_input_test;
INSERT INTO model.lost_user_model_input_test

with users as 
(
select 
country,avg(users) as users
from
	(
	select 
		country, date,count(distinct user_key) as users
		from processed.fact_dau_snapshot
		where 
		date>=DATEADD(DAY, -7, '_YESTERDAY_')    -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
		and date<'_YESTERDAY_'
		and app in ('royal.de.prod','royal.us.prod','royal.fr.prod','royal.ae.prod','royal.nl.prod')
		and country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
		group by country,date
	)
group by country
	--having avg(users) >=2000-- only include country with avg dau>=2000

)
,coins as 
(
select 
	date
	,user_key
	,app
	,country
	,sum(coins_in) as coins_in
	,sum(coins_out) as coins_out
from processed.coins_transaction_user
where 
	app in ('royal.de.prod','royal.us.prod','royal.fr.prod','royal.ae.prod','royal.nl.prod')
	and country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
	and date < '_YESTERDAY_'
	and date >= DATEADD(DAY, -13-14, '_YESTERDAY_')
group by 
	date
	,user_key
	,app
	,country
)
,rc as 
(
select 
	date
	,user_key
	,app
	,country
	,sum(rc_in) as rc_in
	,sum(rc_out) as rc_out
from processed.rc_transaction_user
where 
	app in ('royal.de.prod','royal.us.prod','royal.fr.prod','royal.ae.prod','royal.nl.prod')
	and country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
	and date < '_YESTERDAY_'
	and date >= DATEADD(DAY, -13-14, '_YESTERDAY_')
group by 
	date
	,user_key
	,app
	,country
)
, temp_table as 
(
select 
date, user_key, snsid, country, install_date,is_payer,days_to_install
,bw_days_14, bw_days_7, fd_wk
,case when bw_level_14 is null and bw_level_start_7 is not null then bw_level_start_7 
      when bw_level_14 is null and bw_level_start_7 is null then 1 else bw_level_14 end as bw_level_14
,case when bw_level_7 is null and bw_level_start_7 is not null then bw_level_start_7 
      when bw_level_7 is null and bw_level_start_7 is null then 1 else bw_level_7 end as bw_level_7
,bw_avg_sessions_14, bw_avg_sessions_7
,bw_avg_playtime_14, bw_avg_playtime_7
,bw_total_coins_in_14, bw_total_coins_in_7
,bw_total_coins_out_14, bw_total_coins_out_7
,bw_total_rc_in_7, bw_total_rc_in_14
,bw_total_rc_out_7, bw_total_rc_out_14
from
               (
                   select
						date
						,user_key
						,snsid
						,country
						,install_date
						,is_payer
						,days_to_install
						-- active days
						,count(distinct case when diff<-7 then date2 else null end) as bw_days_14
						,count(distinct case when diff<=0 and diff>=-7 then date2 else null end) as bw_days_7
						,count(distinct case when diff>0 then date2 else null end) as fd_wk
						-- maximum level
						,max(case when diff<-7 then level_end2 else null end) as bw_level_14
						,max(case when diff<=0 and diff>=-7 then level_end2 else null end) as bw_level_7
						,min(case when diff<=0 then level_start2 else null end) as bw_level_start_7
                        -- average daily sessions
						,case when count(distinct case when diff<-7 then date2 else null end) >0 then 
						      sum(case when diff<-7 then session_cnt2 else 0 end) * 1.00/count(distinct case when diff<-7 then date2 else null end)
						      else 0 end as bw_avg_sessions_14
						,case when count(distinct case when diff<=0 and diff>=-7 then date2 else null end) >0 then 
						      sum(case when diff<=0 and diff>=-7 then session_cnt2 else 0 end) * 1.00/count(distinct case when diff<=0 and diff>=-7 then date2 else null end)
						      else 0 end as bw_avg_sessions_7

                        -- average daily playtime seconds
						,case when count(distinct case when diff<-7 then date2 else null end) >0 then 
						      sum(case when diff<-7 then playtime2 else 0 end) * 1.00/count(distinct case when diff<-7 then date2 else null end)
						      else 0 end as bw_avg_playtime_14
						,case when count(distinct case when diff<=0 and diff>=-7 then date2 else null end) >0 then 
						      sum(case when diff<=0 and diff>=-7 then playtime2 else 0 end) * 1.00/count(distinct case when diff<=0 and diff>=-7 then date2 else null end)
						      else 0 end as bw_avg_playtime_7
						-- total coins in/out

						,sum(case when diff<-7 then coins_in2 else 0 end) as bw_total_coins_in_14
						,sum(case when diff<=0 and diff>=-7 then coins_in2 else 0 end) as bw_total_coins_in_7
						,sum(case when diff<-7 then coins_out2 else 0 end) as bw_total_coins_out_14
						,sum(case when diff<=0 and diff>=-7 then coins_out2 else 0 end) as bw_total_coins_out_7

						-- total rc in/out
						,sum(case when diff<-7 then rc_in2 else 0 end) as bw_total_rc_in_14
						,sum(case when diff<=0 and diff>=-7 then rc_in2 else 0 end) as bw_total_rc_in_7
						,sum(case when diff<-7 then rc_out2 else 0 end) as bw_total_rc_out_14
						,sum(case when diff<=0 and diff>=-7 then rc_out2 else 0 end) as bw_total_rc_out_7

						from
							(
							select 
							a.*
							, datediff('day', a.date, c.date) as diff
							, a.date - a.install_date as days_to_install
							,c.date as date2
							,c.level_start as level_start2
							,c.level_end as level_end2
							,c.session_cnt as session_cnt2
							,c.playtime as playtime2
							,c.coins_in as coins_in2
							,c.coins_out as coins_out2
							,c.rc_in as rc_in2
							,c.rc_out as rc_out2
							from
								(select 
									date
									,user_key
									,snsid
									,p1.country
									,install_date
									,is_payer
									,min(level_start) as level_start
									,max(level_end) as level_end
									,sum(session_cnt) as session_cnt
									,sum(playtime_sec) as playtime
									--,sum(coins_in) as coins_in
									--,sum(rc_in) as rc_in
								from processed.fact_dau_snapshot p1
								join users p2
								   on p1.country = p2.country
								where 
									((
									date>=DATEADD(DAY, -13, '_YESTERDAY_')    -----test_date -13, assuming we are do predictions for DAU on 2015-10-01, 
									and date<=DATEADD(DAY, -7, '_YESTERDAY_') 
									) -- test_date -7
									or
									date='_YESTERDAY_'  
									)                     -- test_date
									--and country in ('Germany') 
									--and level_end>3
	                            group by 
	                                date
									,user_key
									,snsid
									,p1.country
									,install_date
									,is_payer
								) a
							LEFT JOIN
							 (
							    select b1.*
							    ,case when coins.coins_in is null then 0 else coins_in end as coins_in
							    ,case when coins.coins_out is null then 0 else coins_out end as coins_out
							    ,case when rc.rc_in is null then 0 else rc.rc_in end as rc_in
							    ,case when rc.rc_out is null then 0 else rc.rc_out end as rc_out
							    from
									(select 
										date
									    , user_key 
									    , min(level_start) as level_start
									    , max(level_end) as level_end
									    , sum(session_cnt) as session_cnt
									    , sum(case when playtime_sec is null then 0 else playtime_sec end) as playtime
									    
									from processed.fact_dau_snapshot
									where 
									--country in ('Germany') 
									app in ('royal.de.prod','royal.us.prod','royal.fr.prod','royal.ae.prod','royal.nl.prod')
			                        and country in ('United States','France','Brazil','Germany','Egypt','Turkey','Italy','Spain','United Kingdom','Netherlands')
			                        and date <=DATEADD(DAY, 7, '_YESTERDAY_')
			                        and date >= DATEADD(DAY, -13-14, '_YESTERDAY_')
									group by 
									date, user_key
									) b1
								left join coins 
									on b1.user_key = coins.user_key
									and b1.date = coins.date
								left join rc
									on b1.user_key = rc.user_key
									and b1.date = rc.date
	                         ) c
							on datediff('day', a.date, c.date)<=7 ----change it for future n days
							and datediff('day', a.date, c.date)>=-14
							and a.user_key = c.user_key

		                ) d
						group by 
							date
							,user_key
							,snsid
							,country
							,install_date
							,is_payer
							,days_to_install
                   )
) 

select 
t1.*
from temp_table t1
where bw_level_7 > 3




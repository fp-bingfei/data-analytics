DELETE FROM model.ffs_message_open;

INSERT INTO model.ffs_message_open

with msg_open as 
(
	select 
	case when country_code in ('BR','EG','FR','DE','PL','RO','RU','SA','TH','TR','US','NL') then country_code
	     else 'others' end as country_code
	,msg_id
	,action
	,date
	,snsid
	from
	(select 
		trunc(ts) as date
		,country_code
		,nullif(json_extract_path_text(properties,'msg_id'),'') as msg_id
	    ,nullif(json_extract_path_text(properties,'action'),'') as action
	    ,snsid
	from public.events_raw
	where ts> '2016-01-11'  -- start since 2016/01/12
	and event='sys_msg' 
	and nullif(json_extract_path_text(properties,'msg_id'),'') = 'system_message_op_title2'
	)
) 
,msg_sent_global as
(
select 
	case when country='Brazil' then 'BR'
	     when country='Egypt'  then 'EG'
	     when country='France' then 'FR'
	     when country='Germany' then 'DE'
	     when country='Poland' then 'PL'
	     when country='Romania' then 'RO'
	     when country='Russian Federation' then 'RU'
	     when country='Saudi Arabia' then 'SA'
	     when country='Thailand' then 'TH'
	     When country='Turkey' then 'TR'
	     when country='United States' then 'US'
	     when country='Netherlands' then 'NL'
	     else 'others' end as country_code
	,country
	,date
	,snsid
from model.lost_user_model_msg_list_2
where 
	date>'2016-01-11' 
	and user_tag='test'
	and obs=1
) 
,msg_sent_tango as
(
select 
	case when country='Brazil' then 'BR'
	     when country='Egypt'  then 'EG'
	     when country='France' then 'FR'
	     when country='Germany' then 'DE'
	     when country='Poland' then 'PL'
	     when country='Romania' then 'RO'
	     when country='Russian Federation' then 'RU'
	     when country='Saudi Arabia' then 'SA'
	     when country='Thailand' then 'TH'
	     When country='Turkey' then 'TR'
	     when country='United States' then 'US'
	     when country='Netherlands' then 'NL'
	     else 'others' end as country_code
	,country
	,date
	,snsid
from model.lost_user_model_msg_list
where 
	date>'2016-01-11' 
	and user_tag='test'
	and obs=1
) 

select
t1.country
,t1.country_code
,t1.app
,t1.date as sent_date
,t2.date as open_date
,t1.snsid 
from
	(
	select a.*, 'global' as app
	from msg_sent_global a
	union all
	select b.*, 'tango' as app
	from msg_sent_tango b
	) t1
left join msg_open t2
on t1.snsid = t2.snsid
;
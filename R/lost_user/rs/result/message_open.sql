DELETE FROM model.message_open;

INSERT INTO model.message_open

with msg_open as 
(
	select
		c.country,
		json_extract_path_text(e.properties,'country_code') as country_code,
		-- to_date(json_extract_path_text(json_extract_path_text(e.properties,'d_c2'),'value'), 'YYYY-MM-DD') as sent_date,
		date(e.ts_pretty) as date,
		json_extract_path_text(properties,'facebook_id') as snsid
	from model.notification_events e, processed.dim_country c
	where   
		json_extract_path_text(json_extract_path_text(properties,'d_c1'),'value')='click'
		and json_extract_path_text(e.properties,'country_code')<>''
		and json_extract_path_text(e.properties,'country_code')=c.country_code
) 
,msg_sent_global as
(
	select 
	 	t.country,
	 	c.country_code,
	 	date,
	 	snsid,
	 	user_tag
	from model.lost_user_model_msg_list_test t, processed.dim_country c
	where t.country = c.country
		and (user_tag ='test1' or user_tag='test2')
		and obs=0

) 

select
t1.country
,t1.country_code
,t1.date as sent_date
,t2.date as open_date
,t1.snsid 
,t1.user_tag
from
	(
	select *
	from msg_sent_global
	) t1
left join msg_open t2
on t1.snsid = t2.snsid
;
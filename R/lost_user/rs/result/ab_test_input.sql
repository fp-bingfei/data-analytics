DELETE FROM model.ab_test_input;
INSERT INTO model.ab_test_input


select 
t1.date
,t1.country
,t1.time_window
,t1.obs_test
,t2.obs_ctr
,t1.retention_rate_test_ab
,t2.retention_rate_ctr_ab
,t3.retention_rate_test_aa
,t4.retention_rate_ctr_aa
,t1.retention_rate_test_ab - t3.retention_rate_test_aa as delta_retention_rate_test
,t2.retention_rate_ctr_ab - t4.retention_rate_ctr_aa as delta_retention_rate_ctr
,t1.delta_active_days_test
,t2.delta_active_days_ctr
,t1.delta_session_cnt_test
,t2.delta_session_cnt_ctr
,t1.fwd_payer_pct_test
,t1.bwd_payer_pct_test
,t1.fwd_arppu_test
,t1.bwd_arppu_test
,t2.fwd_payer_pct_ctr
,t2.bwd_payer_pct_ctr
,t2.fwd_arppu_ctr
,t2.bwd_arppu_ctr


from
	(
	select
		date
		,country
		,time_window
		,count(distinct snsid) as obs_test
		,count(distinct case when retain='retained' then snsid else null end)*1.00/count(distinct snsid) as retention_rate_test_ab
		,avg(fwd_active_days - bwd_active_days) as delta_active_days_test
		,avg(avg_fwd_session_cnt - avg_bwd_session_cnt) as delta_session_cnt_test


		,count(distinct fwd_payers)*1.00/count(distinct snsid) as fwd_payer_pct_test
        ,count(distinct bwd_payers)*1.00/count(distinct snsid) as bwd_payer_pct_test
        ,case when count(distinct fwd_payers)>0 then sum(fwd_revenue)*1.00/count(distinct fwd_payers) else 0 end as fwd_arppu_test
        ,case when count(distinct bwd_payers)>0 then sum(bwd_revenue)*1.00/count(distinct bwd_payers) else 0 end as bwd_arppu_test
	from model.ab_test_metrics
	where (user_tag='test1' or user_tag='test2')
	group by 
		date
		,country
		,time_window
	) t1
left join
	(
	select
		date
		,country
		,time_window
		,count(distinct snsid) as obs_ctr
		,count(distinct case when retain='retained' then snsid else null end)*1.00/count(distinct snsid) as retention_rate_ctr_ab
		,avg(fwd_active_days - bwd_active_days) as delta_active_days_ctr
		,avg(avg_fwd_session_cnt - avg_bwd_session_cnt) as delta_session_cnt_ctr

		,count(distinct fwd_payers)*1.00/count(distinct snsid) as fwd_payer_pct_ctr
        ,count(distinct bwd_payers)*1.00/count(distinct snsid) as bwd_payer_pct_ctr
        ,case when count(distinct fwd_payers)>0 then sum(fwd_revenue)*1.00/count(distinct fwd_payers) else 0 end as fwd_arppu_ctr
        ,case when count(distinct bwd_payers)>0 then sum(bwd_revenue)*1.00/count(distinct bwd_payers) else 0 end as bwd_arppu_ctr
	from model.ab_test_metrics
	where user_tag='control'
	group by 
		date
		,country
		,time_window
	) t2
	on t1.date = t2.date
	and t1.country = t2.country
	and t1.time_window = t2.time_window
left join
	(
	select
		date
		,country
		,time_window
		,count(distinct snsid) as obs_test
		,count(distinct case when retain='retained' then snsid else null end)*1.00/count(distinct snsid) as retention_rate_test_aa
	from model.aa_test_metrics
	where (user_tag='test1' or user_tag='test2')
	group by 
		date
		,country
		,time_window
	) t3
	on t1.date = t3.date
	and t1.country = t3.country
	and t1.time_window = t3.time_window
left join
	(
	select
		date
		,country
		,time_window
		,count(distinct snsid) as obs_test
		,count(distinct case when retain='retained' then snsid else null end)*1.00/count(distinct snsid) as retention_rate_ctr_aa
	from model.aa_test_metrics
	where user_tag='control'
	group by 
		date
		,country
		,time_window
	) t4
	on t1.date = t4.date
	and t1.country = t4.country
	and t1.time_window = t4.time_window
;
-- Delete existing records
delete from model.marketing_ltv_accuracy_report where install_date='_YESTERDAY_';

-- Insert records
insert into model.marketing_ltv_accuracy_report

select 
  a.*
  ,b.revenue as actual_revenue
from

  (select *
  from model.marketing_ltv_report where install_date='_YESTERDAY_'
--  where install_source='Organic'
--  and os='Android'
 -- and country='Germany'
 -- and install_date='2015-03-20'
  ) a
left join
  (select * from model.marketing_ltv_report where metrics='actual' and install_date='_YESTERDAY_') b
on a.install_source=b.install_source
and a.install_date=b.install_date
and a.campaign = b.campaign
and a.sub_publisher=b.sub_publisher
and a.country = b.country
and a.os=b.os
and a.ltv_day = b.ltv_day
;

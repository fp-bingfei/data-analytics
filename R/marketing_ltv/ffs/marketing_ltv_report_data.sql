-- Delete existing records
delete from model.marketing_ltv_report where install_date='_YESTERDAY_';

-- Insert records
insert into model.marketing_ltv_report

----------------------ACTUAL------------------------
select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'actual' as metrics,
	7 as ltv_day,
	new_installs,
	d7_revenue as revenue,
	d7_revenue/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d30
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'actual' as metrics,
	30 as ltv_day,
	new_installs,
	d30_revenue as revenue,
	d30_revenue/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d60
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'actual' as metrics,
	60 as ltv_day,
	new_installs,
	d60_revenue as revenue,
	d60_revenue/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d90
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'actual' as metrics,
	90 as ltv_day,
	new_installs,
	d90_revenue as revenue,
	d90_revenue/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d120
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'actual' as metrics,
	120 as ltv_day,
	new_installs,
	d120_revenue as revenue,
	d120_revenue/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d150
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'actual' as metrics,
	150 as ltv_day,
	new_installs,
	d150_revenue as revenue,
	d150_revenue/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
	
----------------------PREDICTION-------------------------------------
--- d30
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'prediction' as metrics,
	30 as ltv_day,
	new_installs,
	p30 as revenue,
	p30/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d60
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'prediction' as metrics,
	60 as ltv_day,
	new_installs,
	p60 as revenue,
	p60/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d90
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'prediction' as metrics,
	90 as ltv_day,
	new_installs,
	p90 as revenue,
	p90/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d120
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'prediction' as metrics,
	120 as ltv_day,
	new_installs,
	p120 as revenue,
	p120/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d150
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'prediction' as metrics,
	150 as ltv_day,
	new_installs,
	p150 as revenue,
	p150/new_installs as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'

----------------------SIMPLE AVERAGE - INSTALL SOURCE-------------------------------------
--- d30
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'SA_InstallSource' as metrics,
	30 as ltv_day,
	s30 as new_installs,
	a30 as revenue,
	a30/s30 as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d60
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'SA_InstallSource' as metrics,
	60 as ltv_day,
	s60 as new_installs,
	a60 as revenue,
	a60/s60 as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d90
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'SA_InstallSource' as metrics,
	90 as ltv_day,
	s90 as new_installs,
	a90 as revenue,
	a90/s90 as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d120
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'SA_InstallSource' as metrics,
	120 as ltv_day,
	s120 as new_installs,
	a120 as revenue,
	a120/s120 as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'
--- d150
union all

select
	install_date,
	install_source,
	' ' as campaign,
	' ' as sub_publisher,
	country,
	os,
	'SA_InstallSource' as metrics,
	150 as ltv_day,
	s150 as new_installs,
	a150 as revenue,
	a150/s150 as ltv
from model.marketing_ltv where install_date='_YESTERDAY_'
--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony'

----------------------SIMPLE AVERAGE - CAMPAIGN-------------------------------------
----- d30
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	' ' as sub_publisher,
	a.country,
	a.os,
	'SA_Campaign' as metrics,
	'30' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d30_revenue) as revenue,
	sum(b.d30_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,' ' as sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            group by 
            install_date
          	,install_source
          	,campaign
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d30_revenue) as d30_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+30 
          and datediff(day,b.install_date,a.install_date)>=30 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.country,
	a.os
----- d60
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	' ' as sub_publisher,
	a.country,
	a.os,
	'SA_Campaign' as metrics,
	'60' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d60_revenue) as revenue,
	sum(b.d60_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,' ' as sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            group by 
            install_date
          	,install_source
          	,campaign
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d60_revenue) as d60_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+60 
          and datediff(day,b.install_date,a.install_date)>=60 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.country,
	a.os

----- d90
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	' ' as sub_publisher,
	a.country,
	a.os,
	'SA_Campaign' as metrics,
	'90' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d90_revenue) as revenue,
	sum(b.d90_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,' ' as sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            group by 
            install_date
          	,install_source
          	,campaign
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d90_revenue) as d90_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+90 
          and datediff(day,b.install_date,a.install_date)>=90 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.country,
	a.os
----- d120
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	' ' as sub_publisher,
	a.country,
	a.os,
	'SA_Campaign' as metrics,
	'120' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d120_revenue) as revenue,
	sum(b.d120_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,' ' as sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            group by 
            install_date
          	,install_source
          	,campaign
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d120_revenue) as d120_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+120 
          and datediff(day,b.install_date,a.install_date)>=120 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.country,
	a.os
----- d150
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	' ' as sub_publisher,
	a.country,
	a.os,
	'SA_Campaign' as metrics,
	'150' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d150_revenue) as revenue,
	sum(b.d150_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,' ' as sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            group by 
            install_date
          	,install_source
          	,campaign
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d150_revenue) as d150_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+150 
          and datediff(day,b.install_date,a.install_date)>=150 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.country,
	a.os
----------------------SIMPLE AVERAGE - SUB PUBLISHER-------------------------------------

----- d30
union all
select 
	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os,
	'SA_SubPublisher' as metrics,
	'30' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d30_revenue) as revenue,
	sum(b.d30_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            --and sub_publisher='c61a292e6b2d82f0978f4d8dcc766de9f55fe112'
            group by 
            install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d30_revenue) as d30_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+30 
          and datediff(day,b.install_date,a.install_date)>=30 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.sub_publisher = b.sub_publisher
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os
----- d60
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os,
	'SA_SubPublisher' as metrics,
	'60' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d60_revenue) as revenue,
	sum(b.d60_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            --and sub_publisher='c61a292e6b2d82f0978f4d8dcc766de9f55fe112'
            group by 
            install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d60_revenue) as d60_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+60 
          and datediff(day,b.install_date,a.install_date)>=60 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.sub_publisher = b.sub_publisher
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os
----- d90
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os,
	'SA_SubPublisher' as metrics,
	'90' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d90_revenue) as revenue,
	sum(b.d90_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            --and sub_publisher='c61a292e6b2d82f0978f4d8dcc766de9f55fe112'
            group by 
            install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d90_revenue) as d90_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+90 
          and datediff(day,b.install_date,a.install_date)>=90 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.sub_publisher = b.sub_publisher
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os
----- d120
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os,
	'SA_SubPublisher' as metrics,
	'120' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d120_revenue) as revenue,
	sum(b.d120_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            --and sub_publisher='c61a292e6b2d82f0978f4d8dcc766de9f55fe112'
            group by 
            install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d120_revenue) as d120_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+120 
          and datediff(day,b.install_date,a.install_date)>=120 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.sub_publisher = b.sub_publisher
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os
----- d150
union all

select 
	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os,
	'SA_SubPublisher' as metrics,
	'150' as ltv_day,
	sum(b.new_installs) as new_installs,
	sum(b.d150_revenue) as revenue,
	sum(b.d150_revenue)/sum(b.new_installs) as ltv
from
          (select 
          	install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	,count(1)
          	from processed.tab_marketing_kpi where install_date='_YESTERDAY_'
          	--where install_date='2015-03-20' and country='Germany' and os='Android' and install_source='AdColony' and campaign='FFS_DE_AND_ADC'
            --and sub_publisher='c61a292e6b2d82f0978f4d8dcc766de9f55fe112'
            group by 
            install_date
          	,install_source
          	,campaign
          	,sub_publisher
          	,country
          	,os
          	) a
left join  
          (select 
	           install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os,
	           sum(new_installs) as new_installs,
	           sum(d150_revenue) as d150_revenue
           from processed.tab_marketing_kpi
           group by 
               install_date,
	           install_source,
	           campaign,
	           sub_publisher,
	           country,
	           os

           ) b
          on datediff(day,b.install_date,a.install_date)<90+150 
          and datediff(day,b.install_date,a.install_date)>=150 
          and a.install_source = b.install_source
          and a.campaign = b.campaign
          and a.sub_publisher = b.sub_publisher
          and a.country = b.country
          and a.os = b.os
 group by 
 	a.install_date,
	a.install_source,
	a.campaign,
	a.sub_publisher,
	a.country,
	a.os
;
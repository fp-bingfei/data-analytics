DROP TABLE IF EXISTS model.marketing_ltv_input_ha CASCADE;
CREATE TABLE model.marketing_ltv_input_ha
(
   app                varchar(64),
   install_date       date,
   install_source     varchar(128),
   country            varchar(64),
   os                 varchar(32),
   new_installs       integer          DEFAULT 0,
   d7_revenue         numeric(14,4)    DEFAULT 0,
   d30_revenue        numeric(14,4)    DEFAULT 0,
   d60_revenue        numeric(14,4)    DEFAULT 0,
   d90_revenue        numeric(14,4)    DEFAULT 0,
   d120_revenue       numeric(14,4)    DEFAULT 0,
   --d150_revenue       numeric(14,4)    DEFAULT 0,
   d7_retained        integer          DEFAULT 0
);

COMMIT;

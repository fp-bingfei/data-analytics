DROP TABLE IF EXISTS model.marketing_ltv_accuracy_report_ha CASCADE;
CREATE TABLE model.marketing_ltv_accuracy_report_ha
(
   app                varchar(64),
   install_date       date,
   install_source     varchar(128),
   campaign           varchar(1024),
   sub_publisher      varchar(512),
   country            varchar(64),
   os                 varchar(32),
   metrics            varchar(64),
   ltv_day            integer          DEFAULT 0,
   new_installs       numeric(14,4)    DEFAULT 0,
   revenue            numeric(14,4)    DEFAULT 0,
   ltv                numeric(14,4)    DEFAULT 0,
   actual_revenue     numeric(14,4)    DEFAULT 0

);

COMMIT;
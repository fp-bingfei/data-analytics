DELETE FROM model.marketing_ltv_base;
INSERT INTO model.marketing_ltv_base

with temp as (
select * from
	(
	select *, rank() over(partition by app,install_source_group,country,os order by install_month desc) as rk
	from
		(
		select 
			to_char(date_trunc('month',install_date),'yyyy-mm-dd') as install_month
			,app
			--install_source_group for facebook games
			,case when (app like 'farm%' or app like 'ha%' or app like  'royal%') then install_source_group else install_source end as install_source_group
			,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_payers)*1.00/sum(new_installs) as d7_pr
			,sum(d30_payers)*1.00/sum(new_installs) as d30_pr
			,sum(d60_payers)*1.00/sum(new_installs) as d60_pr
			,sum(d90_payers)*1.00/sum(new_installs) as d90_pr
			,sum(d120_payers)*1.00/sum(new_installs) as d120_pr
			,case when sum(d7_payers)>0 then sum(d7_revenue)*1.00/sum(d7_payers) else 0 end as d7_arppu
			,case when sum(d30_payers)>0 then sum(d30_revenue)*1.00/sum(d30_payers) else 0 end as d30_arppu
			,case when sum(d60_payers)>0 then sum(d60_revenue)*1.00/sum(d60_payers) else 0 end as d60_arppu
			,case when sum(d90_payers)>0 then sum(d90_revenue)*1.00/sum(d90_payers) else 0 end as d90_arppu
			,case when sum(d120_payers)>0 then sum(d120_revenue)*1.00/sum(d120_payers) else 0 end as d120_arppu
			,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
			,sum(d30_revenue)*1.00/sum(new_installs) as d30_ltv
			,sum(d60_revenue)*1.00/sum(new_installs) as d60_ltv
			,sum(d90_revenue)*1.00/sum(new_installs) as d90_ltv
			,sum(d120_revenue)*1.00/sum(new_installs) as d120_ltv
		from kpi_processed.agg_marketing_kpi
		where 
		   (
			install_date < dateadd(day, -60, '_YESTERDAY_') 
		   )
		   and (country is not null and country <>'' and country <>'Unknown')
		   and (os is not null and os !='Unknown' and os <>'')
		   --and app = 'battlewarship.global.prod'
			--and install_source = 'Facebook Installs'
			--and country in ('Germany','United States')
		group by 1,2,3,4,5
		having sum(new_installs)>=1000 and sum(d7_payers)>=10 -- only predict ltv for channels with monthly installs over 1000
		)
	)
where rk = 1
)

select app,install_source_group,country,os, 7 as ltv_day, d7_pr as pay_rate, d7_arppu as arppu, d7_ltv as ltv
from temp

union all

select app,install_source_group,country,os, 30 as ltv_day, d30_pr as pay_rate, d30_arppu as arppu, d30_ltv as ltv
from temp

union all

select app,install_source_group,country,os, 60 as ltv_day, d60_pr as pay_rate, d60_arppu as arppu, d60_ltv as ltv
from temp

union all

select app,install_source_group,country,os, 90 as ltv_day, d90_pr as pay_rate, d90_arppu as arppu, d90_ltv as ltv
from temp

union all

select app,install_source_group,country,os, 120 as ltv_day, d120_pr as pay_rate, d120_arppu as arppu, d120_ltv as ltv
from temp

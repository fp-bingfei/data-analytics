# Marketing LTV Prediction for all games
# Creation date: 2016-05-11
# Get parameter
argv <- commandArgs(TRUE)
# Check length
if(length(argv) < 1)
  q()
# Get the date of yesterday
yesterday <- as.Date(argv[1])

# Load SparkR library
library(SparkR)

# Init Spark context and SQL context
sc <- sparkR.init()
sqlContext <- sparkRSQL.init(sc)

# import data from redshift 
raw_data<-loadDF(sqlContext, source="jdbc", driver="com.amazon.redshift.jdbc41.Driver"
                 , url="jdbc:redshift://kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true"
                 , dbtable="model.marketing_ltv_input_all")
raw_data <- collect(raw_data)

ltv_base <- loadDF(sqlContext, source="jdbc", driver="com.amazon.redshift.jdbc41.Driver"
                 , url="jdbc:redshift://kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true"
                 , dbtable="model.marketing_ltv_base") # LTV assumption based on experience
ltv_base <- collect(ltv_base)
############################################
# non linear function for ltv and ltv days #
############################################
ltvF <- function(x, a, b){
  exp(a + b * x)
}
################################################
# estimate coefficients of non linear function #
################################################
LtvPrediction <- function(test_data, train_data, ltv_day_0, ltv_day, adjust_number){
  # test_data: ltv of ltv_day_0
  # ltv_day_0: previous ltv day, e.g 7
  # ltv_day: ltv day to predit, e.g. 30
  model <- nls(ltv ~ ltvF(ltv_day, a, b), train_data, start = list(a=0, b=0))
  param_1 <- base::summary(model)$coefficients[1] #intercept
  param_2 <- base::summary(model)$coefficients[2] #coefficient of ltv day
  pred <- exp(param_1 + param_2 * ltv_day_0) # prediction for d7 ltv
  delta <- log(test_data) - log(pred) # gap btw prediction and actual for d7 ltv
  param_1.new <- param_1 + delta * adjust_number   # adjusted model based on prediction accuracy of d7 ltv
  pred <- exp(param_1.new + param_2 * ltv_day)
  pred
}
# data layers
input <- raw_data
input$install_month <- as.Date(input$install_month)
input$ltv <- unlist(input$ltv)
input <- input[input$install_month<yesterday, ]

app <- split(input, input$app)
install_source_group <- split(input, input$install_source_group)
country <- split(input, input$country)
os <- split(input, input$os)
# loop in data layers
output <- data.frame(install_month="", app = "", install_source_group = "", country = "", os = ""
                     ,new_installs = 0, ltv_day = 0, ltv_prediction = 0)

for (n.app in app){
  input.app <- input[input$app == n.app$app[1], ]
  base.app <- ltv_base[ltv_base$app == n.app$app[1], ]
    for (n.source in install_source_group){
       input.source  <- input.app[input.app$install_source_group == n.source$install_source_group[1], ]
       base.source <- base.app[base.app$install_source_group == n.source$install_source_group[1], ]
        for(n.country in country){
          input.country <- input.source[input.source$country == n.country$country[1], ]
           for(n.os in os){
             input.sub <- input.country[input.country$os == n.os$os[1], ]
             base.sub <- base.source[base.source$os == n.os$os[1], ]
            ################
            # testing code #
            ################
            # data for training and testing 
            # input.sub <- input[input$install_source_group == 'Google Adwords'&input$os =='ffs.global.android'&input$country=='Germany'&input$install_month<as.Date('2016-01-01'), ]
            
            # model <- nls(ltv ~ ltvF(ltv_day, a, b), train.30, start = list(a=0, b=0))
            # param_1 <- summary(model)$coefficients[1] #intercept
            # param_2 <- summary(model)$coefficients[2] #coefficient of ltv day
            # pred.d7 <- exp(param_1 + param_2 * 7) # prediction for d7 ltv
            # delta <-  log(test_data[test_data$ltv_day==7, ]$ltv) - log(pred.d7)  # gap btw prediction and actual for d7 ltv
            # adjust_number <- ifelse(delta >=0.3, ifelse(param_1<=0, 1 + 0.3, 1 - 0.3), ifelse(param_1<=0, 1 - 0.5, 1 + 0.3))
            # param_1.new <- param_1 + delta * 0.4   # adjusted model based on prediction accuracy of d7 ltv
            # exp(param_1.new + param_2 * 30)
            # exp(param_1.new + param_2 * 60)
            # exp(param_1.new + param_2 * 90)
            # exp(param_1.new + param_2 * 120)
             if (nrow(input.sub)>0) { # ignore empty subsets
             
             test <- input.sub[input.sub$install_month > yesterday - 30 -30, ]
             train.30 <- input.sub[input.sub$install_month >= yesterday - 30 - 30 *7 & input.sub$install_month < yesterday - 30 - 30 & input.sub$ltv >0, ]
             train.60 <- input.sub[input.sub$install_month >= yesterday - 60 - 30 *7 & input.sub$install_month < yesterday - 60 - 30 & input.sub$ltv >0, ]
             train.90 <- input.sub[input.sub$install_month >= yesterday - 90 - 30 *7 & input.sub$install_month < yesterday - 90 - 30 & input.sub$ltv >0, ]
             train.120 <- input.sub[input.sub$install_month >= yesterday - 120 - 30 *7 & input.sub$install_month < yesterday - 120 - 30 & input.sub$ltv >0, ]
             # have test data and at least 3-month training data
             if(nrow(test)>0){
              pred.d30 <- ifelse(nrow(train.30)>=3*5, LtvPrediction(test[test$ltv_day==7, ]$ltv,train.30, 7, 30, 0.2)
                , max(median(input.app[input.app$ltv_day==30, ]$ltv), unlist(base.sub[base.sub$ltv_day==30, ]$ltv)))
              # if historical data is not sufficient, use LTV assumption
               pred.d30 <- ifelse(pred.d30>=test[test$ltv_day==7, ]$ltv, pred.d30, test[test$ltv_day==7, ]$ltv * 1.9) #make sure pred.d30 >= d7
               
               pred.d60 <- ifelse(nrow(train.60)>3*5, LtvPrediction(pred.d30, train.60, 30, 60, 0.4)
                , max(median(input.app[input.app$ltv_day==60, ]$ltv), unlist(base.sub[base.sub$ltv_day==60, ]$ltv)))
               pred.d60 <- ifelse(pred.d60>=pred.d30, pred.d60, pred.d30 * 1.3) # d60>=d30
               
               pred.d90 <- ifelse(nrow(train.90)>3*5, LtvPrediction(pred.d60, train.90, 60, 90, 1.2)
                , max(median(input.app[input.app$ltv_day==90, ]$ltv), unlist(base.sub[base.sub$ltv_day==90, ]$ltv)))
               pred.d90 <- ifelse(pred.d90>=pred.d60, pred.d90, pred.d60 * 1.07) # d90>=d60
               
               pred.d120 <- ifelse(nrow(train.120)>3*5, LtvPrediction(pred.d90, train.120, 90, 120, 1.5)
                , max(median(input.app[input.app$ltv_day==120, ]$ltv), unlist(base.sub[base.sub$ltv_day==120, ]$ltv)))
               pred.d120 <- ifelse(pred.d120>=pred.d90, pred.d120, pred.d90 * 1.05) # d120>=d90
               
               out <- data.frame(install_month = as.character(test$install_month[1]), app = test$app[1], install_source_group = test$install_source_group[1], country = test$country[1], os = test$os[1]
                                 , new_installs = test$new_installs[1], ltv_day = c(30, 60, 90, 120)
                                 , ltv_prediction = c(pred.d30, pred.d60, pred.d90, pred.d120 ))
               output <- rbind(output,out)
               }
             }
           }
        }

  }
}
output <- output[-1, ]
if(nrow(output) > 0)
{
  output<-createDataFrame(sqlContext, output)
  output$install_month<-cast(output$install_month, "String")
  saveDF(output, path=paste("s3n://com.funplusgame.bidata/model/r/marketing_ltv/kpi_diandian/", as.character(yesterday), "/", sep=""), source="json",  mode="overwrite")
}
# Stop Spark context
sparkR.stop()

# write.table(output[-1, ],"~/Desktop/Qu Ying/work/models/marketing LTV prediction /opt/test_output.txt",row.names=FALSE)

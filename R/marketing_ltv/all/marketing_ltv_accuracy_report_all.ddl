DROP TABLE IF EXISTS model.marketing_ltv_accuracy_report_all CASCADE;
CREATE TABLE model.marketing_ltv_accuracy_report_all
(
   app                varchar(64),
   os                 varchar(32),
   country            varchar(64),
   install_source_group     varchar(128),
   install_month      varchar(32),
   ltv_day            integer,
   new_installs       numeric(14,4)    DEFAULT 0,
   ltv_actual         numeric(14,4)    DEFAULT 0,
   ltv_prediction     numeric(14,4)    DEFAULT 0

);

COMMIT;
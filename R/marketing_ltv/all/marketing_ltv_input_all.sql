delete from model.marketing_ltv_input_all;
insert into model.marketing_ltv_input_all

with temp as (
select 
	to_char(date_trunc('month',install_date),'yyyy-mm-dd') as install_month
	,app
	--,install_source_group for facebook games
	,case when (app like 'farm%' or app like 'ha%' or app like  'royal%') then install_source_group else install_source end as install_source_group
	,country
	,os
	,sum(new_installs) as new_installs
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	,sum(d30_revenue)*1.00/sum(new_installs) as d30_ltv
	,sum(d60_revenue)*1.00/sum(new_installs) as d60_ltv
	,sum(d90_revenue)*1.00/sum(new_installs) as d90_ltv
	,sum(d120_revenue)*1.00/sum(new_installs) as d120_ltv
from kpi_processed.agg_marketing_kpi
where 
   (
	(install_date < dateadd(day, -7, '_YESTERDAY_') and install_date>=dateadd(month, -1,'_YESTERDAY_')) -- yesterday is first day of each month
	or (install_date < dateadd(month, -1,'_YESTERDAY_') and install_date>=dateadd(month, -10,'_YESTERDAY_')) -- 6 month hisotrical data for d30/60/90/120 ltv
   )
   and (country is not null and country <>'' and country <>'Unknown')
   and (os is not null and os !='Unknown' and os <>'')
	--and app = 'ffs.global.prod'
	--and install_source = 'Facebook Installs'
	--and country in ('Germany','United States')
group by 1,2,3,4,5
having sum(new_installs)>=1000 -- only predict ltv for channels with monthly installs over 1000
)

select 
install_month, app, install_source_group, country,os, new_installs, 7 as ltv_day, d7_ltv as ltv
from temp

union all

select 
install_month, app, install_source_group, country,os, new_installs, 30 as ltv_day, d30_ltv as ltv
from temp

union all

select 
install_month, app, install_source_group, country,os, new_installs, 60 as ltv_day, d60_ltv as ltv
from temp

union all

select 
install_month, app, install_source_group, country,os, new_installs, 90 as ltv_day, d90_ltv as ltv
from temp

union all

select 
install_month, app, install_source_group, country,os, new_installs, 120 as ltv_day, d120_ltv as ltv
from temp
;


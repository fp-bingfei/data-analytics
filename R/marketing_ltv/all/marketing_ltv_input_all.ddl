DROP TABLE IF EXISTS model.marketing_ltv_input_all CASCADE;
CREATE TABLE model.marketing_ltv_input_all
(
   install_month            varchar(32),
   app                      varchar(64),
   install_source_group     varchar(128),
   country                  varchar(64),
   os                       varchar(32),
   new_installs             integer          DEFAULT 0,
   ltv_day                  integer          DEFAULT 0,
   ltv                      numeric(14,4)    DEFAULT 0
);

COMMIT;

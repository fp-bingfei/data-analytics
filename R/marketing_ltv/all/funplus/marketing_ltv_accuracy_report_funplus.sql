-- Delete existing records
delete from model.marketing_ltv_accuracy_report_all 
	where 
	(install_month = date_trunc('month', dateadd(month,-2,'_YESTERDAY_'))  and ltv_day = 30)
 or (install_month = date_trunc('month', dateadd(month,-3,'_YESTERDAY_')) and ltv_day = 60)
 or (install_month = date_trunc('month', dateadd(month,-4,'_YESTERDAY_')) and ltv_day = 90)
 or (install_month = date_trunc('month', dateadd(month,-5,'_YESTERDAY_')) and ltv_day = 120)
;

-- Insert records
insert into model.marketing_ltv_accuracy_report_all


with temp_table as 
(
select
	app
	,os
	,country
	,case when (app like 'farm%' or app like 'ha%' or app like  'royal%') then install_source_group else install_source end as install_source_group
	,to_char(date_trunc('month',install_date),'yyyy-mm-dd') as install_month
	,sum(new_installs) as new_installs

	,sum(d7_revenue) as d7_revenue
	,sum(d30_revenue) as d30_revenue
	,sum(d60_revenue) as d60_revenue
	,sum(d90_revenue) as d90_revenue
	,sum(d120_revenue) as d120_revenue

from kpi_processed.agg_marketing_kpi 
where 
	install_date< dateadd(month,-1, '_YESTERDAY_') 
	and install_date>=dateadd(month,-5,'_YESTERDAY_') 

group by 1,2,3,4,5
)
,temp_table2 as (
select
	app,os,country,install_source_group,install_month, 30 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d30_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where 
install_month = to_char(dateadd(month,-2, '_YESTERDAY_'),'yyyy-mm-dd')
group by 1,2,3,4,5

union all

select
	app,os,country,install_source_group,install_month, 60 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d60_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where 
install_month = to_char(dateadd(month,-3, '_YESTERDAY_'),'yyyy-mm-dd')
group by 1,2,3,4,5

union all 

select
	app,os,country,install_source_group,install_month, 90 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d90_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where 
install_month = to_char(dateadd(month,-4, '_YESTERDAY_'),'yyyy-mm-dd')
group by 1,2,3,4,5

union all

select
	app,os,country,install_source_group,install_month, 120 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d120_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where
install_month = to_char(dateadd(month,-5, '_YESTERDAY_'),'yyyy-mm-dd')
group by 1,2,3,4,5
)

select 
   a.app
  ,a.os
  ,a.country
  ,a.install_source_group
  ,a.install_month
  ,a.ltv_day
  ,a.new_installs
  ,a.ltv_actual
  ,b.ltv_prediction
from temp_table2 a
left join model.marketing_ltv_output_all b
on a.install_source_group = b.install_source_group
and a.app = b.app
and a.install_month = b.install_month
and a.country = b.country
and a.os = b.os
and a.ltv_day = b.ltv_day
;

DROP TABLE IF EXISTS model.marketing_ltv_rs CASCADE;
CREATE TABLE model.marketing_ltv_rs
(
   app                      varchar(64),
   install_date             date,
   install_source_group     varchar(128),
   country            varchar(64),
   os                 varchar(32),
   new_installs       integer          DEFAULT 0,
   d7_revenue         numeric(14,4)    DEFAULT 0,
   d30_revenue        numeric(14,4)    DEFAULT 0,
   d60_revenue        numeric(14,4)    DEFAULT 0,
   d90_revenue        numeric(14,4)    DEFAULT 0,
   d120_revenue       numeric(14,4)    DEFAULT 0,
   --d150_revenue       numeric(14,4)    DEFAULT 0,
   d7_retained        integer          DEFAULT 0,
   --d7_payers          integer          DEFAULT 0,
   p30                numeric(14,4)    DEFAULT 0,
   p60                numeric(14,4)    DEFAULT 0,
   p90                numeric(14,4)    DEFAULT 0,
   p120               numeric(14,4)    DEFAULT 0,
--   p150               numeric(14,4)    DEFAULT 0,
   a30                numeric(14,4)    DEFAULT 0,
   a60                numeric(14,4)    DEFAULT 0,
   a90                numeric(14,4)    DEFAULT 0,
   a120               numeric(14,4)    DEFAULT 0,
--   a150               numeric(14,4)    DEFAULT 0,
   s30                numeric(14,4)    DEFAULT 0,
   s60                numeric(14,4)    DEFAULT 0,
   s90                numeric(14,4)    DEFAULT 0,
   s120               numeric(14,4)    DEFAULT 0
 --  s150               numeric(14,4)    DEFAULT 0
);

COMMIT;

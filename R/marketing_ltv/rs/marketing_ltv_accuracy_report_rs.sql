-- Delete existing records
delete from model.marketing_ltv_accuracy_report_rs where install_date='_YESTERDAY_';
	--yesterday = 2015-05-31

-- Insert records
insert into model.marketing_ltv_accuracy_report_rs

--- d30 actual
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-30 and ltv_day=30 and metrics in ('actual','prediction','SA_InstallSource')
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,sum(d30_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-30
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os

--- d60 actual
union all

select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-60 and ltv_day=60 and metrics in ('actual','prediction','SA_InstallSource')
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,sum(d60_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-60
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os

--- d90 actual
union all
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-90 and ltv_day=90 and metrics in ('actual','prediction','SA_InstallSource')
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,sum(d90_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-90
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os

--- d120 actual
union all
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-120 and ltv_day=120 and metrics in ('actual','prediction','SA_InstallSource')
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,sum(d120_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-120
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os


--- d30 SA_Campaign
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-30 and ltv_day=30 and metrics = 'SA_Campaign'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sum(d30_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-30
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
--- d60 SA_Campaign
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-60 and ltv_day=60 and metrics = 'SA_Campaign'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sum(d60_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-60
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
--- d90 SA_Campaign
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-90 and ltv_day=90 and metrics = 'SA_Campaign'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sum(d90_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-90
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
--- d120 SA_Campaign
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-120 and ltv_day=120 and metrics = 'SA_Campaign'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sum(d120_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-120
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign

--- d30 SA_Subpublisher
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-30 and ltv_day=30 and metrics = 'SA_SubPublisher'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sub_publisher,sum(d30_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-30
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign,sub_publisher) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
and a.sub_publisher=b.sub_publisher
--- d60 SA_Subpublisher
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-60 and ltv_day=60 and metrics = 'SA_SubPublisher'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sub_publisher,sum(d60_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-60
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign,sub_publisher) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
and a.sub_publisher=b.sub_publisher
--- d90 SA_Subpublisher
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-90 and ltv_day=90 and metrics = 'SA_SubPublisher'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sub_publisher,sum(d90_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-90
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign,sub_publisher) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
and a.sub_publisher=b.sub_publisher
--- d120 SA_Subpublisher
union all 
select 
  a.*
  ,b.revenue as actual_revenue
from

	  (select *
	  from model.marketing_ltv_report_rs 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-120 and ltv_day=120 and metrics = 'SA_SubPublisher'
	  ) a
left join
	  (select app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end as install_source_group,country,os,campaign,sub_publisher,sum(d120_revenue) as revenue 
	  from processed.tab_marketing_kpi 
	  where install_date=to_date('_YESTERDAY_','yyyy-mm-dd')-120
	  and app like 'royal%'
	  group by app,install_date,case when install_source like 'RS%' then 'Facebook_Install_Source' else 'Other_Install_Source' end,country,os,campaign,sub_publisher) b
on a.install_source_group=b.install_source_group
and a.app = b.app
and a.install_date=b.install_date
and a.country = b.country
and a.os=b.os
and a.campaign=b.campaign
and a.sub_publisher=b.sub_publisher


raw_data<-read.csv("/Users/funplus/Desktop/Qu Ying/work/models/marketing LTV prediction /input_raw_data_3_agg.csv") 
## suppose we need to make prediction for install date ="2015-03-18", in production it should be today()-7
## test
test<-as.Date("2015-03-20")
test_data<-subset(raw_data,raw_data$install_date==as.character(test))
## prepare training data
#in production, training data = date_add(install_date,-365)<install_date<date_add(install_date,-30)
#For example, given install_date=2015/10/23, we use data of install_date between 2015-09-23 and 2014-10-23 as training
input<-raw_data
input$install_date<-as.Date(input$install_date) ###convert to date type
input<-subset(input,input$install_date>=as.Date(test)-365*2&input$install_date<=as.Date(test))
##dimensions
install_source<-levels(as.factor(test_data$install_source))
country<-levels(as.factor(test_data$country))
os<-levels(as.factor(test_data$os))
##prepare for output
out<-data.frame(input[1,],p30=0,p60=0,p90=0,p120=0,p150=0,a30=0,a60=0,a90=0,a120=0,a150=0)
##iteration number
ni<-length(install_source)
nj<-length(country)
nk<-length(os)
## fitting the model for each dimension combination
for(i in 1:ni){ # each install_souce
  sub_data1<-cbind(input,check1=input$install_source==as.character(install_source[i]))
  sub_data1<-subset(sub_data1,sub_data1$check1=="TRUE")
  test_data1<-cbind(test_data,check1=test_data$install_source==as.character(install_source[i]))
  test_data1<-subset(test_data1,test_data1$check1=="TRUE")
    for(j in 1:nj){ # each country
       t1<-sub_data1
       sub_data2<-cbind(t1,check2=t1$country==country[j])
       sub_data2<-subset(sub_data2,sub_data2$check2=="TRUE")
       t1<-test_data1
       test_data2<-cbind(t1,check2=t1$country==country[j])
       test_data2<-subset(test_data2,test_data2$check2=="TRUE")
          for(k in 1:nk){# each os
              t2<-sub_data2
              sub_data3<-cbind(t2,check3=t2$os==os[k])
              sub_data3<-subset(sub_data3,sub_data3$check3=="TRUE")
              t2<-test_data2
              test_data3<-cbind(t2,check3=t2$os==os[k])
              test_data3<-subset(test_data3,test_data3$check3=="TRUE")
                if(nrow(test_data3)>0){ #make sure we have test data
###d30 prediction
                       train_data30_all<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-30)
                       train_data30<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-30&sub_data3$install_date>=as.Date(test)-30-90)
		               train_data30_1<-subset(train_data30,train_data30$d7_revenue>0) #d7 revenue >0 case
		               train_data30_0<-subset(train_data30,train_data30$d7_revenue<=0) #d7 revenue=0 case
		               p30<-median(train_data30_all$d30_revenue) #initial estimation = median of historical
		               a30<-mean(train_data30_all$d30_revenue) # initial estimation 
		  		   if(nrow(train_data30)>0){a30<-mean(train_data30$d30_revenue)} #update with last 90day data
		              if(nrow(train_data30_1)>0) #given historical data with d7 revenue>0
		                   {
		                      p30<-ifelse(test_data3$d7_revenue>0,median(train_data30_1$d30_revenue),quantile(train_data30_0$d30_revenue,0.5)) #update estimation with last 90-day historical data of d7_revenue>0
		                        if(nrow(train_data30_1)>=2)
		                        {#given more data, we can fit the model
		                          fit1<-lm(d30_revenue~d7_revenue+d7_retained-1,train_data30_1) #linear regression model          
		                          est<-ifelse(is.null(fit1$coefficients),0,predict(fit1,test_data3))
		                          p30<-ifelse(test_data3$d7_revenue>0,
		  ifelse(est<=0,median(train_data30_1$d30_revenue),est) #update the estimation
		  ,quantile(train_data30_0$d30_revenue,0.8))
		                         if(test_data3$d7_revenue>quantile(train_data30_1$d7_revenue,0.9))
		                            {p30<-median(train_data30_1$d30_revenue)} #if test_data has extreme large d7_revenue, do not use model prediction
		                         }
		                       }
		                  

###d60 prediction
            train_data60_all<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-60) #all data
		    train_data60<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-60&sub_data3$install_date>=as.Date(test)-60-90) #last 90day data
		    p60<-median(train_data60_all$d60_revenue) #initial value
		    a60<-mean(train_data60_all$d60_revenue) #initial value
		  if(nrow(train_data60)>0){a60<-mean(train_data60$d60_revenue)}#update with last 90-day data
		  a60<-ifelse(a60<a30,a30,a60)
		    if(nrow(train_data60)>0)
		    {		   
		    train_data60_1<-subset(train_data60,train_data60$d30_revenue>0) #check if d30_revenue>0
		    r2<-1
		    if(nrow(train_data60_1)>0) {r2<-train_data60_1$d60_revenue/train_data60_1$d30_revenue} #d30-to-d60 growth rate estimation
		    p<-ifelse(p30>quantile(train_data60$d30_revenue,0.9),0.8,0.6) #check if d30_revenue is extreme value
		    p60<-ifelse(p30<=0,0,p30*quantile(r2,p)) #use growth rate to estimate d60
		    }
####d90 prediction similar as d60 prediction
		    	train_data90_all<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-90)
		    	train_data90<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-90&sub_data3$install_date>=as.Date(test)-90-90)
		    	p90<-median(train_data90_all$d90_revenue)
		    	a90<-mean(train_data90_all$d90_revenue)
		  if(nrow(train_data90)>0){a90<-mean(train_data90$d90_revenue)}
		  a90<-ifelse(a90<a60,a60,a90)
		    if(nrow(train_data90)>0)
		    {		    
		    train_data90_1<-subset(train_data90,train_data90$d60_revenue>0)
		    r3<-1
		    if(nrow(train_data90_1)>0){r3<-train_data90_1$d90_revenue/train_data90_1$d60_revenue}
		    p<-ifelse(p60>quantile(train_data90$d60_revenue,0.9),0.8,0.6)
		    p90<-ifelse(p60<=0,0,p60*quantile(r3,p))
		    }
####d120 prediction
		    train_data120_all<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-120)	
		    train_data120<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-120&sub_data3$install_date>=as.Date(test)-120-90)
		    	p120<-median(train_data120_all$d120_revenue)
		    	a120<-mean(train_data120_all$d120_revenue)
		  if(nrow(train_data120)>0){a120<-mean(train_data120$d120_revenue)}
		  a120<-ifelse(a120<a90,a90,a120)
		    if(nrow(train_data120)>0)
		    {
		    train_data120_1<-subset(train_data120,train_data120$d90_revenue>0)
		    r4<-1
		    if(nrow(train_data120_1)>0) {r4<-train_data120_1$d120_revenue/train_data120_1$d90_revenue}
		    p<-ifelse(p90>quantile(train_data120$d90_revenue,0.9),0.1,0.4)
		    p120<-ifelse(p90<=0,0,p90*quantile(r4,p))
		    }

####d150 prediction
train_data150_all<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-150)		
train_data150<-subset(sub_data3,sub_data3$install_date<=as.Date(test)-150&sub_data3$install_date>=as.Date(test)-150-90)
		    p150<-median(train_data150_all$d150_revenue)
		    a150<-mean(train_data150_all$d150_revenue)
		  if(nrow(train_data150)>0){a150<-mean(train_data150$d150_revenue)}
		  a150<-ifelse(a150<a120,a120,a150)
		    if(nrow(train_data150)>0)
		    {
		    train_data150_1<-subset(train_data150,train_data150$d120_revenue>0)
		    r5<-1
		    if(nrow(train_data150_1)>0) {r5<-train_data150_1$d150_revenue/train_data150_1$d120_revenue}
		    p<-ifelse(p120>quantile(train_data150$d120_revenue,0.9),0.1,0.4)
		    p150<-ifelse(p120<=0,0,p120*quantile(r5,p))
		    }

n<-ncol(test_data3)
test_data3<-test_data3[,-(n-2):-n]
y<-data.frame(test_data3,p30=p30,p60=p60,p90=p90,p120=p120,p150=p150
,a30=a30,a60=a60,a90=a90,a120=a120,a150=a150)
out<-rbind(out,y)	
}
}
}
}
out<-out[-1,]
write.csv(out,"/Users/funplus/Desktop/Qu Ying/work/models/marketing LTV prediction /validation.csv",row.names=FALSE)
delete from model.marketing_ltv_input_farm;
insert into model.marketing_ltv_input_farm
select 
app
,install_date
,install_source_group
--	,campaign
,country
,os
,sum(new_installs) as new_installs
--LTV
,sum(d7_revenue) as d7_revenue
,sum(d30_revenue) as d30_revenue
,sum(d60_revenue) as d60_revenue
,sum(d90_revenue) as d90_revenue
,sum(d120_revenue) as d120_revenue
--,sum(d150_revenue) as d150_revenue
-- payer%
--	,d1_payers/new_installs as payer_pct_d1
--	,d7_payers/new_installs as payer_pct_d7
--	,d30_payers/new_installs as payer_pct_d30
--	,d60_payers/new_installs as payer_pct_d60
--	,d90_payers/new_installs as payer_pct_d90
--	,d120_payers/new_installs as payer_pct_d120
--	,d150_payers/new_installs as payer_pct_d150
--retention
--	,d1_retained/new_installs as retention_d1
,sum(d7_retained) as d7_retained
--	,d30_retained/new_installs as retention_d30
--	,d60_retained/new_installs as retention_d60
--	,d90_retained/new_installs as retention_d90
---	,d120_retained/new_installs as retention_d120
--   ,cost
from kpi_processed.agg_marketing_kpi
where 
install_date>=dateadd('day',-700,'_YESTERDAY_')
and install_date<='_YESTERDAY_'
and app like 'farm%'
group by 
app
,install_date
,install_source_group
--	,campaign
,country
,os
;

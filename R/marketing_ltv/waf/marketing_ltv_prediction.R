# Get parameter
argv <- commandArgs(TRUE)
# Check length
if(length(argv) < 1)
  q()
# Get the date of yesterday
yesterday <- as.Date(argv[1])

# Load SparkR library
library(SparkR)

# Init Spark context and SQL context
sc <- sparkR.init()
sqlContext <- sparkRSQL.init(sc)


###################################
###predictive model for installs###
###################################
LtvPrediction <- function(Test0, Target, TargetLtvDay, LtvDay0, LtvDay, Metrics, adjr_threshold, adjust_number){ 
   
   Overall.fit <-lm(log(Target)~LtvDay) # e.g. conversion ~ ltv day of FFS
   param.intercept <- Overall.fit$coefficients[1]
   param.LtvDay <- Overall.fit$coefficients[2]
   adjr <- summary(Overall.fit)$adj.r.squared
# e.g. predicted d30_conversion of WaF with FFS
     Test0.prediction <- exp(param.intercept + param.LtvDay * LtvDay0) 
     delta <- ifelse(Test0>0, log(Test0) - log(Test0.prediction), 1) # gap between prediction and actual
     if(Metrics=='arppu'){
     param.intercept.new <- delta + param.intercept * ifelse(adjr>=adjr_threshold, 1, 1+adjust_number)  # update parameters of the model
     # adjr_threshold = 0.9, adjust_number = 0.17
     }
     if(Metrics=='conversion'){
       param.intercept.new <- delta + param.intercept * ifelse(adjr>=adjr_threshold, 1, 1+adjust_number)  # update parameters of the model
       #adjr_threshold = 0.8, adjust_number = -0.05
     }
     prediction <- c(exp(param.intercept.new + param.LtvDay * TargetLtvDay))
     prediction
}
##############################################
### import data and define target&reference###
##############################################
# Get data from redshift
raw_data<-loadDF(sqlContext, source="jdbc", driver="com.amazon.redshift.jdbc41.Driver"
  , url="jdbc:redshift://kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true", dbtable="model.marketing_ltv_input")

raw_data <- collect(raw_data)

raw_data$install_week <- as.Date(raw_data$install_week) 
input_data <- raw_data[order(raw_data$install_week), ]

input.r <- subset(input_data, input_data$app == 'ffs.global.prod') # data to use as reference
input.t <- subset(input_data, input_data$app == 'waf.global.prod') # data of targeting app to predict

################
###prediction###
################
train <- input.r[input.r$install_week< yesterday - 120 & input.r$conversion>0, ]

test <- input.t[input.t$install_week == yesterday - 14, ]

# segments
os <- split(test, test$os)
country <- split(test, test$country)
installSource <- split(test, test$install_source)
install_week <- split(test, test$install_week)

# prepare output
output <- data.frame(app='', os = '', country = '', install_source = '', install_week = '', ltv_day = 0, new_installs=0, conversion_prediction = 0, arppu_prediction = 0, ltv_prediction = 0 ) # for marketing 
output2 <- data.frame(app='', os = '', country = '', install_source = '',install_week = '', ltv_day = 0, new_installs=0, conversion_prediction = 0, arppu_prediction = 0, ltv_prediction = 0 ) # for finance

for(loop0 in install_week){
  # install week loop
 test0 <- test[test$install_week == loop0$install_week[1], ]
 train0 <- train[train$install_week < as.Date(loop0$install_week[1]) & train$install_week >= as.Date(loop0$install_week[1]) - 120 - 13*7, ]
for(loop1 in os){
  # os loop
  train1 <- train0[train0$os == loop1$os[1], ]
  test1 <- test0[test0$os == loop1$os[1], ]
  # country loop
  for(loop2 in country){
    train2 <- train1[train1$country == loop2$country[1], ]
    test2 <- test1[test1$country == loop2$country[1], ]
    # install source loop
    for(loop3 in installSource){
      train3 <- train2[train2$install_source == loop3$install_source[1], ]
      test3<- test2[test2$install_source == loop3$install_source[1], ]
      # prediction
      if (nrow(test3)>0){
       
          ##########################
          TargetLtvDay <-30 * 1:4
          ##########################
          LtvDay0 <- 7 ############
          ########################## for makreting
          conversion.prediction <- LtvPrediction(unlist(test3[test3$ltv_day==LtvDay0, ]$conversion)
                                                 , unlist(train3$conversion), TargetLtvDay, LtvDay0, train3$ltv_day, 'conversion', 0.8, -0.05)
          arppu.prediction <- LtvPrediction(unlist(test3[test3$ltv_day==LtvDay0, ]$arppu)
                                            , unlist(train3$arppu), TargetLtvDay, LtvDay0, train3$ltv_day, 'arppu', 0.9, 0.17)
          out <- data.frame(cbind(app=test3$app[1], os = test3$os[1], country = test3$country[1], install_source = test3$install_source[1], install_week = as.character(test3$install_week[1])
                            ,ltv_day = TargetLtvDay, new_installs=test3[test3$ltv_day==LtvDay0, ]$new_installs, conversion_prediction = conversion.prediction, arppu_prediction = arppu.prediction
                            ,ltv_prediction = conversion.prediction * arppu.prediction))
          output <- rbind(output, out)
          ########################## for finance
          conversion.prediction2 <- LtvPrediction(unlist(test3[test3$ltv_day==LtvDay0, ]$conversion)
                                                 , unlist(train3$conversion), 1:120, LtvDay0, train3$ltv_day, 'conversion', 0.8, -0.05)
          arppu.prediction2 <- LtvPrediction(unlist(test3[test3$ltv_day==LtvDay0, ]$arppu)
                                            , unlist(train3$arppu), 1:120, LtvDay0, train3$ltv_day, 'arppu', 0.9, 0.17)
          out2 <- data.frame(cbind(app=test3$app[1], os = test3$os[1], country = test3$country[1], install_source = test3$install_source[1], install_week = as.character(test3$install_week[1])
                            ,ltv_day = 1:120, new_installs=test3[test3$ltv_day==LtvDay0, ]$new_installs, conversion_prediction = conversion.prediction2, arppu_prediction = arppu.prediction2
                            ,ltv_prediction = conversion.prediction2 * arppu.prediction2))
          output2 <- rbind(output2, out2)
          
      } # end of check if input is null
      
    } # end of install_source loop
  } # end of country loop
} # end of os loop
}# end of install_week loop
output <- output[-1, ]
output2 <- output2[-1, ]

if(nrow(output) > 0)
{
  output<-createDataFrame(sqlContext, output)
  output$install_week<-cast(output$install_week, "String")
  saveDF(output, path=paste("s3n://com.funplusgame.bidata/model/r/waf/marketing_ltv/", as.character(yesterday), "/", sep=""), source="json",  mode="overwrite")
}

if(nrow(output2) > 0)
{
  output2<-createDataFrame(sqlContext, output2)
  output2$install_week<-cast(output2$install_week, "String")
  saveDF(output, path=paste("s3n://com.funplusgame.bidata/model/r/waf/marketing_ltv/finance", as.character(yesterday), "/", sep=""), source="json",  mode="overwrite")
}
# Stop Spark context
sparkR.stop()




-- Delete existing records
delete from model.marketing_ltv_accuracy_report 
	where 
	(install_week = dateadd(day,-7*6,'_YESTERDAY_')  and ltv_day = 30)
 or (install_week = dateadd(day,-7*10,'_YESTERDAY_') and ltv_day = 60)
 or (install_week = dateadd(day,-7*13,'_YESTERDAY_') and ltv_day = 90)
 or (install_week = dateadd(day,-7*16,'_YESTERDAY_') and ltv_day = 120)
;

-- Insert records
insert into model.marketing_ltv_accuracy_report


with temp_table as 
(
select
	app
	,os
	,country
	,install_source
	,install_date
	,sum(new_installs) as new_installs

	,sum(d7_revenue) as d7_revenue
	,sum(d30_revenue) as d30_revenue
	,sum(d60_revenue) as d60_revenue
	,sum(d90_revenue) as d90_revenue
	,sum(d120_revenue) as d120_revenue

from kpi_processed.agg_marketing_kpi 
where 
	install_date< dateadd(day,-7*5, '_YESTERDAY_') 
	and install_date>=dateadd(day,-7*16,'_YESTERDAY_') 
	and os = 'Android' 
	and app = 'waf.global.prod' 
	and country in ('Germany','United Kingdom')
	and install_source in ('Facebook Installs', 'Organic')
group by 
	app
	,os
	,country
	,install_source
	,install_date
)
,temp_table2 as (
select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 30 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d30_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where 
install_date< dateadd(day,-7*5, '_YESTERDAY_') 
and install_date>=dateadd(day,-7*6,'_YESTERDAY_') 
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 60 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d60_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where 
install_date< dateadd(day,-7*9, '_YESTERDAY_') 
and install_date>=dateadd(day,-7*10,'_YESTERDAY_') 
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all 

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 90 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d90_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where 
install_date< dateadd(day,-7*12, '_YESTERDAY_') 
and install_date>=dateadd(day,-7*13,'_YESTERDAY_') 
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 120 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d120_revenue)*1.00/sum(new_installs) as ltv_actual
from temp_table
where
install_date< dateadd(day,-7*15, '_YESTERDAY_') 
and install_date>=dateadd(day,-7*16,'_YESTERDAY_') 
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')
)

select 
  a.*
  ,b.ltv_prediction
from temp_table2 a
left join model.marketing_ltv b
on a.install_source = b.install_source
and a.app = b.app
and a.install_week = b.install_week
and a.country = b.country
and a.os = b.os
and a.ltv_day = b.ltv_day
;

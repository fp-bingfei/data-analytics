delete from model.marketing_ltv_input;
insert into model.marketing_ltv_input
with temp_table as 
(
select
	app
	,case when os = 'ffs.global.android' then 'Android' else os end as os
	,country
	,install_source
	,install_date
	,sum(new_installs) as new_installs

	,sum(d7_payers) as d7_payers
	,sum(d30_payers) as d30_payers
	,sum(d60_payers) as d60_payers
	,sum(d90_payers) as d90_payers
	,sum(d120_payers) as d120_payers

	,sum(d7_revenue) as d7_revenue
	,sum(d30_revenue) as d30_revenue
	,sum(d60_revenue) as d60_revenue
	,sum(d90_revenue) as d90_revenue
	,sum(d120_revenue) as d120_revenue

from kpi_processed.agg_marketing_kpi 
where 
	install_date<dateadd(day,-7, '_YESTERDAY_') -- make sure actual d7 data is available
	and install_date>=dateadd(day,-7-7*18-7*14,'_YESTERDAY_') -- make sure actual 120-day data is available and 3-month training data
	and (os = 'Android' or os ='ffs.global.android')
	and (app = 'waf.global.prod' or app = 'ffs.global.prod')
	and country in ('Germany','United Kingdom')
	and install_source in ('Facebook Installs', 'Organic')
group by 
	app
	,case when os = 'ffs.global.android' then 'Android' else os end
	,country
	,install_source
	,install_date
)

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 7 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d7_payers)*1.00/sum(new_installs) as conversion
	,sum(d7_revenue)*1.00/sum(new_installs) as ltv
	,case when sum(d7_payers)>0 then sum(d7_revenue)*1.00/sum(d7_payers) else 0 end as arppu
from temp_table
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 30 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d30_payers)*1.00/sum(new_installs) as conversion
	,sum(d30_revenue)*1.00/sum(new_installs) as ltv
	,case when sum(d30_payers)>0 then sum(d30_revenue)*1.00/sum(d30_payers) else 0 end as arppu
from temp_table
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all 

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 60 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d60_payers)*1.00/sum(new_installs) as conversion
	,sum(d60_revenue)*1.00/sum(new_installs) as ltv
	,case when sum(d60_payers)>0 then sum(d60_revenue)*1.00/sum(d60_payers) else 0 end as arppu
from temp_table
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 90 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d90_payers)*1.00/sum(new_installs) as conversion
	,sum(d90_revenue)*1.00/sum(new_installs) as ltv
	,case when sum(d90_payers)>0 then sum(d90_revenue)*1.00/sum(d90_payers) else 0 end as arppu
from temp_table
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')

union all

select
	app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd') as install_week, 120 as ltv_day
	,sum(new_installs) as new_installs
	,sum(d120_payers)*1.00/sum(new_installs) as conversion
	,sum(d120_revenue)*1.00/sum(new_installs) as ltv
	,case when sum(d120_payers)>0 then sum(d120_revenue)*1.00/sum(d120_payers) else 0 end as arppu
from temp_table
group by app,os,country,install_source,to_char(date_trunc('week',install_date),'yyyy-mm-dd')
;
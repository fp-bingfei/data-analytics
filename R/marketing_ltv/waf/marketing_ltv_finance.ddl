DROP TABLE IF EXISTS model.marketing_ltv_finance CASCADE;
CREATE TABLE model.marketing_ltv_finance
(
   app                      varchar(64),
   os                       varchar(32),
   country                  varchar(64), 
   install_source           varchar(128),
   install_week             date,
   ltv_day                  integer  DEFAULT 0,
   new_installs             integer  DEFAULT 0,
   conversion_prediction    numeric(14,4)    DEFAULT 0,
   arppu_prediction         numeric(14,4)    DEFAULT 0,
   ltv_prediction           numeric(14,4)    DEFAULT 0
);

COMMIT;

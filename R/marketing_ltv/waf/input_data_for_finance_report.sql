DELETE FROM model.revenue_prediction_finance where run_date = '_YESTERDAY_';
INSERT INTO model.revenue_prediction_finance 

with temp_days as ( -- future 90 days
select 
	to_char(a1.end_day,'yyyy-mm-dd') as end_day
	,to_char(a2.day,'yyyy-mm-dd') as day
	,a1.app
	,count(*)
from
	(
	select 
		dateadd(day, i, '_YESTERDAY_') as end_day
		,dateadd(day, -60, dateadd(day, i, '_YESTERDAY_')) as start_day
		,'waf.global.prod' as app
	from public.seq_0_to_100
	where i<=90
	) a1
left join 
	(
		select 
			dateadd(day, -i, '_YESTERDAY_') as day
			,'waf.global.prod' as app
		from public.seq_0_to_100 a1

		union all 

		select 
			dateadd(day, i, '_YESTERDAY_') as day
			,'waf.global.prod' as app
		from public.seq_0_to_100 a1
	) a2
	on a1.app = a2.app
	and a2.day < a1.end_day
	and a2.day >= a1.start_day
group by 
	to_char(a1.end_day,'yyyy-mm-dd') 
	,to_char(a2.day,'yyyy-mm-dd') 
	,a1.app
)
---------------------------------------------------
-- raw data for revenue distribution calculation --
---------------------------------------------------
,temp_table as ( 
select 
	a.app_id
	,a.country
	,a.os
	,to_char(date_trunc('week',a.date),'yyyy-mm-dd') as week
	,b.install_source
	,case when datediff('day', b.install_date, a.date)>60 then '60+' else '<=60' end as days2install
	--,count(distinct a.user_key) as user_cnt
	,sum(a.revenue_usd) as revenue
from kpi_processed.fact_dau_snapshot a 
left join kpi_processed.dim_user b
	on a.app_id = b.app_id
	and a.user_key = b.user_key
	and b.app_id = 'waf.global.prod'
where 
a.date <'_YESTERDAY_'
and a.date >= dateadd(day,-7*4, '_YESTERDAY_')
and a.revenue_usd>0
and a.app_id = 'waf.global.prod'
group by 
	a.app_id
	,a.country
	,a.os
	,to_char(date_trunc('week',a.date),'yyyy-mm-dd')
	,b.install_source
	,case when datediff('day', b.install_date, a.date)>60 then '60+' else '<=60' end

)
---------------------------
-- latest ltv prediction --
---------------------------
, temp_table_prediction as ( 
select *
from
	(
	select *, rank() over(partition by app, country, os, install_source order by install_week desc) as rk 
	from model.marketing_ltv_finance
	)
where rk=1
)
--------------------------------
-- average daily new installs --
--------------------------------
,temp_table_installs as (
select 
	app
	,country
	,os
	,install_date
	,install_source
	,sum(new_installs) as new_installs
from kpi_processed.agg_marketing_kpi
where 
	install_date <'_YESTERDAY_'
	and install_date >= dateadd(day,-7*4, '_YESTERDAY_')
	and app = 'waf.global.prod'
	and country in ('Germany','United Kingdom')
	and install_source in ('Facebook Installs', 'Organic')
group by 
	app
	,country
	,os
	,install_date
	,install_source
)
 ----------------------------
 ---historical new installs--
 ----------------------------
,temp_table1 as (
select 
t1.app
,t1.os
,t1.country
,t1.install_source
--,t1.end_date
--,t1.install_date
,date_trunc('week',t1.end_date) as week
,t1.ltv_day 
,t1.ltv_day -1 as ltv_day0
,sum(case when t2.new_installs is null then t3.new_installs else t2.new_installs end) as new_installs
from
(
	select 
		a1.* 
		,cast(a2.end_day as date) as end_date
		,cast(a2.day as date) as install_date
		,datediff(day, cast(a2.day as date), cast(a2.end_day as date)) as ltv_day
	from 
	(select app, os, country, install_source, count(*) from temp_table_prediction group by app, os, country, install_source) a1
	left join temp_days a2
	   on a1.app = a2.app
) t1
left join 
(
select 
     app
	,country
	,os
	,install_source
	,install_date
	,sum(new_installs) as new_installs
from kpi_processed.agg_marketing_kpi a1
where 
install_date<'_YESTERDAY_'
	and install_date>=dateadd(day,-60,'_YESTERDAY_') 
	and (app = 'waf.global.prod')
	and country in ('Germany','United Kingdom')
	and install_source in ('Facebook Installs', 'Organic')
group by 
    app
	,country
	,os
	,install_source
	,install_date
) t2
	on t1. app = t2.app
	and t1.os = t2.os
	and t1.country = t2.country
	and t1.install_source =t2.install_source
	and t2.install_date = t1.install_date

left join 
(
select 
app, os, country, install_source, avg(new_installs) as new_installs
from temp_table_installs
group by app, os, country, install_source
) t3
	on t1. app = t3.app
	and t1.os = t3.os
	and t1.country = t3.country
	and t1.install_source =t3.install_source

group by 
t1.app
,t1.os
,t1.country
,t1.install_source
--,t1.end_date
--,t1.install_date
,date_trunc('week',t1.end_date) 
,t1.ltv_day 
,t1.ltv_day -1 

)


--------------------------------------------------------------------------------
-- percentage of total revenue that main os/country/install source contribute --
--------------------------------------------------------------------------------
, temp_table3 as (
select 
a.*
,a.revenue*1.00/b.revenue as pct_revenue 
from
	(
	select 
	app_id
	,week
	,sum(revenue) as revenue
	from temp_table

	where 
		country in ('Germany', 'United Kingdom')
		and os in ('Android')
		and install_source in ('Organic', 'Facebook Installs')
		and days2install = '<=60'
	group by app_id, week
	) a  -- revenue from os/country/install_source for which LTV is predicted 
left join 
	(
	select
		app_id
		,week
		,sum(revenue) as revenue
	from temp_table
	group by app_id, week

	) b -- total weekly revenue
	on a.app_id = b.app_id
	and a.week = b.week
)
-------------------------------
-- revenue prediction --
-------------------------------

select 
'_YESTERDAY_' as run_date
,t.*
,revenue_prediction/pct_revenue as total_revenue_prediction
from
(
select 
c1.app
--,c1.os
--,c1.country
--,c1.install_source
,c1.week 
,c2.pct_revenue
,sum(revenue_prediction) as revenue_prediction
from
(
select 
a1.*
,b2.ltv_prediction as ltv_prediction1
,b4.ltv_prediction as ltv_prediction0
,a1.new_installs*b2.ltv_prediction as revenue_prediction1
,a1.new_installs*b4.ltv_prediction as revenue_prediction0
,a1.new_installs*b2.ltv_prediction - a1.new_installs*b4.ltv_prediction as revenue_prediction
--,case when b1.ltv_prediction is null then b2.ltv_prediction else b1.ltv_prediction end as ltv_prediction1
--,case when b3.ltv_prediction is null then b4.ltv_prediction else b3.ltv_prediction end as ltv_prediction0
--,a1.new_installs * (case when b1.ltv_prediction is null then b2.ltv_prediction else b1.ltv_prediction end) as revenue_prediction1
--,a1.new_installs * (case when b3.ltv_prediction is null then b4.ltv_prediction else b3.ltv_prediction end) as revenue_prediction0
--,a1.new_installs * (case when b1.ltv_prediction is null then b2.ltv_prediction else b1.ltv_prediction end) -
-- a1.new_installs * (case when b3.ltv_prediction is null then b4.ltv_prediction else b3.ltv_prediction end) as revenue_prediction

from temp_table1 a1
--left join model.marketing_ltv_finance b1
--	on a1.app = b1.app
--	and a1.country = b1.country
--	and a1.os = b1.os
--	and a1.install_source = b1.install_source
--	and a1.ltv_day = b1.ltv_day
--	and a1.install_week = b1.install_week
left join temp_table_prediction b2
	on a1.app = b2.app
	and a1.country = b2.country
	and a1.os = b2.os
	and a1.install_source = b2.install_source
	and a1.ltv_day = b2.ltv_day
-- accumulative ltv of previous week	
--left join model.marketing_ltv_finance b3
--	on a1.app = b3.app
--	and a1.country = b3.country
--	and a1.os = b3.os
--	and a1.install_source = b3.install_source
--	and a1.ltv_day0 = b3.ltv_day
--	and a1.install_week = b3.install_week
left join temp_table_prediction b4
	on a1.app = b4.app
	and a1.country = b4.country
	and a1.os = b4.os
	and a1.install_source = b4.install_source
	and a1.ltv_day0 = b4.ltv_day
) c1
left join (select app_id, avg(pct_revenue) as pct_revenue from temp_table3 group by app_id) c2
on c1.app = c2.app_id
group by 
c1.app
--,c1.os
--,c1.country
--,c1.install_source
,c1.week
,c2.pct_revenue
) t
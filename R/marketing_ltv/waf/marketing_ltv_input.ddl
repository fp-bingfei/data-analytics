DROP TABLE IF EXISTS model.marketing_ltv_input CASCADE;
CREATE TABLE model.marketing_ltv_input
(
   app                      varchar(64),
   os                       varchar(32),
   country                  varchar(64),
   install_source           varchar(128),
   install_week             varchar(64),
   ltv_day                  integer          DEFAULT 0,
   new_installs             integer          DEFAULT 0,
   conversion               numeric(14,4)    DEFAULT 0,
   ltv                      numeric(14,4)    DEFAULT 0,
   arppu                    numeric(14,4)    DEFAULT 0
);

COMMIT;

DROP TABLE IF EXISTS model.revenue_prediction_finance;
CREATE TABLE model.revenue_prediction_finance
(
run_date                    date
,app                        varchar(64)
--,os                         varchar(32)
--,country                    varchar(64)
--,install_source             varchar(128)
,week                       date
,pct_revenue                numeric(14,4)
,revenue_prediction         numeric(14,4)
,total_revenue_prediction   numeric(14,4)

)
;
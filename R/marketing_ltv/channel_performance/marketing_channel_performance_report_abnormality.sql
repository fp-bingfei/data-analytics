DELETE FROM processed.marketing_channel_performance_report_abnormality where run_date = '_YESTERDAY_';
INSERT INTO processed.marketing_channel_performance_report_abnormality

with temp_table_1 as
(
		select 
		    install_date
			--,date_trunc('week',install_date) as install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained			
			-- retention and conversion calculation
			,sum(d1_retained)*100.00/sum(new_installs) as d1_retention
			,sum(d7_retained)*100.00/sum(new_installs) as d7_retention
			,sum(d1_payers)*100.00/sum(new_installs) as d1_conversion
			,sum(d7_payers)*100.00/sum(new_installs) as d7_conversion		 
		from processed.tab_marketing_kpi
			where install_date< dateadd(day,-7,'_YESTERDAY_')
			and install_date >= dateadd(day,-7*7,'_YESTERDAY_')

		group by 
		    install_date
			--,date_trunc('week',install_date) 
			,app
			,install_source
			--,country
			,os
)
-- abnormality
,temp_table_abnormality as 
(
-- new installs
select 
	a1.install_date
	,a1.app
	,a1.install_source
	--,a1.country
	,a1.os
	,'new_installs(k)' as metrics
	,a1.new_installs as value
	,cast(2*q1-q3 as real) as lwc
	,cast(2*q3-q1 as real) as upc
	,case when a1.new_installs>=cast(2*q1-q3 as real) and a1.new_installs<=cast(2*q3-q1 as real) then 0 else 1 end as outlier
from temp_table_1 a1
left join 
	(  select app,install_source--,country
		,os, q1, count(*)
		from
			(
			select app, install_source--,country
			,os,new_installs, percentile_cont(0.25) within group (order by new_installs) over(partition by app,install_source,os) as q1
			from temp_table_1
			group by app,install_source--,country
			,os,new_installs
			) t
		group by app,install_source--,country
		,os,q1
	) a2
	on a1.app = a2.app
	and a1.install_source = a2.install_source
	--and a1.country = a2.country
	and a1.os = a2.os
left join 
	(  select app,install_source----,country
		,os, q3, count(*)
		from
			(
			select app, install_source----,country
			,os,new_installs, percentile_cont(0.75) within group (order by new_installs) over(partition by app,install_source,os) as q3
			from temp_table_1
			group by app,install_source----,country
			,os,new_installs
			) t
		group by app,install_source----,country
		,os,q3
	) a3
	on a1.app = a3.app
	and a1.install_source = a3.install_source
	--and a1.country = a3.country
	and a1.os = a3.os
where 
    a1.install_date< dateadd(day,-7,'_YESTERDAY_')
	and a1.install_date >= dateadd(day,-7*2,'_YESTERDAY_')

union all
-- d1_retention
select 
	a1.install_date
	,a1.app
	,a1.install_source
	--,a1.country
	,a1.os
	,'d1_retention(%)' as metrics
	,a1.d1_retention as value
	,cast(2*q1-q3 as real) as lwc
	,cast(2*q3-q1 as real) as upc
	,case when a1.d1_retention>=cast(2*q1-q3 as real) and a1.d1_retention<=cast(2*q3-q1 as real) then 0 else 1 end as outlier
from temp_table_1 a1
left join 
	(  select app,install_source--,country
		,os, q1, count(*)
		from
			(
			select app, install_source--,country
			,os,d1_retention, percentile_cont(0.25) within group (order by d1_retention) over(partition by app,install_source,os) as q1
			from temp_table_1
			group by app,install_source--,country
			,os,d1_retention
			) t
		group by app,install_source--,country
		,os,q1
	) a2
	on a1.app = a2.app
	and a1.install_source = a2.install_source
	--and a1.country = a2.country
	and a1.os = a2.os
left join 
	(  select app,install_source--,country
		,os, q3, count(*)
		from
			(
			select app, install_source--,country
			,os,
			d1_retention, percentile_cont(0.75) within group (order by d1_retention) over(partition by app,install_source,os) as q3
			from temp_table_1
			group by app,install_source--,country
			,os,d1_retention
			) t
		group by app,install_source--,country
		,os,q3
	) a3
	on a1.app = a3.app
	and a1.install_source = a3.install_source
	--and a1.country = a3.country
	and a1.os = a3.os
where 
    a1.install_date< dateadd(day,-7,'_YESTERDAY_')
	and a1.install_date >= dateadd(day,-7*2,'_YESTERDAY_')

union all
-- d7_retention
select 
a1.install_date
,a1.app
,a1.install_source
--,a1.country
,a1.os
,'d7_retention(%)' as metrics
,a1.d7_retention as value
,cast(2*q1-q3 as real) as lwc
,cast(2*q3-q1 as real) as upc
,case when a1.d7_retention>=cast(2*q1-q3 as real) and a1.d7_retention<=cast(2*q3-q1 as real) then 0 else 1 end as outlier
from temp_table_1 a1
left join 
	(  select app,install_source--,country
		,os, q1, count(*)
		from
			(
			select app, install_source--,country
			,os,
			d7_retention, percentile_cont(0.25) within group (order by d7_retention) over(partition by app,install_source,os) as q1
			from temp_table_1
			group by app,install_source--,country
			,os,d7_retention
			) t
		group by app,install_source--,country
		,os,q1
	) a2
	on a1.app = a2.app
	and a1.install_source = a2.install_source
	--and a1.country = a2.country
	and a1.os = a2.os
left join 
	(  select app,install_source--,country
		,os, q3, count(*)
		from
			(
			select app, install_source--,country
			,os,
			d7_retention, percentile_cont(0.75) within group (order by d7_retention) over(partition by app,install_source,os) as q3
			from temp_table_1
			group by app,install_source--,country
			,os,d7_retention
			) t
		group by app,install_source--,country
		,os,q3
	) a3
	on a1.app = a3.app
	and a1.install_source = a3.install_source
	--and a1.country = a3.country
	and a1.os = a3.os
where 
    a1.install_date< dateadd(day,-7,'_YESTERDAY_')
	and a1.install_date >= dateadd(day,-7*2,'_YESTERDAY_')

union all
-- d1_conversion
select 
a1.install_date
,a1.app
,a1.install_source
--,a1.country
,a1.os
,'d1_conversion(%)' as metrics
,a1.d1_conversion as value
,cast(2*q1-q3 as real) as lwc
,cast(2*q3-q1 as real) as upc
,case when a1.d1_conversion>=cast(2*q1-q3 as real) and a1.d1_conversion<=cast(2*q3-q1 as real) then 0 else 1 end as outlier
from temp_table_1 a1
left join 
	(  select app,install_source--,country
		,os, q1, count(*)
		from
			(
			select app, install_source--,country
			,os,
			d1_conversion, percentile_cont(0.25) within group (order by d1_conversion) over(partition by app,install_source,os) as q1
			from temp_table_1
			group by app,install_source--,country
			,os,d1_conversion
			) t
		group by app,install_source--,country
		,os,q1
	) a2
		on a1.app = a2.app
		and a1.install_source = a2.install_source
	--	and a1.country = a2.country
		and a1.os = a2.os
left join 
	(  select app,install_source--,country
		,os, q3, count(*)
		from
			(
			select app, install_source--,country
			,os,d1_conversion, percentile_cont(0.75) within group (order by d1_conversion) over(partition by app,install_source,os) as q3
			from temp_table_1
			group by app,install_source--,country
			,os,d1_conversion
			) t
		group by app,install_source--,country
		,os,q3
	) a3
		on a1.app = a3.app
		and a1.install_source = a3.install_source
		--and a1.country = a3.country
		and a1.os = a3.os
where 
    a1.install_date< dateadd(day,-7,'_YESTERDAY_')
	and a1.install_date >= dateadd(day,-7*2,'_YESTERDAY_')

union all
-- d7_conversion
select 
a1.install_date
,a1.app
,a1.install_source
--,a1.country
,a1.os
,'d7_conversion(%)' as metrics
,a1.d7_conversion as value
,cast(2*q1-q3 as real) as lwc
,cast(2*q3-q1 as real) as upc
,case when a1.d7_conversion>=cast(2*q1-q3 as real) and a1.d7_conversion<=cast(2*q3-q1 as real) then 0 else 1 end as outlier
from temp_table_1 a1
left join 
	(  select app,install_source--,country
		,os, q1, count(*)
		from
			(
			select app, install_source--,country
			,os,d7_conversion, percentile_cont(0.25) within group (order by d7_conversion) over(partition by app,install_source,os) as q1
			from temp_table_1
			group by app,install_source--,country
			,os,d7_conversion
			) t
		group by app,install_source
		--,country
		,os,q1
	) a2
		on a1.app = a2.app
		and a1.install_source = a2.install_source
		--and a1.country = a2.country
		and a1.os = a2.os
left join 
	(  select app,install_source--,country
		,os, q3, count(*)
		from
			(
			select app, install_source--,country
			,os,
			d7_conversion, percentile_cont(0.75) within group (order by d7_conversion) over(partition by app,install_source,os) as q3
			from temp_table_1
			group by app,install_source--,country
			,os,d7_conversion
			) t
		group by app,install_source--,country
		,os,q3
	) a3
		on a1.app = a3.app
		and a1.install_source = a3.install_source
		--and a1.country = a3.country
		and a1.os = a3.os
where 
    a1.install_date< dateadd(day,-7,'_YESTERDAY_')
	and a1.install_date >= dateadd(day,-7*2,'_YESTERDAY_')	
)

select
cast('_YESTERDAY_' as date) as run_date
,'week(-1)' as install_week
,t.*
from
(
select 
	app
	,install_source
	--,country
	,os
	,metrics
	,sum(outlier) as outlier_number
from temp_table_abnormality
group by 
	app
	,install_source
	--,country
	,os
	,metrics
) t
;
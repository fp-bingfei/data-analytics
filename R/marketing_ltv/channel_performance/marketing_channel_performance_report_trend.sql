DELETE FROM processed.marketing_channel_performance_report_trend where run_date = '_YESTERDAY_';
INSERT INTO processed.marketing_channel_performance_report_trend

with temp_table_1 as
(
		select 
		    install_date
			,date_trunc('week',install_date) as install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained			
		 
		from processed.tab_marketing_kpi
			where install_date< dateadd(day,-7,'_YESTERDAY_')
			and install_date >= dateadd(day,-7*7,'_YESTERDAY_')

		group by 
		    install_date
			,date_trunc('week',install_date) 
			,app
			,install_source
			--,country
			,os
)

, temp_table_2 as 
(
select
t3.*
,case when rk = 1 then 'week(-1)'
	  when rk = 2 then 'week(-2)'
	  else 'week(-3:-6)' end as install_week
from
	(
	select 
		t1.*
		,t2.new_installs as total_new_installs
		,t2.d7_revenue as total_d7_revenue
		,rank() over(partition by t1.app,t1.install_source,t1.os order by t1.install_week_begin desc) as rk
	from
		(
		select
			install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained
		from temp_table_1
		group by 
			install_week_begin
			,app
			,install_source
			--,country
			,os
		) t1
	left join
		(
		select 
			install_week_begin
			,app
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
		from temp_table_1
		group by 
			install_week_begin
			,app
			--country
			,os
		) t2
		on t1.install_week_begin = t2.install_week_begin
			and t1.app = t2.app
			--and t1.country = t2.country
			and t1.os = t2.os
	) t3
)
, temp_table_3 as 
(
select
	install_week
	,app
	,install_source
	--,country
	,os
	,avg(new_installs) as new_installs
	,avg(d7_revenue) as d7_revenue
	-- pct
	,sum(new_installs)*1.00/sum(total_new_installs) as pct_new_installs
	,case when sum(d7_revenue)>0 then sum(d7_revenue)*1.00/sum(total_d7_revenue) else 0 end as pct_d7_revenue
    -- retention and conversion calculation
	,sum(d1_retained)*1.00/sum(new_installs) as d1_retention
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d1_payers)*1.00/sum(new_installs) as d1_conversion
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion

	-- ltv calculation
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	from temp_table_2
	group by 
	install_week
	,app
	,install_source
	--,country
	,os
)

-- new install
select 
'_YESTERDAY_' as run_date
,t.*
from
(
select 
install_week, app, install_source
--, country
, os
,new_installs*1.00/1000 as value
,'new_installs(k)' as metrics
from temp_table_3

union all
-- pct new install
select 
install_week, app, install_source
--, country
, os
,pct_new_installs*100 as value
,'pct_new_installs(%)' as metrics
from temp_table_3

union all
-- d7_revenue
select 
install_week, app, install_source
--, country
, os
,d7_revenue/1000 as value
,'d7_revenue($k)' as metrics
from temp_table_3

union all
-- pct d7 revenue
select 
install_week, app, install_source
--, country
, os
,pct_d7_revenue*100 as value
,'pct_d7_revenue(%)' as metrics
from temp_table_3

union all
-- d1_retention
select 
install_week, app, install_source
--, country
, os
,d1_retention*100 as value
,'d1_retention(%)' as metrics
from temp_table_3

union all
-- d7_retention
select 
install_week, app, install_source
--, country
, os
,d7_retention*100 as value
,'d7_retention(%)' as metrics
from temp_table_3

union all
-- d1_conversion
select 
install_week, app, install_source
--, country
, os
,d1_conversion*100 as value
,'d1_conversion(%)' as metrics
from temp_table_3

union all
-- d7_conversion
select 
install_week, app, install_source
--, country
, os
,d7_conversion*100 as value
,'d7_conversion(%)' as metrics
from temp_table_3

union all
-- d7_ltv
select 
install_week, app, install_source
--, country
, os
,d7_ltv as value
,'d7_ltv($)' as metrics
from temp_table_3
) t
;
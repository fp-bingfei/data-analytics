DELETE FROM processed.marketing_channel_performance_report_stability where run_date = '_YESTERDAY_';
INSERT INTO processed.marketing_channel_performance_report_stability

with temp_table_1 as
(
		select 
		    install_date
			--,date_trunc('week',install_date) as install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs)*1.00/1000 as new_installs
			,sum(d7_revenue)*1.00/1000 as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained			
			-- retention and conversion calculation
			,sum(d1_retained)*100.00/sum(new_installs) as d1_retention
			,sum(d7_retained)*100.00/sum(new_installs) as d7_retention
			,sum(d1_payers)*100.00/sum(new_installs) as d1_conversion
			,sum(d7_payers)*100.00/sum(new_installs) as d7_conversion

			-- ltv calculation
	        ,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv		 
		from processed.tab_marketing_kpi
			where install_date< dateadd(day,-7,'_YESTERDAY_')
			and install_date >= dateadd(day,-7*7,'_YESTERDAY_')

		group by 
		    install_date
			--,date_trunc('week',install_date) 
			,app
			,install_source
			--,country
			,os
)

,temp_table_stability as
(
-- new installs
	select 
	a1.*
	,rank() over(partition by app,os order by cov_value) as rk
	,'new_installs(k)' as metrics
	from
	(select 
		app
		,install_source
		--,country
		,os
		,avg(new_installs) as mean
		,cast(stddev(new_installs) as real) as std
		,case when avg(new_installs)>0 then cast(stddev(new_installs) as real)/avg(new_installs) else -1 end as cov_value
	from temp_table_1 
	group by 
		app
		,install_source
		--,country
		,os
	) a1
    where cov_value>0
union all

-- d1_retention
	select 
	a1.*
	,rank() over(partition by app,os order by cov_value) as rk
	,'d1_retention(%)' as metrics
	from
	(select 
		app
		,install_source
		--,country
		,os
		,avg(d1_retention) as mean
		,cast(stddev(d1_retention) as real) as std
		,case when avg(d1_retention)>0 then cast(stddev(d1_retention) as real)/avg(d1_retention) else -1 end as cov_value
	from temp_table_1 
	group by 
		app
		,install_source
		--,country
		,os
	) a1
    where cov_value>0
union all

-- d7_retention
	select 
	a1.*
	,rank() over(partition by app,os order by cov_value) as rk
	,'d7_retention(%)' as metrics
	from
	(select 
		app
		,install_source
		--,country
		,os
		,avg(d7_retention) as mean
		,cast(stddev(d7_retention) as real) as std
		,case when avg(d7_retention)>0 then cast(stddev(d7_retention) as real)/avg(d7_retention) else -1 end as cov_value
	from temp_table_1 
	group by 
		app
		,install_source
		--,country
		,os
	) a1
    where cov_value>0
union all

-- d1_conversion
	select 
	a1.*
	,rank() over(partition by app,os order by cov_value) as rk
	,'d1_conversion(%)' as metrics
	from
	(select 
		app
		,install_source
		--,country
		,os
		,avg(d1_conversion) as mean
		,cast(stddev(d1_conversion) as real) as std
		,case when avg(d1_conversion)>0 then cast(stddev(d1_conversion) as real)/avg(d1_conversion) else -1 end as cov_value
	from temp_table_1 
	group by 
		app
		,install_source
		--,country
		,os
	) a1
    where cov_value>0
union all

-- d7_conversion
	select 
	a1.*
	,rank() over(partition by app,os order by cov_value) as rk
	,'d7_conversion(%)' as metrics
	from
	(select 
		app
		,install_source
		--,country
		,os
		,avg(d7_conversion) as mean
		,cast(stddev(d7_conversion) as real) as std
		,case when avg(d7_conversion)>0 then cast(stddev(d7_conversion) as real)/avg(d7_conversion) else -1 end as cov_value
	from temp_table_1 
	group by 
		app
		,install_source
		--,country
		,os
	) a1
    where cov_value>0
union all

-- d7_ltv
	select 
	a1.*
	,rank() over(partition by app,os order by cov_value) as rk
	,'d7_ltv($)' as metrics
	from
	(select 
		app
		,install_source
		--,country
		,os
		,avg(d7_ltv) as mean
		,cast(stddev(d7_ltv) as real) as std
		,case when avg(d7_ltv)>0 then cast(stddev(d7_ltv) as real)/avg(d7_ltv) else -1 end as cov_value
	from temp_table_1 
	group by 
		app
		,install_source
		--,country
		,os
	) a1
	where cov_value>0
)
 
select 
cast('_YESTERDAY_' as date) as run_date
,'week(-1)' as install_week
,a1.*
,case when a1.rk<round(0.3*max_rk) then 'high'
      when a1.rk>round(0.7*max_rk) then 'low'
      else 'medium' end as stability
from temp_table_stability a1
left join 
(
select 
	app
	--,country
	,os
	,metrics
	,max(rk) as max_rk
from temp_table_stability
group by 
	app
	--,country
	,os
	,metrics
) a2 
on a1.app = a2.app
--and a1.country = a2.country
and a1.os = a2.os
and a1.metrics = a2.metrics
;

DROP TABLE IF EXISTS model.marketing_channel_performance_report_stability CASCADE;
CREATE TABLE model.marketing_channel_performance_report_stability
(
   week_begin_bw_1         varchar(32),
   app                     varchar(64),
   os                      varchar(128),
   install_source          varchar(32),
   stability_new_installs  varchar(32),
   stability_d7_revenue    varchar(32),
   stability_d1_retention  varchar(32),
   stability_d7_retention  varchar(32),
   stability_d1_conversion  varchar(32),
   stability_d7_conversion  varchar(32),
   stability_d7_ltv         varchar(32)
);

COMMIT;
DROP TABLE IF EXISTS processed.marketing_channel_performance_report_abnormality CASCADE;
CREATE TABLE processed.marketing_channel_performance_report_abnormality
(
   run_date           date,
   install_week       varchar(32),
   app                varchar(64),
   install_source     varchar(128),
   --country            varchar(64),
   os                 varchar(32),
   metrics            varchar(64),
   outlier_number     integer
);

COMMIT;
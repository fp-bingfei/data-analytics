DELETE FROM model.marketing_channel_performance_report_abnormality where week_begin_bw_1 = to_char(dateadd(day,-7*2,'_YESTERDAY_'),'YYYY-MM-DD');
INSERT INTO model.marketing_channel_performance_report_abnormality

with temp_table_1 as
(
		select 
		    install_date
			--,date_trunc('week',install_date) as install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			--,sum(d7_revenue) as d7_revenue
			--,sum(d1_payers) as d1_payers
			--,sum(d7_payers) as d7_payers
			--,sum(d1_retained) as d1_retained
			--,sum(d7_retained) as d7_retained			
			-- retention and conversion calculation
			,sum(d1_retained)*100.00/sum(new_installs) as d1_retention
			,sum(d7_retained)*100.00/sum(new_installs) as d7_retention
			,sum(d1_payers)*100.00/sum(new_installs) as d1_conversion
			,sum(d7_payers)*100.00/sum(new_installs) as d7_conversion		 
		from kpi_processed.agg_marketing_kpi
			where install_date< dateadd(day,-7,'_YESTERDAY_')
			and install_date >= dateadd(day,-7*7,'_YESTERDAY_')
			and app like 'ffs%'

		group by 
		    install_date
			--,date_trunc('week',install_date) 
			,app
			,install_source
			--,country
			,os
)
-- abnormality
,temp_table_abnormality as 
(
-- new installs
select 
	a1.install_date
	,a1.app
	,a1.install_source
	--,a1.country
	,a1.os
	,case when a1.new_installs>=cast(2*q1_new_installs-q3_new_installs as real) and a1.new_installs<=cast(2*q3_new_installs-q1_new_installs as real) then 0 else 1 end as outlier_new_installs
	,case when a1.d1_retention>=cast(2*q1_d1_retention-q3_d1_retention as real) and a1.d1_retention<=cast(2*q3_d1_retention-q1_d1_retention as real) then 0 else 1 end as outlier_d1_retention
	,case when a1.d7_retention>=cast(2*q1_d7_retention-q3_d7_retention as real) and a1.d7_retention<=cast(2*q3_d7_retention-q1_d7_retention as real) then 0 else 1 end as outlier_d7_retention
	,case when a1.d1_conversion>=cast(2*q1_d1_conversion-q3_d1_conversion as real) and a1.d1_conversion<=cast(2*q3_d1_conversion-q1_d1_conversion as real) then 0 else 1 end as outlier_d1_conversion
	,case when a1.d7_conversion>=cast(2*q1_d7_conversion-q3_d7_conversion as real) and a1.d7_conversion<=cast(2*q3_d7_conversion-q1_d7_conversion as real) then 0 else 1 end as outlier_d7_conversion
from temp_table_1 a1
left join 
	(  select app,install_source--,country
		,os
		,q1_new_installs
		,q3_new_installs
		,q1_d1_retention
		,q3_d1_retention
		,q1_d7_retention
		,q3_d7_retention
		,q1_d1_conversion
		,q3_d1_conversion
		,q1_d7_conversion
		,q3_d7_conversion
		, count(*)
		from
			(
			select app, install_source--,country
			,os
			,new_installs
			,d1_retention
			,d7_retention
			,d1_conversion
			,d7_conversion
		
			, percentile_cont(0.25) within group (order by new_installs) over(partition by app,install_source,os) as q1_new_installs
			, percentile_cont(0.75) within group (order by new_installs) over(partition by app,install_source,os) as q3_new_installs


			, percentile_cont(0.25) within group (order by d1_retention) over(partition by app,install_source,os) as q1_d1_retention
			, percentile_cont(0.75) within group (order by d1_retention) over(partition by app,install_source,os) as q3_d1_retention

			, percentile_cont(0.25) within group (order by d7_retention) over(partition by app,install_source,os) as q1_d7_retention
			, percentile_cont(0.75) within group (order by d7_retention) over(partition by app,install_source,os) as q3_d7_retention

			, percentile_cont(0.25) within group (order by d1_conversion) over(partition by app,install_source,os) as q1_d1_conversion
			, percentile_cont(0.75) within group (order by d1_conversion) over(partition by app,install_source,os) as q3_d1_conversion

			, percentile_cont(0.25) within group (order by d7_conversion) over(partition by app,install_source,os) as q1_d7_conversion
			, percentile_cont(0.75) within group (order by d7_conversion) over(partition by app,install_source,os) as q3_d7_conversion


			from temp_table_1
			group by app,install_source--,country
			,os
			,new_installs
			,d1_retention
			,d7_retention
			,d1_conversion
			,d7_conversion
			) t
		group by app,install_source--,country
		,os
		,q1_new_installs
		,q3_new_installs
		,q1_d1_retention
		,q3_d1_retention
		,q1_d7_retention
		,q3_d7_retention
		,q1_d1_conversion
		,q3_d1_conversion
		,q1_d7_conversion
		,q3_d7_conversion
	) a2
	on a1.app = a2.app
	and a1.install_source = a2.install_source
	--and a1.country = a2.country
	and a1.os = a2.os
where 
    a1.install_date< dateadd(day,-7,'_YESTERDAY_')
	and a1.install_date >= dateadd(day,-7*2,'_YESTERDAY_')

)

select
to_char(date_trunc('week',dateadd(day,-7*2,'_YESTERDAY_')),'YYYY-MM-DD') as week_begin_BW_1
,app
,install_source
,os
,case when outliers_new_installs>0 then 'Yes' else 'No' end as outliers_new_installs
,case when outliers_d1_retention>0 then 'Yes' else 'No' end as outliers_d1_retention
,case when outliers_d7_retention>0 then 'Yes' else 'No' end as outliers_d7_retention
,case when outliers_d1_conversion>0 then 'Yes' else 'No' end as outliers_d1_conversion
,case when outliers_d7_conversion>0 then 'Yes' else 'No' end as outliers_d7_conversion
from
(
select 
	app
	,install_source
	--,country
	,os
	,sum(outlier_new_installs) as outliers_new_installs
	,sum(outlier_d1_retention) as outliers_d1_retention
	,sum(outlier_d7_retention) as outliers_d7_retention
	,sum(outlier_d1_conversion) as outliers_d1_conversion
	,sum(outlier_d7_conversion) as outliers_d7_conversion
from temp_table_abnormality
group by 
	app
	,install_source
	--,country
	,os
) t
;
DELETE FROM model.marketing_channel_performance_report_trend where week_begin_bw_1 = to_char(dateadd(day,-7*2,'_YESTERDAY_'),'YYYY-MM-DD');
INSERT INTO model.marketing_channel_performance_report_trend

with temp_table_1 as
(
		select 
		    install_date
			,date_trunc('week',install_date) as install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained
		
		 
		from kpi_processed.agg_marketing_kpi
			where install_date< dateadd(day,-7,'_YESTERDAY_')
			and install_date >= dateadd(day,-7*7,'_YESTERDAY_')
            and app like 'ffs%'
		group by 
		    install_date
			,date_trunc('week',install_date) 
			,app
			,install_source
			--,country
			,os
)

, temp_table_2 as 
(

	select 
		t1.*
		,t2.new_installs as total_new_installs
		,t2.d7_revenue as total_d7_revenue
		,case when t1.install_week_begin = date_trunc('week',dateadd(day,-7*2,'_YESTERDAY_')) then 'wk(-1)'
		      when t1.install_week_begin = date_trunc('week',dateadd(day,-7*3,'_YESTERDAY_')) then 'wk(-2)'
		      when ( t1.install_week_begin >= date_trunc('week',dateadd(day,-7*7,'_YESTERDAY_')) 
		      	    and t1.install_week_begin < date_trunc('week',dateadd(day,-7*3,'_YESTERDAY_')) ) then 'wk(-3:-6)' 
		      else 'other' end as install_week
	from
		(
		select
			install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained

		from temp_table_1
		group by 
			install_week_begin
			,app
			,install_source
			--,country
			,os
		) t1
	left join
		(
		select 
			install_week_begin
			,app
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
		from temp_table_1
		group by 
			install_week_begin
			,app
			--country
			,os
		) t2
		on t1.install_week_begin = t2.install_week_begin
			and t1.app = t2.app
			--and t1.country = t2.country
			and t1.os = t2.os
	
)
, temp_table_3 as 
(
select
	install_week
	--,install_week_begin
	,app
	,install_source
	--,country
	,os
	,avg(new_installs) as new_installs
	,avg(d7_revenue) as d7_revenue
	-- pct
	,sum(new_installs)*1.00/sum(total_new_installs) as pct_new_installs
	,case when sum(d7_revenue)>0 then sum(d7_revenue)*1.00/sum(total_d7_revenue) else 0 end as pct_d7_revenue
    -- retention and conversion calculation
	,sum(d1_retained)*1.00/sum(new_installs) as d1_retention
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d1_payers)*1.00/sum(new_installs) as d1_conversion
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion

	-- ltv calculation
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	from temp_table_2
	group by 
	install_week
	--,install_week_begin
	,app
	,install_source
	--,country
	,os
)

select 
t1.app
,t1.install_source
,t1.os
,to_char(date_trunc('week',dateadd(day,-7*2,'_YESTERDAY_')),'YYYY-MM-DD') as week_begin_BW_1
,t1.new_installs as new_installs_BW_1
,t1.d7_revenue as d7_revenue_BW_1
,t1.pct_new_installs as pct_new_installs_BW_1
,t1.pct_d7_revenue as pct_d7_revenue_BW_1
,t1.d1_retention as d1_retention_BW_1
,t1.d7_retention as d7_retention_BW_1
,t1.d1_conversion as d1_conversion_BW_1
,t1.d7_conversion as d7_conversion_BW_1
,t1.d7_ltv as d7_ltv_BW_1
-- week(-2)
,t2.new_installs as new_installs_BW_2
,t2.d7_revenue as d7_revenue_BW_2
,t2.pct_new_installs as pct_new_installs_BW_2
,t2.pct_d7_revenue as pct_d7_revenue_BW_2
,t2.d1_retention as d1_retention_BW_2
,t2.d7_retention as d7_retention_BW_2
,t2.d1_conversion as d1_conversion_BW_2
,t2.d7_conversion as d7_conversion_BW_2
,t2.d7_ltv as d7_ltv_BW_2
-- week (-3:-6)
,t3.new_installs as new_installs_BW_4
,t3.d7_revenue as d7_revenue_BW_4
,t3.pct_new_installs as pct_new_installs_BW_4
,t3.pct_d7_revenue as pct_d7_revenue_BW_4
,t3.d1_retention as d1_retention_BW_4
,t3.d7_retention as d7_retention_BW_4
,t3.d1_conversion as d1_conversion_BW_4
,t3.d7_conversion as d7_conversion_BW_4
,t3.d7_ltv as d7_ltv_BW_4
-- wow change
,case when t2.new_installs>0 then t1.new_installs*1.00/t2.new_installs-1 else null end as new_installs_WoW
,case when t2.d7_revenue>0 then t1.d7_revenue*1.00/t2.d7_revenue-1 else null end as d7_revenue_WoW
,case when t2.pct_new_installs>0 then t1.pct_new_installs*1.00/t2.pct_new_installs-1 else null end as pct_new_installs_WoW
,case when t2.pct_d7_revenue>0 then t1.pct_d7_revenue*1.00/t2.pct_d7_revenue-1 else null end as pct_d7_revenue_WoW
,case when t2.d1_retention>0 then t1.d1_retention*1.00/t2.d1_retention-1 else null end as d1_retention_WoW
,case when t2.d7_retention>0 then t1.d7_retention*1.00/t2.d7_retention-1 else null end as d7_retention_WoW
,case when t2.d1_conversion>0 then t1.d1_conversion*1.00/t2.d1_conversion-1 else null end as d1_conversion_WoW
,case when t2.d7_conversion>0 then t1.d7_conversion*1.00/t2.d7_conversion-1 else null end as d7_conversion_WoW
,case when t2.d7_ltv>0 then t1.d7_ltv*1.00/t2.d7_ltv-1 else null end as d7_ltv_WoW

from (select * from temp_table_3 where install_week = 'wk(-1)' ) t1
left join temp_table_3 t2
	on t1.app = t2.app
	and t1.install_source = t2.install_source
	and t1.os = t2.os
	and t2.install_week = 'wk(-2)'
left join temp_table_3 t3
	on t1.app = t3.app
	and t1.install_source = t3.install_source
	and t1.os = t3.os
	and t3.install_week = 'wk(-3:-6)'



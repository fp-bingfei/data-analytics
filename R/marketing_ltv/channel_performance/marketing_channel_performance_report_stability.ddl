DROP TABLE IF EXISTS processed.marketing_channel_performance_report_stability CASCADE;
CREATE TABLE processed.marketing_channel_performance_report_stability
(
   run_date           date,
   install_week       varchar(32),
   app                varchar(64),
   install_source     varchar(128),
   --country            varchar(64),
   os                 varchar(32),
   mean               numeric(14,4),
   std                numeric(14,4),
   cov_value          numeric(14,4),
   rk                 integer,
   metrics            varchar(64),
   stability          varchar(32)
);

COMMIT;
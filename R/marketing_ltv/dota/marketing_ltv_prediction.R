# Get parameter
argv <- commandArgs(TRUE)
# Check length
if(length(argv) < 1)
  q()
# Get the date of yesterday
yesterday <- as.Date(argv[1])

# Load SparkR library
library(SparkR)

# Init Spark context and SQL context
sc <- sparkR.init()
sqlContext <- sparkRSQL.init(sc)

# Get data from redshift
raw_data<-loadDF(sqlContext, source="jdbc", driver="com.amazon.redshift.jdbc41.Driver", url="jdbc:redshift://bicluster.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/daota?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true", dbtable="processed.tab_marketing_kpi")

# Aggregate data group by "install_date", "install_source", "country", "os"
raw_data<-agg(groupBy(raw_data, "install_date", "install_source", "country", "os"), new_installs = sum(raw_data$new_installs), d7_revenue = sum(raw_data$d7_revenue), d30_revenue = sum(raw_data$d30_revenue), d60_revenue = sum(raw_data$d60_revenue), d90_revenue = sum(raw_data$d90_revenue), d120_revenue = sum(raw_data$d120_revenue), d7_retained = sum(raw_data$d7_retained), d7_payers = sum(raw_data$d7_payers))

## suppose we need to make prediction for install date ="2015-03-20", in production it should be today()-7
## prepare testing data
#test<-as.Date("2015-03-20")
test<-as.Date(yesterday)
test_data<-collect(subset(raw_data, raw_data$install_date==as.Date(test)))

## prepare training data
#in production, training data = date_add(install_date, -365*2)<install_date<date_add(install_date)
#For example, given install_date=2015-03-20, we use data of install_date between 2013-03-20 and 2015-03-20 as training
#input<-collect(subset(raw_data, raw_data$install_date>="2013-03-20" & raw_data$install_date<"2015-03-20"))
input<-collect(subset(raw_data, raw_data$install_date>=as.Date(yesterday - 365*2) & raw_data$install_date<as.Date(yesterday)))

##dimensions
install_source<-levels(as.factor(test_data$install_source))
country<-levels(as.factor(test_data$country))
os<-levels(as.factor(test_data$os))

##prepare for output
out<-data.frame(input[1,],p30=0,p60=0,p90=0,p120=0,a30=0,a60=0,a90=0,a120=0,s30=0,s60=0,s90=0,s120=0,)

##iteration number
ni<-length(install_source)
nj<-length(country)
nk<-length(os)
## fitting the model for each dimension combination
for(i in 1:ni){ # each install_souce
  sub_data1<-cbind(input,check1=input$install_source==as.character(install_source[i]))
  sub_data1<-base::subset(sub_data1,sub_data1$check1=="TRUE")
  test_data1<-cbind(input,check1=input$install_source==as.character(install_source[i]))
  test_data1<-base::subset(test_data1,test_data1$check1=="TRUE")
    for(j in 1:nj){ # each country
       t1<-sub_data1
       sub_data2<-cbind(t1,check2=t1$country==country[j])
       sub_data2<-base::subset(sub_data2,sub_data2$check2=="TRUE")
       t1<-test_data1
       test_data2<-cbind(t1,check2=t1$country==country[j])
       test_data2<-base::subset(test_data2,test_data2$check2=="TRUE")
          for(k in 1:nk){# each os
              t2<-sub_data2
              sub_data3<-cbind(t2,check3=t2$os==os[k])
              sub_data3<-base::subset(sub_data3,sub_data3$check3=="TRUE")
              t2<-test_data2
              test_data30<-cbind(t2,check3=t2$os==os[k])
              test_data30<-base::subset(test_data30,test_data30$check3=="TRUE")
              test_data3<-base::subset(test_data30,test_data30$install_date==test)
                if(nrow(test_data3)>0){ #make sure we have test data
                  ###d30 prediction
                  train_data30_all<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-30)
                  a30<-mean(train_data30_all$d30_revenue) 
                  s30<-mean(train_data30_all$new_installs)
                  p30<-ifelse(test_data3$d7_retained<10,quantile(train_data30_all$d30_revenue,0.3),test_data3$new_installs*a30/s30) ####initial values
                  
                  train_data30<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-30&sub_data3$install_date>=as.Date(test)-30-90)
                  if(nrow(train_data30)>0) ####use last 3-month data if it is available
                  {a30<-mean(train_data30$d30_revenue)
                  s30<-mean(train_data30$new_installs)
                  p30<-ifelse(test_data3$d7_retained<10,quantile(train_data30$d30_revenue,0.3),test_data3$new_installs*a30/s30)
                  
                  train_data30_0<-base::subset(train_data30,train_data30$d7_revenue<=1) #d7 revenue<=1 case
                  if(test_data3$d7_revenue<=1&nrow(train_data30_0)>0) {p30<-quantile(train_data30_0$d30_revenue,0.9)}
                  train_data30_1<-base::subset(train_data30,train_data30$d7_revenue>1) #d7 reveinnue >1 case
                  if(nrow(train_data30_1)>0){
                  p30<-ifelse(test_data3$d7_revenue>quantile(train_data30_1$d7_revenue,0.95)&test_data3$d7_revenue>=150,test_data3$d7_revenue*3,p30) # 3 is experience number
                  p30<-ifelse(test_data3$d7_revenue>max(train_data30_1$d7_revenue),test_data3$d7_revenue*4,p30)
                  p30<-ifelse(test_data3$d7_revenue>max(train_data30_1$d7_revenue)&test_data3$d7_revenue>=5000,test_data3$d7_revenue*1.2,p30)# experience number
                  }
                  if(test_data3$d7_retained>=10&test_data3$d7_revenue>1&test_data3$d7_revenue<=quantile(train_data30$d7_revenue,0.95))
                  { 
                    
                    #if(test_data3$d7_revenue<=1&nrow(train_data30_0)>0) {p30<-quantile(train_data30_0$d30_revenue,0.7)} #if d7 revenue is too small, use quantile60%
                    if(nrow(train_data30_1)>0)
                    {
                      p30<-quantile(train_data30_1$d30_revenue,0.7)
                      if(nrow(train_data30_1)>=2)
                      {#given more data, we can fit the model
                        fit1<-lm(d30_revenue~d7_revenue-1,train_data30_1) #linear regression model  
                        fit.r<-base::summary(fit1)$adj.r.squared
                        est<-ifelse(is.null(fit1$coefficients),0,predict(fit1,test_data3))
                        est<-ifelse(est<0,0,est)
                        p30<-ifelse(fit.r<0.8,test_data3$new_installs*a30/s30,est)
                        #if test_data has extreme large d7_revenue or model fit is not good, do not use model prediction
                      }
                    }
                  }
                  }
                  p30<-ifelse(is.na(p30),0,p30)
                  a30<-ifelse(is.na(a30),0,a30)
                  s30<-ifelse(is.na(s30),0,s30)
                  ###d60 prediction
                  train_data60_all<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-60) #all data
                  train_data60<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-60&sub_data3$install_date>=as.Date(test)-60-90) #last 90day data
                  p60<-p30*1.2 #initial value
                  a60<-mean(train_data60_all$d60_revenue) #initial value
                  s60<-mean(train_data60_all$new_installs)
                  if(nrow(train_data60)>0){a60<-mean(train_data60$d60_revenue)
                  s60<-mean(train_data60$new_installs)}#update with last 90-day data
                  a60<-ifelse(a60<=a30,a30,a60)
                  s60<-ifelse(a60<=a30,s30,s60)
                  if(nrow(train_data60)>0)
                  {		   
                    train_data60_1<-base::subset(train_data60,train_data60$d30_revenue>1) #check if d30_revenue>1
                    r2<-1
                    if(nrow(train_data60_1)>1) {r2<-train_data60_1$d60_revenue/train_data60_1$d30_revenue} #d30-to-d60 growth rate estimation
                    p<-ifelse(p30>quantile(train_data60$d30_revenue,0.9),0.6,0.8) #check if d30_revenue is extreme value
                    p60<-ifelse(p30<=0,0,p30*quantile(r2,p)) #use growth rate to estimate d60
                    p60<-ifelse(p30>=max(train_data60$d30_revenue)&p30>=5000,p30*1.2,p60)
                  }
                  p60<-ifelse(test_data3$d7_retained<10,test_data3$new_installs*a60/s60,p60)
                  p60<-ifelse(p60<=p30,p30*1.2,p60)
                  p60<-ifelse(is.na(p60),p30,p60)
                  a60<-ifelse(is.na(a60),a30,a60)
                  s60<-ifelse(is.na(s60),s30,s60)
                  ####d90 prediction similar as d60 prediction
                  train_data90_all<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-90)
                  train_data90<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-90&sub_data3$install_date>=as.Date(test)-90-90)
                  p90<-median(train_data90_all$d90_revenue)
                  a90<-mean(train_data90_all$d90_revenue)
                  s90<-mean(train_data90_all$new_installs)
                  if(nrow(train_data90)>0){a90<-mean(train_data90$d90_revenue)
                  s90<-mean(train_data90$new_installs)}
                  a90<-ifelse(a90<=a60,a60,a90)
                  s90<-ifelse(a90<=a60,s60,s90)
                  if(nrow(train_data90)>0)
                  {		    
                    train_data90_1<-base::subset(train_data90,train_data90$d60_revenue>1)
                    r3<-1
                    if(nrow(train_data90_1)>1){r3<-train_data90_1$d90_revenue/train_data90_1$d60_revenue}
                    p<-ifelse(p60>quantile(train_data90$d60_revenue,0.9),0.7,0.8)
                    p90<-ifelse(p60<=0,0,p60*quantile(r3,p))
                  }
                  p90<-ifelse(test_data3$d7_retained<10,test_data3$new_installs*a90/s90,p90)
                  p90<-ifelse(p90<=p60,p60*1.1,p90)
                  p90<-ifelse(is.na(p90),p60,p90)
                  a90<-ifelse(is.na(a90),a60,a90)
                  s90<-ifelse(is.na(s90),s60,s90)
                  ####d120 prediction
                  train_data120_all<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-120)	
                  train_data120<-base::subset(sub_data3,sub_data3$install_date<=as.Date(test)-120&sub_data3$install_date>=as.Date(test)-120-30)
                  p120<-median(train_data120_all$d120_revenue)
                  a120<-mean(train_data120_all$d120_revenue)
                  s120<-mean(train_data120_all$new_installs)
                  if(nrow(train_data120)>0){a120<-mean(train_data120$d120_revenue)
                  s120<-mean(train_data120$new_installs)}
                  a120<-ifelse(a120<=a90,a90,a120)
                  s120<-ifelse(a120<=a90,s90,s120)
                  if(nrow(train_data120)>0)
                  {
                    train_data120_1<-base::subset(train_data120,train_data120$d90_revenue>1)
                    r4<-1
                    if(nrow(train_data120_1)>1) {r4<-train_data120_1$d120_revenue/train_data120_1$d90_revenue}
                    p<-ifelse(p90>quantile(train_data120$d90_revenue,0.9),0.7,0.8)
                    p120<-ifelse(p90<=0,0,p90*quantile(r4,p))
                  }
                  p120<-ifelse(test_data3$d7_retained<10,test_data3$new_installs*a120/s120,p120)
                  p120<-ifelse(p120<=p90,p90,p120)
                  p120<-ifelse(is.na(p120),p90,p120)
                  a120<-ifelse(is.na(a120),a90,a120)
                  s120<-ifelse(is.na(s120),s90,s120)
n<-ncol(test_data3)
test_data3<-test_data3[,-(n-2):-n]
y<-data.frame(test_data3,p30=p30,p60=p60,p90=p90,p120=p120,
a30=a30,a60=a60,a90=a90,a120=a120,s30=s30,s60=s60,s90=s90,s120=s120)
out<-rbind(out,y)	

}
}
}
}
out<-out[-1,]

# Save as JSON file
output<-createDataFrame(sqlContext, out)
output$install_date<-cast(output$install_date, "String")
saveDF(output, path=paste("s3n://com.funplusgame.bidata/model/r/dota/marketing_ltv/", as.character(yesterday), "/", sep=""), source="json",  mode="overwrite")

# Stop Spark context
sparkR.stop()

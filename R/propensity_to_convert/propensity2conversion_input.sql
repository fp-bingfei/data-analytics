with temp_table_levelend as ( ---calculate 90% quantile for level_end
select
	install_source
	,country
	,os
	,app
	,q90
	,count(*)
from
	(
	select
		install_source
		,country
		,os
		,app
		,level_end_to_conversion
		,snsid
		,percentile_cont(0.9) within group(order by level_end_to_conversion) over(partition by install_source,country,os,app) as q90
	from
		( --- raw data
		select
			a.snsid
			,a.install_date
			,a.install_source
			,a.country
			,a.os
			,a.app
			,trunc(a.conversion_ts) as conversion_date
			,max(level_end) as level_end_to_conversion
		from processed.dim_user a
		left join processed.fact_dau_snapshot b
		on a.snsid = b.snsid
		and datediff('day',b.date,trunc(a.conversion_ts))>=0
		where 
			a.app = 'ffs.tango.prod'
			--and a.country in ('Brazil','Egypt','Germany', 'Romania','Russian Federation', 'Turkey','Thailand')
			and a.country in ('Germany', 'Russian Federation')
			and trunc(a.conversion_ts)>=dateadd('day',-180,'2015-02-01') --first conversion within recent 6 month
			and trunc(a.conversion_ts)<'2015-02-01' --- today()
			and a.is_payer = 1
		group by 
			a.snsid
			,a.install_date
			,a.install_source
			,a.country
			,a.os
			,a.app
			,trunc(a.conversion_ts)
		) c
	group by 
		install_source
		,country
		,os
		,app
	    ,level_end_to_conversion
	    ,snsid
	) d
group by 
	install_source
	,country
	,os
	,app
	,q90
)

,temp_table_user as (         

         select 
         a1.*
         ,case when conversion_date is null then 'never_convert'
              when (conversion_date is not null and conversion_time>0) then 'convert_after'
              when (conversion_date is not null and conversion_time<0) then 'convert_before' 
              else 'other' end as conversion_tag
         ,case when days_to_install>0 then sum(case when datediff('day',b1.date,a1.date)>0 then 1 else 0 end)*1.00/days_to_install 
               else sum(case when datediff('day',b1.date,a1.date)<0 then 1 else 0 end)*1.00/1 end as activity2today
         from
         (
            select 
				a.snsid
				,a.date
				,a.app
				,a.country
				,a.os
				,a.level_end
				,case when b.conversion_ts is null then 0 else 1 end as is_payer
				,b.install_date
				,case when b.install_source='Organic' then 'Organic' else 'paid' end as install_source
				,trunc(b.conversion_ts) as conversion_date
				,datediff('day',b.install_date,a.date) as days_to_install
				,datediff('day',b.install_date,trunc(b.conversion_ts)) as days_install2conversion
				,case when b.conversion_ts is not null then datediff('day',a.date,trunc(b.conversion_ts)) 
				      when b.conversion_ts is null then -10000 else 0 end as conversion_time
				,b.vip_level
			from processed.fact_dau_snapshot a
			left join processed.dim_user b
			     on a.snsid = b.snsid
			     and a.app = b.app
			where a.date>=dateadd('day',-90,'2015-02-01') --- today
			    and a.date<'2015-02-01'
					and a.is_new_user=0 --- excluding new user
					and a.is_converted_today=0 --- excluding users converted today
					and a.app = 'ffs.tango.prod'
					and a.country in ('Germany', 'Russian Federation')
          ) a1
         left join processed.fact_dau_snapshot b1
		     on a1.snsid = b1.snsid
		     and a1.app = b1.app
		     and datediff('day',a1.install_date,b1.date)>0 -- installed before today
		     and datediff('day',b1.date,a1.date)>=0 -- dates before today
		   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15

)
,temp_table_top_payer as 
(
select
a.*
,q90
from
	(
	select
		snsid
		,install_source
		,app
		,country
		,total_revenue_usd
		,percentile_cont(0.9) within group(order by total_revenue_usd) over(partition by country,app,install_source) as q90
	from processed.dim_user
	where
	app = 'ffs.tango.prod'
	and country in ('Germany', 'Russian Federation')
	and trunc(conversion_ts)>=dateadd('day',-90,'2015-02-01') --- convert after today
	group by 
		snsid
		,install_source
		,app
		,country
		,total_revenue_usd
	) a 
where total_revenue_usd>=q90
)

select
t1.snsid
,t1.date
,t1.app
,t1.country
,t1.os
,t1.level_end
,t1.is_payer
,t1.install_date
,t1.install_source
,t1.conversion_date
,t1.days_to_install
,t1.conversion_tag
,t1.activity2today
,t2.q90 as level_end_q90
,t2.q90 - t1.level_end as diff_level
,365 - t1.days_to_install as diff_install2conversion
,case when t4.total_revenue_usd is not null then 'BigR' else 'other' end as user_tag
from temp_table_user t1
left join temp_table_levelend t2 
	on t1.install_source = t2.install_source
	and t1.country = t2.country
	and t1.app = t2.app
	and t1.os = t2.os     
left join temp_table_top_payer t4
	on t1.snsid = t4.snsid
	and t1.app = t4.app


input <- read.csv("~/Desktop/Qu Ying/work/models/channel ranking/channel_ranking_data.csv")
####levels
os<-levels(as.factor(input$os))
country<-levels(as.factor(input$country))
ltv_day<-levels(as.factor(input$ltv_day))
install_source<-levels(as.factor(input$install_source))
###prepare output
output1<-data.frame(cbind(os='os',country='country',install_source='installsource',ltv_day=0
                          ,pct_of_total_new_installs=0.0,pct_of_total_revenue=0.0
                          ,ltv_score=0.0,retention_score=0.0,payerconversion_score=0.0
                          ,s_newinstall=0.0,s_ltv=0.0,s_retention=0.0,s_payerconversion=0.1
                          ,cov_newinstall=0.0,cov_ltv=0.0,cov_retention=0.0,cov_payerconversion=0.0
                          ,pct_of_newinstall_outlier=0.0,pct_of_ltv_outlier=0.0,pct_of_retention_outlier=0.0,pct_of_conversion_outlier=0.0)
                     )
output2<-cbind(input[1,]
              ,lwc_ltv=0,upc_ltv=0
              ,lwc_install=0,upc_install=0
              ,lwc_retention=0,upc_retention=0
              ,lwc_conversion=0,upc_conversion=0
              ,newinstall_outlier="",ltv_outlier="",retention_outlier="",conversion_outlier="")
###calculate statistics
for(i in 1:length(os)){
  input1<-cbind(input,check=input$os==os[i]) ### os
  input1<-subset(input1,input1$check==TRUE,select=-check)
  for(j in 1:length(country)){
    input2<-cbind(input1,check=input1$country==country[j]) ###country
    input2<-subset(input2,input2$check==TRUE,select=-check)
    for(k in 1:length(ltv_day)){
      input3<-cbind(input2,check=input2$ltv_day==ltv_day[k]) ###ltv
      input3<-subset(input3,input3$check==TRUE,select=-check)
    
      ltv<-tapply(input3$revenue,as.factor(input3$install_source),sum)/tapply(input3$new_installs,as.factor(input3$install_source),sum)
      ltv<-subset(ltv,is.na(ltv)==FALSE)
      retention<-tapply(input3$new_installs*input3$retention,as.factor(input3$install_source),sum)/tapply(input3$new_installs,as.factor(input3$install_source),sum)
      retention<-subset(retention,is.na(retention)==FALSE)
      conversion<-tapply(input3$new_installs*input3$conversion,as.factor(input3$install_source),sum)/tapply(input3$new_installs,as.factor(input3$install_source),sum)
      conversion<-subset(conversion,is.na(conversion)==FALSE)
      
      for(l in 1:length(install_source)){
        input4<-cbind(input3,check=input3$install_source==install_source[l]) ###install source
        input4<-subset(input4,input4$check==TRUE,select=-check)
        
      if(nrow(input4)>0){
      ### % of total
      pct_of_total_new_installs<-sum(input4$new_installs)/sum(input3$new_installs)
      pct_of_total_revenue<-sum(input4$revenue)/sum(input3$revenue)
      ### scores
      ltv_score<-(sum(input4$revenue)/sum(input4$new_installs)-min(ltv))/(max(ltv)-min(ltv))
      retention_score<-(sum(input4$new_installs*input4$retention)/sum(input4$new_installs)-min(retention))/(max(retention)-min(retention))
      payerconversion_score<-(sum(input4$new_installs*input4$conversion)/sum(input4$new_installs)-min(conversion))/(max(conversion)-min(conversion))
      ###temporal trend
      s_newinstall<-ifelse(sd(input4$new_installs)>0,cor(input4$date_rk,input4$new_installs),0)
      
      
      s_ltv<-ifelse(sd(input4$ltv)>0,cor(input4$date_rk,input4$ltv),0)
     
      
      s_retention<-ifelse(sd(input4$retention)>0,cor(input4$date_rk,input4$retention),0)
      
      
      s_payerconversion<-ifelse(sd(input4$conversion)>0,cor(input4$date_rk,input4$conversion),0)
      
      ###cov
      cov_newinstall<-sd(input4$new_installs)/mean(input4$new_installs)
      cov_ltv<-sd(input4$ltv)/mean(input4$ltv)
      cov_retention<-sd(input4$retention)/mean(input4$retention)
      cov_payerconversion<-sd(input4$conversion)/mean(input4$conversion)
      ### outliers
      upc_install<-mean(input4$new_installs)+1.5*(quantile(input4$new_installs,0.75)-quantile(input4$new_installs,0.25))
      lwc_install<-mean(input4$new_installs)-1.5*(quantile(input4$new_installs,0.75)-quantile(input4$new_installs,0.25))
      lwc_install<-ifelse(lwc_install<upc_install,lwc_install,0)
      newinstall_outlier<-ifelse(ifelse(input4$new_installs>upc_install,1,0)+ifelse(input4$new_installs<lwc_install,1,0)>0,"outlier","normal")
      
      upc_ltv<-mean(input4$ltv)+1.5*(quantile(input4$ltv,0.75)-quantile(input4$ltv,0.25))
      lwc_ltv<-mean(input4$ltv)-1.5*(quantile(input4$ltv,0.75)-quantile(input4$ltv,0.25))
      lwc_ltv<-ifelse(lwc_ltv<upc_ltv,lwc_ltv,0)
      ltv_outlier<-ifelse(ifelse(input4$ltv>upc_ltv,1,0)+ifelse(input4$ltv<lwc_ltv,1,0)>0,"outlier","normal")
      
      upc_retention<-mean(input4$retention)+1.5*(quantile(input4$retention,0.75)-quantile(input4$retention,0.25))
      lwc_retention<-mean(input4$retention)-1.5*(quantile(input4$retention,0.75)-quantile(input4$retention,0.25))
      lwc_retention<-ifelse(lwc_retention<upc_retention,lwc_retention,0)
      retention_outlier<-ifelse(ifelse(input4$retention>upc_retention,1,0)+ifelse(input4$retention<lwc_retention,1,0)>0,"outlier","normal")
      
      upc_conversion<-mean(input4$conversion)+1.5*(quantile(input4$conversion,0.75)-quantile(input4$conversion,0.25))
      lwc_conversion<-mean(input4$conversion)-1.5*(quantile(input4$conversion,0.75)-quantile(input4$conversion,0.25))
      lwc_conversion<-ifelse(lwc_conversion<upc_conversion,lwc_conversion,0)
      conversion_outlier<-ifelse(ifelse(input4$conversion>upc_conversion,1,0)+ifelse(input4$conversion<lwc_conversion,1,0)>0,"outlier","normal")
      out<-cbind(input4,newinstall_outlier,ltv_outlier,retention_outlier,conversion_outlier)
      ### % of outlier
      pct_of_newinstall_outlier<-nrow(subset(out,out$newinstall_outlier=="outlier"))/nrow(input4)
      pct_of_ltv_outlier<-nrow(subset(out,out$ltv_outlier=="outlier"))/nrow(input4)
      pct_of_retention_outlier<-nrow(subset(out,out$retention_outlier=="outlier"))/nrow(input4)
      pct_of_conversion_outlier<-nrow(subset(out,out$conversion_outlier=="outlier"))/nrow(input4)
      ### output
      out1<-data.frame(cbind(os=os[i],country=country[j],install_source=install_source[l],ltv_day=ltv_day[k]
                             ,pct_of_total_new_installs,pct_of_total_revenue
                             ,ltv_score,retention_score,payerconversion_score
                             ,s_newinstall,s_ltv,s_retention,s_payerconversion
                             ,cov_newinstall,cov_ltv,cov_retention,cov_payerconversion
                             ,pct_of_newinstall_outlier,pct_of_ltv_outlier,pct_of_retention_outlier,pct_of_conversion_outlier))
      out2<-cbind(input4
                  ,lwc_ltv,upc_ltv
                  ,lwc_install,upc_install
                  ,lwc_retention,upc_retention
                  ,lwc_conversion,upc_conversion
                  ,newinstall_outlier,ltv_outlier,retention_outlier,conversion_outlier)
      output1<-rbind(output1,out1)
      output2<-rbind(output2,out2)
      }
    }
  }
  }
}
output1<-output1[-1,]
output2<-output2[-1,]
write.table(output1,"~/Desktop/Qu Ying/work/models/channel ranking/channel_ranking_output.txt",row.names=FALSE)
write.table(output2,"~/Desktop/Qu Ying/work/models/channel ranking/channel_ranking_outlier_output.txt",row.names=FALSE)
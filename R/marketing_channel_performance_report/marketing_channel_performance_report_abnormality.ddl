DROP TABLE IF EXISTS model.marketing_channel_performance_report_abnormality CASCADE;
CREATE TABLE model.marketing_channel_performance_report_abnormality
(
   week_begin_bw_1        varchar(32),
   app                    varchar(64),
   install_source         varchar(128),
   os                     varchar(32),
   outliers_new_installs  varchar(32),
   outliers_d1_retention  varchar(32),
   outliers_d7_retention  varchar(32),
   outliers_d1_conversion varchar(32),
   outliers_d7_conversion varchar(32)
);

COMMIT;
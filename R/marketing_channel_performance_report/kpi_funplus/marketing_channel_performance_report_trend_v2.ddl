DROP TABLE IF EXISTS model.marketing_channel_performance_report_trend CASCADE;
CREATE TABLE model.marketing_channel_performance_report_trend
(
   install_week_begin     varchar(32)
   ,app                   varchar(64)
   ,install_source        varchar(128)
   ,os                    varchar(32)        
-- backward 1 week
   ,new_installs          numeric(14,4) DEFAULT 0
   ,d7_revenue            numeric(14,4) DEFAULT 0
   ,cost                  numeric(14,4) DEFAULT 0
   ,adjust_installs       numeric(14,4) DEFAULT 0
   ,pct_new_installs      numeric(14,4) DEFAULT 0
   ,pct_d7_revenue        numeric(14,4) DEFAULT 0
   ,d1_retention          numeric(14,4) DEFAULT 0
   ,d7_retention          numeric(14,4) DEFAULT 0
   ,d1_conversion         numeric(14,4) DEFAULT 0
   ,d7_conversion         numeric(14,4) DEFAULT 0
   ,d7_ltv                numeric(14,4) DEFAULT 0


);

COMMIT;
DROP TABLE IF EXISTS model.marketing_channel_performance_report CASCADE;
CREATE TABLE model.marketing_channel_performance_report
(
   
   app                varchar(64),
   install_source     varchar(128),
   --country            varchar(64),
   os                 varchar(32),
   week_begin_bw_1    varchar(32),
   -- backward 1 week
   new_installs_bw_1     numeric(14,4) DEFAULT 0
   ,d7_revenue_bw_1       numeric(14,4) DEFAULT 0
   ,pct_new_installs_bw_1 numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_bw_1   numeric(14,4) DEFAULT 0
   ,d1_retention_bw_1     numeric(14,4) DEFAULT 0
   ,d7_retention_bw_1     numeric(14,4) DEFAULT 0
   ,d1_conversion_bw_1    numeric(14,4) DEFAULT 0
   ,d7_conversion_bw_1    numeric(14,4) DEFAULT 0
   ,d7_ltv_bw_1           numeric(14,4) DEFAULT 0
-- backward 2 week
   ,new_installs_bw_2     numeric(14,4) DEFAULT 0
   ,d7_revenue_bw_2       numeric(14,4) DEFAULT 0
   ,pct_new_installs_bw_2 numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_bw_2   numeric(14,4) DEFAULT 0
   ,d1_retention_bw_2     numeric(14,4) DEFAULT 0
   ,d7_retention_bw_2     numeric(14,4) DEFAULT 0
   ,d1_conversion_bw_2    numeric(14,4) DEFAULT 0
   ,d7_conversion_bw_2    numeric(14,4) DEFAULT 0
   ,d7_ltv_bw_2           numeric(14,4) DEFAULT 0
-- backward 4 week
   ,new_installs_bw_4     numeric(14,4) DEFAULT 0
   ,d7_revenue_bw_4       numeric(14,4) DEFAULT 0
   ,pct_new_installs_bw_4 numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_bw_4   numeric(14,4) DEFAULT 0
   ,d1_retention_bw_4     numeric(14,4) DEFAULT 0
   ,d7_retention_bw_4     numeric(14,4) DEFAULT 0
   ,d1_conversion_bw_4    numeric(14,4) DEFAULT 0
   ,d7_conversion_bw_4    numeric(14,4) DEFAULT 0
   ,d7_ltv_bw_4           numeric(14,4) DEFAULT 0
--  week over week change
   ,new_installs_wow     numeric(14,4) DEFAULT 0
   ,d7_revenue_wow       numeric(14,4) DEFAULT 0
   ,pct_new_installs_wow numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_wow   numeric(14,4) DEFAULT 0
   ,d1_retention_wow     numeric(14,4) DEFAULT 0
   ,d7_retention_wow     numeric(14,4) DEFAULT 0
   ,d1_conversion_wow    numeric(14,4) DEFAULT 0
   ,d7_conversion_wow    numeric(14,4) DEFAULT 0
   ,d7_ltv_wow           numeric(14,4) DEFAULT 0
-- stability
   ,stability_new_installs  varchar(32),
   stability_d7_revenue    varchar(32),
   stability_d1_retention  varchar(32),
   stability_d7_retention  varchar(32),
   stability_d1_conversion  varchar(32),
   stability_d7_conversion  varchar(32),
   stability_d7_ltv         varchar(32),
-- abnormality
   outliers_new_installs  varchar(32),
   outliers_d1_retention  varchar(32),
   outliers_d7_retention  varchar(32),
   outliers_d1_conversion varchar(32),
   outliers_d7_conversion varchar(32),
   d120_ltv               numeric(14,4) DEFAULT 0
);

COMMIT;
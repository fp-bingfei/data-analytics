DROP TABLE IF EXISTS model.marketing_channel_performance_report_trend CASCADE;
CREATE TABLE model.marketing_channel_performance_report_trend
(
   app                    varchar(64)
   ,install_source        varchar(128)
   ,os                    varchar(32)        
   ,week_begin_bw_1       varchar(32)
-- backward 1 week
   ,new_installs_bw_1     numeric(14,4) DEFAULT 0
   ,d7_revenue_bw_1       numeric(14,4) DEFAULT 0
   ,pct_new_installs_bw_1 numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_bw_1   numeric(14,4) DEFAULT 0
   ,d1_retention_bw_1     numeric(14,4) DEFAULT 0
   ,d7_retention_bw_1     numeric(14,4) DEFAULT 0
   ,d1_conversion_bw_1    numeric(14,4) DEFAULT 0
   ,d7_conversion_bw_1    numeric(14,4) DEFAULT 0
   ,d7_ltv_bw_1           numeric(14,4) DEFAULT 0
-- backward 2 week
   ,new_installs_bw_2     numeric(14,4) DEFAULT 0
   ,d7_revenue_bw_2       numeric(14,4) DEFAULT 0
   ,pct_new_installs_bw_2 numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_bw_2   numeric(14,4) DEFAULT 0
   ,d1_retention_bw_2     numeric(14,4) DEFAULT 0
   ,d7_retention_bw_2     numeric(14,4) DEFAULT 0
   ,d1_conversion_bw_2    numeric(14,4) DEFAULT 0
   ,d7_conversion_bw_2    numeric(14,4) DEFAULT 0
   ,d7_ltv_bw_2           numeric(14,4) DEFAULT 0
-- backward 4 week
   ,new_installs_bw_4     numeric(14,4) DEFAULT 0
   ,d7_revenue_bw_4       numeric(14,4) DEFAULT 0
   ,pct_new_installs_bw_4 numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_bw_4   numeric(14,4) DEFAULT 0
   ,d1_retention_bw_4     numeric(14,4) DEFAULT 0
   ,d7_retention_bw_4     numeric(14,4) DEFAULT 0
   ,d1_conversion_bw_4    numeric(14,4) DEFAULT 0
   ,d7_conversion_bw_4    numeric(14,4) DEFAULT 0
   ,d7_ltv_bw_4           numeric(14,4) DEFAULT 0
--  week over week change
   ,new_installs_wow     numeric(14,4) DEFAULT 0
   ,d7_revenue_wow       numeric(14,4) DEFAULT 0
   ,pct_new_installs_wow numeric(14,4) DEFAULT 0
   ,pct_d7_revenue_wow   numeric(14,4) DEFAULT 0
   ,d1_retention_wow     numeric(14,4) DEFAULT 0
   ,d7_retention_wow     numeric(14,4) DEFAULT 0
   ,d1_conversion_wow    numeric(14,4) DEFAULT 0
   ,d7_conversion_wow    numeric(14,4) DEFAULT 0
   ,d7_ltv_wow           numeric(14,4) DEFAULT 0

);

COMMIT;
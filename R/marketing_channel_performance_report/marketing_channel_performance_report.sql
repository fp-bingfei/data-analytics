DELETE FROM model.marketing_channel_performance_report where week_begin_bw_1 = to_char(dateadd(day,-7*2,'_YESTERDAY_'),'YYYY-MM-DD');
INSERT INTO model.marketing_channel_performance_report

with temp_table_1 as
(
		select 
		   to_char(date_trunc('week',dateadd(day,-7*2,'_YESTERDAY_')),'YYYY-MM-DD') as week_begin_BW_1
			,app
			,install_source
			--,country
			,os
			,sum(d120_revenue)*1.00/sum(new_installs) as d120_ltv	 
		from kpi_processed.agg_marketing_kpi
			where install_date<= dateadd(day,-7-120,'_YESTERDAY_')
			and install_date >= dateadd(day,-7-120-30,'_YESTERDAY_')
			and app like 'ffs%'

		group by 
		    to_char(date_trunc('week',dateadd(day,-7*2,'_YESTERDAY_')),'YYYY-MM-DD') 
			,app
			,install_source
			--,country
			,os
)
select 
t1.*
,t2.stability_new_installs
,t2.stability_d7_revenue
,t2.stability_d1_retention
,t2.stability_d7_retention
,t2.stability_d1_conversion
,t2.stability_d7_conversion
,t2.stability_d7_ltv

,t3.outliers_new_installs
,t3.outliers_d1_retention
,t3.outliers_d7_retention
,t3.outliers_d1_conversion
,t3.outliers_d7_conversion
,t4.d120_ltv
from model.marketing_channel_performance_report_trend t1
left join model.marketing_channel_performance_report_stability t2
	on t1.week_begin_bw_1 = t2.week_begin_bw_1
	and t1.app = t2.app
	and t1.install_source = t2.install_source
	and t1.os = t2.os
left join model.marketing_channel_performance_report_abnormality t3
	on t1.week_begin_bw_1 = t3.week_begin_bw_1
	and t1.app = t3.app
	and t1.install_source = t3.install_source
	and t1.os = t3.os
left join temp_table_1 t4
    on t1.week_begin_bw_1 = t4.week_begin_bw_1
    and t1.app = t4.app
	and t1.install_source = t4.install_source
	and t1.os = t4.os
;

DROP TABLE IF EXISTS model.marketing_channel_performance_report CASCADE;
CREATE TABLE model.marketing_channel_performance_report
(
   
   install_week_begin varchar(32),
   app                varchar(64),
   install_source     varchar(128),
   --country            varchar(64),
   os                 varchar(32),
   -- backward 1 week
    new_installs     numeric(14,4) DEFAULT 0
   ,d7_revenue       numeric(14,4) DEFAULT 0
   ,cost             numeric(14,4) DEFAULT 0
   ,adjust_installs  numeric(14,4) DEFAULT 0
   ,pct_new_installs numeric(14,4) DEFAULT 0
   ,pct_d7_revenue   numeric(14,4) DEFAULT 0
   ,d1_retention     numeric(14,4) DEFAULT 0
   ,d7_retention     numeric(14,4) DEFAULT 0
   ,d1_conversion    numeric(14,4) DEFAULT 0
   ,d7_conversion    numeric(14,4) DEFAULT 0
   ,d7_ltv           numeric(14,4) DEFAULT 0

-- stability
   ,stability_new_installs  varchar(32),
   stability_d7_revenue    varchar(32),
   stability_d1_retention  varchar(32),
   stability_d7_retention  varchar(32),
   stability_d1_conversion  varchar(32),
   stability_d7_conversion  varchar(32),
   stability_d7_ltv         varchar(32),
-- abnormality
   outliers_new_installs  varchar(32),
   outliers_d1_retention  varchar(32),
   outliers_d7_retention  varchar(32),
   outliers_d1_conversion varchar(32),
   outliers_d7_conversion varchar(32),
   d120_ltv               numeric(14,4) DEFAULT 0
);

COMMIT;
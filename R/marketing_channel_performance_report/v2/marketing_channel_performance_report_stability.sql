DELETE FROM model.marketing_channel_performance_report_stability where install_week_begin = to_char(dateadd(day,-7,'_YESTERDAY_'),'YYYY-MM-DD');
INSERT INTO model.marketing_channel_performance_report_stability

with temp_table_1 as
(
		select 
		    install_date
			--,date_trunc('week',install_date) as install_week_begin
			,app
			,install_source
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained			
			-- retention and conversion calculation
			,sum(d1_retained)*1.00/sum(new_installs) as d1_retention
			,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
			,sum(d1_payers)*1.00/sum(new_installs) as d1_conversion
			,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion

			-- ltv calculation
	        ,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv		 
		from kpi_processed.agg_marketing_kpi
			where install_date< dateadd(day,-7,'_YESTERDAY_')
			and install_date >= dateadd(day,-7-30,'_YESTERDAY_')
			and app like 'ffs%'

		group by 
		    install_date
			--,date_trunc('week',install_date) 
			,app
			,install_source
			--,country
			,os
)

,temp_table_stability as
(
-- new installs
	select 
	a1.*
	,rank() over(partition by app,os order by cov_new_installs) as rk_new_installs
	,rank() over(partition by app,os order by cov_d7_revenue) as rk_d7_revenue
	,rank() over(partition by app,os order by cov_d1_retention) as rk_d1_retention
	,rank() over(partition by app,os order by cov_d7_retention) as rk_d7_retention
	,rank() over(partition by app,os order by cov_d1_conversion) as rk_d1_conversion
	,rank() over(partition by app,os order by cov_d7_conversion) as rk_d7_conversion
	,rank() over(partition by app,os order by cov_d7_ltv) as rk_d7_ltv
	
	from
	(select 
		app
		,install_source
		--,country
		,case when os like '%iOS' then 'iOS'
			      when os like '%android' then 'Android'
			      when os like '%amazon' then 'Amazon'
			      when os like '%360' then '360'
			      when os like '%tango' then 'tango'
			      else 'others' end as os

		,case when avg(new_installs)>0 then cast(stddev(new_installs) as real)/avg(new_installs) else null end as cov_new_installs
		,case when avg(d7_revenue)>0 then cast(stddev(d7_revenue) as real)/avg(d7_revenue) else null end as cov_d7_revenue
		,case when avg(d1_retention)>0 then cast(stddev(d1_retention) as real)/avg(d1_retention) else null end as cov_d1_retention
		,case when avg(d7_retention)>0 then cast(stddev(d7_retention) as real)/avg(d7_retention) else null end as cov_d7_retention
		,case when avg(d1_conversion)>0 then cast(stddev(d1_conversion) as real)/avg(d1_conversion) else null end as cov_d1_conversion
		,case when avg(d7_conversion)>0 then cast(stddev(d7_conversion) as real)/avg(d7_conversion) else null end as cov_d7_conversion
		,case when avg(d7_ltv)>0 then cast(stddev(d7_ltv) as real)/avg(d7_ltv) else null end as cov_d7_ltv

	from temp_table_1 
	group by 
     1,2,3
	) a1

)
 
select 
to_char(date_trunc('week',dateadd(day,-7,'_YESTERDAY_')),'YYYY-MM-DD') as install_week_begin
,a1.app
,a1.os
,a1.install_source
,case when a1.rk_new_installs<round(0.3*max_rk_new_installs) then 'high' 
      when a1.rk_new_installs>round(0.7*max_rk_new_installs) then 'low'
      else 'medium' end as stability_new_installs
,case when a1.rk_d7_revenue<round(0.3*max_rk_d7_revenue) then 'high' 
      when a1.rk_d7_revenue>round(0.7*max_rk_d7_revenue) then 'low'
      else 'medium' end as stability_d7_revenue
,case when a1.rk_d1_retention<round(0.3*max_rk_d1_retention) then 'high' 
      when a1.rk_d1_retention>round(0.7*max_rk_d1_retention) then 'low'
      else 'medium' end as stability_d1_retention
,case when a1.rk_d7_retention<round(0.3*max_rk_d7_retention) then 'high' 
      when a1.rk_d7_retention>round(0.7*max_rk_d7_retention) then 'low'
      else 'medium' end as stability_d7_retention
,case when a1.rk_d1_conversion<round(0.3*max_rk_d1_conversion) then 'high' 
      when a1.rk_d1_conversion>round(0.7*max_rk_d1_conversion) then 'low'
      else 'medium' end as stability_d1_conversion
,case when a1.rk_d7_conversion<round(0.3*max_rk_d7_conversion) then 'high' 
      when a1.rk_d7_conversion>round(0.7*max_rk_d7_conversion) then 'low'
      else 'medium' end as stability_d7_conversion
,case when a1.rk_d7_ltv<round(0.3*max_rk_d7_ltv) then 'high' 
      when a1.rk_d7_ltv>round(0.7*max_rk_d7_ltv) then 'low'
      else 'medium' end as stability_d7_ltv
from temp_table_stability a1
left join 
(
select 
	app
	--,country
	,os
	,max(rk_new_installs) as max_rk_new_installs
	,max(rk_d7_revenue) as max_rk_d7_revenue
	,max(rk_d1_retention) as max_rk_d1_retention
	,max(rk_d7_retention) as max_rk_d7_retention
	,max(rk_d1_conversion) as max_rk_d1_conversion
	,max(rk_d7_conversion) as max_rk_d7_conversion
	,max(rk_d7_ltv) as max_rk_d7_ltv
from temp_table_stability
group by 
 1,2
) a2 
on a1.app = a2.app
--and a1.country = a2.country
and a1.os = a2.os

;

DELETE FROM model.marketing_channel_performance_report_trend where install_week_begin >= to_char(dateadd(day,-7*3,'_YESTERDAY_'),'YYYY-MM-DD');
INSERT INTO model.marketing_channel_performance_report_trend

with temp_table_1 as
(
	select 
	a.*
	,b.cost
	,c.adjust_installs
      from
		(select 
		    --install_date
			to_char(date_trunc('week',install_date),'YYYY-MM-DD') as install_week_begin
			,app
			,install_source
			--,country
			,case when os like '%iOS' then 'iOS'
			      when os like '%android' then 'Android'
			      when os like '%amazon' then 'Amazon'
			      when os like '%360' then '360'
			      when os like '%tango' then 'tango'
			      else 'others' end as os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
			,sum(d1_payers) as d1_payers
			,sum(d7_payers) as d7_payers
			,sum(d1_retained) as d1_retained
			,sum(d7_retained) as d7_retained
		
		 
		from kpi_processed.agg_marketing_kpi
			where install_date< '_YESTERDAY_'
			and install_date >= dateadd(day,-7*3,'_YESTERDAY_')
            and app like 'ffs%'
		group by 
		    1,2,3,4
			) a
		left join 
		(
          select 
		    to_char(date_trunc('week',install_date),'YYYY-MM-DD') as install_week_begin
		    --install_date
			,app
			,install_source
			--,country
			,case when os like '%iOS' then 'iOS'
			      when os like '%android' then 'Android'
			      when os like '%amazon' then 'Amazon'
			      when os like '%360' then '360'
			      when os like '%tango' then 'tango'
			      else 'others' end as os
			,sum(cost) as cost	 
		from kpi_processed.agg_marketing_kpi_singular
			where install_date< '_YESTERDAY_'
			and install_date >= dateadd(day,-7*3,'_YESTERDAY_')
			and app like 'ffs%'
		group by 
		    1,2,3,4
		) b
			on a.install_week_begin = b.install_week_begin
			and a.app = b.app
			and a.install_source = b.install_source
			and a.os = b.os
		left join 
		(
          select 
		   to_char(date_trunc('week',install_date),'YYYY-MM-DD') as install_week_begin
			--install_date
			,app_id
			,case when install_source = 'App Turbo' then 'Appturbo' 
			      when install_source = 'Off-Facebook Installs' then 'Facebook Installs'
			      when install_source = 'Applift' then 'AppLift'
			      else install_source end as install_source
			--,country
			,os
			,sum(all_adjust_installs) as adjust_installs	 
		from public.adjust_bi_discrepancy
			where install_date< '_YESTERDAY_'
			and install_date >= dateadd(day,-7*3,'_YESTERDAY_')
			and app_id like 'ffs%'
		group by 
		    1,2,3,4

		) c
		    on a.install_week_begin = c.install_week_begin
			and a.app = c.app_id
			and a.install_source = c.install_source
			and a.os = c.os
)

, temp_table_2 as 
(

	select 
		t1.*
		,t2.new_installs as total_new_installs
		,t2.d7_revenue as total_d7_revenue
	from temp_table_1 t1
	left join
		(
		select 
			install_week_begin
			,app
			--,country
			,os
			,sum(new_installs) as new_installs
			,sum(d7_revenue) as d7_revenue
		from temp_table_1
		group by 
             1,2,3
		) t2
		on t1.install_week_begin = t2.install_week_begin
			and t1.app = t2.app
			--and t1.country = t2.country
			and t1.os = t2.os
	
)

select
     install_week_begin
	,app
	,install_source
	--,country
	,os
	,sum(new_installs) as new_installs
	,sum(d7_revenue) as d7_revenue
	,sum(cost) as cost
	,sum(adjust_installs) as adjust_installs
	-- pct
	,sum(new_installs)*1.00/sum(total_new_installs) as pct_new_installs
	,case when sum(d7_revenue)>0 then sum(d7_revenue)*1.00/sum(total_d7_revenue) else 0 end as pct_d7_revenue
    -- retention and conversion calculation
	,sum(d1_retained)*1.00/sum(new_installs) as d1_retention
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d1_payers)*1.00/sum(new_installs) as d1_conversion
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion


	-- ltv calculation
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	from temp_table_2
	group by
	1,2,3,4 
     
UNION ALL 

select
     install_week_begin
	,app
	,install_source
	--,country
	,'overall' as os
	,sum(new_installs) as new_installs
	,sum(d7_revenue) as d7_revenue
	,sum(cost) as cost
	,sum(adjust_installs) as adjust_installs
	-- pct
	,sum(new_installs)*1.00/sum(total_new_installs) as pct_new_installs
	,case when sum(d7_revenue)>0 then sum(d7_revenue)*1.00/sum(total_d7_revenue) else 0 end as pct_d7_revenue
    -- retention and conversion calculation
	,sum(d1_retained)*1.00/sum(new_installs) as d1_retention
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d1_payers)*1.00/sum(new_installs) as d1_conversion
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion


	-- ltv calculation
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	from temp_table_2
	group by
	1,2,3,4

;

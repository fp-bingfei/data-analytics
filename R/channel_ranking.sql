with temp_table as 
(
select 
a1.*
,rank() over(partition by a1.os,a1.country,a1.install_source order by a1.install_date) as date_rk

from
(
	select 
	os
	,country
	,install_source
	,install_date
	,sum(new_installs) as new_installs

	,sum(d1_revenue) as d1_revenue
	,sum(d7_revenue) as d7_revenue
	,sum(d30_revenue) as d30_revenue
	,sum(d60_revenue) as d60_revenue
	,sum(d90_revenue) as d90_revenue
	,sum(d120_revenue) as d120_revenue

    ,sum(d1_revenue)*1.00/sum(new_installs) as  d1_ltv
	,sum(d7_revenue)*1.00/sum(new_installs) as  d7_ltv
	,sum(d30_revenue)*1.00/sum(new_installs) as d30_ltv
	,sum(d60_revenue)*1.00/sum(new_installs) as d60_ltv
	,sum(d90_revenue)*1.00/sum(new_installs) as d90_ltv
	,sum(d120_revenue)*1.00/sum(new_installs) as d120_ltv


	,sum(d1_payers)*1.00/sum(new_installs) as d1_conversion
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion
	,sum(d30_payers)*1.00/sum(new_installs) as d30_conversion
	,sum(d60_payers)*1.00/sum(new_installs) as d60_conversion
	,sum(d90_payers)*1.00/sum(new_installs) as d90_conversion
	,sum(d120_payers)*1.00/sum(new_installs) as d120_conversion

	,sum(d1_retained)*1.00/sum(new_installs) as d1_retention
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d30_retained)*1.00/sum(new_installs) as d30_retention
	,sum(d60_retained)*1.00/sum(new_installs) as d60_retention
	,sum(d90_retained)*1.00/sum(new_installs) as d90_retention
	,sum(d120_retained)*1.00/sum(new_installs) as d120_retention

	from processed.tab_marketing_kpi 

	where 
	country in ('United States','Germany')
	and os in ('Android','iOS')
	--and install_source in ('Organic','Facebook Installs')
	and install_date>=dateadd('day',-90,'2015-09-01')
	and install_date<'2015-09-01'

	group by 
	os
	,country
	,install_source
	,install_date
) a1
)

select 
os
,country
,install_source
,install_date
,date_rk
,1 as ltv_day
,new_installs
,d1_revenue as revenue
,d1_ltv as ltv
,d1_conversion as conversion
,d1_retention as retention
from temp_table b1


union all

select 
os
,country
,install_source
,install_date
,date_rk
,7 as ltv_day
,new_installs
,d7_revenue as revenue
,d7_ltv as ltv
,d7_conversion as conversion
,d7_retention as retention
from temp_table b1

union all

select 
os
,country
,install_source
,install_date
,date_rk
,30 as ltv_day
,new_installs
,d30_revenue as revenue
,d30_ltv as ltv
,d30_conversion as conversion
,d30_retention as retention
from temp_table b1

union all

select 
os
,country
,install_source
,install_date
,date_rk
,60 as ltv_day
,new_installs
,d60_revenue as revenue
,d60_ltv as ltv
,d60_conversion as conversion
,d60_retention as retention
from temp_table b1

union all

select 
os
,country
,install_source
,install_date
,date_rk
,90 as ltv_day
,new_installs
,d90_revenue as revenue
,d90_ltv as ltv
,d90_conversion as conversion
,d90_retention as retention
from temp_table b1

union all

select 
os
,country
,install_source
,install_date
,date_rk
,120 as ltv_day
,new_installs
,d120_revenue as revenue
,d120_ltv as ltv
,d120_conversion as conversion
,d120_retention as retention
from temp_table b1


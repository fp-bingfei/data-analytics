DELETE FROM brand_mkt.input_data;
INSERT INTO brand_mkt.input_data

with temp as (
select 
	install_date
	,case when install_source = 'Organic' then 'organic' else 'paid' end as install_source
	,country
	,app
	,sum(new_installs) as installs
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d30_retained)*1.00/sum(new_installs) as d30_retention
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion
	,sum(d30_payers)*1.00/sum(new_installs) as d30_conversion
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	,sum(d30_revenue)*1.00/sum(new_installs) as d30_ltv

from kpi_processed.agg_marketing_kpi
where 
    app like 'ffs%'
	and install_date >='2016-05-01' --- control start
	and install_date <=dateadd(day,-7,'_YESTERDAY_')  --- test end 
	and country in ('Canada', 'Australia','United States','United Kingdom') -- campaign impacted countries
group  by 1,2,3,4
)
, temp2 as ( -- 14 days return users
select 
date, country, app_id, count(distinct user_key) as return_users
from
	(
	select 
	a.date
	,a.app_id
	,a.country
	,a.user_key
	,count(distinct b.date) as login_14
	from
		(
		select 
			date
			,app_id
			,country
			,user_key
			,count(*)
		from kpi_processed.fact_dau_snapshot
		where 
			app_id like 'ffs%'
			and date >= '2016-05-01' -- control start
			and date <=dateadd(day,-7,'_YESTERDAY_') -- test end
			and country in ('Canada', 'Australia','United States','United Kingdom') 
		group by 1,2,3,4
		) a
		left join kpi_processed.fact_dau_snapshot b 
			on 
			a.app_id = b.app_id
			and a.country = b.country
			and a.user_key = b.user_key
	        and datediff('day', a.date, b.date) <0
	        and datediff('day', a.date, b.date) >= -14
	group by 1,2,3,4
	) 
where login_14<=0
group by 1,2,3
)

select 
install_date as date, app, install_source, country, 'installs' as metrics, installs as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd7_retention' as metrics, d7_retention as value
from temp

union all

select 
install_date as date,app, install_source, country, 'd7_conversion' as metrics, d7_conversion as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd7_ltv' as metrics, d7_ltv as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd30_retention' as metrics, d30_retention as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd30_conversion' as metrics, d30_conversion as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd30_ltv' as metrics, d30_ltv as value
from temp

union all

select date, app_id, 'All' as install_source, country, 'return_users' as metrics, return_users as value
from temp2
;

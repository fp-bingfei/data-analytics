DELETE FROM brand_mkt.input_data_language;
INSERT INTO brand_mkt.input_data_language


with events_date as (
	select 
		min(control_start_date) as min_date
		,max(campaign_end_date) as max_date
	from brand_mkt.brand_mkt_events
	where 
		campaign_end_date > dateadd(day,-7,'_YESTERDAY_')
		and campaign_end_date <='_YESTERDAY_'

)
,events_game as (
	select game, count(*)
	from brand_mkt.brand_mkt_events
	where 
		campaign_end_date > dateadd(day,-7,'_YESTERDAY_')
		and campaign_end_date <='_YESTERDAY_'
	group by game
)
,top_languages as (
	select t1.app_id,t1.language,count(distinct t1.user_key) as installs
	from kpi_processed.fact_dau_snapshot t1
	join events_game t2
	     on split_part(t1.app_id,'.',1) = t2.game
	where 
		t1.date <='_YESTERDAY_'
		and t1.date> dateadd(day,-7,'_YESTERDAY_')
		and t1.is_new_user = 1
	group by 1,2
	having count(distinct user_key)>=1000 
)
,temp as (
	select 
		t1.date as install_date
		,case when t5.install_source = 'Organic' then 'organic' else 'paid' end as install_source
		,t1.language
		,t1.app_id as app
		,count(distinct t1.user_key) as installs

	from kpi_processed.fact_dau_snapshot t1 
	join events_game t2
	     on split_part(t1.app_id,'.',1) = t2.game
	join events_date t3
		on t1.date >= t3.min_date
		and t1.date <= t3.max_date
	join top_languages t4
		on t1.app_id = t4.app_id
		and t1.language = t4.language
	left join kpi_processed.dim_user t5
        on t1.app_id = t5.app_id
		and t1.user_key = t5.user_key
	where t1.is_new_user = 1
	group  by 1,2,3,4
)
,temp2 as (
select 
t1.*
from brand_mkt.return_users_14_language t1 
join events_game t2
on split_part(t1.app_id,'.',1) = t2.game
join events_date t3
	on t1.date >= t3.min_date
	and t1.date <= t3.max_date
join top_languages t4
	on t1.app_id = t4.app_id
	and t1.language = t4.language
)

,temp3 as (
select 
install_date as date, app, install_source, language, 'installs' as metrics, installs as value
from temp

union all

select 
date, app_id, 'All' as install_source, language, 'return_users' as metrics, return_users as value
from temp2
)

select 
	a.event_type, a.event_name,a.game,a.country as campaign_country,a.campaign_start_date,a.campaign_end_date,a.control_start_date,a.control_end_date
	,b.*
from (select * from brand_mkt.brand_mkt_events where campaign_end_date > dateadd(day,-7,'_YESTERDAY_') and campaign_end_date <='_YESTERDAY_') a 
left join temp3 b
on split_part(b.app,'.',1) = a.game
	and b.date>=a.control_start_date
	and b.date<=a.campaign_end_date
;
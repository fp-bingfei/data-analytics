# brand marketing campaign evaluation
# Creation date: 2016-06-28
# Get parameter
argv <- commandArgs(TRUE)
# Check length
if(length(argv) < 1)
  q()
# Get the date of yesterday
yesterday <- as.Date(argv[1])

# Load SparkR library
library(SparkR)

# Init Spark context and SQL context
sc <- sparkR.init()
sqlContext <- sparkRSQL.init(sc)

# import data from redshift 
raw_data<-loadDF(sqlContext, source="jdbc", driver="com.amazon.redshift.jdbc41.Driver"
                 , url="jdbc:redshift://kpi-diandian.cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/kpi?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true"
                 , dbtable="brand_mkt.input_data_language")
raw_data <- collect(raw_data)



library(CausalImpact)
#####################################
###build a function for evaluation###
#####################################
CausalImpactEvaluation <- function(input, pre, post,season,a){
# input: a vector of the metrics that you want to evaluate, e.g. installs
# pre: day number before campaign, i.e. row number of the data, for example c(30,43)
# post: day number after campaign, i.e. row number of the data, for example c(44,61)
# metrics: for example, metrics = "installs", "d7_retention"
# season: number of seasonality, for example nseason = 7
# a: for example alpha = 0.1 will give 90% confidence interval
impact <- CausalImpact(input, pre.period = pre, post.period = post
                         ,model.args = list(prior.level.sd = 0.1, nseasons = season), alpha = a)
 output <- data.frame(actual=0,pred=0) 
 if(length(impact$summary$Pred[1])>0){                        
  output <- data.frame(actual = ifelse(is.na(impact$series$point.pred),"",impact$series$response)
                       , pred = ifelse(is.na(impact$series$point.pred),"",impact$series$point.pred)
                       , pred.lower = ifelse(is.na(impact$series$point.pred.lower),"",impact$series$point.pred.lower)
                       , pred.upper = ifelse(is.na(impact$series$point.pred.upper),"",impact$series$point.pred.upper)
                        #summary
                       , avg_actual = impact$summary$Actual[1], cum_actual = impact$summary$Actual[2]
                       , avg_pred = impact$summary$Pred[1], cum_pred = impact$summary$Pred[2]
                       , avg_pred_lower = impact$summary$Pred.lower[1], cum_pred_lower = impact$summary$Pred.lower[2]
                       , avg_pred_upper = impact$summary$Pred.upper[1], cum_pred_upper = impact$summary$Pred.upper[2]
                       , avg_effect = impact$summary$AbsEffect[1], cum_effect = impact$summary$AbsEffect[2]
                       , avg_effect_lower = impact$summary$AbsEffect.lower[1], cum_effect_lower = impact$summary$AbsEffect.lower[2]
                       , avg_effect_upper = impact$summary$AbsEffect.upper[1], cum_effect_upper = impact$summary$AbsEffect.upper[2]
                       , p_value = impact$summary$p[1]
                       , alpha = a
                         )  
 }
 output
}
###################################
###calculate pre and post period###
###################################
date_num <- function(day, period){
#day: vector of dates which you want to get the number e.g. c('2016-01-01','2016-01-02')
#period: vector of all dates within the period e.g. c('2016-01-01','2016-01-02','2016-01-03')
dates_all <- data.frame(number = row(as.matrix(period)), date = period)  
n <- rep(0, length(day))
for(i in 1:length(day)){
  n[i] <- ifelse(as.Date(day[i])>max(as.Date(period)), length(period), dates_all[as.character(dates_all$date)==day[i], 1])
}
n
}
################
###evaluation###
################

## prepare output
out <- data.frame(campaign_name="", app="", language="", install_source="",metrics="",date=""
                  , pre.period.start = "", pre.period.end = ""
                  , post.period.start = "", post.period.end = ""
                  , actual = 0, pred = 0, pred.lower = 0, pred.upper = 0
                  #summary
                  , avg_actual = 0, cum_actual = 0, avg_pred = 0, cum_pred = 0
                  , avg_pred_lower = 0, cum_pred_lower = 0, avg_pred_upper = 0, cum_pred_upper = 0
                  , avg_effect = 0, cum_effect = 0, avg_effect_lower = 0, cum_effect_lower = 0
                  , avg_effect_upper = 0, cum_effect_upper = 0
                  , p_value = 0, alpha =0)
## data layers
input <- raw_data
if(nrow(input)>0){
campaign <- split(input,input$event_name)

for(k in campaign){
  input.campaign <- input[input$event_name==k$event_name[1], ]
  dates <- levels(as.factor(input.campaign$date))
  campaign_start_num <- date_num(input.campaign$campaign_start_date[1], dates)
  campaign_end_num <- date_num(input.campaign$campaign_end_date[1], dates)
  control_start_num <- date_num(input.campaign$control_start_date[1], dates)
  control_end_num <- date_num(input.campaign$control_end_date[1], dates)
  pre.period <- c(control_start_num, control_end_num)
  post.period <- c(campaign_start_num, campaign_end_num)
  campaign_name <- k$event_name[1]
app <- split(input.campaign, input.campaign$app)
language <- split(input.campaign, input.campaign$language)
metrics_group <- split(input.campaign, input.campaign$metrics)
install_source <- split(input.campaign, input.campaign$install_source)

## parameters

nseason <- 7
alpha <- 0.1


for(i in app){
sub_app <- subset(input.campaign, input.campaign$app==i$app[1])
  for(j in language){
    sub_language <- subset(sub_app, sub_app$language==j$language[1])
    for(m in metrics_group){
      sub_metrics <- subset(sub_language, sub_language$metrics==m$metrics[1])
      for(n in install_source){
        sub <- subset(sub_metrics, sub_metrics$install_source==n$install_source[1])
    sub <- sub[order(sub$date), ]
    y <- unlist(sub$value)
    if(quantile(y,0.9)>0&length(y)>=max(pre.period,post.period)){
    model.output <- CausalImpactEvaluation(input=y, pre=pre.period, post=post.period,season=nseason,a=alpha)
    if(nrow(model.output)>1){
    output <- cbind(campaign_name, app=sub$app[1], language= sub$language[1], install_source=as.character(sub$install_source[1])
                    , metrics= as.character(sub$metrics[1]), date = as.character(sub$date)
                    , pre.period.start = as.character(dates[pre.period[1]]), pre.period.end = as.character(dates[pre.period[2]])
                    , post.period.start = as.character(dates[post.period[1]]), post.period.end = as.character(dates[post.period[2]])
                   ,model.output)
    out <- rbind(out, output)
      }
     }
                              }
                            }
                     }
                  }
}
}
out <- out[-1, ]
colnames(out)<-c("campaign_name", "app", "language", "install_source", "metrics", "date",
"pre_period_start", "pre_preiod_end", "post_period_start", "post_period_end",
"actual", "pred", "pred_lower", "pred_upper", "avg_actual", "cum_actual",
"avg_pred", "cum_pred", "avg_pred_lower", "cum_pred_lower", "avg_pred_upper", "cum_pred_upper",
"avg_effect", "cum_effect", "avg_effect_lower", "cum_effect_lower", "avg_effect_upper", "cum_effect_upper",
"p_value", "alpha")
if(nrow(out) > 0)
{
  output<-createDataFrame(sqlContext, out)
  output$date<-cast(output$date, "String")
  saveDF(output, path=paste("s3n://com.funplusgame.bidata/model/r/brand_mkt/kpi_diandian/language", as.character(yesterday), "/", sep=""), source="json",  mode="overwrite")
}
# Stop Spark context
sparkR.stop()
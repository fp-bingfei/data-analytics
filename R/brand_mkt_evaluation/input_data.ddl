DROP TABLE IF EXISTS brand_mkt.input_data;
CREATE TABLE  brand_mkt.input_data (
date             date,
app              varchar(32),
install_source   varchar(64),
country          varchar(32),
metrics          varchar(32),
value            numeric(14,4)
)
DELETE FROM brand_mkt.return_users_14_language;
INSERT INTO brand_mkt.return_users_14_language
with events_game as (
select game, count(*)
from brand_mkt.brand_mkt_events
where 
	campaign_end_date > dateadd(day,-7,'_YESTERDAY_')
	and campaign_end_date <='_YESTERDAY_'
group by game
)

select f1.date, f1.language, f1.app_id, count(distinct f1.user_key) from
	(select 
	 date, user_key, language, app_id from
		(select 
			 user_key
			 , date
			 , language
			 , app_id
			 , rank() over (partition by user_key, app_id order by date desc) as rnum 
		 from kpi_processed.fact_dau_snapshot t1
		 join events_game t2
	        on split_part(t1.app_id,'.',1) = t2.game
		 where 
			 date > dateadd(day,-90,'_YESTERDAY_')
		 )
	where rnum = 1 
	) f1 
join 
	(
	select date, user_key from
		(
		select 
			user_key
			, date
			, rank() over (partition by user_key, app_id order by date desc) as rnum 
		from kpi_processed.fact_dau_snapshot t1
		join events_game t2
			        on split_part(t1.app_id,'.',1) = t2.game
		)
	where rnum = 2 
	) f2
on f1.user_key = f2.user_key
where datediff('day', f2.date, f1.date) > 14
group by 1,2,3
;



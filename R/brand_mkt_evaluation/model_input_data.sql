DELETE FROM brand_mkt.input_data;
INSERT INTO brand_mkt.input_data


with events_date as (
select 
	min(control_start_date) as min_date
	,max(campaign_end_date) as max_date
from brand_mkt.brand_mkt_events
where 
	campaign_end_date > dateadd(day,-7,'_YESTERDAY_')
	and campaign_end_date <='_YESTERDAY_'

)
,events_game as (
select game,count(*)
from brand_mkt.brand_mkt_events
where 
	campaign_end_date > dateadd(day,-7,'_YESTERDAY_')
	and campaign_end_date <='_YESTERDAY_'
group by 1
)
,top_countries as (
select t1.app,t1.country,sum(new_installs)
from kpi_processed.agg_marketing_kpi t1
join events_game t2
on split_part(t1.app,'.',1) = t2.game
where 
	t1.install_date <='_YESTERDAY_'
	and t1.install_date> dateadd(day,-7,'_YESTERDAY_')
	and install_source = 'Organic'
group by 1,2
having sum(new_installs)>=900 -- at least 150 organic install each day
)
,temp as (
select 
	t1.install_date
	,case when t1.install_source = 'Organic' then 'organic' else 'paid' end as install_source
	,t1.country
	,t1.app
	,sum(new_installs) as installs
	,sum(d7_retained)*1.00/sum(new_installs) as d7_retention
	,sum(d30_retained)*1.00/sum(new_installs) as d30_retention
	,sum(d7_payers)*1.00/sum(new_installs) as d7_conversion
	,sum(d30_payers)*1.00/sum(new_installs) as d30_conversion
	,sum(d7_revenue)*1.00/sum(new_installs) as d7_ltv
	,sum(d30_revenue)*1.00/sum(new_installs) as d30_ltv

from kpi_processed.agg_marketing_kpi t1 
join events_game t2
on split_part(t1.app,'.',1) = t2.game
join events_date t3
	on t1.install_date >= t3.min_date
	and t1.install_date <= t3.max_date
join top_countries t4
	on t1.app = t4.app
	and t1.country = t4.country
group  by 1,2,3,4
)
,temp2 as (
select 
t1.*
from brand_mkt.return_users_14 t1 
join events_game t2
on split_part(t1.app_id,'.',1) = t2.game
join events_date t3
	on t1.date >= t3.min_date
	and t1.date <= t3.max_date
join top_countries t4
	on t1.app_id = t4.app
	and t1.country = t4.country
)

,temp3 as (
select 
install_date as date, app, install_source, country, 'installs' as metrics, installs as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd7_retention' as metrics, d7_retention as value
from temp

union all

select 
install_date as date,app, install_source, country, 'd7_conversion' as metrics, d7_conversion as value
from temp

union all

select
install_date as date, app, install_source, country, 'd7_ltv' as metrics, d7_ltv as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd30_retention' as metrics, d30_retention as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd30_conversion' as metrics, d30_conversion as value
from temp

union all

select 
install_date as date, app, install_source, country, 'd30_ltv' as metrics, d30_ltv as value
from temp

union all

select 
date, app_id, 'All' as install_source, country, 'return_users' as metrics, return_users as value
from temp2
)

select 
	a.event_type, a.event_name,a.game,a.country as campaign_country,a.campaign_start_date,a.campaign_end_date,a.control_start_date,a.control_end_date
	,b.*
from (select * from brand_mkt.brand_mkt_events where campaign_end_date > dateadd(day,-7,'_YESTERDAY_') and campaign_end_date <='_YESTERDAY_') a 
left join temp3 b
on split_part(b.app,'.',1) = a.game
	and b.date>=a.control_start_date
	and b.date<=a.campaign_end_date
;
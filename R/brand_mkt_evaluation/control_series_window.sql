select
f.event_id,
f.event,
f.comment,
f.campaign_start_date,
f.campaign_end_date,
max(case when gap_type= '14d' then control_start end) as control_start_14d,
max(case when gap_type= '14d' then control_end end) as control_end_14d,
max(case when gap_type= '7d' then control_start end) as control_start_7d,
max(case when gap_type= '7d' then control_end end) as control_end_7d
from
(select
event_id,
event,
comment,
campaign_start_date,
campaign_end_date,
gap_type,
max(control_start) as control_start,
max(control_end) as control_end
from
(select
a.event_id,
a.event,
a.comment,
a.start_date as campaign_start_date,
a.end_date as campaign_end_date,
b.control_start,
b.control_end,
case when b.gap >= 7 and b.gap < 14 then '7d' when b.gap >= 14 then '14d' end as gap_type
from
(select
row_number() over (order by start_date, end_date) as event_id,
trim('''' from event) as event,
comment,
to_date(date, 'YYYY MM DD') as start_date,
date(dateadd(day, duration, to_date(date, 'YYYY MM DD'))) as end_date
from kpi_processed.chronicle_anno
order by 1) a
left join
/* choose gap >= 7 days */
(select
event_id || '-' || eventid_prev as id_flow,
event || '-' || event_prev as event_flow ,
c.end_date_prev as control_start,
c.start_date as control_end,
c.gap
from
/* calculate the gap days between two consecutive campagins */
(select
p.*,
datediff(day, p.end_date_prev, p.start_date) as gap
from
(select
e.event_id,
e.event,
e.comment,
lag(e.event_id,1) over (order by e.event_id) as eventid_prev, /* obtain the previous campaign */
lag(e.event,1) over (order by e.event_id) as event_prev,
lag(e.comment,1) over (order by e.event_id) as comment_prev,
e.start_date,
lag(e.end_date,1) over (order by e.event_id) as end_date_prev
from
/* campaign event info */
(select
row_number() over (order by start_date, end_date) as event_id,
trim('''' from event) as event,
comment,
to_date(date, 'YYYY MM DD') as start_date,
date(dateadd(day, duration, to_date(date, 'YYYY MM DD'))) as end_date
from kpi_processed.chronicle_anno
order by 1) as e
order by 1 desc) as p
where p.eventid_prev is not null) c) b
on a.start_date >= b.control_end
where b.gap >= 7)  f
group by 1,2,3,4,5,6
order by 1 desc) f
group by 1,2,3,4,5
order by 1 desc
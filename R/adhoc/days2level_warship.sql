DELETE FROM battlewarship.days2level;
INSERT INTO battlewarship.days2level

with raw_data as (

	select 

		trunc(ts_pretty) as date
		,user_id
		,event
		,level
		,ts_pretty
		,trunc(install_ts) as install_date
		,country_code
		,json_extract_path_text(d_c1,'key') as d_c1_key
		,json_extract_path_text(d_c1,'value') as d_c1_value
		,json_extract_path_text(d_c2,'key') as d_c2_key
		,json_extract_path_text(d_c2,'value') as d_c2_value
		,json_extract_path_text(d_c3,'key') as d_c3_key
		,json_extract_path_text(d_c3,'value') as d_c3_value
		,json_extract_path_text(c1,'key') as c1_key
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c1,'value'),0),'amount') as resource_1
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c1,'value'),1),'amount') as resource_2
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c1,'value'),2),'amount') as resource_3
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c1,'value'),3),'amount') as resource_4
		,json_extract_path_text(c2,'key') as c2_key
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c2,'value'),0),'troop_level') as troop_level
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c2,'value'),0),'troop_count') as troop_count
		,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c2,'value'),0),'troop_change_count') as troop_change_count
		--,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c2,'value'),0),'is_player') as is_player
		--,nvl(json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c2,'value'),0),'amount'),0) as c2_amount
		--,json_extract_path_text(json_extract_array_element_text(json_extract_path_text(c2,'value'),0),'resource_id') as c2_resource_id

	from battlewarship.catchall
	where 
		ts_pretty>='2016-05-03' 
		--and ts_pretty < '2016-05-06'
		--and country_code = 'US'
)
,levels as (
select 
	user_id
	,install_date
	,0 as target_type
	,level as target_level
	,datediff('day',install_date, min_date) as days2level
from
	(
	select 
         user_id
		,install_date
		,level
		,min(date) as min_date
	from raw_data
	group by 1,2,3
	)
)
,target_levels as (
select
	user_id
	,install_date
	,target_type
	,target_level
	,datediff('day',install_date,min_date) as days2level 
from
	(
	select 
		user_id
		,install_date
		,cast(d_c1_value as integer) as target_type
		,cast(d_c2_value as integer) as target_level
		,min(date) as min_date
	from raw_data
	where event = 'March'
	group by 1,2,3,4
	)
)
select * from levels
union all 
select * from target_levels


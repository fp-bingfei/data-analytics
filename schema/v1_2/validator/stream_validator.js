var achievement = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "achievement" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "achievement_id": {
          "type": "string"
        },
        "achievement_name": {
          "type": "string"
        },
        "achievement_type": {
          "type": "string"
        },
        "achievement_difficulty": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "achievement_id": {
          "type": "string"
        },
        "achievement_name": {
          "type": "string"
        },
        "achievement_type": {
          "type": "string"
        },
        "achievement_difficulty": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "achievement_id": {
          "type": "string"
        },
        "achievement_name": {
          "type": "string"
        },
        "achievement_type": {
          "type": "string"
        },
        "achievement_difficulty": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var facebook_register = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "facebook_register" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        },
        "install_source": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "install_ts",
        "install_source"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var gift_received = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "gift_received" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "resources_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "source_user_id": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "source_user_id": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "source_user_id": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var item_actioned = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "item_actioned" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "resources_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_source": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_target": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "action_type": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "action_type"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "action_type": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "action_type"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "action_type": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "action_type"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var level_up = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "level_up" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "from_level": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "from_level"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "from_level": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "from_level"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "from_level": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "from_level"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var load_step = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "load_step" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "load_step": {
          "type": "integer"
        },
        "load_step_desc": {
          "type": "string"
        }
      },
      "required": [
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "load_step",
        "load_step_desc"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "load_step": {
          "type": "integer"
        },
        "load_step_desc": {
          "type": "string"
        }
      },
      "required": [
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "load_step",
        "load_step_desc"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "load_step": {
          "type": "integer"
        },
        "load_step_desc": {
          "type": "string"
        }
      },
      "required": [
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "load_step",
        "load_step_desc"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event"
  ]
};
var message_sent = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "message_sent" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "message_type": {
          "type": "string"
        },
        "recipient_user_id": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "message_type"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "message_type": {
          "type": "string"
        },
        "recipient_user_id": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "message_type"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "message_type": {
          "type": "string"
        },
        "recipient_user_id": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "message_type"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var mission = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "mission" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "resources_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "mission_statistics": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/name_value" },
          "uniqueItems": true
        },
        "mission_parameters": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/name_value" },
          "uniqueItems": true
        },
        "mission_objectives": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/mission_objective" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "mission_objective": {
      "type": "object",
      "properties": {
        "objective_id": {
          "type": "string"
        },
        "objective_name": {
          "type": "string"
        },
        "objective_type": {
          "type": "string"
        },
        "objective_amount": {
          "type": "number"
        },
        "objective_amount_remaining": {
          "type": "number"
        }
      },
      "required": [
        "objective_id",
        "objective_amount",
        "objective_amount_remaining"
      ]
    },
    "name_value": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "required": [
        "name",
        "value"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "mission_id": {
          "type": "string"
        },
        "mission_name": {
          "type": "string"
        },
        "mission_type": {
          "type": "string"
        },
        "mission_difficulty": {
          "type": "string"
        },
        "mission_status": {
          "type": "integer"
        },
        "mission_start_ts": {
          "type": "integer"
        },
        "mission_total_sec": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "mission_status"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "mission_id": {
          "type": "string"
        },
        "mission_name": {
          "type": "string"
        },
        "mission_type": {
          "type": "string"
        },
        "mission_difficulty": {
          "type": "string"
        },
        "mission_status": {
          "type": "integer"
        },
        "mission_start_ts": {
          "type": "integer"
        },
        "mission_total_sec": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "mission_status"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "mission_id": {
          "type": "string"
        },
        "mission_name": {
          "type": "string"
        },
        "mission_type": {
          "type": "string"
        },
        "mission_difficulty": {
          "type": "string"
        },
        "mission_status": {
          "type": "integer"
        },
        "mission_start_ts": {
          "type": "integer"
        },
        "mission_total_sec": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "mission_status"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var new_user = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "new_user" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        },
        "install_source": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "install_ts",
        "install_source"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "gamecenter_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var payment = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "payment" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "amount": {
          "type": "integer"
        },
        "currency": {
          "type": "string"
        },
        "iap_product_id": {
          "type": "string"
        },
        "iap_product_name": {
          "type": "string"
        },
        "iap_product_type": {
          "type": "string"
        },
        "transaction_id": {
          "type": "string"
        },
        "payment_processor": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "amount",
        "currency",
        "iap_product_id",
        "iap_product_name",
        "transaction_id",
        "payment_processor"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "amount": {
          "type": "integer"
        },
        "currency": {
          "type": "string"
        },
        "iap_product_id": {
          "type": "string"
        },
        "iap_product_name": {
          "type": "string"
        },
        "iap_product_type": {
          "type": "string"
        },
        "transaction_id": {
          "type": "string"
        },
        "payment_processor": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "amount",
        "currency",
        "iap_product_id",
        "iap_product_name",
        "transaction_id",
        "payment_processor"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "amount": {
          "type": "integer"
        },
        "currency": {
          "type": "string"
        },
        "iap_product_id": {
          "type": "string"
        },
        "iap_product_name": {
          "type": "string"
        },
        "iap_product_type": {
          "type": "string"
        },
        "transaction_id": {
          "type": "string"
        },
        "payment_processor": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "amount",
        "currency",
        "iap_product_id",
        "iap_product_name",
        "transaction_id",
        "payment_processor"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};
var personal_info = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "personal_info" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        },
        "install_source": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "email",
        "install_ts",
        "install_source"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "email",
        "install_ts"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "email",
        "install_ts"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var session_end = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "session_end" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        },
        "install_source": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "install_ts",
        "install_source"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "gamecenter_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var session_start = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "session_start" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        },
        "install_source": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "install_ts",
        "install_source"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "gamecenter_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "facebook_id": {
          "type": "string"
        },
        "gender": {
          "type": "string"
        },
        "birthday": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "googleplus_id": {
          "type": "string"
        },
        "install_ts": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "install_ts"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var timer = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "timer" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "resources_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_source": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_target": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "timer_type": {
          "type": "string"
        },
        "timer_total_sec": {
          "type": "integer"
        },
        "timer_start_ts": {
          "type": "integer"
        },
        "timer_status": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "timer_type",
        "timer_total_sec",
        "timer_start_ts",
        "timer_status"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "timer_type": {
          "type": "string"
        },
        "timer_total_sec": {
          "type": "integer"
        },
        "timer_start_ts": {
          "type": "integer"
        },
        "timer_status": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "timer_type",
        "timer_total_sec",
        "timer_start_ts",
        "timer_status"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "timer_type": {
          "type": "string"
        },
        "timer_total_sec": {
          "type": "integer"
        },
        "timer_start_ts": {
          "type": "integer"
        },
        "timer_status": {
          "type": "integer"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "timer_type",
        "timer_total_sec",
        "timer_start_ts",
        "timer_status"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var transaction = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "transaction" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        },
        "resources_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "resources_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "items_received": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        },
        "items_spent": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/item" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "transaction_type": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "transaction_type"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "transaction_type": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "transaction_type"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "transaction_type": {
          "type": "string"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "transaction_type"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var tutorial = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "bi_version": {
      "type": "string"
    },
    "app_id": {
      "type": "string"
    },
    "ts": {
      "type": "integer"
    },
    "event": {
      "enum": [ "tutorial" ]
    },
    "user_id": {
      "type": "string"
    },
    "session_id": {
      "type": "string"
    },
    "properties": {
      "type": "object",
      "oneOf": [
        { "$ref": "#/definitions/pc_only" },
        { "$ref": "#/definitions/ios_only" },
        { "$ref": "#/definitions/android_only" }
      ]
    },
    "collections": {
      "type": "object",
      "properties": {
        "player_resources": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/resource" },
          "uniqueItems": true
        },
        "ab_tests": {
          "type": "array",
          "minItems": 1,
          "items": { "$ref": "#/definitions/ab_test" },
          "uniqueItems": true
        }
      }
    }
  },
  "definitions": {
    "item": {
      "type": "object",
      "properties": {
        "item_id": {
          "type": "string"
        },
        "item_name": {
          "type": "string"
        },
        "item_class": {
          "type": "string"
        },
        "item_type": {
          "type": "string"
        },
        "item_amount": {
          "type": "number"
        }
      },
      "required": [
        "item_id",
        "item_amount"
      ]
    },
    "resource": {
      "type": "object",
      "properties": {
        "resource_id": {
          "type": "string"
        },
        "resource_name": {
          "type": "string"
        },
        "resource_type": {
          "type": "string"
        },
        "resource_amount": {
          "type": "number"
        }
      },
      "required": [
        "resource_id",
        "resource_amount"
      ]
    },
    "ab_test": {
      "type": "object",
      "properties": {
        "ab_experiment": {
          "type": "string"
        },
        "ab_variant": {
          "type": "string"
        }
      },
      "required": [
        "ab_experiment",
        "ab_variant"
      ]
    },
    "pc_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "browser": {
          "type": "string"
        },
        "browser_version": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "tutorial_step": {
          "type": "integer"
        },
        "tutorial_step_desc": {
          "type": "string"
        },
        "is_optional_step": {
          "type": "boolean"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "browser",
        "browser_version",
        "ip",
        "lang",
        "level",
        "tutorial_step",
        "tutorial_step_desc"
      ]
    },
    "ios_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "idfa": {
          "type": "string"
        },
        "idfv": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "tutorial_step": {
          "type": "integer"
        },
        "tutorial_step_desc": {
          "type": "string"
        },
        "is_optional_step": {
          "type": "boolean"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "idfa",
        "device",
        "ip",
        "lang",
        "level",
        "tutorial_step",
        "tutorial_step_desc"
      ]
    },
    "android_only": {
      "properties": {
        "app_version": {
          "type": "string"
        },
        "gameserver_id": {
          "type": "string"
        },
        "os": {
          "type": "string"
        },
        "os_version": {
          "type": "string"
        },
        "gaid": {
          "type": "string"
        },
        "mac_address": {
          "type": "string"
        },
        "android_id": {
          "type": "string"
        },
        "device": {
          "type": "string"
        },
        "ip": {
          "type": "string"
        },
        "lang": {
          "type": "string"
        },
        "level": {
          "type": "integer"
        },
        "tutorial_step": {
          "type": "integer"
        },
        "tutorial_step_desc": {
          "type": "string"
        },
        "is_optional_step": {
          "type": "boolean"
        }
      },
      "required": [
        "app_version",
        "os",
        "os_version",
        "gaid",
        "mac_address",
        "device",
        "ip",
        "lang",
        "level",
        "tutorial_step",
        "tutorial_step_desc"
      ]
    }
  },
  "required": [
    "bi_version",
    "app_id",
    "ts",
    "event",
    "user_id",
    "session_id"
  ]
};

var env = jjv();

switch ($log.event) {
	case 'achievement':
		env.addSchema($log.event, achievement);
		break;
	case 'facebook_register':
		env.addSchema($log.event, facebook_register);
		break;
	case 'gift_received':
		env.addSchema($log.event, gift_received);
		break;
	case 'item_actioned':
		env.addSchema($log.event, item_actioned);
		break;
	case 'level_up':
		env.addSchema($log.event, level_up);
		break;
	case 'load_step':
		env.addSchema($log.event, load_step);
		break;
	case 'message_sent':
		env.addSchema($log.event, message_sent);
		break;
	case 'mission':
		env.addSchema($log.event, mission);
		break;
	case 'new_user':
		env.addSchema($log.event, new_user);
		break;
	case 'payment':
		env.addSchema($log.event, payment);
		break;
	case 'personal_info':
		env.addSchema($log.event, personal_info);
		break;
	case 'session_end':
		env.addSchema($log.event, session_end);
		break;
	case 'session_start':
		env.addSchema($log.event, session_start);
		break;
	case 'timer':
		env.addSchema($log.event, timer);
		break;
	case 'transaction':
		env.addSchema($log.event, transaction);
		break;
	case 'tutorial':
		env.addSchema($log.event, tutorial);
		break;
	default:
		return 'Not a valid event name';
}

var errors = env.validate($log.event, $log);

if (!errors) {
	return ;
} else {
	return ('Failed in ' + $log.event + JSON.stringify(errors));
}

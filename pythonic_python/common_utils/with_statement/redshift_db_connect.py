import contextlib


@contextlib.contextmanager
def redshift_db_connect(conf):
    import psycopg2 as pg

    def __get_db_config(conf):
        return dict(user=conf['db_username'],
                    password=conf['db_password'],
                    host=conf['db_host'],
                    database=conf['db_database'],
                    port=conf['db_port'])

    redshift_conn = pg.connect(**__get_db_config(conf))
    try:
        yield redshift_conn, redshift_conn.cursor()
    finally:
        redshift_conn.commit()
        redshift_conn.close()

from __future__ import division

import os, sys, inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
import itertools
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from collections import defaultdict
from common_utils.third_party import HTML
from common_utils.with_statement.redshift_db_connect import redshift_db_connect
from common_utils.with_statement.email import send_email
from common_utils.date.date_string_converter import get_date_str
from common_utils.decorators.retry import retry
from configs.db_config import kpi_diandian_db_config
from configs.event_monitor_config import game_info_config
from event_monitor_sql_pattern import generate_sql
import locale
import logging

locale.setlocale(locale.LC_ALL, 'en_US')

logging.getLogger().addHandler(logging.StreamHandler())
logger = logging.getLogger('')
logger.setLevel(logging.INFO)


def calculate_and_append_the_percentage(item):
    _app_id, _app_name, _event, _cnt = item[:4]
    _other_cnts = item[4:]
    average = reduce(lambda x, y: x + y, _other_cnts) / len(_other_cnts)
    if average:
        percentage = (_cnt - average) / average
    else:
        percentage = float('inf')
    return item + (percentage,)


def generate_columns_in_row(item):
    (_app_name, _app_id, _event, _cnt), (_other_cnts), (_percentage) = item[:4], item[4:-1], item[-1]
    _percentage_str = "{0:.0f}%".format(_percentage * 100)
    columns_in_one_row = list()
    columns_in_one_row.append(HTML.TableCell(_app_name))
    columns_in_one_row.append(HTML.TableCell(_event))
    other_cnt_order_by_date_asc = _other_cnts[::-1]
    columns_in_one_row.extend([HTML.TableCell(locale.format("%d", _other_cnt, grouping=True)) for _other_cnt in other_cnt_order_by_date_asc])

    color = 'white'
    if abs(_percentage) >= 0.3:
        color = 'orange'
    if abs(_percentage) >= 0.7:
        color = 'red'
    if abs(_percentage) == float('inf'):
        color = 'yellow'
    columns_in_one_row.append(HTML.TableCell(locale.format("%d", _cnt, grouping=True), bgcolor=color))
    columns_in_one_row.append(HTML.TableCell(' (' + _percentage_str + ')', bgcolor=color))
    return columns_in_one_row


@retry(3)
def send_tables_email(msg, tables):
    _html = MIMEText('<html>' + '<br/>'.join([str(table) for table in tables]) + '</html>', 'html')
    msg.attach(_html)
    send_email(msg)


def dispatch_and_send_emails(all_tables_dict):
    for key in all_tables_dict.keys():
        logger.info('Dispatching email for %s ....', key.upper())
        recipients = game_info_config[key]['recipients']
        msg = MIMEMultipart('alternative')
        msg['Subject'] = 'Events Monitor for ' + key.upper()
        msg['From'] = "Events Monitor<tableau_admin@funplus.com>"
        msg['To'] = ','.join(recipients)
        send_tables_email(msg, all_tables_dict[key])
        print 'Email for', key.upper(), 'sent.'


@retry(3)
def fetch_raw_data_from_db(base_date_str, nums_previous_dates_to_compare):
    with redshift_db_connect(kpi_diandian_db_config) as (conn, cur):
        # the sql will return at least 4 columns app_id, app_name, event, cnt [, yesterday_cnt [, the_day_before_yesterday_cnt]]
        select_sql = generate_sql(base_date_str, nums_previous_dates_to_compare)
        cur.execute(select_sql)
        results = cur.fetchall()
    return results


def event_monitor_run(base_date_str=None, nums_previous_dates_to_compare=3):
    assert nums_previous_dates_to_compare >= 1
    base_date_str = base_date_str if base_date_str else get_date_str()
    all_tables_dict = defaultdict(list)
    logger.info('Start to fetch data from redshift...')
    data = fetch_raw_data_from_db(base_date_str, nums_previous_dates_to_compare)
    logger.info('Fetching data from db is done.')
    logger.info('Start Calculating...')
    for app_id, group_under_app_id in itertools.groupby(sorted(itertools.imap(calculate_and_append_the_percentage, data), key=lambda e: e[0]), key=lambda x: x[0]):
        header_row = ['app_name', 'event']
        header_row.extend([get_date_str(base_date_str, idx) for idx in range(-nums_previous_dates_to_compare, 0)])
        header_row.append(base_date_str)
        header_row.append('diff%')
        t = HTML.Table(header_row=header_row)
        for app_name, group_under_app_name in itertools.groupby(sorted(group_under_app_id, key=lambda e: e[1]), key=lambda x: x[1]):
            # the rows are sorted by the diff-percentage, grouping by the app_id, app_name,
            # the sort key is the percentage appended to the end of each tuple
            t.rows.extend(list(map(generate_columns_in_row, sorted(group_under_app_name, key=lambda x: -abs(x[-1])))))
        game_info_key = next((k for k in game_info_config.keys() if app_id in game_info_config[k]['app_names']), None)
        if game_info_key:
            all_tables_dict[game_info_key].append(t)
    logger.info('Calculating Done.')
    logger.info('Start Dispatching Emails')
    dispatch_and_send_emails(all_tables_dict=all_tables_dict)


if __name__ == "__main__":
    import argparse

    logger.info('Event monitor start')
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--date', default=None, type=str)
    parser.add_argument('--nums', default=3, type=int)
    args = parser.parse_args()
    base_date_str = args.date
    nums_previous_dates_to_compare = args.nums
    event_monitor_run(base_date_str=base_date_str, nums_previous_dates_to_compare=nums_previous_dates_to_compare)
    logger.info('Event monitor script finished.')

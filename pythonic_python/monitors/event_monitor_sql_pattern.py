from common_utils.date.date_string_converter import get_date_str

sql_pattern = '''
WITH d0 AS(
        SELECT date, app_name, SPLIT_PART(app_id, '.', 1) as app_partial_id, event, SUM(cnt) AS cnt_total
        FROM db_usage.s3_usage
        WHERE date = '[date]' AND app_id <> 'N/A'
        GROUP BY 1,2,3,4
    ) 
    [dx_tables]
    
SELECT d0.app_name, d0.app_partial_id, d0.event, coalesce(d0.cnt_total, 0) as d0_cnt
    [columns]

FROM d0
    [join_statements]

;

'''

dx_table_pattern = '''
, [dx] AS(
    SELECT app_name, SPLIT_PART(app_id, '.', 1) as app_partial_id, event, SUM(cnt) AS cnt_total
    FROM db_usage.s3_usage
    WHERE date = '[date]' AND app_id <> 'N/A'
    GROUP BY 1,2,3
)
'''
dx_cnt_column_pattern = '''
, coalesce([dx].cnt_total, 0) [dx]_cnt
'''

dx_left_out_join_pattern = '''
LEFT OUTER JOIN [dx]
ON d0.app_name = [dx].app_name and d0.app_partial_id = [dx].app_partial_id and d0.event=[dx].event
'''


def generate_sql(base_date_str=None, nums_previous_dates_to_compare=3):
    assert nums_previous_dates_to_compare >= 0
    dx_tables = ''.join([
                            dx_table_pattern
                        .replace('[dx]', 'd' + str(idx))
                        .replace('[date]', get_date_str(base_date_str, delta_day=0 - idx))
                            for idx in range(1, nums_previous_dates_to_compare + 1)
                            ])
    columns = ''.join([
                          dx_cnt_column_pattern.replace('[dx]', 'd' + str(idx))
                          for idx in range(1, nums_previous_dates_to_compare + 1)
                          ])
    join_statements = ''.join([
                                  dx_left_out_join_pattern.replace('[dx]', 'd' + str(idx))
                                  for idx in range(1, nums_previous_dates_to_compare + 1)
                                  ])
    base_sql = sql_pattern.replace('[date]', get_date_str(base_date_str))
    return base_sql.replace('[dx_tables]', dx_tables).replace('[columns]', columns).replace('[join_statements]', join_statements)

# print generate_sql('2016-11-11', nums_previous_dates_to_compare=0)
game_info_config = {
    'rs': {
        'app_names': ['rs_1_2',
                    ],
        'recipients': ['bin.hu@funplus', 'data@funplus.com']
    },
    'ffs': {
        'app_names': ['ffs_1_2',
                    'ffs_2_0',
                    ],
        'recipients': ['zhisheng.wang@funplus.com', 'data@funplus.com']
    },
    'ff': {
        'app_names': [
            'farm_1_1',
            'farmeu_1_1',
        ],
        'recipients': ['xing.qin@funplus.com', 'yuejun.yan@funplus.com', 'data@funplus.com']
    },
    'ha': {
        'app_names': ['ha_1_2',
                    'ff2_1_1'
                    ],
        'recipients': ['li.li@funplus.com', 'data@funplus.com']
    }
}

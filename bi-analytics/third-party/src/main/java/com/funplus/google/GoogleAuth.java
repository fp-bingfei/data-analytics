package com.funplus.google;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;


/**
 * Created by balaji on 3/23/15.
 */
public class GoogleAuth {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final FeedURLFactory FEED_URL_FACTORY = FeedURLFactory.getDefault();

    private static final List<String> SCOPES = Arrays.asList("https://spreadsheets.google.com/feeds",
            "https://docs.google.com/feeds");

    /**
     * Define a global instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    /**
     * Define a global instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new JacksonFactory();


    private static class GoogleAuthHolder {
        public static final GoogleAuth INSTANCE = new GoogleAuth();
    }

    public static GoogleAuth getInstance() {
        return GoogleAuthHolder.INSTANCE;
    }

    public Credential getCredential(String email, String p12FileName) throws Exception {
        logger.info("email({}) p12FileName({})", email, p12FileName);
        GoogleCredential credential = null;
        try{
            ClassLoader classLoader = this.getClass().getClassLoader();
            URL url1 = ClassLoader.getSystemResource(p12FileName);
            File pkFile = new File(url1.getFile());
            if(!pkFile.exists()){
                pkFile = new File("/mnt/funplus/analytics/bi-analytics/third-party/src/main/resources/".concat(p12FileName));
            }
            logger.info("path for p12FileName {}", pkFile.getPath());

            credential = new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT)
                    .setJsonFactory(JSON_FACTORY).setServiceAccountScopes(SCOPES).setServiceAccountId(email)
                    .setServiceAccountPrivateKeyFromP12File(pkFile).build();
            HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory(credential);
            GenericUrl url = new GenericUrl(getSpreadsheetFeedURL().toString());
            HttpRequest request = requestFactory.buildGetRequest(url);
            request.execute();
        }
        catch (Exception e){
            logger.error("Error constructing Credential {}", e.getMessage());
            e.printStackTrace();
            throw new Exception("Error constructing Credential", e);
        }
        finally {
            return credential;
        }
    }

    private static URL getSpreadsheetFeedURL() {
        return FEED_URL_FACTORY.getSpreadsheetsFeedUrl();
    }

}

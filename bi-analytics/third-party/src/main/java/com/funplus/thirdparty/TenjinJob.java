package com.funplus.thirdparty;

import com.amazonaws.util.StringUtils;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.funplus.google.GoogleAuth;
import com.google.api.client.auth.oauth2.Credential;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.choice.CollectionArgumentChoice;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.zip.GZIPOutputStream;

/**
 * Created by balaji on 7/1/15.
 */
public class TenjinJob {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String FIELD_SEPERATOR = "|";


    public static void main(String[] args) throws Exception {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("TenjinJob")
                .defaultHelp(true)
                .description("Get marketing cost data from Tenjin");
        parser.addArgument("-s", "--start_day").required(Boolean.TRUE)
                .help("First day that should be included in the response. It should have the format \"yyyy-mm-dd\"");
        parser.addArgument("-e", "--end_day").required(Boolean.TRUE)
                .help("Last day that should be included in the response. It should have the format \"yyyy-mm-dd\"");
        List types = new ArrayList();
        types.add("apps");
        types.add("campaign");
        parser.addArgument("-t", "--type").required(Boolean.TRUE).choices(new CollectionArgumentChoice(types))
                .help("");
        parser.addArgument("-o", "--output_file_path").required(Boolean.TRUE)
                .help("Output file path");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        String startDay = ns.getString("start_day");
        String endDay = ns.getString("end_day");
        String type = ns.getString("type");
        String outputFilePath = ns.getString("output_file_path");



        TenjinJob job = new TenjinJob();
        job.run(startDay, endDay, type, outputFilePath);

    }

    private void run(String startDay, String endDay, String type, String outputFilePath) throws Exception {
        logger.info("TenjinJob started for args - {}, {}, {}, {}", startDay, endDay, type, outputFilePath);

        outputFilePath = outputFilePath.endsWith(".gz") ? outputFilePath : outputFilePath.concat(".gz");
        File outFile = new File(outputFilePath);
        if (outFile.exists()) {
            logger.error("Output file path ({}) already exists", outputFilePath);
            System.exit(1);
        }

        Properties properties = new Properties();
        properties.load(this.getClass().getClassLoader().getResourceAsStream("third-party.properties"));

        String token = properties.getProperty("tenjin.token");
        String email = properties.getProperty("tenjin.email");
        String apiUrl = properties.getProperty("tenjin.".concat(type).concat(".url"));
        String header = properties.getProperty("tenjin.".concat(type).concat(".format"));

        List<NameValuePair> namedParameters = new ArrayList();
        namedParameters.add(new BasicNameValuePair("start_day", startDay));
        namedParameters.add(new BasicNameValuePair("end_day", endDay));

        Map appIdMapping = null;
        if("apps".equals(type)){
            appIdMapping = generateAppIdMapping();

        }

        generateOutputResponse(token, email, apiUrl, header, namedParameters, outFile, appIdMapping);

        logger.info("TenjinJob completed for args - {}, {}, {}, {}", startDay, endDay, type, outputFilePath);


    }

    private void generateOutputResponse(String token, String email, String apiUrl,
                                        String header, List namedParameters, File outFile,
                                        Map<String, String> appIdMapping) throws IOException, JSONException, URISyntaxException {
        List<JSONArray> outputList = new ArrayList();

        int page = 1;
        int totalPages = 1;
        do {
            String status = "";
            int retries = 0;
            final int maxRetry = 3;
            boolean isSuccess = Boolean.FALSE;

            URIBuilder urlBuilder = new URIBuilder(apiUrl).addParameters(namedParameters);
            urlBuilder.setParameter("page",String.valueOf(page));
            URL url = urlBuilder.build().toURL();
            URLConnection urlConnection = url.openConnection();
            String userPass = email + ":" + token;
            String basicAuth = "Basic " + new String(new Base64().encode(userPass.getBytes()));
            urlConnection.setRequestProperty("Authorization", basicAuth);

            while(retries < maxRetry && !isSuccess) {
                BufferedReader br = null;
                try {
                    retries += 1;


                    br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String temp = "";
                    StringBuilder builder = new StringBuilder();
                    while (null != (temp = br.readLine())) {
                        builder.append(temp);
                    }

                    JSONObject inputObject = new JSONObject(builder.toString());
                    status = inputObject.getString("status");
                    /*if(!"full".equals(status)){
                        throw new Exception(String.format("Data is not complete and status is %s", status));
                    }*/
                    page = getIntValue(inputObject, "page", 1);
                    totalPages = getIntValue(inputObject, "total_pages",1);
                    JSONArray dataArray = inputObject.getJSONArray("data");

                    outputList.add(dataArray);
                    isSuccess = Boolean.TRUE;
                }
                catch (Exception e){
                    logger.warn(String.format("%s - %s", e.getMessage(), apiUrl));
                }
                finally {
                    try{ br.close(); } catch (Exception e){}
                }
            }

            if(!isSuccess){
                throw new IOException(String.format("Max retries exceeded (%s) - Exiting...", apiUrl));
            }
            page += 1;
        } while (page <= totalPages);

        int rowCount = 0;
        BufferedWriter writer = null;
        try {
            GZIPOutputStream zip = new GZIPOutputStream(new FileOutputStream(outFile));
            writer = new BufferedWriter(new OutputStreamWriter(zip, "UTF-8"));
            String suffix = appIdMapping != null ? FIELD_SEPERATOR.concat("app_id".concat(FIELD_SEPERATOR.concat("game_id"))) : "";
            writer.write(header.concat(suffix).concat("\n"));
            String[] headerArray = header.split("\\|");
            List noAppIdFound = new ArrayList();
            for (JSONArray array : outputList) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonRow = array.getJSONObject(i);
                    String[] outputArray = new String[headerArray.length];
                    for(int j = 0; j < headerArray.length; j++) {
                        outputArray[j] = jsonRow.getString(headerArray[j]);
                    }
                    if(null != appIdMapping){
                        String appInfo = appIdMapping.get(outputArray[1]);
                        if(!StringUtils.isNullOrEmpty(appInfo)) {
                            writer.write(StringUtils.join(FIELD_SEPERATOR, outputArray).concat(FIELD_SEPERATOR).concat(appInfo).concat("\n"));
                        }
                        else{
                            noAppIdFound.add(outputArray[1]);
                        }
                    }
                    else{
                        writer.write(StringUtils.join(FIELD_SEPERATOR, outputArray).concat("\n"));
                    }
                    rowCount += 1;
                }
            }

            if(noAppIdFound.size() > 0){
                //todo: Need proper handling
                logger.error(String.format("Found appIDs missing for %d tenjin application(s). Here are list of tenjin application(s) to be fixed %s",
                                noAppIdFound.size(), noAppIdFound ));
               // throw new Exception(String.format("Found appIDs missing for %d tenjin application(s). Here are list of tenjin application(s) to be fixed %s",
               //         noAppIdFound.size(), noAppIdFound ));
            }
        }
        catch (Exception e){
            logger.error("IOException {}", e.getMessage());
            throw new IOException("Error writing output", e);
        }
        finally {
            try { writer.flush(); } catch (Exception e){}
            try { writer.close(); } catch (Exception e){}
            logger.info("Total row count {} written to {}", rowCount, outFile.getAbsolutePath());
        }



    }
    private Integer getIntValue(JSONObject jsonObject, String key, Integer defaultValue){

        Integer outputValue = defaultValue;
        try{
            outputValue = jsonObject.getInt(key);
        }
        catch (NumberFormatException e){}
        catch (JSONException e){}
        return outputValue;
    }


    private Map<String, String> generateAppIdMapping() throws Exception {
        Map<String, String> appIdMapping = new HashMap<String,String>();
        String p12FileName = "FunplusBIData-28c9c5bbf38d.p12";
        String emailId = "1085577572165-ja8s7eai4bgnviqumuqavvkto3u81r5c@developer.gserviceaccount.com";
        String spreadSheetKey = "1TIHKasEeMcqrAmCN2yFwB4qgJP_Rc51xvMWYqjEjMe0";

        Credential credential = GoogleAuth.getInstance().getCredential(emailId, p12FileName);


        SpreadsheetService service = new SpreadsheetService("Tenjin");
        service.setOAuth2Credentials(credential);
        URL workSheetFeedURL = FeedURLFactory.getDefault().getWorksheetFeedUrl(spreadSheetKey, "private", "full");
        WorksheetFeed listFeed = service.getFeed(workSheetFeedURL, WorksheetFeed.class);
        List<WorksheetEntry> workSheets = listFeed.getEntries();

        for (WorksheetEntry workSheet : workSheets) {
            // Fetch the list feed of the worksheet.
            URL listFeedUrl = workSheet.getListFeedUrl();
            ListFeed feed = service.getFeed(listFeedUrl, ListFeed.class);
            String worksheetId = workSheet.getTitle().getPlainText().toLowerCase();
            logger.info("Processing worksheetId {}", worksheetId);

            boolean isFirstRow = Boolean.TRUE;
            List<String> tagList = new ArrayList<>();

            for (ListEntry entry : feed.getEntries()) {
                if(isFirstRow){
                    Set<String> tags = entry.getCustomElements().getTags();
                    tagList.addAll(tags);
                    isFirstRow = Boolean.FALSE;
                }
                // Iterate over the remaining columns, and print each cell value
                String applicationName = entry.getCustomElements().getValue(tagList.get(0));
                String appId = entry.getCustomElements().getValue(tagList.get(1));
                String gameId = entry.getCustomElements().getValue(tagList.get(2));
                if(!StringUtils.isNullOrEmpty(applicationName)
                        && !StringUtils.isNullOrEmpty(appId)
                        && !StringUtils.isNullOrEmpty(gameId)){
                    appIdMapping.put(applicationName,appId.concat(FIELD_SEPERATOR).concat(gameId));
                }
                else{
                    logger.warn(applicationName+"--"+appId+"--"+gameId);
                }

            }
        }
        logger.info("Size {}", appIdMapping.size());
        return appIdMapping;

    }



}

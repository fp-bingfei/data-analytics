package com.funplus.thirdparty;

import com.funplus.google.GoogleAuth;
import com.google.api.client.auth.oauth2.Credential;
import com.google.gdata.client.spreadsheet.FeedURLFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.common.base.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.GZIPOutputStream;

/**
 * Created by balaji on 3/24/15.
 */
public class CostImportProcessor {

    private static final String OUTPUT_HEADER = "date|worksheet_id|country|install_source|amount\n";
    private static final String FIELD_SEPERATOR = "|";

    //1yz3pCsXBPfYWA__5fCwGAHGP93T2ViNA2XnheNFNE08
    private static final String DATE_FORMAT_1 = "yyyy-MM-dd";
    private static final String DATE_FORMAT_2 = "MM/dd/yyyy";
    private static final String FINAL_DATE_FORMAT = "yyyy-MM-dd";

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static class CostImportProcessorHolder {
        public static final CostImportProcessor INSTANCE = new CostImportProcessor();
    }

    public static CostImportProcessor getInstance() {
        return CostImportProcessorHolder.INSTANCE;
    }


    public void process(String serviceId, String emailId, String p12FileName,String spreadSheetKey, File outFile ) throws Exception {

        BufferedWriter writer = null;

        try {
            Credential credential = GoogleAuth.getInstance().getCredential(emailId, p12FileName);

            SpreadsheetService service = new SpreadsheetService(serviceId);
            service.setOAuth2Credentials(credential);
            URL workSheetFeedURL = FeedURLFactory.getDefault().getWorksheetFeedUrl(spreadSheetKey, "private", "full");
            WorksheetFeed listFeed = service.getFeed(workSheetFeedURL, WorksheetFeed.class);
            List<WorksheetEntry> workSheets = listFeed.getEntries();

            GZIPOutputStream zip = new GZIPOutputStream(new FileOutputStream(outFile));
            writer = new BufferedWriter(new OutputStreamWriter(zip, "UTF-8"));
            writer.write(OUTPUT_HEADER);
            for (WorksheetEntry workSheet : workSheets) {
                // Fetch the list feed of the worksheet.
                URL listFeedUrl = workSheet.getListFeedUrl();
                ListFeed feed = service.getFeed(listFeedUrl, ListFeed.class);
                String worksheetId = workSheet.getTitle().getPlainText().toLowerCase();
                logger.info("Processing worksheetId {}", worksheetId);

                boolean isFirstRow = Boolean.TRUE;
                List<String> firstRow = new ArrayList<>();
                List<String> secondRow = new ArrayList<>();
                Set<Integer> skipIndex = new HashSet<>();
                List<String> tagList = new ArrayList<>();
                // Iterate through each row, printing its cell values.
                for (ListEntry entry : feed.getEntries()) {
                    if (null != entry
                            && null != entry.getCustomElements()) {
                        // Compute Headers
                        if (isFirstRow) {
                            skipIndex.add(0);
                            Set<String> tags = entry.getCustomElements().getTags();
                            tagList.addAll(tags);
                            generateFirstRow(tagList, firstRow, skipIndex);
                            generateSecondRow(entry, secondRow, tagList, skipIndex);
                            isFirstRow = Boolean.FALSE;
                        }
                        else {
                            String dateString = null;
                            // Iterate over the remaining columns, and print each cell value
                            for (int i = 0; i < tagList.size(); i++) {
                                String cellValue = entry.getCustomElements().getValue(tagList.get(i));
                                if(i == 0 && StringUtil.isEmpty(cellValue))
                                    break;
                                if(i == 0 && cellValue.toLowerCase().contains("total"))
                                    break;
                                if(i == 0 && StringUtil.isEmpty(dateString))
                                    dateString = generateUnifiedDate(cellValue);
                                if(StringUtil.isEmpty(dateString))
                                    break;
                                if(!skipIndex.contains(i)){
                                            StringBuilder outputBuilder = new StringBuilder();
                                    outputBuilder.append(dateString).append(FIELD_SEPERATOR)
                                            .append(worksheetId).append(FIELD_SEPERATOR)
                                            .append(firstRow.get(i)).append(FIELD_SEPERATOR)
                                            .append(secondRow.get(i)).append(FIELD_SEPERATOR)
                                            .append(getValidatedAmount(cellValue)).append("\n");
                                    writer.write(outputBuilder.toString());
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            logger.error("IOException {}", e.getMessage());
            throw new Exception("Error constructing Credential", e);
        } catch (Throwable t) {
            logger.error("Throwable: " + t.getMessage());
            throw t;
        }
        finally {
            try { writer.flush(); } catch (Exception e){}
            try { writer.close(); } catch (Exception e){}
        }
    }



    private void generateFirstRow(List<String> tagList, List<String> row,  Set<Integer> skipIndex){
        String country = StringUtil.EMPTY_STRING;
        for(int i = 0; i < tagList.size(); i++){
            String cellValue = tagList.get(i);
            if(tagList.get(i).startsWith("_")){
                cellValue = country;
            }
            else if(i != 0
                    && !"".equals(country)
                    && tagList.get(i).startsWith(country)){
                cellValue = country;
            }
            else if(i != 0 && "".equals(tagList.get(i))){
                cellValue = country;
            }
            else if(i != 0 && !"".equals(tagList.get(i))){
                country = tagList.get(i);
            }
            row.add(cellValue);

            if(i != 0 && cellValue.toLowerCase().contains("total")){
                skipIndex.add(i);
            }
        }
    }


    private void generateSecondRow(ListEntry entry, List<String> row, List<String> tagList, Set<Integer> skipIndex) {
        for (int i = 0; i < tagList.size(); i++) {
            String cellValue = entry.getCustomElements().getValue(tagList.get(i));
            row.add(cellValue);
           if (i != 0 && ( cellValue == null || cellValue.toLowerCase().contains("total"))) {
               skipIndex.add(i);
           }
        }
    }


    private String generateUnifiedDate(String inputDateString){
        String outputDateString = getUnifiedDate(inputDateString, DATE_FORMAT_1);
        if(StringUtil.isEmpty(outputDateString)){
            outputDateString = getUnifiedDate(inputDateString, DATE_FORMAT_2);
        }
        if(StringUtil.isEmpty(outputDateString))
            logger.error("Date Format not support for {}",inputDateString);
        return outputDateString;
    }

    private String getUnifiedDate(String inputDateString, String dateFormat){
        String outputDateString = null;
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat(dateFormat);
            SimpleDateFormat outputFormat = new SimpleDateFormat(FINAL_DATE_FORMAT);
            Date inputDate = inputFormat.parse(inputDateString);
            outputDateString = outputFormat.format(inputDate);
        } catch (ParseException e) {
            //logger.warn("Date format parser failed for ({},{}) {}", inputDateString, dateFormat, e.getMessage());
        } finally {
            return outputDateString;
        }

    }

    private String getValidatedAmount(String cellValue) {
        String outputValue = StringUtil.EMPTY_STRING;
        if(!StringUtil.isEmpty(cellValue)){
            String tempValue = cellValue.replaceAll("\\$|,", StringUtil.EMPTY_STRING);
            if(isNumeric(tempValue)){
                outputValue = tempValue;
            }else{
                logger.warn("Not a valid amount({})", cellValue);
            }

        }
        return outputValue;
    }


    private boolean isNumeric(String s) {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

}


package com.funplus.thirdparty;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
/**
 * Created by balaji on 3/23/15.
 */
public class CostImportJob {

    private Logger logger = LoggerFactory.getLogger(this.getClass());



    public static void main(String[] args) throws Exception {

        CostImportJob job = new CostImportJob();
        job.initialize(args);

    }

    public void initialize(String[] args) throws Exception {
        // Argument Parser

        //-i test -e 1085577572165-ja8s7eai4bgnviqumuqavvkto3u81r5c@developer.gserviceaccount.com -p FunplusBIData-28c9c5bbf38d.p12 -k 1yz3pCsXBPfYWA__5fCwGAHGP93T2ViNA2XnheNFNE08 -o test
        ArgumentParser parser = ArgumentParsers.newArgumentParser("CostImportJob")
                .defaultHelp(true)
                .description("Get data from Google Spreadsheet, initialize and write to file.");
        parser.addArgument("-i", "--serviceId").setDefault("CostImportJob")
                .help("service name");
        parser.addArgument("-e", "--emailId").required(Boolean.TRUE)
                .help("email address from Google OAuth Service Account");
        parser.addArgument("-p", "--p12FileName").required(Boolean.TRUE)
                .help("name of the P12 jet file");
        parser.addArgument("-k", "--spreadSheetKey").required(Boolean.TRUE)
                .help("key id of the spreadsheet");
        parser.addArgument("-o", "--outFilePath").required(Boolean.TRUE)
                .help("output file path");

        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        String serviceId = ns.getString("serviceId");
        String emailId = ns.getString("emailId");
        String p12FileName = ns.getString("p12FileName");
        String spreadSheetKey = ns.getString("spreadSheetKey");
        String outFilePath = ns.getString("outFilePath");


        outFilePath = outFilePath.endsWith(".gz") ? outFilePath : outFilePath.concat(".gz");
        File outFile = new File(outFilePath);
        if (outFile.exists()) {
            logger.error("Output file path ({}) already exists", outFilePath);
            System.exit(1);
        }

        CostImportProcessor.getInstance().process(serviceId,emailId, p12FileName, spreadSheetKey, outFile);
    }




}

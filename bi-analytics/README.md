# Bi-Analytics

This project allows to create libraries under one workspace.

Use the command to clone the repository
```sh
$ cd <repository_path>
$ git clone https://bitbucket.org/yitao/analytics.git
```

Create symlink (to be same as production path)
```sh
$ mkdir -p /mnt/funplus/
$ ln -s <repository_path>/analytics
```

Environmental properties in profile/rc -
```sh
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
```

Any libariries should be categorized and placed under right module. Current available modules
 - kinesis-app
 - third-party

All the modules are enclosed with the profile which is module name for easy build.</br>
```sh
$ cd /mnt/funplus/analytics/bi-analytics
$ mvn clean install -DskipTests -P<module-name>
```

### kinesis-app

This module holds all code related to kinesis streaming. Some are 
* Upshot Data Stream
* Event Parser
* Other Utilities

To build
```sh
$ /mnt/funplus/analytics/bi-analytics
$ mvn clean install -DskipTests -Pkinesis-app
```

### third-party

This module holds code related to third party APIs data processing like 
- Marketing Cost from Google Doc
- App Annie Data Import
- Tenjin Cost Data Import

For the first time you might need to add dependency jar to the m2 repository
```sh
 $ /mnt/funplus/analytics/bi-analytics
 $ mvn install:install-file -Dfile=/mnt/funplus/analytics/scripts/jars/RedshiftJDBC41-1.1.2.0002.jar -DgroupId=com.funplus.aws -DartifactId=RedshiftJDBC41 -Dversion=1.1.2.0002 -Dpackaging=jar -DgeneratePom=true
```

To build
```sh
 $ /mnt/funplus/analytics/bi-analytics
 $ mvn clean install -DskipTests -Pthird-party
```



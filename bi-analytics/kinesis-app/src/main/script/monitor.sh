#!/bin/bash

base_dir="/mnt/spark/temp/"                             #running path
proc_name="spark"                                       #process name
log_file=${base_dir}"spark.log"                         #log file
pid_file=${base_dir}"spark.pid"                         #pid file
pid=0
number=0
exception_num=0

proc_num()                                              #get process number
{
    number=`/bin/ps -ef | grep $proc_name | grep -v grep | wc -l`
}

proc_id()                                               #get process id
{
    pid=`/bin/ps -ef | grep $proc_name | grep -v grep | awk '{print $2}'`
}

if [ -f "$log_file" ]; then
    exception_num=`cat $log_file | grep 'Exception' | wc -l`
fi

proc_num                                                #get process number

if [ $number -eq 0 ] || [ $exception_num -gt 0 ]        #if process exit or have Exception in log
then
    proc_id                                             #get new process id
    for i in ${pid}; do kill -9 $i; done
    time_stamp=`date +%F_%s`
    if [ -f "$log_file" ]; then
        cp ${log_file} ${log_file}.${time_stamp}
        cat /dev/null > ${log_file}                     #empty log file
    fi
    cd $base_dir && ./start.sh                          #restart

    proc_id                                             #get new process id
    echo ${pid}, `date +%F_%T` >> ${pid_file}           #log new process id in pid file

    exit 1                                              #exit with error to notify pipeline
else
    cat /dev/null > ${log_file}                         #empty log file
fi

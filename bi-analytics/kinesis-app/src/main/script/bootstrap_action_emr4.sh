#!/bin/bash

# run on every machine in the cluster as they are brought online
# update guava version on EMR 4.1.0
mkdir -p ~/customsetup
cd ~/customsetup

### remove existing files ###
rm -f guava-16.0.1.jar
rm -f parquet-hadoop-1.7.0.jar

### download from s3 ###
aws s3 cp s3://com.funplusgame.bidata/dev/spark/guava-16.0.1.jar .
aws s3 cp s3://com.funplusgame.bidata/dev/spark/parquet-hadoop-1.7.0.jar .

### Use configuration object to update the HADOOP_CLASSPATH ###
# Refer to hadoop-env.json

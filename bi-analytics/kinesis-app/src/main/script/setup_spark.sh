#/bin/bash

# Create by JiaTing 2015-12-04
# To setup a spark cluster running env
# Usage :
# $sh setup_spark.sh  worker_group_name 
worker=$1
if [ ! "$worker" ]; then
    worker="wg-emr-default"
fi

##########################################################
### MAIN
##########################################################
main() {
  	download_dependencies
    download_spark_files	
  	setup_runtime_dir
  	install_taskrunner
}

##########################################################
### DOWNLOAD ALL THE DEPENDECIES REQUIRED FOR SPARK SUBMIT 
##########################################################

download_dependencies(){
  	# Download dependencies for spark job commit 
    sudo mkdir -p /usr/lib/spark/auxlib/
    cd /usr/lib/spark/auxlib/
    sudo hadoop fs -get s3://com.funplusgame.bidata/dev/spark/*.jar
    cd /usr/lib/spark/lib/
    sudo wget https://s3.amazonaws.com/redshift-downloads/drivers/RedshiftJDBC41-1.1.7.1007.jar
	echo "Done: Download dependencies for spark job commit\n"
}


##########################################################
### Download start.sh and spark.conf to /mnt/spark/temp 
##########################################################

download_spark_files(){
	mkdir -p /mnt/spark/temp
    cd /mnt/spark/temp
    hadoop fs -get s3://com.funplusgame.bidata/dev/spark/start_emr4.sh
    hadoop fs -get s3://com.funplusgame.bidata/dev/spark/monitor_emr4.sh
    hadoop fs -get s3://com.funplusgame.bidata/dev/spark/spark.conf
    chmod +x *.sh
	echo "Done: Download start.sh and spark.conf to /mnt/spark/temp\n"
}

##########################################################
### Setup runtime directory 
##########################################################

setup_runtime_dir(){
	hadoop fs -mkdir -p /mnt/spark/checkpoint/
	hadoop fs -mkdir -p /mnt/spark/temp/
	# Before submitting job, you need update the parameters in spark.conf accordingly
	hadoop fs -put spark.conf /mnt/spark/temp/
	echo "Done: Setup runtime directory\n"
	echo "*****Reminder: Before submitting job, you need update the parameters in spark.conf accordingly \n"
}

##########################################################
### INSTALL TASK RUNNER
##########################################################
install_taskrunner(){
    # Install TaskRunner
    mkdir -p /mnt/taskrunner
    cd /mnt/taskrunner
    wget https://s3.amazonaws.com/datapipeline-us-east-1/us-east-1/software/latest/TaskRunner/TaskRunner-1.0.jar
    wget http://s3.amazonaws.com/datapipeline-prod-us-east-1/software/latest/TaskRunner/mysql-connector-java-bin.jar

cat >> /mnt/taskrunner/credentials.json << EOF
{
    "access-id": "AKIAJRKDNC52OAINDWKA",
    "private-key": "FTTVxv9SngsUztHSEuXIdoBODYFRDnHic309+AmI",
    "endpoint": "https://datapipeline.us-west-2.amazonaws.com",
    "region": "us-west-2",
    "log-uri": "s3://com.funplusgame.bidata/datapipeline/logs/"
}
EOF

    nohup java -jar TaskRunner-1.0.jar --config /mnt/taskrunner/credentials.json --workerGroup=$worker --region=us-west-2 --tasks=5 --logUri=s3://com.funplusgame.emr/logs/$worker/ &
    echo "Done: Install task runner, you can find the process use: ps -ef | grep task \n"
}

main

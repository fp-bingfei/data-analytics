#!/bin/bash

# run on every machine in the cluster as they are brought online
# update guava version on EMR
rm /home/hadoop/share/hadoop/common/lib/guava*.jar
hadoop fs -get s3://com.funplusgame.bidata/dev/spark/guava-16.0.1.jar /home/hadoop/share/hadoop/common/lib/
chmod +x /home/hadoop/share/hadoop/common/lib/guava-16.0.1.jar

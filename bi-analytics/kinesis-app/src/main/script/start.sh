#!/bin/bash

###########################################################
# 1 data nodes r3.2xlarge, each with 16 vcores 53G memory
# 3 executors in each node with 5 vcores 14G memory
# driver with 1 vcore 1G memory
###########################################################
nohup /home/hadoop/spark/bin/spark-submit --conf spark.streaming.stopGracefullyOnShutdown=true --conf spark.streaming.receiver.writeAheadLog.enable=true --driver-class-path "/home/hadoop/spark/conf:/home/hadoop/conf:/home/hadoop/spark/classpath/distsupplied/*:/home/hadoop/spark/classpath/emr/*:/home/hadoop/spark/classpath/emrfs/*:/home/hadoop/share/hadoop/common/lib/*:/home/hadoop/share/hadoop/common/lib/hadoop-lzo.jar:/usr/share/aws/emr/auxlib/*" --master yarn-client --driver-memory 4G --conf spark.yarn.am.memory=4G --num-executors 3 --executor-cores 5 --executor-memory 14G --jars /home/hadoop/spark/auxlib/json-schema-validator-2.2.6.jar,/home/hadoop/spark/auxlib/json-schema-validator-2.2.6-lib.jar,/home/hadoop/spark/auxlib/scala-logging-api_2.10-2.1.2.jar,/home/hadoop/spark/auxlib/scala-logging-slf4j_2.10-2.1.2.jar,/home/hadoop/spark/lib/spark-streaming-kinesis-asl_2.10-1.4.0.jar,/home/hadoop/spark/lib/amazon-kinesis-client-1.2.1.jar,/home/hadoop/spark/auxlib/maxmind-db-1.0.0.jar,/home/hadoop/spark/auxlib/geoip2-2.1.0.jar --class com.funplus.stream.KinesisDistEventsParser /home/hadoop/spark/auxlib/kinesis-app-1.0.jar /mnt/spark/temp/spark.conf >>spark.log 2>&1 &

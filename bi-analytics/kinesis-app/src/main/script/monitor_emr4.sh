#!/bin/bash

base_dir="/mnt/spark/temp/"                             #running path
proc_name="EventsParser"                                #process name
log_file=${base_dir}"spark.log"                         #log file
pid_file=${base_dir}"spark.pid"                         #pid file
pid=0
number=0
exception_num=0

proc_num()                                              #get process number
{
    number=`/bin/ps -ef | grep $proc_name | grep -v grep | wc -l`
}

proc_id()                                               #get process id
{
    pid=`/bin/ps -ef | grep $proc_name | grep -v grep | awk '{print $2}'`
}

if [ -f "$log_file" ]; then
    exception_num=`cat $log_file | grep 'Exception' | wc -l`
    # EMR 4.1.0 issue, ignore recovery exception
    s3_exception_num=`cat $log_file | grep 'AmazonS3Exception' | wc -l`
    records_exception_num=`cat $log_file | grep 'getting records using shard iterator' | wc -l`
    shard_exception_num=`cat $log_file | grep 'getting shard iterator from sequence number' | wc -l`
    amazon_exception_num=`cat $log_file | grep 'AmazonServiceException' | wc -l`
    rate_exception_num=`cat $log_file | grep 'ProvisionedThroughputExceededException' | wc -l`
    timeout_exception_num=`cat $log_file | grep 'TimeoutException' | wc -l`
    io_exception=`cat $log_file | grep 'IOException' | wc -l`
    connect_exception=`cat $log_file | grep 'ConnectException' | wc -l`
    http_exception=`cat $log_file | grep 'NoHttpResponseException' | wc -l`
    recovery_exception_total=`expr $s3_exception_num + $records_exception_num + $shard_exception_num + $amazon_exception_num + $rate_exception_num + $timeout_exception_num + $io_exception + $connect_exception + $http_exception`
    if [ $exception_num -eq $recovery_exception_total ]; then
        exception_num=0
    fi
    # Spark is hanging, restart the driver again
    yarn_scheduler_num=`cat $log_file | grep 'YarnScheduler' | wc -l`
    if [ $yarn_scheduler_num -eq 0 ]; then
        exception_num=1
    fi
fi

proc_num                                                #get process number

if [ $number -eq 0 ] || [ $exception_num -gt 0 ]        #if process exit or have Exception in log
then
    proc_id                                             #get new process id
    for i in ${pid}; do kill -9 $i; done
    time_stamp=`date +%F_%s`
    if [ -f "$log_file" ]; then
        cp ${log_file} ${log_file}.${time_stamp}
        cat /dev/null > ${log_file}                     #empty log file
    fi
    cd $base_dir && ./start_emr4.sh                     #restart

    proc_id                                             #get new process id
    echo ${pid}, `date +%F_%T` >> ${pid_file}           #log new process id in pid file

    exit 1                                              #exit with error to notify pipeline
else
    cat /dev/null > ${log_file}                         #empty log file
fi

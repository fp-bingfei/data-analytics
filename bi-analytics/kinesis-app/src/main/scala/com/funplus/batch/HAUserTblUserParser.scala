package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}


/**
  * Created by JiaTing on 09/08/16.
  */
object HAUserTblUserParser extends Logging {

  private val NULL_STRING = "NULL"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: HAUserFriendsParser <job-name> <date> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. HA_TBL_PAYMENT_PARSER)
          |    <date> is the data date to be processed
          |                 (e.g. 2016-08-08)
          |    <input-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/ff2pc/global/mongodb/user/)
          |    <output-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/results/ha/tbl_user/ha.us.prod/)
        """.stripMargin)
      System.exit(1)
    }
    val jobName = args(0)
    val inputDate = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val date = inputDate.replaceAll("-", "")
    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val inputPath = baseInputPath.concat(date).concat("/").concat("*")

    val inputRDD = sc.textFile(inputPath)
    val outPutPath = baseOutputPath + date + '/'
    val tblUserResult = inputRDD.map(record => {
      try {
        val jsonRecord = new JSONObject(record)
        getOutPutJson(jsonRecord, inputDate)
      }
      catch {
        case e: Exception => None
      }
    }).saveAsTextFile(outPutPath, classOf[GzipCodec])
    sc.stop()
  }


  private def getOutPutJson(jsonInput: JSONObject, date: String): String = {
    val outputFields = Array(
      "_id", //uid
      "uid", //snsid
      "ref", //install_source
      "create_time", //install_ts
      "os",
      "os_version",
      "country", //sns.country
      "level",
      "device",
      "language",
      "browser",
      "browser_version",
      "cash", //rc
      "coins",
      "email",
      "gender", //sns.gender
      "first_name",
      "age",
      "last_login_time", //last_login_ts
      "sign_email", //sign_email
      "birthday"//birthday 03/26/1987
    )
    val outputJson = new JSONObject()
    for (key <- outputFields) {
      var value: String = null
      try {
        if (key.equals("country_code") || key.equals("gender")) {
          val snsJson = jsonInput.getJSONObject("sns")
          value = snsJson.getString(key)
        } else {
          val numberLongJson = jsonInput.getJSONObject(key)
          value = numberLongJson.getString("$numberLong")
        }
      }
      catch {
        case e: Exception => {
          try {
            value = jsonInput.getString(key)
          }
          catch {
            case e: Exception => {
              println("missed" + key)
              value = NULL_STRING
            }
          }
        }
      }
      outputJson.put(key, value)
    }
    outputJson.toString
  }

}

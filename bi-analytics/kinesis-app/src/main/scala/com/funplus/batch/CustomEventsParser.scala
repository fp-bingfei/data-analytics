package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser
import com.funplus.util.{Utilities, EventParserUtils}
import com.funplus.validator.JsonError
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.{Logging, SparkConf, SparkContext}
import org.apache.hadoop.mapred.TextOutputFormat

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by yangzhenxuan on 10/21/15.
 */
object CustomEventsParser extends Logging{

  private val INVALID_EVENT: String = "invalid"
  private val ERROR: String = "error"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: CustomEventsParser <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/logserver/custom/{app_id}/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4) + "/" + date
    val appIds = if (args.length > 5) args(5) else ""
    val deviceInfo = if (args.length > 6 && args(6).toLowerCase == "true") args(6).toBoolean else false

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(sc.hadoopConfiguration, date, baseInputPath, appIds)
    if (inputPaths.length == 0) {
      return
    }
    val inputRDD = sc.textFile(inputPaths)

    val tmpResult = inputRDD.flatMap(record => {
      try{
        Some(EventParser.parse((record)))
      } catch {
        case e: Exception => None
      }
    }).map(
        jsonString => {
          val jsonObject = new JSONObject(jsonString)
          // Check error field
          var error = None: Option[String]
          if (jsonObject.has(ERROR)) {
            error = Some(jsonObject.getString(ERROR))
          }
          var output = None: Option[(String, String)]

          if(error.isEmpty)
            output = Some(getOutputString(jsonString, jsonObject, baseOutputPath, jsonObject.getString(EVENT), false, deviceInfo))
          else if (error.equals(JsonError.VALIDATION_FAILED.toString))
            output = Some(EventParserUtils.getOutputJson(jsonString, new JSONObject(jsonObject.getString(INVALID_JSON)), baseOutputPath, INVALID_EVENT, false))
          else
            output = Some(("invalid/archive", jsonString))

          /* val event = if (error.isEmpty) jsonObject.getString("event") else INVALID_EVENT
           val output: (String, String) = EventParserUtils.getOutputJson(jsonString, jsonObject, baseOutputPath, event)*/

          output.get
        }).groupByKey()

    val result = tmpResult.map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath, classOf[String], classOf[String],
        classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])

    val counters = tmpResult.map({case (k,v) =>
      (k, v.size)
    }).repartition(1).
      saveAsHadoopFile(scratchOutputPath + "/counters", classOf[Text], classOf[IntWritable],
        classOf[TextOutputFormat[Text, IntWritable]])

    sc.stop()
  }

  /**
   * Get output String for custom
   */
  def getOutputString(jsonOutput: String, jsonObject: JSONObject, basePath: String,
                      event: String, appPartition: Boolean = false, deviceInfo: Boolean = false): (String, String) = {
    val attributes = List("data_version", "app_id", "ts", "ts_pretty", "event",
      "user_id", "session_id")
    val properties = if (deviceInfo)
      List("os", "install_ts_pretty", "lang", "level", "browser",
      "app_version", "ip", "country_code", "d_c1", "d_c2", "d_c3",
      "d_c4", "d_c5", "m1", "m2", "m3", "m4", "m5", "c1", "c2", "idfa", "gaid")
    else
      List("os", "install_ts_pretty", "lang", "level", "browser",
      "app_version", "ip", "country_code", "d_c1", "d_c2", "d_c3",
      "d_c4", "d_c5", "m1", "m2", "m3", "m4", "m5", "c1", "c2")

    val builder = new StringBuilder("")
    try {
      var biVersion: String = null
      if (jsonObject.has("data_version")) {
        // 2.0: data_version
        biVersion = jsonObject.getString("data_version")
      } else if (jsonObject.has("bi_version")) {
        // 1.2
        biVersion = jsonObject.getString("bi_version")
      } else {
        //biVersion is not available then default it to 1.1
        biVersion = "1.1"
      }

      // Check log_ts
      var log_ts = None: Option[Long]
      if (jsonObject.has("log_ts")) {
        log_ts = Option(jsonObject.getLong("log_ts"))
      }

      var app_id = None: Option[String]
      var ts = None: Option[Long]
      // app and ts varies for biVersion 1.1
      if ("1.1".equals(biVersion)) {
        app_id = Option(jsonObject.getString("@key"))
        ts = Option(jsonObject.getLong("@ts"))
      } else if (log_ts.nonEmpty) {
        app_id = Option(jsonObject.getString("app_id"))
        // output path should depends on log_ts and not ts.
        ts = log_ts
      } else {
        // use ts
        app_id = Option(jsonObject.getString("app_id"))
        ts = Option(jsonObject.getLong("ts"))
      }
      for (key <- attributes) {
        if(jsonObject.has(key)) {
          builder.append(Option(jsonObject.getString(key)).getOrElse("")).append("\t")
        }
        else {
          builder.append("\t")
        }
      }
      for (key <- properties) {
        if (jsonObject.getJSONObject("properties").has(key)) {
          builder.append(Option(jsonObject.getJSONObject("properties").getString(key)).getOrElse(""))
            .append("\t")
        }
        else {
          builder.append("\t")
        }
      }
      val outputPath = getOutputPath(basePath, event, app_id.get, ts.get, appPartition)
      (outputPath, builder.toString())
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive", jsonOutput)
      }
    }
  }

  def getOutputPath(basePath: String, event: String, app_id: String, ts: Long,
                    appPartition: Boolean = false, eventPartition: Boolean = true):String = {

    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }
    // Set game partition
    if (appPartition) {
      // Get short name of game
      val appName = if (app_id.indexOf('.') > 0) app_id.substring(0, app_id.indexOf('.')) else app_id
      builder.append("game=").append(appName).append("/")
    }
    // Set event partition
    if (eventPartition) {
      builder.append("event=").append(event).append("/")
    } else {
      builder.append(event).append("/")
    }
    val outputPath = builder.append("app=").append(app_id).append("/")
      .append("year=").append(Utilities.getFormattedDateForTimestamp(ts, "yyyy")).append("/")
      .append("month=").append(Utilities.getFormattedDateForTimestamp(ts, "MM")).append("/")
      .append("day=").append(Utilities.getFormattedDateForTimestamp(ts, "dd")).append("/")
      .append("hour=").append(Utilities.getFormattedDateForTimestamp(ts, "HH")).append("/")
      .append("archive").append(Utilities.getFormattedDateForTimestamp(ts, "mm")(0)).toString()
    outputPath
  }

  private def generateInputPaths(conf: Configuration, date:String, baseInputPath:String,
                                 appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          val inputPath = baseInputPath.concat(appId).concat(datePath)
          if (EventParserUtils.checkPathExists(conf, inputPath)) {
            inputPathBuffer += inputPath.concat("*")
          }
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }

}

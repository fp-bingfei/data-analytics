package com.funplus.batch

import java.io.{ByteArrayOutputStream, InputStreamReader}
import java.util
import java.util.HashMap

import com.amazonaws.util.json.JSONObject
import com.funplus.batch.EventsParquetDaily._
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.output.SerializationExtensions._
import com.funplus.parser.EventParser
import com.funplus.util.EventParserUtils
import com.funplus.validator.JsonError
import com.typesafe.config.ConfigFactory
import org.apache.avro.Schema
import org.apache.avro.generic.{GenericData, GenericDatumWriter, GenericRecord}
import org.apache.avro.io.EncoderFactory
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.hadoop.io.{NullWritable, Text}
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by Yanyu on 02/18/16.
 *
 * To generate parquet files daily directly from raw JSON file for RS/FFS
 */

object EventsTextToParquetDaily extends Logging {
  private val ERROR: String = "error"


  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: EventsTextToParquetDaily <job-name> <date> <base-input-path> <base-output-path> \
          |                                <scratch-output-path> [<app_ids>] [<parquet-config>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. rs_text_to_parquet_daily)
          |    <date> is the data date or hour to be processed
          |                 (e.g. 2015-12-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/fluentd/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/rs_1_1/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/rs_1_1/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4)
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(sc.hadoopConfiguration, date, baseInputPath, appIds)
    // Check input paths
    if (inputPaths.length == 0) {
      return
    }
    val inputRDD = sc.textFile(inputPaths)

    // Broadcast date to generate file name
    val broadcastDate = sc.broadcast(date)

    val result = inputRDD.flatMap(record => {
      try {
        Some(EventParser.parse(record))
      } catch {
        case e: Exception => None
      }
    }).filter{record => ! record.contains("item_transaction")
     }
      .map(
      jsonString => {
        val jsonObject = new JSONObject(jsonString)
        // Check error field
        var error = None: Option[String]
        if (jsonObject.has(ERROR)) {
          error = Some(jsonObject.getString(ERROR))
        }
        var output = None: Option[(String, String)]
        error match {
          case Some(jsonError) => {
            // Invalid events
            output = Some(("invalid/archive", jsonString))
          }
          case None => {
            // Valid events
            // Read avro schema
            val stream = getClass().getClassLoader.getResourceAsStream("avro_schema_for_event_partition.avsc")
            val avroSchema = new Schema.Parser().parse(stream)

            // Generate generic record
            val datum = new GenericData.Record(avroSchema)
            datum.put("data_version", "2.0")
            datum.put("app_id", jsonObject.get("@key").toString)
            datum.put("ts", jsonObject.get("@ts").toString)
            datum.put("ts_pretty", jsonObject.get("@ts_pretty").toString)
            //datum.put("event", jsonObject.get("event").toString)
            datum.put("user_id", jsonObject.get("uid").toString)
            datum.put("session_id", "")
            //properties

            val keys = jsonObject.keys().toList
            val propertiesMap = new HashMap[String, String](keys.length - 4)
            keys.foreach {
              key => {
                if (! key.equals("@key") && ! key.equals("@ts") && ! key.equals("@ts_pretty")
                  && ! key.equals("uid")) {
                  propertiesMap.put(key.toString, jsonObject.get(key.toString).toString)
                }
              }
            }
            datum.put("properties", propertiesMap)

            val outputPath = EventParserUtils.getDailyOutputPath(baseOutputPath, jsonObject.get("event").toString,
              jsonObject.get("@key").toString, broadcastDate.value, jsonObject.getLong("@ts"), true)

            //Encode the record as JSON
            val out = new ByteArrayOutputStream()
            val encoder = EncoderFactory.get().jsonEncoder(avroSchema, out)
            val writer = new GenericDatumWriter[GenericRecord](avroSchema)
            writer.write(datum, encoder)
            encoder.flush()
            output = Some((outputPath, new String(out.toByteArray)))
          }
        }

        output.get
      })
      .groupByKey()
      .map({case (k,v) => {
        //val v1 = v.mkString(EOL)
        // Add EOL length and minus the head length (will be added back during StringBuilder construction)
        val len = v.map(_.length).reduce(_+_) + (v.size - 1) * EOL.length - v.head.length
        val v1 = (v.tail.foldLeft(new StringBuilder(len, v.head)) {(acc, e) => acc.append(EOL).append(e)}).toString
        (k, v1)
      }})

    result.filter {case (key, value) =>
      ! key.contains("invalid/archive")
    }.saveAsMultipleParquetFiles(k => k, scratchOutputPath, "avro_schema_for_event_partition.avsc")

    result.filter {case  (key, value) =>
      key.contains("invalid/archive")
    }.saveAsHadoopFile(scratchOutputPath.concat("invalid").concat("/"), classOf[String], classOf[String],
     classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])


    sc.stop()
  }

  private def generateInputPaths(conf: Configuration, date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-","/")).concat("/")
      val inputPathBuffer = new  ArrayBuffer[String]()
      appIdArray.foreach{
        appId => {
          val inputPath = baseInputPath.concat(appId).concat(datePath)
          if (EventParserUtils.checkPathExists(conf, inputPath)) {
            inputPathBuffer += inputPath.concat("*")
          }
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }
}

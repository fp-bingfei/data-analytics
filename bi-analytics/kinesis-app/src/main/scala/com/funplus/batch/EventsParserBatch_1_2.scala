package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import com.funplus.format.{RDDMultipleSequenceFileOutputFormat, RDDMultipleTextFileOutputFormat}
import com.funplus.parser.EventParser
import com.funplus.util.EventParserUtils
import com.funplus.validator.JsonError
import org.apache.hadoop.io.{NullWritable, Text}
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}
import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._
import com.funplus.output.SerializationExtensions._

/**
 * Created by chunzeng on 12/22/15.
 */

object EventsParserBatch_1_2 extends Logging {

  private val INVALID_EVENT: String = "invalid"
  private val ERROR: String = "error"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: EventsParserBatch_1_2 <job-name> <date> <base-input-path> <base-output-path> \
          |                                <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_hourly)
          |    <date> is the data date or hour to be processed
          |                 (e.g. 2015-12-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/fluentd/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/events_seq/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4)
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    val inputRDD = sc.textFile(inputPaths)
    // Broadcast date to generate file name
    val broadcastDate = sc.broadcast(date)

    val result = inputRDD.flatMap(record => {
      try {
        Some(EventParser.parse(record))
      } catch {
        case e: Exception => None
      }
    }).map(
      jsonString => {
        val jsonObject = new JSONObject(jsonString)
        // Check error field
        var error = None: Option[String]
        if (jsonObject.has(ERROR)) {
          error = Some(jsonObject.getString(ERROR))
        }
        var output = None: Option[(String, String)]
        error match {
          case Some(jsonError) => {
            // Invalid events
            if (jsonError.equals(JsonError.VALIDATION_FAILED.toString)) {
              output = Some(EventParserUtils.getOutputJson(jsonString, new JSONObject(jsonObject.getString(INVALID_JSON)), baseOutputPath, INVALID_EVENT, false, broadcastDate.value))
            } else {
              // Other exceptions
              output = Some(("invalid/archive", jsonString))
            }
          }
          case None => {
            // Valid events
            output = Some(EventParserUtils.getOutputJson(jsonString, jsonObject, baseOutputPath, jsonObject.getString(EVENT), false, broadcastDate.value))
          }
        }

        output.get
      })
      .groupByKey()
      .map({case (k,v) => {
        val v1 = v.mkString(EOL)
        (k, v1)
      }})
//      .saveAsHadoopFile(scratchOutputPath, classOf[NullWritable], classOf[Text],
//        classOf[RDDMultipleSequenceFileOutputFormat], classOf[GzipCodec])
      .saveAsMultipleSequenceFiles(k => k + "-" + currentTime, scratchOutputPath)

    sc.stop()
  }

  private def generateInputPaths(date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    // Fix the issue of different s3 path for EU
    val euAppIds = Array("farm.de.prod", "farm.fr.prod", "farm.it.prod", "farm.nl.prod", "farm.pl.prod")
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/").concat("*")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          if (euAppIds.contains(appId)) {
            // EU appId
            inputPathBuffer += "s3n://com.funplusgame.bidata/fluentd-eu-mirror/".concat(appId).concat(datePath)
          } else {
            // Other appId
            inputPathBuffer += baseInputPath.concat(appId).concat(datePath)
          }
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }


}

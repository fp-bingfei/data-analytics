package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}


/**
  * Created by JiaTing on 18/03/16.
  */
object HAUserFriendsParser extends Logging {

  private val NULLSTRING = "NULL"
  private val NULL_INT = 0

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: HAUserFriendsParser <job-name> <date> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. HA_USER_FRIENDS_PARSER)
          |    <date> is the data date to be processed
          |                 (e.g. 2016-03-01)
          |    <input-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/ff2pc/global/mongodb/friend/)
          |    <output-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/results/ha/friends/)
        """.stripMargin)
      System.exit(1)
    }
    val jobName = args(0)
    val inputDate = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val date = inputDate.replaceAll("-","")
    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val inputPath = baseInputPath.concat(date).concat("/").concat("*")

    val inputRDD = sc.textFile(inputPath)
    val outPutPath = baseOutputPath + date + '/'
    val tblUserResult = inputRDD.map(record => {
      try {
        val jsonRecord = new JSONObject(record)
        getOutPutJson(jsonRecord, inputDate)
      }
      catch {
        case e: Exception => None
      }
    }).saveAsTextFile(outPutPath, classOf[GzipCodec])
    sc.stop()
  }


  private def getOutPutJson(jsonInput: JSONObject, date: String): String = {
    val outputFields = Array(
      "game",
      "app_id",
      "_id",
      "cfuids",
      "fuids",
      "fb_friends_count",
      "update_date"
    )
    val outputJson = new JSONObject()
    for (key <- outputFields) {
      if (key == "game") {
        outputJson.put(key, "ha")
      } else if (key == "app_id") {
        outputJson.put(key, "")
      } else if (key == "_id") {
        outputJson.put("uid", jsonInput.getString(key))
      } else if (key == "cfuids") {
        var gameFriendsCount = 0
        try {
          val gameFriendsList = jsonInput.getJSONArray(key)
          gameFriendsCount = gameFriendsList.length()
        } catch {
          case e: Exception => {
            println("No json array: " + key)
          }
        }
        outputJson.put("game_friends_count", gameFriendsCount)
      } else if (key == "fuids") {
        var gameAndFBFriendsCount = 0
        try {
          val gameAndFBFriendsList = jsonInput.getJSONArray(key)
          gameAndFBFriendsCount = gameAndFBFriendsList.length() - 2
        } catch {
          case e: Exception => {
            println("No json array: " + key)
          }
        }
        outputJson.put("game_and_fb_friends_count", gameAndFBFriendsCount)
      } else if (key == "update_date") {
        outputJson.put(key, date)
      } else if (key == "fb_friends_count") {
        outputJson.put(key, NULL_INT)
      } else {
        var value: String = null
        try {
          value = jsonInput.getString(key)
        }
        catch {
          case e: Exception => {
            println("missed " + key)
            value = NULLSTRING
          }
        }
        outputJson.put(key, value)
      }
    }
    outputJson.toString
  }

}

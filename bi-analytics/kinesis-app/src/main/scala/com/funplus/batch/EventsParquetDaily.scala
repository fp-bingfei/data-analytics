package com.funplus.batch

import collection.JavaConversions._
import com.amazonaws.util.json.JSONObject
import com.funplus.output.SerializationExtensions._
import com.funplus.util.EventParserUtils
import com.typesafe.config.ConfigFactory
import java.io.{ByteArrayOutputStream, InputStreamReader}
import java.util.HashMap
import org.apache.avro.generic.{GenericDatumWriter, GenericData, GenericRecord}
import org.apache.avro.io.EncoderFactory
import org.apache.avro.Schema
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.{Text, NullWritable}
import org.apache.spark.{Logging, SparkConf, SparkContext}
import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by chunzeng on 01/27/16.
 *
 * To generate parquet files daily for Farm
 */

object EventsParquetDaily extends Logging {

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: EventsParquetDaily <job-name> <date> <base-input-path> <base-output-path> \
          |                                <scratch-output-path> [<app_ids>] [<parquet-config>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_parquet_daily)
          |    <date> is the data date or hour to be processed
          |                 (e.g. 2015-12-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/farm_1_1/events_seq/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/events_daily/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
          |    <parquet-config> is the config file path to output events as parquet files
          |                 (e.g. s3n://com.funplusgame.bidata/dev/spark/farm_parquet.conf)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4)
    val appIds = if (args.length > 5) args(5) else ""
    val parquetConfig = if (args.length > 6) args(6) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    // Get the events list
    var events = List[String]()
    if (parquetConfig.length > 0) {
      val confPath = new Path(parquetConfig)
      val fileSystem = confPath.getFileSystem(sc.hadoopConfiguration)
      val fileReader = new InputStreamReader(fileSystem.open(confPath))
      val conf = ConfigFactory.parseReader(fileReader)
      events = events ++ conf.getStringList("events").toList
      fileReader.close()
    }

    val inputPaths = generateInputPaths(sc.hadoopConfiguration, date, baseInputPath, appIds, events)
    // Check input paths
    if (inputPaths.length == 0) {
      return
    }
    val inputRDD = sc.sequenceFile(inputPaths, classOf[NullWritable], classOf[Text])

    val result = inputRDD.map {
      case (key, record) => {
        val jsonObject = new JSONObject(record.toString)

        // Read avro schema
        val stream = getClass().getClassLoader.getResourceAsStream("avro_schema.avsc")
        val avroSchema = new Schema.Parser().parse(stream)

        // Generate generic record
        val datum = new GenericData.Record(avroSchema)
        datum.put("data_version", "2.0")
        datum.put("app_id", jsonObject.get("@key").toString)
        datum.put("ts", jsonObject.get("@ts").toString)
        datum.put("ts_pretty", jsonObject.get("@ts_pretty").toString)
        datum.put("event", jsonObject.get("event").toString)
        datum.put("user_id", jsonObject.get("uid").toString)
        datum.put("session_id", "")
        // properties
        val keys = jsonObject.keys().toList
        val propertiesMap = new HashMap[String, String](keys.length - 5)
        keys.foreach {
          key => {
            if (! key.equals("@key") && ! key.equals("@ts") && ! key.equals("@ts_pretty")
              && ! key.equals("event") && ! key.equals("uid")) {
              propertiesMap.put(key.toString, jsonObject.get(key.toString).toString)
            }
          }
        }
        datum.put("properties", propertiesMap)

        val outputPath = EventParserUtils.getDailyOutputPath(baseOutputPath, jsonObject.get("event").toString,
          jsonObject.get("@key").toString, date)

        // Encode the record as JSON
        val out = new ByteArrayOutputStream()
        val encoder = EncoderFactory.get().jsonEncoder(avroSchema, out)
        val writer = new GenericDatumWriter[GenericRecord](avroSchema)
        writer.write(datum, encoder)
        encoder.flush()

        (outputPath, new String(out.toByteArray))
      }}
      .groupByKey()
      .map({case (k,v) => {
        //val v1 = v.mkString(EOL)
        // Add EOL length and minus the head length (will be added back during StringBuilder construction)
        val len = v.map(_.length).reduce(_+_) + (v.size - 1) * EOL.length - v.head.length
        val v1 = (v.tail.foldLeft(new StringBuilder(len, v.head)) {(acc, e) => acc.append(EOL).append(e)}).toString
        (k, v1)
      }})
      .saveAsMultipleParquetFiles(k => k, scratchOutputPath, "avro_schema.avsc")

    sc.stop()
  }

  private def generateInputPaths(conf: Configuration, date:String, baseInputPath:String, appIds:String,
                                 events: List[String]) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val dateObj = date.split("-")
      val datePath = "/".concat("year=").concat(dateObj(0)).concat("/")
                        .concat("month=").concat(dateObj(1)).concat("/")
                        .concat("day=").concat(dateObj(2)).concat("/")
      val inputPathBuffer = new ArrayBuffer[String]()
      events.foreach {
        eventName => {
          appIdArray.foreach {
            appId => {
              val inputPath = baseInputPath.concat(eventName).concat("/").concat("app=").concat(appId).concat(datePath)
              if (EventParserUtils.checkPathExists(conf, inputPath)) {
                inputPathBuffer += inputPath.concat("*")
              }
            }
          }
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }

}

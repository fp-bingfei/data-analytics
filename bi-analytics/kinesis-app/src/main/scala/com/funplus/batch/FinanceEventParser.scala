package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser
import com.funplus.util.{Utilities, EventParserUtils}
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions

import org.apache.hadoop.mapred.TextOutputFormat
import org.apache.spark.{Logging, SparkContext, SparkConf}

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by yangzhenxuan on 11/23/15.
 */
object FinanceEventParser extends Logging{
  private val INVALID_EVENT: String = "invalid"
  private val ERROR: String = "error"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"
  private val CURRENCY_SPENT: String = "c_currency_spent"
  private val CURRENCY_RECEIVED: String = "c_currency_received"
  private val CURRENCY_TYPE: String = "d_currency_type"
  private val CURRENCY_AMOUNT: String = "m_currency_amount"

  private val attributes = List("data_version", "app_id", "ts", "ts_pretty", "event",
    "user_id", "session_id")
  private val properties = List("os", "install_ts_pretty", "lang", "level", "browser",
    "app_version", "ip", "country_code", "c_currency_received","c_currency_spent",
    "d_transaction_type", "d_action")
  private val currencyChanges = List(CURRENCY_SPENT, CURRENCY_RECEIVED)
  private val currencyChangesLegacy = List("d_currency_received_type",
    "d_currency_received","d_currency_spent_type","d_currency_spent")
  private val propertiesLegacy = List("os", "install_ts_pretty", "lang", "level", "browser",
    "app_version", "ip", "country_code", "d_currency_received_type",
    "d_currency_received","d_currency_spent_type","d_currency_spent",
    "d_transaction_type", "d_action")


  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: CustomEventsParser <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/logserver/custom/{app_id}/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4) + "/" + date
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    val inputRDD = sc.textFile(inputPaths)

    val tmpResult = inputRDD.flatMap(record => {
      try{
        Some(EventParser.parse((record)))
      } catch {
        case e: Exception => None
      }
    }).map(
        jsonString => {
          val jsonObject = new JSONObject(jsonString)
          // Check error field
          var error = None: Option[String]
          if (jsonObject.has(ERROR)) {
            error = Some(jsonObject.getString(ERROR))
          }
          var output = None: Option[(String, List[String])]

          if(error.isEmpty)
            output = Some(getOutputString(jsonString, jsonObject, baseOutputPath, jsonObject.getString(EVENT), false))
          else
            output = Some(getOutputString(jsonString, new JSONObject(jsonObject.getString(INVALID_JSON)), baseOutputPath, INVALID_EVENT, false))

          /* val event = if (error.isEmpty) jsonObject.getString("event") else INVALID_EVENT
           val output: (String, String) = EventParserUtils.getOutputJson(jsonString, jsonObject, baseOutputPath, event)*/

          output.get
        }).reduceByKey(_ ::: _)

      val currencyResult = tmpResult.map({case (k,v) => {
        val v1 = v.mkString(EOL)
        (k, v1)
      }})
        .saveAsHadoopFile(scratchOutputPath, classOf[String], classOf[String],
          classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])

      val counters = tmpResult.map({case (k,v) =>
          (k, v.size)
        }).repartition(1).
        saveAsHadoopFile(scratchOutputPath + "/counters/finance/", classOf[Text], classOf[IntWritable],
          classOf[TextOutputFormat[Text, IntWritable]])

    sc.stop()
  }


  def getOutputString(jsonOutput: String, jsonObject: JSONObject, basePath: String,
                            event: String, appPartition: Boolean = false): (String, List[String]) = {

    var output = Nil : List[String]

    try {
      val biVersion: String = jsonObject.getString("data_version")
      val app_id = Option(jsonObject.getString("app_id"))
      val ts = Option(jsonObject.getLong("ts"))
      val propertiesJson = jsonObject.getJSONObject("properties")

      val builder = new StringBuilder("")
      var flag = 0 : Int

      // building output attributes part
      for (key <- attributes) {
        if(jsonObject.has(key)) {
          builder.append(Option(jsonObject.getString(key)).getOrElse("")).append("\t")
        }
        else {
          builder.append("\t")
        }
      }
      // building properties
      // judge if it's new format
      //@TODO: replace if-else with case if possible
      for (key <- currencyChanges) {
        if (flag == 0 && propertiesJson.has(key)) {
          flag = 1
        }
      }
      for (key <- currencyChangesLegacy) {
        if (flag == 0 && propertiesJson.has(key)) {
          flag = 2
        }
      }

      if (flag == 1) {
        for (key <- properties) {
          if (propertiesJson.has(key)) {
            if (currencyChanges.contains(key)){
              builder.append(key).append("\t").append(key).append("\t")
            }
            else{
              builder.append(Option(jsonObject.getJSONObject("properties").getString(key)).getOrElse("")).append("\t")
            }
          }
          else if (currencyChanges.contains(key)){
            builder.append("\t").append("\t")
          }
          else {
            builder.append("\t")
          }
        }
        if (builder.containsSlice(CURRENCY_RECEIVED)) {
          val currencyReceived = propertiesJson.getJSONArray(CURRENCY_RECEIVED)
          for (i <- 0 until currencyReceived.length()) {
            var tmpBuilder: String = builder.toString()
            if (currencyReceived.getJSONObject(i).getInt(CURRENCY_AMOUNT) > 0) {
              tmpBuilder = tmpBuilder.replaceFirst(CURRENCY_RECEIVED, currencyReceived.getJSONObject(i).getString(CURRENCY_TYPE))
              tmpBuilder = tmpBuilder.replaceFirst(CURRENCY_RECEIVED, currencyReceived.getJSONObject(i).getString(CURRENCY_AMOUNT))
              tmpBuilder = tmpBuilder.replaceAll(CURRENCY_SPENT, "")
              output = tmpBuilder :: output
            }
          }
        }
        if (builder.containsSlice(CURRENCY_SPENT)) {
          val currencySpent = propertiesJson.getJSONArray(CURRENCY_SPENT)
          for (i <- 0 until currencySpent.length()) {
            var tmpBuilder: String = builder.toString()
            if (currencySpent.getJSONObject(i).getInt(CURRENCY_AMOUNT) > 0) {
              tmpBuilder = tmpBuilder.replaceFirst(CURRENCY_SPENT, currencySpent.getJSONObject(i).getString(CURRENCY_TYPE))
              tmpBuilder = tmpBuilder.replaceFirst(CURRENCY_SPENT, currencySpent.getJSONObject(i).getString(CURRENCY_AMOUNT))
              tmpBuilder = tmpBuilder.replaceAll(CURRENCY_RECEIVED, "")
              output = tmpBuilder :: output
            }
          }
        }
      }
      else if (flag == 2) {
        for (key <- propertiesLegacy) {
          if (propertiesJson.has(key)) {
            builder.append(Option(jsonObject.getJSONObject("properties").getString(key)).getOrElse(""))
              .append("\t")
          }
          else {
            builder.append("\t")
          }
        }
        output = builder.toString() :: output
      }
      val outputPath = getOutputPath(basePath, event, app_id.get, ts.get, appPartition)
      (outputPath, output)
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive", jsonOutput :: output)
      }
    }
  }

  def getOutputPath(basePath: String, event: String, app_id: String, ts: Long,
                    appPartition: Boolean = false, eventPartition: Boolean = true):String = {

    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }
    // Set game partition
    if (appPartition) {
      // Get short name of game
      val appName = if (app_id.indexOf('.') > 0) app_id.substring(0, app_id.indexOf('.')) else app_id
      builder.append("game=").append(appName).append("/")
    }
    // Set event partition
    if (eventPartition) {
      builder.append("event=").append(event).append("/")
    } else {
      builder.append(event).append("/")
    }
    val outputPath = builder.append("app=").append(app_id).append("/")
      .append("year=").append(Utilities.getFormattedDateForTimestamp(ts, "yyyy")).append("/")
      .append("month=").append(Utilities.getFormattedDateForTimestamp(ts, "MM")).append("/")
      .append("day=").append(Utilities.getFormattedDateForTimestamp(ts, "dd")).append("/")
      .append("hour=").append(Utilities.getFormattedDateForTimestamp(ts, "HH")).append("/")
      .append("archive").append(Utilities.getFormattedDateForTimestamp(ts, "mm")(0)).toString()
    outputPath
  }

  private def generateInputPaths(date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/").concat("*")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          inputPathBuffer += baseInputPath.concat(appId).concat(datePath)
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }
}

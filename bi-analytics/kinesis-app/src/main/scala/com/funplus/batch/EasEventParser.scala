package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}

object EasEventParser extends Logging {

  private val NULL_STRING = "NULL"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: EasEventParser <job-name> <date> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. Eas_Job)
          |    <date> is the data date to be processed
          |                 (e.g. 2016-09-22)
          |    <input-path> is the output s3 path
          |                 (e.g. s3n://com.funplus.bithirdparty/logserver/sendgrid/)
          |    <output-path> is the output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/eas/)
        """.stripMargin)
      System.exit(1)
    }
    val jobName = args(0)
    val inputDate = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val date = inputDate.replaceAll("-", "")
    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val inputPath = baseInputPath.concat("/").concat("*")

    val inputRDD = sc.textFile(inputPath)
    val outPutPath = baseOutputPath + inputDate + '/'
    val EasResult = inputRDD.map(record => {
      try {
        val jsonRecord = new JSONObject(record)
        getOutPutJson(jsonRecord, inputDate)
      }
      catch {
        case e: Exception => None
      }
    }).saveAsTextFile(outPutPath, classOf[GzipCodec])
    sc.stop()
  }


  private def getOutPutJson(jsonInput: JSONObject, date: String): String = {
      //var ts = None: Option[Long]
            
      //var app_id: String = null
      
      val attributes = List(
      "snsid", 
      "email", 
      "category", 
      "uid", 
      if(jsonInput.has("app_id")) "app_id" else "app",
      if(jsonInput.has("timestamp")) "timestamp" else "time",
      "ip", 
      "event",
      "day",
      "campaign",
      "s3_path"
    )
      
      val builder = new StringBuilder("")
         try {
      
      
      for (key <- attributes) {
        if(jsonInput.has(key)) {
          builder.append(Option(jsonInput.getString(key)).getOrElse("")).append("\t")
        }
        else {
          builder.append("\t")
        }
      }
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive")
      }
    }
     builder.toString()
  }
}
    
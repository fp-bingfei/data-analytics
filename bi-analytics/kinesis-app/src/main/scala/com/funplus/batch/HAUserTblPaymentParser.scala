package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}


/**
  * Created by JiaTing on 18/03/16.
  */
object HAUserTblPaymentParser extends Logging {

  private val NULL_STRING = "NULL"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: HAUserFriendsParser <job-name> <date> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. HA_TBL_PAYMENT_PARSER)
          |    <date> is the data date to be processed
          |                 (e.g. 2016-03-01)
          |    <input-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/ff2pc/global/mongodb/friend/)
          |    <output-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/results/ha/friends/)
        """.stripMargin)
      System.exit(1)
    }
    val jobName = args(0)
    val inputDate = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val date = inputDate.replaceAll("-", "")
    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val inputPath = baseInputPath.concat(date).concat("/").concat("*")

    val inputRDD = sc.textFile(inputPath)
    val outPutPath = baseOutputPath + date + '/'
    val tblUserResult = inputRDD.map(record => {
      try {
        val jsonRecord = new JSONObject(record)
        getOutPutJson(jsonRecord, inputDate)
      }
      catch {
        case e: Exception => None
      }
    }).saveAsTextFile(outPutPath, classOf[GzipCodec])
    sc.stop()
  }


  private def getOutPutJson(jsonInput: JSONObject, date: String): String = {
    val outputFields = Array(
      "snsid",
      "item",
      "currency",
      "payTime",
      "tid",
      "trackRef",
      "type",
      "amount",
      "gameamount",
      "logTime",
      "status")
    val outputJson = new JSONObject()
    for (key <- outputFields) {
      var value: String = null
      try {
        val numberLongJson = jsonInput.getJSONObject(key)
        value = numberLongJson.getString("$numberLong")
      }
      catch {
        case e: Exception => {
          try {
            value = jsonInput.getString(key)
          }
          catch {
            case e: Exception => {
              println("missed" + key)
              value = NULL_STRING
            }
          }
        }
      }
      outputJson.put(key, value)
    }
    outputJson.toString
  }

}

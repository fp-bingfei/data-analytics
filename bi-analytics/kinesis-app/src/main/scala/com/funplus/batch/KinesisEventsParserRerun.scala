package com.funplus.batch


import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser
import com.funplus.util.EventParserUtils
import com.funplus.validator.JsonError
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by balaji on 7/13/15.
 */

object KinesisEventsParserRerun extends Logging {

  private val INVALID_EVENT: String = "invalid"
  private val ERROR: String = "error"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: KinesisEventsParserRerun <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/input/kinesis/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/kinesis/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4)
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    val inputRDD = sc.textFile(inputPaths)
    //val inputRDD = sc.textFile("/Users/balaji/funplus/code/misc/batch/input/us/*")

    val result = inputRDD.flatMap(record => {
      try {
        Some(EventParser.parse(record))
      } catch {
        case e: Exception => None
      }
    }).map(
        jsonString => {
          val jsonObject = new JSONObject(jsonString)
          // Check error field
          var error = None: Option[String]
          if (jsonObject.has(ERROR)) {
            error = Some(jsonObject.getString(ERROR))
          }
          var output = None: Option[(String, String)]
          error match {
            case Some(jsonError) => {
              // Invalid events
              if (jsonError.equals(JsonError.VALIDATION_FAILED.toString)) {
                output = Some(EventParserUtils.getOutputJson(jsonString, new JSONObject(jsonObject.getString(INVALID_JSON)), baseOutputPath, INVALID_EVENT, true))
              } else {
                // Other exceptions
                output = Some(("invalid/archive", jsonString))
              }
            }
            case None => {
              // Valid events
              output = Some(EventParserUtils.getOutputJson(jsonString, jsonObject, baseOutputPath, jsonObject.getString(EVENT), true))
            }
          }

          /* val event = if (error.isEmpty) jsonObject.getString("event") else INVALID_EVENT
           val output: (String, String) = EventParserUtils.getOutputJson(jsonString, jsonObject, baseOutputPath, event)*/

          output.get
        }).groupByKey().map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath, classOf[String], classOf[String],
        classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])
    sc.stop()

  }

  private def generateInputPaths(date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/").concat("*")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          inputPathBuffer += baseInputPath.concat(appId).concat(datePath)
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }


}

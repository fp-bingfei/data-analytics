package com.funplus.batch

import java.sql.Date

import com.amazonaws.util.json.JSONObject
import org.apache.hadoop.fs.Path
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{DateType, IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, _}
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

//import play.api.libs.json._

import scala.util.{Failure, Try}

object ResourceTest {
  def main(args: Array[String]): Unit = {
    val logFile = "/Users/funplus/Downloads/rstest/*" // Should be some file on your system
    val conf = new SparkConf().setAppName("Resource Test")
    //.setMaster("local[*]")
    val sc = new SparkContext(conf)
    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", "AKIAJRKDNC52OAINDWKA")
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey", "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")

    //options are SaveMode.Overwrite/ErrorIfExists/Append/Ignore, default is ErrorIfExists
    val inputPath = args(0)
    /*val saveMode = if (args.length>1) {args(1) match {
      case "overwrite" => SaveMode.Overwrite
      case "append" => SaveMode.Append
      case "ignore" => SaveMode.Ignore
      case _ => SaveMode.ErrorIfExists
    }} else SaveMode.Append*/
    //val logData = sc.textFile(logFile)

    var inputPathBuffer = new ArrayBuffer[String]()
    inputPath.split(",").foreach {
      inputPath => {
        println(inputPath)
        val s3Path = new Path(inputPath)
        val fs = s3Path.getFileSystem(sc.hadoopConfiguration)
        val statuses = fs.globStatus(s3Path)
        if (statuses.size>0) {
          println("Good path!")
          statuses.foreach
          {
            status => inputPathBuffer += status.getPath().toString;
          }
        }
      }
    }
    val inputPaths = inputPathBuffer.toArray.mkString(",")

    //processConfig contains the path of the values to be extracted
    class processConfig(
                         var groupbyCols: List[List[String]],
                         var arrayPath: (List[String], String, String),
                         var addCols: List[(String, String)],
                         var clusterDB: (String, String),
                         var destTable: String)

    //for every input path (source), we support a list of process
    class Config(
                  var name: String,
                  var source: String,
                  var processes: List[processConfig])

    val dw_processes = new processConfig(
      List(List("user_id"), List("properties", "d_action"), List("app_id"), List("properties", "level"), List("properties", "gameserver_id")),
      (List("properties", "c_currency_received"), "d_currency_type", "m_currency_amount"),
      List(("flow","in")),
      ("custom-funplus", "custom"),
      "dragonwar.agg_resource_test")::
      new processConfig(
        List(List("user_id"), List("properties", "d_action"), List("app_id"), List("properties", "level"),List("properties", "gameserver_id")),
        (List("properties", "c_currency_spent"), "d_currency_type", "m_currency_amount"),
        List(("flow","out")),
        ("custom-funplus", "custom"),
        "dragonwar.agg_resource_test")::
      Nil

    val config = new Config("dw", inputPaths, dw_processes)

    val logData = sc.textFile(config.source)
    println(logData.first())
    /*val t0 = System.currentTimeMillis()
    val j1Objs: Array[JsValue] = logData.take(100000).map(Json.parse)
    println(System.currentTimeMillis()-t0)
    val t1 = System.currentTimeMillis()
    val j2Objs = logData.take(100000).map(x => new JSONObject(x))
    println(System.currentTimeMillis()-t1)*/

    //val json: JsValue = Json.parse(logData.first())

    //value corresponding to a key can be int/string
    def awsListFromJson(paths: List[List[String]], jsonObj: JSONObject): List[String] = {
      paths.map(path => {
        val secondToLast = Try(path.init.foldLeft(jsonObj)((j, attr) => j.optJSONObject(attr)))
        secondToLast match {
          case scala.util.Success(lastObj) => {
            if (lastObj.has(path.last)) {
              Option(lastObj.optString(path.last)).filter(_.trim.nonEmpty).orElse(Option(lastObj.optInt(path.last))).get.toString
            }
            else "Unknown"
          }
          case scala.util.Failure(e) => "Unknown"
        }
      })
    }

    //int cast as strings are supported
    def awsTryKVJson(basepath: List[String], jsonObj: JSONObject, k: String, v: String): Try[List[(String, Int)]] = {
      val arrayRead = Try {
        val secondToLast = basepath.init.foldLeft(jsonObj)((j, attr) => j.optJSONObject(attr))
        secondToLast.getJSONArray(basepath.last)
      }
      arrayRead match {
        case scala.util.Success(arr) =>
          Try {
            var arrList = List[(String, Int)]()
            for (i<- 0 until arr.length){
              arrList = (arr.getJSONObject(i).optString(k, ""), Try(arr.getJSONObject(i).optString(v, "").toInt).getOrElse(arr.getJSONObject(i).optInt(v,0))) :: arrList
            }
            arrList.filter {case (a, _) => !a.isEmpty }
          }
        case scala.util.Failure(e) => {
          scala.util.Failure(new Exception("error"))}
      }
    }

    //Play 2.4 only works only Java SDK 8...commenting out the code so that others can build
    /*val a = awsListFromJson(List(List("app_id"), List("properties", "level")), new JSONObject(logData.first()))
    println(a)*/
    /*
    def tupleFromJson(paths: List[List[String]], json: JsValue): List[String] = {
      paths.map(path => {
        val JsRetList = JsPath(path.map(KeyPathNode(_)))(json)
        if (JsRetList.length>0) {
          JsRetList(0).asOpt[String].orElse(JsRetList(0).asOpt[Int]).getOrElse("Unknown").toString
        } else "Unknown"
      })
    }

    def tryKVJson(basepath: List[String], json: JsValue, k: String, v: String): Try[List[(String, Int)]] = {
      val arrayRead: Reads[JsArray] = JsPath(basepath.map(KeyPathNode(_))).read[JsArray]
      val arrayResult: JsResult[JsArray] = json.validate[JsArray](arrayRead)
      val ret = arrayResult match {
        case s: JsSuccess[JsArray] => Try {
          s.get.as[List[JsObject]].map(ele => (ele.\(k).asOpt[String].getOrElse(""), ele.\(v).asOpt[String].orElse(ele.\(v).asOpt[Int]).getOrElse(0).toString.toInt)).filter {case (a, _) => !a.isEmpty }
        }
        case e: JsError => Failure(new Exception("error"))
      }
      ret
    }*/

    val sqlContext = new SQLContext(sc)

    for (process <- config.processes) {
      val groupbyCols = process.groupbyCols
      val arrayTuple = process.arrayPath
      val addCols = process.addCols

      //StructType(StructField("date", DateType, true) ::
      val schema =  StructType(StructField("date", StringType, true) :: groupbyCols.map(path => StructField(path.last, StringType, true)) ::: addCols.map(ele => StructField(ele._1, StringType, true)) ::: List(StructField(arrayTuple._2, StringType, true), StructField(arrayTuple._3, IntegerType, true)))

      val total: RDD[((String, List[String]), Int)] = logData.filter{_.contains("transaction")
      }.flatMap(record => {
        try {
          var jsonObj = new JSONObject(record)

          if (jsonObj.optLong("ts", 0) == 0) {
            None
          }
          else {
            //val jsonObj = Json.parse(record)
            Try {
              jsonObj
            } match {
              case scala.util.Success(obj) => {
                val date = new Date((obj.getLong("ts") / (24 * 60 * 60 * 1000)) * (24 * 60 * 60 * 1000)).toString
                val tupleList = awsListFromJson(groupbyCols, obj) ::: addCols.map(_._2)
                val qtyList = awsTryKVJson(arrayTuple._1, obj, arrayTuple._2, arrayTuple._3)
                qtyList match {
                  case scala.util.Success(lines) if lines.length > 0 => lines.map(z => ((date, tupleList :+ z._1), z._2))
                  case _ => None
                }
              }
              case _ => {
                println("Invalid entry: " + record)
                None
              }
            }
          }
        }
        catch {
          case e: Exception => {
            println("Invalid entry:" + record)
            None
          }
        }
      }
      ).reduceByKey(_ + _)

      val createdRDD = total.map(p => Row.fromSeq((p._1._1 :: p._1._2) :+ p._2))
      val df = sqlContext.createDataFrame(createdRDD, schema)
      df.show()

      df.coalesce(2).write.format("org.apache.spark.sql.json").mode(SaveMode.Append).save("s3n://tangtest/test/dw_resources/"+args(1)+"/")
      /*df.coalesce(2).write
        .format("com.databricks.spark.redshift")
        .option("url", "jdbc:redshift://"+ process.clusterDB._1 +".cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/"+ process.clusterDB._2 +"?user=biadmin&password=Halfquest_2014")
        .option("dbtable", process.destTable)
        .option("tempdir", "s3n://tangtest/redshift_spark")
        .mode(saveMode)
        .save()*/
    }
  }
}
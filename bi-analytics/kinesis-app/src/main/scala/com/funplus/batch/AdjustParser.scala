package com.funplus.batch

import java.text.SimpleDateFormat
import java.util.Date

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}
import scala.compat.Platform._


/**
  * Created by jiating on 12/11/15.
  * Can use for data come from adjust callback server
  */
object AdjustParser extends Logging {
  def main(args: Array[String]): Unit = {
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: AdjustParser <job-name> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/adjust/ffs.th.prod/2015/12/15/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/results/ffs.th.prod/adjust/staging/daily/2015/12/15/)
          |    <base-scratch-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/etl/results/ffs.th.prod/adjust/staging/daily/scratch/)
          |
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val inputPath = args(1)
    val outputPath = args(2)
    val scratchPath = args(3)

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)
    val inputRDD = sc.textFile(inputPath)

    val adjustResult = inputRDD.map(record => {
      try {

        val jsonRecord = new JSONObject(record)
        val result = Some(getOutPutString(jsonRecord, outputPath))
        result.get
      } catch {
        case e: Exception => None
          ("invalid", record.toString)
      }
    }).groupByKey().map({ case (k, v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }
    }).saveAsHadoopFile(scratchPath, classOf[String], classOf[String],
      classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])


  }

  def getOutPutString(jsonInput: JSONObject, outPutPath: String): (String, String) = {

    val keyList = List(
      "@id",
      "adid",
      "android_id",
      "app_id",
      "country",
      "game",
      "idfa",
      "idfa_md5",
      "ip_address",
      "mac_sha1",
      "timestamp",
      "tracker",
      "tracker_name",
      "userid",
      "gps_adid",
      "device_name",
      "click_id",
      "os_name",
      "os_version",
      "rejection_reason"
    )

    val builder = new StringBuilder("")
    try {
      val id = DigestUtils.md5Hex(jsonInput.toString())
      jsonInput.put("@id", id)

      for (key <- keyList) {
        var value: String = null
        if (jsonInput.has(key)) {
          value = jsonInput.getString(key)
        } else {
          value = ""
        }

        if (key == "timestamp") {
          val timeStamp: Long = jsonInput.getLong(key)
          val unixDate: Date = new Date(if (timeStamp > Int.MaxValue) timeStamp else timeStamp * 1000)
          val dateFormatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
          val timeStr = dateFormatter.format(unixDate)
          builder.append(timeStr)
        } else {
          builder.append(value)
        }
        builder.append("\t")
      }
      (outPutPath + "archive", builder.toString())
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive", jsonInput.toString())
      }
    }


  }
}

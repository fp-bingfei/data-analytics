package com.funplus.batch

import java.text.SimpleDateFormat
import java.util.Date

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.util.EventParserUtils
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.compat.Platform._

/**
  * Created by funplus on 6/1/16.
  */
object AdjustIntegrateParser extends Logging {
  def main(args: Array[String]): Unit = {
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: AdjustParser <job-name> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/adjust/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/adjust/processed/)
          |    <base-scratch-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/adjust/scratch/)
          |    <app-ids> is the app id list split by comma(,)
          |                 (e.g. farm.th.prod,farm.global.prod)
          |    <date> is the date in input and output path
          |                 (e.g. 2016/06/01)
          |
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val inputBasePath = args(1)
    val outputBasePath = args(2)
    val scratchBasePath = args(3)
    val appIds = args(4)
    val dateStr = args(5)

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val appIdList = appIds.split(",")
    for (appId <- appIdList) {
      val outputPath = outputBasePath + appId + "/" + dateStr + "/";
      val scratchPath = scratchBasePath  + appId + "/";
      val inputPath = inputBasePath + appId + "/" + dateStr + "/";
      if (EventParserUtils.checkPathExists(sc.hadoopConfiguration, inputPath)) {
        val inputRDD = sc.textFile(inputPath)

        val adjustResult = inputRDD.map(record => {
          try {

            val jsonRecord = new JSONObject(record)
            val result = Some(getOutPutString(jsonRecord, outputPath))
            result.get
          } catch {
            case e: Exception => None
              ("invalid", record.toString)
          }
        }).groupByKey().map({ case (k, v) => {
          val v1 = v.mkString(EOL)
          (k, v1)
        }
        }).saveAsHadoopFile(scratchPath, classOf[String], classOf[String],
          classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])
      }
    }
  }

  def getOutPutString(jsonInput: JSONObject, outPutPath: String): (String, String) = {

    val keyList = List(
      "@id",
      "adid",
      "android_id",
      "app_id",
      "country",
      "game",
      "idfa",
      "idfa_md5",
      "ip_address",
      "mac_sha1",
      "timestamp",
      "tracker",
      "tracker_name",
      "userid",
      "gps_adid",
      "device_name",
      "click_id",
      "os_name",
      "os_version",
      "rejection_reason"
    )

    val builder = new StringBuilder("")
    try {
      val id = DigestUtils.md5Hex(jsonInput.toString())
      jsonInput.put("@id", id)

      for (key <- keyList) {
        var value: String = null
        if (jsonInput.has(key)) {
          value = jsonInput.getString(key)
        } else {
          value = ""
        }

        if (key == "timestamp") {
          val timeStamp: Long = jsonInput.getLong(key)
          val unixDate: Date = new Date(if (timeStamp > Int.MaxValue) timeStamp else timeStamp * 1000)
          val dateFormatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
          val timeStr = dateFormatter.format(unixDate)
          builder.append(timeStr)
        } else {
          builder.append(value)
        }
        builder.append("\t")
      }
      (outPutPath + "archive", builder.toString())
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive", jsonInput.toString())
      }
    }
  }
}

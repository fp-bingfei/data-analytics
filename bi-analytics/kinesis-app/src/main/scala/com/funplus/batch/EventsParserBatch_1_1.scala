package com.funplus.batch

import java.text.SimpleDateFormat

import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser_1_1
import com.funplus.util.EventParserUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by Yanyu on 12/16/15.
 */

object EventsParserBatch_1_1 extends Logging {

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: KinesisEventsParserRerun <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/input/kinesis/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/kinesis/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4)
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    val inputRDD = sc.textFile(inputPaths)

    //Broadcast date to generate file name
    val broadcastDate = sc.broadcast(date)

    val result = inputRDD.flatMap(record => {
      try {
        Some(EventParser_1_1.parse(record))
      } catch {
        case e: Exception => None
      }
    }).map(
        outputString => {
          val error = outputString.contains("Invalid JSON")
          var output = None: Option[(String, String)]
          if(error) {
            output = Some(("invalid/archive", outputString))
          } else {
            val outputArray = outputString.split("\t")
            val timeStr = outputArray(2)
            val dateFormatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val timeStamp = dateFormatter.parse(timeStr).getTime()
            output = Some(EventParserUtils.getOutPutString(outputArray, baseOutputPath , outputArray(1), timeStamp, broadcastDate.value))
          }
//          logWarning("Process(%s) --> data".format(output.toString))
          output.get
        })
      .groupByKey().map({case (k,v) => {
//      println("Process(%s) --> v".format(v.toString))
       val v1 = v.mkString(EOL)
//      println("Process(%s) --> k".format(k.toString))
//      println("Process(%s) --> v1".format(v1.toString))
       (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath, classOf[String], classOf[String],
      classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])
    sc.stop()

  }

  private def generateInputPaths(date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/").concat("*")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          inputPathBuffer += baseInputPath.concat(appId).concat(datePath)
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }


}

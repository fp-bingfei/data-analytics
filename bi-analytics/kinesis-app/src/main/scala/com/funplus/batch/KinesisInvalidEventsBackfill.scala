package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.util.{CountryCodeLookupService, Utilities, EventParserUtils}
import com.typesafe.config._
import java.io.InputStreamReader
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.{Logging, SparkConf, SparkContext}
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, Days}
import collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer

import scala.compat.Platform._

/**
 * Created by chunzeng on 8/3/15.
 */
object KinesisInvalidEventsBackfill extends Logging {

  private val INVALID_EVENT: String = "invalid"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 8) {
      System.err.println(
        """
          |Usage: KinesisInvalidEventsBackfill <job-name> <base-input-path> <base-output-path> <scratch-output-path> \
          |                                    <start-date> <end-date> <app_ids> <update-conf>
          |
          |    <job-name> is the name of the job
          |                 (e.g. invalid_backfill)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplus.data-diandian/kinesis/diandian_core/game=poker/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/events/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/scratch/)
          |    <start-date> is the start date of processing
          |                 (e.g 2015-12-23)
          |    <end-date> is the end date of processing, including the end date
          |                 (e.g 2016-04-13)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. poker.ar.prod)
          |    <update-conf> is the config file path for JSON updating
          |                 (e.g. update.conf)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(jobName, baseInputPath, baseOutputPath, scratchOutputPath, startDate, endDate, appIds, updateConf) = args
    // Init config
    val filePath = new Path(updateConf)
    val fileSystem = filePath.getFileSystem(new Configuration())
    val fileReader = new InputStreamReader(fileSystem.open(filePath))
    val conf = ConfigFactory.parseReader(fileReader)
    fileReader.close()

    // Setup the SparkConfig and StreamingContext
    val sparkConfig = new SparkConf().setAppName(jobName)
    val ssc = new SparkContext(sparkConfig)

    val inputPaths = generateInputPaths(ssc.hadoopConfiguration, startDate, endDate, baseInputPath, appIds)
    // Check input paths
    if (inputPaths.length == 0) {
      return
    }
    val inputRDD = ssc.textFile(inputPaths)
    inputRDD.map(record => {
      val jsonObject = new JSONObject(record)
      // Check event field
      val event = jsonObject.getString(EVENT)
      var output = None: Option[(String, String)]
      if (! event.equals(INVALID_EVENT)) {
        val jsonString = jsonObject.getString(INVALID_JSON)
        // Generate output Json
        val outputJson = new JSONObject(jsonString)
        Utilities.includeDefaultPrettyDateInJson("ts", outputJson)
        if (outputJson.has("properties")) {
          val properties = outputJson.getJSONObject("properties")
          CountryCodeLookupService.includeCountryCodeInJson(properties)
          Utilities.includeDefaultPrettyDateInJson("install_ts", properties)
        }
        // Update fields
        updateJsonFields(outputJson, conf)
        output = Some(EventParserUtils.getOutputJson(outputJson.toString(), outputJson, baseOutputPath, event, true))
      }
      output.get
    }).groupByKey().map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath, classOf[String], classOf[String],
      classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])

    ssc.stop()
  }

  /**
   * Update JSON fields
   */
  def updateJsonFields(jsonObject: JSONObject, conf: Config): Unit = {
    // Loop config
    val fieldList = conf.root.toList
    fieldList.foreach(field => {
      val fieldName = field._1
      val fieldValue = field._2
      fieldName match {
        case "properties" => {
          // Process recursively
          updateJsonFields(jsonObject.getJSONObject("properties"), conf.getObject(fieldName).toConfig)
        }
        case _ => {
          fieldValue.valueType() match {
            case ConfigValueType.STRING => {
              // update value
              jsonObject.put(fieldName, conf.getString(fieldName))
            }
            case ConfigValueType.OBJECT => {
              // key & value
              val keyPath = fieldName.concat(".key")
              val valuePath = fieldName.concat(".value")
              var key: String = fieldName
              var value: String = ""
              if (jsonObject.has(fieldName)) {
                value = jsonObject.getString(fieldName)
              }
              if (conf.hasPath(keyPath)) {
                key = conf.getString(keyPath)
                // remove old key
                if (jsonObject.has(fieldName)) {
                  jsonObject.remove(fieldName)
                }
              }
              if (conf.hasPath(valuePath)) {
                value = conf.getString(valuePath)
              }
              // Add new key and value
              jsonObject.put(key, value)
            }
            case _ => {
              // do nothing
            }
          }
        }
      }
    })
  }

  private def generateInputPaths(conf: Configuration, startDate:String, endDate:String, baseInputPath:String,
                                 appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val inputPathBuffer = new ArrayBuffer[String]()
      val format = DateTimeFormat.forPattern("yyyy-MM-dd")
      val startTime = DateTime.parse(startDate, format)
      val endTime = DateTime.parse(endDate, format)
      // Including the end date
      val daysCount = Days.daysBetween(startTime, endTime).getDays() + 1
      (0 until daysCount).map(startTime.plusDays(_)).foreach {
        date => {
          val dateObj = format.print(date).split("-")
          val datePath = "/".concat("year=").concat(dateObj(0)).concat("/")
            .concat("month=").concat(dateObj(1)).concat("/")
            .concat("day=").concat(dateObj(2)).concat("/")
          appIdArray.foreach {
            appId => {
              val inputPath = baseInputPath.concat("event=invalid").concat("/")
                .concat("app=").concat(appId).concat(datePath)
              if (EventParserUtils.checkPathExists(conf, inputPath)) {
                inputPathBuffer += inputPath.concat("*")
              }
            }
          }
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }
}

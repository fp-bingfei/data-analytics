package com.funplus.batch

import java.text.SimpleDateFormat
import java.util.Date

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.util.EventParserUtils
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}
import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
  * Created by funplus on 6/1/16.
  */
object AdjustParseAll extends Logging {
  def main(args: Array[String]): Unit = {
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: AdjustParser <job-name> <base-input-path> <base-output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/adjust/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/adjust/processed/)
          |    <base-scratch-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/adjust/scratch/)
          |    <date> is the date in input and output path
          |                 (e.g. 2016/06/01)
          |
        """.stripMargin)
      System.exit(1)
    }
    val jobName = args(0)
    val inputBasePath = args(1)
    val outputBasePath = args(2)
    val scratchBasePath = args(3)
    val dateStr = args(4)

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)


    val inputBasePathWithDateStr = inputBasePath + dateStr + "/"
    val scratchPath = scratchBasePath + dateStr + "/"

    val inputPaths = ArrayBuffer[String]()

    for (i <- 0 to 24){
      val inputPath = inputBasePathWithDateStr + "%02d".format(i)
      if (EventParserUtils.checkPathExists(sc.hadoopConfiguration, inputPath)){
        inputPaths += inputPath
      }
    }

    if (inputPaths.size > 0) {
      val inputRDD = sc.textFile(inputPaths.mkString(","))

      val appidValues = inputRDD.map(record => {
        try {

          val jsonRecord = new JSONObject(record)
          val result = Some(getOutPutString(jsonRecord))
          result.get
        } catch {
          case e: Exception => None
            ("invalid", record.toString)
        }
      })
        .groupByKey().map({
        case(appId, v) => {
          val v1 = v.mkString(EOL)
          val outputPath = outputBasePath + appId + "/" + dateStr + "/archive"
          (outputPath, v1)
        }
      }).saveAsHadoopFile(scratchPath, classOf[String], classOf[String],
        classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])
    }
  }

  def getOutPutString(jsonInput: JSONObject): (String, String) = {

    val keyList = List(
      "adid",
      "event",
      "app_id",
      "app_version",
      "device_name",
      "os_name",
      "os_version",
      "idfa",
      "idfv",
      "gaid",
      "android_id",
      "ip_address",
      "country",
      "city",
      "lang",
      "tracker",
      "tracker_name",
      "rejection_reason",
      "click_id",
      "click_time",
      "engagement_time",
      "installed_at",
      "created_at",
      "received_at",
      "timezone",
      "time_spent",
      "game",
      "user_id"
    )

    val builder = new StringBuilder("")
    try {
      for (key <- keyList) {
        var value: String = null
        if (jsonInput.has(key)) {
          // replace all the \t and \n avoid breaking the lines or the db table when copying to.
          value = jsonInput.getString(key).replaceAll("[\t\n]", "")
        } else {
          value = ""
        }

        builder.append(value)
        builder.append("\t")
      }
      val appId = jsonInput.getString("game")
      val adid = jsonInput.getString("adid")
      val game = jsonInput.getString("game")
      val tracker = jsonInput.getString("tracker")
      val trackerName = jsonInput.getString("tracker_name")
      if (adid.isEmpty || appId.isEmpty || game.isEmpty || tracker.isEmpty || trackerName.isEmpty){
        ("invalid", jsonInput.toString())
      } else{
        (appId, builder.toString())
      }

    } catch {
      case e: Exception => {
        // exception
        ("invalid", jsonInput.toString())
      }
    }
  }
}

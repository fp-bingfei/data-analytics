package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser
import com.funplus.util.{Utilities, EventParserUtils}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.{Logging, SparkConf, SparkContext}
import org.apache.hadoop.mapred.TextOutputFormat

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by yangzhenxuan on 11/17/15.
 */
object EventsCounterLegacy extends Logging {

  private val INVALID_EVENT: String = "invalid"
  private val ERROR: String = "error"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: CustomEventsParser <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/logserver/custom/{app_id}/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4)
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    //val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    //val inputRDD = sc.textFile(inputPaths)
    val inputPaths = generateInputPaths(sc.hadoopConfiguration, date, baseInputPath, appIds)
    if (inputPaths.length == 0) {
      return
    }
    val inputRDD = sc.textFile(inputPaths)

    val tmpResult = inputRDD.map(record => {
      try{
        var output = None: Option[(String, Int)]
        val jsonObject = new JSONObject(record)
        output = Some(getOutput(record, jsonObject, baseOutputPath, jsonObject.getString(EVENT), false))
        output.get
      } catch {
        case e: Exception => {
          ("invalid/archive", 1)
        }
      }
    }).reduceByKey(_ + _)
      .repartition(1).
      saveAsHadoopFile(scratchOutputPath + "/" + date + "/counters", classOf[Text], classOf[IntWritable],
        classOf[TextOutputFormat[Text, IntWritable]])

    sc.stop()
  }


  def getOutput(jsonOutput: String, jsonObject: JSONObject, basePath: String,
                    event: String, appPartition: Boolean = false): (String, Int) = {
    try {
      var biVersion: String = null
      if (jsonObject.has("data_version")) {
        // 2.0: data_version
        biVersion = jsonObject.getString("data_version")
      } else if (jsonObject.has("bi_version")) {
        // 1.2
        biVersion = jsonObject.getString("bi_version")
      } else {
        //biVersion is not available then default it to 1.1
        biVersion = "1.1"
      }

      var app_id = None: Option[String]
      var ts = None: Option[Long]
      // app and ts varies for biVersion 1.1
      if ("1.1".equals(biVersion)) {
        app_id = Option(jsonObject.getString("@key"))
        ts = Option(jsonObject.getLong("@ts"))
      }else {
        // use ts
        app_id = Option(jsonObject.getString("app_id"))
        ts = Option(jsonObject.getLong("ts"))
      }
      val outputPath = EventParserUtils.getOutputPath(basePath, event, app_id.get, ts.get, appPartition)
      (outputPath, 1)
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive", 1)
      }
    }
  }

  private def generateInputPaths(conf: Configuration, date:String, baseInputPath:String,
                                 appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          val inputPath = baseInputPath.concat(appId).concat(datePath)
          if (EventParserUtils.checkPathExists(conf, inputPath)) {
            inputPathBuffer += inputPath.concat("*")
          }
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }
}

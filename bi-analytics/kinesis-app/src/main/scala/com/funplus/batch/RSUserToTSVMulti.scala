package com.funplus.batch

import com.amazonaws.util.json.JSONObject
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer


/**
  * Created by Yanyu on 01/04/16.
  */
object RSUserToTSVMulti extends Logging {
  private val LOCALES = Array("ae", "de", "fr", "nl", "th", "us")
  private val DIRPATH0 = "s3://com.funplusgame.bidata/etl/rs/"
  private val DIRPATH1 = "/mongodb/user/"
  private val LOCALEINDEX = 5
  private val NULLSTRING = "NULL"

  def main(args: Array[String]):Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 3) {
      System.err.println(
        """
          |Usage: KinesisEventsParserRerun <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <output-path> is the output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/kinesis/)
        """.stripMargin)
      System.exit(1)
    }
    val jobName = args(0)
    val date = args(1)
    val baseoutPutPath = args(2)
    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val inputPath = generateInputPath(date)
    for (inputpath <- inputPath)
      {
        val app ="royal." + inputpath.split("/")(LOCALEINDEX) + ".prod"
        val inputRDD = sc.textFile(inputpath)
        val outPutPath = baseoutPutPath + date.replaceAll("-","") + '/' + app + '/'
        val tblUserResult = inputRDD.map(record => {
          try{
            val jsonRecord = new JSONObject(record)
            getOutPutJson(jsonRecord, app)
          }
          catch {
            case e: Exception => None
          }
        }).saveAsTextFile(outPutPath, classOf[GzipCodec])
      }
    sc.stop()
  }


  private def getOutPutJson(jsonInPut: JSONObject, app: String): String = {
    val outputFields = Array(
      "app",
      "guid",
      "uid",
      "ref",
      "create_time",
      "lang",
      "cash",
      "coins",
      "email",
      "gender",
      "first_name",
      "birthday",
      "friends_total_count")
    val outputJson = new JSONObject()
    for (key <- outputFields) {
      if (key == "app") {
        outputJson.put(key, app)
      }else {
        var value: String = null
        try {
          val numberLongJson = jsonInPut.getJSONObject(key)
          value = numberLongJson.getString("$numberLong")
        }
        catch {
          case e: Exception => {
            try {
              value = jsonInPut.getString(key)
            }
            catch {
              case e: Exception => {
                println("missed" + key)
                value = NULLSTRING
              }
            }
          }
        }
        outputJson.put(key, value)
      }
    }
    outputJson.toString
  }

  private def generateInputPath(date:String): Array[String] = {
    val datePath = date.replaceAll("-","").concat("/").concat("*")
    val inputPathBuffer = new ArrayBuffer[String](LOCALEINDEX)
    LOCALES.foreach(locale => (inputPathBuffer += DIRPATH0.concat(locale).concat(DIRPATH1).concat(datePath)))
    val inPutPath = inputPathBuffer.toArray
    println(inPutPath)
    inPutPath

  }

}

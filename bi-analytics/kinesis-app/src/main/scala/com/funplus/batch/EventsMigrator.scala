package com.funplus.batch

import java.io.InputStreamReader

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.util.EventParserUtils
import com.typesafe.config._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io._
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.JavaConversions._
import scala.compat.Platform._

/**
 * Created by chunzeng on 8/10/15.
 */
object EventsMigrator extends Logging {

  private val EVENT: String = "event"

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: EventsMigrator <job-name> <base-input-path> <base-output-path> <scratch-output-path> \
          |                      <migrate-conf>
          |
          |    <job-name> is the name of the job
          |                 (e.g. events_migration)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/input/*)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/events/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/scratch/)
          |    <migrate-conf> is the config file path for JSON migrating
          |                 (e.g. migrate.conf)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(jobName, baseInputPath, baseOutputPath, scratchOutputPath, migrateConf) = args
    // Init config
    val filePath = new Path(migrateConf)
    val fileSystem = filePath.getFileSystem(new Configuration())
    val fileReader = new InputStreamReader(fileSystem.open(filePath))
    val conf = ConfigFactory.parseReader(fileReader)
    fileReader.close()

    // Setup the SparkConfig and StreamingContext
    val sparkConfig = new SparkConf().setAppName(jobName)
    val ssc = new SparkContext(sparkConfig)
    val inputRDD = ssc.sequenceFile(baseInputPath, classOf[NullWritable], classOf[Text])
    inputRDD.map(record => {
      val jsonObject = new JSONObject(record._2.toString)
      // Get event name
      val event = jsonObject.getString(EVENT)
      // Migrate fields
      migrateJsonFields(jsonObject, conf)
      // Output migrated events
      EventParserUtils.getOutputJson(jsonObject.toString(), jsonObject, baseOutputPath, event, true)
    }).groupByKey().map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath, classOf[String], classOf[String],
      classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])

    ssc.stop()
  }

  /**
   * Migrate JSON fields
   */
  def migrateJsonFields(jsonObject: JSONObject, conf: Config): Unit = {
    // Loop config
    val fieldList = conf.root.toList
    fieldList.foreach(field => {
      val fieldName = field._1
      val fieldValue = field._2
      fieldName match {
        case "properties" => {
          // Process recursively
          migrateJsonFields(jsonObject.getJSONObject("properties"), conf.getObject(fieldName).toConfig)
        }
        case _ => {
          fieldValue.valueType() match {
            case ConfigValueType.STRING => {
              // update value
              jsonObject.put(fieldName, conf.getString(fieldName))
            }
            case ConfigValueType.OBJECT => {
              // type, key & value
              val keyPath = fieldName.concat(".key")
              val valuePath = fieldName.concat(".value")
              val typePath = fieldName.concat(".type")
              if (conf.hasPath(typePath)) {
                // update type
                if (jsonObject.has(fieldName)) {
                  // Only support String now
                  val value = jsonObject.getString(fieldName)
                  jsonObject.put(fieldName, value)
                }
              } else {
                // update key and value
                var key: String = fieldName
                var value: String = ""
                if (jsonObject.has(fieldName)) {
                  value = jsonObject.getString(fieldName)
                }
                if (conf.hasPath(keyPath)) {
                  key = conf.getString(keyPath)
                  // remove old key
                  if (jsonObject.has(fieldName)) {
                    jsonObject.remove(fieldName)
                  }
                }
                if (conf.hasPath(valuePath)) {
                  value = conf.getString(valuePath)
                }
                // Add new key and value
                jsonObject.put(key, value)
              }
            }
            case _ => {
              // do nothing
            }
          }
        }
      }
    })
  }
}

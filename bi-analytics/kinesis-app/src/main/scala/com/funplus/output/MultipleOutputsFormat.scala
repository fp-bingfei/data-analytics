package com.funplus.output

/**
 * Created by zengchun on 1/14/16.
 *
 * Refer to https://gist.github.com/silasdavis/d1d1f1f7ab78249af462
 */
import com.funplus.output.HadoopIO.MultipleOutputer
import com.funplus.output.HadoopIO.MultipleOutputer._
import org.apache.avro.generic.{GenericDatumReader, GenericData, GenericContainer, GenericRecord}
import org.apache.avro.io.DecoderFactory
import org.apache.avro.mapreduce.AvroJob
import org.apache.avro.Schema
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.io.{Text, DataInputBuffer, NullWritable}
import org.apache.hadoop.mapred.RawKeyValueIterator
import org.apache.hadoop.mapreduce.counters.GenericCounter
import org.apache.hadoop.mapreduce.lib.output.{SequenceFileOutputFormat, TextOutputFormat, LazyOutputFormat, MultipleOutputs}
import org.apache.hadoop.mapreduce.task.ReduceContextImpl
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl.DummyReporter
import org.apache.hadoop.mapreduce.{Job, _}
import org.apache.hadoop.util.Progress
import org.apache.spark.rdd.RDD
import org.apache.parquet.avro.{AvroParquetOutputFormat, AvroWriteSupport}
import org.apache.parquet.hadoop.ParquetOutputFormat
import org.apache.parquet.hadoop.metadata.CompressionCodecName
import scala.compat.Platform._
import scala.reflect.{ClassTag, _}

object HadoopIO {

  /**
   * it allows for extending the functionality of MultipleOutputsFormat for other multiple output writers by
   * providing this missing interface as a trait.
   */
  trait MultipleOutputer[K, V] {
    def write(key: K, value: V, path: String): Unit
    def close(): Unit
  }

  object MultipleOutputer {

    implicit class PlainMultipleOutputer[K, V](mo: MultipleOutputs[K, V]) extends MultipleOutputer[K, V] {
      def write(key: K, value: V, path: String): Unit = mo.write(key, value, path)
      def close(): Unit = mo.close()
    }

  }

}

object MultipleOutputsFormat {
  // Type inference fails with this inlined in constructor parameters
  private def defaultMultipleOutputsMaker[K, V](io: TaskInputOutputContext[_, _, K, V]): MultipleOutputer[K, V] =
    new MultipleOutputs[K, V](io)
}

/**
 * Subclass this to create a multiple output creating OutputFormat. The subclass must have a nullary constructor so
 * hadoop can construct it with `.newInstance`, ugh...
 *
 * The output format expects a two-part key of the form (outputPath, actualKey), the string outputPath will be used to
 * partition the output into different directories ('/' separated filenames).
 *
 * For some reason MultipleOutputs does not work with Avro, but the near-identical AvroMultipleOutputs does. Irritatingly
 * these obviously related classes have no common ancestor so they are combined under the MultipleOutputer type class
 * which at least allows for future extension.
 *
 * @param outputFormat the underlying OutputFormat responsible for writing to varies
 * @param multipleOutputsMaker factory method for constructing an object implementing the MultiplerOutputer trait
 * @tparam K key type of the underlying OutputFormat
 * @tparam V value type of the underlying OutputFormat
 */
abstract class MultipleOutputsFormat[K, V]
(outputFormat: OutputFormat[K, V],
 multipleOutputsMaker: TaskInputOutputContext[_, _, K, V] => MultipleOutputer[K, V] =
 (r: TaskInputOutputContext[_, _, K, V]) => MultipleOutputsFormat.defaultMultipleOutputsMaker[K, V](r))
  extends OutputFormat[(String, K), V] {

  override def checkOutputSpecs(context: JobContext): Unit = outputFormat.checkOutputSpecs(context)

  override def getOutputCommitter(context: TaskAttemptContext): OutputCommitter = outputFormat
    .getOutputCommitter(context)

  override def getRecordWriter(context: TaskAttemptContext): RecordWriter[(String, K), V] =
    new RecordWriter[(String, K), V] {
      val job = Job.getInstance(context.getConfiguration)
      // Set underlying output format using lazy output
      LazyOutputFormat.setOutputFormatClass(job, outputFormat.getClass)
      // We pass a ReduceContext with most fields dummied-out since they will not be used in the context
      // of Spark's saveAs*Hadoop* methods
      val ioContext = new ReduceContextImpl(job.getConfiguration, context.getTaskAttemptID,
        new DummyIterator, new GenericCounter, new GenericCounter,
        new DummyRecordWriter, new DummyOutputCommitter, new DummyReporter, null,
        classOf[NullWritable], classOf[NullWritable])
      val multipleOutputs: MultipleOutputer[K, V] = multipleOutputsMaker(ioContext)

      override def write(keys: (String, K), value: V): Unit = {
        keys match {
          case (path, key) => {
            if (value.isInstanceOf[Text]) {
              val fileType = job.getConfiguration.get("type")
              var schema = None : Option[Schema]
              if (fileType.equals("ParquetFile")) {
                // Read avro schema
                val schemaName = job.getConfiguration.get("schema")
                val stream = getClass().getClassLoader.getResourceAsStream(schemaName)
                schema = Some(new Schema.Parser().parse(stream))
              }
              val text = value.asInstanceOf[Text]
              var start = 0
              while(start < text.getLength) {
                var end = text.find(EOL, start)
                if (end < 0) {
                  end = text.getLength
                }
                // Get the current line
                val v = Text.decode(text.getBytes(), start, end - start)
                if (fileType.equals("TextFile") || fileType.equals("SequenceFile")) {
                  // Write the string
                  multipleOutputs.write(key, new Text(v).asInstanceOf[V], path)
                } else if (fileType.equals("ParquetFile") && !schema.isEmpty) {
                  // Decode the record
                  val decoder = DecoderFactory.get().jsonDecoder(schema.get, v)
                  val reader = new GenericDatumReader[GenericRecord](schema.get)
                  val datum = reader.read(null, decoder)
                  // Write the record
                  multipleOutputs.write(key, datum.asInstanceOf[V], path)
                }
                // Update the start index
                start = end + 1
              }
            } else {
              multipleOutputs.write(key, value, path)
            }
          }
        }
      }

      override def close(context: TaskAttemptContext): Unit = multipleOutputs.close()
    }


  private class DummyOutputCommitter extends OutputCommitter {
    override def setupJob(jobContext: JobContext): Unit = ()
    override def needsTaskCommit(taskContext: TaskAttemptContext): Boolean = false
    override def setupTask(taskContext: TaskAttemptContext): Unit = ()
    override def commitTask(taskContext: TaskAttemptContext): Unit = ()
    override def abortTask(taskContext: TaskAttemptContext): Unit = ()
  }

  private class DummyRecordWriter extends RecordWriter[K, V] {
    override def write(key: K, value: V): Unit = ()
    override def close(context: TaskAttemptContext): Unit = ()
  }

  private class DummyIterator extends RawKeyValueIterator {
    override def getKey: DataInputBuffer = null
    override def getValue: DataInputBuffer = null
    override def getProgress: Progress = null
    override def close(): Unit = ()
    override def next: Boolean = true
  }

}

// For Text File
class MultipleTextFileOutputsFormat
  extends MultipleOutputsFormat(new TextOutputFormat[NullWritable, Text]) {
}

// For Sequence File
class MultipleSequenceFileOutputsFormat
  extends MultipleOutputsFormat(new SequenceFileOutputFormat[NullWritable, Text]) {
}

// For Parquet File
class MultipleParquetFileOutputsFormat[T <: GenericContainer]
  extends MultipleOutputsFormat(new ParquetOutputFormat[T]) {
}

object SerializationExtensions {

  def avroParquetJob[T: ClassTag](schemaName: String, job: Job = Job.getInstance(new Configuration())): Job = {
    //val schema: Schema = SpecificData.get.getSchema(classTag[T].runtimeClass)
    // Read avro schema
    val stream = getClass().getClassLoader.getResourceAsStream(schemaName)
    val schema = new Schema.Parser().parse(stream)
    AvroJob.setInputKeySchema(job, schema)
    AvroJob.setOutputKeySchema(job, schema)
    ParquetOutputFormat.setWriteSupportClass(job, classOf[AvroWriteSupport])
    ParquetOutputFormat.setCompression(job, CompressionCodecName.SNAPPY)
    ParquetOutputFormat.setEnableDictionary(job, true)
    ParquetOutputFormat.setPageSize(job, 1048576)
    ParquetOutputFormat.setBlockSize(job, 134217728)
    AvroParquetOutputFormat.setSchema(job, schema)
    job.getConfiguration.set("parquet.enable.summary-metadata", "false")
    job.getConfiguration.set("type", "ParquetFile")
    job.getConfiguration.set("schema", schemaName)
    job
  }

  implicit class TextFileRDD[K, V](rdd: RDD[(K, V)]) {
    def saveAsMultipleTextFiles(outputKeyFunction: (K) => String, outputPath: String): Unit = {
      rdd.map { case (k, v) => ((outputKeyFunction(k), NullWritable.get), v) }.map {
        case (k, v: String) => (k, new Text(v))
        case (k, v: Array[Byte]) => (k, new Text(v))
        case (k, v: Text) => (k, new Text(v))
      }.saveAsNewAPIHadoopFile(outputPath, classOf[NullWritable], classOf[Text],
          classOf[MultipleTextFileOutputsFormat],
          {
            val conf = new Configuration()
            conf.set("mapreduce.output.fileoutputformat.compress", "true")
            conf.set("mapreduce.output.fileoutputformat.compress.codec", "org.apache.hadoop.io.compress.GzipCodec")
            conf.set("type", "TextFile")
            conf
          })
    }
  }

  implicit class SequenceFileRDD[K, V](rdd: RDD[(K, V)]) {
    def saveAsMultipleSequenceFiles(outputKeyFunction: (K) => String, outputPath: String): Unit = {
      rdd.map { case (k, v) => ((outputKeyFunction(k), NullWritable.get), v) }.map {
        case (k, v: String) => (k, new Text(v))
        case (k, v: Array[Byte]) => (k, new Text(v))
        case (k, v: Text) => (k, new Text(v))
      }.saveAsNewAPIHadoopFile(outputPath, classOf[NullWritable], classOf[Text],
          classOf[MultipleSequenceFileOutputsFormat],
          {
            val conf = new Configuration()
            conf.set("mapreduce.output.fileoutputformat.compress", "true")
            conf.set("mapreduce.output.fileoutputformat.compress.codec", "org.apache.hadoop.io.compress.GzipCodec")
            conf.set("mapreduce.output.fileoutputformat.compress.type", "BLOCK")
            conf.set("type", "SequenceFile")
            conf
          })
    }
  }

  implicit class ParquetFileRDD[K, V](val rdd: RDD[(K, V)]) {
    def saveAsMultipleParquetFiles(outputKeyFunction: (K) => String, outputPath: String, schemaName: String): Unit = {
      rdd.map { case (k, v) => ((outputKeyFunction(k), null), v) }.map {
        case (k, v: String) => (k, new Text(v))
        case (k, v: Array[Byte]) => (k, new Text(v))
        case (k, v: Text) => (k, new Text(v))
      }.saveAsNewAPIHadoopFile(outputPath,
          classOf[Null],
          classOf[GenericData.Record],
          classOf[MultipleParquetFileOutputsFormat[GenericData.Record]],
          avroParquetJob[GenericData.Record](schemaName).getConfiguration)
    }
  }
}
package com.funplus.stream

import com.funplus.parser.EventParser
import org.apache.hadoop.io.NullWritable
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext._
import org.apache.spark.{Logging, SparkConf}
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.Milliseconds
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.StreamingContext.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisUtils
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.util.json.JSONObject
import scala.compat.Platform.EOL
import org.elasticsearch.spark._

/**
 * Created by chunzeng on 3/11/15.
 */
object KinesisEventConsumer extends Logging {
  def main(args: Array[String]) {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: KinesisEventConsumer <stream-name> <endpoint-url> <base-path> <es-nodes> <es-port>
          |    <stream-name> is the name of the Kinesis stream
          |                  (e.g. bv-global)
          |    <endpoint-url> is the endpoint of the Kinesis service
          |                   (e.g. https://kinesis.us-west-2.amazonaws.com)
          |    <base-path> is the base output s3 path
          |                (e.g. s3n://com.funplusgame.bidata/dev/test/parser/%s/)
          |    <es-nodes> is the list of Elasticsearch nodes to connect to
          |              (e.g. localhost)
          |    <es-port> is the HTTP/REST port for connecting to Elasticsearch
          |              (e.g. 9200)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(streamName, endpointUrl, basePath, esHost, esPort) = args

    /* Determine the number of shards from the stream */
    val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
    kinesisClient.setEndpoint(endpointUrl)
    val numShards = kinesisClient.describeStream(streamName).getStreamDescription().getShards()
      .size()

    /* In this example, we're going to create 1 Kinesis Worker/Receiver/DStream for each shard. */
    val numStreams = numShards

    /* Setup the and SparkConfig and StreamingContext */
    /* Spark Streaming batch interval */
    val batchInterval = Milliseconds(2000)
    val sparkConfig = new SparkConf().setAppName(streamName + "Consumer")
    // Setting for ElasticSearch
    sparkConfig.set("es.index.auto.create", "true")
    sparkConfig.set("es.resource", "events/invalid")
    sparkConfig.set("es.nodes", esHost)
    sparkConfig.set("es.port", esPort)
    // Initialize streaming context
    val ssc = new StreamingContext(sparkConfig, batchInterval)

    /* Kinesis checkpoint interval.  Same as batchInterval for this example. */
    val kinesisCheckpointInterval = batchInterval

    /* Create the same number of Kinesis DStreams/Receivers as Kinesis stream's shards */
    val kinesisStreams = (0 until numStreams).map { i =>
      KinesisUtils.createStream(ssc, streamName, endpointUrl, kinesisCheckpointInterval,
        InitialPositionInStream.LATEST, StorageLevel.MEMORY_AND_DISK_2)
    }

    /* Union all the streams */
    val unionStreams = ssc.union(kinesisStreams)

    /* Validate each record */
    try {
      // Split lines
      val lines = unionStreams.flatMap(byteArray => new String(byteArray).split(EOL))
      // MapReduce processing
      val results = lines.map(record => {
        val jsonOutput = EventParser.parse(record)
        val jsonObject = new JSONObject(jsonOutput)
        (jsonObject.get("event").toString, jsonOutput :: Nil)
      }).reduceByKey(_ ::: _)
      // Save in the s3 path
      results.foreachRDD(rdd => {
        if (rdd.count() > 0) {
          logDebug("RDD count (%d)".format(rdd.count()))
          // Collect events
          val events = rdd.collect()
          events.foreach(event => {
            val eventName = event._1
            val eventRDD = ssc.sparkContext.makeRDD(event._2)
            val outputPath = basePath.format(eventName)
            //eventRDD.saveAsTextFile(outputPath, classOf[GzipCodec])
            eventRDD.map(event => (NullWritable.get(), event))
              .saveAsSequenceFile(outputPath, Some(classOf[GzipCodec]))

            // Index invalid events in ElasticSearch
            indexInvalidEvents(eventRDD)
          })
        }
      })
    } catch {
      case e: Exception => {
        logError("Error message (%s)".format(e.getMessage))
      }
    }

    /* Start the streaming context and await termination */
    ssc.start()
    ssc.awaitTermination()
  }

  /**
   * Index invalid events in ElasticSearch
   */
  def indexInvalidEvents(events: RDD[String]) = {
    try {
      events.filter(event => {
        val jsonObject = new JSONObject(event)
        jsonObject.get("invalid").equals("1")
      }).saveJsonToEs("events/invalid")
    } catch {
      case e: Exception => {
        logError("ElasticSearch Error message (%s)".format(e.getMessage))
      }
    }
  }
}

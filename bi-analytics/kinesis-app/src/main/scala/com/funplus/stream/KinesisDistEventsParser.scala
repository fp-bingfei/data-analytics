package com.funplus.stream

import collection.JavaConversions._
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser
import com.funplus.util.EventParserUtils
import com.funplus.validator.JsonError
import com.typesafe.config._
import java.io.InputStreamReader
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.StreamingContext.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisUtils
import org.apache.spark.streaming.{Duration, Milliseconds, StreamingContext}
import org.apache.spark.{Logging, SparkConf}
import scala.compat.Platform.EOL

/**
 * Created by chunzeng on 7/21/15.
 *
 * Consume distributed Kinesis streams
 */
object KinesisDistEventsParser extends Logging {

  private val INVALID_EVENT = "invalid"
  private val ERROR: String = "error"

  def main(args: Array[String]) {
    /* Check that all required args were passed in. */
    if (args.length < 1) {
      System.err.println(
        """
          |Usage: KinesisDistEventsParser <stream-conf>
          |
          |       <stream-conf> is the config file path (e.g. spark.conf)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(streamConf) = args
    // Init config
    val filePath = new Path(streamConf)
    val fileSystem = filePath.getFileSystem(new Configuration())
    val fileReader = new InputStreamReader(fileSystem.open(filePath))
    val conf = ConfigFactory.parseReader(fileReader)
    fileReader.close()
    // Get Settings
    val basePath = conf.getString("basePath")
    val checkpointPath = conf.getString("checkpointPath")
    val interval = conf.getString("batchInterval")
    val appName = conf.getString("appName")

    /* Spark Streaming batch interval */
    val batchInterval = Milliseconds(interval.toInt * 1000)

    // Get or create Spark Streaming Context
    val ssc = StreamingContext.getOrCreate(checkpointPath,
      () => {
        createContext(conf, basePath, checkpointPath, batchInterval, appName)
      })

    // Add shutdown hook to stop gracefully
    // Set spark.streaming.stopGracefullyOnShutdown to true instead
//    sys.ShutdownHookThread {
//      logDebug("Gracefully stopping Spark Streaming Application")
//      ssc.stop(true, true)
//      logDebug("Application stopped")
//    }

    /* Start the streaming context and await termination */
    ssc.start()
    ssc.awaitTermination()
  }

  /**
   * Function to create and setup a new StreamingContext
   */
  def createContext(conf: Config, basePath: String, checkpointPath: String,
                    batchInterval: Duration, appName: String): StreamingContext = {
    /* Setup the SparkConfig and StreamingContext */
    val sparkConfig = new SparkConf().setAppName(appName)
    // Initialize streaming context
    val ssc = new StreamingContext(sparkConfig, batchInterval)
    // Set s3 path for Spark metadata checkpoint and Write Ahead Log
    // Set spark.streaming.receiver.writeAheadLog.enable to true
    ssc.checkpoint(checkpointPath)

    /* Kinesis checkpoint interval */
    val kinesisCheckpointInterval = batchInterval

    // Loop Kinesis streams
    var kinesisStreams = IndexedSeq[ReceiverInputDStream[Array[Byte]]]()
    val streams = conf.getObject("kinesisStreams")
    val streamList = streams.keys.toList
    streamList.foreach(streamName => {
      val endpointList = streams.toConfig.getStringList(streamName).toList
      endpointList.foreach(endpointUrl => {
        /* Determine the number of shards from the stream */
        val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
        kinesisClient.setEndpoint(endpointUrl)
        val numShards = kinesisClient.describeStream(streamName).getStreamDescription().getShards()
          .size()

        /* Create 1 Kinesis Worker/Receiver/DStream for each shard. */
        val numStreams = numShards

        /**
         *  Create the same number of Kinesis DStreams/Receivers as Kinesis stream's shards
         *
         *  Replication of the received data within Spark can be disabled by setting the storage level
         *  for the input stream to StorageLevel.MEMORY_AND_DISK_SER when the Write Ahead Log is enabled
         *  as the log is already stored in a replicated storage system.
         */
        val currentStreams = (0 until numStreams).map { i =>
          KinesisUtils.createStream(ssc, streamName, endpointUrl, kinesisCheckpointInterval,
            InitialPositionInStream.LATEST, StorageLevel.MEMORY_AND_DISK_SER)
        }
        kinesisStreams = kinesisStreams ++ currentStreams
      })
    })

    /* Union all the streams */
    val unionStreams = ssc.union(kinesisStreams)

    // Split lines
    val lines = unionStreams.flatMap(byteArray => new String(byteArray).split(EOL))
    // Events processing
    lines.map(record => {
      try {
        val jsonOutput = EventParser.parse(record)
        val jsonObject = new JSONObject(jsonOutput)
        // Check error field
        var error = None: Option[String]
        if (jsonObject.has(ERROR)) {
          error = Some(jsonObject.getString(ERROR))
        }
        error match {
          case Some(output) => {
            // Forward invalid events
            EventParserUtils.forwardInvalidEvents(conf, record)
            // Invalid events
            if (output.equals(JsonError.VALIDATION_FAILED.toString)) {
              // Validation failed
              EventParserUtils.getOutputJson(jsonOutput, new JSONObject(record), basePath, INVALID_EVENT, true)
            } else {
              // Other exceptions
              ("invalid/archive", jsonOutput)
            }
          }
          case None => {
            // Forward to other Kinesis streams
            EventParserUtils.forwardKinesisStreams(conf, record)
            // Valid events
            EventParserUtils.getOutputJson(jsonOutput, jsonObject, basePath, jsonObject.getString("event"), true)
          }
        }
      } catch {
        case e: Exception => {
          logError("Error message (%s)".format(e.getMessage))
          ("invalid/archive", record)
        }
      }
//    }).reduceByKey((a:String, b:String) => (a + EOL + "(null)  " + b))
//    }).reduceByKey((a:String, b:String) => (a + EOL + b))
    }).groupByKey().map({case (k,v) => {
        val v1 = v.mkString(EOL)
        (k, v1)
      }})
      .foreachRDD(rdd => {
      // Save in the s3 path
//      if (rdd.count() > 0) {
//        logDebug("RDD count (%d)".format(rdd.count()))
//        rdd.map(event => (new Text(event._1), new Text(event._2)))
//          .saveAsHadoopFile(basePath, classOf[NullWritable], classOf[Text],
//            classOf[RDDMultipleSequenceFileOutputFormat], classOf[GzipCodec])
        rdd.saveAsHadoopFile(basePath, classOf[String], classOf[String],
          classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])
//      }
      })

    ssc
  }
}

package com.funplus.stream

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.util.EventParserUtils
import org.apache.hadoop.io.Text
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.{Milliseconds, StreamingContext}
import org.apache.spark.streaming.StreamingContext.toPairDStreamFunctions
import org.apache.spark.streaming.kinesis.KinesisUtils
import org.apache.spark.{Logging, SparkConf}
import scala.compat.Platform.EOL

/**
 * Created by chunzeng on 4/15/15.
 */
object KinesisS3Consumer extends Logging {
  def main(args: Array[String]) {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: KinesisS3Consumer <app-name> <stream-name> <endpoint-url> <base-path> <batch-interval>
          |
          |    <app-name> is the name of the streaming application
          |               (e.g. myFirstStreamConsumer)
          |    <stream-name> is the name of the Kinesis stream
          |                  (e.g. myFirstStream)
          |    <endpoint-url> is the endpoint of the Kinesis service
          |                   (e.g. https://kinesis.us-west-2.amazonaws.com)
          |    <base-path> is the base output s3 path
          |                (e.g. s3n://com.funplusgame.bidata/dev/test/kinesis/)
          |    <batch-interval> is the batch interval in seconds
          |                    (e.g. 10)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(appName, streamNameList, endpointUrl, basePath, interval) = args

    /* Setup the and SparkConfig and StreamingContext */
    /* Spark Streaming batch interval */
    val batchInterval = Milliseconds(interval.toInt * 1000)
    val sparkConfig = new SparkConf().setAppName(appName)
    // Initialize streaming context
    val ssc = new StreamingContext(sparkConfig, batchInterval)

    /* Kinesis checkpoint interval */
    val kinesisCheckpointInterval = batchInterval

    // Loop Kinesis streams
    var kinesisStreams = IndexedSeq[ReceiverInputDStream[Array[Byte]]]()
    val streamList = streamNameList.split(",").toList
    streamList.foreach(streamName => {
      /* Determine the number of shards from the stream */
      val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
      kinesisClient.setEndpoint(endpointUrl)
      val numShards = kinesisClient.describeStream(streamName).getStreamDescription().getShards()
        .size()

      /* Create 1 Kinesis Worker/Receiver/DStream for each shard. */
      val numStreams = numShards

      /* Create the same number of Kinesis DStreams/Receivers as Kinesis stream's shards */
      val currentStreams = (0 until numStreams).map { i =>
        KinesisUtils.createStream(ssc, streamName, endpointUrl, kinesisCheckpointInterval,
          InitialPositionInStream.LATEST, StorageLevel.MEMORY_AND_DISK_2)
      }
      kinesisStreams = kinesisStreams ++ currentStreams
    })

    /* Union all the streams */
    val unionStreams = ssc.union(kinesisStreams)

    // Split lines
    val lines = unionStreams.flatMap(byteArray => new String(byteArray).split(EOL))
    // Events processing
    lines.map(record => {
      try {
        val jsonObject = new JSONObject(record)
        try {
          // Version 1.2
          val app_id = jsonObject.getString("app_id")
          val ts = jsonObject.getLong("ts")
          val outputPath = EventParserUtils.getOutputPath(basePath, app_id, ts)
          (outputPath, record)
        } catch {
          case e: Exception => {
            // Version 1.1
            val app_id = jsonObject.getString("@key")
            val ts = jsonObject.getLong("@ts")
            val outputPath = EventParserUtils.getOutputPath(basePath, app_id, ts)
            (outputPath, record)
          }
        }
      } catch {
        case e: Exception => {
          logError("Error message (%s)".format(e.getMessage))
          ("invalid/archive", record)
        }
      }
//    }).reduceByKeyAndWindow((a:String, b:String) => (a + EOL + b), windowLength, slideInterval)
    }).groupByKey().map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
      }})
      .foreachRDD(rdd => {
        // Save in the s3 path
//        if (rdd.count() > 0) {
//          logDebug("RDD count (%d)".format(rdd.count()))
          rdd.saveAsHadoopFile(basePath, classOf[String], classOf[String],
              classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])
//        }
      })

    // Add shutdown hook to stop gracefully
    // Set spark.streaming.stopGracefullyOnShutdown to true instead
//    sys.ShutdownHookThread {
//      logDebug("Gracefully stopping Spark Streaming Application")
//      ssc.stop(true, true)
//      logDebug("Application stopped")
//    }

    /* Start the streaming context and await termination */
    ssc.start()
    ssc.awaitTermination()
  }
}

package com.funplus.stream

import com.funplus.parser.EventParser
import com.typesafe.scalalogging.Logger
import com.typesafe.scalalogging.slf4j.{LazyLogging, StrictLogging}
import org.apache.spark.{SparkContext, SparkConf}
import org.slf4j.LoggerFactory
import scala.compat.Platform.EOL

/**
 * Created by balaji on 3/12/15.
 */
object EventStream extends LazyLogging{


  def main (args: Array[String]) {
    logger.info("Starting EventStream Application")
    val conf = new SparkConf().setAppName("EventStream").setMaster("local[*]")
    val sc = new SparkContext(conf)
    val data = sc.textFile("kinesis-app/src/main/resources/sample.txt")
    /*    for (jsonInput <- data) {
          val jsonOutput = EventParser.parse(jsonInput)
          logger.debug("jsonOutput {}", jsonOutput)
        }*/
    val unionStreams = sc.union(data)
    val lines = unionStreams.flatMap(byteArray => new String(byteArray).split(EOL))
    logger.info("count is {}",lines.count().toString)
    /*lines.foreach(
      record => println(record))*/
    /*    val jsonObject = Scala.j
        lines.

        logger.info("Closing EventStream Application")*/
  }


}

package com.funplus.parser

import com.amazonaws.util.json.JSONObject
import com.funplus.util.{Utilities, CountryCodeLookupService}
import com.funplus.validator.{JsonError, JsonDataError, JsonSchemaFactory}
import com.typesafe.scalalogging.slf4j.LazyLogging

/**
 * Created by balaji on 3/9/15.
 */

/**
 * This is singleton object which parses and validates given inputJson
 */
object EventParser extends LazyLogging {

  private val INVALID_EVENT = "invalid"
  private val INVALID_JSON = "invalidJson"

  def parse(originalInputJson: String): String = {
    // Save the value
    var inputJson = originalInputJson

    logger.debug("EventParser started for inputJson({})", inputJson)
    var outputJson: JSONObject = null
    var jsonDataError: JsonDataError = null
    var eventName: String = null
    var appId: String = null
    var biVersion: String = null
    try {
      outputJson = new JSONObject(inputJson)
    }
    catch {
      // If the input data is not json we will capture here
      // construct new json object with needed info and return it
      case e: Exception => {
        logger.warn("Exception({}) thrown when processing data ({})", e.getMessage, inputJson)
        outputJson = null
        outputJson = new JSONObject
        outputJson.put("event", INVALID_EVENT)
        jsonDataError = new JsonDataError(JsonError.JSON_EXCEPTION.toString, e.getMessage)
        Utilities.includeErrorDataInJson(outputJson, jsonDataError)
        outputJson.put(INVALID_JSON, inputJson)
        return outputJson.toString
      }
    }

    // Fix the issue of public ip
    var public_ip = None: Option[String]
    if (outputJson.has("ip") && outputJson.has("msg")) {
      public_ip = Some(outputJson.getString("ip"))
      outputJson = outputJson.getJSONObject("msg")
      // Reset the value
      inputJson = outputJson.toString
    }

    //identify biVersion
    if (outputJson.has("data_version")) {
      // 2.0: data_version
      biVersion = outputJson.getString("data_version")
    } else if (outputJson.has("bi_version")) {
      // 1.2
      biVersion = outputJson.getString("bi_version")
    } else {
      //biVersion is not available then default it to 1.1
      biVersion = "1.1"
      outputJson.put("bi_version", biVersion)
    }

    try {
      eventName = outputJson.getString("event")
      // app and ts varies for biVersion 1.1
      if("1.1".equals(biVersion)){
        appId = outputJson.getString("@key")
      }
      else{
        appId = outputJson.getString("app_id")
      }
      jsonDataError = JsonSchemaFactory.validate(biVersion, appId, eventName, inputJson)
    }
    catch {
      case e: Exception => {
        jsonDataError = new JsonDataError(JsonError.PROCESSING_ERROR.toString, e.getMessage)
      }
    }
    //if data process has error, add it to outputJson
    if(null != jsonDataError) {
      outputJson = null
      outputJson = new JSONObject
      if (eventName != null) {
        outputJson.put("event", eventName)
      } else {
        outputJson.put("event", INVALID_EVENT)
      }
      Utilities.includeErrorDataInJson(outputJson, jsonDataError)
      outputJson.put(INVALID_JSON, inputJson)
    }
    //if data process has no error then include country_code,@ts_pretty,install_ts_pretty
    else {
      // app and ts varies for biVersion 1.1
      if("1.1".equals(biVersion)){
        Utilities.includeDefaultPrettyDateInJson("@ts", outputJson)
        // Fix the issue of public ip
        if (! public_ip.isEmpty) {
          outputJson.put("ip", public_ip.get)
        }
        CountryCodeLookupService.includeCountryCodeInJson(outputJson)
        //CountryCodeLookupSandboxService.includeCountryCodeInJson(outputJson)
        Utilities.includeDefaultPrettyDateInJson("install_ts", outputJson)
      }
      else{
        Utilities.includeDefaultPrettyDateInJson("ts", outputJson)
        // if properties are not available
        if (outputJson.has("properties")) {
          val properties = outputJson.getJSONObject("properties")
          // Fix the issue of public ip
          if (! public_ip.isEmpty) {
            properties.put("ip", public_ip.get)
          }
          CountryCodeLookupService.includeCountryCodeInJson(properties)
          //CountryCodeLookupSandboxService.includeCountryCodeInJson(properties)
          Utilities.includeDefaultPrettyDateInJson("install_ts", properties)
        }
      }
    }
    logger.debug("EventParser completed for inputJson({}) and generated outputJson({})", inputJson, outputJson)
    outputJson.toString()
  }


}

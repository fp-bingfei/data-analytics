package com.funplus.parser

import org.apache.commons.lang3.StringUtils
import java.text.SimpleDateFormat
import java.util.UUID

import com.amazonaws.util.json.JSONObject
import com.funplus.util.{CountryCodeLookupService, Utilities}
import com.typesafe.scalalogging.slf4j.LazyLogging

/**
 * Created by Yanyu on 12/16/15.
 */

/**
 * This is singleton object which parses and validates given inputJson
 */
object EventParser_1_1 extends LazyLogging {

//  private val INVALID_EVENT = "invalid"
//  private val INVALID_JSON = "invalidJson"
    private val MISSED = "-1"
    private val OUTPUT_FIELDS = Array("id", "@key", "@ts",
    "uid", "snsid", "install_ts", "install_source", "country_code", "ip", "browser",
    "browser_version", "os", "os_version", "event", "properties")

  def parse(inputJson: String): String = {
    logger.debug("EventParserV1_1 started for inputJson({})", inputJson)
    var jsonObject: JSONObject = null
    try {
      jsonObject = new JSONObject(inputJson)
      jsonObject = preProcessJSON(jsonObject)

      if(validateInputData(jsonObject)){
        val outputFields = getOutputFields(jsonObject, OUTPUT_FIELDS)
        //set properties field value - if > 5000, remove action_detail attribute
        if(jsonObject.toString().length()> 5000) {
          if (jsonObject.has("action_detail")) {
            jsonObject.remove("action_detail")
          }
        }
        outputFields(OUTPUT_FIELDS.length - 1) = jsonObject.toString()
        outputFields.mkString("\t")
       // logger.warn("Process({}) --> data", outputFields)
      //  outputTSV = outputFields.mkString("\t")
      //  logger.warn("Stringoutput({}) --> String", outputTSV)
      //  outputTSV
      }
      else {
        logger.warn(String.format("Invalid JSON - %s", inputJson))
        val outputArray = new Array[String](2)
        outputArray(0) = "Invalid JSON"
        outputArray(1) = inputJson
        outputArray.mkString("\t")
      }
    }
    catch {
      // If the input data is not json we will capture here
      // construct new json object with needed info and return it
      case e: Exception => {
        logger.warn("Exception({}) thrown when processing data ({})", e.getMessage, inputJson)
        val outputArray = new Array[String](2)
        outputArray(0) = "Invalid JSON"
        outputArray(1) = inputJson
        outputArray.mkString("\t")
      }
    }

  }

  def getOutputFields(jsonObject:JSONObject, outputFieldNames:Array[String]):Array[String] = {
    val outputArray = new Array[String](outputFieldNames.length)
    var columnIndex = 0
    for(outputFieldName <- outputFieldNames) {
      try{
        if("id".equals(outputFieldName)){
          outputArray(columnIndex) = UUID.randomUUID().toString()
        } else if ("@ts".equals(outputFieldName) || "install_ts".equals(outputFieldName)) {
          try{
            val timeStamp = jsonObject.getLong(outputFieldName)
            outputArray(columnIndex) = String.valueOf(Utilities.getDefaultFormattedDateForTimestamp(timeStamp))
          } catch {
            case e: Exception => {
              val timeStamp = jsonObject.getString(outputFieldName)
              val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
              outputArray(columnIndex) = String.valueOf(Utilities.getDefaultFormattedDateForTimestamp((df.parse(timeStamp).getTime()/1000).toInt))
            }
          }
        } else if ("country_code".equals(outputFieldName)) {
            outputArray(columnIndex) = CountryCodeLookupService.getCountryCode(Some(jsonObject.getString("ip")))
        } else if ("properties".equals(outputFieldName)) {
            //do nothing will be handled lately
        } else {
        outputArray(columnIndex) = String.valueOf(jsonObject.get(outputFieldName))
      }
      } catch {
        case e: Exception => {
          outputArray(columnIndex) = StringUtils.EMPTY
        }
      }
      jsonObject.remove(outputFieldName)
      columnIndex += 1
    }
      outputArray

  }

  def preProcessJSON(jsonObject:JSONObject):JSONObject = {
    var json = Utilities.renameJSONStringKey("event_name","event",jsonObject)
    json = Utilities.renameJSONStringKey("install_src", "install_source", json)
    json = Utilities.renameJSONStringKey("track_ref", "install_source", json)
    json = Utilities.renameJSONStringKey("addtime", "install_ts", json)
    json = Utilities.renameJSONStringKey("browserVersion", "browser_version", json)
    json
  }

  def validateInputData(jsonObject: JSONObject):Boolean = {
    Utilities.validate(jsonObject, "snsid", StringUtils.EMPTY) && Utilities.validate(jsonObject, "@key", StringUtils.EMPTY) &&
    Utilities.validate(jsonObject, "event", StringUtils.EMPTY) && Utilities.validate(jsonObject, "@ts", MISSED) && Utilities.validate(jsonObject, "uid", MISSED)
  }
}

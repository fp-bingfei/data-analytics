package com.funplus.finance

import com.funplus.util.Utilities
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by balaji on 8/19/15.
 */
object ConsumableItemJob extends Logging {


  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 2) {
      System.err.println(
        """
          |Usage: ConsumableItemJob <job-name> <input-path> <output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. xyz)
          |    <input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/input/*/20150826/)
          |    <output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/processed/20150826/)
        """.stripMargin)
      System.exit(1)
    }
    val Array(jobName, inputPath, outputPath) = args

    val sparkConf = new SparkConf().setAppName(jobName)
    //  .setMaster("local")
    val sc = new SparkContext(sparkConf)

    val inputRDD = sc.textFile(inputPath)
    //val inputRDD = sc.textFile("/tmp/6r")

    val result = inputRDD.flatMap(record => {
      Some(record)
    }).
      map( record => {
      val columns = record.split("\t")
      var value: (String, String, Long, String, String, String, String, String, String,
        Int, Int, String, Double, Int, Double, String, String, String) = null
      //identify data type (fact or dim) based on column size
      columns.size match {
        // fact data type
        case 20 => {
          //id  tsPretty  ts  appId playerId  sessionId event itemId  itemType  itemClass itemPurchasedQty  itemSpentQty  transactionType usdPaid costPerUnit  dataVersion appVersion  action currencyType
          val Array(id, tsPretty, ts, appId, playerId, sessionId, event, itemId, itemType, itemName, itemClass,
          itemPurchasedQty, itemSpentQty, transactionType, usdPaid,
          costPerUnit, dataVersion, appVersion, action, currencyType) = columns
          value = (id, tsPretty, Utilities.toLong(ts), appId, playerId, sessionId, itemId, itemType, itemClass,
            Utilities.toInt(itemPurchasedQty), Utilities.toInt(itemSpentQty), transactionType, Utilities.toDouble(usdPaid), 0, 0d, itemName, action, currencyType)
        }
        // dim data type
        case 12 => {
          //id      ts  tsPretty      appId   playerId        sessionId       itemId  itemType        itemClass       itemPurchasedQty        itemSpentQty    transactionType usdPaid costPerUnit
          val Array(id, tsPretty, ts, appId, playerId, itemId, itemType, itemName, itemClass, currencyType,
          batchItemBalance, batchUsdBalance) = columns
          value = (id, tsPretty, Utilities.toLong(ts), appId, playerId, "", itemId, itemType, itemClass,
            0, 0, "", 0d, Utilities.toInt(batchItemBalance), Utilities.toDouble(batchUsdBalance), itemName, "", currencyType)
        }
        // invalid data type
        case _ => {
          logError(String.format("Invalid data format(%s)", columns.toSeq))
          throw new RuntimeException(String.format("Invalid data format(%s)", columns.toSeq))
        }

      }
      // generate key based on appId playerId itemId
      val key = DigestUtils.md5Hex(value._4 + value._5 + value._7)
      val outData = (key, value)
      outData
    }
      ).groupByKey().map({ case (k, v) => {
      // sort by unix timestamp (in millis)
      val rows = v.toSeq.sortBy(_._3)

      // need a queue for FIFO logic
      val queue : mutable.Queue[ConsumableItem] = new scala.collection.mutable.Queue[ConsumableItem]
      val buffer = new ArrayBuffer[String](v.size)

      var itemBalanceQty : Int= 0
      var usdBalance : Double = 0

      rows.foreach(transaction => {

        // invalid transaction
        if((transaction._10 > 0 && transaction._11 > 0) ||
          (transaction._11 > 0 && transaction._14 > 0) || (transaction._10 > 0 && transaction._11 > 0)){
          throw new RuntimeException(String.format("Invalid transaction data(%s)", transaction))
        }
        // purchase transaction
        else if(transaction._10 > 0){
          val item = new ConsumableItem(transaction._3, transaction._4, transaction._5, transaction._7,
            transaction._8, transaction._9, transaction._10, transaction._13, transaction._14,
            transaction._15, transaction._16, transaction._18 )
          buffer += item.purchaseTransaction(transaction._1, transaction._2, transaction._6, transaction._12, itemBalanceQty, usdBalance, transaction._17)
          queue += item

          itemBalanceQty = item.itemBalanceQty
          usdBalance = item.usdBalance
        }
        // used transaction
        else if(transaction._11 > 0){
          var usedBalance = transaction._11
          while(usedBalance > 0) {
            val itemOption = queue.get(0)
            itemOption match {
              case Some(item) => {
                buffer += item.usedTransaction(transaction._1, transaction._2, transaction._6, transaction._12, usedBalance, itemBalanceQty, usdBalance, transaction._17)
                usedBalance = item.usedBalance
                itemBalanceQty = item.itemBalanceQty
                usdBalance = item.usdBalance
                if(item.batchItemEndingBalQty == 0) queue.dequeue()
              }
              case None => {
                logError("Atleast one item should be available for used transaction")
                usedBalance = 0
                //throw new RuntimeException("Atleast one item should be available for used transaction")
              }
            }
          }
        }
        // balance transaction
        else if(transaction._14 > 0){
          val item = new ConsumableItem(transaction._3, transaction._4, transaction._5, transaction._7,
            transaction._8, transaction._9, transaction._10, transaction._13, transaction._14, transaction._15, transaction._16, transaction._18 )
          item.balanceTransaction(transaction._1, transaction._2, transaction._6, transaction._12, itemBalanceQty, usdBalance)
          queue += item
          itemBalanceQty = item.itemBalanceQty
          usdBalance = item.usdBalance
        }
        // atleast 1 transaction should have qty > 0
        else{
          logError(String.format("No transaction qty is > 0 (%s)", transaction))
          //todo: need filtering from prep data
          //throw new RuntimeException(String.format("No transaction qty is > 0 (%s)", transaction))
        }
      })
      // clear queue once all the transaction for the given is processed.
      queue.clear()
      buffer.mkString(EOL)
    }
    }).saveAsTextFile(outputPath, classOf[GzipCodec])

    sc.stop()

  }

}



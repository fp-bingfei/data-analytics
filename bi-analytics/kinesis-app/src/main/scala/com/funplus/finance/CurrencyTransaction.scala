package com.funplus.finance

/**
 * Created by balaji on 8/19/15.
 */
class CurrencyTransaction {

  var currencyBalance: Int = 0
  var usdUsed: Double = 0d
  var usdBalance: Double = 0d
  var costPerUnit: Double = 0d


  def updateAndGet(transaction: (String, String, Long, String, String, Int, Double, Int, String, String, String, Int, Double, Double, String)): String = {

    val id = transaction._1
    val tsPretty = transaction._2
    val ts = transaction._3
    val appId = transaction._4
    val playerId = transaction._5
    val currencyPurchaseQty = transaction._6
    val usdPaid = transaction._7
    val currencyUsedQty = transaction._8
    val currencyType = transaction._9
    val sessionId = transaction._10
    val transactionType = transaction._11
    val currencyBeginningBalance: Int = transaction._12
    val usdBeginningBalance: Double = transaction._13
    val beginningCostPerUnit: Double = transaction._14
    val action = transaction._15

    val isPreviousBalance = currencyBeginningBalance > 0 || usdBeginningBalance > 0 || beginningCostPerUnit > 0

    //reset all metrics. Assuming this will be first row (i.e. balance)
    // no log output for balance transaction
    if (isPreviousBalance) {
      currencyBalance = currencyBeginningBalance
      usdUsed = 0
      usdBalance = usdBeginningBalance
      costPerUnit = beginningCostPerUnit
      ""
    }
    else {
      //metrics
      // previous - currencyBalance
      currencyBalance = currencyBalance + currencyPurchaseQty - currencyUsedQty
      // previous - costPerUnit
      usdUsed = currencyUsedQty * costPerUnit
      // previous - usdBalance; updated - usdUsed
      usdBalance = usdBalance + usdPaid - usdUsed
      // updated - usdBalance & currencyBalance

      costPerUnit = if (currencyBalance == 0) 0 else usdBalance / currencyBalance
      //Filter test users
      if (usdBalance <= 1000000000d && costPerUnit <= 1000000000d && usdUsed <= 1000000000d && usdPaid <= 1000000000d) {
        val output = Array(id, tsPretty, ts, appId, playerId, currencyPurchaseQty, "%.3f".format(usdPaid), currencyUsedQty,
          currencyType, sessionId, transactionType, "%.3f".format(usdUsed), currencyBalance, "%.3f".format(usdBalance), "%.3f".format(costPerUnit), action)
        output.mkString("\t")
      }
      else{
        ""
      }
    }
  }

}

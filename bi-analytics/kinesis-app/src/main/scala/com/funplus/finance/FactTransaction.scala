package com.funplus.finance

import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.parser.EventParser
import com.funplus.util.{Utilities, EventParserUtils}
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions

import org.apache.hadoop.mapred.TextOutputFormat
import org.apache.spark.{Logging, SparkContext, SparkConf}

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by yangzhenxuan on 12/06/15.
 */
object FactTransaction extends Logging{
  private val INVALID_EVENT: String = "invalid"
  private val ERROR: String = "error"
  private val INVALID_JSON: String = "invalidJson"
  private val EVENT: String = "event"
  private val CURRENCY_SPENT: String = "c_currency_spent"
  private val CURRENCY_RECEIVED: String = "c_currency_received"
  private val CURRENCY_TYPE: String = "d_currency_type"
  private val CURRENCY_AMOUNT: String = "m_currency_amount"

  private val currencyChanges = List(CURRENCY_SPENT, CURRENCY_RECEIVED)


  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 5) {
      System.err.println(
        """
          |Usage: CustomEventsParser <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/logserver/custom/{app_id}/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val baseOutputPath = args(3)
    val scratchOutputPath = args(4) + "/" + date
    val appIds = if (args.length > 5) args(5) else ""

    val sparkConf = new SparkConf().setAppName(jobName)
    //.setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    val inputRDD = sc.textFile(inputPaths)

    val tmpResult = inputRDD.flatMap(record => {
      try{
        Some(EventParser.parse((record)))
      } catch {
        case e: Exception => None
      }
    })

    val currencyResult = tmpResult.map(
      jsonString => {
        val jsonObject = new JSONObject(jsonString)
        // Check error field
        var error = None: Option[String]
        if (jsonObject.has(ERROR)) {
          error = Some(jsonObject.getString(ERROR))
        }
        var output = None: Option[(String, List[String])]

        if(error.isEmpty)
          output = Some(getOutputCurrency(jsonString, jsonObject, baseOutputPath, jsonObject.getString(EVENT), false))
        else
          output = Some(getOutputCurrency(jsonString, new JSONObject(jsonObject.getString(INVALID_JSON)), baseOutputPath, INVALID_EVENT, false))

       output.get
      }).reduceByKey(_ ::: _).map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath + "/fact_currency_transaction", classOf[String], classOf[String],
        classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])

    val itemResult = tmpResult.map(
      jsonString => {
        val jsonObject = new JSONObject(jsonString)
        // Check error field
        var error = None: Option[String]
        if (jsonObject.has(ERROR)) {
          error = Some(jsonObject.getString(ERROR))
        }
        var output = None: Option[(String, List[String])]

        if(error.isEmpty)
          output = Some(getOutputItem(jsonString, jsonObject, baseOutputPath, jsonObject.getString(EVENT), false))
        else
          output = Some(getOutputItem(jsonString, new JSONObject(jsonObject.getString(INVALID_JSON)), baseOutputPath, INVALID_EVENT, false))

        output.get
      }).reduceByKey(_ ::: _).map({case (k,v) => {
      val v1 = v.mkString(EOL)
      (k, v1)
    }})
      .saveAsHadoopFile(scratchOutputPath + "/fact_item_transaction", classOf[String], classOf[String],
        classOf[RDDMultipleTextFileOutputFormat], classOf[GzipCodec])

     /* for event counter
      val counters = tmpResult.map({case (k,v) =>
          (k, v.size)
        }).repartition(1).
        saveAsHadoopFile(scratchOutputPath + "/counters/finance/", classOf[Text], classOf[IntWritable],
          classOf[TextOutputFormat[Text, IntWritable]])*/

    sc.stop()
  }


  def getOutputCurrency(jsonOutput: String, jsonObject: JSONObject, basePath: String,
                        event: String, appPartition: Boolean = false): (String, List[String]) = {

    var output = Nil : List[String]

    try {
      val biVersion = Option(jsonObject.getString("data_version"))
      val ts_pretty = Option(jsonObject.getString("ts_pretty"))
      val ts = Option(jsonObject.getLong("ts"))
      val app_id = Option(jsonObject.getString("app_id"))
      val user_id = Option(jsonObject.getString("user_id"))
      val session_id = Option(jsonObject.getString("session_id"))
      val event = Option(jsonObject.getString("event"))
      val propertiesJson = jsonObject.getJSONObject("properties")
      val currency_purchased_qty: String = "currency_purchased_qty"
      val amount_paid: String = ""
      val usd_paid: String = ""
      val currency: String = ""
      val currency_used_qty: String = "currency_used_qty"
      val currency_type: String = "currency_type"
      val transaction_type = Option(propertiesJson.optString("d_transaction_type", ""))
      val app_version = Option(propertiesJson.getString("app_version"))
      val action = Option(propertiesJson.getString("d_action"))

      val builder = new StringBuilder("")
      val id = DigestUtils.md5Hex(app_id.get + event.get + user_id.get + session_id.get + ts.get.toString)

      builder.append(id).append("\t").append(ts_pretty.get).append("\t").append(ts.get.toString).append("\t")
        .append(app_id.get).append("\t").append(user_id.get).append("\t").append(currency_purchased_qty).append("\t")
        .append(amount_paid).append("\t").append(usd_paid).append("\t").append(currency).append("\t").append(currency_used_qty)
        .append("\t").append(currency_type).append("\t").append(session_id.get).append("\t").append(transaction_type.get).append("\t")
        .append(biVersion).append("\t").append(app_version.get).append("\t").append(action.get)

      // judge if it's new format
      var flag = 0 : Int
      //@TODO: replace if-else with case if possible
      for (key <- currencyChanges) {
        if (propertiesJson.has(key)) {
          flag = 1
        }
      }

      if (flag == 1) {
        if (propertiesJson.has(CURRENCY_RECEIVED)) {
          val currencyReceived = propertiesJson.getJSONArray(CURRENCY_RECEIVED)
          for (i <- 0 until currencyReceived.length()) {
            var tmpBuilder: String = builder.toString()
            if (currencyReceived.getJSONObject(i).getInt(CURRENCY_AMOUNT) > 0) {
              tmpBuilder = tmpBuilder.replaceAll("currency_type", currencyReceived.getJSONObject(i).getString(CURRENCY_TYPE))
              tmpBuilder = tmpBuilder.replaceAll("currency_purchased_qty", currencyReceived.getJSONObject(i).getString(CURRENCY_AMOUNT))
              tmpBuilder = tmpBuilder.replaceAll("currency_used_qty", "")
              output = tmpBuilder :: output
            }
          }
        }
        if (propertiesJson.has(CURRENCY_SPENT)) {
          val currencySpent = propertiesJson.getJSONArray(CURRENCY_SPENT)
          for (i <- 0 until currencySpent.length()) {
            var tmpBuilder: String = builder.toString()
            if (currencySpent.getJSONObject(i).getInt(CURRENCY_AMOUNT) > 0) {
              tmpBuilder = tmpBuilder.replaceAll("currency_type", currencySpent.getJSONObject(i).getString(CURRENCY_TYPE))
              tmpBuilder = tmpBuilder.replaceAll("currency_used_qty", currencySpent.getJSONObject(i).getString(CURRENCY_AMOUNT))
              tmpBuilder = tmpBuilder.replaceAll("currency_purchased_qty", "")
              output = tmpBuilder :: output
            }
          }
        }
      }
      else {
        if (propertiesJson.has("d_currency_spent_type") && propertiesJson.has("m_currency_spent")) {
          var tmpBuilder: String = builder.toString()
            tmpBuilder = tmpBuilder.replaceAll("currency_type", propertiesJson.getString("d_currency_spent_type"))
            tmpBuilder = tmpBuilder.replaceAll("currency_used_qty", propertiesJson.getString("m_currency_spent"))
            tmpBuilder = tmpBuilder.replaceAll("currency_purchased_qty", "")
            output = tmpBuilder :: output
        }
        if (propertiesJson.has("d_currency_received_type") && propertiesJson.has("m_currency_received")) {
          var tmpBuilder: String = builder.toString()
          tmpBuilder = tmpBuilder.replaceAll("currency_type", propertiesJson.getString("d_currency_received_type"))
          tmpBuilder = tmpBuilder.replaceAll("currency_purchased_qty", propertiesJson.getString("m_currency_received"))
          tmpBuilder = tmpBuilder.replaceAll("currency_used_qty", "")
          output = tmpBuilder :: output
        }
      }

      val outputPath = getOutputPath(basePath, "fact_currency_transaction", app_id.get, ts.get, appPartition)
      (outputPath, output)
    } catch {
      case e: Exception => {
        // exception
        ("invalid/fact_currency_transaction/archive", e.toString :: jsonOutput :: output)
      }
    }
  }

  def getOutputItem(jsonOutput: String, jsonObject: JSONObject, basePath: String,
                        event: String, appPartition: Boolean = false): (String, List[String]) = {

    var output = Nil : List[String]

    try {
      val biVersion = Option(jsonObject.getString("data_version"))
      val ts_pretty = Option(jsonObject.getString("ts_pretty"))
      val ts = Option(jsonObject.getLong("ts"))
      val app_id = Option(jsonObject.getString("app_id"))
      val user_id = Option(jsonObject.getString("user_id"))
      val session_id = Option(jsonObject.getString("session_id"))
      val event = Option(jsonObject.getString("event"))
      val propertiesJson = jsonObject.getJSONObject("properties")
      val item_id: String = "item_id"
      val item_type: String = "item_type"
      val item_name:String = "item_name"
      val item_class: String = "item_class"
      val item_purchased_qty: String = "item_purchased_qty"
      val item_used_qty: String = "item_used_qty"
      val transaction_type = Option(propertiesJson.optString("d_transaction_type", ""))
      val usd_paid: String = "usd_paid"
      val cost_per_unit = ""
      val app_version = Option(propertiesJson.getString("app_version"))
      val action = Option(propertiesJson.getString("d_action"))
      val currency_type = "currency_type"
      val builder = new StringBuilder("")
      val id = DigestUtils.md5Hex(app_id.get + event.get + user_id.get + session_id.get + ts.get.toString)

      builder.append(id).append("\t").append(ts_pretty.get).append("\t").append(ts.get.toString).append("\t")
        .append(app_id.get).append("\t").append(user_id.get).append("\t").append(session_id.get).append("\t")
        .append(event.get).append("\t").append(item_id).append("\t").append(item_type).append("\t").append(item_name)
        .append("\t").append(item_class).append("\t").append(item_purchased_qty).append("\t").append(item_used_qty).append("\t")
        .append(transaction_type.get).append("\t").append(usd_paid).append("\t").append(cost_per_unit).append("\t")
        .append(biVersion.get).append("\t").append(app_version.get).append("\t").append(action.get).append("\t").append(currency_type)

      if (propertiesJson.has("c_items_used")) {
        val items_used = propertiesJson.getJSONArray("c_items_used")
        for (i <- 0 until items_used.length()) {
          var tmpBuilder: String = builder.toString()
          if (items_used.getJSONObject(i).optInt("m_item_amount", 0) > 0) {
            tmpBuilder = tmpBuilder.replaceAll("item_id", items_used.getJSONObject(i).getString("d_item_id"))
            tmpBuilder = tmpBuilder.replaceAll("item_type", items_used.getJSONObject(i).getString("d_item_type"))
            tmpBuilder = tmpBuilder.replaceAll("item_name", items_used.getJSONObject(i).getString("d_item_name"))
            tmpBuilder = tmpBuilder.replaceAll("item_class", items_used.getJSONObject(i).getString("d_item_class"))
            tmpBuilder = tmpBuilder.replaceAll("item_used_qty", items_used.getJSONObject(i).getString("m_item_amount"))
            tmpBuilder = tmpBuilder.replaceAll("item_purchased_qty", "")
            tmpBuilder = tmpBuilder.replaceAll("usd_paid", "")
            tmpBuilder = tmpBuilder.replaceAll("currency_type", "")
            output = tmpBuilder :: output
          }
        }
      }
      if (propertiesJson.has("c_items_received") && propertiesJson.getString("c_items_received") != "") {

        val items_received = propertiesJson.getJSONArray("c_items_received")
        for (i <- 0 until items_received.length()) {
          var tmpBuilder: String = builder.toString()
          if (items_received.getJSONObject(i).optInt("m_item_amount", 0) > 0) {
            tmpBuilder = tmpBuilder.replaceAll("item_id", items_received.getJSONObject(i).getString("d_item_id"))
            tmpBuilder = tmpBuilder.replaceAll("item_type", items_received.getJSONObject(i).getString("d_item_type"))
            tmpBuilder = tmpBuilder.replaceAll("item_name", items_received.getJSONObject(i).getString("d_item_name"))
            tmpBuilder = tmpBuilder.replaceAll("item_class", items_received.getJSONObject(i).optString("d_item_class",""))
            tmpBuilder = tmpBuilder.replaceAll("item_purchased_qty", items_received.getJSONObject(i).getString("m_item_amount"))
            tmpBuilder = tmpBuilder.replaceAll("item_used_qty", "")
            if (propertiesJson.has(CURRENCY_SPENT)) {
              val currencySpent = propertiesJson.getJSONArray(CURRENCY_SPENT)
              for (i <- 0 until currencySpent.length()) {
                if (currencySpent.getJSONObject(i).getInt(CURRENCY_AMOUNT) > 0) {
                  tmpBuilder = tmpBuilder.replaceAll("currency_type", currencySpent.getJSONObject(i).getString(CURRENCY_TYPE))
                  tmpBuilder = tmpBuilder.replaceAll("usd_paid", (currencySpent.getJSONObject(i).getInt(CURRENCY_AMOUNT)/items_received.length()).toString)
                  output = tmpBuilder :: output
                }
              }
            }
            else if (propertiesJson.has("d_currency_spent_type") && propertiesJson.has("m_currency_spent")) {
              tmpBuilder = tmpBuilder.replaceAll("currency_type", propertiesJson.getString("d_currency_spent_type"))
              tmpBuilder = tmpBuilder.replaceAll("usd_paid", (propertiesJson.getInt("m_currency_spent")/items_received.length()).toString)
              output = tmpBuilder :: output
            }
          }
        }
      }

      val outputPath = getOutputPath(basePath, "fact_item_transaction", app_id.get, ts.get, appPartition)
      (outputPath, output)
    } catch {
      case e: Exception => {
        // exception
        ("invalid/fact_item_transaction/archive", e.toString :: jsonOutput :: output)
      }
    }
  }

  def getOutputPath(basePath: String, event: String, app_id: String, ts: Long,
                    appPartition: Boolean = false, eventPartition: Boolean = true):String = {

    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }
    // Set game partition
    if (appPartition) {
      // Get short name of game
      val appName = if (app_id.indexOf('.') > 0) app_id.substring(0, app_id.indexOf('.')) else app_id
      builder.append("game=").append(appName).append("/")
    }
    // Set event partition
    if (eventPartition) {
      builder.append("event=").append(event).append("/")
    } else {
      builder.append(event).append("/")
    }
    val outputPath = builder.append("app=").append(app_id).append("/")
      .append("year=").append(Utilities.getFormattedDateForTimestamp(ts, "yyyy")).append("/")
      .append("month=").append(Utilities.getFormattedDateForTimestamp(ts, "MM")).append("/")
      .append("day=").append(Utilities.getFormattedDateForTimestamp(ts, "dd")).append("/")
      .append("hour=").append(Utilities.getFormattedDateForTimestamp(ts, "HH")).append("/")
      .append("archive").append(Utilities.getFormattedDateForTimestamp(ts, "mm")(0)).toString()
    outputPath
  }
  private def generateInputPaths(date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/").concat("*")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          inputPathBuffer += baseInputPath.concat(appId).concat(datePath)
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }
}

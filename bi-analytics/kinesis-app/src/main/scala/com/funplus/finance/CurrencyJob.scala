package com.funplus.finance

import com.funplus.util.Utilities
import org.apache.commons.codec.digest.DigestUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext.rddToPairRDDFunctions
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer
import scala.compat.Platform._

/**
 * Created by balaji on 8/19/15.
 */
object CurrencyJob {


  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 2) {
      System.err.println(
        """
          |Usage: CurrencyJob <job-name> <input-path> <output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. xyz)
          |    <input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/input/*/20150826/)
          |    <output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/output/processed/20150826/)
        """.stripMargin)
      System.exit(1)
    }
    val Array(jobName, inputPath, outputPath) = args

    val sparkConf = new SparkConf().setAppName(jobName)
    //  .setMaster("local")
    val sc = new SparkContext(sparkConf)

    val inputRDD = sc.textFile(inputPath)

    val result = inputRDD.flatMap(record => {
      Some(record)
    }).
      map( record => {
      var key: String = ""
      val columns = record.split("\t")
      var value: (String, String, Long, String, String, Int, Double, Int, String, String, String, Int, Double, Double, String) = null
      //identify data type (fact or dim) based on column size
      columns.size match {
        // fact data type
        case 16 => {
          //id  tsPretty  ts  appId playerId  currencyPurchasedQty  amountPaid  usdPaid currency  currencyUsedQty currencyType  sessionId transactionType dataVersion appVersion  action
          val Array(id, tsPretty, ts, appId, playerId, currencyPurchasedQty,
          amountPaid, usdPaid, currency, currencyUsedQty, currencyType,
          sessionId, transactionType, dataVersion, appVersion,  action) = columns
          value = (id, tsPretty, Utilities.toLong(ts), appId, playerId, Utilities.toInt(currencyPurchasedQty),
            Utilities.toDouble(usdPaid), Utilities.toInt(currencyUsedQty),
            currencyType, sessionId, transactionType, 0, 0d, 0d, action)
          key = DigestUtils.md5Hex(value._4 + value._5 + value._9)
        }
        // dim data type
        case 9 => {
          //id tsPretty  ts  appId playerId currencyType  currencyBeginningBalance  usdBeginningBalance beginningCostPerUnit
          val Array(id, tsPretty, ts, appId, playerId, currencyType, currencyBeginningBalance, usdBeginningBalance, beginningCostPerUnit) = columns
          value = (id, tsPretty, Utilities.toLong(ts), appId, playerId, 0, 0d, 0,
            currencyType, "", "beginning_balance", Utilities.toInt(currencyBeginningBalance),
            Utilities.toDouble(usdBeginningBalance), Utilities.toDouble(beginningCostPerUnit), "")
          key = DigestUtils.md5Hex(value._4 + value._5 + value._9)
        }
        // invalid data type
        case _ => {
          throw new RuntimeException(String.format("Invalid data format(%s)", columns.toSeq))
        }

      }
      // generate key based on appId and playerId
      //val key = DigestUtils.md5Hex(value._4 + value._5)
      val outData = (key, value)
      outData
    }
    ).groupByKey().map({ case (k, v) => {
      // sort by unix timestamp (in millis)
      val rows = v.toSeq.sortBy(_._3)
      val buffer = new ArrayBuffer[String](v.size)
      val transaction = new CurrencyTransaction
      rows.foreach(row => {
        val data = transaction.updateAndGet(row)
        buffer += data
      })
      buffer.mkString(EOL)
    }
    }).saveAsTextFile(outputPath, classOf[GzipCodec])

    sc.stop()

  }

}



package com.funplus.finance

/**
 * Created by balaji on 8/24/15.
 */

class ConsumableItem (val itemBatchId :Long, val appId: String,
                       val playerId: String, val itemId : String,
                       val itemType : String, val itemClass : String,
                       val itemPurchasedQty : Int, val usdPaid : Double,
                       val itemBeginningBalQty : Int, val usdBeginningBal : Double,
                       val itemName : String, val currencyType : String){

  // metrics
  var batchItemEndingBalQty : Int = itemBeginningBalQty + itemPurchasedQty
  var batchUsdEndingBal : Double = usdBeginningBal + usdPaid
  val costPerUnit : Double = (usdBeginningBal + usdPaid)/(itemBeginningBalQty + itemPurchasedQty)


  // flag
  var usedBalance:Int = 0

  var itemBalanceQty : Int= 0
  var usdBalance : Double = 0


  def usedTransaction(transaction: (String, String, String, String, Int, Int, Double, String)) : String = {
    val id = transaction._1
    val tsPretty = transaction._2
    val sessionId = transaction._3
    val transactionType = transaction._4
    var itemUsedQty = transaction._5
    var previousBalanceQty = transaction._6
    var previousUsdBalance = transaction._7
    var action = transaction._8

    (itemUsedQty > batchItemEndingBalQty) match {
      case true => {
        usedBalance = itemUsedQty - batchItemEndingBalQty
        itemUsedQty = batchItemEndingBalQty
      }
      case false => {
        usedBalance = 0
      }
    }
    batchItemEndingBalQty = batchItemEndingBalQty - itemUsedQty
    batchUsdEndingBal = batchItemEndingBalQty * costPerUnit

    val usdUsed = itemUsedQty * costPerUnit

    itemBalanceQty = previousBalanceQty - itemUsedQty
    usdBalance = previousUsdBalance - usdUsed

    val output = Array(id, tsPretty, itemBatchId, appId, playerId, sessionId, transactionType,
      itemId, itemType, itemName, itemClass, 0, 0d, itemUsedQty, "%.3f".format(usdUsed), batchItemEndingBalQty,
      "%.3f".format(batchUsdEndingBal), itemBalanceQty, "%.3f".format(usdBalance),
      "%.3f".format(costPerUnit), action, currencyType)
    output.mkString("\t")
  }

  def purchaseTransaction(transaction: (String, String, String, String, Int, Double, String)) : String = {
    val id = transaction._1
    val tsPretty = transaction._2
    val sessionId = transaction._3
    val transactionType = transaction._4
    var previousBalanceQty = transaction._5
    var previousUsdBalance = transaction._6
    var action = transaction._7


    itemBalanceQty = previousBalanceQty + itemPurchasedQty
    usdBalance = previousUsdBalance + usdPaid

    val output = Array(id, tsPretty, itemBatchId, appId, playerId, sessionId, transactionType,
      itemId, itemType, itemName, itemClass, itemPurchasedQty, "%.3f".format(usdPaid), 0, 0d,
      batchItemEndingBalQty, "%.3f".format(batchUsdEndingBal), itemBalanceQty, "%.3f".format(usdBalance),
      "%.3f".format(costPerUnit), action, currencyType )
    output.mkString("\t")
  }

  // no log output for balance transaction
  def balanceTransaction(transaction: (String, String, String, String, Int, Double)) : Unit = {
    val id = transaction._1
    val tsPretty = transaction._2
    val sessionId = transaction._3
    val transactionType = transaction._4
    var previousBalanceQty = transaction._5
    var previousUsdBalance = transaction._6

    itemBalanceQty = previousBalanceQty + itemBeginningBalQty
    usdBalance = previousUsdBalance + usdBeginningBal


  }


}

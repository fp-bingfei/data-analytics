package com.funplus.format

import org.apache.hadoop.io.{NullWritable, Text}
import org.apache.hadoop.mapred.lib.{MultipleTextOutputFormat}
import scala.compat.Platform.{currentTime}

/**
 * Created by balaji on 7/14/15.
 */

class RDDMultipleTextFileOutputFormat extends MultipleTextOutputFormat[Any, Any] {
  override def generateFileNameForKeyValue(key: Any, value: Any, name: String): String = {
    key.asInstanceOf[String] + "-" + currentTime
  }

  override def generateActualKey(key: Any, value: Any): Any = {
    NullWritable.get()
  }
}
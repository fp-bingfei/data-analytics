package com.funplus.format

import org.apache.hadoop.io.{NullWritable, Text}
import org.apache.hadoop.mapred.lib.MultipleSequenceFileOutputFormat
import scala.compat.Platform.{currentTime}

/**
 * Created by balaji on 7/14/15.
 */

class RDDMultipleSequenceFileOutputFormat extends MultipleSequenceFileOutputFormat[Any, Any] {
  override def generateFileNameForKeyValue(key: Any, value: Any, name: String): String = {
    key.asInstanceOf[Text].toString + "-" + currentTime
  }

  override def generateActualKey(key: Any, value: Any): Any = {
    NullWritable.get()
  }
}
package com.funplus.util

import akka.io.Udp.SO.Broadcast

import collection.JavaConversions._
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import com.amazonaws.util.json.JSONObject
import com.typesafe.config.Config
import java.nio.ByteBuffer
import scala.compat.Platform.{currentTime}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

/**
 * Created by balaji on 7/14/15.
 *
 * Shared code specific to event parser.
 */

object EventParserUtils {
  /**
    * Get output for TSV
    */
  def getOutPutString(outputArray: Array[String], basePath: String, app_id: String, ts: Long, broadcastDate: String = ""): (String, String) = {
    val suffix = "events/staging/hourly/"
    val outputPath = getOutputPathV1_1(basePath, app_id, ts, suffix, broadcastDate)
    (outputPath, outputArray.mkString("\t"))
  }

  /**
   * Get output JSON
   */
  def getOutputJson(jsonOutput: String, jsonObject: JSONObject, basePath: String,
                    event: String, appPartition: Boolean = false, broadcastDate: String = ""): (String, String) = {
    try {
      var biVersion: String = null
      if (jsonObject.has("data_version")) {
        // 2.0: data_version
        biVersion = jsonObject.getString("data_version")
      } else if (jsonObject.has("bi_version")) {
        // 1.2
        biVersion = jsonObject.getString("bi_version")
      } else {
        //biVersion is not available then default it to 1.1
        biVersion = "1.1"
      }

      // Check log_ts
      var log_ts = None: Option[Long]
      if (jsonObject.has("log_ts")) {
        log_ts = Option(jsonObject.getLong("log_ts"))
      }

      var app_id = None: Option[String]
      var ts = None: Option[Long]
      // app and ts varies for biVersion 1.1
      if ("1.1".equals(biVersion)) {
        app_id = Option(jsonObject.getString("@key"))
        ts = Option(jsonObject.getLong("@ts"))
      } else if (log_ts.nonEmpty) {
        app_id = Option(jsonObject.getString("app_id"))
        // output path should depends on log_ts and not ts.
        ts = log_ts
      } else {
        // use ts
        app_id = Option(jsonObject.getString("app_id"))
        ts = Option(jsonObject.getLong("ts"))
      }
      var outputPath = None: Option[String]
      if ("1.1".equals(biVersion)) {
        outputPath = Option(getOutputPath(basePath, event, app_id.get, ts.get, appPartition, false, broadcastDate))
      } else {
        outputPath = Option(getOutputPath(basePath, event, app_id.get, ts.get, appPartition, true, broadcastDate))
      }
      (outputPath.get, jsonOutput)
    } catch {
      case e: Exception => {
        // exception
        ("invalid/archive", jsonOutput)
      }
    }
  }


  /**
   * Get the output path for events with partition information
   */
  def getOutputPath(basePath: String, event: String, app_id: String, ts: Long,
                    appPartition: Boolean = false, eventPartition: Boolean = true,
                    broadcastDate: String = ""):String = {
    // Get short name of game
    val appName = if (app_id.indexOf('.') > 0) app_id.substring(0, app_id.indexOf('.')) else app_id
    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }
    // Set game partition
    if (appPartition) {
      builder.append("game=").append(appName).append("/")
    }
    // Set event partition
    if (eventPartition) {
      builder.append("event=").append(event).append("/")
    } else {
      builder.append(event).append("/")
    }
    val outputPath = builder.append("app=").append(app_id).append("/")
      .append("year=").append(Utilities.getFormattedDateForTimestamp(ts, "yyyy")).append("/")
      .append("month=").append(Utilities.getFormattedDateForTimestamp(ts, "MM")).append("/")
      .append("day=").append(Utilities.getFormattedDateForTimestamp(ts, "dd")).append("/")
      .append("hour=").append(Utilities.getFormattedDateForTimestamp(ts, "HH")).append("/")
      .append("archive").append(if (broadcastDate.length > 0) "-" else "").append(broadcastDate)
      .append(if (broadcastDate.length > 0) {
                // Per 10 minutes
                val minutes = Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "mm")) % 6
                "-0" + minutes
              } else "")
      .toString()
    outputPath
  }

  /**
   * Get the output path for events with partition information
   */
  def getDailyOutputPath(basePath: String, event: String, app_id: String, date: String, ts: Long = -1, eventPartition: Boolean = false):String = {
    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }
    if(eventPartition && ts != -1) {
      val outputPath = builder.append("event=").append(event).append("/")
        .append("app=").append(app_id).append("/")
        .append("dt=").append(Utilities.getFormattedDateForTimestamp(ts, "yyyy-MM-dd")).append("/")
        .append("archive").append("-").append(date).append("-")
        .append {
          var hours = None: Option[String]
          event match {
            case "payment" => {
              hours = Some("")
            }
            case "newuser" => {
              hours = Some("")
            }
            case _ => {
              hours = Some(Utilities.getFormattedDateForTimestamp(ts, "HH"))
            }
          }
          hours.get
        }
        .append("-")
        .append {
          var minutes = None: Option[String]
          app_id match {
            case "ffs.global.prod" => {
              event match {
                case "coins_transaction" => {
                  minutes = Some(Utilities.getFormattedDateForTimestamp(ts, "mm")
                    + "-0" +(Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "ss")) % 6))
                }
                case "UseFarmAids" | "QuestComplete" | "Kitchen"
                     | "CollectableDecoration" | "VisitNeighbors"
                     | "claim_daily_story" | "UseOP" | "SellProduct" | "FishingRecord" => {
                  minutes = Some("0" + Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "mm")) % 10)
                }
                case "login" | "luckypackage" | "barnView" | "lab"
                     | "Mystery_Store_Trade" | "Session_end"
                     | "BeautyshopProcessRecord" | "BeautyshopProcessReward"
                     | "FishingStart"  => {
                  minutes = Some("0" + Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "mm")) % 6)
                }
                case "cook_add" | "OnlinePackage" |  "fbc_vote_action"
                     | "fbc_vote_collect" | "cook_eat" | "WareHouse" => {
                  minutes = Some("0" + Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "mm")) % 3)
                }
                /*case "item_transaction" => {
                  minutes = Some(Utilities.getFormattedDateForTimestamp(ts, "mm")
                                      + "-0" +(Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "ss")) % 6)
                  )
                }*/
                case _ => {
                  minutes = Some("")
                }
              }
            }
            case "royal.us.prod" => {
              event match {
                case "coins_transaction" => {
                  minutes = Some(Utilities.getFormattedDateForTimestamp(ts, "mm"))
                }
                case "session_active" => {
                  minutes = Some("0" + Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "mm")) % 10)
                }
                /*case "item_transaction" => {
                  minutes = Some(Utilities.getFormattedDateForTimestamp(ts, "mm")
                                      + "-0" +(Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "ss")) % 6)
                  )
                }*/
                case _ => {
                  minutes = Some("")
                }
              }
            }
            case _ => {
              event match {
                case "coins_transaction" => {
                  minutes = Some("0" + Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "mm")) % 10)
                }
                /*case "item_transaction" => {
                  minutes = Some(Utilities.getFormattedDateForTimestamp(ts, "mm")
                                      + "-0" +(Integer.parseInt(Utilities.getFormattedDateForTimestamp(ts, "ss")) % 6)
                  )
                }*/
                case _ => {
                  minutes = Some("")
                }
              }
            }
          }
          minutes.get
        }
        .toString()
      outputPath
    } else {
      val outputPath = builder.append("raw_").append(event).append("_daily").append("/")
        .append("app=").append(app_id).append("/")
        .append("dt=").append(date).append("/")
        .append("archive").toString()
      outputPath
    }
  }

  /**
   * Get the output path for events
   */
  def getOutputPath(basePath: String, app_id: String, ts: Long):String = {
    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }
    val outputPath = builder.append(app_id).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "yyyy")).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "MM")).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "dd")).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "HH")).append("/")
      .append("archive").toString()
    outputPath
  }

  /**
    * Get the output path for V1_1 TSV events
    */
  def getOutputPathV1_1(basePath: String, app_id: String, ts: Long, suffix: String, broadcastDate: String = ""):String = {
    val builder = new StringBuilder(basePath)
    // Check slash
    if (! basePath.endsWith("/")) {
      builder.append("/")
    }

    val outputPath = builder.append(app_id).append("/").append(suffix)
      .append(Utilities.getFormattedDateForTimestamp(ts, "yyyy")).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "MM")).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "dd")).append("/")
      .append(Utilities.getFormattedDateForTimestamp(ts, "HH")).append("/")
      .append("archive").append(if (broadcastDate.length > 0) "-" else "").append(broadcastDate).append("-")
      .append(Utilities.getFormattedDateForTimestamp(ts, "mm")).toString()
    outputPath
  }

  /**
   * Forward to other Kinesis streams
   */
  def forwardKinesisStreams(conf: Config, record: String): Unit = {
    // Check forwardKinesisStreams
    if (conf.hasPath("forwardKinesisStreams")) {
      // Loop Kinesis streams
      val streams = conf.getObject("forwardKinesisStreams")
      val streamList = streams.keys.toList
      streamList.foreach(streamName => {
        val endpointUrl = streams.toConfig.getString(streamName)
        /* Create the Kinesis client */
        val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
        kinesisClient.setEndpoint(endpointUrl)

        /* Create a partitionKey based on recordNum */
        val partitionKey = s"partitionKey-$currentTime"

        /* Create a PutRecordRequest with an Array[Byte] version of the data */
        val putRecordRequest = new PutRecordRequest().withStreamName(streamName)
          .withPartitionKey(partitionKey)
          .withData(ByteBuffer.wrap(record.getBytes()))

        /* Put the record onto the stream */
        kinesisClient.putRecord(putRecordRequest)
      })
    }
  }

  /**
   * Forward invalid events to other Kinesis streams
   */
  def forwardInvalidEvents(conf: Config, record: String): Unit = {
    // Check forwardInvalidEvents
    if (conf.hasPath("forwardInvalidEvents")) {
      // Loop Kinesis streams
      val streams = conf.getObject("forwardInvalidEvents")
      val streamList = streams.keys.toList
      streamList.foreach(streamName => {
        val endpointUrl = streams.toConfig.getString(streamName)
        /* Create the Kinesis client */
        val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
        kinesisClient.setEndpoint(endpointUrl)

        /* Create a partitionKey based on recordNum */
        val partitionKey = s"partitionKey-$currentTime"

        /* Create a PutRecordRequest with an Array[Byte] version of the data */
        val putRecordRequest = new PutRecordRequest().withStreamName(streamName)
          .withPartitionKey(partitionKey)
          .withData(ByteBuffer.wrap(record.getBytes()))

        /* Put the record onto the stream */
        kinesisClient.putRecord(putRecordRequest)
      })
    }
  }

  /**
   * Check if s3 path exists
   */
  def checkPathExists(conf: Configuration, path: String): Boolean = {
    val s3Path = new Path(path)
    val fs = s3Path.getFileSystem(conf)
    return fs.exists(s3Path)
  }

  /**
   * Remove s3 path recursively
   */
  def removeS3Path(conf: Configuration, path: String): Boolean = {
    val s3Path = new Path(path)
    val fs = s3Path.getFileSystem(conf)
    return fs.delete(s3Path, true)
  }

}

package com.funplus.util

import java.nio.charset.Charset
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import com.amazonaws.util.json.JSONObject
import com.funplus.validator.JsonDataError
import org.apache.commons.codec.binary.Hex

import scala.collection.mutable

/**
 * Created by balaji on 3/11/15.
 */
object Utilities {

  private val DEFAULT_DATE_FORMAT: String = "yyyy-MM-dd HH:mm:ss"
  private val ERROR: String = "error"
  private val ERROR_DETAILS: String = "errorDetails"

  def includeErrorDataInJson (outputJson: JSONObject, jsonDataError: JsonDataError): Unit = {
    if (null != jsonDataError) {
      outputJson.put(ERROR, jsonDataError.error)
      val detailMessages: mutable.MutableList[String] = jsonDataError.detailMessages
      if (null != detailMessages) {
        for (message <- detailMessages) {
          outputJson.append(ERROR_DETAILS, message)
        }
      }
    }
  }

  def includePrettyDateInJson (inputKey: String, outputKey: String, outputJson:JSONObject, format: String): Unit = {
    var prettyDate = "";
    try {
      prettyDate = getFormattedDateForTimestamp(outputJson.getLong(inputKey), format);
    }
    catch {
      case e: Exception => { //do nothing
      }
    }
    outputJson.put(outputKey, prettyDate);
  }


  def getFormattedDateForTimestamp (timeStamp: Long, format: String): String = {
    val unixDate: Date = new Date(if (timeStamp > Int.MaxValue) timeStamp else timeStamp * 1000)
    val dateFormatter: SimpleDateFormat = new SimpleDateFormat(format)

    dateFormatter.format(unixDate)
  }

  def getDefaultFormattedDateForTimestamp (timeStamp: Long): String = {
    getFormattedDateForTimestamp(timeStamp, DEFAULT_DATE_FORMAT)
  }

  def includeDefaultPrettyDateInJson(inputKey: String, outputJson: JSONObject) {
    val outputKey: String = mutable.StringBuilder.newBuilder.append(inputKey).append("_").append("pretty").toString
    includePrettyDateInJson(inputKey, outputKey, outputJson, DEFAULT_DATE_FORMAT)
    // Fix the problem of long value
//    val timeStamp = outputJson.getLong(inputKey)
//    if (timeStamp > Int.MaxValue) {
//      outputJson.put(inputKey, timeStamp / 1000)
//    }
  }

  def renameJSONStringKey(renameFrom:String,renameTo:String, json: JSONObject):JSONObject = {
    if(json.has(renameTo)){
      json
    }
    if(json.has(renameFrom)){
      val value = json.optString(renameFrom,"")
      json.remove(renameFrom)
      json.put(renameTo,value)
    }
    json
  }

  def validate(jsonObject: JSONObject, fieldName: String, inValidValue: String):Boolean = {
    try{
      val jsonValue = String.valueOf(jsonObject.get(fieldName))
      !inValidValue.equals(jsonValue)
    }
    catch{
      case e: Exception => false
    }

  }
  def toInt(s: String): Int = {
    try {
      s.toInt
    } catch {
      case e: Exception => 0
    }
  }

  def toLong(s: String): Long = {
    try {
      s.toLong
    } catch {
      case e: Exception => 0
    }
  }

  def toDouble(s: String): Double = {
    try {
      s.toDouble
    } catch {
      case e: Exception => 0d
    }
  }


}

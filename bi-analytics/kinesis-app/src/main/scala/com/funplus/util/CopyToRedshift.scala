package com.funplus.util

import java.sql.{SQLException, DriverManager, Statement, Connection}
import java.util.Properties

import org.apache.spark.Logging

/**
 * Created by balaji on 6/30/15.
 */

class CopyToRedshift(val redshiftDelimiter : String, val options: Array[String],
                     var propertiesOption: Option[Properties]) extends Logging{
  
  
  def getPropertiesOption : Option[Properties] = {
    if(propertiesOption.isEmpty) {
      val properties = new Properties()
      properties.load(this.getClass.getClassLoader.getResourceAsStream("common.properties"))
      propertiesOption = Some(properties)
    }
      propertiesOption
  }

  private def executeStatement(query: String, connOption:Option[Connection]) = {
    var e = None: Option[Statement]
    try {
      if(connOption.nonEmpty)
        e = Some(connOption.get.createStatement())
      if(e.nonEmpty) {
        e.get.execute(query)
      }
    }
    finally {
      if(e.nonEmpty) e.get.close()
    }
  }

  private def getNumberOfCopiedRecords(connOption: Option[Connection]): Int = {
    val query = "select pg_last_copy_count();"
    var e = None: Option[Statement]
    var output = -1
    try {
      if(connOption.nonEmpty)
        e = Some(connOption.get.createStatement())
      if(e.nonEmpty) {
        val rs = e.get.executeQuery(query)
        rs.next()
        output = rs.getInt(1)
      }
      output
    }
    finally {
      close(connOption)
      output
    }
  }

  private def close(connOption: Option[Connection]) = {
     if(connOption.nonEmpty) {
       try {
         connOption.get.close()
       }
       catch {
         case e: Exception => {
           //do nothing
         }
       }
     }
  }


  def generateCopyStatement(mod: Integer, s3File:String, redshiftTable : String) : String = {
    val buf = new StringBuilder
    buf.append("COPY ").append(redshiftTable).append(" FROM \'")
      .append(s3File).append("\' CREDENTIALS \'aws_access_key_id=")
      .append(propertiesOption.get.getProperty("fs.s3n.awsAccessKeyId"))
      .append(";aws_secret_access_key=")
      .append(propertiesOption.get.getProperty("fs.s3n.awsSecretAccessKey"))
      .append("\' DELIMITER \'").append(redshiftDelimiter).append("\' ")
      .append(options.mkString(" ")).append(";")
    buf.toString()
  }

  def run(id : Integer, s3File:String, redshiftTable : String, redshiftUrl: String) = {
    val startTime = System.currentTimeMillis()
    val mod = ((id)+10)%10
    val query = generateCopyStatement(mod, s3File, redshiftTable)
    var connOption = None : Option[Connection]
    var retry = 0
    var error = None : Option[Exception]

    var redshiftURL = redshiftUrl
    if (!Option(redshiftURL).getOrElse("").isEmpty)
      redshiftURL = propertiesOption.get.getProperty("redshift.url")
    try {
      do {
        try {
          error = None
          Class.forName("com.amazon.redshift.jdbc41.Driver")
          connOption = Some(DriverManager.getConnection(redshiftURL, propertiesOption.get))
          executeStatement(query, connOption)
        }
        catch {
          case e: SQLException => {
            retry += 1
            error = Some(e)
            logWarning("retryAttempt(%d)-%s".format(retry, query))
          }
        }
      }
      while(error.nonEmpty && retry < 3)
      if(error.nonEmpty)
        throw error.get
      logDebug("Successfully copied %d records to Amazon Redshift from file %s".format(getNumberOfCopiedRecords(connOption), s3File))

    }
    finally {
      close(connOption)
      logInfo("%d-%d - CopyToRedshift - Total execution time %d (in Secs) for query %s".format(id, mod, (System.currentTimeMillis() - startTime)/1000, query))
    }
  }

}

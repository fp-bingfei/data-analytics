package com.funplus.util

import java.io.{InputStream, File}
import java.net.InetAddress

import com.amazonaws.util.json.JSONObject
import com.maxmind.geoip2.{DatabaseReader}
import com.maxmind.geoip2.model.CityResponse
import com.typesafe.scalalogging.Logging
import com.typesafe.scalalogging.slf4j.{LazyLogging, StrictLogging}

/**
 * Created by balaji on 3/11/15.
 * This API should be production env.
 */
object CountryCodeLookupService extends LazyLogging{

  private val COUNTRY_CODE = "country_code"

  var reader = init()

  def init(): DatabaseReader={
    var stream: Option[InputStream] = None
    try {
      // get the right database and add it to classpath.
      stream = Option(getClass().getClassLoader.getResourceAsStream("GeoIP2.mmdb"))
      if (stream.isEmpty) {
        logger.warn("GeoIP2.mmdb not found. Using GeoLite2-City.mmdb")
        stream = Option(getClass().getClassLoader.getResourceAsStream("GeoLite2-City.mmdb"))
      }
      reader = new DatabaseReader.Builder(stream.get).build()
    }
    catch {
      //todo: what to do if reader throws error?
      case e: Exception => { //do nothing
        logger.warn("Exception thrown during reader creation {}", e.getMessage)
      }
    }
    reader
  }


  def getCityResponse (ip: Option[String]): Option[CityResponse] = {
    var response: Option[CityResponse] = None
    if (ip.nonEmpty &&  null != reader) {
      try {
        val ipAddress = InetAddress.getByName(ip.get)
        response = Some(reader.city(ipAddress))
      }
      catch {
        case e: Exception  =>  { //do nothing
          logger.warn("Exception thrown when calling getCityResponse({}) {}", ip, e.getMessage)
        }
      }
    }
    response
  }


  def getCountryCode(ip: Option[String]): String = {
    var countryCode = None: Option[String]
    val response = getCityResponse(ip)
    if(response.nonEmpty){
      val cityResponse = response.get
      if (null != cityResponse.getCountry)
        countryCode = Some(cityResponse.getCountry.getIsoCode)
    }
    countryCode.getOrElse("")
  }

  def includeCountryCodeInJson (outputJson: JSONObject): Unit = {
    var ip = Some("")
    try {
        ip = Some(outputJson.getString("ip"))
    }
    catch {
      case e: Exception => { //do nothing
        logger.warn("Exception thrown during ip access from json object", e.getMessage)
      }
    }
    outputJson.put(COUNTRY_CODE, getCountryCode(ip))
  }


}

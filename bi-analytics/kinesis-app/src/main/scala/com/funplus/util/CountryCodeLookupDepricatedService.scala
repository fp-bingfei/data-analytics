package com.funplus.util

import com.amazonaws.util.json.JSONObject
//import com.maxmind.geoip.Country
//import com.maxmind.geoip.LookupService
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

/**
 * Created by balaji on 3/11/15.
 * Old geo ip. not in use
 */
object CountryCodeLookupDepricatedService {
/*


  val logger = Logger(LoggerFactory.getLogger(CountryCodeLookupService.getClass))
  val geoIpDatabase:String = "GeoIP.dat";
  var lookupService: LookupService = init()

  def init()={
    try {
      lookupService = new LookupService (geoIpDatabase, LookupService.GEOIP_MEMORY_CACHE)
    }
    catch {
      case e: Exception => { //do nothing
        logger.warn("Exception thrown during lookupService creation {}", e.getMessage)
      }
    }
    lookupService
  }

  /**
   * Provides the country code for the given ip
   * @param ip
   * @return
   */
  def getCountryCode (ip: String): String = {
    var countryCode: String = ""
    if (null != ip && !ip.isEmpty && null != lookupService) {
      val country: Country = lookupService.getCountry(ip)
      if (null != country) {
        countryCode = country.getCode
      }
    }
    countryCode
  }

  /**
   * Utility method w.r.t. country code to include country code in output json
   * @param outputJson
   */
  def includeCountryCodeInJson (outputJson: JSONObject): Unit = {
    var ip: String = ""
    try {
      ip = outputJson.getString("ip")
    }
    catch {
      case e: Exception => { //do nothing
        logger.warn("Exception thrown during ip access from json object", e.getMessage)
      }
    }
    outputJson.put("country_code", getCountryCode(ip))
  }


*/


}

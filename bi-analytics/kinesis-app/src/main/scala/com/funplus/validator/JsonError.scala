package com.funplus.validator

/**
 * Created by balaji on 3/10/15.
 */
object JsonError extends Enumeration {
  type JsonError = Value
  val SCHEMA_NOT_FOUND,
  PROCESSING_ERROR,
  IO_ERROR,
  JSON_EXCEPTION,
  VALIDATION_FAILED = Value
}
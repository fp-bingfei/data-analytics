package com.funplus.validator

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.iterable.S3Objects
import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.main.JsonSchema
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.apache.commons.io.FilenameUtils
import scala.collection.mutable
import scala.io.Source


/**
 * Created by balaji on 3/10/15.
 */
object JsonSchemaLoader extends LazyLogging {


  val factory = com.github.fge.jsonschema.main.JsonSchemaFactory.byDefault()
  val s3 = new AmazonS3Client()
  var registeredSchemas:mutable.HashMap[String, JsonSchema] = collection.mutable.HashMap.empty[String, JsonSchema]

  def loadFromS3(): mutable.HashMap[String, JsonSchema] = {
    logger.info("Started schema load from s3")
    loadSchema()
    logger.info("Completed schema load from s3")
    registeredSchemas
  }

  def loadSchema() : Unit = {
    val s3PathList = collection.mutable.MutableList.empty[Array[String]]
    val s3ObjectIterator =  S3Objects.withPrefix(s3,"com.funplusgame.bidata","dev/schema/").iterator()
    while(s3ObjectIterator.hasNext){
      val s3Object = s3ObjectIterator.next()
      if(s3Object.getKey.endsWith(".json"))
        s3PathList += (Array(s3Object.getBucketName, s3Object.getKey))
    }
    s3PathList.par.map(registerSchema(_))
  }

  def registerSchema(s3Object: Array[String]): Unit = {
    try {
      val validationKey = getValidationKeyFromS3Path(s3Object(1))
      val is = s3.getObject(s3Object(0),s3Object(1)).getObjectContent
      val data = Source.fromInputStream(is).mkString("")
      val jsonNode = JsonLoader.fromString(data)
      registeredSchemas(validationKey) = factory.getJsonSchema(jsonNode)
    }
    catch {
      case e: Exception => {
        logger.error("Schema setup failed for filePath(s3://{}/{}) - {}",
          s3Object(0), s3Object(1), e.getMessage)
        throw new RuntimeException(String.format("Schema setup failed for filePath(s3://%s/%s) - %s",
          s3Object(0), s3Object(1), e.getMessage))
      }
    }
  }

  def getValidationKeyFromS3Path(filePath: String): String = {
    val pathSplits: Array[String] = filePath.split("/")
    val eventName = FilenameUtils.removeExtension(pathSplits(pathSplits.length - 1))
    val appId = pathSplits(pathSplits.length - 2)
    val biVersion = pathSplits(pathSplits.length - 3)
    getValidatorKey(biVersion, appId, eventName)
  }

  /**
   * key -> biVersion-appId-eventName
   * @param biVersion
   * @param appId
   * @param eventName
   * @return
   */
  def getValidatorKey (biVersion: String, appId: String, eventName: String): String = {
    // Get short name of appId
    val appName = if (appId.indexOf('.') > 0) appId.substring(0, appId.indexOf('.')) else appId
    // Get key
    biVersion match  {
      case "2.0" => "v2_0".concat("_").concat(appName).concat("_").concat(eventName)
      case "1.2" => "v1_2".concat("_").concat(appName).concat("_").concat(eventName)
      case "1.1" => "v1_1".concat("_").concat(appName).concat("_").concat(eventName)
      case _ =>  biVersion.concat("_").concat(appName).concat("_").concat(eventName)
    }
  }

}
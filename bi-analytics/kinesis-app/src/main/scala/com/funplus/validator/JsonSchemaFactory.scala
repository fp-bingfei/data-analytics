package com.funplus.validator

import java.io.IOException

import com.fasterxml.jackson.databind.JsonNode
import com.funplus.validator
import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.core.exceptions.ProcessingException
import com.github.fge.jsonschema.core.report.{ProcessingMessage, ProcessingReport}
import com.github.fge.jsonschema.main.JsonSchema
import com.typesafe.scalalogging.Logger
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.slf4j.LoggerFactory

import scala.collection.mutable

/**
 * Created by balaji on 3/10/15.
 */

object JsonSchemaFactory extends LazyLogging{

  private val GLOBAL = "global"
  private val DEFAULT_SCHEMA = "default"

  private val registeredSchemas: mutable.HashMap[String, JsonSchema] = JsonSchemaLoader.loadFromS3()

  def validate(biVersion: String, appId: String, eventName: String, inputJson:String): validator.JsonDataError = {
    val error: validator.JsonDataError = null
    logger.debug("Validating inputJson({}) against schema validationKey({}-{}-{})",
      inputJson, biVersion, appId, eventName)
    var report: ProcessingReport = null
    try {
      var jsonSchema = registeredSchemas.get(JsonSchemaLoader.getValidatorKey(biVersion, appId, eventName))
      if (jsonSchema.isEmpty) {
        jsonSchema = registeredSchemas.get(JsonSchemaLoader.getValidatorKey(biVersion, appId, DEFAULT_SCHEMA))
        if (jsonSchema.isEmpty) {
          jsonSchema = registeredSchemas.get(JsonSchemaLoader.getValidatorKey(biVersion, GLOBAL, DEFAULT_SCHEMA))
          if (jsonSchema.isEmpty) {
            return new JsonDataError(JsonError.SCHEMA_NOT_FOUND.toString)
          }
        }
      }
      val inputJsonNode = JsonLoader.fromString(inputJson)
      jsonSchema match {
        case Some(js) => {
          report = js.validate(inputJsonNode)
        }
        case None => return new JsonDataError(JsonError.PROCESSING_ERROR.toString)
      }
      if (!report.isSuccess) {
        return constructJsonErrorForValidationFailed(report)
      }
    }
    catch {
      case e: IOException => {
        return new JsonDataError(JsonError.IO_ERROR.toString, e.getMessage)
      }
      case e: ProcessingException => {
        return new JsonDataError(JsonError.PROCESSING_ERROR.toString, e.getMessage)
      }
    }
    error
  }

  private def constructJsonErrorForValidationFailed (report: ProcessingReport): JsonDataError = {
    val jsonDataError: JsonDataError = new JsonDataError(JsonError.VALIDATION_FAILED.toString)
    val iterator = report.iterator()
    while (iterator.hasNext) {
      val detailMessageBuilder = StringBuilder.newBuilder
      val processingMessage: ProcessingMessage = iterator.next
      val instanceNode: JsonNode = processingMessage.asJson.get("instance")
      if (null != instanceNode) {
        val pointerNode: JsonNode = instanceNode.get("pointer")
        if (null != pointerNode) {
          val pointerText: String = pointerNode.asText
          if (pointerText != null && !pointerText.isEmpty) {
            detailMessageBuilder.append(pointerText.replaceFirst("/", "")).append(": ")
          }
        }
      }
      detailMessageBuilder.append(processingMessage.getMessage)
      jsonDataError.addDetailMessage(detailMessageBuilder.toString().replaceAll("\"",""))
    }
    jsonDataError
  }


}

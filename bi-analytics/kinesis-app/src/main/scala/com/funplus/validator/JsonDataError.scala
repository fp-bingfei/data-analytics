package com.funplus.validator

import scala.collection
import scala.collection.parallel.mutable

/**
 * Created by balaji on 3/10/15.
 */
class JsonDataError {
  var error: String = null
  var detailMessages = collection.mutable.MutableList.empty[String]

  def this(error: String) {
    this()
    this.error = error
  }

  def this(error: String, detailMessage: String) {
    this()
    this.error = error
    detailMessages += (detailMessage)
  }

  def addDetailMessage(detailMessage: String): Unit ={
    detailMessages += (detailMessage)
  }

}

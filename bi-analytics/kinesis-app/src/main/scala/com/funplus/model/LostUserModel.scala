package com.funplus.model

import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, DataFrame, SQLContext}
import org.apache.spark.{SparkContext, SparkConf, Logging}
import org.joda.time.DateTime
import scala.collection.mutable
import scala.collection.mutable.StringBuilder

/**
 * Created by zengchun on 11/24/15.
 */
object LostUserModel extends Logging {

  object schema {
    val tag = StructField("tag", DoubleType)
    val level_end = StructField("level_end", DoubleType)
    val bd_wk = StructField("bd_wk", DoubleType)
    val session_cnt = StructField("session_cnt", DoubleType)
    val age = StructField("age", DoubleType)
    val date = StructField("date", DateType)
    val snsid = StructField("snsid", StringType)
    val country = StructField("country", StringType)
    val install_date = StructField("install_date", DateType)
    val days_to_install = StructField("days_to_install", IntegerType)
    val is_payer = StructField("is_payer", IntegerType)
    val fd_wk = StructField("fd_wk", IntegerType)
    val probability = StructField("probability", DoubleType)
    val prediction = StructField("prediction", DoubleType)

    val resultStruct = StructType(Seq(tag, level_end, bd_wk, session_cnt, age, date, snsid, country,
      install_date, days_to_install, is_payer, fd_wk, probability, prediction))

    val outputStruct = StructType(Seq(StructField("date", StringType), snsid, country,
      StructField("install_date", StringType), days_to_install, is_payer, StructField("bd_wk", IntegerType),
      fd_wk, StructField("level_end", IntegerType), StructField("session_cnt", IntegerType),
      probability, StructField("prediction", IntegerType)))
  }

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 6) {
      System.err.println(
        """
          |Usage: LostUserModel <job-name> <cluster-name> <db-name> <table-name> <test-date> <output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. LostUserModel)
          |    <cluster-name> is the cluster name running Redshift
          |                 (e.g. bicluster)
          |    <db-name> is the database name in Redshift
          |                 (e.g. ffs)
          |    <table-name> is the table name in the database
          |                 (e.g. model.lost_user_model_input)
          |    <test-date> is the test date for lost user
          |                (e.g. 2015-10-01)
          |    <output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/model/output/)
        """.stripMargin)
      System.exit(1)
    }
    val Array(jobName, clusterName, dbName, tableName, testDate, outputPath) = args

    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val sqlContext = new SQLContext(sc)

    // Load data from Redshift
    val url = new StringBuilder("")
    url.append("jdbc:redshift://")
    url.append(clusterName)
    url.append(".cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/")
    url.append(dbName)
    url.append("?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true")
    val jdbcDF = sqlContext.read.format("jdbc").options(
      Map("url" ->  url.toString(),
        "dbtable" -> tableName,
        "driver" -> "com.amazon.redshift.jdbc41.Driver")).load()
    val input = jdbcDF.selectExpr("cast(case when fd_wk>1 then 1 else 0 end as double) as tag",
      "cast(level_end as double) as level_end", "cast(bd_wk as double) as bd_wk",
      "cast(session_cnt as double) as session_cnt",
      "cast(case when days_to_install>180 then 1 else 0 end as double) as age",
      "date", "snsid", "country", "install_date", "days_to_install", "is_payer", "fd_wk")

    // Split the data into training and test sets (30% held out for testing)
    //val splits = input.randomSplit(Array(0.7, 0.3))
    //val (trainingData, testData) = (splits(0), splits(1))
    // Filter data
    val testDateTime = DateTime.parse(testDate)
    val dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd")
    val trainingData = input.filter(input("date") >= dateFormat.format(testDateTime.minusDays(13).toDate)
      && input("date") <= dateFormat.format(testDateTime.minusDays(7).toDate))
    val testData = input.filter(input("date") === dateFormat.format(testDateTime.toDate))

    // Predict lost user for each country
    val country = input.select("country").distinct.collect()
    var model_output = sqlContext.createDataFrame(sc.emptyRDD[Row], schema.outputStruct)
    country.foreach(c => {
      val train = trainingData.filter(trainingData("country") === c.getString(0))
      val test = testData.filter(testData("country") === c.getString(0))

      if(train.count() > 0 && test.count() > 0) {
        // Train a RandomForest model.
        //  Empty categoricalFeaturesInfo indicates all features are continuous.
        val numClasses = 2
        val categoricalFeaturesInfo = Map[Int, Int]()
        val numTrees = 500 // Use more in practice.
        val featureSubsetStrategy = "auto" // Let the algorithm choose.
        val impurity = "gini"
        val maxDepth = 4
        val maxBins = 32

        // Map feature names to indices
        val featureInd = List("level_end", "bd_wk", "session_cnt", "age").map(input.columns.indexOf(_))
        // Get index of target
        val targetInd = input.columns.indexOf("tag")

        val trainRDD = train.rdd.map(r => LabeledPoint(
          r.getDouble(targetInd), // Get target value
          // Map feature indices to values
          Vectors.dense(featureInd.map(r.getDouble(_)).toArray)
        ))
        val testRDD = test.rdd.map(r => (LabeledPoint(
          r.getDouble(targetInd), // Get target value
          // Map feature indices to values
          Vectors.dense(featureInd.map(r.getDouble(_)).toArray)),
          r.toSeq)
        )

        // Training model
        val model = RandomForest.trainClassifier(trainRDD, numClasses, categoricalFeaturesInfo,
          numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins)

        // Evaluate model on test instances and compute test error
        val labelAndPreds = testRDD.map { case (point, seq) =>
          val votes = mutable.Map.empty[Int, Double]
          val treeWeights = Array.fill(model.trees.length)(1.0)
          model.trees.view.zip(treeWeights).foreach { case (tree, weight) =>
            val prediction = tree.predict(point.features).toInt
            votes(prediction) = votes.getOrElse(prediction, 0.0) + weight
          }
          val probability = votes.getOrElse(1, 0.0) / votes.values.sum
          val prediction = if(probability > 0.1) 1.0 else 0.0
          //val prediction = model.predict(point.features)
          (point.label, prediction, probability, seq)
        }
        //val testErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / testRDD.count()
        //println("Test Error = " + testErr)
        //println("Learned classification forest model:\n" + model.toDebugString)

        val zeroCount = labelAndPreds.filter(r => r._1 == 0.0 && r._2 == 0.0).count.toDouble
        val recallCount = labelAndPreds.filter(r => r._1 == 0.0).count.toDouble
        val precisionCount = labelAndPreds.filter(r => r._2 == 0.0).count.toDouble
        logInfo("Lost User Prediction Recall = " + (zeroCount / recallCount))
        logInfo("Lost User Prediction Precision = " + (zeroCount / precisionCount))

        // Output model prediction
        val result = labelAndPreds.map { case (label, prediction, probability, seq) =>
          Row.fromSeq(seq :+ probability :+ prediction)
        }
        val resultDF = sqlContext.createDataFrame(result, schema.resultStruct)
        val output = resultDF.selectExpr("cast(date as string) as date", "snsid", "country",
          "cast(install_date as string) as install_date", "days_to_install", "is_payer",
          "cast(bd_wk as integer) as bd_wk", "fd_wk", "cast(level_end as integer) as level_end",
          "cast(session_cnt as integer) as session_cnt", "probability", "cast(prediction as integer) as prediction")
        // Union output
        model_output = model_output.unionAll(output)
      }
    })
    // Generate output path
    val builder = new StringBuilder(outputPath)
    if (! outputPath.endsWith("/")) {
      builder.append("/")
    }
    builder.append(testDate).append("/")
    // Save into s3 path
    model_output.write.format("json").mode("overwrite").save(builder.toString())

    sc.stop()
  }
}

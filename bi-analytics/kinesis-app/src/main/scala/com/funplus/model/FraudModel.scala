package com.funplus.model

import java.net.InetAddress

import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SQLContext}
import org.apache.spark.{Logging, SparkConf, SparkContext}
import org.joda.time.DateTime

import scala.collection.mutable
import scala.collection.mutable.StringBuilder

/**
 * Created by zengchun on 01/29/16.
 */
object FraudModel extends Logging {

  object schema {
    val flag = StructField("flag", DoubleType)
    val level_end = StructField("level_end", DoubleType)
    val session_cnt = StructField("session_cnt", DoubleType)
    val purchase_cnt = StructField("purchase_cnt", DoubleType)
    val active_days = StructField("active_days", DoubleType)
    val ip_value = StructField("ip_value", DoubleType)
    val ip_address = StructField("ip_address", StringType)
    val country = StructField("country", StringType)
    val device_name = StructField("device_name", StringType)
    val user_key = StructField("user_key", StringType)
    val snsid = StructField("snsid", StringType)
    val app_id = StructField("app_id", StringType)
    val install_source = StructField("install_source", StringType)
    val sub_publisher = StructField("sub_publisher", StringType)
    val install_date = StructField("install_date", DateType)
    val os = StructField("os", StringType)
    val os_version = StructField("os_version", StringType)
    val probability = StructField("probability", DoubleType)
    val prediction = StructField("prediction", DoubleType)

    val resultStruct = StructType(Seq(flag, level_end, session_cnt, purchase_cnt, active_days, ip_value, ip_address,
      country, device_name, user_key, snsid, app_id, install_source, sub_publisher, install_date, os, os_version,
      probability, prediction))

    val outputStruct = StructType(Seq(user_key, snsid, app_id, install_source, sub_publisher,
      StructField("install_date", StringType), ip_address, country, device_name, os, os_version,
      StructField("level_end", IntegerType), StructField("session_cnt", IntegerType),
      StructField("purchase_cnt", IntegerType), StructField("active_days", IntegerType),
      StructField("flag", IntegerType), probability, StructField("prediction", IntegerType)))
  }

  def main(args: Array[String]): Unit = {
    /* Check that all required args were passed in. */
    if (args.length < 6) {
      System.err.println(
        """
          |Usage: FraudModel <job-name> <cluster-name> <db-name> <table-name> <test-date> <output-path>
          |
          |    <job-name> is the name of the job
          |                 (e.g. FraudModel)
          |    <cluster-name> is the cluster name running Redshift
          |                 (e.g. bicluster)
          |    <db-name> is the database name in Redshift
          |                 (e.g. ffs)
          |    <table-name> is the table name in the database
          |                 (e.g. processed.training_data)
          |    <test-date> is the test date for fraud installs
          |                (e.g. 2016-01-26)
          |    <output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/dev/test/model/output/)
        """.stripMargin)
      System.exit(1)
    }
    val Array(jobName, clusterName, dbName, tableName, testDate, outputPath) = args

    val sparkConf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(sparkConf)
    val sqlContext = new SQLContext(sc)

    // Load data from Redshift
    val url = new StringBuilder("")
    url.append("jdbc:redshift://")
    url.append(clusterName)
    url.append(".cpaytjecvzyu.us-west-2.redshift.amazonaws.com:5439/")
    url.append(dbName)
    url.append("?user=biadmin&password=Halfquest_2014&tcpKeepAlive=true")
    val jdbcDF = sqlContext.read.format("jdbc").options(
      Map("url" ->  url.toString(),
        "dbtable" -> tableName,
        "driver" -> "com.amazon.redshift.jdbc41.Driver")).load()
    // Register UDF function
    sqlContext.udf.register("packIPAddress", packIPAddress _)
    val input = jdbcDF.selectExpr("cast(flag as double) as flag",
      "cast(level_end as double) as level_end", "cast(session_cnt as double) as session_cnt",
      "cast(purchase_cnt as double) as purchase_cnt", "cast(active_days as double) as active_days",
      "cast(packIPAddress(ip_address) as double) as ip_value", "ip_address", "country", "device_name",
      "user_key", "snsid", "app_id", "install_source", "sub_publisher", "install_date", "os", "os_version")

    // Split the data into training and test sets (30% held out for testing)
    //val splits = input.randomSplit(Array(0.7, 0.3))
    //val (trainingData, testData) = (splits(0), splits(1))
    // Filter data
    val testDateTime = DateTime.parse(testDate)
    val dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd")
    val trainingData = input.filter(input("install_date") >= dateFormat.format(testDateTime.minusDays(13).toDate)
      && input("install_date") <= dateFormat.format(testDateTime.minusDays(7).toDate))
    val testData = input.filter(input("install_date") === dateFormat.format(testDateTime.toDate))

    // Predict fraud installs
    if(trainingData.count() > 0 && testData.count() > 0) {
      // Train a RandomForest model.
      //  Empty categoricalFeaturesInfo indicates all features are continuous.
      val numClasses = 2
      val categoricalFeaturesInfo = Map[Int, Int]()
      val numTrees = 500 // Use more in practice.
      val featureSubsetStrategy = "auto" // Let the algorithm choose.
      val impurity = "gini"
      val maxDepth = 4
      val maxBins = 32

      // Map feature names to indices
      val featureInd = List("level_end", "session_cnt", "purchase_cnt", "active_days",
        "ip_value").map(input.columns.indexOf(_))
      // Get index of target
      val targetInd = input.columns.indexOf("flag")

      val trainRDD = trainingData.rdd.map(r => LabeledPoint(
        r.getDouble(targetInd), // Get target value
        // Map feature indices to values
        Vectors.dense(featureInd.map(r.getDouble(_)).toArray)
      ))
      val testRDD = testData.rdd.map(r => (LabeledPoint(
        r.getDouble(targetInd), // Get target value
        // Map feature indices to values
        Vectors.dense(featureInd.map(r.getDouble(_)).toArray)),
        r.toSeq)
      )

      // Training model
      val model = RandomForest.trainClassifier(trainRDD, numClasses, categoricalFeaturesInfo,
        numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins)

      // Evaluate model on test instances and compute test error
      val labelAndPreds = testRDD.map { case (point, seq) =>
        val votes = mutable.Map.empty[Int, Double]
        val treeWeights = Array.fill(model.trees.length)(1.0)
        model.trees.view.zip(treeWeights).foreach { case (tree, weight) =>
          val prediction = tree.predict(point.features).toInt
          votes(prediction) = votes.getOrElse(prediction, 0.0) + weight
        }
        val probability = votes.getOrElse(1, 0.0) / votes.values.sum
        val prediction = if (probability > 0.1) 1.0 else 0.0
        //val prediction = model.predict(point.features)
        (point.label, prediction, probability, seq)
      }
      //val testErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / testRDD.count()
      //println("Test Error = " + testErr)
      //println("Learned classification forest model:\n" + model.toDebugString)

      val oneCount = labelAndPreds.filter(r => r._1 == 1.0 && r._2 == 1.0).count.toDouble
      val recallCount = labelAndPreds.filter(r => r._1 == 1.0).count.toDouble
      val precisionCount = labelAndPreds.filter(r => r._2 == 1.0).count.toDouble
      logInfo("One Count = " + oneCount)
      logInfo("Recall Count = " + recallCount)
      logInfo("Precision Count = " + precisionCount)
      logInfo("Fraud Prediction Recall = " + (oneCount / recallCount))
      logInfo("Fraud Prediction Precision = " + (oneCount / precisionCount))

      // Output model prediction
      val result = labelAndPreds.map { case (label, prediction, probability, seq) =>
        Row.fromSeq(seq :+ probability :+ prediction)
      }
      val resultDF = sqlContext.createDataFrame(result, schema.resultStruct)
      val output = resultDF.selectExpr("user_key", "snsid", "app_id", "install_source", "sub_publisher",
        "cast(install_date as string) as install_date", "ip_address", "country", "device_name", "os", "os_version",
        "cast(level_end as integer) as level_end", "cast(session_cnt as integer) as session_cnt",
        "cast(purchase_cnt as integer) as purchase_cnt", "cast(active_days as integer) as active_days",
        "cast(flag as integer) as flag", "probability", "cast(prediction as integer) as prediction")

      // Generate output path
      val builder = new StringBuilder(outputPath)
      if (!outputPath.endsWith("/")) {
        builder.append("/")
      }
      builder.append(testDate).append("/")
      // Save into s3 path
      output.filter(output("prediction") === 1).write.format("json").mode("overwrite").save(builder.toString())
    }

    sc.stop()
  }

  // Only for IPv4
  def packIPAddress(ip_address: String): Int = {
    var result = 0
    val inetAddress = InetAddress.getByName(ip_address)
    inetAddress.getAddress().foreach( b =>
    {
      result = result << 8 | (b & 0xFF)
    })
    result
  }

}

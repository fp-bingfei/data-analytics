package com.test

import java.nio.ByteBuffer

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.model.PutRecordRequest
import org.apache.spark.Logging

import scala.io.Source
import scala.util.Random


/**
 * Created by chunzeng on 7/9/15.
 */
object KinesisFileProducer extends Logging{

  def main(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: KinesisFileProducer <stream-name> <endpoint-url> <file-path>")
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(stream, endpoint, filePath) = args

    /* Create the Kinesis client */
    val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
    kinesisClient.setEndpoint(endpoint)

    println(s"Putting records onto stream $stream and endpoint $endpoint with $filePath");

    /* Put String records onto the stream per the given file */
    val file = Source.fromFile(filePath)
    try {
      var recordNum = 1
      file.getLines().foreach(line => {
        if (line.trim.length > 0) {
          /* Create a partitionKey based on recordNum */
          val partitionKey = s"partitionKey-$recordNum"

          /* Create a PutRecordRequest with an Array[Byte] version of the data */
          val putRecordRequest = new PutRecordRequest().withStreamName(stream)
            .withPartitionKey(partitionKey)
            .withData(ByteBuffer.wrap(line.getBytes()))

          /* Put the record onto the stream and capture the PutRecordResult */
          val putRecordResult = kinesisClient.putRecord(putRecordRequest)

          /* Sleep for 20 milliseconds */
          Thread.sleep(20)
          println(recordNum + " " + line)
          recordNum = recordNum + 1
        }
      })
    } finally {
      file.close()
    }
  }
}
package com.test

import java.util.Properties
import java.util.concurrent.{Executors, ExecutorService}

import com.funplus.util.CopyToRedshift
import org.apache.spark.Logging

/**
 * Created by balaji on 6/30/15.
 */
object CopyToRedshiftLoadPerformance extends Logging{


  def main (args: Array[String]): Unit = {

    logInfo("CopyToRedshiftLoadPerformance Started")

    val range = args(0).toInt
    val options = Array("GZIP" ,"EMPTYASNULL", "TRUNCATECOLUMNS", "maxerror 5")
    var table = "third_party.appannie_ranking_raw_test"
    val s3File = "s3://com.funplus.datawarehouse/third-party/appannie/iOS/copy/data1.tsv.gz"

    if(args.length == 2)
      table = args(1)

    val properties = new Properties()
    properties.load(this.getClass.getClassLoader.getResourceAsStream("common.properties"))
    val propertiesOption = Some(properties)

    val copyToRedshift = new CopyToRedshift("\t", options, propertiesOption )

    //At max can submit 100 threads
    val pool: ExecutorService = Executors.newFixedThreadPool(100)

    for (i <- Range (0, range)){
      pool.execute(new CopyToRedshiftRunnable(i, table, copyToRedshift, s3File))
    }

    pool.shutdown()

    while (!pool.isTerminated()) {
    }

    logInfo("CopyToRedshiftLoadPerformance Completed")

  }

}

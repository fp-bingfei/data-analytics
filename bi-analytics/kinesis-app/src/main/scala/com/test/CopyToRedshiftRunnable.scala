package com.test

import com.funplus.util.CopyToRedshift

/**
 * Created by balaji on 6/30/15.
 */
class CopyToRedshiftRunnable(val id: Integer, val table: String,
                             val copyToRedshift: CopyToRedshift, val s3File: String ) extends Runnable{

  override def run(): Unit = {
    copyToRedshift.run(id, s3File, table, null)
  }

}

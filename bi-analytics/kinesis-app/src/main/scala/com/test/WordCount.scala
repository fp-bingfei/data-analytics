package com.test

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import com.amazonaws.util.json.JSONObject
import org.apache.spark.rdd.RDD
import org.apache.hadoop.fs.{Path, FileUtil, FileSystem}
import org.apache.hadoop.conf.Configuration
import java.net.URI
import com.funplus.parser.EventParser
import com.funplus.util.{Utilities, EventParserUtils}
import scala.collection.mutable.ArrayBuffer


object WordCount{
  def main(args: Array[String]): Unit = {
    if (args.length < 4) {
      System.err.println(
        """
          |Usage: CustomEventsParser <job-name> <base-input-path> <base-output-path> <scratch-output-path> [<app_ids>]
          |
          |    <job-name> is the name of the job
          |                 (e.g. farm_07-01-2015_rerun)
          |    <date> is the data date to be processed
          |                 (e.g. 2015-07-01)
          |    <base-input-path> is the base input s3 path
          |                 (e.g. s3n://com.funplusgame.bidata/logserver/custom/{app_id}/)
          |    <base-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/)
          |    <scratch-output-path> is the base output s3 path
          |                 (e.g. s3n://com.funplus.datawarehouse/{app}_2_0/custom/scratch/)
          |    <app_ids> is the app_ids to be processed in comma(,) seperated
          |                 (e.g. farm.nl.prod,farm.tw.prod)
        """.stripMargin)
      System.exit(1)
    }

    val jobName = args(0)
    val date = args(1)
    val baseInputPath = args(2)
    val scratchOutputPath = args(3)
    val appIds = if (args.length > 4) args(4) else ""

    val logFile = "/Users/funplus/Downloads/archive" // Should be some file on your system
    val conf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(conf)
    //val inputPaths = generateInputPaths(date, baseInputPath, appIds)
    //val inputRDD = sc.textFile(inputPaths)
    //val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    //import sqlContext.implicits._
    val logData = sc.textFile(baseInputPath)
    //val logData = sc.textFile(logFile)
    println(logData.first())

    //val testJson = new JSONObject(logData.first())

    //given a path 'attrs' to the json array of jsonObj, extract the values of k, v (v is int) and put (k,v) into a list
    def getKVfromJSONArray(attrs: Array[String], jsonObj: JSONObject, k: String, v: String) : List[(String, Int)] = {
      var output = Nil : List[(String, Int)]
      var j = jsonObj
      for (i<- 0 until attrs.length){
        if (i < attrs.length-1) {
          j = j.optJSONObject(attrs(i))
        }
        else {
          val arrayObj = j.optJSONArray(attrs(i))
          if (arrayObj != null) {
            for (i <- 0 until arrayObj.length()) {
              output = (arrayObj.getJSONObject(i).optString(k), arrayObj.getJSONObject(i).optInt(v, 0)) :: output
            }
          }
        }
      }

      return output
    }

    //given a list of path 'attrs' to jsonObj, extract the relevant attributes (as string) as columns in the resultant table
    def getTupleFromJson(attrs: List[Array[String]], jsonObj: JSONObject): List[String] = {
      var output = Nil: List[String]
      for (l<- attrs){
        var j = jsonObj
        for (i<- 0 until l.length){
          if (i < l.length-1) {
            j = j.optJSONObject(l(i))
          }
          else {
            output = j.optString(l(i)) :: output
          }
        }
      }
      return output.reverse
    }

    def combine(x: List[String], y: List[(String, Int)]): List[(List[String], Int)]= {
      y.map(z => (x :+ z._1, z._2))
    }

    val t0 = System.currentTimeMillis()

    /*.flatMap(record => {
      try{
        Some(EventParser.parse((record)))
      } catch {
        case e: Exception => {
          println("Invalid: "+record)
          None}
      }
    })*/
    val total: RDD[(List[String], Int)] = logData.flatMap(x => {
      val jsonObj = new JSONObject(x)
      val columnTuples = getTupleFromJson(List(Array("app_id"), Array("properties", "level")), jsonObj)
      val parsedArray = getKVfromJSONArray(Array("properties", "c_currency_spent"), jsonObj, "d_currency_type", "m_currency_amount")
      combine(columnTuples, parsedArray)}).reduceByKey(_+_)

    // Import Row.
    import org.apache.spark.sql.Row

    // Import Spark SQL data types
    //import org.apache.spark.sql.types.{StructType,StructField,StringType,IntegerType}

    //val schemaMap = (List(Array("app", Array("app_id"), StringType), Array("level", Array("properties", "level"), StringType)),)

    //Map("app"-> (Array("app_id"), StringType), "level"-> (Array("properties", "level"), StringType), "currency"->StringType, "amount"->IntegerType)
    // Generate the schema based on the string of schema
    /*val schema =
      StructType(
        schemaMap.keys.toArray.map(k => StructField(k, schemaMap(k), true))
      )*/

    total.map(x => (x._1 :+ x._2).mkString("\t")).saveAsTextFile(scratchOutputPath)

    val hadoopConfig = new Configuration()
    merge(scratchOutputPath, scratchOutputPath+"_combined", true)(hadoopConfig)

    /*val RDD = total.map(p => Row.fromSeq(p._1 :+ p._2))
    val df = sqlContext.createDataFrame(RDD, schema)
    df.show()*/

    var t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) + "ms")

    /*for (z<-total.collect()){
      println(z._1.mkString(" ")+" "+z._2)
    }*/

    //total.flatMap(x => x._1 :+ x._2)
    //println(total.first())


    def flatten(ls: List[Any]): List[Any] = ls flatMap {
      case i: List[_] => flatten(i)
      case e => List(e)
    }

    t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) + "ms")
  }

  private def generateInputPaths(date:String, baseInputPath:String, appIds:String) : String = {
    val appIdArray = appIds.split(",")
    var inputPaths = baseInputPath
    if (appIdArray.size > 0) {
      val datePath = "/".concat(date.replaceAll("-", "/")).concat("/").concat("*")
      val inputPathBuffer = new ArrayBuffer[String](appIdArray.size)
      appIdArray.foreach {
        appId => {
          inputPathBuffer += baseInputPath.concat(appId).concat(datePath)
        }
      }
      inputPaths = inputPathBuffer.toArray.mkString(",")
    }
    inputPaths
  }

  def merge(srcPath: String, destPath: String, deleteOriginal: Boolean = false)
           (implicit hadoopConfig: Configuration) = {
    val srcFs = FileSystem.get(URI.create(srcPath), hadoopConfig)
    val destFs = FileSystem.get(URI.create(destPath), hadoopConfig)
    FileUtil.copyMerge(
      srcFs, new Path(srcPath),
      destFs, new Path(destPath),
      deleteOriginal, hadoopConfig, null)
  }

}

package com.test

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.services.kinesis.AmazonKinesisClient
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream
import com.amazonaws.util.json.JSONObject
import com.funplus.format.RDDMultipleTextFileOutputFormat
import com.funplus.util.EventParserUtils
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.StreamingContext.toPairDStreamFunctions
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.kinesis.KinesisUtils
import org.apache.spark.streaming.{Milliseconds, StreamingContext}
import org.apache.spark.{Logging, SparkConf}

import scala.compat.Platform.EOL

/**
 * Created by chunzeng on 9/16/15.
 */
object KinesisStreamChecker extends Logging {
  def main(args: Array[String]) {
    /* Check that all required args were passed in. */
    if (args.length < 2) {
      System.err.println(
        """
          |Usage: KinesisStreamChecker <stream-name> <endpoint-url>
          |
          |    <stream-name> is the name of the Kinesis stream, support multiple streams delimited by comma
          |                  (e.g. myFirstStream)
          |    <endpoint-url> is the endpoint of the Kinesis service
          |                   (e.g. https://kinesis.us-west-2.amazonaws.com)
        """.stripMargin)
      System.exit(1)
    }

    /* Populate the appropriate variables from the given args */
    val Array(streamNameList, endpointUrl) = args

    // Loop Kinesis streams
    val streamList = streamNameList.split(",").toList
    streamList.foreach(streamName => {
      /* Determine the number of shards from the stream */
      val kinesisClient = new AmazonKinesisClient(new DefaultAWSCredentialsProviderChain())
      kinesisClient.setEndpoint(endpointUrl)
      val numShards = kinesisClient.describeStream(streamName).getStreamDescription().getShards()
        .size()

      /* Print shard number */
      val numStreams = numShards
      println(s"$streamName shard number: $numStreams")
    })
  }
}

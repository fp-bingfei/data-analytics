package com.funplus.kinesis.sample_es.consumer.model;

import java.io.Serializable;

/**
 * 
 * This is the data model for the objects being sent through the Amazon Kinesis streams in the samples
 * 
 */
public class KinesisMessageModel implements Serializable {
	
    public String dataBlob;

    /**
     * Default constructor for Jackson JSON mapper - uses bean pattern.
     */
    public KinesisMessageModel() {

    }

    /**
     * 
     * @param dataBlob
     *        Sample String data field
     */
    public KinesisMessageModel(
            String dataBlob
            ) {
        this.dataBlob = dataBlob;
    }

    @Override
    public String toString() {
            return dataBlob;
    }

   

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dataBlob == null) ? 0 : dataBlob.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof KinesisMessageModel)) {
            return false;
        }
        KinesisMessageModel other = (KinesisMessageModel) obj;
        return true;
    }
}

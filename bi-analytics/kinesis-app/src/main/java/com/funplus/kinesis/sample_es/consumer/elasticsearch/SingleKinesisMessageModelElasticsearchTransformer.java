package com.funplus.kinesis.sample_es.consumer.elasticsearch;

import com.amazonaws.services.kinesis.connectors.elasticsearch.ElasticsearchObject;
import com.amazonaws.services.kinesis.connectors.interfaces.ITransformer;
import com.amazonaws.services.kinesis.model.Record;
import com.funplus.kinesis.sample_es.consumer.model.KinesisMessageModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/**
 * Extends KinesisMessageModelElasticsearchTransformer and implements ITransformer
 * to provide a toClass method to transform an Amazon Kinesis Record into a KinesisMessageModel.
 * 
 * To see how this record was put, view {@class samples.StreamSource}.
 */
public class SingleKinesisMessageModelElasticsearchTransformer extends KinesisMessageModelElasticsearchTransformer
        implements ITransformer<KinesisMessageModel, ElasticsearchObject> {
    private static final Log LOG = LogFactory.getLog(SingleKinesisMessageModelElasticsearchTransformer.class);
    
    private final CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();


    //input from kinesis stream i.e. input
    @Override
    public KinesisMessageModel toClass(Record record) throws IOException {
        	KinesisMessageModel km = new KinesisMessageModel();
        	km.dataBlob = decoder.decode(record.getData()).toString();
        	//System.out.println("Data Blob Value is:"+km.dataBlob);
            return km;
    }

}

package com.funplus.kinesis.sample_es.consumer.elasticsearch;

import com.amazonaws.services.kinesis.connectors.KinesisConnectorConfiguration;
import com.amazonaws.services.kinesis.connectors.elasticsearch.ElasticsearchEmitter;
import com.amazonaws.services.kinesis.connectors.elasticsearch.ElasticsearchObject;
import com.amazonaws.services.kinesis.connectors.impl.AllPassFilter;
import com.amazonaws.services.kinesis.connectors.impl.BasicMemoryBuffer;
import com.amazonaws.services.kinesis.connectors.interfaces.*;
import com.funplus.kinesis.sample_es.consumer.model.KinesisMessageModel;

public class ElasticsearchPipeline implements IKinesisConnectorPipeline<KinesisMessageModel, ElasticsearchObject> {

    @Override
    public IEmitter<ElasticsearchObject> getEmitter(KinesisConnectorConfiguration configuration) {
        return new ElasticsearchEmitter(configuration);
    }

    @Override
    public IBuffer<KinesisMessageModel> getBuffer(KinesisConnectorConfiguration configuration) {
        return new BasicMemoryBuffer<KinesisMessageModel>(configuration);
    }

    @Override
    public ITransformerBase<KinesisMessageModel, ElasticsearchObject>
            getTransformer(KinesisConnectorConfiguration configuration) {
      
            return new SingleKinesisMessageModelElasticsearchTransformer();
        
    }

    @Override
    public IFilter<KinesisMessageModel> getFilter(KinesisConnectorConfiguration configuration) {
        return new AllPassFilter<KinesisMessageModel>();
    }

}

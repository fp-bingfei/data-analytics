package com.funplus.kinesis.sample_es.consumer.elasticsearch;

import com.amazonaws.services.kinesis.connectors.elasticsearch.ElasticsearchObject;
import com.amazonaws.services.kinesis.connectors.elasticsearch.ElasticsearchTransformer;
import com.funplus.kinesis.sample_es.consumer.model.KinesisMessageModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Extends ElasticsearchTransformer for {@link KinesisMessageModel}. Provides implementation for fromClass by
 * transforming the record into JSON format and setting the index, type and id to use for Elasticsearch.
 * 
 * Abstract to give same implementation of fromClass for both Batched and Single processing scenarios.
 */
public abstract class KinesisMessageModelElasticsearchTransformer extends ElasticsearchTransformer<KinesisMessageModel> {
    private static final Log LOG = LogFactory.getLog(KinesisMessageModelElasticsearchTransformer.class);

    private static final String INDEX_NAME = "bv-prod";
    Object obj;
    JSONParser parser = new JSONParser();
    JSONObject jsonObject;
    String time, formattedTime;
    Date d;
    SimpleDateFormat sdf,output;

    /**
     * convert kinesis msg model to elastic search obj ie. output
     * record by record
     */
    @Override
    public ElasticsearchObject fromClass(KinesisMessageModel record) throws IOException {
        String index = getIndexDaily(record.dataBlob); // like a db
        String type = "bv-game-events"; // like a table
        //record.getClass().getSimpleName();
        String id = null;
        String source = null;
        boolean create = false; //User Modified
        source = record.dataBlob;
        ElasticsearchObject elasticsearchObject = new ElasticsearchObject(index, type, id, source);
        elasticsearchObject.setCreate(create);

        return elasticsearchObject;
    }
    
    public String getIndexDaily(String jsonBlob){  	
    	
    	try {
			jsonObject = (JSONObject) parser.parse(jsonBlob);
			
			time = (String) jsonObject.get("time");
			sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
			output = new SimpleDateFormat("yyyy.MM.dd");
			output.setTimeZone(TimeZone.getTimeZone("UTC"));
			
			d = sdf.parse(time);		 
			formattedTime = output.format(d);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//System.out.println(INDEX_NAME+"-"+formattedTime);
    	return INDEX_NAME+"-"+formattedTime;
    }
}

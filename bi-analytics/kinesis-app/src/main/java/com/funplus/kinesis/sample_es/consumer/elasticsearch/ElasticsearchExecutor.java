package com.funplus.kinesis.sample_es.consumer.elasticsearch;

import com.amazonaws.services.kinesis.connectors.KinesisConnectorRecordProcessorFactory;
import com.amazonaws.services.kinesis.connectors.elasticsearch.ElasticsearchObject;
import com.funplus.kinesis.sample_es.consumer.model.KinesisConnectorExecutor;
import com.funplus.kinesis.sample_es.consumer.model.KinesisMessageModel;

public class ElasticsearchExecutor extends KinesisConnectorExecutor<KinesisMessageModel, ElasticsearchObject> {
    private static String configFile = "sample_es-consumer-app.properties";

    /**
     * Creates a new ElasticsearchExecutor.
     * 
     * @param configFile The name of the configuration file to look for on the classpath.
     */
    public ElasticsearchExecutor(String configFile) {
        super(configFile);
    }

    @Override
    public KinesisConnectorRecordProcessorFactory<KinesisMessageModel, ElasticsearchObject>
            getKinesisConnectorRecordProcessorFactory() {
        return new KinesisConnectorRecordProcessorFactory<KinesisMessageModel, ElasticsearchObject>(new ElasticsearchPipeline(),
                config);
    }

    /**
     * Main method starts and runs the ElasticsearchExecutor.
     * 
     * @param args
     */
    public static void main(String[] args) {
        try {
            Class.forName("org.elasticsearch.client.transport.TransportClient");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Could not load Elasticsearch jar", e);
        }
        try {
            Class.forName("org.apache.lucene.util.Version");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Could not load Apache Lucene jar", e);
        }

        KinesisConnectorExecutor<KinesisMessageModel, ElasticsearchObject> elasticsearchExecutor =
                new ElasticsearchExecutor(configFile);
        elasticsearchExecutor.run();
    }

}

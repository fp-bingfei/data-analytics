package com.funplus.kinesis.sample.producer;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.amazonaws.services.kinesis.model.PutRecordResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.ByteBuffer;

public class AmazonKinesisProducer {

	static AmazonKinesisClient kinesisClient;
	private static final Log LOG = LogFactory
			.getLog(AmazonKinesisProducer.class);

	private static void init() throws Exception {

		/*
		 * The ProfileCredentialsProvider will return your [default] credential
		 * profile by reading from the credentials file located at
		 * (/Users/<user>/.aws/credentials).
		 */
		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider("default")
					.getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException(
					"Cannot load the credentials from the credential profiles file. "
							+ "Please make sure that your credentials file is at the correct "
							+ "location (/Users/<user>/.aws/credentials), and is in valid format.",
					e);
		}

		kinesisClient = new AmazonKinesisClient(credentials);
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		kinesisClient.setRegion(usWest2);// Set region
	}

	public static void main(String[] args) throws Exception {
		init();

		final String myStreamName = "myFirstStream";
		final Integer myStreamSize = 1;
		
	
		 // Create a stream. The number of shards determines the provisioned throughput.

        /*CreateStreamRequest createStreamRequest = new CreateStreamRequest();
        createStreamRequest.setStreamName(myStreamName);
        createStreamRequest.setShardCount(myStreamSize);

        kinesisClient.createStream(createStreamRequest);
        // The stream is now being created.
        LOG.info("Creating Stream : " + myStreamName);
        waitForStreamToBecomeAvailable(myStreamName);

        // list all of my streams
        ListStreamsRequest listStreamsRequest = new ListStreamsRequest();
        listStreamsRequest.setLimit(10);
        ListStreamsResult listStreamsResult = kinesisClient.listStreams(listStreamsRequest);
        List<String> streamNames = listStreamsResult.getStreamNames();
        while (listStreamsResult.isHasMoreStreams()) {
            if (streamNames.size() > 0) {
                listStreamsRequest.setExclusiveStartStreamName(streamNames.get(streamNames.size() - 1));
            }

            listStreamsResult = kinesisClient.listStreams(listStreamsRequest);
            streamNames.addAll(listStreamsResult.getStreamNames());

        }
        LOG.info("Printing my list of streams : ");

        // print all of my streams.
        if (!streamNames.isEmpty()) {
            System.out.println("List of my streams: ");
        }
        for (int i = 0; i < streamNames.size(); i++) {
            System.out.println(streamNames.get(i));
        }*/
        

		LOG.info("Putting records in stream : " + myStreamName);
		// Write 10 records to the stream
		for (int j = 1; j < 20; j++) {
			PutRecordRequest putRecordRequest = new PutRecordRequest();
			putRecordRequest.setStreamName(myStreamName);
			putRecordRequest.setData(ByteBuffer.wrap(String.format(
					"testData-%d", j).getBytes()));
			putRecordRequest.setPartitionKey(String
					.format("partitionKey-%d", j));
			PutRecordResult putRecordResult = kinesisClient
					.putRecord(putRecordRequest);
			System.out.println("Successfully putrecord, partition key : "
					+ putRecordRequest.getPartitionKey() + ", ShardID : "
					+ putRecordResult.getShardId());
		}

		/*
		 * // Delete the stream. LOG.info("Deleting stream : " + myStreamName);
		 * DeleteStreamRequest deleteStreamRequest = new DeleteStreamRequest();
		 * deleteStreamRequest.setStreamName(myStreamName);
		 * 
		 * kinesisClient.deleteStream(deleteStreamRequest); // The stream is now
		 * being deleted. LOG.info("Stream is now being deleted : " +
		 * myStreamName);
		 */
	}
}

package com.funplus.util

import com.amazonaws.util.json.JSONObject
import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.junit.JUnitRunner

/**
 * Created by balaji on 3/16/15.
 */

@RunWith(classOf[JUnitRunner])
class CountryCodeLookupServiceTest extends FlatSpec with Matchers{

  "CountryCodeLookupService" should "return correct country code for given ip address" in {
    CountryCodeLookupService.getCountryCode(Some("187.188.72.17")) should be ("MX")
    CountryCodeLookupService.getCountryCode(Some("36.44.124.37")) should be ("CN")
  }


  it should "return empty country code if given ip is empty or response not available" in {
    CountryCodeLookupService.getCountryCode(Some("")) should be ("")
    CountryCodeLookupService.getCountryCode(Some("testIp")) should be ("")

  }

  it should "include \"country_code\" response in given inputJson" in {
    val jsonObj = new JSONObject("{ properties :{\"os\":\"ios\",\"os_version\":\"8.1.3\",\"ip\":\"163.23.70.129\"}}")
    val properties: JSONObject = jsonObj.getJSONObject("properties")
    CountryCodeLookupService.includeCountryCodeInJson(properties)
    jsonObj.toString should be ("{\"properties\":{\"os\":\"ios\",\"country_code\":\"TW\",\"ip\":\"163.23.70.129\",\"os_version\":\"8.1.3\"}}")
  }

}

package com.funplus.parser

import com.amazonaws.util.json.{JSONException, JSONObject}
import com.typesafe.config._
import org.junit.runner.RunWith
import org.scalatest.{Matchers, FlatSpec}
import org.scalatest.junit.JUnitRunner
import collection.JavaConversions._

/**
 * Created by balaji on 3/9/15.
 */

@RunWith(classOf[JUnitRunner])
class EventParserTest extends FlatSpec with Matchers{

  "EventParser" should "be success if basic mandatory properties are available" in {
    val inputJson = "{\"@key\":\"bv.global.prod\",\"@ts\":\"1426033532\", \"bi_version\":\"1.2\",\"app_id\":\"bv.global.prod\",\"ts\":1426033531,\"event\":\"session_start\",\"user_id\":\"713803\",\"snsid\":\"0d3b4912f29273ba49ea493a438c4cc2\",\"session_id\":\"DD437DCF-8834-4B5B-9950-38AE0D9E9AD8_1426033532\"}"
    val outputJson = EventParser.parse(inputJson)
    val outputJsonObject = new JSONObject(outputJson)
    a [JSONException] should be thrownBy {
      outputJsonObject.get("error")
    }
    a [JSONException] should be thrownBy {
      outputJsonObject.getJSONArray("errorDetails")
    }
  }

  /*
  ** NOT IN USE
  "EventParser" should "be success for bi_version 1.1" in {
    val inputJson = "{\"uid\":10783946,\"os\":\"Windows 7\",\"@key\":\"farm.us.prod\",\"location\":\"Storage\",\"@ts\":\"1417327194\",\"item_out\":1,\"@ts_pretty\":\"2014-11-29 21:59:54\",\"country_code\":\"\",\"lang\":\"am\",\"os_version\":\"\",\"ip\":\"\",\"browser_version\":\"33.0.1750\",\"snsid\":\"100005370640350\",\"install_ts_pretty\":\"2013-03-22 06:05:33\",\"install_ts\":1363957533,\"level\":51,\"item_name\":\"Mario's Sausage Pizza\",\"item_id\":52016,\"item_class\":\"c\",\"event\":\"item_transaction\",\"browser\":\"Chrome\",\"action\":\"sold\",\"install_source\":\"\",\"item_type\":\"products\"}"
    val expectedOutputJson = "{\"uid\":10783946,\"location\":\"Storage\",\"item_out\":1,\"lang\":\"am\",\"install_ts_pretty\":\"2013-03-22 06:05:33\",\"bi_version\":\"1.1\",\"install_ts\":1363957533,\"level\":51,\"event\":\"item_transaction\",\"browser\":\"Chrome\",\"action\":\"sold\",\"install_source\":\"\",\"item_type\":\"products\",\"os\":\"Windows 7\",\"@key\":\"farm.us.prod\",\"@ts\":\"1417327194\",\"@ts_pretty\":\"2014-11-29 21:59:54\",\"country_code\":\"\",\"ip\":\"\",\"os_version\":\"\",\"browser_version\":\"33.0.1750\",\"snsid\":\"100005370640350\",\"item_name\":\"Mario's Sausage Pizza\",\"item_id\":52016,\"item_class\":\"c\"}"
    val outputJson = EventParser.parse(inputJson)
    outputJson should be (expectedOutputJson)
  }
*/

  it should "return VALIDATION_FAILED for given json which has no properties" in {
    val inputJson = "{\"bi_version\":\"1.2\",\"app_id\":\"bv.global.prod\",\"ts\":1426033531,\"event\":\"load_step\",\"user_id\":\"713803\",\"snsid\":\"0d3b4912f29273ba49ea493a438c4cc2\",\"session_id\":\"DD437DCF-8834-4B5B-9950-38AE0D9E9AD8_1426033532\",\"properties\":{\"os\":\"ios\",\"os_version\":\"8.1.3\",\"device\":\"iPad2,1\",\"ip\":\"36.44.124.37\",\"lang\":\"cn\"},\"collections\":{}}"
    val outputJson = EventParser.parse(inputJson)
    val outputJsonObject = new JSONObject(outputJson)
    outputJsonObject.getString("error") should be ("VALIDATION_FAILED")
    outputJsonObject.getJSONArray("errorDetails").get(0) should be ("properties: instance failed to match exactly one schema (matched 0 out of 3)")
  }

  it should "output correct value" in {
    val conf = ConfigFactory.parseResources("spark.conf")
    val basePath = conf.getString("basePath")
    val checkpointPath = conf.getString("checkpointPath")
    val interval = conf.getString("batchInterval")
    val appName = conf.getString("appName")
    println(basePath)
    println(checkpointPath)
    println(interval)
    println(appName)
    val streams = conf.getObject("kinesisStreams")
    val streamList = streams.keys.toList
    streamList.foreach(streamName => {
      val endpointList = streams.toConfig.getStringList(streamName).toList
      endpointList.foreach(
        endpointUrl => {
          println(streamName + " " + endpointUrl)
        }
      )
    })
    if (conf.hasPath("forwardKinesisStreams")) {
      val forwardStreams = conf.getObject("forwardKinesisStreams")
      val forwardStreamList = forwardStreams.keys.toList
      forwardStreamList.foreach(streamName => {
        val endpointUrl = forwardStreams.toConfig.getString(streamName)
        println(streamName + " " + endpointUrl)
      })
    }
  }

  it should "output correct fields" in {
    val conf = ConfigFactory.parseResources("update.conf")
    val fieldList = conf.root().toList
    fieldList.foreach(field => {
      val fieldName = field._1
      val fieldValue = field._2
      fieldName match {
        case "properties" => {
          println(conf.getObject(fieldName))
        }
        case _ => {
          fieldValue.valueType() match {
            case ConfigValueType.STRING => {
              println(fieldName + "=>" + conf.getString(fieldName))
            }
            case ConfigValueType.OBJECT => {
              println(fieldName  + "=>" + conf.getString(fieldName.concat(".key"))
                + " " + conf.getString(fieldName.concat(".value")))
            }
            case _ => {
              // do nothing
            }
          }
        }
      }
    })
  }
}

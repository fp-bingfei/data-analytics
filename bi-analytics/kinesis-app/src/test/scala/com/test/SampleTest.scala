package com.test

import com.test
import org.scalatest.FlatSpec

/**
 * Created by balaji on 3/9/15.
 */
class SampleTest extends FlatSpec{

    "Adding 1 and 3" should "equals to 4" in {
      assert(test.Sample.addInt(1,3) == 4)
    }

  }
